﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace net.nimera.cmt.TicketMasterService
{
    /// <summary>
    /// Terminal admin states
    /// </summary>
    public enum AdmStatus
    {
        LOCKED = 1, OPERATIONAL = 2, DECOMMISSIONED = 3, Request = 5, Deleted = 6
    }
}
