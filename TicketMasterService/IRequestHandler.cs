﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace net.nimera.cmt.TicketMasterService
{
    /// <summary>
    /// Looksup the status of a ticket and processes the result
    /// </summary>
    interface IRequestHandler
    {
       /// <summary>
       /// Does the actual ticket processing
       /// </summary>
       /// <param name="cmtTicket"></param>
       void ProcessTicket(CmtTicket ticket);
    }
}
