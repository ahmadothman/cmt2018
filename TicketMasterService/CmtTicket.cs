﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using net.nimera.cmt.TicketMasterService.BOMonitorControlWSRef;

namespace net.nimera.cmt.TicketMasterService
{
    /// <summary>
    /// This class represents a ticket in the CMT database
    /// </summary>
    class CmtTicket
    {
        /// <summary>
        /// The ticket identifier as received from NMS or SES
        /// </summary>
        public string TicketId { get; set; }

        /// <summary>
        /// Date the ticket was created
        /// </summary>
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// The MAC address of the terminal modem
        /// </summary>
        public string MacAddress { get; set; }

        /// <summary>
        /// The source adminstate
        /// </summary>
        public AdmStatus SourceAdmState { get; set; }

        /// <summary>
        /// The target adminstate
        /// </summary>
        public AdmStatus TargetAdmState { get; set; }

        /// <summary>
        /// The ticket status (Busy, Failed)
        /// </summary>
        public CmtRequestStatus TicketStatus { get; set; }

        /// <summary>
        /// The reason for the failure
        /// </summary>
        public string FailureReason { get; set; }

        /// <summary>
        /// The failure stack trace
        /// </summary>
        public string FailureStackTrace { get; set; }

        /// <summary>
        /// The ticket activity (Suspension, re-activation ...)
        /// </summary>
        public TerminalActivityTypes TicketActivity { get; set; }

        /// <summary>
        /// The ticket SLA
        /// </summary>
        public int Sla { get; set; }

        /// <summary>
        /// The ISP identifier
        /// </summary>
        public int IspId { get; set; }

        /// <summary>
        /// The new MAC address of the terminal if the management action is a 
        /// MAC address change
        /// </summary>
        public string NewMacAddress { get; set; }

        /// <summary>
        /// Indicates whether an activation in the NMS is necessary
        /// 0 - Activity only performed on the NMS or in the SES HUB, depends on activity
        /// 1 - Poll the SES Hub for the ticket result and perform the action in the NMS
        /// 2 - Poll the NMS for the result of the action and close this ticket with the result
        /// of the NMS activity
        /// </summary>
        public short NMSActivate { get; set; }

        /// <summary>
        /// New IP Address in case of a IP Change activity
        /// </summary>
        public string NewIPAddress { get; set; }

        /// <summary>
        /// New IP Mask in case of an IP Change activity
        /// </summary>
        /// <remarks>
        /// This value can be identical to the existing value in case the mask did not change
        /// </remarks>
        public int NewIPMask { get; set; }

        /// <summary>
        /// A counter to keep track of the number of times a busy ticket has been processed
        /// This will be used to prevent the database from overflowing
        /// </summary>
        public int RetryCounter { get; set; }

        /// <summary>
        /// The Id of the user who has done something that made the ticket
        /// </summary>
        public Guid UserId { get; set; }
    
    }
}
