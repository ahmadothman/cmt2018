﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO;

namespace net.nimera.cmt.TicketMasterService
{
    /// <summary>
    /// This class contains the main TicketMaster thread
    /// </summary>
    class TicketMasterThreadMill
    {
        DataGateway _dg = null;
        int _runInterval = 0;
        string _logFilePath = "";

        /// <summary>
        /// The run flag determines allows to stop the thread
        /// </summary>
        public bool RunFlag { get; set; }

        public TicketMasterThreadMill()
        {
            RunFlag = true;
            _dg = new DataGateway();
            _runInterval = Properties.Settings.Default.Interval * 1000;
            _logFilePath = Properties.Settings.Default.LogFile;
        }

        /// <summary>
        /// Main thread
        /// </summary>
        public void Run()
        {
            while (RunFlag)
            {
                List<CmtTicket> busyTickets = _dg.getBusyTickets();

                foreach (CmtTicket ticket in busyTickets)
                {
                    ISPIFRequestHandler ispifRequestHandler = new ISPIFRequestHandler();
                    ispifRequestHandler.ProcessTicket(ticket);
                }

                Thread.Sleep(_runInterval);
            }
        }

        /// <summary>
        /// Simple log method
        /// </summary>
        /// <param name="logMsg"></param>
        private void log(string logMsg)
        {
            using (StreamWriter writer = new StreamWriter(_logFilePath, true))
            {
                writer.WriteLine("[" + DateTime.Now + "]" + logMsg);
            }
        }
    }
}
