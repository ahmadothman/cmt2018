﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using net.nimera.cmt.TicketMasterService.BOLogControlWSRef;
using net.nimera.cmt.TicketMasterService.BOMonitorControlWSRef;
using net.nimera.cmt.TicketMasterService.BOAccountingControlWSRef;

namespace net.nimera.cmt.TicketMasterService
{
    /// <summary>
    /// Contains the data support methods
    /// </summary>
    class DataGateway
    {
        BOLogControlWS _boLogControlWS = null;
        string _connectionString = "";

        public DataGateway()
        {
            _boLogControlWS = new BOLogControlWS();
            _connectionString = Properties.Settings.Default.ConnectionString;
        }

        /// <summary>
        /// Change the status of a terminal
        /// </summary>
        /// <param name="macAddress"></param>
        /// <param name="admStatus"></param>
        /// <returns></returns> 
        public bool changeTerminalStatus(string macAddress, AdmStatus admStatus)
        {
            string changeTerminalStatusCmd = "UPDATE Terminals SET AdmStatus = @AdmStatus WHERE MacAddress = @MacAddress";
            bool result = false;

            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                try
                {
                    using (SqlCommand cmd = new SqlCommand(changeTerminalStatusCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@AdmStatus", (int)admStatus);
                        cmd.Parameters.AddWithValue("@MacAddress", macAddress);
                        int numRows = cmd.ExecuteNonQuery();

                        //For NMS the ExecuteNonQuery command returns 2 rows! Not sure why.
                        if (numRows == 0)
                        {
                            result = false;
                            CmtApplicationException cmtApplicationException = new CmtApplicationException();
                            cmtApplicationException.ExceptionDateTime = DateTime.Now;
                            cmtApplicationException.ExceptionDesc = "Terminals not updated!";
                            cmtApplicationException.ExceptionStacktrace = "";
                            cmtApplicationException.UserDescription = "TicketMaster - changeTeminalStatus";
                            _boLogControlWS.LogApplicationException(cmtApplicationException);
                        }
                        else
                        {
                            result = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    CmtApplicationException cmtApplicationException = new CmtApplicationException();
                    cmtApplicationException.ExceptionDateTime = DateTime.Now;
                    cmtApplicationException.ExceptionDesc = ex.Message;
                    cmtApplicationException.ExceptionStacktrace = ex.StackTrace;
                    cmtApplicationException.UserDescription = "TicketMaster - changeTerminalStatus";
                    _boLogControlWS.LogApplicationException(cmtApplicationException);
                }
            }

            return result;
        }

        /// <summary>
        /// Changes the SLA after the operation was completed successfully by the ISPIF
        /// </summary>
        /// <param name="macAddress"></param>
        /// <param name="newSla"></param>
        /// <returns></returns>
        public bool changeTerminalSla(string macAddress, int newSla)
        {
            string changeTerminalStatusCmd = "UPDATE Terminals SET SlaID = @SlaID WHERE MacAddress = @MacAddress";
            bool result = false;

            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                try
                {
                    using (SqlCommand cmd = new SqlCommand(changeTerminalStatusCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@SlaID", newSla);
                        cmd.Parameters.AddWithValue("@MacAddress", macAddress);
                        int numRows = cmd.ExecuteNonQuery();

                        if (numRows == 0)
                        {
                            result = false;
                            CmtApplicationException cmtApplicationException = new CmtApplicationException();
                            cmtApplicationException.ExceptionDateTime = DateTime.Now;
                            cmtApplicationException.ExceptionDesc = "Terminals not updated!";
                            cmtApplicationException.ExceptionStacktrace = "";
                            cmtApplicationException.UserDescription = "TicketMaster - changeTeminalSla";
                            _boLogControlWS.LogApplicationException(cmtApplicationException);
                        }
                        else
                        {
                            result = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    CmtApplicationException cmtApplicationException = new CmtApplicationException();
                    cmtApplicationException.ExceptionDateTime = DateTime.Now;
                    cmtApplicationException.ExceptionDesc = ex.Message;
                    cmtApplicationException.ExceptionStacktrace = ex.StackTrace;
                    cmtApplicationException.UserDescription = "TicketMaster - changeTerminalSla";
                    _boLogControlWS.LogApplicationException(cmtApplicationException);
                }
            }

            return result;
        }

        /// <summary>
        /// Changes the Edge SLA after the operation was completed successfully by the ISPIF
        /// </summary>
        /// <param name="macAddress"></param>
        /// <param name="newSla"></param>
        /// <returns></returns>
        public bool changeTerminalEdgeSla(string macAddress, int newSla)
        {
            string changeTerminalStatusCmd = "UPDATE Terminals SET EdgeSlaID = @SlaID WHERE MacAddress = @MacAddress";
            bool result = false;

            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                try
                {
                    using (SqlCommand cmd = new SqlCommand(changeTerminalStatusCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@SlaID", newSla);
                        cmd.Parameters.AddWithValue("@MacAddress", macAddress);
                        int numRows = cmd.ExecuteNonQuery();

                        if (numRows == 0)
                        {
                            result = false;
                            CmtApplicationException cmtApplicationException = new CmtApplicationException();
                            cmtApplicationException.ExceptionDateTime = DateTime.Now;
                            cmtApplicationException.ExceptionDesc = "Terminals not updated!";
                            cmtApplicationException.ExceptionStacktrace = "";
                            cmtApplicationException.UserDescription = "TicketMaster - changeTeminalEdgeSla";
                            _boLogControlWS.LogApplicationException(cmtApplicationException);
                        }
                        else
                        {
                            result = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    CmtApplicationException cmtApplicationException = new CmtApplicationException();
                    cmtApplicationException.ExceptionDateTime = DateTime.Now;
                    cmtApplicationException.ExceptionDesc = ex.Message;
                    cmtApplicationException.ExceptionStacktrace = ex.StackTrace;
                    cmtApplicationException.UserDescription = "TicketMaster - changeTerminalEdgeSla";
                    _boLogControlWS.LogApplicationException(cmtApplicationException);
                }
            }

            return result;
        }

        /// <summary>
        /// Change the status of a ticket
        /// </summary>
        /// <param name="ticketId"></param>
        /// <param name="ticketStatus"></param>
        /// <returns></returns>
        public bool changeTicketStatus(CmtTicket ticket, CmtRequestStatus ticketStatus)
        {
            bool result = false;
            string changeTicketStatusCmd = "UPDATE Tickets SET TicketStatus = @TicketStatus, FailureReason = @FailureReason, FailureStackTrace = @FailureStackTrace " + 
                "WHERE TicketId = @TicketId";
            string failureReason = "";
            string failureStackTrace = "";

            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                conn.Open();

                try
                {
                    using (SqlCommand cmd = new SqlCommand(changeTicketStatusCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@TicketStatus", (int)ticketStatus);
                        cmd.Parameters.AddWithValue("@TicketId", ticket.TicketId);
                        if (ticket.FailureReason.Length > 1024)
                        {
                            failureReason = ticket.FailureReason.Substring(0, 1023);
                        }
                        else
                        {
                            failureReason = ticket.FailureReason;
                        }
                        cmd.Parameters.AddWithValue("@FailureReason", failureReason);

                        if (ticket.FailureStackTrace.Length > 1024)
                        {
                            failureStackTrace = ticket.FailureStackTrace.Substring(0, 1023);
                        }
                        else
                        {
                            failureStackTrace = ticket.FailureStackTrace;
                        }
                        cmd.Parameters.AddWithValue("@FailureStackTrace", failureStackTrace);

                        int numRows = cmd.ExecuteNonQuery();

                        if (numRows == 0)
                        {
                            result = false;
                            CmtApplicationException cmtApplicationException = new CmtApplicationException();
                            cmtApplicationException.ExceptionDateTime = DateTime.Now;
                            cmtApplicationException.ExceptionDesc = "Tickets not updated!";
                            cmtApplicationException.ExceptionStacktrace = "";
                            cmtApplicationException.UserDescription = "TicketMaster - changeTicketStatus";
                            _boLogControlWS.LogApplicationException(cmtApplicationException);
                        }
                        else
                        {
                            result = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    CmtApplicationException cmtApplicationException = new CmtApplicationException();
                    cmtApplicationException.ExceptionDateTime = DateTime.Now;
                    cmtApplicationException.ExceptionDesc = ex.Message;
                    cmtApplicationException.ExceptionStacktrace = ex.StackTrace;
                    cmtApplicationException.UserDescription = "TicketMaster - changeTicketStatus";
                    _boLogControlWS.LogApplicationException(cmtApplicationException);
                }
            }


            return result;
        }

        /// <summary>
        /// Adds a new terminal activity to the terminal activity log
        /// </summary>
        /// <param name="macAddress"></param>
        /// <param name="termActivity"></param>
        /// <returns></returns>
        public bool addTerminalActivity(string macAddress, TerminalActivityTypes termActivity, Guid userId)
        {
            bool result = false;

            BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();
            Terminal term = boAccountingControl.GetTerminalDetailsByMAC(macAddress);
            TerminalActivity terminalActivity = new TerminalActivity();
            terminalActivity.Action = termActivity.ToString();
            terminalActivity.ActionDate = DateTime.Now;
            terminalActivity.MacAddress = macAddress;
            terminalActivity.UserId = userId;
            terminalActivity.SlaName = term.SlaId.ToString();
            terminalActivity.NewSlaName = null;
            terminalActivity.AccountingFlag = true; //?????? This is not correct. Accounting flag should come from TerminalActivityTypes!
            terminalActivity.Comment = " "; // This should be set somewhere else
            terminalActivity.Prepaid = false; // Same as above
            _boLogControlWS.LogTerminalActivity(terminalActivity);

            return result;
        }

        /// <summary>
        /// Adds a new terminal activity to the terminal activity log, if the SLA is changed
        /// </summary>
        /// <param name="macAddress"></param>
        /// <param name="termActivity"></param>
        /// <param name="oldSla"></param>
        /// <returns></returns>
        public bool addTerminalActivity(string macAddress, TerminalActivityTypes termActivity, string oldSla, Guid userId)
        {
            bool result = false;

            BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();
            Terminal term = boAccountingControl.GetTerminalDetailsByMAC(macAddress);
            TerminalActivity terminalActivity = new TerminalActivity();
            terminalActivity.Action = termActivity.ToString();
            terminalActivity.ActionDate = DateTime.Now;
            terminalActivity.MacAddress = macAddress;
            terminalActivity.UserId = userId;
            terminalActivity.SlaName = oldSla;
            terminalActivity.NewSlaName = term.SlaId.ToString();
            terminalActivity.AccountingFlag = true; //?????? This is not correct. Accounting flag should come from TerminalActivityTypes!
            terminalActivity.Prepaid = false; // this should be set somewhere else
            terminalActivity.Comment = " "; // Same as above
            _boLogControlWS.LogTerminalActivity(terminalActivity);

            return result;
        }

        /// <summary>
        /// Returns the list of busy tickets
        /// </summary>
        /// <returns></returns>
        public List<CmtTicket> getBusyTickets()
        {
            string busyTicketsQueryCmd = "SELECT TicketId, CreationDate, MacAddress, SourceAdmState, TargetAdmState, "
                                           + "TicketStatus, FailureReason, FailureStackTrace, TermActivity, newSla, IspId, "
                                           + "NewMacAddress, NMSActivate, NewIPAddress, NewMask, RetryCounter, UserId FROM Tickets " 
                                           + "WHERE TicketStatus = @TicketStatus";
            List<CmtTicket> ticketIdList = new List<CmtTicket>();

            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                  conn.Open();
                  try
                  {
                    using (SqlCommand cmd = new SqlCommand(busyTicketsQueryCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@TicketStatus", (int)CmtRequestStatus.BUSY);

                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            CmtTicket cmtTicket = new CmtTicket();
                            cmtTicket.TicketId = reader.GetString(0);
                            cmtTicket.CreationDate = reader.GetDateTime(1);
                            cmtTicket.MacAddress = reader.GetString(2);
                            cmtTicket.SourceAdmState = (AdmStatus) Enum.ToObject(typeof(AdmStatus), reader.GetInt32(3));
                            cmtTicket.TargetAdmState = (AdmStatus) Enum.ToObject(typeof(AdmStatus), reader.GetInt32(4));
                            cmtTicket.TicketStatus = (CmtRequestStatus)Enum.ToObject(typeof(CmtRequestStatus), reader.GetInt32(5));
                            cmtTicket.FailureReason = reader.GetString(6);
                            cmtTicket.FailureStackTrace = reader.GetString(7);
                            cmtTicket.TicketActivity = (TerminalActivityTypes) Enum.ToObject(typeof(TerminalActivityTypes), reader.GetInt32(8));
                            if (reader.IsDBNull(9))
                            {
                                cmtTicket.Sla = 0;
                            }
                            else
                            {
                                cmtTicket.Sla = reader.GetInt32(9);
                            }
                            if (reader.IsDBNull(10))
                            {
                                cmtTicket.IspId = 112;
                            }
                            else
                            {
                                cmtTicket.IspId = reader.GetInt32(10);
                            }

                            if (reader.IsDBNull(11))
                            {
                                cmtTicket.NewMacAddress = "";
                            }
                            else
                            {
                                cmtTicket.NewMacAddress = reader.GetString(11);
                            }

                            cmtTicket.NMSActivate = reader.GetInt16(12);

                            if (reader.IsDBNull(13))
                            {
                                cmtTicket.NewIPAddress = "";
                            }
                            else
                            {
                                cmtTicket.NewIPAddress = reader.GetString(13);
                            }

                            if (!reader.IsDBNull(14))
                            {
                                cmtTicket.NewIPMask = reader.GetInt32(14);
                            }
                            else
                            {
                                cmtTicket.NewIPMask = 32;
                            }
                            if (reader.IsDBNull(15))
                            {
                                cmtTicket.RetryCounter = 0;
                            }
                            else
                            {
                                cmtTicket.RetryCounter = reader.GetInt32(15);
                            }
                            if (reader.IsDBNull(16))
                            {
                                cmtTicket.UserId = new Guid(Properties.Settings.Default.AppGuid);
                            }
                            else
                            {
                                cmtTicket.UserId = reader.GetGuid(16);
                            }

                            ticketIdList.Add(cmtTicket);
                        }
                    }
                  }
                  catch (Exception ex)
                  {
                      CmtApplicationException cmtApplicationException = new CmtApplicationException();
                      cmtApplicationException.ExceptionDateTime = DateTime.Now;
                      cmtApplicationException.ExceptionDesc = ex.Message;
                      cmtApplicationException.ExceptionStacktrace = ex.StackTrace;
                      cmtApplicationException.UserDescription = "TicketMaster - getBusyTickets";
                      _boLogControlWS.LogApplicationException(cmtApplicationException);
                  }
            }

            return ticketIdList;
        }

        /// <summary>
        /// Changes the MAC address of a terminal
        /// </summary>
        /// <remarks>
        /// This method also sets the terminal state to OPERATIONAL
        /// </remarks>
        /// <param name="macAddress">The original Mac Address</param>
        /// <param name="newMacAddress">The new Mac Address</param>
        /// <returns>True if the address was changed correctly</returns>
        public bool changeTerminalMac(string macAddress, string newMacAddress)
        {
            bool result = false;
            string changeTerminalMacCmd = "UPDATE Terminals SET MacAddress = @NewMacAddress, AdmStatus = @AdmStatus WHERE MacAddress = @OldMacAddress";

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(changeTerminalMacCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@NewMacAddress", newMacAddress);
                        cmd.Parameters.AddWithValue("@AdmStatus", (int)AdmStatus.OPERATIONAL);
                        cmd.Parameters.AddWithValue("@OldMacAddress", macAddress);

                        if (cmd.ExecuteNonQuery() == 0)
                        {
                            result = false;
                            CmtApplicationException cmtApplicationException = new CmtApplicationException();
                            cmtApplicationException.ExceptionDateTime = DateTime.Now;
                            cmtApplicationException.ExceptionDesc = "MAC Address not changed";
                            cmtApplicationException.ExceptionStacktrace = "";
                            cmtApplicationException.UserDescription = "TicketMaster - changeTerminalMac";
                            _boLogControlWS.LogApplicationException(cmtApplicationException);
                        }
                        else
                        {
                            result = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtApplicationException = new CmtApplicationException();
                cmtApplicationException.ExceptionDateTime = DateTime.Now;
                cmtApplicationException.ExceptionDesc = ex.Message;
                cmtApplicationException.ExceptionStacktrace = ex.StackTrace;
                cmtApplicationException.UserDescription = "TicketMaster - changeTerminalMac";
                _boLogControlWS.LogApplicationException(cmtApplicationException);
            }

            return result;
        }
    
    
        /// <summary>
        /// Retrieves the ticket identified by ticketId
        /// </summary>
        /// <param name="ticketId">The ticket identifier</param>
        /// <returns>A CmtTicket instance, this instance can be null</returns>
        public CmtTicket GetTicket(string ticketId)
        {
            CmtTicket ticket = null;
            string queryCmd = "SELECT CreationDate, MacAddress, SourceAdmState, TargetAdmState, "
                                           + "TicketStatus, FailureReason, FailureStackTrace, TermActivity, newSla, IspId, NewMacAddress, NMSActivate, RetryCounter FROM Tickets WHERE TicketId = @TicketId";

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(queryCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@TicketId", ticketId);
                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            ticket.SourceAdmState = (AdmStatus)Enum.ToObject(typeof(AdmStatus), reader.GetInt32(3));
                            ticket.TargetAdmState = (AdmStatus)Enum.ToObject(typeof(AdmStatus), reader.GetInt32(4));
                            ticket.TicketStatus = (CmtRequestStatus)Enum.ToObject(typeof(CmtRequestStatus), reader.GetInt32(5));
                            ticket.FailureReason = reader.GetString(6);
                            ticket.FailureStackTrace = reader.GetString(7);
                            ticket.TicketActivity = (TerminalActivityTypes)Enum.ToObject(typeof(TerminalActivityTypes), reader.GetInt32(8));
                            if (reader.IsDBNull(9))
                            {
                                ticket.Sla = 0;
                            }
                            else
                            {
                                ticket.Sla = reader.GetInt32(9);
                            }
                            if (reader.IsDBNull(10))
                            {
                                ticket.IspId = 112;
                            }
                            else
                            {
                                ticket.IspId = reader.GetInt32(10);
                            }

                            if (reader.IsDBNull(11))
                            {
                                ticket.NewMacAddress = "";
                            }
                            else
                            {
                                ticket.NewMacAddress = reader.GetString(11);
                            }
                            ticket.NMSActivate = reader.GetInt16(12);
                            if (reader.IsDBNull(13))
                            {
                                ticket.RetryCounter = 0;
                            }
                            else
                            {
                                ticket.RetryCounter = reader.GetInt32(13);
                            }

                        }
                        else
                        {
                            CmtApplicationException cmtApplicationException = new CmtApplicationException();
                            cmtApplicationException.ExceptionDateTime = DateTime.Now;
                            cmtApplicationException.ExceptionDesc = "Could not retrieve ticket: " + ticketId;
                            cmtApplicationException.ExceptionStacktrace = "";
                            cmtApplicationException.UserDescription = "TicketMaster - GetTicket";
                            _boLogControlWS.LogApplicationException(cmtApplicationException);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtApplicationException = new CmtApplicationException();
                cmtApplicationException.ExceptionDateTime = DateTime.Now;
                cmtApplicationException.ExceptionDesc = ex.Message;
                cmtApplicationException.ExceptionStacktrace = ex.StackTrace;
                cmtApplicationException.UserDescription = "TicketMaster - GetTicket";
                _boLogControlWS.LogApplicationException(cmtApplicationException);
            }

            return ticket;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ticketId"></param>
        /// <param name="retryCounter"></param>
        public void updateTicketRetryCounter(string ticketId, int retryCounter)
        {
            string changeTicketStatusCmd = "UPDATE Tickets SET RetryCounter = @RetryCounter " +
                "WHERE TicketId = @TicketId";
            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                conn.Open();

                try
                {
                    using (SqlCommand cmd = new SqlCommand(changeTicketStatusCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@TicketId", ticketId);
                        cmd.Parameters.AddWithValue("@RetryCounter", retryCounter);
                        
                        int numRows = cmd.ExecuteNonQuery();

                        if (numRows == 0)
                        {
                            CmtApplicationException cmtApplicationException = new CmtApplicationException();
                            cmtApplicationException.ExceptionDateTime = DateTime.Now;
                            cmtApplicationException.ExceptionDesc = "Ticket not updated!";
                            cmtApplicationException.ExceptionStacktrace = "";
                            cmtApplicationException.UserDescription = "TicketMaster - updateTicketRetryCounter";
                            _boLogControlWS.LogApplicationException(cmtApplicationException);
                        }
                    }
                }
                catch (Exception ex)
                {
                    CmtApplicationException cmtApplicationException = new CmtApplicationException();
                    cmtApplicationException.ExceptionDateTime = DateTime.Now;
                    cmtApplicationException.ExceptionDesc = ex.Message;
                    cmtApplicationException.ExceptionStacktrace = ex.StackTrace;
                    cmtApplicationException.UserDescription = "TicketMaster - updateTicketRetryCounter";
                    _boLogControlWS.LogApplicationException(cmtApplicationException);
                }
            }
        }

        /// <summary>
        /// Changes the IP Address and IP Mask of the terminal to the new values
        /// </summary>
        /// <param name="ticketId">The ticket Id of the terminal activity</param>
        /// <returns>true if the operation succeeds</returns>
        public bool changeTerminalIPAddress(string macAddress, string newIPAddress, int newIPMask)
        {
            bool result = false;


            return result;
        }
    }
}
