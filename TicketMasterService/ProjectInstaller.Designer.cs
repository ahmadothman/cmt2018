﻿namespace net.nimera.cmt.TicketMasterService
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TicketMasterProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.TicketMasterInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // TicketMasterProcessInstaller
            // 
            this.TicketMasterProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.TicketMasterProcessInstaller.Password = null;
            this.TicketMasterProcessInstaller.Username = null;
            this.TicketMasterProcessInstaller.AfterInstall += new System.Configuration.Install.InstallEventHandler(this.TicketMasterProcessInstaller_AfterInstall);
            // 
            // TicketMasterInstaller
            // 
            this.TicketMasterInstaller.Description = "The Ticket Master checks the assynchronous received tickets for completion";
            this.TicketMasterInstaller.DisplayName = "Ticket  Master Service";
            this.TicketMasterInstaller.ServiceName = "TicketMasterService";
            this.TicketMasterInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            this.TicketMasterInstaller.AfterInstall += new System.Configuration.Install.InstallEventHandler(this.TicketMasterInstaller_AfterInstall);
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.TicketMasterProcessInstaller,
            this.TicketMasterInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller TicketMasterProcessInstaller;
        private System.ServiceProcess.ServiceInstaller TicketMasterInstaller;
    }
}