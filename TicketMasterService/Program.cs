﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;

namespace net.nimera.cmt.TicketMasterService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            System.IO.FileInfo file = new System.IO.FileInfo(Properties.Settings.Default.LogFile);
            file.Directory.Create();
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new TicketMasterService()
            };
            ServiceBase.Run(ServicesToRun);
            //var ticketMasterThreadMill = new TicketMasterThreadMill();
            //ticketMasterThreadMill.Run();
        }
    }
}
