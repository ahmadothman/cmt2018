﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;

namespace net.nimera.cmt.TicketMasterService
{
    public partial class TicketMasterService : ServiceBase
    {
        TicketMasterThreadMill _ticketMasterThreadMill;

        public TicketMasterService()
        {
            InitializeComponent();
            _ticketMasterThreadMill = new TicketMasterThreadMill();
        }

        protected override void OnStart(string[] args)
        {
            TicketMasterLog.WriteEntry("TicketMasterService start");
            Thread mainThread = new Thread(new ThreadStart(_ticketMasterThreadMill.Run));
            mainThread.Start();
        }

        protected override void OnStop()
        {
            TicketMasterLog.WriteEntry("TicketMasterService stop");
            _ticketMasterThreadMill.RunFlag = false;
        }
    }
}
