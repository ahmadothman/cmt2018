﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using net.nimera.cmt.TicketMasterService.BOMonitorControlWSRef;
using net.nimera.cmt.TicketMasterService.BOAccountingControlWSRef;
using net.nimera.cmt.TicketMasterService.BOConfigurationControlWSRef;
using net.nimera.cmt.TicketMasterService.BOMultiCastGroupControllerWSRef;

namespace net.nimera.cmt.TicketMasterService
{
    class ISPIFRequestHandler : IRequestHandler
    {
        BOMonitorControlWS _boMonitorControl = new BOMonitorControlWS();
        BOAccountingControlWS _boAccountingControl = new BOAccountingControlWS();
        BOConfigurationControllerWS _boConfigurationControl = new BOConfigurationControllerWS();
        BOMultiCastGroupControllerWS _boMultiCastGroupControl = new BOMultiCastGroupControllerWS();
        DataGateway _dg = new DataGateway();
        private const string SES_HUB_ISP = "SESIsp";
        private const int _retryCounter = 4;

        #region IRequestHandler Members

        /// <summary>
        /// Processes the ticket
        /// </summary>
        /// <remarks>
        /// If the NMSActivate flag of the ticket is set to 1, the ticket must be updated in the 
        /// NMS first. A new ticket is created 
        /// </remarks>
        /// <param name="ticket">The ticket retrieved from the database</param>
        public void ProcessTicket(CmtTicket ticket)
        {
            //Call the lookup ticket method
            this.logNewLine();
            this.log("Processing ticket: " + ticket.TicketId + " for " + ticket.MacAddress + " - " + ticket.TicketActivity.ToString());
            this.log("Isp: " + ticket.IspId + " NMSActivate: " + ticket.NMSActivate + " UserId: " + ticket.UserId);
            try
            {
                CmtRequestTicket cmtRequestTicket = null;
                if (ticket.NMSActivate == 2)
                {
                    //Lookup the ticket in the NMS
                    cmtRequestTicket = _boMonitorControl.NMSLookupRequestTicket(ticket.TicketId, ticket.IspId);
                }
                else
                {
                    cmtRequestTicket = _boMonitorControl.LookupRequestTicket(ticket.TicketId, ticket.IspId);
                    //cmtRequestTicket = new CmtRequestTicket()
                    //{
                    //    failureReason = "",
                    //    id = Guid.NewGuid().ToString(),
                    //    failureStackTrace = "",
                    //    NMSFlag = true,
                    //    requestStatus = CmtRequestStatus.SUCCESSFUL
                    //};
                }
                if (cmtRequestTicket.requestStatus != null)
                {
                    this.log("Ticket status: " + cmtRequestTicket.requestStatus.ToString());
                }
                else
                {
                    this.log("No ticket retrieved.");
                }

                if (cmtRequestTicket.requestStatus == CmtRequestStatus.SUCCESSFUL)
                {
                    if (ticket.NMSActivate == 1)
                    {
                        //Perform action in NMS
                        this.updateNMS(ticket, cmtRequestTicket);
                        this.log("Added ticket with NMSActivate 2");
                    }
                    else
                    {
                        //Update the ticket in the CMT 
                        this.updateTicket(ticket, cmtRequestTicket, ticket.UserId);
                    }
                }
                else if (cmtRequestTicket.requestStatus == CmtRequestStatus.FAILED)
                {
                    //Update the ticket
                    ticket.FailureReason = cmtRequestTicket.failureReason;
                    ticket.FailureStackTrace = cmtRequestTicket.failureStackTrace;
                    ticket.TicketStatus = CmtRequestStatus.FAILED;
                    _dg.changeTicketStatus(ticket, (CmtRequestStatus)cmtRequestTicket.requestStatus);
                }
            }
            catch (Exception ex)
            {
                this.log("MacAddress: " + ticket.MacAddress + ", Error: " + ex.Message);

                if (ticket.TicketStatus == CmtRequestStatus.BUSY)
                {
                    if (ticket.RetryCounter < _retryCounter)
                    {
                        //increment the retry counter
                        ticket.RetryCounter++;
                        _dg.updateTicketRetryCounter(ticket.TicketId, ticket.RetryCounter);
                    }
                    else
                    {
                        //fail the ticket
                        ticket.FailureReason = ex.Message;
                        ticket.FailureStackTrace = ex.StackTrace;
                        _dg.changeTicketStatus(ticket, CmtRequestStatus.FAILED);
                        this.log("MacAddress: " + ticket.MacAddress
                                            + ", Failed after 5 attempts");
                    }
                }
            }
        }

        #endregion

        /// <summary>
        /// Updates the ticket in the CMT database if the LookupRequestTicket method returned
        /// a SUCCESSFUL ticket state.
        /// </summary>
        /// <param name="ticket">Representation of the ticket in the CMT database</param>
        /// <param name="cmtRequestTicket">The ticket received from the HUB</param>
        public void updateTicket(CmtTicket ticket, CmtRequestTicket cmtRequestTicket, Guid userId)
        {
            //Update the ticket
            if (_dg.changeTicketStatus(ticket, (CmtRequestStatus)cmtRequestTicket.requestStatus))
            {
                if (ticket.TicketActivity == TerminalActivityTypes.ChangeSla)
                {
                    int? oldSla;
                    Terminal term = _boAccountingControl.GetTerminalDetailsByMAC(ticket.MacAddress);
                    oldSla = term.SlaId;
                    //Update the sla of the terminal
                    if (!_dg.changeTerminalSla(ticket.MacAddress, ticket.Sla))
                    {
                        this.log("Could not change SLA in Terminals table for MAC address: " + ticket.MacAddress);

                        //Change back the ticket status to allow retrial of the operation
                        _dg.changeTicketStatus(ticket, CmtRequestStatus.BUSY);
                    }
                    else
                    {
                        //Add the activity to the activity log table
                        _dg.addTerminalActivity(ticket.MacAddress, ticket.TicketActivity, oldSla.ToString(), userId);

                        this.log("MacAddress: " + ticket.MacAddress
                                                + ", Activity: " + ticket.TicketActivity);
                    }
                }
                else if (ticket.TicketActivity == TerminalActivityTypes.ChangeEdgeSla)
                {
                    //Update the sla of the terminal
                    if (!_dg.changeTerminalEdgeSla(ticket.MacAddress, ticket.Sla))
                    {
                        this.log("Could not change Edge SLA in Terminals table for MAC address: " + ticket.MacAddress);

                        //Change back the ticket status to allow retrial of the operation
                        _dg.changeTicketStatus(ticket, CmtRequestStatus.BUSY);
                    }
                    else
                    {
                        //Add the activity to the activity log table
                        _dg.addTerminalActivity(ticket.MacAddress, ticket.TicketActivity, userId);

                        this.log("MacAddress: " + ticket.MacAddress
                                                + ", Activity: " + ticket.TicketActivity);
                    }
                }
                else if (ticket.TicketActivity == TerminalActivityTypes.ChangeMac)
                {
                    //Recall the terminal which is still registered with the old Mac Address. Delete this 
                    //terminal, finally put the new terminal in Operational mode and update the database.

                    Terminal term = _boAccountingControl.GetTerminalDetailsByMAC(ticket.MacAddress);
                    term.AdmStatus = (int)AdmStatus.Deleted;

                    //We also need the new IP address as this might have been changed by the
                    //hub.
                    TerminalInfo ti = _boMonitorControl.getTerminalDetailsFromHubMacChange(ticket.MacAddress, ticket.NewMacAddress, ticket.IspId);

                    if (_boAccountingControl.UpdateTerminal(term))
                    {
                        //Set the terminals status to OPERATIONAL and change MAC Addresses
                        term.AdmStatus = (int)AdmStatus.OPERATIONAL;
                        term.MacAddress = ticket.NewMacAddress;

                        if (ti != null)
                        {
                            if (term.IpAddress.Equals(ti.IPAddress))
                            {
                                term.IpAddress = ti.IPAddress;
                                this.log("Warning, IP Address changed for Mac Address" + term.MacAddress);
                            }
                        }

                        //Add new terminal
                        if (!_boAccountingControl.UpdateTerminal(term))
                        {
                            this.log("Could not change MacAddress: " + ticket.MacAddress + ", Activity: " + ticket.TicketActivity
                                                    + ". Unable to create new terminal.");
                            //Change back the ticket status to allow retrial of the operation
                            _dg.changeTicketStatus(ticket, CmtRequestStatus.BUSY);
                        }
                        else
                        {
                            //Add the activity to the activity log table
                            _dg.addTerminalActivity(ticket.NewMacAddress, ticket.TicketActivity, userId);
                        }
                    }
                    else
                    {
                        this.log("Could not change MacAddress: " + ticket.MacAddress + ", Activity: " + ticket.TicketActivity
                                                    + ". Could not delete terminal.");
                        //Change back the ticket status to allow retrial of the operation
                        _dg.changeTicketStatus(ticket, CmtRequestStatus.BUSY);
                    }
                }
                else if (ticket.TicketActivity == TerminalActivityTypes.EnableStream)
                {
                    //Set the Multicast Group in the CMT to enabled
                    MultiCastGroup mc = _boMultiCastGroupControl.GetMultiCastGroupByMac(ticket.MacAddress);
                    if (mc != null)
                    {
                        //Set to enabled and update the MultiCastGroup in the CMT
                        mc.State = true;
                        if (!_boMultiCastGroupControl.UpdateMultiCastGroup(mc))
                        {
                            //Switch back to BUSY an try again
                            this.log("Could not enable MultiCastGroup with MAC address: " + ticket.MacAddress + ", Activity: " +
                                                                            ticket.TicketActivity);
                            //Change back the ticket status to allow retrial of the operation
                            _dg.changeTicketStatus(ticket, CmtRequestStatus.BUSY);
                        }
                    }
                    else
                    {
                        this.log("Could not find MultiCastGroup with IP Address: " + mc.IPAddress + " in CMT database");
                        //Change back the ticket status to allow retrial of the operation
                        _dg.changeTicketStatus(ticket, CmtRequestStatus.BUSY);
                    }
                }
                else if (ticket.TicketActivity == TerminalActivityTypes.DisableStream)
                {
                    //Set the Multicast Group in the CMT to enabled
                    MultiCastGroup mc = _boMultiCastGroupControl.GetMultiCastGroupByMac(ticket.MacAddress);
                    if (mc != null)
                    {
                        //Set to enabled and update the MultiCastGroup in the CMT
                        mc.State = false;
                        if (!_boMultiCastGroupControl.UpdateMultiCastGroup(mc))
                        {
                            //Switch back to BUSY an try again
                            this.log("Could not disable MultiCastGroup with MAC address: " + ticket.MacAddress + ", Activity: " +
                                                                            ticket.TicketActivity);
                            //Change back the ticket status to allow retrial of the operation
                            _dg.changeTicketStatus(ticket, CmtRequestStatus.BUSY);
                        }
                    }
                    else
                    {
                        this.log("Could not find MultiCastGroup with IP Address: " + mc.IPAddress + " in CMT database");
                        //Change back the ticket status to allow retrial of the operation
                        _dg.changeTicketStatus(ticket, CmtRequestStatus.BUSY);
                    }
                }
                else if (ticket.TicketActivity == TerminalActivityTypes.IPAddressChange)
                {
                    Terminal term = _boAccountingControl.GetTerminalDetailsByMAC(ticket.MacAddress);

                    if (term != null)
                    {
                        term.IpAddress = ticket.NewIPAddress;
                        term.IPMask = ticket.NewIPMask;

                        if (!_boAccountingControl.UpdateTerminal(term))
                        {
                            this.log("Could not change IP Address or IP Mask: " + ticket.MacAddress + ", Activity: " + ticket.TicketActivity);

                            //Change back the ticket status to allow retrial of the operation
                            _dg.changeTicketStatus(ticket, CmtRequestStatus.BUSY);
                        }
                        else
                        {
                            //Add the activity to the activity log table
                            _dg.addTerminalActivity(ticket.MacAddress, ticket.TicketActivity, userId);
                        }
                    }
                }
                else
                {
                    //Update the AdmStatus of the terminal
                    if (!_dg.changeTerminalStatus(ticket.MacAddress, ticket.TargetAdmState))
                    {
                        this.log("Could not change terminal admin status in Terminals table for MAC address: " + ticket.MacAddress);

                        //Change back the ticket status to allow retrial of the operation
                        _dg.changeTicketStatus(ticket, CmtRequestStatus.BUSY);
                    }
                    else
                    {
                        //Check if the TargetAdmState is OPERATIONAL, if it is load the 
                        //terminal data from the ISPIF for ISPs and
                        //update the terminal entry in the Terminals table. Loading the terminal
                        //data will not work for terminals using the ISPIF Emulator!
                        Terminal term = _boAccountingControl.GetTerminalDetailsByMAC(ticket.MacAddress);
                        try
                        {
                            //Recover the terminal details and update the terminals table
                            TerminalInfo ti = _boMonitorControl.getTerminalDetailsFromHub(ticket.MacAddress, ticket.IspId);
                            term.Blade = Int32.Parse(ti.BladeId);
                            term.IpAddress = ti.IPAddress;
                            term.SitId = Int32.Parse(ti.SitId);
                            _boAccountingControl.UpdateTerminal(term);
                            this.log("Terminals table successfully updated for terminal: " + term.MacAddress + " SitId: " + ti.SitId);
                        }
                        catch (Exception ex)
                        {
                            this.log("Error while retrieving terminal data from ISPIF: " + ex.Message);
                        }

                        //Add the activity to the activity log table
                        _dg.addTerminalActivity(ticket.MacAddress, ticket.TicketActivity, userId);

                        this.log("MacAddress: " + ticket.MacAddress
                                                + ", Admin state: "
                                                + ticket.TargetAdmState
                                                + ", Activity: " + ticket.TicketActivity
                                                + ", SlaName: " + term.SlaName);
                    }
                }
            }
        }

        /// <summary>
        /// The UpdateNMS method performs the necesssary terminal management actions on the 
        /// NMS. Before updating the NMS the terminal data are retreived from the ISPIF in order
        /// to get the IP address and the IP Mask
        /// </summary>
        /// <remarks>
        /// The FUPReset and AddVolume acivities are done immediately on the NMS and not on the
        /// SES Hub. So these activities are not handled in this list as they are already done in 
        /// step 1 of the procedure.
        /// </remarks>
        /// <param name="ticket">The ticket from the CMT Ticket table</param>
        /// <param name="cmtRequestTicket">The Request ticket received from the ISPIF</param>
        public void updateNMS(CmtTicket ticket, CmtRequestTicket cmtRequestTicket)
        {
            TerminalInfo ti = null;

            //Put the ticket in the successful state
            if (_dg.changeTicketStatus(ticket, (CmtRequestStatus)cmtRequestTicket.requestStatus))
            {
                Terminal term = _boAccountingControl.GetTerminalDetailsByMAC(ticket.MacAddress);
                if (ticket.TicketActivity == TerminalActivityTypes.ChangeMac)
                {
                    //The old method has been replaces in order to solve an issue with the Edge ISP not being found for the new MAC address
                    //ti = _boMonitorControl.getTerminalDetailsFromHub(ticket.NewMacAddress, ticket.IspId);
                    ti = _boMonitorControl.getTerminalDetailsFromHubMacChange(ticket.MacAddress, ticket.NewMacAddress, ticket.IspId);
                }
                else
                {
                    ti = _boMonitorControl.getTerminalDetailsFromHub(ticket.MacAddress, ticket.IspId);
                }

                //The following is true if the terminal is decommissioned. In that case the terminal
                //does not exist in the HUB anymore and we should not update the terminal. It's being 
                //removed anyway!
                if (ti != null)
                {
                    term.Blade = Int32.Parse(ti.BladeId);
                    term.IpAddress = ti.IPAddress;
                    term.SitId = Int32.Parse(ti.SitId);
                    _boAccountingControl.UpdateTerminal(term);
                }

                //Perform the action on the NMS and create a new ticket in the ticket database
                switch (ticket.TicketActivity)
                {
                    case TerminalActivityTypes.ChangeMac:
                        if (!_boMonitorControl.NMSChangeMacAddress(term.ExtFullName, ticket.MacAddress, ticket.IspId, ticket.NewMacAddress, ticket.UserId))
                        {
                            this.log("ChangeMac in NMS failed, Old Mac: " + ticket.MacAddress + " New Mac: " + ticket.NewMacAddress);
                        }
                        else
                        {
                            this.log("ChangeMac in NMS succeeded, Old Mac: " + ticket.MacAddress + " New Mac: " + ticket.NewMacAddress);
                        }
                        break;

                    case TerminalActivityTypes.Activation:
                        if (!_boMonitorControl.NMSTerminalActivateWithRangeExt(term.ExtFullName, ticket.MacAddress, ticket.Sla, ticket.IspId, term.IPMask, (int)term.FreeZone, ticket.UserId))
                        {
                            this.log("Activation of terminal " + ticket.MacAddress + " in NMS failed");
                        }
                        else
                        {
                            ServiceLevel sl = _boAccountingControl.GetServicePack((int)term.SlaId);
                            if (sl.ServiceClass == 1)
                            {
                                int sohoDefaultvolumeCode = int.Parse(_boConfigurationControl.getParameter("SohoDefaultVolume"));
                                _boMonitorControl.AddVolume(term.SitId, sohoDefaultvolumeCode, ticket.IspId);
                            }
                            this.log("Activation of terminal " + ticket.MacAddress + " in NMS succeeded");
                        }
                        break;

                    case TerminalActivityTypes.Decommissioning:
                        if (!_boMonitorControl.NMSTerminalDecommission(term.ExtFullName, ticket.MacAddress, ticket.IspId, ticket.UserId))
                        {
                            this.log("Decommissioning of terminal " + ticket.MacAddress + " in NMS failed");
                        }
                        else
                        {
                            this.log("Decommissioning of terminal " + ticket.MacAddress + " in NMS succeeded");
                        }
                        break;


                    case TerminalActivityTypes.ReActivation:
                        if (!_boMonitorControl.NMSTerminalReActivate(term.ExtFullName, ticket.MacAddress, ticket.IspId, ticket.UserId))
                        {
                            this.log("Reactivation of terminal " + ticket.MacAddress + " in NMS failed");
                        }
                        else
                        {
                            this.log("Reactivation of terminal " + ticket.MacAddress + " in NMS succeeded");
                        }
                        break;

                    case TerminalActivityTypes.Suspension:
                        if (!_boMonitorControl.NMSTerminalSuspend(term.ExtFullName, ticket.MacAddress, ticket.IspId, ticket.UserId))
                        {
                            this.log("Suspension of terminal " + ticket.MacAddress + " in NMS failed");
                        }
                        else
                        {
                            this.log("Suspension of terminal " + ticket.MacAddress + " in NMS succeeded");
                        }
                        break;

                    case TerminalActivityTypes.ChangeSla:
                        if (!_boMonitorControl.NMSTerminalChangeSla(term.ExtFullName, ticket.MacAddress, ticket.Sla, BOMonitorControlWSRef.AdmStatus.OPERATIONAL, ticket.IspId, ticket.UserId))
                        {
                            this.log("Change Sla for terminal " + ticket.MacAddress + " in NMS failed");
                        }
                        else
                        {
                            this.log("Change Sla for terminal " + ticket.MacAddress + " in NMS succeeded");
                        }
                        break;

                    default:
                        //Do nothing
                        break;
                }
            }
        }

        /// <summary>
        /// Simple log method
        /// </summary>
        /// <param name="logMsg"></param>
        private void log(string logMsg)
        {
            string logFilePath = Properties.Settings.Default.LogFile;
            using (StreamWriter writer = new StreamWriter(logFilePath, true))
            {
                writer.WriteLine("[" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss.fff") + "]" + logMsg);
            }
        }

        private void logNewLine()
        {
            string logFilePath = Properties.Settings.Default.LogFile;
            using (StreamWriter writer = new StreamWriter(logFilePath, true))
            {
                writer.WriteLine();
            }
        }
    }
}
