﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace net.nimera.supportlibrary
{
    /// <summary>
    /// This class contains methods necessary for Date and Time
    /// calculations.
    /// </summary>
    public class DateHelper
    {
        /// <summary>
        /// This method returns the epoch time stamp
        /// </summary>
        /// <remarks>
        /// Unix time, or POSIX time, is a system for describing 
        /// instants in time, defined as the number of seconds 
        /// elapsed since midnight Coordinated Universal Time (UTC) 
        /// of Thursday, January 1, 1970 (Unix times are defined, 
        /// but negative, before that date), not counting leap seconds,
        /// which are declared by the International Earth Rotation and
        /// Reference Systems Service and are not predictable. 
        /// It is used widely in Unix-like and many other operating 
        /// systems and file formats. It is neither a linear 
        /// representation of time nor a true representation of UTC 
        /// (though it is frequently mistaken for both), as it cannot 
        /// unambiguously represent UTC leap seconds 
        /// (e.g. December 31, 1998 23:59:60), although otherwise the 
        /// times it represents are UTC. Unix time may be checked on 
        /// some Unix systems by typing date +%s on the command line.
        /// (Wikipedia)
        /// 
        /// In .NET a single tick represents one hundred nanoseconds 
        /// or one ten-millionth of a second. There are 10,000 ticks 
        /// in a millisecond. The value of this property represents 
        /// the number of 100-nanosecond intervals that have elapsed 
        /// since 12:00:00 midnight, January 1, 0001, which represents 
        /// DateTime.MinValue. It does not include the number of ticks 
        /// that are attributable to leap seconds.
        /// See: http://msdn.microsoft.com/en-us/library/system.datetime.ticks.aspx
        /// </remarks>
        /// <param name="dt">Target timestamp (.NET DateTime)</param>
        /// <returns>Epoch timestamp as a long</returns>
        public static long DateToEpoch(DateTime dt)
        {
            long timeTicks = dt.Ticks;
            long epochTime = (long)(timeTicks - new DateTime(1970, 1, 1).Ticks)/10000;
            return epochTime;
        }

        /// <summary>
        /// Convertes a time given as Epoch ticks into a DateTime value
        /// </summary>
        /// <param name="epochTime">Epoch time ticks</param>
        /// <returns>Converted DateTime value</returns>
        public static DateTime EpochToDateTime(long epochTime)
        {
            DateTime dt = new DateTime(1970, 1, 1, 0, 0, 0);
            dt = dt.AddMilliseconds(epochTime);
            return dt;
        }
    }
}