﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace net.nimera.supportlibrary
{
    /// <summary>
    /// Contains simple methods for converting hex to dec and visa versa
    /// </summary>
    public class HexDecConvertor
    {
        /// <summary>
        /// Converts an hex value given as a string into an integer
        /// </summary>
        /// <param name="hex">Hexadecimal value as a string</param>
        /// <returns>The hex converted into an integer</returns>
        public static int ConvertToInt(string hex)
        {
            return Convert.ToInt32(hex, 16);
        }

        /// <summary>
        /// Converts an integer to an hex number
        /// </summary>
        /// <param name="dec">The decimal value</param>
        /// <returns>The integer value as a string</returns>
        public static string ConvertToHex(int dec)
        {
            return dec.ToString("X4");
        }
    }
}
