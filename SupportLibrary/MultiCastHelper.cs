﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
 
namespace net.nimera.supportlibrary
{
    /// <summary>
    /// This class contains support functionality for working with
    /// Multicast groups
    /// </summary>
    public class MultiCastHelper
    {
        /// <summary>
        /// This method creates a MAC address from the given IP address
        /// </summary>
        /// <remarks>
        /// For more information about the algorithm, please consult the document:
        /// MultiCastProvisioningVS0.1.pdf on the sharepoint server.
        /// </remarks>
        /// <param name="ipAddress">The base IP Address</param>
        /// <returns></returns>
        public static string CreateMacFromIP(string ipAddress)
        {
            string[] ipAddressTokens = ipAddress.Split('.');
            string multiCastMac = "01:00:5e";
            for (int i = 1; i < 4; i++)
            {
                string hexNum = Convert.ToString(Int32.Parse(ipAddressTokens[i]), 16);
                if (hexNum.Length == 1)
                {
                    hexNum = 0 + hexNum;
                }
                multiCastMac = multiCastMac + ":" + hexNum;
            }
            return multiCastMac;
        }
    }
}
