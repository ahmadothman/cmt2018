﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NMSMultiCastGroupSchedulesSync.BOAccountingControlWSRef;
using NMSMultiCastGroupSchedulesSync.BOMultiCastGroupControllerWSRef;
using NMSMultiCastGroupSchedulesSync.NMSGatewayServiceRef;
using NMSMultiCastGroupSchedulesSync.Util;

namespace NMSMultiCastGroupSchedulesSync
{
    class Program
    {
        //SATCORP-52 
        //NMSMulitCastLogSync is designed for this issue, it gets MultiCastLogs for the coming day, from the CMT DB and puts them into the MultiCastTimer table in the NMS DB

        const string _connectionString = "Server=192.168.100.100;database=NMS;user=SatADSL;password=Fulvio";
        private string _logFile = "NMSMultiCastLogSync.log";

        static void Main(string[] args)
        {
            Program prg = new Program();
            prg.mainBody();
        }

        private void mainBody()
        {
            Logger.log("Synchronisation started", _logFile);

            BOMultiCastGroupControllerWS boMultiCastGroupController = new BOMultiCastGroupControllerWS();
            NMSGatewayService nmsGateWayService = new NMSGatewayService();

            //Get MultiCastLogs from CMT DB
            List<MultiCastGroupSchedule> schedules = boMultiCastGroupController.GetAllMultiCastGroupSchedulesBetweenDateTimes(DateTime.UtcNow.Date, DateTime.UtcNow.Date).ToList();

            //Write MultiCastLogs to NMS DB
            foreach (MultiCastGroupSchedule schedule in schedules)
            {
                nmsGateWayService.insertMultiCastTimer(schedule.MultiCastGroupID.ToString(), Convert.ToDateTime(schedule.StartDate + schedule.StartTime), Convert.ToDateTime(schedule.EndDate + schedule.EndTime));
            }

            Logger.log("Synchronzation finished", _logFile);

        }
    }
}
