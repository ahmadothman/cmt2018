﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using SubscriptionManagerProcess.BOLogControlWSRef;
using log4net;

namespace SubscriptionManagerProcess
{
    class Program
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof (Program));

        static void Main(string[] args)
        {
            log4net.Config.XmlConfigurator.Configure();
            Log.Debug("Application started");

            BOLogControlWS logger = new BOLogControlWS();// ConfigurationManager.ConnectionStrings["Database"].ConnectionString, ConfigurationManager.ConnectionStrings["Database"].ProviderName);
            
            try
            {
                Log.Debug("Logging user activity");   
                logger.LogUserActivity(new UserActivity()
                                           {
                                               Action = "SMP_START",
                                               ActionDate = DateTime.Now,
                                               UserId = new Guid(ConfigurationManager.AppSettings["AdminGuid"])
                                               // runas administrator set userid.
                                               
                                           });
                var m = new SMP();

                Log.Debug("Starting SMP");   
                m.Process();
            }
            catch (Exception ex)
            {
               // logger.LogApplicationException(new CmtApplicationException(ex, "Subscription Manager Process Error",""));
            }
            finally
            {
                Log.Debug("Logging SMP Stop");   
                logger.LogUserActivity(new UserActivity()
                {
                    Action = "SMP_STOP",
                    ActionDate = DateTime.Now,
                    UserId = new Guid(ConfigurationManager.AppSettings["AdminGuid"])
                    // runas administrator set userid.

                });
            }
        }


        //static void ShowHelp ()
        //{
        //    Console.BufferWidth = 400;
        //    Console.WriteLine("Subscription Manager Process - Usage:");
        //    Console.WriteLine("-------------------------------------");
        //    Console.WriteLine();
        //    Console.WriteLine("SMP.exe");
        //    Console.WriteLine("Required:");
        //    WriteParameter("--database \"ConnectionString\"", "Required: The full connectionstring of the database we're going to connect to.");
        //    Console.WriteLine("Optional:");
        //    WriteParameter("--d", "Number of days before warning. Default is 5");
        //    WriteParameter("--nn --nolog-nulls", "Do not log the NULL expiration date terminals. When this option is passed, all invalid expiration dates will be skipped from the view. ");
        //    WriteParameter("--nd --nolog-dues", "Do not log the already expired terminals to file");
        //    WriteParameter("--due-log {FileName}", "The file name of the due terminals. Default is terminals_due.txt");
        //    WriteParameter("--null-log {FileName}", "The file name of the null terminals. Default is terminals_null.txt");
            
        //    Console.ReadKey();
        //}

        //private static void WriteParameter(string parameterName, string parameterdescription)
        //{
        //    Console.ForegroundColor = ConsoleColor.Green;
        //    Console.Write(string.Format("{0,2}", ""));
        //    Console.Write(string.Format("{0,-22}", parameterName));
        //    Console.ResetColor();
        //    Console.Write(string.Format("{0,2}", ""));
        //    Console.WriteLine(string.Format("{0}", parameterdescription));
        //}
    }
}
