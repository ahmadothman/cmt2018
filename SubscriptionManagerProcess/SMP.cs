﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using PetaPoco;
using SubscriptionManagerProcess.BOLogControlWSRef;
using SubscriptionManagerProcess.BOAccountingControlWSRef;
using SubscriptionManagerProcess.Model;
using log4net;

namespace SubscriptionManagerProcess
{
    public class SMP
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(SMP));

        //send emails to the dev instead of the distributor/org/nocop
        public const bool TestMode = false;

        // we may need to refactor this to using the webservice instead, but for now (time contraints) use it as a library.
        private readonly BOLogControlWS _logController;
        private BOAccountingControlWS _boaccountingControl;
        //private readonly BONotifyController _notifyController;

        public SMP()
        {
            Log.Debug("SMP Constructor");
            _logController = new BOLogControlWS();// (ConfigurationManager.ConnectionStrings["Database"].ConnectionString, ConfigurationManager.ConnectionStrings["Database"].ProviderName);
                                                  //_notifyController = new BONotifyController(ConfigurationManager.ConnectionStrings["Database"].ConnectionString, ConfigurationManager.ConnectionStrings["Database"].ProviderName);
            _boaccountingControl = new BOAccountingControlWS();
            Log.Debug("SMP Constructor - END");
        }

        /// <summary>
        ///   Will run through the database view and see which terminals and which data needs accessing.
        /// </summary>
        public void Process()
        {
            // do we need to log the nulls? If not, we can add a filter that doesnt show them.
            bool logNulls = bool.Parse(ConfigurationManager.AppSettings["LogNulls"]);
            bool logDues = bool.Parse(ConfigurationManager.AppSettings["LogDues"]);
            int warningDays = Int32.Parse(ConfigurationManager.AppSettings["Days"]);
            var list = GetTerminals(!logNulls);

            // this list will contain all unique mail addresses and the list of terminals we need to send them. We do this to avoid any mail spam.
            var actionList = new SortedList<string, TerminalInfoResultList>();

            var nulls = new StringBuilder();
            var dues = new StringBuilder();

            foreach (var i in list)
            { 
                try
                {
                    if (!i.daystoexpire.HasValue) // no need to check for lognulls, as we wouldnt get nulls here otherwise (cfr filter)
                    {
                        nulls.AppendLine(i.MacAddress);
                        continue; // no value, so continue with the next value
                    }

                    if (logDues && i.daystoexpire <= 0)
                        dues.AppendLine(i.ToString());

                    TerminalExpirationCategory category = TerminalExpirationCategory.Unknown;

                    // terminal expired today. Send mail for immediate termination
                   //limit the check for only business service. this check should be done afterwords for all subscriptions related to cusomter organization
                    // if ((_boaccountingControl.GetServicePack((int)i.SlaId).ServiceClass == 2))
                    //{
                        if (i.daystoexpire == 0)
                            category = TerminalExpirationCategory.ExpiredToday;
                        else if (i.daystoexpire == warningDays)
                            category = TerminalExpirationCategory.ExpiresAtWarningDays;

                        AddToList(i.distributormail, actionList, i, category);
                        AddToList(i.organizationmail, actionList, i, category);
                        AddToList(ConfigurationManager.AppSettings["adminMail"], actionList, i, category);
                    //}
                }
                catch (Exception ex)
                {
                    Log.Warn("Error while processing terminal i : " + i.MacAddress, ex);
                }
            }

            Log.Debug("ActionList: " + actionList.Count);
            // now send the results via mail
            SendMails(actionList);

            Log.Debug("Now checking lognulls: " + logNulls);
            // now log the full stringbuilders to file (as configured).
            if (logNulls)
            {
                Log.Debug("Logging nulls");
                WriteLog(ConfigurationManager.AppSettings["LogNullsFileName"], nulls);
                Log.Debug("Logging nulls - END");
            }

            Log.Debug("Now checking logDues: " + logDues);
            if (logDues)
            {
                Log.Debug("Logging dues");
                WriteLog(ConfigurationManager.AppSettings["LogDuesFileName"], dues);
                Log.Debug("Logging dues - END");
            }
        }

        private void SendMails(SortedList<string, TerminalInfoResultList> actionList)
        {
            try
            {
                foreach (var toEmailAddress in actionList.Keys)
                {
                    if (actionList[toEmailAddress].Warning.Count > 0 || actionList[toEmailAddress].Expired.Count > 0)
                    {
                        var emailData = new StringBuilder();
                        emailData.AppendLine("Dear Partner,");
                        emailData.AppendLine("<br/>");
                        emailData.AppendLine("<br/>");
                        emailData.AppendLine(string.Format("Please be informed that the following terminals will reach their Expiry Date within {0} days: <br/>", ConfigurationManager.AppSettings["Days"]));
                        foreach (var terminal in actionList[toEmailAddress].Warning)
                            emailData.AppendLine(string.Format("Terminal: {3}, Mac: {0}, StartDate: {1}, DateExpiry: {2} <br/>", terminal.MacAddress, terminal.StartDate, terminal.ExpiryDate, terminal.FullName));
                        emailData.AppendLine("<br/>");
                        emailData.AppendLine(string.Format("Please be also informed that the following terminals reached their Expiry Date today: <br/>"));
                        foreach (var terminal in actionList[toEmailAddress].Expired)
                        emailData.AppendLine(string.Format("Terminal: {3}, Mac: {0}, StartDate: {1}, DateExpiry: {2} <br/>", terminal.MacAddress, terminal.StartDate, terminal.ExpiryDate, terminal.FullName));
                        emailData.AppendLine("<br/>");
                        emailData.AppendLine("This message is for information only to help you in managing your customers payments. No automated action will be taken by the system. <br/>");
                        emailData.AppendLine("<br/>");
                        emailData.AppendLine("Best Regards, <br/>");
                        emailData.AppendLine("The SatADSL Team <br/>");
                        emailData.AppendLine("<br/>");
                        emailData.AppendLine("<br/>");
                        emailData.AppendLine("<br/>");


                        emailData.AppendLine("Cher Partenaire,");
                        emailData.AppendLine("<br/>");
                        emailData.AppendLine("<br/>");
                        emailData.AppendLine(string.Format("Veuillez noter que les terminaux suivantes vont arriver à leur Date d'Expiration dans {0} jours: <br/>", ConfigurationManager.AppSettings["Days"]));
                        foreach (var terminal in actionList[toEmailAddress].Warning)
                            emailData.AppendLine(string.Format("Terminal: {3}, Mac: {0}, StartDate: {1}, DateExpiry: {2} <br/>", terminal.MacAddress, terminal.StartDate, terminal.ExpiryDate, terminal.FullName));            
                        emailData.AppendLine("<br/>");
                        emailData.AppendLine(string.Format("Veuillez aussi noter que les terminaux suivants arrivent à leur Date d'Expiration aujourd'hui: <br/>"));
                        emailData.AppendLine("<br/>");
                        foreach (var terminal in actionList[toEmailAddress].Expired)
                            emailData.AppendLine(string.Format("Terminal: {3}, Mac: {0}, StartDate: {1}, DateExpiry: {2} <br/>", terminal.MacAddress, terminal.StartDate, terminal.ExpiryDate, terminal.FullName));
                        emailData.AppendLine("<br/>");
                        emailData.AppendLine("Ce message a un but informatif pour vous aider à gérer les paiements de vos clients. Aucune action automatique sera exécutée par le système. <br/>");
                        emailData.AppendLine("<br/>");
                        emailData.AppendLine("Meilleures salutations <br/>");
                        emailData.AppendLine("L'équipe SatADSL <br/>");
                        emailData.AppendLine("<br/>");
                        emailData.AppendLine("<br/>");
                        emailData.AppendLine("<br/>");


                        emailData.AppendLine("Caro Distribuidor,");
                        emailData.AppendLine("<br/>");
                        emailData.AppendLine("<br/>");
                        emailData.AppendLine(string.Format("Por favor tome em consideração que os seguintes terminais atingirão a data de expiração dentro de {0} dias: <br/>", ConfigurationManager.AppSettings["Days"]));
                        foreach (var terminal in actionList[toEmailAddress].Warning)
                            emailData.AppendLine(string.Format("Terminal: {3}, Mac: {0}, StartDate: {1}, DateExpiry: {2} <br/>", terminal.MacAddress, terminal.StartDate, terminal.ExpiryDate, terminal.FullName));
                        emailData.AppendLine("<br/>");
                        emailData.AppendLine(string.Format("Por favor tome também em consideração que os seguintes terminais atingiram a data de expiração hoje: <br/>"));
                        emailData.AppendLine("<br/>");
                        foreach (var terminal in actionList[toEmailAddress].Expired)
                            emailData.AppendLine(string.Format("Terminal: {3}, Mac: {0}, StartDate: {1}, DateExpiry: {2} <br/>", terminal.MacAddress, terminal.StartDate, terminal.ExpiryDate, terminal.FullName));
                        emailData.AppendLine("<br/>");
                        emailData.AppendLine("Esta mensagem é somente informativa e tem como objetivo auxiliar a fazer a gestão dos pagamentos dos seus clientes. Nenhuma ação automática será levada a cabo pelo sistema. <br/>");
                        emailData.AppendLine("<br/>");
                        emailData.AppendLine("Os nossos melhores cumprimentos, <br/>");
                        emailData.AppendLine("A Equipa Satadsl <br/>");
                        string[] reciever = new string[1] { "ahmad.satadsl@gmail.com" };// toEmailAddress };
                        if (_logController.SendMail(emailData.ToString(), "Terminal Expiration Warnings", reciever))
                            Log.Debug("Email sent to " + toEmailAddress);
                        else
                            Log.Error("Failed to send Email to " + toEmailAddress);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("Error while performing SendMails", ex);
            }
        }

        private void WriteLog(string appSetting, StringBuilder nulls)
        {
            try {
            using (var writer = new StreamWriter(appSetting, false, Encoding.UTF8))
            {
                writer.Write(nulls);
            }
                }
            catch (Exception ex)
            {
                Log.Error("WriteLog", ex);
            }
        }

        private static void AddToList(string mail, SortedList<string, TerminalInfoResultList> actionList, TerminalExpirationInfo i, TerminalExpirationCategory category)
        {
            if (!String.IsNullOrEmpty(mail))
            {
                if (!actionList.ContainsKey(mail))
                    actionList[mail] = new TerminalInfoResultList();

                switch (category)
                {
                    case TerminalExpirationCategory.ExpiredToday:
                        actionList[mail].Expired.Add(i);
                        return;
                    case TerminalExpirationCategory.ExpiresAtWarningDays:
                        actionList[mail].Warning.Add(i);
                        return;
                }
            }
        }

        private IEnumerable<TerminalExpirationInfo> GetTerminals(bool addNullFilter)
        {
            try
            {
                string sql =
                    "SELECT t.*, a.*  " +
                    "FROM vw_terminalexpiration t " +
                    "join addresses a on a.id = t.addressid ";

                if (addNullFilter)
                    sql += " WHERE expirationdaysleft is not null";


                using (var db = new Database(ConfigurationManager.ConnectionStrings["Database"].ConnectionString, ConfigurationManager.ConnectionStrings["Database"].ProviderName))
                {
                    var dbObj = db.Fetch<TerminalExpirationInfo, Coordinate, Address, TerminalExpirationInfo>(
                        (i, c, a) =>
                            {
                                i.LatLong = c;
                                i.Address = a;
                                return i;
                            },
                        sql);
                    return dbObj;
                }
            }
            catch (Exception ex)
            {
               // _logController.LogApplicationException(new CmtApplicationException(ex, "BOAccountingController: Error while executing GetTerminals"," "));
                throw;
            }
        }
    }
}
