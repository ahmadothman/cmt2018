﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SubscriptionManagerProcess.Model
{
    class TerminalInfoResultList
    {
        public TerminalInfoResultList ()
        {
            Expired = new List<TerminalExpirationInfo>();
            Warning = new List<TerminalExpirationInfo>();
        }
        /// <summary>
        /// Terminal expires today
        /// </summary>
        public List<TerminalExpirationInfo> Expired { get; set; }

        /// <summary>
        /// Terminal expires soon (in the x days)
        /// </summary>
        public List<TerminalExpirationInfo> Warning { get; set; }
    }
}
