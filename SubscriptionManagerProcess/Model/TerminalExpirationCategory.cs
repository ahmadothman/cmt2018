﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SubscriptionManagerProcess.Model
{
    public enum TerminalExpirationCategory
    {
        AlreadyExpired = 0,
        ExpiredToday = 1,
        ExpiresAtWarningDays = 2,
        Null = 3,
        Valid = 4,
        Unknown = 5
    }
}
