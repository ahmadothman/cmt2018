﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SubscriptionManagerProcess.BOAccountingControlWSRef;

namespace SubscriptionManagerProcess.Model
{
    class TerminalExpirationInfo : Terminal
    {
        public int? daystoexpire { get; set; }
        public string distributormail { get; set; }
        public string organizationmail { get; set; }

        public override string ToString()
        {
            return string.Format("FullName: {2}, Mac: {0}, DaysToExpiration: {1}", MacAddress, daystoexpire, FullName);
        }
    }
}
