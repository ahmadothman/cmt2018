﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NMSMonitor.UserControls;

namespace NMSMonitor
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();
        }

        private void treeViewNMS_Click(object sender, EventArgs e)
        {
            if (treeViewNMS.SelectedNode.Name.Equals("NMS1"))
            {
                //Get the NMS URL from the config file and load the monitoring
                //control
                MonitorView monitorView = new MonitorView();
                monitorView.MonitorGraphUri = new Uri("http://212.123.0.199:83/render?target=RTT.Term1&height=800&width=1200&lineMode=connected&title=RTT average Terminal 1");
                splitContainer1.Panel2.Controls.Add(monitorView);
                splitContainer1.Panel2.Controls[0].Dock = DockStyle.Fill;

            }
        }
    }
}
