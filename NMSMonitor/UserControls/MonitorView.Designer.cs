﻿namespace NMSMonitor.UserControls
{
    partial class MonitorView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.webBrowserMonitor = new System.Windows.Forms.WebBrowser();
            this.SuspendLayout();
            // 
            // webBrowserMonitor
            // 
            this.webBrowserMonitor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBrowserMonitor.Location = new System.Drawing.Point(0, 0);
            this.webBrowserMonitor.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowserMonitor.Name = "webBrowserMonitor";
            this.webBrowserMonitor.Size = new System.Drawing.Size(150, 150);
            this.webBrowserMonitor.TabIndex = 0;
            // 
            // MonitorView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.webBrowserMonitor);
            this.Name = "MonitorView";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.WebBrowser webBrowserMonitor;
    }
}
