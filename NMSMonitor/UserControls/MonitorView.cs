﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;

namespace NMSMonitor.UserControls
{
    public partial class MonitorView : UserControl
    {
        Uri _monitorGraphUri;

        public Uri MonitorGraphUri
        {
            get
            {
                return _monitorGraphUri;
            }

            set
            {
                _monitorGraphUri = value;
                webBrowserMonitor.Url = _monitorGraphUri;

            }
        }

        public MonitorView()
        {
            InitializeComponent();
        }
    }
}
