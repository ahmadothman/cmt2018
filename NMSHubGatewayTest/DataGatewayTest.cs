﻿using NMSHubGateway;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;

namespace NMSHubGatewayTest
{
    
    
    /// <summary>
    ///This is a test class for DataGatewayTest and is intended
    ///to contain all DataGatewayTest Unit Tests
    ///</summary>
    [TestClass()]
    public class DataGatewayTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for userExists
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Projects\\SatFinAfrica\\CustomerManagementTool\\NMSHubGateway", "/")]
        [UrlToTest("http://localhost:50166/Default.aspx")]
        public void userExistsTest()
        {
            DataGateway target = new DataGateway();
            string userId = "DSD - Tchad Kelo SN 2010800123433";
            bool result = false;
            result = target.userExists(userId);
            Assert.IsTrue(result, "userExists method failed");
        }

        /// <summary>
        ///A test for terminalExists
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Projects\\SatFinAfrica\\CustomerManagementTool\\NMSHubGateway", "/")]
        [UrlToTest("http://localhost:50166/")]
        public void terminalExistsTest()
        {
            DataGateway target = new DataGateway(); // TODO: Initialize to an appropriate value
            string macAddress = string.Empty; // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.terminalExists(macAddress);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for slaExists
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Projects\\SatFinAfrica\\CustomerManagementTool\\NMSHubGateway", "/")]
        [UrlToTest("http://localhost:50166/")]
        public void slaExistsTest()
        {
            DataGateway target = new DataGateway(); // TODO: Initialize to an appropriate value
            int slaId = 0; // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.slaExists(slaId);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for cleanUpTickets
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Projects\\SatFinAfrica\\CustomerManagementTool\\NMSHubGateway", "/")]
        [UrlToTest("http://localhost:50166/")]
        public void cleanUpTicketsTest()
        {
            DataGateway target = new DataGateway(); // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.cleanUpTickets();
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }
}
