﻿using NMSHubGateway;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;

namespace NMSHubGatewayTest
{
    
    
    /// <summary>
    ///This is a test class for ConvertorTest and is intended
    ///to contain all ConvertorTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ConvertorTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for ConvertMacToInt
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Projects\\SatFinAfrica\\CustomerManagementTool\\NMSHubGateway", "/")]
        [UrlToTest("http://localhost:50166/")]
        public void ConvertMacToLongTest()
        {
            string sMacAddress = "00:06:39:83:89:9e";
            long expected = 26734725534;
            long actual;
            actual = Convertor.ConvertMacToLong(sMacAddress);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for ConverLongToMac
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Projects\\SatFinAfrica\\CustomerManagementTool\\NMSHubGateway", "/")]
        [UrlToTest("http://localhost:50166/")]
        public void ConverLongToMacTest()
        {
            //System.Diagnostics.Debugger.Break();
            long iMacAddress = 26734725534;
            string expected = "00:06:39:83:89:9e"; // TODO: Initialize to an appropriate value
            string actual;
            actual = Convertor.ConvertLongToMac(iMacAddress);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for ConvertIPToInt
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Projects\\SatFinAfrica\\CustomerManagementTool\\NMSHubGateway", "/")]
        [UrlToTest("http://localhost:50166/")]
        public void ConvertIPToLongTest()
        {
            //System.Diagnostics.Debugger.Break();
            string sIpAddress = "81.82.192.145";
            long expected = 2445300305; // TODO: Initialize to an appropriate value
            long actual;
            actual = Convertor.ConvertIPToLong(sIpAddress);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for ConvertIntToIP
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Projects\\SatFinAfrica\\CustomerManagementTool\\NMSHubGateway", "/")]
        [UrlToTest("http://localhost:50166/")]
        public void ConvertLongToIPTest()
        {
            //System.Diagnostics.Debugger.Break();
            long iIpAddress = 2445300305; // TODO: Initialize to an appropriate value
            string expected = "81.82.192.145"; // TODO: Initialize to an appropriate value
            string actual;
            actual = Convertor.ConvertLongToIP(iIpAddress);
            Assert.AreEqual(expected, actual);
        }
    }
}
