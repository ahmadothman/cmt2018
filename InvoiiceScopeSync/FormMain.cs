﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Microsoft.Synchronization;
using Microsoft.Synchronization.Data;
using Microsoft.Synchronization.Data.SqlServer;

namespace InvoiiceScopeSync
{
    public partial class FormMain : Form
    {
        const string Scope = "CMTInvoiceScope";
        string clientConnectionString;
        string serverConnectionString;

        public FormMain()
        {
            InitializeComponent();
            clientConnectionString = InvoiiceScopeSync.Properties.Settings.Default.LocalConnectionString;
            serverConnectionString = InvoiiceScopeSync.Properties.Settings.Default.RemoteConnectionString;
        }

        private void buttonSync_Click(object sender, EventArgs e)
        {
            textBoxOut.Text = "";
            buttonSync.Enabled = false;
            this.UseWaitCursor = true;

            try
            {
                SqlConnection clientConn = new SqlConnection(clientConnectionString);
                SqlConnection serverConn = new SqlConnection(serverConnectionString);

                // create the sync orhcestrator
                SyncOrchestrator syncOrchestrator = new SyncOrchestrator();

                // set local provider of orchestrator to a sync provider associated with the 
                // ProductsScope in the SyncExpressDB express client database
                syncOrchestrator.LocalProvider = new SqlSyncProvider(Scope, clientConn);

                // set the remote provider of orchestrator to a server sync provider associated with
                // the ProductsScope in the SyncDB server database
                syncOrchestrator.RemoteProvider = new SqlSyncProvider(Scope, serverConn);

                // set the direction of sync session to Upload and Download
                syncOrchestrator.Direction = SyncDirectionOrder.UploadAndDownload;

                // subscribe for errors that occur when applying changes to the client
                ((SqlSyncProvider)syncOrchestrator.LocalProvider).ApplyChangeFailed += new EventHandler<DbApplyChangeFailedEventArgs>(Program_ApplyChangeFailed);

                // execute the synchronization process
                SyncOperationStatistics syncStats = syncOrchestrator.Synchronize();

                //Print stats
                log("Start Time: " + syncStats.SyncStartTime);
                log("Total Changes Uploaded: " + syncStats.UploadChangesTotal);
                log("Total Changes Downloaded: " + syncStats.DownloadChangesTotal);
                log("Complete Time: " + syncStats.SyncEndTime);
                log(String.Empty);
                buttonSync.Enabled = true;
                this.UseWaitCursor = false;
            }
            catch (Exception ex)
            {
                log(ex.Message);
            }
        }

        protected void log(string msg)
        {
            textBoxOut.Text = textBoxOut.Text + msg + Environment.NewLine;
        }

        protected void Program_ApplyChangeFailed(object sender, DbApplyChangeFailedEventArgs e)
        {
            // display conflict type
            log(e.Conflict.Type.ToString());

            // display error message 
            log(e.Error.Message);
        }
    
    }
}
