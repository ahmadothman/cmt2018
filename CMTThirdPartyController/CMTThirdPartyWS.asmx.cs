﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using CMTThirdPartyController.BOAccountingControlWSRef;
using CMTThirdPartyController.BOMonitorControlWSRef;

namespace CMTThirdPartyController
{
    /// <summary>
    /// The CMT Third Party Web Service allows certain functionality of the CMT to be
    /// executed from outside of the application
    /// </summary>
    [WebService(Namespace = "http://cmt.nimera.net/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class CMTThirdPartyWS : System.Web.Services.WebService
    {
        BOAccountingControlWS _boAccountingControl;
        BOMonitorControlWS _boMonitorControl;

        public CMTThirdPartyWS()
        {
            _boAccountingControl = new BOAccountingControlWS();
            _boMonitorControl = new BOMonitorControlWS();
        }
        
        /// <summary>
        /// This method requests a change of the edge SLA for a terminal in the CMT. The purpose is to change the SLA
        /// in the hub, but not in the NMS
        /// </summary>
        /// <param name="macAddress">The MAC address of the terminal</param>
        /// <param name="overVolume">Is terminal over volume? 0: Under, 1: Anti abuse, 2 Over
        /// The parameter is used to determine which edge SLA to change to</param>
        /// <returns>True if the request is successfully sent to the CMT</returns>
        [WebMethod]
        public bool ChangeEdgeSLAForTerminal(string macAddress, int overVolume)
        {
            bool result = false;
            Terminal term = _boAccountingControl.GetTerminalDetailsByMAC(macAddress);
            ServiceLevel sla = _boAccountingControl.GetServicePack((int)term.SlaId);
            if (overVolume == 2)
            {
                result = _boMonitorControl.TerminalChangeSla(term.ExtFullName, term.MacAddress, (int)sla.EdgeSlaOverFUP, (AdmStatus)(term.AdmStatus), (int)sla.EdgeISP);
            }
            else if (overVolume == 0)
            {
                result = _boMonitorControl.TerminalChangeSla(term.ExtFullName, term.MacAddress, (int)sla.EdgeSLA, (AdmStatus)(term.AdmStatus), (int)sla.EdgeISP);
            }
            else if (overVolume == 1)
            {
                result = true;
            }

            return result;
        }

        /// <summary>
        /// This method fetches the current terminal details from the CMT database
        /// </summary>
        /// <param name="macAddress">The MAC address of the terminal</param>
        /// <returns>A terminal object</returns>
        [WebMethod]
        public Terminal GetTerminalDetailsByMac(string macAddress)
        {
            return _boAccountingControl.GetTerminalDetailsByMAC(macAddress);
        }

        /// <summary>
        /// This method fetches the MAC addresses of all terminals in the CMT database
        /// </summary>
        /// <returns>A list of strings containing MAC addresses</returns>
        [WebMethod]
        public List<string> GetAllTerminals()
        {
            List<string> macAddressList = new List<string>();
            Terminal[] terminalList = _boAccountingControl.GetTerminals();

            foreach (Terminal t in terminalList)
            {
                macAddressList.Add(t.MacAddress);
            }

            return macAddressList;
        }
    }
}
