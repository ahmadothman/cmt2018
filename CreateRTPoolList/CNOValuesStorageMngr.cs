﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;

namespace CreateRTPoolList
{
    /// <summary>
    /// This class contains the methods adding C/No records to the
    /// CNOValues table and update the Terminals table with the latest
    /// C/No value.
    /// </summary>
    public class CNOValuesStorageMngr
    {
        /// <summary>
        /// The name of the connectionstring as stored in the Application settings
        /// file.
        /// </summary>
        private string _connectionString = "";

        public String LastError { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public CNOValuesStorageMngr()
        {
            _connectionString = Properties.Settings.Default.CmtConnectionString;
        }

        /// <summary>
        /// Inserts the CNO value retrieved from the Hub into the CMT database
        /// </summary>
        /// <param name="SiteId">The Site identifier</param>
        /// <param name="IspId">The ISP identifier</param>
        /// <param name="CNoValue">The retrieved C/No value</param>
        /// <returns>
        /// True if the insert succeeded. If the method returns false the error is 
        /// stored in the LastError property
        /// </returns>
        public Boolean insertCNOValue(DateTime timeStamp, int SiteId, int IspId, double CNoValue)
        {
            Boolean success = false;
            string insertString = "INSERT INTO CNOValues (RecDateTime, SitId, IspId, CNo) " +
                                                    "VALUES (@RecDateTime, @SitId, @IspId, @CNo)";
            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(insertString, conn))
                    {
                        cmd.Parameters.AddWithValue("@RecDateTime", timeStamp);
                        cmd.Parameters.AddWithValue("@SitId", SiteId);
                        cmd.Parameters.AddWithValue("@IspId", IspId);
                        if (CNoValue > 99.99)
                        {
                            CNoValue = -1.00;
                        }
                        cmd.Parameters.AddWithValue("@CNo", CNoValue);
                        int rtn = cmd.ExecuteNonQuery();
                        if (rtn == 0)
                        {
                            LastError = "Unable to insert C/No value in the CNOValues table";
                            success = false;
                        }
                        else
                        {
                            success = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                success = false;
                LastError = ex.Message;
            }

            return success;
        }

        /// <summary>
        /// Update a terminal with the last C/No value
        /// </summary>
        /// <param name="macAddress">The MacAddress</param>
        /// <param name="CNoValue">The C/No value</param>
        /// <returns>True if the update succeeded, false otherwise. If the update fails the error
        /// message is contained by the LastError property</returns>
        public Boolean updateCNOValueForTerminal(string macAddress, double CNoValue)
        {
            string updateString = "UPDATE Terminals SET CNo = @CNoValue WHERE MacAddress = @MacAddress";
            Boolean success = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(updateString, conn))
                    {
                        cmd.Parameters.AddWithValue("@CNoValue", CNoValue);
                        cmd.Parameters.AddWithValue("@MacAddress", macAddress);

                        int rtn = cmd.ExecuteNonQuery();
                        if (rtn == 0)
                        {
                            success = false;
                            LastError = "Could not update terminal: " + macAddress + " with C/No value: " + CNoValue;
                        }
                        else
                        {
                            success = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LastError = ex.Message;
                success = false;
            }

            return success;
        }
    }
}
