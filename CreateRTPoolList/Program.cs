﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;
using System.Configuration;
using System.Globalization;
using CreateRTPoolList.BOAccountingControlWSRef;
using CreateRTPoolList.BOMonitorControlWSRef;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace CreateRTPoolList
{
    class Program
    {
        string _logFile = "";

        static void Main(string[] args)
        {
            Program prg = new Program();
            prg.mainBody(args);
        }

        private void mainBody(string[] args)
        {
            int errorCounter = 0;
            int processedRecords = 0;
            DateTime timeStamp = DateTime.UtcNow;
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("en-US");

            NumberStyles styles = NumberStyles.AllowDecimalPoint | NumberStyles.AllowLeadingSign;

            _logFile = Properties.Settings.Default.LogFile;
            StreamWriter logWriter = new StreamWriter(_logFile);
            BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();

            //Get the list of terminals
            //Eutelsat has complained for the big number of calls per minutes so the number of calls needed to be
            //seperated into two batches
            Terminal[] terms = boAccountingControl.GetTerminals();
            //Terminal[] termsPart1 = new Terminal[terms.Length / 2];
            //Terminal[] termsPart2 = new Terminal[terms.Length-(terms.Length / 2)];

            //Array.Copy(terms, 0, termsPart1, 0, terms.Length / 2);
            //Array.Copy(terms, terms.Length / 2, termsPart2, 0, terms.Length - (terms.Length / 2));

            //For each terminal retrieve the terminal info object
            BOMonitorControlWS boMonitorControl = new BOMonitorControlWS();
            CNOValuesStorageMngr cnoValueStorageMngr = new CNOValuesStorageMngr();

            logWriter.WriteLine("Start processing: " + DateTime.Now);
            logWriter.WriteLine("Terminals to process: " + terms.Length);
            int i = 0;
            Parallel.ForEach(terms, new ParallelOptions { MaxDegreeOfParallelism=5},(t) =>
            {

                //We do only the unlocked terminals
                if (t.AdmStatus == 2)
                {
                    i++;
                    try
                    {
                        //if (t.MacAddress== "00:06:39:86:66:ec")
                        //{
                        //    int i = 6;
                        //}
                        string RTN = boMonitorControl.getRTNValue(t.SitId.ToString(), t.IspId);
                        if (RTN != null)
                            RTN = Regex.Replace(RTN, @"\t|\n|\r", "");
                        bool error = false;

                        //commented CMT-113
                        //if (!cnoValueStorageMngr.insertCNOValue(timeStamp, t.SitId, t.IspId, Double.Parse(RTN, styles)))
                        //{
                        //    logWriter.WriteLine("MacAddress: " + t.MacAddress + " Error: " + cnoValueStorageMngr.LastError);
                        //    error = true;
                        //}

                        if (!cnoValueStorageMngr.updateCNOValueForTerminal(t.MacAddress, Double.Parse(RTN, styles)))
                        {
                            logWriter.WriteLine("MacAddress: " + t.MacAddress + " Error: " + cnoValueStorageMngr.LastError);
                            error = true;
                        }

                        if (!error)
                        {
                            processedRecords++;
                        }
                    }
                    catch (Exception ex)
                    {
                        logWriter.Write("MacAddress: " + t.MacAddress + " Error: ");
                        logWriter.WriteLine(ex.Message);
                        errorCounter++;
                    }
                }

                if (i % 75 == 0)
                    System.Threading.Thread.Sleep(60000);
            });

            ////sleep
            //System.Threading.Thread.Sleep(90000);

            //Parallel.ForEach(termsPart2, (t) =>
            //{
            //    //We do only the unlocked terminals
            //    if (t.AdmStatus == 2)
            //    {
            //        try
            //        {
            //            //if (t.MacAddress== "00:06:39:86:66:ec")
            //            //{
            //            //    int i = 6;
            //            //}
            //            TerminalInfo ti = boMonitorControl.getTerminalInfoByMacAddress(t.MacAddress, t.IspId);
            //            if (ti.RTN != null)
            //                ti.RTN = Regex.Replace(ti.RTN, @"\t|\n|\r", "");
            //            bool error = false;
            //            if (!cnoValueStorageMngr.insertCNOValue(timeStamp, Int32.Parse(ti.SitId), Int32.Parse(ti.IspId), Double.Parse(ti.RTN, styles)))
            //            {
            //                logWriter.WriteLine("MacAddress: " + t.MacAddress + " Error: " + cnoValueStorageMngr.LastError);
            //                error = true;
            //            }

            //            if (!cnoValueStorageMngr.updateCNOValueForTerminal(ti.MacAddress, Double.Parse(ti.RTN, styles)))
            //            {
            //                logWriter.WriteLine("MacAddress: " + t.MacAddress + " Error: " + cnoValueStorageMngr.LastError);
            //                error = true;
            //            }

            //            if (!error)
            //            {
            //                processedRecords++;
            //            }
            //        }
            //        catch (Exception ex)
            //        {
            //            logWriter.Write("MacAddress: " + t.MacAddress + " Error: ");
            //            logWriter.WriteLine(ex.Message);
            //            errorCounter++;
            //        }
            //    }
            //});


            logWriter.WriteLine("End of processing: " + DateTime.Now);
            logWriter.WriteLine("Processed records: " + processedRecords);
            logWriter.WriteLine("Total errors: " + errorCounter);

            logWriter.Flush();
            logWriter.Close();
        }
    }
}
