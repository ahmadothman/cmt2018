﻿using Ninject.Extensions.Logging;
using Ninject;
using VolumeAccountingService.Models.Interfaces;
using VolumeAccountingService.Models.Mail;
using System.Linq;
using System;

namespace VolumeAccountingService.Models
{
    /// <summary>
    /// This service will check the database for users that have passed beyond their allowed volume.
    /// 
    /// At 85% we will give a warning to the user and the distributor
    /// At 100% we will lock the users' account
    /// 
    /// </summary>
    public class VolumeAccountingService : IVolumeAccountingService
    {
        private ILogger _log;

        public VolumeAccountingService(ILogger log)
        {
            _log = log;
        }

        [Inject]
        public IVolumeDatabaseService DatabaseService { get; set; }

        [Inject]
        public ISendMail SendMail { get; set; }

        public void ProcessVolumeReport()
        {
            _log.Debug("Entered: ProcessVolumeReport");
            var emailList = DatabaseService.GetEmailsForSitIds();
            
            // get the 85%'ers
            var warnings = DatabaseService.GetWarningData();
            foreach (var i in warnings)
            {
                // just warn the user with his data depending on the message
                //SendMail.SendMail("me", "you", "that");
                _log.Info(string.Format("Sending a warning for 85% to user {0}", i.sitid));

                

                var contents = string.Format(
                @"
                        <p>Dear customer,</p>
                        <p>Please be advised that you have consumed 85% of the monthly volume allocated to your subscription. We suggest you keep an eye on your consumption by looking at MySatADSL space on our website at www.satadsl.net. </p>
                        <p>Please do not hesitate to contact your local distributor for further details.</p>
                    ", i.rtn_usage_percentage, i.fwd_usage_percentage, i.sitid);
                string emailTo = String.Join(";",emailList.Where(t => t.SitId == i.sitid).Select(t => t.Email));
                if (SendMail.SendMail(contents, "SatADSL Volume Notification 85%", emailTo.Split(';')))
                {
                    DatabaseService.SetNotificationFlag(i.sitid, 1);    
                }

            }

            // get the 100%'ers
            var blocked = DatabaseService.GetBlockedData();
            foreach (var i in blocked) {
                _log.Info(string.Format("Send a blocked warning for 100% to user {0}", i.sitid));
                
                var contents = string.Format(
@"
                        <p>Dear customer, </p>
                        <p>Please be advised that you have reached the limit of the monthly volume allocated to your subscription. This means that your data rate is throttled down until next automatic reset. You can check such date on MySatADSL space on our website www.satadsl.net</p>
                        <p>Please do not hesitate to contact your local distributor for further details or to purchase a manual reset.</p>
                    ", i.rtn_usage_percentage, i.fwd_usage_percentage, i.sitid);
                string emailTo = String.Join(";", emailList.Where(t => t.SitId == i.sitid).Select(t => t.Email));
                if (SendMail.SendMail(contents, "SatADSL Volume Notification 100%", emailTo))
                {
                    DatabaseService.SetNotificationFlag(i.sitid, 2);    
                }
            }
        }
        
        // als we over 85%/100% spreken: is dit dan individueel voor de forward en de return? Of de 2 samen geteld? Igv samentelling, heeft het geen nut dat je vergelijkt met de forward fup en return fup.
        // er zijn servicepacks met null fup waardes: kan ik deze gewoon excluden van de resultaten?
        // momenteel zijn er nogal veel die boven hun limiet zitten: is dat normaal? 


        public void ResetVolumeNotificationWarnings()
        {
            _log.Debug("Entered: ResetVolumeNotificationWarnings");
            var resetIds = DatabaseService.ResetClearedSitIds();
            _log.Debug(string.Format("ResetVolumeNotificationWarnings resulted with : {0} ", resetIds));
        }
    }
}
