﻿namespace VolumeAccountingService.Models.DbContext
{
    public class SitEmail
    {
        public string Email { get; set; }
        public int SitId { get; set; }
    }
}
