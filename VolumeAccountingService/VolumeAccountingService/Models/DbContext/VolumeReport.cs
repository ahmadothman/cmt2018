﻿namespace VolumeAccountingService.Models.DbContext
{
    public class VolumeReport : IVolumeReport
    {
        public int sitid { get; set; }
        public float fwd_allowed_mb { get; set; }
        public float fwd_current_mb { get; set; }
        public float fwd_traffic_left_mb { get; set; }
        public float fwd_usage_percentage { get; set; }
        public float rtn_allowed_mb { get; set; }
        public float rtn_current_mb { get; set; }
        public float rtn_traffic_left_mb { get; set; }
        public float rtn_usage_percentage { get; set; }

    }
}
