﻿using System;
using PetaPoco;

namespace VolumeAccountingService.Models.DbContext
{
    public class VolumeConsumptionAlerts
    {
        public int IspId { get; set; }
        public int SitId { get; set; }

        /// <summary>
        /// Alerttype is 1 : warn, 2 : blocked
        /// </summary>
        public int AlertType { get; set; }

        public DateTime Alerted { get; set; }

    }
}
