﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VolumeAccountingService.Models.Mail
{
    public interface ISendMail
    {
        bool SendMail(string body, string subject, params string[] toAddresses);
    }
}
