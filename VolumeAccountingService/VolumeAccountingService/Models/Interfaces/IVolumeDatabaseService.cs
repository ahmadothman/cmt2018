﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VolumeAccountingService.Models.DbContext;

namespace VolumeAccountingService.Models
{
    public interface IVolumeDatabaseService
    {
        int IspId { get;  }
        /// <summary>
        /// This method will return all sitids and volume information for terminals that have exceeded 85% of their usage,
        /// either on the return or forward volumes.
        /// It will not show terminals that are between 85% and 100% (or above 100)
        /// </summary>
        /// <returns></returns>
        IList<IVolumeReport> GetWarningData();

        /// <summary>
        /// Gets a collection of all sitid's that have valid emails.
        /// </summary>
        /// <returns></returns>
        IList<SitEmail> GetEmailsForSitIds();

        /// <summary>
        /// This method will return all sitids and volume information for terminals that have exceeded 100% of their usage,
        /// either on the return or forward volumes.
        /// </summary>
        /// <returns></returns>
        IList<IVolumeReport> GetBlockedData();

        /// <summary>
        /// Set a warned/blocked notification flag on a sitid/terminal.
        /// </summary>
        /// <param name="sitId"></param>
        /// <param name="type">1 for warning, 2 for blocked</param>
        /// <returns></returns>
        bool SetNotificationFlag(int sitId, int type);

        /// <summary>
        /// Get a list of all sitids that have been warned/blocked before, and now have been reset. forward & return below 85%
        /// </summary>
        /// <returns></returns>
        IList<int> GetResettedSitIds ();

        /// <summary>
        /// Reset the sitids that sitids that have been warned/blocked before, and now have been reset. forward & return below 85%
        /// </summary>
        int ResetClearedSitIds ();

        IList<IVolumeReport> GetTopNResults(string type, int? limit);
    }
}
