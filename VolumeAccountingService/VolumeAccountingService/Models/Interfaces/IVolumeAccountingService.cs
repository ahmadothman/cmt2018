﻿namespace VolumeAccountingService.Models.Interfaces
{
    interface IVolumeAccountingService
    {
        void ProcessVolumeReport();
        void ResetVolumeNotificationWarnings();
    }
}
