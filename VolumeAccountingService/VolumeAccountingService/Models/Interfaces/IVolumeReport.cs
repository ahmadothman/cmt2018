﻿using System;
namespace VolumeAccountingService.Models
{
    public interface IVolumeReport
    {
        float fwd_allowed_mb { get; set; }
        float fwd_current_mb { get; set; }
        float fwd_traffic_left_mb { get; set; }
        float fwd_usage_percentage { get; set; }
        float rtn_allowed_mb { get; set; }
        float rtn_current_mb { get; set; }
        float rtn_traffic_left_mb { get; set; }
        float rtn_usage_percentage { get; set; }
        int sitid { get; set; }
    }
}
