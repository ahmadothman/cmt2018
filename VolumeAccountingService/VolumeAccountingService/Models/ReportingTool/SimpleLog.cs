﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Configuration;

namespace VolumeAccountingService.Models.ReportingTool
{
    /// <summary>
    /// Here the report of outgoing emails is created 
    /// in the for of a CSV file
    /// This does not replace the logging of the app
    /// </summary>
    class SimpleLog
    {
        string _delimiter = ",";
        string _logFilePath = "";
        string _logFileName = "";
        string _date = "";

        public SimpleLog()
        {
            _date = DateTime.Now.ToShortDateString();
            _date = _date.Replace("/", "-");
            _logFilePath = ConfigurationManager.AppSettings["LogFilePath"];
            _logFileName = _logFilePath + "EmailLog_" + _date + ".csv"; //this way a new file is created each day

        }
        /// <summary>
        /// Simple log method recording a sent email 
        /// in the CSV file
        /// </summary>
        /// <param name="addressee">The recipient(s) of the email</param>
        /// <param name="subject">The subject of the email</param>
        /// <param name="body">The body of the email</param>
        public void log(string addressee, string subject, string body)
        {
            using (StreamWriter writer = new StreamWriter(_logFileName, true))
            {
                writer.WriteLine(DateTime.Now + _delimiter + addressee + _delimiter + subject + _delimiter + body);
            }
        }
    }
}
