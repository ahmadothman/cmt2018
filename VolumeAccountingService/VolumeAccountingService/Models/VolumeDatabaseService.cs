﻿using System;
using System.Collections.Generic;
using System.Linq;
using PetaPoco;
using System.Configuration;
using VolumeAccountingService.Models.DbContext;

namespace VolumeAccountingService.Models
{
    public class VolumeDatabaseService : IVolumeDatabaseService
    {
        private int _ispId;

        public VolumeDatabaseService(int ispId)
        {
            _ispId = ispId;
        }

        public IList<IVolumeReport> GetWarningData()
        {
            using (var db = new Database(ConfigurationManager.ConnectionStrings["volumeDatabase"].ConnectionString,
                                         ConfigurationManager.ConnectionStrings["volumeDatabase"].ProviderName))
            {
                return db.Query<VolumeReport>(
                    @"
                        select	vw.sitid, 
		                        -- forward
		                        round(cast(vw.allowed_bytes as float) / 1048576, 2) as fwd_allowed_mb, 
		                        round(cast (vw.current_forward_bytes as float) / 1048576, 2) as fwd_current_mb,
		                        round(cast((vw.allowed_bytes - vw.current_forward_bytes) as float) / 1048576, 2) as fwd_traffic_left_mb,
		                        vw.forward_percentage  as fwd_usage_percentage,
		                        -- return
		                        round(cast(vw.allowed_return_bytes as float) / 1048576, 2) as rtn_allowed_mb, 
		                        round(cast (vw.current_return_bytes as float) / 1048576, 2) as rtn_current_mb,
		                        round(cast((vw.allowed_return_bytes - vw.current_return_bytes) as float) / 1048576, 2) as rtn_traffic_left_mb,
		                        vw.return_percentage as rtn_usage_percentage
                        from vw_volume_usage_statistics4 as vw
                        where vw.ispid = @ispId
                        -- limit to >= 85% with some value
                        and (forward_percentage >= 85 or return_percentage >= 85 or total_percentage >= 85)
                        -- but do not show the ones with 100% somewhere, because they'll be reported in the 100% view
                        and forward_percentage < 100 
                        and return_percentage < 100
                        and total_percentage < 100
                        and sitid not in (select sitid from volumeconsumptionalerts vc where vw.sitid =vc.SitId and vc.AlertType = 1)
                        order by sitid desc
                    ", new { ispId = IspId }).ToList<IVolumeReport>();
            }
        }

        public IList<SitEmail> GetEmailsForSitIds()
        {
            const string sql = @"
                select distinct SitId, Email 
                from Terminals 
                where EMail is not null and EMail <> '' 
                and ispid = @ispId 
                order by sitid 
                ";

            using (var db = new Database(ConfigurationManager.ConnectionStrings["cmtDatabase"].ConnectionString,
                                         ConfigurationManager.ConnectionStrings["cmtDatabase"].ProviderName))
            {
                return db.Query<SitEmail>(sql, new { ispId = IspId }).ToList();
            }

        }

        public IList<IVolumeReport> GetBlockedData()
        {
            using (var db = new Database(ConfigurationManager.ConnectionStrings["volumeDatabase"].ConnectionString,
                                         ConfigurationManager.ConnectionStrings["volumeDatabase"].ProviderName))
            {
                return db.Query<VolumeReport>(
                    @"
                        select	vw.sitid, 
		                        -- forward
		                        round(cast(vw.allowed_bytes as float) / 1048576, 2) as fwd_allowed_mb, 
		                        round(cast (vw.current_forward_bytes as float) / 1048576, 2) as fwd_current_mb,
		                        round(cast((vw.allowed_bytes - vw.current_forward_bytes) as float) / 1048576, 2) as fwd_traffic_left_mb,
		                        vw.forward_percentage  as fwd_usage_percentage,
		                        -- return
		                        round(cast(vw.allowed_return_bytes as float) / 1048576, 2) as rtn_allowed_mb, 
		                        round(cast (vw.current_return_bytes as float) / 1048576, 2) as rtn_current_mb,
		                        round(cast((vw.allowed_return_bytes - vw.current_return_bytes) as float) / 1048576, 2) as rtn_traffic_left_mb,
		                        vw.return_percentage as rtn_usage_percentage
                        from vw_volume_usage_statistics4 as vw
                        where vw.ispid = @ispId 
                        and (forward_percentage >= 100 or return_percentage >= 100 or total_percentage >= 100)
                        and sitid not in (select sitid from volumeconsumptionalerts vc where vw.sitid =vc.SitId and vc.AlertType = 2)
                        order by sitid desc
                    ", new { ispId = IspId }).ToList<IVolumeReport>();
            }
        }

        public IList<int> GetResettedSitIds()
        {
            using (var db = new Database(ConfigurationManager.ConnectionStrings["volumeDatabase"].ConnectionString,
                                         ConfigurationManager.ConnectionStrings["volumeDatabase"].ProviderName))
            {
                return db.Query<int>(
                    @"
                            select distinct vw.sitid
                            from vw_volume_usage_statistics4 as vw
                            join volumeconsumptionalerts vca on vca.SitId = vw.sitid
                            where vw.ispId = @ispId
                            and (forward_percentage < 85 and return_percentage < 85 and total_percentage < 85)
                    ", new { ispId = IspId }).ToList<int>();
            }
        }

        public int ResetClearedSitIds()
        {
            using (var db = new Database(ConfigurationManager.ConnectionStrings["volumeDatabase"].ConnectionString,
                                         ConfigurationManager.ConnectionStrings["volumeDatabase"].ProviderName))
            {
                return db.Execute(@"
                    delete from VolumeConsumptionAlerts
                    where VolumeConsumptionAlerts.SitId in (
	                    select distinct vw.sitid
	                    from vw_volume_usage_statistics4 as vw
	                    join volumeconsumptionalerts vca on vca.SitId = vw.sitid and vw.ispid = vca.ispid
	                    where (forward_percentage < 85 and return_percentage < 85 and total_percentage < 85)
                        and vw.ispId = @ispId
                        )
                ", new { ispId = IspId });

            }
        }

        /// <summary>
        /// Update the volumeconsumptionalerts table. Set 1 for warned, 2 for blocked
        /// </summary>
        /// <param name="sitId"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public bool SetNotificationFlag(int sitId, int type)
        {
            using (var db = new Database(ConfigurationManager.ConnectionStrings["volumeDatabase"].ConnectionString,
                                         ConfigurationManager.ConnectionStrings["volumeDatabase"].ProviderName))
            {
                db.Insert(new VolumeConsumptionAlerts
                              {
                                  IspId = IspId,
                                  SitId = sitId,
                                  AlertType = type,
                                  Alerted = DateTime.Now
                              });
                return true;
            }
        }

        public IList<IVolumeReport> GetTopNResults(string type, int? limit)
        {
            using (var db = new Database(ConfigurationManager.ConnectionStrings["volumeDatabase"].ConnectionString,
                                         ConfigurationManager.ConnectionStrings["volumeDatabase"].ProviderName))
            {
                var query = string.Format(@"
                        select {0}	
                                vw.sitid, 
		                        -- forward
		                        round(cast(vw.allowed_bytes as float) / 1048576, 2) as fwd_allowed_mb, 
		                        round(cast (vw.current_forward_bytes as float) / 1048576, 2) as fwd_current_mb,
		                        round(cast((vw.allowed_bytes - vw.current_forward_bytes) as float) / 1048576, 2) as fwd_traffic_left_mb,
		                        vw.forward_percentage  as fwd_usage_percentage,
		                        -- return
		                        round(cast(vw.allowed_return_bytes as float) / 1048576, 2) as rtn_allowed_mb, 
		                        round(cast (vw.current_return_bytes as float) / 1048576, 2) as rtn_current_mb,
		                        round(cast((vw.allowed_return_bytes - vw.current_return_bytes) as float) / 1048576, 2) as rtn_traffic_left_mb,
		                        vw.return_percentage as rtn_usage_percentage
                        from vw_volume_usage_statistics4 as vw
                        where vw.ispId = @ispId 
                        {1}
                    ", limit.HasValue ? "TOP " + limit.Value : "",
                     " ORDER BY " + (type == "fwd" ? "vw.forward_percentage" : "vw.return_percentage") + " DESC "
                     );
                return db.Query<VolumeReport>(query, new { ispId = IspId }).ToList<IVolumeReport>();
            }
        }


        public int IspId
        {
            get { return _ispId; }
        }

    }
}

