﻿using System;
using System.Collections.Generic;
using System.Configuration;
using Amazon.SimpleEmail;
using Amazon.SimpleEmail.Model;
using Ninject;
using VolumeAccountingService.Models.Mail;
using VolumeAccountingService.Models.ReportingTool;
using VolumeAccountingService.BOLogControlWSRef;

namespace VolumeAccountingService.Models
{
    public class BoAccountingMail : ISendMail
    {
        private BOLogControlWS _boLogControl;
        [Inject]
        public Ninject.Extensions.Logging.ILogger Log { get; set; }

        public bool SendMail(string body, string subject, params string[] toAddresses)
        {
            _boLogControl = new BOLogControlWS();
            SimpleLog reporter = new SimpleLog();
            if (toAddresses.Length == 0)
            {
                Log.Debug("Returning without sending mails because no mailaddress is defined.");
                reporter.log("No emailaddress defined", subject, body);
                return false;
            }
            // minor bits for debugging.
            if (bool.Parse(ConfigurationManager.AppSettings["debug"]))
            {
                Log.Info(string.Format("Sending mail to {0} with subject {1} and body {2}",
                    String.Join(";", toAddresses),
                    subject,
                    body
                    ));
                return true;
            }

            var result = false;

                    try
                    {
                        //var client =
                        //    new AmazonSimpleEmailServiceClient(ConfigurationManager.AppSettings["AmazonAccessId"],
                        //                                       ConfigurationManager.AppSettings["AmazonPrivateKey"]);
                        //var msg = new SendEmailRequest();

                        //// to addresses here
                        //var destinationObj = new Destination(new List<string>(toAddresses));
                        //msg.Source = ConfigurationManager.AppSettings["MailFromAddress"];
                        //msg.ReturnPath = ConfigurationManager.AppSettings["MailFromAddress"];
                        //msg.Destination = destinationObj;

                        //var emailBody = new Body {Html = new Content(body)};
                        //var emailMsg = new Message(new Content(subject), emailBody);
                        //msg.Message = emailMsg;

                        //client.SendEmail(msg);
                        _boLogControl.SendMail(body, subject, toAddresses);
                        result = true;
                        reporter.log(String.Join(";", toAddresses), subject, body);
                    }
                    catch (Exception ex)
                    {
                        result = false;
                        Log.Error("Error when trying to send amazon mail", ex);
                    }
            return result;
        }
    }
}
