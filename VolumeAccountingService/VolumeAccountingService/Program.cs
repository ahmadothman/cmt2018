﻿using System;
using Ninject;
using Ninject.Parameters;
using VolumeAccountingService.Models.Interfaces;
using VolumeAccountingService.Models.Mail;
using VolumeAccountingService.Models;

namespace VolumeAccountingService
{
    class Program
    {
        private static readonly IKernel Kernel = new StandardKernel();

        static void Main(string[] args)
        {
            // configure which ispid we're running for.
            int ispId = 413;
            if (args.Length == 1)
                ispId = Int32.Parse(args[0]);
                
            // setup IoC and configuration parameters
            BindAndConfig(ispId);

            // get our accountingservice, who will do most of the work.
            var volumeAccountingService = Kernel.Get<IVolumeAccountingService>();

            // execute the process volume report
            volumeAccountingService.ProcessVolumeReport();

            // reset the warnings of the terminals for the terminals that have reached expiration.
            volumeAccountingService.ResetVolumeNotificationWarnings();
        }

        static void BindAndConfig(int ispId)
        {
            Kernel.Bind<ISendMail>().To<BoAccountingMail>();
            Kernel.Bind<IVolumeAccountingService>().To<Models.VolumeAccountingService>();
            Kernel.Bind<IVolumeDatabaseService>().To<VolumeDatabaseService>().WithConstructorArgument("ispId", ispId);

            log4net.Config.XmlConfigurator.Configure();
        }
    }
}
