﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
//using System.ServiceModel;
//using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Web.Services.Protocols;
using SatLinkManagerTests.LinkInterfaceWSRef;
using SatLinkManagerTests.SessionInterfaceWSRef;
using SatLinkManagerTests.ProvisioningInterfaceWSRef;
using System.Web.Services.Description;

namespace SatLinkManagerTests
{
    class Program
    {
        
        private static string _username = "SatADSL";
        private static string _password = "S@t4dS1";
        
        static void Main(string[] args)
        {
            Program prg = new Program();
            prg.SLMTests();
        }

        void SLMTests()
        {
            
            vnId _vnId = new vnId();
            //_vnId.id = "SatADSL";
            
            LinkInterface li = new LinkInterface();
            ProvisioningInterface pi = new ProvisioningInterface();
            SessionInterface si = new SessionInterface();
            linkProfile[] profiles;
            terminal[] term;
            virtualNetwork[] vn;
            sessionProfile[] sp;
            session ses = si.findSession((long)1);
            DateTime startTime = DateTime.Now.AddDays(10);
            DateTime endTime = startTime.AddHours(2);
            session conf;

            //try
            //{
            //    co               
            //}
            //catch (Exception ex)
            //{
                
            //    throw;
            //}

            int test = 5;// term.Length;
        }

        private string GetSessionForBooking()
        {
            ProvisioningInterface pi = new ProvisioningInterface();
            sessionProfile[] profiles = pi.getAllSessionProfiles(new vnId());
            //TODO some kind of matching between SLA details and Session Profile names

            return profiles[0].name;


        }
    }
}

namespace SatLinkManagerTests.ProvisioningInterfaceWSRef 
{
    
    //Note the use of the partial class and the CustomHeaderExtensionAttribute.
    //This allows us to add the CustomHeaderExtension
    //without changing the existing proxy code, which allows us to 
    //apply the attribute to just a single method on the proxy.
    public partial class ProvisioningInterface : SoapHttpClientProtocol
    {
        private static string _username = "SatADSL";
        private static string _password = "S@t4dS1";
        private static string _authValue = "";

        protected override WebRequest GetWebRequest(Uri uri)
        {
            _authValue = Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(_username + ":" + _password));
            System.Net.WebRequest request = base.GetWebRequest(uri);
            request.Headers.Add("Authorization", String.Format("Basic {0}", _authValue));
            return request;
        }

    }
}

namespace SatLinkManagerTests.LinkInterfaceWSRef
{

    //Note the use of the partial class and the CustomHeaderExtensionAttribute.
    //This allows us to add the CustomHeaderExtension
    //without changing the existing proxy code, which allows us to 
    //apply the attribute to just a single method on the proxy.
    public partial class LinkInterface : SoapHttpClientProtocol
    {
        private static string _username = "SatADSL";
        private static string _password = "S@t4dS1";
        private static string _authValue = "";

        protected override WebRequest GetWebRequest(Uri uri)
        {
            _authValue = Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(_username + ":" + _password));
            System.Net.WebRequest request = base.GetWebRequest(uri);
            request.Headers.Add("Authorization", String.Format("Basic {0}", _authValue));
            return request;
        }

    }
}

namespace SatLinkManagerTests.SessionInterfaceWSRef
{

    //Note the use of the partial class and the CustomHeaderExtensionAttribute.
    //This allows us to add the CustomHeaderExtension
    //without changing the existing proxy code, which allows us to 
    //apply the attribute to just a single method on the proxy.
    public partial class SessionInterface : SoapHttpClientProtocol
    {
        private static string _username = "SatADSL";
        private static string _password = "S@t4dS1";
        private static string _authValue = "";

        protected override WebRequest GetWebRequest(Uri uri)
        {
            _authValue = Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(_username + ":" + _password));
            System.Net.WebRequest request = base.GetWebRequest(uri);
            request.Headers.Add("Authorization", String.Format("Basic {0}", _authValue));
            return request;
        }

    }
}
