﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Data.SqlClient;
using SyncWithISPIF.BOAccountingControlWSRef;
using SyncWithISPIF.BOMonitorControlWSRef;

namespace SyncWithISPIF
{
    /// <summary>
    /// This application syncs the IP Address of the Terminals table
    /// in the CMTData database with the IP Address reported by the ISPIF. The application
    /// takes the target ISP as its parameter.
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 1 || args.Length == 2)
            {
                Program prog = new Program();
                prog.mainBody(args);
            }
            else
            {
                Console.WriteLine("Usage: SyncWithISPIF [ISPID][UP] (optional)");
            }
        }

        private void mainBody(string[] args)
        {
            int ispId = Int32.Parse(args[0]);
            Boolean updateFlag = false;
            if (args.Length == 2)
            {
                updateFlag = args[1].Equals("UP");
            }
            DateTime startDateTime = DateTime.Now;
            string connectionString = Properties.Settings.Default.ConnectionString;
            Console.WriteLine("--- Application started: " + startDateTime);
            Console.WriteLine("--- Running");

            //Output files
            StreamWriter ipWriter = new StreamWriter("C:/Temp/IPCheck_" + ispId +".csv", false);
            StreamWriter slaIdWriter = new StreamWriter("C:/Temp/SLACheck_" + ispId + ".csv", false);
            StreamWriter statusWriter = new StreamWriter("C:/Temp/StatusCheck_" + ispId + ".csv", false);
            StreamWriter errorWriter = new StreamWriter("C:/Temp/ISPIFErrors.log", false);
            
            DBGateway dbGateway = new DBGateway();

            BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();
            BOMonitorControlWS boMonitorControl = new BOMonitorControlWS();
            Terminal[] terminals = boAccountingControl.GetTerminals();
            int numIpRecords = 0;
            int numSlaRecords = 0;
            int numExceptions = 0;
            int numStatusRecords = 0;

            ipWriter.WriteLine("MacAddress; CMT IP Address; HUB IP Address");
            slaIdWriter.WriteLine("MacAddress; CMT Sla Id; HUB Sla Id");
            statusWriter.WriteLine("MacAddress; CMT Status; HUB Status");

            foreach (Terminal term in terminals)
            {
                if (term.IspId == ispId)
                {
                    try
                    {
                        TerminalInfo termInfo = boMonitorControl.getTerminalDetailsFromHub(term.MacAddress, term.IspId);
                        if (termInfo != null && term.AdmStatus != 6)
                        {
                            if (!term.IpAddress.Trim().Equals(termInfo.IPAddress.Trim()))
                            {
                                ipWriter.WriteLine(term.MacAddress + ";" + term.IpAddress + ";"  + termInfo.IPAddress);
                                term.IpAddress = termInfo.IPAddress;

                                if (updateFlag)
                                {
                                    if (!dbGateway.UpdateIPAddress(term))
                                    {
                                        errorWriter.WriteLine("IP Check, Could not update MAC address: " + term.MacAddress);
                                    }
                                }
                                numIpRecords++;
                            }

                            if (term.SlaId != Int32.Parse(termInfo.SlaId))
                            {
                                ipWriter.WriteLine(term.MacAddress + ";" + term.SlaId + ";" + termInfo.SlaId);
                                term.SlaId = Int32.Parse(termInfo.SlaId);

                                if (updateFlag)
                                {
                                    if (!dbGateway.UpdateSlaId(term))
                                    {
                                        errorWriter.WriteLine("SlaId Check, Could not update MAC address: " + term.MacAddress);
                                    }
                                }
                                numSlaRecords++;
                            }

                            //Check if the terminal flagged as unlocked in the CMT database
                            //is indeed Operational in the ISPIF, if not lock the terminal.
                            if (term.AdmStatus == 2 && !termInfo.Status.Equals("OPERATIONAL"))
                            {
                                statusWriter.WriteLine(term.MacAddress + ";" + "Unlocked" + ";" + termInfo.Status);

                                if (updateFlag)
                                {
                                    if (!dbGateway.LockTerminal(term))
                                    {
                                        errorWriter.WriteLine("Status Check, Could lock terminal: " + term.MacAddress);
                                    }
                                }

                                numStatusRecords++;
                            }

                            //Check if the terminal flagged as locked in the CMT database
                            //is indeed locked in the ISPIF, if not unlock the terminal.
                            if (term.AdmStatus == 1 && !termInfo.Status.Equals("LOCKED"))
                            {
                                statusWriter.WriteLine(term.MacAddress + ";" + "Locked" + ";" + termInfo.Status);

                                if (updateFlag)
                                {
                                    if (!dbGateway.UnLockTerminal(term))
                                    {
                                        errorWriter.WriteLine("Status Check, Could not uplock MAC address: " + term.MacAddress);
                                    }
                                }

                                numStatusRecords++;
                            }
                        }
                    }
                    catch(Exception ex)
                    {
                        errorWriter.WriteLine(term.MacAddress + "   " + ex.Message);
                        numExceptions++;
                    }
                }
            }

            ipWriter.Close();
            slaIdWriter.Close();
            errorWriter.Close();
            statusWriter.Close();
            DateTime endDateTime = DateTime.Now;
            Console.WriteLine("--- IP Check Records processed: " + numIpRecords);
            Console.WriteLine("--- SlaId Check Records processed: " + numSlaRecords);
            Console.WriteLine("--- Exceptions:        " + numExceptions);
            Console.WriteLine("--- Application ended: " + endDateTime);
            Console.WriteLine("--- Processing time: " + endDateTime.Subtract(startDateTime));
        }
    }
}
