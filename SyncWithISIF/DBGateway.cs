﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using SyncWithISPIF.BOAccountingControlWSRef;

namespace SyncWithISPIF
{
    /// <summary>
    /// Contains database update methods
    /// </summary>
    public class DBGateway
    {
        /// <summary>
        /// Updates the IP address in the CMT database
        /// </summary>
        /// <param name="term">The terminal to update</param>
        /// <returns>True if the update succeeded</returns>
        public bool UpdateIPAddress(Terminal term)
        {
            string updateCmd = "UPDATE Terminals SET IPAddress = @IPAddress WHERE MacAddress= @macAddress";

            try
            {
                using (SqlConnection conn = new SqlConnection(Properties.Settings.Default.ConnectionString))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(updateCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@IPAddress", term.IpAddress);
                        cmd.Parameters.AddWithValue("@macAddress", term.MacAddress);

                        int retVal = cmd.ExecuteNonQuery();

                        if (retVal == 0)
                            return false;
                        else
                            return true;
                    }
                }
            }
            catch
            {
                Console.WriteLine("Error while updating IP Address");
                throw;
            }
        }

        /// <summary>
        /// Updates the SLA in the CMT
        /// </summary>
        /// <param name="term">The terminal to update</param>
        /// <returns>True if the update succeeded</returns>
        public bool UpdateSlaId(Terminal term)
        {
            string updateCmd = "UPDATE Terminals SET SlaId  = @SlaId WHERE MacAddress= @macAddress";

            try
            {
                using (SqlConnection conn = new SqlConnection(Properties.Settings.Default.ConnectionString))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(updateCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@SlaId", term.SlaId);
                        cmd.Parameters.AddWithValue("@macAddress", term.MacAddress);

                        int retVal = cmd.ExecuteNonQuery();

                        if (retVal == 0)
                            return false;
                        else
                            return true;
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Locks a terminal (AdmStatus = 1)
        /// </summary>
        /// <param name="term">The terminal to lock</param>
        /// <returns>True if the status was successfuly changed</returns>
        public bool LockTerminal(Terminal term)
        {
             string updateCmd = "UPDATE Terminals SET AdmStatus  = 1 WHERE MacAddress= @macAddress";

            try
            {
                using (SqlConnection conn = new SqlConnection(Properties.Settings.Default.ConnectionString))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(updateCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@macAddress", term.MacAddress);

                        int retVal = cmd.ExecuteNonQuery();

                        if (retVal == 0)
                            return false;
                        else
                            return true;
                    }
                }
            }
            catch
            {
                throw;
            }
        }
        
         /// <summary>
        /// UnLocks a terminal (AdmStatus = 2)
        /// </summary>
        /// <param name="term">The terminal to lock</param>
        /// <returns>True if the status was successfuly changed</returns>
        public bool UnLockTerminal(Terminal term)
        {
             string updateCmd = "UPDATE Terminals SET AdmStatus  = 2 WHERE MacAddress= @macAddress";

            try
            {
                using (SqlConnection conn = new SqlConnection(Properties.Settings.Default.ConnectionString))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(updateCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@macAddress", term.MacAddress);

                        int retVal = cmd.ExecuteNonQuery();

                        if (retVal == 0)
                            return false;
                        else
                            return true;
                    }
                }
            }
            catch
            {
                throw;
            }
        }
    }
}
