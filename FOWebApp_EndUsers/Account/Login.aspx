﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Design/EndUserSite.Master" AutoEventWireup="true"
    CodeBehind="Login.aspx.cs" Inherits="FOWebApp_EndUsers.Account.Login" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="ContentBody" ContentPlaceHolderID="BodyContentPlaceHolder" runat="server">
    <script type="text/javascript">
        function ResetPassword() {
            //Open the password reset window
            var oWnd = radopen("PasswordReset.aspx", "RadWindowPasswordReset");
            return true;
        }
    </script>
    <asp:PlaceHolder ID="PlaceHolderHeader" runat="server"></asp:PlaceHolder>
    <div id="main-container">
        <div class="wrapper clearfix">
            <div id="path">
                <span>You are here:</span> <a href="http://www.satadsl.net/index.html">Home</a>
                &gt; <a href="#">My SatADSL</a>
            </div>
            <div id="main">
                You must be registered to use MySatADSL. If you have not done so, register <a href="Register.aspx">
                    here</a>.
                <br />
                <br />
                <table width="500px">
                    <tr>
                        <td style="border-bottom: 0px; border-top: 0px; background: white; vertical-align: middle;
                            font-size: 12px" class="style1">
                            <asp:Literal ID="LiteralUserName" runat="server">User Name:</asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td style="border-bottom: 0px; border-top: 0px; background: white; vertical-align: middle;
                            font-size: 12px">
                            <telerik:RadTextBox ID="RadTextBoxUserName" runat="server" Font-Size="Larger" Skin="WebBlue"
                                Width="200px">
                            </telerik:RadTextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorUserName" runat="server" ErrorMessage="Please provide a User Name!"
                                ControlToValidate="RadTextBoxUserName" ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td style="border-bottom: 0px; border-top: 0px; background: white; vertical-align: middle;
                            font-size: 12px" class="style1">
                            <asp:Literal ID="LiteralPassword" runat="server">Password:</asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td style="border-bottom: 0px; border-top: 0px; background: white; vertical-align: middle;
                            font-size: 12px">
                            <telerik:RadTextBox ID="RadTextBoxPassword" runat="server" Font-Size="Larger" Skin="WebBlue"
                                TextMode="Password" Width="200px">
                            </telerik:RadTextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorPassword" runat="server" ErrorMessage="Please provide a password!"
                                ControlToValidate="RadTextBoxPassword" ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td style="border-bottom: 0px; border-top: 0px; background: white; vertical-align: middle;
                            font-size: 12px" class="style1">
                            <asp:CheckBox ID="CheckBoxKeepLoggedIn" runat="server" TextAlign="Left" 
                                TabIndex="3" Text="Keep me logged in" Width="300px" />
                        </td>
                    </tr>
                </table>
                <telerik:RadButton ID="RadButtonLogin" runat="server" Text="Login" Skin="WebBlue"
                    OnClick="RadButtonLogin_Click">
                </telerik:RadButton>
                &nbsp&nbsp
                <telerik:RadButton ID="RadButtonPwdReset" runat="server" Text="Password Reset" Skin="WebBlue"
                    AutoPostBack="False" OnClientClicked="ResetPassword" CausesValidation="False">
                </telerik:RadButton>
                <br />
                <p>
                    <asp:Label ID="LabelStatus" runat="server" ForeColor="Red"></asp:Label>
                </p>
            </div>
        </div>
    </div>
    <div id="footer-container">
        <div id="Div1">
            <footer class="wrapper clearfix">
            <div id="footerLinks">
				<span id="copyright">&copy;2012 SatADSL</span>
				<a href="http://www.satadsl.net/Disclaimer.html">Disclaimer</a>
                 <a href="http://www.nimera.net" target="_blank">Created by Nimera Mobile ICT</a>
			</div>
        </footer>
        </div>
    </div>
    <telerik:RadWindowManager ID="RadWindowManagerPasswordReset" runat="server" Animation="Fade"
        DestroyOnClose="True" EnableShadow="True" Skin="WebBlue" Height="350px" Modal="True"
        Width="500px">
        <Windows>
            <telerik:RadWindow ID="RadWindowPasswordReset" runat="server" Title="Modify Terminal"
                Modal="True" Opacity="70" IconUrl="~/Images/key_preferences.png">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
</asp:Content>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="head">
    <style type="text/css">
        .style1
        {
            width: 160px;
        }
    </style>
</asp:Content>
