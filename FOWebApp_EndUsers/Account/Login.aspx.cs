﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

namespace FOWebApp_EndUsers.Account
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Add the about control to the content pane
            Control headerControl = Page.LoadControl("~/DefaultHeader.ascx");
            PlaceHolderHeader.Controls.Clear();
            PlaceHolderHeader.Controls.Add(headerControl);
        }

        protected void RadButtonLogin_Click(object sender, EventArgs e)
        {
            //Validate user
            if (Membership.ValidateUser(RadTextBoxUserName.Text, RadTextBoxPassword.Text))
            {
                FormsAuthentication.RedirectFromLoginPage(RadTextBoxUserName.Text, CheckBoxKeepLoggedIn.Checked);
            }
            else
            {
                LabelStatus.Text = "Your user name or password are not valid!";
            }
        }
    }
}