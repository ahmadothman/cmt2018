﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TerminalStatusTab.ascx.cs"
    Inherits="FOWebApp_EndUsers.TerminalStatusTab" %>
<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>
<!-- <table border="0" cellspacing="5px" class="termDetails">
    <tr>
        <td style="font-size: 12px">
            Forward Volume Allocation:
        </td>
        <td style="font-size: 12px">
            <asp:Label ID="LabelVolumeAllocation" runat="server"></asp:Label>
        </td>
    </tr>
     <tr>
        <td style="font-size: 12px">
            Return Volume Allocation:
        </td>
        <td style="font-size: 12px">
            <asp:Label ID="LabelReturnVolumeAllocation" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td style="font-size: 12px">
            Forward Volume Consumption:
        </td>
        <td style="font-size: 12px">
            <asp:Label ID="LabelVolumeConsumed" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td style="font-size: 12px">
            Return Volume Consumption:
        </td>
        <td style="font-size: 12px">
            <asp:Label ID="LabelReturnVolumeConsumed" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td style="font-size: 12px">
            FUP Reset Day:
        </td>
        <td style="font-size: 12px">
            <asp:Label ID="LabelFUPResetDay" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td style="font-size: 12px">
            Generated on:
        </td>
        <td style="font-size: 12px">
            <asp:Label ID="LabelTimeStamp" runat="server"></asp:Label>
        </td>
    </tr>
</table> -->
<!-- <asp:Panel ID="PanelFreeZone" runat="server" Visible="False" BackColor="#F1FEF3">
<table border="0" cellspacing="5px" class="termDetails">
    <tr>
        <td style="font-size: 12px" width="200px">
            <asp:Label ID="Label2" runat="server" Text="Forward Freezone Consumed:"></asp:Label>
        </td>
        <td style="font-size: 12px">
            <asp:Label ID="LabelFreezoneConsumed" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td style="font-size: 12px">
            <asp:Label ID="Label1" runat="server" Text="Return Freezone Consumed:"></asp:Label>
        </td>
        <td style="font-size: 12px">
            <asp:Label ID="LabelRTNFreezoneConsumed" runat="server"></asp:Label>
        </td>
    </tr>
</table>
</asp:Panel> -->
<div class="chartPane">
    <asp:Chart ID="ChartForwardVolume" runat="server" BackImageAlignment="Center">
        <Series>
        </Series>
        <ChartAreas>
            <asp:ChartArea Name="ChartAreaForward">
            </asp:ChartArea>
        </ChartAreas>
    </asp:Chart>
    <asp:Chart ID="ChartReturnVolume" runat="server" BackImageAlignment="Center">
        <Series>
        </Series>
        <ChartAreas>
            <asp:ChartArea Name="ChartAreaReturn">
            </asp:ChartArea>
        </ChartAreas>
    </asp:Chart>
</div>
