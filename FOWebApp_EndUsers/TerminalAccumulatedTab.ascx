﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TerminalAccumulatedTab.ascx.cs" Inherits="FOWebApp_EndUsers.TerminalAccumulatedTab" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Chart ID="ChartAccumulated" runat="server" ToolTip="Accumulated for 1 month until today">
    <ChartAreas>
        <asp:ChartArea Name="ChartArea1">
        </asp:ChartArea>
    </ChartAreas>
</asp:Chart>
<div>
    Right click to E-Mail, save or print this graph!
</div>