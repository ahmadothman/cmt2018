﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FOWebApp_EndUsers.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp_EndUsers.net.nimera.cmt.BOLogControlWSRef;

namespace FOWebApp_EndUsers
{
    public partial class TerminalTabView : System.Web.UI.UserControl
    {
        //The Mac Address
        public string MacAddress { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //Initialize the tabview with the provided tab address
                if (!MacAddress.Equals(""))
                {
                    //Load the tab view
                    RadTab terminalsTab = RadTabStripTerminalView.FindTabByText("Terminal");
                    AddPageView(terminalsTab);
                }
            }
        }

        private void AddPageView(RadTab tab)
        {
            RadPageView pageView = new RadPageView();
            pageView.ID = tab.Text;
            RadMultiPageTerminalView.PageViews.Add(pageView);
            pageView.CssClass = "pageView";
            tab.PageViewID = pageView.ID;
        }

        protected void RadTabStripTerminalView_TabClick(object sender, Telerik.Web.UI.RadTabStripEventArgs e)
        {
            AddPageView(e.Tab);
            e.Tab.PageView.Selected = true;
        }

        protected void RadMultiPageTerminalView_PageViewCreated(object sender, Telerik.Web.UI.RadMultiPageEventArgs e)
        {
            Control control = null;

            //Add content to the page view depending on the selected tab
            if (e.PageView.ID.Equals("Terminal"))
            {
                if (!MacAddress.Equals(""))
                {
                    //Load the TerminalDetailTab control
                    TerminalDetailTab terminalDetailTab =
                        (TerminalDetailTab)Page.LoadControl("../TerminalDetailTab.ascx");
                    terminalDetailTab.MacAddress = MacAddress;
                    control = terminalDetailTab;
                    control.ID = e.PageView.ID + "_userControl";
                }
            }
            else if (e.PageView.ID.Equals("Status"))
            {
                if (!MacAddress.Equals(""))
                {
                    //Load the TerminalDetailTab control
                    TerminalStatusTab terminalStatusTab =
                        (TerminalStatusTab)Page.LoadControl("../TerminalStatusTab.ascx");
                    terminalStatusTab.MacAddress = MacAddress;
                    control = terminalStatusTab;
                    control.ID = e.PageView.ID + "_userControl";
                }
            }
            else if (e.PageView.ID.Equals("Traffic"))
            {
                if (!MacAddress.Equals(""))
                {
                    //Load the TerminalDetailTab control
                    TerminalConsumedTab terminalConsumedTab =
                        (TerminalConsumedTab)Page.LoadControl("../TerminalConsumedTab.ascx");
                    terminalConsumedTab.MacAddress = MacAddress;
                    control = terminalConsumedTab;
                    control.ID = e.PageView.ID + "_userControl";
                }
            }
            else if (e.PageView.ID.Equals("Accumulated"))
            {
                //Load the TerminalDetailTab control
                TerminalAccumulatedTab terminalAccumulatedTab =
                    (TerminalAccumulatedTab)Page.LoadControl("../TerminalAccumulatedTab.ascx");
                terminalAccumulatedTab.MacAddress = MacAddress;
                control = terminalAccumulatedTab;
                control.ID = e.PageView.ID + "_userControl";
            }
            else
            {
                control = Page.LoadControl("../TestUserControl.ascx");
                control.ID = e.PageView.ID + "_userControl";
            }

            if (control != null)
            {
                e.PageView.Controls.Add(control);
            }
        }
    }
}