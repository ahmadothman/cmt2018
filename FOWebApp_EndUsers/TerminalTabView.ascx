﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TerminalTabView.ascx.cs" Inherits="FOWebApp_EndUsers.TerminalTabView" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script type="text/javascript">
    function onTabSelecting(sender, args) {
        if (args.get_tab().get_pageViewID()) {
            args.get_tab().set_postBack(false);
        }
    }
</script>
<telerik:RadAjaxPanel ID="RadAjaxPanelEndUser" runat="server" Height="600px" Width="450px" LoadingPanelID="RadAjaxLoadingPanel1">
<br />
<!-- Terminal view Rad panel -->
<telerik:RadTabStrip ID="RadTabStripTerminalView" runat="server" 
    MultiPageID="RadMultiPageTerminalView" Skin="WebBlue" 
    SelectedIndex="0" OnClientTabSelecting="onTabSelecting" 
        ontabclick="RadTabStripTerminalView_TabClick">
    <Tabs>
        <telerik:RadTab runat="server" Text="Terminal" Selected="True">
        </telerik:RadTab>
        <telerik:RadTab runat="server" Text="Status">
        </telerik:RadTab>
        <telerik:RadTab runat="server" Text="Traffic" Visible="False">
        </telerik:RadTab>
         <telerik:RadTab runat="server" Text="Accumulated" Visible="False">
        </telerik:RadTab>
    </Tabs>
</telerik:RadTabStrip>
<telerik:RadMultiPage ID="RadMultiPageTerminalView" runat="server" 
    BorderColor="#CCCCCC" BorderStyle="Inset" BorderWidth="2px" Height="400px" 
        ScrollBars="Auto" SelectedIndex="0" BackColor="#f7f7f7" 
        onpageviewcreated="RadMultiPageTerminalView_PageViewCreated">
</telerik:RadMultiPage>
</telerik:RadAjaxPanel>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Default" Width="450" Height="800">
</telerik:RadAjaxLoadingPanel>