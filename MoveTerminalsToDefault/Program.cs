﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;

namespace MoveTerminalsToDefault
{
    class Program
    {
        string defaultOrgQuery = "SELECT Id, DistributorId FROM Organizations WHERE FullName LIKE 'Default%'";
        string termUpdate = "UPDATE Terminals SET OrgId = @OrgId WHERE DistributorId = @DistributorId AND OrgId != 143";

        class Organization
        {
            public int OrgId { get; set; }
            public int DistributorId { get; set; }
        }


        static void Main(string[] args)
        {
            Program prg = new Program();
            prg.mainBody(args);
        }

        private void mainBody(string[] args)
        {
            int numRecords = 0;
            string msg = "";
            int processedRecords = 0;

            List<Organization> organizations = new List<Organization>();

            //Get the connectionstring
            string connectionString = MoveTerminalsToDefault.Properties.Settings.Default.ConnectionString;

            try
            {
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    con.Open();
                    SqlCommand defaultQueryCmd = new SqlCommand(defaultOrgQuery, con);
                    SqlDataReader reader = defaultQueryCmd.ExecuteReader();
                   
                    //Foreach distributor link the terminals to the organization Id, except if the organization
                    //is Express Union
                    while (reader.Read())
                    {
                        if (!reader.IsDBNull(0) && !reader.IsDBNull(1))
                        {
                            Organization o = new Organization();
                            o.DistributorId = reader.GetInt32(1);
                            o.OrgId = reader.GetInt32(0);
                            organizations.Add(o);
                        }
                    }

                    reader.Close();

                    foreach (Organization o in organizations)
                    {

                        SqlCommand termUpdateCmd = new SqlCommand(termUpdate, con);
                        termUpdateCmd.Parameters.AddWithValue("@OrgId", o.OrgId);
                        termUpdateCmd.Parameters.AddWithValue("@DistributorId", o.DistributorId);

                        numRecords = termUpdateCmd.ExecuteNonQuery();
                        msg = "Distributor: " + o.DistributorId + " Processed records: " + numRecords;
                        processedRecords++;

                        this.log(msg);
                    }

                    this.log("Number of processed default organizations: " + processedRecords);
                    reader.Close();
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                this.log(ex.Message);
            }
        }

        private void log(string msg)
        {
            Console.WriteLine("[" + DateTime.Now + "] " + msg);
        }
    }
}
