﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Microsoft.Synchronization;
using Microsoft.Synchronization.Data;
using Microsoft.Synchronization.Data.SqlServer;

namespace ServerDeprovisioning
{
    class Program
    {
        const string _SCOPE = "CMTDataScope";
        const string _ServerConnectionString = "Data source=cmtdata.nimera.net;Database=CMTData; User Id=SatFinAfrica; Password=Lagos#1302";

        static void Main(string[] args)
        {
            Program prg = new Program();
            prg.mainBody();
        }

        private void mainBody()
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(_ServerConnectionString))
                {
                    SqlSyncScopeDeprovisioning serverSqlDepro = new SqlSyncScopeDeprovisioning(conn);
                    serverSqlDepro.DeprovisionScope(_SCOPE);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
