﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace NMSHubGateway
{
    /// <summary>
    /// Thrown if the terminal could not be inserted in the NMS database
    /// </summary>
    [Serializable]
    public class TerminalCreationException : System.Exception
    {
        public TerminalCreationException()
        {
        }

        public TerminalCreationException(string message)
            : base(message)
        {
        }

        public TerminalCreationException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected TerminalCreationException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}