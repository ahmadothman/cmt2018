﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Configuration;
using System.Globalization;
using MySql.Data.MySqlClient;
using NMSHubGateway.Model;

namespace NMSHubGateway
{
    /// <summary>
    /// Data Gateway methods
    /// </summary>
    public class DataGateway
    {
        string _connectionString;

        public DataGateway()
        {
            //Retrieve the connection string from the Web.Config file
            _connectionString = ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
        }

        /// <summary>
        /// Returns true if the specified user exists
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public bool userExists(string userId)
        {
            string queryCmd = "SELECT Id FROM Terminals WHERE Id = @UserId";
            bool result;

            Logger.log("Caller userExists with userId: " + userId);

            using (MySqlConnection con = new MySqlConnection(_connectionString))
            {
                con.Open();
                try
                {
                    using (MySqlCommand command = new MySqlCommand(queryCmd, con))
                    {
                        command.Parameters.AddWithValue("@UserId", userId);
                        MySqlDataReader reader = command.ExecuteReader();

                        result = reader.HasRows;
                    }
                }
                catch (Exception ex)
                {
                    //TO DO: Send an error message to the log file
                    result = false;
                    Logger.log("userExists "+ex.Message);
                }
            }

            return result;
        }

        /// <summary>
        /// Returns true if the terminal exists
        /// </summary>
        /// <param name="macAddress"></param>
        /// <returns></returns>
        public bool terminalExists(long macAddress)
        {
            string queryCmd = "SELECT count(*) FROM Terminals WHERE MacAddress = @macAddress";
            bool result;

            using (MySqlConnection con = new MySqlConnection(_connectionString))
            {
                con.Open();
                try
                {
                    using (MySqlCommand command = new MySqlCommand(queryCmd, con))
                    {
                        command.Parameters.AddWithValue("@macAddress", macAddress);
                        MySqlDataReader reader = command.ExecuteReader();

                        if (reader.Read())
                        {
                            if (reader.GetInt32(0) == 1)
                            {
                                result = true;
                            }
                            else
                            {
                                result = false;
                            }
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
                catch (Exception ex)
                {
                    
                    result = false;
                    Logger.log("terminalExists "+ex.Message);
                }
            }

            return result;
        }

        /// <summary>
        /// Returns true if the IP address exists
        /// in the terminal table
        /// </summary>
        /// <param name="ipAddress"></param>
        /// <returns></returns>
        public bool ipAddressExists(long ipAddress)
        {
            string queryCmd = "SELECT count(*) FROM Terminals WHERE IP = @ipAddress";
            bool result;

            using (MySqlConnection con = new MySqlConnection(_connectionString))
            {
                con.Open();
                try
                {
                    using (MySqlCommand command = new MySqlCommand(queryCmd, con))
                    {
                        command.Parameters.AddWithValue("@ipAddress", ipAddress);
                        MySqlDataReader reader = command.ExecuteReader();

                        if (reader.Read())
                        {
                            if (reader.GetInt32(0) == 1)
                            {
                                result = true;
                            }
                            else
                            {
                                result = false;
                            }
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
                catch (Exception ex)
                {
                    //TO DO: Send an error message to the log file
                    result = false;
                    Logger.log("ipAddressExists "+ex.Message);
                }
            }

            return result;
        }

        public bool slaExists(int slaId)
        {
            string queryCmd = "SELECT * FROM ServicePacks WHERE SlaID = @SlaId";
            bool result;

            using (MySqlConnection con = new MySqlConnection(_connectionString))
            {
                con.Open();
                try
                {
                    using (MySqlCommand command = new MySqlCommand(queryCmd, con))
                    {
                        command.Parameters.AddWithValue("@SlaId", slaId);
                        //command.Parameters.AddWithValue("@IspId", ispId);
                        MySqlDataReader reader = command.ExecuteReader();

                        result = reader.HasRows;
                    }
                }
                catch (Exception ex)
                {
                    //TO DO: Send an error message to the log file
                    result = false;
                    Logger.log(ex.Message);
                }
            }

            return result;
        }



        /// <summary>
        /// Returns true if the user is registerd (linked to a terminal)
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public bool isUserRegistered(string userId)
        {
            string queryCmd = "SELECT MacAddress FROM Terminals WHERE Id = @UserId";
            bool result = false;

            using (MySqlConnection con = new MySqlConnection(_connectionString))
            {
                con.Open();
                try
                {
                    using (MySqlCommand command = new MySqlCommand(queryCmd, con))
                    {
                        command.Parameters.AddWithValue("@UserId", userId);
                        MySqlDataReader reader = command.ExecuteReader();

                        if (reader.Read())
                        {
                            long macAddress = reader.GetInt64(0);
                            if (!macAddress.Equals(0L))
                            {
                                result = true;
                            }
                            else
                            {
                                result = false;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    //TO DO: Send an error message to the log file
                    result = false;
                    Logger.log(ex.Message);
                }
            }

            return result;
        }

        /// <summary>
        /// Returns true if the user is indeed registered for the given terminal
        /// </summary>
        /// <param name="macAddress"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public bool isUserRegisteredForTerminal(long macAddress, string userId)
        {
            string queryCmd = "SELECT Count(*) FROM Terminals WHERE Id = @UserId AND MacAddress = @macAddress";
            bool result = false;

            using (MySqlConnection con = new MySqlConnection(_connectionString))
            {
                con.Open();
                try
                {
                    using (MySqlCommand command = new MySqlCommand(queryCmd, con))
                    {
                        command.Parameters.AddWithValue("@UserId", userId);
                        command.Parameters.AddWithValue("@macAddress", macAddress);
                        MySqlDataReader reader = command.ExecuteReader();

                        result = reader.HasRows;
                    }
                }
                catch (Exception ex)
                {
                    //TO DO: Send an error message to the log file
                    result = false;
                    Logger.log(ex.Message);
                }
            }

            return result;
        }

        /// <summary>
        /// Checks if the terminal has a pending request
        /// </summary>
        /// <param name="macAddress"></param>
        /// <returns></returns>
        public bool hasPendingRequest(long macAddress)
        {
            string queryCmd = "SELECT Status FROM Tickets WHERE MacAddress = @macAddress";
            bool result = false;

            using (MySqlConnection con = new MySqlConnection(_connectionString))
            {
                con.Open();
                try
                {
                    using (MySqlCommand command = new MySqlCommand(queryCmd, con))
                    {
                        command.Parameters.AddWithValue("@macAddress", macAddress);
                        MySqlDataReader reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            int status = reader.GetInt32(0);
                            if (status == (int)RequestStatus.BUSY)
                            {
                                result = true;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    //TO DO: Send an error message to the log file
                    result = false;
                    Logger.log(ex.Message);
                }
            }

            return result;
        }

        /// <summary>
        /// Checks if the terminal is in use 
        /// </summary>
        /// <param name="macAddress"></param>
        /// <returns></returns>
        public bool isTerminalInUse(long macAddress)
        {
            string queryCmd = "SELECT MacAddress FROM Terminals WHERE MacAddress = @macAddress AND State = 1";
            bool result;

            using (MySqlConnection con = new MySqlConnection(_connectionString))
            {
                con.Open();
                try
                {
                    using (MySqlCommand command = new MySqlCommand(queryCmd, con))
                    {
                        command.Parameters.AddWithValue("@macAddress", macAddress);
                        MySqlDataReader reader = command.ExecuteReader();

                        result = reader.HasRows;
                    }
                }
                catch (Exception ex)
                {
                    //TO DO: Send an error message to the log file
                    result = false;
                    Logger.log(ex.Message);
                }
            }

            return result;
        }

        /// <summary>
        /// Returns true if a registration is ongoing.
        /// </summary>
        /// <remarks>
        /// This method checks if a ticket in the BUSY state for the
        /// given MAC address exists.
        /// </remarks>
        /// <param name="macAddress">The terminal MAC address</param>
        /// <returns></returns>
        public bool isRegistrationOnGoing(long macAddress)
        {
            string queryCmd = "SELECT Status FROM Tickets WHERE MacAddress = @macAddress AND Type = @Type";
            bool result = false;

            using (MySqlConnection con = new MySqlConnection(_connectionString))
            {
                con.Open();
                try
                {
                    using (MySqlCommand command = new MySqlCommand(queryCmd, con))
                    {
                        command.Parameters.AddWithValue("@macAddress", macAddress);
                        command.Parameters.AddWithValue("@Type", (int)TypeEnum.CreateRegistration);
                        MySqlDataReader reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            int status = reader.GetInt32(0);
                            if (status == (int)RequestStatus.BUSY)
                            {
                                result = true;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    //TO DO: Send an error message to the log file
                    result = false;
                    Logger.log(ex.Message);
                }
            }

            return result;
        }
        
        /// <summary>
        /// Checks if a Terminal is the active Terminal in a redundant setup, by MacAddress
        /// </summary>
        /// <param name="macAddress"></param>
        /// <returns></returns>
        public bool IsSatDivTerminalActive(long macAddress)
        {
            string queryCmd = "SELECT Active FROM SatDiv WHERE Active = @Active";
            bool result;

            using (MySqlConnection con = new MySqlConnection(_connectionString))
            {
                con.Open();
                try
                {
                    using (MySqlCommand command = new MySqlCommand(queryCmd, con))
                    {
                        command.Parameters.AddWithValue("@Active", macAddress);
                        MySqlDataReader reader = command.ExecuteReader();

                        result = reader.HasRows;
                    }
                }
                catch (Exception ex)
                {
                    //TO DO: Send an error message to the log file
                    result = false;
                    Logger.log(ex.Message);
                }
            }

            return result;
        }
        
        /// <summary>
        /// Generates a new ticket. The ticket is inserted into the Tickets table.
        /// </summary>
        /// <param name="rt"></param>
        /// <param name="macAddress"></param>
        public bool generateTicket(ref RequestTicket rt, TypeEnum type, long macAddress)
        {
            Logger.log("Entering generateTicket with type: " + type.ToString());
            string insertCmd = "INSERT INTO Tickets (TicketId, DateTimeCreated, MacAddress, Status, FailureReason, FailureStackTrace, Type) " +
                                    "VALUES (@TicketId, @DateTimeCreated, @MacAddress, @TicketStatus, '', '', @Type)";

            rt.requestStatus = RequestStatus.BUSY;
            bool result = false;

            using (MySqlConnection con = new MySqlConnection(_connectionString))
            {
                con.Open();

                try
                {
                    using (MySqlCommand command = new MySqlCommand(insertCmd, con))
                    {
                        Logger.log("Before inserting ticket of type: " + type.ToString());
                        int reqStatus = (int)rt.requestStatus;
                        command.Parameters.AddWithValue("@MacAddress", macAddress);
                        command.Parameters.AddWithValue("@DateTimeCreated", DateTime.UtcNow);
                        command.Parameters.AddWithValue("@TicketId", rt.id);
                        command.Parameters.AddWithValue("@TicketStatus", reqStatus);
                        command.Parameters.AddWithValue("@Type", (int)type);
                        int numRows = command.ExecuteNonQuery();

                        if (numRows == 1)
                        {
                            result = true;
                        }

                        Logger.log("After inserting ticket of type: " + type.ToString());
                    }
                }
                catch (Exception ex)
                {
                    rt.requestStatus = RequestStatus.FAILED;
                    rt.failureReason = ex.Message;
                    if (ex.InnerException != null)
                    {
                        rt.failureStackTrace = ex.InnerException.ToString();
                    }
                    result = false;
                    Logger.log(ex.Message);
                }
            }

            return result;
        }

        /// <summary>
        /// Generates a new ticket. The ticket is inserted into the Tickets table.
        /// </summary>
        /// <remarks>
        /// This implementation of generateTicket must be used when changing a Mac Address, an SLA or IP address
        /// </remarks>
        /// <param name="rt"></param>
        /// <param name="macAddress"></param>
        public bool generateTicket(ref RequestTicket rt, TypeEnum type, long oriMacAddress, long newMacAddress, long newSlaId, long newIPAddress, int newIPMask)
        {
            string insertCmd = "INSERT INTO Tickets (TicketId, DateTimeCreated, MacAddress, Status, FailureReason, FailureStackTrace, Type, NewMac, NewIP, NewSLA, NewIPMask) " +
                                    "VALUES (@TicketId, @DateTimeCreated, @MacAddress, @TicketStatus, '', '', @Type, @NewMacAddress, @NewIP, @NewSla, @NewIPMask)";

            rt.requestStatus = RequestStatus.BUSY;
            bool result = false;

            using (MySqlConnection con = new MySqlConnection(_connectionString))
            {
                con.Open();

                try
                {
                    using (MySqlCommand command = new MySqlCommand(insertCmd, con))
                    {
                        Logger.log("Before inserting ticket of type: " + type.ToString());
                        int reqStatus = (int)rt.requestStatus;
                        command.Parameters.AddWithValue("@MacAddress", oriMacAddress);
                        command.Parameters.AddWithValue("@DateTimeCreated", DateTime.UtcNow);
                        command.Parameters.AddWithValue("@TicketId", rt.id);
                        command.Parameters.AddWithValue("@TicketStatus", reqStatus);
                        command.Parameters.AddWithValue("@Type", (int)type);
                        command.Parameters.AddWithValue("@NewMacAddress", newMacAddress);
                        command.Parameters.AddWithValue("@NewSla", newSlaId);
                        command.Parameters.AddWithValue("@NewIP", newIPAddress);
                        command.Parameters.AddWithValue("@NewIPMask", newIPMask);
                        int numRows = command.ExecuteNonQuery();

                        if (numRows == 1)
                        {
                            result = true;
                        }

                        Logger.log("Ticket of type: " + type.ToString() + " inserted, ticketId = " + rt.id);
                    }
                }
                catch (Exception ex)
                {
                    rt.requestStatus = RequestStatus.FAILED;
                    rt.failureReason = ex.Message;
                    if (ex.InnerException != null)
                    {
                        rt.failureStackTrace = ex.InnerException.ToString();
                    }
                    result = false;
                    Logger.log(ex.Message);
                }
            }

            return result;
        }

        /// <summary>
        /// Inserts a terminal into the Terminal table. 
        /// </summary>
        /// <param name="macAddress"></param>
        /// <returns></returns>
        public bool insertTerminal(string endUserId, long macAddress, long ipAddress, int mask, long slaId, int sitId, int ispId, int freeZone)
        {
            return this.insertTerminal(endUserId, macAddress, ipAddress, mask, slaId, sitId, ispId, freeZone
                                                                                , RegistrationStatus.OPERATIONAL);
        }


        

        /// <summary>
        /// Inserts a terminal into the Terminal table. 
        /// </summary>
        /// <param name="macAddress"></param>
        /// <returns></returns>
        public bool insertTerminal(string endUserId, long macAddress, long ipAddress, int mask, long slaId, int sitId, int ispId, int freeZone, RegistrationStatus state)
        {
            DateTime expirationDate = DateTime.UtcNow.AddMonths(1);
            Logger.log("Freezone for Terminal: " + Convertor.ConvertLongToMac(macAddress) + " is " + freeZone);
            //string insertCmd =
            //    "INSERT INTO Terminals (Id, MacAddress, Type, State, SlaId, IP, IPMask, `BWFWD`, `BWRTN`, "
            //    + "MaxVolFWD, MaxVolRTN, MaxVolSum, CurVolFWD, CurVolRTN, CurVolSum, "
            //    + "FreeZoneRTN, FreeZoneFWD, FreeZoneSUM, InFreeZone, ExpirationDate, BadDistributor, OverVolume, "
            //    + "FreeZone, Isp, SitId) "
            //    + "VALUES (@Id, @macAddress, @Type, @RegistrationStatus, @SlaId, @IP, @IPMask, @BWFWD, @BWRTN, "
            //    + "@MaxVolFWD, @MaxVolRTN, @MaxVolSum, @CurVolFWD, @CurVolRTN, @CurVolSum, "
            //    + "@FreeZoneRTN, @FreeZoneFWD, @FreeZoneSUM, @InFreeZone, @ExpirationDate, @BadDistributor, "
            //    + "@OverVolume, @FreeZone, @Isp, @SitId)";
            string insertCmd =
                "INSERT INTO Terminals (Id, MacAddress, State, SlaId, IP, IPMask, `BWFWD`, `BWRTN`, "
                + "MaxVolFWD, MaxVolRTN, MaxVolSum, CurVolFWD, CurVolRTN, CurVolSum, "
                + "FreeZoneRTN, FreeZoneFWD, FreeZoneSUM, InFreeZone, ExpirationDate, BadDistributor, OverVolume, "
                + "FreeZone, Isp, SitId) "
                + "VALUES (@Id, @macAddress, @RegistrationStatus, @SlaId, @IP, @IPMask, @BWFWD, @BWRTN, "
                + "@MaxVolFWD, @MaxVolRTN, @MaxVolSum, @CurVolFWD, @CurVolRTN, @CurVolSum, "
                + "@FreeZoneRTN, @FreeZoneFWD, @FreeZoneSUM, @InFreeZone, @ExpirationDate, @BadDistributor, "
                + "@OverVolume, @FreeZone, @Isp, @SitId)";

            bool result = false;

            using (MySqlConnection con = new MySqlConnection(_connectionString))
            {
                con.Open();
                try
                {
                    using (MySqlCommand command = new MySqlCommand(insertCmd, con))
                    {
                        command.Parameters.AddWithValue("Id", endUserId);
                        command.Parameters.AddWithValue("@macAddress", macAddress);
                        //command.Parameters.AddWithValue("@Type", '0');
                        command.Parameters.AddWithValue("@RegistrationStatus", (int)state);
                        command.Parameters.AddWithValue("@SlaId", slaId);
                        command.Parameters.AddWithValue("@IP", ipAddress);
                        command.Parameters.AddWithValue("@IPMask", mask);
                        command.Parameters.AddWithValue("@BWFWD", 0.0);
                        command.Parameters.AddWithValue("@BWRTN", 0.0);
                        command.Parameters.AddWithValue("@MaxVolFWD", 0.0);
                        command.Parameters.AddWithValue("@MaxVolRTN", 0.0);
                        command.Parameters.AddWithValue("@MaxVolSum", 0.0);
                        command.Parameters.AddWithValue("@CurVolFWD", 0.0);
                        command.Parameters.AddWithValue("@CurVolRTN", 0.0);
                        command.Parameters.AddWithValue("@CurVolSum", 0.0);
                        command.Parameters.AddWithValue("@FreeZoneRTN", 0.0);
                        command.Parameters.AddWithValue("@FreeZoneFWD", 0.0);
                        command.Parameters.AddWithValue("@FreeZoneSUM", 0.0);
                        command.Parameters.AddWithValue("@InFreeZone", false);
                        command.Parameters.AddWithValue("@ExpirationDate", expirationDate);
                        command.Parameters.AddWithValue("@BadDistributor", false);
                        command.Parameters.AddWithValue("@OverVolume", false);
                        command.Parameters.AddWithValue("@FreeZone", freeZone);
                        command.Parameters.AddWithValue("@Isp", ispId);
                        command.Parameters.AddWithValue("@SitId", sitId);

                        int numRows = command.ExecuteNonQuery();

                        if (numRows == 1)
                        {
                            result = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    result = false;
                    Logger.log(ex.Message);
                }
            }

            return result;
        }


        /// <summary>
        /// Inserts a terminal with VNO into the Terminal table. 
        /// </summary>
        /// <param name="macAddress"></param>
        /// <returns></returns>
        public bool insertTerminal(string endUserId, long macAddress, long ipAddress, int mask, long slaId, int sitId, int ispId, int freeZone, string vno)
        {
            DateTime expirationDate = DateTime.UtcNow.AddMonths(1);
            Logger.log("Freezone for Terminal: " + Convertor.ConvertLongToMac(macAddress) + " is " + freeZone);

            string insertCmd =
                "INSERT INTO Terminals (Id, MacAddress, State, SlaId, IP, IPMask, `BWFWD`, `BWRTN`, "
                + "MaxVolFWD, MaxVolRTN, MaxVolSum, CurVolFWD, CurVolRTN, CurVolSum, "
                + "FreeZoneRTN, FreeZoneFWD, FreeZoneSUM, InFreeZone, ExpirationDate, BadDistributor, OverVolume, "
                + "FreeZone, Isp, SitId, VNO) "
                + "VALUES (@Id, @macAddress, @RegistrationStatus, @SlaId, @IP, @IPMask, @BWFWD, @BWRTN, "
                + "@MaxVolFWD, @MaxVolRTN, @MaxVolSum, @CurVolFWD, @CurVolRTN, @CurVolSum, "
                + "@FreeZoneRTN, @FreeZoneFWD, @FreeZoneSUM, @InFreeZone, @ExpirationDate, @BadDistributor, "
                + "@OverVolume, @FreeZone, @Isp, @SitId, @vno )";

            bool result = false;

            using (MySqlConnection con = new MySqlConnection(_connectionString))
            {
                con.Open();
                try
                {
                    using (MySqlCommand command = new MySqlCommand(insertCmd, con))
                    {
                        command.Parameters.AddWithValue("Id", endUserId);
                        command.Parameters.AddWithValue("@macAddress", macAddress);
                        //command.Parameters.AddWithValue("@Type", '0');
                        command.Parameters.AddWithValue("@RegistrationStatus", (int)RegistrationStatus.OPERATIONAL);
                        command.Parameters.AddWithValue("@SlaId", slaId);
                        command.Parameters.AddWithValue("@IP", ipAddress);
                        command.Parameters.AddWithValue("@IPMask", mask);
                        command.Parameters.AddWithValue("@BWFWD", 0.0);
                        command.Parameters.AddWithValue("@BWRTN", 0.0);
                        command.Parameters.AddWithValue("@MaxVolFWD", 0.0);
                        command.Parameters.AddWithValue("@MaxVolRTN", 0.0);
                        command.Parameters.AddWithValue("@MaxVolSum", 0.0);
                        command.Parameters.AddWithValue("@CurVolFWD", 0.0);
                        command.Parameters.AddWithValue("@CurVolRTN", 0.0);
                        command.Parameters.AddWithValue("@CurVolSum", 0.0);
                        command.Parameters.AddWithValue("@FreeZoneRTN", 0.0);
                        command.Parameters.AddWithValue("@FreeZoneFWD", 0.0);
                        command.Parameters.AddWithValue("@FreeZoneSUM", 0.0);
                        command.Parameters.AddWithValue("@InFreeZone", false);
                        command.Parameters.AddWithValue("@ExpirationDate", expirationDate);
                        command.Parameters.AddWithValue("@BadDistributor", false);
                        command.Parameters.AddWithValue("@OverVolume", false);
                        command.Parameters.AddWithValue("@FreeZone", freeZone);
                        command.Parameters.AddWithValue("@Isp", ispId);
                        command.Parameters.AddWithValue("@SitId", sitId);
                        command.Parameters.AddWithValue("@vno", vno);

                        int numRows = command.ExecuteNonQuery();

                        if (numRows == 1)
                        {
                            result = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    result = false;
                    Logger.log(ex.Message);
                }
            }

            return result;
        }





        /// <summary>
        /// Delete a terminal from the Terminals table. This method also deletes the IP range 
        /// associated to the terminal.
        /// </summary>
        /// <remarks>
        /// The deletion is done transactional
        /// </remarks>
        /// <param name="macAddress">The MAC address of the terminal to delete</param>
        /// <returns>True if the operation succeeded</returns>
        public bool deleteTerminal(string endUserId, long macAddress)
        {
            string deleteTerminalCmd = "DELETE FROM Terminals WHERE MacAddress = @MacAddress AND Id = @EndUserId";
            string deleteIPRangeCmd = "DELETE FROM RangeVolumes WHERE MacAddress = @TermMacAddress";

            bool result = false;

            using (MySqlConnection con = new MySqlConnection(_connectionString))
            {
                con.Open();

                MySqlCommand command = con.CreateCommand();
                MySqlTransaction transaction = con.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted);

                command.Connection = con;
                command.Transaction = transaction;

                try
                {
                    Logger.log("Deleting IP Range");
                    command.CommandText = deleteIPRangeCmd;
                    command.Parameters.AddWithValue("@TermMacAddress", macAddress);
                    command.ExecuteNonQuery();
                    Logger.log("After deleting IP Range");

                    Logger.log("Deleting Terminal");
                    command.CommandText = deleteTerminalCmd;
                    command.Parameters.AddWithValue("@MacAddress", macAddress);
                    command.Parameters.AddWithValue("@EndUserId", endUserId);
                    command.ExecuteNonQuery();
                    Logger.log("After Deleting terminal");

                    transaction.Commit();
                    Logger.log("After commit");
                    result = true;
                }
                catch (Exception ex)
                {
                    //TO DO: Send an error message to the log file
                    result = false;
                    Logger.log(ex.Message);
                }
            }

            return result;
        }

        /// <summary>
        /// Identical method to the deleteTerminal(endUserId, macAddress) method.
        /// This method only needs the macAddress.
        /// </summary>
        /// <param name="macAddress">The terminals MacAddress</param>
        /// <returns>True if the operation succeeded</returns>
        public bool deleteTerminal(long macAddress)
        {
            string deleteTerminalCmd = "DELETE FROM Terminals WHERE MacAddress = @MacAddress";
            string deleteIPRangeCmd = "DELETE FROM RangeVolumes WHERE MacAddress = @TermMacAddress";

            bool result = false;

            using (MySqlConnection con = new MySqlConnection(_connectionString))
            {
                con.Open();

                MySqlCommand command = con.CreateCommand();
                MySqlTransaction transaction = con.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted);

                command.Connection = con;
                command.Transaction = transaction;

                try
                {
                    Logger.log("Deleting IP Range");
                    command.CommandText = deleteIPRangeCmd;
                    command.Parameters.AddWithValue("@TermMacAddress", macAddress);
                    command.ExecuteNonQuery();
                    Logger.log("After deleting IP Range");

                    Logger.log("Deleting Terminal");
                    command.CommandText = deleteTerminalCmd;
                    command.Parameters.AddWithValue("@MacAddress", macAddress);
                    command.ExecuteNonQuery();
                    Logger.log("After Deleting terminal");

                    transaction.Commit();
                    Logger.log("After commit");
                    result = true;
                }
                catch (Exception ex)
                {
                    //TO DO: Send an error message to the log file
                    result = false;
                    Logger.log(ex.Message);
                }
            }

            return result;
        }

        /// <summary>
        /// Deletes a user from the database
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        /*public bool deleteUser(string userId)
        {
            string deleteCmd = "DELETE FROM EndUsers WHERE Id = @UserId";

            bool result = false;

            using (MySqlConnection con = new MySqlConnection(_connectionString))
            {
                con.Open();
                try
                {
                    using (MySqlCommand command = new MySqlCommand(deleteCmd, con))
                    {
                        command.Parameters.AddWithValue("@UserId", userId);
                        int numRows = command.ExecuteNonQuery();

                        if (numRows == 1)
                        {
                            result = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    //TO DO: Send an error message to the log file
                    result = false;
                    Logger.log(ex.Message);
                }
            }

            return result;
        }*/

        /// <summary>
        /// Removes all tickets from the Tickets table. This method is used by the
        /// unit tests
        /// </summary>
        /// <returns></returns>
        public bool cleanUpTickets()
        {
            bool result = false;
            string deleteCmd = "DELETE FROM Tickets";

            using (MySqlConnection con = new MySqlConnection(_connectionString))
            {
                con.Open();
                try
                {
                    using (MySqlCommand command = new MySqlCommand(deleteCmd, con))
                    {
                        int numRows = command.ExecuteNonQuery();
                        result = true;
                    }
                }
                catch (Exception ex)
                {
                    //TO DO: Send an error message to the log file
                    result = false;
                    Logger.log(ex.Message);
                }
            }

            return result;
        }

        /// <summary>
        /// Removes a MacAddress from a User. The User record remains in the table.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="macAddress"></param>
        /// <returns></returns>
        /*public bool removeRegistration(string userId, string macAddress)
        {
            string deleteCmd = "UPDATE EndUsers SET MacAddress = 0 WHERE Id = @UserId AND macAddress = @MacAddress";

            bool result = false;

            using (MySqlConnection con = new MySqlConnection(_connectionString))
            {
                con.Open();
                try
                {
                    using (MySqlCommand command = new MySqlCommand(deleteCmd, con))
                    {
                        command.Parameters.AddWithValue("@UserId", userId);
                        command.Parameters.AddWithValue("@MacAddress", Convertor.ConvertMacToLong(macAddress));
                        int numRows = command.ExecuteNonQuery();

                        if (numRows == 1)
                        {
                            result = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    //TO DO: Send an error message to the log file
                    result = false;
                    Logger.log(ex.Message);
                }
            }

            return result;


        }*/

        /// <summary>
        /// This method simple changes all busy states to success states, indicating that the
        /// registration completed successfully.
        /// </summary>
        /// <returns>-1 if the method fails, otherwise the number of rows modified is returned</returns>
        public int completeRegistrations()
        {
            string ticketUpdateCmd = "UPDATE Tickets SET Status = @TargetStatus WHERE Status = @SourceStatus";
            int numRows = 0;

            using (MySqlConnection con = new MySqlConnection(_connectionString))
            {
                con.Open();
                try
                {
                    using (MySqlCommand command = new MySqlCommand(ticketUpdateCmd, con))
                    {
                        command.Parameters.AddWithValue("@TargetStatus", RequestStatus.SUCCESSFUL);
                        command.Parameters.AddWithValue("@SourceStatus", RequestStatus.BUSY);
                        numRows = command.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    //TO DO: Send an error message to the log file
                    numRows = -1;
                    Logger.log(ex.Message);
                }
            }

            return numRows;
        }

        /// <summary>
        /// Returns true if the user is already registered for the given terminal
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="macAddress"></param>
        /// <returns></returns>
        public bool IsSameTerminal(string userId, long macAddress)
        {
            return this.isUserRegisteredForTerminal(macAddress, userId);
        }
        
        /// <summary>
        /// Returns true if the IP address already exists
        /// </summary>
        /// <param name="ipAddress">The IP address</param>
        /// <returns>True if the IP address already exists</returns>
        public bool IsExistingIPAddress(long ipAddress)
        {
            string queryString = "SELECT MacAddress FROM IPUsed WHERE IP = @IPAddress";
            bool result = false;

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_connectionString))
                {
                    conn.Open();
                    using (MySqlCommand cmd = new MySqlCommand(queryString, conn))
                    {
                        cmd.Parameters.AddWithValue("@IPAddress", ipAddress);
                        MySqlDataReader reader = cmd.ExecuteReader();
                        result = reader.HasRows;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex.Message);
            }

            return result;
        }

        /// <summary>
        /// Changes the terminal registration. 
        /// </summary>
        /// <remarks>
        /// This method changes the MAC address for the given user id. Also the IP address is updated. If a mask
        /// is specified this mask is applied which means that the necessary MAC addresses are generated and
        /// stored in the database.
        /// </remarks>
        /// <param name="userId">The unique user id </param>
        /// <param name="macAddress">The new MAC address for this terminal</param>
        /// <returns>True if the operation succeeded.</returns>
        public bool changeTerminal(string userId, long newMacAddress)
        {
            string updateEndUsersCmd = "UPDATE Terminals SET MacAddress = @MacAddress WHERE Id = @UserId";

            bool result = false;

            using (MySqlConnection con = new MySqlConnection(_connectionString))
            {
                con.Open();
                try
                {
                    using (MySqlCommand command = new MySqlCommand(updateEndUsersCmd, con))
                    {
                        command.Parameters.AddWithValue("@MacAddress", newMacAddress);
                        command.Parameters.AddWithValue("@UserId", userId);
                        int numRows = command.ExecuteNonQuery();

                        result = (numRows == 1);
                    }
                }
                catch (Exception ex)
                {
                    //TO DO: Send an error message to the log file
                    result = false;
                    Logger.log(ex.Message);
                }
            }

            return result;
        }

        /// <summary>
        /// Change the sla field in the EndUsers table
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="slaId"></param>
        /// <returns></returns>
        public bool changeSla(string userId, long newSlaId, int ispId)
        {
            //In this version of the NMSGateway we do not set the SlaId in the Terminals table. Only a
            //ticket is inserted and the NMS itself sets the new SlaId after the update is done in the router!
            //This might change later so for the moment keep this code in comment.
            /*
            string changeTerminalCmd = "UPDATE Terminals SET SlaId = @SlaId WHERE Id = @UserId AND Isp = @IspId";

            bool result = false;

            using (MySqlConnection con = new MySqlConnection(_connectionString))
            {
                con.Open();
                try
                {
                    using (MySqlCommand command = new MySqlCommand(changeTerminalCmd, con))
                    {
                        command.Parameters.AddWithValue("@SlaId", newSlaId);
                        command.Parameters.AddWithValue("@UserId", userId);
                        command.Parameters.AddWithValue("@IspId", ispId);
                        int numRows = command.ExecuteNonQuery();

                        if (numRows == 1)
                        {
                            result = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    //TO DO: Send an error message to the log file
                    result = false;
                    Logger.log(ex.Message);
                }
            }

            return result;*/
            return true;
        }

        /// <summary>
        /// Changes the status of a terminal (LOCKED or OPERATIONAL)
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public bool changeStatus(string userId, RegistrationStatus status)
        {
            string changeTerminalCmd = "UPDATE Terminals SET  State = @State WHERE MacAddress = @MacAddress";

            //Get the MacAddress of userId
            long macAddress = this.getRegisteredTerminal(userId);
            bool result = false;

            using (MySqlConnection con = new MySqlConnection(_connectionString))
            {
                con.Open();
                try
                {
                    using (MySqlCommand command = new MySqlCommand(changeTerminalCmd, con))
                    {
                        command.Parameters.AddWithValue("@State", (int)status);
                        command.Parameters.AddWithValue("@MacAddress", macAddress);
                        int numRows = command.ExecuteNonQuery();

                        if (numRows == 1)
                        {
                            result = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    //TO DO: Send an error message to the log file
                    result = false;
                    Logger.log(ex.Message);
                }
            }

            return result;
        }

        /// <summary>
        /// Changes the weight or bandwidth correction of a terminal
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="newWeight">The weight</param>
        /// <param name="customerId">The ID of the customer the terminal belongs to</param>
        /// <returns></returns>
        public bool changeWeight(string userId, int newWeight, int customerId)
        {
            string changeTerminalCmd = "UPDATE Terminals SET BWcorrection = @BWcorrection, CustomerID = @CustomerID WHERE MacAddress = @MacAddress";

            //Get the MacAddress of userId
            long macAddress = this.getRegisteredTerminal(userId);
            bool result = false;

            using (MySqlConnection con = new MySqlConnection(_connectionString))
            {
                con.Open();
                try
                {
                    using (MySqlCommand command = new MySqlCommand(changeTerminalCmd, con))
                    {
                        command.Parameters.AddWithValue("@BWcorrection", newWeight);
                        command.Parameters.AddWithValue("@CustomerID", customerId);
                        command.Parameters.AddWithValue("@MacAddress", macAddress);
                        int numRows = command.ExecuteNonQuery();

                        if (numRows == 1)
                        {
                            result = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    //TO DO: Send an error message to the log file
                    result = false;
                    Logger.log(ex.Message);
                }
            }

            return result;
        }

        /// <summary>
        /// This method returns the terminal registered for a user
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public long getRegisteredTerminal(string userId)
        {
            string registeredTerminalCmd = "SELECT MacAddress FROM Terminals WHERE Id = @userId";
            long macAddress = 0;

            using (MySqlConnection conn = new MySqlConnection(_connectionString))
            {
                conn.Open();

                using (MySqlCommand cmd = new MySqlCommand(registeredTerminalCmd, conn))
                {
                    try
                    {
                        cmd.Parameters.AddWithValue("@userId", userId);
                        MySqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            macAddress = reader.GetInt64(0);
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.log(ex.Message);
                    }
                }
            }

            return macAddress;
        }

        /// <summary>
        /// Returns the base IP address corresponding to the given MacAddress
        /// </summary>
        /// <param name="macAddress">The source MacAddress</param>
        /// <exception cref="InvalidIPAddressException">
        ///     Thrown if no IP Address is specified for the given MAC addresss
        /// </exception>
        /// <returns>The IP address as a long</returns>
        public long getTerminalIpAddress(long macAddress)
        {
            string queryCmd = "SELECT IP FROM Terminals WHERE MacAddress = @MacAddress";
            long ipAddress = 0L;

            using (MySqlConnection conn = new MySqlConnection(_connectionString))
            {
                conn.Open();

                try
                {
                    using (MySqlCommand cmd = new MySqlCommand(queryCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@MacAddress", macAddress);
                        MySqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            ipAddress = reader.GetInt64(0);
                        }
                        else
                        {
                            throw new InvalidIPAddressException("MacAddress: " + macAddress + " has no matching IP");
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.log(ex.Message);
                }
            }

            return ipAddress;
        }


        /// <summary>
        /// This method returns the terminal registered for a user belonging to a specific Isp
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public long getRegisteredTerminal(string userId, int Isp)
        {
            string registeredTerminalCmd = "SELECT MacAddress FROM Terminals WHERE Id = @userId AND Isp = @Isp";
            long macAddress = 0;

            using (MySqlConnection conn = new MySqlConnection(_connectionString))
            {
                conn.Open();

                using (MySqlCommand cmd = new MySqlCommand(registeredTerminalCmd, conn))
                {
                    try
                    {
                        cmd.Parameters.AddWithValue("@userId", userId);
                        cmd.Parameters.AddWithValue("@Isp", Isp);
                        MySqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            macAddress = Int64.Parse(reader.GetString(0).Trim());
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.log(ex.Message);
                    }
                }
            }

            return macAddress;
        }

        /// <summary>
        /// This method returns the terminal registered for a user belonging to a specific Isp
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public long getRegisteredTerminalBySitId(int Isp, int sitId)
        {
            string registeredTerminalCmd = "SELECT MacAddress FROM Terminals WHERE SitId = @SitId AND Isp = @Isp"; ;
            string macAddress = "";

            using (MySqlConnection conn = new MySqlConnection(_connectionString))
            {
                conn.Open();

                using (MySqlCommand cmd = new MySqlCommand(registeredTerminalCmd, conn))
                {
                    try
                    {
                        cmd.Parameters.AddWithValue("@SitId", sitId);
                        cmd.Parameters.AddWithValue("@Isp", Isp);
                        MySqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            macAddress = reader.GetString(0).Trim();
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.log(ex.Message);
                    }
                }
            }

            return Int64.Parse(macAddress);
        }

        /// <summary>
        /// Returns the ticket identifief by RequestTicketId
        /// </summary>
        /// <param name="RequestTicketId"></param>
        /// <returns></returns>
        public RequestTicket getRequestTicket(string RequestTicketId)
        {
            string requestTicketQueryCmd = "SELECT TicketId, Status, FailureReason, FailureStackTrace FROM Tickets " +
                                                "WHERE TicketId = @TicketId";

            RequestTicket rt = new RequestTicket();

            using (MySqlConnection conn = new MySqlConnection(_connectionString))
            {
                conn.Open();

                using (MySqlCommand cmd = new MySqlCommand(requestTicketQueryCmd, conn))
                {
                    try
                    {
                        cmd.Parameters.AddWithValue("@TicketId", RequestTicketId);
                        MySqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            rt.id = reader.GetGuid(0).ToString();
                            rt.requestStatus = (RequestStatus)reader.GetInt32(1);
                            rt.failureReason = reader.GetString(2);
                            rt.failureStackTrace = reader.GetString(3);
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.log(ex.Message);
                    }
                }
            }

            return rt;
        }

        /// <summary>
        /// This method returns the type of the ticket
        /// </summary>
        /// <param name="RequestTicketId">The ticket identifier</param>
        /// <exception cref=">InvalidTicketException">Thrown if the the ticket does not exist</exception>
        /// <returns>The type of the ticket as a TypeEnum value</returns>
        public TypeEnum getTicketType(string RequestTicketId)
        {
            string queryCmd = "SELECT Type FROM Tickets WHERE TicketId = @TicketId";
            TypeEnum type;

            using (MySqlConnection conn = new MySqlConnection(_connectionString))
            {
                conn.Open();
                using (MySqlCommand cmd = new MySqlCommand(queryCmd, conn))
                {
                    cmd.Parameters.AddWithValue("@TicketId", RequestTicketId);
                    MySqlDataReader reader = cmd.ExecuteReader();

                    if (reader.Read())
                    {
                        type = (TypeEnum)reader.GetInt32(0);
                    }
                    else
                    {
                        throw new InvalidTicketException("Ticket: " + RequestTicketId + " does not exist");
                    }
                }
            }

            return type;
        }

        /// <summary>
        /// Returns the MAC address from a ticket record
        /// </summary>
        /// <param name="RequestTicketId">The ticket identifier</param>
        /// <exception cref=">InvalidTicketException">Thrown if the the ticket does not exist</exception>
        /// <returns>The internal MAC address as a long value</returns>
        public long getTicketMacAddress(string RequestTicketId)
        {
            string queryCmd = "SELECT MacAddress FROM Tickets WHERE TicketId = @TicketId";
            long macAddress = 0L;

            using (MySqlConnection conn = new MySqlConnection(_connectionString))
            {
                conn.Open();
                using (MySqlCommand cmd = new MySqlCommand(queryCmd, conn))
                {
                    cmd.Parameters.AddWithValue("@TicketId", RequestTicketId);
                    MySqlDataReader reader = cmd.ExecuteReader();

                    if (reader.Read())
                    {
                       macAddress = reader.GetInt64(0);
                    }
                    else
                    {
                        throw new InvalidTicketException("Ticket: " + RequestTicketId + " does not exist");
                    }
                }
            }
            return macAddress;
        }

        /// <summary>
        /// Sets the consumed volume column back to 0 for the given terminal
        /// </summary>
        /// <param name="endUserId"></param>
        /// <returns></returns>
        public bool resetVolume(long macAddress)
        {
            string resetCmd = "UPDATE Terminals SET CurVolFWD = 0.0, CurVolRTN = 0.0, CurVolSum = 0.0, FreeZoneRTN = 0.0, FreeZoneFWD = 0.0, FreeZoneSum = 0.0 " +
                                    "WHERE MacAddress = @MacAddress";

            bool result = false;

            using (MySqlConnection con = new MySqlConnection(_connectionString))
            {
                con.Open();
                try
                {
                    using (MySqlCommand command = new MySqlCommand(resetCmd, con))
                    {
                        command.Parameters.AddWithValue("@MacAddress", macAddress);
                        int numRows = command.ExecuteNonQuery();

                        if (numRows == 1)
                        {
                            result = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    //TO DO: Send an error message to the log file
                    result = false;
                    Logger.log(ex.Message);
                }
            }

            return result;

        }

        /// <summary>
        /// Returns  the volume for a terminal
        /// </summary>
        /// <param name="macAddress"></param>
        /// <returns></returns>
        public double getTerminalVolume(string macAddress)
        {
            string volumeQueryCmd = "SELECT CurVolSum FROM Terminals WHERE MacAddress = @MacAddress";
            double volume = 0.0;

            using (MySqlConnection conn = new MySqlConnection(_connectionString))
            {
                conn.Open();

                using (MySqlCommand cmd = new MySqlCommand(volumeQueryCmd, conn))
                {
                    try
                    {
                        cmd.Parameters.AddWithValue("@MacAddress", Convertor.ConvertMacToLong(macAddress));
                        MySqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            volume = reader.GetDouble(0);
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.log(ex.Message);
                    }
                }
            }

            return volume;
        }

        /// <summary>
        /// Adds new volume to the given terminal
        /// </summary>
        /// <param name="sitId">The SitId of the terminal</param>
        /// <param name="volume">The amount of volume to add</param>
        /// <param name="ispId">The ispId</param>
        /// <param name="addMaxVolFwd">The forward volume added</param>
        /// <param name="addMaxVolRtn">The return volume added</param>
        /// <param name="validity">The number of days the new volume is valid</param>
        /// <returns>True if the operation succeeded</returns>
        public bool addVolumeToTerminal(int sitId, int ispId, long addMaxVolFwd, long addMaxVolRtn, int validity)
        {
            bool success = false;
            DateTime expirationDate;
            string terminalQueryString = "SELECT MaxVolSum, MaxVolFWD, MaxVolRTN, CurVolSum, CurVolFWD, CurVolRTN FROM Terminals WHERE SitId = @SitId AND Isp = @IspId";
            string updateVolumeString = "UPDATE Terminals SET ExpirationDate = @ExpirationDate, MaxVolSum = @MaxVolSum, MaxVolFwd = @MaxVolFwd, MaxVolRtn = @MaxVolRtn, CurVolSum = @CurVolSum, CurVolFWD = @CurVolFWD, CurVolRTN = @CurVolRTN" +
            " WHERE SitId=@SitId AND Isp=@IspId";

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_connectionString))
                {
                    conn.Open();

                    using (MySqlCommand terminalQuery = new MySqlCommand(terminalQueryString, conn))
                    {
                        terminalQuery.Parameters.AddWithValue("@SitId", sitId);
                        terminalQuery.Parameters.AddWithValue("@IspId", ispId);
                        Logger.log("Terminal Query:"  + terminalQuery.ToString());
                        Logger.log("Before TerminalQuery execute");
                        MySqlDataReader readerTerminal = terminalQuery.ExecuteReader();
                        Logger.log("After TerminalQuery execute");
                        if (readerTerminal.Read())
                        {
                            long maxVolSum = readerTerminal.GetInt64(0);
                            long maxVolFwd = readerTerminal.GetInt64(1);
                            long maxVolRtn = readerTerminal.GetInt64(2);
                            long curVolSum = readerTerminal.GetInt64(3);
                            long curVolFWD = readerTerminal.GetInt64(4);
                            long curVolRTN = readerTerminal.GetInt64(5);
                            
                            readerTerminal.Close();
                            
                            //Calculate new values
                            if (curVolSum > maxVolSum)
                            {
                                //setting all three values to the total value of the voucher
                                //addMaxVolFWD and addMaxVolRtn both contain the same value, so either can be used
                                maxVolFwd = addMaxVolFwd;
                                maxVolRtn = addMaxVolFwd;
                                maxVolSum = addMaxVolFwd;
                            }
                            else
                            {
                                //setting all three values to the total value of the voucher
                                //addMaxVolFWD and addMaxVolRtn both contain the same value, so either can be used
                                long currentRemaining = maxVolSum - curVolSum;
                                //if the last voucher was unlimited then negelect the whole remaining data GBs
                                if (maxVolSum >= 9000000000000)
                                    currentRemaining = 0;

                                maxVolFwd = currentRemaining + addMaxVolFwd;
                                maxVolRtn = currentRemaining + addMaxVolFwd;
                                maxVolSum = currentRemaining + addMaxVolFwd;
                            }
                            //all current volumes are reset
                            curVolFWD = 0;
                            curVolRTN = 0;
                            curVolSum = 0;
                            //Set the expiration date to today + period of validity 
                            expirationDate = DateTime.UtcNow.AddDays(validity);

                            //Update terminals table
                            using (MySqlCommand updateTerminal = new MySqlCommand(updateVolumeString, conn))
                            {
                                updateTerminal.Parameters.AddWithValue("@ExpirationDate", expirationDate);
                                updateTerminal.Parameters.AddWithValue("@MaxVolSum", maxVolSum);
                                updateTerminal.Parameters.AddWithValue("@MaxVolFwd", maxVolFwd);
                                updateTerminal.Parameters.AddWithValue("@MaxVolRtn", maxVolRtn);
                                updateTerminal.Parameters.AddWithValue("@CurVolSum", curVolSum);
                                updateTerminal.Parameters.AddWithValue("@CurVolFWD", curVolFWD);
                                updateTerminal.Parameters.AddWithValue("@CurVolRTN", curVolRTN);
                                updateTerminal.Parameters.AddWithValue("@SitId", sitId);
                                updateTerminal.Parameters.AddWithValue("@IspId", ispId);
                                int rowsUpdated = updateTerminal.ExecuteNonQuery();

                                if (rowsUpdated != 1)
                                {
                                    Logger.log("Unable to add volume to terminal with SitId: " + sitId + " and ISP: " + ispId);
                                }
                                else
                                {
                                    Logger.log(addMaxVolFwd + " bytes added to column MaxVolSum or SitId: " + sitId + "/ Isp: " + ispId);
                                    success = true;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex.Message);
                Logger.log(ex.StackTrace);
                Logger.log(ex.Source);
            }
            return success;
        }

        /// <summary>
        /// Returns the TerminalInfo object identified  by its MAC Address
        /// </summary>
        /// <remarks>
        /// The returned object can be null
        /// </remarks>
        /// <param name="macAddress"></param>
        /// <returns></returns>
        public NMSTerminalInfo getTerminalInfoByMac(long macAddress)
        {
            string queryString = "SELECT MacAddress, State, SlaId, IP, IPMask, BWFWD, BWRTN, " +
                    "MaxVolFWD, MaxVolRTN, MaxVolSum, CurVolFWD, CurVolRTN, CurVolSum, FreeZoneRTN, FreeZoneFWD, " +
                    "FreeZoneSUM, InFreeZone, ExpirationDate, BadDistributor, OverVolume, FreeZone, Isp, SitId " +
                    "FROM Terminals WHERE MacAddress = @macAddress";


            NMSTerminalInfo ti = null;

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_connectionString))
                {
                    conn.Open();

                    using (MySqlCommand cmd = new MySqlCommand(queryString, conn))
                    {
                        cmd.Parameters.AddWithValue("@macAddress", macAddress);
                        MySqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            ti = new NMSTerminalInfo();
                            if (!reader.IsDBNull(0))
                                ti.MacAddress = Convertor.ConvertLongToMac(reader.GetInt64("MacAddress"));

                            //if (!reader.IsDBNull(1))
                            //    ti.Type = reader.GetString("Type");

                            if (!reader.IsDBNull(1))
                                ti.State = reader.GetInt32("State");

                            if (!reader.IsDBNull(2))
                                ti.SlaId = reader.GetInt32("SlaId");

                            if (!reader.IsDBNull(3))
                                ti.IP = Convertor.ConvertLongToIP(reader.GetInt64("IP"));

                            if (!reader.IsDBNull(4))
                                ti.IPMask = reader.GetInt32("IPMask");

                            if (!reader.IsDBNull(5))
                                ti.BwFwd = reader.GetFloat("BWFWD");

                            if (!reader.IsDBNull(6))
                                ti.BwRtn = reader.GetFloat("BWRTN");

                            if (!reader.IsDBNull(7))
                                ti.MaxVolFwd = reader.GetFloat("MaxVolFWD");

                            if (!reader.IsDBNull(8))
                                ti.MaxVolRtn = reader.GetFloat("MaxVolRTN");

                            if (!reader.IsDBNull(9))
                                ti.MaxVolSum = reader.GetFloat("MaxVolSum");

                            if (!reader.IsDBNull(10))
                                ti.CurVolFwd = reader.GetFloat("CurVolFWD");

                            if (!reader.IsDBNull(11))
                                ti.CurVolRtn = reader.GetFloat("CurVolRTN");

                            if (!reader.IsDBNull(12))
                                ti.CurVolSum = reader.GetFloat("CurVolSum");

                            if (!reader.IsDBNull(13))
                                ti.FreeZoneRtn = reader.GetFloat("FreeZoneRTN");

                            if (!reader.IsDBNull(14))
                                ti.FreeZoneFwd = reader.GetFloat("FreeZoneFWD");

                            if (!reader.IsDBNull(15))
                                ti.FreeZoneSum = reader.GetFloat("FreeZoneSUM");

                            if (!reader.IsDBNull(16))
                                ti.InFreeZone = reader.GetBoolean("InFreeZone");

                            if (!reader.IsDBNull(17))
                                ti.ExpirationDate = reader.GetDateTime("ExpirationDate");

                            if (!reader.IsDBNull(18))
                                ti.BadDistributor = reader.GetBoolean("BadDistributor");

                            if (!reader.IsDBNull(19))
                                ti.OverVolume = reader.GetBoolean("OverVolume");

                            if (!reader.IsDBNull(20))
                                ti.FreeZone = reader.GetInt32("FreeZone");

                            if (!reader.IsDBNull(21))
                                ti.Isp = reader.GetInt32("Isp");

                            if (!reader.IsDBNull(22))
                                ti.SitId = reader.GetInt32("SitId");

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex.Message);
            }

            return ti;
        }

        /// <summary>
        /// Returns the terminal idle time in minutes
        /// </summary>
        /// <remarks>
        /// If the value is null the method returns -1
        /// </remarks>
        /// <param name="macAddress"></param>
        /// <returns>Terminal idle time in minutes</returns>
        public int getTerminalIdleTime(long macAddress)
        {
            string queryString = "SELECT LastTrafficTime " +
                    "FROM Terminals WHERE State=1 and MacAddress = @macAddress";
                       

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_connectionString))
                {
                    conn.Open();

                    using (MySqlCommand cmd = new MySqlCommand(queryString, conn))
                    {
                        cmd.Parameters.AddWithValue("@macAddress", macAddress);
                        MySqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {

                            if (!reader.IsDBNull(0))
                                return reader.GetInt32("LastTrafficTime");
                            else
                                return -1;



                        }
                        else
                            return -1;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex.Message);
            }

            return -1;
        }


        /// <summary>
        /// Returns a list of terminal info objects from the NMS
        /// </summary>
        /// <returns>A list of terminal info objects</returns>
        public List<NMSTerminalInfo> getTerminals()
        {
            //string queryString = "SELECT MacAddress, Type, State, SlaId, IP, IPMask, BWFWD, BWRTN, " +
            //        "MaxVolFWD, MaxVolRTN, MaxVolSum, CurVolFWD, CurVolRTN, CurVolSum, FreeZoneRTN, FreeZoneFWD, " +
            //        "FreeZoneSUM, InFreeZone, ExpirationDate, BadDistributor, OverVolume, FreeZone, Isp, SitId " +
            //        "FROM Terminals";
            string queryString = "SELECT MacAddress, State, SlaId, IP, IPMask, BWFWD, BWRTN, " +
                     "MaxVolFWD, MaxVolRTN, MaxVolSum, CurVolFWD, CurVolRTN, CurVolSum, FreeZoneRTN, FreeZoneFWD, " +
                     "FreeZoneSUM, InFreeZone, ExpirationDate, BadDistributor, OverVolume, FreeZone, Isp, SitId " +
                     "FROM Terminals";

            List<NMSTerminalInfo> termList = new List<NMSTerminalInfo>();
            NMSTerminalInfo ti = null;

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_connectionString))
                {
                    conn.Open();

                    using (MySqlCommand cmd = new MySqlCommand(queryString, conn))
                    {
                        MySqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            ti = new NMSTerminalInfo();
                            if (!reader.IsDBNull(0))
                                ti.MacAddress = Convertor.ConvertLongToMac(reader.GetInt64("MacAddress"));

                            //if (!reader.IsDBNull(1))
                            //    ti.Type = reader.GetString("Type");

                            if (!reader.IsDBNull(1))
                                ti.State = reader.GetInt32("State");

                            if (!reader.IsDBNull(2))
                                ti.SlaId = reader.GetInt32("SlaId");

                            if (!reader.IsDBNull(3))
                                ti.IP = Convertor.ConvertLongToIP(reader.GetInt64("IP"));

                            if (!reader.IsDBNull(4))
                                ti.IPMask = reader.GetInt32("IPMask");

                            if (!reader.IsDBNull(5))
                                ti.BwFwd = reader.GetFloat("BWFWD");

                            if (!reader.IsDBNull(6))
                                ti.BwRtn = reader.GetFloat("BWRTN");

                            if (!reader.IsDBNull(7))
                                ti.MaxVolFwd = reader.GetFloat("MaxVolFWD");

                            if (!reader.IsDBNull(8))
                                ti.MaxVolRtn = reader.GetFloat("MaxVolRTN");

                            if (!reader.IsDBNull(9))
                                ti.MaxVolSum = reader.GetFloat("MaxVolSum");

                            if (!reader.IsDBNull(10))
                                ti.CurVolFwd = reader.GetFloat("CurVolFWD");

                            if (!reader.IsDBNull(11))
                                ti.CurVolRtn = reader.GetFloat("CurVolRTN");

                            if (!reader.IsDBNull(12))
                                ti.CurVolSum = reader.GetFloat("CurVolSum");

                            if (!reader.IsDBNull(13))
                                ti.FreeZoneRtn = reader.GetFloat("FreeZoneRTN");

                            if (!reader.IsDBNull(14))
                                ti.FreeZoneFwd = reader.GetFloat("FreeZoneFWD");

                            if (!reader.IsDBNull(15))
                                ti.FreeZoneSum = reader.GetFloat("FreeZoneSUM");

                            if (!reader.IsDBNull(16))
                                ti.InFreeZone = reader.GetBoolean("InFreeZone");

                            if (!reader.IsDBNull(17))
                                ti.ExpirationDate = reader.GetDateTime("ExpirationDate");

                            if (!reader.IsDBNull(18))
                                ti.BadDistributor = reader.GetBoolean("BadDistributor");

                            if (!reader.IsDBNull(19))
                                ti.OverVolume = reader.GetBoolean("OverVolume");

                            if (!reader.IsDBNull(20))
                                ti.FreeZone = reader.GetInt32("FreeZone");

                            if (!reader.IsDBNull(21))
                                ti.Isp = reader.GetInt32("Isp");

                            if (!reader.IsDBNull(22))
                                ti.SitId = reader.GetInt32("SitId");

                            termList.Add(ti);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex.Message);
            }

            return termList;
        }


        /// <summary>
        /// Returns a list of terminal info objects for SoHo terminals which has no traffic according to duration from the NMS
        /// </summary>
        /// <param name="duration">Terminal idle time in minutes</param>
        /// <returns>A list of terminal info objects</returns>
        public List<NMSTerminalInfo> getUnusedSoHoTerminals(int duration)
        {
            //string queryString = "SELECT MacAddress, Type, State, SlaId, IP, IPMask, BWFWD, BWRTN, " +
            //        "MaxVolFWD, MaxVolRTN, MaxVolSum, CurVolFWD, CurVolRTN, CurVolSum, FreeZoneRTN, FreeZoneFWD, " +
            //        "FreeZoneSUM, InFreeZone, ExpirationDate, BadDistributor, OverVolume, FreeZone, Isp, SitId " +
            //        "FROM Terminals";
            string queryString = "SELECT MacAddress, State, Terminals.SlaId, IP, IPMask, BWFWD, BWRTN, " +
                     "MaxVolFWD, MaxVolRTN, MaxVolSum, CurVolFWD, CurVolRTN, CurVolSum, FreeZoneRTN, FreeZoneFWD, " +
                     "FreeZoneSUM, InFreeZone, ExpirationDate, BadDistributor, OverVolume, FreeZone, Isp, SitId " +
                     "FROM Terminals inner join ServicePacks on Terminals.SlaId = ServicePacks.SlaId "+
                     "where ServicePacks.ServiceClass = 1 and State = 1 and OverVolume = 2 and LastTrafficTime >= @duration ";

            List<NMSTerminalInfo> termList = new List<NMSTerminalInfo>();
            NMSTerminalInfo ti = null;

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_connectionString))
                {
                    conn.Open();

                    using (MySqlCommand cmd = new MySqlCommand(queryString, conn))
                    {
                        cmd.Parameters.AddWithValue("@duration", duration);
                        MySqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            ti = new NMSTerminalInfo();
                            if (!reader.IsDBNull(0))
                                ti.MacAddress = Convertor.ConvertLongToMac(reader.GetInt64("MacAddress"));

                            //if (!reader.IsDBNull(1))
                            //    ti.Type = reader.GetString("Type");

                            if (!reader.IsDBNull(1))
                                ti.State = reader.GetInt32("State");

                            if (!reader.IsDBNull(2))
                                ti.SlaId = reader.GetInt32("SlaId");

                            if (!reader.IsDBNull(3))
                                ti.IP = Convertor.ConvertLongToIP(reader.GetInt64("IP"));

                            if (!reader.IsDBNull(4))
                                ti.IPMask = reader.GetInt32("IPMask");

                            if (!reader.IsDBNull(5))
                                ti.BwFwd = reader.GetFloat("BWFWD");

                            if (!reader.IsDBNull(6))
                                ti.BwRtn = reader.GetFloat("BWRTN");

                            if (!reader.IsDBNull(7))
                                ti.MaxVolFwd = reader.GetFloat("MaxVolFWD");

                            if (!reader.IsDBNull(8))
                                ti.MaxVolRtn = reader.GetFloat("MaxVolRTN");

                            if (!reader.IsDBNull(9))
                                ti.MaxVolSum = reader.GetFloat("MaxVolSum");

                            if (!reader.IsDBNull(10))
                                ti.CurVolFwd = reader.GetFloat("CurVolFWD");

                            if (!reader.IsDBNull(11))
                                ti.CurVolRtn = reader.GetFloat("CurVolRTN");

                            if (!reader.IsDBNull(12))
                                ti.CurVolSum = reader.GetFloat("CurVolSum");

                            if (!reader.IsDBNull(13))
                                ti.FreeZoneRtn = reader.GetFloat("FreeZoneRTN");

                            if (!reader.IsDBNull(14))
                                ti.FreeZoneFwd = reader.GetFloat("FreeZoneFWD");

                            if (!reader.IsDBNull(15))
                                ti.FreeZoneSum = reader.GetFloat("FreeZoneSUM");

                            if (!reader.IsDBNull(16))
                                ti.InFreeZone = reader.GetBoolean("InFreeZone");

                            if (!reader.IsDBNull(17))
                                ti.ExpirationDate = reader.GetDateTime("ExpirationDate");

                            if (!reader.IsDBNull(18))
                                ti.BadDistributor = reader.GetBoolean("BadDistributor");

                            if (!reader.IsDBNull(19))
                                ti.OverVolume = reader.GetBoolean("OverVolume");

                            if (!reader.IsDBNull(20))
                                ti.FreeZone = reader.GetInt32("FreeZone");

                            if (!reader.IsDBNull(21))
                                ti.Isp = reader.GetInt32("Isp");

                            if (!reader.IsDBNull(22))
                                ti.SitId = reader.GetInt32("SitId");

                            termList.Add(ti);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex.Message);
            }

            return termList;
        }

        /// <summary>
        /// Creates a new service pack in the NMS
        /// </summary>
        /// <param name="servicePack">The service pack data</param>
        /// <returns>True if the operation succeeded</returns>
        public bool createServicePack(ServicePack servicePack)
        {
            bool result = false;

            if (!this.slaExists((Int32)servicePack.SlaId))
            {
                string insertCmd =
                "INSERT INTO ServicePacks (SlaId, SlaName, DRFWD, DRRTN, CIRFWD, CIRRTN, SlaCommonName, LowVolume, HighVolume, MinBWFWD, MinBWRTN, ServiceClass, Weight) "
                + "VALUES (@SlaId, @SlaName, @DRFWD, @DRRTN, @CIRFWD, @CIRRTN, @SlaCommonName, @LowVolume, @HighVolume, @MinBWFWD, @MinBWRTN, @ServiceClass, @Weight)";

                using (MySqlConnection con = new MySqlConnection(_connectionString))
                {
                    con.Open();
                    try
                    {
                        using (MySqlCommand command = new MySqlCommand(insertCmd, con))
                        {
                            command.Parameters.AddWithValue("@SlaId", servicePack.SlaId);
                            command.Parameters.AddWithValue("@SlaName", servicePack.SlaName);
                            command.Parameters.AddWithValue("@DRFWD", servicePack.DRFWD);
                            command.Parameters.AddWithValue("@DRRTN", servicePack.DRRTN);
                            command.Parameters.AddWithValue("@CIRFWD", servicePack.CIRFWD);
                            command.Parameters.AddWithValue("@CIRRTN", servicePack.CIRRTN);
                            command.Parameters.AddWithValue("@SlaCommonName", servicePack.SlaCommonName);
                            //This extra check corrects an issue where a null is provided to the LowVolume
                            //HighVolume and Weight columns. This issue is due to a 2.1.1/2.1.2 compatibiliy problem
                            
                            if (servicePack.LowVolumeSUM == null)
                            {
                                Logger.log("LowVolumeSum is null");
                                servicePack.LowVolumeSUM = 0L;
                            }
                            Logger.log("Adding LowVolumeSUM: " + servicePack.LowVolumeSUM);
                            command.Parameters.AddWithValue("@LowVolume", servicePack.LowVolumeSUM);

                            if (servicePack.HighVolumeSUM == null)
                            {
                                Logger.log("HighVolumeSum is null");
                                servicePack.HighVolumeSUM = 0L;
                            }
                            Logger.log("Adding HighVolumeSum: " + servicePack.HighVolumeSUM);
                            command.Parameters.AddWithValue("@HighVolume", servicePack.HighVolumeSUM);

                            command.Parameters.AddWithValue("@MinBWFWD", servicePack.MinBWFWD);
                            command.Parameters.AddWithValue("@MinBWRTN", servicePack.MinBWRTN);
                            command.Parameters.AddWithValue("@ServiceClass", servicePack.ServiceClass);
                            if (servicePack.Weight == null)
                            {
                                servicePack.Weight = 0L;
                            }
                            command.Parameters.AddWithValue("@Weight", servicePack.Weight);

                            int numRows = command.ExecuteNonQuery();

                            if (numRows == 1)
                            {
                                result = true;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        result = false;
                        Logger.log(ex.Message);
                    }
                }
            }
            else
            {
                result = false;
                Logger.log("Error adding new service pack: service pack already exists.");
            }

            return result;
        }


        /// <summary>
        /// Creates a new service pack in the NMS
        /// </summary>
        /// <param name="servicePack">The service pack data</param>
        /// <returns>True if the operation succeeded</returns>
        public bool createServicePackwithUPlink(ServicePack servicePack,int uplink)
        {
            bool result = false;

            if (!this.slaExists((Int32)servicePack.SlaId))
            {
                string insertCmd =
                "INSERT INTO ServicePacks (SlaId, SlaName, DRFWD, DRRTN, CIRFWD, CIRRTN, SlaCommonName, LowVolume, HighVolume, MinBWFWD, MinBWRTN, ServiceClass, Weight,uplinkId) "
                + "VALUES (@SlaId, @SlaName, @DRFWD, @DRRTN, @CIRFWD, @CIRRTN, @SlaCommonName, @LowVolume, @HighVolume, @MinBWFWD, @MinBWRTN, @ServiceClass, @Weight, @uplinkId)";

                using (MySqlConnection con = new MySqlConnection(_connectionString))
                {
                    con.Open();
                    try
                    {
                        using (MySqlCommand command = new MySqlCommand(insertCmd, con))
                        {
                            command.Parameters.AddWithValue("@SlaId", servicePack.SlaId);
                            command.Parameters.AddWithValue("@SlaName", servicePack.SlaName);
                            command.Parameters.AddWithValue("@DRFWD", servicePack.DRFWD);
                            command.Parameters.AddWithValue("@DRRTN", servicePack.DRRTN);
                            command.Parameters.AddWithValue("@CIRFWD", servicePack.CIRFWD);
                            command.Parameters.AddWithValue("@CIRRTN", servicePack.CIRRTN);
                            command.Parameters.AddWithValue("@SlaCommonName", servicePack.SlaCommonName);
                            //This extra check corrects an issue where a null is provided to the LowVolume
                            //HighVolume and Weight columns. This issue is due to a 2.1.1/2.1.2 compatibiliy problem

                            if (servicePack.LowVolumeSUM == null)
                            {
                                Logger.log("LowVolumeSum is null");
                                servicePack.LowVolumeSUM = 0L;
                            }
                            Logger.log("Adding LowVolumeSUM: " + servicePack.LowVolumeSUM);
                            command.Parameters.AddWithValue("@LowVolume", servicePack.LowVolumeSUM);

                            if (servicePack.HighVolumeSUM == null)
                            {
                                Logger.log("HighVolumeSum is null");
                                servicePack.HighVolumeSUM = 0L;
                            }
                            Logger.log("Adding HighVolumeSum: " + servicePack.HighVolumeSUM);
                            command.Parameters.AddWithValue("@HighVolume", servicePack.HighVolumeSUM);

                            command.Parameters.AddWithValue("@MinBWFWD", servicePack.MinBWFWD);
                            command.Parameters.AddWithValue("@MinBWRTN", servicePack.MinBWRTN);
                            command.Parameters.AddWithValue("@ServiceClass", servicePack.ServiceClass);
                            if (servicePack.Weight == null)
                            {
                                servicePack.Weight = 0L;
                            }
                            command.Parameters.AddWithValue("@Weight", servicePack.Weight);
                            command.Parameters.AddWithValue("@uplinkId", uplink);

                            int numRows = command.ExecuteNonQuery();

                            if (numRows == 1)
                            {
                                result = true;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        result = false;
                        Logger.log(ex.Message);
                    }
                }
            }
            else
            {
                result = false;
                Logger.log("Error adding new service pack: service pack already exists.");
            }

            return result;
        }

        /// <summary>
        /// Updates a service pack in the NMS
        /// </summary>
        /// <param name="servicePack">The service pack data</param>
        /// <returns>True if the operation succeeded</returns>
        public bool updateServicePack(ServicePack servicePack)
        {
            bool result = false;

            if (this.slaExists((Int32)servicePack.SlaId))
            {
                string updateCmd =
                "UPDATE ServicePacks "
                + "SET SlaName = @SlaName, DRFWD = @DRFWD, DRRTN = @DRRTN, CIRFWD = @CIRFWD, CIRRTN = @CIRRTN, SlaCommonName = @SlaCommonName, LowVolume = @LowVolume, HighVolume = @HighVolume, MinBWFWD = @MinBWFWD, MinBWRTN = @MinBWRTN, ServiceClass = @ServiceClass, Weight = @Weight "
                + "WHERE SlaId = @SlaId";

                using (MySqlConnection con = new MySqlConnection(_connectionString))
                {
                    con.Open();
                    try
                    {
                        using (MySqlCommand command = new MySqlCommand(updateCmd, con))
                        {
                            command.Parameters.AddWithValue("@SlaId", servicePack.SlaId);
                            command.Parameters.AddWithValue("@SlaName", servicePack.SlaName);
                            command.Parameters.AddWithValue("@DRFWD", servicePack.DRFWD);
                            command.Parameters.AddWithValue("@DRRTN", servicePack.DRRTN);
                            command.Parameters.AddWithValue("@CIRFWD", servicePack.CIRFWD);
                            command.Parameters.AddWithValue("@CIRRTN", servicePack.CIRRTN);
                            command.Parameters.AddWithValue("@SlaCommonName", servicePack.SlaCommonName);
                            //This extra check corrects an issue where a null is provided to the LowVolume, 
                            //HighVolume and Weight columns. This problem is due to a 2.1.1, 2.1.2 compatibily
                            //problem
                            if (servicePack.LowVolumeSUM == null)
                            {
                                servicePack.LowVolumeSUM = 0L;
                            }
                            Logger.log("Adding LowVolumeSUM: " + servicePack.LowVolumeSUM);
                            command.Parameters.AddWithValue("@LowVolume", servicePack.LowVolumeSUM);

                            if (servicePack.HighVolumeSUM == null)
                            {
                                servicePack.HighVolumeSUM = 0L;
                            }
                            Logger.log("Adding HighVolumeSum: " + servicePack.HighVolumeSUM);
                            command.Parameters.AddWithValue("@HighVolume", servicePack.HighVolumeSUM);

                            command.Parameters.AddWithValue("@MinBWFWD", servicePack.MinBWFWD);
                            command.Parameters.AddWithValue("@MinBWRTN", servicePack.MinBWRTN);
                            command.Parameters.AddWithValue("@ServiceClass", servicePack.ServiceClass);

                            if (servicePack.Weight == null)
                            {
                                servicePack.Weight = 0L;
                            }
                            command.Parameters.AddWithValue("@Weight", servicePack.Weight);

                            int numRows = command.ExecuteNonQuery();

                            if (numRows == 1)
                            {
                                result = true;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        result = false;
                        Logger.log(ex.Message);
                    }
                }
            }
            else //create new service pack
            {
                if (this.createServicePack(servicePack))
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }

            return result;
        }


        /// <summary>
        /// Updates a service pack including the uplinkID in the NMS
        /// </summary>
        /// <param name="servicePack">The service pack data</param>
        /// <param name="uplink">The uplink id in the NMS</param>
        /// <returns>True if the operation succeeded</returns>
        public bool updateServicePackwithUPlink(ServicePack servicePack,int uplink)
        {
            bool result = false;

            if (this.slaExists((Int32)servicePack.SlaId))
            {
                string updateCmd =
                "UPDATE ServicePacks "
                + "SET SlaName = @SlaName, DRFWD = @DRFWD, DRRTN = @DRRTN, CIRFWD = @CIRFWD, CIRRTN = @CIRRTN, SlaCommonName = @SlaCommonName, LowVolume = @LowVolume, HighVolume = @HighVolume, MinBWFWD = @MinBWFWD, MinBWRTN = @MinBWRTN, ServiceClass = @ServiceClass, Weight = @Weight, uplinkId = @uplinkId "
                + "WHERE SlaId = @SlaId";

                using (MySqlConnection con = new MySqlConnection(_connectionString))
                {
                    con.Open();
                    try
                    {
                        using (MySqlCommand command = new MySqlCommand(updateCmd, con))
                        {
                            command.Parameters.AddWithValue("@SlaId", servicePack.SlaId);
                            command.Parameters.AddWithValue("@SlaName", servicePack.SlaName);
                            command.Parameters.AddWithValue("@DRFWD", servicePack.DRFWD);
                            command.Parameters.AddWithValue("@DRRTN", servicePack.DRRTN);
                            command.Parameters.AddWithValue("@CIRFWD", servicePack.CIRFWD);
                            command.Parameters.AddWithValue("@CIRRTN", servicePack.CIRRTN);
                            command.Parameters.AddWithValue("@SlaCommonName", servicePack.SlaCommonName);
                            //This extra check corrects an issue where a null is provided to the LowVolume, 
                            //HighVolume and Weight columns. This problem is due to a 2.1.1, 2.1.2 compatibily
                            //problem
                            if (servicePack.LowVolumeSUM == null)
                            {
                                servicePack.LowVolumeSUM = 0L;
                            }
                            Logger.log("Adding LowVolumeSUM: " + servicePack.LowVolumeSUM);
                            command.Parameters.AddWithValue("@LowVolume", servicePack.LowVolumeSUM);

                            if (servicePack.HighVolumeSUM == null)
                            {
                                servicePack.HighVolumeSUM = 0L;
                            }
                            Logger.log("Adding HighVolumeSum: " + servicePack.HighVolumeSUM);
                            command.Parameters.AddWithValue("@HighVolume", servicePack.HighVolumeSUM);

                            command.Parameters.AddWithValue("@MinBWFWD", servicePack.MinBWFWD);
                            command.Parameters.AddWithValue("@MinBWRTN", servicePack.MinBWRTN);
                            command.Parameters.AddWithValue("@ServiceClass", servicePack.ServiceClass);

                            if (servicePack.Weight == null)
                            {
                                servicePack.Weight = 0L;
                            }
                            command.Parameters.AddWithValue("@Weight", servicePack.Weight);
                            command.Parameters.AddWithValue("@uplinkId", uplink);
                            int numRows = command.ExecuteNonQuery();

                            if (numRows == 1)
                            {
                                result = true;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        result = false;
                        Logger.log(ex.Message);
                    }
                }
            }
            else //create new service pack
            {
                if (this.createServicePackwithUPlink(servicePack,uplink))
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }

            return result;
        }

        /// <summary>
        /// Deletes a service pack in the NMS
        /// </summary>
        /// <param name="slaId">The identifier of the service pack to be deleted</param>
        /// <returns>True if the operation succeeded</returns>
        public bool deleteServicePack(int slaId)
        {
            bool result = false;

            if (this.slaExists(slaId))
            {
                string deleteCmd =
                "DELETE FROM ServicePacks "
                + "WHERE SlaId = @SlaId";

                using (MySqlConnection con = new MySqlConnection(_connectionString))
                {
                    con.Open();
                    try
                    {
                        using (MySqlCommand command = new MySqlCommand(deleteCmd, con))
                        {
                            command.Parameters.AddWithValue("@SlaId", slaId);

                            int numRows = command.ExecuteNonQuery();

                            if (numRows == 1)
                            {
                                result = true;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        result = false;
                        Logger.log(ex.Message);
                    }
                }
            }
            else
            {
                result = false;
                Logger.log("Error deleting service pack: service does not exist.");
            }

            return result;
        }

        /// <summary>
        /// Checks if the SLA (which eventually needs to be deleted) has associated terminals
        /// </summary>
        /// <param name="slaId">The identifier of the SLA</param>
        /// <returns>True when the SLA has still associated terminals</returns>
        public bool SlaHasAssociatedTerminals(int slaId)
        {
            bool result = false;

            if (this.slaExists(slaId))
            {
                string sqlQuery = "SELECT * FROM Terminals WHERE SlaId = @slaId";

                using (MySqlConnection connection = new MySqlConnection(_connectionString))
                {
                    if (connection.State != ConnectionState.Open)
                    {
                        connection.Open();
                    }

                    try
                    {
                        using (MySqlCommand command = new MySqlCommand(sqlQuery, connection))
                        {
                            command.Parameters.AddWithValue("@slaId", slaId);
                            MySqlDataReader reader = command.ExecuteReader();
                            result = reader.HasRows;
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.log(ex.Message);
                    }
                }
            }
            else
            {
                Logger.log("Error getting terminals from sla: sla does not exist.");
            }

            return result;
        }

        /// <summary>
        /// Creates a freezone in the NMS
        /// </summary>
        /// <param name="freeZone">The freezone to be created</param>
        /// <returns>True if succesful</returns>
        public bool createFreeZone(FreeZone freeZone)
        {
            bool result = false;

            string createCmd =
                "INSERT INTO FreeZones (FreeZoneId, FreeZoneName, CurrentActive, MonOn, MonOff, TueOn, TueOff, WedOn, WedOff, ThuOn, ThuOff, FriOn, FriOff, SatOn, SatOff, SunOn, SunOff)"
                + "VALUES (@Id, @Name, @CurrentActive, @MonOn, @MonOff, @TueOn, @TueOff, @WedOn, @WedOff, @ThuOn, @ThuOff, @FriOn, @FriOff, @SatOn, @SatOff, @SunOn, @SunOff)";

            using (MySqlConnection con = new MySqlConnection(_connectionString))
            {
                con.Open();
                try
                {
                    using (MySqlCommand command = new MySqlCommand(createCmd, con))
                    {
                        command.Parameters.AddWithValue("@Id", freeZone.Id);
                        command.Parameters.AddWithValue("@Name", freeZone.Name);
                        command.Parameters.AddWithValue("@CurrentActive", freeZone.CurrentActive);
                        command.Parameters.AddWithValue("@MonOn", freeZone.MonOn);
                        command.Parameters.AddWithValue("@MonOff", freeZone.MonOff);
                        command.Parameters.AddWithValue("@TueOn", freeZone.TueOn);
                        command.Parameters.AddWithValue("@TueOff", freeZone.TueOff);
                        command.Parameters.AddWithValue("@WedOn", freeZone.WedOn);
                        command.Parameters.AddWithValue("@WedOff", freeZone.WedOff);
                        command.Parameters.AddWithValue("@ThuOn", freeZone.ThuOn);
                        command.Parameters.AddWithValue("@ThuOff", freeZone.ThuOff);
                        command.Parameters.AddWithValue("@FriOn", freeZone.FriOn);
                        command.Parameters.AddWithValue("@FriOff", freeZone.FriOff);
                        command.Parameters.AddWithValue("@SatOn", freeZone.SatOn);
                        command.Parameters.AddWithValue("@SatOff", freeZone.SatOff);
                        command.Parameters.AddWithValue("@SunOn", freeZone.SunOn);
                        command.Parameters.AddWithValue("@SunOff", freeZone.SunOff);

                        int numRows = command.ExecuteNonQuery();

                        if (numRows == 1)
                        {
                            result = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    result = false;
                    Logger.log(ex.Message);
                }
            }

            return result;
        }
        
        /// <summary>
        /// Updates a freezone in the NMS
        /// </summary>
        /// <param name="freeZone">The freezone to be updated</param>
        /// <returns>True if succesful</returns>
        public bool updateFreeZone(FreeZone freeZone)
        {
            bool result = false;

            string updateCmd =
                "UPDATE FreeZones "
                + "SET FreeZoneName = @Name, CurrentActive = @CurrentActive, MonOn = @MonOn, MonOff = @MonOff, TueOn = @TueOn, TueOff = @TueOff, WedOn = @WedOn, WedOff = @WedOff, ThuOn = @ThuOn, ThuOff = @ThuOff, FriOn = @FriOn, FriOff = @FriOff, SatOn = @SatOn, SatOff = @SatOff, SunOn = @SunOn, SunOff = @SunOff "
                + "WHERE FreeZoneId = @Id";

            using (MySqlConnection con = new MySqlConnection(_connectionString))
            {
                con.Open();
                try
                {
                    using (MySqlCommand command = new MySqlCommand(updateCmd, con))
                    {
                        command.Parameters.AddWithValue("@Id", freeZone.Id);
                        command.Parameters.AddWithValue("@Name", freeZone.Name);
                        command.Parameters.AddWithValue("@CurrentActive", freeZone.CurrentActive);
                        command.Parameters.AddWithValue("@MonOn", freeZone.MonOn);
                        command.Parameters.AddWithValue("@MonOff", freeZone.MonOff);
                        command.Parameters.AddWithValue("@TueOn", freeZone.TueOn);
                        command.Parameters.AddWithValue("@TueOff", freeZone.TueOff);
                        command.Parameters.AddWithValue("@WedOn", freeZone.WedOn);
                        command.Parameters.AddWithValue("@WedOff", freeZone.WedOff);
                        command.Parameters.AddWithValue("@ThuOn", freeZone.ThuOn);
                        command.Parameters.AddWithValue("@ThuOff", freeZone.ThuOff);
                        command.Parameters.AddWithValue("@FriOn", freeZone.FriOn);
                        command.Parameters.AddWithValue("@FriOff", freeZone.FriOff);
                        command.Parameters.AddWithValue("@SatOn", freeZone.SatOn);
                        command.Parameters.AddWithValue("@SatOff", freeZone.SatOff);
                        command.Parameters.AddWithValue("@SunOn", freeZone.SunOn);
                        command.Parameters.AddWithValue("@SunOff", freeZone.SunOff);

                        int numRows = command.ExecuteNonQuery();

                        if (numRows == 1)
                        {
                            result = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    result = false;
                    Logger.log(ex.Message);
                }
            }

            return result;
        }

        /// <summary>
        /// Fetches a list of all freezones in the NMS database
        /// </summary>
        /// <returns>The list of freezones</returns>
        public List<FreeZone> getFreeZones()
        {
            List<FreeZone> freeZones = new List<FreeZone>();
            
            try
            {
                FreeZone freeZone;
                string getCmd = "SELECT FreeZoneId, FreeZoneName, CurrentActive, MonOn, MonOff, TueOn, TueOff, WedOn, WedOff, ThuOn, ThuOff, FriOn, FriOff, SatOn, SatOff, SunOn, SunOff "
                                + "FROM FreeZones";

                using (MySqlConnection con = new MySqlConnection(_connectionString))
                {
                    con.Open();
                    try
                    {
                        using (MySqlCommand command = new MySqlCommand(getCmd, con))
                        {
                            MySqlDataReader reader = command.ExecuteReader();
                            while (reader.Read())
                            {
                                // read data from NMS
                                freeZone = new FreeZone();
                                freeZone.Id = reader.GetInt32(0);
                                freeZone.Name = reader.GetString(1);
                                freeZone.CurrentActive = reader.GetByte(2);
                                freeZone.MonOn = reader.GetTimeSpan(3).ToString();
                                freeZone.MonOff = reader.GetTimeSpan(4).ToString();
                                freeZone.TueOn = reader.GetTimeSpan(5).ToString();
                                freeZone.TueOff = reader.GetTimeSpan(6).ToString();
                                freeZone.WedOn = reader.GetTimeSpan(7).ToString();
                                freeZone.WedOff = reader.GetTimeSpan(8).ToString();
                                freeZone.ThuOn = reader.GetTimeSpan(9).ToString();
                                freeZone.ThuOff = reader.GetTimeSpan(10).ToString();
                                freeZone.FriOn = reader.GetTimeSpan(11).ToString();
                                freeZone.FriOff = reader.GetTimeSpan(12).ToString();
                                freeZone.SatOn = reader.GetTimeSpan(13).ToString();
                                freeZone.SatOff = reader.GetTimeSpan(14).ToString();
                                freeZone.SunOn = reader.GetTimeSpan(15).ToString();
                                freeZone.SunOff = reader.GetTimeSpan(16).ToString();

                                freeZones.Add(freeZone);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.log(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex.Message);
            }
            return freeZones;
        }

        /// <summary>
        /// Fetches the details of a specific freezone from the NMS
        /// </summary>
        /// <param name="freeZoneId">The ID of the freezone</param>
        /// <returns>The freezone, can be null</returns>
        public FreeZone getFreeZone(int freeZoneId)
        {
            FreeZone freeZone = null;

            try
            {
                string getCmd = "SELECT FreeZoneId, FreeZoneName, CurrentActive, MonOn, MonOff, TueOn, TueOff, WedOn, WedOff, ThuOn, ThuOff, FriOn, FriOff, SatOn, SatOff, SunOn, SunOff "
                                + "FROM FreeZones "
                                + "WHERE FreeZoneId = @Id";

                using (MySqlConnection con = new MySqlConnection(_connectionString))
                {
                    con.Open();
                    try
                    {
                        using (MySqlCommand command = new MySqlCommand(getCmd, con))
                        {
                            command.Parameters.AddWithValue("@Id", freeZoneId.ToString());
                            
                            MySqlDataReader reader = command.ExecuteReader();

                            if (reader.Read())
                            {
                                freeZone = new FreeZone();
                                freeZone.Id = reader.GetInt32(0);
                                freeZone.Name = reader.GetString(1);
                                freeZone.CurrentActive = reader.GetByte(2);
                                freeZone.MonOn = reader.GetTimeSpan(3).ToString();
                                freeZone.MonOff = reader.GetTimeSpan(4).ToString();
                                freeZone.TueOn = reader.GetTimeSpan(5).ToString();
                                freeZone.TueOff = reader.GetTimeSpan(6).ToString();
                                freeZone.WedOn = reader.GetTimeSpan(7).ToString();
                                freeZone.WedOff = reader.GetTimeSpan(8).ToString();
                                freeZone.ThuOn = reader.GetTimeSpan(9).ToString();
                                freeZone.ThuOff = reader.GetTimeSpan(10).ToString();
                                freeZone.FriOn = reader.GetTimeSpan(11).ToString();
                                freeZone.FriOff = reader.GetTimeSpan(12).ToString();
                                freeZone.SatOn = reader.GetTimeSpan(13).ToString();
                                freeZone.SatOff = reader.GetTimeSpan(14).ToString();
                                freeZone.SunOn = reader.GetTimeSpan(15).ToString();
                                freeZone.SunOff = reader.GetTimeSpan(16).ToString();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.log(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex.Message);
            }
            return freeZone;
        }

        /// <summary>
        /// This method updates the FreeZone and InFreeZone columns of the Terminals table
        /// </summary>
        /// <param name="macAddress">The terminals MAC address</param>
        /// <param name="newFreeZoneId">The Identifier of the new freezone</param>
        /// <param name="freeZone">The corresponding FreeZone object retrieved from the FreeZones</param>
        /// <returns></returns>
        public Boolean changeFreeZoneForTerminal(long macAddress, FreeZone freeZone)
        {
            string updateCmd = "UPDATE Terminals SET FreeZone = @FreeZone, InFreeZone = @InFreeZone " +
                                        "WHERE MacAddress = @MacAddress";
            Boolean success = false;
            try
            {
                using (MySqlConnection conn = new MySqlConnection(_connectionString))
                {
                    conn.Open();

                    using (MySqlCommand cmd = new MySqlCommand(updateCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@FreeZone", freeZone.Id);
                        cmd.Parameters.AddWithValue("@InFreeZone", freeZone.CurrentActive);
                        cmd.Parameters.AddWithValue("@MacAddress", macAddress);
                        int ret = cmd.ExecuteNonQuery();

                        Logger.log("changeFreeZoneForTerminal result: " + ret);
                        success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                success = false;
                Logger.log(ex.Message);
            }
            return success;
       }

        /// <summary>
        /// Updates the RangeVolumes table with the list of ipAddresses.
        /// The existing IP addresses are deleted first. The operation is done
        /// transactionaly
        /// </summary>
        /// <param name="macAddress">The MAC address</param>
        /// <param name="ipAddresses">The IP Adress</param>
        /// <returns>True if the operation succeeded, false otherwise</returns>
        public bool RefreshIPAddresses(long macAddress, List<long> ipAddresses)
        {
            string ipAddressUpdateCmd = "INSERT INTO RangeVolumes (IP, MacAddress, CurVolFWD, CurVolRTN, FreeZoneFWD, FreeZoneRTN, InFreeZone) " +
                "VALUES (@IPAddress, @UpdateMacAddress, 0, 0, 0, 0, false)";
            string ipDeleteCmd = "DELETE FROM RangeVolumes WHERE MacAddress = @MacAddress";
            bool result = false;

            using (MySqlConnection con = new MySqlConnection(_connectionString))
            {
                con.Open();

                MySqlCommand command = con.CreateCommand();
                MySqlTransaction transaction = con.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted);

                command.Connection = con;
                command.Transaction = transaction;

                try
                {
                    command.CommandText = ipDeleteCmd;
                    command.Parameters.AddWithValue("@MacAddress", macAddress);
                    command.ExecuteNonQuery();

                    command.CommandText = ipAddressUpdateCmd;
                    command.Parameters.AddWithValue("@UpdateMacAddress", macAddress);
                    command.Parameters.Add("@IPAddress", MySqlDbType.Int32);

                    foreach (long ipAddress in ipAddresses)
                    {
                        command.Parameters["@IPAddress"].Value = (int)ipAddress;
                        command.ExecuteNonQuery();
                    }

                    transaction.Commit();
                    result = true;
                }
                catch (Exception ex1)
                {
                    Logger.log(ex1.Message);

                    //Try a rollback
                    try
                    {
                        transaction.Rollback();
                        Logger.log("RefreshAddresses for MacAddress: " + macAddress + " rolled back");
                    }
                    catch (Exception ex2)
                    {
                        Logger.log(ex2.Message);
                    }
                }

                return result;
            }
        }

        /// <summary>
        /// Changes the BadDistributor flag, set in the CMT and makes changes in the NMS database.
        /// </summary>
        /// <param name="macAddress">The MACAddress</param>
        /// <param name="badDistributor">BadDisributor flag from CMT back-end</param>
        /// <returns>True if the operation succeeded, false otherwise</returns>
        public bool ChangeBadDistributor(long macAddress, bool badDistributor) 
        {
            string updateCmd = "UPDATE Terminals Set BadDistributor = @BadDistributor " +
                                        "WHERE MacAddress = @MacAddress";
            bool result = false;

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_connectionString))
                {
                    conn.Open();

                    using (MySqlCommand cmd = new MySqlCommand(updateCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@MacAddress", macAddress);
                        if (badDistributor == true)
                        {
                            cmd.Parameters.AddWithValue("@BadDistributor", 1); 
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@BadDistributor", 0); 
                        }
                        int ret = cmd.ExecuteNonQuery();

                        Logger.log("ChangeDistributor result: " + ret);
                        result = true;
                    }
                }
            }
            catch (Exception ex)
            {

                result = false;
                Logger.log(ex.Message);
            }
            return result;
        }

        /// <summary>
        /// Delete all IP addresses related to the given IP address
        /// </summary>
        /// <param name="macAddress">The terminals MAC address</param>
        /// <returns>True if the operation succeeded</returns>
        public bool DeleteIPRange(long macAddress)
        {
            string ipDeleteCmd = "DELETE FROM RangeVolumes WHERE MacAddress = @MacAddress";
            bool result = false;

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_connectionString))
                {
                    conn.Open();

                    using (MySqlCommand cmd = new MySqlCommand(ipDeleteCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@MacAddress", macAddress);
                        cmd.ExecuteNonQuery();
                        result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.log("Delete IP Range, " + ex.Message);
            }
            return result;
        }

        /// <summary>
        /// Adds the IP address to the multicast group with the given configuration.
        /// </summary>
        /// <param name="macAddress">The MAC address</param>
        /// <param name="ipAddress">The IP address to add</param>
        /// <param name="multicastName">The multicast name</param>
        /// <param name="sourceUrl">The SourceURL</param>
        /// <param name="portNr">The port number</param>
        /// <param name="bandWith">The bandwith</param>
        /// <param name="state">State of the multicast group</param>
        /// <returns>True if the operation succeeded</returns>
        public bool UpdateMulticastGroup(MultiCastInfo mi)
        {
            string insertCmd = "INSERT INTO MultiCast (MacAddress, IP, Name, Bytes, SourceUrl, PortNr, Bandwidth, State) " +
                "VALUES (@MacAddress, @IP, @Name, 0, @SourceUrl, @PortNr, @BandWidth, @State)";

            string updateCmd = "UPDATE MultiCast SET Name = @Name, SourceUrl = @SourceUrl, PortNr = @PortNr, Bandwidth = @Bandwidth, " +
                 "State = @State WHERE IP = @ipAddress";

            bool result = false;

            if (!this.MultiCastGroupExists(mi.IpAddress))
            {
                //Insert a new record
                try
                {
                    Logger.log("Inserting a new Multicast group IP: " + mi.IpAddress + ", MAC Address: " + mi.MacAddress);
                    using (MySqlConnection conn = new MySqlConnection(_connectionString))
                    {
                        conn.Open();

                        using (MySqlCommand cmd = new MySqlCommand(insertCmd, conn))
                        {
                            cmd.Parameters.AddWithValue("@MacAddress", mi.MacAddress);
                            cmd.Parameters.AddWithValue("@IP", mi.IpAddress);
                            cmd.Parameters.AddWithValue("@Name", mi.MultiCastName);
                            cmd.Parameters.AddWithValue("@SourceUrl", mi.SourceURL);
                            cmd.Parameters.AddWithValue("@PortNr", mi.PortNr);
                            cmd.Parameters.AddWithValue("@BandWidth", mi.Bandwidth);
                            cmd.Parameters.AddWithValue("@State", mi.State);

                            cmd.ExecuteNonQuery();
                            result = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.log(ex.Message);
                }
            }
            else
            {
                //Update the record
                try
                {
                    Logger.log("Updating Multicast group IP: " + mi.IpAddress + ", MAC Address: " + mi.MacAddress);
                    using (MySqlConnection conn = new MySqlConnection(_connectionString))
                    {
                        conn.Open();

                        using (MySqlCommand cmd = new MySqlCommand(updateCmd, conn))
                        {
                            cmd.Parameters.AddWithValue("@Name", mi.MultiCastName);
                            cmd.Parameters.AddWithValue("@SourceURL", mi.SourceURL);
                            cmd.Parameters.AddWithValue("@PortNr", mi.PortNr);
                            cmd.Parameters.AddWithValue("@Bandwidth", mi.Bandwidth);
                            cmd.Parameters.AddWithValue("@State", mi.State);
                            cmd.Parameters.AddWithValue("@IPAddress", mi.IpAddress);

                            cmd.ExecuteNonQuery();
                            result = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.log(ex.Message);
                }
            }
            Logger.log("UpdateMulticastGroup returns " + result);
            return result;
        }


        /// <summary>
        /// Checks if a MultiCastGroup already exists for the given ipAddress
        /// </summary>
        /// <param name="ipAddress">The IP address to check</param>
        /// <returns>True if the Multicast group exists</returns>
        public bool MultiCastGroupExists(long ipAddress)
        {
            string queryCmd = "SELECT COUNT(*) FROM MultiCast WHERE IP = @IPAddress";
            bool result = false;
            int numRecords = 0;

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_connectionString))
                {
                    conn.Open();

                    using (MySqlCommand cmd = new MySqlCommand(queryCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@IPAddress", ipAddress);
                        MySqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            numRecords = reader.GetInt32(0);
                            if (numRecords > 0)
                            {
                                result = true;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex.Message);
            }

            return result;
        }

        /// <summary>
        /// Adds a URL in an NMS table through which the terminal can access certain functionalities
        /// The URLs are scrambled for the end-user and this method matches the clear-text URL with
        /// the scrambled one.
        /// Functionalities include traffic data and a speedtest
        /// Issue CMTBO-48
        /// </summary>
        /// <param name="clearURL">The URL in readable text</param>
        /// <param name="scrambledURL">The scrambled version of the URL</param>
        /// <returns>True if successful</returns>
        public bool AddTerminalNMSURL(string clearURL, string scrambledURL)
        {
            //Awaiting table information from the NMS
            return true;
        }

        /// <summary>
        /// Sets the name of a terminal in the NMS
        /// </summary>
        /// <param name="termName">The name to be set in the NMS</param>
        /// <param name="macAddress">the MAC address of the terminal</param>
        /// <returns>true if successfull</returns>
        public bool SetTerminalName(long macAddress, string terminalName)
        {
            string updateCmd = "UPDATE Terminals SET Id = @Id WHERE MacAddress = @MacAddress";
            bool success = false;

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_connectionString))
                {
                    conn.Open();

                    using (MySqlCommand cmd = new MySqlCommand(updateCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@MacAddress", macAddress);
                        cmd.Parameters.AddWithValue("@Id", terminalName);
                        
                        cmd.ExecuteNonQuery();
                        success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex.Message);
            }

            return success;
        }

        /// <summary>
        /// Adds a terminal setup to the satellite diversity table
        /// </summary>
        /// <param name="satDivMac">The MAC address of the virtual terminal</param>
        /// <param name="primaryMac">The MAC address of the primary terminal</param>
        /// <param name="secondaryMac">The MAC address of the secondary terminal</param>
        /// <returns>true if successfull</returns>
        public bool AddSatDivEntry(long satDivMac, long primaryMac, long secondaryMac)
        {
            string insertCmd = "INSERT INTO SatDiv (SatDiv, Main, BackUp) VALUES (@SatDiv, @Main, @BackUp)";
            bool success = false;

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_connectionString))
                {
                    conn.Open();

                    using (MySqlCommand cmd = new MySqlCommand(insertCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@SatDiv", satDivMac);
                        cmd.Parameters.AddWithValue("@Main", primaryMac);
                        cmd.Parameters.AddWithValue("@BackUp", secondaryMac);
                        MySqlDataReader reader = cmd.ExecuteReader();

                        cmd.ExecuteNonQuery();
                        success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex.Message);
            }

            return success;
        }

        /// <summary>
        /// Removes the Multicast group with the given IP Address
        /// </summary>
        /// <param name="ipAddress">The IP Address</param>
        /// <returns>True if the operation succeeded</returns>
        internal bool RemoveMulticastGroup(uint ipAddress)
        {
            string deleteCmd = "DELETE FROM Multicast WHERE IP = @IPAddress";
            bool success = false;

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_connectionString))
                {
                    conn.Open();

                    using (MySqlCommand cmd = new MySqlCommand(deleteCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@IPAddress", ipAddress);
                        cmd.ExecuteNonQuery();
                        success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex.Message);
            }

            return success;
        }

        /// <summary>
        /// Returns the number of transferred bytes for the given Multicast group
        /// </summary>
        /// <param name="ipAddress">The IP address of the Multicast group</param>
        /// <returns>The number of bytes, this value can be 0</returns>
        internal ulong GetTransferredBytes(long ipAddress)
        {
            Logger.log("Get the transferred bytes for IP: " + ipAddress);
            ulong bytesTransferred = 0;
            string queryCmd = "SELECT Bytes FROM MultiCast WHERE IP = @IPAddress";

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_connectionString))
                {
                    conn.Open();
                    using (MySqlCommand cmd = new MySqlCommand(queryCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@IPAddress", ipAddress);
                        MySqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            bytesTransferred = reader.GetUInt64(0);
                        }
                        else
                        {
                            Logger.log("Could not retrieve bytese transferred for IP:" + ipAddress);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex.Message);
            }

            return bytesTransferred;
        }

        /// <summary>
        /// Returns a MultiCastInfo object corresponding to the given IP Address
        /// </summary>
        /// <remarks>
        /// This value can be NULL!
        /// </remarks>
        /// <param name="ipAddress">The IP Address respresenting the MultiCast group</param>
        /// <returns>A valid MC group or NULL if not found</returns>
        internal MultiCastInfo GetMultiCastGroupByIp(long ipAddress)
        {
            MultiCastInfo mci = null;
            string queryCmd = "SELECT IP, MacAddress, Name, Bytes, SourceURL, PortNr, Bandwidth, State FROM " +
                                "MultiCast WHERE IP = @IPAddress";

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_connectionString))
                {
                    conn.Open();
                    using (MySqlCommand cmd = new MySqlCommand(queryCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@IPAddress", ipAddress);
                        MySqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            mci = new MultiCastInfo();
                            mci.IpAddress = reader.GetInt32(0);
                            mci.MacAddress = reader.GetInt64(1);
                            mci.MultiCastName = reader.GetString(2);
                            mci.Bytes = reader.GetInt64(3);
                            mci.SourceURL = reader.GetString(4);
                            mci.PortNr = reader.GetInt32(5);
                            mci.Bandwidth = reader.GetInt64(6);
                            mci.State = reader.GetInt32(7);
                        }
                        else
                        {
                            Logger.log("Multicast group with IP: " + ipAddress + " not found");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex.Message);
            }
            return mci;
        }

        /// <summary>
        /// Inserts a MultiCastTimer into the MultiCastTimer table in the NMS (equivalent to CMT MultiCastGroupSchedule)
        /// </summary>
        /// <param name="macAddress"></param>
        /// <returns></returns>
        /// SATCORP-52: this method writes a CMT MultiCastLogSchedule to the NMS MultiCastTimer table, method is used in the NMSMultiCastLogSync
        public bool insertMultiCastTimer(string streamId, DateTime startDate, DateTime endDate)
        {
            string insertCmd =
                "INSERT INTO MultiCastTimer (StreamId, StartTime, EndTime) "
                + "VALUES (@SStreamId, @StartTime, @EndTime) ";

            bool result = false;

            using (MySqlConnection con = new MySqlConnection(_connectionString))
            {
                con.Open();
                try
                {
                    using (MySqlCommand command = new MySqlCommand(insertCmd, con))
                    {
                        command.Parameters.AddWithValue("@StreamId", streamId);
                        command.Parameters.AddWithValue("@StartTime", startDate);
                        command.Parameters.AddWithValue("@EndTime", endDate);

                        int numRows = command.ExecuteNonQuery();

                        if (numRows == 1)
                        {
                            result = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    result = false;
                    Logger.log(ex.Message);
                }
            }

            return result;
        }


        /// <summary>
        /// Returns a list of Shaper rules from the NMS
        /// </summary>
        /// <returns>A list of Shaper rules</returns>
        public List<ShaperRule> getAllShaperRules()
        {
            
            string queryString = "SELECT name, parent_group, position, indent , peak_rate , peak_rate_unit , peak_rate_enabled ,"+
                                  "prioritized_rate , prioritized_rate_unit ,prioritized_rate_enabled , guaranteed_rate , guaranteed_rate_unit ,"+
                                  "guaranteed_rate_enabled , weight , weight_unit , weight_enabled , priority " +  
                                  "FROM NMS.ShaperRules" ;

            List<ShaperRule> ShaperList = new List<ShaperRule>();
            ShaperRule sr = null;

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_connectionString))
                {
                    conn.Open();

                    using (MySqlCommand cmd = new MySqlCommand(queryString, conn))
                    {
                        MySqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            sr = new ShaperRule();
                            sr.name = reader.GetString(0);
                            sr.parent_group = reader.GetString(1);
                            sr.position = reader.GetInt32(2);
                            sr.indent = reader.GetInt32(3);
                            sr.peak_rate = reader.GetInt64(4);
                            sr.peak_rate_unit  = reader.GetString(5);
                            sr.peak_rate_enabled  = reader.GetInt32(6);
                            sr.prioritized_rate  = reader.GetInt32(7);
                            sr.prioritized_rate_unit  = reader.GetString(8);
                            sr.prioritized_rate_enabled = reader.GetInt32(9);
                            sr.guaranteed_rate  = reader.GetInt64(10);
                            sr.guaranteed_rate_unit = reader.GetString(11);
                            sr.guaranteed_rate_enabled  = reader.GetInt32(12);
                            sr.weight = reader.GetInt32(13);
                            sr.weight_unit = reader.GetString(14);
                            sr.weight_enabled= reader.GetInt32(15);
                            sr.priority= reader.GetInt32(16);
                            

                            ShaperList.Add(sr);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex.Message);
            }

            return ShaperList;
        }

        /// <summary>
        /// Returns one shaper rule if exist
        /// </summary>
        /// <param> name="Shaping rule name" </param>
        /// <returns>a shaper rule</returns>
        public ShaperRule getShaperRuleByName(string name)
        {

            string queryString = "SELECT name, parent_group, position, indent , peak_rate , peak_rate_unit , peak_rate_enabled ," +
                                  "prioritized_rate , prioritized_rate_unit ,prioritized_rate_enabled , guaranteed_rate , guaranteed_rate_unit ," +
                                  "guaranteed_rate_enabled , weight , weight_unit , weight_enabled , priority " +
                                  "FROM NMS.ShaperRules "+
                                  "Where name=@srname";

            ShaperRule sr = null;

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_connectionString))
                {
                    conn.Open();

                    using (MySqlCommand cmd = new MySqlCommand(queryString, conn))
                    {
                        cmd.Parameters.AddWithValue("@srname", name);
                        MySqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {

                            sr = new ShaperRule();
                            sr.name = reader.GetString(0);
                            sr.parent_group = reader.GetString(1);
                            sr.position = reader.GetInt32(2);
                            sr.indent = reader.GetInt32(3);
                            sr.peak_rate = reader.GetInt64(4);
                            sr.peak_rate_unit = reader.GetString(5);
                            sr.peak_rate_enabled = reader.GetInt32(6);
                            sr.prioritized_rate = reader.GetInt32(7);
                            sr.prioritized_rate_unit = reader.GetString(8);
                            sr.prioritized_rate_enabled = reader.GetInt32(9);
                            sr.guaranteed_rate = reader.GetInt64(10);
                            sr.guaranteed_rate_unit = reader.GetString(11);
                            sr.guaranteed_rate_enabled = reader.GetInt32(12);
                            sr.weight = reader.GetInt32(13);
                            sr.weight_unit = reader.GetString(14);
                            sr.weight_enabled = reader.GetInt32(15);
                            sr.priority = reader.GetInt32(16);


            
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex.Message);
            }

            return sr;
        }


        /// <summary>
        /// Returns one shaper rule if exist
        /// </summary>
        /// <param> name="Shaping rule position" </param>
        /// <returns>a shaper rule</returns>
        public ShaperRule getShaperRuleByPosition(int position)
        {

            string queryString = "SELECT name, parent_group, position, indent , peak_rate , peak_rate_unit , peak_rate_enabled ," +
                                  "prioritized_rate , prioritized_rate_unit ,prioritized_rate_enabled , guaranteed_rate , guaranteed_rate_unit ," +
                                  "guaranteed_rate_enabled , weight , weight_unit , weight_enabled , priority " +
                                  "FROM NMS.ShaperRules " +
                                  "Where position=@position";

            ShaperRule sr = null;

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_connectionString))
                {
                    conn.Open();

                    using (MySqlCommand cmd = new MySqlCommand(queryString, conn))
                    {
                        cmd.Parameters.AddWithValue("@position", position);
                        MySqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {

                            sr = new ShaperRule();
                            sr.name = reader.GetString(0);
                            sr.parent_group = reader.GetString(1);
                            sr.position = reader.GetInt32(2);
                            sr.indent = reader.GetInt32(3);
                            sr.peak_rate = reader.GetInt64(4);
                            sr.peak_rate_unit = reader.GetString(5);
                            sr.peak_rate_enabled = reader.GetInt32(6);
                            sr.prioritized_rate = reader.GetInt32(7);
                            sr.prioritized_rate_unit = reader.GetString(8);
                            sr.prioritized_rate_enabled = reader.GetInt32(9);
                            sr.guaranteed_rate = reader.GetInt64(10);
                            sr.guaranteed_rate_unit = reader.GetString(11);
                            sr.guaranteed_rate_enabled = reader.GetInt32(12);
                            sr.weight = reader.GetInt32(13);
                            sr.weight_unit = reader.GetString(14);
                            sr.weight_enabled = reader.GetInt32(15);
                            sr.priority = reader.GetInt32(16);



                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex.Message);
            }

            return sr;
        }



        /// <summary>
        /// Insert shaping rule into ShaperRules table
        /// </summary>
        /// <param> name="Shaping rule name" </param>
        /// <returns> True is successful </returns>
        public bool addShaperRule(string name , string parent_group,int indent, int peak_rate_enabled, long peak_rate,int guaranteed_rate_enabled, long guaranteed_rate, int weight_enabled, int weight)
        {

            string insertCmd = "INSERT INTO ShaperRules (name , parent_group, indent, position, peak_rate_enabled, peak_rate, " +
                "guaranteed_rate_enabled, guaranteed_rate, weight_enabled, weight, changed) VALUES " +
                "(@name , @parent_group, @indent, @position, @peak_rate_enabled, @peak_rate, " +
                "@guaranteed_rate_enabled, @guaranteed_rate, @weight_enabled, @weight, 0)";
                
            bool result = false;

            using (MySqlConnection con = new MySqlConnection(_connectionString))
            {
                con.Open();
                try
                {
                    using (MySqlCommand command = new MySqlCommand(insertCmd, con))
                    {
                        command.Parameters.AddWithValue("@name", name);
                        command.Parameters.AddWithValue("@parent_group", parent_group);
                        command.Parameters.AddWithValue("@indent", indent);
                        command.Parameters.AddWithValue("@position", this.getPosition());
                        command.Parameters.AddWithValue("@peak_rate_enabled", peak_rate_enabled);
                        command.Parameters.AddWithValue("@peak_rate", peak_rate);
                        command.Parameters.AddWithValue("@guaranteed_rate_enabled", guaranteed_rate_enabled);
                        command.Parameters.AddWithValue("@guaranteed_rate", guaranteed_rate);
                        command.Parameters.AddWithValue("@weight_enabled", weight_enabled);
                        command.Parameters.AddWithValue("@weight", weight);
                        

                        int numRows = command.ExecuteNonQuery();

                        if (numRows == 1)
                        {
                            result = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    result = false;
                    Logger.log(ex.Message);
                }
            }
            return result;
        }

        private int getPosition()
        {
            string queryString = "SELECT max(position) from NMS.ShaperRules ";

            int position=10;

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_connectionString))
                {
                    conn.Open();

                    using (MySqlCommand cmd = new MySqlCommand(queryString, conn))
                    {
                        
                        MySqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {

                           position=position+ reader.GetInt32(0);
                            

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex.Message);
            }

            return position;
        }

        /// <summary>
        /// Remove shaping rule from ShaperRules table
        /// </summary>
        /// <param> name="Shaping rule name" </param>
        /// <returns> True is successful </returns>
        public bool deleteShaperRule(string name)
        {
            string Cmd = "DELETE FROM ShaperRules WHERE name=@name ";
            

            bool result = false;

            using (MySqlConnection con = new MySqlConnection(_connectionString))
            {
                con.Open();

                MySqlCommand command = con.CreateCommand();
                MySqlTransaction transaction = con.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted);

                command.Connection = con;
                command.Transaction = transaction;

                try
                {
                    command.CommandText = Cmd;
                    command.Parameters.AddWithValue("@name", name);
                    command.ExecuteNonQuery();
                    transaction.Commit();
                    result = true;
                }
                catch (Exception ex)
                {
                    //TO DO: Send an error message to the log file
                    result = false;
                    Logger.log(ex.Message);
                }
            }

            return result;
        }

    }
}