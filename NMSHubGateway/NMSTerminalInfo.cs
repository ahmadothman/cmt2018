﻿using System;
using System.Collections.Generic;
using System.Web;

namespace NMSHubGateway
{
    /// <summary>
    /// Represents the information of a terminal
    /// </summary>
    public class NMSTerminalInfo
    {
        /// <summary>
        /// The terminal MAC address
        /// </summary>
        public string MacAddress { get; set; }

        /// <summary>
        /// The type of the terminal (voucher based etc.)
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// The state of the terminal (Locked, Unlocked)
        /// </summary>
        public int State { get; set; }

        /// <summary>
        /// The Service Pack Id
        /// </summary>
        public int SlaId { get; set; }

        /// <summary>
        /// The terminal IP address
        /// </summary>
        public string IP { get; set; }

        /// <summary>
        /// This value determines the IP address range and corresponds with a \value.
        /// For instance a value of 32 means \32 and indicates that the terminal only has one IP address. A
        /// \28 means that the terminal has 2^4 = 16 IP addresses.
        /// </summary>
        public int IPMask { get; set; }

        /// <summary>
        /// Forward bandwidth
        /// </summary>
        public float BwFwd { get; set; }

        /// <summary>
        /// Return bandwidth
        /// </summary>
        public float BwRtn { get; set; }

        /// <summary>
        /// The maximum forward volume
        /// </summary>
        public float MaxVolFwd { get; set; }

        /// <summary>
        /// The maximum return volume
        /// </summary>
        public float MaxVolRtn { get; set; }

        /// <summary>
        /// The sum of the maximum forward and return volumes
        /// </summary>
        public float MaxVolSum { get; set; }

        /// <summary>
        /// The current forward volume consumption
        /// </summary>
        public float CurVolFwd { get; set; }

        /// <summary>
        /// The current return volume consumption
        /// </summary>
        public float CurVolRtn { get; set; }

        /// <summary>
        /// The sum of the current forward and return volumes
        /// </summary>
        public float CurVolSum { get; set; }

        /// <summary>
        /// The freezone return volume
        /// </summary>
        public float FreeZoneRtn { get; set; }

        /// <summary>
        /// The freezone forward volume
        /// </summary>
        public float FreeZoneFwd { get; set; }

        /// <summary>
        /// The sum of the freezone forward and return volumes
        /// </summary>
        public float FreeZoneSum { get; set; }

        /// <summary>
        /// Indicates if the terminal is in the freezone
        /// </summary>
        public bool InFreeZone { get; set; }

        /// <summary>
        /// The subscription expiration date
        /// </summary>
        public DateTime ExpirationDate { get; set; }

        /// <summary>
        /// A flag which indicates a dodgy 
        /// </summary>
        public bool BadDistributor { get; set; }

        /// <summary>
        /// indicates if the volume consumption is over the limit
        /// </summary>
        public bool OverVolume { get; set; }

        /// <summary>
        /// Check with Kristof
        /// </summary>
        public int FreeZone { get; set; }

        /// <summary>
        /// The ISP identifier of the terminal
        /// </summary>
        public int Isp { get; set; }

        /// <summary>
        /// The SitId allocated to the terminal
        /// </summary>
        public int SitId { get; set; }

    }
}