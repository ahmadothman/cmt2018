﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace NMSHubGateway
{
    /// <summary>
    /// This exception is triggered in case the new IP Address is identical to the old one. In that case
    /// nothing should happen!
    /// </summary>
    public class SameIPAddressException : System.Exception
    {
        public SameIPAddressException()
        {
        }

        public SameIPAddressException(string message)
            : base(message)
        {
        }

        public SameIPAddressException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected SameIPAddressException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}