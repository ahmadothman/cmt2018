﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Runtime.Serialization;

namespace NMSHubGateway
{
    [Serializable]
    public class TerminalMismatchException : System.Exception
    {
        public TerminalMismatchException()
        {
        }

        public TerminalMismatchException(string message)
            : base(message)
        {
        }

        public TerminalMismatchException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected TerminalMismatchException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}