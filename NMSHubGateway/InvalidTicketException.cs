﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace NMSHubGateway
{
    [Serializable]
    public class InvalidTicketException : System.Exception
    {
        public InvalidTicketException()
        {
        }

        public InvalidTicketException(string message)
            : base(message)
        {
        }

        public InvalidTicketException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected InvalidTicketException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}