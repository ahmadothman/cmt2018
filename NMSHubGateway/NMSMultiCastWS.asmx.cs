﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Configuration;
using NMSHubGateway.Model.MultiCast;


namespace NMSHubGateway
{
    /// <summary>
    /// Summary description for BOMultiCastGroupController
    /// </summary>
    [WebService(Namespace = "http://cmt.nimera.net/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class MultiCastGroupControllerWS : System.Web.Services.WebService, IMultiCastGroupController
    {
        private IMultiCastGroupController _controller;

        public MultiCastGroupControllerWS()
        {
            // in time we'll use dependency injection here for lifetime and implementation activation.
            _controller = new MultiCastGroupController(
                connectionString: ConfigurationManager.ConnectionStrings["MCApplicationServices"].ConnectionString,
                databaseProvider: ConfigurationManager.ConnectionStrings["MCApplicationServices"].ProviderName);
        }

        /// <summary>
        /// Creates the given MultiCastGroup
        /// </summary>
        /// <remarks>
        /// This method first tries to update the Multicast group in the NMS. If this is done
        /// successfully, the Multicast group data is updated in the NMS database
        /// </remarks>
        /// <param name="mc">The Multicast group to update</param>
        /// <returns>True if the Multicast group is successfully created</returns>
        [WebMethod]
        public bool CreateMultiCastGroup(NMSHubGateway.Model.MultiCast.MultiCastGroup mc)
        {
            return _controller.CreateMultiCastGroup(mc);
        }

        /// <summary>
        /// Updates the given Multicast group in the NMS database
        /// </summary>
        /// <param name="mc">The Multicast group to update</param>
        /// <returns>True if the operation succeeded</returns>
        [WebMethod]
        public bool UpdateMultiCastGroup(NMSHubGateway.Model.MultiCast.MultiCastGroup mc)
        {
            return _controller.UpdateMultiCastGroup(mc);
        }

        /// <summary>
        /// Deletes the Multicast group from the NMS databases.
        /// </summary>
        /// <remarks>
        /// The record in the NMS is not completely deleted but on flagged as deleted
        /// </remarks>
        /// <param name="mc">The Multicast group to delete</param>
        /// <returns>True if the MC group was successfully deleted</returns>
        [WebMethod]
        public bool DeleteMultiCastGroup(NMSHubGateway.Model.MultiCast.MultiCastGroup mc)
        {
            return _controller.DeleteMultiCastGroup(mc);
        }

        /// <summary>
        /// Retrieves a Multicast group identified by its IP address from the
        /// NMS Database
        /// </summary>
        /// <param name="IPAddress">The IP Address of the Multicast group</param>
        /// <returns>A valid Multicast group object or NULL if not found</returns>
        [WebMethod]
        public NMSHubGateway.Model.MultiCast.MultiCastGroup GetMultiCastGroupByIP(string IPAddress)
        {
            return _controller.GetMultiCastGroupByIP(IPAddress);
        }

       


        /// <summary>
        /// Checks if the Multicast group exists in the NMS Database
        /// </summary>
        /// <param name="IPAddress">The MC IP Address</param>
        /// <returns>True if the MC exists</returns>
        [WebMethod]
        public bool MultiCastGroupExists(string IPAddress)
        {
            return _controller.MultiCastGroupExists(IPAddress);
        }

        /// <summary>
        /// Returns a list of all Multicast groups contained by the MulticastGroups table
        /// </summary>
        /// <remarks>
        /// This method only brings back the non-deleted MC groups
        /// </remarks>
        /// <returns>A list of MultiCastGroup elements. This list can be empty</returns>
        [WebMethod]
        public List<NMSHubGateway.Model.MultiCast.MultiCastGroup> GetMultiCastGroups()
        {
            return _controller.GetMultiCastGroups();
        }

        /// <summary>
        /// Helper method to get multicast groups registered to a specific distributor
        /// </summary>
        /// <param name="distributorId">The ID of the distributor</param>
        /// <returns>A list of MultiCastGroup elements. This list can be empty</returns>
        [WebMethod]
        public List<MultiCastGroup> GetMultiCastGroupsByDistributor(int distributorId)
        {
            return _controller.GetMultiCastGroupsByDistributor(distributorId);
        }

        /// <summary>
        /// Helper method to get multicast groups registered to a specific customer
        /// </summary>
        /// <param name="orgId">The ID of the customer organization</param>
        /// <returns>A list of MultiCastGroup elements. This list can be empty</returns>
        [WebMethod]
        public List<MultiCastGroup> GetMultiCastGroupsByOrganization(int orgId)
        {
            return _controller.GetMultiCastGroupsByOrganization(orgId);
        }

        /// <summary>
        /// Returns the MultiCastGroup by its MAC address
        /// </summary>
        /// <remarks>
        /// This value can be NULL if the MC group is not found
        /// </remarks>
        /// <param name="macAddress">The MAC address</param>
        /// <returns>The MultiCastGroup or null if not found</returns>
        [WebMethod]
        public MultiCastGroup GetMultiCastGroupByMac(string macAddress)
        {
            return _controller.GetMultiCastGroupByMac(macAddress);
        }

       

        [WebMethod]
        public Boolean UpdateMultiCastGroupSchedule(NMSHubGateway.Model.MultiCast.MultiCastGroupSchedule mcs)
        {
            return _controller.UpdateMultiCastGroupSchedule(mcs);
        }

        [WebMethod]
        public Boolean DeleteMultiCastGroupSchedule(MultiCastGroupSchedule mcs)
        {
            return _controller.DeleteMultiCastGroupSchedule(mcs);
        }

        [WebMethod]
        public Boolean CreateMultiCastGroupSchedule(NMSHubGateway.Model.MultiCast.MultiCastGroupSchedule mcs, List<MultiCastGroupSchedule> scheduleList)
        {
            return _controller.CreateMultiCastGroupSchedule(mcs, scheduleList);
        }

        [WebMethod]
        public List<MultiCastGroupSchedule> GetMultiCastGroupSchedules()
        {
            return _controller.GetMultiCastGroupSchedules();
        }

        /// <summary>
        /// Returns all schedules for a specific multicast group
        /// </summary>
        /// <param name="id">The ID of the multicast group</param>
        /// <returns>A list of multicast group schedules. Can be null</returns>
        [WebMethod]
        public List<MultiCastGroupSchedule> GetMulticastSchedulesByMCGroup(int id)
        {
            return _controller.GetMulticastSchedulesByMCGroup(id);
        }

        [WebMethod]
        public MultiCastGroup GetMultiCastGroupByID(int id)
        {
            return _controller.GetMultiCastGroupByID(id);
        }

        [WebMethod]
        public MultiCastGroupSchedule GetMultiCastGroupScheduleByID(int id)
        {
            return _controller.GetMultiCastGroupScheduleByID(id);
        }

        /// <summary>
        /// Method for creating a recurring multicast group schedule
        /// First create the recurrent event entry and the creates the separate scheduled events
        /// </summary>
        /// <param name="mcs">The data of the first scheduled event</param>
        /// <param name="mcr">The data of the overall recurrent event</param>
        /// <returns>True if successful</returns>
        [WebMethod]
        public bool CreateRecurrentMultiCastGroupSchedule(MultiCastGroupSchedule mcs, MultiCastRecurrentEvents mcr)
        {
            return _controller.CreateRecurrentMultiCastGroupSchedule(mcs, mcr);
        }

        /// <summary>
        /// Updates a set of recurrent events in the NMS db
        /// </summary>
        /// <param name="mcs">The single multicast schedule that was edited</param>
        /// <param name="mcr">The recurrent event</param>
        /// <returns>true if successful</returns>
        [WebMethod]
        public bool UpdateRecurrentMultiCastGroupSchedule(MultiCastGroupSchedule mcs, MultiCastRecurrentEvents mcr)
        {
            return _controller.UpdateRecurrentMultiCastGroupSchedule(mcs, mcr);
        }

        [WebMethod]
        public MultiCastRecurrentEvents GetMulticastRecurrentEventById(int id)
        {
            return _controller.GetMulticastRecurrentEventById(id);
        }

        /// <summary>
        /// Method for creating a new multicast source
        /// </summary>
        /// <param name="source">The multicast source object to be added to the database</param>
        /// <returns>True if successful</returns>
        [WebMethod]
        public bool AddMultiCastSource(MultiCastSource source)
        {
            return _controller.AddMultiCastSource(source);
        }

        /// <summary>
        /// Deletes a multicast source from the database
        /// </summary>
        /// <param name="sourceID">The ID of the multicast source to be deleted</param>
        /// <returns>True if successful</returns>
        [WebMethod]
        public bool DeleteMultiCastSource(MultiCastSource src)
        {
            return _controller.DeleteMultiCastSource(src);
        }

        /// <summary>
        /// Updates a multicast source in the database
        /// </summary>
        /// <param name="source">The multicast object with the new paramaters</param>
        /// <returns>True if successful</returns>
        [WebMethod]
        public bool UpdateMultiCastSource(MultiCastSource source)
        {
            return _controller.UpdateMultiCastSource(source);
        }

        /// <summary>
        /// Fetches a specific multicast source
        /// </summary>
        /// <param name="sourceID">The ID of the multicast source to be fetched</param>
        /// <returns>A multicast source object</returns>
        [WebMethod]
        public MultiCastSource GetMultiCastSourceByID(int sourceID)
        {
            return _controller.GetMultiCastSourceByID(sourceID);
        }

        /// <summary>
        /// Fetches all multicast sources in the database
        /// </summary>
        /// <returns>A list of multicast source objects</returns>
        [WebMethod]
        public List<MultiCastSource> GetAllMultiCastSources()
        {
            return _controller.GetAllMultiCastSources();
        }

        /// <summary>
        /// Fetches all multicast sources for a specific multicast group
        /// </summary>
        /// <param name="multiCastGroupID">The ID of the multicast group for which sources should be fetched</param>
        /// <returns>A list of multicast source objects</returns>
        [WebMethod]
        public List<MultiCastSource> GetMultiCastSourcesByMCGroupID(int multiCastGroupID)
        {
            return _controller.GetMultiCastSourcesByMCGroupID(multiCastGroupID);
        }



        //[WebMethod]
        //public MultiCastLog GetLatestMultiCastLog2(string macAddress) 
        //{
        //    return _controller.GetLatestMultiCastLog2(macAddress);
        //}

        /// <summary>
        /// Gets all MultiCastGroupSchedules available in the NMS DB, between two given DateTimes.
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        [WebMethod]
        public List<MultiCastGroupSchedule> GetAllMultiCastGroupSchedulesBetweenDateTimes(DateTime startDate, DateTime endDate) 
        {
            return _controller.GetAllMultiCastGroupSchedulesBetweenDateTimes(startDate, endDate);
        }

    }
}
