﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace NMSHubGateway
{
    /// <summary>
    /// This exception is thrown if the IP range could not be created
    /// </summary>
    [Serializable]
    public class IPRangeCreationException : System.Exception
    {
        public IPRangeCreationException()
        {
        }

        public IPRangeCreationException(string message)
            : base(message)
        {
        }

        public IPRangeCreationException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected IPRangeCreationException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}