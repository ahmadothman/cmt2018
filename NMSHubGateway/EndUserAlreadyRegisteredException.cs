﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Runtime.Serialization;

namespace NMSHubGateway
{
    [Serializable]
    public class EndUserAlreadyRegisteredException : System.Exception
    {
        public EndUserAlreadyRegisteredException()
        {
        }

        public EndUserAlreadyRegisteredException(string message)
            : base(message)
        {
        }

        public EndUserAlreadyRegisteredException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected EndUserAlreadyRegisteredException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}