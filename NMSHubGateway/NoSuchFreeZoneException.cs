﻿using System;
using System.Linq;
using System.Runtime.Serialization;

namespace NMSHubGateway
{
    [Serializable]
    public class NoSuchFreeZoneException : Exception
    {
        public NoSuchFreeZoneException()
        {
        }

        public NoSuchFreeZoneException(string message)
            : base(message)
        {
        }

        public NoSuchFreeZoneException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected NoSuchFreeZoneException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}