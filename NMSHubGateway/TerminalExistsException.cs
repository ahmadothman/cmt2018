﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace NMSHubGateway
{
    /// <summary>
    /// Thrown if the terminal already exists during the creation of a terminal
    /// </summary>
    public class TerminalExistsException : System.Exception
    {
        public TerminalExistsException()
        {
        }

        public TerminalExistsException(string message)
            : base(message)
        {
        }

        public TerminalExistsException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected TerminalExistsException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}