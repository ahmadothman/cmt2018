﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace NMSHubGateway
{
    [Serializable]
    public class NoSuchMultiCastGroupException : System.Exception
    {
        public NoSuchMultiCastGroupException()
        {
        }

        public NoSuchMultiCastGroupException(string message)
            : base(message)
        {
        }

        public NoSuchMultiCastGroupException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected NoSuchMultiCastGroupException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}