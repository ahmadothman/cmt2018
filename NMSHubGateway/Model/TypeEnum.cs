﻿using System;
using System.Collections.Generic;
using System.Web;

namespace NMSHubGateway
{
    public enum TypeEnum
    {
        CreateRegistration,
        ChangeStatus,
        ChangeTerminal,
        ChangeSLA,
        RemoveRegistration,
        EnableStream,
        DisableStream,
        ChangeIP,
        ChangeWeight,
        ChangeFreeZoneForTerminal
    }
}