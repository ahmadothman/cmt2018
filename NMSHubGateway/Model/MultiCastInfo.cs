﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NMSHubGateway.Model
{
    /// <summary>
    /// Contains information about a Multicast entry
    /// </summary>
    public class MultiCastInfo
    {
        /// <summary>
        /// The MAC address of the Multicast group
        /// </summary>
        /// <remarks>
        /// The MAC address is calculated according to RFC-1112. See also the document
        /// MultiCastProvisioningVs0.1.pdf in the CCN2 sharepoint library
        /// </remarks>
        public long MacAddress { get; set; }
        
        /// <summary>
        /// The MultiCast IP address
        /// </summary>
        public long IpAddress { get; set; }

        /// <summary>
        /// Name of the Multicast group
        /// </summary>
        public string MultiCastName { get; set; }

        /// <summary>
        /// Volume consumption in bytes
        /// </summary>
        public long Bytes { get; set; }

        /// <summary>
        /// The Soure URL
        /// </summary>
        public string SourceURL { get; set; }

        /// <summary>
        /// The associated Port number
        /// </summary>
        public int PortNr { get; set; }

        /// <summary>
        /// The bandwith for this Multicast group
        /// </summary>
        public long Bandwidth { get; set; }

        /// <summary>
        /// State of the Multicast group
        /// </summary>
        public int State { get; set; }
    }
}