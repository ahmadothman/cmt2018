﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NMSHubGateway.Model.MultiCast
{
    /// <summary>
    /// This class represents a MultiCastGroup in the CMT database and corresponds with the 
    /// MultiCastGroups table.
    /// </summary>
    public class MultiCastGroup
    {
        /// <summary>
        /// The ID of the the multicast group
        /// This ID uniquely defines the Multicast group
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Multicast group IP address
        /// </summary>
        public string IPAddress { get; set; }

        /// <summary>
        /// MAC address for this MC
        /// </summary>
        public string MacAddress { get; set; }

        /// <summary>
        /// Name of the MC group address
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Bytes transmitted for this MC group
        /// </summary>
        /// <remarks>
        /// This value is obtained from the NMS
        /// </remarks>
        public long Bytes { get; set; }

        /// <summary>
        /// Source URL of the stream
        /// </summary>
        public String sourceURL { get; set; }

        /// <summary>
        /// Port on which the stream will be sent
        /// </summary>
        public int portNr { get; set; }

        /// <summary>
        /// The bandwith of the stream in Kbit/s 
        /// </summary>
        public long BandWidth { get; set; }

        /// <summary>
        /// State of the Multicast group
        /// </summary>
        /// <remarks>
        /// False is disabled, true is enabled
        /// </remarks>
        public Boolean State { get; set; }

        /// <summary>
        /// The ISP identifier which represents the NMS where this MC is located
        /// </summary>
        public int IspId { get; set; }

        /// <summary>
        /// Guid of the system user to whom the Multicast group is assigned
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        /// First activation date of the MC
        /// </summary>
        public DateTime FirstActivationDate { get; set; }

        /// <summary>
        /// Organization who owns this MC
        /// </summary>
        public int OrganizationId { get; set; }

        /// <summary>
        /// Distributor who owns this MC
        /// </summary>
        public int DistributorId { get; set; }

        /// <summary>
        /// Indicates if the MultiCastGroup was deleted from the NMS or not
        /// </summary>
        public Boolean Deleted { get; set; }

        /// <summary>
        /// The ID of the Multicast group in the NMS
        /// </summary>
        public int NMSId { get; set; }

        /// <summary>
        /// Flag to determined if the multicast group is reserved or available
        /// </summary>
        public bool Reserved { get; set; }
    }
}
