﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace NMSHubGateway.Model.MultiCast
{
    public class MultiCastRecurrentEvents
    {
        public int ID { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        [XmlIgnore]
        public TimeSpan StartTime { get; set; }
        [XmlIgnore]
        public TimeSpan EndTime { get; set; }
        public int Interval { get; set; }
        
        [Browsable(false)]
        [XmlElement(DataType = "duration", ElementName = "StartTime")]
        public string StartTimeString
        {
            get { return XmlConvert.ToString(this.StartTime); }
            set { this.StartTime = string.IsNullOrEmpty(value) ? TimeSpan.Zero : XmlConvert.ToTimeSpan(value); }
        }
        
        [Browsable(false)]
        [XmlElement(DataType = "duration", ElementName = "EndTime")]
        public string EndTimeString
        {
            get { return XmlConvert.ToString(this.EndTime); }
            set { this.EndTime = string.IsNullOrEmpty(value) ? TimeSpan.Zero : XmlConvert.ToTimeSpan(value); }
        }

        public bool Deleted { get; set; }
    }

    public enum RecInterval 
    {
        Daily = 1,
        Weekly,
        Monthly
    }

}
