﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NMSHubGateway.Model
{
    /// <summary>
    /// This model describes a freezone as used in the NMS
    /// A freezone is a set of timestamps at which the freezone is either activated or de-activated
    /// </summary>
    public class FreeZone
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int CurrentActive { get; set; }
        public string MonOn { get; set; }
        public string MonOff { get; set; }
        public string TueOn { get; set; }
        public string TueOff { get; set; }
        public string WedOn { get; set; }
        public string WedOff { get; set; }
        public string ThuOn { get; set; }
        public string ThuOff { get; set; }
        public string FriOn { get; set; }
        public string FriOff { get; set; }
        public string SatOn { get; set; }
        public string SatOff { get; set; }
        public string SunOn { get; set; }
        public string SunOff { get; set; }
    }
}