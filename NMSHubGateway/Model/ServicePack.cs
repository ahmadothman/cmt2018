﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NMSHubGateway.Model
{
    /// <summary>
    ///   This is the service pack in the NMS database
    ///   The model was originally copied from BOControllerLibrary.Model.Terminal
    ///   .ServiceLevel.cs
    ///   But it has now been heavily changed
    /// </summary>
    public class ServicePack
    {
        public int? SlaId { get; set; }
        public string SlaName { get; set; }
        public string SlaCommonName { get; set; }
        public Int64? DRFWD { get; set; }
        public Int64? DRRTN { get; set; }
        public Int64? CIRFWD { get; set; }// CIR is Committed Interface Rate
        public Int64? CIRRTN { get; set; }
        public Int64? MinBWFWD { get; set; }
        public Int64? MinBWRTN { get; set; }
        public Int64? ServiceClass { get; set; }
        public Int64? HighVolumeSUM { get; set; }
        public Int64? LowVolumeSUM { get; set; }
        public double? Weight { get; set; }
        public int? IspId { get; set; } //Not needed by the NMS, but still necessary for the gateway creator
    }
}