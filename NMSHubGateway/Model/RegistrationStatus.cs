﻿using System;
using System.Collections.Generic;
using System.Web;

namespace NMSHubGateway.Model
{
    public enum RegistrationStatus { LOCKED, OPERATIONAL }
}