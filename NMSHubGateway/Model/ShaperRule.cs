﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NMSHubGateway.Model
{
    // This model object is used apply CRUD operations on ShaperRules table in the NMS
    public class ShaperRule
    {
        
       public string name { get; set; }
       public string parent_group { get; set; }
       public int position { get; set; }
       public int indent { get; set; }
       public long peak_rate { get; set; }
       public string peak_rate_unit { get; set; }
       public int peak_rate_enabled { get; set; }
       public int prioritized_rate { get; set; }
       public string prioritized_rate_unit { get; set; }
       public int prioritized_rate_enabled { get; set; }
       public long guaranteed_rate { get; set; }
       public string guaranteed_rate_unit { get; set; }
       public int guaranteed_rate_enabled { get; set; }
       public int weight { get; set; }
       public string weight_unit { get; set; }
       public int weight_enabled { get; set; }
       public int priority { get; set; }
    }
}