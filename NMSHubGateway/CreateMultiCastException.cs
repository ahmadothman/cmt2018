﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace NMSHubGateway
{
    public class CreateMultiCastException : System.Exception
    {
        public CreateMultiCastException()
        {
        }

        public CreateMultiCastException(string message)
            : base(message)
        {
        }

        public CreateMultiCastException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected CreateMultiCastException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}