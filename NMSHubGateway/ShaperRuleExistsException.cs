﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Runtime.Serialization;

namespace NMSHubGateway
{
    [Serializable]
    public class ShaperRuleExistsException : System.Exception
    {
        public ShaperRuleExistsException()
        {
        }

        public ShaperRuleExistsException(string message)
            : base(message)
        {
        }

        public ShaperRuleExistsException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected ShaperRuleExistsException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}