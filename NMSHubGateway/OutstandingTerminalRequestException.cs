﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Runtime.Serialization;

namespace NMSHubGateway
{
    [Serializable]
    public class OutstandingTerminalRequestException : System.Exception
    {
        public OutstandingTerminalRequestException()
        {
        }

        public OutstandingTerminalRequestException(string message)
            : base(message)
        {
        }

        public OutstandingTerminalRequestException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected OutstandingTerminalRequestException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}