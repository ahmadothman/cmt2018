﻿using System;
using System.Collections.Generic;
using System.Web;
using System.IO;

namespace NMSHubGateway
{
    enum OperatingSystem { Windows, Linux }

    public class Logger
    {
        const bool _DEBUG = true;
        const OperatingSystem _OS = OperatingSystem.Linux;
        const string _LOG_WINDOWS = "C:/Temp/NMSGateway.log";
        const string _LOG_LINUX = "/var/log/NMSGateway/NMSGateway.log";

        /// <summary>
        /// Simple logger
        /// </summary>
        
        public static void log(string msg)
        {
            string logPath = "";

            if (_DEBUG)
            {
                if (_OS == OperatingSystem.Windows)
                {
                    logPath = _LOG_WINDOWS;
                }
                else
                {
                    logPath = _LOG_LINUX;
                }

                using (StreamWriter writer = new StreamWriter(logPath, true))
                {
                    writer.WriteLine("[" + DateTime.UtcNow + "] " + msg);
                }
            }
        }
    }
}