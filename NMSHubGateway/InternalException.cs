﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Runtime.Serialization;

namespace NMSHubGateway
{
    [Serializable]
    public class InternalException : System.Exception
    {
        public InternalException()
        {
        }

        public InternalException(string message)
            : base(message)
        {
        }

        public InternalException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected InternalException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}