﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="NMSHubGateway._Default" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Welcome to the NMS Hub Gateway
    </h2>
    <p>
        This web application offers a web service allowing the CMT Back-Office to access
        the NMS.
    </p>
    <p>
        Link to:
    </p>
    <ul>
       <li><a href="NMSGatewayService.asmx">NMSGatewayService Methods</a></li>
       <li><a href="NMSGatewayService.asmx?WSDL">NMSGatewayService WSDL</a></li>
        <li><a href="NMSMultiCastWS.asmx">MultiCast Webservice Methods</a></li>
       <li><a href="NMSMultiCastWS.asmx?WSDL">MultiCast Webservices WSDL</a></li>
    </ul>
</asp:Content>
