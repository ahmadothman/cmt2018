﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Runtime.Serialization;

namespace NMSHubGateway
{
    [Serializable]
    public class PermissionException : System.Exception
    {
        public PermissionException()
        {
        }

        public PermissionException(string message)
            : base(message)
        {
        }

        public PermissionException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected PermissionException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}