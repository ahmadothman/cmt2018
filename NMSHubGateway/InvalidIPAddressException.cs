﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Runtime.Serialization;

namespace NMSHubGateway
{
    [Serializable]
    public class InvalidIPAddressException : Exception
    {
        public InvalidIPAddressException()
        {
        }

        public InvalidIPAddressException(string message)
            : base(message)
        {
        }

        public InvalidIPAddressException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected InvalidIPAddressException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}