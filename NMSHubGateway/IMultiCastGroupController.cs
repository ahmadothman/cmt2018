﻿using System;
using System.Collections.Generic;
using NMSHubGateway.Model.MultiCast;

namespace NMSHubGateway
{
    /// <summary>
    /// SMT-7-3 Feed Multicasting Tables in the NMS with the same records stored in the CMT 
    /// Contains the methods which allow the NMS to manage MultiCastGroups
    /// </summary>
    /// <remarks>
    /// The MultiCastGroups are first created in the HUB and next created in the 
    /// CMT Database then in the NMS database
    /// </remarks>
    public interface IMultiCastGroupController
    {
        /// <summary>
        /// Creates the given MultiCastGroup
        /// </summary>
        /// <remarks>
        /// This method first tries to update the Multicast group in the NMS. If this is done
        /// successfully, the Multicast group data is updated in the NMS database
        /// </remarks>
        /// <param name="mc">The Multicast group to update</param>
        /// <returns>True if the Multicast group is successfully created</returns>
        Boolean CreateMultiCastGroup (MultiCastGroup mc);

        /// <summary>
        /// Updates the given Multicast group in the NMS database AND the NMS
        /// </summary>
        /// <param name="mc">The Multicast group to update</param>
        /// <returns>True if the operation succeeded</returns>
        Boolean UpdateMultiCastGroup(MultiCastGroup mc);

        /// <summary>
        /// Deletes the Multicast group from the NMS and NMS databases.
        /// </summary>
        /// <remarks>
        /// The record in the NMS is not completely deleted but on flagged as deleted
        /// </remarks>
        /// <param name="mc">The Multicast group to delete</param>
        /// <returns>True if the MC group was successfully deleted</returns>
        Boolean DeleteMultiCastGroup(MultiCastGroup mc);

        /// <summary>
        /// Retrieves a Multicast group identified by its IP address from the
        /// NMS Database
        /// </summary>
        /// <param name="IPAddress">The IP Address of the Multicast group</param>
        /// <returns>A valid Multicast group object or NULL if not found</returns>
        MultiCastGroup GetMultiCastGroupByIP(string IPAddress);


        


        /// <summary>
        /// Checks if the Multicast group exists
        /// </summary>
        /// <param name="IPAddress">The MC IP Address</param>
        /// <returns>True if the MC exists</returns>
        Boolean MultiCastGroupExists(String IPAddress);

        /// <summary>
        /// Returns a list of all Multicast groups contained by the MulticastGroups table
        /// </summary>
        /// <returns>A list of MultiCastGroup elements. This list can be empty</returns>
        List<MultiCastGroup> GetMultiCastGroups();


        /// <summary>
        /// Returns the MultiCastGroup by its MAC address
        /// </summary>
        /// <remarks>
        /// This value can be NULL if the MC group is not found
        /// </remarks>
        /// <param name="macAddress">The MAC address</param>
        /// <returns>The MultiCastGroup or null if not found</returns>
        MultiCastGroup GetMultiCastGroupByMac(string macAddress);


        Boolean UpdateMultiCastGroupSchedule(MultiCastGroupSchedule mcs);

        Boolean CreateMultiCastGroupSchedule(MultiCastGroupSchedule mcs, List<MultiCastGroupSchedule> scheduleList);

        List<NMSHubGateway.Model.MultiCast.MultiCastGroupSchedule> GetMultiCastGroupSchedules();

        /// <summary>
        /// Returns all schedules for a specific multicast group
        /// </summary>
        /// <param name="id">The ID of the multicast group</param>
        /// <returns>A list of multicast group schedules. Can be null</returns>
        List<MultiCastGroupSchedule> GetMulticastSchedulesByMCGroup(int id);

        MultiCastGroup GetMultiCastGroupByID(int id);

        MultiCastGroupSchedule GetMultiCastGroupScheduleByID(int id);

        /// <summary>
        /// Updates a set of recurrent events in the NMS db
        /// </summary>
        /// <param name="mcs">The single multicast schedule that was edited</param>
        /// <param name="mcr">The recurrent event</param>
        /// <returns>true if successful</returns>
        Boolean UpdateRecurrentMultiCastGroupSchedule(MultiCastGroupSchedule mcs, MultiCastRecurrentEvents mcr);

        /// <summary>
        /// Method for creating a recurring multicast group schedule
        /// First create the recurrent event entry and the creates the separate scheduled events
        /// </summary>
        /// <param name="mcs">The data of the first scheduled event</param>
        /// <param name="mcr">The data of the overall recurrent event</param>
        /// <returns>True if successful</returns>
        Boolean CreateRecurrentMultiCastGroupSchedule(MultiCastGroupSchedule mcs, MultiCastRecurrentEvents mcr);



        List<MultiCastGroup> GetMultiCastGroupsByDistributor(int distributorId);

        MultiCastRecurrentEvents GetMulticastRecurrentEventById(int id);

        Boolean DeleteMultiCastGroupSchedule(MultiCastGroupSchedule mcs);
        List<MultiCastGroup> GetMultiCastGroupsByOrganization(int orgId);

        /// <summary>
        /// Method for creating a new multicast source
        /// </summary>
        /// <param name="source">The multicast source object to be added to the database</param>
        /// <returns>True if successful</returns>
        Boolean AddMultiCastSource(MultiCastSource source);

        /// <summary>
        /// Deletes a multicast source from the database
        /// </summary>
        /// <param name="sourceID">The ID of the multicast source to be deleted</param>
        /// <returns>True if successful</returns>
        Boolean DeleteMultiCastSource(MultiCastSource src);

        /// <summary>
        /// Updates a multicast source in the database
        /// </summary>
        /// <param name="source">The multicast object with the new paramaters</param>
        /// <returns>True if successful</returns>
        Boolean UpdateMultiCastSource(MultiCastSource source);

        /// <summary>
        /// Fetches a specific multicast source
        /// </summary>
        /// <param name="sourceID">The ID of the multicast source to be fetched</param>
        /// <returns>A multicast source object</returns>
        MultiCastSource GetMultiCastSourceByID(int sourceID);

        /// <summary>
        /// Fetches all multicast sources in the database
        /// </summary>
        /// <returns>A list of multicast source objects</returns>
        List<MultiCastSource> GetAllMultiCastSources();

        /// <summary>
        /// Fetches all multicast sources for a specific multicast group
        /// </summary>
        /// <param name="multiCastGroupID">The ID of the multicast group for which sources should be fetched</param>
        /// <returns>A list of multicast source objects</returns>
        List<MultiCastSource> GetMultiCastSourcesByMCGroupID(int multiCastGroupID);

       

        //MultiCastLog GetLatestMultiCastLog2(string macAddress);

        /// <summary>
        /// Gets all MultiCastGroupSchedules available in the NMS DB, between two given DateTimes.
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        List<MultiCastGroupSchedule> GetAllMultiCastGroupSchedulesBetweenDateTimes(DateTime startDate, DateTime endDate);

    }
}
