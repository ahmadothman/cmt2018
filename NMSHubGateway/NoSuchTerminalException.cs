﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Runtime.Serialization;

namespace NMSHubGateway
{
    [Serializable]
    public class NoSuchTerminalException : Exception
    {
        public NoSuchTerminalException()
        {
        }

        public NoSuchTerminalException(string message)
            : base(message)
        {
        }

        public NoSuchTerminalException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected NoSuchTerminalException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}