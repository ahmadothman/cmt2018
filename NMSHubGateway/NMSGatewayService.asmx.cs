﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;
using System.IO;
using NMSHubGateway.Model;
//using net.nimera.cmt.enums;

namespace NMSHubGateway
{
    /// <summary>
    /// Allows access to the NMS
    /// </summary>
    [WebService(Namespace = "http://cmt.nimera.net/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class NMSGatewayService : System.Web.Services.WebService
    {
        const bool _DEBUG = true;

        /// <summary>
        /// This is an asynchronous operation to create a registration between an end user and a terminal. 
        /// There can be only one registration per end user. 
        /// A maximum of 100 characters is available for end user identifier. 
        /// The list of allowed SLA identifiers is provided by the CMT. 
        /// </summary>
        /// <param name="endUserId">The unique end-user id</param>
        /// <param name="macAddress">The MAC address of the terminal</param>
        /// <param name="mask">Defines the range of IP addresses. A \32 means only one IP address and not a range!</param>
        /// <param name="slaId">The Service Level Agreement Identifier</param>
        /// <param name="ispId">The ISP identifier</param>
        /// <param name="sitId">The Satellite Interactive Terminal (Sit) identifier</param>
        /// <returns>
        /// A request ticket which allows the calling application to verify the progress of the 
        /// registration
        /// </returns>
        [WebMethod]
        public RequestTicket createRegistration(string endUserId, string macAddress, int mask, int slaId, int ispId, string ipAddress, int sitId, int freeZone)
        {
            ipAddress = ipAddress.Trim(); //Remove trailing spaces

            RequestTicket rt = new RequestTicket();
            rt.id = Guid.NewGuid().ToString();
            
            //Check the pre-conditions
            DataGateway dg = new DataGateway();

            //Check for the correctness of the parameters
            if (endUserId.Length > 100 || macAddress.Equals("") || endUserId.Equals("") 
                || ipAddress.Equals ("") || sitId == 0 || slaId == 0)
            {
                throw new MalformedParameterException("Parameters passed are not correct");
            }

            long macAddressNum = Convertor.ConvertMacToLong(macAddress);
            long ipAddressNum = Convertor.ConvertIPToLong(ipAddress);

            //Since we are applying a direct registration, the terminal is created as well
            //in case the terminal does not exist.
            if (dg.terminalExists(macAddressNum))
            {
                throw new TerminalExistsException(macAddress + " already exists");
            }

            //Check if the terminal is in use
            //Not sure if we still need this exception since we cannot create a terminal which already
            //exists.
            if (dg.isTerminalInUse(macAddressNum))
            {
                throw new TerminalInUseException(macAddress + " is in use");
            }

            //Check if the terminal is not blocked by another request
            if (dg.hasPendingRequest(macAddressNum))
            {
                throw new OutstandingTerminalRequestException(macAddress + " is Busy");
            }

            //Check if registration is ongoing
            if (dg.isRegistrationOnGoing(macAddressNum))
            {
                throw new OutstandingRegistrationRequestException(macAddress + " has a registration going on");
            }

            //Check if the IP address exists
            if (dg.ipAddressExists(ipAddressNum))
            {
                throw new TerminalInUseException("IP address " + ipAddress + " already exists");
            }

            //Create the registration
            if (dg.insertTerminal(endUserId, macAddressNum, ipAddressNum, mask, slaId, sitId, ispId, freeZone))
            {
                //Refresh the IP Range!
                if (this.RefreshIPRange(macAddress, ipAddress, mask))
                {
                    rt.requestStatus = RequestStatus.BUSY;
                }
                else
                {
                    //Delete the terminal again
                    dg.deleteTerminal(endUserId, macAddressNum);
                    throw new IPRangeCreationException("Could not create IP range for MAC address: " + macAddress);
                }
            }
            else
            {
                throw new TerminalCreationException("Could not create terminal with MAC address: " + macAddress + " due to an NMS database issue");
            }

            if (!dg.generateTicket(ref rt, TypeEnum.CreateRegistration, macAddressNum))
            {
                throw new InternalException("Could not generate Ticket for new terminal provisioning!");
            }
            return rt;
        }


        /// <summary>
        /// This is an asynchronous operation to create a registration with VNO parameter between an end user and a terminal. 
        /// There can be only one registration per end user. 
        /// A maximum of 100 characters is available for end user identifier. 
        /// The list of allowed SLA identifiers is provided by the CMT. 
        /// </summary>
        /// <param name="endUserId">The unique end-user id</param>
        /// <param name="macAddress">The MAC address of the terminal</param>
        /// <param name="mask">Defines the range of IP addresses. A \32 means only one IP address and not a range!</param>
        /// <param name="slaId">The Service Level Agreement Identifier</param>
        /// <param name="ispId">The ISP identifier</param>
        /// <param name="sitId">The Satellite Interactive Terminal (Sit) identifier</param>
        /// <param name="VNO"> The VNO pool dedicated for a distributor in the Shaper tree</param>
        /// <returns>
        /// A request ticket which allows the calling application to verify the progress of the 
        /// registration
        /// </returns>
        [WebMethod]
        public RequestTicket createRegistrationWithVNO(string endUserId, string macAddress, int mask, int slaId, int ispId, string ipAddress, int sitId, int freeZone,string vno)
        {
            ipAddress = ipAddress.Trim(); //Remove trailing spaces

            RequestTicket rt = new RequestTicket();
            rt.id = Guid.NewGuid().ToString();

            //Check the pre-conditions
            DataGateway dg = new DataGateway();

            //Check for the correctness of the parameters
            if (endUserId.Length > 100 || macAddress.Equals("") || endUserId.Equals("")
                || ipAddress.Equals("") || sitId == 0 || slaId == 0)
            {
                throw new MalformedParameterException("Parameters passed are not correct");
            }

            long macAddressNum = Convertor.ConvertMacToLong(macAddress);
            long ipAddressNum = Convertor.ConvertIPToLong(ipAddress);

            //Since we are applying a direct registration, the terminal is created as well
            //in case the terminal does not exist.
            if (dg.terminalExists(macAddressNum))
            {
                throw new TerminalExistsException(macAddress + " already exists");
            }

            //Check if the terminal is in use
            //Not sure if we still need this exception since we cannot create a terminal which already
            //exists.
            if (dg.isTerminalInUse(macAddressNum))
            {
                throw new TerminalInUseException(macAddress + " is in use");
            }

            //Check if the terminal is not blocked by another request
            if (dg.hasPendingRequest(macAddressNum))
            {
                throw new OutstandingTerminalRequestException(macAddress + " is Busy");
            }

            //Check if registration is ongoing
            if (dg.isRegistrationOnGoing(macAddressNum))
            {
                throw new OutstandingRegistrationRequestException(macAddress + " has a registration going on");
            }

            //Check if the IP address exists
            if (dg.ipAddressExists(ipAddressNum))
            {
                throw new TerminalInUseException("IP address " + ipAddress + " already exists");
            }

            //Create the registration
            if (dg.insertTerminal(endUserId, macAddressNum, ipAddressNum, mask, slaId, sitId, ispId, freeZone,vno))
            {
                //Refresh the IP Range!
                if (this.RefreshIPRange(macAddress, ipAddress, mask))
                {
                    rt.requestStatus = RequestStatus.BUSY;
                }
                else
                {
                    //Delete the terminal again
                    dg.deleteTerminal(endUserId, macAddressNum);
                    throw new IPRangeCreationException("Could not create IP range for MAC address: " + macAddress);
                }
            }
            else
            {
                throw new TerminalCreationException("Could not create terminal with MAC address: " + macAddress + " due to an NMS database issue");
            }

            if (!dg.generateTicket(ref rt, TypeEnum.CreateRegistration, macAddressNum))
            {
                throw new InternalException("Could not generate Ticket for new terminal provisioning!");
            }
            return rt;
        }



        /// <summary>
        /// This is an asynchronous operation which removes a terminal from the terminal list. Togheter with the 
        /// terminal the EndUser is removed from the list as well.
        /// </summary>
        /// <param name="endUserId">The End User identifier (maximum 100 characters)</param>
        /// <param name="macAddress">The MAC address</param>
        /// <returns>A request ticket</returns>
        [WebMethod]
        public RequestTicket removeRegistration(string endUserId, string macAddress)
        {
            RequestTicket rt = new RequestTicket();
            rt.id = Guid.NewGuid().ToString();

            long macAddressNum = Convertor.ConvertMacToLong(macAddress);

            //Check the pre-conditions
            DataGateway dg = new DataGateway();

            Logger.log("removeRegistration called");

            //Check if the terminal is in use
            if (dg.isTerminalInUse(macAddressNum))
            {
                throw new TerminalInUseException(macAddress + " is in use");
            }

            //Check if the terminal is not blocked by another request
            if (dg.hasPendingRequest(macAddressNum))
            {
                throw new OutstandingTerminalRequestException(macAddress + " is Busy");
            }

            //Check if registration is ongoing
            if (dg.isRegistrationOnGoing(macAddressNum))
            {
                throw new OutstandingRegistrationRequestException(macAddress + " has a registration going on");
            }

            //Check for the correctness of the parameters
            if (endUserId.Length > 100 || macAddress.Equals("") || endUserId.Equals(""))
            {
                throw new MalformedParameterException("Parameters passed are not correct");
            }

            //Check if the terminal exists
            if (!dg.terminalExists(macAddressNum))
            {
                throw new NoSuchTerminalException(macAddress + " does not exist");
            }

            if (!dg.userExists(endUserId))
            {
                throw new NoSuchEndUserException(endUserId + " does not exist");
            }

            //SMT-18
            //if (!dg.deleteTerminal(endUserId, Convertor.ConvertMacToLong(macAddress)))
            //{
            //    throw new TerminalMismatchException();
            //}

            if (!dg.generateTicket(ref rt, TypeEnum.RemoveRegistration, macAddressNum ))
            {
                throw new InternalException("Could not generate Ticket for remove registration!");
            }

            return rt;
        }

        /// <summary>
        /// This is an asynchronous operation to change the terminal used by end user and a terminal. There
        /// can be only one terminal per end user. The replacing terminal will become OPERATIONAL and 
        /// depending on the provisioning model the replaced terminal will no longer be referenced at all
        /// or pre-provisioned with a limited service level.
        /// </summary>
        /// <param name="endUserId">The identification of the terminal</param>
        /// <param name="newMacAddress">The new mac address</param>
        /// <param name="oriMacAddress">The original MAC address</param>
        /// <returns>A Request ticket</returns>
        [WebMethod]
        public RequestTicket changeTerminal(string endUserId, string oriMacAddress, string newMacAddress)
        {
            NMSTerminalInfo ti = null;
            RequestTicket rt = new RequestTicket();
            rt.id = Guid.NewGuid().ToString();
            long lOriMacAddress = Convertor.ConvertMacToLong(oriMacAddress);

            //Check the pre-conditions
            DataGateway dg = new DataGateway();

            Logger.log("changeTerminal called");

            if (!dg.isUserRegistered(endUserId))
            {
                throw new NoCurrentTerminalException();
            }

            //Check if the terminal is not blocked by another request
            if (dg.hasPendingRequest(lOriMacAddress))
            {
                throw new OutstandingTerminalRequestException(newMacAddress + " is Busy");
            }

            if (!dg.IsSameTerminal(endUserId, Convertor.ConvertMacToLong(newMacAddress)))
            {
                throw new SameTerminalException();
            }

            //Check for the correctness of the parameters
            if (endUserId.Length > 100 || newMacAddress.Equals("") || endUserId.Equals(""))
            {
                throw new MalformedParameterException("Parameters passed are not correct");
            }

            //Redundant check, if a user is registered a terminal is allocated!
            if (!dg.terminalExists(lOriMacAddress))
            {
                throw new NoSuchTerminalException(oriMacAddress + " does not exist");
            }

            //Check if registration is ongoing
            if (dg.isRegistrationOnGoing(lOriMacAddress))
            {
                throw new OutstandingRegistrationRequestException(oriMacAddress + " has a registration going on");
            }

            if (!dg.userExists(endUserId))
            {
                throw new NoSuchEndUserException(endUserId + " does not exist");
            }

            try
            {
                //Get the IP address of the terminal
                 ti = dg.getTerminalInfoByMac(lOriMacAddress);
            }
            catch (Exception ex)
            {
                throw new InternalException(ex.Message);
            }

            //Change the terminal
            //This is not necessary any more since the new MAC address is set by the NMS scripts
            /*if (dg.changeTerminal(endUserId, Convertor.ConvertMacToLong(newMacAddress)))
            {
                //Create a new ticket
                rt.requestStatus = RequestStatus.BUSY;
            }
            else
            {
                rt.requestStatus = RequestStatus.FAILED;
                rt.failureReason = "Unable to register user";
            }*/
            rt.requestStatus = RequestStatus.BUSY;
            if (!dg.generateTicket(ref rt, TypeEnum.ChangeTerminal, lOriMacAddress, Convertor.ConvertMacToLong(newMacAddress), 0L, 0L, 0 ))
            {
                throw new InternalException("Could not change terminal!");
            }

            return rt;
        }

        /// <summary>
        /// This is an asynchronous operation to change the quality of service provided by the terminal 
        /// to the end user. The list of allowed SLA identifiers is provided by the ISPIF Support SAP
        /// Operator on setup of an ISP account. See 2.1.3 for details of the asynchronous mechanism and 
        /// the returned RequestTicket data type.
        /// </summary>
        /// <param name="endUserId"></param>
        /// <param name="newSlaId"></param>
        /// <returns></returns>
        [WebMethod]
        public RequestTicket changeSla(string endUserId, string newSlaId, string ispId)
        {
            RequestTicket rt = new RequestTicket();
            rt.id = Guid.NewGuid().ToString();
            DataGateway dg = new DataGateway();
            Logger.log("changeSla called");

            if (!dg.slaExists(Int32.Parse(newSlaId)))
            {
                throw new NoSuchSlaException();
            }

            if (!dg.userExists(endUserId))
            {
                throw new NoSuchEndUserException();
            }

            long macAddress = dg.getRegisteredTerminal(endUserId);

            //Check if registration is ongoing
            if (dg.isRegistrationOnGoing(macAddress))
            {
                throw new OutstandingRegistrationRequestException(macAddress + " has a registration going on");
            }

            //Check if the terminal is not blocked by another request
            if (dg.hasPendingRequest(macAddress))
            {
                throw new OutstandingTerminalRequestException(macAddress + " is Busy");
            }

            if (dg.changeSla(endUserId, Int32.Parse(newSlaId), Int32.Parse(ispId)))
            {
                rt.requestStatus = RequestStatus.BUSY;
            }
            else
            {
                rt.requestStatus = RequestStatus.FAILED;
                rt.failureReason = "Unable to change sla";
            }

            if (!dg.generateTicket(ref rt,  TypeEnum.ChangeSLA, macAddress, 0L, Int32.Parse(newSlaId), 0L, 0 ))
            {
                throw new InternalException("Could not generate Ticket for change of MAC address!");
            }

            return rt;
        }

        /// <summary>
        /// This is an asynchronous operation to change the terminal weight (or bandwidth correction)
        /// of a the terminal 
        /// See 2.1.3 for details of the asynchronous mechanism and 
        /// the returned RequestTicket data type.
        /// </summary>
        /// <param name="endUserId"></param>
        /// <param name="newWeight">terminal weight or bandwidth correction</param>
        /// <param name="customerId">the ID of the customer organization the terminal belongs to</param>
        /// <returns></returns>
        [WebMethod]
        public RequestTicket changeWeight(string endUserId, int newWeight, int customerId, string ispId)
        {
            RequestTicket rt = new RequestTicket();
            rt.id = Guid.NewGuid().ToString();
            DataGateway dg = new DataGateway();
            Logger.log("changeWeight called");

            if (!dg.userExists(endUserId))
            {
                throw new NoSuchEndUserException();
            }

            long macAddress = dg.getRegisteredTerminal(endUserId);

            //Check if registration is ongoing
            if (dg.isRegistrationOnGoing(macAddress))
            {
                throw new OutstandingRegistrationRequestException(macAddress + " has a registration going on");
            }

            //Check if the terminal is not blocked by another request
            if (dg.hasPendingRequest(macAddress))
            {
                throw new OutstandingTerminalRequestException(macAddress + " is Busy");
            }

            if (dg.changeWeight(endUserId, newWeight, customerId))
            {
                rt.requestStatus = RequestStatus.BUSY;
            }
            else
            {
                rt.requestStatus = RequestStatus.FAILED;
                rt.failureReason = "Unable to change weight";
            }

            if (!dg.generateTicket(ref rt, TypeEnum.ChangeWeight, macAddress))
            {
                throw new InternalException("Could not generate Ticket for change of weight!");
            }

            return rt;
        }

        /// <summary>
        /// Asynchronous operation which allows to change the IP address of a terminal.
        /// </summary>
        /// <param name="endUserId">The End User Identifier</param>
        /// <param name="newIPAddress">The new IP Address</param>
        /// <param name="ispId">The ISP Identifier</param>
        /// <returns>A request ticket</returns>
        [WebMethod]
        public RequestTicket changeIPAddress(string endUserId, string newIPAddress, string ispId, int newIpMask)
        {
            RequestTicket rt = new RequestTicket();
            rt.id = Guid.NewGuid().ToString();
            DataGateway dg = new DataGateway();
            Logger.log("changeIPAddress called");
            long newIP = Convertor.ConvertIPToLong(newIPAddress);

            if (dg.IsExistingIPAddress(newIP))
            {
                throw new SameIPAddressException("IP Address=" + newIPAddress);
            }

            long macAddress = dg.getRegisteredTerminal(endUserId, Int32.Parse(ispId));

            if (macAddress == 0)
            {
                throw new NoSuchTerminalException("Terminal Id: " + endUserId);
            }

            //Check if registration is ongoing
            if (dg.isRegistrationOnGoing(macAddress))
            {
                throw new OutstandingRegistrationRequestException(macAddress + " has a registration going on");
            }

            //Check if the terminal is not blocked by another request
            if (dg.hasPendingRequest(macAddress))
            {
                throw new OutstandingTerminalRequestException(macAddress + " is Busy");
            }

            rt.requestStatus = RequestStatus.BUSY;

            if (this.RefreshIPRange(macAddress, newIP, newIpMask))
            {
                if (!dg.generateTicket(ref rt, TypeEnum.ChangeIP, macAddress, 0L, 0L, newIP, newIpMask))
                {
                    throw new InternalException("Could not generate Ticket for IP Address change!");
                }
            }
            else
            {
                throw new InternalException("Refresh of IP Range failed!");
            }

            return rt;
        }
        
        
        /// <summary>
        /// This is an asynchronous operation to de-activate, re-activate or temporarily 
        /// block the end user's terminal. See 2.1.3 for details of the asynchronous mechanism 
        /// and the returned RequestTicket data type.
        /// </summary>
        /// <param name="endUserId"></param>
        /// <param name="newStatus"></param>
        /// <returns></returns>
        [WebMethod (CacheDuration=0)]
        public RequestTicket changeStatus(string endUserId, RegistrationStatus newStatus, string ispId)
        {
            RequestTicket rt = new RequestTicket();
            rt.id = Guid.NewGuid().ToString();
            DataGateway dg = new DataGateway();

            Logger.log("changeStatus called for endUserId: " + endUserId);

            if (!dg.userExists(endUserId))
            {
                throw new NoSuchEndUserException();
            }
            
            //Retrieve the macAddress 
            long macAddress = dg.getRegisteredTerminal(endUserId);

            //Check if registration is ongoing
            if (dg.isRegistrationOnGoing(macAddress))
            {
                throw new OutstandingRegistrationRequestException(macAddress + " has a registration going on");
            }

            //Check if the terminal is not blocked by another request
            if (dg.hasPendingRequest(macAddress))
            {
                throw new OutstandingTerminalRequestException(macAddress + " is Busy");
            }

            //Check for the correctness of the parameters
            if (ispId.Equals("") || endUserId.Equals(""))
            {
                throw new MalformedParameterException("Parameters passed are not correct");
            }

            if (dg.changeStatus(endUserId, newStatus))
            {
                if (!dg.generateTicket(ref rt, TypeEnum.ChangeStatus, macAddress ))
                {
                    throw new InternalException("Could not generate Ticket!");
                }
            }
            else
            {
                throw new InternalException("Could not change the status of terminal: " + endUserId);
            }

            return rt;
        }

        /// <summary>
        /// This is a synchronous operation to recover the current state of the identified asynchronous request. 
        /// See 2.1.3 for details of the asynchronous mechanism and the returned RequestTicket data type.
        /// </summary>
        /// <remarks>
        ///  Here we need to check if the following conditions are true:
        /// - The ticket is about a terminal activation
        /// - The ticket failed
        /// If this is the case we must remove the terminal again from the terminals table and if 
        /// necessary also remove the IPRange if such a range is associated to the terminal.
        /// Check Jira issue CMTSUPPORT-19 for more information about this fix.
        /// </remarks>
        /// <param name="requestTicketId"></param>
        /// <returns></returns>
        [WebMethod]
        public RequestTicket lookupRequestTicket(string requestTicketId)
        {
            DataGateway dg = new DataGateway();
            RequestTicket rqt = dg.getRequestTicket(requestTicketId);
            try
            {
                TypeEnum ticketType = dg.getTicketType(requestTicketId);

                if (ticketType == TypeEnum.CreateRegistration && rqt.requestStatus == RequestStatus.FAILED)
                {
                    long macAddress = dg.getTicketMacAddress(requestTicketId);
                    dg.deleteTerminal(macAddress);
                }
            }
            catch (InvalidTicketException itex)
            {
                Logger.log("lookupRequestTicket failed because ticket: " + requestTicketId + " does not exist");
                Logger.log("Exception: " + itex.Message);
            }

            return rqt;
        }

        /// <summary>
        /// Resets user's traffic information for the current month. This may be 
        /// invoked by a normal isp (in which case he may only call it for his own terminals, where ispId is
        /// identical to his own id) or by the admin (who may cal it for any ispId). The ispId is the ISPIF id.
        /// </summary>
        /// <param name="endUserId"></param>
        /// <param name="ispId"></param>
        [WebMethod]
        public void resetUser(string endUserId, int ispId)
        {
            DataGateway dg = new DataGateway();

            Logger.log("resetUser called");

            //Check if the end-user exists
            if (!dg.userExists(endUserId))
            {
                throw new NoSuchEndUserException();
            }

            long macAddress = dg.getRegisteredTerminal(endUserId, ispId);

            if (macAddress != 0)
            {
                dg.resetVolume(macAddress);
            }
            else
            {
                throw new InternalException("Terminal does not exist");
            }
        }

        /// <summary>
        /// Resets user's traffic information for the current month. This may be 
        /// invoked by a normal isp (in which case he may only call it for his own terminals, where ispId is
        /// identical to his own id) or by teh admin (who may cal it for any ispId). The ispId is the ISPIF id.
        /// </summary>
        /// <param name="ispId"></param>
        /// <param name="sitId"></param>
        [WebMethod]
        public void resetUserBySit(string ispId, string sitId)
        {
            DataGateway dg = new DataGateway();

            int isp = Int32.Parse(ispId);
            int sit = Int32.Parse(sitId);

            Logger.log("resetUserBySit called");

            long macAddress = dg.getRegisteredTerminalBySitId(isp, sit);

            if (macAddress != 0)
            {
                dg.resetVolume(macAddress);
            }
            else
            {
                throw new InternalException("Terminal does not exist");
            }
        }
        
        /// <summary>
        /// Complete regsitrations by putting the states to SUCCESSFULL
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public int completeRegistrations()
        {
            DataGateway dg = new DataGateway();

            return dg.completeRegistrations();
        }

        /// <summary>
        /// Adds volume to a voucher (pre-paid) terminal
        /// </summary>
        /// <param name="addMaxVolFwd">Forward volume to add to the minimum return volume</param>
        /// <param name="addMaxVolRtn">Return voume to add to the maximum return volume</param>
        /// <param name="sitId">The Satelllite Interactive Terminal identifier</param>
        /// <param name="ispId">The ISP identifier</param>
        /// <returns>True if the operation succeeded</returns>
        [WebMethod]
        public bool AddVolume(int sitId, int ispId, long addMaxVolFwd, long addMaxVolRtn)
        {
            DataGateway dg = new DataGateway();
            //Volumes should be changed from MB to bytes
            return dg.addVolumeToTerminal(sitId, ispId, 1000000 * addMaxVolFwd, 1000000 * addMaxVolRtn, 90);
        }

        /// <summary>
        /// Adds volume to a voucher (pre-paid) terminal
        /// </summary>
        /// <param name="addMaxVolFwd">Forward volume to add to the minimum return volume</param>
        /// <param name="addMaxVolRtn">Return voume to add to the maximum return volume</param>
        /// <param name="sitId">The Satelllite Interactive Terminal identifier</param>
        /// <param name="ispId">The ISP identifier</param>
        /// <param name="validity">The number of days the new volume is valid</param>
        /// <returns>True if the operation succeeded</returns>
        [WebMethod]
        public bool AddVolumeWithValidity(int sitId, int ispId, long addMaxVolFwd, long addMaxVolRtn, int validity)
        {
            DataGateway dg = new DataGateway();
            //Volumes should be changed from MB to bytes
            return dg.addVolumeToTerminal(sitId, ispId, 1000000 * addMaxVolFwd, 1000000 * addMaxVolRtn, validity);
        }
        
        /// <summary>
        /// Returns a NMSTerminalInfo object for a terminal with the given
        /// MAC address.
        /// </summary>
        /// <param name="macAddress">The MAC address</param>
        /// <returns>A NMSTerminalInfo instance or NULL if the terminal could not be found</returns>
        [WebMethod]
        public NMSTerminalInfo getTerminalInfoByMacAddress(string macAddress)
        {
            DataGateway dg = new DataGateway();
            if (!dg.terminalExists(Convertor.ConvertMacToLong(macAddress)))
            {
                throw new NoSuchTerminalException(macAddress + " does not exists");
            }
            return dg.getTerminalInfoByMac(Convertor.ConvertMacToLong(macAddress));
        }

        /// <summary>
        /// Returns a list of terminal info objects for SoHo terminals which has no traffic according to duration from the NMS
        /// </summary>
        /// <param name="duration">Terminal idle time in minutes</param>
        /// <returns>A list of terminal info objects</returns>
        [WebMethod]
        public List<NMSTerminalInfo> getUnusedSoHoTerminals(int duration)
        {
            DataGateway dg = new DataGateway();
            return dg.getUnusedSoHoTerminals(duration);
        }

        /// <summary>
        /// Returns the terminal idle time in minutes for operational terminals
        /// </summary>
        /// <remarks>
        /// If the value is null the method returns -1
        /// </remarks>
        /// <param name="macAddress"></param>
        /// <returns>Terminal idle time in minutes</returns>
        [WebMethod]
        public int getTerminalIdleTime(string macAddress)
        {
           
            DataGateway dg = new DataGateway();
            if (!dg.terminalExists(Convertor.ConvertMacToLong(macAddress)))
            {
                throw new NoSuchTerminalException(macAddress + " does not exists");
            }
            return dg.getTerminalIdleTime(Convertor.ConvertMacToLong(macAddress));
        }
    /// <summary>
    /// Creates a new service pack in the NMS database
    /// </summary>
    /// <param name="servicePack">The Service pack information to store in the DB</param>
    /// <returns>True if the operation succeeded</returns>
    [WebMethod]
        public bool CreateServicePack(ServicePack servicePack)
        {
            DataGateway dg = new DataGateway();
            return dg.createServicePack(servicePack);
        }


        /// <summary>
        /// Creates a new service pack with UPlink id which reference to the Satellite link in the NMS database
        /// </summary>
        /// <param name="servicePack">The Service pack information to store in the DB</param>
        /// <param name="servicePack">The Service pack information to store in the DB</param>
        /// <returns>True if the operation succeeded</returns>
        [WebMethod]
        public bool CreateServicePackwithUPlink(ServicePack servicePack,int uplink)
        {
            DataGateway dg = new DataGateway();
            return dg.createServicePackwithUPlink(servicePack,uplink);
        }


        /// <summary>
        /// Updates a service pack in the NMS database
        /// </summary>
        /// <param name="servicePack">The service pack to update</param>
        /// <returns>True if the operation succeeded</returns>
        [WebMethod]
        public bool UpdateServicePack(ServicePack servicePack)
        {
            DataGateway dg = new DataGateway();
            return dg.updateServicePack(servicePack);
        }


        /// <summary>
        /// Updates a service pack in the NMS database
        /// </summary>
        /// <param name="servicePack">The service pack to update</param>
        /// <returns>True if the operation succeeded</returns>
        [WebMethod]
        public bool UpdateServicePackwithUPlink(ServicePack servicePack, int uplink)
        {
            DataGateway dg = new DataGateway();
            return dg.updateServicePackwithUPlink(servicePack,uplink);
        }


        /// <summary>
        /// Deletes a service pack from the database
        /// </summary>
        /// <param name="slaId">The identifier of the service pack to delete</param>
        /// <returns>True if the delete succeeded</returns>
        [WebMethod]
        public bool DeleteServicePack(int slaId)
        {
            DataGateway dg = new DataGateway();
            return dg.deleteServicePack(slaId);
        }

        /// <summary>
        /// Checks if the SLA exists
        /// </summary>
        /// <param name="slaId">The identifier of the SLA</param>
        /// <returns>True when the SLA exists</returns>
        [WebMethod]
        public bool slaExists(int slaId)
        {
            DataGateway dg = new DataGateway();
            return dg.slaExists(slaId);
        }

        /// <summary>
        /// Checks if the SLA (which eventually needs to be deleted) has associated terminals
        /// </summary>
        /// <param name="slaId">The identifier of the SLA</param>
        /// <returns>True when the SLA has still associated terminals</returns>
        [WebMethod]
        public bool SlaHasAssociatedTerminals(int slaId)
        {
            DataGateway dg = new DataGateway();
            return dg.SlaHasAssociatedTerminals(slaId);
        }

        /// <summary>
        /// Creates a freezone in the NMS
        /// </summary>
        /// <param name="freeZone">The freezone to be created</param>
        /// <returns>True if succesful</returns>
        [WebMethod]
        public bool createFreeZone(FreeZone freeZone)
        {
            DataGateway dg = new DataGateway();
            return dg.createFreeZone(freeZone);
        }

        /// <summary>
        /// Updates a freezone in the NMS
        /// </summary>
        /// <param name="freeZone">The freezone to be updated</param>
        /// <returns>True if succesful</returns>
        [WebMethod]
        public bool updateFreeZone(FreeZone freeZone)
        {
            DataGateway dg = new DataGateway();
            return dg.updateFreeZone(freeZone);
        }

        /// <summary>
        /// Fetches a list of all freezones in the NMS database
        /// </summary>
        /// <returns>The list of freezones</returns>
        [WebMethod]
        public List<FreeZone> getFreeZones()
        {
            DataGateway dg = new DataGateway();
            return dg.getFreeZones();
        }

        /// <summary>
        /// Fetches the details of a specific freezone from the NMS
        /// </summary>
        /// <param name="freeZoneId">The ID of the freezone</param>
        /// <returns>The freezone</returns>
        [WebMethod]
        public FreeZone getFreeZone(int freeZoneId)
        {
            DataGateway dg = new DataGateway();
            return dg.getFreeZone(freeZoneId);
        }

        /// <summary>
        /// This Web method allows to change the freezone allocated to a terminal or to allocate a
        /// freezone to a terminal!  The newFreeZoneId parameter determines the freezone schedule 
        /// allocated to the terminal.
        /// </summary>
        /// <remarks>
        /// First the application checks if the user and freezone exist. Next the mehtod reads the
        /// Active flag from the FreeZone table and sets the InFreeZone column of the 
        /// </remarks>
        /// <param name="endUserId">End User identifier which uniquely identifies a terminal</param>
        /// <param name="newFreeZoneId">The identifier of the freezone</param>
        /// <param name="newFreeZoneFlag">Boolean which indicates whether the freezone option is added or removed from the terminal</param>
        /// <param name="ispId">The ISPId this terminal belongs to</param>
        /// <returns>True if the operation succeeded or false otherwise</returns>
        /// <exception cref="NoSuchFreeZoneException">Thrown if the the given freezone does not exist</exception>
        [WebMethod]
        public Boolean ChangeFreeZoneForTerminal(string endUserId, int newFreeZoneId)
        {
            Boolean success = false;
            DataGateway dg = new DataGateway();
            Logger.log("ChangeFreeZoneForTerminal called. Terminal: " + endUserId + " ,FreeZoneId: " + newFreeZoneId);

            FreeZone freeZone = getFreeZone(newFreeZoneId);

            if (freeZone == null)
            {
                throw new NoSuchFreeZoneException();
            }

            if (!dg.userExists(endUserId))
            {
                throw new NoSuchEndUserException();
            }

            long macAddress = dg.getRegisteredTerminal(endUserId);

            success = dg.changeFreeZoneForTerminal(macAddress, freeZone);

            if (!success)
            {
                Logger.log("ChangeFreeZoneForTerminal: " + endUserId + " failed");
            }
            return success;
        }

        /// <summary>
        /// This method refreshes the RangeVolumes table with the range of IP addresses, 
        /// starting from the IP base address and defined by the ipMask value.
        /// </summary>
        /// <remarks>
        /// The range is first deleted and next inserted again. The whole operation is done 
        /// transactional. So if the the update fails the existing situation is rolled back.
        /// </remarks>
        /// <param name="macAddress">The MAC address of the terminal to refresh</param>
        /// <param name="baseIPaddress">The base IP address, this address is stored in the </param>
        /// <param name="ipMask">The IP Mask which determines the range</param>
        /// <returns>True if the operation succeeded</returns>
        [WebMethod]
        public bool RefreshIPRange(string macAddress, string baseIPaddress, int ipMask)
        {
            long lMacAddress = Convertor.ConvertMacToLong(macAddress);
            long lIPAddress = Convertor.ConvertIPToLong(baseIPaddress) + 1;
            return this.RefreshIPRange(lMacAddress, lIPAddress, ipMask);
        }

        private bool RefreshIPRange(long macAddress, long baseIPaddress, int ipMask)
        {
            //int ipAddressRange = (int)(Math.Pow(2.0, (32.0 - (double)ipMask)) - 2.0); //-2 because we do not store the base address and the last (broadcast address)
            int ipAddressRange = (int)(Math.Pow(2.0, (32.0 - (double)ipMask))); // NMSDEV-23: no more -2 because we want to store the base address and the last again (broadcast address), Google ip mask calculator
            List<long> ipAddresses = new List<long>();
            DataGateway dg = new DataGateway();

            Logger.log("Adding to list");
            for (long newIpAddress = baseIPaddress; newIpAddress < baseIPaddress + ipAddressRange;
                newIpAddress++)
            {
                ipAddresses.Add(newIpAddress);
            }

            return dg.RefreshIPAddresses(macAddress, ipAddresses);
        }

        /// <summary>
        /// Deletes the IP Range from the RangeVolumes table for the given MAC address
        /// </summary>
        /// <remarks>
        /// This method removes all IP addresses belonging to the given MAC address, including
        /// the reported volumes!
        /// </remarks>
        /// <param name="MacAddress">The MAC address</param>
        /// <returns>True if the operation succeeded</returns>
        [WebMethod]
        public bool DeleteIPRange(string macAddress)
        {
            DataGateway dg = new DataGateway();
            return dg.DeleteIPRange(Convertor.ConvertMacToLong(macAddress));
        }

        /// <summary>
        /// Adds an IP address to a multicast group. The information for the multicast 
        /// group is provided by the CMT.
        /// </summary>
        /// <param name="macAddress">The MAC address of the MultiCastGroup. This address is calculated by the 
        /// CMT</param>
        /// <param name="ipAddress">The IP address to add</param>
        /// <param name="multicastName">The name of the Multicast group</param>
        /// <param name="sourceUrl">The source URL</param>
        /// <param name="portNr">The port number</param>
        /// <param name="bandWith">The b</param>
        /// <param name="state"></param>
        /// <returns>True if the operation succeeded</returns>
        [WebMethod]
        public RequestTicket CreateMulticastGroup(string macAddress, string ipAddress, string multiCastName, string sourceUrl, int portNr, long bandWidth, int state)
        {
            long lMacAddress = Convertor.ConvertMacToLong(macAddress);
            long lIpAddress = Convertor.ConvertIPToLong(ipAddress);

            RequestTicket rt = new RequestTicket();
            rt.id = Guid.NewGuid().ToString();

            MultiCastInfo mi = new MultiCastInfo();
            mi.Bytes = 0;
            mi.Bandwidth = bandWidth;
            mi.IpAddress = lIpAddress;
            mi.MultiCastName = multiCastName;
            mi.PortNr = portNr;
            mi.SourceURL = sourceUrl;
            mi.State = state;
            mi.MacAddress = lMacAddress;

            DataGateway dg = new DataGateway();
            //if (!dg.MultiCastGroupExists(mi.IpAddress))
            //{
                if (dg.UpdateMulticastGroup(mi))
                {
                    Logger.log("Before generation ticket!");
                    //Generate a request ticket
                    if (!dg.generateTicket(ref rt, TypeEnum.EnableStream, Convertor.ConvertMacToLong(macAddress)))
                    {
                        Logger.log("Could not generate ticket for Multicast create");
                        throw new InternalException("Could not generate Ticket!");
                    }
                    else
                    {
                        Logger.log("Ticket successfully generated");
                    }
                }
                else
                {
                    Logger.log("Update of Multicast group failed");
                }
            //}
            //else
            //{
            //    throw new CreateMultiCastException("MultiCastGroup with MAC Address: " + macAddress + " already exists");
            //}

            return rt;
        }

        /// <summary>
        /// Removes an IP address from a MultiCast group
        /// </summary>
        /// <param name="macAddress">The MAC address which owns the IP address to remove</param>
        /// <param name="ipAddress">The IP address to remove</param>
        /// <returns></returns>
        [WebMethod]
        public bool RemoveIPFromMulticast(string ipAddress)
        {
            DataGateway dg = new DataGateway();
            Boolean success = false;

            if (!dg.MultiCastGroupExists(Convertor.ConvertIPToLong(ipAddress)))
            {
                throw new MulticastNotSupportedException("Multicast group with IP address: " +
                        ipAddress + " does noet exist");
            }
            else
            {
                success = dg.RemoveMulticastGroup(Convertor.ConvertIPToLong(ipAddress));
            }

            return success;
        }

        /// <summary>
        /// Updates the properties of a MultiCastGroup
        /// </summary>
        /// <param name="multiCastName">The name of the Multicast group</param>
        /// <param name="sourceUrl">The SourceURL of the Multicast group</param>
        /// <param name="portNr">The port number</param>
        /// <param name="bandWidth">The bandwith</param>
        /// <param name="state">The state of the Multicast group</param>
        /// <returns>True if the update succeeded</returns>
        [WebMethod]
        public bool ModifyMulticastGroup(string macAddress, string ipAddress, string multiCastName, string sourceUrl, int portNr, long bandWidth, int state)
        {
            long lMacAddress = Convertor.ConvertMacToLong(macAddress);
            long lIpAddress = Convertor.ConvertIPToLong(ipAddress);
            bool result = false;

            MultiCastInfo mi = new MultiCastInfo();
            mi.Bytes = 0;
            mi.Bandwidth = bandWidth;
            mi.IpAddress = lIpAddress;
            mi.MacAddress = lMacAddress;
            mi.MultiCastName = multiCastName;
            mi.PortNr = portNr;
            mi.SourceURL = sourceUrl;
            mi.State = state;
            mi.MacAddress = lMacAddress;

            DataGateway dg = new DataGateway();
            //Only modify the Multicast group if it exists, otherwise a new one is created
            if (!dg.MultiCastGroupExists(mi.IpAddress))
            {
                result = dg.UpdateMulticastGroup(mi);
            }
            else
            {
                throw new CreateMultiCastException("MultiCastGroup with MAC Address: " + macAddress + " does not exist");
            }

            return result;
        }

        /// <summary>
        /// Returns the number of bytes transferred for the Multicast group defined by the 
        /// IPAddress
        /// </summary>
        /// <param name="IPAddress">The IP address of the Multicast group</param>
        /// <returns>The number of bytes as an ulong</returns>
        [WebMethod]
        public ulong GetTransferredBytes(string IPAddress)
        {
            long lIpAddress = Convertor.ConvertIPToLong(IPAddress);
            DataGateway dg = new DataGateway();

            return dg.GetTransferredBytes(lIpAddress);
        }

        /// <summary>
        /// This method returns a MultiCastInfo object from the NMS Database
        /// </summary>
        /// <remarks>
        /// The return value can be NULL!
        /// </remarks>
        /// <param name="IPAddress">The IP Address which references the MC Group</param>
        /// <returns>A MultiCastInfo object or NULL if not found</returns>
        [WebMethod]
        public MultiCastInfo GetMultiCastGroupByIp(string IPAddress)
        {
            DataGateway dg = new DataGateway();
            return dg.GetMultiCastGroupByIp(Convertor.ConvertIPToLong(IPAddress));
        }

        /// <summary>
        /// Allows to enable the multicast group. The method returns an <i>EnableStream</i>
        /// request ticket.
        /// </summary>
        /// <param name="IPAddress">The IP Address of the Multicast group</param>
        /// <returns>An <i>EnableStream</i> request ticket</returns>
        [WebMethod]
        public RequestTicket EnableMulticastGroup(string IPAddress)
        {
            return this.ChangeMultiCastGroupState(IPAddress, true);
        }

        /// <summary>
        /// Disables the multicast group. The method returns a <i>DisableStream</i> request ticket
        /// </summary>
        /// <param name="IPAddress">The IP Address of the Multicast group</param>
        /// <returns>An <i>DisableStream</i> request ticket</returns>
        [WebMethod]
        public RequestTicket DisableMulticastGroup(string IPAddress)
        {
            return this.ChangeMultiCastGroupState(IPAddress, false);
        }

        /// <summary>
        /// Adds a URL in an NMS table through which the terminal can access certain functionalities
        /// The URLs are scrambled for the end-user and this method matches the clear-text URL with
        /// the scrambled one.
        /// Functionalities include traffic data and a speedtest
        /// Issue CMTBO-48
        /// </summary>
        /// <param name="clearURL">The URL in readable text</param>
        /// <param name="scrambledURL">The scrambled version of the URL</param>
        /// <returns>True if successful</returns>
        [WebMethod]
        public bool AddTerminalNMSURL(string clearURL, string scrambledURL)
        {
            DataGateway dg = new DataGateway();
            return dg.AddTerminalNMSURL(clearURL, scrambledURL);
        }

        /// <summary>
        /// Changes the BadDistributor flag, set in the CMT
        /// </summary>
        /// <remarks>
        /// Uses the ConvertMacToLong method from the Convertor class
        /// </remarks>
        /// <param name="macAddress">The MACAddress</param>
        /// <param name="badDistributor">BadDisributor flag from CMT back-end</param>
        /// <returns>True if the operation succeeded, false otherwise</returns>
        [WebMethod]
        public bool ChangeBadDistributor(string macAddressCMT, bool badDistributor)
        {
            long macAddress = Convertor.ConvertMacToLong(macAddressCMT);
            DataGateway dg = new DataGateway();
            return dg.ChangeBadDistributor(macAddress, badDistributor);
        }

        /// <summary>
        /// Sets the name of a terminal in the NMS
        /// </summary>
        /// <param name="termName">The name to be set in the NMS</param>
        /// <param name="macAddress">the MAC address of the terminal</param>
        /// <returns>true if successfull</returns>
        [WebMethod]
        public bool SetTerminalName(string termName, string macAddress)
        {
            long lMacAddress = Convertor.ConvertMacToLong(macAddress);
            DataGateway dg = new DataGateway();
            return dg.SetTerminalName(lMacAddress, termName);
        }

        /// <summary>
        /// Adds a terminal setup to the satellite diversity table
        /// </summary>
        /// <param name="satDivMac">The MAC address of the virtual terminal</param>
        /// <param name="primaryMac">The MAC address of the primary terminal</param>
        /// <param name="secondaryMac">The MAC address of the secondary terminal</param>
        /// <returns>true if successfull</returns>
        [WebMethod]
        public bool AddSatDivEntry(string satDivMac, string primaryMac, string secondaryMac)
        {
            long lSatDivMac = Convertor.ConvertMacToLong(satDivMac);
            long lPrimaryMac = Convertor.ConvertMacToLong(primaryMac);
            long lSecondaryMac = Convertor.ConvertMacToLong(secondaryMac);
            DataGateway dg = new DataGateway();
            return dg.AddSatDivEntry(lSatDivMac, lPrimaryMac, lSecondaryMac);
        }

        /// <summary>
        /// Checks if a Terminal is the active Terminal in a redundant setup, by MacAddress
        /// </summary>
        /// <param name="macAddress"></param>
        /// <returns></returns>
        [WebMethod]
        public bool IsSatDivTerminalActive(string macAddress)
        {
            long lMacAddress = Convertor.ConvertMacToLong(macAddress);
            DataGateway dg = new DataGateway();
            return dg.IsSatDivTerminalActive(lMacAddress);
        }

        /// <summary>
        /// Inserts a MultiCastTimer into the MultiCastTimer table in the NMS (equivalent to CMT MultiCastGroupSchedule)
        /// </summary>
        /// <param name="macAddress"></param>
        /// <returns></returns>
        /// SATCORP-52: this method writes a CMT MultiCastLogSchedule to the NMS MultiCastTimer table, method is used in the NMSMultiCastLogSync
        [WebMethod]
        public bool insertMultiCastTimer(string streamId, DateTime startDate, DateTime endDate) 
        {
            return this.insertMultiCastTimer(streamId, startDate, endDate);
        }


        /// <summary>
        /// Get all shaper rules from the NMS 
        /// </summary>
        /// 
        /// <returns> returns all shaping rules</returns>
        /// SMT-7:1 Get VNO FW and RTN pools
        [WebMethod]
        public List<ShaperRule> getAllShaperRules()
        {
            DataGateway dg = new DataGateway();
            return dg.getAllShaperRules();
                
        }

        /// <summary>
        /// Get one shaper rule by its name 
        /// </summary>
        /// <param name="Rule name"></param>
        /// <returns> one shaping rule</returns>
        /// SMT-7:1 Get VNO FW and RTN pools
        [WebMethod]
        public ShaperRule getShaperRuleByName(string name)
        {
            DataGateway dg = new DataGateway();
            return dg.getShaperRuleByName(name);
        }


        /// <summary>
        /// Get one shaper rule by its position 
        /// </summary>
        /// <param position="Rule position"></param>
        /// <returns> one shaping rule</returns>
        /// SMT-7:1 Get VNO FW and RTN pools
        [WebMethod]
        public ShaperRule getShaperRuleByPosition(int position)
        {
            DataGateway dg = new DataGateway();
            return dg.getShaperRuleByPosition(position);
        }

        /// <summary>
        /// Insert shaper rule into ShaperRules table
        /// </summary>
        /// <param name="Rule name"></param>
        /// <returns> one shaping rule</returns>
        /// SMT-7:6 Create VNO pool into NMS Shaper
        [WebMethod]
        public bool addShaperRule(string name, string parent_group,int indent, int peak_rate_enabled, int peak_rate, int guaranteed_rate_enabled, int guaranteed_rate, int weight_enabled, int weight)
        {
            DataGateway dg = new DataGateway();

            if (dg.getShaperRuleByName(name)!= null)
            {
                throw new ShaperRuleExistsException("The Shaper Rule already exist in the NMS");
            }
                return dg.addShaperRule(name, parent_group, indent, peak_rate_enabled, 1000 * peak_rate, guaranteed_rate_enabled, 1000 * guaranteed_rate, weight_enabled,weight);
            
        }


        /// <summary>
        /// delete shaper rule into ShaperRules table
        /// </summary>
        /// <param name="Rule name"></param>
        /// <returns> true if successful to remove</returns>
        /// SMT-7:6 Create VNO pool into NMS Shaper
        [WebMethod]
        public bool removeShaperRule(string name)
        {
            DataGateway dg = new DataGateway();

            if (dg.getShaperRuleByName(name) == null)
            {
                throw new ShaperRuleExistsException("The Shaper Rule does not exist");
            }
            return dg.deleteShaperRule(name);

        }

        #region protected methods

        /// <summary>
        /// This method enables or disables a MC group in the NMS database
        /// </summary>
        /// <param name="ipAddress">The IP Address of the MC group to Enable/Disable</param>
        /// <param name="state">False = enable, True is disable</param>
        /// <returns>A request ticket with either the EnableStream or DisableStream status</returns>
        protected RequestTicket ChangeMultiCastGroupState(string ipAddress, bool state)
        {
            DataGateway dg = new DataGateway();
            RequestTicket rt = new RequestTicket();
            rt.id = Guid.NewGuid().ToString();

            TypeEnum action;

            if (state)
            {
                action = TypeEnum.EnableStream;
            }
            else
            {
                action = TypeEnum.DisableStream;
            }

            MultiCastInfo mci = this.GetMultiCastGroupByIp(ipAddress);

            if (mci == null)
            {
                throw new NoSuchMultiCastGroupException("MultiCastGroup with IP Address: " + ipAddress + " not available in NMS");
            }
            else
            {
                mci.State = 1; //Enable MC
                if (dg.UpdateMulticastGroup(mci))
                {
                    if (!dg.generateTicket(ref rt, action, mci.MacAddress ))
                    {
                        Logger.log("Could not generate ticket for Multicast create");
                        throw new InternalException("Could not generate Ticket!");
                    }
                }
                else
                {
                    throw new InternalException("Could not change status of Multicast Group with IP: " + mci.IpAddress);
                }
            }

            return rt;
        }

        #endregion

    }
}
