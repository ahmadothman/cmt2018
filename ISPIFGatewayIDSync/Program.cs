﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace ISPIFGatewayIDSync
{
    /// <summary>
    /// Synchronizes the Id field of the endusers table in the ISPIFData database with
    /// the ExtFullName field in the Temrinals table of the CMT Data database
    /// </summary>
    class Program
    {
        class TerminalData
        {
            public string MacAddress { get; set; }
            public string ExtFullName { get; set; }
        }

        static void Main(string[] args)
        {
            Program prg = new Program();
            prg.mainBody();
        }

        private void mainBody()
        {
            if (this.UpdateEndUsers(this.SelectTerminals()))
            {
                Console.WriteLine("Update of End Users succeeded");
            }
            else
            {
                Console.WriteLine("Update of End Users failed");
            }
        }

        /// <summary>
        /// Selects the list of terminals
        /// </summary>
        /// <returns></returns>
        private List<TerminalData> SelectTerminals()
        {
            List<TerminalData> terminals = new List<TerminalData>();

            string CMTDataConnectionString = Properties.Settings.Default.CMTDataConnectionString;
            string selectString = "SELECT MacAddress, ExtFullName FROM Terminals";

            try
            {
                using (SqlConnection conn = new SqlConnection(CMTDataConnectionString))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(selectString, conn))
                    {
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            if (!reader.IsDBNull(1))
                            {
                                TerminalData termData = new TerminalData();
                                termData.MacAddress = reader.GetString(0);
                                termData.ExtFullName = reader.GetString(1);
                                terminals.Add(termData);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return terminals;
        }

        /// <summary>
        /// Updates the EndUsers table
        /// </summary>
        /// <param name="Terminals"></param>
        /// <returns></returns>
        private Boolean UpdateEndUsers(List<TerminalData> Terminals)
        {
            string ISPIFDataConnectionString = Properties.Settings.Default.ISPIFDataConnectionString;
            string updateString = "UPDATE EndUsers SET Id = @ExtFullName WHERE MacAddress = @MacAddress";
            Boolean success = false;
            int numRows = 0;

            using (SqlConnection conn = new SqlConnection(ISPIFDataConnectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(updateString, conn);
                SqlParameter extFullNameParam = new SqlParameter("@ExtFullName", SqlDbType.NVarChar, 100);
                SqlParameter macAddressParam = new SqlParameter("@MacAddress", SqlDbType.NChar, 20);
                cmd.Parameters.Add(extFullNameParam);
                cmd.Parameters.Add(macAddressParam);
                cmd.Prepare();

                foreach (TerminalData term in Terminals)
                {
                    cmd.Parameters[0].Value = term.ExtFullName;
                    cmd.Parameters[1].Value = term.MacAddress;

                    try
                    {
                        numRows += cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        Console.WriteLine("MacAddress: " + term.MacAddress + "; " + "ExtFullName: " + term.ExtFullName);
                    }
                }

                success = true;
            }

            return success;
        }
    }
}
