﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using EmailSyncApp.BOAccountingControlWSRef;

namespace EmailSyncApp
{
    class Program
    {
        const bool _DEBUG = true;
        enum OperatingSystem { Windows, Linux }
        const OperatingSystem _OS = OperatingSystem.Windows;
        const string _LOG_WINDOWS = "C:/Temp/EmailSync.csv";
        const string _LOG_LINUX = "/var/log/EmailSync/EmailSync.log";
        
        static void Main(string[] args)
        {
            Program prg = new Program();
            prg.mainbody();
        }

        protected void mainbody()
        {
            BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();
            Console.WriteLine("Getting terminal list");
            this.log("Getting terminal list");
            Terminal[] terminals = boAccountingControl.GetTerminals();
            this.log("No. of terminals in list;" + terminals.Length);
            Console.WriteLine("No. of terminals in list: " + terminals.Length);
            Distributor dist;
            int counter = 0;
            int countErrors = 0;
            foreach (Terminal t in terminals)
            {
                try
                {
                    if (String.IsNullOrEmpty(t.Email.Trim()) || t.Email.Trim() == "")
                    {
                    
                        dist = boAccountingControl.GetDistributor((int)t.DistributorId);
                        if (dist.Email != null && (dist.Email).Trim() != "")
                        {
                            t.Email = dist.Email;
                            boAccountingControl.UpdateTerminal(t);
                            this.log("New email address for sit ID;" + t.SitId + ";" + t.Email);
                            counter++;
                            Console.WriteLine("New email address for sit ID " + t.SitId + ": " + t.Email);
                        }
                        else
                        {
                            this.log("No email address registered for distributor ID;" + t.DistributorId);
                        }
                    }
                }
                catch
                {
                    this.log("Changing email address failed for sit ID;" + t.SitId);
                    countErrors++;
                }
            }
            this.log("No. of email addresses changed;" + counter);
            this.log("No. of errors;" + countErrors);
        }

        
        /// <summary>
        /// Simple logger
        /// </summary>
        public void log(string msg)
        {
            string logPath = "";

            if (_DEBUG)
            {
                if (_OS == OperatingSystem.Windows)
                {
                    logPath = _LOG_WINDOWS;
                }
                else
                {
                    logPath = _LOG_LINUX;
                }

                using (StreamWriter writer = new StreamWriter(logPath, true))
                {
                    writer.WriteLine("[" + DateTime.Now + "];" + msg);
                }
            }
        }
        
    }
}
