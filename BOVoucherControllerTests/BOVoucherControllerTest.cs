﻿using BOControllerLibrary.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using BOControllerLibrary.Model.Voucher;
using System.Collections.Generic;
using net.nimera.CRCCalculator;
using System.Data;

namespace BOVoucherControllerTests
{
    
    /// <summary>
    ///This is a test class for BOVoucherControllerTest and is intended
    ///to contain all BOVoucherControllerTest Unit Tests
    ///</summary>
    [TestClass()]
    public class BOVoucherControllerTest
    {
        int _distributorId = 216; //The SatADSL distributor
        Guid _userId = Guid.NewGuid();
        string _connectionString = "data source=cmtdata.nimera.net;Database=CMTData; User Id=SatFinAfrica; Password=Lagos#1302";
        string _provider = "System.Data.SqlClient";
        int _volumeId = 200;

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        /// <summary>
        ///A test for GetVoucher
        ///</summary>
        [TestMethod()]
        public void GetVoucherTest()
        {
            BOVoucherController target = new BOVoucherController(_connectionString, _provider); 

            //Create 1 new voucher
            int batchId = 0;
            List<Voucher> vouchers = target.CreateVouchers(_distributorId, 1, _userId, out batchId, _volumeId);

            //Voucher batch info
            VoucherBatch vb = target.GetVoucherBatchInfo(batchId);

            Assert.AreEqual(vb.DistributorId, _distributorId, "Distributors not equal");
            Assert.AreEqual(vb.Volume, 250, "Volumes do not match");
            Assert.AreEqual(vb.UserId, _userId, "User Ids not equal");

            string code = vouchers[0].Code;
            Voucher actual = target.GetVoucher(code);

            Assert.AreEqual(vouchers[0], actual, "Vouchers not equal");
        }

        /// <summary>
        ///A test for BOVoucherController Constructor
        ///</summary>
        [TestMethod()]
        public void BOVoucherControllerConstructorTest()
        {
            BOVoucherController target = new BOVoucherController(_connectionString, _provider);
            Assert.IsNotNull(target, "Could not create BOVoucherController");
        }

        /// <summary>
        ///A test for CreateVouchers
        ///</summary>
        [TestMethod()]
        public void CreateVouchersTest()
        {
            int voucherCounter = 0;
            BOVoucherController target = new BOVoucherController(_connectionString, _provider); 
            int numVouchers = 10; // Create 10 vouchers
            int batchId = 0;

            List<Voucher> actual = target.CreateVouchers(_distributorId, numVouchers, _userId, out batchId, _volumeId);

            Assert.IsTrue (actual.Count == 10, "Could not create " + numVouchers + " vouchers");
            Assert.IsTrue(batchId > 0, "Invalid batch id (=0)");

            foreach(Voucher v in actual)
            {
                voucherCounter++;
                Assert.IsTrue(v.Code.Length == 13, "Code length not correct for voucher: " + voucherCounter);
                Assert.IsTrue(this.CheckCRC(v), "CRC not correct for voucher: " + voucherCounter);
                Assert.IsTrue(v.Available, "Voucher not available for voucher: " + voucherCounter);
            }

        }

        /// <summary>
        ///A test for GetValidVouchers
        ///</summary>
        [TestMethod()]
        public void GetValidVouchersTest()
        {
            BOVoucherController target = new BOVoucherController(_connectionString, _provider); // TODO: Initialize to an appropriate value

            //Create 1 voucher
            int batchId = 0;
            List<Voucher> vouchers = target.CreateVouchers(_distributorId, 1, _userId, out batchId, _volumeId);

            Assert.IsTrue(vouchers.Count != 0, "Voucher not created");

            //Check if the available flag is set to true
            Assert.IsTrue(vouchers[0].Available, "Voucher not available");
        }

        /// <summary>
        ///A test for InvalidateBatch
        ///</summary>
        [TestMethod()]
        public void ValidateBatchTest()
        {
            int voucherIdx = 0;

            BOVoucherController target = new BOVoucherController(_connectionString, _provider);

            //Create a batch of 10 vouchers
            int batchId = 0;
            List<Voucher> vouchers = target.CreateVouchers(_distributorId, 10, _userId, out batchId, _volumeId);
            Assert.IsTrue(vouchers.Count == 10, "Vouchers not created!");

            //Check if the vouchers are available
            foreach (Voucher v in vouchers)
            {
                voucherIdx++;
                Assert.IsTrue(v.Available, "Voucher not available: " + voucherIdx);
            }

            Assert.IsTrue(target.ValidateBatch(batchId), "InvalidateBatch failed");

            //Check if vouchers are indeed invalidated
            List<Voucher> validatedVouchers = target.GetVouchers(batchId);

            voucherIdx = 0;
            foreach (Voucher v in validatedVouchers)
            {
                voucherIdx++;
                Assert.IsFalse(v.Available, "Voucher still available: " + voucherIdx);
            }
        }

        /// <summary>
        ///A test for ValidateVoucher
        ///</summary>
        [TestMethod()]
        public void ValidateVoucherTest()
        {
            int voucherIdx = 0;

            BOVoucherController target = new BOVoucherController(_connectionString, _provider);

            //Create a batch of 1 voucher
            int batchId = 0;
            List<Voucher> vouchers = target.CreateVouchers(_distributorId, 1, _userId, out batchId, _volumeId);
            Assert.IsTrue(vouchers.Count == 1, "Vouchers not created!");

            //Check if the vouchers are available
            foreach (Voucher v in vouchers)
            {
                voucherIdx++;
                Assert.IsTrue(v.Available, "Voucher not available: " + voucherIdx);
            }

            Assert.IsTrue(target.ValidateVoucher(vouchers[0].Code, vouchers[0].Crc.ToString("x4")), "InvalidateBatch failed");

            //Check if vouchers are indeed validated
            List<Voucher> invalidatedVouchers = target.GetVouchers(batchId);

            voucherIdx = 0;
            foreach (Voucher v in invalidatedVouchers)
            {
                voucherIdx++;
                Assert.IsFalse(v.Available, "Voucher still available: " + voucherIdx);
            }
        }

        /// <summary>
        ///A test for GetVoucherBatchInfo
        ///</summary>
        [TestMethod()]
        public void GetVoucherBatchInfoTest()
        {
            BOVoucherController target = new BOVoucherController(_connectionString, _provider);
            int batchId = 0;

            //Create a new batch of 1 voucher
            List<Voucher> vouchers = target.CreateVouchers(_distributorId, 1, _userId, out batchId, _volumeId);
            VoucherBatch actual = target.GetVoucherBatchInfo(batchId);

            Assert.AreEqual(batchId, actual.BatchId, "Batch Ids are not equal");
            Assert.AreEqual(actual.UserId, _userId, "User Ids don't match");
            Assert.IsNotNull(actual.DateCreated, "Creation date is not correct");
            Assert.AreEqual(actual.DistributorId, _distributorId, "Distributor Ids are not equal");
        }

        /// <summary>
        ///A test for GetVouchers
        ///</summary>
        [TestMethod()]
        public void GetVouchersTest()
        {
            BOVoucherController target = new BOVoucherController(_connectionString, _provider);

            //Create a batch of 10 vouchers
            int batchId = 0;
            List<Voucher> generatedVouchers = target.CreateVouchers(_distributorId, 10, _userId, out batchId, _volumeId);

            //retrieve the vouchers again from the database
            List<Voucher> storedVouchers = target.GetVouchers(batchId);

            Assert.IsTrue(storedVouchers.Count != 0, "GetVouchers method failed");

            //Check if vouchers are equal
            for (int i = 0; i < 10; i++)
            {
                Assert.AreEqual(storedVouchers[i], generatedVouchers[i], "Voucher " + i + " does not match");
            }
        }

        /// <summary>
        ///A test for GetVoucherVolumes
        ///</summary>
        [TestMethod()]
        public void GetVoucherVolumesTest()
        {
            BOVoucherController target = new BOVoucherController(_connectionString, _provider); // TODO: Initialize to an appropriate value
            List<VoucherVolume> vvList = target.GetVoucherVolumes();
            Assert.IsTrue(vvList.Count > 0, "Voucher volume list empty");
        }

        /// <summary>
        /// A test for ResetVoucher
        /// </summary>
        [TestMethod]
        public void ResetVoucherTest()
        {
            int voucherIdx = 0;

            BOVoucherController target = new BOVoucherController(_connectionString, _provider);

            //Create a batch of 1 voucher
            int batchId = 0;
            List<Voucher> vouchers = target.CreateVouchers(_distributorId, 1, _userId, out batchId, _volumeId);
            Assert.IsTrue(vouchers.Count == 1, "Vouchers not created!");

            //Validate voucher batch
            target.ValidateBatch(batchId);
            List<Voucher> validatedVouchers = target.GetVouchers(batchId);
            foreach (Voucher v in validatedVouchers)
            {
                voucherIdx++;
                Assert.IsFalse(v.Available, "Voucher not validated: " + voucherIdx);
            }

            Assert.IsTrue(target.ResetVoucher(vouchers[0].Code, vouchers[0].Crc.ToString("X4")), "InvalidateBatch failed");

            //Check if vouchers are indeed Reset
            List<Voucher> availableVouchers = target.GetVouchers(batchId);

            voucherIdx = 0;
            foreach (Voucher v in availableVouchers)
            {
                voucherIdx++;
                Assert.IsTrue(v.Available, "Voucher not available: " + voucherIdx);
            }
        }

        #region Helper methods

        /// <summary>
        /// Checks the CRC of a voucher
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public bool CheckCRC(Voucher v)
        {
            Byte[] codeBytes = new System.Text.UTF8Encoding().GetBytes(v.Code); 
            ushort crc = Crc16.FastCrc16Calc(v.CrcSeed, codeBytes);
            return crc == v.Crc;
        }

        #endregion


        /// <summary>
        ///A test for GenerateVoucherReport
        ///</summary>
        [TestMethod()]
        public void GenerateVoucherReportTest()
        {
            BOVoucherController target = new BOVoucherController(_connectionString, _provider);
            DateTime startDate = new DateTime(2012, 08, 15);
            DateTime endDate = new DateTime(2012, 08, 17);
            DataSet voucherReport = target.GenerateVoucherReport(startDate, endDate);
            Assert.IsTrue(voucherReport.Tables["VoucherReportTable"].Rows.Count > 0, "No rows retreived");
        }
    }
}
