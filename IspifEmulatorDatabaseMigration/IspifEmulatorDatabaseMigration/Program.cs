﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using BOControllerLibrary.Model.Terminal;
using PetaPoco;

namespace IspifEmulatorDatabaseMigration
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             Tasks:
                1.	Replace ServicePacks by ServicePacks from the CMT Database -> OK
             * 
                2.	Copy terminals MacAddress from CMT Database to ISPIF  Emulator database (leave State & Volume at 0)
             * 
                3.	For the EndUsers table from the Terminals table copy the MacAddress, the SitID, the ISP and the SLaId fields.
             * 
                4.	Before start clean-up all records from the EndUser and Terminal tables in the ISPIF Emulator Database.
                The ISPIF Database runs on the development (server cmtdata.nimera.net), the source CMT Database runs on the staging server at: 176.34.182.231 . (http://satadsl2.nimera.mobi)
                The application must run only once on the staging server, later we will need to run the same procedure on the preview server.

             * SELECT MacAddress, EndUserId, SitId, IspId, SlaId from Terminals
             */

            try {
                var ispifConn = new Database("data source=176.34.243.152; Database=ISPIFData; User Id=ISPIFUser; Password=ISPIFUser", "System.Data.SqlClient");
                var cmtData = new Database("data source=176.34.243.152;Database=CMTData-Production; User Id=SatFinAfrica; Password=Lagos#1302", "System.Data.SqlClient");

            //// 4 cleanup ispif
                Cleanup(ispifConn);
            
            //// 1
            ReplaceServicePacks(ispifConn, cmtData);
            
            //// 2
            MigrateTerminalMacAddresses(ispifConn, cmtData);

            // 3
            MigrateEndUsersFromTerminalsTable(ispifConn, cmtData);

            

                }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            Console.ReadKey();
        }

        private static void Cleanup(Database ispifConn)
        {
            ispifConn.Execute("DELETE FROM Tickets"); // dependency
            ispifConn.Execute("DELETE FROM Terminals");
            ispifConn.Execute("DELETE FROM EndUsers");
            ispifConn.Execute("DELETE FROM ServicePacks");
            // delete from servicepacks too?

        }

        /// <summary>
        /// 3.	For the EndUsers table from the Terminals table copy the MacAddress, the SitID, the ISP and the SLaId fields.
        /// </summary>
        /// <param name="ispifConn"></param>
        /// <param name="cmtData"></param>
        private static void MigrateEndUsersFromTerminalsTable(Database ispifConn, Database cmtData)
        {
            Console.WriteLine("Performing MigrateEndUsersFromTerminalsTable");
            var records = cmtData.Query<dynamic>(@"SELECT MacAddress, FullName, SitId, IspId, SlaId from Terminals");
            foreach (var record in records)
            {
                try
                {
                    ispifConn.Execute(
                        @"
                                INSERT INTO EndUsers
                                SELECT @Id, @Mac, @SlaId, @IspId, @SitId
                            ",
                        new
                        {
                            Id = record.FullName,
                            Mac = record.MacAddress,
                            SlaId = record.SlaId,
                            IspId = record.IspId,
                            SitId = record.SitId
                        });
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
            }
            Console.WriteLine("END Performing MigrateEndUsersFromTerminalsTable");
        }

        /// <summary>
        /// 2.	Copy terminals MacAddress from CMT Database to ISPIF  Emulator database (leave State & Volume at 0)
        /// </summary>
        /// <param name="ispifConn"></param>
        /// <param name="cmtData"></param>
        private static void MigrateTerminalMacAddresses(Database ispifConn, Database cmtData)
        {
            Console.WriteLine("Performing MigrateTerminalMacAddresses");
           
            var macs = cmtData.Query<string>(@"SELECT MacAddress from Terminals");
            foreach (var mac in macs)
            {
                try
                {
                    ispifConn.Execute(
                        @"
                                INSERT INTO Terminals
                                SELECT @mac, 0, 0
                            ",
                        new
                            {
                                mac
                            });
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
            }
            Console.WriteLine("END Performing MigrateTerminalMacAddresses");
        }
        
        /// <summary>
        /// 1.	Replace ServicePacks by ServicePacks from the CMT Database -> OK
        /// </summary>
        /// <param name="ispifConn"></param>
        /// <param name="cmtData"></param>
        private static void ReplaceServicePacks(Database ispifConn, Database cmtData)
        {
            var results = cmtData.Query<ServiceLevel>(@"SELECT * FROM ServicePacks");
            foreach (var i in results)
            {
                try
                {
                    ispifConn.Execute(
                        @"
                                INSERT INTO ServicePacks 
                                SELECT @SlaId, @SlaName, @FupThreshold, @DrAboveFup, @SlaCommonName, @IspId, @FreeZone, @Business
                            ",
                        new
                            {
                                i.SlaId,
                                i.SlaName,
                                i.FupThreshold,
                                i.DrAboveFup,
                                i.SlaCommonName,
                                i.IspId,
                                i.FreeZone,
                                i.Business
                            });
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
            }
        }
    }
}
