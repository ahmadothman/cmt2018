﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Data.SqlClient;
using Microsoft.Synchronization;
using Microsoft.Synchronization.Data;
using Microsoft.Synchronization.Data.SqlServer;

namespace SyncManager
{
    public partial class FormMain : Form
    {
        const string _SCOPE = "CMTDataScope";
        const string _ClientConnectionString = "Data Source=.\\SQLEXPRESS; Initial Catalog=CMTData-Local; Integrated Security=SSPI;";
        const string _ServerConnectionString = "Data source=cmtdata.nimera.net;Database=CMTData; User Id=SatFinAfrica; Password=Lagos#1302";
        delegate void logCallBack(string msg);
        delegate void progressCallBack(int value);

        // create the sync orhcestrator
        SyncOrchestrator syncOrchestrator = new SyncOrchestrator();

        public FormMain()
        {
            InitializeComponent();
        }

        private void buttonSync_Click(object sender, EventArgs e)
        {
            textBoxOut.Text = "";
            Thread workerThread = new Thread(this.Sync);
            workerThread.Start();
        }

        public void Sync()
        {
            this.Log("Synchronization started");
            try
            {
                SqlConnection clientConn = new SqlConnection(_ClientConnectionString);
                SqlConnection serverConn = new SqlConnection(_ServerConnectionString);

                

                // set local provider of orchestrator to a sync provider associated with the 
                // ProductsScope in the SyncExpressDB express client database
                syncOrchestrator.LocalProvider = new SqlSyncProvider(_SCOPE, clientConn);

                // set the remote provider of orchestrator to a server sync provider associated with
                // the ProductsScope in the SyncDB server database
                syncOrchestrator.RemoteProvider = new SqlSyncProvider(_SCOPE, serverConn);

                // set the direction of sync session to Upload and Download
                syncOrchestrator.Direction = SyncDirectionOrder.UploadAndDownload;

                // subscribe for errors that occur when applying changes to the client
                ((SqlSyncProvider)syncOrchestrator.LocalProvider).ApplyChangeFailed += new EventHandler<DbApplyChangeFailedEventArgs>(Program_ApplyChangeFailed);
                syncOrchestrator.SessionProgress += this.Program_SessionProgress;

                // execute the synchronization process
                SyncOperationStatistics syncStats = syncOrchestrator.Synchronize();

                //Print stats
                this.Log("Start Time: " + syncStats.SyncStartTime);
                this.Log("Total Changes Uploaded: " + syncStats.UploadChangesTotal);
                this.Log("Total Changes Downloaded: " + syncStats.DownloadChangesTotal);
                this.Log("Complete Time: " + syncStats.SyncEndTime);
                this.Log(String.Empty);
                this.Log("Synchronization ended");

            }
            catch (Exception ex)
            {
                this.Log(ex.Message);
                this.Log("Synchronization ended");
            }
        }

        protected void Program_ApplyChangeFailed(object sender, DbApplyChangeFailedEventArgs e)
        {
            // display conflict type
            this.Log("Change failed: " + e.Conflict.Type.ToString());

            // display error message 
            this.Log(e.Error.Message);
        }

        private void Program_SessionProgress(object sender, SyncStagedProgressEventArgs args)
        {
            this.Log(args.Stage.ToString());
            updateProgress((int) (100 * args.CompletedWork / args.TotalWork));
        }



        private void Log(string msg)
        {
            if (textBoxOut.InvokeRequired)
            {
                logCallBack l = new logCallBack(Log);
                this.Invoke(l, new object[] { msg });
            }
            else
            {
                textBoxOut.Text = textBoxOut.Text + msg + Environment.NewLine;
            }
        }

        private void updateProgress(int value)
        {
            if (progressBar1.InvokeRequired)
            {
                progressCallBack p = new progressCallBack(updateProgress);
                this.Invoke(p, new object[] { value });
            }
            else
            {
                progressBar1.Value = value;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            syncOrchestrator.Cancel();
        }
    }
}
