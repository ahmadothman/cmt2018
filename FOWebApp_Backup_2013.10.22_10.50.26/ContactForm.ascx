﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContactForm.ascx.cs" Inherits="FOWebApp.ContactForm" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" 
    EnableHistory="True">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="ButtonSend">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadAjaxPanelContactForm" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManager>
<telerik:RadAjaxPanel ID="RadAjaxPanelContactForm" runat="server" Height="200px" Width="500px">
<table cellpadding="10" cellspacing="5" width="100%">
    <tr>
        <td>
            <asp:Label ID="Label1" runat="server" Text="Your E-Mail Address:" ForeColor="#FF3300" Font-Bold="True"></asp:Label>
        </td>
        <td>
            <asp:TextBox ID="TextBoxEMail" runat="server" Columns="40"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
             ControlToValidate="TextBoxEMail" Display="None" 
                ErrorMessage="E-Mail address of the sender cannot be empty!" 
                SetFocusOnError="True"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Subject:
        </td>
        <td>
            <asp:DropDownList ID="DropDownListSubject" runat="server">
            <asp:ListItem Value="Problem report">Problem report</asp:ListItem>
            <asp:ListItem Value="Service request">Service Request or Question</asp:ListItem>
            <asp:ListItem Value="Other">Other</asp:ListItem>
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td colspan="2">Message:</td>
    </tr>
    <tr>
        <td colspan="2">
            <!-- <asp:TextBox ID="TextBoxMessage" runat="server" TextMode="MultiLine" Rows="10" 
                Columns="80"></asp:TextBox> -->
            <telerik:RadTextBox ID="RadTextBoxMessage" Runat="server" 
                EmptyMessage="Enter your message" Font-Size="Medium" Rows="20" Skin="WebBlue" 
                TextMode="MultiLine" Width="100%">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Button ID="ButtonSend" runat="server" Text="Send mail" 
                onclick="ButtonSend_Click" />
        </td>
        <td>
            <asp:Label ID="LabelResult" runat="server" Text="" Visible="False"></asp:Label>
        </td>
    </tr>
</table>
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
        HeaderText="Please review the following errors:" ShowMessageBox="True" 
        ShowSummary="False" />
</telerik:RadAjaxPanel>
