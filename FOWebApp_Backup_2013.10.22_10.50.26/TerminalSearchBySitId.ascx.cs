﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;

namespace FOWebApp
{
    public partial class TerminalSearchBySitId : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Make the management tab invisible for non Distributors and Administrators
            if (HttpContext.Current.User.IsInRole("Organization") ||
                    HttpContext.Current.User.IsInRole("EndUser"))
            {
                RadTabManagement.Visible = false;
            }
        }

        protected void ButtonSearch_Click(object sender, EventArgs e)
        {
            //Initialize the tabview with the provided tab address
            string macAddress = TextBoxValue.Text;
            if (!macAddress.Equals(""))
            {
                //Load the tab view
                RadTab terminalsTab = RadTabStripTerminalView.FindTabByText("Terminal");
                AddPageView(terminalsTab);
            }
        }

        private void AddPageView(RadTab tab)
        {
            RadPageView pageView = new RadPageView();
            pageView.ID = tab.Text;
            RadMultiPageTerminalView.PageViews.Add(pageView);
            pageView.CssClass = "pageView";
            tab.PageViewID = pageView.ID;
        }

        protected void RadMultiPageTerminalView_PageViewCreated(object sender, RadMultiPageEventArgs e)
        {
            Control control = null;
            string macAddress = "";

            //Add content to the page view depending on the selected tab
            if (e.PageView.ID.Equals("Terminal"))
            {
                macAddress = this.getMacAddress(TextBoxValue.Text, TextBoxISP.Text);

                if (!macAddress.Equals(""))
                {
                    //Load the TerminalDetailTab control
                    TerminalDetailTab terminalDetailTab =
                        (TerminalDetailTab)Page.LoadControl("TerminalDetailTab.ascx");
                    terminalDetailTab.MacAddress = macAddress;
                    control = terminalDetailTab;
                    control.ID = e.PageView.ID + "_userControl";
                }
            }
            else if (e.PageView.ID.Equals("Status"))
            {
                macAddress = this.getMacAddress(TextBoxValue.Text, TextBoxISP.Text);

                if (!macAddress.Equals(""))
                {
                    //Load the TerminalDetailTab control
                    TerminalStatusTab terminalStatusTab =
                        (TerminalStatusTab)Page.LoadControl("TerminalStatusTab.ascx");
                    terminalStatusTab.MacAddress = macAddress;
                    control = terminalStatusTab;
                    control.ID = e.PageView.ID + "_userControl";
                }
            }
            else if (e.PageView.ID.Equals("Traffic"))
            {
                macAddress = this.getMacAddress(TextBoxValue.Text, TextBoxISP.Text);

                if (!macAddress.Equals(""))
                {
                    //Load the TerminalDetailTab control
                    TerminalConsumedTab terminalConsumedTab =
                        (TerminalConsumedTab)Page.LoadControl("TerminalConsumedTab.ascx");
                    terminalConsumedTab.MacAddress = macAddress;
                    control = terminalConsumedTab;
                    control.ID = e.PageView.ID + "_userControl";
                }
            }
            else if (e.PageView.ID.Equals("Accumulated"))
            {
                macAddress = this.getMacAddress(TextBoxValue.Text, TextBoxISP.Text);

                if (!macAddress.Equals(""))
                {
                    //Load the TerminalDetailTab control
                    TerminalAccumulatedTab terminalAccumulatedTab =
                        (TerminalAccumulatedTab)Page.LoadControl("TerminalAccumulatedTab.ascx");
                    terminalAccumulatedTab.MacAddress = macAddress;
                    control = terminalAccumulatedTab;
                    control.ID = e.PageView.ID + "_userControl";
                }
            }
            else if (e.PageView.ID.Equals("Management"))
            {
                //Load the TerminalDetailTab control
                TerminalManagementTab terminalManagementTab =
                    (TerminalManagementTab)Page.LoadControl("TerminalManagementTab.ascx");
                terminalManagementTab.MacAddress = TextBoxValue.Text;
                control = terminalManagementTab;
                control.ID = e.PageView.ID + "_userControl";
            }
            else
            {
                control = Page.LoadControl("TestUserControl.ascx");
                control.ID = e.PageView.ID + "_userControl";
            }

            if (control != null)
            {
                e.PageView.Controls.Add(control);
            }
        }

        protected void RadTabStripTerminalView_TabClick(object sender, Telerik.Web.UI.RadTabStripEventArgs e)
        {
            AddPageView(e.Tab);
            e.Tab.PageView.Selected = true;
        }

        /// <summary>
        /// Returns a MAC Address for the given sitId
        /// </summary>
        /// <param name="sitId"></param>
        /// <returns></returns>
        private string getMacAddress(string sitId, string ispId)
        {
            string macAddress = "";
            try
            {
                int iSitId = Int32.Parse(sitId);
                int iIspId = Int32.Parse(ispId);
                BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();
                Terminal terminal = boAccountingControl.GetTerminalDetailsBySitId(iSitId, iIspId);

                if (terminal != null)
                {
                    macAddress = terminal.MacAddress;
                }
            }
            catch (Exception ex)
            {
                BOLogControlWS boLogControlWS = new BOLogControlWS();
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDateTime = DateTime.Now;
                cmtEx.ExceptionDesc = ex.Message;
                cmtEx.UserDescription = "Method: TerminalSearchBySitId.getMacAddress";
                cmtEx.StateInformation = "SitId: " + sitId;
                boLogControlWS.LogApplicationException(cmtEx);
            }
            return macAddress;
        }
    }
}