﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FOWebApp.App_Code
{
    /// <summary>
    /// Enumerates the possible error levels to be used by the 
    /// ApplicationExceptionLog function
    /// </summary>
    enum ExceptionLevelEnum
    {
        Debug, Info, Warning, Error, Critical
    }
}
