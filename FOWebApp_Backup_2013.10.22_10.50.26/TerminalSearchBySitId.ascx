﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TerminalSearchBySitId.ascx.cs" Inherits="FOWebApp.TerminalSearchBySitId" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script type="text/javascript">
    function onTabSelecting(sender, args) {
        if (args.get_tab().get_pageViewID()) {
            args.get_tab().set_postBack(false);
        }
    }
</script>
<telerik:RadAjaxPanel ID="RadAjaxPanelTerminalSearchByMac" runat="server" Height="550px" Width="900px" LoadingPanelID="RadAjaxLoadingPanel1">
<div id="searchDiv">
    <table>
        <tr>
            <td>
                <asp:Label ID="LabelValue" runat="server" Text="Sit Id:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxValue" runat="server"></asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBoxValue"
                    ErrorMessage="Please enter a valid Sit Id" Display="Dynamic">
                </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelISPValue" runat="server" Text="ISP:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxISP" runat="server"></asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorIspId" runat="server" ControlToValidate="TextBoxISP"
                    ErrorMessage="Please enter a valid ISP" Display="Dynamic">
                </asp:RequiredFieldValidator>
            </td>
            <td>
                <asp:Button ID="ButtonSearch" runat="server" Text="Search" OnClick="ButtonSearch_Click" />
            </td>
        </tr>
    </table>
</div>
<br />
<!-- Terminal view Rad panel -->
<telerik:RadTabStrip ID="RadTabStripTerminalView" runat="server" 
    MultiPageID="RadMultiPageTerminalView" Skin="WebBlue" 
    SelectedIndex="4" OnClientTabSelecting="onTabSelecting" 
    ontabclick="RadTabStripTerminalView_TabClick">
    <Tabs>
        <telerik:RadTab runat="server" Text="Terminal">
        </telerik:RadTab>
        <telerik:RadTab runat="server" Text="Status">
        </telerik:RadTab>
        <telerik:RadTab runat="server" Text="Traffic">
        </telerik:RadTab>
         <telerik:RadTab runat="server" Text="Accumulated">
        </telerik:RadTab>
        <telerik:RadTab ID="RadTabManagement" runat="server" Text="Management" Visible="false">
        </telerik:RadTab>
    </Tabs>
</telerik:RadTabStrip>
<telerik:RadMultiPage ID="RadMultiPageTerminalView" runat="server" 
    BorderColor="#CCCCCC" BorderStyle="Solid" BorderWidth="1px" Height="550px" 
    Width="900px" ScrollBars="Auto" SelectedIndex="0" OnPageViewCreated="RadMultiPageTerminalView_PageViewCreated">
</telerik:RadMultiPage>
</telerik:RadAjaxPanel>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Default" Width="900" Height="550">
</telerik:RadAjaxLoadingPanel>