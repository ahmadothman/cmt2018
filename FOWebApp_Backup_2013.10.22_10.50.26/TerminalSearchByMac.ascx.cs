﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace FOWebApp
{
    public partial class TerminalSearchByMac : System.Web.UI.UserControl
    {
        const string _HEXBYTE = "<0|1|2|3|4|5|6|7|8|9|a|b|c|d|e|f><0|1|2|3|4|5|6|7|8|9|a|b|c|d|e|f>";

        protected void Page_Load(object sender, EventArgs e)
        {
            RadMaskedTextBoxMacAddress.Mask =
              _HEXBYTE + "<:>" + _HEXBYTE + "<:>" + _HEXBYTE + "<:>" + _HEXBYTE + "<:>" + _HEXBYTE + "<:>" +
              _HEXBYTE;

            //Make the management tab invisible for non Distributors and Administrators
            if (HttpContext.Current.User.IsInRole("Organization") ||
                    HttpContext.Current.User.IsInRole("EndUser"))
            {
                RadTabManagement.Visible = false;
            }
        }

        protected void ButtonSearch_Click(object sender, EventArgs e)
        {
            //Initialize the tabview with the provided tab address
            string macAddress = RadMaskedTextBoxMacAddress.Text;
            if (!macAddress.Equals(""))
            {
                //Load the tab view
                 RadTab terminalsTab = RadTabStripTerminalView.FindTabByText("Terminal");
                 AddPageView(terminalsTab);
            }
        }

        private void AddPageView(RadTab tab)
        {
            RadPageView pageView = new RadPageView();
            pageView.ID = tab.Text;
            RadMultiPageTerminalView.PageViews.Add(pageView);
            pageView.CssClass = "pageView";
            tab.PageViewID = pageView.ID;
        }

        protected void RadMultiPageTerminalView_PageViewCreated(object sender, RadMultiPageEventArgs e)
        {
            Control control = null;
            //Add content to the page view depending on the selected tab
            if (e.PageView.ID.Equals("Terminal"))
            {
                //Load the TerminalDetailTab control
                TerminalDetailTab terminalDetailTab =
                    (TerminalDetailTab)Page.LoadControl("TerminalDetailTab.ascx");
                terminalDetailTab.MacAddress = RadMaskedTextBoxMacAddress.Text;
                control = terminalDetailTab;
                control.ID = e.PageView.ID + "_userControl";
            }
            else if (e.PageView.ID.Equals("Status"))
            {
                //Load the TerminalDetailTab control
                TerminalStatusTab terminalStatusTab =
                    (TerminalStatusTab)Page.LoadControl("TerminalStatusTab.ascx");
                terminalStatusTab.MacAddress = RadMaskedTextBoxMacAddress.Text;
                control = terminalStatusTab;
                control.ID = e.PageView.ID + "_userControl";
            }
            else if (e.PageView.ID.Equals("Traffic"))
            {
                //Load the TerminalDetailTab control
                TerminalConsumedTab terminalConsumedTab =
                    (TerminalConsumedTab)Page.LoadControl("TerminalConsumedTab.ascx");
                terminalConsumedTab.MacAddress = RadMaskedTextBoxMacAddress.Text;
                control = terminalConsumedTab;
                control.ID = e.PageView.ID + "_userControl";
            }
            else if (e.PageView.ID.Equals("Accumulated"))
            {
                //Load the TerminalDetailTab control
                TerminalAccumulatedTab terminalAccumulatedTab =
                    (TerminalAccumulatedTab)Page.LoadControl("TerminalAccumulatedTab.ascx");
                terminalAccumulatedTab.MacAddress = RadMaskedTextBoxMacAddress.Text;
                control = terminalAccumulatedTab;
                control.ID = e.PageView.ID + "_userControl";
            }
            else if (e.PageView.ID.Equals("Management"))
            {
                //Load the TerminalDetailTab control
                TerminalManagementTab terminalManagementTab=
                    (TerminalManagementTab)Page.LoadControl("TerminalManagementTab.ascx");
                terminalManagementTab.MacAddress = RadMaskedTextBoxMacAddress.Text;
                control = terminalManagementTab;
                control.ID = e.PageView.ID + "_userControl";
            }
            else
            {
                control = Page.LoadControl("TestUserControl.ascx");
                control.ID = e.PageView.ID + "_userControl";
            }

            e.PageView.Controls.Add(control);
        }

        protected void RadTabStripTerminalView_TabClick(object sender, Telerik.Web.UI.RadTabStripEventArgs e)
        {
            AddPageView(e.Tab);
            e.Tab.PageView.Selected = true;
        }
    }
}