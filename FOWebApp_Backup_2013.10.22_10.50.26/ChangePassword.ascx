﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ChangePassword.ascx.cs"
    Inherits="FOWebApp.ChangePassword" %>
<style type="text/css">
    .style2
    {
        width: 367px;
    }
</style>
<asp:ChangePassword ID="ChangeUserPassword" runat="server">
    <ChangePasswordTemplate>
        <table cellpadding="10" cellspacing="5">
            <tr>
                <td>
                    Old Password:
                </td>
                <td>
                    <asp:TextBox ID="CurrentPassword" runat="server" TextMode="Password" />
                </td>
            </tr>
            <tr>
                <td>
                    New Password:
                </td>
                <td>
                    <asp:TextBox ID="NewPassword" runat="server" TextMode="Password" />
                </td>
            </tr>
            <tr>
                <td>
                    Confirmation:
                </td>
                <td>
                    <asp:TextBox ID="ConfirmNewPassword" runat="server" TextMode="Password" />
                </td>
            </tr>
            <td>
                <asp:Button ID="ChangePasswordPushButton" CommandName="ChangePassword" runat="server"
                    Text="Change Password" />
            </td>
            <td>
                <asp:Literal ID="FailureText" runat="server" EnableViewState="false" />
            </td>
        </table>
    </ChangePasswordTemplate>
    <SuccessTemplate>
        Your password has been changed!
    </SuccessTemplate>
</asp:ChangePassword>
