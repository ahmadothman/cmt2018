<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="DefaultHeader.ascx.cs" Inherits="DefaultHeader" %>
<div class="logotext">
<div class="title">Customer Management Tool</div>
<%  
    if (HttpContext.Current.User.IsInRole("Distributor"))
    { 
%>
    <div class="subtitle">Distributor 
        <asp:Label ID="LabelVersionDistr" runat="server" Font-Italic="True"></asp:Label></div>
<%
    }
%>
<%  
    if (HttpContext.Current.User.IsInRole("NOC Administrator"))
    { 
%>
    <div class="subtitle">NOCSA 
        <asp:Label ID="LabelVersionNocsa" runat="server" Font-Italic="True"></asp:Label></div>
<%
    }
%>
<%
    if (HttpContext.Current.User.IsInRole("NOC Operator"))
    {
%>
    <div class="subtitle">NOCOP 
        <asp:Label ID="LabelVersionNocop" runat="server" Font-Italic="True"></asp:Label></div>
<%
    }
%>
<%
    if (HttpContext.Current.User.IsInRole("Organization"))
    {
%>
    <div class="subtitle">Organization 
        <asp:Label ID="LabelVersionOrg" runat="server" Font-Italic="True"></asp:Label></div>
<%
    }
%>
</div>
<div class="logo">
    <img src="Images/LogoSatADSL.jpg" alt="SatADSL Logo" height="80px"/>
</div>

