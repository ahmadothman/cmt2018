﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebFormTest.aspx.cs" Inherits="FOWebApp.WebFormTest" %>

<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div>
    
        <telerik:RadGrid ID="RadGrid1" runat="server">
        </telerik:RadGrid>
    
    </div>
        <telerik:RadRadialGauge ID="RadRadialGauge1" runat="server" Height="300px" Skin="BlackMetroTouch" Width="300px">
        </telerik:RadRadialGauge>
    </form>
</body>
</html>
