﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TerminalTabView.ascx.cs" Inherits="FOWebApp.EndUsers.TerminalTabView" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script type="text/javascript">
    function onTabSelecting(sender, args) {
        if (args.get_tab().get_pageViewID()) {
            args.get_tab().set_postBack(false);
        }
    }
</script>
<telerik:RadAjaxPanel ID="RadAjaxPanelEndUser" runat="server" Height="600px" Width="650px" LoadingPanelID="RadAjaxLoadingPanel1">
<br />
<!-- Terminal view Rad panel -->
<telerik:RadTabStrip ID="RadTabStripTerminalView" runat="server" 
    MultiPageID="RadMultiPageTerminalView" Skin="WebBlue" 
    SelectedIndex="3" OnClientTabSelecting="onTabSelecting" 
        ontabclick="RadTabStripTerminalView_TabClick">
    <Tabs>
        <telerik:RadTab runat="server" Text="Terminal" Selected="true">
        </telerik:RadTab>
        <telerik:RadTab runat="server" Text="Status">
        </telerik:RadTab>
        <telerik:RadTab runat="server" Text="Traffic">
        </telerik:RadTab>
         <telerik:RadTab runat="server" Text="Accumulated">
        </telerik:RadTab>
    </Tabs>
</telerik:RadTabStrip>
<telerik:RadMultiPage ID="RadMultiPageTerminalView" runat="server" 
    BorderColor="#CCCCCC" BorderStyle="Inset" BorderWidth="1px" Height="500px" 
    Width="650px" ScrollBars="Auto" SelectedIndex="0" BackColor="#f7f7f7" 
        onpageviewcreated="RadMultiPageTerminalView_PageViewCreated">
</telerik:RadMultiPage>
</telerik:RadAjaxPanel>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Default" Width="800" Height="800">
</telerik:RadAjaxLoadingPanel>