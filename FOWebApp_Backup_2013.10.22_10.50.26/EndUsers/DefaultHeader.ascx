﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DefaultHeader.ascx.cs"
    Inherits="FOWebApp.EndUsers.DefaultHeader" %>
<div id="header-container">
    <header class="wrapper">
			<a href="/" id="logo"><img src="../images/logo_satadsl.png" alt="SatADSL" width="177" height="53"></a>
			<nav>
				<ul>
					<li><a href="http://www.satadsl.net/index.html">Home</a></li>
					<li><a href="http://www.satadsl.net/service.html">Services</a></li>
					<li><a href="http://www.satadsl.net/system.html">System</a></li>
                    <li class="active"><a href="#">My SatADSL</a></li>
					<li><a href="http://www.satadsl.net/about.html">About</a></li>
				</ul>
			</nav>
			<div id="myMenu">
				<a href="#">Welcome <asp:Label ID="LabelName" runat="server"></asp:Label> </a>
				<a href="../EndUsers/EndUserLogoff.aspx">Logoff</a>
			</div>
			<!-- <ul id="languages">
				<li><a href="#">Nl</a></li>
				<li><a href="#">Fr</a></li>
				<li>En</li>
			</ul> -->
		</header>
</div>
