﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;

namespace FOWebApp.EndUsers
{
    public partial class TerminalTabView : System.Web.UI.UserControl
    {
        //The target SitId
        public int SitId { get; set; }

        //The target IspId
        public int IspId { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //Initialize the tabview with the provided tab address
                if (!SitId.Equals(""))
                {
                    //Load the tab view
                    RadTab terminalsTab = RadTabStripTerminalView.FindTabByText("Terminal");
                    AddPageView(terminalsTab);
                }
            }
        }

        private void AddPageView(RadTab tab)
        {
            RadPageView pageView = new RadPageView();
            pageView.ID = tab.Text;
            RadMultiPageTerminalView.PageViews.Add(pageView);
            pageView.CssClass = "pageView";
            tab.PageViewID = pageView.ID;
        }

        protected void RadTabStripTerminalView_TabClick(object sender, Telerik.Web.UI.RadTabStripEventArgs e)
        {
            AddPageView(e.Tab);
            e.Tab.PageView.Selected = true;
        }

        protected void RadMultiPageTerminalView_PageViewCreated(object sender, Telerik.Web.UI.RadMultiPageEventArgs e)
        {
            Control control = null;
            string macAddress = this.getMacAddress(SitId, IspId);

            //Add content to the page view depending on the selected tab
            if (e.PageView.ID.Equals("Terminal"))
            {
                if (!macAddress.Equals(""))
                {
                    //Load the TerminalDetailTab control
                    TerminalDetailTab terminalDetailTab =
                        (TerminalDetailTab)Page.LoadControl("../TerminalDetailTab.ascx");
                    terminalDetailTab.MacAddress = macAddress;
                    control = terminalDetailTab;
                    control.ID = e.PageView.ID + "_userControl";
                }
            }
            else if (e.PageView.ID.Equals("Status"))
            {
                if (!macAddress.Equals(""))
                {
                    //Load the TerminalDetailTab control
                    TerminalStatusTab terminalStatusTab =
                        (TerminalStatusTab)Page.LoadControl("../TerminalStatusTab.ascx");
                    terminalStatusTab.MacAddress = macAddress;
                    control = terminalStatusTab;
                    control.ID = e.PageView.ID + "_userControl";
                }
            }
            else if (e.PageView.ID.Equals("Traffic"))
            {
                if (!macAddress.Equals(""))
                {
                    //Load the TerminalDetailTab control
                    TerminalConsumedTab terminalConsumedTab =
                        (TerminalConsumedTab)Page.LoadControl("../TerminalConsumedTab.ascx");
                    terminalConsumedTab.MacAddress = macAddress;
                    control = terminalConsumedTab;
                    control.ID = e.PageView.ID + "_userControl";
                }
            }
            else if (e.PageView.ID.Equals("Accumulated"))
            {
                //Load the TerminalDetailTab control
                TerminalAccumulatedTab terminalAccumulatedTab =
                    (TerminalAccumulatedTab)Page.LoadControl("../TerminalAccumulatedTab.ascx");
                terminalAccumulatedTab.MacAddress = macAddress;
                control = terminalAccumulatedTab;
                control.ID = e.PageView.ID + "_userControl";
            }
            else
            {
                control = Page.LoadControl("../TestUserControl.ascx");
                control.ID = e.PageView.ID + "_userControl";
            }

            if (control != null)
            {
                e.PageView.Controls.Add(control);
            }
        }

        /// <summary>
        /// Returns a MAC Address for the given sitId
        /// </summary>
        /// <param name="sitId">The StiId of the end user</param>
        /// <param name="ispId">The unique ISP Id</param>
        /// <returns>The MAC address</returns>
        private string getMacAddress(int sitId, int ispId)
        {
            string macAddress = "";
            try
            {
                BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();
                Terminal terminal = boAccountingControl.GetTerminalDetailsBySitId(sitId, ispId);

                if (terminal != null)
                {
                    macAddress = terminal.MacAddress;
                }
            }
            catch (Exception ex)
            {
                BOLogControlWS boLogControlWS = new BOLogControlWS();
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDateTime = DateTime.Now;
                cmtEx.ExceptionDesc = ex.Message;
                cmtEx.UserDescription = "Method: TerminalSearchBySitId.getMacAddress";
                cmtEx.StateInformation = "SitId: " + sitId;
                boLogControlWS.LogApplicationException(cmtEx);
            }
            return macAddress;
        }
    }
}