﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NOCOPMenuAjax.ascx.cs" Inherits="FOWebApp.Nocop.NOCOPMenuAjax" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<telerik:RadTreeView ID="RadTreeViewDistributorMenu" runat="server" 
    onnodeclick="RadTreeViewDistributorMenu_NodeClick" Skin="WebBlue" 
    ValidationGroup="Administrator main menu">
    <Nodes>
        <telerik:RadTreeNode runat="server" Expanded="True" 
            ImageUrl="~/Images/earth_preferences.png" Owner="RadTreeViewDistributorMenu" 
            Text="NOCOP Menu" Font-Bold="True" Value="n_Tasks">
            <Nodes>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/businesspeople.png" 
                    Owner="" Text="Distributors" Value="n_Distributors">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Owner="" Text="New distributor" 
                            Value="n_New_Distributor">
                        </telerik:RadTreeNode>
                     </Nodes>
                </telerik:RadTreeNode>
<telerik:RadTreeNode runat="server" Owner="" Text="Terminals" Value="n_Terminals" 
                    ImageUrl="~/Images/Terminal.png"><Nodes>
<telerik:RadTreeNode runat="server" Owner="" Text="New Terminal" Value="n_New_Terminal"></telerik:RadTreeNode>
        <telerik:RadTreeNode runat="server" Owner="" Text="Search By MAC" 
            Value="n_Search_By_MAC" Visible="False">
        </telerik:RadTreeNode>
        <telerik:RadTreeNode runat="server" Owner="" Text="Search By SitId" 
            Value="n_Search_By_SitId" Visible="False">
        </telerik:RadTreeNode>
         <telerik:RadTreeNode runat="server" Text="Search" 
            Value="n_Advanced_Search">
        </telerik:RadTreeNode>
</Nodes>
</telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/businesspeople.png" 
                    Owner="" Text="Customers" Value="n_Customers">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Owner="" Text="New Customer" 
                            Value="n_New_Customer">
                        </telerik:RadTreeNode>
                        <telerik:RadTreeNode runat="server" Owner="" Text="Delete Customer" 
                            Value="n_Delete_Customer">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/customers.png" 
                    Text="End Users" Value="n_EndUsers">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Text="New End User" Value="n_NewEndUser">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/form_blue.png" Owner="" 
                    Text="Requests" Value="n_Requests">
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/lock_ok.png" Text="Change Password" 
                    ToolTip="Show account information" Value="n_Change_Password">
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/report.png" 
                    Text="Reports" Value="n_Reports">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Text="Activations" 
                            Value="n_Reports_Activations">
                        </telerik:RadTreeNode>
                        <telerik:RadTreeNode runat="server" Text="Accounting" 
                            Value="n_Reports_Accounting">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/exit.png" Text="Log Off" 
                    ToolTip="Close the session" Value="n_log_off">
                </telerik:RadTreeNode>
            </Nodes>
        </telerik:RadTreeNode>
        <telerik:RadTreeNode runat="server" ImageUrl="~/Images/information.png" 
            Text="About" Value="n_About">
        </telerik:RadTreeNode>
    </Nodes>
</telerik:RadTreeView>