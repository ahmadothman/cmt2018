﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18051
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This source code was auto-generated by Microsoft.VSDesigner, Version 4.0.30319.18051.
// 
#pragma warning disable 1591

namespace FOWebApp.net.nimera.cmt.BOServicesControllerWSRef {
    using System;
    using System.Web.Services;
    using System.Diagnostics;
    using System.Web.Services.Protocols;
    using System.Xml.Serialization;
    using System.ComponentModel;
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.17929")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name="BOServicesControllerWSSoap", Namespace="http://tempuri.org/")]
    public partial class BOServicesControllerWS : System.Web.Services.Protocols.SoapHttpClientProtocol {
        
        private System.Threading.SendOrPostCallback DeleteServiceOperationCompleted;
        
        private System.Threading.SendOrPostCallback GetParentOperationCompleted;
        
        private System.Threading.SendOrPostCallback GetServicesForVNOOperationCompleted;
        
        private System.Threading.SendOrPostCallback GetSiblingsOperationCompleted;
        
        private System.Threading.SendOrPostCallback LinkServiceToVNOOperationCompleted;
        
        private System.Threading.SendOrPostCallback RemoveServiceFromVNOOperationCompleted;
        
        private System.Threading.SendOrPostCallback UpdateServiceOperationCompleted;
        
        private System.Threading.SendOrPostCallback GetServiceOperationCompleted;
        
        private System.Threading.SendOrPostCallback GetAllAvailableServicesOperationCompleted;
        
        private System.Threading.SendOrPostCallback GetChildrenOperationCompleted;
        
        private System.Threading.SendOrPostCallback GetIconOperationCompleted;
        
        private bool useDefaultCredentialsSetExplicitly;
        
        /// <remarks/>
        public BOServicesControllerWS() {
            this.Url = global::FOWebApp.Properties.Settings.Default.FOWebApp_net_nimera_cmt_BOServicesControllerWSRef_BOServicesControllerWS;
            if ((this.IsLocalFileSystemWebService(this.Url) == true)) {
                this.UseDefaultCredentials = true;
                this.useDefaultCredentialsSetExplicitly = false;
            }
            else {
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        public new string Url {
            get {
                return base.Url;
            }
            set {
                if ((((this.IsLocalFileSystemWebService(base.Url) == true) 
                            && (this.useDefaultCredentialsSetExplicitly == false)) 
                            && (this.IsLocalFileSystemWebService(value) == false))) {
                    base.UseDefaultCredentials = false;
                }
                base.Url = value;
            }
        }
        
        public new bool UseDefaultCredentials {
            get {
                return base.UseDefaultCredentials;
            }
            set {
                base.UseDefaultCredentials = value;
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        /// <remarks/>
        public event DeleteServiceCompletedEventHandler DeleteServiceCompleted;
        
        /// <remarks/>
        public event GetParentCompletedEventHandler GetParentCompleted;
        
        /// <remarks/>
        public event GetServicesForVNOCompletedEventHandler GetServicesForVNOCompleted;
        
        /// <remarks/>
        public event GetSiblingsCompletedEventHandler GetSiblingsCompleted;
        
        /// <remarks/>
        public event LinkServiceToVNOCompletedEventHandler LinkServiceToVNOCompleted;
        
        /// <remarks/>
        public event RemoveServiceFromVNOCompletedEventHandler RemoveServiceFromVNOCompleted;
        
        /// <remarks/>
        public event UpdateServiceCompletedEventHandler UpdateServiceCompleted;
        
        /// <remarks/>
        public event GetServiceCompletedEventHandler GetServiceCompleted;
        
        /// <remarks/>
        public event GetAllAvailableServicesCompletedEventHandler GetAllAvailableServicesCompleted;
        
        /// <remarks/>
        public event GetChildrenCompletedEventHandler GetChildrenCompleted;
        
        /// <remarks/>
        public event GetIconCompletedEventHandler GetIconCompleted;
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/DeleteService", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public bool DeleteService(Service service) {
            object[] results = this.Invoke("DeleteService", new object[] {
                        service});
            return ((bool)(results[0]));
        }
        
        /// <remarks/>
        public void DeleteServiceAsync(Service service) {
            this.DeleteServiceAsync(service, null);
        }
        
        /// <remarks/>
        public void DeleteServiceAsync(Service service, object userState) {
            if ((this.DeleteServiceOperationCompleted == null)) {
                this.DeleteServiceOperationCompleted = new System.Threading.SendOrPostCallback(this.OnDeleteServiceOperationCompleted);
            }
            this.InvokeAsync("DeleteService", new object[] {
                        service}, this.DeleteServiceOperationCompleted, userState);
        }
        
        private void OnDeleteServiceOperationCompleted(object arg) {
            if ((this.DeleteServiceCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.DeleteServiceCompleted(this, new DeleteServiceCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/GetParent", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Service GetParent(Service service) {
            object[] results = this.Invoke("GetParent", new object[] {
                        service});
            return ((Service)(results[0]));
        }
        
        /// <remarks/>
        public void GetParentAsync(Service service) {
            this.GetParentAsync(service, null);
        }
        
        /// <remarks/>
        public void GetParentAsync(Service service, object userState) {
            if ((this.GetParentOperationCompleted == null)) {
                this.GetParentOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetParentOperationCompleted);
            }
            this.InvokeAsync("GetParent", new object[] {
                        service}, this.GetParentOperationCompleted, userState);
        }
        
        private void OnGetParentOperationCompleted(object arg) {
            if ((this.GetParentCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GetParentCompleted(this, new GetParentCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/GetServicesForVNO", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Service[] GetServicesForVNO(int VNOId) {
            object[] results = this.Invoke("GetServicesForVNO", new object[] {
                        VNOId});
            return ((Service[])(results[0]));
        }
        
        /// <remarks/>
        public void GetServicesForVNOAsync(int VNOId) {
            this.GetServicesForVNOAsync(VNOId, null);
        }
        
        /// <remarks/>
        public void GetServicesForVNOAsync(int VNOId, object userState) {
            if ((this.GetServicesForVNOOperationCompleted == null)) {
                this.GetServicesForVNOOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetServicesForVNOOperationCompleted);
            }
            this.InvokeAsync("GetServicesForVNO", new object[] {
                        VNOId}, this.GetServicesForVNOOperationCompleted, userState);
        }
        
        private void OnGetServicesForVNOOperationCompleted(object arg) {
            if ((this.GetServicesForVNOCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GetServicesForVNOCompleted(this, new GetServicesForVNOCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/GetSiblings", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Service[] GetSiblings(Service service) {
            object[] results = this.Invoke("GetSiblings", new object[] {
                        service});
            return ((Service[])(results[0]));
        }
        
        /// <remarks/>
        public void GetSiblingsAsync(Service service) {
            this.GetSiblingsAsync(service, null);
        }
        
        /// <remarks/>
        public void GetSiblingsAsync(Service service, object userState) {
            if ((this.GetSiblingsOperationCompleted == null)) {
                this.GetSiblingsOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetSiblingsOperationCompleted);
            }
            this.InvokeAsync("GetSiblings", new object[] {
                        service}, this.GetSiblingsOperationCompleted, userState);
        }
        
        private void OnGetSiblingsOperationCompleted(object arg) {
            if ((this.GetSiblingsCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GetSiblingsCompleted(this, new GetSiblingsCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/LinkServiceToVNO", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public bool LinkServiceToVNO(int VNOId, string ServiceId) {
            object[] results = this.Invoke("LinkServiceToVNO", new object[] {
                        VNOId,
                        ServiceId});
            return ((bool)(results[0]));
        }
        
        /// <remarks/>
        public void LinkServiceToVNOAsync(int VNOId, string ServiceId) {
            this.LinkServiceToVNOAsync(VNOId, ServiceId, null);
        }
        
        /// <remarks/>
        public void LinkServiceToVNOAsync(int VNOId, string ServiceId, object userState) {
            if ((this.LinkServiceToVNOOperationCompleted == null)) {
                this.LinkServiceToVNOOperationCompleted = new System.Threading.SendOrPostCallback(this.OnLinkServiceToVNOOperationCompleted);
            }
            this.InvokeAsync("LinkServiceToVNO", new object[] {
                        VNOId,
                        ServiceId}, this.LinkServiceToVNOOperationCompleted, userState);
        }
        
        private void OnLinkServiceToVNOOperationCompleted(object arg) {
            if ((this.LinkServiceToVNOCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.LinkServiceToVNOCompleted(this, new LinkServiceToVNOCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/RemoveServiceFromVNO", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public bool RemoveServiceFromVNO(int VNOId, string ServiceId) {
            object[] results = this.Invoke("RemoveServiceFromVNO", new object[] {
                        VNOId,
                        ServiceId});
            return ((bool)(results[0]));
        }
        
        /// <remarks/>
        public void RemoveServiceFromVNOAsync(int VNOId, string ServiceId) {
            this.RemoveServiceFromVNOAsync(VNOId, ServiceId, null);
        }
        
        /// <remarks/>
        public void RemoveServiceFromVNOAsync(int VNOId, string ServiceId, object userState) {
            if ((this.RemoveServiceFromVNOOperationCompleted == null)) {
                this.RemoveServiceFromVNOOperationCompleted = new System.Threading.SendOrPostCallback(this.OnRemoveServiceFromVNOOperationCompleted);
            }
            this.InvokeAsync("RemoveServiceFromVNO", new object[] {
                        VNOId,
                        ServiceId}, this.RemoveServiceFromVNOOperationCompleted, userState);
        }
        
        private void OnRemoveServiceFromVNOOperationCompleted(object arg) {
            if ((this.RemoveServiceFromVNOCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.RemoveServiceFromVNOCompleted(this, new RemoveServiceFromVNOCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/UpdateService", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public bool UpdateService(Service service) {
            object[] results = this.Invoke("UpdateService", new object[] {
                        service});
            return ((bool)(results[0]));
        }
        
        /// <remarks/>
        public void UpdateServiceAsync(Service service) {
            this.UpdateServiceAsync(service, null);
        }
        
        /// <remarks/>
        public void UpdateServiceAsync(Service service, object userState) {
            if ((this.UpdateServiceOperationCompleted == null)) {
                this.UpdateServiceOperationCompleted = new System.Threading.SendOrPostCallback(this.OnUpdateServiceOperationCompleted);
            }
            this.InvokeAsync("UpdateService", new object[] {
                        service}, this.UpdateServiceOperationCompleted, userState);
        }
        
        private void OnUpdateServiceOperationCompleted(object arg) {
            if ((this.UpdateServiceCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.UpdateServiceCompleted(this, new UpdateServiceCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/GetService", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Service GetService(string ServiceId) {
            object[] results = this.Invoke("GetService", new object[] {
                        ServiceId});
            return ((Service)(results[0]));
        }
        
        /// <remarks/>
        public void GetServiceAsync(string ServiceId) {
            this.GetServiceAsync(ServiceId, null);
        }
        
        /// <remarks/>
        public void GetServiceAsync(string ServiceId, object userState) {
            if ((this.GetServiceOperationCompleted == null)) {
                this.GetServiceOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetServiceOperationCompleted);
            }
            this.InvokeAsync("GetService", new object[] {
                        ServiceId}, this.GetServiceOperationCompleted, userState);
        }
        
        private void OnGetServiceOperationCompleted(object arg) {
            if ((this.GetServiceCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GetServiceCompleted(this, new GetServiceCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/GetAllAvailableServices", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Service[] GetAllAvailableServices(string level) {
            object[] results = this.Invoke("GetAllAvailableServices", new object[] {
                        level});
            return ((Service[])(results[0]));
        }
        
        /// <remarks/>
        public void GetAllAvailableServicesAsync(string level) {
            this.GetAllAvailableServicesAsync(level, null);
        }
        
        /// <remarks/>
        public void GetAllAvailableServicesAsync(string level, object userState) {
            if ((this.GetAllAvailableServicesOperationCompleted == null)) {
                this.GetAllAvailableServicesOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetAllAvailableServicesOperationCompleted);
            }
            this.InvokeAsync("GetAllAvailableServices", new object[] {
                        level}, this.GetAllAvailableServicesOperationCompleted, userState);
        }
        
        private void OnGetAllAvailableServicesOperationCompleted(object arg) {
            if ((this.GetAllAvailableServicesCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GetAllAvailableServicesCompleted(this, new GetAllAvailableServicesCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/GetChildren", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Service[] GetChildren(Service service) {
            object[] results = this.Invoke("GetChildren", new object[] {
                        service});
            return ((Service[])(results[0]));
        }
        
        /// <remarks/>
        public void GetChildrenAsync(Service service) {
            this.GetChildrenAsync(service, null);
        }
        
        /// <remarks/>
        public void GetChildrenAsync(Service service, object userState) {
            if ((this.GetChildrenOperationCompleted == null)) {
                this.GetChildrenOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetChildrenOperationCompleted);
            }
            this.InvokeAsync("GetChildren", new object[] {
                        service}, this.GetChildrenOperationCompleted, userState);
        }
        
        private void OnGetChildrenOperationCompleted(object arg) {
            if ((this.GetChildrenCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GetChildrenCompleted(this, new GetChildrenCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/GetIcon", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        [return: System.Xml.Serialization.XmlElementAttribute(DataType="base64Binary")]
        public byte[] GetIcon(string ServiceId) {
            object[] results = this.Invoke("GetIcon", new object[] {
                        ServiceId});
            return ((byte[])(results[0]));
        }
        
        /// <remarks/>
        public void GetIconAsync(string ServiceId) {
            this.GetIconAsync(ServiceId, null);
        }
        
        /// <remarks/>
        public void GetIconAsync(string ServiceId, object userState) {
            if ((this.GetIconOperationCompleted == null)) {
                this.GetIconOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetIconOperationCompleted);
            }
            this.InvokeAsync("GetIcon", new object[] {
                        ServiceId}, this.GetIconOperationCompleted, userState);
        }
        
        private void OnGetIconOperationCompleted(object arg) {
            if ((this.GetIconCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GetIconCompleted(this, new GetIconCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        public new void CancelAsync(object userState) {
            base.CancelAsync(userState);
        }
        
        private bool IsLocalFileSystemWebService(string url) {
            if (((url == null) 
                        || (url == string.Empty))) {
                return false;
            }
            System.Uri wsUri = new System.Uri(url);
            if (((wsUri.Port >= 1024) 
                        && (string.Compare(wsUri.Host, "localHost", System.StringComparison.OrdinalIgnoreCase) == 0))) {
                return true;
            }
            return false;
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.18058")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://tempuri.org/")]
    public partial class Service {
        
        private System.Guid serviceIdField;
        
        private string serviceNameField;
        
        private string serviceDescriptionField;
        
        private string serviceControllerField;
        
        private int levelField;
        
        private string parentServiceIdField;
        
        private bool deletedFlagField;
        
        private string iconField;
        
        /// <remarks/>
        public System.Guid ServiceId {
            get {
                return this.serviceIdField;
            }
            set {
                this.serviceIdField = value;
            }
        }
        
        /// <remarks/>
        public string ServiceName {
            get {
                return this.serviceNameField;
            }
            set {
                this.serviceNameField = value;
            }
        }
        
        /// <remarks/>
        public string ServiceDescription {
            get {
                return this.serviceDescriptionField;
            }
            set {
                this.serviceDescriptionField = value;
            }
        }
        
        /// <remarks/>
        public string ServiceController {
            get {
                return this.serviceControllerField;
            }
            set {
                this.serviceControllerField = value;
            }
        }
        
        /// <remarks/>
        public int Level {
            get {
                return this.levelField;
            }
            set {
                this.levelField = value;
            }
        }
        
        /// <remarks/>
        public string ParentServiceId {
            get {
                return this.parentServiceIdField;
            }
            set {
                this.parentServiceIdField = value;
            }
        }
        
        /// <remarks/>
        public bool DeletedFlag {
            get {
                return this.deletedFlagField;
            }
            set {
                this.deletedFlagField = value;
            }
        }
        
        /// <remarks/>
        public string Icon {
            get {
                return this.iconField;
            }
            set {
                this.iconField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.17929")]
    public delegate void DeleteServiceCompletedEventHandler(object sender, DeleteServiceCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.17929")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class DeleteServiceCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal DeleteServiceCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public bool Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((bool)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.17929")]
    public delegate void GetParentCompletedEventHandler(object sender, GetParentCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.17929")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class GetParentCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal GetParentCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public Service Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((Service)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.17929")]
    public delegate void GetServicesForVNOCompletedEventHandler(object sender, GetServicesForVNOCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.17929")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class GetServicesForVNOCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal GetServicesForVNOCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public Service[] Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((Service[])(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.17929")]
    public delegate void GetSiblingsCompletedEventHandler(object sender, GetSiblingsCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.17929")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class GetSiblingsCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal GetSiblingsCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public Service[] Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((Service[])(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.17929")]
    public delegate void LinkServiceToVNOCompletedEventHandler(object sender, LinkServiceToVNOCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.17929")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class LinkServiceToVNOCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal LinkServiceToVNOCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public bool Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((bool)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.17929")]
    public delegate void RemoveServiceFromVNOCompletedEventHandler(object sender, RemoveServiceFromVNOCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.17929")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class RemoveServiceFromVNOCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal RemoveServiceFromVNOCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public bool Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((bool)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.17929")]
    public delegate void UpdateServiceCompletedEventHandler(object sender, UpdateServiceCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.17929")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class UpdateServiceCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal UpdateServiceCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public bool Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((bool)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.17929")]
    public delegate void GetServiceCompletedEventHandler(object sender, GetServiceCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.17929")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class GetServiceCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal GetServiceCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public Service Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((Service)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.17929")]
    public delegate void GetAllAvailableServicesCompletedEventHandler(object sender, GetAllAvailableServicesCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.17929")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class GetAllAvailableServicesCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal GetAllAvailableServicesCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public Service[] Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((Service[])(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.17929")]
    public delegate void GetChildrenCompletedEventHandler(object sender, GetChildrenCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.17929")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class GetChildrenCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal GetChildrenCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public Service[] Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((Service[])(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.17929")]
    public delegate void GetIconCompletedEventHandler(object sender, GetIconCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.17929")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class GetIconCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal GetIconCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public byte[] Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((byte[])(this.results[0]));
            }
        }
    }
}

#pragma warning restore 1591