﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TerminalInfoView.ascx.cs"
    Inherits="FOWebApp.TerminalInfoView" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>
<style type="text/css">
    .style1
    {
        width: 221px;
    }
</style>
<div id="SearchDiv">
    <table>
        <tr>
            <td>
                <asp:Label ID="LabelValue" runat="server" Text="Mac Address:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxValue" runat="server"></asp:TextBox>
            </td>
            <td>
                <asp:Button ID="ButtonSearch" runat="server" Text="Search" OnClick="ButtonSearch_Click" />
            </td>
            <td>
                <asp:UpdateProgress runat="server" ID="updateTerminalInfoRead">
                    <ProgressTemplate>
                        <img src="Images/ajax-loader.gif" alt="Contacting Server..." />
                    </ProgressTemplate>
                </asp:UpdateProgress>
            </td>
            <td>
                <!-- Validations -->
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBoxValue"
                    ErrorMessage="Please enter a valid value" Display="Dynamic">
                </asp:RequiredFieldValidator>
            </td>
        </tr>
    </table>
    <hr />
</div>
<asp:Panel ID="PanelResult" runat="server" Visible="False">
    <table border="0">
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Text="User Name:"></asp:Label>
            </td>
            <td class="style1">
                <asp:Label ID="LabelSiteName" runat="server" Width="500px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label2" runat="server" Text="SIT ID:"></asp:Label>
            </td>
            <td class="style1">
                <asp:Label ID="LabelSiteId" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label9" runat="server" Text="MAC Address:"></asp:Label>
            </td>
            <td class="style1">
                <asp:Label ID="LabelMacAddress" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label11" runat="server" Text="IP Address:"></asp:Label>
            </td>
            <td class="style1">
                <asp:Label ID="LabelIPAddress" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label3" runat="server" Text="Service Package:"></asp:Label>
            </td>
            <td class="style1">
                <asp:Label ID="LabelServicePackage" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label4" runat="server" Text="Terminal status:"></asp:Label>
            </td>
            <td class="style1">
                <asp:Label ID="LabelStatus" runat="server" Font-Bold="True" ToolTip="If Not operational, this may result from bad pointing, loose connections or other problems on the local or satellite network."></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label7" runat="server" Text="Volume Allocation:"></asp:Label>
            </td>
            <td class="style1">
                <asp:Label ID="LabelVolumeAllocation" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label5" runat="server" Text="Volume Consumed:"></asp:Label>
            </td>
            <td class="style1">
                <asp:Label ID="LabelFConsumed" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label8" runat="server" Text="FUP Reset Day:"></asp:Label>
            </td>
            <td class="style1">
                <asp:Label ID="LabelResetDayOfMOnth" runat="server" ToolTip="Day of the month on which volume reset takes place."
                    Font-Bold="True" ForeColor="Green"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label10" runat="server" Text="Terminal pointing:"></asp:Label>
            </td>
            <td class="style1">
                <asp:Label ID="LabelRTN" runat="server" Font-Bold="True" ToolTip="Above 58.4 dBHz, the terminal uses the most efficient return pool. Between 55.2 dBHz and 58.4 dBHz, the terminal must use a less efficient return pool. Below 55.2 dBHz the signal is too weak."></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label6" runat="server" Text="Error message:"></asp:Label>
            </td>
            <td class="style1">
                <asp:Label ID="LabelError" runat="server" Width="500px" Font-Bold="True" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    <!-- Here comes the graph -->
    <asp:Chart ID="ChartVolume" runat="server">
        <Series>
        </Series>
        <ChartAreas>
            <asp:ChartArea Name="ChartAreaVolume">
            </asp:ChartArea>
        </ChartAreas>
    </asp:Chart>
</asp:Panel>
