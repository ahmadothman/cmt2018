﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;

namespace FOWebApp
{
    public partial class TerminalActivityTab : System.Web.UI.UserControl
    {
        BOLogControlWS _boLogControl = null;

        /// <summary>
        /// The ISP Identifier
        /// </summary>
        public int IspId
        {
            get
            {
                return Int32.Parse(HiddenFieldIspId.Value);
            }

            set
            {
                HiddenFieldIspId.Value = value.ToString();
            }
        }


        /// <summary>
        /// The Site Identifier
        /// </summary>
        public int SitId 
        {
            get
            {
                return Int32.Parse(HiddenFieldSitId.Value);
            }

            set
            {
                HiddenFieldSitId.Value = value.ToString();
            }
        }

        /// <summary>
        /// The height of the table
        /// </summary>
        public int Height 
        {
            get
            {
                return Int32.Parse(HiddenFieldHeight.Value);
            }

            set
            {
                HiddenFieldHeight.Value = value.ToString();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _boLogControl = new BOLogControlWS();
            RadGridActivityList.Height = Height;
        }

        protected void RadGridActivityList_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            TerminalActivityExt[] terminalActivities = _boLogControl.GetTerminalActivityBySitId(SitId, IspId);
            RadGridActivityList.DataSource = terminalActivities;
        }


        protected void RadGridActivityList_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToCsvCommandName)
            {
                /*RadGridActivityList.ExportSettings.FileName = "TermActivities_" + SitId + "_" + IspId;
                RadGridActivityList.ExportSettings.ExportOnlyData = true;
                RadGridActivityList.ExportSettings.IgnorePaging = true;
                RadGridActivityList.ExportSettings.OpenInNewWindow = true;
                RadGridActivityList.ExportSettings.Csv.ColumnDelimiter = GridCsvDelimiter.Semicolon;
                RadGridActivityList.ExportSettings.Csv.EncloseDataWithQuotes = false;
                RadGridActivityList.ExportSettings.Csv.RowDelimiter = GridCsvDelimiter.NewLine;
                RadGridActivityList.MasterTableView.ExportToCSV();*/

                StringBuilder outText = new StringBuilder();

                TerminalActivityExt[] terminalActivities = _boLogControl.GetTerminalActivityBySitId(SitId, IspId);
                foreach(TerminalActivityExt termActivitiesExt in terminalActivities)
                {
                    outText.AppendLine("Test Text");
                   
                }
                Response.Clear();
                Response.AddHeader("content-disposition", "attachment; filename=" + "test.csv");
                Response.AddHeader("content-length", outText.Length.ToString());
                Response.ContentType = "text/plain";
                TextWriter writer = Response.Output;
                writer.Write(outText.ToString());
                Response.Flush();
                Response.End();
            }
        }


    }
}