﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TerminalSearchByMac.ascx.cs"
    Inherits="FOWebApp.TerminalSearchByMac" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script type="text/javascript">
    function onTabSelecting(sender, args) {
        if (args.get_tab().get_pageViewID()) {
            args.get_tab().set_postBack(false);
        }
    }
</script>
<telerik:RadAjaxPanel ID="RadAjaxPanelTerminalSearchByMac" runat="server" Height="200px" Width="600px" LoadingPanelID="RadAjaxLoadingPanel1">
<div id="searchDiv">
    <table>
        <tr>
            <td>
                <asp:Label ID="LabelValue" runat="server" Text="Mac Address:"></asp:Label>
            </td>
            <td>
                <telerik:RadMaskedTextBox ID="RadMaskedTextBoxMacAddress" runat="server" Skin="WebBlue">
                </telerik:RadMaskedTextBox>
            </td>
            <td>
                <asp:Button ID="ButtonSearch" runat="server" Text="Search" OnClick="ButtonSearch_Click" />
            </td>
            <td>
                <!-- Validations -->
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="RadMaskedTextBoxMacAddress"
                    ErrorMessage="Please enter a valid MAC Address" Display="Dynamic">
                </asp:RequiredFieldValidator>
            </td>
        </tr>
    </table>
</div>
<br />
<!-- Terminal view Rad panel -->
<telerik:RadTabStrip ID="RadTabStripTerminalView" runat="server" 
    MultiPageID="RadMultiPageTerminalView" Skin="WebBlue" 
    SelectedIndex="4" OnClientTabSelecting="onTabSelecting" 
    ontabclick="RadTabStripTerminalView_TabClick">
    <Tabs>
        <telerik:RadTab runat="server" Text="Terminal" Selected="true">
        </telerik:RadTab>
        <telerik:RadTab runat="server" Text="Status">
        </telerik:RadTab>
        <telerik:RadTab runat="server" Text="Traffic">
        </telerik:RadTab>
         <telerik:RadTab runat="server" Text="Accumulated">
        </telerik:RadTab>
        <telerik:RadTab ID="RadTabManagement" runat="server" Text="Management" Visible="false">
        </telerik:RadTab>
    </Tabs>
</telerik:RadTabStrip>
<telerik:RadMultiPage ID="RadMultiPageTerminalView" runat="server" 
    BorderColor="#CCCCCC" BorderStyle="Solid" BorderWidth="1px" Height="550px" 
    Width="900px" ScrollBars="Auto" SelectedIndex="0" OnPageViewCreated="RadMultiPageTerminalView_PageViewCreated">
</telerik:RadMultiPage>
</telerik:RadAjaxPanel>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Default" Width="750" Height="550">
</telerik:RadAjaxLoadingPanel>




