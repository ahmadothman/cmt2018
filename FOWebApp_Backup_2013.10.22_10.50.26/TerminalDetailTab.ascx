﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TerminalDetailTab.ascx.cs"
    Inherits="FOWebApp.TerminalDetailTab" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script type="text/jscript">
    function showReport() {
        alert("showReport function executed");
        return false;
    }
</script>
<asp:Panel ID="PanelTerminalDetails" runat="server" GroupingText="Terminal details">
<table border="0" cellspacing="5px" class="termDetails">
    <tr>
        <td>
            <asp:Label ID="Label1" runat="server" Text="Terminal:" Width="200px"></asp:Label>
        </td>
        <td class="style1">
            <asp:Label ID="LabelSiteName" runat="server" Width="300px"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="Label2" runat="server" Text="SIT ID:"></asp:Label>
        </td>
        <td class="style1">
            <asp:Label ID="LabelSiteId" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="Label18" runat="server" Text="ISP:"></asp:Label>
        </td>
        <td class="style1">
            <asp:Label ID="LabelISP" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="Label9" runat="server" Text="MAC Address:"></asp:Label>
        </td>
        <td class="style1">
            <asp:Label ID="LabelMacAddress" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="Label11" runat="server" Text="IP Address:"></asp:Label>
        </td>
        <td class="style1">
            <asp:Label ID="LabelIPAddress" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="Label3" runat="server" Text="SLA:"></asp:Label>
        </td>
        <td class="style1">
            <asp:Label ID="LabelServicePackage" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="Label4" runat="server" Text="Operational status:"></asp:Label>
        </td>
        <td class="style1">
            <asp:Label ID="LabelStatus" runat="server" Font-Bold="True" ToolTip="If Not operational, this may result from bad pointing, loose connections or other problems on the local or satellite network."></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="Label15" runat="server" Text="Administrative status:"></asp:Label>
        </td>
        <td class="style1">
            <asp:Label ID="LabelAdmStatus" runat="server" Font-Bold="True" ToolTip="Indicates the administrative status of a terminal (locked, un-locked etc.)"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="Label7" runat="server" Text="Forward Volume Allocation:"></asp:Label>
        </td>
        <td class="style1">
            <asp:Label ID="LabelVolumeAllocation" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="Label17" runat="server" Text="Return Volume Allocation:"></asp:Label>
        </td>
        <td class="style1">
            <asp:Label ID="LabelReturnVolumeAllocation" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="Label5" runat="server" Text="Forward Volume Consumed:"></asp:Label>
        </td>
        <td class="style1">
            <asp:Label ID="LabelFConsumed" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="Label14" runat="server" Text="Return Volume Consumed:"></asp:Label>
        </td>
        <td class="style1">
            <asp:Label ID="LabelRTNConsumed" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="Label19" runat="server" Text="Forward Volume Remaining:" Visible="false"></asp:Label>
        </td>
        <td class="style1">
            <asp:Label ID="LabelFWDRemaining" runat="server" Visible="false"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="Label21" runat="server" Text="Return Volume Remaining:" Visible="false"></asp:Label>
        </td>
        <td class="style1">
            <asp:Label ID="LabelRTNRemaining" runat="server" Visible="false"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="Label8" runat="server" Text="FUP Reset Day:"></asp:Label>
        </td>
        <td class="style1">
            <asp:Label ID="LabelResetDayOfMOnth" runat="server" ToolTip="Day of the month on which volume reset takes place."
                Font-Bold="True" ForeColor="Green"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="Label10" runat="server" Text="Terminal pointing:"></asp:Label>
        </td>
        <td class="style1">
            <asp:Label ID="LabelRTN" runat="server" Font-Bold="True"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="Label12" runat="server" Text="Expiry Date:"></asp:Label>
        </td>
        <td class="style1">
            <asp:Label ID="LabelExpiryDate" runat="server" Width="300px"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="Label6" runat="server" Text="Error message:"></asp:Label>
        </td>
        <td class="style1">
            <asp:Label ID="LabelError" runat="server" Width="300px" Font-Bold="True" ForeColor="Red"></asp:Label>
        </td>
    </tr>
</table>
</asp:Panel>
<asp:Panel ID="PanelFreeZone" runat="server" Visible="False" BackColor="#F1FEF3" GroupingText="Freezone details">
<table border="0" cellspacing="5px" class="termDetails">
    <tr>
        <td width="200px">
            <asp:Label ID="Label13" runat="server" Text="Forward Freezone Consumed:" Visible="true"></asp:Label>
        </td>
        <td class="style1">
            <asp:Label ID="LabelFreeZoneVolume" runat="server" Visible="true"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="Label16" runat="server" Text="Return Freezone Consumed:" Visible="true"></asp:Label>
        </td>
        <td class="style1">
            <asp:Label ID="LabelRTNFreeZoneVolume" runat="server" Visible="true"></asp:Label>
        </td>
    </tr>
</table>
</asp:Panel>

