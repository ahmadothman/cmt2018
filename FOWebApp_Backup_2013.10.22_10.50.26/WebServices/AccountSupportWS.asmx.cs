﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;

namespace FOWebApp.WebServices
{
    /// <summary>
    /// Summary description for AccountSupportWS
    /// </summary>
    [WebService(Namespace = "http://nimera.net/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class AccountSupportWS : System.Web.Services.WebService
    {
        /// <summary>
        /// Check if the given MAC address is true
        /// </summary>
        /// <param name="macAddress"></param>
        /// <returns></returns>
        [WebMethod]
        public Boolean IsMacAddressUnique(string macAddress)
        {
            Boolean isUnique = false;

            BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();
            Terminal term = boAccountingControl.GetTerminalDetailsByMAC(macAddress);

            if (term != null)
            {
                //Terminal already exists, clear the mac address field and show an error
                //window
                isUnique = false;
            }
            else
            {
                isUnique = true;
            }
            return isUnique;
        }

        /// <summary>
        /// Adds an ISP to a distributor
        /// </summary>
        /// <remarks>
        /// This method calls the parallel method of the BOAccountingControl website
        /// </remarks>
        /// <param name="distributorId">The distributor identifier</param>
        /// <param name="ispId">The ISP identifier</param>
        /// <returns>True if the ISP was successfully added to the Distributor</returns>
        [WebMethod]
        public bool AddISPToDistributor(string distributorId, string ispId)
        {
            BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();
            return boAccountingControl.AddISPToDistributor(Int32.Parse(distributorId), Int32.Parse(ispId));
        }

        /// <summary>
        /// This method de-allocates an ISP from a distributor
        /// </summary>
        /// <remarks>
        /// This method calls the parallel method of the BOAccountingControl website
        /// </remarks>
        /// <param name="distributorId">The distributor identifier</param>
        /// <param name="ispId">The ISP identifier</param>
        /// <returns>True if the de-allocation succeeded</returns>
        [WebMethod]
        public bool RemoveISPFromDistributor(string distributorId, string ispId)
        {
            BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();
            return boAccountingControl.RemoveISPFromDistributor(Int32.Parse(distributorId), Int32.Parse(ispId));
        }
    
    }
}
