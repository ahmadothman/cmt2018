﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FOWebApp.net.nimera.cmt.BOInvoiceControllerWSRef;

namespace FOWebApp
{
    public partial class Invoice : System.Web.UI.UserControl
    {
        /// <summary>
        /// The DistributorId
        /// </summary>
        public int DistributorId { get; set; }

        /// <summary>
        /// The invoice year
        /// </summary>
        public int InvoiceYear { get; set; }

        /// <summary>
        /// The invoice month
        /// </summary>
        public int InvoiceMonth { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            BOInvoiceController boInvoiceController = new BOInvoiceController();

            InvoiceHeader[] invList = boInvoiceController.GetInvoicesForDistributor(DistributorId, InvoiceYear, InvoiceMonth);
            if (invList.Length != 0)
            {
                InvoiceHeader invHeader = invList[0];
                InvoiceLine[] invLines = boInvoiceController.GetInvoiceLines(InvoiceYear, invHeader.InvoiceNumber);

                //Complete the form
                LabelInvoiceNumber.Text = invHeader.InvoiceMonth + "-" + invHeader.InvoiceYear + "-" + invHeader.InvoiceNumber;
                LabelInvoiceDate.Text = invHeader.InvoiceDate.ToShortDateString();
                LabelDistributorName.Text = invHeader.Distributor.FullName;
                LabelDistributorAddressLine1.Text = invHeader.Distributor.Address.AddressLine1;
                LabelDistributorAddressLine2.Text = invHeader.Distributor.Address.AddressLine2;
                LabelDistributorLocation.Text = invHeader.Distributor.Address.Location;
                LabelDistributorCountry.Text = invHeader.Distributor.Address.Country;
                LabelDistributorPostCode.Text = invHeader.Distributor.Address.PostalCode;

                RadGridInvoiceLines.DataSource = invLines;
                RadGridInvoiceLines.DataBind();

                LabelTotalEUR.Text = "€" + invHeader.InvoiceTotalEUR;
                LabelTotalUSD.Text = "$" + invHeader.InvoiceTotalUSD;
            }
        }
    }
}