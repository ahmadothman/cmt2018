﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TerminalDetail.ascx.cs" Inherits="FOWebApp.TerminalDetail" %>
<asp:Panel ID="PanelTerminalDetails" runat="server" Visible="True">
    <table border="0">
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Text="User Name:"></asp:Label>
            </td>
            <td class="style1">
                <asp:Label ID="LabelSiteName" runat="server" Width="500px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label2" runat="server" Text="SIT ID:"></asp:Label>
            </td>
            <td class="style1">
                <asp:Label ID="LabelSiteId" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label12" runat="server" Text="ISP:"></asp:Label>
            </td>
            <td class="style1">
                <asp:Label ID="LabelISP" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label9" runat="server" Text="MAC Address:"></asp:Label>
            </td>
            <td class="style1">
                <asp:Label ID="LabelMacAddress" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label11" runat="server" Text="IP Address:"></asp:Label>
            </td>
            <td class="style1">
                <asp:Label ID="LabelIPAddress" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label3" runat="server" Text="SLA:"></asp:Label>
            </td>
            <td class="style1">
                <asp:Label ID="LabelServicePackage" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label4" runat="server" Text="Terminal status:"></asp:Label>
            </td>
            <td class="style1">
                <asp:Label ID="LabelStatus" runat="server" Font-Bold="True" ToolTip="If Not operational, this may result from bad pointing, loose connections or other problems on the local or satellite network."></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label7" runat="server" Text="Volume Allocation:"></asp:Label>
            </td>
            <td class="style1">
                <asp:Label ID="LabelVolumeAllocation" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label5" runat="server" Text="Volume Consumed:"></asp:Label>
            </td>
            <td class="style1">
                <asp:Label ID="LabelFConsumed" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label8" runat="server" Text="FUP Reset Day:"></asp:Label>
            </td>
            <td class="style1">
                <asp:Label ID="LabelResetDayOfMOnth" runat="server" ToolTip="Day of the month on which volume reset takes place."
                    Font-Bold="True" ForeColor="Green"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label10" runat="server" Text="Terminal pointing:"></asp:Label>
            </td>
            <td class="style1">
                <asp:Label ID="LabelRTN" runat="server" Font-Bold="True"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label6" runat="server" Text="Error message:"></asp:Label>
            </td>
            <td class="style1">
                <asp:Label ID="LabelError" runat="server" Width="500px" Font-Bold="True" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    <!-- Here comes the graph -->
    <asp:Chart ID="ChartVolume" runat="server">
        <Series>
        </Series>
        <ChartAreas>
            <asp:ChartArea Name="ChartAreaVolume">
            </asp:ChartArea>
        </ChartAreas>
    </asp:Chart>
</asp:Panel>