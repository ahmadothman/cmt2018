﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OrgMenuAjax.ascx.cs" Inherits="FOWebApp.Organizations.OrgMenuAjax" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<telerik:RadTreeView ID="RadTreeViewOrganizationMenu" runat="server" Skin="WebBlue" 
    ValidationGroup="Organization main menu" 
    onnodeclick="RadTreeViewOrganizationMenu_NodeClick">
    <Nodes>
        <telerik:RadTreeNode runat="server" Expanded="True" 
            ImageUrl="~/Images/earth_preferences.png" Owner="RadTreeViewOrganizationMenu" 
            Text="Organization Menu" Font-Bold="True">
            <Nodes>
<telerik:RadTreeNode runat="server" Owner="" Text="Terminals" Value="n_Terminals" 
                    ImageUrl="~/Images/Terminal.png"><Nodes>
        <telerik:RadTreeNode runat="server" Owner="" Text="Search By MAC" 
            Value="n_Search_By_MAC">
        </telerik:RadTreeNode>
        <telerik:RadTreeNode runat="server" Owner="" Text="Search By SitId" 
            Value="n_Search_By_SitId">
        </telerik:RadTreeNode>
</Nodes>
</telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/Account.png" Text="Account" 
                    ToolTip="Show account information" Value="n_Account">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Text="Change Password" 
                            Value="n_Change_Password">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/exit.png" Text="Log Off" 
                    ToolTip="Close the session" Value="n_log_off">
                </telerik:RadTreeNode>
            </Nodes>
        </telerik:RadTreeNode>
        <telerik:RadTreeNode runat="server" ImageUrl="~/Images/information.png" 
            Text="About" Value="n_About">
        </telerik:RadTreeNode>
    </Nodes>
</telerik:RadTreeView>