﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Drawing;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;

namespace FOWebApp.Organizations
{
    public partial class OrganizationAccount : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Load organization data into fields
            //First get the Organization entity from the database
            BOAccountingControlWS boAccountingControlWS =
                                        new BOAccountingControlWS();

            MembershipUser myObject = Membership.GetUser();
            string userId = myObject.ProviderUserKey.ToString();

            Organization org = boAccountingControlWS.GetOrganizationForUser(userId);

            if (org != null)
            {
                TextBoxName.Text = org.FullName;
                TextBoxAddressLine1.Text = org.Address.AddressLine1;
                TextBoxAddressLine2.Text = org.Address.AddressLine2;
                TextBoxLocation.Text = org.Address.Location;
                TextBoxPCO.Text = org.Address.PostalCode;
                CountryList.SelectedValue = org.Address.Country;
                TextBoxPhone.Text = org.Phone;
                TextBoxRemarks.Text = org.Remarks;
                TextBoxVAT.Text = org.Vat;
                TextBoxWebSite.Text = org.WebSite;
                TextBoxFax.Text = org.Fax;
                TextBoxEMail.Text = org.Email;
            }
        }

        protected void ButtonSubmit_Click(object sender, EventArgs e)
        {
            //Store organization data to database
            BOAccountingControlWS boAccountingControlWS =
                                        new BOAccountingControlWS();

            //First load the "old" organization data
            MembershipUser myObject = Membership.GetUser();
            string userId = myObject.ProviderUserKey.ToString();

            Organization org = boAccountingControlWS.GetOrganizationForUser(userId);

            //Overwrite with the new data
            org.FullName = TextBoxName.Text;
            org.Address.AddressLine1 = TextBoxAddressLine1.Text;
            org.Address.AddressLine2 = TextBoxAddressLine2.Text;
            org.Address.Location = TextBoxLocation.Text;
            org.Address.PostalCode = TextBoxPCO.Text;
            org.Address.Country = CountryList.SelectedValue;
            org.Phone = TextBoxPhone.Text;
            org.Remarks = TextBoxRemarks.Text;
            org.Vat = TextBoxVAT.Text;
            org.WebSite = TextBoxWebSite.Text;
            org.Fax = TextBoxFax.Text;
            org.Email = TextBoxEMail.Text;

            if (boAccountingControlWS.UpdateOrganization(org))
            {
                LabelResult.ForeColor = Color.DarkGreen;
                LabelResult.Text = "Update done successful!";
            }
            else
            {
                LabelResult.ForeColor = Color.Red;
                LabelResult.Text = "Update failed!";
            }
        }
    }
}