﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using Telerik.Web.UI;

namespace FOWebApp.Organizations
{
    public partial class OrgMenuAjax : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //Set the user name as root node of the treeview
                if (HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    string user = HttpContext.Current.User.Identity.Name;
                    RadTreeViewOrganizationMenu.Nodes[0].Text = "Welcome " + user;
                }
            }

            //Load the active component into the Main content placeholder
            RadTreeNode selectedNode = RadTreeViewOrganizationMenu.SelectedNode;

            if (selectedNode != null)
            {
                //Get the MainContentPanel
                Control sheetContainer = scanForControl("SheetContentPlaceHolder", this);

                if (sheetContainer != null)
                {
                    Control contentContainer = sheetContainer.FindControl("MainContentPlaceHolder");

                    if (contentContainer != null)
                    {
                        contentContainer.Controls.Clear();
                        Control contentControl = this.LoadUserControl(selectedNode.Value);
                        if (contentControl != null)
                        {
                            contentControl.ID = selectedNode.Value + "_ID";
                            contentContainer.Controls.Add(contentControl);
                        }
                    }
                }
            }
        }

        protected void RadTreeViewOrganizationMenu_NodeClick(object sender, Telerik.Web.UI.RadTreeNodeEventArgs e)
        {

        }

        /// <summary>
        /// Scans the page for the given controlId starting from the given root
        /// </summary>
        /// <param name="controlId">The controlId to search for</param>
        /// <param name="root">Root to start the search from</param>
        /// <returns>The found Control</returns>
        protected Control scanForControl(string controlId, Control root)
        {
            Control resCtrl = root.FindControl(controlId);

            if (resCtrl == null)
            {
                root = root.Parent;
                if (root != null)
                {
                    resCtrl = this.scanForControl(controlId, root.Parent);
                }
            }

            return resCtrl;
        }

        /// <summary>
        /// Loads the Control specifie d by the nodeSeletor
        /// </summary>
        /// <param name="nodeSelector"></param>
        /// <returns></returns>
        protected Control LoadUserControl(string nodeSelector)
        {
            Control control = null;
            if (nodeSelector.Equals("n_log_off"))
            {
                //Close the session
                Session.Abandon();
                FormsAuthentication.SignOut();
                Response.Redirect("Default.aspx");
            }
            else if (nodeSelector.Equals("n_Terminals"))
            {
                control = Page.LoadControl("~/Organizations/TerminalListForOrganization.ascx"); ;
            }
            else if (nodeSelector.Equals("n_Search_By_MAC"))
            {
                control = Page.LoadControl("TerminalSearchByMac.ascx");
            }
            else if (nodeSelector.Equals("n_Search_By_SitId"))
            {
                control = Page.LoadControl("TerminalSearchBySitId.ascx");
            }
            else if (nodeSelector.Equals("n_About"))
            {
                control = Page.LoadControl("~/Organizations/AboutControl.ascx");
            }
            else if (nodeSelector.Equals("n_Account"))
            {
                control = Page.LoadControl("~/Organizations/OrganizationAccount.ascx");
            }
            else if (nodeSelector.Equals("n_Change_Password"))
            {
                control = Page.LoadControl("ChangePassword.ascx");
            }
            else
            {
                control = Page.LoadControl("BlankControl.ascx");
            }

            return control;
        }
    }
}