﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AboutControl.ascx.cs"
    Inherits="FOWebApp.Organizations.AboutControl" %>
<div class="picturespace">
    <asp:Image ID="Image1" runat="server" Width="300px" ImageUrl="~/Images/LogoSatADSL.jpg" />
</div>
<div class="infospace">
    <div class="art-blockheader"><b>Customer Managment Tool, Organization version.</b></div>
    <p>
        Version:
        <asp:Label ID="LabelVersion" runat="server"></asp:Label>
    </p>
    <p>
        Release date: 26/07/2013
    </p>
    <p>
        SatADSL (c) 2011, 2013
    </p>
    <p>
        For more information about SatADSL and our Services please do not hesitate
        to visit our <a href="http://www.satadsl.net" target="_blank">website</a>.
    </p>
     <p>
        Application developed by <a href="http://www.nimera.net" target="_blank">Nimera Mobile ICT</a>
    </p>
</div>
