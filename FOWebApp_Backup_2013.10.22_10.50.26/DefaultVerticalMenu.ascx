<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="DefaultVerticalMenu.ascx.cs"
    Inherits="VMenu" %>
<%@ Register Src="~/Distributors/DistributorMenuAjax.ascx" TagName="DistributorMenuAjax"
    TagPrefix="uc2" %>
<%@ Register Src="~/Nocsa/NOCSAMenuAjax.ascx" TagName="NOCSAMenuAjax"
    TagPrefix="uc2" %>
<%@ Register Src="~/Nocop/NOCOPMenuAjax.ascx" TagName="NOCOPMenuAjax"
    TagPrefix="uc2" %>
<%@ Register Src="~/Organizations/OrgMenuAjax.ascx" TagName="ORGMenuAjax" 
    TagPrefix="uc2" %>
<div class="art-vmenublock">
    <div class="art-vmenublock-body">
        <div class="art-vmenublockcontent">
            <div class="art-vmenublockcontent-body">
                <%
                    //Add the distributor menu items if the user has the admin role.
                    if (HttpContext.Current.User.IsInRole("Distributor"))
                    {
                %>
                <uc2:DistributorMenuAjax ID="DistributorMenuAjax1" runat="server" />
                <%
                    }
                %>
                <%
                    if (HttpContext.Current.User.IsInRole("NOC Administrator"))
                    {
                
                %>
                <uc2:NOCSAMenuAjax ID="NOCSAMenuAjax2" runat="server" />
                <%
                    }
                %>
                <%
                    if (HttpContext.Current.User.IsInRole("NOC Operator"))
                    {
                
                %>
                <uc2:NOCOPMenuAjax ID="NOCOPMenuAjax1" runat="server" />
                <%
                    }
                %>
                <%
                    if (HttpContext.Current.User.IsInRole("Organization"))
                    {
                
                %>
                <uc2:ORGMenuAjax ID="ORGMenuAjax1" runat="server" />
                <%
                    }
                %>
            </div>
        </div>
    </div>
</div>
<hr />
<br /><br />
<div style="text-align: center">
    <img src="Images/PossitiveSSL_tl_trans.gif" 
        alt="SSL Cerficate Authority" title="SSL Certificate Authority" border="0"/>
</div>