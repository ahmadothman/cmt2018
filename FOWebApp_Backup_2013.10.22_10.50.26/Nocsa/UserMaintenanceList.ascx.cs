﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using Telerik.Web.UI;
using FOWebApp.net.nimera.cmt.BOUserControlWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;

namespace FOWebApp.Nocsa
{
    public partial class UserMaintenanceList : System.Web.UI.UserControl
    {
        BOUserControlWS _boUserControlWS;

        public UserMaintenanceList()
        {
            _boUserControlWS = new BOUserControlWS();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Session["_userMaintenanceListGrid_PageIdx"] != null)
            {
                RadGridUserMaintenanceList.CurrentPageIndex =
                            (int)this.Session["_userMaintenanceListGrid_PageIdx"];
            }
        }

        protected void RadGridUserMaintenanceList_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            //Load the users into the datagrid
            RadGridUserMaintenanceList.DataSource =
                                    Membership.GetAllUsers();
        }

        protected void RadGridUserMaintenanceList_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToExcelCommandName)
            {
                this.ConfigureExport();
                RadGridUserMaintenanceList.MasterTableView.ExportToExcel();
            }

            if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToCsvCommandName)
            {
                this.ConfigureExport();
                RadGridUserMaintenanceList.MasterTableView.ExportToCSV();
            }

            if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToPdfCommandName)
            {
                this.ConfigureExport();
                RadGridUserMaintenanceList.MasterTableView.ExportToPdf();
            }

            if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToWordCommandName)
            {
                this.ConfigureExport();
                RadGridUserMaintenanceList.MasterTableView.ExportToWord();
            }
        }

        protected void RadGridUserMaintenanceList_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            BOLogControlWS boLogControl = new BOLogControlWS();
            GridDataKeyArray dataItems = e.Item.OwnerTableView.DataKeyValues;
            string userId = dataItems[e.Item.ItemIndex]["ProviderUserKey"].ToString();
            string userName = dataItems[e.Item.ItemIndex]["UserName"].ToString();

            try
            {
                _boUserControlWS.DeconnectUser(new Guid(userId));

                //Remove user from the Membership database
                if (!Membership.DeleteUser(userName))
                {
                    
                    CmtApplicationException cmtEx = new CmtApplicationException();
                    cmtEx.ExceptionDesc = "Deleting user: " + userName + " failed";
                    cmtEx.StateInformation = "Delete user failed";
                    boLogControl.LogApplicationException(cmtEx);
                }
                else
                {
                    UserActivity userActivity = new UserActivity();
                    userActivity.Action = "User delete";
                    userActivity.ActionDate = DateTime.Now;
                    userActivity.UserId = new Guid(userId);
                    boLogControl.LogUserActivity(userActivity);
                }

                RadGridUserMaintenanceList.DataSource =
                                    Membership.GetAllUsers();
                RadGridUserMaintenanceList.Rebind();
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDesc = ex.Message;
                cmtEx.ExceptionStacktrace = ex.StackTrace;
                cmtEx.StateInformation = "Delete user failed";
                boLogControl.LogApplicationException(cmtEx);
            }

        }

        protected void RadGridUserMaintenanceList_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            //Save the page index
            int pageIdx = e.NewPageIndex;
            this.Session["_userMaintenanceListGrid_PageIdx"] = pageIdx;
        }

        /// <summary>
        /// Configures the export settings for this radgrid
        /// </summary>
        public void ConfigureExport()
        {
            RadGridUserMaintenanceList.ExportSettings.FileName = "CMTExport";
            RadGridUserMaintenanceList.ExportSettings.ExportOnlyData = true;
            RadGridUserMaintenanceList.ExportSettings.IgnorePaging = true;
            RadGridUserMaintenanceList.ExportSettings.OpenInNewWindow = true;
            RadGridUserMaintenanceList.ExportSettings.Excel.Format = GridExcelExportFormat.Html;
            RadGridUserMaintenanceList.ExportSettings.Csv.ColumnDelimiter = GridCsvDelimiter.Semicolon;
            RadGridUserMaintenanceList.ExportSettings.Csv.EncloseDataWithQuotes = false;
            RadGridUserMaintenanceList.ExportSettings.Csv.RowDelimiter = GridCsvDelimiter.NewLine;
        }

    }
}