﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VoucherBatchesReport.ascx.cs" Inherits="FOWebApp.Nocsa.VoucherBatchesReport" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<table cellpadding="5" cellspacing="2">
    <tr>
        <td>Start date:</td>
        <td> 
            <telerik:RadDatePicker ID="RadDatePickerStart" runat="server">
            </telerik:RadDatePicker>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Provide a start date" ControlToValidate="RadDatePickerStart" Display="Dynamic"></asp:RequiredFieldValidator>
        </td>
        <td>End date:</td>
        <td>
            <telerik:RadDatePicker ID="RadDatePickerEnd" runat="server">
            </telerik:RadDatePicker>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Provide an end date" ControlToValidate="RadDatePickerEnd"></asp:RequiredFieldValidator>
        </td>
        <td>
            <asp:ImageButton ID="ImageButtonQuery" runat="server" 
                ImageUrl="~/Images/invoice_euro.png" onclick="ImageButtonQuery_Click" 
                ToolTip="Create accounting report" />
        </td>
        <td>
            <asp:ImageButton ID="ImageButtonCSVExport" runat="server" 
                ImageUrl="~/Images/CSV.png" onclick="ImageButtonCSVExport_Click" 
                Enabled="True" ToolTip="Export as a CSV file" />
        </td>
        <td>
            <asp:ImageButton ID="ImageButtonExcelExport" runat="server" Enabled="True" 
                ImageUrl="~/Images/Excel.png" onclick="ImageButtonExcelExport_Click" 
                ToolTip="Export as an Excel file" />
        </td>
    </tr>
</table>
<telerik:RadGrid ID="RadGridVoucherReport" runat="server" 
    AutoGenerateColumns="False" CellSpacing="0" GridLines="None" 
    onitemcommand="RadGridVoucherReport_ItemCommand" 
    onneeddatasource="RadGridVoucherReport_NeedDataSource" Skin="WebBlue">
    <GroupingSettings ShowUnGroupButton="True" />
<MasterTableView ShowFooter="True" ShowGroupFooter="True">
<CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>

<RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
<HeaderStyle Width="20px"></HeaderStyle>
</RowIndicatorColumn>

<ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
<HeaderStyle Width="20px"></HeaderStyle>
</ExpandCollapseColumn>

    <Columns>
        <telerik:GridBoundColumn DataField="DistId" 
            FilterControlAltText="Filter DistId column" HeaderText="Dist Id" 
            UniqueName="DistId">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="BatchId" 
            FilterControlAltText="Filter BatchId column" HeaderText="Batch Id" 
            UniqueName="BatchId">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="DateCreated" 
            FilterControlAltText="Filter DateCreated column" HeaderText="Date" 
            UniqueName="DateCreated">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="MB" FilterControlAltText="Filter MB column" 
            HeaderText="MB" UniqueName="MB">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="Vouchers" 
            FilterControlAltText="Filter Vouchers column" HeaderText="Vouchers" 
            UniqueName="Vouchers">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="UP" FilterControlAltText="Filter UP column" 
            HeaderText="UP" UniqueName="UP">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn Aggregate="Sum" DataField="SubTotal" 
            FilterControlAltText="Filter SubTotal column" FooterText="Totals: " 
            HeaderText="Sub Total" UniqueName="SubTotal">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="Currency" 
            FilterControlAltText="Filter Currency column" HeaderText="Currency" 
            UniqueName="Currency">
        </telerik:GridBoundColumn>
    </Columns>
    <GroupByExpressions>
        <telerik:GridGroupByExpression>
            <SelectFields>
                <telerik:GridGroupByField FieldAlias="Name" FieldName="Name" FormatString="" 
                    HeaderText="Distributor" />
                <telerik:GridGroupByField FieldAlias="DistId" FieldName="DistId" 
                    FormatString="" HeaderText="Id" />
            </SelectFields>
            <GroupByFields>
                <telerik:GridGroupByField FieldAlias="BatchId" FieldName="DistId" 
                    FormatString="" HeaderText="" />
            </GroupByFields>
        </telerik:GridGroupByExpression>
    </GroupByExpressions>

<EditFormSettings>
<EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
</EditFormSettings>
</MasterTableView>

<FilterMenu EnableImageSprites="False"></FilterMenu>

<HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default"></HeaderContextMenu>
</telerik:RadGrid>