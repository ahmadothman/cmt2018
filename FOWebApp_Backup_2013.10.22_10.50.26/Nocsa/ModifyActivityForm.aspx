﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ModifyActivityForm.aspx.cs" Inherits="FOWebApp.Nocsa.ModifyActivityForm" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="nim" TagName="ModifyActivity" Src="~/Nocsa/NewActivityLogItem.ascx"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Modify Activity</title>
</head>
<body>
    <form id="formMain" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div>
        <nim:ModifyActivity ID="nimModifyActivity" runat="server" />
    </div>
    </form>
</body>
</html>
