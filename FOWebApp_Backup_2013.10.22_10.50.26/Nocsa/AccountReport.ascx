﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AccountReport.ascx.cs" Inherits="FOWebApp.Nocsa.AccountReport" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="ImageButtonQuery">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGridAcccountingReport" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<table cellpadding="5" cellspacing="2">
    <tr>
        <td>Start date:</td>
        <td> 
            <telerik:RadDatePicker ID="RadDatePickerStart" runat="server">
            </telerik:RadDatePicker>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Provide a start date" ControlToValidate="RadDatePickerStart" Display="Dynamic"></asp:RequiredFieldValidator>
        </td>
        <td>End date:</td>
        <td>
            <telerik:RadDatePicker ID="RadDatePickerEnd" runat="server">
            </telerik:RadDatePicker>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Provide an end date" ControlToValidate="RadDatePickerEnd"></asp:RequiredFieldValidator>
        </td>
        <td>
            <asp:ImageButton ID="ImageButtonQuery" runat="server" 
                ImageUrl="~/Images/invoice_euro.png" onclick="ImageButtonQuery_Click" 
                ToolTip="Create accounting report" />
        </td>
        <td>
            <asp:ImageButton ID="ImageButtonCSVExport" runat="server" 
                ImageUrl="~/Images/CSV.png" onclick="ImageButtonCSVExport_Click" 
                Enabled="True" ToolTip="Export as a CSV file" />
        </td>
        <td>
            <asp:ImageButton ID="ImageButtonExcelExport" runat="server" Enabled="True" 
                ImageUrl="~/Images/Excel.png" onclick="ImageButtonExcelExport_Click" 
                ToolTip="Export as an Excel file" />
        </td>
    </tr>
</table>
<telerik:RadGrid ID="RadGridAcccountingReport" runat="server" 
        CellSpacing="0" GridLines="None" 
        onitemcommand="RadGridAcccountingReport_ItemCommand" 
        onneeddatasource="RadGridAcccountingReport_NeedDataSource" 
    Skin="WebBlue" onitemcreated="RadGridAcccountingReport_ItemCreated">
    <ExportSettings IgnorePaging="True" OpenInNewWindow="True">
        <Pdf Author="CMT" Creator="CMT" Title="CMT Billable Termnal Activities" 
            DefaultFontFamily="Arial Unicode MS" FontType="Embed" PageBottomMargin="20mm" 
            PageFooterMargin="20mm" PageHeight="297mm" PageLeftMargin="20mm" 
            PageRightMargin="20mm" PageTitle="Billable Terminal Activities" 
            PageWidth="210mm" PaperSize="A4" Producer="SatADSL" 
            Subject="Billable Activity Overview" />
        <Csv ColumnDelimiter="Semicolon" EncloseDataWithQuotes="False" />
    </ExportSettings>
    <ClientSettings ReorderColumnsOnClient="True">
    </ClientSettings>
    <MasterTableView CommandItemDisplay="None">
        <CommandItemSettings ExportToPdfText="Export to PDF" 
            ShowAddNewRecordButton="False" ShowExportToCsvButton="False" 
            ShowExportToExcelButton="False" ShowExportToPdfButton="False" 
            ShowExportToWordButton="False" ShowRefreshButton="False" />
        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
            <HeaderStyle Width="20px" />
        </RowIndicatorColumn>
        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
            <HeaderStyle Width="20px" />
        </ExpandCollapseColumn>
        <EditFormSettings>
            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
            </EditColumn>
        </EditFormSettings>
    </MasterTableView>
    <FilterMenu EnableImageSprites="False">
    </FilterMenu>
    <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default">
    </HeaderContextMenu>
</telerik:RadGrid>
