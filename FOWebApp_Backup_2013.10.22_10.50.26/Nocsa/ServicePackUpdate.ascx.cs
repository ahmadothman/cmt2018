﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOMonitorControlWSRef;
using Telerik.Web.UI;

namespace FOWebApp.Nocsa
{
    public partial class ServicePackUpdate : System.Web.UI.UserControl
    {
        //Boolean _newSla = false;
        BOAccountingControlWS _boAccountingControlWS = new BOAccountingControlWS();

        protected void Page_Load(object sender, EventArgs e)
        {
            RadComboBoxISPId.Items.Clear();
            _boAccountingControlWS = new BOAccountingControlWS();
            Isp[] ispList = _boAccountingControlWS.GetIsps();
            RadComboBoxISPId.DataValueField = "Id";
            RadComboBoxISPId.DataTextField = "CompanyName";
            RadComboBoxISPId.DataSource = ispList;
            RadComboBoxISPId.DataBind();
            RadComboBoxISPId.Items.Insert(0, new RadComboBoxItem(""));

            if (Request.Params["id"] == null)
            {
                //Parameter passed
                //_newSla = true;
                buttonSubmit.CommandName = "Insert";

            }
            else
            {
                //_newSla = false;
                TextBoxId.Enabled = false;
                RadComboBoxISPId.Enabled = false;
                this.LoadForm(Int32.Parse(Request.Params["id"]));
            }
        }

        protected void buttonSubmit_Command(object sender, CommandEventArgs e)
        {
            //Create a new Service pack
            ServiceLevel serviceLevel = new ServiceLevel();
            serviceLevel.SlaId = Int32.Parse(TextBoxId.Text);
            serviceLevel.SlaName = TextBoxName.Text;
            serviceLevel.FupThreshold = (decimal)RadNumericTextBoxFUPThreshold.Value;
            serviceLevel.RTNFUPThreshold = (decimal)RadNumericTextBoxRTNFUPThreshold.Value;
            serviceLevel.DRFWD = (int)RadNumericTextBoxForwardDefault.Value;
            serviceLevel.DRRTN = (int)RadNumericTextBoxReturnDefault.Value;
            serviceLevel.DrAboveFup = RadNumericTextBoxForwardAboveFup.Text + "/" + RadNumericTextBoxReturnAboveFup.Text;
            serviceLevel.CIRFWD = (int)RadNumericTextBoxCIRFWD.Value;
            serviceLevel.CIRRTN = (int)RadNumericTextBoxCIRRTN.Value;
            serviceLevel.SlaCommonName = TextBoxSlaCommonName.Text;
            serviceLevel.IspId = Int32.Parse(RadComboBoxISPId.SelectedValue);
            serviceLevel.Business = CheckBoxBusinesss.Checked;
            serviceLevel.FreeZone = CheckBoxFreeZone.Checked;
            serviceLevel.VoIPFlag = CheckBoxVoIP.Checked;
            serviceLevel.Voucher = CheckBoxVoucher.Checked;
            serviceLevel.Hide = CheckBoxHide.Checked;

            serviceLevel.ServicePackAmt = (float)RadNumericTextBoxServicePackAmt.Value;
            serviceLevel.FreeZoneAmt = (float)RadNumericTextBoxFreeZoneAmt.Value;
            serviceLevel.VoIPAmt = (float)Double.Parse(RadNumericTextBoxVoIPAmt.Text);

            //The SerivcePack class is used for the NMS
            ServicePack servicePack = this.serviceLevelToServicePack(serviceLevel);
            BOMonitorControlWS boMonitorControlWS = new BOMonitorControlWS();

            if (boMonitorControlWS.NMSUpdateServicePack(servicePack)) //will return true for all non-NMS SLA's as well
            {
                if (_boAccountingControlWS.UpdateServicePack(serviceLevel))
                {
                    LabelResult.ForeColor = Color.DarkGreen;
                    LabelResult.Text = "Servicepack update succeeded";
                }
                else
                {
                    LabelResult.ForeColor = Color.Red;
                    LabelResult.Text = "Servicepack update failed";
                }
            }
            else
            {
                LabelResult.ForeColor = Color.Red;
                LabelResult.Text = "Servicepack update failed";
            }
        }

        private ServicePack serviceLevelToServicePack(ServiceLevel serviceLevel)
        {
            ServicePack servicePack = new ServicePack();

            servicePack.SlaId = serviceLevel.SlaId;
            servicePack.SlaName = serviceLevel.SlaName;
            servicePack.FupThreshold = serviceLevel.FupThreshold * 1000000000;
            servicePack.RTNFUPThreshold = serviceLevel.RTNFUPThreshold * 1000000000;
            //all NMS speeds must be transformed from kbps to bits/sec
            servicePack.DRFWD = serviceLevel.DRFWD * 1000;
            servicePack.DRRTN = serviceLevel.DRRTN * 1000;
            servicePack.DrAboveFup = (decimal)RadNumericTextBoxForwardAboveFup.Value * 1000;
            servicePack.DRAboveFupFWD = (decimal)RadNumericTextBoxReturnAboveFup.Value * 1000;
            servicePack.CIRFWD = serviceLevel.CIRFWD * 1000;
            servicePack.CIRRTN = serviceLevel.CIRRTN * 1000;
            servicePack.SlaCommonName = serviceLevel.SlaCommonName;
            servicePack.IspId = serviceLevel.IspId;
            servicePack.Business = serviceLevel.Business;
            servicePack.FreeZone = serviceLevel.FreeZone;
            servicePack.VoIPFlag = serviceLevel.VoIPFlag;
            servicePack.Voucher = serviceLevel.Voucher;
            servicePack.Hide = serviceLevel.Hide;
            servicePack.ServicePackAmt = serviceLevel.ServicePackAmt;
            servicePack.FreeZoneAmt = serviceLevel.FreeZoneAmt;
            servicePack.VoIPAmt = serviceLevel.VoIPAmt;

            return servicePack;
        }

        /// <summary>
        /// This method loads the SLA data into the form
        /// </summary>
        /// <param name="slaId">The identifier of the SLA to load</param>
        private void LoadForm(int slaId)
        {
            ServiceLevel sla = _boAccountingControlWS.GetServicePack(slaId);
            TextBoxId.Text = sla.SlaId.ToString();
            TextBoxName.Text = sla.SlaName.ToString().Trim();
            RadNumericTextBoxFUPThreshold.Value = (double) sla.FupThreshold;
            RadNumericTextBoxRTNFUPThreshold.Value = (double)sla.RTNFUPThreshold;
            RadNumericTextBoxForwardDefault.Value = (double)sla.DRFWD;
            RadNumericTextBoxReturnDefault.Value = (double)sla.DRRTN;
            String[] DRAboveFUPTokens = sla.DrAboveFup.Split('/');
            if (DRAboveFUPTokens.Length == 2)
            {
                RadNumericTextBoxForwardAboveFup.Value = Double.Parse(DRAboveFUPTokens[0]);
                RadNumericTextBoxReturnAboveFup.Value = Double.Parse(DRAboveFUPTokens[1]);
            }
            else
            {
                RadNumericTextBoxForwardAboveFup.Value = 0;
                RadNumericTextBoxReturnAboveFup.Value = 0;
            }
            RadNumericTextBoxCIRFWD.Value = (double)sla.CIRFWD;
            RadNumericTextBoxCIRRTN.Value = (double)sla.CIRRTN;
            TextBoxSlaCommonName.Text = sla.SlaCommonName.Trim();
            RadComboBoxISPId.SelectedValue = sla.IspId.ToString();
            CheckBoxBusinesss.Checked = (bool)sla.Business;
            CheckBoxFreeZone.Checked = (bool)sla.FreeZone;
            CheckBoxVoIP.Checked = (bool)sla.VoIPFlag;
            CheckBoxVoucher.Checked = (bool)sla.Voucher;
            CheckBoxHide.Checked = (bool)sla.Hide;

            RadNumericTextBoxServicePackAmt.Value = sla.ServicePackAmt;
            RadNumericTextBoxFreeZoneAmt.Value = sla.FreeZoneAmt;
            RadNumericTextBoxVoIPAmt.Value = sla.VoIPAmt;

        }
    }
}