﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FOWebApp.Data;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;

namespace FOWebApp.Nocsa
{
    public partial class ISPList : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Read the ISPs from the database
            DataHelper dataHelper = new DataHelper();
            List<Isp> ispList = dataHelper.getISPs();
            RadGridISPList.DataSource = ispList;
            RadGridISPList.DataBind();
        }
    }
}