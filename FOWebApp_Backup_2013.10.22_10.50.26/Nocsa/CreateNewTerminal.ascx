﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CreateNewTerminal.ascx.cs"
    Inherits="FOWebApp.Nocsa.CreateNewTerminal" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script language="javascript" type="text/javascript">
    function OnClientSelectedIndexChanged(sender, eventArgs) {
        var item = eventArgs.get_item();
        //sender.set_text("You selected ISP with Id: " + item.get_value());

        var panel = $find("<%=RadXmlHttpPanelSLA.ClientID %>");
        panel.set_value(item.get_value());
        return false;
    }

    function nodeClicking(sender, args) {
        var comboBox = $find("<%= RadComboBoxSLA.ClientID %>");

        var node = args.get_node()
        if (node.get_level() == 1) {

            comboBox.set_text(node.get_text());
            comboBox.set_value(node.get_value());
            document.getElementById('HiddenFieldSLAId').value = node.get_value();
            comboBox.trackChanges();
            comboBox.get_items().getItem(0).set_text(node.get_text());
            comboBox.commitChanges();

            comboBox.hideDropDown();
        }
    }

    function IPRangeChanged(chkBoxId) {
        if (chkBoxId.checked == true) {
            document.getElementById('<%= RadComboBoxIPMask.ClientID %>').disabled = false;
            ValidatorEnable(document.getElementById('<%= RequiredFieldValidatorStartIP.ClientID %>'), true);
        }
        else {
            document.getElementById('<%= RadComboBoxIPMask.ClientID %>').disabled = true;
            ValidatorEnable(document.getElementById('<%= RequiredFieldValidatorStartIP.ClientID %>'), false);
        }
    }

    function OnMacAddressChanged(sender, eventArgs) {
        var macAddress = eventArgs.get_newValue();
        FOWebApp.WebServices.AccountSupportWS.IsMacAddressUnique(macAddress, OnRequestComplete, OnError);
        return false;
    }

    function OnRequestComplete(result) {
        if (result == false) {
            document.getElementById('<%= LabelMacExists.ClientID %>').innerHTML = "Sorry but this MAC address already exists!";
        }
        else {
            document.getElementById('<%= LabelMacExists.ClientID %>').innerHTML = "";
        }
    }

    function OnError(result) {
        document.getElementById('<%= LabelMacExists.ClientID %>').innerHTML = "An error occured while checking the MAC address";
    }
</script>
<table cellpadding="10" cellspacing="5">
    <tr>
        <td>Mac Address:
        </td>
        <td>
            <telerik:RadMaskedTextBox ID="RadMaskedTextBoxMacAddress" runat="server" Skin="WebBlue"
                ControlToValidate="RadMaskedTextBoxMacAddress" ClientEvents-OnValueChanged="OnMacAddressChanged">
            </telerik:RadMaskedTextBox>
            <asp:Label ID="LabelMacExists" runat="server" ForeColor="Red"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>ISP:
        </td>
        <td>
            <telerik:RadComboBox ID="RadComboBoxISP" runat="server" Skin="WebBlue" OnClientSelectedIndexChanged="OnClientSelectedIndexChanged"
                Width="350px">
            </telerik:RadComboBox>
        </td>
    </tr>
    <!--
    <tr>
        <td>Sit Id:
        </td>
        <td>
            <asp:TextBox ID="TextBoxSitId" runat="server"></asp:TextBox>
            <asp:Label ID="LabelSitIdRequired" runat="server" ForeColor="#FF3300"></asp:Label>
        </td>
    </tr>
    -->
    <tr>
        <td>Serial:
        </td>
        <td>
            <asp:TextBox ID="TextBoxSerial" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Please provide the serial number!"
                ControlToValidate="TextBoxSerial"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>Terminal Name:
        </td>
        <td>
            <asp:TextBox ID="TextBoxTerminalName" runat="server" Width="425px" MaxLength="50"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Please provide a unique terminal name!"
                ControlToValidate="TextBoxTerminalName"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>Test Mode:
        </td>
        <td>
            <asp:CheckBox ID="CheckBoxTestMode" runat="server" />
        </td>
    </tr>
    <tr>
        <td>Distributor:
        </td>
        <td>
            <telerik:RadComboBox ID="RadComboBoxDistributor" runat="server" Skin="WebBlue" ExpandDirection="Up"
                Height="200px" Width="430px">
            </telerik:RadComboBox>
        </td>
    </tr>
    <tr>
        <td>Service Package:
        </td>
        <td>
            <telerik:RadXmlHttpPanel ID="RadXmlHttpPanelSLA" runat="server" EnableClientScriptEvaluation="True"
                OnServiceRequest="RadXmlHttpPanelSLA_ServiceRequest">
                <telerik:RadComboBox ID="RadComboBoxSLA" runat="server" Skin="WebBlue" ExpandDirection="Up"
                    Height="200px" Width="430px">
                </telerik:RadComboBox>
            </telerik:RadXmlHttpPanel>
        </td>
    </tr>
    <tr>
        <td>Free zone schedule:
        </td>
        <td>
            <telerik:RadComboBox ID="RadComboBoxFreeZone" runat="server" Skin="WebBlue" ExpandDirection="Up"
                Height="200px" Width="430px">
            </telerik:RadComboBox>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <table>
                <tr>
                    <td>(Start) IP Address:
                        <asp:TextBox ID="TextBoxIP" runat="server" Columns="39"></asp:TextBox></td>
                    <td>IP Mask:
                        <!-- <asp:TextBox ID="TextBoxIPMask" runat="server" ToolTip="Specify an IP Mask (\ value)" Enabled="False" Columns="39"></asp:TextBox></td> -->
                        <telerik:RadComboBox ID="RadComboBoxIPMask" runat="server" Enabled="false" Width="60">
                            <Items>
                                <telerik:RadComboBoxItem runat="server" Text="32" Value="32" />
                                <telerik:RadComboBoxItem runat="server" Text="31" Value="31" />
                                <telerik:RadComboBoxItem runat="server" Text="30" Value="30" />
                                <telerik:RadComboBoxItem runat="server" Text="29" Value="29" />
                                <telerik:RadComboBoxItem runat="server" Text="28" Value="28" />
                                <telerik:RadComboBoxItem runat="server" Text="27" Value="27" />
                                <telerik:RadComboBoxItem runat="server" Text="26" Value="26" />
                                <telerik:RadComboBoxItem runat="server" Text="25" Value="25" />
                                <telerik:RadComboBoxItem runat="server" Text="24" Value="24" />
                            </Items>
                        </telerik:RadComboBox>
                    </td>
                    <td>
                        <asp:CheckBox ID="CheckBoxIPRange" runat="server" Text="Specify IP Range" ToolTip="Check this box if the terminal has an IP Address range. In this case specify an end range IP address." OnClick="javascript:IPRangeChanged(this);" AutoPostBack="False" /></td>
                </tr>
                <tr>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorStartIP" runat="server" ErrorMessage="Please specify a start IP address" Enabled="False" ControlToValidate="TextBoxIP"></asp:RequiredFieldValidator></td>
                    <td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>Subscription Start Date:
        </td>
        <td>
            <telerik:RadDatePicker ID="RadDatePickerStartDate" runat="server" Skin="WebBlue">
                <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x"
                    Skin="WebBlue" runat="server">
                </Calendar>
                <DateInput DisplayDateFormat="d/MM/yyyy" DateFormat="d/MM/yyyy" runat="server">
                </DateInput>
                <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
            </telerik:RadDatePicker>
        </td>
    </tr>
    <tr>
        <td>Subscription Expiry Date:
        </td>
        <td>
            <telerik:RadDatePicker ID="RadDatePickerExpiryDate" runat="server" Skin="WebBlue">
                <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x"
                    Skin="WebBlue" runat="server">
                </Calendar>
                <DateInput DisplayDateFormat="d/MM/yyyy" DateFormat="d/MM/yyyy" runat="server">
                </DateInput>
                <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
            </telerik:RadDatePicker>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Button ID="ButtonSave" runat="server" Text="Save" OnClick="ButtonSave_Click" />
        </td>
        <td>
            <asp:Label ID="LabelResult" runat="server"></asp:Label>
        </td>
    </tr>
</table>
