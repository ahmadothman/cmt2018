﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ServicePackUpdate.ascx.cs"
    Inherits="FOWebApp.Nocsa.ServicePackUpdate" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<style type="text/css">
    .auto-style1 {
        height: 47px;
    }
</style>
<table cellpadding="2" cellspacing="5">
    <tr>
        <td width="20%">
            Id:
        </td>
        <td>
            <asp:TextBox ID="TextBoxId"  runat="server" Columns="4" Width="177px"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="You must provide a SLA ID!"
                ControlToValidate="TextBoxId" SetFocusOnError="True"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Name:
        </td>
        <td>
            <asp:TextBox ID="TextBoxName" runat="server" Columns="50">
            </asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="You must provide a SLA Name!"
                ControlToValidate="TextBoxName" SetFocusOnError="True"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Forward FUP:
        </td>
        <td>
            <telerik:RadNumericTextBox ID="RadNumericTextBoxFUPThreshold" runat="server" Culture="en-US" ToolTip="Value in GB" ShowSpinButtons="True" Skin="WebBlue" Value="0" Width="125px">
                <IncrementSettings Step="0.01" />
<NumberFormat ZeroPattern="n"></NumberFormat>
            </telerik:RadNumericTextBox>
             GB
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="You must provide a Forward FUP value!"
                ControlToValidate="RadNumericTextBoxFUPThreshold" SetFocusOnError="True"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Return FUP:
        </td>
        <td>
            <telerik:RadNumericTextBox ID="RadNumericTextBoxRTNFUPThreshold" Culture="en-US" runat="server" ToolTip="Value in GB" ShowSpinButtons="True" Skin="WebBlue" Value="0" Width="125px">
                <IncrementSettings Step="0.01" />
<NumberFormat ZeroPattern="n"></NumberFormat>
            </telerik:RadNumericTextBox>
             GB
            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="You must provide a Return FUP Value!"
                ControlToValidate="RadNumericTextBoxRTNFUPThreshold" SetFocusOnError="True"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td class="auto-style1">
            Forward speed (NMS only):
        </td>
        <td class="auto-style1">
            <telerik:RadNumericTextBox ID="RadNumericTextBoxForwardDefault" runat="server" DataType="System.Int32" ShowSpinButtons="True" Culture="nl-BE" Skin="WebBlue" Value="0" Width="125px">
<NumberFormat ZeroPattern="n" DecimalDigits="0"></NumberFormat>
            </telerik:RadNumericTextBox> Kbps
        </td>
    </tr>
    <tr>
        <td class="auto-style1">
            Return speed (NMS only):
        </td>
        <td class="auto-style1">
            <telerik:RadNumericTextBox ID="RadNumericTextBoxReturnDefault" runat="server" DataType="System.Int32" ShowSpinButtons="True" Culture="nl-BE" Skin="WebBlue" Value="0" Width="125px">
<NumberFormat ZeroPattern="n" DecimalDigits="0"></NumberFormat>
            </telerik:RadNumericTextBox> Kbps
        </td>
    </tr>
    <tr>
        <td class="auto-style1">
            Forward speed Above FUP:
        </td>
        <td class="auto-style1">
            <telerik:RadNumericTextBox ID="RadNumericTextBoxForwardAboveFup" runat="server" DataType="System.Int32" ShowSpinButtons="True" Culture="nl-BE" Skin="WebBlue" Value="0" Width="125px">
<NumberFormat ZeroPattern="n" DecimalDigits="0"></NumberFormat>
            </telerik:RadNumericTextBox> Kbps
            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="You must provide Above FUP speed values!"
                ControlToValidate="RadNumericTextBoxForwardAboveFup" SetFocusOnError="True"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Return speed Above FUP:
        </td>
        <td>
            <telerik:RadNumericTextBox ID="RadNumericTextBoxReturnAboveFup" runat="server" DataType="System.Int32" ShowSpinButtons="True" Culture="nl-BE" Skin="WebBlue" Value="0" Width="125px">
<NumberFormat ZeroPattern="n" DecimalDigits="0"></NumberFormat>
            </telerik:RadNumericTextBox> Kbps
            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="You must provide Above FUP speed values!"
                ControlToValidate="RadNumericTextBoxReturnAboveFup" SetFocusOnError="True"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td class="auto-style1">
            CIR forward (NMS only):
        </td>
        <td class="auto-style1">
            <telerik:RadNumericTextBox ID="RadNumericTextBoxCIRFWD" runat="server" DataType="System.Int32" ShowSpinButtons="True" Culture="nl-BE" Skin="WebBlue" Value="0" Width="125px">
<NumberFormat ZeroPattern="n" DecimalDigits="0"></NumberFormat>
            </telerik:RadNumericTextBox> Kbps
        </td>
    </tr>
    <tr>
        <td class="auto-style1">
            CIR return (NMS only):
        </td>
        <td class="auto-style1">
            <telerik:RadNumericTextBox ID="RadNumericTextBoxCIRRTN" runat="server" DataType="System.Int32" ShowSpinButtons="True" Culture="nl-BE" Skin="WebBlue" Value="0" Width="125px">
<NumberFormat ZeroPattern="n" DecimalDigits="0"></NumberFormat>
            </telerik:RadNumericTextBox> Kbps
        </td>
    </tr>
    <tr>
        <td>
            Common Name:
        </td>
        <td>
            <asp:TextBox ID="TextBoxSlaCommonName" runat="server"
                Columns="50">
            </asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            ISP:
        </td>
        <td>
            <telerik:RadComboBox ID="RadComboBoxISPId" runat="server" Width="345px" ExpandDirection="Up"></telerik:RadComboBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="You must provide an ISP!"
                ControlToValidate="RadComboBoxISPId" SetFocusOnError="True"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Business:
        </td>
        <td>
            <asp:CheckBox ID="CheckBoxBusinesss" runat="server" ToolTip="Check if  this is a business pack SLA" />
        </td>
    </tr>
    <tr>
        <td>
            Free Zone:
        </td>
        <td>
            <asp:CheckBox ID="CheckBoxFreeZone" runat="server" ToolTip="Check if this service pack contains the Freezone option" />
        </td>
    </tr>
    <tr>
        <td>
            VoIP:
        </td>
        <td>
            <asp:CheckBox ID="CheckBoxVoIP" runat="server" ToolTip="Check if this service pack contains a VoIP option" />
        </td>
    </tr>
    <tr>
        <td>
            Voucher:
        </td>
        <td>
            <asp:CheckBox ID="CheckBoxVoucher" runat="server" ToolTip="Check if the service pack is a voucher based SLA" />
        </td>
    </tr>
    <tr>
        <td>
            Hide for distributor:
        </td>
        <td>
            <asp:CheckBox ID="CheckBoxHide" runat="server" ToolTip="This flag determines if the ServicePack is visible to the distributor or not" />
        </td>
    </tr>
</table>
<br />
<asp:Panel ID="PanelAccounting" runat="server" GroupingText="Monthly Subscription Fee" BackColor="#CCFFCC" BorderStyle="Ridge">
    <table>
        <tr>
            <td>
                Service Pack Base Amount ($US):
            </td>
            <td>
                <telerik:RadNumericTextBox ID="RadNumericTextBoxServicePackAmt" runat="server" Culture="en-US" ShowSpinButtons="True" Skin="WebBlue" ToolTip="Amount in $US, use dot for decimal point" Type="Currency" Width="125px" Value="0">
                    <IncrementSettings Step="0.5" />
                    <NumberFormat ZeroPattern="$n" />
                </telerik:RadNumericTextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="Please specify base amount" ControlToValidate="RadNumericTextBoxServicePackAmt"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                Freezone Option ($US):
            </td>
            <td>
                <telerik:RadNumericTextBox ID="RadNumericTextBoxFreeZoneAmt" runat="server" Culture="en-US" ShowSpinButtons="True" Skin="WebBlue" ToolTip="Amount in $US, use dot for decimal point" Type="Currency" Width="125px" Value="0">
                    <IncrementSettings Step="0.5" />
                    <NumberFormat ZeroPattern="$n" />
                </telerik:RadNumericTextBox>
            </td>
        </tr>
        <tr>
            <td>
                VoIP Option ($US):
            </td>
            <td>
                <telerik:RadNumericTextBox ID="RadNumericTextBoxVoIPAmt" runat="server" Culture="en-US" ShowSpinButtons="True" Skin="WebBlue" ToolTip="Amount in $US, use dot for decimal point" Type="Currency" Width="125px" Value="0">
                    <IncrementSettings Step="0.5" />
                    <NumberFormat ZeroPattern="$n" />
                </telerik:RadNumericTextBox>
            </td>
        </tr>
    </table>
</asp:Panel>
<br />
 <asp:Button ID="buttonSubmit" Text= "Submit" runat="server" OnCommand="buttonSubmit_Command">
 </asp:Button>&nbsp&nbsp
<asp:Label ID="LabelResult" runat="server"></asp:Label>

