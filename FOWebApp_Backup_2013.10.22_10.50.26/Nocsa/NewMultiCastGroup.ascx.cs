﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Drawing;
using Telerik.Web.UI;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOMultiCastGroupControllerWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;
using net.nimera.supportlibrary;

namespace FOWebApp.Nocsa
{
    public partial class NewMultiCastGroup : System.Web.UI.UserControl
    {
        BOAccountingControlWS _boAccountingControlWS = null;
        BOMultiCastGroupControllerWS _boMultiCastGroupController = null;
        BOLogControlWS _boLogControlWS = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            _boAccountingControlWS = new BOAccountingControlWS();
            _boMultiCastGroupController = new BOMultiCastGroupControllerWS();
            _boLogControlWS = new BOLogControlWS();

            //Load the drop down boxes
            //Fill the lookup comboboxes
            Isp[] IspList = _boAccountingControlWS.GetIsps(); ;

            foreach (Isp isp in IspList)
            {
                RadComboBoxItem item = new RadComboBoxItem();
                item.Value = isp.Id.ToString();
                item.Text = isp.CompanyName;
                RadComboBoxISP.Items.Add(item);
            }

            //Load customers into the combobox
            Distributor[] distributors = _boAccountingControlWS.GetDistributors();

            foreach (Distributor distributor in distributors)
            {
                RadComboBoxItem item = new RadComboBoxItem();
                item.Value = distributor.Id.ToString();
                item.Text = distributor.FullName;
                RadComboBoxDistributor.Items.Add(item);
            }
        }

        protected void ButtonSubmit_Click(object sender, EventArgs e)
        {
            //Dump the data to the CMT Database and NMS
            //First calculate the MAC address from the IP Address
            string macAddress = MultiCastHelper.CreateMacFromIP(TextBoxIPAddress.Text);
            MultiCastGroup mc = new MultiCastGroup();

            //Determine the actual user
            MembershipUser user = Membership.GetUser();

            mc.IPAddress = TextBoxIPAddress.Text;
            mc.MacAddress = macAddress;
            mc.Name = TextBoxName.Text;
            mc.OrganizationId = 0;
            mc.portNr = Int32.Parse(TextBoxPortNr.Text);
            mc.sourceURL = TextBoxSourceURL.Text;
            mc.State = false;
            mc.UserId = (Guid)user.ProviderUserKey;
            mc.Bytes = 0L;
            mc.Deleted = false;
            mc.BandWidth = Int32.Parse(TextBoxBandwidth.Text);
            mc.FirstActivationDate = DateTime.UtcNow;
            mc.DistributorId = Int32.Parse(RadComboBoxDistributor.SelectedValue);
            mc.IspId = Int32.Parse(RadComboBoxISP.SelectedValue);

            if (_boMultiCastGroupController.CreateMultiCastGroup(mc))
            {
                LabelResult.ForeColor = Color.DarkGreen;
                LabelResult.Text = "Multicast group creation succeeded";
                TextBoxMacAddress.Text = macAddress;
            }
            else
            {
                LabelResult.ForeColor = Color.Red;
                LabelResult.Text = "Multicast group creation failed";
            }
        }
    }
}