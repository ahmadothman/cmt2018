﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using FOWebApp.net.nimera.cmt.BOVoucherControllerWSRef;

namespace FOWebApp.Nocsa
{
    public partial class VoucherBatchRelease : System.Web.UI.Page
    {
        BOVoucherControllerWS _boVoucherControl = new BOVoucherControllerWS();

        protected void Page_Load(object sender, EventArgs e)
        {
            //Get BatchId parameter
            string batchId = Request.Params["bid"];
            LabelBatchId.Text = batchId;

            if (!IsPostBack)
            {
                VoucherBatch vb = _boVoucherControl.GetVoucherBatchInfo(Int32.Parse(batchId));
                CheckBoxRelease.Checked = vb.Release;

                if (vb.Release)
                {
                    RadButtonSubmit.Text = "Un-Release Voucher";
                }
                else
                {
                    RadButtonSubmit.Text = "Release Voucher";
                }
            }
        }

        protected void RadButtonSubmit_Click(object sender, EventArgs e)
        {
            if (RadButtonSubmit.Text.Equals("Release Voucher"))
            {
                //Release voucher batch
                if (_boVoucherControl.ReleaseVoucherBatch(Int32.Parse(LabelBatchId.Text)))
                {
                    LabelResult.ForeColor = Color.Green;
                    LabelResult.Text = "Release of voucher batch succeeded";
                    RadButtonSubmit.Text = "Un-Release Voucher";
                    CheckBoxRelease.Checked = true;
                }
                else
                {
                    LabelResult.ForeColor = Color.Red;
                    LabelResult.Text = "Release of voucher batch failed";
                }
            }
            else
            {
                //Unrelease voucher batch
                if (_boVoucherControl.UnreleaseVoucherBatch(Int32.Parse(LabelBatchId.Text)))
                {
                    LabelResult.ForeColor = Color.Green;
                    LabelResult.Text = "Un-Release of voucher batch succeeded";
                    RadButtonSubmit.Text = "Release Voucher";
                    CheckBoxRelease.Checked = false;
                }
                else
                {
                    LabelResult.ForeColor = Color.Red;
                    LabelResult.Text = "Un-Release of voucher batch failed";
                }
            }

        }
    }
}