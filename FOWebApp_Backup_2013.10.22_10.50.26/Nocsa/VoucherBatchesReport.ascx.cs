﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FOWebApp.net.nimera.cmt.BOVoucherControllerWSRef;

namespace FOWebApp.Nocsa
{
    public partial class VoucherBatchesReport : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int actMonth = DateTime.Now.Month;
            int actYear = DateTime.Now.Year;

            int daysInMonth = DateTime.DaysInMonth(actYear, actMonth);

            RadDatePickerStart.SelectedDate = new DateTime(actYear, actMonth, 1);
            RadDatePickerEnd.SelectedDate = new DateTime(actYear, actMonth, daysInMonth);
        }

        protected void ImageButtonQuery_Click(object sender, ImageClickEventArgs e)
        {
            DateTime startDate;
            DateTime endDate;

            BOVoucherControllerWS boVoucherControl = new BOVoucherControllerWS();

            if (!RadDatePickerStart.IsEmpty || !RadDatePickerEnd.IsEmpty)
            {
                startDate = (DateTime)RadDatePickerStart.SelectedDate;
                endDate = (DateTime)RadDatePickerEnd.SelectedDate;

                this.Session["voucherReport_StartDate"] = startDate;
                this.Session["voucherReport_EndDate"] = endDate;

                RadGridVoucherReport.DataSource = boVoucherControl.GenerateVoucherReport(startDate, endDate);
                RadGridVoucherReport.Rebind();
                RadGridVoucherReport.Visible = true;
            }
            else
            {
                RadGridVoucherReport.Visible = false;
            }
        }

        protected void ImageButtonCSVExport_Click(object sender, ImageClickEventArgs e)
        {
            RadGridVoucherReport.MasterTableView.ExportToCSV();
        }

        protected void ImageButtonPDFExport_Click(object sender, ImageClickEventArgs e)
        {
            DateTime startDate = (DateTime)RadDatePickerStart.SelectedDate;
            DateTime endDate = (DateTime)RadDatePickerEnd.SelectedDate;
            RadGridVoucherReport.ExportSettings.Pdf.Title = "Voucher batches from " + startDate.Date + " To " + endDate.Date;
            RadGridVoucherReport.ExportSettings.Pdf.FontType = Telerik.Web.Apoc.Render.Pdf.FontType.Embed;
            RadGridVoucherReport.ExportSettings.Pdf.DefaultFontFamily = "Arial";
            RadGridVoucherReport.ExportSettings.Pdf.PaperSize = GridPaperSize.A4;
            RadGridVoucherReport.MasterTableView.ExportToPdf();
        }

        protected void ImageButtonExcelExport_Click(object sender, ImageClickEventArgs e)
        {
            RadGridVoucherReport.MasterTableView.ExportToExcel();
        }

        protected void RadGridVoucherReport_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            DateTime startDate;
            DateTime endDate;

            BOVoucherControllerWS boVoucherControl = new BOVoucherControllerWS();

            if (!RadDatePickerStart.IsEmpty || !RadDatePickerEnd.IsEmpty)
            {
                startDate = (DateTime)RadDatePickerStart.SelectedDate;
                endDate = (DateTime)RadDatePickerEnd.SelectedDate;

                this.Session["voucherReport_StartDate"] = startDate;
                this.Session["voucherReport_EndDate"] = endDate;

                RadGridVoucherReport.DataSource = boVoucherControl.GenerateVoucherReport(startDate, endDate);

            }
            else
            {
                //Check if values were stored in session variables
                if (this.Session["voucherReport_StartDate"] != null && this.Session["voucherReport_EndDate"] != null)
                {
                    startDate = (DateTime)this.Session["voucherReport_StartDate"];
                    endDate = (DateTime)this.Session["voucherReport_EndDate"];

                    RadGridVoucherReport.DataSource = boVoucherControl.GenerateVoucherReport(startDate, endDate);
                }
                else
                {
                    RadGridVoucherReport.Visible = false;
                }
            }
        }

        protected void RadGridVoucherReport_ItemCommand(object sender, GridCommandEventArgs e)
        {
            this.ConfigureExport();
            if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToExcelCommandName)
            {
                RadGridVoucherReport.MasterTableView.ExportToExcel();
            }

            if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToCsvCommandName)
            {
                RadGridVoucherReport.MasterTableView.ExportToCSV();
            }

            if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToPdfCommandName)
            {
                RadGridVoucherReport.MasterTableView.ExportToPdf();
            }

            if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToWordCommandName)
            {
                RadGridVoucherReport.MasterTableView.ExportToWord();
            }
        }

        /// <summary>
        /// Configures the export settings for this radgrid
        /// </summary>
        public void ConfigureExport()
        {
            RadGridVoucherReport.ExportSettings.FileName = "CMTVouchers";
            RadGridVoucherReport.ExportSettings.ExportOnlyData = true;
            RadGridVoucherReport.ExportSettings.IgnorePaging = true;
            RadGridVoucherReport.ExportSettings.OpenInNewWindow = true;
            RadGridVoucherReport.ExportSettings.Excel.Format = GridExcelExportFormat.Html;
            RadGridVoucherReport.ExportSettings.Csv.ColumnDelimiter = GridCsvDelimiter.Semicolon;
            RadGridVoucherReport.ExportSettings.Csv.EncloseDataWithQuotes = false;
            RadGridVoucherReport.ExportSettings.Csv.RowDelimiter = GridCsvDelimiter.NewLine;
        }
    }
}