﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOMonitorControlWSRef;

namespace FOWebApp.Nocsa
{
    public partial class FreeZoneList : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Load data into the grid view.
            BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();
            FreeZoneCMT[] freeZones = boAccountingControl.GetFreeZones();
            List<FreeZoneListItem> freeZoneList = new List<FreeZoneListItem>();
            FreeZoneListItem freeZone;
            int count = freeZones.Length;
            for (int i = 0; i < count; i++)
            {
                freeZone = new FreeZoneListItem();
                freeZone.FreeZoneId = freeZones[i].Id;
                freeZone.FreeZoneName = freeZones[i].Name;
                freeZone.CurrentActive = freeZones[i].CurrentActive;
                freeZone.Monday = freeZones[i].MonOff + " - " + freeZones[i].MonOn;
                freeZone.Tuesday = freeZones[i].TueOff + " - " + freeZones[i].TueOn;
                freeZone.Wednesday = freeZones[i].WedOff + " - " + freeZones[i].WedOn;
                freeZone.Thursday = freeZones[i].ThuOff + " - " + freeZones[i].ThuOn;
                freeZone.Friday = freeZones[i].FriOff + " - " + freeZones[i].FriOn;
                freeZone.Saturday = freeZones[i].SatOff + " - " + freeZones[i].SatOn;
                freeZone.Sunday = freeZones[i].SunOff + " - " + freeZones[i].SunOn;

                freeZoneList.Add(freeZone);
            }

            RadGridFreeZones.DataSource = freeZoneList;
            RadGridFreeZones.DataBind();
        }

        protected void RadGridFreeZones_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridItemCollection selectedItems = RadGridFreeZones.SelectedItems;

            if (selectedItems.Count > 0)
            {
                GridItem selectedItem = selectedItems[0];
                FreeZoneCMT freeZone = (FreeZoneCMT)selectedItem.DataItem;
                string id = freeZone.Id.ToString();
            }
        }

        protected void RadGridFreeZones_ItemDataBound(object sender, GridItemEventArgs e)
        {
        }

        protected void RadWindowManager1_Disposed(object sender, EventArgs e)
        {
        }

        protected void RadWindowManager1_Unload(object sender, EventArgs e)
        {
        }

        protected void RadGridFreeZones_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            this.Session["_FreeZonesList_PageSize"] = e.NewPageSize;
        }

        protected void RadGridFreeZones_ItemCommand(object sender, GridCommandEventArgs e)
        {
        }

        public class FreeZoneListItem
        {
            public int FreeZoneId {get; set;}
            public string FreeZoneName { get; set; }
            public int CurrentActive { get; set; }
            public string Monday { get; set; }
            public string Tuesday { get; set; }
            public string Wednesday { get; set; }
            public string Thursday { get; set; }
            public string Friday { get; set; }
            public string Saturday { get; set; }
            public string Sunday { get; set; }
        }
    }
}