﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using FOWebApp.net.nimera.cmt.BOServicesControllerWSRef;

namespace FOWebApp.Nocsa
{
    /// <summary>
    /// This handler is used for retreiving the icons from the database
    /// and properly displaying them when called
    /// </summary>
    public class ServicesImageHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            BOServicesControllerWS servicesController = new BOServicesControllerWS();
            
            context.Response.Clear();

            if (!String.IsNullOrEmpty(context.Request.QueryString["serviceId"]))
            {
                string serviceId = context.Request.QueryString["serviceId"];

                // Retrieve the image
                var blob = servicesController.GetIcon(serviceId);
                MemoryStream memoryStream = new MemoryStream(blob, false);
                Image image = Image.FromStream(memoryStream);

                // set the format of the image
                context.Response.ContentType = "image/png";
                // Save the image to the OutputStream
                image.Save(context.Response.OutputStream, ImageFormat.Png);
            }
            else
            {
                context.Response.ContentType = "text/html";
                context.Response.Write("<p>Need a valid serviceId</p>");
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}