﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using Telerik.Web.UI;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;

namespace FOWebApp.Nocsa
{
    /// <summary>
    /// Form which allows to add or MODIFY an activity entry. In case the form serves as
    /// an entry for a new activity, 
    /// </summary>
    public partial class NewActivityLogItem : System.Web.UI.UserControl
    {
        const string _HEXBYTE = "<0|1|2|3|4|5|6|7|8|9|a|b|c|d|e|f><0|1|2|3|4|5|6|7|8|9|a|b|c|d|e|f>";
        BOAccountingControlWS _boAccountingControl = null;

        public Boolean NewFlag { set; get; }
        public int ActivityId { set; get; }

        protected void Page_Load(object sender, EventArgs e)
        {
            _boAccountingControl = new BOAccountingControlWS();
            BOLogControlWS boLogControl = new BOLogControlWS();

            ServiceLevel[] serviceLevels = null;

            //Fill-in the terminal activity combobox
            ActivityType[] activityTypes = boLogControl.GetTerminalActivityTypes();
            RadComboBoxTerminalActivity.DataSource = activityTypes;
            RadComboBoxTerminalActivity.DataBind();

            //Fill-up the treeview in the Sla RadComboBox
            Isp[] isps = _boAccountingControl.GetIsps();
            RadTreeView slaTree = (RadTreeView)RadComboBoxSLA.Items[0].FindControl("RadTreeViewSLA");
            slaTree.Nodes.Clear();
            foreach (Isp isp in isps)
            {
                RadTreeNode rtn = new RadTreeNode(isp.CompanyName, isp.Id.ToString());
                rtn.ImageUrl = "~/Images/contract.png";
                rtn.ToolTip = isp.Id.ToString();

                //Add child nodes to the tree node
                serviceLevels = _boAccountingControl.GetServicePacksByIsp((int)isp.Id);

                RadTreeNode slNode = null;
                foreach (ServiceLevel sl in serviceLevels)
                {
                    slNode = new RadTreeNode(sl.SlaName, sl.SlaId.ToString());
                    slNode.ToolTip = sl.SlaCommonName;
                    rtn.Nodes.Add(slNode);
                }
                slaTree.Nodes.Add(rtn);
            }

            //Fill-up the treeview in the NewSla RadComboBox
            RadTreeView newSlaTree = (RadTreeView)RadComboBoxNewSla.Items[0].FindControl("RadTreeViewNewSLA");
            newSlaTree.Nodes.Clear();
            foreach (Isp isp in isps)
            {
                RadTreeNode rtn = new RadTreeNode(isp.CompanyName, isp.Id.ToString());
                rtn.ImageUrl = "~/Images/contract.png";
                rtn.ToolTip = isp.Id.ToString();

                //Add child nodes to the tree node
                serviceLevels = _boAccountingControl.GetServicePacksByIsp((int)isp.Id);

                RadTreeNode slNode = null;
                foreach (ServiceLevel sl in serviceLevels)
                {
                    slNode = new RadTreeNode(sl.SlaName, sl.SlaId.ToString());
                    slNode.ToolTip = sl.SlaCommonName;
                    rtn.Nodes.Add(slNode);
                }
                slaTree.Nodes.Add(rtn);
                newSlaTree.Nodes.Add(rtn);
            }

            if (!RadDatePickerTimeStamp.SelectedDate.HasValue)
            {
                RadDatePickerTimeStamp.SelectedDate = DateTime.Now.Date;
            }

            if (NewFlag)
            {
                LabelTitle.Text = "Add a new terminal activity";
                CheckBoxAccountingFlag.Checked = true;
            }
            else
            {
                LabelTitle.Text = "Update terminal activity";

                //Retrieve the activity object corresponding with the given Id
                TerminalActivity termActivity = boLogControl.GetTerminalActivityById(ActivityId);

                if (termActivity != null)
                {
                    //Keep the ActivityId for later postbacks
                    HiddenFieldActivityId.Value = ActivityId.ToString();

                    //Fill in the text boxes
                    RadMaskedTextBoxMacAddress.Text = termActivity.MacAddress;

                    //Get the SLA identifier for the given SLA Name
                    RadTreeView slaTreeView = (RadTreeView)RadComboBoxSLA.Items[0].FindControl("RadTreeViewSLA");
                    int? servicePackId = this.GetServicePackIdByName(termActivity.SlaName, termActivity.MacAddress);
                    if (servicePackId != null)
                    {
                        RadComboBoxSLA.SelectedValue = servicePackId.ToString();
                        RadComboBoxSLA.Text = termActivity.SlaName;
                    }

                    RadTreeView NewSlaTreeView = (RadTreeView)RadComboBoxNewSla.Items[0].FindControl("RadTreeViewNewSla");
                    servicePackId = this.GetServicePackIdByName(termActivity.NewSlaName, termActivity.MacAddress);
                    if (servicePackId != null)
                    {
                        RadComboBoxNewSla.SelectedValue = servicePackId.ToString();
                        RadComboBoxNewSla.Text = termActivity.NewSlaName;
                    }

                    RadDatePickerTimeStamp.SelectedDate = termActivity.ActionDate;
                    RadComboBoxTerminalActivity.SelectedValue = termActivity.TerminalActivityId.ToString();
                    CheckBoxAccountingFlag.Checked = termActivity.AccountingFlag;
                    RadTextBoxComment.Text = termActivity.Comment;
                }
                else
                {
                    LabelStatus.ForeColor = Color.DarkRed;
                    LabelStatus.Text = "Sorry but I could not retrieve the terminal activity details";
                }
            }

            //Initialize the form
            RadMaskedTextBoxMacAddress.Mask =
                _HEXBYTE + "<:>" + _HEXBYTE + "<:>" + _HEXBYTE + "<:>" + _HEXBYTE + "<:>" + _HEXBYTE + "<:>" +
                _HEXBYTE;

            LabelStatus.Text = "";
        }

        //Write the new activity log to the database
        protected void RadButtonSubmit_Click(object sender, EventArgs e)
        {
            //Get the user id first
            //Get the authenticated user
            MembershipUser myObject = Membership.GetUser();
            string userId = myObject.ProviderUserKey.ToString();

            BOLogControlWS boLogControl = new BOLogControlWS();
            TerminalActivity termActivity = new TerminalActivity();

            termActivity.Action = RadComboBoxTerminalActivity.Text;
            termActivity.ActionDate = (DateTime)RadDatePickerTimeStamp.SelectedDate;
            termActivity.MacAddress = RadMaskedTextBoxMacAddress.Text;
            termActivity.SlaName = RadComboBoxSLA.Text;
            termActivity.NewSlaName = RadComboBoxNewSla.Text;
            termActivity.TerminalActivityId = Int32.Parse(RadComboBoxTerminalActivity.SelectedValue);
            termActivity.UserId = new Guid(userId);
            termActivity.AccountingFlag = CheckBoxAccountingFlag.Checked;
            termActivity.Comment = RadTextBoxComment.Text;

            if (NewFlag)
            {
                try
                {
                    boLogControl.LogTerminalActivity(termActivity);
                    LabelStatus.ForeColor = Color.DarkGreen;
                    LabelStatus.Text = "Insert of new activity succeeded!";
                }
                catch (Exception ex)
                {
                    CmtApplicationException cmtEx = new CmtApplicationException();
                    cmtEx.ExceptionDateTime = DateTime.Now;
                    cmtEx.ExceptionDesc = ex.Message;
                    cmtEx.UserDescription = "Could not insert new activity message";
                    cmtEx.ExceptionStacktrace = ex.StackTrace;
                    boLogControl.LogApplicationException(cmtEx);

                    LabelStatus.ForeColor = Color.DarkRed;
                    LabelStatus.Text = "Insert of new activity failed!";
                }
            }
            else
            {
                try
                {
                    //Read the ActivityId from the hiddenfield
                    termActivity.Id = Int32.Parse(HiddenFieldActivityId.Value);

                    if (boLogControl.UpdateTerminalActivity(termActivity))
                    {
                        LabelStatus.ForeColor = Color.DarkGreen;
                        LabelStatus.Text = "Update of new activity succeeded!";
                    }
                    else
                    {
                        LabelStatus.ForeColor = Color.DarkRed;
                        LabelStatus.Text = "Update of new activity failed!";
                    }
                }
                catch (Exception ex)
                {
                    CmtApplicationException cmtEx = new CmtApplicationException();
                    cmtEx.ExceptionDateTime = DateTime.Now;
                    cmtEx.ExceptionDesc = ex.Message;
                    cmtEx.UserDescription = "Could not insert new activity message";
                    cmtEx.ExceptionStacktrace = ex.StackTrace;
                    boLogControl.LogApplicationException(cmtEx);

                    LabelStatus.ForeColor = Color.DarkRed;
                    LabelStatus.Text = "Update of new activity failed!";
                }
            }
        }

        /// <summary>
        /// Returns the ServicePack identifier for a given service pack name
        /// </summary>
        /// <param name="servicePackName">the service pack name</param>
        /// <param name="serviceLevels">A list of service packs</param>
        /// <returns>the service pack identifier. This value can be null</returns>
        private int? GetServicePackIdByName(string servicePackName, string macAddress)
        {
            int? servicePackId = null;
            Boolean found = false;
            int idx = 0;

            //First we need to get the ISP for the given terminal
            Terminal term = _boAccountingControl.GetTerminalDetailsByMAC(macAddress);
            if (term != null)
            {
                ServiceLevel[] serviceLevels = _boAccountingControl.GetServicePacksByIsp(term.IspId);

                while (idx < serviceLevels.Length && !found)
                {
                    if (serviceLevels[idx].SlaName.Trim().Equals(servicePackName))
                    {
                        servicePackId = serviceLevels[idx].SlaId;
                        found = true;
                    }
                    idx++;
                }
            }
            else
            {
                servicePackId = 0;
            }
            return servicePackId;
        }
    }
}