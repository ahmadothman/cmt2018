﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ToolsTab.ascx.cs" Inherits="FOWebApp.Nocsa.ToolsTab" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %> 
<script type="text/javascript">
    function onSyncButtonClicked(button, args) {
        if (window.confirm('Are you sure you want to synchronize this terminal?')) {
            button.set_autoPostBack(true);
        }
        else {
            button.set_autoPostBack(false);
        }
    }

    function onAdmStatusButtonClicked(button, args) {
        if (window.confirm('Are you sure you want to change the admin status this terminal?')) {
            button.set_autoPostBack(true);
        }
        else {
            button.set_autoPostBack(false);
        }
    }
</script>
<asp:Panel ID="PanelSynchronize" runat="server" 
    GroupingText="Synchronize with HUB" ForeColor="White">
<asp:HiddenField ID="HiddenFieldISP" runat="server" />
<asp:HiddenField ID="HiddenFieldMacAddress" runat="server" />
<table border="0" cellspacing="5px" class="termDetails" frame="void">
    <tr>
        <td>
            <asp:Label ID="Label2" runat="server" Text="Terminal External Name:"></asp:Label> 
        </td>
        <td>
            <asp:Label ID="LabelExtFullName" runat="server" 
                ToolTip="This is the name used to access the terminal in the Teleport Hub" 
                ForeColor="White"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <telerik:RadButton ID="RadButtonSynchronize" runat="server" Text="Synchronize" 
                onclick="RadButtonSynchronize_Click" OnClientClicked="onSyncButtonClicked" ToolTip="Pulls the name of the terminal from the hub and sets it as Terminal External Name in the CMT (here above). The Terminal Name in the CMT (higher above) remains unchanged."
                Skin="WebBlue">
            </telerik:RadButton>
        </td>
        <td>
            <asp:Label ID="LabelResult" runat="server"></asp:Label>
        </td>
    </tr>
</table>
</asp:Panel>
<br />
<asp:Panel ID="PanelModifyAdmStatus" runat="server" 
    GroupingText="Modify Admin Status" ForeColor="White">
<table border="0" cellspacing="5px" class="termDetails" frame="void">
    <tr>
        <td>
            <asp:Label ID="Label3" runat="server" Text="Change Administration Status:"></asp:Label>
        </td>
        <td>
            <telerik:RadComboBox ID="RadComboBoxAdminStatus" runat="server">
            </telerik:RadComboBox>
        </td>
    </tr>
    <tr>
        <td>
            <telerik:RadButton ID="RadButtonSubmit" runat="server" Text="Change Status" 
                onclick="RadButtonSubmit_Click" Skin="WebBlue" OnClientClicked="onAdmStatusButtonClicked">
            </telerik:RadButton>
        </td>
        <td>
            <asp:Label ID="LabelAdmStatusChangeResult" runat="server"></asp:Label>
        </td>
    </tr>
</table>
</asp:Panel>
