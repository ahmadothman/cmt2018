﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using FOWebApp.net.nimera.cmt.BOMonitorControlWSRef;


namespace FOWebApp.Nocsa
{
    public partial class PreProvisioningForm : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ButtonPreProv_Click(object sender, EventArgs e)
        {
            string macAddress = TextBoxMacAddress.Text;

            BOMonitorControlWS boMonitorControlWS = new BOMonitorControlWS();
            if (boMonitorControlWS.PreProvisionTerminal(macAddress))
            {
                LabelResult.ForeColor = Color.DarkGreen;
                LabelResult.Text = "This terminal has been successfully pre-provisioned!";
            }
            else
            {
                LabelResult.ForeColor = Color.Red;
                LabelResult.Text = "Pre-Provisioning failed, this terminal might already exist!";
            }

        }
    }
}