﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NOCSAMenuAjax.ascx.cs" Inherits="FOWebApp.Nocsa.NOCSAMenuAjax" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<telerik:RadTreeView ID="RadTreeViewDistributorMenu" runat="server" 
    onnodeclick="RadTreeViewDistributorMenu_NodeClick" Skin="WebBlue" 
    ValidationGroup="Administrator main menu">
    <Nodes>
        <telerik:RadTreeNode runat="server" Expanded="True" 
            ImageUrl="~/Images/earth_preferences.png" Owner="RadTreeViewDistributorMenu" 
            Text="NOCSA Menu" Font-Bold="True" Value="n_Tasks">
            <Nodes>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/satellite_dish.png" 
                    Text="ISP" Value="n_ISP">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Text="New ISP" Value="n_New_ISP">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/businesspeople.png" 
                    Owner="" Text="Distributors" Value="n_Distributors">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Owner="" Text="New distributor" 
                            Value="n_New_Distributor">
                        </telerik:RadTreeNode>
                     </Nodes>
                </telerik:RadTreeNode>
<telerik:RadTreeNode runat="server" Owner="" Text="Terminals" Value="n_Terminals" 
                    ImageUrl="~/Images/Terminal.png"><Nodes>
<%--<telerik:RadTreeNode runat="server" Owner="" Text="New Terminal" Value="n_New_Terminal"></telerik:RadTreeNode>--%>
        <telerik:RadTreeNode runat="server" Owner="" Text="Search By MAC" 
            Value="n_Search_By_MAC" Visible="False">
        </telerik:RadTreeNode>
        <telerik:RadTreeNode runat="server" Owner="" Text="Search By SitId" 
            Value="n_Search_By_SitId" Visible="False">
        </telerik:RadTreeNode>
        <telerik:RadTreeNode runat="server" Text="Search" 
            Value="n_Advanced_Search">
        </telerik:RadTreeNode>
</Nodes>
</telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/radio.png" Text="Multicast Groups" ToolTip="Multicast group management" Value="n_Multicast">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Text="New Multicast Group" Value="n_NewMulticast">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/businesspeople.png" 
                    Owner="" Text="Customers" Value="n_Customers">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Owner="" Text="New Customer" 
                            Value="n_New_Customer">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/customers.png" 
                    Text="End Users" Value="n_EndUsers">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Text="New End User" Value="n_NewEndUser">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/form_blue.png"  
                    Text="Requests" Value="n_Requests">
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/lock_ok.png" Text="Change Password" 
                    ToolTip="Show account information" Value="n_Change_Password">
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/Logs.png" Owner="" 
                    Text="Logs" Value="n_Logs">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Owner="" Text="Usage Log" 
                            Value="n_Usage_Log">
                        </telerik:RadTreeNode>
                        <telerik:RadTreeNode runat="server" Owner="" Text="Activity Log" 
                            Value="n_Activity_Log">
                            <Nodes>
                                <telerik:RadTreeNode runat="server" Text="New Activity" Value="n_New_Activity">
                                </telerik:RadTreeNode>
                            </Nodes>
                        </telerik:RadTreeNode>
                        <telerik:RadTreeNode runat="server" Owner="" Text="Error Log" 
                            Value="n_Error_Log">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/report.png" 
                    Text="Reports" Value="n_Reports">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Text="Accounting" 
                            Value="n_Reports_Accounting">
                        </telerik:RadTreeNode>
                        <telerik:RadTreeNode runat="server" Text="Voucher Batches" 
                            Value="n_VoucherBatchesReport">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/ticket_blue.png" 
                    Text="Vouchers" Value="n_Vouchers">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Text="Create Vouchers" 
                            Value="n_CreateVouchers">
                        </telerik:RadTreeNode>
                        <telerik:RadTreeNode runat="server" Text="Available vouchers" 
                            Value="n_Available">
                        </telerik:RadTreeNode>
                        <telerik:RadTreeNode runat="server" Text="Validate voucher" 
                            Value="n_ValidateVoucher">
                        </telerik:RadTreeNode>
                        <telerik:RadTreeNode runat="server" Text="Reset voucher" 
                            Value="n_ResetVoucher">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" Text="Issues" ImageUrl="~/Images/user_headset.png" Value="n_Issues" ToolTip="Show issues">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Text="New Issue" Value="n_NewIssue" ToolTip="Submit a new issue">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/key.png" Owner="" 
                    Text="System Users" Value="n_Users">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Owner="" Text="New User" Value="n_New_User">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/exit.png" Text="Log Off" 
                    ToolTip="Close the session" Value="n_log_off">
                </telerik:RadTreeNode>
            </Nodes>
        </telerik:RadTreeNode>
        <telerik:RadTreeNode runat="server" ImageUrl="~/Images/information.png" 
            Text="About" Value="n_About">
        </telerik:RadTreeNode>
        <telerik:RadTreeNode runat="server" ImageUrl="~/Images/configuration.png" 
            Text="Configuration" Value="n_Configuration">
            <Nodes>
                <telerik:RadTreeNode runat="server" Text="Terminal States" 
                    Value="n_TerminalStates" Visible="False">
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" Text="User Activity Types" 
                    Value="n_UserActivity_Types" Visible="False">
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" Text="Service Packs" 
                    Value="n_Service_Packs">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Text="New Service Pack" 
                            Value="n_New_ServicePack">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" Text="Free Zones" 
                    Value="n_FreeZones">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Text="New Free Zone" 
                            Value="n_New_FreeZone">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" Text="Services" 
                    Value="n_Services">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Text="Create New Service" 
                            Value="n_Create_New_Service">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" Text="Procedures" Value="n_Procedures" 
                    Visible="False">
                </telerik:RadTreeNode>
            </Nodes>
        </telerik:RadTreeNode>
        <telerik:RadTreeNode runat="server" ImageUrl="~/Images/server_to_client.png" 
            Owner="RadTreeViewDistributorMenu" Text="NMS" Value="n_NMS">
            <Nodes>
                <telerik:RadTreeNode runat="server" Text="NMS Dashboard" Value="n_NMSDashboard">
                </telerik:RadTreeNode>
            </Nodes>
        </telerik:RadTreeNode>
    </Nodes>
</telerik:RadTreeView>