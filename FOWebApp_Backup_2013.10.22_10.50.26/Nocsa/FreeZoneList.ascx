﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FreeZoneList.ascx.cs" Inherits="FOWebApp.Nocsa.FreeZoneList" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script type="text/javascript">
    function RowSelected(sender, eventArgs) {
        var grid = sender;
        var masterTable = grid.get_masterTableView();
        var dataItems = masterTable.get_selectedItems();
        if (dataItems.length != 0) {
            var id = dataItems[0].get_element().cells[0].innerHTML;
            var oWnd = radopen("Nocsa/FreeZoneDetailsPage.aspx?id=" + id, "RadWindowFreeZoneDetails");
        }
    }
</script>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="RadGridFreeZones">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGridFreeZones" />
                <telerik:AjaxUpdatedControl ControlID="RadWindowManager1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" />
<telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" Height="200px" 
    Width="467px" HorizontalAlign="NotSet" 
    LoadingPanelID="RadAjaxLoadingPanel1">
<telerik:RadGrid ID="RadGridFreeZones" runat="server" AllowSorting="True"
    AutoGenerateColumns="False" CellSpacing="0" GridLines="None" Skin="WebBlue"
    ShowStatusBar="True" Width="992px" AllowFilteringByColumn="False" 
        onitemdatabound="RadGridFreeZones_ItemDataBound" 
        onpagesizechanged="RadGridFreeZones_PageSizeChanged" 
        PageSize="50" onitemcommand="RadGridFreeZones_ItemCommand">
        <GroupingSettings CaseSensitive="false" /> 
    <ClientSettings>
        <Selecting AllowRowSelect="True" />
        <ClientEvents OnRowSelected="RowSelected" />
    </ClientSettings>
    <MasterTableView CommandItemDisplay="Top" CommandItemSettings-ShowExportToExcelButton="False" 
        CommandItemSettings-ShowExportToCsvButton="False" RowIndicatorColumn-Visible="True" RowIndicatorColumn-Resizable="True" RowIndicatorColumn-AndCurrentFilterFunction="Contains" CommandItemSettings-ShowAddNewRecordButton="False" ShowFooter="False">
        <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
        </RowIndicatorColumn>
        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
        </ExpandCollapseColumn>
        <Columns>
            <telerik:GridBoundColumn DataField="FreeZoneId"
                FilterControlAltText="Filter IdColumn column" HeaderText="ID" 
                UniqueName="Key" ShowFilterIcon="False" AutoPostBackOnFilter="True">
                <ItemStyle Wrap="True" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="FreeZoneName"
                FilterControlAltText="Filter NameColumn column" HeaderText="Name" 
                UniqueName="Key" ShowFilterIcon="False" AutoPostBackOnFilter="True">
                <ItemStyle Wrap="True" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="CurrentActive"
                FilterControlAltText="Filter ActiveColumn column" HeaderText="Active" 
                UniqueName="Key" ShowFilterIcon="False" AutoPostBackOnFilter="True">
                <ItemStyle Wrap="True" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Monday"
                FilterControlAltText="Filter MondayColumn column" HeaderText="Monday" 
                UniqueName="Key" ShowFilterIcon="False" AutoPostBackOnFilter="True">
                <ItemStyle Wrap="True" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Tuesday"
                FilterControlAltText="Filter TuesdayColumn column" HeaderText="Tuesday" 
                UniqueName="Key" ShowFilterIcon="False" AutoPostBackOnFilter="True">
                <ItemStyle Wrap="True" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Wednesday"
                FilterControlAltText="Filter WednesdayColumn column" HeaderText="Wednesday" 
                UniqueName="Key" ShowFilterIcon="False" AutoPostBackOnFilter="True">
                <ItemStyle Wrap="True" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Thursday" 
                FilterControlAltText="Filter ThursdayColumn column" HeaderText="Thursday" 
                UniqueName="Key" ShowFilterIcon="False" AutoPostBackOnFilter="True">
                <ItemStyle Wrap="True" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Friday" 
                FilterControlAltText="Filter FridayColumn column" HeaderText="Friday" 
                UniqueName="Key" ShowFilterIcon="False" AutoPostBackOnFilter="True">
                <ItemStyle Wrap="True" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Saturday"
                FilterControlAltText="Filter SaturdayColumn column" HeaderText="Saturday" 
                UniqueName="Key" ShowFilterIcon="False" AutoPostBackOnFilter="True">
                <ItemStyle Wrap="True" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Sunday" 
                FilterControlAltText="Filter SundayColumn column" HeaderText="Sunday" 
                UniqueName="Key" ShowFilterIcon="False" AutoPostBackOnFilter="True">
                <ItemStyle Wrap="True" />
            </telerik:GridBoundColumn>
        </Columns>
        <EditFormSettings>
            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
            </EditColumn>
        </EditFormSettings>
    </MasterTableView>
    <FilterMenu EnableImageSprites="False">
    </FilterMenu>
    <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default">
    </HeaderContextMenu>
</telerik:RadGrid>
 <p>
     Times are OffTime-OnTime: Freezone stops on the first, and starts again on the second.
 </p>
<telerik:RadWindowManager ID="RadWindowManager1" runat="server" Skin="Vista" 
        DestroyOnClose="True">
    <Windows>
        <telerik:RadWindow ID="RadWindowFreeZoneDetails" runat="server" 
            NavigateUrl="NOCSA/FreeZoneDetailsPage.aspx" Animation="Fade" Opacity="100" Skin="WebBlue" 
            Title="Free Zone Details" AutoSize="False" Width="1000px" Height="700px" KeepInScreenBounds="True" EnableShadow="True" EnableAriaSupport="True" VisibleStatusbar="True" Modal="False" DestroyOnClose="False">
        </telerik:RadWindow>
    </Windows>
</telerik:RadWindowManager>
</telerik:RadAjaxPanel>