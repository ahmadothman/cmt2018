﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Services.ascx.cs" Inherits="FOWebApp.Nocsa.Services" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script type="text/javascript">
    function RowSelected(sender, eventArgs) {
        var grid = sender;
        var masterDataView = grid.get_masterTableView();
        var rowNum = eventArgs.get_itemIndexHierarchical();
        var row = masterDataView.get_dataItems()[rowNum];
        var serviceId = masterDataView.getCellByColumnUniqueName(row, "ServiceId").innerHTML;

        //Initialize and show the service details window
        var oWnd = radopen("Nocsa/ServiceDetailsPage.aspx?serviceId=" + serviceId, "RadWindowServiceDetails");
    }
</script>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="RadGridServices">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGridServices" />
                <telerik:AjaxUpdatedControl ControlID="RadWindowManager1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" />
<telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" Height="200px" 
    Width="467px" HorizontalAlign="NotSet" 
    LoadingPanelID="RadAjaxLoadingPanel1">
<telerik:RadGrid ID="RadGridServices" runat="server" AllowSorting="True"
    AutoGenerateColumns="False" CellSpacing="0" GridLines="None" Skin="WebBlue"
    ShowStatusBar="True" Width="992px" AllowFilteringByColumn="False" 
        onitemdatabound="RadGridServices_ItemDataBound" 
        onpagesizechanged="RadGridServices_PageSizeChanged" 
        PageSize="50" onitemcommand="RadGridServices_ItemCommand" ondeletecommand="RadGridServices_DeleteCommand">
        <GroupingSettings CaseSensitive="false" /> 
    <ClientSettings>
        <Selecting AllowRowSelect="True" />
        <ClientEvents OnRowSelected="RowSelected" />
    </ClientSettings>
    <MasterTableView CommandItemDisplay="Top" CommandItemSettings-ShowExportToExcelButton="False" 
        CommandItemSettings-ShowExportToCsvButton="False" RowIndicatorColumn-Visible="True" RowIndicatorColumn-Resizable="True" RowIndicatorColumn-AndCurrentFilterFunction="Contains" CommandItemSettings-ShowAddNewRecordButton="False" ShowFooter="False" DataKeyNames="ServiceId">
        <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
        </RowIndicatorColumn>
        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
        </ExpandCollapseColumn>
        <Columns>
            <telerik:GridBoundColumn DataField="ServiceId" 
                FilterControlAltText="Filter ServiceIdColumn column" HeaderText="Service ID" 
                UniqueName="ServiceId" ShowFilterIcon="False" AutoPostBackOnFilter="True" Visible="true">
                <ItemStyle Wrap="True" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="ServiceName" FilterControlAltText="Filter ServiceNameColumn column"
                HeaderText="Name" ReadOnly="True" Resizable="False" 
                UniqueName="ServiceName" CurrentFilterFunction="Contains" 
                ShowFilterIcon="False" AutoPostBackOnFilter="True">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="ServiceController" FilterControlAltText="Filter ServiceControllerColumn column"
                HeaderText="Controller" ReadOnly="True" UniqueName="ServiceController" 
                ShowFilterIcon="False" AutoPostBackOnFilter="True">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Level" FilterControlAltText="Filter ServiceLevelColumn column"
                HeaderText="Level" ReadOnly="True" UniqueName="Level" 
                ShowFilterIcon="False" AutoPostBackOnFilter="True">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="ParentServiceId" FilterControlAltText="Filter ParentServiceColumn column"
                HeaderText="Parent" ReadOnly="True" UniqueName="ParentServiceId" 
                ShowFilterIcon="False" AutoPostBackOnFilter="True">
            </telerik:GridBoundColumn>
            <telerik:GridButtonColumn HeaderText="Delete" ImageUrl="~/Images/delete.png" ConfirmText="Are you sure you want to delete this service?"
                UniqueName="DeleteColumn" ButtonType="ImageButton" HeaderButtonType="PushButton"
                Text="Delete" CommandName="Delete">
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </telerik:GridButtonColumn>
        </Columns>
        <EditFormSettings>
            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
            </EditColumn>
        </EditFormSettings>
    </MasterTableView>
    <FilterMenu EnableImageSprites="False">
    </FilterMenu>
    <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default">
    </HeaderContextMenu>
</telerik:RadGrid>

<telerik:RadWindowManager ID="RadWindowManager1" runat="server" Skin="Vista" 
        DestroyOnClose="True">
    <Windows>
        <telerik:RadWindow ID="RadWindowServiceDetails" runat="server" 
            NavigateUrl="Nocsa/ServiceDetailsPage.aspx" Animation="Fade" Opacity="100" Skin="WebBlue" 
            Title="Service Details" AutoSize="False" Width="1000px" Height="700px" KeepInScreenBounds="True" EnableShadow="True" EnableAriaSupport="True" VisibleStatusbar="True" Modal="False" DestroyOnClose="False">
        </telerik:RadWindow>
    </Windows>
</telerik:RadWindowManager>
</telerik:RadAjaxPanel>