﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MultiCastDetailsForm.aspx.cs" Inherits="FOWebApp.Nocsa.MultiCastDetailsForm" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Multicast Details Form</title>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            function onButtonClicked(sender, args) {
                var button = args.get_item();
                buttonClicked = button.get_index();
                if (button.get_text() == "Save") {
                    radconfirm('Are you sure you want to save?', confirmSaveCallBackFn, 400, 200, null, 'You are about to update a Multicast Group!');
                }

                if (button.get_text() == "Enable") {
                    radconfirm('Are you sure you want to Enable this Multicast Group?', confirmEnableCallBackFn, 400, 200, null, 'You are about to Enable a Multicast Group!');
                }

                if (button.get_text() == "Disable") {
                    radconfirm('Are you sure you want to Disable this Multicast Group?', confirmDisableCallBackFn, 400, 200, null, 'You are about to Disable a Multicast Group!');
                }
            }

            function confirmSaveCallBackFn(arg) {
                if (arg == true) {
                    __doPostBack("RadToolBarMultiCastGroup", buttonClicked); //save button idx.
                }
            }

            function confirmDisableCallBackFn(arg) {
                if (arg == true) {
                    __doPostBack("RadToolBarMultiCastGroup", buttonClicked); //Disable button idx.
                }
            }

            function confirmEnableCallBackFn(arg) {
                if (arg == true) {
                    __doPostBack("RadToolBarMultiCastGroup", buttonClicked); //Enable button idx.
                }
            }
        </script>
    </telerik:RadCodeBlock>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div>
            <telerik:RadToolBar ID="RadToolBarMultiCastGroup" runat="server" BorderStyle="Groove" BorderWidth="1px"
                EnableRoundedCorners="True" EnableShadows="True" Skin="WebBlue" Width="100%"
                OnClientButtonClicked="onButtonClicked" OnButtonClick="RadToolBarMultiCastGroup_ButtonClick">
                <Items>
                    <telerik:RadToolBarButton runat="server" ImageUrl="~/floppy_disk.png" Owner="RadToolBarMultiCastGroup"
                        Text="Save" ToolTip="Submit Multicast Group data" Value="Save_Button" CommandName="Save" PostBack="False">
                    </telerik:RadToolBarButton>
                    <telerik:RadToolBarButton runat="server" ImageUrl="~/Images/gear_run.png" Text="Enable"
                        Value="Enable_Button" Enabled="False" ToolTip="Enables the Multicast Group" PostBack="False">
                    </telerik:RadToolBarButton>
                    <telerik:RadToolBarButton runat="server" Enabled="False" ImageUrl="~/Images/stop.png"
                        Text="Disable" ToolTip="Disables the Multicast Group" Value="Disable_Button"
                        PostBack="False">
                    </telerik:RadToolBarButton>
                </Items>
            </telerik:RadToolBar>
            <table cellpadding="10" cellspacing="5">
                <tr>
                    <td class="requiredLabel">IP Address:</td>
                    <td>
                        <asp:TextBox ID="TextBoxIPAddress" runat="server" ControlToValidate="TextBoxIPAddress" Enabled="False"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="You must provide an IP Address" ControlToValidate="TextBoxIPAddress"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>Mac Address:</td>
                    <td>
                        <asp:TextBox ID="TextBoxMacAddress" runat="server" Width="280px" Enabled="False"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>Name:</td>
                    <td class="requiredLabel">
                        <asp:TextBox ID="TextBoxName" runat="server" Width="430px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="You must provide a Name" ControlToValidate="TextBoxName"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>Source URL:</td>
                    <td>
                        <asp:TextBox ID="TextBoxSourceURL" runat="server" Width="427px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>Port Nr:</td>
                    <td>
                        <asp:TextBox ID="TextBoxPortNr" runat="server" Width="45px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>Bandwidth:</td>
                    <td>
                        <asp:TextBox ID="TextBoxBandwidth" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>State:</td>
                    <td>
                        <asp:CheckBox ID="CheckBoxState" runat="server" Enabled="False" />
                    </td>
                </tr>
                <tr>
                    <td>IspId:</td>
                    <td>
                        <asp:Label ID="LabelISP" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>FirstActivationDate:</td>
                    <td>
                        <telerik:RadDatePicker ID="RadDatePickerFirstActivationDate" runat="server" Skin="WebBlue">
                            <Calendar ID="Calendar4" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False"
                                ViewSelectorText="x" Skin="WebBlue" runat="server">
                            </Calendar>
                            <DateInput ID="DateInput2" DisplayDateFormat="d/MM/yyyy" DateFormat="d/MM/yyyy" runat="server">
                            </DateInput>
                            <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                        </telerik:RadDatePicker>
                    </td>
                </tr>
                <tr>
                    <td>Distributor:</td>
                    <td>
                        <telerik:RadComboBox ID="RadComboBoxDistributor" runat="server" Width="422px"></telerik:RadComboBox>
                    </td>
                </tr>
            </table>
        </div>
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server" Skin="Sunset" EnableShadow="True"
            Left="0px" Right="0px" Modal="True" VisibleStatusbar="False">
        </telerik:RadWindowManager>
    </form>
</body>
</html>
