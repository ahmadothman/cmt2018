﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using Telerik.Web.UI;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;

namespace FOWebApp.Nocsa
{
    public partial class AdvancedSearch : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Load the RadComboBoxStatus
            BOAccountingControlWS boAccountingControl =
                                                new BOAccountingControlWS();
            TerminalStatus[] termStatusList = boAccountingControl.GetTerminalStatus();

            foreach (TerminalStatus termStatus in termStatusList)
            {
                RadComboBoxItem item = new RadComboBoxItem();
                if (termStatus.Id != 6)
                {
                    item.Value = termStatus.Id.ToString();
                    item.Text = termStatus.StatusDesc;
                    RadComboBoxStatus.Items.Add(item);
                }
            }

            if (this.Session["advancedSearch_Bound"] != null)
            {
                if ((bool)this.Session["advancedSearch_Bound"])
                {
                    RadGridTerminals.Rebind();

                    //Set the sort expression
                    if (this.Session["advancedSearch_SortExpression"] != null)
                    {
                        string sortExpression = (string)this.Session["advancedSearch_SortExpression"];
                        GridSortExpression gridSortExpression = new GridSortExpression();
                        gridSortExpression.FieldName = sortExpression;
                        gridSortExpression.SortOrder = (GridSortOrder)this.Session["advancedSearch_SortOrder"];
                        RadGridTerminals.MasterTableView.SortExpressions.Add(gridSortExpression);
                    }

                    if (this.Session["advancedSearch_PageSize"] != null)
                    {
                        RadGridTerminals.MasterTableView.PageSize = (int)this.Session["advancedSearch_PageSize"];
                        RadGridTerminals.PageSize = (int)this.Session["advancedSearch_PageSize"];
                    }


                    if (this.Session["advancedSearch_PageIdx"] != null)
                    {
                        RadGridTerminals.CurrentPageIndex =
                                    (int)this.Session["advancedSearch_PageIdx"];
                    }
                }
            }
        }

        protected void ButtonQuery_Click(object sender, EventArgs e)
        {
            //Store the selection in session variables
            if (RadioButtonStatus.Checked)
            {
                this.Session["advancedSearch_Status"] = true;
                this.Session["advancedSearch_ExpiryDate"] = false;
                this.Session["advancedSearch_MacAddress"] = false;
                this.Session["advancedSearch_StatusValue"] = Int32.Parse(RadComboBoxStatus.SelectedValue);
                RadGridTerminals.DataSource = this.getTerminalSelection();
                RadGridTerminals.Rebind();
            }
            else if (RadioButtonMacAddress.Checked)
            {
                this.Session["advancedSearch_Status"] = false;
                this.Session["advancedSearch_ExpiryDate"] = false;
                this.Session["advancedSearch_MacAddress"] = true;
                this.Session["advancedSearch_MacAddressValue"] = TextBoxMacAddress.Text;
                RadGridTerminals.DataSource = this.getTerminalSelection();
                RadGridTerminals.Rebind();
            }
            else if (RadioButtonExpiryDate.Checked)
            {
                this.Session["advancedSearch_Status"] = false;
                this.Session["advancedSearch_ExpiryDate"] = true;
                this.Session["advancedSearch_MacAddress"] = false;
                this.Session["advancedSearch_ExpiryDateValue"] = RadDatePickerExpiryDate.SelectedDate;
                this.Session["advancedSearch_SelectionOperator"] = RadComboBoxExpiryDate.SelectedValue;
                RadGridTerminals.DataSource = this.getTerminalSelection();
                RadGridTerminals.Rebind();
            }
            else
            {
                this.Session["advancedSearch_Status"] = false;
                this.Session["advancedSearch_ExpiryDate"] = false;
                this.Session["advancedSearch_MacAddress"] = false;
                RadGridTerminals.DataSource = this.getTerminalSelection();
                RadGridTerminals.Rebind();
            }
        }

        protected Terminal[] getTerminalSelection()
        {
            BOAccountingControlWS boAccountingControl =
                                                new BOAccountingControlWS();

            Terminal[] terminals = null;

            //Check the radio button and load the data into the datagrid
            if (this.Session["advancedSearch_Status"] != null)
            {
                if ((bool)this.Session["advancedSearch_Status"])
                {
                    int statusId = (int)this.Session["advancedSearch_StatusValue"];
                    terminals = boAccountingControl.GetTerminalsByStatus(statusId);
                }
            }

            if (this.Session["advancedSearch_MacAddress"] != null)
            {
                if ((bool)this.Session["advancedSearch_MacAddress"])
                {
                    string macAddress = (string)this.Session["advancedSearch_MacAddressValue"];
                    terminals = boAccountingControl.GetTerminalsByMac(macAddress);
                }
            }

            if (this.Session["advancedSearch_ExpiryDate"] != null)
            {
                if ((bool)this.Session["advancedSearch_ExpiryDate"])
                {
                    DateTime selectedDate = (DateTime)this.Session["advancedSearch_ExpiryDateValue"];
                    string selectionOperator = (string)this.Session["advancedSearch_SelectionOperator"];
                    terminals = boAccountingControl.GetTerminalsByExpiryDate(selectedDate, selectionOperator);
                }
            }

            return terminals;
        }

        protected void RadGridTerminals_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            RadGridTerminals.DataSource = this.getTerminalSelection();
            this.Session["advancedSearch_Bound"] = true;
        }

        protected void RadGridTerminals_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            //Save the page index
            int pageIdx = e.NewPageIndex;
            this.Session["advancedSearch_PageIdx"] = pageIdx;
        }

        protected void RadGridTerminals_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem dataItem = (GridDataItem)e.Item;

                if (dataItem["AdmStatusColumn"].Text.Equals("1"))
                {
                    dataItem["AdmStatusColumn"].Text = "Locked";
                    dataItem["AdmStatusColumn"].ForeColor = Color.Red;
                }

                if (dataItem["AdmStatusColumn"].Text.Equals("2"))
                {
                    dataItem["AdmStatusColumn"].Text = "Unlocked";
                    dataItem["AdmStatusColumn"].ForeColor = Color.Green;
                }

                if (dataItem["AdmStatusColumn"].Text.Equals("3"))
                {
                    dataItem["AdmStatusColumn"].Text = "Decommissioned";
                    dataItem["AdmStatusColumn"].ForeColor = Color.Blue;
                }

                if (dataItem["AdmStatusColumn"].Text.Equals("5"))
                {
                    dataItem["AdmStatusColumn"].Text = "Request";
                    dataItem["AdmStatusColumn"].ForeColor = Color.DarkOrange;
                }

                if (dataItem["AdmStatusColumn"].Text.Equals("6"))
                {
                    dataItem["AdmStatusColumn"].Text = "Deleted";
                    dataItem["AdmStatusColumn"].ForeColor = Color.Black;
                }
            }
        }

        protected void RadGridTerminals_SortCommand(object sender, GridSortCommandEventArgs e)
        {
            this.Session["advancedSearch_SortExpression"] = e.SortExpression;
            if (e.NewSortOrder != e.OldSortOrder)
            {
                this.Session["advancedSearch_SortOrder"] = e.NewSortOrder;
            }
            else
            {
                this.Session["advancedSearch_SortOrder"] = e.OldSortOrder;
            }
        }

        protected void RadGridTerminals_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            this.Session["advancedSearch_PageSize"] = e.NewPageSize;

        }

        protected void RadGridTerminals_PreRender(object sender, EventArgs e)
        {
            
        }

        /// <summary>
        /// Configures the export settings for this radgrid
        /// </summary>
        public void ConfigureExport()
        {
            RadGridTerminals.ExportSettings.FileName = "CMTExport";
            RadGridTerminals.ExportSettings.ExportOnlyData = true;
            RadGridTerminals.ExportSettings.IgnorePaging = true;
            RadGridTerminals.ExportSettings.OpenInNewWindow = true;
            RadGridTerminals.ExportSettings.Excel.Format = GridExcelExportFormat.Html;
            RadGridTerminals.ExportSettings.Csv.ColumnDelimiter = GridCsvDelimiter.Semicolon;
            RadGridTerminals.ExportSettings.Csv.EncloseDataWithQuotes = false;
            RadGridTerminals.ExportSettings.Csv.RowDelimiter = GridCsvDelimiter.NewLine;
        }

        protected void RadGridTerminals_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToExcelCommandName)
            {
                this.ConfigureExport();
                RadGridTerminals.MasterTableView.ExportToExcel();
            }

            if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToCsvCommandName)
            {
                this.ConfigureExport();
                RadGridTerminals.MasterTableView.ExportToCSV();
            }

            if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToPdfCommandName)
            {
                this.ConfigureExport();
                RadGridTerminals.MasterTableView.ExportToPdf();
            }

            if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToWordCommandName)
            {
                this.ConfigureExport();
                RadGridTerminals.MasterTableView.ExportToWord();
            }
        }
    }
}