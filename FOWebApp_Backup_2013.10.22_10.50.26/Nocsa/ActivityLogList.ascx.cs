﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;
using Telerik.Web.UI;

namespace FOWebApp.Nocsa
{
    public partial class ActivityLogList : System.Web.UI.UserControl
    {
        /// <summary>
        /// Determines the activity type (Term_Activity_Log, Exception_Log, User_Log)
        /// </summary>
        public string ActivityType { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            RadDatePickerStart.SelectedDate = DateTime.Now.AddDays(-1.0);
            RadDatePickerEnd.SelectedDate = DateTime.Now.AddDays(1.0);

            if (this.Session[ActivityType + "_Bound"] != null)
            {
                if ((bool)this.Session[ActivityType + "_Bound"])
                {
                    RadGridUserActivityLog.Rebind();

                    if (this.Session[ActivityType + "_SortExpression"]!= null)
                    {
                        string sortExpression = (string)this.Session[ActivityType + "_SortExpression"];
                        GridSortExpression gridSortExpression = new GridSortExpression();
                        gridSortExpression.FieldName = sortExpression;
                        gridSortExpression.SortOrder = GridSortOrder.Ascending;
                        RadGridUserActivityLog.MasterTableView.SortExpressions.Add(gridSortExpression);
                    }

                    if (this.Session[ActivityType + "_PageSize"] != null)
                    {
                        int pageSize = (int) this.Session[ActivityType + "_PageSize"];
                        RadGridUserActivityLog.PageSize = pageSize;
                    }

                    if (this.Session[ActivityType + "_PageIdx"] != null)
                    {
                        RadGridUserActivityLog.CurrentPageIndex =
                                    (int)this.Session[ActivityType + "_PageIdx"];
                    }
                }
            }
        }

        protected void ImageButtonQuery_Click(object sender, ImageClickEventArgs e)
        {
            DateTime startDate;
            DateTime endDate;

            if (!RadDatePickerStart.IsEmpty || !RadDatePickerEnd.IsEmpty)
            {
                startDate = (DateTime)RadDatePickerStart.SelectedDate;
                endDate = (DateTime)RadDatePickerEnd.SelectedDate;

                this.Session[ActivityType + "_StartDate"] = startDate;
                this.Session[ActivityType + "_EndDate"] = endDate;

                this.generateReport(startDate, endDate);
                RadGridUserActivityLog.DataBind();
                RadGridUserActivityLog.Visible = true;

            }
            else
            {
                //Check if values were stored in session variables
                if (this.Session[ActivityType + "_StartDate"] != null && this.Session[ActivityType + "_EndDate"] != null)
                {
                    startDate = (DateTime)this.Session[ActivityType + "_StartDate"];
                    endDate = (DateTime)this.Session[ActivityType + "_EndDate"];

                    this.generateReport(startDate, endDate);
                    RadGridUserActivityLog.DataBind();
                    RadGridUserActivityLog.Visible = true;
                }
                else
                {
                    RadGridUserActivityLog.Visible = false;
                }
            }
        }

        /// <summary>
        /// Generates the report depending on the selecte report type
        /// </summary>
        /// <param name="startDate">Start date of the report</param>
        /// <param name="endDate">End date of the report</param>
        private void generateReport(DateTime startDate, DateTime endDate)
        {
            //Fill data grid
            BOLogControlWS boLogControl = new BOLogControlWS();
            DataTable dt = null;

            if (ActivityType.Equals("User_Log"))
            {
                dt = boLogControl.GetUserActivityBetweenDates(startDate, endDate);
            }
            else if (ActivityType.Equals("Term_Activity_Log"))
            {
                dt = boLogControl.GetTermActivitiesBetweenDates(startDate, endDate);
            }
            //else
            //{
            //    dt = boLogControl.GetExceptionsBetweenDates(startDate, endDate);
            //}

            if (dt != null)
            {
                RadGridUserActivityLog.DataSource = dt;
                RadGridUserActivityLog.Visible = true;
                this.Session[ActivityType + "_Bound"] = true;
            }
            else
            {
                RadGridUserActivityLog.Visible = false;
            }
        }

        protected void RadGridUserActivityLog_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
        {
            //Save the page index
            int pageIdx = e.NewPageIndex;
            this.Session[ActivityType + "_PageIdx"] = pageIdx;
        }

        protected void RadGridUserActivityLog_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            DateTime startDate;
            DateTime endDate;

            if (!RadDatePickerStart.IsEmpty || !RadDatePickerEnd.IsEmpty)
            {
                startDate = (DateTime)RadDatePickerStart.SelectedDate;
                endDate = (DateTime)RadDatePickerEnd.SelectedDate;

                this.Session[ActivityType + "_StartDate"] = startDate;
                this.Session[ActivityType + "_EndDate"] = endDate;

                this.generateReport(startDate, endDate);
            }
            else
            {
                //Check if values were stored in session variables
                if (this.Session[ActivityType + "_StartDate"] != null && this.Session[ActivityType + "_EndDate"] != null)
                {
                    startDate = (DateTime)this.Session[ActivityType + "_StartDate"];
                    endDate = (DateTime)this.Session[ActivityType + "_EndDate"];

                    this.generateReport(startDate, endDate);
                }
                else
                {
                    RadGridUserActivityLog.Visible = false;
                }
            }
        }

        public void clearGrid()
        {
            this.Session[ActivityType + "_Bound"] = false;
        }

        protected void RadGridUserActivityLog_SortCommand(object sender, Telerik.Web.UI.GridSortCommandEventArgs e)
        {
            this.Session[ActivityType + "_SortExpression"] = e.SortExpression;
        }

        protected void RadGridUserActivityLog_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            this.Session[ActivityType + "_PageSize"] = e.NewPageSize;
        }

    }
}