﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Web.Security;
using Telerik.Web.UI;
using FOWebApp.Data;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;
using FOWebApp.net.nimera.cmt.BOMonitorControlWSRef;
using FOWebApp.net.nimera.cmt.BOTaskControlWSRef;

namespace FOWebApp.Nocsa
{
    public partial class TerminalForm : System.Web.UI.Page
    {
        BOAccountingControlWS _boAccountingControlWS = null;
        BOLogControlWS _boLogControlWS = null;
        BOMonitorControlWS _boMonitorControlWS = null;
        BOTaskControlWS _boTaskControlWS = null;
        DataHelper _dataHelper;

        const int _activateIdx = 3;
        const int _reactivateIdx = 4;
        const int _suspendIdx = 5;
        const int _decommissionIdx = 6;
        const int _deleteIdx = 7;
        const int _fupresetIdx = 8;

        public TerminalForm()
        {
            _boAccountingControlWS = new BOAccountingControlWS();
            _boLogControlWS = new BOLogControlWS();
            _boMonitorControlWS = new BOMonitorControlWS();
            _boTaskControlWS = new BOTaskControlWS();
            _dataHelper = new DataHelper();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Terminal terminal = null;

            if (!IsPostBack)
            {
                //Initialize the form
                string macAddress = Request.Params["mac"];

                if (macAddress != null)
                {
                    //Fill in the form
                    terminal = _boAccountingControlWS.GetTerminalDetailsByMAC(macAddress);

                    //Store the terminal as a session value for subsequent calss
                    this.Session["TerminalForm_Terminal"] = terminal;

                    this.loadForm(terminal);
                }

                if (Roles.IsUserInRole("NOC Administrator"))
                {
                    RadTabTools.Visible = true;
                }
            }
            else
            {
                //Just load the correct Service Packs
                terminal = (Terminal)this.Session["TerminalForm_Terminal"];
            }
        }

        protected void RadToolBarTerminal_ButtonClick(object sender, RadToolBarEventArgs e)
        {
            //Check the button commmand and execute the command
            RadToolBarItem toolbarItem = e.Item;
            if (toolbarItem.Value.Equals("Save_Button"))
            {
                //Save the data back to the database
                if (this.saveTerminalData())
                {
                    RadWindowManager1.RadAlert("Terminal data was saved successfully. If the SLA was changed, Wait approx. 10 minutes for this new SLA to change in the HUB", 250, 200, "Save terminal data", null);
                }
                else
                {
                    RadWindowManager1.RadAlert("Saving terminal data failed!", 250, 200, "Save terminal data", null);
                }
            }

            if (toolbarItem.Value.Equals("Delete_Button"))
            {
                Terminal term = (Terminal)this.Session["TerminalForm_Terminal"];

                //Set the delete flag in the database
                LabelStatus.Text = "Deleted";
                LabelStatus.ForeColor = Color.Black;
                this.saveTerminalData();

                //Update TerminalActivity log
                TerminalActivity terminalActivity = new TerminalActivity();
                terminalActivity.Action = "Delete";
                terminalActivity.ActionDate = DateTime.Now;
                terminalActivity.MacAddress = term.MacAddress;
                
                //User who did the delete
                MembershipUser myObject = Membership.GetUser();
                string userId = myObject.ProviderUserKey.ToString();

                terminalActivity.UserId = new Guid(userId);

                _boLogControlWS.LogTerminalActivity(terminalActivity);
            }

            if (toolbarItem.Value.Equals("Refresh_Button"))
            {
                Terminal term = (Terminal)this.Session["TerminalForm_Terminal"];
                if (term != null)
                {
                    this.loadForm(term);
                }
            }

            if (toolbarItem.Value.Equals("Activate_Button"))
            {
                Terminal term = (Terminal)this.Session["TerminalForm_Terminal"];

                //Check if the sitId has changed 
                if (_boMonitorControlWS.TerminalActivate(term.ExtFullName, term.MacAddress, (long)term.SlaId, term.IspId))
                {
                    RadWindowManager1.RadAlert("Terminal activation request sent to ISPIF.<br/>Wait approx. 10 minutes for completion!", 250, 200, "Terminal activation", null);
                    this.CreateNewTask("Activation", "Activation of a new terminal", term);
                }
                else
                {
                    RadWindowManager1.RadAlert("Terminal activation failed!", 250, 100, "Activation result", null);
                }
            }

            if (toolbarItem.Value.Equals("Suspend_Button"))
            {
                Terminal term = (Terminal)this.Session["TerminalForm_Terminal"];
                if (term.ExtFullName == null || term.ExtFullName.Equals(""))
                {
                    this.logError("term.ExtFullName is null or empty for SitId: " + term.SitId, "", "Suspending a terminal");
                    RadWindowManager1.RadAlert("Terminal suspension failed", 250, 100, "Suspension result", null);
                }
                else
                {
                    if (_boMonitorControlWS.TerminalSuspend(term.ExtFullName, term.MacAddress, term.IspId))
                    {
                        RadWindowManager1.RadAlert("Terminal suspension request sent to ISPIF.<br/>Wait approx. 10 minutes for completion!", 250, 200, "Terminal suspension", null);
                        this.CreateNewTask("Suspend", "Suspension of an existing terminal", term);
                    }
                    else
                    {
                        RadWindowManager1.RadAlert("Terminal suspension failed", 250, 100, "Suspension result", null);
                    }
                }
            }

            if (toolbarItem.Value.Equals("ReActivate_Button"))
            {
                Terminal term = (Terminal)this.Session["TerminalForm_Terminal"];
                if (_boMonitorControlWS.TerminalReActivate(term.ExtFullName, term.MacAddress, term.IspId))
                {
                    RadWindowManager1.RadAlert("Terminal re-activation request sent to ISPIF.<br/>Wait approx. 10 minutes for completion!", 250, 200, "Terminal re-activation", null);
                    this.CreateNewTask("Re-Activate", "Re-activation of an existing terminal", term);
                }
                else
                {
                    RadWindowManager1.RadAlert("Terminal re-activation failed", 250, 100, "Terminal re-activation", null);
                }

            }

            if (toolbarItem.Value.Equals("Decommission_Button"))
            {
                Terminal term = (Terminal)this.Session["TerminalForm_Terminal"];
                if (_boMonitorControlWS.TerminalDecommission(term.ExtFullName, term.MacAddress, term.IspId))
                {
                    RadWindowManager1.RadAlert("Terminal decommissioning request sent to ISPIF.<br/>Wait approx. 10 minutes for completion!", 250, 200, "Terminal decommissioning", null);
                    this.CreateNewTask("Decommissioning", "Decommissioning of a terminal", term);
                }
                else
                {
                    RadWindowManager1.RadAlert("Terminal decommissioning failed", 250, 100, "Terminal decommissioning", null);
                }
            }

            if (toolbarItem.Value.Equals("FUPReset_Button"))
            {
                Terminal term = (Terminal)this.Session["TerminalForm_Terminal"];
                if (_boMonitorControlWS.TerminalUserReset(term.ExtFullName, term.IspId))
                {
                    //Update TerminalActivity log
                    TerminalActivity terminalActivity = new TerminalActivity();
                    terminalActivity.Action = "FUPReset";
                    terminalActivity.ActionDate = DateTime.Now;
                    terminalActivity.MacAddress = term.MacAddress;
                    terminalActivity.AccountingFlag = true;

                    //User who did the delete
                    MembershipUser myObject = Membership.GetUser();
                    string userId = myObject.ProviderUserKey.ToString();

                    terminalActivity.UserId = new Guid(userId);

                    _boLogControlWS.LogTerminalActivity(terminalActivity);

                    RadWindowManager1.RadAlert("Terminal volume reset succeeded.", 250, 100, "FUP Reset", null);
                    this.CreateNewTask("FUP Reset", "Fair Usage Policy (FUP) Volume Reset", term);
                }
                else
                {
                    RadWindowManager1.RadAlert("Terminal volume reset failed.", 250, 100, "FUP Reset", null);
                }
            }

        }

        //Reads the data from the form and stores the information in the database
        private bool saveTerminalData()
        {
            bool result = false;
            bool slaChangedFlag = false; //Indicates if the SLA has been changed
            bool macAddressChangeFlag = false; //Indicates if the MAC Address has been changed
            bool ispChangeFlag = false; //Indicates if the ISP has been changed
            string newMacAddress = ""; //The new MAC Address
            Terminal terminal = (Terminal)this.Session["TerminalForm_Terminal"];

            try
            {
                //Check if the SLA has been changed.
                slaChangedFlag = (terminal.SlaId != Int32.Parse(RadComboBoxSLA.SelectedValue));

                //Check if the MAC Address has been changed.
                macAddressChangeFlag = (!terminal.MacAddress.Equals(TextBoxMacAddress.Text));

                //Check if the ISP Address has been changed
                ispChangeFlag = (!terminal.IspId.Equals(Int32.Parse(RadComboBoxISP.SelectedValue)));

                //Keep the new MAC Address, at this stage the terminal keeps the old MAC address in the
                //CMT. Only if the change of MAC address is reported as successfully by the TicketMaster
                //the terminal is recreated with a the new MAC address
                if (macAddressChangeFlag)
                {
                    newMacAddress = TextBoxMacAddress.Text;
                }

                if (!TextBoxSitId.Text.Equals(""))
                {
                    terminal.SitId = Int32.Parse(TextBoxSitId.Text);
                }

                terminal.FullName = TextBoxTerminalName.Text;

                if (!RadComboBoxISP.SelectedValue.Equals(""))
                {
                    terminal.IspId = Int32.Parse(RadComboBoxISP.SelectedValue);
                }


                terminal.Serial = TextBoxSerial.Text;

                if (!RadComboBoxSLA.SelectedValue.Equals(""))
                {
                    terminal.SlaId = Int32.Parse(RadComboBoxSLA.SelectedValue);
                }

                terminal.IpAddress = TextBoxIP.Text;
                terminal.IPMask = Int32.Parse(TextBoxIPMask.Text);
                terminal.IPRange = CheckBoxIPRange.Checked;
                terminal.TestMode = CheckBoxTestMode.Checked;

                //Address information
                if (terminal.Address != null)
                {
                    terminal.Address.AddressLine1 = TextBoxAddressLine1.Text;
                    terminal.Address.AddressLine2 = TextBoxAddressLine2.Text;
                    terminal.Address.Location = TextBoxLocation.Text;
                    terminal.Address.PostalCode = TextBoxPCO.Text;
                    terminal.Address.Country = CountryList.SelectedValue;
                }
                terminal.Phone = TextBoxPhone.Text;
                terminal.Fax = TextBoxFax.Text;
                terminal.Email = TextBoxEMail.Text;

                //Subscription
                if (!RadComboBoxCustomer.SelectedValue.Equals(""))
                {
                    terminal.OrgId = Int32.Parse(RadComboBoxCustomer.SelectedValue);
                }

                if (!RadComboBoxDistributor.SelectedValue.Equals(""))
                {
                    terminal.DistributorId = Int32.Parse(RadComboBoxDistributor.SelectedValue);
                }

                terminal.StartDate = RadDatePickerStartDate.SelectedDate;
                terminal.ExpiryDate = RadDatePickerExpiryDate.SelectedDate;
                terminal.AdmStatus = _dataHelper.getStatusId(LabelStatus.Text);

                //Geolocation tab
                Coordinate coordinates = new Coordinate();

                if (!TextBoxLatitude.Text.Equals(""))
                {
                    coordinates.Latitude = Double.Parse(TextBoxLatitude.Text);
                }

                if (!TextBoxLongitude.Text.Equals(""))
                {
                    coordinates.Longitude = Double.Parse(TextBoxLongitude.Text);
                }

                terminal.LatLong = coordinates;

                //Hub
                if (!TextBoxBlade.Text.Equals(""))
                {
                    terminal.Blade = Int32.Parse(TextBoxBlade.Text);
                }

                terminal.IPMgm = TextBoxIPMgm.Text;
                terminal.IPTunnel = TextBoxIPTunnel.Text;

                if (!TextBoxRTPool.Text.Equals(""))
                {
                    terminal.RTPool = Int32.Parse(TextBoxRTPool.Text);
                }

                if (!TextBoxFWPool.Text.Equals(""))
                {
                    terminal.FWPool = Int32.Parse(TextBoxFWPool.Text);
                }

                if (!TextBoxSpotID.Text.Equals(""))
                {
                    terminal.SpotID = Int32.Parse(TextBoxSpotID.Text);
                }

                //Remarks
                terminal.Remarks = TextBoxRemarks.Text;

                //Update in the new CMT Database
                result = _boAccountingControlWS.UpdateTerminal(terminal);

                if (result && terminal.AdmStatus != 5)
                {
                    //Update the SLA and MacAddress in the HUB if necessary, only if the status is NOT request
                    if (slaChangedFlag)
                    {
                        if (!_boMonitorControlWS.TerminalChangeSla(terminal.ExtFullName, terminal.MacAddress,
                                                         (int)terminal.SlaId, (AdmStatus)(terminal.AdmStatus), terminal.IspId))
                        {
                            result = false;
                            this.logError("Change of SLA for End UserId: " + terminal.FullName + " failed!",
                                            "Change of SLA failed", "");
                        }
                    }

                    if (result && macAddressChangeFlag)
                    {
                        if (!_boMonitorControlWS.ChangeMacAddress(terminal.ExtFullName, terminal.MacAddress, terminal.IspId, newMacAddress))
                        {
                            result = false;
                            this.logError("Change of Mac Address failed for End UserId: " + terminal.FullName + " failed!",
                                "Original Mac Address: " + terminal.MacAddress + ", Target Mac Address: " + newMacAddress,
                                "Method TerminalForm - saveTerminalData");
                        }
                        else
                        {
                            string oldMacAddress = terminal.MacAddress;
                            terminal.MacAddress = newMacAddress;
                            this.CreateNewTask("MacAddressChange", "Verify MAC address change. Original MAC Address: " + oldMacAddress + ", Target Mac Address: " + newMacAddress, terminal);
                        }
                    }

                    if (result && ispChangeFlag)
                    {
                        int newIsp = Int32.Parse(RadComboBoxISP.SelectedValue);
                        if (!_boMonitorControlWS.ChangeISP(terminal.ExtFullName, terminal.IspId, newIsp))
                        {
                            result = false;
                            this.logError("Change of ISP failed for End UserId: " + terminal.FullName, "Original ISP: " + terminal.IspId + ", Target ISP: " + terminal.IspId,
                                            "Method TerminalForm - saveTerminalData");
                        }
                    }
                }

                //Store new version in session variable
                if (result)
                {
                    this.Session["TerminalForm_Terminal"] = terminal;
                    this.loadForm(terminal);
                }
            }
            catch (Exception ex)
            {
                this.logError(ex.Message, "TerminalForm - saveTerminalData failed", ex.StackTrace);
                result = false;
            }

            return result;
        }

        /// <summary>
        /// Loads the form with the given terminal data
        /// </summary>
        /// <param name="terminal"></param>
        public void loadForm(Terminal terminal)
        {
            if (terminal != null)
            {
                //Setup the terminal management buttons
                this.initTerminalManagementButtons(terminal);

                TextBoxMacAddress.Text = terminal.MacAddress;

                if (terminal.AdmStatus == (int)AdmStatus.Request + 1)
                {
                    RadComboBoxISP.Enabled = true;
                }
                else
                {
                    RadComboBoxISP.Enabled = false;
                }

                if (terminal.AdmStatus == (int)AdmStatus.LOCKED + 1)
                {
                    TextBoxMacAddress.Enabled = true;
                    RadComboBoxSLA.Enabled = false;
                }
                else
                {
                    TextBoxMacAddress.Enabled = false;
                }

                if (terminal.AdmStatus == (int)AdmStatus.OPERATIONAL + 1)
                {
                    TextBoxMacAddress.Enabled = true;
                }
 
                TextBoxSitId.Text = terminal.SitId.ToString();
                TextBoxTerminalName.Text = terminal.FullName;
                TextBoxSerial.Text = terminal.Serial;

                if (terminal.TestMode != null)
                    CheckBoxTestMode.Checked = (bool)terminal.TestMode;

                List<Isp> IspList = _dataHelper.getISPs();
                RadComboBoxISP.Items.Clear();
                foreach (Isp isp in IspList)
                {
                    RadComboBoxItem item = new RadComboBoxItem();
                    item.Value = isp.Id.ToString();
                    item.Text = isp.CompanyName;
                    RadComboBoxISP.Items.Add(item);
                }

                RadComboBoxISP.SelectedValue = terminal.IspId.ToString();

                ServiceLevel[] slaItems = _boAccountingControlWS.GetServicePacksByIsp(terminal.IspId);
                RadComboBoxSLA.Items.Clear();
                foreach (ServiceLevel slaItem in slaItems)
                {
                    RadComboBoxItem item = new RadComboBoxItem();
                    item.Value = slaItem.SlaId.ToString();
                    item.Text = slaItem.SlaName.Trim();
                    RadComboBoxSLA.Items.Add(item);
                }

                RadComboBoxSLA.SelectedValue = terminal.SlaId.ToString();
                TextBoxIP.Text = terminal.IpAddress;

                if (terminal.IPRange != null)
                {
                    CheckBoxIPRange.Checked = (bool)terminal.IPRange;
                    if ((bool)terminal.IPRange)
                    {
                        TextBoxIPMask.Text = terminal.IPMask.ToString();
                        TextBoxIPMask.Enabled = true;
                    }
                    else
                    {
                        TextBoxIPMask.Text = "32";
                        TextBoxIPMask.Enabled = false;
                    }
                }
                else
                {
                    CheckBoxIPRange.Checked = false;
                    TextBoxIPMask.Enabled = false;
                }

                if (terminal.Address != null)
                {
                    TextBoxAddressLine1.Text = terminal.Address.AddressLine1;
                    TextBoxAddressLine2.Text = terminal.Address.AddressLine2;
                    TextBoxLocation.Text = terminal.Address.Location;
                    TextBoxPCO.Text = terminal.Address.PostalCode;
                    CountryList.SelectedValue = terminal.Address.Country;
                }
                TextBoxPhone.Text = terminal.Phone;
                TextBoxFax.Text = terminal.Fax;
                TextBoxEMail.Text = terminal.Email;

                //Load customers into the combobox
                List<Organization> organizations = _dataHelper.getOrganizations();

                foreach (Organization org in organizations)
                {
                    RadComboBoxItem item = new RadComboBoxItem();
                    item.Value = org.Id.ToString();
                    item.Text = org.FullName;
                    RadComboBoxCustomer.Items.Add(item);
                }

                RadComboBoxCustomer.SelectedValue = terminal.OrgId.ToString();

                //Load the Distributor combobox
                List<Distributor> distributors = _dataHelper.getDistributors();

                foreach (Distributor distributor in distributors)
                {
                    RadComboBoxItem item = new RadComboBoxItem();
                    item.Value = distributor.Id.ToString();
                    item.Text = distributor.FullName;
                    RadComboBoxDistributor.Items.Add(item);
                }

                RadComboBoxDistributor.SelectedValue = "" + terminal.DistributorId;

                RadDatePickerStartDate.SelectedDate = terminal.StartDate;
                RadDatePickerExpiryDate.SelectedDate = terminal.ExpiryDate;

                if (terminal.FirstActivationDate == null)
                {
                    terminal.FirstActivationDate = terminal.StartDate;
                }

                //Load status data
                LabelStatus.Text = _dataHelper.getStatusById((int)terminal.AdmStatus);
                this.Title = "Modify Terminal - " + terminal.MacAddress + " - " + LabelStatus.Text;
                if (terminal.AdmStatus == 1)
                    LabelStatus.ForeColor = Color.Red;

                if (terminal.AdmStatus == 2)
                    LabelStatus.ForeColor = Color.Green;

                if (terminal.AdmStatus == 3)
                    LabelStatus.ForeColor = Color.Blue;

                if (terminal.AdmStatus == 5)
                    LabelStatus.ForeColor = Color.DarkOrange;

                if (terminal.LatLong != null)
                {
                    TextBoxLatitude.Text = terminal.LatLong.Latitude.ToString();
                }

                if (terminal.LatLong != null)
                {
                    TextBoxLongitude.Text = terminal.LatLong.Longitude.ToString();
                }

                if (terminal.Blade != null)
                {
                    TextBoxBlade.Text = terminal.Blade.ToString();
                }

                if (terminal.IPMgm != null)
                {
                    TextBoxIPMgm.Text = terminal.IPMgm;
                }

                if (terminal.IPTunnel != null)
                {
                    TextBoxIPTunnel.Text = terminal.IPTunnel;
                }

                TextBoxRTPool.Text = terminal.RTPool.ToString();
                TextBoxFWPool.Text = terminal.FWPool.ToString();

                if (terminal.SpotID != null)
                {
                    TextBoxSpotID.Text = terminal.SpotID.ToString();
                }

                if (terminal.Remarks != null)
                {
                    TextBoxRemarks.Text = terminal.Remarks;
                }

                //Set the SitId and IspId to the terminal activity list control
                TermActivityList.IspId = terminal.IspId;
                TermActivityList.SitId = terminal.SitId;
                TermActivityList.Height = 800;

                //Set the MacAddress, IspId and FullName to the tools form
                TerminalTools.IspId = terminal.IspId;
                TerminalTools.MacAddress = terminal.MacAddress;
                TerminalTools.FullName = terminal.FullName;
                TerminalTools.ExtFullName = terminal.ExtFullName;
                TerminalTools.AdmStatus = terminal.AdmStatus.ToString();
            }
            else
            {
                RadAjaxPanel1.Enabled = false;
            }
        }

        /// <summary>
        /// Initializes the buttons depending on the status of the terminal
        /// </summary>
        /// <param name="term"></param>
        public void initTerminalManagementButtons(Terminal term)
        {
            switch (term.AdmStatus)
            {
                case 1:
                    //Locked
                    RadToolBarTerminal.Items[_activateIdx].Enabled = false;
                    RadToolBarTerminal.Items[_deleteIdx].Enabled = false;
                    RadToolBarTerminal.Items[_reactivateIdx].Enabled = true;
                    RadToolBarTerminal.Items[_suspendIdx].Enabled = false;
                    RadToolBarTerminal.Items[_decommissionIdx].Enabled = true;
                    RadToolBarTerminal.Items[_fupresetIdx].Enabled = false;
                    TextBoxSitId.Enabled = false;
                    break;

                case 2:
                    //Unlocked
                    RadToolBarTerminal.Items[_activateIdx].Enabled = false;
                    RadToolBarTerminal.Items[_deleteIdx].Enabled = false;
                    RadToolBarTerminal.Items[_reactivateIdx].Enabled = false;
                    RadToolBarTerminal.Items[_suspendIdx].Enabled = true;
                    RadToolBarTerminal.Items[_decommissionIdx].Enabled = false;
                    RadToolBarTerminal.Items[_fupresetIdx].Enabled = true;
                    TextBoxSitId.Enabled = false;
                    break;

                case 3:
                    //Decommissioned
                    RadToolBarTerminal.Items[_activateIdx].Enabled = false;
                    RadToolBarTerminal.Items[_deleteIdx].Enabled = true;
                    RadToolBarTerminal.Items[_reactivateIdx].Enabled = false;
                    RadToolBarTerminal.Items[_suspendIdx].Enabled = false;
                    RadToolBarTerminal.Items[_decommissionIdx].Enabled = false;
                    RadToolBarTerminal.Items[_fupresetIdx].Enabled = false;
                    TextBoxSitId.Enabled = false;
                    break;

                case 5:
                    //Request
                    RadToolBarTerminal.Items[_activateIdx].Enabled = true;
                    RadToolBarTerminal.Items[_deleteIdx].Enabled = true;
                    RadToolBarTerminal.Items[_reactivateIdx].Enabled = false;
                    RadToolBarTerminal.Items[_suspendIdx].Enabled = false;
                    RadToolBarTerminal.Items[_decommissionIdx].Enabled = false;
                    RadToolBarTerminal.Items[_fupresetIdx].Enabled = false;
                    RadToolBarTerminal.Items[2].Visible = false;
                    TextBoxSitId.Enabled = true;
                    TextBoxSitId.ForeColor = Color.DarkRed;
                    break;

                case 6:
                    //Deleted
                    RadToolBarTerminal.Items[_activateIdx].Enabled = false;
                    RadToolBarTerminal.Items[_deleteIdx].Enabled = false;
                    RadToolBarTerminal.Items[_reactivateIdx].Enabled = false;
                    RadToolBarTerminal.Items[_suspendIdx].Enabled = false;
                    RadToolBarTerminal.Items[_decommissionIdx].Enabled = false;
                    RadToolBarTerminal.Items[_fupresetIdx].Enabled = false;
                    TextBoxSitId.Enabled = false;
                    break;
            }
        }

        private bool SendActivationAcknowledgement(Terminal term)
        {
            bool result = false;
            string termMessage = "";

            termMessage = DateTime.Now.ToLongDateString() + " - " + " terminal activation completed.<br/>";
            termMessage = termMessage + "Your terminal with Mac Address: " + term.MacAddress + " has been activated.<br/>";
            termMessage = termMessage + "New SitId = " + term.SitId + "<br/><br/>";
            termMessage = termMessage + "<b>This is an automated mail, do not respond.</b></b></b>";
            termMessage = termMessage + "Your SatADSL support team!";

            //Get the Distributor E-Mail address
            if (term.DistributorId != null)
            {
                Distributor dist = _boAccountingControlWS.GetDistributor((int)term.DistributorId);
                if (dist != null)
                {
                    string[] distributorEmail = { dist.Email };

                    if (!_boLogControlWS.SendMail(termMessage, "Terminal Activation", distributorEmail))
                    {
                        this.logError("Could not send E-Mail to distributor: " + term.DistributorId, "Terminal Activation", "SendActivationAcknowledgment");
                    }
                    else
                    {
                        result = true;
                    }

                    if (term.OrgId != null)
                    {
                        Organization org = _boAccountingControlWS.GetOrganization((int)term.OrgId);

                        if (org != null)
                        {
                            string[] organizationEmail = { org.Email };
                            if (!_boLogControlWS.SendMail(termMessage, "Terminal Activation", organizationEmail))
                            {
                                this.logError("Could not send E-Mail to organization: " + term.OrgId, "Terminal Activation", "SendActivationAcknowledgment");
                            }
                        }
                        else
                        {
                            this.logError("Could not retrieve organization for id: " + term.OrgId,
                                        "Error while retrieving organization while activating terminal",
                                        "SendActivationAcknowledgment");
                        }
                    }
                }
                else
                {
                    this.logError("Could not retrieve distributor for id: " + term.DistributorId,
                                        "Error while retrieving distributor while activating terminal",
                                        "SendActivationAcknowledgment");
                    result = false;
                }
            }

            return result;
        }

        private void logError(string ex, string userdesc, string stack)
        {
            CmtApplicationException cmtApplicationException = new CmtApplicationException();
            cmtApplicationException.ExceptionDateTime = DateTime.Now;
            cmtApplicationException.ExceptionDesc = ex;
            cmtApplicationException.ExceptionStacktrace = stack;
            cmtApplicationException.UserDescription = userdesc;
            _boLogControlWS.LogApplicationException(cmtApplicationException);
        }

        /// <summary>
        /// Creates a new task in the database
        /// </summary>
        /// <param name="task"></param>
        /// <param name="subject"></param>
        private void CreateNewTask(string task, string subject, Terminal term)
        {
            MembershipUser myObject = Membership.GetUser();
            string userId = myObject.ProviderUserKey.ToString();

            TaskType[] taskTypes = _boTaskControlWS.GetTaskTypes();
            TaskType currentTaskType = null;
            foreach (TaskType taskType in taskTypes)
            {
                if (taskType.Type.Equals(task))
                {
                    currentTaskType = taskType;
                    break;
                }
            }


            Task activateTask = new Task();
            activateTask.Completed = false;
            activateTask.DateCreated = DateTime.Now;
            activateTask.Type = currentTaskType;
            activateTask.DistributorId = (int)term.DistributorId;
            activateTask.DateDue = DateTime.Now.AddDays(1.0);
            activateTask.MacAddress = term.MacAddress;
            activateTask.Subject = subject;
            activateTask.UserId = new Guid(userId);
            

            //Add task to task list.
            if (!_boTaskControlWS.UpdateTask(activateTask))
            {
                CmtApplicationException cmtAppEx = new CmtApplicationException();
                cmtAppEx.ExceptionDateTime = DateTime.Now;
                cmtAppEx.ExceptionDesc = "Could not add " + task + " task to task table";
                cmtAppEx.StateInformation = "Adding task failed";
                cmtAppEx.UserDescription = "Check " + task + " of terminal and next update in Dashboard";
                _boLogControlWS.LogApplicationException(cmtAppEx);
            }
        }

        protected void RadXmlHttpPanelSLA_ServiceRequest(object sender, RadXmlHttpPanelEventArgs e)
        {
            int ispId = Int32.Parse(e.Value);
            ServiceLevel[] slaItems = _boAccountingControlWS.GetServicePacksByIsp(ispId);

            RadComboBoxSLA.Items.Clear();
            RadComboBoxSLA.Items.Clear();
            foreach (ServiceLevel slaItem in slaItems)
            {
                RadComboBoxItem item = new RadComboBoxItem();
                item.Value = slaItem.SlaId.ToString();
                item.Text = slaItem.SlaName.Trim();
                RadComboBoxSLA.Items.Add(item);
            }

            RadComboBoxSLA.SelectedValue = slaItems[0].SlaName;
            RadComboBoxSLA.Text = slaItems[0].SlaName;
        }
    }
}