﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdvancedSearch.ascx.cs" Inherits="FOWebApp.Nocsa.AdvancedSearch" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script type="text/javascript">
    function RowSelected(sender, eventArgs) {
        var grid = sender;
        var masterDataView = grid.get_masterTableView();
        var rowNum = eventArgs.get_itemIndexHierarchical();
        var row = masterDataView.get_dataItems()[rowNum];
        var macAddress = masterDataView.getCellByColumnUniqueName(row, "MacAddressColumn").innerHTML;

        //Initialize and show the terminal details window
        var oWnd = radopen("Nocsa/TerminalForm.aspx?mac=" + macAddress, "RadWindowModifyTerminal");
    }

    function CommandSelected(sender, eventArgs) {
        var grid = sender;
        if (eventArgs.get_commandName() == "ModifyTerminal") {
            var macAddress = eventArgs.get_commandArgument();
            alert(macAddress);
            var oWnd = radopen("Nocsa/TerminalForm.aspx?mac=" + macAddress, "RadWindowModifyTerminal");
        }
    }
</script>
<style type="text/css">
    .style1
    {
        width: 161px;
    }
</style>
<h1>Advanced Terminal Search</h1>
<table cellpadding="10" cellspacing="2" style="width: 717px" 
    border="0" frame="void" width="100%">
    <tr>
        <td>
            <asp:RadioButton ID="RadioButtonStatus" runat="server" 
                GroupName="StatusSelectionButtons" Checked="True" /> 
        </td>
        <td>
            Status:
        </td>
        <td colspan="2">
            <telerik:RadComboBox ID="RadComboBoxStatus" runat="server" Skin="WebBlue">
            </telerik:RadComboBox>
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton ID="RadioButtonExpiryDate" runat="server" Skin="WebBlue" 
                GroupName="StatusSelectionButtons"/>
        </td>
        <td>
            Expiry Date:
        </td>
        <td class="style1">
            <telerik:RadDatePicker ID="RadDatePickerExpiryDate" Runat="server" 
                Culture="nl-BE" SelectedDate="2012-03-23" Skin="WebBlue">
<Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" 
                    ViewSelectorText="x" Skin="WebBlue" Runat="server"></Calendar>

<DateInput DisplayDateFormat="d/MM/yyyy" DateFormat="d/MM/yyyy" SelectedDate="2012-03-23" runat="server"></DateInput>

<DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
            </telerik:RadDatePicker>
        </td>
        <td>
            <telerik:RadComboBox ID="RadComboBoxExpiryDate" runat="server" Skin="WebBlue" 
                Font-Bold="True" ForeColor="#0066FF" Width="50px">
                <Items>
                    <telerik:RadComboBoxItem runat="server" Text="&lt;" Value="&lt;" />
                    <telerik:RadComboBoxItem runat="server" Text="&gt;" Value="&gt;" />
                    <telerik:RadComboBoxItem runat="server" Text="=&lt;" Value="=&lt;" />
                    <telerik:RadComboBoxItem runat="server" Text="&gt;=" Value="&gt;=" />
                    <telerik:RadComboBoxItem runat="server" Text="=" Value="=" />
                </Items>
            </telerik:RadComboBox>
        </td>
    </tr>
    <tr>
        <td> 
            <asp:RadioButton ID="RadioButtonMacAddress" runat="server" 
                GroupName="StatusSelectionButtons" />
        </td>
        <td>
            MacAddress:
        </td>
        <td class="style1">
            <asp:TextBox ID="TextBoxMacAddress" runat="server"></asp:TextBox>
        </td>
        <td>
            &nbsp;</td>
    </tr>
    <tr>
        <td colspan="4">
            <asp:Button ID="ButtonQuery" runat="server" Text="Query" 
                onclick="ButtonQuery_Click" />
        </td>
    </tr>
</table>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxyTerminals" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="ButtonQuery">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGridTerminals" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="RadGridTerminals">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGridTerminals" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadGrid ID="RadGridTerminals" runat="server" AllowPaging="True" AllowSorting="True"
    AutoGenerateColumns="False" CellSpacing="0" GridLines="None" 
    Skin="WebBlue" ShowStatusBar="True"
    Width="100%" 
    OnNeedDataSource="RadGridTerminals_NeedDataSource" 
    onitemdatabound="RadGridTerminals_ItemDataBound" 
    onpagesizechanged="RadGridTerminals_PageSizeChanged" 
    onprerender="RadGridTerminals_PreRender" 
    onsortcommand="RadGridTerminals_SortCommand" 
    onitemcommand="RadGridTerminals_ItemCommand" PageSize="20">
    <GroupingSettings CaseSensitive="false" /> 
    <ClientSettings>
        <Selecting AllowRowSelect="True" />
        <ClientEvents OnRowSelected="RowSelected" />
        <ClientEvents OnCommand="CommandSelected" />
    </ClientSettings>
    <MasterTableView CommandItemDisplay="Top" CommandItemSettings-ShowExportToExcelButton="True"
        CommandItemSettings-ShowExportToCsvButton="True" RowIndicatorColumn-Visible="True"
        RowIndicatorColumn-Resizable="True" RowIndicatorColumn-AndCurrentFilterFunction="Contains"
        CommandItemSettings-ShowAddNewRecordButton="False" CommandItemSettings-ShowExportToWordButton="True">
        <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
        </RowIndicatorColumn>
        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
        </ExpandCollapseColumn>
        <Columns>
            <telerik:GridBoundColumn DataField="FullName" FilterControlAltText="Filter FullNameColumn column"
                HeaderText="Name" UniqueName="FullNameColumn" ShowFilterIcon="True" AutoPostBackOnFilter="True">
                <ItemStyle Wrap="False" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="MacAddress" FilterControlAltText="Filter MacAddressColumn column"
                HeaderText="MAC Address" MaxLength="12" ReadOnly="True" Resizable="False" UniqueName="MacAddressColumn"
                CurrentFilterFunction="Contains" ShowFilterIcon="True" AutoPostBackOnFilter="True">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="SitId" FilterControlAltText="Filter SitIdColumn column"
                HeaderText="Site ID" UniqueName="SitIdColumn" ShowFilterIcon="False">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="IpAddress" FilterControlAltText="Filter IpAddressColumn column"
                HeaderText="IP Address" ReadOnly="True" UniqueName="IpAddressColumn" ShowFilterIcon="True"
                AutoPostBackOnFilter="True">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn AllowSorting="True" DataField="AdmStatus" FilterControlAltText="Filter AdmStatusColumn column"
                HeaderText="Status" ReadOnly="True" UniqueName="AdmStatusColumn" ShowFilterIcon="False"
                AutoPostBackOnFilter="True" AllowFiltering="True" ItemStyle-Wrap="False">
                <ItemStyle Wrap="False"></ItemStyle>
            </telerik:GridBoundColumn>
            <telerik:GridDateTimeColumn DataField="StartDate" FilterControlAltText="Filter StartDateColumn column"
                HeaderText="Start Date" ReadOnly="True" UniqueName="StartDateColumn" ItemStyle-Wrap="False"
                ShowFilterIcon="True" DataFormatString="{0:dd/MM/yyyy}" PickerType="DatePicker" DataType="System.DateTime">
                <ItemStyle Wrap="False"></ItemStyle>
            </telerik:GridDateTimeColumn>
            <telerik:GridDateTimeColumn AllowSorting="True" DataField="ExpiryDate" DataType="System.DateTime"
                FilterControlAltText="Filter ExpiryDateColumn column" HeaderText="Expiry Date"
                ReadOnly="True" UniqueName="ExpiryDateColumn" ItemStyle-Wrap="False" ShowFilterIcon="True"
                AutoPostBackOnFilter="True" DataFormatString="{0:dd/MM/yyyy}" PickerType="DatePicker">
                <ItemStyle Wrap="False"></ItemStyle>
            </telerik:GridDateTimeColumn>
        </Columns>
        <EditFormSettings>
            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
            </EditColumn>
        </EditFormSettings>
        <PagerStyle AlwaysVisible="True" />
    </MasterTableView>
    <FilterMenu EnableImageSprites="False">
    </FilterMenu>
    <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default">
    </HeaderContextMenu>
</telerik:RadGrid>
<telerik:RadWindowManager ID="RadWindowManager1" runat="server" Skin="Vista" DestroyOnClose="True">
    <Windows>
        <telerik:RadWindow ID="RadWindowTerminalDetails" runat="server" 
            Animation="Resize" Opacity="100" Skin="WebBlue" Title="Terminal Details" AutoSize="False"
            Width="1000px" Height="700px" KeepInScreenBounds="True" EnableShadow="True" EnableAriaSupport="True"
            VisibleStatusbar="True" Modal="True" DestroyOnClose="False">
        </telerik:RadWindow>
        <telerik:RadWindow ID="RadWindowModifyTerminal" runat="server" Animation="Resize" Opacity="100"
            Skin="WebBlue" Title="Modify Terminal" AutoSize="False" Width="850px" Height="700px"
            KeepInScreenBounds="True" EnableShadow="True" EnableAriaSupport="True" VisibleStatusbar="True"
            Modal="False" DestroyOnClose="False">
        </telerik:RadWindow>
        <telerik:RadWindow ID="RadWindowModifyActivity" runat="server" Animation="Resize" Opacity="100"
            Skin="WebBlue" Title="Modify Activity" AutoSize="False" Width="850px" Height="655px"
            KeepInScreenBounds="True" EnableShadow="True" EnableAriaSupport="True" VisibleStatusbar="True"
            Modal="False" DestroyOnClose="False">
        </telerik:RadWindow>
    </Windows>
</telerik:RadWindowManager>


