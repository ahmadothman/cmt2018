﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FOWebApp.net.nimera.cmt.BOMultiCastGroupControllerWSRef;

namespace FOWebApp.Nocsa
{
    public partial class MultiCastGroupList : System.Web.UI.UserControl
    {
        BOMultiCastGroupControllerWS _boMultiCastGroupController = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            _boMultiCastGroupController = new BOMultiCastGroupControllerWS();
        }

        protected void RadGridMultiCastGroups_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            MultiCastGroup[] mcList = _boMultiCastGroupController.GetMultiCastGroups();
            RadGridMultiCastGroups.DataSource = mcList;
        }
    }
}