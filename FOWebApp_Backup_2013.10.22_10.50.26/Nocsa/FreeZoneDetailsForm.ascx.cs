﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOMonitorControlWSRef;
using System.Drawing;

namespace FOWebApp.Nocsa
{
    public partial class FreeZoneDetailsForm : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            TextBoxMonOff.Mask = _HOUR + "<:>" + _MINUTE + "<:>" + _SECOND;
            TextBoxMonOn.Mask = _HOUR + "<:>" + _MINUTE + "<:>" + _SECOND;
            TextBoxTueOff.Mask = _HOUR + "<:>" + _MINUTE + "<:>" + _SECOND;
            TextBoxTueOn.Mask = _HOUR + "<:>" + _MINUTE + "<:>" + _SECOND;
            TextBoxWedOff.Mask = _HOUR + "<:>" + _MINUTE + "<:>" + _SECOND;
            TextBoxWedOn.Mask = _HOUR + "<:>" + _MINUTE + "<:>" + _SECOND;
            TextBoxThuOff.Mask = _HOUR + "<:>" + _MINUTE + "<:>" + _SECOND;
            TextBoxThuOn.Mask = _HOUR + "<:>" + _MINUTE + "<:>" + _SECOND;
            TextBoxFriOff.Mask = _HOUR + "<:>" + _MINUTE + "<:>" + _SECOND;
            TextBoxFriOn.Mask = _HOUR + "<:>" + _MINUTE + "<:>" + _SECOND;
            TextBoxSatOff.Mask = _HOUR + "<:>" + _MINUTE + "<:>" + _SECOND;
            TextBoxSatOn.Mask = _HOUR + "<:>" + _MINUTE + "<:>" + _SECOND;
            TextBoxSunOff.Mask = _HOUR + "<:>" + _MINUTE + "<:>" + _SECOND;
            TextBoxSunOn.Mask = _HOUR + "<:>" + _MINUTE + "<:>" + _SECOND;
        }

        public void componentDataInit(int freeZoneId)
        {
            //Load free zone data from NMS
            FreeZone freeZone = boMonitorControl.NMSgetFreeZone(freeZoneId, ispId);
            LabelId.Text = freeZone.Id.ToString();
            TextBoxName.Text = freeZone.Name;
            if (freeZone.CurrentActive == 1)
            {
                CheckBoxActive.Checked = true;
            }
            TextBoxMonOff.Text = freeZone.MonOff.ToString();
            TextBoxMonOn.Text = freeZone.MonOn.ToString();
            TextBoxTueOff.Text = freeZone.TueOff.ToString();
            TextBoxTueOn.Text = freeZone.TueOn.ToString();
            TextBoxWedOff.Text = freeZone.WedOff.ToString();
            TextBoxWedOn.Text = freeZone.WedOn.ToString();
            TextBoxThuOn.Text = freeZone.ThuOn.ToString();
            TextBoxThuOff.Text = freeZone.ThuOff.ToString();
            TextBoxFriOn.Text = freeZone.FriOn.ToString();
            TextBoxFriOff.Text = freeZone.FriOff.ToString();
            TextBoxSatOn.Text = freeZone.SatOn.ToString();
            TextBoxSatOff.Text = freeZone.SatOff.ToString();
            TextBoxSunOn.Text = freeZone.SunOn.ToString();
            TextBoxSunOff.Text = freeZone.SunOff.ToString();
        }

        public BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();
        public BOMonitorControlWS boMonitorControl = new BOMonitorControlWS();
        public int ispId = 469; //ISP ID of the NMS ISP
        const string _HOUR = "<0|1|2><0|1|2|3|4|5|6|7|8|9>";
        const string _MINUTE = "<0|1|2|3|4|5><0|1|2|3|4|5|6|7|8|9>";
        const string _SECOND = "<0|1|2|3|4|5><0|1|2|3|4|5|6|7|8|9>";
    }
}