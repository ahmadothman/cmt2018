﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;
using Telerik.Web.UI;
using System.Drawing;

namespace FOWebApp.Nocsa
{
    public partial class ErrorLogList : System.Web.UI.UserControl
    {
        /// <summary>
        /// Determines the activity type (Term_Activity_Log, Exception_Log, User_Log)
        /// </summary>
        public string ActivityType { get; set; }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            RadDatePickerStart.SelectedDate = DateTime.Now.AddDays(-1.0);
            RadDatePickerEnd.SelectedDate = DateTime.Now.AddDays(1.0);
        }

        protected void ImageButtonQuery_Click(object sender, ImageClickEventArgs e)
        {
            DateTime startDate;
            DateTime endDate;

            if (!RadDatePickerStart.IsEmpty || !RadDatePickerEnd.IsEmpty)
            {
                startDate = (DateTime)RadDatePickerStart.SelectedDate;
                endDate = (DateTime)RadDatePickerEnd.SelectedDate;

                this.Session[ActivityType + "_StartDate"] = startDate;
                this.Session[ActivityType + "_EndDate"] = endDate;

                this.generateReport(startDate, endDate);
                RadGridUserActivityLog.DataBind();
                RadGridUserActivityLog.Visible = true;

            }
            else
            {
                //Check if values were stored in session variables
                if (this.Session[ActivityType + "_StartDate"] != null && this.Session[ActivityType + "_EndDate"] != null)
                {
                    startDate = (DateTime)this.Session[ActivityType + "_StartDate"];
                    endDate = (DateTime)this.Session[ActivityType + "_EndDate"];

                    this.generateReport(startDate, endDate);
                    RadGridUserActivityLog.DataBind();
                    RadGridUserActivityLog.Visible = true;
                }
                else
                {
                    RadGridUserActivityLog.Visible = false;
                }
            }
        }

        /// <summary>
        /// Generates the report depending on the selecte report type
        /// </summary>
        /// <param name="startDate">Start date of the report</param>
        /// <param name="endDate">End date of the report</param>
        private void generateReport(DateTime startDate, DateTime endDate)
        {
            BOLogControlWS boLogControl = new BOLogControlWS();
            DataTable dt = null;

            string check = "";
            string[] temp = new string[5];
            int i = 0;
            foreach (ListItem item in CheckBoxListExceptionLevel.Items)
            { 
                if (item.Selected)
                {
                    temp[i] = item.Value.ToString();
                    i++;
                }
            }


            if (i == 0)
            {
                LabelExceptionLevel.Text = "Please select at<br>least one option";
                LabelExceptionLevel.ForeColor = Color.Red;
            }
            if (i >= 1)
            {
                check = temp[0];
                if (i > 1)
                {
                    for (int j = 1; j < i; j++)
                    {
                        check = check + ", " + temp[j];
                    }
                }
                dt = boLogControl.GetExceptionsBetweenDates(startDate, endDate, check);
            }
                
            //}

            if (dt != null)
            {
                
                RadGridUserActivityLog.DataSource = dt;
                RadGridUserActivityLog.Visible = true;
                this.Session[ActivityType + "_Bound"] = true;
            }
            else
            {
                RadGridUserActivityLog.Visible = false;
            }
        }

        protected void RadGridUserActivityLog_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
        {
            //Save the page index
            int pageIdx = e.NewPageIndex;
            this.Session[ActivityType + "_PageIdx"] = pageIdx;
        }

        protected void RadGridUserActivityLog_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            DateTime startDate;
            DateTime endDate;

            if (!RadDatePickerStart.IsEmpty || !RadDatePickerEnd.IsEmpty)
            {
                startDate = (DateTime)RadDatePickerStart.SelectedDate;
                endDate = (DateTime)RadDatePickerEnd.SelectedDate;

                this.Session[ActivityType + "_StartDate"] = startDate;
                this.Session[ActivityType + "_EndDate"] = endDate;

                this.generateReport(startDate, endDate);
            }
            else
            {
                //Check if values were stored in session variables
                if (this.Session[ActivityType + "_StartDate"] != null && this.Session[ActivityType + "_EndDate"] != null)
                {
                    startDate = (DateTime)this.Session[ActivityType + "_StartDate"];
                    endDate = (DateTime)this.Session[ActivityType + "_EndDate"];

                    this.generateReport(startDate, endDate);
                }
                else
                {
                    RadGridUserActivityLog.Visible = false;
                }
            }
        }

        protected void RadGridUserActivityLog_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem dataItem = (GridDataItem)e.Item;

                if (dataItem["ExceptionLevel"].Text.Equals("0"))
                {
                    dataItem["ExceptionLevel"].Text = "Debug";
                    dataItem["ExceptionLevel"].ForeColor = Color.Green;
                }

                if (dataItem["ExceptionLevel"].Text.Equals("1"))
                {
                    dataItem["ExceptionLevel"].Text = "Info";
                    dataItem["ExceptionLevel"].ForeColor = Color.Green;
                }

                if (dataItem["ExceptionLevel"].Text.Equals("2"))
                {
                    dataItem["ExceptionLevel"].Text = "Warning";
                    dataItem["ExceptionLevel"].ForeColor = Color.Black;
                }

                if (dataItem["ExceptionLevel"].Text.Equals("3"))
                {
                    dataItem["ExceptionLevel"].Text = "Error";
                    dataItem["ExceptionLevel"].ForeColor = Color.Red;
                }

                if (dataItem["ExceptionLevel"].Text.Equals("4"))
                {
                    dataItem["ExceptionLevel"].Text = "Critical";
                    dataItem["ExceptionLevel"].ForeColor = Color.Red;
                }
            }
        }

        public void clearGrid()
        {
            this.Session[ActivityType + "_Bound"] = false;
        }

        protected void RadGridUserActivityLog_SortCommand(object sender, Telerik.Web.UI.GridSortCommandEventArgs e)
        {
            this.Session[ActivityType + "_SortExpression"] = e.SortExpression;
        }

        protected void RadGridUserActivityLog_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            this.Session[ActivityType + "_PageSize"] = e.NewPageSize;
        }

    }
}