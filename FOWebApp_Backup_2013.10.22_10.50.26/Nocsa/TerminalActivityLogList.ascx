﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TerminalActivityLogList.ascx.cs" Inherits="FOWebApp.Nocsa.TerminalActivityLogList" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script type="text/javascript">
    function RowSelected(sender, eventArgs) {
        var grid = sender;
        var masterDataView = grid.get_masterTableView();
        var rowNum = eventArgs.get_itemIndexHierarchical();
        var row = masterDataView.get_dataItems()[rowNum];
        var Action = masterDataView.getCellByColumnUniqueName(row, "Action").innerHTML;
        if (Action != "Delete") {
            var Id = masterDataView.getCellByColumnUniqueName(row, "Id").innerHTML;

            //Initialize and show the activity details window
            var oWnd = radopen("Nocsa/ModifyActivityForm.aspx?Id=" + Id, "RadWindowModifyActivity");
        }
        else {
            //alert("Sorry but you can not review deleted activities!");
            var oManager = GetRadWindowManager();
            oManager.radalert("Sorry but you can not review deleted activities!", 200, 150, "Warning");
        }
    }
</script>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="ImageButtonQuery">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGridTerminalActivityList" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<table cellpadding="5" cellspacing="2">
    <tr>
        <td>Start date:</td>
        <td> 
            <telerik:RadDatePicker ID="RadDatePickerStart" runat="server">
            </telerik:RadDatePicker>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Provide a start date" ControlToValidate="RadDatePickerStart" Display="Dynamic"></asp:RequiredFieldValidator>
        </td>
        <td>End date:</td>
        <td>
            <telerik:RadDatePicker ID="RadDatePickerEnd" runat="server">
            </telerik:RadDatePicker>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Provide an end date" ControlToValidate="RadDatePickerEnd"></asp:RequiredFieldValidator>
        </td>
        <td>
            <asp:ImageButton ID="ImageButtonQuery" runat="server" 
                ImageUrl="~/Images/invoice_euro.png" ToolTip="Show user log" 
                onclick="ImageButtonQuery_Click" />
        </td>
    </tr>
</table>
<telerik:RadGrid ID="RadGridTerminalActivityList" runat="server" 
    AutoGenerateColumns="False" CellSpacing="0" GridLines="None" 
    onneeddatasource="RadGridTerminalActivityList_NeedDataSource" 
    Skin="WebBlue" Visible="False" ShowHeader="False">
    <ClientSettings>
        <Selecting AllowRowSelect="True" />
        <ClientEvents OnRowSelected="RowSelected" />
    </ClientSettings>
<MasterTableView AllowFilteringByColumn="True" AllowSorting="True" 
        ShowHeader="True">
<CommandItemSettings ExportToPdfText="Export to PDF" ShowAddNewRecordButton="False" 
        ShowExportToCsvButton="True"></CommandItemSettings>

<RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
</RowIndicatorColumn>

<ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
</ExpandCollapseColumn>

    <Columns>
        <telerik:GridCheckBoxColumn AllowFiltering="False" AllowSorting="False" 
            DataField="AccountingFlag" FilterControlAltText="Filter AccountingFlag column" 
            HeaderText="Acc" UniqueName="AccountingFlag">
        </telerik:GridCheckBoxColumn>
        <telerik:GridBoundColumn AllowFiltering="False" AllowSorting="False" 
            DataField="Id" FilterControlAltText="Filter Id column" HeaderText="Id" 
            UniqueName="Id">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="MacAddress" 
            FilterControlAltText="Filter MacAddress column" HeaderText="MAC Address" 
            UniqueName="MacAddress" AllowFiltering="False" AllowSorting="False" 
            CurrentFilterFunction="Contains" ShowFilterIcon="False">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="ActionDate" 
            FilterControlAltText="Filter ActionDate column" HeaderText="Action Date" 
            UniqueName="ActionDate" AllowFiltering="False" AllowSorting="False" 
            CurrentFilterFunction="Contains" ShowFilterIcon="False">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="Action" 
            FilterControlAltText="Filter Action column" HeaderText="Action" 
            SortExpression="Contains" UniqueName="Action" AllowFiltering="False" 
            AllowSorting="False" CurrentFilterFunction="Contains" ShowFilterIcon="False">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="Terminal Name" 
            FilterControlAltText="Filter Terminal Name column" HeaderText="Terminal Name" 
            SortExpression="Contains" UniqueName="TerminalName" AllowFiltering="False" 
            AllowSorting="False" CurrentFilterFunction="Contains" ShowFilterIcon="False">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="Distributor Name" 
            FilterControlAltText="Filter Distributor Name column" 
            HeaderText="Disributor Name" SortExpression="Contains" 
            UniqueName="DistributorName" AllowFiltering="False" AllowSorting="False" 
            CurrentFilterFunction="Contains" ShowFilterIcon="False">
        </telerik:GridBoundColumn>
    </Columns>

<EditFormSettings>
<EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
</EditFormSettings>
</MasterTableView>

<FilterMenu EnableImageSprites="False"></FilterMenu>

<HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default"></HeaderContextMenu>
</telerik:RadGrid>
<telerik:RadWindowManager ID="RadWindowManagerActivityLogList" runat="server" Skin="WebBlue" DestroyOnClose="True">
    <Windows>
        <telerik:RadWindow ID="RadWindowModifyActivity" runat="server" Animation="Resize" Opacity="100"
            Skin="WebBlue" Title="Modify Activity" AutoSize="False" Width="850px" Height="655px"
            KeepInScreenBounds="True" EnableShadow="True" EnableAriaSupport="True" VisibleStatusbar="True"
            Modal="False" DestroyOnClose="False">
        </telerik:RadWindow>
    </Windows>
</telerik:RadWindowManager>