﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using Telerik.Web.UI;

namespace FOWebApp.Nocsa
{
    public partial class ServicePackList : System.Web.UI.UserControl
    {
        BOAccountingControlWS _boAccountingControlWS = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            RadComboBoxISP.Items.Clear();
            _boAccountingControlWS = new BOAccountingControlWS();
            Isp[] ispList = _boAccountingControlWS.GetIsps();
            RadComboBoxISP.DataValueField = "Id";
            RadComboBoxISP.DataTextField = "CompanyName";
            RadComboBoxISP.DataSource = ispList;
            RadComboBoxISP.DataBind();
            RadComboBoxISP.Items.Insert(0, new RadComboBoxItem(""));
        }

        protected void RadGridServicePacks_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            RadGridServicePacks.DataSource = _boAccountingControlWS.GetServicePacks();
        }

        protected void RadGridServicePacks_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.UpdateCommandName)
            {
                GridEditableItem editedItem = e.Item as GridEditableItem;
            }
        }

        protected void RadGridServicePacks_EditCommand(object sender, GridCommandEventArgs e)
        {
        }

        protected void RadGridServicePacks_UpdateCommand(object sender, GridCommandEventArgs e)
        {
        }

        protected void RadComboBoxISP_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            //Search Service packs for the given ISP
            RadGridServicePacks.Visible = true;
            int ispId = Int32.Parse(e.Value);
            ServiceLevel[] servicePacks = _boAccountingControlWS.GetServicePacksByIspWithHide(ispId);
            RadGridServicePacks.DataSource = servicePacks;
            RadGridServicePacks.DataBind();
        }

        protected void RadGridServicePacks_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem dataItem = (GridDataItem)e.Item;

                if (dataItem["Hide"].Text.Equals("True"))
                {
                    dataItem["SlaId"].ForeColor = Color.Red;
                }
                else
                {
                    dataItem["SlaId"].ForeColor = Color.Green;
                }
            }

        }
    }
}