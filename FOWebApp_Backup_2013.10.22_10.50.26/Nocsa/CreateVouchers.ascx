﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CreateVouchers.ascx.cs"
    Inherits="FOWebApp.Nocsa.CreateVouchers" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script type="text/javascript">
    function CreateVouchersButtonClicked() {
        radconfirm('Are you sure to continue?', confirmCreateCallBackFn, 400, 200, null, 'You are about to create new vouchers!');
    }

    function confirmCreateCallBackFn(arg) {
        if (arg == true) {
            document.getElementById("<%= DownloadURL.ClientID %>").innerHTML = "Creating vouchers, please  wait";
            document.getElementById("<%= ImageAjax.ClientID %>").style.display = "";     

            //Get the distributor Id and number of vouchers from the controls
            var distributorCombo = $find("<%= RadComboBoxDistributors.ClientID %>");
            var distributorId = parseInt(distributorCombo.get_value());

            var voucherVolumeCombo = $find("<%= RadComboBoxVoucherVolume.ClientID %>");
            var volumeId = parseInt(voucherVolumeCombo.get_value());

            var numVoucherTextbox = $find("<%= RadNumericTextBoxvouchers.ClientID %>");
            var numVouchers = parseInt(numVoucherTextbox.get_textBoxValue());

            //Call the webservice
            FOWebApp.WebServices.VoucherSupportWS.CreateVouchers(distributorId, numVouchers, volumeId, OnVoucherCreationComplete);
        }
    }

    function OnVoucherCreationComplete(result) {
        document.getElementById("<%= ImageAjax.ClientID %>").style.display = "none";
        document.getElementById("<%= DownloadURL.ClientID %>").innerHTML = result;
    }

</script>
<style type="text/css">
    .style1
    {
        width: 308px;
    }
</style>
<table cellpadding="10" cellspacing="5">
    <tr>
        <td>
            Create vouchers for Distributor:
        </td>
        <td class="style1">
            <telerik:RadComboBox ID="RadComboBoxDistributors" runat="server" Height="27px" Skin="WebBlue"
                Width="300px" ExpandDirection="Up" MaxHeight="500px" NoWrap="True" ShowMoreResultsBox="True">
                <ExpandAnimation Type="OutCubic" />
            </telerik:RadComboBox>
        </td>
    </tr>
    <tr>
        <td>
            Voucher volume:
        </td>
        <td>
            <telerik:RadComboBox ID="RadComboBoxVoucherVolume" runat="server" Height="27px" Skin="WebBlue"
                Width="300px" ExpandDirection="Up" MaxHeight="500px" NoWrap="True" ShowMoreResultsBox="True">
                <ExpandAnimation Type="OutCubic" />
            </telerik:RadComboBox>
        </td>
    </tr>
    <tr>
        <td>
            Number of vouchers to create:
        </td>
        <td class="style1">
            <telerik:RadNumericTextBox ID="RadNumericTextBoxvouchers" runat="server" DataType="System.Int32"
                EmptyMessage="Number of vouchers" MaxValue="1000" MinValue="0" NumberFormat-DecimalDigits="0"
                ShowButton="False" ShowSpinButtons="True" Value="10" Height="21px" Skin="WebBlue"
                Width="125px" Culture="en-GB">
<NumberFormat DecimalDigits="0" ZeroPattern="n" decimalseparator=" " groupseparator=""></NumberFormat>
            </telerik:RadNumericTextBox>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <telerik:RadButton ID="RadButtonCreateVouchers" runat="server" OnClientClicked="CreateVouchersButtonClicked"
                Skin="WebBlue" Text="Create Voucher(s)" OnClick="RadButtonCreateVouchers_Click"
                AutoPostBack="False">
            </telerik:RadButton>
        </td>
    </tr>
    <tr>
        <td class="style1" colspan="2">
            <asp:Image ID="ImageAjax" runat="server" ImageUrl="~/Images/ajax-loader.gif" style="display: none; vertical-align: middle"/><asp:Label ID="DownloadURL" runat="server"></asp:Label>
        </td>
    </tr>
</table>
<telerik:RadWindowManager ID="RadWindowManager1" runat="server" Skin="Sunset" EnableShadow="True"
    Left="0px" Right="0px" Modal="True" VisibleStatusbar="False">
</telerik:RadWindowManager>
