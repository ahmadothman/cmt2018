﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using Telerik.Web.UI;

namespace FOWebApp.Nocsa
{
    public partial class DistributorDetailsForm : System.Web.UI.UserControl
    {
        BOAccountingControlWS _boAccountingControl = new BOAccountingControlWS();

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public void componentDataInit(int distributorId)
        {
            //Get the Distributor information an fill up the form
            BOAccountingControlWS boAccountingControl =
              new net.nimera.cmt.BOAccountingControlWSRef.BOAccountingControlWS();

            //Get the Distributor linked to the user
            Distributor dist = boAccountingControl.GetDistributor(distributorId);

            if (dist != null)
            {
                TextBoxAddressLine1.Text = dist.Address.AddressLine1;
                TextBoxAddressLine2.Text = dist.Address.AddressLine2;
                //TextBoxCountry.Text = dist.Address.Country;
                CountryList.SelectedValue = dist.Address.Country;
                TextBoxDistributorName.Text = dist.FullName;
                TextBoxEMail.Text = dist.Email;
                TextBoxFax.Text = dist.Fax;
                TextBoxLocation.Text = dist.Address.Location;
                TextBoxPCO.Text = dist.Address.PostalCode;
                TextBoxPhone.Text = dist.Phone;
                TextBoxRemarks.Text = dist.Remarks;
                TextBoxVAT.Text = dist.Vat;
                TextBoxWebSite.Text = dist.WebSite;
                LabelDistId.Text = "" + dist.Id;
                PanelISPAllocation.Visible = true;
                RadListBoxIsp.Visible = true;

                //Load the Checkbox list
                Isp[] ispList = _boAccountingControl.GetIsps();
                foreach (Isp isp in ispList)
                {
                    RadListBoxItem listItem = new RadListBoxItem(isp.CompanyName, isp.Id.ToString());
                    RadListBoxIsp.Items.Add(listItem);
                }

                //Check the ISPs allocated to this distributor
                Isp[] ispListForDistributor = _boAccountingControl.GetISPsForDistributor(distributorId);

                foreach (Isp isp in ispListForDistributor)
                {
                    for (int i = 0; i < RadListBoxIsp.Items.Count; i++)
                    {
                        if (!RadListBoxIsp.Items[i].Checked && RadListBoxIsp.Items[i].Value.Equals(isp.Id.ToString()))
                        {
                            RadListBoxIsp.Items[i].Checked = true;
                        }
                    }
                }
            
            }
        }

        //recursive function that adds attribute to all child controls 
        private void addBlurAtt(Control cntrl)
        {
            if (cntrl.Controls.Count > 0)
            {
                foreach (Control childControl in cntrl.Controls)
                {
                    addBlurAtt(childControl);
                }
            }
            if (cntrl.GetType() == typeof(TextBox))
            {
                TextBox TempTextBox = (TextBox)cntrl;
                //TempTextBox.Attributes.Add("onFocus", "DoFocus(this);");
                //TempTextBox.Attributes.Add("onBlur", "DoBlur(this);");
                TempTextBox.Attributes.Add("onchange", "DoChange(this);");
            }
            if (cntrl.GetType() == typeof(ListBox))
            {
                ListBox TempTextBox = (ListBox)cntrl;
                //TempTextBox.Attributes.Add("onFocus", "DoFocus(this);");
                //TempTextBox.Attributes.Add("onBlur", "DoBlur(this);");
                TempTextBox.Attributes.Add("onchange", "DoChange(this);");
            }
            if (cntrl.GetType() == typeof(DropDownList))
            {
                DropDownList TempTextBox = (DropDownList)cntrl;
                //TempTextBox.Attributes.Add("onFocus", "DoFocus(this);");
                //TempTextBox.Attributes.Add("onBlur", "DoBlur(this);");
                TempTextBox.Attributes.Add("onchange", "DoChange(this);");
            }
        }

        protected void ButtonSubmit_Click(object sender, EventArgs e)
        {
            //Submit the data to the database
            Distributor dist = new Distributor();
            dist.Address = new Address();
            Isp isp = new Isp();

            //In one of the next releases remove the Isp field from the Distributor entity
            //This is not necessary anymore
            isp.Id = 112;
            dist.Isp = isp;

            try
            {
                if (!LabelDistId.Text.Equals(""))
                {
                    dist.Id = Int32.Parse(LabelDistId.Text);
                }
                dist.Address.AddressLine1 = TextBoxAddressLine1.Text;
                dist.Address.AddressLine2 = TextBoxAddressLine2.Text;
                dist.Address.Country = CountryList.SelectedValue;
                dist.FullName = TextBoxDistributorName.Text;
                dist.Email = TextBoxEMail.Text;
                dist.Fax = TextBoxFax.Text;
                dist.Address.Location = TextBoxLocation.Text;
                dist.Address.PostalCode = TextBoxPCO.Text;
                dist.Phone = TextBoxPhone.Text;
                dist.Remarks = TextBoxRemarks.Text;
                dist.Vat = TextBoxVAT.Text;
                dist.WebSite = TextBoxWebSite.Text;

                //Update data
                BOAccountingControlWS boAccountingControl =
                    new net.nimera.cmt.BOAccountingControlWSRef.BOAccountingControlWS();

                if (boAccountingControl.UpdateDistributor(dist))
                {
                    LabelResult.Text = "Distributor modified successfully!";
                    LabelResult.ForeColor = Color.DarkGreen;
                    ButtonSubmit.Enabled = false;
                }
                else
                {
                    LabelResult.Text = "Adding distributor failed!";
                    LabelResult.ForeColor = Color.Red;
                }
            }
            catch (Exception ex)
            {
                //Display an error message
                LabelResult.Text = ex.Message;
                LabelResult.ForeColor = Color.Red;
            }
        }
    }
}