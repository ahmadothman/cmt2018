﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FOWebApp.net.nimera.cmt.BOMultiCastGroupControllerWSRef;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;

namespace FOWebApp.Nocsa
{
    public partial class MultiCastDetailsForm : System.Web.UI.Page
    {
        BOMultiCastGroupControllerWS _boMultiCastGroupController = new BOMultiCastGroupControllerWS();
        BOAccountingControlWS _boAccountingController = new BOAccountingControlWS();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //Fill the form fields
                String ipAddress = Request.Params["ip"];
                if (ipAddress != null)
                {
                    MultiCastGroup mc = _boMultiCastGroupController.GetMultiCastGroupByIP(ipAddress);

                    TextBoxIPAddress.Text = mc.IPAddress;
                    TextBoxSourceURL.Text = mc.sourceURL;
                    TextBoxBandwidth.Text = mc.BandWidth.ToString();
                    TextBoxMacAddress.Text = mc.MacAddress.ToString();
                    TextBoxName.Text = mc.Name;
                    TextBoxPortNr.Text = mc.portNr.ToString();
                    CheckBoxState.Checked = mc.State;
                    LabelISP.Text = _boAccountingController.GetISP(mc.IspId).CompanyName;
                    RadDatePickerFirstActivationDate.SelectedDate = mc.FirstActivationDate;

                    //Fill up the distributors list
                    Distributor[] distributors = _boAccountingController.GetDistributors();

                    foreach (Distributor distributor in distributors)
                    {
                        RadComboBoxItem item = new RadComboBoxItem();
                        item.Value = distributor.Id.ToString();
                        item.Text = distributor.FullName;
                        RadComboBoxDistributor.Items.Add(item);
                    }

                    RadComboBoxDistributor.SelectedValue = mc.DistributorId.ToString();

                    if (mc.State)
                    {
                        RadToolBarMultiCastGroup.Items[1].Enabled = false;
                        RadToolBarMultiCastGroup.Items[2].Enabled = true;
                    }
                    else
                    {
                        RadToolBarMultiCastGroup.Items[1].Enabled = true;
                        RadToolBarMultiCastGroup.Items[2].Enabled = false;
                    }
                }
            }
        }

        protected void RadToolBarMultiCastGroup_ButtonClick(object sender, RadToolBarEventArgs e)
        {
            //Check the button commmand and execute the command
            RadToolBarItem toolbarItem = e.Item;

            MultiCastGroup mc = _boMultiCastGroupController.GetMultiCastGroupByIP(TextBoxIPAddress.Text);

                mc.Name = TextBoxName.Text;
                mc.sourceURL = TextBoxSourceURL.Text;
                mc.portNr = Int32.Parse(TextBoxPortNr.Text);
                mc.BandWidth = Int32.Parse(TextBoxBandwidth.Text);
                mc.FirstActivationDate = (DateTime)RadDatePickerFirstActivationDate.SelectedDate;
                mc.DistributorId = Int32.Parse(RadComboBoxDistributor.SelectedValue);

            if (toolbarItem.Value.Equals("Save_Button"))
            {
                

                if (!_boMultiCastGroupController.UpdateMultiCastGroup(mc))
                {
                    RadWindowManager1.RadAlert("Update of Multicast group failed", 250, 100, "Update result", null);
                }
                else
                {
                    RadWindowManager1.RadAlert("Update of Multicast group succeeded", 250, 100, "Update result", null);
                }
            }

            if (toolbarItem.Value.Equals("Enable_Button"))
            {
                if (!_boMultiCastGroupController.EnableMultiCastGroup(TextBoxIPAddress.Text, TextBoxMacAddress.Text, mc.IspId))
                {
                    RadWindowManager1.RadAlert("Enable of Multicast group failed", 250, 100, "Enable result", null);
                }
                else
                {
                    RadWindowManager1.RadAlert("Enable of Multicast group succeeded", 250, 100, "Enable result", null);
                }
            }

            if (toolbarItem.Value.Equals("Disable_Button"))
            {

                if (!_boMultiCastGroupController.DisableMultiCastGroup(TextBoxIPAddress.Text, TextBoxMacAddress.Text, mc.IspId ))
                {
                    RadWindowManager1.RadAlert("Disable of Multicast group failed", 250, 100, "Disable result", null);
                }
                else
                {
                    RadWindowManager1.RadAlert("Disable of Multicast group succeeded", 250, 100, "Disable result", null);
                }
            }
        }
    }
}