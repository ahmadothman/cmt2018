﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserMaintenanceList.ascx.cs" Inherits="FOWebApp.Nocsa.UserMaintenanceList" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script type="text/javascript">
    function RowSelected(sender, eventArgs) {
        var grid = sender;
        var masterDataView = grid.get_masterTableView();
        var rowNum = eventArgs.get_itemIndexHierarchical();
        var row = masterDataView.get_dataItems()[rowNum];
        var userName = masterDataView.getCellByColumnUniqueName(row, "UserNameColumn").innerHTML;

        //Initialize and show the terminal details window
        var oWnd = radopen("Nocsa/ModifyUser.aspx?UserName=" + userName, "RadWindowModifyUser");
    }
</script>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxyUserMaintenanceList" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="RadGridUserMaintenanceList">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGridUserMaintenanceList" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadGrid ID="RadGridUserMaintenanceList" runat="server" 
    AllowFilteringByColumn="True" AllowPaging="True" AllowSorting="True" 
    CellSpacing="0" GridLines="None" 
    onneeddatasource="RadGridUserMaintenanceList_NeedDataSource" Skin="WebBlue" 
    Width="100%" AutoGenerateColumns="False" PageSize="20" 
    ShowStatusBar="True" EnableAriaSupport="True" 
    ondeletecommand="RadGridUserMaintenanceList_DeleteCommand" 
    onitemcommand="RadGridUserMaintenanceList_ItemCommand" 
    onpageindexchanged="RadGridUserMaintenanceList_PageIndexChanged">
    <ExportSettings IgnorePaging="True" OpenInNewWindow="True">
    </ExportSettings>
    <ClientSettings>
        <Selecting AllowRowSelect="True" />
        <ClientEvents OnRowSelected="RowSelected" />
    </ClientSettings>
<MasterTableView PageSize="20" EditMode="PopUp" CommandItemSettings-ShowExportToCsvButton="True" CommandItemSettings-ShowExportToExcelButton="True" CommandItemSettings-ShowExportToPdfButton="False" CommandItemSettings-ShowExportToWordButton="True" CommandItemSettings-ShowAddNewRecordButton="False" CommandItemDisplay="Top" DataKeyNames="ProviderUserKey,UserName">
<CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>

<RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
<HeaderStyle Width="20px"></HeaderStyle>
</RowIndicatorColumn>

<ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
<HeaderStyle Width="20px"></HeaderStyle>
</ExpandCollapseColumn>

    <Columns>
        <telerik:GridBoundColumn DataField="ProviderUserKey" 
            FilterControlAltText="Filter UserIdColumn column" HeaderText="User Id" 
            UniqueName="UserIdColumn" Visible="False" AllowSorting="False" AllowFiltering="False" MaxLength="10">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="UserName" 
            FilterControlAltText="Filter UserNameColumn column" HeaderText="User Name" 
            ShowFilterIcon="False" UniqueName="UserNameColumn" 
            AndCurrentFilterFunction="Contains" AutoPostBackOnFilter="True">
            <ItemStyle Wrap="False" />
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="CreationDate" 
            FilterControlAltText="Filter CreationDateColumn column" HeaderText="Created" 
            UniqueName="CreationDateColumn" AllowFiltering="False">
            <ItemStyle Wrap="False" />
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn AllowSorting="False" DataField="Email" 
            FilterControlAltText="Filter EMailColumn column" HeaderText="E-Mail" 
            ShowSortIcon="False" UniqueName="EMailColumn" 
            AndCurrentFilterFunction="Contains" ShowFilterIcon="False">
        </telerik:GridBoundColumn>
        <telerik:GridCheckBoxColumn CurrentFilterFunction="EqualTo" 
            DataField="IsOnline" FilterControlAltText="Filter IsOnlineColumn column" 
            HeaderText="Online" ShowFilterIcon="False" UniqueName="IsOnlineColumn" AutoPostBackOnFilter="True">
        </telerik:GridCheckBoxColumn>
        <telerik:GridCheckBoxColumn DataField="IsApproved" DataType="System.Boolean" 
            FilterControlAltText="Filter IsApprovedColumn column" HeaderText="Approved" 
            UniqueName="IsApprovedColumn" AndCurrentFilterFunction="EqualTo" 
            ShowFilterIcon="False" AutoPostBackOnFilter="True">
        </telerik:GridCheckBoxColumn>
        <telerik:GridCheckBoxColumn DataField="IsLockedOut" 
            FilterControlAltText="Filter IsLockedOutColumn column" HeaderText="Locked Out" 
            UniqueName="IsLockedOutColumn" AndCurrentFilterFunction="EqualTo" 
            ShowFilterIcon="False" AutoPostBackOnFilter="True">
        </telerik:GridCheckBoxColumn>
        <telerik:GridBoundColumn DataField="LastActivityDate" 
            DataType="System.DateTime" 
            FilterControlAltText="Filter LastActivityDateColumn column" 
            HeaderText="Last Activity Date" UniqueName="LastActivityDateColumn" 
            AllowFiltering="False">
            <ItemStyle Wrap="False" />
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="LastLoginDate" 
            FilterControlAltText="Filter LastLoginDateColumn column" 
            HeaderText="Last Login Date" UniqueName="LastLoginDateColumn" 
            AllowFiltering="False">
            <ItemStyle Wrap="False" />
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="LastPasswordChangedDate" 
            FilterControlAltText="Filter PasswordChangedDateColumn column" 
            HeaderText="Password Changed" UniqueName="PasswordChangedDateColumn" 
            AllowFiltering="False">
            <ItemStyle Wrap="False" />
        </telerik:GridBoundColumn>
        <telerik:GridButtonColumn 
            HeaderText="Delete" ImageUrl="~/Images/delete.png" 
            ConfirmText="Are you sure you want to delete this User?" 
            UniqueName="DeleteColumn" ButtonType="ImageButton"
            HeaderButtonType="PushButton" Text="Delete" CommandName="Delete">
            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
        </telerik:GridButtonColumn>
    </Columns>

<EditFormSettings>
<EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
</EditFormSettings>
</MasterTableView>

<FilterMenu EnableImageSprites="False"></FilterMenu>

<HeaderContextMenu CssClass="GridContextMenu GridContextMenu_WebBlue"></HeaderContextMenu>
</telerik:RadGrid>
<telerik:RadWindowManager ID="RadWindowManager1" runat="server" Skin="Vista" DestroyOnClose="True">
    <Windows>
        <telerik:RadWindow ID="RadWindowModifyUser" runat="server" Animation="Resize" Opacity="100"
            Skin="WebBlue" Title="Modify Terminal" AutoSize="True" Width="900px" Height="800px"
            KeepInScreenBounds="True" EnableShadow="True" EnableAriaSupport="True" VisibleStatusbar="True"
            Modal="True" DestroyOnClose="False">
        </telerik:RadWindow>
    </Windows>
</telerik:RadWindowManager>
