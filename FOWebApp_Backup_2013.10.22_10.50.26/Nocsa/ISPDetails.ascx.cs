﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;

namespace FOWebApp.Nocsa
{
    public partial class ISPDetails : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ButtonSubmit_Click(object sender, EventArgs e)
        {
            LabelResult.Visible = false;

            //Read data from controls and update
            try
            {
                BOAccountingControlWS boAccountingControl =
                             new BOAccountingControlWS();

                Isp isp = new Isp();
                Address address = new Address();
                isp.Address = address;
                isp.Id = Int32.Parse(TextBoxISPID.Text);
                isp.CompanyName = TextBoxISPName.Text;
                isp.Address.AddressLine1 = TextBoxAddressLine1.Text;
                isp.Address.AddressLine2 = TextBoxAddressLine2.Text;
                isp.Address.Location = TextBoxLocation.Text;
                isp.Address.Country = CountryList.SelectedValue;
                isp.Address.PostalCode = TextBoxPostCode.Text;
                isp.Phone = TextBoxPhone.Text;
                isp.Fax = TextBoxFax.Text;
                isp.Email = TextBoxEMail.Text;
                isp.Ispif_Password = TextBoxISPIFPassword.Text;
                isp.Ispif_User = TextBoxISPIFUserName.Text;
                isp.Dashboard_Password = TextBoxDashPassword.Text;
                isp.Dashboard_User = TextBoxDashUserName.Text;

                if (boAccountingControl.UpdateIsp(isp))
                {
                    LabelResult.ForeColor = Color.Green;
                    LabelResult.Text = "ISP creation/update successfully";
                }
                else
                {
                    LabelResult.ForeColor = Color.Red;
                    LabelResult.Text = "Could not create ISP!";
                }

            }
            catch (Exception ex)
            {
                LabelResult.ForeColor = Color.Red;
                LabelResult.Text = ex.Message;
            }

            LabelResult.Visible = true;
        }

        /// <summary>
        /// Initializes the form and loads the data for the 
        /// given isp id into the form fields.
        /// </summary>
        /// <param name="ispId">ISP ID</param>
        public void initForm(int ispId)
        {
            BOAccountingControlWS boAccountingControl =
                                            new BOAccountingControlWS();

            Isp isp = boAccountingControl.GetISP(ispId);

            if (isp != null)
            {
                TextBoxISPID.Text = isp.Id.ToString();
                TextBoxISPName.Text = isp.CompanyName;
                TextBoxAddressLine1.Text = isp.Address.AddressLine1;
                TextBoxAddressLine2.Text = isp.Address.AddressLine2;

                CountryList.SelectedValue = isp.Address.Country;

                TextBoxLocation.Text = isp.Address.Location;
                TextBoxEMail.Text = isp.Email;
                TextBoxPostCode.Text = isp.Address.PostalCode;
                TextBoxFax.Text = isp.Fax;
                TextBoxPhone.Text = isp.Phone;

                if (isp.Ispif_User != null)
                {
                    TextBoxISPIFUserName.Text = isp.Ispif_User.Trim();
                }

                if (isp.Ispif_Password != null)
                {
                    TextBoxISPIFPassword.Text = isp.Ispif_Password.Trim();
                }

                if (isp.Dashboard_User != null)
                {
                    TextBoxDashUserName.Text = isp.Dashboard_User.Trim();
                }

                if (isp.Dashboard_Password != null)
                {
                    TextBoxDashPassword.Text = isp.Dashboard_Password.Trim();
                }
            }
        }
    }
}