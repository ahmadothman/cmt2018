﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MultiCastGroupList.ascx.cs" Inherits="FOWebApp.Nocsa.MultiCastGroupList" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script type="text/javascript">
    function RowSelected(sender, eventArgs) {
        var grid = sender;
        var masterDataView = grid.get_masterTableView();
        var rowNum = eventArgs.get_itemIndexHierarchical();
        var row = masterDataView.get_dataItems()[rowNum];
        var ipAddress = masterDataView.getCellByColumnUniqueName(row, "IPAddress").innerHTML;

        //Initialize and show the terminal details window
        var oWnd = radopen("Nocsa/MultiCastDetailsForm.aspx?ip=" + ipAddress, "RadWindowModifyMultiGroup");
    }

    function CommandSelected(sender, eventArgs) {
        var grid = sender;
        if (eventArgs.get_commandName() == "ModifyTerminal") {
            var ipAddress = eventArgs.get_commandArgument();
            alert(macAddress);
            var oWnd = radopen("Nocsa/MultiCastDetailsForm.aspx?ip=" + ipAddress, "RadWindowModifyMultiGroup");
        }
    }
</script>
<telerik:RadGrid ID="RadGridMultiCastGroups" runat="server" AllowSorting="True" AutoGenerateColumns="False" CellSpacing="0"
    GridLines="None" ShowStatusBar="True" Skin="WebBlue"
    EnableLinqExpressions="False" OnNeedDataSource="RadGridMultiCastGroups_NeedDataSource">
        <ClientSettings>
        <Selecting AllowRowSelect="True" />
        <ClientEvents OnRowSelected="RowSelected" />
        <ClientEvents OnCommand="CommandSelected" />
    </ClientSettings>
    <MasterTableView AllowFilteringByColumn="True">
        <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>

        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
            <HeaderStyle Width="20px"></HeaderStyle>
        </RowIndicatorColumn>

        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
            <HeaderStyle Width="20px"></HeaderStyle>
        </ExpandCollapseColumn>

        <Columns>
            <telerik:GridBoundColumn DataField="IPAddress" FilterControlAltText="Filter IPAddress column" HeaderText="IP Address" UniqueName="IPAddress">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="MacAddress" FilterControlAltText="Filter MacAddress column" HeaderText="MAC Address" UniqueName="MacAddress">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Name" FilterControlAltText="Filter Name column" HeaderText="Name" UniqueName="Name">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="sourceURL" FilterControlAltText="Filter SourceURL column" HeaderText="Source URL" UniqueName="SourceURL">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="portNr" FilterControlAltText="Filter PortNr column" HeaderText="Port" UniqueName="PortNr">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Bandwidth" FilterControlAltText="Filter Bandwidth column" HeaderText="Bandwidth" UniqueName="Bandwidth">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="IspId" FilterControlAltText="Filter IspId column" HeaderText="Isp" UniqueName="IspId">
            </telerik:GridBoundColumn>
        </Columns>

        <EditFormSettings>
            <EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
        </EditFormSettings>
    </MasterTableView>

    <FilterMenu EnableImageSprites="False"></FilterMenu>

    <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_WebBlue"></HeaderContextMenu>
</telerik:RadGrid>
<telerik:RadWindowManager ID="RadWindowManager1" runat="server" Skin="Vista" DestroyOnClose="True">
    <Windows>
        <telerik:RadWindow ID="RadWindowModifyMultiGroup" runat="server" Animation="Resize" Opacity="100"
            Skin="WebBlue" Title="Modify Terminal" AutoSize="False" Width="850px" Height="700px"
            KeepInScreenBounds="True" EnableShadow="True" EnableAriaSupport="True" VisibleStatusbar="True"
            Modal="False" DestroyOnClose="False">
        </telerik:RadWindow>
    </Windows>
</telerik:RadWindowManager>