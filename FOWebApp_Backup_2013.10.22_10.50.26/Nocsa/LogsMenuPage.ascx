﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LogsMenuPage.ascx.cs"
    Inherits="FOWebApp.Nocsa.LogsMenuPage" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<style type="text/css">
    .style1
    {
        width: 250px;
    }
</style>
<table cellpadding="5" cellspacing="2">
    <tr>
        <td class="style1">
            <telerik:RadButton ID="RadButtonUsageLog" runat="server" Text="Usage Log" ButtonType="LinkButton"
                Skin="Sitefinity" onclick="RadButtonUsageLog_Click" Width="200px">
            </telerik:RadButton>
        </td>
    </tr>
    <tr>
        <td class="style1">
            <telerik:RadButton ID="RadButtonActivityLog" runat="server" Text="Activity Log" Skin="Sitefinity"
                ButtonType="LinkButton" onclick="RadButtonActivityLog_Click" Width="200px">
            </telerik:RadButton>
        </td>
    </tr>
    <tr>
        <td class="style1">
            <telerik:RadButton ID="RadButtonExceptionLog" runat="server" Text="Exception Log"
                Skin="Sitefinity" ButtonType="LinkButton" 
                onclick="RadButtonExceptionLog_Click" Width="200px">
            </telerik:RadButton>
        </td>
    </tr>
</table>
