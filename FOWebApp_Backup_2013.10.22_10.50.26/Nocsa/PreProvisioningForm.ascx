﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PreProvisioningForm.ascx.cs" Inherits="FOWebApp.Nocsa.PreProvisioningForm" %>
<table cellpadding="10" cellspacing="5">
    <tr>
        <td>
            Pre-Provisioning Mac Adddress:
        </td>
        <td>
            <asp:TextBox ID="TextBoxMacAddress" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Button ID="ButtonPreProv" runat="server" Text="Pre-Provision" 
                onclick="ButtonPreProv_Click" /> 
        </td>
        <td>
            <asp:Label ID="LabelResult" runat="server"></asp:Label>
        </td>
    </tr>
</table>