﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOMonitorControlWSRef;
using System.Drawing;

namespace FOWebApp.Nocsa
{
    public partial class NewFreeZone : System.Web.UI.UserControl
    {
        const string _HOUR = "<0|1|2><0|1|2|3|4|5|6|7|8|9>";
        const string _MINUTE = "<0|1|2|3|4|5><0|1|2|3|4|5|6|7|8|9>";
        const string _SECOND = "<0|1|2|3|4|5><0|1|2|3|4|5|6|7|8|9>";
        
        protected void Page_Load(object sender, EventArgs e)
        {
            TextBoxMonOff.Mask = _HOUR + "<:>" + _MINUTE + "<:>" + _SECOND;
            TextBoxMonOn.Mask = _HOUR + "<:>" + _MINUTE + "<:>" + _SECOND;
            TextBoxTueOff.Mask = _HOUR + "<:>" + _MINUTE + "<:>" + _SECOND;
            TextBoxTueOn.Mask = _HOUR + "<:>" + _MINUTE + "<:>" + _SECOND;
            TextBoxWedOff.Mask = _HOUR + "<:>" + _MINUTE + "<:>" + _SECOND;
            TextBoxWedOn.Mask = _HOUR + "<:>" + _MINUTE + "<:>" + _SECOND;
            TextBoxThuOff.Mask = _HOUR + "<:>" + _MINUTE + "<:>" + _SECOND;
            TextBoxThuOn.Mask = _HOUR + "<:>" + _MINUTE + "<:>" + _SECOND;
            TextBoxFriOff.Mask = _HOUR + "<:>" + _MINUTE + "<:>" + _SECOND;
            TextBoxFriOn.Mask = _HOUR + "<:>" + _MINUTE + "<:>" + _SECOND;
            TextBoxSatOff.Mask = _HOUR + "<:>" + _MINUTE + "<:>" + _SECOND;
            TextBoxSatOn.Mask = _HOUR + "<:>" + _MINUTE + "<:>" + _SECOND;
            TextBoxSunOff.Mask = _HOUR + "<:>" + _MINUTE + "<:>" + _SECOND;
            TextBoxSunOn.Mask = _HOUR + "<:>" + _MINUTE + "<:>" + _SECOND;
        }

        protected void ButtonSubmit_Click(object sender, EventArgs e)
        {
            FreeZoneCMT freeZone = new FreeZoneCMT();
            freeZone.Id = Convert.ToInt32(TextBoxId.Text);
            freeZone.Name = TextBoxName.Text;
            if (CheckBoxActive.Checked)
            {
                freeZone.CurrentActive = 1;
            }
            else
            {
                freeZone.CurrentActive = 0;
            }
            freeZone.MonOff = TextBoxMonOff.Text;
            freeZone.MonOn = TextBoxMonOn.Text;
            freeZone.TueOff = TextBoxTueOff.Text;
            freeZone.TueOn = TextBoxTueOn.Text;
            freeZone.WedOff = TextBoxWedOff.Text;
            freeZone.WedOn = TextBoxWedOn.Text;
            freeZone.ThuOff = TextBoxThuOff.Text;
            freeZone.ThuOn = TextBoxThuOn.Text;
            freeZone.FriOff = TextBoxFriOff.Text;
            freeZone.FriOn = TextBoxFriOn.Text;
            freeZone.SatOff = TextBoxSatOff.Text;
            freeZone.SatOn = TextBoxSatOn.Text;
            freeZone.SunOff = TextBoxSunOff.Text;
            freeZone.SunOn = TextBoxSunOn.Text;

            LabelResult.Text = "Creating free zone failed";
            LabelResult.ForeColor = Color.Red;

            BOMonitorControlWS boMonitorControl = new BOMonitorControlWS();
            FreeZone freeZoneNMS = this.cmtToNms(freeZone);
            if (boMonitorControl.NMScreateFreeZone(freeZoneNMS, ispId))
            {
                BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();
                if (boAccountingControl.CreateFreeZone(freeZone))
                {
                    LabelResult.Text = "Free zone created successfully";
                    LabelResult.ForeColor = Color.Green;
                }
            }
        }

        protected FreeZone cmtToNms(FreeZoneCMT cmt)
        {
            FreeZone nms = new FreeZone();
            nms.Id = cmt.Id;
            nms.Name = cmt.Name;
            nms.CurrentActive = cmt.CurrentActive;
            nms.MonOff = TextBoxMonOff.Text;
            nms.MonOn = TextBoxMonOn.Text;
            nms.TueOff = TextBoxTueOff.Text;
            nms.TueOn = TextBoxTueOn.Text;
            nms.WedOff = TextBoxWedOff.Text;
            nms.WedOn = TextBoxWedOn.Text;
            nms.ThuOff = TextBoxThuOff.Text;
            nms.ThuOn = TextBoxThuOn.Text;
            nms.FriOff = TextBoxFriOff.Text;
            nms.FriOn = TextBoxFriOn.Text;
            nms.SatOff = TextBoxSatOff.Text;
            nms.SatOn = TextBoxSatOn.Text;
            nms.SunOff = TextBoxSunOff.Text;
            nms.SunOn = TextBoxSunOn.Text;

            return nms;
        }
        public int ispId = 469; //ISP ID of the NMS ISP
    }
}