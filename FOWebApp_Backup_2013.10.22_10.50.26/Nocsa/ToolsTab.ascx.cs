﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using Telerik.Web.UI;
using FOWebApp.net.nimera.cmt.BOMonitorControlWSRef;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;

namespace FOWebApp.Nocsa
{
    /// <summary>
    /// Provides a few utilities to the NOCSA/NOCOP
    /// </summary>
    public partial class ToolsTab : System.Web.UI.UserControl
    {
        BOMonitorControlWS _boMonitorControl = new BOMonitorControlWS();
        BOAccountingControlWS _boAccountingControl = new BOAccountingControlWS();

        /// <summary>
        /// The terminals MacAddress
        /// </summary>
        public String MacAddress
        {
            get;
            set;
        }

        /// <summary>
        /// The Terminals ISP Id
        /// </summary>
        public int IspId
        {
            get;
            set;
        }

        /// <summary>
        /// The terminal fullname
        /// </summary>
        public string FullName
        {
            get;
            set;
        }

        /// <summary>
        /// The terminal External Fullname
        /// </summary>
        public string ExtFullName
        {
            get;
            set;
        }

        /// <summary>
        /// The current admin status of the terminal
        /// </summary>
        public string AdmStatus
        {
            get;
            set;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LabelExtFullName.Text = ExtFullName;
                HiddenFieldISP.Value = IspId.ToString();
                HiddenFieldMacAddress.Value = MacAddress;

                TerminalStatus[] termStatusList = _boAccountingControl.GetTerminalStatus();

                foreach (TerminalStatus termStatus in termStatusList)
                {
                    RadComboBoxItem item = new RadComboBoxItem();
                    if (termStatus.Id != 6)
                    {
                        item.Value = termStatus.Id.ToString();
                        item.Text = termStatus.StatusDesc;
                        RadComboBoxAdminStatus.Items.Add(item);
                    }
                }

                RadComboBoxAdminStatus.SelectedValue = AdmStatus;
            }
        }

        protected void RadButtonSynchronize_Click(object sender, EventArgs e)
        {
            TerminalInfo ti = _boMonitorControl.getTerminalDetailsFromHub(HiddenFieldMacAddress.Value, Int32.Parse(HiddenFieldISP.Value));
            if (ti != null)
            {
                LabelExtFullName.Text = ti.EndUserId;
               
                //Read the current data from the CMT database
                Terminal term = _boAccountingControl.GetTerminalDetailsByMAC(HiddenFieldMacAddress.Value);
                term.ExtFullName = ti.EndUserId;

                if (_boAccountingControl.UpdateTerminal(term))
                {
                    LabelResult.ForeColor = Color.DarkGreen;
                    LabelResult.Text = "Synchronization succeeded, please refresh the form";
                }
                else
                {
                    LabelResult.ForeColor = Color.DarkRed;
                    LabelResult.Text = "Synchronization failed, please check error log";
                }
            }
            else
            {
                LabelResult.ForeColor = Color.DarkRed;
                LabelResult.Text = "Sorry but this terminal does not exist in the Hub";
            }
        }

        protected void RadButtonSubmit_Click(object sender, EventArgs e)
        {
            int admStatus = Int32.Parse(RadComboBoxAdminStatus.SelectedValue);
            //Read the current data from the CMT database
            Terminal term = _boAccountingControl.GetTerminalDetailsByMAC(HiddenFieldMacAddress.Value);

            term.AdmStatus = admStatus;

            if (_boAccountingControl.UpdateTerminal(term))
            {
                LabelAdmStatusChangeResult.ForeColor = Color.DarkGreen;
                LabelAdmStatusChangeResult.Text = "Admin Status updated successfully, please refresh the form";
            }
            else
            {
                LabelAdmStatusChangeResult.ForeColor = Color.DarkRed;
                LabelAdmStatusChangeResult.Text = "Admin Status update failed, please check error log";
            }
        }
    }
}