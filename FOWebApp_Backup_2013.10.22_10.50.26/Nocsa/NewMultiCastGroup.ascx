﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NewMultiCastGroup.ascx.cs" Inherits="FOWebApp.Nocsa.NewMultiCastGroup" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<table cellpadding="10" cellspacing="5">
    <tr>
        <td class="requiredLabel">IP Address:</td>
        <td>
            <asp:TextBox ID="TextBoxIPAddress" runat="server" ControlToValidate="TextBoxIPAddress"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="You must provide an IP Address" ControlToValidate="TextBoxIPAddress"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>Mac Address:</td>
        <td>
            <asp:TextBox ID="TextBoxMacAddress" runat="server" Width="280px" Enabled="False"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>Name:</td>
        <td class="requiredLabel">
            <asp:TextBox ID="TextBoxName" runat="server" Width="430px"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="You must provide a Name" ControlToValidate="TextBoxName"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>Source URL:</td>
        <td>
            <asp:TextBox ID="TextBoxSourceURL" runat="server" Width="427px"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>Port Nr:</td>
        <td>
            <asp:TextBox ID="TextBoxPortNr" runat="server" Width="45px"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>Bandwidth:</td>
        <td>
            <asp:TextBox ID="TextBoxBandwidth" runat="server"></asp:TextBox>
        </td>
    </tr>
     <tr>
        <td>State:</td>
        <td>
            <asp:CheckBox ID="CheckBoxState" runat="server" Enabled="False" />
        </td>
    </tr>
    <tr>
        <td>IspId:</td>
        <td>
            <telerik:RadComboBox ID="RadComboBoxISP" runat="server" Width="351px"></telerik:RadComboBox>
        </td>
    </tr>
    <tr>
        <td>FirstActivationDate:</td>
        <td>
            <telerik:RadDatePicker ID="RadDatePickerFirstActivationDate" runat="server" Skin="WebBlue">
                    <Calendar ID="Calendar4" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False"
                        ViewSelectorText="x" Skin="WebBlue" runat="server">
                    </Calendar>
                    <DateInput ID="DateInput2" DisplayDateFormat="d/MM/yyyy" DateFormat="d/MM/yyyy" runat="server">
                    </DateInput>
                    <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                </telerik:RadDatePicker>
        </td>
    </tr>
    <tr>
        <td>Distributor:</td>
        <td>
            <Telerik:RadComboBox ID="RadComboBoxDistributor" runat="server" Width="422px"></Telerik:RadComboBox>
        </td>
    </tr>
    <tr>
        <td>Customer:</td>
        <td>
            <asp:DropDownList ID="DropDownListCustomer" runat="server" Width="421px"></asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Button ID="ButtonSubmit" runat="server" Text="Submit" OnClick="ButtonSubmit_Click" />
        </td>
        <td>
            <asp:Label ID="LabelResult" runat="server"></asp:Label>
        </td>
    </tr>
</table>