﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdvancedSearch2.ascx.cs"
    Inherits="FOWebApp.Nocsa.AdvancedSearch2" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<telerik:RadCodeBlock ID="RadCodeBlockAdvancedSearch2" runat="server">
    <script type="text/javascript">
        function RowSelected(sender, eventArgs) {
            var grid = sender;
            var masterDataView = grid.get_masterTableView();
            var rowNum = eventArgs.get_itemIndexHierarchical();
            var row = masterDataView.get_dataItems()[rowNum];
            var macAddress = masterDataView.getCellByColumnUniqueName(row, "MacAddress").innerHTML;

            //Initialize and show the terminal details window
            var oWnd = radopen("Nocsa/TerminalForm.aspx?mac=" + macAddress, "RadWindowModifyTerminal");
        }

        function CommandSelected(sender, eventArgs) {
            var grid = sender;
            if (eventArgs.get_commandName() == "ModifyTerminal") {
                var macAddress = eventArgs.get_commandArgument();
                alert(macAddress);
                var oWnd = radopen("Nocsa/TerminalForm.aspx?mac=" + macAddress, "RadWindowModifyTerminal");
            }
        }

        function ClearForm() {
            $find("<%= RadTextBoxFullName.ClientID %>").clear();
            $find("<%= RadComboBoxISP.ClientID %>").clearSelection();
            $find("<%= RadMaskedTextBoxSitId.ClientID %>").clear();
            $find("<%= RadComboBoxDistributor.ClientID %>").clearSelection();
            $find("<%= RadComboBoxAdminState.ClientID %>").clearSelection();
            $find("<%= RadTextBoxIPAddress.ClientID %>").clear();
            $find("<%= RadTextBoxMacAddress.ClientID %>").clear();
            $find("<%= RadTextBoxSerial.ClientID %>").clear();
            $find("<%= RadComboBoxSLA.ClientID %>").clearSelection();
            $find("<%= RadMaskedTextBoxFWDPool.ClientID  %>").clear();
            $find("<%= RadMaskedTextBoxRTNPool.ClientID %>").clear();
            var combo = $find("<%= RadComboBoxExpiryDate.ClientID %>");
            combo.trackChanges();
            combo.get_items().getItem(0).select();
            combo.updateClientState();
            combo.commitChanges();
            $find("<%= RadTextBoxEMail.ClientID %>").clear();
    }
    </script>
</telerik:RadCodeBlock>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="RadButtonQuery">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="LabelNumberOfTerminals" />
                <telerik:AjaxUpdatedControl ControlID="LabelNumTerminals" />
                <telerik:AjaxUpdatedControl ControlID="RadGridTerminals" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<table>
    <tr>
        <td valign="top">
            <table>
                <!-- Inner table -->
                <tr>
                    <td colspan="2">ISP:
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <telerik:RadComboBox ID="RadComboBoxISP" runat="server" Skin="WebBlue"
                            Width="272px">
                        </telerik:RadComboBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">SIT:
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <telerik:RadMaskedTextBox ID="RadMaskedTextBoxSitId" runat="server" Mask="############">
                        </telerik:RadMaskedTextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">Terminal Name:
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <telerik:RadTextBox ID="RadTextBoxFullName" runat="server" Skin="WebBlue"
                            Width="272px">
                        </telerik:RadTextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">Distributor:
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <telerik:RadComboBox ID="RadComboBoxDistributor" runat="server" Skin="WebBlue"
                            Width="272px">
                        </telerik:RadComboBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">Admin State:
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <telerik:RadComboBox ID="RadComboBoxAdminState" runat="server" Skin="WebBlue">
                        </telerik:RadComboBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">IP Address:
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <telerik:RadTextBox ID="RadTextBoxIPAddress" runat="server" Columns="39">
                        </telerik:RadTextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">MAC Address:
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <telerik:RadTextBox ID="RadTextBoxMacAddress" runat="server">
                        </telerik:RadTextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">Serial:
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <telerik:RadTextBox ID="RadTextBoxSerial" runat="server">
                        </telerik:RadTextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">SLA:
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <telerik:RadComboBox ID="RadComboBoxSLA" runat="server" Width="274px">
                        </telerik:RadComboBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">Forward pool:
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <telerik:RadMaskedTextBox ID="RadMaskedTextBoxFWDPool" runat="server"
                            Skin="WebBlue" Mask="#####" Width="50px">
                        </telerik:RadMaskedTextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">Return pool:
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <telerik:RadMaskedTextBox ID="RadMaskedTextBoxRTNPool" runat="server"
                            Skin="WebBlue" Mask="#####" Width="50px">
                        </telerik:RadMaskedTextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">Expiry Date:
                    </td>
                </tr>
                <tr>
                    <td>
                        <telerik:RadDatePicker ID="RadDatePickerExpiryDate" runat="server" Culture="nl-BE"
                            Skin="WebBlue">
                            <Calendar ID="Calendar1" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False"
                                ViewSelectorText="x" Skin="WebBlue" runat="server">
                            </Calendar>
                            <DateInput ID="DateInput1" DisplayDateFormat="d/MM/yyyy" DateFormat="d/MM/yyyy"
                                runat="server">
                            </DateInput>
                            <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                        </telerik:RadDatePicker>
                    </td>
                    <td>
                        <telerik:RadComboBox ID="RadComboBoxExpiryDate" runat="server" Skin="WebBlue" Font-Bold="True"
                            ForeColor="#0066FF" Width="50px">
                            <Items>
                                <telerik:RadComboBoxItem runat="server" Text="&lt;" Value="&lt;" />
                                <telerik:RadComboBoxItem runat="server" Text="&gt;" Value="&gt;" />
                                <telerik:RadComboBoxItem runat="server" Text="=&lt;" Value="=&lt;" />
                                <telerik:RadComboBoxItem runat="server" Text="&gt;=" Value="&gt;=" />
                                <telerik:RadComboBoxItem runat="server" Text="=" Value="=" />
                            </Items>
                        </telerik:RadComboBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">E-Mail:
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <telerik:RadTextBox ID="RadTextBoxEMail" runat="server" Width="272px">
                        </telerik:RadTextBox>
                    </td>
                </tr>
                <tr>
                    <td align="center" height="50px">
                        <telerik:RadButton ID="RadButtonQuery" runat="server" Text="Search Terminals"
                            OnClick="RadButtonQuery_Click" Skin="WebBlue">
                        </telerik:RadButton>
                    </td>
                    <td align="center" height="50px">
                        <telerik:RadButton ID="RadButtonClear" runat="server" Text="Clear Form"
                            Skin="WebBlue" OnClientClicked="ClearForm" AutoPostBack="False">
                        </telerik:RadButton>
                    </td>
                </tr>
            </table>
        </td>
        <td valign="top">
            <table>
                <tr>
                    <td width="15%">
                        <asp:Label ID="LabelNumberOfTerminals" runat="server" Text="Number of terminals retrieved:" Visible="False" Width="200px"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="LabelNumTerminals" Text="0" runat="server" Visible="False" ForeColor="#0033CC"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <telerik:RadGrid ID="RadGridTerminals" runat="server" Skin="WebBlue"
                            AutoGenerateColumns="False" CellSpacing="0" GridLines="None" Width="850px"
                            Visible="False" AllowSorting="True" Height="550px" ShowFooter="True">
                            <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True">
                                <Selecting AllowRowSelect="True" />
                                <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                                <ClientEvents OnRowSelected="RowSelected" />
                            </ClientSettings>
                            <MasterTableView>
                                <CommandItemTemplate>
                                    <div>
                                        <div style="float: left">
                                            <asp:Label ID="LabelTotals" runat="server" Text="Label"></asp:Label>
                                        </div>
                                    </div>
                                </CommandItemTemplate>
                                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>

                                <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
                                    <HeaderStyle Width="20px"></HeaderStyle>
                                </RowIndicatorColumn>

                                <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
                                    <HeaderStyle Width="20px"></HeaderStyle>
                                </ExpandCollapseColumn>

                                <Columns>
                                    <telerik:GridBoundColumn DataField="FullName"
                                        FilterControlAltText="Filter FullName column" HeaderText="Name"
                                        UniqueName="FullName" MaxLength="50">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="MacAddress"
                                        FilterControlAltText="Filter MacAddress column" HeaderText="MAC Address"
                                        UniqueName="MacAddress">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="SitId"
                                        FilterControlAltText="Filter SitId column" HeaderText="SIT" UniqueName="SitId">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="IpAddress" FilterControlAltText="Filter IpAddress column" HeaderText="IP" UniqueName="IpAddress">
                                    </telerik:GridBoundColumn>
                                </Columns>

                                <EditFormSettings>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
                                </EditFormSettings>
                            </MasterTableView>

                            <FilterMenu EnableImageSprites="False"></FilterMenu>

                            <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_WebBlue"></HeaderContextMenu>
                        </telerik:RadGrid>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<telerik:RadWindowManager ID="RadWindowManager1" runat="server" Skin="Vista" DestroyOnClose="True">
    <Windows>
        <telerik:RadWindow ID="RadWindowTerminalDetails" runat="server"
            Animation="Resize" Opacity="100" Skin="WebBlue" Title="Terminal Details" AutoSize="False"
            Width="1000px" Height="700px" KeepInScreenBounds="True" EnableShadow="True" EnableAriaSupport="True"
            VisibleStatusbar="True" Modal="True" DestroyOnClose="False">
        </telerik:RadWindow>
        <telerik:RadWindow ID="RadWindowModifyTerminal" runat="server" Animation="Resize" Opacity="100"
            Skin="WebBlue" Title="Modify Terminal" AutoSize="False" Width="850px" Height="700px"
            KeepInScreenBounds="True" EnableShadow="True" EnableAriaSupport="True" VisibleStatusbar="True"
            Modal="False" DestroyOnClose="False">
        </telerik:RadWindow>
        <telerik:RadWindow ID="RadWindowModifyActivity" runat="server" Animation="Resize" Opacity="100"
            Skin="WebBlue" Title="Modify Activity" AutoSize="False" Width="850px" Height="655px"
            KeepInScreenBounds="True" EnableShadow="True" EnableAriaSupport="True" VisibleStatusbar="True"
            Modal="False" DestroyOnClose="False">
        </telerik:RadWindow>
    </Windows>
</telerik:RadWindowManager>

