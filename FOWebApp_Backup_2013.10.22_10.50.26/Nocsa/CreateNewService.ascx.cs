﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using FOWebApp.net.nimera.cmt.BOServicesControllerWSRef;
using Telerik.Web.UI;
using System.Drawing;
using System.Web.Security;

namespace FOWebApp.Nocsa
{
    public partial class CreateNewService : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            servicesController = new BOServicesControllerWS();

            // fill up the parent lists
            Service[] servicesLevel1 = servicesController.GetAllAvailableServices("1");

            foreach (Service service in servicesLevel1)
            {
                RadComboBoxItem item = new RadComboBoxItem();
                item.Value = service.ServiceId.ToString();
                item.Text = service.ServiceName;
                RadComboBoxParent1.Items.Add(item);
            }
            RadComboBoxParent1.Items.Insert(0, new RadComboBoxItem(""));
            RadComboBoxParent1.ClearSelection();

            if (this.Session["CreateNewService_Parent1"] != null)
            {
                parent1Id = this.Session["CreateNewService_Parent1"].ToString();
            }
            RadComboBoxParent1.SelectedValue = "" + parent1Id;

            Service serviceTemp = servicesController.GetService(parent1Id);
            Service[] servicesLevel2 = servicesController.GetChildren(serviceTemp);

            foreach (Service service in servicesLevel2)
            {
                RadComboBoxItem item = new RadComboBoxItem();
                item.Value = service.ServiceId.ToString();
                item.Text = service.ServiceName;
                RadComboBoxParent2.Items.Add(item);
            }
            RadComboBoxParent2.Items.Insert(0, new RadComboBoxItem(""));
            RadComboBoxParent2.SortItems();

            // fill up file list
            string[] folders = { "Nocsa/", "Nocop/", "Distributors/", "Organizations/" };
            List<string> fileList = new List<string>();
            string[] fileListExceptions = { "ISPList.ascx", "PaypalCheckoutReview.ascx" };

            for (int i = 0; i < folders.Length; i++)
            {
                DirectoryInfo dirInfo = new DirectoryInfo(Server.MapPath(folders[i]));
                FileInfo[] fileInfo = dirInfo.GetFiles("*.ascx"); ;

                foreach (FileInfo fi in fileInfo)
                {
                    bool add = true;

                    foreach (string exception in fileListExceptions)
                    {
                        if (fi.Name == exception)
                        {
                            add = false;
                            break;
                        }
                    }

                    if (add)
                    {
                        fileList.Add(folders[i] + fi.Name);
                    }
                }
            }

            RadGridServiceControllers.DataSource = fileList;
            RadGridServiceControllers.DataBind();
        }
        
        protected void ButtonSubmit_Click(object sender, EventArgs e)
        {
            if (RadioButtonLevel1.Checked || RadioButtonLevel2.Checked || RadioButtonLevel3.Checked)
            {
                // add service data
                Service newService = new Service();
                newService.ServiceName = TextBoxServiceName.Text.ToString();
                newService.ServiceDescription = TextBoxDescription.Text.ToString();
                newService.ServiceController = TextBoxController.Text.ToString();
                newService.Icon = "";
                // check service level and add parent and/or icon if applicable
                if (RadioButtonLevel1.Checked)
                {
                    newService.Level = 1;
                    newService.ParentServiceId = "none";
                    // upload icon file
                    if (FileUpload1.HasFile)
                    {
                        // only files of type jpeg and png are accepted
                        if (FileUpload1.PostedFile.ContentType == "image/jpeg" || FileUpload1.PostedFile.ContentType == "image/png")
                        {
                            // only files smaller than 512kb are accepted
                            if (FileUpload1.PostedFile.ContentLength < 512000)
                            {
                                try
                                {
                                    string filename = Path.GetFileName(FileUpload1.FileName);
                                    FileUpload1.SaveAs("C:\\temp\\" + filename);
                                    newService.Icon = filename;
                                }
                                catch (Exception ex)
                                {
                                    LabelUploadStatus.Text = "The file could not be uploaded. The following error occured: " + ex.Message;
                                    LabelUploadStatus.ForeColor = Color.Red;
                                    return;
                                }
                            }
                            else
                            {
                                LabelUploadStatus.Text = "The file is too large. Please use a smaller file";
                                LabelUploadStatus.ForeColor = Color.Red;
                                return;
                            }
                        }
                        else
                        {
                            LabelUploadStatus.Text = "Please select a JPEG or a PNG file";
                            LabelUploadStatus.ForeColor = Color.Red;
                            return;
                        }
                    }
                    //else
                    //{
                    //    LabelUploadStatus.Text = "Please select a file for upload";
                    //    LabelUploadStatus.ForeColor = Color.Red;
                    //    return;
                    //}
                }
                else if (RadioButtonLevel2.Checked)
                {
                    newService.Level = 2;
                    newService.ParentServiceId = RadComboBoxParent1.SelectedValue;
                }
                else if (RadioButtonLevel3.Checked)
                {
                    newService.Level = 3;
                    newService.ParentServiceId = RadComboBoxParent2.SelectedItem.Value;
                }

                // add the GUID of the service
                MembershipUser user;
                try
                {
                    user = Membership.CreateUser(TextBoxController.Text.ToString(), Membership.GeneratePassword(8, 2));
                }
                catch (Exception ex)
                {
                    LabelSubmit.Text = "Adding service failed: controller already in use";
                    LabelSubmit.ForeColor = Color.Red;
                    return;
                }
                newService.ServiceId = (Guid)user.ProviderUserKey;

                // store the service in the database
                bool add = servicesController.UpdateService(newService);
                if (add)
                {
                    LabelSubmit.Text = "Service successfully added";
                    LabelSubmit.ForeColor = Color.Green;
                }
                else
                {
                    LabelSubmit.Text = "Adding service failed. See logs for details.";
                    LabelSubmit.ForeColor = Color.Red;
                }
            }
        }

        protected void RadXmlHttpPanelParent2_ServiceRequest(object sender, Telerik.Web.UI.RadXmlHttpPanelEventArgs e)
        {
            parent1Id = e.Value;
            Service serviceTemp = servicesController.GetService(parent1Id);
            Service[] servicesLevelTwo = servicesController.GetChildren(serviceTemp);

            RadComboBoxParent2.ClearSelection();
            RadComboBoxParent2.Items.Clear();

            foreach (Service service in servicesLevelTwo)
            {
                RadComboBoxItem item = new RadComboBoxItem();
                item.Value = service.ServiceId.ToString();
                item.Text = service.ServiceName;
                RadComboBoxParent2.Items.Add(item);
            }
            RadComboBoxParent2.Items.Insert(0, new RadComboBoxItem(""));
            RadComboBoxParent2.SelectedIndex = 0;

            //Store the parent ID for successive pageloads
            this.Session["CreateNewService_Parent1"] = parent1Id;
        }

        public BOServicesControllerWS servicesController;
        public string parent1Id = "";

    }
}