﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TerminalList.ascx.cs"
    Inherits="FOWebApp.Nocsa.TerminalList" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script type="text/javascript">
    function RowSelected(sender, eventArgs) {
        var grid = sender;
        var masterDataView = grid.get_masterTableView();
        var rowNum = eventArgs.get_itemIndexHierarchical();
        var row = masterDataView.get_dataItems()[rowNum];
        var macAddress = masterDataView.getCellByColumnUniqueName(row, "MacAddress").innerHTML;

        //Initialize and show the terminal details window
        var oWnd = radopen("Nocsa/TerminalForm.aspx?mac=" + macAddress, "RadWindowModifyTerminal");
    }

    function CommandSelected(sender, eventArgs) {
        var grid = sender;
        if (eventArgs.get_commandName() == "ModifyTerminal") {
            var macAddress = eventArgs.get_commandArgument();
            alert(macAddress);
            var oWnd = radopen("Nocsa/TerminalForm.aspx?mac=" + macAddress, "RadWindowModifyTerminal");
        }
    }
</script>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxyTerminalList" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="RadGridTerminals">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGridTerminals" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadGrid ID="RadGridTerminals" runat="server" AllowPaging="True" AllowSorting="True"
    AutoGenerateColumns="False" CellSpacing="0" GridLines="None" 
    Skin="WebBlue" ShowStatusBar="True"
    Width="100%" AllowFilteringByColumn="True" OnItemDataBound="RadGridTerminals_ItemDataBound"
    OnNeedDataSource="RadGridTerminals_NeedDataSource" OnColumnCreated="RadGridTerminals_ColumnCreated"
    OnItemCreated="RadGridTerminals_ItemCreated" 
    onitemcommand="RadGridTerminals_ItemCommand" 
    onpageindexchanged="RadGridTerminals_PageIndexChanged" 
    onsortcommand="RadGridTerminals_SortCommand" 
    onpagesizechanged="RadGridTerminals_PageSizeChanged" 
    onprerender="RadGridTerminals_PreRender" EnableLinqExpressions="False">
    <GroupingSettings CaseSensitive="false" /> 
    <ExportSettings ExportOnlyData="True" IgnorePaging="True" 
        OpenInNewWindow="True">
        <Excel Format="ExcelML" />
    </ExportSettings>
    <ClientSettings>
        <Selecting AllowRowSelect="True" />
        <ClientEvents OnRowSelected="RowSelected" />
        <ClientEvents OnCommand="CommandSelected" />
    </ClientSettings>
    <MasterTableView CommandItemDisplay="Top" CommandItemSettings-ShowExportToExcelButton="True"
        CommandItemSettings-ShowExportToCsvButton="True" RowIndicatorColumn-Visible="True"
        RowIndicatorColumn-Resizable="True" RowIndicatorColumn-AndCurrentFilterFunction="Contains"
        CommandItemSettings-ShowAddNewRecordButton="False" PageSize="20" CommandItemSettings-ShowExportToWordButton="True">
        <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
        </RowIndicatorColumn>
        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
        </ExpandCollapseColumn>
        <Columns>
            <telerik:GridBoundColumn DataField="FullName" FilterControlAltText="Filter FullNameColumn column"
                HeaderText="Name" UniqueName="FullName" ShowFilterIcon="True" 
                AutoPostBackOnFilter="True" CurrentFilterFunction="Contains">
                <ItemStyle Wrap="True" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="MacAddress" FilterControlAltText="Filter MacAddressColumn column"
                HeaderText="MAC Address" MaxLength="12" ReadOnly="True" Resizable="False" UniqueName="MacAddress"
                CurrentFilterFunction="Contains" ShowFilterIcon="True" 
                AutoPostBackOnFilter="True">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="SitId" FilterControlAltText="Filter SitIdColumn column"
                HeaderText="Site ID" UniqueName="SitId" CurrentFilterFunction="Contains">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="IpAddress" FilterControlAltText="Filter IpAddressColumn column"
                HeaderText="IP Address" ReadOnly="True" UniqueName="IpAddress" ShowFilterIcon="True"
                AutoPostBackOnFilter="True" CurrentFilterFunction="Contains">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn AllowSorting="True" DataField="AdmStatus" FilterControlAltText="Filter AdmStatusColumn column"
                HeaderText="Status" ReadOnly="True" UniqueName="AdmStatus" ShowFilterIcon="False"
                AutoPostBackOnFilter="True" AllowFiltering="True" ItemStyle-Wrap="False">
                <ItemStyle Wrap="False"></ItemStyle>
            </telerik:GridBoundColumn>
            <telerik:GridDateTimeColumn DataField="StartDate" FilterControlAltText="Filter StartDateColumn column"
                HeaderText="Start Date" ReadOnly="True" UniqueName="StartDate" ItemStyle-Wrap="False"
                ShowFilterIcon="True" DataFormatString="{0:dd/MM/yyyy}" 
                PickerType="DatePicker" DataType="System.DateTime">
                <ItemStyle Wrap="False"></ItemStyle>
            </telerik:GridDateTimeColumn>
            <telerik:GridDateTimeColumn AllowSorting="True" DataField="ExpiryDate" DataType="System.DateTime"
                FilterControlAltText="Filter ExpiryDateColumn column" HeaderText="Expiry Date"
                ReadOnly="True" UniqueName="ExpiryDate" ItemStyle-Wrap="False" ShowFilterIcon="True"
                AutoPostBackOnFilter="True" DataFormatString="{0:dd/MM/yyyy}" 
                PickerType="DatePicker">
                <ItemStyle Wrap="False"></ItemStyle>
            </telerik:GridDateTimeColumn>
            <telerik:GridBoundColumn DataField="CNo" 
                FilterControlAltText="Filter CNo column" HeaderText="C/No" UniqueName="CNo" 
                Visible="False">
            </telerik:GridBoundColumn>
        </Columns>
        <EditFormSettings>
            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
            </EditColumn>
        </EditFormSettings>
        <PagerStyle AlwaysVisible="True" />
    </MasterTableView>
    <FilterMenu EnableImageSprites="False">
    </FilterMenu>
    <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default">
    </HeaderContextMenu>
</telerik:RadGrid>
<telerik:RadWindowManager ID="RadWindowManager1" runat="server" Skin="Vista" DestroyOnClose="True">
    <Windows>
        <telerik:RadWindow ID="RadWindowTerminalDetails" runat="server" 
            Animation="Resize" Opacity="100" Skin="WebBlue" Title="Terminal Details" AutoSize="False"
            Width="1000px" Height="700px" KeepInScreenBounds="True" EnableShadow="True" EnableAriaSupport="True"
            VisibleStatusbar="True" Modal="True" DestroyOnClose="False">
        </telerik:RadWindow>
        <telerik:RadWindow ID="RadWindowModifyTerminal" runat="server" Animation="Resize" Opacity="100"
            Skin="WebBlue" Title="Modify Terminal" AutoSize="False" Width="850px" Height="700px"
            KeepInScreenBounds="True" EnableShadow="True" EnableAriaSupport="True" VisibleStatusbar="True"
            Modal="False" DestroyOnClose="False">
        </telerik:RadWindow>
        <telerik:RadWindow ID="RadWindowModifyActivity" runat="server" Animation="Resize" Opacity="100"
            Skin="WebBlue" Title="Modify Activity" AutoSize="False" Width="850px" Height="655px"
            KeepInScreenBounds="True" EnableShadow="True" EnableAriaSupport="True" VisibleStatusbar="True"
            Modal="False" DestroyOnClose="False">
        </telerik:RadWindow>
    </Windows>
</telerik:RadWindowManager>
