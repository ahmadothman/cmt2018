﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NewActivityLogItem.ascx.cs"
    Inherits="FOWebApp.Nocsa.NewActivityLogItem" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy2" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="RadButtonSubmit">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="LabelStatus" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
    <script type="text/javascript" language="javascript">
        function nodeClickingSla(sender, args) {
            var comboBox = $find("<%= RadComboBoxSLA.ClientID %>");

            var node = args.get_node()
            if (node.get_level() == 1) {

                comboBox.set_text(node.get_text());
                comboBox.set_value(node.get_value());
                document.getElementById('HiddenFieldSLAId').value = node.get_value();
                comboBox.trackChanges();
                comboBox.get_items().getItem(0).set_text(node.get_text());
                comboBox.commitChanges();

                comboBox.hideDropDown();
            }
        }

        function nodeClickingNewSla(sender, args) {
            var comboBox = $find("<%= RadComboBoxNewSla.ClientID %>");

            var node = args.get_node()
            if (node.get_level() == 1) {

                comboBox.set_text(node.get_text());
                comboBox.set_value(node.get_value());
                document.getElementById('HiddenFieldNewSlAId').value = node.get_value();
                comboBox.trackChanges();
                comboBox.get_items().getItem(0).set_text(node.get_text());
                comboBox.commitChanges();

                comboBox.hideDropDown();
            }
        }
    </script>
</telerik:RadCodeBlock>
<asp:HiddenField ID="HiddenFieldSLAId" runat="server" ClientIDMode="Static" />
<asp:HiddenField ID="HiddenFieldNewSlAId" runat="server" ClientIDMode="Static" />
<asp:HiddenField ID="HiddenFieldActivityId" runat ="server" ClientIDMode="Static" />
<h2>
    <asp:Label ID="LabelTitle" runat="server"></asp:Label>
</h2>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="RadButtonSubmit">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="LabelStatus" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<div id="ActivityDiv">
<table cellpadding="10" cellspacing="5">
    <tr>
        <td>
            Terminal MAC address:
        </td>
        <td>
            <telerik:RadMaskedTextBox ID="RadMaskedTextBoxMacAddress" runat="server" Skin="WebBlue"
                Width="125px">
            </telerik:RadMaskedTextBox>
            <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Please provide a valid MAC address!"
                ControlToValidate="RadMaskedTextBoxMacAddress" Operator="NotEqual" ValueToCompare="00:00:00:00:00:00"></asp:CompareValidator>
        </td>
    </tr>
    <tr>
        <td>
            Service Level Agreement:
        </td>
        <td>
            <telerik:RadComboBox ID="RadComboBoxSLA" runat="server" Skin="WebBlue" Sort="Ascending"
                ExpandDirection="Up" Width="352px" EnableVirtualScrolling="True" EmptyMessage="Select a Service Pack"
                MaxHeight="200px">
                <ItemTemplate>
                    <telerik:RadTreeView ID="RadTreeViewSLA" runat="server" Skin="WebBlue" OnClientNodeClicking="nodeClickingSla">
                    </telerik:RadTreeView>
                </ItemTemplate>
                <Items>
                    <telerik:RadComboBoxItem Text="" />
                </Items>
            </telerik:RadComboBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorSLA" runat="server" 
                ErrorMessage="Please provide an SLA" ControlToValidate="RadComboBoxSLA"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            New Service Level Agreement:
        </td>
        <td>
            <telerik:RadComboBox ID="RadComboBoxNewSla" runat="server" Skin="WebBlue" Sort="Ascending"
                ExpandDirection="Up" Width="352px" EnableVirtualScrolling="True" EmptyMessage="Select a Service Pack"
                MaxHeight="200px" ToolTip="Select an SLA if this activity is an SLA change">
                <ItemTemplate>
                    <telerik:RadTreeView ID="RadTreeViewNewSLA" runat="server" Skin="WebBlue" OnClientNodeClicking="nodeClickingNewSla">
                    </telerik:RadTreeView>
                </ItemTemplate>
                <Items>
                    <telerik:RadComboBoxItem Text="" />
                </Items>
            </telerik:RadComboBox>
        </td>
    </tr>
    <tr>
        <td>
            Activity Date:
        </td>
        <td>
            <telerik:RadDatePicker ID="RadDatePickerTimeStamp" runat="server">
            </telerik:RadDatePicker>
        </td>
    </tr>
    <tr>
        <td>
            Terminal Activity:
        </td>
        <td>
            <telerik:RadComboBox ID="RadComboBoxTerminalActivity" runat="server" DataTextField="Action"
                DataValueField="Id" Skin="WebBlue">
            </telerik:RadComboBox>
        </td>
    </tr>
    <tr>
        <td style="vertical-align: top">
            Comment:
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxComment" runat="server" EmptyMessage="Additional information" Columns="100" Rows="5" Skin="WebBlue" TextMode="MultiLine"></telerik:RadTextBox>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <asp:CheckBox ID="CheckBoxAccountingFlag" runat="server" Text="Include in accounting report:" TextAlign="Left" ToolTip="Check if you want this activity to be part of the accounting report" />
        </td>
    </tr>
    <tr>
        <td>
            <telerik:RadButton ID="RadButtonSubmit" runat="server" Text="Submit" OnClick="RadButtonSubmit_Click">
            </telerik:RadButton>
        </td>
        <td>
            <asp:Label ID="LabelStatus" runat="server"></asp:Label>
        </td>
    </tr>
</table>
</div>