﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Drawing;
using Telerik.Web.UI;
using FOWebApp.Data;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOUserControlWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;

namespace FOWebApp.Nocsa
{
    public partial class ModifyUser : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BOAccountingControlWS boAccountingControlWS =
                                                        new BOAccountingControlWS();

                DataHelper dataHelper = new DataHelper();

                string userName = Request.Params["UserName"];

                //Load the Membership info
                MembershipUserCollection users = Membership.GetAllUsers();
                MembershipUser user = users[userName];

                //Fill the textboxes
                TextBoxUserName.Text = user.UserName;
                TextBoxEMailAddress.Text = user.Email;

                if (Roles.IsUserInRole(user.UserName, "NOC Administrator"))
                {
                    LabelRole.Text = "NOC Administrator";
                    LabelRelatedEntity.Visible= false;
                    LabelRelatedEntityName.Visible = false;
                }

                if (Roles.IsUserInRole(user.UserName, "NOC Operator"))
                {
                    LabelRole.Text = "NOC Operator";
                    LabelRelatedEntity.Visible = false;
                    LabelRelatedEntityName.Visible = false;
                }

                if (Roles.IsUserInRole(user.UserName, "Distributor"))
                {
                    LabelRole.Text = "Distributor";
                    Distributor dist = boAccountingControlWS.GetDistributorForUser(user.ProviderUserKey.ToString());
                    LabelRelatedEntity.Text = "Distributor:";
                    LabelRelatedEntityName.Text = dist.FullName;
                }

                if (Roles.IsUserInRole(user.UserName, "Organization"))
                {
                    LabelRole.Text = "Organization";
                    Organization org = boAccountingControlWS.GetOrganizationForUser(user.ProviderUserKey.ToString());
                    LabelRelatedEntity.Text = "Organization/Customer:";
                    LabelRelatedEntityName.Text = org.FullName;
                }

                if (Roles.IsUserInRole(user.UserName, "EndUser"))
                {
                    LabelRole.Text = "EndUser";
                    EndUser endUser = boAccountingControlWS.GetEndUser(user.ProviderUserKey.ToString());
                    LabelRelatedEntity.Text = "End User:";
                    LabelRelatedEntityName.Text = endUser.FirstName + " " + endUser.LastName;
                }

                CheckBoxActivated.Checked = user.IsApproved;
                CheckBoxLockedOut.Checked = user.IsLockedOut;
                if (user.IsLockedOut)
                {
                    CheckBoxLockedOut.Visible = true;
                    LabelLockOutDate.Visible = true;
                    LabelLockOutDate.Text = "Last lock-out date: " + user.LastLockoutDate.ToShortDateString();
                }
                else
                {
                    CheckBoxLockedOut.Visible = false;
                    LabelLockOutDate.Visible = false;
                }
            }
        }

        protected void ButtonCreate_Click(object sender, EventArgs e)
        {
            string errorMsg = "";
            bool errorFlag = false;

            //Update the database with the modified data
            string userName = TextBoxUserName.Text;

            //Load the Membership info
            MembershipUserCollection storedUsers = Membership.GetAllUsers();
            MembershipUser storedUser = storedUsers[userName];

            storedUser.IsApproved = CheckBoxActivated.Checked;
            storedUser.Email = TextBoxEMailAddress.Text;
            Membership.UpdateUser(storedUser);
            
            if (storedUser.IsLockedOut && !CheckBoxLockedOut.Checked)
            {
                //Unlock user
                if (!storedUser.UnlockUser())
                {
                    errorMsg = "Could not unlock user!";
                    errorFlag = true;
                }
            }

            if (errorFlag)
            {
                LabelStatus.Visible = true;
                LabelStatus.ForeColor = Color.DarkRed;
                LabelStatus.Text = errorMsg;
            }
            else
            {
                LabelStatus.Visible = true;
                LabelStatus.ForeColor = Color.DarkGreen;
                LabelStatus.Text = "User modified successfully";
            }
        }

        protected void ButtonPasswordReset_Click(object sender, EventArgs e)
        {
            //Update the database with the modified data
            string userName = TextBoxUserName.Text;

            //Load the Membership info
            MembershipUserCollection storedUsers = Membership.GetAllUsers();
            MembershipUser storedUser = storedUsers[userName];

            storedUser.IsApproved = CheckBoxActivated.Checked;
            storedUser.Email = TextBoxEMailAddress.Text;

            //Reset the password of the user
            string newPassword = storedUser.ResetPassword();

            //Forward the password by E-Mail
            BOLogControlWS logControl = new BOLogControlWS();

            string bodyText = "Dear customer, <br/> your password has been reset. " + 
                "For your next logon into the CMT please use the following password: " + 
                "<b><br/><br/>" + newPassword + "</b><br/><br/>" + "Please change your password as soon as possible " +
                "by means of the Change Password option in the CMT menu.<br/><br/>" +
                "Best regards,<br/><br/>" +
                "Your SatADSL support team";

            string[] toAddress = {TextBoxEMailAddress.Text};

            if (logControl.SendMail(bodyText, "Password change", toAddress))
            {
                LabelStatus.Visible = true;
                LabelStatus.ForeColor = Color.DarkGreen;
                LabelStatus.Text = "Customer password successfully reset!";
            }
            else
            {
                LabelStatus.Visible = true;
                LabelStatus.ForeColor = Color.DarkRed;
                LabelStatus.Text = "Customer password reset failed!";
            }
        }
    }
}