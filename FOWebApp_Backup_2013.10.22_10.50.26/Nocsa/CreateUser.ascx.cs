﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Drawing;
using Telerik.Web.UI;
using FOWebApp.Data;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOUserControlWSRef;

namespace FOWebApp.Nocsa
{
    public partial class CreateUser : System.Web.UI.UserControl
    {
        DataHelper _dataHelper = null;
        BOUserControlWS _boUserControl = null;
        BOAccountingControlWS _boAccountingControl = null;

        public CreateUser()
        {
            _dataHelper = new DataHelper();
            _boUserControl = new BOUserControlWS();
            _boAccountingControl = new BOAccountingControlWS();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Fill the drop down boxes
            //Load customers into the combobox
            Organization[] organizations = _boAccountingControl.GetOrganizations();

            foreach (Organization org in organizations)
            {
                RadComboBoxItem item = new RadComboBoxItem();
                item.Value = org.Id.ToString();
                item.Text = org.FullName;
                RadComboBoxOrganization.Items.Add(item);
            }

            //Load the Distributor combobox
            EndUser[] endUsers = _boAccountingControl.GetEndUsers();

            foreach (EndUser endUser in endUsers)
            {
                RadComboBoxItem item = new RadComboBoxItem();
                item.Value = endUser.Id.ToString();
                item.Text = endUser.FirstName + " " + endUser.LastName;
                RadComboBoxEndUsers.Items.Add(item);
            }

            //Load the EndUsers combobox
            Distributor[] distributors = _boAccountingControl.GetDistributors();

            foreach (Distributor distributor in distributors)
            {
                RadComboBoxItem item = new RadComboBoxItem();
                item.Value = distributor.Id.ToString();
                item.Text = distributor.FullName;
                RadComboBoxDistributor.Items.Add(item);
            }
        }

        protected void ButtonCreate_Click(object sender, EventArgs e)
        {
            MembershipUser user = null;
            MembershipCreateStatus Status;
            
            //Create the user and connect to the correct entity if necessary
            try
            {
                user = Membership.CreateUser(
                                TextBoxUserName.Text,
                                TextBoxPassword.Text,
                                TextBoxEMailAddress.Text,
                                "void",
                                "void",
                                CheckBoxActivated.Checked,
                                out Status);

                if (user == null)
                {
                    LabelStatus.ForeColor = Color.Red;
                    LabelStatus.Text = "Unable to create user: " + Status.ToString();
                }
                else
                {

                    Guid userId = (Guid)user.ProviderUserKey;

                    if (RadioButtonNOCSA.Checked)
                    {
                        Roles.AddUserToRole(TextBoxUserName.Text, RadioButtonNOCSA.Text);
                    }

                    if (RadioButtonNOCOP.Checked)
                    {
                        Roles.AddUserToRole(TextBoxUserName.Text, RadioButtonNOCOP.Text);
                    }

                    //Connect user with actor entity
                    if (RadioButtonDistributor.Checked)
                    {
                        Roles.AddUserToRole(TextBoxUserName.Text, RadioButtonDistributor.Text);
                        int distributorId = Int32.Parse(RadComboBoxDistributor.SelectedValue);
                        _boUserControl.ConnectUserToDistributor(userId, distributorId);
                    }

                    if (RadioButtonOrganization.Checked)
                    {
                        Roles.AddUserToRole(TextBoxUserName.Text, RadioButtonOrganization.Text);
                        int orgId = Int32.Parse(RadComboBoxOrganization.SelectedValue);
                        _boUserControl.ConnectUserToCustomer(userId, orgId);
                    }

                    if (RadioButtonEndUser.Checked)
                    {
                        Roles.AddUserToRole(TextBoxUserName.Text, RadioButtonEndUser.Text);
                        int endUserId = Int32.Parse(RadComboBoxEndUsers.SelectedValue);
                        _boUserControl.ConnectUserToEndUser(userId, endUserId);
                    }


                    LabelStatus.ForeColor = Color.DarkGreen;
                    LabelStatus.Text = "User created successfully!";
                }
            }
            catch (Exception ex)
            {
                LabelStatus.ForeColor = Color.Red;
                LabelStatus.Text = "Unable to create user: " + ex.Message;
            }
        }
    }
}