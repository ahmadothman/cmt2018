﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;

namespace FOWebApp.Nocsa
{
    public partial class EndUserForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int id = Int32.Parse(Request.Params["Id"]);

            //Load the data into the form
            BOAccountingControlWS boAccountingControlWS = new BOAccountingControlWS();

            EndUser endUser = boAccountingControlWS.GetEndUserById(id);
            LabelId.Text = endUser.Id.ToString();
            TextBoxFirstName.Text = endUser.FirstName;
            TextBoxLastName.Text = endUser.LastName;
            TextBoxEMail.Text = endUser.Email;
            TextBoxPhone.Text = endUser.Phone;
            TextBoxMobilePhone.Text = endUser.MobilePhone;
            TextBoxSitId.Text = endUser.SitId.ToString();
            
        }

        protected void ButtonSubmit_Click(object sender, EventArgs e)
        {
            //Write data to the database
            int id = Int32.Parse(LabelId.Text);

            BOAccountingControlWS boAccountingControlWS = new BOAccountingControlWS();
            EndUser endUser = boAccountingControlWS.GetEndUserById(id);
            endUser.FirstName = TextBoxFirstName.Text;
            endUser.LastName = TextBoxLastName.Text;
            endUser.Email = TextBoxEMail.Text;
            endUser.Phone = TextBoxPhone.Text;
            endUser.MobilePhone = TextBoxMobilePhone.Text;
            endUser.SitId = Int32.Parse(TextBoxSitId.Text);

            if (boAccountingControlWS.UpdateEndUser(endUser))
            {
                LabelResult.ForeColor = Color.DarkGreen;
                LabelResult.Text = "End User updated!";
            }
            else
            {
                LabelResult.ForeColor = Color.Red;
                LabelResult.Text = "End User update failed!";
            }
        }
    }
}