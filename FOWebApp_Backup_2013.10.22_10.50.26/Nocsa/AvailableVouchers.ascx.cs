﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using Telerik.Web.UI;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOVoucherControllerWSRef;

namespace FOWebApp.Nocsa
{
    public partial class AvailableVouchers : System.Web.UI.UserControl
    {
        BOAccountingControlWS _boAccountingControlWS = new BOAccountingControlWS();

        protected void Page_Load(object sender, EventArgs e)
        {
            //Load customers into the combobox
            Distributor[] distributors = _boAccountingControlWS.GetDistributors();

            foreach (Distributor distributor in distributors)
            {
                RadComboBoxItem item = new RadComboBoxItem();
                item.Value = distributor.Id.ToString();
                item.Text = distributor.FullName;
                RadComboBoxDistributors.Items.Add(item);
            }
        }

        protected void RadGridVouchers_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            //BOVoucherControllerWS boVoucherControl = new BOVoucherControllerWS();
            //List<Voucher> vouchers = new List<Voucher>();
            //RadGridVouchers.DataSource = vouchers;
        }
    }
}