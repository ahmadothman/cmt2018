﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;

namespace FOWebApp.Nocsa
{
    public partial class AdvancedSearch2 : System.Web.UI.UserControl
    {
        BOAccountingControlWS _boAccountingControl;

        public AdvancedSearch2()
        {
            _boAccountingControl = new BOAccountingControlWS();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Isp[] ispList = _boAccountingControl.GetIsps();
            RadComboBoxISP.DataValueField = "Id";
            RadComboBoxISP.DataTextField = "CompanyName";
            RadComboBoxISP.DataSource = ispList;
            RadComboBoxISP.DataBind();
            RadComboBoxISP.Items.Insert(0, new RadComboBoxItem(""));

            Distributor[] distList = _boAccountingControl.GetDistributors();
            RadComboBoxDistributor.DataValueField = "Id";
            RadComboBoxDistributor.DataTextField = "FullName";
            RadComboBoxDistributor.DataSource = distList;
            RadComboBoxDistributor.DataBind();
            RadComboBoxDistributor.Items.Insert(0, new RadComboBoxItem(""));

            TerminalStatus[] termStatusList = _boAccountingControl.GetTerminalStatus();
            RadComboBoxAdminState.DataValueField = "Id";
            RadComboBoxAdminState.DataTextField = "StatusDesc";
            RadComboBoxAdminState.DataSource = termStatusList;
            RadComboBoxAdminState.DataBind();
            RadComboBoxAdminState.Items.Insert(0, new RadComboBoxItem(""));

            ServiceLevel[] servicePacks = _boAccountingControl.GetServicePacks();
            RadComboBoxSLA.DataValueField = "SlaId";
            RadComboBoxSLA.DataTextField = "SlaName";
            RadComboBoxSLA.DataSource = servicePacks;
            RadComboBoxSLA.DataBind();
            RadComboBoxSLA.Items.Insert(0, new RadComboBoxItem(""));
        }

        protected void RadButtonQuery_Click(object sender, EventArgs e)
        {
            //Retrieve the different parameters and call the Advanced Query method
            AdvancedQueryParams advQP = new AdvancedQueryParams();

            if (RadComboBoxISP.SelectedValue != "")
                advQP.IspId = Int32.Parse(RadComboBoxISP.SelectedValue);
            else
                advQP.IspId = -1;

            if (RadComboBoxAdminState.SelectedValue != "")
                advQP.AdmState = Int32.Parse(RadComboBoxAdminState.SelectedValue);
            else
                advQP.AdmState = -1; //Means no value

            if (RadComboBoxDistributor.SelectedValue != "")
                advQP.DistributorId = Int32.Parse(RadComboBoxDistributor.SelectedValue);
            else
                advQP.DistributorId = -1;

            if (RadDatePickerExpiryDate.SelectedDate != null)
                advQP.ExpDate = (DateTime)RadDatePickerExpiryDate.SelectedDate;

            advQP.FullName = RadTextBoxFullName.Text;
            advQP.EMail = RadTextBoxEMail.Text;
            advQP.Serial = RadTextBoxSerial.Text;

            if (RadMaskedTextBoxFWDPool.Text != "")
                advQP.FwdPool = Int32.Parse(RadMaskedTextBoxFWDPool.Text);
            else
                advQP.FwdPool = -1;

            if (RadMaskedTextBoxRTNPool.Text != "")
                advQP.RtnPool = Int32.Parse(RadMaskedTextBoxRTNPool.Text);
            else
                advQP.RtnPool = -1;

            if (RadMaskedTextBoxSitId.Text != "")
                advQP.SitId = Int32.Parse(RadMaskedTextBoxSitId.Text);
            else
                advQP.SitId = -1;

            if (RadComboBoxSLA.SelectedValue != "")
                advQP.SlaId = Int32.Parse(RadComboBoxSLA.SelectedValue);
            else
                advQP.SlaId = -1;

            advQP.IpAddress = RadTextBoxIPAddress.Text;
            advQP.MacAddress = RadTextBoxMacAddress.Text;

            advQP.ExpDateOper = RadComboBoxExpiryDate.SelectedValue;
            Terminal[] terms = _boAccountingControl.GetTerminalsWithAdvancedQuery(advQP);

            if (terms.Length > 0)
            {
                RadGridTerminals.Visible = true;
                RadGridTerminals.DataSource = terms;
                RadGridTerminals.DataBind();
                LabelNumTerminals.Text = terms.Length.ToString();
                LabelNumberOfTerminals.Visible = true;
                LabelNumTerminals.Visible = true;
                //RadGridTerminals.MasterTableView.Caption = "Number of terminals: " + terms.Length;
            }
            else
            { 
                RadGridTerminals.Visible = false;
            }
        }
    }
}