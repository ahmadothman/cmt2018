﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FOWebApp.Nocsa
{
    public partial class ServicePackUpdateForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ServicePackUpdate df =
                (ServicePackUpdate)Page.LoadControl("~/Nocsa/ServicePackUpdate.ascx");
            PlaceHolderServicePackUpdateForm.Controls.Add(df);
        }
    }
}