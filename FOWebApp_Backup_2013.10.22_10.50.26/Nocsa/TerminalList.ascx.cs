﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using Telerik.Web.UI;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.Data;

namespace FOWebApp.Nocsa
{
    public partial class TerminalList : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void RadGridTerminals_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();
            //Bind the grid to the list of terminals
            DataHelper dataHelper = new DataHelper();
            //Terminal[] terminalList = boAccountingControl.GetTerminals();
            List<Terminal> terminalList = dataHelper.getTerminals();
            RadGridTerminals.DataSource = terminalList;
            
            //Set the filter expression
            /*if (this.Session["TerminalList_FilterPair"] != null)
            {
                Pair filterPair = (Pair)this.Session["TerminalList_FilterPair"];
                GridColumn col = RadGridTerminals.MasterTableView.GetColumnSafe(filterPair.Second.ToString());
                col.CurrentFilterValue = this.Session["TerminalList_FilterValue"].ToString();
                col.CurrentFilterFunction = (GridKnownFunction)filterPair.First;
                RadGridTerminals.MasterTableView.FilterExpression = "([" + filterPair.Second + "] LIKE \'%" + this.Session["TerminalList_FilterValue"].ToString() + "%\') ";
            }*/

            //Set the sort expression
            if (this.Session["TerminalList_SortExpression"] != null)
            {
                string sortExpression = (string)this.Session["TerminalList_SortExpression"];
                GridSortExpression gridSortExpression = new GridSortExpression();
                gridSortExpression.FieldName = sortExpression;
                gridSortExpression.SortOrder = (GridSortOrder)this.Session["TerminalList_SortOrder"];
                RadGridTerminals.MasterTableView.SortExpressions.Add(gridSortExpression);
            }

            //Set the pagesize
            if (this.Session["TerminalList_PageSize"] != null)
            {
                RadGridTerminals.PageSize = (int)this.Session["TerminalList_PageSize"];
                RadGridTerminals.MasterTableView.PageSize = (int)this.Session["TerminalList_PageSize"];
            }

            //Set the pageindex
            if (this.Session["TerminalList_PageIdx"] != null)
            {
                RadGridTerminals.CurrentPageIndex =
                            (int)this.Session["TerminalList_PageIdx"];
            }

            if (this.Session["TerminalList_FilterPair"] != null)
            {
                Pair filterPair = (Pair)this.Session["TerminalList_FilterPair"];
                GridColumn col = RadGridTerminals.MasterTableView.GetColumnSafe(filterPair.Second.ToString());

                col.CurrentFilterValue = this.Session["TerminalList_FilterValue"].ToString();
                col.CurrentFilterFunction = (GridKnownFunction)filterPair.First;
                RadGridTerminals.MasterTableView.FilterExpression = "([" + filterPair.Second.ToString() + "] LIKE \'%" + this.Session["TerminalList_FilterValue"].ToString() + "%\') ";
            }
        }

        protected void RadGridTerminals_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem dataItem = (GridDataItem)e.Item;

                if (dataItem["AdmStatus"].Text.Equals("1"))
                {
                    dataItem["AdmStatus"].Text = "Locked";
                    dataItem["AdmStatus"].ForeColor = Color.Red;
                }

                if (dataItem["AdmStatus"].Text.Equals("2"))
                {
                    dataItem["AdmStatus"].Text = "Unlocked";
                    dataItem["AdmStatus"].ForeColor = Color.Green;
                }

                if (dataItem["AdmStatus"].Text.Equals("3"))
                {
                    dataItem["AdmStatus"].Text = "Decommissioned";
                    dataItem["AdmStatus"].ForeColor = Color.Blue;
                }

                if (dataItem["AdmStatus"].Text.Equals("5"))
                {
                    dataItem["AdmStatus"].Text = "Request";
                    dataItem["AdmStatusColumn"].ForeColor = Color.DarkOrange;
                }

                if (dataItem["AdmStatus"].Text.Equals("6"))
                {
                    dataItem["AdmStatus"].Text = "Deleted";
                    dataItem["AdmStatus"].ForeColor = Color.Black;
                }

                //Put the MAC addresses of the non-operational terminals in red
                if (dataItem["CNo"].Text.Equals("-1.00") || dataItem["CNo"].Text.Equals("") 
                    || dataItem["CNo"].Text.Equals("-1,00"))
                {
                    dataItem["MacAddress"].ForeColor = Color.Red;
                }
                else
                {
                    dataItem["MacAddress"].ForeColor = Color.Green;
                }
            }
        }

        protected void RadGridTerminals_ItemCreated(object sender, GridItemEventArgs e)
        {
        }

        protected void RadGridTerminals_ColumnCreated(object sender, GridColumnCreatedEventArgs e)
        {
        }

        /// <summary>
        /// Configures the export settings for this radgrid
        /// </summary>
        public void ConfigureExport()
        {
            RadGridTerminals.ExportSettings.FileName = "CMTExport";
            RadGridTerminals.ExportSettings.ExportOnlyData = true;
            RadGridTerminals.ExportSettings.IgnorePaging = true;
            RadGridTerminals.ExportSettings.OpenInNewWindow = true;
            RadGridTerminals.ExportSettings.Excel.Format = GridExcelExportFormat.Html;
            RadGridTerminals.ExportSettings.Csv.ColumnDelimiter = GridCsvDelimiter.Semicolon;
            RadGridTerminals.ExportSettings.Csv.EncloseDataWithQuotes = false;
            RadGridTerminals.ExportSettings.Csv.RowDelimiter = GridCsvDelimiter.NewLine;
        }

        protected void RadGridTerminals_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToExcelCommandName)
            {
                this.ConfigureExport();
                RadGridTerminals.MasterTableView.ExportToExcel();
            }

            if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToCsvCommandName)
            {
                this.ConfigureExport();
                RadGridTerminals.MasterTableView.ExportToCSV();
            }

            if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToPdfCommandName)
            {
                this.ConfigureExport();
                RadGridTerminals.MasterTableView.ExportToPdf();
            }

            if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToWordCommandName)
            {
                this.ConfigureExport();
                RadGridTerminals.MasterTableView.ExportToWord();
            }

            if (e.CommandName == RadGrid.FilterCommandName)
            {
                Pair filterPair = new Pair();
                filterPair.Second = ((Pair)e.CommandArgument).Second;
  
                if (filterPair.Second.ToString() == "AdmStatus")
                {
                    //Change the filter expression to the correponding status Id
                    TextBox filterBox = (e.Item as GridFilteringItem)[filterPair.Second.ToString()].Controls[0] as TextBox;
                    string filterText = filterBox.Text;
                    GridColumn col = RadGridTerminals.MasterTableView.GetColumnSafe("AdmStatus");
                    int statusValueId = this.getStatusValueId(filterText);
                    col.CurrentFilterValue = statusValueId.ToString();
                    filterBox.Text = statusValueId.ToString();
                }

                //Set the session variables to mark the filter
                GridColumn column = RadGridTerminals.MasterTableView.GetColumnSafe(filterPair.Second.ToString());
                string columnAndFilterValue = column.AndCurrentFilterValue;
                filterPair.First = column.CurrentFilterFunction; 
                this.Session["TerminalList_FilterPair"] = filterPair;

                string currentFilter = column.CurrentFilterValue;
                this.Session["TerminalList_FilterValue"] = currentFilter;

                string filterExpression = RadGridTerminals.MasterTableView.FilterExpression;
                this.Session["TerminalList_FilterExpression"] = filterExpression;

            }
        }

        protected void RadGridTerminals_SortCommand(object sender, GridSortCommandEventArgs e)
        {
            this.Session["TerminalList_SortExpression"] = e.SortExpression;
            if (e.NewSortOrder != e.OldSortOrder)
            {
                this.Session["TerminalList_SortOrder"] = e.NewSortOrder;
            }
            else
            {
                this.Session["TerminalList_SortOrder"] = e.OldSortOrder;
            }
        }

        protected void RadGridTerminals_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            //Save the page index
            int pageIdx = e.NewPageIndex;
            this.Session["TerminalList_PageIdx"] = pageIdx;
        }

        /// <summary>
        /// Returns the identifier for the given status
        /// </summary>
        /// <param name="statusValue"></param>
        /// <returns></returns>
        private int getStatusValueId(string statusValue)
        {
            BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();
            TerminalStatus[] termStatusList = boAccountingControl.GetTerminalStatus();
            int statusId = 0;

            foreach (TerminalStatus termStatus in termStatusList)
            {
                string statusDesc = termStatus.StatusDesc.Trim();
                if (statusDesc.Equals(statusValue))
                {
                    statusId = (int)termStatus.Id;
                }
            }

            return statusId;
        }

        protected void RadGridTerminals_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            this.Session["TerminalList_PageSize"] = e.NewPageSize;
        }

        protected void RadGridTerminals_PreRender(object sender, EventArgs e)
        {
            string fileExpression = RadGridTerminals.MasterTableView.FilterExpression;
        }
    }
}