﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ErrorLogList.ascx.cs" Inherits="FOWebApp.Nocsa.ErrorLogList" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<table cellpadding="5" cellspacing="2">
    <tr>
        <td>Start date:</td>
        <td> 
            <telerik:RadDatePicker ID="RadDatePickerStart" runat="server">
            </telerik:RadDatePicker>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Provide a start date" ControlToValidate="RadDatePickerStart" Display="Dynamic"></asp:RequiredFieldValidator>
        </td>
        <td>End date:</td>
        <td>
            <telerik:RadDatePicker ID="RadDatePickerEnd" runat="server">
            </telerik:RadDatePicker>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Provide an end date" ControlToValidate="RadDatePickerEnd"></asp:RequiredFieldValidator>
        </td>
        <td>Exception level:<br /><asp:Label ID="LabelExceptionLevel" runat="server"></asp:Label></td>
        <td>
            <asp:CheckBoxList ID="CheckBoxListExceptionLevel" runat="server">
                <asp:ListItem Value="0">Debug</asp:ListItem>
                <asp:ListItem Value="1">Info</asp:ListItem>
                <asp:ListItem Value="2">Warning</asp:ListItem>
                <asp:ListItem Value="3">Error</asp:ListItem>
                <asp:ListItem Value="4">Critical</asp:ListItem>
            </asp:CheckBoxList>
        </td>
        <td>
            <asp:ImageButton ID="ImageButtonQuery" runat="server" 
                ImageUrl="~/Images/invoice_euro.png" ToolTip="Show user log" 
                onclick="ImageButtonQuery_Click" />
        </td>
    </tr>
</table>
<telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" Height="800px" Width="90%">
    <telerik:RadGrid ID="RadGridUserActivityLog" runat="server" AllowSorting="False" 
        CellSpacing="0" GridLines="None" AutoGenerateColumns="False"
        Skin="WebBlue" Visible="False" AllowFilteringByColumn="False"
        onpageindexchanged="RadGridUserActivityLog_PageIndexChanged" Width="90%" 
        onneeddatasource="RadGridUserActivityLog_NeedDataSource" 
        onpagesizechanged="RadGridUserActivityLog_PageSizeChanged" 
        onsortcommand="RadGridUserActivityLog_SortCommand" OnItemDataBound="RadGridUserActivityLog_ItemDataBound">
        <MasterTableView AllowPaging="False" 
            PageSize="20">
            <CommandItemSettings ExportToPdfText="Export to PDF" />
            <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
                <HeaderStyle Width="20px" />
            </RowIndicatorColumn>
            <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
                <HeaderStyle Width="20px" />
            </ExpandCollapseColumn>
            <Columns>
                <telerik:GridBoundColumn DataField="ExceptionDateTime" FilterControlAltText="Filter ExceptionDateTimeColumn column"
                    HeaderText="Exception Date Time" UniqueName="ExceptionDateTime" AllowFiltering="false">
                    <HeaderStyle Width="15%" />
                    <ItemStyle Wrap="True" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="ExceptionLevel" FilterControlAltText="Filter ExceptionLevelColumn column"
                HeaderText="Exception Level" UniqueName="ExceptionLevel">
                    <HeaderStyle Width="15%" />
                    <ItemStyle Wrap="True" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="ExceptionDesc" FilterControlAltText="Filter ExceptionDescColumn column"
                    HeaderText="Exception Description" UniqueName="ExceptionDesc" AllowFiltering="false">
                    <HeaderStyle Width="35%" />
                    <ItemStyle Wrap="True" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="UserDescription" FilterControlAltText="Filter UserDescriptionColumn column"
                    HeaderText="User Description" UniqueName="UserDescription" AllowFiltering="false">
                    <HeaderStyle Width="35%" />
                    <ItemStyle Wrap="True" />
                </telerik:GridBoundColumn>
            </Columns>
            <EditFormSettings>
                <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                </EditColumn>
            </EditFormSettings>
            <SortExpressions>
           <telerik:GridSortExpression FieldName="ExceptionDateTime" SortOrder="Descending" />
     </SortExpressions>
        </MasterTableView>
        <FilterMenu EnableImageSprites="False">
        </FilterMenu>
        <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default">
        </HeaderContextMenu>
    </telerik:RadGrid>
</telerik:RadAjaxPanel>