﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;
using FOWebApp.net.nimera.cmt.BOVoucherControllerWSRef;

namespace FOWebApp.Nocsa
{
    public partial class CreateVouchers : System.Web.UI.UserControl
    {
        BOAccountingControlWS _boAccountingControlWS = new BOAccountingControlWS();
        BOVoucherControllerWS _boVoucherControlWS = new BOVoucherControllerWS();

        protected void Page_Load(object sender, EventArgs e)
        {
            //Load customers into the combobox
            Distributor[] distributors = _boAccountingControlWS.GetDistributors();

            foreach (Distributor distributor in distributors)
            {
                RadComboBoxItem item = new RadComboBoxItem();
                item.Value = distributor.Id.ToString();
                item.Text = distributor.FullName;
                RadComboBoxDistributors.Items.Add(item);
            }

            //Load available voucher volumes into the combobox
            VoucherVolume[] vvList = _boVoucherControlWS.GetVoucherVolumes();

            foreach (VoucherVolume vv in vvList)
            {
                RadComboBoxItem item = new RadComboBoxItem();
                item.Value = vv.Id.ToString();
                item.Text = vv.VolumeMB + " MB";
                RadComboBoxVoucherVolume.Items.Add(item);
            }
        }

        protected void RadButtonCreateVouchers_Click(object sender, EventArgs e)
        {
            string test = e.ToString();
        }
    }
}