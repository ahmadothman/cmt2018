﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AvailableVouchers.ascx.cs"
    Inherits="FOWebApp.Nocsa.AvailableVouchers" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script type="text/javascript">
    function AvailableVouchersButtonClicked() {
        //Get the distributor Id and number of vouchers from the controls
        var distributorCombo = $find("<%= RadComboBoxDistributors.ClientID %>");
        var distributorId = parseInt(distributorCombo.get_value());

        //Call the webservice
        FOWebApp.WebServices.VoucherSupportWS.GetAvailableVouchers(distributorId, OnAvailableVouchersComplete);
    }

    function OnAvailableVouchersComplete(result) {
        //Set result to grid
        var tableView = $find("<%= RadGridVouchers.ClientID %>").get_masterTableView();
        tableView.set_dataSource(result);
        tableView.dataBind();
    }

    function RadGrid_OnCommand(sender, args) {
        //intentionally left blank 
    }

    function RowSelected(sender, eventArgs) {
        var grid = sender;
        var masterDataView = grid.get_masterTableView();
        var rowNum = eventArgs.get_itemIndexHierarchical();
        var row = masterDataView.get_dataItems()[rowNum];
        var batchId = masterDataView.getCellByColumnUniqueName(row, "BatchId").innerHTML;

        //Initialize and show the terminal details window
        var oWnd = radopen("Nocsa/VoucherBatchRelease.aspx?bid=" + batchId, "RadWindowReleaseVoucherBatch");
    }

</script>
<div style="margin-top: 10px">
    <asp:Label ID="LabelDistributor" runat="server" Text="Distributor:"></asp:Label>&nbsp
    <telerik:RadComboBox ID="RadComboBoxDistributors" runat="server" Skin="WebBlue">
    </telerik:RadComboBox>
    &nbsp
    <telerik:RadButton ID="RadButtonSubmit" runat="server" Text="Submit" AutoPostBack="False"
        Skin="WebBlue" OnClientClicked="AvailableVouchersButtonClicked">
    </telerik:RadButton>
    <br />
    <br />
    <telerik:RadGrid ID="RadGridVouchers" runat="server" CellSpacing="0"
        GridLines="None" AutoGenerateColumns="False" Skin="WebBlue" 
        onneeddatasource="RadGridVouchers_NeedDataSource" PageSize="20" 
        ShowStatusBar="True">
        <ClientSettings ClientEvents-OnRowSelected="RowSelected">
            <Selecting AllowRowSelect="True" />
            <ClientEvents OnCommand="RadGrid_OnCommand" />

        </ClientSettings>
<MasterTableView AutoGenerateColumns="False" PageSize="50">
<CommandItemSettings ExportToPdfText="Export to PDF" ShowAddNewRecordButton="False" 
        ShowExportToCsvButton="True"></CommandItemSettings>

<RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
<HeaderStyle Width="20px"></HeaderStyle>
</RowIndicatorColumn>

<ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
<HeaderStyle Width="20px"></HeaderStyle>
</ExpandCollapseColumn>

    <Columns>
        <telerik:GridCheckBoxColumn DataField="Release" DataType="System.Boolean" 
            FilterControlAltText="Filter Release column" 
            HeaderImageUrl="~\Images\lock_ok.png" HeaderText="Rel" 
            HeaderTooltip="Release Voucher Batch" UniqueName="Release">
        </telerik:GridCheckBoxColumn>
        <telerik:GridBoundColumn DataField="BatchId" 
            FilterControlAltText="Filter BatchId column" HeaderText="Batch Id" 
            UniqueName="BatchId">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="AvailableVouchers" 
            FilterControlAltText="Filter column column" HeaderText="Available Vouchers" 
            UniqueName="AvailableVouchers">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="DateCreated" 
            FilterControlAltText="Filter column1 column" HeaderText="Date Created" 
            UniqueName="DateCreated">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="UserName" 
            FilterControlAltText="Filter column2 column" HeaderText="Created By" 
            UniqueName="UserName">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="Volume" 
            FilterControlAltText="Filter Volume column" HeaderText="Volume" 
            UniqueName="Volume">
        </telerik:GridBoundColumn>
        <telerik:GridHyperLinkColumn 
            FilterControlAltText="Filter BatchIdLink column" HeaderText="Download" 
            UniqueName="BatchIdLink" DataNavigateUrlFields="BatchId" 
            DataNavigateUrlFormatString="Servlets/VoucherDownload.aspx?BatchId={0}" 
            ImageUrl="../Images/server_to_client.png">
        </telerik:GridHyperLinkColumn>
    </Columns>

<EditFormSettings>
<EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
</EditFormSettings>
</MasterTableView>

<FilterMenu EnableImageSprites="False"></FilterMenu>

<HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default"></HeaderContextMenu>
    </telerik:RadGrid>
</div>
<telerik:RadWindowManager ID="RadWindowManager1" runat="server" Skin="Vista" DestroyOnClose="True">
    <Windows>
        <telerik:RadWindow ID="RadWindowReleaseVoucherBatch" runat="server" Animation="Resize" Opacity="100"
            Skin="WebBlue" Title="Modify Terminal" AutoSize="False" Width="500px" Height="200px"
            KeepInScreenBounds="True" EnableShadow="True" EnableAriaSupport="True" VisibleStatusbar="False"
            Modal="False" DestroyOnClose="False" VisibleTitlebar="True" IconUrl="~/Images/lock_open.png">
        </telerik:RadWindow>
    </Windows>
</telerik:RadWindowManager>