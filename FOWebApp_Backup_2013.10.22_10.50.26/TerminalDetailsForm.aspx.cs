﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using Telerik.Web.UI;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;

namespace FOWebApp
{
    public partial class TerminalDetailsForm : System.Web.UI.Page
    {
        BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();

        protected void Page_Load(object sender, EventArgs e)
        {
            //Read the passed macAddress and load the data
            string macAddress = Request.Params["mac"];
            LabelMacAddress.Text = macAddress;

            Terminal term = boAccountingControl.GetTerminalDetailsByMAC(macAddress);

            if (!IsPostBack)
            {
                RadPageViewTerminal.Controls.Clear();
                RadPageViewStatus.Controls.Clear();
                RadPageViewConsumption.Controls.Clear();
                RadPageViewManagement.Controls.Clear();
                RadPageViewAccumulated.Controls.Clear();
                this.loadTerminalTab();
 
                //Make the management tab only visible for Distributors
                if (HttpContext.Current.User.IsInRole("Distributor"))
                {
                    RadTabManagement.Visible = true;
                    RadTabActivities.Visible = true;
                }

                //Hide volume and traffic tabs for SoHo-terminals
                if (term.IspId == 473)
                {
                    if (Roles.IsUserInRole("Distributor") || Roles.IsUserInRole("Organization") || Roles.IsUserInRole("EndUser"))
                    {
                        RadTabVolumes.Visible = false;
                        RadTabTraffic.Visible = false;
                    }
                }
            }

            //Pre-load management tab & accumulated graph tab
            this.loadConsumedTab();
            this.loadAccumulatedTab();
            this.loadManagementTab();
            this.loadTerminalActivitiesTab(term);
        }

        protected void RadMultiPageTerminalView_PageViewCreated(object sender, RadMultiPageEventArgs e)
        {
        }

        protected void RadTabStripTerminalView_TabClick(object sender, Telerik.Web.UI.RadTabStripEventArgs e)
        {
            RadTab tab = e.Tab;

            switch (tab.Index)
            {
                case 0:
                    this.loadTerminalTab();
                    break;

                case 1:
                    this.loadStatusTab();
                    break;

                case 2:
                    //this.loadConsumedTab();
                    break;

                case 3:
                    //this.loadAccumulatedTab();
                    break;

                /*case 4:
                    this.loadRTNAccumulatedTab();
                    break;*/

                default:
                    this.loadTerminalTab();
                    break;

            }
          
        }

        protected void loadTerminalTab()
        {
            //Load the TerminalDetailTab control
            TerminalDetailTab terminalDetailTab =
                (TerminalDetailTab)Page.LoadControl("TerminalDetailTab.ascx");
            terminalDetailTab.MacAddress = LabelMacAddress.Text;
            RadPageViewTerminal.Controls.Add(terminalDetailTab);
        }


        protected void loadStatusTab()
        {
            TerminalStatusTab terminalStatusTab =
                (TerminalStatusTab)Page.LoadControl("TerminalStatusTab.ascx");
            terminalStatusTab.MacAddress = LabelMacAddress.Text;
            RadPageViewStatus.Controls.Add(terminalStatusTab);
        }

        protected void loadConsumedTab()
        {
            TerminalConsumedTab terminalConsumedTab =
                (TerminalConsumedTab)Page.LoadControl("TerminalConsumedTab.ascx");
            terminalConsumedTab.MacAddress = LabelMacAddress.Text;
            RadPageViewConsumption.Controls.Add(terminalConsumedTab);
        }

        protected void loadAccumulatedTab()
        {
            TerminalAccumulatedTab terminalAccumulatedTab =
                (TerminalAccumulatedTab)Page.LoadControl("TerminalAccumulatedTab.ascx");
            terminalAccumulatedTab.MacAddress = LabelMacAddress.Text;
            RadPageViewAccumulated.Controls.Add(terminalAccumulatedTab);
            //terminalAccumulatedTab.initTab();
        }

        protected void loadRTNAccumulatedTab()
        {
            TerminalRTNAccumulatedTab terminalRTNAccumulatedTab =
                (TerminalRTNAccumulatedTab)Page.LoadControl("TerminalRTNAccumulatedTab.ascx");
            terminalRTNAccumulatedTab.MacAddress = LabelMacAddress.Text;
            RadPageViewAccumulated.Controls.Add(terminalRTNAccumulatedTab);
        }

        protected void loadManagementTab()
        {
            TerminalManagementTab terminalManagementControl =
                (TerminalManagementTab)Page.LoadControl("TerminalManagementTab.ascx");
            terminalManagementControl.MacAddress = LabelMacAddress.Text;
            RadPageViewManagement.Controls.Add(terminalManagementControl);
        }

        protected void loadTerminalActivitiesTab(Terminal term)
        {
            TerminalActivityTab terminalActivityControl =
                (TerminalActivityTab)Page.LoadControl("TerminalActivityTab.ascx");
            terminalActivityControl.SitId = term.SitId;
            terminalActivityControl.IspId = term.IspId;
            terminalActivityControl.Height = 525;
            RadPageViewActivities.Controls.Add(terminalActivityControl);

        }
    }
}