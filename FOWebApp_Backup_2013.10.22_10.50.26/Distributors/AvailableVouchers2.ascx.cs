﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using Telerik.Web.UI;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FOWebApp.net.nimera.cmt.BOVoucherControllerWSRef;

namespace FOWebApp.Distributors
{
    public partial class AvailableVouchers2 : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Load data into the grid view. This view lists all issues for the authenticated user.

            BOVoucherControllerWS boVoucherControl = new BOVoucherControllerWS();
            BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();

            //Get the distributor ID of the active user
            MembershipUser myObject = Membership.GetUser();
            string userId = myObject.ProviderUserKey.ToString();
            int distributorId = Convert.ToInt32(boAccountingControl.GetDistributorForUser(userId).Id);

            //Get the voucher batches linked to the user
            VoucherBatchInfo[] voucherBatches = boVoucherControl.GetAvailableVoucherBatches(distributorId);

            RadGridVouchers.DataSource = voucherBatches;
            RadGridVouchers.DataBind();
        }

        protected void RadGridVouchers_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        protected void RadGridVouchers_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem dataItem = (GridDataItem)e.Item;
                string check = dataItem["Release2"].Text.ToString();
                int batchId = Convert.ToInt32(dataItem["BatchId"].Text);
                
                // check if voucher batch has been released or not
                if (check == "True")
                {
                    //dataItem["Payment"].Text = "";
                    dataItem["Payment2"].ToolTip = "Voucher batch has already been paid for";
                    // the hyperlink is still disabled in this state
                }
                else if (check == "False")
                {
                    // enable the hyperlink to proceed to the checkout
                    HyperLink payLink = (HyperLink)e.Item.FindControl("PayLink");
                    payLink.Attributes["href"] = "javascript:void(0);";
                    payLink.Attributes["onclick"] = String.Format("return ShowPayForm('{0}');", batchId);
                }
            }
        }

        protected void RadGridVouchers_ItemCreated(object sender, GridItemEventArgs e)
        {
        }

        protected void RadWindowManager1_Disposed(object sender, EventArgs e)
        {
        }

        protected void RadWindowManager1_Unload(object sender, EventArgs e)
        {
        }

        protected void RadGridVouchers_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            this.Session["_voucherBatchListForDistributor_PageSize"] = e.NewPageSize;
        }

        protected void RadGridVouchers_ItemCommand(object sender, GridCommandEventArgs e)
        {
            //if (e.CommandName.Equals("Complete"))
            //{
            //    GridDataKeyArray dataItems = e.Item.OwnerTableView.DataKeyValues;
            //    string batchId = dataItems[e.Item.ItemIndex]["BatchId"].ToString();
            //    HyperLink payLink = (HyperLink)e.Item.FindControl("PayLink");
            //    payLink.Attributes["href"] = "javascript:void(0);";
            //    payLink.Attributes["onclick"] = String.Format("return ShowPayForm('{0}');", batchId);
            //}
        }
    }
}