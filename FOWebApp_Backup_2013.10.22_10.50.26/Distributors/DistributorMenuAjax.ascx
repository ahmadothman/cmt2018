﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DistributorMenuAjax.ascx.cs" Inherits="FOWebApp.Distributors.DistributorMenuAjax" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<telerik:RadTreeView ID="RadTreeViewDistributorMenu" runat="server" 
    onnodeclick="RadTreeViewDistributorMenu_NodeClick" Skin="Outlook" 
    ValidationGroup="Distributors main menu">
    <Nodes>
        <telerik:RadTreeNode runat="server" Expanded="True" 
            ImageUrl="~/Images/earth_preferences.png" Owner="RadTreeViewDistributorMenu" 
            Text="Distributor Menu" Font-Bold="True">
            <Nodes>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/satellite_dish.png" 
                    Owner="" Text="Terminals" Value="n_Terminals">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Owner="" Text="Search by MAC" 
                            Value="n_Search_By_Mac">
                        </telerik:RadTreeNode>
                        <telerik:RadTreeNode runat="server" Owner="" Text="Search by SitId" 
                            Value="n_Search_By_SitId">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/businesspeople.png" 
                    Owner="" Text="Customers" Value="n_Customers">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Owner="" Text="Create new customer" 
                            Value="n_Create_New" Visible="True">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/form_blue.png" Owner="" 
                    Text="Request Forms" Value="n_Request_Form">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Owner="" Text="Activation" Visible="True" Value="n_ActivationRequest">
                        </telerik:RadTreeNode>
                        <telerik:RadTreeNode runat="server" Owner="" Text="Suspension" Visible="False">
                        </telerik:RadTreeNode>
                        <telerik:RadTreeNode runat="server" Owner="" Text="Contact SatADSL" Value="n_Contact">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/ticket_blue.png" 
                    Text="Vouchers" Value="n_Vouchers">
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/Account.png" Text="Account" 
                    ToolTip="Show account information" Value="n_account">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Text="Change Password" 
                            Value="n_Change_Password">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/cash_flow.png" 
                    Text="Billing &amp; Invoicing" ToolTip="Billing and invoicing" 
                    Value="n_Billing" Visible="True">
                    <Nodes>
                        <telerik:RadTreeNode runat="server"  
                            Text="Terminal Activity" ToolTip="Terminal Activity Report" 
                            Value="n_TerminalActivity">
                        </telerik:RadTreeNode>
                        <telerik:RadTreeNode runat="server"  
                            Text="Invoice On-Line" ToolTip="Invoices on-line" Value="n_Invoices" 
                            Visible="True">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" Text="Issues" ImageUrl="~/Images/user_headset.png" Value="n_Issues" ToolTip="Show issues">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Text="New Issue" Value="n_NewIssue" ToolTip="Submit a new issue">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/exit.png" Text="Log Off" 
                    ToolTip="Close the session" Value="n_log_off">
                </telerik:RadTreeNode>
            </Nodes>
        </telerik:RadTreeNode>
        <telerik:RadTreeNode runat="server" ImageUrl="~/Images/information.png" 
            Text="About" Value="n_about">
        </telerik:RadTreeNode>
    </Nodes>
</telerik:RadTreeView>
