﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using Telerik.Web.UI;
using FOWebApp;

namespace FOWebApp.Distributors
{
    public partial class DistributorMenuAjax : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //Set the user name as root node of the treeview
                if (HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    string user = HttpContext.Current.User.Identity.Name;
                    RadTreeViewDistributorMenu.Nodes[0].Text = "Welcome " + user;
                }
            }

            //Load the active component into the Main content placeholder
            RadTreeNode selectedNode = RadTreeViewDistributorMenu.SelectedNode;
 
            if (selectedNode != null)
            {
                //Get the MainContentPanel
                Control sheetContainer = scanForControl("SheetContentPlaceHolder", this);

                if (sheetContainer != null)
                {
                    Control contentContainer = sheetContainer.FindControl("MainContentPlaceHolder");

                    if (contentContainer != null)
                    {
                        contentContainer.Controls.Clear();
                        Control contentControl = this.LoadUserControl(selectedNode.Value);
                        if (contentControl != null)
                        {
                            contentControl.ID = selectedNode.Value + "_ID";
                            contentContainer.Controls.Add(contentControl);
                        }
                    }
                }
            }
        }

        protected void RadTreeViewDistributorMenu_NodeClick(object sender, Telerik.Web.UI.RadTreeNodeEventArgs e)
        {
            
        }

        protected Control scanForControl(string controlId, Control root)
        {
            Control resCtrl = root.FindControl(controlId);

            if (resCtrl == null)
            {
                root = root.Parent;
                if (root != null)
                {
                    resCtrl = this.scanForControl(controlId, root.Parent);
                }
            }

            return resCtrl;
        }

        protected Control LoadUserControl(string nodeSelector)
        {
            Control control = null;
            if (nodeSelector.Equals("n_Search_By_Mac"))
            {
                TerminalSearchByMac tiView =
                    (TerminalSearchByMac)Page.LoadControl("TerminalSearchByMac.ascx");
                control = tiView;
            }
            else if (nodeSelector.Equals("n_Search_By_SitId"))
            {
                TerminalSearchBySitId tiView =
                    (TerminalSearchBySitId)Page.LoadControl("TerminalSearchBySitId.ascx");
                 control = tiView;
            }
            else if (nodeSelector.Equals("n_log_off"))
            {
                //Close the session
                Session.Abandon();
                FormsAuthentication.SignOut();
                Response.Redirect("Default.aspx");
            }
            else if (nodeSelector.Equals("n_Terminals"))
            {
                control = Page.LoadControl("TerminalListForDistributor.ascx");
            }
            else if (nodeSelector.Equals("n_Customers"))
            {
                CustomerListForDistributor customerListForDistributor = 
                    (CustomerListForDistributor) Page.LoadControl("CustomerListForDistributor.ascx");
                customerListForDistributor.initDataGrid();
                control = customerListForDistributor;
            }
            else if (nodeSelector.Equals("n_account"))
            {
                DistributorAccountForm da = (DistributorAccountForm) Page.LoadControl("DistributorAccountForm.ascx");
                //da.componentDataInit();
                control = da;
            }
            else if (nodeSelector.Equals("n_Change_Password"))
            {
                control = Page.LoadControl("ChangePassword.ascx");
            }
            else if (nodeSelector.Equals("n_Contact"))
            {
                control = Page.LoadControl("ContactForm.ascx");
            }
            else if (nodeSelector.Equals("n_Create_New"))
            {
                control = Page.LoadControl("~/Distributors/NewCustomerForm.ascx");
            }
            else if (nodeSelector.Equals("n_about"))
            {
                control = Page.LoadControl("~/Distributors/AboutControl.ascx");
            }
            else if (nodeSelector.Equals("n_ActivationRequest"))
            {
                control = Page.LoadControl("~/Distributors/TerminalActivationRequestForm.ascx");
            }
            else if (nodeSelector.Equals("n_Request_Form"))
            {
                control = Page.LoadControl("BlankControl.ascx");
            }
            else if (nodeSelector.Equals("n_Vouchers"))
            {
                control = Page.LoadControl("~/Distributors/AvailableVouchers2.ascx");
            }
            else if (nodeSelector.Equals("n_TerminalActivity"))
            {
                control = Page.LoadControl("~/Distributors/TerminalActivityListSelection.ascx");
            }
            else if (nodeSelector.Equals("n_Invoices"))
            {
                control = Page.LoadControl("~/Distributors/InvoiceOnLine.ascx");
            }
            else if (nodeSelector.Equals("n_Issues"))
            {
                control = Page.LoadControl("~/Issues.ascx");
            }
            else if (nodeSelector.Equals("n_NewIssue"))
            {
                control = Page.LoadControl("~/Distributors/NewIssue.ascx");
            }
            else
            {
                control = Page.LoadControl("TerminalListForDistributor.ascx");
            }
            return control;
        }
    }
}