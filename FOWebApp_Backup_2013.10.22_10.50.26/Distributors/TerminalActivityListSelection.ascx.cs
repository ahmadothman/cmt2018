﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using Telerik.Web.UI;
using BOInvoiceController = FOWebApp.net.nimera.cmt.BOInvoiceControllerWSRef;
using BOAccountingController = FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;

namespace FOWebApp.Distributors
{
    public partial class TerminalActivityList : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void RadButtonActivitySelection_Click(object sender, EventArgs e)
        {
            this.GenerateReport();
            RadGridActivityList.Rebind();
        }

        protected void ImageButtonExport_Click(object sender, ImageClickEventArgs e)
        {
            this.ConfigureExport();
            RadGridActivityList.MasterTableView.ExportToCSV();
        }

        protected void RadGridActivityList_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            this.GenerateReport();
        }

        protected void GenerateReport()
        {
            int ReportYear = Int32.Parse(RadComboBoxYear.SelectedValue);
            int ReportMonth = Int32.Parse(RadComboBoxMonth.SelectedValue);

            BOInvoiceController.BOInvoiceController boInvoiceController = new BOInvoiceController.BOInvoiceController();

            //Retrieve the distributor id
            MembershipUser myObject = Membership.GetUser();
            string userId = myObject.ProviderUserKey.ToString();

            //Get the Distributor linked to the user
            BOAccountingController.BOAccountingControlWS boAccountingControl = new BOAccountingController.BOAccountingControlWS();
            BOAccountingController.Distributor dist = boAccountingControl.GetDistributorForUser(userId);

            RadGridActivityList.DataSource = boInvoiceController.GetInvoiceLinesForDistributor((int)dist.Id, ReportYear, ReportMonth);
        }

        /// <summary>
        /// Configures the export settings for this radgrid
        /// </summary>
        public void ConfigureExport()
        {
             int ReportYear = Int32.Parse(RadComboBoxYear.SelectedValue);
            int ReportMonth = Int32.Parse(RadComboBoxMonth.SelectedValue);

            RadGridActivityList.ExportSettings.FileName = "AccountingReport_" + ReportYear + "_" + ReportMonth;
            RadGridActivityList.ExportSettings.ExportOnlyData = true;
            RadGridActivityList.ExportSettings.IgnorePaging = true;
            RadGridActivityList.ExportSettings.OpenInNewWindow = true;
            RadGridActivityList.ExportSettings.Csv.ColumnDelimiter = GridCsvDelimiter.Semicolon;
            RadGridActivityList.ExportSettings.Csv.EncloseDataWithQuotes = false;
            RadGridActivityList.ExportSettings.Csv.RowDelimiter = GridCsvDelimiter.NewLine;
        }
   }
}