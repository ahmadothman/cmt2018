﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InvoiceOnLine.ascx.cs" Inherits="FOWebApp.Distributors.InvoiceOnLine" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<style type="text/css">
    .style1
    {
        width: 97px;
    }
    .style2
    {
        width: 103px;
    }
</style>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxyInvoiceOnLine" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="RadButtonLoadInvoice">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="PlaceHolderInvoice" 
                    LoadingPanelID="RadAjaxLoadingPanelInvoice" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<table cellpadding="10" cellspacing="5">
    <tr>
        <td class="style1">Invoice Year:</td>
        <td>
            <telerik:RadComboBox ID="RadComboBoxYear" runat="server" Skin="WebBlue" 
                Height="18px" MaxHeight="500px" Width="103px">
                <Items>
                    <telerik:RadComboBoxItem runat="server" Owner="RadComboBoxYear" Text="2012" 
                        Value="2012" />
                    <telerik:RadComboBoxItem runat="server" Owner="RadComboBoxYear" Text="2013" 
                        Value="2013" />
                    <telerik:RadComboBoxItem runat="server" Owner="RadComboBoxYear" Text="2014" 
                        Value="2014" />
                    <telerik:RadComboBoxItem runat="server" Owner="RadComboBoxYear" Text="2015" 
                        Value="2015" />
                    <telerik:RadComboBoxItem runat="server" Owner="RadComboBoxYear" Text="2016" 
                        Value="2016" />
                    <telerik:RadComboBoxItem runat="server" Owner="RadComboBoxYear" Text="2017" 
                        Value="2017" />
                    <telerik:RadComboBoxItem runat="server" Owner="RadComboBoxYear" Text="2018" 
                        Value="2018" />
                    <telerik:RadComboBoxItem runat="server" Owner="RadComboBoxYear" Text="2019" 
                        Value="2019" />
                    <telerik:RadComboBoxItem runat="server" Owner="RadComboBoxYear" Text="2020" 
                        Value="2020" />
                    <telerik:RadComboBoxItem runat="server" Owner="RadComboBoxYear" Text="2021" 
                        Value="2021" />
                    <telerik:RadComboBoxItem runat="server" Owner="RadComboBoxYear" Text="2022" 
                        Value="2022" />
                    <telerik:RadComboBoxItem runat="server" Owner="RadComboBoxYear" Text="2023" 
                        Value="2023" />
                    <telerik:RadComboBoxItem runat="server" Owner="RadComboBoxYear" Text="2024" 
                        Value="2024" />
                    <telerik:RadComboBoxItem runat="server" Owner="RadComboBoxYear" Text="2025" 
                        Value="2025" />
                    <telerik:RadComboBoxItem runat="server" Owner="RadComboBoxYear" Text="2026" 
                        Value="2026" />
                    <telerik:RadComboBoxItem runat="server" Owner="RadComboBoxYear" Text="2027" 
                        Value="2027" />
                    <telerik:RadComboBoxItem runat="server" Owner="RadComboBoxYear" Text="2028" 
                        Value="2028" />
                    <telerik:RadComboBoxItem runat="server" Owner="RadComboBoxYear" Text="2029" 
                        Value="2029" />
                    <telerik:RadComboBoxItem runat="server" Owner="RadComboBoxYear" Text="2030" 
                        Value="2030" />
                </Items>
            </telerik:RadComboBox> 
         </td>
         <td class="style2">Invoice Month:</td>
         <td>
            <telerik:RadComboBox ID="RadComboBoxMonth" runat="server" Skin="WebBlue">
                <Items>
                    <telerik:RadComboBoxItem runat="server" Text="January" Value="1" />
                    <telerik:RadComboBoxItem runat="server" Text="February" Value="2" />
                    <telerik:RadComboBoxItem runat="server" Text="March" Value="3" />
                    <telerik:RadComboBoxItem runat="server" Text="April" Value="4" />
                    <telerik:RadComboBoxItem runat="server" Text="May" Value="5" />
                    <telerik:RadComboBoxItem runat="server" Text="June" Value="6" />
                    <telerik:RadComboBoxItem runat="server" Text="July" Value="7" />
                    <telerik:RadComboBoxItem runat="server" Text="August" Value="8" />
                    <telerik:RadComboBoxItem runat="server" Text="September" Value="9" />
                    <telerik:RadComboBoxItem runat="server" Text="Oktober" Value="10" />
                    <telerik:RadComboBoxItem runat="server" Text="November" Value="11" />
                    <telerik:RadComboBoxItem runat="server" Text="December" Value="12" />
                </Items>
            </telerik:RadComboBox> 
         </td>
         <td>
             <telerik:RadButton ID="RadButtonLoadInvoice" runat="server" Text="Load Invoice" 
                 Skin="WebBlue" onclick="RadButtonLoadInvoice_Click">
             </telerik:RadButton>
         </td>
    </tr>
</table>
<!-- Invoice on-line -->
<asp:PlaceHolder ID="PlaceHolderInvoice" runat="server">
</asp:PlaceHolder>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanelInvoice" runat="server" Skin="Default">
</telerik:RadAjaxLoadingPanel>
