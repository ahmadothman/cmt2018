﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IssueDetailsForm.ascx.cs" Inherits="FOWebApp.Distributors.IssueDetailsForm" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<style type="text/css">
    .auto-style2 {
        width: 102%;
    }
</style>
<telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
    <script type="text/javascript">
        function DoBlur(fld) {
            fld.className = 'normalfld';
        }

        function DoFocus(fld) {
            fld.className = 'focusfld';
        }

        function DoChange(fld) {
            fld.className = 'changedfld';
        }
    </script>
</telerik:RadCodeBlock>
<telerik:RadAjaxPanel ID="RadAjaxPanelIssueDetails" runat="server" GroupingText="Issue details" Height="200px" Width="232px"
    EnableHistory="True" Style="margin-right: 114px">
<table cellpadding="10" cellspacing="5" class="issueDetails">
    <tr>
        <td>
            <asp:Label ID="Label1" runat="server" Text="Issue key:" Width="150px"></asp:Label>
        </td>
        <td class="style1">
            <asp:Label ID="LabelIssueKey" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="Label2" runat="server" Text="Summary:"></asp:Label>
        </td>
        <td class="style1">
            <asp:Label ID="LabelSummary" runat="server" Font-Bold="True"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="Label3" runat="server" Text="Status:"></asp:Label>
        </td>
        <td class="style1">
            <asp:Label ID="LabelStatus" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="Label4" runat="server" Text="Priority:"></asp:Label>
        </td>
        <td class="style1">
            <asp:Label ID="LabelPriority" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="Label5" runat="server" Text="Description:"></asp:Label>
        </td>
        <td class="style1">
            <asp:TextBox runat="server" ID="TextBoxIssueDescription" TextMode="MultiLine" Width="600px" Rows="5" ReadOnly="true"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="Label6" runat="server" Text="Date reported:"></asp:Label>
        </td>
        <td class="style1">
            <asp:Label ID="LabelDateReported" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="Label7" runat="server" Text="Date updated:"></asp:Label>
        </td>
        <td class="style1">
            <asp:Label ID="LabelDateUpdated" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelComments" runat="server" Text="Comments:"></asp:Label>
        </td>
        <td class="style1">
            <asp:GridView ID="GridViewComments" runat="server" AutoGenerateColumns="false">
                <columns>
                    <asp:TemplateField HeaderText="Date">
                    <ItemTemplate>
                        <%# (Eval("updated").ToString().Length >= 10) ? Eval("updated").ToString().Substring(0, 10) : Eval("updated")%>
                    </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="author.displayName" HeaderText="Author" />
                    <asp:BoundField DataField="body" HeaderText="Comment" />
                </columns>
            </asp:GridView>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelResolution" runat="server" Text="Resolution:"></asp:Label>
        </td>
        <td class="style1">
            <asp:Label ID="LabelResolutionText" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="Label10" runat="server" Text="Submit new comment:"></asp:Label>
        </td>
        <td class="style1">
            <asp:TextBox runat="server" ID="TextBoxNewComment" TextMode="MultiLine" Width="600px" Rows="5"></asp:TextBox><br />
            <asp:Button ID="ButtonSubmitComment" runat="server" Text="Submit comment" OnClick="ButtonSubmitComment_Click" /><asp:Label ID="LabelCommentSubmission" runat="server"></asp:Label>
        </td>
    </tr>
 </table>
</telerik:RadAjaxPanel>