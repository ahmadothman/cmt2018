﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using Telerik.Web.UI;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FOWebApp.net.nimera.cmt.BOVoucherControllerWSRef;

namespace FOWebApp.Distributors
{
    public partial class AvailableVouchers : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Set the distibutor id to the hidden field
            BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();
            MembershipUser myObject = Membership.GetUser();
            Guid userId = new Guid(myObject.ProviderUserKey.ToString());
            Distributor dist = boAccountingControl.GetDistributorForUser(userId.ToString());
            DistributorId.Text = dist.Id.ToString();
        }


        protected void RadGridVouchers_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem dataItem = (GridDataItem)e.Item;
                string check = dataItem["Release"].Text.ToString();
                //int batchId = Convert.ToInt32(dataItem["BatchId"].Text);
                label1.Text = dataItem["BatchId"].Text.ToString();

                if (check == "true" || check == "1" || check == "yes")
                {
                    label1.Text = check;
                    dataItem["Payment"].Text = "";
                }
            }
            
        }

        protected void RadGridVouchers_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName.Equals("Complete"))
            {
                GridDataKeyArray dataItems = e.Item.OwnerTableView.DataKeyValues;
                string batchId = dataItems[e.Item.ItemIndex]["BatchId"].ToString();
                HyperLink payLink = (HyperLink)e.Item.FindControl("PayLink");
                payLink.Attributes["href"] = "javascript:void(0);";
                payLink.Attributes["onclick"] = String.Format("return ShowPayForm('{0}');", batchId);
            }
        }

        //protected void RadGridVouchers_ItemCreated(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        //{
        //    if (e.Item is GridDataItem)
        //    {
        //        HyperLink payLink = (HyperLink)e.Item.FindControl("PayLink");
        //        payLink.Attributes["href"] = "javascript:void(0);";
        //        GridDataKeyArray dataItems = e.Item.OwnerTableView.DataKeyValues;
        //        string batchId = dataItems[e.Item.ItemIndex]["BatchId"].ToString();
        //        payLink.Attributes["onclick"] = String.Format("return ShowPayForm('{0}');", batchId);
        //    }
        //}

        
    }
}