﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CheckoutCancel.aspx.cs" Inherits="FOWebApp.Distributors.CheckoutCancel" %>
<body>
    <form runat="server">
        <table cellpadding="10" cellspacing="5" class="checkoutCancel">
            <tr>
                <td class="style1">
                    <asp:Label ID="Label1" runat="server"></asp:Label>            
                </td>
            </tr>
            <tr>
                <td>
                    <asp:button ID="ButtonClose" runat="server" text="Close Window" OnClientClick="javascript:window.close(); return false;" />
                </td>
            </tr>
        </table>
    </form>
</body>