﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NewIssue.ascx.cs" Inherits="FOWebApp.Distributors.NewIssue" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<telerik:RadAjaxPanel ID="RadAjaxPanelCreateTerminal" runat="server" Height="1600px"
    Width="100%" LoadingPanelID="RadAjaxLoadingPanelCreateTerminal">
    <h2>Submit a new issue to the SatADSL support</h2>
    <table cellpadding="10" cellspacing="5">
        <tr>
            <td class="requiredLabel">
                Issue summary:
            </td>
            <td>
                <asp:TextBox ID="TextBoxSummary" runat="server" Width="556px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="This is a required field!"
                    ControlToValidate="TextBoxSummary"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="requiredLabel">
                Issue description:
            </td>
            <td>
                <asp:TextBox ID="TextBoxIssueDescription" runat="server" TextMode="MultiLine" Width="556px" Rows="5"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="This is a required field!"
                    ControlToValidate="TextBoxIssueDescription"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>MAC address:</td>
            <td><telerik:RadMaskedTextBox ID="RadMaskedTextBoxMACAddress" runat="server"></telerik:RadMaskedTextBox></td>
        </tr>
        <tr>
            <td class="requiredLabel">
                Type of issue:
            </td>
            <td>
                <asp:DropDownList ID="IssueTypeList" runat="server">
                    <asp:ListItem Value="Bug">Bug</asp:ListItem>
                    <asp:ListItem Value="Improvement">Improvement</asp:ListItem>
                    <asp:ListItem Value="New Feature">New feature</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="This is a required field!"
                    ControlToValidate="IssueTypeList"></asp:RequiredFieldValidator>
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td>
                <asp:Button ID="ButtonSumbit" runat="server" Text="Submit issue to SatADSL" OnClick="ButtonSubmit_Click" />
            </td>
            <td>
                <asp:Label ID="LabelResult" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
</telerik:RadAjaxPanel>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanelCreateTerminal" runat="server"
    Skin="Default">
</telerik:RadAjaxLoadingPanel>
<telerik:RadWindowManager ID="RadWindowManager1" runat="server" Skin="Sunset" EnableShadow="True"
    Left="0px" Right="0px" Modal="True" VisibleStatusbar="False">
</telerik:RadWindowManager>