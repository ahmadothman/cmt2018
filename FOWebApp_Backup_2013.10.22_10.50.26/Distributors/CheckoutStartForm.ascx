﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CheckoutStartForm.ascx.cs" Inherits="FOWebApp.Distributors.CheckoutStartForm" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<style type="text/css">
    .auto-style2 {
        width: 102%;
    }
</style>
<telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
    <script type="text/javascript">
        function DoBlur(fld) {
            fld.className = 'normalfld';
        }

        function DoFocus(fld) {
            fld.className = 'focusfld';
        }

        function DoChange(fld) {
            fld.className = 'changedfld';
        }
    </script>
</telerik:RadCodeBlock>
<telerik:RadAjaxPanel ID="RadAjaxPanelCheckoutStart" runat="server" GroupingText="Checkout start" Height="200px" Width="700px"
    EnableHistory="True" Style="margin-right: 114px">
<table cellpadding="10" cellspacing="5" class="checkoutStart">
    <tr>
        <td colspan="2">
            <h2>Please proceed with the payment via PayPal</h2>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="Label1" runat="server" Text="Batch ID:" Width="150px"></asp:Label>
        </td>
        <td class="style1">
            <asp:Label ID="LabelBatchID" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="Label2" runat="server" Text="Volume:"></asp:Label>
        </td>
        <td class="style1">
            <asp:Label ID="LabelVolume" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="Label3" runat="server" Text="No. of vouchers:"></asp:Label>
        </td>
        <td class="style1">
            <asp:Label ID="LabelNoOfVouchers" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="Label4" runat="server" Text="Creation date:"></asp:Label>
        </td>
        <td class="style1">
            <asp:Label ID="LabelCreationDate" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="Label5" runat="server" Text="Amount to pay:"></asp:Label>
        </td>
        <td class="style1">
            <asp:Label ID="LabelAmountToPay" runat="server" Font-Bold="True"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:ImageButton ID="ButtonPaypalCheckout" runat="server" 
              ImageUrl="https://www.paypal.com/en_US/i/btn/btn_xpressCheckout.gif" 
              Width="145" AlternateText="Pay with PayPal"
              OnClick="ButtonPaypalCheckout_Click"  
              BackColor="Transparent" BorderWidth="0" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:Button ID="ButtonCancel" runat="server" Text="Cancel" OnClick="ButtonCancel_Click" />
        </td>
    </tr>
</table>
</telerik:RadAjaxPanel>