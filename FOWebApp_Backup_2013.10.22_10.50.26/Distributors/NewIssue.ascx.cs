﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using Telerik.Web.UI;
using System.Globalization;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOMonitorControlWSRef;
using FOWebApp.net.nimera.cmt.BOIssuesControllerWSRef;

namespace FOWebApp.Distributors
{
    public partial class NewIssue : System.Web.UI.UserControl
    {
        const string _HEXBYTE = "<0|1|2|3|4|5|6|7|8|9|a|b|c|d|e|f><0|1|2|3|4|5|6|7|8|9|a|b|c|d|e|f>";
        
        protected void Page_Load(object sender, EventArgs e)
        {
            RadMaskedTextBoxMACAddress.Mask =
               _HEXBYTE + "<:>" + _HEXBYTE + "<:>" + _HEXBYTE + "<:>" + _HEXBYTE + "<:>" + _HEXBYTE + "<:>" +
               _HEXBYTE;
        }

        protected void ButtonSubmit_Click(object sender, EventArgs e)
        {
            BOIssuesControllerWS boIssuesControl =
              new net.nimera.cmt.BOIssuesControllerWSRef.BOIssuesControllerWS();

            Issue newIssue = new Issue();
            newIssue.Summary = TextBoxSummary.Text.ToString();
            newIssue.Description = TextBoxIssueDescription.Text.ToString();
            newIssue.MACAddress = RadMaskedTextBoxMACAddress.Text.ToString();
            newIssue.IssueType = IssueTypeList.Text.ToString();
            
            //Get the distributor ID of the active user
            BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();
            MembershipUser myObject = Membership.GetUser();
            string userId = myObject.ProviderUserKey.ToString();
            newIssue.DistributorId = (int)boAccountingControl.GetDistributorForUser(userId).Id;

            //submit the issue
            try
            {
                string issueKey = boIssuesControl.SubmitNewIssue(newIssue);
                if (issueKey != "0")
                {
                    LabelResult.ForeColor = Color.DarkGreen;
                    LabelResult.Text = "New issue submitted with issue key: " + issueKey;
                }
                else
                {
                    LabelResult.ForeColor = Color.DarkRed;
                    LabelResult.Text = "Unable to sumbit issue:";
                }
            }
            catch (Exception ex)
            {
                LabelResult.ForeColor = Color.DarkRed;
                LabelResult.Text = "Unable to sumbit issue:" + ex.ToString();
            }
        }
    }
}