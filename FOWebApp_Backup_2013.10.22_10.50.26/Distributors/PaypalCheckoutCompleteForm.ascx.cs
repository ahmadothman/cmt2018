﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using Telerik.Web.UI;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOVoucherControllerWSRef;
using FOWebApp.net.nimera.cmt.BOPaymentControllerWSRef;

namespace FOWebApp.Distributors
{
    public partial class PaypalCheckoutCompleteForm : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public void componentDataInit()
        {
            string completed = Session["userCheckoutCompleted"].ToString();
            int amount = Convert.ToInt32(Session["amount"]);
            string payerId = Session["payerId"].ToString();
            string token = Session["token"].ToString();
            int batchId = Convert.ToInt32(Session["batchId"]);

            BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();
            MembershipUser myObject = Membership.GetUser();
            Guid userId = new Guid(myObject.ProviderUserKey.ToString());
            Distributor dist = boAccountingControl.GetDistributorForUser(userId.ToString());
            int distributorId = Convert.ToInt32(dist.Id);

            // Verify user has agreed to complete the checkout process.
            if (completed != "true")
            {
                Session["userCheckoutCompleted"] = "";
                Response.Redirect("CheckoutError.aspx?" + "Desc=Unvalidated%20Checkout.");
            }

            BOPaymentControllerWS boPaymentControl = new BOPaymentControllerWS();

            // Carry out the final step of the Paypal checkout procedure
            string retUrl = boPaymentControl.PaypalCheckoutComplete(amount, payerId, token, batchId, distributorId);

            // "1" means checkout was succesful 
            if (retUrl != "1")
            {
                Response.Redirect(retUrl);
            }
            else
            {
                Label1.ForeColor = Color.Green;
                Label2.ForeColor = Color.Green;
                Label1.Text = "Payment completed";
                Label2.Text = "Voucher batch " + batchId + " has been released";
            }
        }
    }
}