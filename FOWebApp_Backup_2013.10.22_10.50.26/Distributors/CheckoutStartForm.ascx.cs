﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using Telerik.Web.UI;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Web.UI.DataVisualization.Charting;
using System.Globalization;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOVoucherControllerWSRef;
using FOWebApp.net.nimera.cmt.BOPaymentControllerWSRef;

namespace FOWebApp.Distributors
{
    public partial class CheckoutStartForm : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public void componentDataInit(int batchIdInit)
        {
            batchId = batchIdInit;

            // get the distributor ID of the current user
            BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();
            MembershipUser myObject = Membership.GetUser();
            Guid userId = new Guid(myObject.ProviderUserKey.ToString());
            Distributor dist = boAccountingControl.GetDistributorForUser(userId.ToString());
            distributorId = Convert.ToInt32(dist.Id);

            // get the batch details form the database
            BOVoucherControllerWS boVoucherControl = new BOVoucherControllerWS();
            VoucherBatch vBatch = boVoucherControl.GetVoucherBatchInfo(batchId);
            
            // get the unit price and volume from the database
            int volumeID = vBatch.Volume;
            VoucherVolume voucherVolume = boVoucherControl.GetVoucherVolumeById(volumeID);
            int batchVolume = voucherVolume.VolumeMB;
            int unitPrice = Convert.ToInt32(voucherVolume.UnitPrice);
            
            amount = vBatch.NumVouchers * unitPrice;

            LabelBatchID.Text = batchId.ToString();
            LabelVolume.Text = batchVolume.ToString() + " MB";
            LabelNoOfVouchers.Text = vBatch.NumVouchers.ToString();
            LabelCreationDate.Text = vBatch.DateCreated.ToShortDateString();
            LabelAmountToPay.Text = amount + " USD";          
        }

        public void ButtonPaypalCheckout_Click(object sender, EventArgs e)
        {
            string token = "";
            BOPaymentControllerWS boPaymentControl = new BOPaymentControllerWS();
            string url = boPaymentControl.CheckoutStart("paypal", batchId, distributorId, amount, ref token);

            Session["token"] = token;
            Session["amount"] = amount;
            Session["batchId"] = batchId;
            Response.Redirect(url);
        }

        public void ButtonCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("CheckoutCancel.aspx");
        }

        public int batchId = 0;
        public int amount = 0;
        public int distributorId = 0;
    }
}