﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Security;
using System.Web.UI.WebControls;
using BOInvoiceController = FOWebApp.net.nimera.cmt.BOInvoiceControllerWSRef;
using BOAccountingController = FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;

namespace FOWebApp
{
    public partial class DistributorTerminalActivityList : System.Web.UI.UserControl
    {
        public int ReportMonth { get; set; }
        public int ReportYear { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
           
            DateTime startDate = new DateTime(ReportYear, ReportMonth, 1);
            DateTime endDate = new DateTime(ReportYear, ReportMonth, DateTime.DaysInMonth(ReportYear, ReportMonth));
        }

        public void ExportReport()
        {
            RadGridTerminalActivity.Rebind();
            RadGridTerminalActivity.MasterTableView.ExportToCSV();
        }

        protected void RadGridTerminalActivity_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            BOInvoiceController.BOInvoiceController boInvoiceController = new BOInvoiceController.BOInvoiceController();

            //Retrieve the distributor id
            MembershipUser myObject = Membership.GetUser();
            string userId = myObject.ProviderUserKey.ToString();

            //Get the Distributor linked to the user
            BOAccountingController.BOAccountingControlWS boAccountingControl = new BOAccountingController.BOAccountingControlWS();
            BOAccountingController.Distributor dist = boAccountingControl.GetDistributorForUser(userId);

            RadGridTerminalActivity.DataSource = boInvoiceController.GetInvoiceLinesForDistributor((int)dist.Id, ReportYear, ReportMonth);
        }
    }
}