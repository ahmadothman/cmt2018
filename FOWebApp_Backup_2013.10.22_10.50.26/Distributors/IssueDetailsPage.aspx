﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="IssueDetailsPage.aspx.cs" Inherits="FOWebApp.Distributors.IssueDetailsForm1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Issue details</title>
    <link href="../CMTStyle.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManagerIssueDetails" runat="server">
        <Services>
            <asp:ServiceReference Path="../WebServices/AccountSupportWS.asmx" />
        </Services>
    </asp:ScriptManager>
    <div>
        <asp:PlaceHolder ID="PlaceHolderIssueDetailsForm" runat="server"></asp:PlaceHolder>
    </div>
    </form>
</body>
</html>
