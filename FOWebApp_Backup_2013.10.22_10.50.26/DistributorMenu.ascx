﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DistributorMenu.ascx.cs" Inherits="FOWebApp.DistributorMenu" %>
<asp:TreeView ID="TreeViewMenu" runat="server" ExpandDepth="1" 
    Font-Names="Calibri" Font-Size="Medium" LineImagesFolder="~/TreeLineImages" 
    NodeWrap="True" onselectednodechanged="TreeViewMenu_SelectedNodeChanged" 
    ShowLines="True">
    <Nodes>
        <asp:TreeNode Text="Distributor Menu" ToolTip="Show the terminal list" 
            Value="Distributor Menu" ImageUrl="~/Images/earth_preferences.png">
            <asp:TreeNode Text="Terminals" ToolTip="Terminal information" Value="Terminals" 
                ImageUrl="~/Images/satellite_dish.png">
                <asp:TreeNode Text="Search by MAC" 
                    ToolTip="Search a terminal by means of its MAC Address" 
                    Value="Search by MAC"></asp:TreeNode>
                <asp:TreeNode Text="Search by SitId" ToolTip="Search a terminal by SIT Id" 
                    Value="Search by SitId"></asp:TreeNode>
            </asp:TreeNode>
            <asp:TreeNode Text="Customers" ToolTip="Show and edit customer information" 
                Value="Customers" ImageUrl="~/Images/businesspeople.png">
                <asp:TreeNode Text="Search by Id" ToolTip="Search a customer by Identifier" 
                    Value="Search by Id"></asp:TreeNode>
            </asp:TreeNode>
            <asp:TreeNode Text="Request forms" ToolTip="Request forms" 
                Value="Request forms" ImageUrl="~/Images/form_blue.png">
                <asp:TreeNode Text="Activation" 
                    ToolTip="Request for a terminal activaiton" Value="Activation Request">
                </asp:TreeNode>
                <asp:TreeNode Text="Contact SatADSL" ToolTip="SatADSL contact form" 
                    Value="Contact SatADSL"></asp:TreeNode>
            </asp:TreeNode>
        </asp:TreeNode>
    </Nodes>
    <ParentNodeStyle BorderStyle="None" BorderWidth="1px" ChildNodesPadding="10px" 
        ForeColor="Black" />
</asp:TreeView>
