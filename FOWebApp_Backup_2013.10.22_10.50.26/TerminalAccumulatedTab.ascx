﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TerminalAccumulatedTab.ascx.cs" Inherits="FOWebApp.TerminalAccumulatedTab" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<div style="padding-top: 5px; padding-bottom: 2px">
Select period: 
    <telerik:RadComboBox ID="RadComboBoxPeriodSelection" runat="server" 
    Skin="WebBlue" AutoPostBack="False">
    <Items>
        <telerik:RadComboBoxItem runat="server" Text="1 Week" Value="7" />
        <telerik:RadComboBoxItem runat="server" Text="2 Weeks" Value="14" />
        <telerik:RadComboBoxItem runat="server" Text="1 Month" Value="30" />
        <telerik:RadComboBoxItem runat="server" Text="2 Months" Value="60" />
        <telerik:RadComboBoxItem runat="server" Text="6 Months" Value="180" />
        <telerik:RadComboBoxItem runat="server" Text="1 Year" Value="365" />
    </Items>
</telerik:RadComboBox>
&nbsp&nbsp
    <telerik:RadButton ID="RadButtonRefresh" runat="server" Text="Show Chart" 
        onclick="RadButtonRefresh_Click">
    </telerik:RadButton>
&nbsp&nbsp&nbsp
    <telerik:RadDatePicker ID="RadDatePickerStartDate" runat="server">
    </telerik:RadDatePicker>
    <telerik:RadDatePicker ID="RadDatePickerEndDate" runat="server">
    </telerik:RadDatePicker>
&nbsp&nbsp
    <telerik:RadButton ID="RadButtonDateSelection" runat="server" Text="Show Chart" 
        onclick="RadButtonDateSelection_Click">
    </telerik:RadButton>
&nbsp&nbsp
    <telerik:RadButton ID="RadButtonDataDownload" runat="server" Text="Download Data" 
        onclick="RadButtonDataDownload_Click" Visible="false">
    </telerik:RadButton>
</div>
<asp:Chart ID="ChartAccumulated" runat="server" 
    ToolTip="Accumulated for 1 month until today" Visible="False">
    <ChartAreas>
        <asp:ChartArea Name="ChartArea1">
        </asp:ChartArea>
    </ChartAreas>
</asp:Chart>
<div>
    Right click to E-Mail, save or print this graph!
</div>

