﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Drawing;
using System.Web.Security;
using FOWebApp.net.nimera.cmt.BOMonitorControlWSRef;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;

namespace FOWebApp
{
    public partial class TerminalDetailTab : System.Web.UI.UserControl
    {
        string _macAddress;

        BOAccountingControlWS _boAccountingControl = null;
        BOMonitorControlWS _boMonitoringControl = null;

        /// <summary>
        /// MacAddress which must be set before loading the component!
        /// </summary>
        public string MacAddress
        {
            get { return _macAddress; }
            set { _macAddress = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            bool termChecked = false;
            bool freeZoneFlag = false;
            bool voucherFlag = false;
            bool sohoFlag = false;

            //if (!IsPostBack)
            {
                CultureInfo culture = new CultureInfo("en-US");
                _boMonitoringControl =
                    new net.nimera.cmt.BOMonitorControlWSRef.BOMonitorControlWS();

                _boAccountingControl =
                    new net.nimera.cmt.BOAccountingControlWSRef.BOAccountingControlWS();

                MembershipUser user = Membership.GetUser();
                if (Roles.IsUserInRole("Distributor"))
                {
                    Distributor dist = _boAccountingControl.GetDistributorForUser(user.ProviderUserKey.ToString());
                    int distributorId = (int)dist.Id;
                    try
                    {
                        termChecked = _boAccountingControl.IsTermOwnedByDistributor(distributorId, _macAddress);
                    }
                    catch (Exception ex)
                    {
                        BOLogControlWS boLogControlWS = new BOLogControlWS();
                        CmtApplicationException cmtEx = new CmtApplicationException();
                        cmtEx.ExceptionDesc = ex.Message;
                        cmtEx.ExceptionStacktrace = ex.StackTrace;
                        boLogControlWS.LogApplicationException(cmtEx);
                        termChecked = false;
                    }
                }
                else
                {
                    termChecked = true;
                }
                
                //Check if the _macAddress belongs to the distributor
                if (termChecked)
                {
                    //We must fetch the terminal information first  to get to the ISP identifier
                    Terminal term = _boAccountingControl.GetTerminalDetailsByMAC(_macAddress);
                    TerminalInfo ti = _boMonitoringControl.getTerminalInfoByMacAddress(_macAddress, term.IspId);
                    Isp isp = _boAccountingControl.GetISP(Int32.Parse(ti.IspId));

                    if (ti.ErrorFlag)
                    {
                        Label6.Visible = true;
                        LabelError.Visible = true;
                        LabelError.Text = ti.ErrorMsg;
                    }
                    else
                    {
                        //Read terminal accounting data
                        Terminal terminal = _boAccountingControl.GetTerminalDetailsByMAC(_macAddress);
                        
                        // set flags for voucher, freezone and SoHo
                        voucherFlag = _boAccountingControl.IsVoucherSLA(Int32.Parse(ti.SlaId));
                        freeZoneFlag = _boAccountingControl.IsFreeZoneSLA(Int32.Parse(ti.SlaId));
                        if (isp.Id == 473)
                        {
                            sohoFlag = true;
                        }
                        
                        //Load ti properties into fields.
                        Label6.Visible = false;
                        LabelError.Visible = false;
                        LabelSiteId.Text = ti.SitId;
                        LabelSiteName.Text = ti.EndUserId;
                        LabelIPAddress.Text = ti.IPAddress;
                        LabelMacAddress.Text = ti.MacAddress;
                        LabelServicePackage.Text = ti.SlaName;

                        if (isp != null)
                        {
                            LabelISP.Text = isp.CompanyName + ", Id: " + isp.Id;
                        }
                        else
                        {
                            LabelISP.Text = "Unknown";
                        }

                        TerminalStatus[] termStatusList = _boAccountingControl.GetTerminalStatus();
                        LabelAdmStatus.Text = termStatusList[(int)--terminal.AdmStatus].StatusDesc.Trim();

                        if (LabelAdmStatus.Text.Equals("Locked"))
                        {
                            LabelAdmStatus.ForeColor = Color.Red;
                        }
                        else
                        {
                            LabelAdmStatus.ForeColor = Color.Green;
                        }

                        // calculate and display volume allocation
                        decimal FUPThresholdBytes = ti.FUPThreshold * (decimal)1000000000.0 + Convert.ToDecimal(ti.Additional);
                        decimal RTNFUPThresholdBytes = ti.RTNFUPThreshold * (decimal)1000000000.0 + Convert.ToDecimal(ti.AdditionalReturn);
                        decimal FUPThresholdBytesGB = FUPThresholdBytes / (decimal)1000000000.0;
                        decimal RTNFUPThresholdBytesGB = RTNFUPThresholdBytes / (decimal)1000000000.0;
                        LabelVolumeAllocation.Text = FUPThresholdBytesGB.ToString("0.00") + " GB";
                        LabelReturnVolumeAllocation.Text = RTNFUPThresholdBytesGB.ToString("0.00") + " GB";

                        // calculate and display volume consumption
                        decimal consumedGB = (Convert.ToInt64(ti.Consumed) - Convert.ToInt64(ti.FreeZone)) / (decimal)1000000000.0;
                        decimal returnConsumedGB = (Convert.ToInt64(ti.ConsumedReturn) - Convert.ToInt64(ti.ConsumedFreeZone)) / (decimal)1000000000.0;
                        LabelFConsumed.Text = "" + consumedGB.ToString("0.000") + " GB";
                        LabelRTNConsumed.Text = "" + returnConsumedGB.ToString("0.000") + " GB";

                        // calculate and display volume percentage consumed                        
                        if (!sohoFlag)
                        {
                            decimal percentConsumedGB = (decimal)0.0;
                            decimal returnPercentConsumedGB = (decimal)0.0;
                            //avoid divide by zero and display text (FWD)
                            if (FUPThresholdBytes != (decimal)0.0)
                            {
                                percentConsumedGB = (consumedGB / FUPThresholdBytesGB) * (decimal)100.0;
                                LabelFConsumed.Text += " (" + percentConsumedGB.ToString("0.00") + " %)";
                            }
                            else if (voucherFlag)
                            {
                                LabelFConsumed.Text += " (Voucher-based)";
                            }
                            else
                            {
                                LabelFConsumed.Text += " (Unlimited)";
                            }
                            //avoid divide by zero and display text (RTN)
                            if (RTNFUPThresholdBytes != (decimal)0.0)
                            {
                                returnPercentConsumedGB = (returnConsumedGB / RTNFUPThresholdBytesGB) * (decimal)100.0;
                                LabelRTNConsumed.Text += " (" + returnPercentConsumedGB.ToString("0.00") + " %)";
                            }
                            else if (voucherFlag)
                            {
                                LabelRTNConsumed.Text += " (Voucher-based)";
                            }
                            else
                            {
                                LabelRTNConsumed.Text += " (Unlimited)";
                            }
                        }

                        //set freezone display
                        if (freeZoneFlag)
                        {
                            decimal freeZoneGB = Convert.ToInt64(ti.FreeZone) / (decimal)1000000000.0;
                            decimal returnFreeZoneGB = Convert.ToInt64(ti.ConsumedFreeZone) / (decimal)1000000000.0;
                            LabelFreeZoneVolume.Text = "" + freeZoneGB.ToString("0.000") + " GB";
                            LabelRTNFreeZoneVolume.Text = "" + returnFreeZoneGB.ToString("0.000") + " GB";
                            PanelFreeZone.Visible = true;
                        }
                        else
                        {
                            PanelFreeZone.Visible = false;
                        }

                        //set SoHo-specific display
                        if (sohoFlag)
                        {
                            // calculate remaining volumes
                            decimal sohoRemainingFWD = FUPThresholdBytesGB - consumedGB;
                            decimal sohoRemainingRTN = RTNFUPThresholdBytesGB - returnConsumedGB;
                            // display values
                            LabelFWDRemaining.Text = "" + sohoRemainingFWD.ToString("0.000") + " GB";
                            LabelFWDRemaining.Visible = true;
                            LabelRTNRemaining.Text = "" + sohoRemainingRTN.ToString("0.000") + " GB";
                            LabelRTNRemaining.Visible = true;
                            Label19.Visible = true;
                            Label21.Visible = true;

                            // hide other volume information for distributors, customers and end-user
                            if (Roles.IsUserInRole("Distributor") || Roles.IsUserInRole("Organization") || Roles.IsUserInRole("EndUser"))
                            {
                                Label5.Visible = false;
                                Label7.Visible = false;
                                Label14.Visible = false;
                                Label17.Visible = false;
                                LabelVolumeAllocation.Visible = false;
                                LabelReturnVolumeAllocation.Visible = false;
                                LabelFConsumed.Visible = false;
                                LabelRTNConsumed.Visible = false;
                                LabelResetDayOfMOnth.Visible = false;
                                Label8.Visible = false;
                            }
                        }

                        //fill up final labels with information
                        LabelResetDayOfMOnth.Text = ti.InvoiceDayOfMonth;
                        double rtnNum = Double.Parse(ti.RTN, culture);
                        LabelRTN.Text = rtnNum.ToString("#.0") + " dBHz";

                        LabelRTN.ForeColor = this.CNOColorValidator(rtnNum, term.IspId);
                        LabelStatus.Text = ti.Status;

                        if (ti.Status == "Operational")
                        {
                            LabelStatus.ForeColor = Color.Green;
                        }
                        else
                        {
                            LabelStatus.ForeColor = Color.Red;
                        }

                        if (terminal.ExpiryDate != null)
                        {
                            LabelExpiryDate.Text = terminal.ExpiryDate.ToString();

                            if (terminal.ExpiryDate < DateTime.Now)
                            {
                                LabelExpiryDate.ForeColor = Color.Red;
                            }
                            else
                            {
                                LabelExpiryDate.ForeColor = Color.Green;
                            }
                        }
                        else
                        {
                            LabelExpiryDate.Text = "Not Set";
                            LabelExpiryDate.ForeColor = Color.Orange;
                        }
                    }
                }
                else
                {
                    LabelStatus.ForeColor = Color.Red;
                    LabelStatus.Text = "Terminal not found";
                }
            }
        }

        /// <summary>
        /// Determines the color of the CNO pointing label depending on the 
        /// Return number value and the ISP.
        /// </summary>
        /// <remarks>
        /// The return value levels which determine the label color depend on the
        /// ISP. Later we should thing on a more flexible and "plugable" strategy to
        /// implement this feature.
        /// </remarks>
        /// <param name="rtnNum">The return number value</param>
        /// <param name="ispId">The ISP identifier</param>
        /// <returns>A Color value depending on the given rtnNum value and isp</returns>
        protected Color CNOColorValidator(double rtnNum, int ispId)
        {
            Color labelColor;

            if (ispId == 112)
            {
                if (rtnNum < 55.2)
                {
                    labelColor = Color.Red;
                }
                else if (rtnNum >= 55.2 && rtnNum < 58.4)
                {
                    labelColor = Color.DarkOrange;
                }
                else
                {
                    labelColor = Color.DarkGreen;
                }
                LabelRTN.ToolTip = "Above 58.4 dBHz, the terminal uses the most efficient return pool. " +
                    "Between 55.2 dBHz and 58.4 dBHz, the terminal must use a less efficient return pool. " +
                    "Below 55.2 dBHz the signal is too weak.";
            }
            else if (ispId == 412)
            {
                if (rtnNum < 52.7)
                {
                    labelColor = Color.Red;
                }
                else if (rtnNum >= 52.7 && rtnNum < 55.8)
                {
                    labelColor = Color.DarkOrange;
                }
                else
                {
                    labelColor = Color.DarkGreen;
                }
                LabelRTN.ToolTip = "Above 55.8 dBHz, the terminal uses the most efficient return pool. " +
                    "Between 52.7 dBHz and 55.8 dBHz, the terminal must use a less efficient return pool. " +
                    "Below 52.7 dBHz the signal is too weak.";
            }
            else
            {
                //General case
                if (rtnNum < 55.2)
                {
                    labelColor = Color.Red;
                }
                else if (rtnNum >= 55.2 && rtnNum < 58.4)
                {
                    labelColor = Color.DarkOrange;
                }
                else
                {
                    labelColor = Color.DarkGreen;
                }
            }


            return labelColor;
        }
    }
}