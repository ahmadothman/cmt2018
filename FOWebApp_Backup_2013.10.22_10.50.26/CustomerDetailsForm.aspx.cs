﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;

namespace FOWebApp
{
    public partial class CustomerDetailsForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //Read the Organization details from the database and fill out the form
                string strOrgId = Request.Params["id"];
                int orgId = Int32.Parse(strOrgId);

                LabelId.Text = strOrgId;

                BOAccountingControlWS boAccountingControl =
                  new net.nimera.cmt.BOAccountingControlWSRef.BOAccountingControlWS();

                Organization organization = boAccountingControl.GetOrganization(orgId);
                LabelDistributor.Text = "" + organization.Distributor.Id;
                TextBoxAddressLine1.Text = organization.Address.AddressLine1;
                TextBoxAddressLine2.Text = organization.Address.AddressLine2;
                TextBoxEMail.Text = organization.Email;
                TextBoxFax.Text = organization.Fax;
                TextBoxLocation.Text = organization.Address.Location;
                TextBoxName.Text = organization.FullName;
                TextBoxPCO.Text = organization.Address.PostalCode;
                TextBoxPhone.Text = organization.Phone;
                TextBoxRemarks.Text = organization.Remarks;
                TextBoxVAT.Text = organization.Vat;
                TextBoxWebSite.Text = organization.WebSite;
                CountryList.SelectedValue = organization.Address.Country;
            }

        }

        protected void ButtonSubmit_Click(object sender, EventArgs e)
        {
            if (TextBoxName.Equals("") || TextBoxEMail.Equals(""))
            {
                LabelResult.ForeColor = Color.Red;
                LabelResult.Text = "Customer update failed!";
            }
            else
            {
                BOAccountingControlWS boAccountingControl =
                      new net.nimera.cmt.BOAccountingControlWSRef.BOAccountingControlWS();

                Organization organization = boAccountingControl.GetOrganization(Int32.Parse(LabelId.Text));

                organization.Id = Int32.Parse(LabelId.Text);
                organization.Address.AddressLine1 = TextBoxAddressLine1.Text;
                organization.Address.AddressLine2 = TextBoxAddressLine2.Text;
                organization.Email = TextBoxEMail.Text;
                organization.Fax = TextBoxFax.Text;
                organization.Address.Location = TextBoxLocation.Text;
                organization.FullName = TextBoxName.Text;
                organization.Address.PostalCode = TextBoxPCO.Text;
                organization.Phone = TextBoxPhone.Text;
                organization.Remarks = TextBoxRemarks.Text;
                organization.WebSite = TextBoxWebSite.Text;
                organization.Address.Country = CountryList.SelectedValue;
                organization.Distributor.Id = Int32.Parse(LabelDistributor.Text);
                organization.Vat = TextBoxVAT.Text;

                if (boAccountingControl.UpdateOrganization(organization))
                {
                    LabelResult.ForeColor = Color.Green;
                    LabelResult.Text = "Customer update successful!";
                }
                else
                {
                    LabelResult.ForeColor = Color.Red;
                    LabelResult.Text = "Customer update failed!";
                }
            }
        }
    }
}