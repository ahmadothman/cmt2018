﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using System.Globalization;

namespace FOWebApp.Account
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //RegisterHyperLink.NavigateUrl = "Register.aspx?ReturnUrl=" + HttpUtility.UrlEncode(Request.QueryString["ReturnUrl"]);
        }

        protected override void InitializeCulture()
        {
            if (Request.Form["ImageButtonEnglish"] != null)
            {
                UICulture = "en";
                Culture = "en";

                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("en");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("en");
            }

            if (Request.Form["ImageButtonFrench"] != null)
            {
                 UICulture = "fr";
                 Culture = "fr";

                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("fr");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("fr");
            }
            base.InitializeCulture();
        }
    }
}
