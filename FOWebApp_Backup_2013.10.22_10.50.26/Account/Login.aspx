﻿<%@ Page Title="Log In" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="Login.aspx.cs" Inherits="FOWebApp.Account.Login" Culture="auto" meta:resourcekey="PageResource1"
    UICulture="auto" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
<div style="margin-left: 50px">
<table>
    <tr>
        <td>
            <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/LogoSatADSL2.jpg" 
                Width="177px" />
        </td>
        <td>
            &nbsp;</td>
    </tr>
</table>
</div>
    <div style="width: 960px; margin-left: 240px; margin-right: auto;">
        <h2>
            <asp:Label ID="LabelLoginHeader" runat="server" Text="CMT Login" meta:resourcekey="LabelLoginHeaderResource1"></asp:Label>
        </h2>
        <!-- <p>
        Please enter your username and password.
        <asp:HyperLink ID="RegisterHyperLink" runat="server" EnableViewState="false">Register</asp:HyperLink> if you don't have an account.
    </p> -->
        <asp:Login ID="LoginUser" runat="server" EnableViewState="False" RenderOuterTable="False"
            meta:resourcekey="LoginUserResource1">
            <LayoutTemplate>
                <span class="failureNotification">
                    <asp:Literal ID="FailureText" runat="server" meta:resourcekey="FailureTextResource1"></asp:Literal>
                </span>
                <asp:ValidationSummary ID="LoginUserValidationSummary" runat="server" CssClass="failureNotification"
                    ValidationGroup="LoginUserValidationGroup" meta:resourcekey="LoginUserValidationSummaryResource1" />
                <div class="accountInfo">
                    <fieldset class="login">
                        <p>
                            <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName" meta:resourcekey="UsernameLabel" />
                            <asp:TextBox ID="UserName" runat="server" CssClass="textEntry" meta:resourcekey="UserNameResource1"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName"
                                CssClass="failureNotification" ErrorMessage="User Name is required." ToolTip="User Name is required."
                                ValidationGroup="LoginUserValidationGroup" meta:resourcekey="UserNameRequiredResource1">*</asp:RequiredFieldValidator>
                        </p>
                        <p>
                            <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password" meta:resourcekey="PasswordLabel" />
                            <asp:TextBox ID="Password" runat="server" CssClass="passwordEntry" TextMode="Password"
                                meta:resourcekey="PasswordResource1"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password"
                                CssClass="failureNotification" ErrorMessage="Password is required." ToolTip="Password is required."
                                ValidationGroup="LoginUserValidationGroup" meta:resourcekey="PasswordRequiredResource1">*</asp:RequiredFieldValidator>
                        </p>
                        <p>
                            <asp:CheckBox ID="RememberMe" runat="server" meta:resourcekey="RememberMeResource1" />
                            <asp:Label ID="RememberMeLabel" runat="server" AssociatedControlID="RememberMe" CssClass="inline"
                                meta:resourcekey="RememberMeLabelResource1">Keep me logged in</asp:Label>
                        </p>
                    </fieldset>
                    <p class="submitButton">
                        <asp:Button ID="LoginButton" runat="server" CommandName="Login" Text="Log In" ValidationGroup="LoginUserValidationGroup"
                            meta:resourcekey="LoginButtonResource1" />
                    </p>
                </div>
            </LayoutTemplate>
        </asp:Login>
    </div>
</asp:Content>
