﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TerminalStatusTab.ascx.cs"
    Inherits="FOWebApp.TerminalStatusTab" %>
<table border="0" cellspacing="5px" class="termDetails">
    <tr>
        <td>
            <asp:Label ID="LabelForwardAllocation" runat="server">Forward Volume Allocation:</asp:Label>
        </td>
        <td>
            <asp:Label ID="LabelVolumeAllocation" runat="server"></asp:Label>
        </td>
    </tr>
     <tr>
        <td>
            <asp:Label ID="LabelReturnAllocation" runat="server">Return Volume Allocation:</asp:Label>
        </td>
        <td>
            <asp:Label ID="LabelReturnVolumeAllocation" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelForwardVolume" runat="server" Text="Forward Volume Consumption:"></asp:Label>
        </td>
        <td>
            <asp:Label ID="LabelVolumeConsumed" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelReturnVolume" runat="server" Text="Return Volume Consumption:"></asp:Label>
        </td>
        <td>
            <asp:Label ID="LabelReturnVolumeConsumed" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelFUPReset" runat="server">FUP Reset Day:</asp:Label>
        </td>
        <td>
            <asp:Label ID="LabelFUPResetDay" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            Generated on:
        </td>
        <td>
            <asp:Label ID="LabelTimeStamp" runat="server"></asp:Label>
        </td>
    </tr>
</table>
<asp:Panel ID="PanelFreeZone" runat="server" Visible="False" BackColor="#F1FEF3">
<table border="0" cellspacing="5px" class="termDetails">
    <tr>
        <td width="200px">
            <asp:Label ID="Label2" runat="server" Text="Forward Freezone Consumed:"></asp:Label>
        </td>
        <td>
            <asp:Label ID="LabelFreezoneConsumed" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="Label1" runat="server" Text="Return Freezone Consumed:"></asp:Label>
        </td>
        <td>
            <asp:Label ID="LabelRTNFreezoneConsumed" runat="server"></asp:Label>
        </td>
    </tr>
</table>
</asp:Panel>
<div class="chartPane">
    <asp:Chart ID="ChartForwardVolume" runat="server" BackImageAlignment="Center">
        <Series>
        </Series>
        <ChartAreas>
            <asp:ChartArea Name="ChartAreaForward">
            </asp:ChartArea>
        </ChartAreas>
    </asp:Chart>
    <asp:Chart ID="ChartReturnVolume" runat="server" BackImageAlignment="Center">
        <Series>
        </Series>
        <ChartAreas>
            <asp:ChartArea Name="ChartAreaReturn">
            </asp:ChartArea>
        </ChartAreas>
    </asp:Chart>
</div>
