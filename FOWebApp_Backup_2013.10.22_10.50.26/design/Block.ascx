<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="Block.ascx.cs" Inherits="design_WebUserControlBlock" %>

<div class="art-block">
    <div class="art-block-body">

<div class="art-blockheader">
    <h3 class="t"><asp:placeholder runat="server" id="HeaderPlaceholder"/></h3>
</div>
<div class="art-blockcontent">
    <div class="art-blockcontent-body">

<asp:placeholder runat="server" id="ContentPlaceholder"/>


		<div class="cleared"></div>
    </div>
</div>


		<div class="cleared"></div>
    </div>
</div>

