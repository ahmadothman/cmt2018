﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Invoice.ascx.cs" Inherits="FOWebApp.Invoice" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<table width="900px">
    <tr>
        <td>
            <div style="float: left">
                <p>
                    <asp:Image ID="ImageLogo" runat="server" ImageUrl="~/Images/logo_satadsl.png" />
                </p>
                <p>
                    Customer:<br />
                    <br />
                    <asp:Label ID="LabelDistributorName" runat="server" Text="Label" Font-Bold="True"></asp:Label><br />
                    <asp:Label ID="LabelDistributorAddressLine1" runat="server" Text="Label" Font-Bold="True"></asp:Label><br />
                    <asp:Label ID="LabelDistributorAddressLine2" runat="server" Text="Label" Font-Bold="True"></asp:Label><br />
                    <asp:Label ID="LabelDistributorPostCode" runat="server" Text="Label" Font-Bold="True"></asp:Label>
                    &nbsp&nbsp&nbsp&nbsp
                    <asp:Label ID="LabelDistributorLocation" runat="server" Text="Label" Font-Bold="True"></asp:Label><br />
                    <br />
                    <asp:Label ID="LabelDistributorCountry" runat="server" Text="Label" Font-Bold="True"></asp:Label>
                </p>
            </div>
            <div style="float: right">
                <p>
                    Invoice Number:
                    <asp:Label ID="LabelInvoiceNumber" runat="server" Font-Bold="True"></asp:Label>
                </p>
                <p>
                    Invoice Date:
                    <asp:Label ID="LabelInvoiceDate" runat="server" Font-Bold="True"></asp:Label>
                </p>
                <br />
                <p>
                    <span style="font-size: larger; font-style: italic; font-weight: bold">SatADSl SPRL</span><br />
                    <br />
                    Rue Royale 182, PO Box 23 D7<br />
                    1000 BRUSSELS<br />
                    Belgium<br />
                    VAT: BE 0467066876
                </p>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            Overview
        </td>
    </tr>
    <tr>
        <td>
            <telerik:RadGrid ID="RadGridInvoiceLines" runat="server" 
                AutoGenerateColumns="False" CellSpacing="0" GridLines="None" Skin="Outlook">
<MasterTableView>
<CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>

<RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
</RowIndicatorColumn>

<ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
</ExpandCollapseColumn>

    <Columns>
        <telerik:GridBoundColumn DataField="TerminalId" 
            FilterControlAltText="Filter TerminalId column" HeaderText="Terminal" 
            UniqueName="TerminalId">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="MacAddress" 
            FilterControlAltText="Filter MacAddress column" HeaderText="MAC Address" 
            UniqueName="MacAddress">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="Serial" 
            FilterControlAltText="Filter Serial column" HeaderText="Serial" 
            UniqueName="Serial">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="ServicePack" 
            FilterControlAltText="Filter ServicePack column" HeaderText="SLA" 
            UniqueName="ServicePack">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="ItemDescription" 
            FilterControlAltText="Filter ItemDescription column" HeaderText="Description" 
            UniqueName="ItemDescription">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="UnitPrice" 
            FilterControlAltText="Filter UnitPrice column" HeaderText="Unit Price" 
            UniqueName="UnitPrice">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="Items" 
            FilterControlAltText="Filter Items column" HeaderText="Items" 
            UniqueName="Items">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="AmountUSD" 
            FilterControlAltText="Filter AmountUSD column" HeaderText="Amt $" 
            UniqueName="AmountUSD">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="AmountEUR" 
            FilterControlAltText="Filter AmountEUR column" HeaderText="Amt €" 
            UniqueName="AmountEUR">
        </telerik:GridBoundColumn>
    </Columns>

<EditFormSettings>
<EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
</EditFormSettings>
</MasterTableView>

<FilterMenu EnableImageSprites="False"></FilterMenu>

<HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default"></HeaderContextMenu>
            </telerik:RadGrid>
        </td>
    </tr>
     <tr>
        <td>
            <br /><br />
            <span style="font-size: large">
            Totals for this invoice:&nbsp&nbsp 
            <asp:Label ID="LabelTotalUSD" runat="server" Font-Bold="True" Font-Underline="True"></asp:Label>&nbsp&nbsp&nbsp
            <asp:Label ID="LabelTotalEUR" runat="server" Font-Bold="True" Font-Underline="True"></asp:Label>
            </span>
        </td>
    </tr>
</table>
