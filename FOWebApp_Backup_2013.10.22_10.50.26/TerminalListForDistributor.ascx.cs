﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using Telerik.Web.UI;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Web.UI.DataVisualization.Charting;
using System.Globalization;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOMonitorControlWSRef;

namespace FOWebApp
{
    public partial class TerminalListForDistributor : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Session["_termListForDistributor_PageSize"] == null)
            {
                RadGridTerminals.PageSize = 20;
            }
            else
            {
                RadGridTerminals.PageSize = (int) this.Session["_termListForDistributor_PageSize"];
            }
            //Load data into the grid view. This view lists all terminals for the 
            //authenticated user.
            BOAccountingControlWS boAccountingControl =
              new net.nimera.cmt.BOAccountingControlWSRef.BOAccountingControlWS();

            //Get the authenticated user
            MembershipUser myObject = Membership.GetUser();
            string userId = myObject.ProviderUserKey.ToString();

            //Get the Distributor linked to the user
            Terminal[] terminals = boAccountingControl.GetTerminalsByDistributor(userId);

            RadGridTerminals.DataSource = terminals;
            RadGridTerminals.DataBind();

            LabelNumRows.Text = terminals.Length.ToString();
        }

        protected void RadGridTerminals_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridItemCollection selectedItems =  RadGridTerminals.SelectedItems;

            if (selectedItems.Count > 0)
            {
                GridItem selectedItem = selectedItems[0];
                Terminal terminal = (Terminal) selectedItem.DataItem;
                string macAddress = terminal.MacAddress;
            }
          
        }

        protected void RadGridTerminals_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem dataItem = (GridDataItem)e.Item;

                if (dataItem["AdmStatus"].Text.Equals("1"))
                {
                    dataItem["AdmStatus"].Text = "Locked";
                    dataItem["AdmStatus"].ForeColor = Color.Red;
                }

                if (dataItem["AdmStatus"].Text.Equals("2"))
                {
                    dataItem["AdmStatus"].Text = "Unlocked";
                    dataItem["AdmStatus"].ForeColor = Color.Green;
                }

                if (dataItem["AdmStatus"].Text.Equals("3"))
                {
                    dataItem["AdmStatus"].Text = "Decommissioned";
                    dataItem["AdmStatus"].ForeColor = Color.Blue;
                }

                if (dataItem["AdmStatus"].Text.Equals("5"))
                {
                    dataItem["AdmStatus"].Text = "Request";
                    dataItem["AdmStatus"].ForeColor = Color.DarkOrange;
                }

                if (dataItem["AdmStatus"].Text.Equals("6"))
                {
                    dataItem["AdmStatus"].Text = "Deleted";
                    dataItem["AdmStatus"].ForeColor = Color.Black;
                }

                //Put the MAC addresses of the non-operational terminals in red
                if (dataItem["CNo"].Text.Equals("-1,00") || dataItem["CNo"].Text.Equals("") || dataItem["CNo"].Text.Equals("-1.00"))
                {
                    dataItem["MacAddress"].ForeColor = Color.Red;
                }
                else
                {
                    dataItem["MacAddress"].ForeColor = Color.Green;
                }
            }
        }

        protected void RadWindowManager1_Disposed(object sender, EventArgs e)
        {
        }

        protected void RadWindowManager1_Unload(object sender, EventArgs e)
        {
        }

        protected void RadGridTerminals_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            this.Session["_termListForDistributor_PageSize"] = e.NewPageSize;
        }

        protected void RadGridTerminals_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.FilterCommandName)
            {
                Pair filterPair = (Pair)e.CommandArgument;

                //Set the session variables to mark the filter
                this.Session["TerminalList_FilterPair"] = filterPair;

                if (filterPair.Second.ToString() == "AdmStatusColumn")
                {
                    //Change the filter expression to the correponding status Id

                    TextBox filterBox = (e.Item as GridFilteringItem)[filterPair.Second.ToString()].Controls[0] as TextBox;
                    string filterText = filterBox.Text;
                    GridColumn col = RadGridTerminals.MasterTableView.GetColumnSafe("AdmStatusColumn");
                    int statusValueId = this.getStatusValueId(filterText);
                    col.CurrentFilterValue = statusValueId.ToString();
                    filterBox.Text = statusValueId.ToString();
                }

            }
        }

        /// <summary>
        /// Returns the identifier for the given status
        /// </summary>
        /// <param name="statusValue"></param>
        /// <returns></returns>
        private int getStatusValueId(string statusValue)
        {
            BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();
            TerminalStatus[] termStatusList = boAccountingControl.GetTerminalStatus();
            int statusId = 0;

            foreach (TerminalStatus termStatus in termStatusList)
            {
                string statusDesc = termStatus.StatusDesc.Trim();
                if (statusDesc.Equals(statusValue))
                {
                    statusId = (int)termStatus.Id;
                }
            }

            return statusId;
        }
    }
}