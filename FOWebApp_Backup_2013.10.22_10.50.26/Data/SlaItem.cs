﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FOWebApp.Data
{
    public class SlaItem
    {
        int _slaID;

        public int SlaID
        {
            get { return _slaID; }
            set { _slaID = value; }
        }

        string _slaName;

        public string SlaName
        {
            get { return _slaName; }
            set { _slaName = value; }
        }

        double _FUPThreshold;

        public double FUPThreshold
        {
            get { return _FUPThreshold; }
            set { _FUPThreshold = value; }
        }

        string _DRAboveFup;

        public string DRAboveFup
        {
            get { return _DRAboveFup; }
            set { _DRAboveFup = value; }
        }

        string _slaCommonName;

        public string SlaCommonName
        {
            get { return _slaCommonName; }
            set { _slaCommonName = value; }
        }
    }
}