﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Globalization;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;

namespace FOWebApp.Data
{
    /// <summary>
    /// This class is a temporary solution for methods which are not yet
    /// provided by the BOAccountingControlWS. Later the methods from the
    /// BOAccountingControl web service must be used!
    /// </summary>
    public class DataHelper
    {
        BOAccountingControlWS _boAccountingControlWS = null;

        string _ispQueryCmd = "SELECT Id, CompanyName, Phone, Fax, Email FROM ISPS ORDER BY Id";
        string _distributorQueryCmd = "SELECT Id, FullName, Phone, Fax, EMail, WebSite, Vat FROM Distributors ORDER BY FullName";
        string _userLogInsertCmd = "INSERT INTO UserActivityLog (UserId, ActionDate, UserActivityTypeId) VALUES (@UserId, @ActionDate, @Action)";
        string _organizationsQueryCmd = "SELECT Id, FullName, Phone, Fax, Email, WebSite FROM Organizations ORDER BY FullName";
        string _terminalsQueryCmd = "SELECT FullName, MacAddress, SitID, IPAddress, AdmStatus, StartDate, ExpiryDate, CNo " +
                                    " FROM Terminals WHERE AdmStatus != 5 AND AdmStatus != 6 ORDER BY FullName";
        string _terminalRequestsQuery = "SELECT FullName, MacAddress, SitID, IPAddress, AdmStatus, StartDate, ExpiryDate " +
                                    " FROM Terminals WHERE AdmStatus = 5 ORDER BY FullName";
        
        string _terminalStatusQueryCmd = "SELECT Id, StatusDesc FROM TerminalStatus";
        string _terminalStatusDescQueryCmd = "SELECT StatusDesc FROM TerminalStatus WHERE Id = @Id";
        string _terminalStatusIdQueryCmd = "SELECT Id FROM TerminalStatus WHERE StatusDesc = @StatusDesc";
        string _servicePacksQueryCmd = "SELECT SlaID, SlaName, FUPThreshold, DRAboveFup, SlaCommonName FROM ServicePacks";
        string _endUsersQueryCmd = "SELECT Id, FirstName, LastName FROM EndUsers";

        string _accountReportQuery = "SELECT Terminals.DistributorId AS Distributor, Distributors.FullName AS 'Distributor Name', TerminalActivityLog.ActionDate, " +
            "TerminalActivityLog.MacAddress, Terminals.FullName AS 'Terminal Name', Terminals.SitId, " +
            "TerminalActivityTypes.Action, TerminalActivityLog.SlaName, TerminalActivityLog.NewSlaName, TerminalActivityLog.Comment FROM TerminalActivityLog, TerminalActivityTypes, Terminals, Distributors " +
            "WHERE ActionDate >= @StartDate AND ActionDate <= @EndDate " +
            "AND TerminalActivityLog.TerminalActivityTypeId = TerminalActivityTypes.Id " +
            "AND TerminalActivityTypes.Billable = 1 " +
            "AND TerminalActivityLog.MacAddress = Terminals.MacAddress " +
            "AND Terminals.DistributorId = Distributors.Id " +
            "AND TerminalActivityLog.AccountingFlag = 1 " +
            "ORDER BY Terminals.DistributorId, Terminals.MacAddress, TerminalActivityLog.ActionDate";


        string _connectionString = "";

        /// <summary>
        /// Default constructor
        /// </summary>
        public DataHelper()
        {
            _boAccountingControlWS = new BOAccountingControlWS();

            //Retrieve the connection string from the Web.Config file
            _connectionString = ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
        }

        /// <summary>
        /// Returns the full list of ISPs from the ISP table
        /// </summary>
        /// <returns>A list of Isp instances</returns>
        public List<Isp> getISPs()
        {
            List<Isp> ispList = new List<Isp>();

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                con.Open();
                try
                {
                    using (SqlCommand command = new SqlCommand(_ispQueryCmd, con))
                    {
                        SqlDataReader reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            Isp isp = new Isp();
                            isp.Id = reader.GetInt32(0);
                            if (!reader.IsDBNull(1))
                                isp.CompanyName = reader.GetString(1);
                            else
                                isp.CompanyName = "";

                            if (!reader.IsDBNull(2))
                                isp.Phone = reader.GetString(2);
                            else
                                isp.Phone = "";

                            if (!reader.IsDBNull(3))
                                isp.Fax = reader.GetString(3);
                            else
                                isp.Fax = "";

                            if (!reader.IsDBNull(4))
                                isp.Email = reader.GetString(4);
                            else
                                isp.Email = "";

                            ispList.Add(isp);
                        }
                    }
                }
                catch (Exception ex)
                {
                    BOLogControlWS boLogControlWS = new BOLogControlWS();
                    CmtApplicationException cmtEx = new CmtApplicationException();
                    cmtEx.ExceptionDesc = ex.Message;
                    cmtEx.ExceptionStacktrace = ex.StackTrace;
                    cmtEx.UserDescription = "DataHelper GetISPs failed!";
                    boLogControlWS.LogApplicationException(cmtEx);
                    ispList = null;
                }
            }

            return ispList;
        }

        /// <summary>
        /// Returns a full list of distributors
        /// </summary>
        /// <returns>List of distributors</returns>
        public List<Distributor> getDistributors()
        {
            List<Distributor> distributorList = new List<Distributor>();

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                con.Open();
                try
                {
                    using (SqlCommand command = new SqlCommand(_distributorQueryCmd, con))
                    {
                        SqlDataReader reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            Distributor distributor = new Distributor();

                            distributor.Id = reader.GetInt32(0);

                            if (!reader.IsDBNull(1))
                                distributor.FullName = reader.GetString(1);
                            else
                                distributor.FullName = "";

                            if (!reader.IsDBNull(2))
                                distributor.Phone = reader.GetString(2);
                            else
                                distributor.Phone = "";

                            if (!reader.IsDBNull(3))
                                distributor.Fax = reader.GetString(3);
                            else
                                distributor.Fax = "";

                            if (!reader.IsDBNull(4))
                                distributor.Email = reader.GetString(4);
                            else
                                distributor.Email = "";

                            if (!reader.IsDBNull(5))
                                distributor.WebSite = reader.GetString(5);
                            else
                                distributor.WebSite = "";

                            if (!reader.IsDBNull(6))
                                distributor.Vat = reader.GetString(6);
                            else
                                distributor.Vat = "";

                            distributorList.Add(distributor);

                        }
                    }
                }
                catch (Exception ex)
                {
                    BOLogControlWS boLogControlWS = new BOLogControlWS();
                    CmtApplicationException cmtEx = new CmtApplicationException();
                    cmtEx.ExceptionDesc = ex.Message;
                    cmtEx.ExceptionStacktrace = ex.StackTrace;
                    boLogControlWS.LogApplicationException(cmtEx);
                    distributorList = null;
                }
            }

            return distributorList;
        }

        /// <summary>
        /// Inserts a UserActivy record in the UserActivityLog message
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <param name="action">Action</param>
        /// <returns></returns>
        public bool insertUserActivity(UserActivity userActivity)
        {
            bool result = false;

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                con.Open();
                try
                {
                    using (SqlCommand command = new SqlCommand(_userLogInsertCmd, con))
                    {
                        command.Parameters.Add("@UserId", SqlDbType.NVarChar);
                        command.Parameters["@UserId"].Value = userActivity.UserId.ToString();
                        command.Parameters.Add("@ActionDate", SqlDbType.DateTime);
                        command.Parameters["@ActionDate"].Value = userActivity.ActionDate;
                        command.Parameters.Add("@Action", SqlDbType.NVarChar);
                        command.Parameters["@Action"].Value = Int32.Parse(userActivity.Action);
                        int numRecords = command.ExecuteNonQuery();

                        if (numRecords == 1)
                        {
                            result = true;
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
                catch (Exception ex)
                {
                    BOLogControlWS boLogControlWS = new BOLogControlWS();
                    CmtApplicationException cmtEx = new CmtApplicationException();
                    cmtEx.ExceptionDesc = ex.Message;
                    cmtEx.ExceptionStacktrace = ex.StackTrace;
                    boLogControlWS.LogApplicationException(cmtEx);
                    result = false;
                }
            }

            return result;
        }

        /// <summary>
        /// Returns a list of organizations
        /// </summary>
        /// <returns></returns>
        public List<Organization> getOrganizations()
        {
            List<Organization> organizationList = new List<Organization>();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                con.Open();
                try
                {
                    using (SqlCommand command = new SqlCommand(_organizationsQueryCmd, con))
                    {
                        SqlDataReader reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            Organization organization = new Organization();

                            organization.Id = reader.GetInt32(0);

                            if (!reader.IsDBNull(1))
                                organization.FullName = reader.GetString(1);
                            else
                                organization.FullName = "";

                            if (!reader.IsDBNull(2))
                                organization.Phone = reader.GetString(2).Trim();
                            else
                                organization.Phone = "";

                            if (!reader.IsDBNull(3))
                                organization.Fax = reader.GetString(3).Trim();
                            else
                                organization.Fax = "";

                            if (!reader.IsDBNull(4))
                                organization.Email = reader.GetString(4).Trim();
                            else
                                organization.Email = "";

                            if (!reader.IsDBNull(5))
                                organization.WebSite = reader.GetString(5).Trim();
                            else
                                organization.WebSite = "";

                            organizationList.Add(organization);
                        }
                    }
                }
                catch (Exception ex)
                {
                    BOLogControlWS boLogControlWS = new BOLogControlWS();
                    CmtApplicationException cmtEx = new CmtApplicationException();
                    cmtEx.ExceptionDesc = ex.Message;
                    cmtEx.ExceptionStacktrace = ex.StackTrace;
                    boLogControlWS.LogApplicationException(cmtEx);
                    organizationList = null;
                }
            }

            return organizationList;
        }

        /// <summary>
        /// Returns a complete list of terminals
        /// </summary>
        /// <returns>List of Terminal entities</returns>
        public List<Terminal> getTerminals()
        {
            List<Terminal> terminalList = new List<Terminal>();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                con.Open();
                try
                {
                    using (SqlCommand command = new SqlCommand(_terminalsQueryCmd, con))
                    {
                        SqlDataReader reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            Terminal terminal = new Terminal();

                            if (!reader.IsDBNull(0))
                                terminal.FullName = reader.GetString(0).Trim();

                            if (!reader.IsDBNull(1))
                                terminal.MacAddress = reader.GetString(1).Trim();

                            if (!reader.IsDBNull(2))
                                terminal.SitId = reader.GetInt32(2);

                            if (!reader.IsDBNull(3))
                                terminal.IpAddress = reader.GetString(3).Trim();

                            if (!reader.IsDBNull(4))
                                terminal.AdmStatus = reader.GetInt32(4);

                            if (!reader.IsDBNull(5))
                                terminal.StartDate = reader.GetDateTime(5);

                            if (!reader.IsDBNull(6))
                                terminal.ExpiryDate = reader.GetDateTime(6);

                            if (!reader.IsDBNull(7))
                                terminal.CNo = (decimal?)reader.GetDecimal(7);

                            terminalList.Add(terminal);

                        }
                    }
                }
                catch (Exception ex)
                {
                    BOLogControlWS boLogControlWS = new BOLogControlWS();
                    CmtApplicationException cmtEx = new CmtApplicationException();
                    cmtEx.ExceptionDesc = ex.Message;
                    cmtEx.ExceptionStacktrace = ex.StackTrace;
                    boLogControlWS.LogApplicationException(cmtEx);
                    terminalList = null;
                }
            }

            return terminalList;
        }

        /// <summary>
        /// Returns a list of terminals which have their admin status set to Request (Status 5)
        /// </summary>
        /// <returns>A list of Requests</returns>
        public List<Terminal> getTerminalRequests()
        {
            List<Terminal> terminalList = new List<Terminal>();
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                con.Open();
                try
                {
                    using (SqlCommand command = new SqlCommand(_terminalRequestsQuery, con))
                    {
                        SqlDataReader reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            Terminal terminal = new Terminal();

                            if (!reader.IsDBNull(0))
                                terminal.FullName = reader.GetString(0).Trim();

                            if (!reader.IsDBNull(1))
                                terminal.MacAddress = reader.GetString(1).Trim();

                            if (!reader.IsDBNull(2))
                                terminal.SitId = reader.GetInt32(2);

                            if (!reader.IsDBNull(3))
                                terminal.IpAddress = reader.GetString(3).Trim();

                            if (!reader.IsDBNull(4))
                                terminal.AdmStatus = reader.GetInt32(4);

                            if (!reader.IsDBNull(5))
                                terminal.StartDate = reader.GetDateTime(5);

                            if (!reader.IsDBNull(6))
                                terminal.ExpiryDate = reader.GetDateTime(6);

                            terminalList.Add(terminal);

                        }
                    }
                }
                catch (Exception ex)
                {
                    BOLogControlWS boLogControlWS = new BOLogControlWS();
                    CmtApplicationException cmtEx = new CmtApplicationException();
                    cmtEx.ExceptionDesc = ex.Message;
                    cmtEx.ExceptionStacktrace = ex.StackTrace;
                    boLogControlWS.LogApplicationException(cmtEx);
                    terminalList = null;
                }
            }

            return terminalList;
        }

        /// <summary>
        /// Returns a list of terminal status values
        /// </summary>
        /// <returns></returns>
        public List<TerminalStatus> getTerminalStatus()
        {
            List<TerminalStatus> terminalStatusList = new List<TerminalStatus>();

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                con.Open();
                try
                {
                    using (SqlCommand command = new SqlCommand(_terminalStatusQueryCmd, con))
                    {
                        SqlDataReader reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            TerminalStatus terminalStatus = new TerminalStatus();

                            if (!reader.IsDBNull(0))
                            {
                                terminalStatus.Id = reader.GetInt32(0);
                            }

                            if (!reader.IsDBNull(1))
                            {
                                terminalStatus.StatusDesc = reader.GetString(1).Trim();
                            }

                            terminalStatusList.Add(terminalStatus);
                        }
                    }
                }
                catch (Exception ex)
                {
                    BOLogControlWS boLogControlWS = new BOLogControlWS();
                    CmtApplicationException cmtEx = new CmtApplicationException();
                    cmtEx.ExceptionDesc = ex.Message;
                    cmtEx.ExceptionStacktrace = ex.StackTrace;
                    boLogControlWS.LogApplicationException(cmtEx);
                    terminalStatusList = null;
                }
            }

            return terminalStatusList;
        }

        public List<SlaItem> getSlaItems()
        {
            List<SlaItem> slaItems = new List<SlaItem>();

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                con.Open();
                try
                {
                    using (SqlCommand command = new SqlCommand(_servicePacksQueryCmd, con))
                    {
                        SqlDataReader reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            SlaItem slaItem = new SlaItem();

                            if (!reader.IsDBNull(0))
                            {
                                slaItem.SlaID = reader.GetInt32(0);
                            }

                            if (!reader.IsDBNull(1))
                            {
                                slaItem.SlaName = reader.GetString(1).Trim();
                            }

                            if (!reader.IsDBNull(2))
                            {
                                slaItem.FUPThreshold = (double)reader.GetDecimal(2);
                            }

                            if (!reader.IsDBNull(3))
                            {
                                slaItem.DRAboveFup = reader.GetString(3).Trim();
                            }

                            if (!reader.IsDBNull(4))
                            {
                                slaItem.SlaCommonName = reader.GetString(4).Trim();
                            }

                            slaItems.Add(slaItem);
                        }
                    }
                }
                catch (Exception ex)
                {
                    BOLogControlWS boLogControlWS = new BOLogControlWS();
                    CmtApplicationException cmtEx = new CmtApplicationException();
                    cmtEx.ExceptionDesc = ex.Message;
                    cmtEx.ExceptionStacktrace = ex.StackTrace;
                    boLogControlWS.LogApplicationException(cmtEx);
                    slaItems = null;
                }
            }

            return slaItems;
        }
        
        /// <summary>
        /// Returns a list of EndUsers
        /// </summary>
        /// <returns></returns>
        public List<EndUser> getEndUsers()
        {
            List<EndUser> endUserList = new List<EndUser>();

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                con.Open();
                try
                {
                    using (SqlCommand command = new SqlCommand(_endUsersQueryCmd, con))
                    {
                        SqlDataReader reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            EndUser endUser = new EndUser();
                            endUser.Id = reader.GetInt32(0);
                            if (!reader.IsDBNull(1))
                                endUser.FirstName = reader.GetString(1).Trim();
                            else
                                endUser.FirstName = "";

                            if (!reader.IsDBNull(2))
                                endUser.LastName = reader.GetString(2);
                            else
                                endUser.LastName = "";

                            endUserList.Add(endUser);
                        }
                    }
                }
                catch (Exception ex)
                {
                    BOLogControlWS boLogControlWS = new BOLogControlWS();
                    CmtApplicationException cmtEx = new CmtApplicationException();
                    cmtEx.ExceptionDesc = ex.Message;
                    cmtEx.ExceptionStacktrace = ex.StackTrace;
                    boLogControlWS.LogApplicationException(cmtEx);
                    endUserList = null;
                }
            }

            return endUserList;
        }

        /// <summary>
        /// Returns the status description for the given status id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public string getStatusById(int Id)
        {
            string statusDesc = "";

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                con.Open();
                try
                {
                    using (SqlCommand command = new SqlCommand(_terminalStatusDescQueryCmd, con))
                    {
                        command.Parameters.AddWithValue("@Id", Id);
                        SqlDataReader reader = command.ExecuteReader();

                        if (reader.Read())
                        {
                            statusDesc = reader.GetString(0);
                        }
                    }
                }
                catch (Exception ex)
                {
                    BOLogControlWS boLogControlWS = new BOLogControlWS();
                    CmtApplicationException cmtEx = new CmtApplicationException();
                    cmtEx.ExceptionDesc = ex.Message;
                    cmtEx.ExceptionStacktrace = ex.StackTrace;
                    boLogControlWS.LogApplicationException(cmtEx);
                    statusDesc = null;
                }
            }
            return statusDesc;
        }

        /// <summary>
        /// Returns the status description for the given status description
        /// </summary>
        /// <param name="statusDesc"></param>
        /// <returns></returns>
        public int getStatusId(string statusDesc)
        {
            int Id = 0;

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                con.Open();
                try
                {
                    using (SqlCommand command = new SqlCommand(_terminalStatusIdQueryCmd, con))
                    {
                        command.Parameters.AddWithValue("@StatusDesc", statusDesc);
                        SqlDataReader reader = command.ExecuteReader();

                        if (reader.Read())
                        {
                            Id = reader.GetInt32(0);
                        }
                    }
                }
                catch (Exception ex)
                {
                    BOLogControlWS boLogControlWS = new BOLogControlWS();
                    CmtApplicationException cmtEx = new CmtApplicationException();
                    cmtEx.ExceptionDesc = ex.Message;
                    cmtEx.ExceptionStacktrace = ex.StackTrace;
                    boLogControlWS.LogApplicationException(cmtEx);
                }
            }
            return Id;
        }

        /// <summary>
        /// Returns a datatable filled with the account data
        /// </summary>
        /// <param name="startDate">Start date of the report</param>
        /// <param name="endDate">End date of the report</param>
        /// <returns>A DataTable object</returns>
        public DataTable getAccountReport(DateTime startDate, DateTime endDate)
        {
            DataTable accountReportData = new DataTable();
            SqlConnection conn = new SqlConnection(_connectionString);
            SqlDataAdapter adapter = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand(_accountReportQuery, conn);

            SqlParameter startDateParam = new SqlParameter();
            startDateParam.ParameterName = "@StartDate";
            startDateParam.SqlDbType = SqlDbType.Date;
            startDateParam.Value = startDate.Date;
            cmd.Parameters.Add(startDateParam);

            SqlParameter endDateParam = new SqlParameter();
            endDateParam.ParameterName = "@EndDate";
            endDateParam.SqlDbType = SqlDbType.Date;
            endDateParam.Value = endDate.Date;
            cmd.Parameters.Add(endDateParam);

            adapter.SelectCommand = cmd;

            conn.Open();

            try
            {
                adapter.Fill(accountReportData);
            }
            catch (Exception ex)
            {
                BOLogControlWS boLogControlWS = new BOLogControlWS();
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDesc = ex.Message;
                cmtEx.ExceptionStacktrace = ex.StackTrace;
                boLogControlWS.LogApplicationException(cmtEx);
                accountReportData= null;
            }
            finally
            {
                conn.Close();
            }

            return accountReportData;
        }
    }
}