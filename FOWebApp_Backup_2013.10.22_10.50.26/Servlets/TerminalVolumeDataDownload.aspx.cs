﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOMonitorControlWSRef;


namespace FOWebApp.Servlets
{
    public partial class TerminalVolumeDataDownload : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.HttpMethod.Equals("GET"))
            {
                //Retrieve the parameters from the URL (batch id)
                try
                {
                    string macAddress = Request.Params["MAC"];
                    string startDate = Request.Params["start"];
                    string endDate = Request.Params["end"];
                    this.DownloadVolumeData(macAddress, startDate, endDate);
                }
                catch (Exception ex)
                {
                    CmtApplicationException cmtEx = new CmtApplicationException();
                    cmtEx.ExceptionDesc = ex.Message;
                    cmtEx.ExceptionStacktrace = ex.StackTrace;
                    cmtEx.UserDescription = "VolumeDataDownload  servlet failed!";
                    //_boLogController.LogApplicationException(cmtEx);
                    Response.StatusCode = 400; //Bad request
                }
            }
        }

        private void DownloadVolumeData(string mac, string start, string end)
        {
            String filename = "VolumeData_" + mac + ".csv";
            filename = filename.Replace(':', '_');
            String filePath = Server.MapPath("~/temp/" + filename);
            FileInfo myfile = null;

            if (File.Exists(filePath))
                File.Delete(filePath);

            //try
            {
                using (StreamWriter oWriter = System.IO.File.CreateText(filePath))
                {

                    myfile = new FileInfo(filePath);

                    oWriter.WriteLine("DateTime;ReturnData;ForwardData");

                    DateTime startDate = DateTime.Parse(start);
                    DateTime endDate = DateTime.Parse(end);

                    BOMonitorControlWS boMonitorControl = new net.nimera.cmt.BOMonitorControlWSRef.BOMonitorControlWS();
                    BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();
                    TimeSpan ts = endDate - startDate;
                    int numDays = ts.Days;

                    Terminal term = boAccountingControl.GetTerminalDetailsByMAC(mac);
                    ServiceLevel serviceLevel = boAccountingControl.GetServicePack((int)term.SlaId);

                    TerminalInfo termInfo = boMonitorControl.getTerminalInfoByMacAddress(mac, term.IspId);
                    TrafficDataPoint[] trafficDataPoints = null;

                    if (numDays > 14)
                    {
                        trafficDataPoints = boMonitorControl.getAccumulatedViewsBetweenDates(mac, term.IspId, startDate, endDate);
                    }
                    else
                    {
                        trafficDataPoints = boMonitorControl.getAccumulatedTrafficViews(mac, startDate, endDate, term.IspId);
                    }

                    DateTime offset;
                    long epochTicks;
                    DateTime dt;
                    string dateTick;

                    for (int i = 0; i < trafficDataPoints.Length; i++)
                    {
                        offset = new DateTime(1970, 1, 1, 0, 0, 0);
                        epochTicks = Int64.Parse(trafficDataPoints[i].FwdDataPoint.timestamp);
                        dt = offset.AddMilliseconds(epochTicks);

                        dateTick = "";

                        if (dt.Day < 10)
                            dateTick = dateTick + "0" + dt.Day + "/";
                        else
                            dateTick = dateTick + dt.Day + "/";

                        if (dt.Month < 10)
                            dateTick = dateTick + "0" + dt.Month + "/";
                        else
                            dateTick = dateTick + dt.Month + "/";

                        dateTick = dateTick + dt.Year + " ";

                        dateTick = dateTick + dt.Hour + ":";

                        if (dt.Minute < 10)
                            dateTick = dateTick + "0" + dt.Minute;
                        else
                            dateTick = dateTick + dt.Minute;

                        oWriter.WriteLine(dateTick + ";" + trafficDataPoints[i].RtnDataPoint.value.ToString() + ";" + trafficDataPoints[i].FwdDataPoint.value.ToString());
                    }
                }

                //Download file
                Response.ClearContent();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + filename);
                Response.ContentType = "application/vnd.ms-excel";
                Response.AppendHeader("Content-Length", myfile.Length.ToString());
                Response.TransmitFile(Server.MapPath("~/temp/" + filename));
                HttpContext.Current.ApplicationInstance.CompleteRequest(); 
                //Response.End();
            }
            /*catch (Exception ex)
            {
                Response.StatusCode = 500;
            }*/
            //HttpContext.Current.ApplicationInstance.CompleteRequest();
        }
    }
}