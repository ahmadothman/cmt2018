﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using FOWebApp.net.nimera.cmt.BOVoucherControllerWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;

namespace FOWebApp.Servlets
{
    public partial class VoucherDownload : System.Web.UI.Page
    {
        BOVoucherControllerWS _boVoucherController;
        BOAccountingControlWS _boAccountingController;
        BOLogControlWS _boLogController;

        public VoucherDownload()
        {
            _boVoucherController = new BOVoucherControllerWS();
            _boAccountingController = new BOAccountingControlWS();
            _boLogController = new BOLogControlWS();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.HttpMethod.Equals("GET"))
            {
                //Retrieve the parameters from the URL (batch id)
                try
                {
                    int batchId = Int32.Parse(Request.Params["BatchId"]);
                    VoucherBatch vb = _boVoucherController.GetVoucherBatchInfo(batchId);
                    Distributor distributor = _boAccountingController.GetDistributor(vb.DistributorId);
                    this.DownloadVouchers(vb, distributor);
                }
                catch (Exception ex)
                {
                    CmtApplicationException cmtEx = new CmtApplicationException();
                    cmtEx.ExceptionDesc = ex.Message;
                    cmtEx.ExceptionStacktrace = ex.StackTrace;
                    cmtEx.UserDescription = "VoucherDownload  servlet failed!";
                    _boLogController.LogApplicationException(cmtEx);
                    Response.StatusCode = 400; //Bad request
                }
            }
        }

        private void DownloadVouchers(VoucherBatch vb, Distributor dist)
        {
            String Header = "Attachment; Filename=V_" + dist.Id + "_" + vb.BatchId + ".txt";
            Response.ClearContent();
            Response.ContentType = "application/octet-stream";
            Response.AppendHeader("Content-Disposition", Header);

            VoucherVolume vVolume = _boVoucherController.GetVoucherVolumeById(vb.Volume);
            Voucher[] vouchers = _boVoucherController.GetVouchers(vb.BatchId);

            Response.Write("Distributor: " + dist.FullName + ", Distributor Id: " + dist.Id + "\r\n");
            Response.Write("Batch Id: " + vb.BatchId + "\r\n");
            Response.Write("Date created: " + vb.DateCreated + " UTC" + "\r\n");
            if (vVolume != null)
            {
                Response.Write("Volume: " + vVolume.VolumeMB + " MB" + "\r\n");
            }
            Response.Write("\r\n");
            Response.Write("Vouchers:");
            Response.Write("\r\n\r\n");

            foreach (Voucher v in vouchers)
            {
                Response.Write(v.Code);
                Response.Write(";");
                Response.Write(v.Crc.ToString("X4"));
                Response.Write("\r\n");
            }

            HttpContext.Current.ApplicationInstance.CompleteRequest();
        }
    }
}