﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Web.UI.DataVisualization.Charting;
using System.Globalization;
using System.Web.Security;
using FOWebApp.net.nimera.cmt.BOMonitorControlWSRef;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;

namespace FOWebApp
{
    public partial class TerminalStatusTab : System.Web.UI.UserControl
    {
        string _macAddress;
        BOAccountingControlWS _boAccountingControl = null;
        BOMonitorControlWS _boMonitoringControl = null;

        /// <summary>
        /// MacAddress which must be set before loading the component!
        /// </summary>
        public string MacAddress
        {
            get { return _macAddress; }
            set { _macAddress = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            bool termChecked = false;
            _boAccountingControl = new net.nimera.cmt.BOAccountingControlWSRef.BOAccountingControlWS();
            _boMonitoringControl = new net.nimera.cmt.BOMonitorControlWSRef.BOMonitorControlWS();

            if (!MacAddress.Equals(""))
            {
                //Check if the MAC Address belongs to the distributor if the user is in the
                //distributor role
                MembershipUser user = Membership.GetUser();
                if (Roles.IsUserInRole("Distributor"))
                {
                    Distributor dist = _boAccountingControl.GetDistributorForUser(user.ProviderUserKey.ToString());
                    int distributorId = (int)dist.Id;
                    try
                    {
                        termChecked = _boAccountingControl.IsTermOwnedByDistributor(distributorId, _macAddress);
                    }
                    catch (Exception ex)
                    {
                        BOLogControlWS boLogControlWS = new BOLogControlWS();
                        CmtApplicationException cmtEx = new CmtApplicationException();
                        cmtEx.ExceptionDateTime = DateTime.Now;
                        cmtEx.ExceptionDesc = ex.Message;
                        cmtEx.UserDescription = "Method: PageLoad (IsUserInRole)";
                        cmtEx.StateInformation = "Role: Distributor";
                        boLogControlWS.LogApplicationException(cmtEx);
                        termChecked = false;
                    }
                }
                else
                {
                    termChecked = true;
                }

                if (termChecked)
                    this.initTab(_macAddress);
            }
        }

        /// <summary>
        /// Initializes the tab page
        /// </summary>
        /// <param name="macAddress">Target macAddress</param>
        public void initTab(string macAddress)
        {
            CultureInfo culture = new CultureInfo("en-US");
            
            //We must fetch the terminal information first  to get to the ISP identifier
            Terminal term = _boAccountingControl.GetTerminalDetailsByMAC(macAddress);
            TerminalInfo ti = _boMonitoringControl.getTerminalInfoByMacAddress(macAddress, term.IspId);

            if (_boAccountingControl.IsVoucherSLA(Int32.Parse(ti.SlaId)))
            {
                this.showForwardGraphVoucherAbs(ti);
                this.showReturnGraphVoucherAbs(ti);
            }
            else
            {
                this.showForwardGraph(ti);
                this.showReturnGraph(ti);
            }
        }

        /// <summary>
        /// Shows the consumed forward volume
        /// </summary>
        /// <param name="ti">The soruce TerminalInfo entity</param>
        public void showForwardGraph(TerminalInfo ti)
        {
            //Check if this is a freezone SLA
            bool freeZoneFlag = _boAccountingControl.IsFreeZoneSLA(Int32.Parse(ti.SlaId));
            
            LabelFUPResetDay.Text = ti.InvoiceDayOfMonth;

            decimal consumedGB = Convert.ToInt64(ti.Consumed) / (decimal)1000000000.0;

            decimal FUPThresholdBytes = ti.FUPThreshold * (decimal)1000000000.0;
            //FUPThresholdBytes = FUPThresholdBytes + Convert.ToDecimal(ti.Additional);

            Decimal FUPThresholdBytesGB = FUPThresholdBytes / (decimal)1000000000.0;
            LabelVolumeAllocation.Text = FUPThresholdBytesGB.ToString("#.00") + " GB";
            LabelTimeStamp.Text = DateTime.Now.ToUniversalTime() + " UTC";
            double percentConsumed = 0.0;
            Decimal percentConsumedDec = (decimal)0.0;

            if (FUPThresholdBytes != (decimal)0.0)
            {
                percentConsumed = (Convert.ToDouble(ti.Consumed) / Convert.ToDouble(FUPThresholdBytes)) * 100.0;
                percentConsumedDec = new Decimal(percentConsumed);
            }

            ChartForwardVolume.Width = 320;
            ChartForwardVolume.Titles.Add("Forward Volume Status");
            ChartForwardVolume.Titles[0].Font = new Font("Utopia", 16);
            ChartForwardVolume.ChartAreas[0].BackColor = Color.LightSalmon;
            ChartForwardVolume.ChartAreas[0].BackSecondaryColor = Color.LightGreen;
            ChartForwardVolume.ChartAreas[0].BackGradientStyle = System.Web.UI.DataVisualization.Charting.GradientStyle.TopBottom;
            ChartForwardVolume.ChartAreas[0].Area3DStyle.Enable3D = false;

            ChartForwardVolume.Series.Add(new Series("FVolumeSeries"));

            ChartForwardVolume.Series["FVolumeSeries"].ChartType = SeriesChartType.StackedColumn;
            ChartForwardVolume.Series["FVolumeSeries"].BorderWidth = 3;
            ChartForwardVolume.Series["FVolumeSeries"].ShadowOffset = 2;
            ChartForwardVolume.Series["FVolumeSeries"].XValueType = ChartValueType.Double;

            if (freeZoneFlag)
            {
                PanelFreeZone.Visible = true;
                decimal freeZoneGB = Convert.ToInt64(ti.FreeZone) / (decimal)1000000000.0;

                //Take free zone consumption into account
                consumedGB = consumedGB - freeZoneGB;

                //avoid divide by zero
                if (FUPThresholdBytes != (decimal)0.0)
                {
                    percentConsumed = (Convert.ToDouble(consumedGB) / Convert.ToDouble(FUPThresholdBytes)) * 100.0;
                }

                LabelFreezoneConsumed.Text = "" + freeZoneGB.ToString("0.000") + " GB";
                
                ChartForwardVolume.Series.Add(new Series("FreeZoneSeries"));
                ChartForwardVolume.Series["FreeZoneSeries"].ChartType = SeriesChartType.StackedColumn;
                ChartForwardVolume.Series["FreeZoneSeries"].BorderWidth = 3;
                ChartForwardVolume.Series["FreeZoneSeries"].ShadowOffset = 2;
                ChartForwardVolume.Series["FreeZoneSeries"].XValueType = ChartValueType.Double;
                ChartForwardVolume.Series["FreeZoneSeries"].Color = Color.LightGray;

                string[] X2Values = new String[] { "Free Zone", };
                List<decimal> Y2Values = new List<decimal>();
                Y2Values.Add(freeZoneGB);
                ChartForwardVolume.Series["FreeZoneSeries"].Points.DataBindXY(X2Values, "Volume", Y2Values, "GB");
                ChartForwardVolume.Series["FreeZoneSeries"].LegendText = "Freezone in GB";
            }
            else
            {
                PanelFreeZone.Visible = false;
            }

            List<decimal> YValues = new List<decimal>();

            Color chartColor = Color.Gray;

            if (percentConsumed <= 80.0)
            {
                chartColor = Color.Green;
            }
            else if (percentConsumed > 80.0 && percentConsumed < 100.0)
            {
                chartColor = Color.Orange;
            }
            else
            {
                chartColor = Color.Red;
            }

            ChartForwardVolume.Series["FVolumeSeries"].Color = chartColor;
            YValues.Add(consumedGB);
            LabelVolumeConsumed.Text = "" + consumedGB.ToString("0.000") + " GB";

            string[] XValues = new String[] { "Forward Volume" };
            ChartForwardVolume.Series["FVolumeSeries"].Points.DataBindXY(XValues, "Volume", YValues, "GB");
            ChartForwardVolume.Series["FVolumeSeries"].LegendText = "Volumes in GB";
            ChartForwardVolume.Series.Add(new Series("FUPThreshold"));
            ChartForwardVolume.Series["FUPThreshold"].ChartType = SeriesChartType.Line;
            ChartForwardVolume.Series["FUPThreshold"].BorderWidth = 3;

            if (percentConsumed < 100)
            {
                ChartForwardVolume.Series["FUPThreshold"].Color = Color.Red;
            }
            else
            {
                ChartForwardVolume.Series["FUPThreshold"].Color = Color.Wheat;
            }

            if (FUPThresholdBytes != (decimal)0.0)
            {
                ChartForwardVolume.Series["FUPThreshold"].LegendText = "FUP Threshold in GB";
                ChartForwardVolume.Series["FUPThreshold"].AxisLabel = "FUP Threshold";
                ChartForwardVolume.Series["FUPThreshold"].MarkerStyle = MarkerStyle.Diamond;
                ChartForwardVolume.Series["FUPThreshold"].MarkerSize = 15;
                ChartForwardVolume.Series["FUPThreshold"].IsValueShownAsLabel = true;
                ChartForwardVolume.Series["FUPThreshold"].LabelForeColor = Color.Yellow;
                ChartForwardVolume.Series["FUPThreshold"].LabelBackColor = Color.Black;

                List<decimal> FUPValues = new List<decimal>();
                FUPValues.Add(Decimal.Round(FUPThresholdBytesGB, 2));
                ChartForwardVolume.Series["FUPThreshold"].Points.DataBindY(FUPValues);
            }
            
            Legend volumeLegend = new Legend("VolLegend");
            ChartForwardVolume.Legends.Add(new Legend("Volume in GB"));
        }

        /// <summary>
        /// Shows the consumed return volume
        /// </summary>
        /// <param name="ti">The source TerminalInfo entity</param>
        public void showReturnGraph(TerminalInfo ti)
        {
            //Check if this is a freezone SLA
            bool freeZoneFlag = _boAccountingControl.IsFreeZoneSLA(Int32.Parse(ti.SlaId));
            
            LabelFUPResetDay.Text = ti.InvoiceDayOfMonth;

            decimal consumedGB = Convert.ToInt64(ti.ConsumedReturn) / (decimal)1000000000.0;

            decimal RTNFUPThresholdBytes = ti.RTNFUPThreshold * (decimal)1000000000.0;
            //RTNFUPThresholdBytes = RTNFUPThresholdBytes + Convert.ToDecimal(ti.AdditionalReturn);

            Decimal RTNFUPThresholdBytesGB = RTNFUPThresholdBytes / (decimal)1000000000.0;
            LabelReturnVolumeAllocation.Text = RTNFUPThresholdBytesGB.ToString("#.00") + " GB";

            double percentConsumed = 0.0;

            LabelTimeStamp.Text = DateTime.Now.ToUniversalTime() + " UTC";

            //avoid divide by zero
            if (RTNFUPThresholdBytes != (decimal)0.0)
            {
                percentConsumed = (Convert.ToDouble(ti.ConsumedReturn) / Convert.ToDouble(RTNFUPThresholdBytes)) * 100.0;
                Decimal percentConsumedDec = new Decimal(percentConsumed);
            }

            ChartReturnVolume.Width = 320;
            ChartReturnVolume.Titles.Add("Return Volume Status");
            ChartReturnVolume.Titles[0].Font = new Font("Utopia", 16);
            //ChartReturnVolume.BackSecondaryColor = Color.WhiteSmoke;
            //ChartReturnVolume.BackGradientStyle = System.Web.UI.DataVisualization.Charting.GradientStyle.DiagonalRight;
            ChartReturnVolume.ChartAreas[0].BackColor = Color.LightSalmon;
            ChartReturnVolume.ChartAreas[0].BackSecondaryColor = Color.LightGreen;
            ChartReturnVolume.ChartAreas[0].BackGradientStyle = System.Web.UI.DataVisualization.Charting.GradientStyle.TopBottom;
            ChartReturnVolume.ChartAreas[0].Area3DStyle.Enable3D = false;

            ChartReturnVolume.Series.Add(new Series("RVolumeSeries"));

            ChartReturnVolume.Series["RVolumeSeries"].ChartType = SeriesChartType.StackedColumn;
            ChartReturnVolume.Series["RVolumeSeries"].BorderWidth = 3;
            ChartReturnVolume.Series["RVolumeSeries"].ShadowOffset = 2;
            ChartReturnVolume.Series["RVolumeSeries"].XValueType = ChartValueType.Double;

            if (freeZoneFlag)
            {
                decimal freeZoneGB = Convert.ToInt64(ti.ConsumedFreeZone) / (decimal)1000000000.0;

                //Take free zone consumption into account
                consumedGB = consumedGB - freeZoneGB;
                
                //avoid divide by zero
                if (RTNFUPThresholdBytes != (decimal)0.0)
                {
                    percentConsumed = (Convert.ToDouble(consumedGB) / Convert.ToDouble(RTNFUPThresholdBytesGB)) * 100.0;
                }
                
                LabelRTNFreezoneConsumed.Text = "" + freeZoneGB.ToString("0.000") + " GB";

                ChartReturnVolume.Series.Add(new Series("FreeZoneSeries"));
                ChartReturnVolume.Series["FreeZoneSeries"].ChartType = SeriesChartType.StackedColumn;
                ChartReturnVolume.Series["FreeZoneSeries"].BorderWidth = 3;
                ChartReturnVolume.Series["FreeZoneSeries"].ShadowOffset = 2;
                ChartReturnVolume.Series["FreeZoneSeries"].XValueType = ChartValueType.Double;
                ChartReturnVolume.Series["FreeZoneSeries"].Color = Color.LightGray;

                string[] X2Values = new String[] { "Free Zone", };
                List<decimal> Y2Values = new List<decimal>();
                Y2Values.Add(freeZoneGB);
                ChartReturnVolume.Series["FreeZoneSeries"].Points.DataBindXY(X2Values, "Volume", Y2Values, "GB");
                ChartReturnVolume.Series["FreeZoneSeries"].LegendText = "Freezone in GB";
            }
          
            List<decimal> YValues = new List<decimal>();

            Color chartColor = Color.Gray;

            if (percentConsumed <= 80.0)
            {
                chartColor = Color.Green;
            }
            else if (percentConsumed > 80.0 && percentConsumed < 100.0)
            {
                chartColor = Color.Orange;
            }
            else
            {
                chartColor = Color.Red;
            }

            ChartReturnVolume.Series["RVolumeSeries"].Color = chartColor;
            YValues.Add(consumedGB);
            LabelReturnVolumeConsumed.Text = "" + consumedGB.ToString("0.000") + " GB";

            string[] XValues = new String[] { "Return Volume" };
            ChartReturnVolume.Series["RVolumeSeries"].Points.DataBindXY(XValues, "Volume", YValues, "GB");
            ChartReturnVolume.Series["RVolumeSeries"].LegendText = "Volumes in GB";
            ChartReturnVolume.Series.Add(new Series("FUPThreshold"));
            ChartReturnVolume.Series["FUPThreshold"].ChartType = SeriesChartType.Line;
            ChartReturnVolume.Series["FUPThreshold"].BorderWidth = 3;

            if (percentConsumed < 100)
            {
                ChartReturnVolume.Series["FUPThreshold"].Color = Color.Red;
            }
            else
            {
                ChartReturnVolume.Series["FUPThreshold"].Color = Color.Wheat;
            }

            if (RTNFUPThresholdBytes != (decimal)0.0)
            {
                ChartReturnVolume.Series["FUPThreshold"].LegendText = "FUP Threshold in GB";
                ChartReturnVolume.Series["FUPThreshold"].AxisLabel = "FUP Threshold";
                ChartReturnVolume.Series["FUPThreshold"].MarkerStyle = MarkerStyle.Diamond;
                ChartReturnVolume.Series["FUPThreshold"].MarkerSize = 15;
                ChartReturnVolume.Series["FUPThreshold"].IsValueShownAsLabel = true;
                ChartReturnVolume.Series["FUPThreshold"].LabelForeColor = Color.Yellow;
                ChartReturnVolume.Series["FUPThreshold"].LabelBackColor = Color.Black;

                List<decimal> FUPValues = new List<decimal>();
                //Scaling
                FUPValues.Add(Decimal.Round(RTNFUPThresholdBytesGB, 2));
                ChartReturnVolume.Series["FUPThreshold"].Points.DataBindY(FUPValues);
            }
             
            Legend volumeLegend = new Legend("VolLegend");
            ChartReturnVolume.Legends.Add(new Legend("Volume in GB"));
         
        }

        ///// <summary>
        ///// Shows the consumed forward volume for a voucher SLA
        ///// </summary>
        ///// <param name="ti">The soruce TerminalInfo entity</param>
        //public void showForwardGraphVoucher(TerminalInfo ti)
        //{
        //    //Calculate volume allocation
        //    decimal FUPThresholdBytes = ti.FUPThreshold * (decimal)1000000000.0 + Convert.ToDecimal(ti.Additional);
        //    //Calculate remaining volume
        //    decimal remainingGB = ((FUPThresholdBytes - Convert.ToInt64(ti.Consumed) + Convert.ToDecimal(ti.FreeZone)) / (decimal)1000000000.0);
            
        //    //Convert numbers
        //    Decimal FUPThresholdBytesGB = FUPThresholdBytes / (decimal)1000000000.0;
        //    Decimal percentRemainingDec = (decimal)0.0;

        //    if (FUPThresholdBytes != (decimal)0.0)
        //    {
        //        percentRemainingDec = (remainingGB / FUPThresholdBytesGB) * (decimal)100.0;
        //    }

        //    //Create chart
        //    ChartForwardVolume.Width = 320;
        //    ChartForwardVolume.Titles.Add("Forward Volume Status");
        //    ChartForwardVolume.Titles[0].Font = new Font("Utopia", 16);
        //    ChartForwardVolume.ChartAreas[0].BackColor = Color.LightGreen;
        //    ChartForwardVolume.ChartAreas[0].BackSecondaryColor = Color.LightSalmon;
        //    ChartForwardVolume.ChartAreas[0].BackGradientStyle = System.Web.UI.DataVisualization.Charting.GradientStyle.TopBottom;
        //    ChartForwardVolume.ChartAreas[0].Area3DStyle.Enable3D = false;
        //    ChartForwardVolume.Series.Add(new Series("FVolumeSeries"));
        //    ChartForwardVolume.Series["FVolumeSeries"].ChartType = SeriesChartType.StackedColumn;
        //    ChartForwardVolume.Series["FVolumeSeries"].BorderWidth = 3;
        //    ChartForwardVolume.Series["FVolumeSeries"].ShadowOffset = 2;
        //    ChartForwardVolume.Series["FVolumeSeries"].XValueType = ChartValueType.Double;

        //    //Create freezone area
        //    if (_boAccountingControl.IsFreeZoneSLA(Int32.Parse(ti.SlaId)))
        //    {
        //        PanelFreeZone.Visible = true;
        //        decimal freeZoneGB = Convert.ToInt64(ti.FreeZone) / (decimal)1000000000.0;
        //        LabelFreezoneConsumed.Text = "" + freeZoneGB.ToString("0.000") + " GB";
        //    }
        //    else
        //    {
        //        PanelFreeZone.Visible = false;
        //    }

        //    //Plot chart
        //    List<decimal> YValues = new List<decimal>();

        //    Color chartColor = Color.Gray;

        //    if (percentRemainingDec >= (decimal)20.0)
        //    {
        //        chartColor = Color.Green;
        //    }
        //    else if (percentRemainingDec < (decimal)80.0 && percentRemainingDec > (decimal)0.0)
        //    {
        //        chartColor = Color.Orange;
        //    }
        //    else
        //    {
        //        chartColor = Color.Red;
        //    }

        //    ChartForwardVolume.Series["FVolumeSeries"].Color = chartColor;
        //    YValues.Add(percentRemainingDec);
            
        //    string[] XValues = new String[] { "Forward Volume" };
        //    ChartForwardVolume.Series["FVolumeSeries"].Points.DataBindXY(XValues, "Volume", YValues, "%");
        //    ChartForwardVolume.Series["FVolumeSeries"].LegendText = "Remaining volume in %";

        //    ChartForwardVolume.ChartAreas[0].AxisY.Minimum = 0;
        //    ChartForwardVolume.ChartAreas[0].AxisY.Maximum = 100;
            
        //    Legend volumeLegend = new Legend("VolLegend");
        //    ChartForwardVolume.Legends.Add(new Legend("Remaining volume in %"));
            
        //    //Fill up labels
        //    LabelVolumeAllocation.Text = FUPThresholdBytesGB.ToString("0.00") + " GB";
        //    LabelTimeStamp.Text = DateTime.Now.ToUniversalTime() + " UTC";
        //    LabelVolumeConsumed.Text = "" + percentRemainingDec.ToString("0.00") + "%";
        //    LabelForwardVolume.Text = "Forward Volume Remaining:";
        //    LabelFUPResetDay.Text = ti.InvoiceDayOfMonth;
        //}

        ///// <summary>
        ///// Shows the consumed return volume for a voucher SLA
        ///// </summary>
        ///// <param name="ti">The soruce TerminalInfo entity</param>
        //public void showReturnGraphVoucher(TerminalInfo ti)
        //{
        //    //Calculate volume allocation
        //    decimal FUPThresholdBytes = ti.RTNFUPThreshold * (decimal)1000000000.0 + Convert.ToDecimal(ti.AdditionalReturn);
        //    //Calculate remaining volume
        //    decimal remainingGB = ((FUPThresholdBytes - Convert.ToInt64(ti.ConsumedReturn) + Convert.ToDecimal(ti.ConsumedFreeZone)) / (decimal)1000000000.0);

        //    //Convert numbers
        //    Decimal FUPThresholdBytesGB = FUPThresholdBytes / (decimal)1000000000.0;
        //    Decimal percentRemainingDec = (decimal)0.0;

        //    if (FUPThresholdBytes != (decimal)0.0)
        //    {
        //        percentRemainingDec = (remainingGB / FUPThresholdBytesGB) * (decimal)100.0;
        //    }

        //    //Create chart
        //    ChartReturnVolume.Width = 320;
        //    ChartReturnVolume.Titles.Add("Return Volume Status");
        //    ChartReturnVolume.Titles[0].Font = new Font("Utopia", 16);
        //    ChartReturnVolume.ChartAreas[0].BackColor = Color.LightGreen;
        //    ChartReturnVolume.ChartAreas[0].BackSecondaryColor = Color.LightSalmon;
        //    ChartReturnVolume.ChartAreas[0].BackGradientStyle = System.Web.UI.DataVisualization.Charting.GradientStyle.TopBottom;
        //    ChartReturnVolume.ChartAreas[0].Area3DStyle.Enable3D = false;
        //    ChartReturnVolume.Series.Add(new Series("RVolumeSeries"));
        //    ChartReturnVolume.Series["RVolumeSeries"].ChartType = SeriesChartType.StackedColumn;
        //    ChartReturnVolume.Series["RVolumeSeries"].BorderWidth = 3;
        //    ChartReturnVolume.Series["RVolumeSeries"].ShadowOffset = 2;
        //    ChartReturnVolume.Series["RVolumeSeries"].XValueType = ChartValueType.Double;

        //    //Create freezone area
        //    if (_boAccountingControl.IsFreeZoneSLA(Int32.Parse(ti.SlaId)))
        //    {
        //        decimal freeZoneGB = Convert.ToInt64(ti.ConsumedFreeZone) / (decimal)1000000000.0;
        //        LabelRTNFreezoneConsumed.Text = "" + freeZoneGB.ToString("0.000") + " GB";
        //    }

        //    //Plot chart
        //    List<decimal> YValues = new List<decimal>();

        //    Color chartColor = Color.Gray;

        //    if (percentRemainingDec >= (decimal)20.0)
        //    {
        //        chartColor = Color.Green;
        //    }
        //    else if (percentRemainingDec < (decimal)80.0 && percentRemainingDec > (decimal)0.0)
        //    {
        //        chartColor = Color.Orange;
        //    }
        //    else
        //    {
        //        chartColor = Color.Red;
        //    }

        //    ChartReturnVolume.Series["RVolumeSeries"].Color = chartColor;
        //    YValues.Add(percentRemainingDec);

        //    string[] XValues = new String[] { "Return Volume" };
        //    ChartReturnVolume.Series["RVolumeSeries"].Points.DataBindXY(XValues, "Volume", YValues, "%");
        //    ChartReturnVolume.Series["RVolumeSeries"].LegendText = "Remaining volume in %";

        //    ChartReturnVolume.ChartAreas[0].AxisY.Minimum = 0;
        //    ChartReturnVolume.ChartAreas[0].AxisY.Maximum = 100;

        //    Legend volumeLegend = new Legend("VolLegend");
        //    ChartReturnVolume.Legends.Add(new Legend("Remaining volume in %"));

        //    //Fill up labels
        //    LabelReturnVolumeAllocation.Text = FUPThresholdBytesGB.ToString("0.00") + " GB";
        //    LabelTimeStamp.Text = DateTime.Now.ToUniversalTime() + " UTC";
        //    LabelReturnVolumeConsumed.Text = "" + percentRemainingDec.ToString("0.00") + "%";
        //    LabelReturnVolume.Text = "Return Volume Remaining:";
        //}

        /// <summary>
        /// Shows the consumed forward volume for a voucher SLA
        /// This version uses absolute values instead of %
        /// </summary>
        /// <param name="ti">The soruce TerminalInfo entity</param>
        public void showForwardGraphVoucherAbs(TerminalInfo ti)
        {
            //Calculate volume allocation
            decimal FUPThresholdBytes = ti.FUPThreshold * (decimal)1000000000.0 + Convert.ToDecimal(ti.Additional);
            //Calculate remaining volume
            decimal remainingGB = ((FUPThresholdBytes - Convert.ToInt64(ti.Consumed) + Convert.ToDecimal(ti.FreeZone)) / (decimal)1000000000.0);

            //Convert numbers
            Decimal FUPThresholdBytesGB = FUPThresholdBytes / (decimal)1000000000.0;
            Decimal percentRemainingDec = (decimal)0.0;

            if (FUPThresholdBytes != (decimal)0.0)
            {
                percentRemainingDec = (remainingGB / FUPThresholdBytesGB) * (decimal)100.0;
            }

            //Create chart
            ChartForwardVolume.Width = 320;
            ChartForwardVolume.Titles.Add("Forward Volume Status");
            ChartForwardVolume.Titles[0].Font = new Font("Utopia", 16);
            ChartForwardVolume.ChartAreas[0].BackColor = Color.LightGreen;
            ChartForwardVolume.ChartAreas[0].BackSecondaryColor = Color.LightSalmon;
            ChartForwardVolume.ChartAreas[0].BackGradientStyle = System.Web.UI.DataVisualization.Charting.GradientStyle.TopBottom;
            ChartForwardVolume.ChartAreas[0].Area3DStyle.Enable3D = false;
            ChartForwardVolume.Series.Add(new Series("FVolumeSeries"));
            ChartForwardVolume.Series["FVolumeSeries"].ChartType = SeriesChartType.StackedColumn;
            ChartForwardVolume.Series["FVolumeSeries"].BorderWidth = 3;
            ChartForwardVolume.Series["FVolumeSeries"].ShadowOffset = 2;
            ChartForwardVolume.Series["FVolumeSeries"].XValueType = ChartValueType.Double;

            //Create freezone area
            if (_boAccountingControl.IsFreeZoneSLA(Int32.Parse(ti.SlaId)))
            {
                PanelFreeZone.Visible = true;
                decimal freeZoneGB = Convert.ToInt64(ti.FreeZone) / (decimal)1000000000.0;
                LabelFreezoneConsumed.Text = "" + freeZoneGB.ToString("0.000") + " GB";
            }
            else
            {
                PanelFreeZone.Visible = false;
            }

            //Plot chart
            List<decimal> YValues = new List<decimal>();

            Color chartColor = Color.Gray;

            if (percentRemainingDec >= (decimal)20.0)
            {
                chartColor = Color.Green;
            }
            else if (percentRemainingDec < (decimal)80.0 && percentRemainingDec > (decimal)0.0)
            {
                chartColor = Color.Orange;
            }
            else
            {
                chartColor = Color.Red;
            }

            ChartForwardVolume.Series["FVolumeSeries"].Color = chartColor;
            YValues.Add(remainingGB);

            string[] XValues = new String[] { "Forward Volume" };
            ChartForwardVolume.Series["FVolumeSeries"].Points.DataBindXY(XValues, "Volume", YValues, "GB");
            ChartForwardVolume.Series["FVolumeSeries"].LegendText = "Remaining volume in GB";

            ChartForwardVolume.ChartAreas[0].AxisY.Minimum = 0;
            ChartForwardVolume.ChartAreas[0].AxisY.Maximum = 10;
            if (remainingGB > 10)
            {
                ChartForwardVolume.ChartAreas[0].AxisY.Maximum = Convert.ToInt32(remainingGB) + 2;
            }

            Legend volumeLegend = new Legend("VolLegend");
            ChartForwardVolume.Legends.Add(new Legend("Remaining volume in GB"));

            //Fill up labels
            LabelVolumeAllocation.Visible = false;
            LabelForwardAllocation.Visible = false;
            LabelTimeStamp.Text = DateTime.Now.ToUniversalTime() + " UTC";
            LabelVolumeConsumed.Text = "" + remainingGB.ToString("0.00") + " GB";
            LabelForwardVolume.Text = "Forward Volume Remaining:";
            LabelFUPReset.Visible = false;
        }

        /// <summary>
        /// Shows the consumed return volume for a voucher SLA
        /// This version uses absolute values instead of % 
        /// </summary>
        /// <param name="ti">The soruce TerminalInfo entity</param>
        public void showReturnGraphVoucherAbs(TerminalInfo ti)
        {
            //Calculate volume allocation
            decimal FUPThresholdBytes = ti.RTNFUPThreshold * (decimal)1000000000.0 + Convert.ToDecimal(ti.AdditionalReturn);
            //Calculate remaining volume
            decimal remainingGB = ((FUPThresholdBytes - Convert.ToInt64(ti.ConsumedReturn) + Convert.ToDecimal(ti.ConsumedFreeZone)) / (decimal)1000000000.0);

            //Convert numbers
            Decimal FUPThresholdBytesGB = FUPThresholdBytes / (decimal)1000000000.0;
            Decimal percentRemainingDec = (decimal)0.0;

            if (FUPThresholdBytes != (decimal)0.0)
            {
                percentRemainingDec = (remainingGB / FUPThresholdBytesGB) * (decimal)100.0;
            }

            //Create chart
            ChartReturnVolume.Width = 320;
            ChartReturnVolume.Titles.Add("Return Volume Status");
            ChartReturnVolume.Titles[0].Font = new Font("Utopia", 16);
            ChartReturnVolume.ChartAreas[0].BackColor = Color.LightGreen;
            ChartReturnVolume.ChartAreas[0].BackSecondaryColor = Color.LightSalmon;
            ChartReturnVolume.ChartAreas[0].BackGradientStyle = System.Web.UI.DataVisualization.Charting.GradientStyle.TopBottom;
            ChartReturnVolume.ChartAreas[0].Area3DStyle.Enable3D = false;
            ChartReturnVolume.Series.Add(new Series("RVolumeSeries"));
            ChartReturnVolume.Series["RVolumeSeries"].ChartType = SeriesChartType.StackedColumn;
            ChartReturnVolume.Series["RVolumeSeries"].BorderWidth = 3;
            ChartReturnVolume.Series["RVolumeSeries"].ShadowOffset = 2;
            ChartReturnVolume.Series["RVolumeSeries"].XValueType = ChartValueType.Double;

            //Create freezone area
            if (_boAccountingControl.IsFreeZoneSLA(Int32.Parse(ti.SlaId)))
            {
                decimal freeZoneGB = Convert.ToInt64(ti.ConsumedFreeZone) / (decimal)1000000000.0;
                LabelRTNFreezoneConsumed.Text = "" + freeZoneGB.ToString("0.000") + " GB";
            }

            //Plot chart
            List<decimal> YValues = new List<decimal>();

            Color chartColor = Color.Gray;

            if (percentRemainingDec >= (decimal)20.0)
            {
                chartColor = Color.Green;
            }
            else if (percentRemainingDec < (decimal)80.0 && percentRemainingDec > (decimal)0.0)
            {
                chartColor = Color.Orange;
            }
            else
            {
                chartColor = Color.Red;
            }

            ChartReturnVolume.Series["RVolumeSeries"].Color = chartColor;
            YValues.Add(remainingGB);

            string[] XValues = new String[] { "Return Volume" };
            ChartReturnVolume.Series["RVolumeSeries"].Points.DataBindXY(XValues, "Volume", YValues, "GB");
            ChartReturnVolume.Series["RVolumeSeries"].LegendText = "Remaining volume in GB";

            ChartReturnVolume.ChartAreas[0].AxisY.Minimum = 0;
            ChartReturnVolume.ChartAreas[0].AxisY.Maximum = ChartForwardVolume.ChartAreas[0].AxisY.Maximum;

            Legend volumeLegend = new Legend("VolLegend");
            ChartReturnVolume.Legends.Add(new Legend("Remaining volume in GB"));

            //Fill up labels
            LabelReturnVolumeAllocation.Visible = false;
            LabelReturnAllocation.Visible = false;
            LabelTimeStamp.Text = DateTime.Now.ToUniversalTime() + " UTC";
            LabelReturnVolumeConsumed.Text = "" + remainingGB.ToString("0.00") + " GB";
            LabelReturnVolume.Text = "Return Volume Remaining:";
        }
    }
}