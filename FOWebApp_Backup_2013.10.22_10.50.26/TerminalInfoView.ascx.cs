﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Web.UI.DataVisualization.Charting;
using System.Globalization;
using System.Web.Security;
using FOWebApp.net.nimera.cmt.BOMonitorControlWSRef;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;

namespace FOWebApp
{
    public enum CriteriaType
    {
        bymac, bysitid
    }

    /// <summary>
    /// The TerminalInfoView control provides a quick overview of the different terminal parameters.
    /// </summary>
    /// <remarks>
    /// This control uses the BOMonitorControlWS web service provided by the BOMonitorController.
    /// </remarks>
    public partial class TerminalInfoView : System.Web.UI.UserControl
    {
        CriteriaType _cType;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (_cType == CriteriaType.bymac)
            {
                LabelValue.Text = "Mac Address:";
            }
            else
            {
                LabelValue.Text = "Sit Id:";
            }
        }

        #region Getters and Setters

        public CriteriaType CType
        {
            set { _cType = value; }
        }
 

        #endregion

        /// <summary>
        /// Loads the TerminalInfo properties retrieved from the BO in the different text boxes
        /// </summary>
        /// <remarks>
        /// This method uses the BOMonitorController to retrieve the data from the ISPIF. 
        /// </remarks>
        /// <param name="macAddress">The terminal MAC address as a string</param>
        protected void LoadTerminalInfo(string macAddress)
        {
            CultureInfo culture = new CultureInfo("en-US");
            BOMonitorControlWS boMonitorControl =
                new net.nimera.cmt.BOMonitorControlWSRef.BOMonitorControlWS();

            BOAccountingControlWS boAccountingControl =
              new BOAccountingControlWS();

            //We must fetch the terminal information first  to get to the ISP identifier
            Terminal term = boAccountingControl.GetTerminalDetailsByMAC(macAddress);
            TerminalInfo ti = boMonitorControl.getTerminalInfoByMacAddress(macAddress, term.IspId);

            if (ti.ErrorFlag)
            {
                Label6.Visible = true;
                LabelError.Visible = true;
                LabelError.Text = ti.ErrorMsg;
            }
            else
            {
                //Load ti properties into fields.
                Label6.Visible = false;
                LabelError.Visible = false;
                LabelSiteId.Text = ti.SitId;
                LabelSiteName.Text = ti.EndUserId;
                LabelIPAddress.Text = ti.IPAddress;
                LabelMacAddress.Text = ti.MacAddress;

                //Changed this code:
                //long consumedMB = Convert.ToInt64(ti.Consumed) / 10485760;
                //decimal consumedGB = Convert.ToInt64(ti.Consumed) / (decimal)1073741824.0;
                //Into this code:
                decimal consumedGB = Convert.ToInt64(ti.Consumed) / (decimal)1000000000.0;
                LabelFConsumed.Text = "" + consumedGB.ToString("0.000") + " GB";

                LabelServicePackage.Text = ti.SlaName;
                //decimal FUPThresholdBytes = ti.FUPThreshold * (decimal)1073741824.0;
                decimal FUPThresholdBytes = ti.FUPThreshold * (decimal)1000000000.0;

                //double percentConsumed = (consumedMB / FUPThresholdMB) * 100.0;
                double percentConsumed = (Convert.ToDouble(ti.Consumed) / Convert.ToDouble(FUPThresholdBytes)) * 100.0;
                Decimal percentConsumedDec = new Decimal(percentConsumed);

                LabelVolumeAllocation.Text = ti.FUPThreshold + " GB";
                //LabelVolumeAllocation.Text = FUPThresholdBytes + " Bytes";
                LabelFConsumed.Text += " (" + percentConsumedDec.ToString("#.00") + " %)";
                LabelResetDayOfMOnth.Text = ti.InvoiceDayOfMonth;

                double rtnNum = Double.Parse(ti.RTN, culture);
                LabelRTN.Text = rtnNum.ToString("#.0") + " dBHz";
                //double rtnNum = Convert.ToDouble(ti.RTN);

                if (rtnNum < 55.2)
                {
                    LabelRTN.ForeColor = Color.Red;
                }
                else if (rtnNum >= 55.2 && rtnNum < 58.4)
                {
                    LabelRTN.ForeColor = Color.DarkOrange;
                }
                else
                {
                    LabelRTN.ForeColor = Color.DarkGreen;
                }


                LabelStatus.Text = ti.Status;

                if (ti.Status == "Operational")
                {
                    LabelStatus.ForeColor = Color.Green;
                }
                else
                {
                    LabelStatus.ForeColor = Color.Red;
                }

                ChartVolume.Width = 640;
                ChartVolume.Titles.Add("Consumption Overview");
                ChartVolume.Titles[0].Font = new Font("Utopia", 16);
                ChartVolume.BackColor = Color.Gray;
                ChartVolume.BackSecondaryColor = Color.WhiteSmoke;
                ChartVolume.BackGradientStyle = System.Web.UI.DataVisualization.Charting.GradientStyle.DiagonalRight;
                ChartVolume.BorderSkin.SkinStyle = System.Web.UI.DataVisualization.Charting.BorderSkinStyle.Raised;
                //ChartVolume.ChartAreas[0].AlignmentOrientation = AreaAlignmentOrientations.Horizontal;
                ChartVolume.ChartAreas[0].BackColor = Color.LightSalmon;
                ChartVolume.ChartAreas[0].BackSecondaryColor = Color.LightGreen;
                ChartVolume.ChartAreas[0].BackGradientStyle = System.Web.UI.DataVisualization.Charting.GradientStyle.TopBottom;
                ChartVolume.ChartAreas[0].Area3DStyle.Enable3D = false;

                ChartVolume.Series.Add(new Series("FVolumeSeries"));

                ChartVolume.Series["FVolumeSeries"].ChartType = SeriesChartType.Column;
                ChartVolume.Series["FVolumeSeries"].BorderWidth = 3;
                ChartVolume.Series["FVolumeSeries"].ShadowOffset = 2;
                ChartVolume.Series["FVolumeSeries"].XValueType = ChartValueType.Double;

                Color chartColor = Color.Gray;

                if (percentConsumed <= 80.0)
                {
                    chartColor = Color.Green;
                }
                else if (percentConsumed > 80.0 && percentConsumed < 100.0)
                {
                    chartColor = Color.Orange;
                }
                else
                {
                    chartColor = Color.Red;
                }

                ChartVolume.Series["FVolumeSeries"].Color = chartColor;
                List<decimal> YValues = new List<decimal>();
                YValues.Add(consumedGB);
                string[] XValues = new String[] { "Forward Volume" };
                ChartVolume.Series["FVolumeSeries"].Points.DataBindXY(XValues, "Volume", YValues, "GB");
                ChartVolume.Series["FVolumeSeries"].LegendText = "Volumes in GB";

                ChartVolume.Series.Add(new Series("FUPThreshold"));
                ChartVolume.Series["FUPThreshold"].ChartType = SeriesChartType.Line;
                ChartVolume.Series["FUPThreshold"].BorderWidth = 3;
                if (percentConsumed < 100)
                {
                    ChartVolume.Series["FUPThreshold"].Color = Color.Red;
                }
                else
                {
                    ChartVolume.Series["FUPThreshold"].Color = Color.Wheat;
                }
                ChartVolume.Series["FUPThreshold"].LegendText = "FUP Threshold in GB";
                ChartVolume.Series["FUPThreshold"].AxisLabel = "FUP Threshold";
                ChartVolume.Series["FUPThreshold"].MarkerStyle = MarkerStyle.Diamond;
                ChartVolume.Series["FUPThreshold"].MarkerSize = 15;
                ChartVolume.Series["FUPThreshold"].IsValueShownAsLabel = true;
                ChartVolume.Series["FUPThreshold"].LabelForeColor = Color.Yellow;
                ChartVolume.Series["FUPThreshold"].LabelBackColor = Color.Black;



                List<decimal> FUPValues = new List<decimal>();
                FUPValues.Add(ti.FUPThreshold);
                ChartVolume.Series["FUPThreshold"].Points.DataBindY(FUPValues);

                Legend volumeLegend = new Legend("VolLegend");
                ChartVolume.Legends.Add(new Legend("Volume in GB"));
            }
        }

        /// <summary>
        /// Loads the terminal data for a terminal with the given macAddress
        /// </summary>
        /// <param name="macAddress">MAC Address as a string</param>
        public void LoadTerminalInfoByMacAddress(string macAddress)
        {
            //Check if the terminal is owned by the authenticated user
            if (this.IsOnwedByDistributor(macAddress))
            {
                this.LoadTerminalInfo(macAddress);
            }
            else
            {
                Label6.Visible = true;
                LabelError.Visible = true;
                LabelError.Text = "Terminal not found";
            }
        }

        /// <summary>
        /// Returns the volume information for the terminal specified by its site id
        /// </summary>
        /// <param name="sitId">Sit id as an integer</param>
        public void LoadTerminalInfoBySitId(int sitId, int ispId)
        {
            //Find the macAddress from the sitId
            BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();
            Terminal terminal = boAccountingControl.GetTerminalDetailsBySitId(sitId, ispId);

            //Check if the terminal is owned by the authenticated user
            if (terminal != null)
            {
                if (this.IsOnwedByDistributor(terminal.MacAddress))
                {
                    this.LoadTerminalInfo(terminal.MacAddress);
                }
            }
            else
            {
                Label6.Visible = true;
                LabelError.Visible = true;
                LabelError.Text = "Terminal not found";
            }
        }

        /// <summary>
        /// Returns true if the terminal is owned by the authenticated user
        /// </summary>
        /// <param name="macAddress">The mac address as a string</param>
        /// <returns>true if the terminal is owned by the logged-in user</returns>
        protected bool IsOnwedByDistributor(string macAddress)
        {
            bool result = false;

            BOAccountingControlWS boAccountingControl =
               new net.nimera.cmt.BOAccountingControlWSRef.BOAccountingControlWS();

            MembershipUser myObject = Membership.GetUser();
            string userId = myObject.ProviderUserKey.ToString();

            //Check if the terminal is operated by the distributor
            //First  retrieve the list of terminals for this distributor
            Terminal[] terminals = boAccountingControl.GetTerminalsByDistributor(userId);

            foreach (Terminal term in terminals)
            {
                if (term.MacAddress.Equals(macAddress))
                {
                    result = true;
                    break;
                }
            }

            return result;
        }

        protected void ButtonSearch_Click(object sender, EventArgs e)
        {
            switch (_cType)
            {
                case CriteriaType.bysitid:
                    try
                    {
                        this.LoadTerminalInfoBySitId(Convert.ToInt32(TextBoxValue.Text), 112);
                    }
                    catch (Exception ex)
                    {
                        BOLogControlWS boLogControlWS = new BOLogControlWS();
                        CmtApplicationException cmtEx = new CmtApplicationException();
                        cmtEx.ExceptionDesc = ex.Message;
                        cmtEx.ExceptionStacktrace = ex.StackTrace;
                        boLogControlWS.LogApplicationException(cmtEx);
                        Label6.Visible = true;
                        LabelError.Visible = true;
                        LabelError.Text = "The value given is not the site id!";
                    }
                    break;
                case CriteriaType.bymac:
                    this.LoadTerminalInfoByMacAddress(TextBoxValue.Text);
                    break;

                default:
                    this.LoadTerminalInfoByMacAddress(TextBoxValue.Text);
                    break;
            }

            PanelResult.Visible = true;
        }

    }
}