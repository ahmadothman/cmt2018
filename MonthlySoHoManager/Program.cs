﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MonthlySoHoManager.BOMonitorControlWSRef;
using MonthlySoHoManager.BOLogControlWSRef;
using MonthlySoHoManager.BOTaskControlWSRef;
using MonthlySoHoManager.Util;
using System.Data.SqlClient;
using MonthlySoHoManager.BOAccountingControlWSRef;
using MonthlySoHoManager.NMSGatewayWSRef;

namespace MonthlySoHoManager
{
    class Program
    {
        private string _connectionString;
        private float _volThreshold;
        private BOLogControlWS _boLogControl = null;
        private BOTaskControlWS _boTaskControlWS = null;
        private BOMonitorControlWS _boMonitorControlWS = null;
        private BOAccountingControlWS _boAccountingControlWS = null;
        private string userId;
        static void Main(string[] args)
        {
            Program prg = new Program();
            prg.mainBody();
        }

        private void mainBody()
        {
            Logger.log("Processing started MonthlySoHoManagerRenewal");
            _connectionString = Properties.Settings.Default.ConnectionString;
            userId = Properties.Settings.Default.UserID;
            _boLogControl = new BOLogControlWS();
            _boAccountingControlWS = new BOAccountingControlWS();
            _boMonitorControlWS = new BOMonitorControlWS();
            NMSGatewayService nms = new NMSGatewayService();

            //get unlocked prepaid business service terminals
            string queryCmd = "select MacAddress, t.FullName, t.ExpiryDate, t.ispid, d.EMail ,s.SlaName ,t.distributorid from terminals t join servicepacks s on t.slaid = s.SlaID " +
                              "join Distributors d on t.distributorid = d.Id " +
                              "where t.admstatus in (1,2) and s.ServiceClass = 7 ";

            List<terminalObject> terminals = new List<terminalObject>();

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(queryCmd, conn))
                    {
                        // get the terminals
                        terminalObject to;
                        SqlDataReader reader = cmd.ExecuteReader();
                        while(reader.Read())
                        {
                            to = new terminalObject();
                            to.macAddress = reader.GetString(0);
                            to.termName = reader.GetString(1);
                            if (reader.IsDBNull(2))
                                to.expiryDate = null;
                            else
                                to.expiryDate = reader.GetDateTime(2);

                            to.ispId = reader.GetInt32(3);
                            if (reader.IsDBNull(4))
                                to.distEMail = null;
                            else
                                to.distEMail = reader.GetString(4);
                            to.slaName = reader.GetString(5);
                            to.distributorId = reader.GetInt32(6);
                            terminals.Add(to);


                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
            }
            Terminal term;
            //check the remaining subscription days for each terminal
            foreach (terminalObject t in terminals)
            {
                NMSTerminalInfo nmsTerminal = nms.getTerminalInfoByMacAddress(t.macAddress);
                Terminal trm = _boAccountingControlWS.GetTerminalDetailsByMAC(t.macAddress);

                ServiceLevel sl = _boAccountingControlWS.GetServicePack((int)trm.SlaId);
               
                try
                {
                    
                        if (nmsTerminal.ExpirationDate.Date == DateTime.Now.Date|| nmsTerminal.ExpirationDate.Date == DateTime.Now.Date.AddYears(100))
                        {
                        if (!nmsTerminal.OverVolume)
                            nms.AddVolumeWithValidity(trm.SitId, trm.IspId, (long)-nmsTerminal.CurVolSum, (long)-nmsTerminal.CurVolSum, 0);

                        if (nms.AddVolumeWithValidity(trm.SitId, trm.IspId, (long)sl.HighVolumeFWD*1000,(long)sl.HighVolumeRTN*1000,30))
                            
                        {
                            this.addTerminalActivity("Volume added", "Renewal of monthly SoHo terminal", t);
                            this.sendMailVolumeAdded(t.distEMail.Split(';'), t.termName, t.macAddress);

                        }
                        else
                        {

                            string body = "Dear Support Team ,  "
                               + " ,  CMT was not able to renew the monthly SOHO subscription for the terminals :" + t.macAddress + " SLA:" + t.slaName ;
                            string[] toAddresses = new string[1];
                            toAddresses[0] = "support@satadsl.net";
                            _boLogControl.SendMail(body, "Faild to Renew Monthly SOHO", toAddresses);
                        }


                    }




                }

                catch (Exception ex)
                {
                    Logger.LogError(ex);
                    string body = "Dear Support Team ,  "
                               + " ,  CMT was not able to add volume activation for the monthly SOHO subscription for the terminals :" + t.macAddress + " SLA:" + t.slaName +
                               " The reason: " + ex.ToString();
                    string[] toAddresses = new string[1];
                    toAddresses[0] = "support@satadsl.net";
                    _boLogControl.SendMail(body, "Faild to activate Monthly SOHO", toAddresses);
                }
            }

            Logger.log("Processing finished");
        }



        private bool sendMailVolumeAdded(string[] recipient, string termName, string macAddress)
        {
            bool success = false;
            string body = "<p>Dear distributor, </br></p>" +
                          "<p>Please be advised that a new volume has been added to the terminal </br> </p> <p><b>" + termName + " MAC " + macAddress + "</b>.</br></p> <p> </br></p>" +
                          "<p>Your subscription has been renewed for one month. </br></p>" +
                          "<p></br></p>" +
                          "<p>Best regards, </br></p>" +
                          "<p>The SatADSL Team </br></p>" +
                          "<p>--------</br></p>" +
                          "<p>   </br></p>";
            /*"<p>Caro Distribuidor, </br></p>" +
            "<p>Por favor tome em consideração que o volume do terminal: </br> </p> <p><b>" + termName + " MAC " + macAddress + "</b>.</br></p> <p>é quase zero. </br></p>" +
            "<p>Quando o volume esgotar, o terminal perderá o acesso à internet. </br></p>" +
            "<p>Poderá renovar a sua subscrição adquirindo um voucher. </br></p>" +
            "<p>Os nossos melhores cumprimentos, </br></p>" +
            "<p>A Equipa Satadsl </br></p>" +
            "<p>--------</br></p>" +
            "<p>   </br></p>" +
            "<p>Cher Partenaire,</br></p>" +
            "<p>Veuillez noter que le volume du terminal: </br> </p> <p><b>" + termName + " MAC " + macAddress + "</b>.</br></p> <p>est presque épuisé.</br></p>" +
            "<p>Un fois ce volume utilisé, le terminal n'aura plus accès à Internet.</br></p>" +
            "<p>Vous pouvez rétablir la connectivité en achetant une recharge de volume.</br></p>" +
            "<p>Meilleures salutation, </br></p>" +
            "<p>L'équipe SatADSL </br></p>";*/




            string subject = "Your monthly subscription has been renewed";
            
            try
            {
                if (_boLogControl.SendMail(body, subject, recipient))
                {
                    Logger.log("Volume Notification Email sent to " + recipient + " for terminal " + macAddress);
                    success = true;
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
            }

            return success;
        }

    
        private void addTerminalActivity(string task, string subject, terminalObject term)
        {
            TerminalActivity terminalActivity = new TerminalActivity();
            terminalActivity.Action = "Monthly subscription renewal - Volume added";
            terminalActivity.ActionDate = DateTime.Now;
            terminalActivity.MacAddress = term.macAddress;
            terminalActivity.AccountingFlag = false;
            terminalActivity.SlaName = "";
            terminalActivity.NewSlaName = "";
            terminalActivity.TerminalActivityId = 1001;
            //Setting the SOHO manager app user as user
            string cmtUser = Properties.Settings.Default.UserID;
            terminalActivity.UserId = new Guid(cmtUser);
            _boLogControl.LogTerminalActivity(terminalActivity);
        }


        private class terminalObject
        {
            public string macAddress { get; set; }
            public string termName { get; set; }
            public DateTime? expiryDate { get; set; }
            public int ispId { get; set; }
            public string distEMail { get; set; }
            public float remainingVolume { get; set; }
            public string slaName { get; set; }
            public int distributorId { get; set; }
        }
    }
}

