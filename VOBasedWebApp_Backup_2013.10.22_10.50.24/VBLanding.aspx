﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="VBLanding.aspx.cs" Inherits="VOBasedWebApp.VBLanding" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <div id="main-container">
        <div class="wrapper clearfix">
    <article>
        <img src="Images/empty.jpg" alt="Locked Terminal" width="163" height="161">
        <h1>Dear Customer,</h1>
        <p>
            Your volume allocation is exhausted. Please contact your distributor to buy 
            additional volume. <br /><br />Thank you. 
        </p>
        <br />
        <p>
            <b>Your SatADSL team</b>
        </p>
    </article>
    <article>
        <img src="Images/Vide.jpg" alt="Locked Terminal" width="163" height="161">
        <h1>Cher Client,</h1>
        <p>
            Votre volume est terminé. Veuillez contacter votre distributeur pour acheter 
            du volume additionnel.
            <br /><br />Merci. 
            <br />
        </p>
        <p>   
            <b>Votre équipe SatADSL</b>
        </p>
    </article>
    <br />
    <br />
        </div>
        </div>
   </article>
</asp:Content>
