﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="Blocked.aspx.cs" Inherits="VOBasedWebApp._Default" %>
<%@ Register TagPrefix="abc" TagName="AccountBlockedControl" Src="~/AccountBlockedControl.ascx" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <div id="main-container">
        <div class="wrapper clearfix">
            <abc:AccountBlockedControl id="AccountBlocked" runat="server"/>
        </div>
    </div>
</asp:Content>
