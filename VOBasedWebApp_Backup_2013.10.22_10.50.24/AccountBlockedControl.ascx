﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AccountBlockedControl.ascx.cs" Inherits="VOBasedWebApp.AccountBlockedControl" %>
<article>
<img src="images/lock.JPG" alt="Locked Terminal" width="163" height="161">
<h1>Dear Customer,</h1>
<p>Your Distributor does not qualify anymore as Authorised Distributor for our service.
If you want to still benefit of SatADSL service, please contact us at
<a href="mailto:sales@satadsl.net">sales @ satadsl.net</a>
and we will put you in touch with the best placed Authorised Distributor in your region.
<br /><br />
Please note that any payment made to your previous Distributor may not be recoverable.
</p>
<br />
<p>
<b>Your SatADSL team</b>
</p>
</article>
