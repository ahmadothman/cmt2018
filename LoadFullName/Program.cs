﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using LoadFullName.BOAccountingControlWSRef;

namespace LoadFullName
{
    /// <summary>
    /// Loads the fullname into the CMTData Terminals table
    /// </summary>
    class Program
    {
        string updateCmd = "UPDATE Terminals SET FullName = @FullName WHERE MacAddress = @MacAddress";
 
        static void Main(string[] args)
        {
            Program prg = new Program();
            prg.mainBody(args);
        }


        private void mainBody(string[] args)
        {
            string inputFileName = "C:/Temp/TermInfoInFull.csv"; //The input csv file
            string errorFileName = "C:/Temp/LoadFullNameErrors.txt";
            string inputLine = "";
            int line = 1;

            BOAccountingControlWS accountingController =
                                          new BOAccountingControlWS();

            //Read-in the input file and process the file line by line
                StreamReader reader = new StreamReader(inputFileName);

                FileInfo fi = new FileInfo(errorFileName);
                if (fi.Exists)
                {
                    fi.Delete();
                }

                reader.ReadLine(); //First line is header
                while (!reader.EndOfStream)
                {
                    inputLine = reader.ReadLine();
                    line++;

                    string[] inputLineTokens = inputLine.Split(';');

                    Terminal terminal = accountingController.GetTerminalDetailsByMAC(inputLineTokens[3]);

                    if (terminal == null)
                    {
                        using (StreamWriter outFile = new StreamWriter(errorFileName, true))
                        {
                            outFile.WriteLine(inputLineTokens[3]);
                        }
                    }
                    else
                    {
                        terminal.FullName = inputLineTokens[2];
                        int retry = 0;

                        while (retry < 4)
                        {
                            try
                            {

                                accountingController.UpdateTerminal(terminal);
                                Console.WriteLine("Processed: " + terminal.FullName);
                                retry = 99;
                            }
                            catch (Exception ex)
                            {
                                retry++;
                            }
                        }

                        if (retry == 5)
                        {
                            using (StreamWriter outFile = new StreamWriter(errorFileName, true))
                            {
                                outFile.WriteLine("Failed to update: " + inputLineTokens[3]);
                                Console.WriteLine("Failed to update: " + inputLineTokens[3]);
                            }
                        }
                    }

                   
                }
        }
    }
}
