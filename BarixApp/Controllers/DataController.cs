﻿using BarixApp.Models;
using BOControllerLibrary.Controllers;
using BOControllerLibrary.Controllers.Interfaces;
using BOControllerLibrary.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BarixApp.Controllers
{
    public class DataController : ApiController
    {
        private string connectionString;
        private string databaseProvider;
        private readonly IBOLogController logger;

        public DataController()
        {
            connectionString = ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
            databaseProvider = ConfigurationManager.ConnectionStrings["ApplicationServices"].ProviderName;
            logger = new BOLogController(connectionString: connectionString, databaseProvider: databaseProvider);
        }

        /// <summary>
        /// Save Barix device report to database
        /// </summary>
        /// <param name="mac"></param>
        /// <param name="alarm"></param>
        /// <param name="info"></param>
        /// <returns>Http status code</returns>
        public IHttpActionResult GetSubmit(string mac, string alarm, string info)
        {
            try
            {
                Data barixReport = new Data(mac, alarm, info);
                string cmdString = @"INSERT INTO MultiCastLog (MacAddress, Url, BufferLevel, Latency, FrameLoss, FrameDup, FrameDrop, 
							SoftErrorCount, StreamNumber, Bitrate, Reconnects, LastError, LastErrorDesc, Volume, UpTime, LastUpdate) 
							VALUES (@val1, @val2, @val3, @val4, @val5, @val6, @val7, @val8, @val9, @val10, @val11, @val12, @val13, @val14, @val15, @val16)";
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    using (SqlCommand comm = new SqlCommand())
                    {
                        comm.Connection = conn;
                        comm.CommandText = cmdString;
                        comm.Parameters.AddWithValue("@val1", barixReport.mac);
                        comm.Parameters.AddWithValue("@val2", barixReport.url);
                        comm.Parameters.AddWithValue("@val3", barixReport.bufferLevel);
                        comm.Parameters.AddWithValue("@val4", barixReport.latency);
                        comm.Parameters.AddWithValue("@val5", barixReport.frameLoss);
                        comm.Parameters.AddWithValue("@val6", barixReport.frameDup);
                        comm.Parameters.AddWithValue("@val7", barixReport.frameDrop);
                        comm.Parameters.AddWithValue("@val8", barixReport.softErrorCount);
                        comm.Parameters.AddWithValue("@val9", barixReport.streamNumber);
                        comm.Parameters.AddWithValue("@val10", barixReport.bitrate);
                        comm.Parameters.AddWithValue("@val11", barixReport.reconnects);
                        comm.Parameters.AddWithValue("@val12", barixReport.error);
                        comm.Parameters.AddWithValue("@val13", barixReport.errorDescription);
                        comm.Parameters.AddWithValue("@val14", barixReport.volume);
                        comm.Parameters.AddWithValue("@val15", barixReport.upTime);
                        comm.Parameters.AddWithValue("@val16", barixReport.lastUpdate);
                        try
                        {
                            conn.Open();
                            comm.ExecuteNonQuery();
                        }
                        catch (SqlException e)
                        {
                            logger.LogApplicationException(new CmtApplicationException(e, "BarixApp: Error inserting Barix status report to database", "Barix mac = " + mac));
                            return InternalServerError();
                        }
                    }
                }
                return Ok();
            }
            catch (IndexOutOfRangeException e)
            {
                logger.LogApplicationException(new CmtApplicationException(e, "BarixApp: Barix status report contains less parameters than expected", "Barix mac = " + mac));
                return InternalServerError();
            }
            catch (Exception e)
            {
                logger.LogApplicationException(new CmtApplicationException(e, "BarixApp: Error while submitting Barix status report", "Barix mac = " + mac));
                return InternalServerError();
            }
        }
    }
}
