﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace BarixApp.Models
{
    /// <summary>
    /// BarixReport
    /// </summary>
    public class Data
    {
        public int bufferLevel { get; set; }
        public int latency { get; set; }
        public int frameLoss { get; set; }
        public int frameDup { get; set; }
        public int frameDrop { get; set; }
        public int softErrorCount { get; set; }
        public int streamNumber { get; set; }
        public int bitrate { get; set; }
        public int reconnects { get; set; }
        public int error { get; set; }
        public string errorDescription { get; set; }
        public short volume { get; set; }
        public int upTime { get; set; }
        public string url { get; set; }
        public string mac { get; set; }
        public string alarm { get; set; }
        public string lastUpdate { get; set; }

        /// <summary>
        /// Barix Report Constructor
        /// </summary>
        /// <param name="macAddress"></param>
        /// <param name="alarmSatus"></param>
        /// <param name="info"></param>
        public Data(string macAddress, string alarmSatus, string info)
        {
            try
            {
                mac = Regex.Replace(macAddress, "(.{2})(.{2})(.{2})(.{2})(.{2})(.{2})", "$1:$2:$3:$4:$5:$6");
                alarm = alarmSatus;
                var splitInfo = info.Split(',');
                bufferLevel = Convert.ToInt32(splitInfo[0].After("="));
                latency = Convert.ToInt32(splitInfo[1].After("="));
                frameLoss = Convert.ToInt32(splitInfo[2].After("="));
                frameDup = Convert.ToInt32(splitInfo[3].After("="));
                frameDrop = Convert.ToInt32(splitInfo[4].After("="));
                softErrorCount = Convert.ToInt32(splitInfo[5].After("="));
                streamNumber = Convert.ToInt32(splitInfo[6].After("="));
                bitrate = Convert.ToInt32(splitInfo[7].After("="));
                reconnects = Convert.ToInt32(splitInfo[8].After("="));
                error = Convert.ToInt32(splitInfo[9].After("="));
                string errorDesc;
                errorDescriptions.TryGetValue(error, out errorDesc);
                errorDescription = errorDesc;
                volume = Convert.ToInt16(splitInfo[10].After("="));
                upTime = Convert.ToInt32(splitInfo[11].After("="));
                url = splitInfo[12].After("=");
                lastUpdate = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss:fff");
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Error descriptions
        /// </summary>
        private static Dictionary<int, string> errorDescriptions = new Dictionary<int, string> {
            { 0, "No Error"},
            { 4, "No HTTP Response"},
            { 11, "Connection Timed Out" },
            { 400, "HTTP Bad Request" }
        };
    }

    #region Extension Methods
    /// <summary>
    /// Extension Methods
    /// </summary>
    public static class ExtensionMethods
    {
        /// <summary>
        /// Get string value after [last] a.
        /// </summary>
        public static string After(this string value, string a)
        {
            int posA = value.LastIndexOf(a);
            if (posA == -1)
            {
                return "";
            }
            int adjustedPosA = posA + a.Length;
            if (adjustedPosA >= value.Length)
            {
                return "";
            }
            return value.Substring(adjustedPosA);
        }
    }
    #endregion Extension Methods
}