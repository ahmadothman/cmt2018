﻿using net.nimera.CRCCalculator;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace CRCCalculatorTests
{
    
    
    /// <summary>
    ///This is a test class for Crc16Test and is intended
    ///to contain all Crc16Test Unit Tests
    ///</summary>
    [TestClass()]
    public class Crc16Test
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for FastCrc16Calc
        ///</summary>
        [TestMethod()]
        public void FastCrc16CalcTest()
        {
            ushort aSeed = 0xEBBE; // TODO: Initialize to an appropriate value
            string inputValue = "AAAA-0001TMP-REACTOR_001VAL89.22";
            byte[] buffer = System.Text.Encoding.ASCII.GetBytes(inputValue);
            ushort expected = 18997; // TODO: Initialize to an appropriate value
            ushort actual;
            actual = Crc16.FastCrc16Calc(aSeed, buffer);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for SlowCrc16Calc, includes Hex value conversion
        ///</summary>
        [TestMethod()]
        public void SlowCrc16CalcTest()
        {
            ushort aSeed = 0xEBBE; // TODO: Initialize to an appropriate value
            string inputValue = "AAAA-0001TMP-REACTOR_001VAL89.22";
            byte[] buffer = System.Text.Encoding.ASCII.GetBytes(inputValue);
            ushort actual;
            actual = Crc16.SlowCrc16Calc(aSeed, buffer);
            //Assert.AreEqual(expected, actual);

            string hexValue = actual.ToString("X");

            Assert.AreEqual("4A35", hexValue);
        }
    }
}
