﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EpochTest
{
    class Program
    {
        static void Main(string[] args)
        {
            //Epoch test
            long epochTime = 1321353000000;
            Console.WriteLine("Epoch Time: " + epochTime);

            //.NET DateTime
            DateTime offset = new DateTime(1970, 1, 1, 0, 0, 0);

            DateTime DotNetTime = offset.AddMilliseconds(epochTime);

            Console.WriteLine(".NET TIme: " + DotNetTime);
            
        }
    }
}
