﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Security;
using System.IO;
using System.Threading;

namespace CSVFileDownload
{
    /// <summary>
    /// Test application which downloads a file from the Dashboard
    /// </summary>
    class Program
    {
        const string _DEFAULT_URL = "https://sap3.astra2connect.com/rt/ACC-P1M/ACC-P1D-112-201201.csv";
        const string _PASSWORD = "vGtzCFpzKn";
        const string _USER_NAME = "Sea_Space";
        const int _MAX_RETRIAL = 10;

        static void Main(string[] args)
        {
            Program p = new Program();
            p.mainBody(args);
        }

        private void mainBody(string[] args)
        {
            string targetUrl = "";

            if (args.Length == 1)
            {
                targetUrl = args[0];
            }
            else
            {
                targetUrl = _DEFAULT_URL;
            }

            bool failed = true;
            int trials = 0;


            while (trials < 10 && failed)
            {
                Console.WriteLine("Trial: " + (trials + 1));
                Console.WriteLine("--------------------------------------------------");
                try
                {
                    using (WebClient webClient = new WebClient())
                    {
                        webClient.Credentials = new NetworkCredential(_USER_NAME, _PASSWORD);
                        ServicePointManager.Expect100Continue = false; //Necessary to work around a Tomcat bug
                        ServicePointManager.ServerCertificateValidationCallback = delegate { return true; }; 
                        webClient.DownloadFile(targetUrl, "C:/Temp/DAILY_ACCUMULATED.csv");
                    }
                    failed = false;
                    Console.WriteLine("!!!!!!!!!!Success!!!!!!!!!!");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    failed = true;
                }
                Thread.Sleep(2000);
                trials++;
            }
        }
    }
}
