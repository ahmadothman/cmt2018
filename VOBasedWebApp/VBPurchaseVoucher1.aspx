﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="VBPurchaseVoucher1.aspx.cs" Inherits="VOBasedWebApp.VBPurchaseVoucher1" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <!-- <script type="text/javascript">
        function ValidateVouchersButtonClicked() {
            document.getElementById("<%= LabelResult.ClientID %>").style.display = "";

            var macTextBox = $find("<%= RadMaskedTextBoxMacAddress.ClientID  %>");
            var macAddress = macTextBox.get_textBoxValue();


            //Call the webservice
            VOBasedWebApp.WebServices.ValidateVoucherWS.ValidateVoucher(macAddress, code, crc, OnValidateVoucherComplete);
        }

        function OnValidateVoucherComplete(result) {
            if (result) {
                document.getElementById("<%= LabelResult.ClientID %>").style.Color = "green";
                document.getElementById("<%= LabelResult.ClientID %>").innerHTML = "Voucher validated successfully!";
            }
            else {
                document.getElementById("<%= LabelResult.ClientID %>").style.Color = "red";
                document.getElementById("<%= LabelResult.ClientID %>").innerHTML = "Voucher validation failed!<br/>(Already validated, wrong crc, wrong SLA or non-existing)";
            }
        }
    </script> -->
    <div id="main-container">
        <div class="wrapper clearfix">
            <article>
                &nbsp;<asp:Localize ID="LocalizeHeader" runat="server" meta:resourcekey="LocalizeHeaderResource1" Text="&lt;h1&gt;Dear Customer,&lt;/h1&gt;"></asp:Localize>
                <p>
                    <br />In order to purchase a voucher, please enter your MAC address below.</a>.
                </p>
               <div class="VoucherCode">
                  <asp:Localize ID="Localize1" runat="server" meta:resourcekey="Localize1Resource1" Text="Your MAC Address"></asp:Localize>:&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp:
                    <telerik:RadMaskedTextBox ID="RadMaskedTextBoxMacAddress" runat="server" Skin="WebBlue"
                    Width="125px" Columns="17" meta:resourcekey="RadMaskedTextBoxMacAddressResource1"></telerik:RadMaskedTextBox>
                    <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Please provide a valid MAC address!"
                    ControlToValidate="RadMaskedTextBoxMacAddress" Operator="NotEqual" ValueToCompare="00:00:00:00:00:00" meta:resourcekey="CompareValidator1Resource1"></asp:CompareValidator>
                       <br /><br />
                        <telerik:RadButton ID="RadButtonSubmit" runat="server" Text="Continue" onclick="RadButtonSubmit_Click" meta:resourcekey="RadButtonSubmitResource1">
                        </telerik:RadButton><br /><br /><br />
                        <asp:Label ID="LabelResult" runat="server" meta:resourcekey="LabelResultResource1"></asp:Label>
              </div>
              <br />
              <br />
            </article>
            <br />
            <br />
        </div>
    </div>
</asp:Content>
