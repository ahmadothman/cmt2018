﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Drawing;
using System.Web.UI.WebControls;
using VOBasedWebApp.net.nimera.cmt.BOVoucherControllerWSRef;
using VOBasedWebApp.net.nimera.cmt.BOMonitorControllerWSRef;
using VOBasedWebApp.net.nimera.cmt.BOLogControllerWSRef;
using VOBasedWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using VOBasedWebApp.App_Code;
using VOBasedWebApp.controllers;
using Telerik.Web.UI;

namespace VOBasedWebApp
{
    public partial class VBAddVolume : System.Web.UI.Page
    {
        const string _HEXBYTE = "<0|1|2|3|4|5|6|7|8|9|a|b|c|d|e|f><0|1|2|3|4|5|6|7|8|9|a|b|c|d|e|f>";

        protected void Page_Load(object sender, EventArgs e)
        {
            RadMaskedTextBoxMacAddress.Mask =
                _HEXBYTE + "<:>" + _HEXBYTE + "<:>" + _HEXBYTE + "<:>" + _HEXBYTE + "<:>" + _HEXBYTE + "<:>" +
                _HEXBYTE;
            if (Session["macAddress"] != null)
            {
                RadMaskedTextBoxMacAddress.Text = Session["macAddress"].ToString();
            }
        }

        protected void RadButtonSubmit_Click(object sender, EventArgs e)
        {
            bool result = false;

            // Read text boxes
            string macAddress = "";
            string code = "";
            string crc = "";

            code = RadMaskedTextBoxCode.Text;
            crc = RadMaskedTextBoxCrc.Text;

            BOLogControlWS boLogController = new BOLogControlWS();

            try
            {
                BOAccountingControlWS boAccountingController = new BOAccountingControlWS();
                BOVoucherControllerWS boVoucherController = new BOVoucherControllerWS();

                macAddress = RadMaskedTextBoxMacAddress.Text;

                //Check if the SLA is voucher based.
                Terminal term = boAccountingController.GetTerminalDetailsByMAC(macAddress);
                ServiceLevel sl = boAccountingController.GetServicePack((int)term.SlaId);
                if (term != null)
                {
                    VoucherBatch vb = boVoucherController.GetVoucherBatch(code, crc);
                    VoucherVolume vv = boVoucherController.GetVoucherVolumeById(vb.Volume);

                    if (vb != null)
                    {
                        if (vb.DistributorId == term.DistributorId)
                        {

                            if (vb != null && vb.Release)
                            {
                                if (term != null)
                                {
                                    if (boAccountingController.IsVoucherSLA((int)term.SlaId))
                                    {
                                        Voucher v = boVoucherController.GetVoucher(code);
                                        if (v != null)
                                        {
                                            result = boVoucherController.ValidateVoucher(code, crc, macAddress);

                                            VoucherBatch vbInfo = boVoucherController.GetVoucherBatchInfo(v.BatchId);
                                            DateTime oldvoucher = new DateTime(2016, 7, 2);

                                            //if ((((result) && vbInfo.DateCreated.Date < Convert.ToDateTime("30-06-2016").Date)) ||((result) && (vbInfo.Sla == term.SlaId)))
                                            if (result)
                                            {
                                                
                                                if ((vbInfo.Sla == term.SlaId)||(sl.ServiceClass==7 && vv.ValidityPeriod==0))
                                                {



                                                    //Read the VolumeCode Id, necessary for the SES Hub or NMS from the 
                                                    //VolumesCode table
                                                    int volumeCode = vbInfo.Volume;

                                                    BOMonitorControlWS boMonitorControl = new BOMonitorControlWS();

                                                    try
                                                    {
                                                        result = boMonitorControl.AddVolume(term.SitId, volumeCode, term.IspId);

                                                        if (result)
                                                        {
                                                            //Update TerminalActivity log 
                                                            TerminalActivity terminalActivity = new TerminalActivity();
                                                            terminalActivity.Action = "Volume added with voucher";
                                                            terminalActivity.ActionDate = DateTime.Now;
                                                            terminalActivity.MacAddress = macAddress;
                                                            terminalActivity.AccountingFlag = false;
                                                            terminalActivity.SlaName = "";
                                                            terminalActivity.NewSlaName = "";
                                                            terminalActivity.TerminalActivityId = 1001;
                                                            //Setting the Voucher app user as user
                                                            string voBasedWebAppUser = System.Configuration.ConfigurationManager.AppSettings["VOBasedWebAppUser"];
                                                            terminalActivity.UserId = new Guid(voBasedWebAppUser);
                                                            boLogController.LogTerminalActivity(terminalActivity);

                                                            LabelResult.ForeColor = Color.DarkGreen;
                                                            //LabelResult.Text = "Validation succeeded!";
                                                            LabelResult.Text = (string)this.GetLocalResourceObject("LabelResultResource1.Text.Success");

                                                            
                                                            term.ExpiryDate = DateTime.UtcNow.AddDays(vv.ValidityPeriod);
                                                            boAccountingController.UpdateTerminal(term);
                                                        }
                                                        else
                                                        {
                                                            boVoucherController.ResetVoucher(code, crc);
                                                            LabelResult.ForeColor = Color.Red;
                                                            LabelResult.Text = (string)this.GetLocalResourceObject("LabelResultResource1.Text.Failed");

                                                            //We must un-validate this voucher again
                                                            boVoucherController.ResetVoucher(code, crc);
                                                        }
                                                    }
                                                    catch
                                                    {
                                                        boVoucherController.ResetVoucher(code, crc);
                                                        LabelResult.ForeColor = Color.Red;
                                                        LabelResult.Text = "An unexpected error has occured";

                                                        //We must un-validate this voucher again
                                                        boVoucherController.ResetVoucher(code, crc);
                                                    }
                                                }

                                                else
                                                {
                                                    boVoucherController.ResetVoucher(code, crc);
                                                    LabelResult.ForeColor = Color.Red;
                                                    LabelResult.Text = (string)this.GetLocalResourceObject("LabelResultResource1.Text.VoucherNotValidForMac");
                                                }
                                            }
                                            else
                                            {
                                                LabelResult.ForeColor = Color.Red;
                                                LabelResult.Text = (string)this.GetLocalResourceObject("LabelResultResource1.Text.InvalidVoucher");
                                            }
                                        }
                                        else
                                        {
                                            LabelResult.ForeColor = Color.Red;

                                            LabelResult.Text = (string)this.GetLocalResourceObject("LabelResultResource1.Text.InvalidVoucher");
                                        }
                                    }
                                    else
                                    {
                                        LabelResult.ForeColor = Color.Red;
                                        LabelResult.Text = (string)this.GetLocalResourceObject("LabelResultResource1.Text.NotVoucherSla");
                                    }
                                }
                                else
                                {
                                    LabelResult.ForeColor = Color.Red;
                                    LabelResult.Text = (string)this.GetLocalResourceObject("LabelResultResource1.Text.DoesNotExist");
                                }
                            }
                            else
                            {
                                LabelResult.ForeColor = Color.Red;
                                LabelResult.Text = (string)this.GetLocalResourceObject("LabelResultResource1.Text.VoucherNotValid");
                            }
                        }
                        else
                        {
                            LabelResult.ForeColor = Color.Red;
                            LabelResult.Text = (string)this.GetLocalResourceObject("LabelResultResource1.Text.VoucherNotValidForMac");
                        }
                    }
                    else
                    {
                        LabelResult.ForeColor = Color.Red;
                        LabelResult.Text = (string)this.GetLocalResourceObject("LabelResultResource1.Text.InvalidVoucher");
                    }
                }
                else
                {
                    LabelResult.ForeColor = Color.Red;
                    LabelResult.Text = (string)this.GetLocalResourceObject("LabelResultResource1.Text.TermDoesNotExist");
                }
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDateTime = DateTime.UtcNow;
                cmtEx.ExceptionDesc = ex.Message;
                cmtEx.ExceptionStacktrace = ex.StackTrace;
                cmtEx.ExceptionLevel = (short)ExceptionLevelEnum.Warning;
                cmtEx.UserDescription = "Validation of voucher for MAC address: " + macAddress;
                boLogController.LogApplicationException(cmtEx);
                LabelResult.ForeColor = Color.Red;
                LabelResult.Text = (string) this.GetLocalResourceObject("LabelResultResource1.Text.Failed");
            }
        }
    }
}