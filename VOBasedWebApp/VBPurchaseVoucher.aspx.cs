﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Drawing;
using Telerik.Web.UI;
using System.Web.UI.WebControls;
using VOBasedWebApp.net.nimera.cmt.BOVoucherControllerWSRef;
using VOBasedWebApp.net.nimera.cmt.BOMonitorControllerWSRef;
using VOBasedWebApp.net.nimera.cmt.BOLogControllerWSRef;
using VOBasedWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using VOBasedWebApp.controllers;
using VOBasedWebApp.App_Code;

namespace VOBasedWebApp
{
    public partial class VBPurchaseVoucher : System.Web.UI.Page
    {
        VBPaymentController _vbPaymentControl;
        BOVoucherControllerWS _boVoucherControl;
        BOMonitorControlWS _boMonitorControl;
        BOLogControlWS _boLogControl;
        BOAccountingControlWS _boAccountingControl;
        VoucherVolume[] retailPrice;
        const string _HEXBYTE = "<0|1|2|3|4|5|6|7|8|9|a|b|c|d|e|f><0|1|2|3|4|5|6|7|8|9|a|b|c|d|e|f>";
        
        protected void Page_Load(object sender, EventArgs e)
        {
            _vbPaymentControl = new VBPaymentController();
            _boAccountingControl = new BOAccountingControlWS();
            _boLogControl = new BOLogControlWS();
            _boMonitorControl = new BOMonitorControlWS();
            _boVoucherControl = new BOVoucherControllerWS();
            int distId = Convert.ToInt32(Session["distId"]);
            string macAddress = Session["macAddress"].ToString();

            //Load available voucher volumes into the combobox
            VoucherVolume[] vvList = _boVoucherControl.GetVoucherVolumes();
            retailPrice = _boVoucherControl.GetVoucherRetailPricesForDistributor(distId);
            foreach (VoucherVolume vv in vvList)
            {
                RadComboBoxItem item = new RadComboBoxItem();
                item.Value = vv.Id.ToString();
                foreach (VoucherVolume rp in retailPrice)
                {
                    if (rp.Id == vv.Id)
                    {
                        vv.UnitPriceUSD = rp.UnitPriceUSD;
                        vv.UnitPriceEUR = rp.UnitPriceEUR;
                    }
                }
                vv.UnitPriceUSD = (double)1.04 * vv.UnitPriceUSD;
                vv.UnitPriceEUR = (double)1.04 * vv.UnitPriceEUR;
                item.Text = vv.VolumeMB + " MB - Valid " + vv.ValidityPeriod + " days - " + vv.UnitPriceUSD.ToString("0.00") + "$";
                RadComboBoxVoucherVolume.Items.Add(item);
            }

            RadMaskedTextBoxMacAddress.Mask =
                _HEXBYTE + "<:>" + _HEXBYTE + "<:>" + _HEXBYTE + "<:>" + _HEXBYTE + "<:>" + _HEXBYTE + "<:>" +
                _HEXBYTE;

            RadMaskedTextBoxMacAddress.Text = macAddress;
        }

        public void ButtonPaypalCheckout_Click(object sender, EventArgs e)
        {
            string macAddress = RadMaskedTextBoxMacAddress.Text;
            try
            {
                //Get the terminal details
                Terminal term = _boAccountingControl.GetTerminalDetailsByMAC(macAddress);
                if (term != null)
                {
                    //Check if SLA is voucher based
                    if (_boAccountingControl.IsVoucherSLA((int)term.SlaId))
                    {
                        //Create the underlying voucher batch of 1 voucher
                        //Setting the Voucher app user as user
                        int batchId;
                        int distributorId = (int)term.DistributorId;
                        int voucherVolumeId = Convert.ToInt32(RadComboBoxVoucherVolume.SelectedValue);
                        Voucher[] voucher = _boVoucherControl.CreateVouchers(distributorId, 1, new Guid("A386DCF5-ACF7-46DB-92B4-F804A3F153CB"), voucherVolumeId, out batchId);
                        VoucherBatch vb = _boVoucherControl.GetVoucherBatchInfo(batchId);
                        
                        //Initiate the payment for the underlying voucher batch
                        string token = "";
                        VoucherVolume price = _boVoucherControl.GetVoucherVolumeById(voucherVolumeId);
                        foreach (VoucherVolume rp in retailPrice)
                        {
                            if (rp.Id == voucherVolumeId)
                            {
                                price = rp;
                            }
                        }
                        double amount = price.UnitPriceUSD * 1.04;
                        string url = _vbPaymentControl.CheckoutStart("paypal", batchId, distributorId, amount, macAddress, ref token);

                        Session["token"] = token;
                        Session["amount"] = amount;
                        Session["batchId"] = batchId;
                        Session["macAddress"] = macAddress;
                        Response.Redirect(url);
                    }
                    else
                    {
                        LabelResult.ForeColor = Color.Red;
                        LabelResult.Text = "Terminal does not have a voucher based SLA.";
                    }
                }
                else
                {
                    LabelResult.ForeColor = Color.Red;
                    LabelResult.Text = "Terminal does not exist.";
                }
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDateTime = DateTime.UtcNow;
                cmtEx.ExceptionDesc = ex.Message;
                cmtEx.ExceptionStacktrace = ex.StackTrace;
                cmtEx.ExceptionLevel = (short)ExceptionLevelEnum.Warning;
                cmtEx.UserDescription = "Validation of voucher for MAC address: " + macAddress + " failed";
                _boLogControl.LogApplicationException(cmtEx);
                LabelResult.ForeColor = Color.Red;
                LabelResult.Text = "Top-up failed. Please try again. If the problem persists, please contact your distributor.";
            }
        }
    }
}