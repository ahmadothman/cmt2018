﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="PaypalCheckoutReview.aspx.cs" Inherits="VOBasedWebApp.PaypalCheckoutReview" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="main-container">
        <div class="wrapper clearfix">
            <article>
                <asp:Localize ID="LocalizeHeader" runat="server" meta:resourcekey="LocalizeHeaderResource1" Text="&lt;h1&gt;Please review the details below and confirm the payment&lt;/h1&gt;"></asp:Localize>
                <div class="VoucherCode">
                    <table cellpadding="10" cellspacing="5" class="checkoutReview">
                        <tr>
                            <td>
                                <asp:Label ID="Label1" runat="server" Text="MAC address:"></asp:Label>
                            </td>
                            <td class="style1">
                                <asp:Label ID="LabelMacAddress" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label5" runat="server" Text="Amount to pay:"></asp:Label>
                            </td>
                            <td class="style1" style="white-space:nowrap;">
                                <asp:Label ID="LabelAmountToPay" runat="server" Font-Bold="True"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button ID="ButtonConfirmPayment" runat="server" Text="Confirm Payment" OnClick="ButtonConfirmPayment_Click" />
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button ID="ButtonCancel" runat="server" Text="Cancel" OnClick="ButtonCancel_Click" />
                            </td>
                            <td></td>
                        </tr>
                    </table>
              </div>
              <br />
              <br />
            </article>
            <br />
            <br />
        </div>
    </div>
</asp:Content>