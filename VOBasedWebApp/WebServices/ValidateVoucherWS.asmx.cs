﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using VOBasedWebApp.net.nimera.cmt.BOVoucherControllerWSRef;
using VOBasedWebApp.net.nimera.cmt.BOMonitorControllerWSRef;
using VOBasedWebApp.net.nimera.cmt.BOLogControllerWSRef;
using VOBasedWebApp.net.nimera.cmt.BOAccountingControlWSRef;


namespace VOBasedWebApp.WebServices
{
    /// <summary>
    /// Summary description for ValidateVoucher
    /// </summary>
    [WebService(Namespace = "http://nimera.net/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [System.Web.Script.Services.ScriptService]
    public class ValidateVoucherWS : System.Web.Services.WebService
    {
        [WebMethod]
        public Boolean ValidateVoucher(int sitId, string code, string crc, int ispId)
        {
            bool result = false;
            try
            {
                BOAccountingControlWS boAccountingController = new BOAccountingControlWS();

                //Check if the SLA is voucher based.
                Terminal term = boAccountingController.GetTerminalDetailsBySitId(sitId, ispId);

                if (term != null)
                {
                    if (boAccountingController.IsVoucherSLA((int)term.SlaId))
                    {
                        BOVoucherControllerWS boVoucherController = new BOVoucherControllerWS();
                        Voucher v = boVoucherController.GetVoucher(code);
                        if (v != null)
                        {
                            result = boVoucherController.ValidateVoucher(code, crc, "");
                            if (result)
                            {
                                VoucherBatch vb = boVoucherController.GetVoucherBatchInfo(v.BatchId);
                                BOMonitorControlWS boMonitorControl = new BOMonitorControlWS();
                                result = boMonitorControl.AddVolume(sitId, vb.Volume, term.IspId);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDesc = ex.Message;
                cmtEx.ExceptionStacktrace = ex.StackTrace;
                cmtEx.UserDescription = "Validation of voucher: " + code + " by SitId: " + sitId + " failed!";
            }

            return result;
         }
    }
}
