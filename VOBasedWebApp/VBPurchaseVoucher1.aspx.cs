﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Drawing;
using System.Web.UI.WebControls;
using VOBasedWebApp.net.nimera.cmt.BOVoucherControllerWSRef;
using VOBasedWebApp.net.nimera.cmt.BOMonitorControllerWSRef;
using VOBasedWebApp.net.nimera.cmt.BOLogControllerWSRef;
using VOBasedWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using VOBasedWebApp.App_Code;
using Telerik.Web.UI;

namespace VOBasedWebApp
{
    public partial class VBPurchaseVoucher1 : System.Web.UI.Page
    {
        const string _HEXBYTE = "<0|1|2|3|4|5|6|7|8|9|a|b|c|d|e|f><0|1|2|3|4|5|6|7|8|9|a|b|c|d|e|f>";

        protected void Page_Load(object sender, EventArgs e)
        {
            RadMaskedTextBoxMacAddress.Mask =
                _HEXBYTE + "<:>" + _HEXBYTE + "<:>" + _HEXBYTE + "<:>" + _HEXBYTE + "<:>" + _HEXBYTE + "<:>" +
                _HEXBYTE;
        }

        protected void RadButtonSubmit_Click(object sender, EventArgs e)
        {
            // Read text box
            string macAddress = "";
            
            BOLogControlWS boLogController = new BOLogControlWS();

            try
            {
                BOAccountingControlWS boAccountingController = new BOAccountingControlWS();
                BOVoucherControllerWS boVoucherController = new BOVoucherControllerWS();

                macAddress = RadMaskedTextBoxMacAddress.Text;

                //Check if the SLA is voucher based.
                Terminal term = boAccountingController.GetTerminalDetailsByMAC(macAddress);
                if (term != null)
                {
                    if (boAccountingController.IsVoucherSLA((int)term.SlaId))
                    {
                        int dist = (int)term.DistributorId;
                        Session["distId"] = dist.ToString();
                        Session["macAddress"] = macAddress;
                        Response.Redirect("VBPurchaseVoucher.aspx");
                    }
                    else
                    {
                        LabelResult.ForeColor = Color.Red;
                        LabelResult.Text = (string)this.GetLocalResourceObject("LabelResultResource1.Text.NotVoucherSla");
                    }
                }
                else
                {
                    LabelResult.ForeColor = Color.Red;
                    LabelResult.Text = (string)this.GetLocalResourceObject("LabelResultResource1.Text.DoesNotExist");
                }
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDateTime = DateTime.UtcNow;
                cmtEx.ExceptionDesc = ex.Message;
                cmtEx.ExceptionStacktrace = ex.StackTrace;
                cmtEx.ExceptionLevel = (short)ExceptionLevelEnum.Warning;
                cmtEx.UserDescription = "Validation of MAC address: " + macAddress;
                boLogController.LogApplicationException(cmtEx);
                LabelResult.ForeColor = Color.Red;
                LabelResult.Text = (string)this.GetLocalResourceObject("LabelResultResource1.Text.Failed");
            }
        }            
    }
}