﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Security;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VOBasedWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using VOBasedWebApp.net.nimera.cmt.BOVoucherControllerWSRef;
using VOBasedWebApp.net.nimera.cmt.BOMonitorControllerWSRef;
using VOBasedWebApp.net.nimera.cmt.BOLogControllerWSRef;
using VOBasedWebApp.controllers;

namespace VOBasedWebApp
{
    public partial class PaypalCheckoutComplete : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string completed = Session["userCheckoutCompleted"].ToString();
            double amount = Convert.ToDouble(Session["amount"]);
            string payerId = Session["payerId"].ToString();
            string token = Session["token"].ToString();
            int batchId = Convert.ToInt32(Session["batchId"]);
            string macAddress = Session["macAddress"].ToString();
            
            BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();
            Terminal term = boAccountingControl.GetTerminalDetailsByMAC(macAddress);
            int distributorId = (int)term.DistributorId;

            // Verify user has agreed to complete the checkout process.
            if (completed != "true")
            {
                Session["userCheckoutCompleted"] = "";
                Response.Redirect("CheckoutError.aspx?" + "Desc=Unvalidated%20Checkout.");
            }

            VBPaymentController vbPaymentControl = new VBPaymentController();

            // Carry out the final step of the Paypal checkout procedure which also releases the voucher batch
            string retUrl = vbPaymentControl.PaypalCheckoutComplete(amount, payerId, token, batchId, distributorId);
            
            // "1" means checkout was succesful 
            if (retUrl != "1")
            {
                Response.Redirect("CheckoutError.aspx?Desc=" + retUrl.Replace(" ", "%20"));
            }
            else
            {
                //Validate the voucher for the terminal
                BOVoucherControllerWS boVoucherControl = new BOVoucherControllerWS();
                Voucher[] vouchers = boVoucherControl.GetVouchers(batchId);

                if (boVoucherControl.ValidateVoucher(vouchers[0].Code, vouchers[0].Crc.ToString("X"), macAddress))
                {
                    //Add the volume to the terminal
                    //Read the VolumeCode Id, necessary for the SES Hub or NMS from the VolumesCode table
                    VoucherBatch vbInfo = boVoucherControl.GetVoucherBatchInfo(batchId);
                    VoucherVolume voucherVolume = boVoucherControl.GetVoucherVolumeById(vbInfo.Volume);

                    BOMonitorControlWS boMonitorControl = new BOMonitorControlWS();
                    if (boMonitorControl.AddVolume(term.SitId, voucherVolume.Id, term.IspId))
                    {
                        //Update TerminalActivity log 
                        TerminalActivity terminalActivity = new TerminalActivity();
                        terminalActivity.Action = "Volume added with voucher";
                        terminalActivity.ActionDate = DateTime.Now;
                        terminalActivity.MacAddress = macAddress;
                        terminalActivity.AccountingFlag = false;
                        terminalActivity.SlaName = "";
                        terminalActivity.NewSlaName = "";
                        terminalActivity.TerminalActivityId = 1001;
                        //Setting the Voucher app user as user
                        string voBasedWebAppUser = System.Configuration.ConfigurationManager.AppSettings["VOBasedWebAppUser"];
                        terminalActivity.UserId = new Guid(voBasedWebAppUser);
                        BOLogControlWS boLogControl = new BOLogControlWS();
                        boLogControl.LogTerminalActivity(terminalActivity);

                        LabelHeader.Text = "Top-up completed";
                        LabelText.Text = voucherVolume.VolumeMB + "MB of volume has been added to the terminal with MAC address " + macAddress + ". The volume is valid for " + voucherVolume.ValidityPeriod + " days.";
                    }
                    else
                    {
                        boVoucherControl.ResetVoucher(vouchers[0].Code, vouchers[0].Crc.ToString());
                        LabelHeader.ForeColor = Color.Red;
                        LabelHeader.Text = "Top-up failed. Please contact your distributor";
                    }
                }
                else
                {
                    LabelHeader.ForeColor = Color.Red;
                    LabelHeader.Text = "Top-up failed. Please contact your distributor";
                }
            }
        }
    }
}