﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="PaypalCheckoutComplete.aspx.cs" Inherits="VOBasedWebApp.PaypalCheckoutComplete" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="main-container">
        <div class="wrapper clearfix">
            <article>
                <table cellpadding="10" cellspacing="5" class="checkoutStart">
                    <tr>
                        <td class="style1">
                            <h1><asp:Label ID="LabelHeader" runat="server"></asp:Label></h1>
                        </td>
                    </tr>
                    <tr>
                        <td class="style1">
                            <asp:Label ID="LabelText" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
            </article>
            <br />
            <br />
        </div>
    </div>
</asp:Content>
