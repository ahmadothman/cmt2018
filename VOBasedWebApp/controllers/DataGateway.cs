﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using VOBasedWebApp.net.nimera.cmt.BOLogControllerWSRef;
using VOBasedWebApp.net.nimera.cmt.BOVoucherControllerWSRef;
using VOBasedWebApp.net.nimera.cmt.BOPaymentControllerWSRef;
using VOBasedWebApp.controllers;
using VOBasedWebApp.App_Code;

namespace VOBasedWebApp.controllers
{
    /// <summary>
    /// Data Gateway methods
    /// </summary>

    public class DataGateway
    {
        string _connectionString;

        public DataGateway()
        {
            //Retrieve the connection string from the Web.Config file
            _connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["PaymentServices"].ConnectionString;
        }

        /// <summary>
        /// Checks if a terminal is in the list of Avanti terminals
        /// </summary>
        /// <param name="macAddress">The MAC address of the terminal</param>
        /// <returns>True if the terminals is in the list</returns>
        public bool IsTerminalKa(string macAddress)
        {
            string queryCmd = "SELECT MacAddress FROM AvantiHardware WHERE MacAddress = @MacAddress";
            bool result;

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                con.Open();
                try
                {
                    using (SqlCommand command = new SqlCommand(queryCmd, con))
                    {
                        command.Parameters.AddWithValue("@MacAddress", macAddress);
                        SqlDataReader reader = command.ExecuteReader();

                        result = reader.HasRows;
                    }
                }
                catch (Exception ex)
                {
                    result = false;
                }
            }

            return result;
        }
    }
}