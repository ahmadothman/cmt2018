﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using VOBasedWebApp.net.nimera.cmt.BOLogControllerWSRef;
using VOBasedWebApp.net.nimera.cmt.BOVoucherControllerWSRef;
using VOBasedWebApp.net.nimera.cmt.BOPaymentControllerWSRef;
using VOBasedWebApp.controllers;
using VOBasedWebApp.App_Code;


namespace VOBasedWebApp.controllers
{
    /// <summary>
    /// Contains the methods for processing a checkout
    /// In the first implementation, a distributor can choose an unreleased batch of vouchers
    /// to pay for
    /// The payment will be processed by an external provider, PayPal in the first case
    /// Payments are stored in the CMT database
    /// </summary>
    public class VBPaymentController
    {
        private BOLogControlWS _logController;
        private BOPaymentControllerWS _paymentController;
        private VBPaypalController _paypalController;
        
        public VBPaymentController()
        {
            _logController = new BOLogControlWS();
            _paymentController = new BOPaymentControllerWS();
            _paypalController = new VBPaypalController();
        }
        
        /// <summary>
        /// Initiates the checkout procedure
        /// </summary>
        /// <param name="checkOutMethod">Determines the checkout method, in the first implemantation, only PayPal is available</param>
        /// <param name="batchId">The batch of vouchers which is being paid for</param>
        /// <param name="distributorId">The ID of the paying distributor</param>
        /// <param name="amount">The amount to be paid</param>
        /// <param name="macAddress">The MAC address of the terminal</param>
        /// <param name="token">Paypal session token</param>
        public string CheckoutStart(string checkOutMethod, int batchId, int distributorId, double amount, string macAddress, ref string token)
        {
            string retMsg = "";
            // intermediate token variable
            string tokenTemp = token;


            try
            {
                if (checkOutMethod == "paypal")
                {
                    if (amount > 0)
                    {
                        string amt = amount.ToString();

                        bool ret = _paypalController.ShortcutExpressCheckout(amt, ref tokenTemp, ref retMsg, batchId);
                        if (!ret)
                        {
                            retMsg = "CheckoutError.aspx?" + retMsg;
                        }
                    }
                    else
                    {
                        retMsg = "CheckoutError.aspx?ErrorCode=AmtMissing";
                    }
                }
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDateTime = DateTime.UtcNow;
                cmtEx.ExceptionDesc = ex.Message;
                cmtEx.ExceptionStacktrace = ex.StackTrace;
                cmtEx.ExceptionLevel = (short)ExceptionLevelEnum.Error;
                cmtEx.UserDescription = "BOPaymentController: Error while initiating new payment procedure";
                _logController.LogApplicationException(cmtEx);
            }

            token = tokenTemp;
            return retMsg;
        }

        /// <summary>
        /// Cancels the checkout procedure
        /// This method has been replaced by a redirect
        /// </summary>
        public void CheckoutCancel()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Proceeds with the user review the Paypal checkout
        /// This user still has to confirm the payment after this step
        /// </summary>
        /// <param name="token">Paypal session token</param>
        /// <param name="payerId">Paypal payer ID</param>
        /// <returns>A URL, possibly containing an error message</returns>
        public string PaypalCheckoutProceed(string token, ref string payerId)
        {
            string retMsg = "";
            string payerIdTemp = payerId;
            NVPCodec decoder = new NVPCodec();

            bool ret = _paypalController.GetCheckoutDetails(token, ref payerIdTemp, ref decoder, ref retMsg);
            if (ret)
            {
                retMsg = "PaypalCheckoutComplete.aspx";
            }
            else
            {
                retMsg = "CheckoutError.aspx?" + retMsg;
            }

            payerId = payerIdTemp;
            return retMsg;
        }

        /// <summary>
        /// Completes the Paypal checkout upon confirmation by the user
        /// </summary>
        /// <param name="amount">Final amount to be paid</param>
        /// <param name="payerId">Paypal payer ID</param>
        /// <param name="token">Paypal session token</param>
        /// <param name="batchId">The ID of the voucher batch paid for</param>
        /// <param name="distributorId">The ID of the paying distributor</param>
        /// <returns>1 if succesful, else an error message</returns>
        public string PaypalCheckoutComplete(double amount, string payerId, string token, int batchId, int distributorId)
        {
            string retMsg = "";
            string finalPaymentAmount = amount.ToString();
            NVPCodec decoder = new NVPCodec();

            bool ret = _paypalController.DoCheckoutPayment(finalPaymentAmount, token, payerId, ref decoder, ref retMsg);
            if (ret)
            {
                // Retrieve PayPal confirmation value.
                string paymentConfirmation = decoder["PAYMENTINFO_0_TRANSACTIONID"].ToString();
                
                // Store the payment in the CMT database
                if (this.StorePayment(amount, batchId, distributorId, paymentConfirmation))
                {
                    retMsg = "1";
                    // Release the Voucher Batch
                    BOVoucherControllerWS boVoucherControl = new BOVoucherControllerWS();

                    if (!boVoucherControl.ReleaseVoucherBatch(batchId))
                    {
                        retMsg = "Releasing voucher batch failed";
                    }

                }
                else
                {
                    retMsg = "Storing payment failed";
                }
            }
            else
            {
                retMsg = "CheckoutError.aspx?" + retMsg;
            }

            return retMsg;
        }

        /// <summary>
        /// Stores the payment in the CMT database
        /// </summary>
        /// <param name="amount">Amount paid</param>
        /// <param name="batchId">Batch ID paid for</param>
        /// <param name="distributorId">ID of paying distributor</param>
        /// <param name="paymentId">Unique payment ID</param>
        public bool StorePayment(double amount, int batchId, int distributorId, string paymentId)
        {
            string _connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["PaymentServices"].ConnectionString;
            
            // Write the data in the Payments table
            string insertCmd = "INSERT INTO Payments (PaymentID, DistributorID, BatchID, Amount, PaymentDateTime, AdditionalInfo) " +
                               "VALUES (@PaymentID, @DistributorID, @BatchID, @Amount, @PaymentDateTime, @AdditionalInfo)";

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(insertCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@PaymentID", paymentId);
                        cmd.Parameters.AddWithValue("@DistributorID", distributorId);
                        cmd.Parameters.AddWithValue("@BatchID", batchId);
                        cmd.Parameters.AddWithValue("@Amount", amount);
                        cmd.Parameters.AddWithValue("@PaymentDateTime", DateTime.Now);
                        cmd.Parameters.AddWithValue("@AdditionalInfo", "Paid by enduser");
                        cmd.ExecuteNonQuery();
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDateTime = DateTime.UtcNow;
                cmtEx.ExceptionDesc = ex.Message;
                cmtEx.ExceptionStacktrace = ex.StackTrace;
                cmtEx.ExceptionLevel = (short)ExceptionLevelEnum.Error;
                cmtEx.UserDescription = "VBPaymentController: Error while storing new payment. Distributor ID: " + distributorId + ", batch ID: " + batchId + ", payment ID: " + paymentId;
                _logController.LogApplicationException(cmtEx);
                return false;
            }
        }

        /// <summary>
        /// A method to check if a terminal is on the list of terminals
        /// with access to the automatic top-up page
        /// </summary>
        /// <param name="macAddress">The MAC address of the terminal</param>
        /// <returns>True if the terminal is on the list</returns>
        public bool IsTerminalOnAppList(string macAddress)
        {
            bool onList = false;
            string _connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["PaymentServices"].ConnectionString;
            string queryCmd = "SELECT MacAddress FROM VOBasedWebAppList WHERE MacAddress = @MacAddress";

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(queryCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@MacAddress", macAddress);
                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.Read())
                        {
                            onList = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDateTime = DateTime.UtcNow;
                cmtEx.ExceptionDesc = ex.Message;
                cmtEx.ExceptionStacktrace = ex.StackTrace;
                cmtEx.ExceptionLevel = (short)ExceptionLevelEnum.Warning;
                cmtEx.UserDescription = "VBPaymentController: Error while looking for terminal on VOBasedWebAppList. MacAddress: " + macAddress;
                _logController.LogApplicationException(cmtEx);
            }

            return onList;
        }
    }
}
