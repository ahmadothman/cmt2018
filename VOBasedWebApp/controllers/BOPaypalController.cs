﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Data;
using System.Configuration;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using VOBasedWebApp.net.nimera.cmt.BOLogControllerWSRef;
using VOBasedWebApp.App_Code;

namespace VOBasedWebApp.controllers
{
    /// <summary>
    /// Contains the methods needed for the Paypal checkout procedure
    /// All methods and logic come directly from Paypal, meaning there might be excessive code
    /// </summary>
    
    public class VBPaypalController
    {
        BOLogControlWS _logController;

        //Paypal credentials. These are the same variables for both live and sandbox
        //Currently test credentials are being used.
        //These credentials must be changed before going live!!
        public string APIUsername = "caroline.devos_api1.satadsl.net";
        private string APIPassword = "CWTYWCESYWLPB9MY";
        private string APISignature = "AyEZggmquJ23WMV-mzp8.JSMSAcnA3YcRdiNDkngokuspXNbKjn6dr26";
        //public string APIUsername = "daniel-facilitator_api1.samecar.eu";
        //private string APIPassword = "1369657121";
        //private string APISignature = "AdnIvZs2qPv7s5nMnawGA-hkp0SsATkkzveOygGEywRoZ8cz5ezZvYiA";
        
        public VBPaypalController()
        {
            _logController = new BOLogControlWS();
        }

        //Flag that determines the PayPal environment (live or sandbox)
        private const bool bSandbox = false;
        private const bool staging = false;
        private const string CVV2 = "CVV2";

        // Live strings.
        private string pEndPointURL = "https://api-3t.paypal.com/nvp";
        private string host = "www.paypal.com";

        // Sandbox strings.
        private string pEndPointURL_SB = "https://api-3t.sandbox.paypal.com/nvp";
        private string host_SB = "www.sandbox.paypal.com";

        private const string SIGNATURE = "SIGNATURE";
        private const string PWD = "PWD";
        private const string ACCT = "ACCT";

        private string Subject = "";
        private string BNCode = "PP-ECWizard";


        //HttpWebRequest Timeout specified in milliseconds 
        private const int Timeout = 15000;
        private static readonly string[] SECURED_NVPS = new string[] { ACCT, CVV2, SIGNATURE, PWD };

        public void SetCredentials(string Userid, string Pwd, string Signature)
        {
            APIUsername = Userid;
            APIPassword = Pwd;
            APISignature = Signature;
        }

        /// <summary>
        /// This is the first step of the Paypal checkout procedure
        /// It is initiated by the user
        /// </summary>
        /// <param name="amt">The total amount to be paid</param>
        /// <param name="token">A variable token, which is set by the method as the Paypal identifier of the current payment procedure</param>
        /// <param name="retMsg">A URL, possibly containing an error message</param>
        /// <param name="batchId">Payment reference from the CMT</param>
        /// <returns>A boolean, true if the payment procedure was succesful so far, otherwise false</returns>
        public bool ShortcutExpressCheckout(string amt, ref string token, ref string retMsg, int batchId)
        {
            if (bSandbox)
            {
                pEndPointURL = pEndPointURL_SB;
                host = host_SB;
            }

            //URL in the VOBasedWebApp where the next step of Paypal procedure takes place
            string returnURL = "http://cmt.satadsl.net/cmtvoucher/PaypalCheckoutReview.aspx?batchId=" + batchId;
            //URL on the CMT-server in case the user decides to cancel the checkout procedure
            string cancelURL = "http://cmt.satadsl.net/cmtvoucher/CheckoutCancel.aspx";
            //Alternative URLs for the staging server
            if (staging)
            {
                returnURL = "http://satadsl2.nimera.net:8085/cmtvoucher/PaypalCheckoutReview.aspx?batchId=" + batchId;
                cancelURL = "http://satadsl2.nimera.net:8085/cmtvoucher/CheckoutCancel.aspx";
            }
            
            NVPCodec encoder = new NVPCodec();
            encoder["METHOD"] = "SetExpressCheckout";
            encoder["RETURNURL"] = returnURL;
            encoder["CANCELURL"] = cancelURL;
            //The brandname is shown on the Paypal page
            encoder["BRANDNAME"] = "SatADSL CMT";
            //This section will have to be changed in case payments are accepted for multiple items at once
            encoder["PAYMENTREQUEST_0_AMT"] = amt;
            encoder["PAYMENTREQUEST_0_ITEMAMT"] = amt;
            encoder["PAYMENTREQUEST_0_PAYMENTACTION"] = "Sale";
            encoder["PAYMENTREQUEST_0_CURRENCYCODE"] = "USD";
            //This part could be put into a for loop when processing multiple items
            encoder["L_PAYMENTREQUEST_0_NAME0"] = "Top-up for terminal";
            encoder["L_PAYMENTREQUEST_0_AMT0"] = amt;
            encoder["L_PAYMENTREQUEST_0_QTY0"] = "1";

            string pStrrequestforNvp = encoder.Encode();
            string pStresponsenvp = HttpCall(pStrrequestforNvp);

            NVPCodec decoder = new NVPCodec();
            decoder.Decode(pStresponsenvp);

            string strAck = decoder["ACK"].ToLower();
            if (strAck != null && (strAck == "success" || strAck == "successwithwarning"))
            {
                token = decoder["TOKEN"];
                string ECURL = "https://" + host + "/cgi-bin/webscr?cmd=_express-checkout" + "&token=" + token;
                retMsg = ECURL;
                return true;
            }
            else
            {
                retMsg = "ErrorCode=" + decoder["L_ERRORCODE0"] + "&" +
                    "Desc=" + decoder["L_SHORTMESSAGE0"] + "&" +
                    "Desc2=" + decoder["L_LONGMESSAGE0"];
                return false;
            }
        }
        /// <summary>
        /// This is the second part of the Paypal checkout procedure
        /// This method is called after the first step has re-directed the user to the second page of the procedure within the CMT
        /// On that page, the user will be asked by the CMT to confirm the payment details
        /// </summary>
        /// <param name="token">A variable token, which is set by the method as the Paypal identifier of the current payment procedure</param>
        /// <param name="PayerID">A payer ID as set by Paypal for the current payment procedure</param>
        /// <param name="decoder">A decoder containg payment details</param>
        /// <param name="retMsg">A URL containing an error message, if applicable</param>
        /// <returns>A boolean, true if the payment procedure was succesful so far, otherwise false</returns>
        public bool GetCheckoutDetails(string token, ref string PayerID, ref NVPCodec decoder, ref string retMsg)
        {
            if (bSandbox)
            {
                pEndPointURL = pEndPointURL_SB;
            }

            NVPCodec encoder = new NVPCodec();
            encoder["METHOD"] = "GetExpressCheckoutDetails";
            encoder["TOKEN"] = token;

            string pStrrequestforNvp = encoder.Encode();
            string pStresponsenvp = HttpCall(pStrrequestforNvp);

            decoder = new NVPCodec();
            decoder.Decode(pStresponsenvp);

            string strAck = decoder["ACK"].ToLower();
            if (strAck != null && (strAck == "success" || strAck == "successwithwarning"))
            {
                PayerID = decoder["PAYERID"];
                return true;
            }
            else
            {
                retMsg = "ErrorCode=" + decoder["L_ERRORCODE0"] + "&" +
                    "Desc=" + decoder["L_SHORTMESSAGE0"] + "&" +
                    "Desc2=" + decoder["L_LONGMESSAGE0"];

                return false;
            }
        }
        /// <summary>
        /// The final step in the Paypal checkout procedure
        /// The method is initiated upon user confirmation
        /// </summary>
        /// <param name="finalPaymentAmount">The final amount to be paid</param>
        /// <param name="token">A variable token, which is set by the method as the Paypal identifier of the current payment procedure</param>
        /// <param name="PayerID">A payer ID as set by Paypal for the current payment procedure</param>
        /// <param name="decoder">A decoder containg payment details</param>
        /// <param name="retMsg">A URL containing an error message, if applicable</param>
        /// <returns>A boolean, true if the payment procedure was succesful, otherwise false</returns>
        public bool DoCheckoutPayment(string finalPaymentAmount, string token, string PayerID, ref NVPCodec decoder, ref string retMsg)
        {
            if (bSandbox)
            {
                pEndPointURL = pEndPointURL_SB;
            }

            NVPCodec encoder = new NVPCodec();
            encoder["METHOD"] = "DoExpressCheckoutPayment";
            encoder["TOKEN"] = token;
            encoder["PAYERID"] = PayerID;
            encoder["PAYMENTREQUEST_0_AMT"] = finalPaymentAmount;
            encoder["PAYMENTREQUEST_0_CURRENCYCODE"] = "USD";
            encoder["PAYMENTREQUEST_0_PAYMENTACTION"] = "Sale";

            string pStrrequestforNvp = encoder.Encode();
            string pStresponsenvp = HttpCall(pStrrequestforNvp);

            decoder = new NVPCodec();
            decoder.Decode(pStresponsenvp);

            string strAck = decoder["ACK"].ToLower();
            if (strAck != null && (strAck == "success" || strAck == "successwithwarning"))
            {
                return true;
            }
            else
            {
                retMsg = "ErrorCode=" + decoder["L_ERRORCODE0"] + "&" +
                    "Desc=" + decoder["L_SHORTMESSAGE0"] + "&" +
                    "Desc2=" + decoder["L_LONGMESSAGE0"];

                return false;
            }
        }

        /// <summary>
        /// Method from Paypal
        /// </summary>
        public string HttpCall(string NvpRequest)
        {
            string url = pEndPointURL;

            string strPost = NvpRequest + "&" + buildCredentialsNVPString();
            strPost = strPost + "&BUTTONSOURCE=" + HttpUtility.UrlEncode(BNCode);

            HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(url);
            objRequest.Timeout = Timeout;
            objRequest.Method = "POST";
            objRequest.ContentLength = strPost.Length;

            try
            {
                using (StreamWriter myWriter = new StreamWriter(objRequest.GetRequestStream()))
                {
                    myWriter.Write(strPost);
                }
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDateTime = DateTime.UtcNow;
                cmtEx.ExceptionDesc = ex.Message;
                cmtEx.ExceptionStacktrace = ex.StackTrace;
                cmtEx.ExceptionLevel = (short)ExceptionLevelEnum.Info;
                cmtEx.UserDescription = "VBPaypalController: Error while processing Paypal payment"; 
            }

            //Retrieve the Response returned from the NVP API call to PayPal.
            HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
            string result;
            using (StreamReader sr = new StreamReader(objResponse.GetResponseStream()))
            {
                result = sr.ReadToEnd();
            }

            return result;
        }

        /// <summary>
        /// Method from Paypal
        /// </summary>
        public string buildCredentialsNVPString()
        {
            NVPCodec codec = new NVPCodec();

            if (!IsEmpty(APIUsername))
                codec["USER"] = APIUsername;

            if (!IsEmpty(APIPassword))
                codec[PWD] = APIPassword;

            if (!IsEmpty(APISignature))
                codec[SIGNATURE] = APISignature;

            if (!IsEmpty(Subject))
                codec["SUBJECT"] = Subject;

            codec["VERSION"] = "88.0";

            return codec.Encode();
        }

        public bool IsEmpty(string s)
        {
            return s == null || s.Trim() == string.Empty;
        }
    }

    /// <summary>
    /// Method from Paypal
    /// </summary>
    public sealed class NVPCodec : NameValueCollection
    {
        private const string AMPERSAND = "&";
        private const string EQUALS = "=";
        private static readonly char[] AMPERSAND_CHAR_ARRAY = AMPERSAND.ToCharArray();
        private static readonly char[] EQUALS_CHAR_ARRAY = EQUALS.ToCharArray();

        public string Encode()
        {
            StringBuilder sb = new StringBuilder();
            bool firstPair = true;
            foreach (string kv in AllKeys)
            {
                string name = HttpUtility.UrlEncode(kv);
                string value = HttpUtility.UrlEncode(this[kv]);
                if (!firstPair)
                {
                    sb.Append(AMPERSAND);
                }
                sb.Append(name).Append(EQUALS).Append(value);
                firstPair = false;
            }
            return sb.ToString();
        }

        public void Decode(string nvpstring)
        {
            Clear();
            foreach (string nvp in nvpstring.Split(AMPERSAND_CHAR_ARRAY))
            {
                string[] tokens = nvp.Split(EQUALS_CHAR_ARRAY);
                if (tokens.Length >= 2)
                {
                    string name = HttpUtility.UrlDecode(tokens[0]);
                    string value = HttpUtility.UrlDecode(tokens[1]);
                    Add(name, value);
                }
            }
        }

        public void Add(string name, string value, int index)
        {
            this.Add(GetArrayName(index, name), value);
        }

        public void Remove(string arrayName, int index)
        {
            this.Remove(GetArrayName(index, arrayName));
        }

        public string this[string name, int index]
        {
            get
            {
                return this[GetArrayName(index, name)];
            }
            set
            {
                this[GetArrayName(index, name)] = value;
            }
        }

        private static string GetArrayName(int index, string name)
        {
            if (index < 0)
            {
                throw new ArgumentOutOfRangeException("index", "index cannot be negative : " + index);
            }
            return name + index;
        }
    }
}
