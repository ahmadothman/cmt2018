﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="VBAddVolume.aspx.cs" Inherits="VOBasedWebApp.VBAddVolume" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <!-- <script type="text/javascript">
        function ValidateVouchersButtonClicked() {
            document.getElementById("<%= LabelResult.ClientID %>").style.display = "";

            var macTextBox = $find("<%= RadMaskedTextBoxMacAddress.ClientID  %>");
            var macAddress = macTextBox.get_textBoxValue();

            var codeTextBox = $find("<%= RadMaskedTextBoxCode.ClientID %>");
            var code = codeTextBox.get_textBoxValue();

            var crcTextBox = $find("<%= RadMaskedTextBoxCrc.ClientID %>");
            var crc = crcTextBox.get_textBoxValue();

            //Call the webservice
            VOBasedWebApp.WebServices.ValidateVoucherWS.ValidateVoucher(macAddress, code, crc, OnValidateVoucherComplete);
        }

        function OnValidateVoucherComplete(result) {
            if (result) {
                document.getElementById("<%= LabelResult.ClientID %>").style.Color = "green";
                document.getElementById("<%= LabelResult.ClientID %>").innerHTML = "Voucher validated successfully!";
            }
            else {
                document.getElementById("<%= LabelResult.ClientID %>").style.Color = "red";
                document.getElementById("<%= LabelResult.ClientID %>").innerHTML = "Voucher validation failed!<br/>(Already validated, wrong crc, wrong SLA or non-existing)";
            }
        }
    </script> -->
    <div id="main-container">
        <div class="wrapper clearfix">
            <article>
                <h1><asp:Localize ID="LocalizeHeader" runat="server" meta:resourcekey="LocalizeHeaderResource1" Text="DearCustomer,"></asp:Localize></h1>
                <p>
                    <asp:Localize ID="LocalizeMessage" runat="server" meta:resourcekey="LocalizeMessageResource1" Text="Your volume allocation is exhausted. Use your voucher to add volume.&lt;br /&gt;
                    Please allow few minutes for the volume to be added to your accound."></asp:Localize><br />
               </p>
               <div class="VoucherCode">
                  <asp:Localize ID="Localize1" runat="server" meta:resourcekey="Localize1Resource1" Text="Your MAC Address"></asp:Localize>:&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    <telerik:RadMaskedTextBox ID="RadMaskedTextBoxMacAddress" runat="server" Skin="WebBlue"
                    Width="125px" Columns="17" meta:resourcekey="RadMaskedTextBoxMacAddressResource1"></telerik:RadMaskedTextBox>
                    <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Please provide a valid MAC address!"
                    ControlToValidate="RadMaskedTextBoxMacAddress" Operator="NotEqual" ValueToCompare="00:00:00:00:00:00" meta:resourcekey="CompareValidator1Resource1"></asp:CompareValidator>
                        <br /><br/>
                  <asp:Localize ID="Localize2" runat="server" meta:resourcekey="Localize2Resource1" Text="Voucher"></asp:Localize>:
                        <telerik:RadMaskedTextBox ID="RadMaskedTextBoxCode" runat="server" Columns="13" Mask="#############"
                            Rows="1" Skin="WebBlue" Font-Bold="True" ForeColor="#0066FF" Width="130px" EmptyMessage="Code" Height="20px" TabIndex="2" meta:resourcekey="RadMaskedTextBoxCodeResource1">
                        </telerik:RadMaskedTextBox>
                        -
                        <telerik:RadMaskedTextBox ID="RadMaskedTextBoxCrc" runat="server" EmptyMessage="Crc"
                            Font-Bold="True" ForeColor="#0066FF" Mask="aaaa" Rows="1" Skin="WebBlue" Width="50px" Height="20px" TabIndex="3" meta:resourcekey="RadMaskedTextBoxCrcResource1">
                        </telerik:RadMaskedTextBox>
                   <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                       ErrorMessage="Please specify your Voucher code!" 
                       ControlToValidate="RadMaskedTextBoxCode" meta:resourcekey="RequiredFieldValidator2Resource1"></asp:RequiredFieldValidator>
                       <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                       ErrorMessage="You must specify your complete code!" 
                       ControlToValidate="RadMaskedTextBoxCrc" meta:resourcekey="RequiredFieldValidator3Resource1"></asp:RequiredFieldValidator>
                       <br /><br />
                        <telerik:RadButton ID="RadButtonSubmit" runat="server" Text="Validate" ButtonType="StandardButton" onclick="RadButtonSubmit_Click" OnClientClick="if (Page_IsValid){ this.value = 'Validating...'; this.disabled = true;} " meta:resourcekey="RadButtonSubmitResource1">
                        </telerik:RadButton><br /><br /><br />
                        <asp:Label ID="LabelResult" runat="server" meta:resourcekey="LabelResultResource1"></asp:Label>
              </div>
              <br />
              <br />
            </article>
            <br />
            <br />
        </div>
    </div>
</asp:Content>
