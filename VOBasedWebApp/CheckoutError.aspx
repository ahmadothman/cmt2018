﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="CheckoutError.aspx.cs" Inherits="VOBasedWebApp.CheckoutError" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="main-container">
        <div class="wrapper clearfix">
            <article>
                <asp:Localize ID="LocalizeHeader" runat="server" meta:resourcekey="LocalizeHeaderResource1" Text="&lt;h1&gt;Top-up error&lt;/h1&gt;"></asp:Localize>
                <table id="ErrorTable">
		            <tr>
			            <td class="field"></td>
			            <td><%=Request.QueryString.Get("ErrorCode")%></td>
		            </tr>
		            <tr>
			            <td class="field"></td>
			            <td><%=Request.QueryString.Get("Desc")%></td>
		            </tr>
		            <tr>
			            <td class="field"></td>
			            <td><%=Request.QueryString.Get("Desc2")%></td>
		            </tr>
	            </table>
            </article>
            <br />
            <br />
        </div>
    </div>
</asp:Content>
