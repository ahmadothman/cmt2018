﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using Telerik.Web.UI;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Globalization;
using VOBasedWebApp.controllers;


namespace VOBasedWebApp
{
    public partial class PaypalCheckoutReview : System.Web.UI.Page
    {
        public string token = "";
        public int batchId = 0;
        public double amount = 0;
        public string macAddress = "";
        
        protected void Page_Load(object sender, EventArgs e)
        {
            batchId = Convert.ToInt32(Session["batchId"]);
            amount = Convert.ToDouble(Session["amount"]);
            token = Session["token"].ToString();
            macAddress = "00:06:39:86:12:80";// Session["macAddress"].ToString();
            LabelMacAddress.Text = macAddress;
            LabelAmountToPay.Text = amount + " USD";
        }

        public void ButtonConfirmPayment_Click(object sender, EventArgs e)
        {
            VBPaymentController vbPaymentControl = new VBPaymentController();
            string payerId = "";
            string url = vbPaymentControl.PaypalCheckoutProceed(token, ref payerId);

            Session["userCheckoutCompleted"] = "true"; // User has comfirmed the completion of the payment
            Session["payerId"] = payerId;
            Response.Redirect(url);
        }

        public void ButtonCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("CheckoutCancel.aspx");
        }
    }
}