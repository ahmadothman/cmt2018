﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="CheckoutCancel.aspx.cs" Inherits="VOBasedWebApp.CheckoutCancel" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="main-container">
        <div class="wrapper clearfix">
            <article>
                <asp:Localize ID="LocalizeHeader" runat="server" meta:resourcekey="LocalizeHeaderResource1" Text="&lt;h1&gt;Top-up cancelled&lt;/h1&gt;"></asp:Localize>
                <p>
                    <asp:Localize ID="LocalizeMessage" runat="server" meta:resourcekey="LocalizeMessageResource1" Text="The top-up was cancelled by the user."></asp:Localize>
                </p>
            </article>
            <br />
            <br />
        </div>
    </div>
</asp:Content>
