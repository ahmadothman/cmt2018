﻿<%@ Page Language="C#" AutoEventWireup="true"  MasterPageFile="~/Site.Master" CodeBehind="VBPurchaseVoucher.aspx.cs" Inherits="VOBasedWebApp.VBPurchaseVoucher" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="main-container">
        <div class="wrapper clearfix">
            <article>
                <asp:Localize ID="LocalizeHeader" runat="server" meta:resourcekey="LocalizeHeaderResource2" Text="&lt;h1&gt;Purchase top-up&lt;/h1&gt;"></asp:Localize>
                <p>
                    <asp:Localize ID="LocalizeMessage" runat="server" meta:resourcekey="LocalizeMessageResource5" Text="Here you can purchase a top-up for your terminal. Enter the MAC address of the terminal and select your desired volume from the list. Finally select your payment method in order to complete the transaction."></asp:Localize>
                </p>
               <div class="VoucherCode">
                    <table>
                        <tr>
                            <td>
                                <asp:Localize ID="Localize1" runat="server" meta:resourcekey="Localize1Resource1" Text="MAC Address:"></asp:Localize>
                            </td>
                            <td>
                                <telerik:RadMaskedTextBox ID="RadMaskedTextBoxMacAddress" runat="server" Skin="WebBlue"
                                    Width="125px" Columns="17" meta:resourcekey="RadMaskedTextBoxMacAddressResource1">
                                </telerik:RadMaskedTextBox>
                                <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Please provide a valid MAC address!"
                                    ControlToValidate="RadMaskedTextBoxMacAddress" Operator="NotEqual" ValueToCompare="00:00:00:00:00:00" meta:resourcekey="CompareValidator1Resource1">
                                </asp:CompareValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Localize ID="Localize2" runat="server" meta:resourcekey="Localize2Resource1" Text="Type of volume:"></asp:Localize>
                            </td>
                            <td>
                                <telerik:RadComboBox ID="RadComboBoxVoucherVolume" runat="server" Width="300"></telerik:RadComboBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Payment method:
                            </td>
                            <td>
                                <asp:ImageButton ID="ButtonPaypalCheckout" runat="server" 
                                  ImageUrl="https://www.paypal.com/en_US/i/btn/btn_xpressCheckout.gif" 
                                   AlternateText="Pay with PayPal" ToolTip="Pay with PayPal"
                                  OnClick="ButtonPaypalCheckout_Click"  
                                  BackColor="Transparent" BorderWidth="0" />
                            </td>
                            <%--<td>
                                Mobile payment:<br /><asp:Image ID="ImageQR" runat="server" ImageUrl="Images/qrcode_satadsl.net.jpeg" Width="80" Tooltip="Mobile payment"/>
                            </td>--%>
                            
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:Label ID="LabelResult" runat="server" meta:resourcekey="LabelResultResource1"></asp:Label>
                            </td>
                        </tr>
                    </table>
              </div>
              <br />
              <br />
            </article>
            <br />
            <br />
        </div>
    </div>
</asp:Content>
