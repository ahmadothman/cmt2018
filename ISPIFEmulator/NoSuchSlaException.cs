﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace net.nimera.cmt.ISPIFEmulator
{
    [Serializable]
    public class NoSuchSlaException : System.Exception
    {
        public NoSuchSlaException()
        {
        }

        public NoSuchSlaException(string message)
            : base(message)
        {
        }

        public NoSuchSlaException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected NoSuchSlaException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}