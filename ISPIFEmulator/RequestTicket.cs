﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace net.nimera.cmt.ISPIFEmulator
{
    /// <summary>
    /// A Request Ticket is returned immediately by a successfully queued asynchronous operation. 
    /// This provides a handle to status of the requested operation. The following operations are asynchronous,
    /// meaning they will take a non-negligible amount of time to complete. 
    /// Thus they all return a Request Ticket on successful queuing. 
    /// If queuing was not successful a relevant exception fault will be returned immediately.
    /// </summary>
    public class RequestTicket
    {
        public string id { set; get; }
        public string failureReason { set; get; }
        public RequestStatus? requestStatus { set; get; }
        public string failureStackTrace { set; get; }

    }
}