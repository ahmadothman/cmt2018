﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace net.nimera.cmt.ISPIFEmulator
{
    [Serializable]
    public class SameTerminalException : System.Exception
    {
        public SameTerminalException()
        {
        }

        public SameTerminalException(string message)
            : base(message)
        {
        }

        public SameTerminalException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected SameTerminalException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}