﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;

namespace net.nimera.cmt.ISPIFEmulator
{
    /// <summary>
    /// Data Gateway methods
    /// </summary>
    public class DataGateway
    {
        string _connectionString;

        public DataGateway()
        {
            //Retrieve the connection string from the Web.Config file
            _connectionString = ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
        }

        /// <summary>
        /// Returns true if the specified user exists
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public bool userExists(string userId)
        {
            string queryCmd = "SELECT Id FROM EndUsers WHERE Id = @UserId";
            bool result;

            Logger.log("Caller userExists with userId: " + userId);

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                con.Open();
                try
                {
                    using (SqlCommand command = new SqlCommand(queryCmd, con))
                    {
                        command.Parameters.AddWithValue("@UserId", userId);
                        SqlDataReader reader = command.ExecuteReader();

                        result = reader.HasRows;
                    }
                }
                catch (Exception ex)
                {
                    //TO DO: Send an error message to the log file
                    result = false;
                    Logger.log(ex.Message);
                }
            }

            return result;
        }

        /// <summary>
        /// Returns true if the terminal exists
        /// </summary>
        /// <param name="macAddress"></param>
        /// <returns></returns>
        public bool terminalExists(string macAddress)
        {
            string queryCmd = "SELECT count(*) FROM Terminals WHERE MacAddress = @macAddress";
            bool result;

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                con.Open();
                try
                {
                    using (SqlCommand command = new SqlCommand(queryCmd, con))
                    {
                        command.Parameters.AddWithValue("@macAddress", macAddress);
                        SqlDataReader reader = command.ExecuteReader();

                        if (reader.Read())
                        {
                            if (reader.GetInt32(0) == 1)
                            {
                                result = true;
                            }
                            else
                            {
                                result = false;
                            }
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
                catch (Exception ex)
                {
                    //TO DO: Send an error message to the log file
                    result = false;
                    Logger.log(ex.Message);
                }
            }

            return result;
        }

        public bool slaExists(int slaId)
        {
            string queryCmd = "SELECT count(*) FROM ServicePacks WHERE SlaID = @SlaId";
            bool result;

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                con.Open();
                try
                {
                    using (SqlCommand command = new SqlCommand(queryCmd, con))
                    {
                        command.Parameters.AddWithValue("@SlaId", slaId);
                        SqlDataReader reader = command.ExecuteReader();

                        result = reader.HasRows;
                    }
                }
                catch (Exception ex)
                {
                    //TO DO: Send an error message to the log file
                    result = false;
                    Logger.log(ex.Message);
                }
            }

            return result;
        }

        /// <summary>
        /// Returns true if the user is registerd (linked to a terminal)
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public bool isUserRegistered(string userId)
        {
            string queryCmd = "SELECT MacAddress FROM EndUsers WHERE Id = @UserId";
            bool result = false;

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                con.Open();
                try
                {
                    using (SqlCommand command = new SqlCommand(queryCmd, con))
                    {
                        command.Parameters.AddWithValue("@UserId", userId);
                        SqlDataReader reader = command.ExecuteReader();

                        if (reader.Read())
                        {
                            string macAddress = reader.GetString(0).Trim();
                            if (macAddress != null && !macAddress.Equals(""))
                            {
                                result = true;
                            }
                            else
                            {
                                result = false;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    //TO DO: Send an error message to the log file
                    result = false;
                    Logger.log(ex.Message);
                }
            }

            return result;
        }

        /// <summary>
        /// Returns true if the user is indeed registered for the given terminal
        /// </summary>
        /// <param name="macAddress"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public bool isUserRegisteredForTerminal(string macAddress, string userId)
        {
            string queryCmd = "SELECT Count(*) FROM EndUsers WHERE Id = @UserId AND MacAddress = @macAddress";
            bool result = false;

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                con.Open();
                try
                {
                    using (SqlCommand command = new SqlCommand(queryCmd, con))
                    {
                        command.Parameters.AddWithValue("@UserId", userId);
                        command.Parameters.AddWithValue("@macAddress", macAddress);
                        SqlDataReader reader = command.ExecuteReader();

                        result = reader.HasRows;
                    }
                }
                catch (Exception ex)
                {
                    //TO DO: Send an error message to the log file
                    result = false;
                    Logger.log(ex.Message);
                }
            }

            return result;
        }

        /// <summary>
        /// Checks if the terminal has a pending request
        /// </summary>
        /// <param name="macAddress"></param>
        /// <returns></returns>
        public bool hasPendingRequest(string macAddress)
        {
            string queryCmd = "SELECT Status FROM Tickets WHERE MacAddress = @macAddress";
            bool result = false;

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                con.Open();
                try
                {
                    using (SqlCommand command = new SqlCommand(queryCmd, con))
                    {
                        command.Parameters.AddWithValue("@macAddress", macAddress);
                        SqlDataReader reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            int status = reader.GetInt32(0);
                            if (status == (int)RequestStatus.BUSY)
                            {
                                result = true;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    //TO DO: Send an error message to the log file
                    result = false;
                    Logger.log(ex.Message);
                }
            }

            return result;
        }

        /// <summary>
        /// Checks if the terminal is in use 
        /// </summary>
        /// <param name="macAddress"></param>
        /// <returns></returns>
        public bool isTerminalInUse(string macAddress)
        {
            string queryCmd = "SELECT MacAddress FROM Terminals WHERE MacAddress = @macAddress AND State = 1";
            bool result;

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                con.Open();
                try
                {
                    using (SqlCommand command = new SqlCommand(queryCmd, con))
                    {
                        command.Parameters.AddWithValue("@macAddress", macAddress);
                        SqlDataReader reader = command.ExecuteReader();

                        result = reader.HasRows;
                    }
                }
                catch (Exception ex)
                {
                    //TO DO: Send an error message to the log file
                    result = false;
                    Logger.log(ex.Message);
                }
            }

            return result;
        }

        public bool isRegistrationOnGoing(string macAddress)
        {
            string queryCmd = "SELECT Status FROM Tickets WHERE MacAddress = @macAddress AND Type = @Type";
            bool result = false;

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                con.Open();
                try
                {
                    using (SqlCommand command = new SqlCommand(queryCmd, con))
                    {
                        command.Parameters.AddWithValue("@macAddress", macAddress);
                        command.Parameters.AddWithValue("@Type", (int)TypeEnum.Registration);
                        SqlDataReader reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            int status = reader.GetInt32(0);
                            if (status == (int)RequestStatus.BUSY)
                            {
                                result = true;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    //TO DO: Send an error message to the log file
                    result = false;
                    Logger.log(ex.Message);
                }
            }

            return result;
        }

        /// <summary>
        /// Registers a user
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="macAddress"></param>
        /// <returns></returns>
        public bool registerUser(string userId, string macAddress, long slaId)
        {
            string updateCmd = "UPDATE EndUsers SET MacAddress = @MacAddress, SlaId = @SlaId WHERE Id = @UserId";

            bool result = false;

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                con.Open();
                try
                {
                    using (SqlCommand command = new SqlCommand(updateCmd, con))
                    {
                        command.Parameters.AddWithValue("@MacAddress", macAddress);
                        command.Parameters.AddWithValue("@UserId", userId);
                        command.Parameters.AddWithValue("@SlaId", slaId);
                        int numRows = command.ExecuteNonQuery();

                        if (numRows == 1)
                        {
                            result = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    //TO DO: Send an error message to the log file
                    result = false;
                    Logger.log(ex.Message);
                }
            }

            return result;
        }

        /// <summary>
        /// Creates a user
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public bool createUser(string userId, int Isp)
        {
            string insertCmd = "INSERT INTO EndUsers (Id, Isp) VALUES (@UserId, @Isp)";

            bool result = false;

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                con.Open();
                try
                {
                    using (SqlCommand command = new SqlCommand(insertCmd, con))
                    {
                        command.Parameters.AddWithValue("@UserId", userId);
                        command.Parameters.AddWithValue("@Isp", Isp);
                        int numRows = command.ExecuteNonQuery();

                        if (numRows == 1)
                        {
                            result = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    //TO DO: Send an error message to the log file
                    result = false;
                    Logger.log(ex.Message);
                }
            }

            return result;
        }

        /// <summary>
        /// Creates a user
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public bool createUserWithSitId(string userId, int Isp, int sitId)
        {
            string insertCmd = "INSERT INTO EndUsers (Id, Isp, SitId) VALUES (@UserId, @Isp, @SitId)";

            bool result = false;

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                con.Open();
                try
                {
                    using (SqlCommand command = new SqlCommand(insertCmd, con))
                    {
                        command.Parameters.AddWithValue("@UserId", userId);
                        command.Parameters.AddWithValue("@Isp", Isp);
                        command.Parameters.AddWithValue("@SitId", sitId);
                        int numRows = command.ExecuteNonQuery();

                        if (numRows == 1)
                        {
                            result = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    //TO DO: Send an error message to the log file
                    result = false;
                    Logger.log(ex.Message);
                }
            }

            return result;
        }

        /// <summary>
        /// Generates a new ticket. The ticket is inserted into the Tickets table.
        /// </summary>
        /// <param name="rt"></param>
        /// <param name="macAddress"></param>
        public bool generateTicket(ref RequestTicket rt, string macAddress, TypeEnum type)
        {
            string insertCmd = "INSERT INTO Tickets (TicketId, DateTimeCreated, MacAddress, Status, FailureReason, FailureStackTrace, Type) " +
                                    "VALUES (@TicketId, GetDate(), @MacAddress, @TicketStatus, '', '', @Type)";

            rt.requestStatus = RequestStatus.BUSY;
            bool result = false;

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                con.Open();

                try
                {
                    using (SqlCommand command = new SqlCommand(insertCmd, con))
                    {
                        command.Parameters.AddWithValue("@MacAddress", macAddress);
                        command.Parameters.AddWithValue("@TicketId", rt.id);
                        command.Parameters.AddWithValue("@TicketStatus", (int)rt.requestStatus);
                        command.Parameters.AddWithValue("@Type", (int)type);
                        int numRows = command.ExecuteNonQuery();

                        if (numRows == 1)
                        {
                            result = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    rt.requestStatus = RequestStatus.FAILED;
                    rt.failureReason = ex.Message;
                    if (ex.InnerException != null)
                    {
                        rt.failureStackTrace = ex.InnerException.ToString();
                    }
                    result = false;
                    Logger.log(ex.Message);
                }
            }

            return result;
        }

        /// <summary>
        /// Inserts a temrinal into the Terminal table. This method is mainly used by the
        /// unit tests
        /// </summary>
        /// <param name="macAddress"></param>
        /// <returns></returns>
        public bool insertTerminal(string macAddress)
        {
            return this.insertTerminal(macAddress, RegistrationStatus.OPERATIONAL);
        }

        /// <summary>
        /// Inserts a temrinal into the Terminal table. This method is mainly used by the
        /// unit tests and while executing a direct registration
        /// </summary>
        /// <param name="macAddress"></param>
        /// <returns></returns>
        public bool insertTerminal(string macAddress, RegistrationStatus state)
        {
            string insertCmd = "INSERT INTO Terminals (MacAddress, State) VALUES (@macAddress, @RegistrationStatus)";

            bool result = false;

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                con.Open();
                try
                {
                    using (SqlCommand command = new SqlCommand(insertCmd, con))
                    {
                        command.Parameters.AddWithValue("@macAddress", macAddress);
                        command.Parameters.AddWithValue("@RegistrationStatus", (int)state);
                        int numRows = command.ExecuteNonQuery();

                        if (numRows == 1)
                        {
                            result = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    result = false;
                    Logger.log(ex.Message);
                }
            }

            return result;
        }

        /// <summary>
        /// Delete a terminal from the Terminals table
        /// </summary>
        /// <param name="macAddress"></param>
        /// <returns></returns>
        public bool deleteTerminal(string macAddress)
        {
            string deleteCmd = "DELETE FROM Terminals WHERE MacAddress = @MacAddress";

            bool result = false;

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                con.Open();
                try
                {
                    using (SqlCommand command = new SqlCommand(deleteCmd, con))
                    {
                        command.Parameters.AddWithValue("@macAddress", macAddress);
                        int numRows = command.ExecuteNonQuery();

                        if (numRows == 1)
                        {
                            result = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    //TO DO: Send an error message to the log file
                    result = false;
                    Logger.log(ex.Message);
                }
            }

            return result;
        }

        /// <summary>
        /// Deletes a user from the database
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public bool deleteUser(string userId)
        {
            string deleteCmd = "DELETE FROM EndUsers WHERE Id = @UserId";

            bool result = false;

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                con.Open();
                try
                {
                    using (SqlCommand command = new SqlCommand(deleteCmd, con))
                    {
                        command.Parameters.AddWithValue("@UserId", userId);
                        int numRows = command.ExecuteNonQuery();

                        if (numRows == 1)
                        {
                            result = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    //TO DO: Send an error message to the log file
                    result = false;
                    Logger.log(ex.Message);
                }
            }

            return result;
        }

        /// <summary>
        /// Removes all tickets from the Tickets table. This method is used by the
        /// unit tests
        /// </summary>
        /// <returns></returns>
        public bool cleanUpTickets()
        {
            bool result = false;
            string deleteCmd = "DELETE FROM Tickets";

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                con.Open();
                try
                {
                    using (SqlCommand command = new SqlCommand(deleteCmd, con))
                    {
                        int numRows = command.ExecuteNonQuery();
                        result = true;
                    }
                }
                catch (Exception ex)
                {
                    //TO DO: Send an error message to the log file
                    result = false;
                    Logger.log(ex.Message);
                }
            }

            return result;
        }

        /// <summary>
        /// Removes a MacAddress from a User. The User record remains in the table.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="macAddress"></param>
        /// <returns></returns>
        public bool removeRegistration(string userId, string macAddress)
        {
            string deleteCmd = "UPDATE EndUsers SET MacAddress = '' WHERE Id = @UserId AND macAddress = @MacAddress";

            bool result = false;

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                con.Open();
                try
                {
                    using (SqlCommand command = new SqlCommand(deleteCmd, con))
                    {
                        command.Parameters.AddWithValue("@UserId", userId);
                        command.Parameters.AddWithValue("@MacAddress", macAddress);
                        int numRows = command.ExecuteNonQuery();

                        if (numRows == 1)
                        {
                            result = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    //TO DO: Send an error message to the log file
                    result = false;
                    Logger.log(ex.Message);
                }
            }

            return result;


        }

        /// <summary>
        /// This method simple changes all busy states to success states, indicating that the
        /// registration completed successfully.
        /// </summary>
        /// <returns>-1 if the method fails, otherwise the number of rows modified is returned</returns>
        public int completeRegistrations()
        {
            string ticketUpdateCmd = "UPDATE Tickets SET Status = @TargetStatus WHERE Status = @SourceStatus";
            int numRows = 0;

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                con.Open();
                try
                {
                    using (SqlCommand command = new SqlCommand(ticketUpdateCmd, con))
                    {
                        command.Parameters.AddWithValue("@TargetStatus", RequestStatus.SUCCESSFUL);
                        command.Parameters.AddWithValue("@SourceStatus", RequestStatus.BUSY);
                        numRows = command.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    //TO DO: Send an error message to the log file
                    numRows = -1;
                    Logger.log(ex.Message);
                }
            }

            return numRows;
        }

        /// <summary>
        /// Returns true if the user is already registered for the given terminal
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="macAddress"></param>
        /// <returns></returns>
        public bool IsSameTerminal(string userId, string macAddress)
        {
            return this.isUserRegisteredForTerminal(macAddress, userId);
        }

        /// <summary>
        /// Changes the terminal registration
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="macAddress"></param>
        /// <returns></returns>
        public bool changeTerminal(string userId, string macAddress)
        {
            string updateEndUsersCmd = "UPDATE EndUsers SET MacAddress = @MacAddress WHERE Id = @UserId";
           
            bool result = false;

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                con.Open();
                try
                {
                    using (SqlCommand command = new SqlCommand(updateEndUsersCmd, con))
                    {
                        command.Parameters.AddWithValue("@MacAddress", macAddress);
                        command.Parameters.AddWithValue("@UserId", userId);
                        int numRows = command.ExecuteNonQuery();

                        if (numRows == 1)
                        {
                            //Insert the new terminal with the given MacAddress and put it in the
                            //operational state. This we need to do, otherwise we cannot generate new
                            //tickets for the new terminal.
                            result = this.insertTerminal(macAddress);
                         }
                    }
                }
                catch (Exception ex)
                {
                    //TO DO: Send an error message to the log file
                    result = false;
                    Logger.log(ex.Message);
                }
            }

            return result;
        }

        /// <summary>
        /// Change the sla field in the EndUsers table
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="slaId"></param>
        /// <returns></returns>
        public bool changeSla(string userId, long slaId, int ispId)
        {
            string changeTerminalCmd = "UPDATE EndUsers SET SlaId = @SlaId WHERE Id = @UserId AND Isp = @IspId";

            bool result = false;

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                con.Open();
                try
                {
                    using (SqlCommand command = new SqlCommand(changeTerminalCmd, con))
                    {
                        command.Parameters.AddWithValue("@SlaId", slaId);
                        command.Parameters.AddWithValue("@UserId", userId);
                        command.Parameters.AddWithValue("@IspId", ispId);
                        int numRows = command.ExecuteNonQuery();

                        if (numRows == 1)
                        {
                            result = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    //TO DO: Send an error message to the log file
                    result = false;
                    Logger.log(ex.Message);
                }
            }

            return result;
        }

        /// <summary>
        /// Changes the status of a terminal (LOCKED or OPERATIONAL)
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public bool changeStatus(string userId, RegistrationStatus status)
        {
            string changeTerminalCmd = "UPDATE Terminals SET  State = @State WHERE MacAddress = @MacAddress";

            //Get the MacAddress of userId
            string macAddress = this.getRegisteredTerminal(userId);
            bool result = false;

            if (macAddress != null)
            {
                using (SqlConnection con = new SqlConnection(_connectionString))
                {
                    con.Open();
                    try
                    {
                        using (SqlCommand command = new SqlCommand(changeTerminalCmd, con))
                        {
                            command.Parameters.AddWithValue("@State", (int)status);
                            command.Parameters.AddWithValue("@MacAddress", macAddress);
                            int numRows = command.ExecuteNonQuery();

                            if (numRows == 1)
                            {
                                result = true;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        //TO DO: Send an error message to the log file
                        result = false;
                        Logger.log(ex.Message);
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// This method returns the terminal registered for a user
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public string getRegisteredTerminal(string userId)
        {
            string registeredTerminalCmd = "SELECT MacAddress FROM EndUsers WHERE Id = @userId";
            string macAddress = "";

            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                conn.Open();

                using (SqlCommand cmd = new SqlCommand(registeredTerminalCmd, conn))
                {
                    try
                    {
                        cmd.Parameters.AddWithValue("@userId", userId);
                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            macAddress = reader.GetString(0).Trim();
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.log(ex.Message);
                    }
                }
            }

            return macAddress;
        }

        /// <summary>
        /// This method returns the terminal registered for a user belonging to a specific Isp
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public string getRegisteredTerminal(string userId, int Isp)
        {
            string registeredTerminalCmd = "SELECT MacAddress FROM EndUsers WHERE Id = @userId AND Isp = @Isp";
            string macAddress = "";

            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                conn.Open();

                using (SqlCommand cmd = new SqlCommand(registeredTerminalCmd, conn))
                {
                    try
                    {
                        cmd.Parameters.AddWithValue("@userId", userId);
                        cmd.Parameters.AddWithValue("@Isp", Isp);
                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            macAddress = reader.GetString(0).Trim();
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.log(ex.Message);
                    }
                }
            }

            return macAddress;
        }

        /// <summary>
        /// This method returns the terminal registered for a user belonging to a specific Isp
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public string getRegisteredTerminalBySitId(int Isp, int sitId)
        {
            string registeredTerminalCmd = "SELECT MacAddress FROM EndUsers WHERE SitId = @SitId AND Isp = @Isp"; ;
            string macAddress = "";

            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                conn.Open();

                using (SqlCommand cmd = new SqlCommand(registeredTerminalCmd, conn))
                {
                    try
                    {
                        cmd.Parameters.AddWithValue("@userId", sitId);
                        cmd.Parameters.AddWithValue("@Isp", Isp);
                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            macAddress = reader.GetString(0).Trim();
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.log(ex.Message);
                    }
                }
            }

            return macAddress;
        }

        /// <summary>
        /// Returns the ticket identifief by RequestTicketId
        /// </summary>
        /// <param name="RequestTicketId"></param>
        /// <returns></returns>
        public RequestTicket getRequestTicket(string RequestTicketId)
        {
            string requestTicketQueryCmd = "SELECT TicketId, Status, FailureReason, FailureStackTrace FROM Tickets " +
                                                "WHERE TicketId = @TicketId";

            RequestTicket rt = new RequestTicket();

            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                conn.Open();

                using (SqlCommand cmd = new SqlCommand(requestTicketQueryCmd, conn))
                {
                    try
                    {
                        cmd.Parameters.AddWithValue("@TicketId", RequestTicketId);
                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            rt.id = reader.GetGuid(0).ToString();
                            rt.requestStatus = (RequestStatus)reader.GetInt32(1);
                            rt.failureReason = reader.GetString(2);
                            rt.failureStackTrace = reader.GetString(3);
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.log(ex.Message);
                    }
                }
            }

            return rt;
        }

        /// <summary>
        /// Sets the consumed volume column back to 0 for the given terminal
        /// </summary>
        /// <param name="endUserId"></param>
        /// <returns></returns>
        public bool resetVolume(string macAddress)
        {
            string resetCmd = "UPDATE Terminals SET Volume = 0.0 WHERE MacAddress = @MacAddress";

             bool result = false;

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                con.Open();
                try
                {
                    using (SqlCommand command = new SqlCommand(resetCmd, con))
                    {
                        command.Parameters.AddWithValue("@MacAddress", macAddress);
                        int numRows = command.ExecuteNonQuery();

                        if (numRows == 1)
                        {
                            result = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    //TO DO: Send an error message to the log file
                    result = false;
                    Logger.log(ex.Message);
                }
            }

            return result;

        }

        /// <summary>
        /// Returns  the volume for a terminal
        /// </summary>
        /// <param name="macAddress"></param>
        /// <returns></returns>
        public double getTerminalVolume(string macAddress)
        {
            string volumeQueryCmd = "SELECT Volume FROM Terminals WHERE MacAddress = @MacAddress";
            double volume = 0.0;

            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                conn.Open();

                using (SqlCommand cmd = new SqlCommand(volumeQueryCmd, conn))
                {
                    try
                    {
                        cmd.Parameters.AddWithValue("@MacAddress", macAddress);
                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            volume = reader.GetDouble(0);
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.log(ex.Message);
                    }
                }
            }

            return volume;
        }

    }
}