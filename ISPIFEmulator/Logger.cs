﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;

namespace net.nimera.cmt.ISPIFEmulator
{
    public class Logger
    {
        const bool _DEBUG = true;

        /// <summary>
        /// Simple logger
        /// </summary>
        /// <param name="msg">Message to log</param>
        public static void log(string msg)
        {
            if (_DEBUG)
            {
                using (StreamWriter writer = new StreamWriter("C:/Temp/ISPIFEmu.log", true))
                {
                    writer.WriteLine("[" + DateTime.Now + "] " + msg);
                }
            }
        }
    }
}