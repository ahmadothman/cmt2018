﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace net.nimera.cmt.ISPIFEmulator
{
    [Serializable]
    public class TerminalInUseException : System.Exception
    {
        public TerminalInUseException()
        {
        }

        public TerminalInUseException(string message)
            : base(message)
        {
        }

        public TerminalInUseException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected TerminalInUseException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}