﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace net.nimera.cmt.ISPIFEmulator
{
    [Serializable]
    public class OutstandingRegistrationRequestException : System.Exception
    {
        public OutstandingRegistrationRequestException()
        {
        }

        public OutstandingRegistrationRequestException(string message)
            : base(message)
        {
        }

        public OutstandingRegistrationRequestException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected OutstandingRegistrationRequestException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}