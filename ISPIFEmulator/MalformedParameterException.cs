﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace net.nimera.cmt.ISPIFEmulator
{
    [Serializable]
    public class MalformedParameterException : Exception
    {
        public MalformedParameterException()
        {
        }

        public MalformedParameterException(string message)
            : base(message)
        {
        }

        public MalformedParameterException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected MalformedParameterException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}