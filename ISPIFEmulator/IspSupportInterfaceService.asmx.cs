﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.IO;
//using net.nimera.cmt.enums;

namespace net.nimera.cmt.ISPIFEmulator
{
    /// <summary>
    /// Summary description for IspSupportInterfaceService
    /// </summary>
    [WebService(Namespace = "http://cmt.nimera.net/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class IspSupportInterfaceService : System.Web.Services.WebService
    {
        const bool _DEBUG = true;

        /// <summary>
        /// This is an asynchronous operation to create a registration between an end user and a terminal. 
        /// There can be only one registration per end user. 
        /// As a side effect the end user is created if necessary. 
        /// The end user will not be removed from the system even with remove registration operation, 
        /// so that a create registration on an already existing user will just link him to the terminal. 
        /// A maximum of 50 characters is available for end user identifier. 
        /// The list of allowed SLA identifiers is provided by the ISPIF Support SAP Operator on setup of an ISP account, 
        /// see 3.3 for list of other details provided during setup. See 2.1.3 for details of the asynchronous mechanism and 
        /// the returned RequestTicket data type.
        /// </summary>
        /// <param name="endUserId"></param>
        /// <param name="macAddress"></param>
        /// <param name="slaId"></param>
        /// <returns></returns>
        [WebMethod]
        public RequestTicket createRegistration(string endUserId, string macAddress, string slaId, string ispId)
        {
            long iSlaId = Int64.Parse(slaId);
            int iIspId = Int32.Parse(ispId);

            RequestTicket rt = new RequestTicket();
            rt.id = Guid.NewGuid().ToString();
            
            //Check the pre-conditions
            DataGateway dg = new DataGateway();

            Logger.log("createRegistration called");

            //Check for the correctness of the parameters
            //Remove the issue with the endUserId length, eventually put back in a later version
            //if (endUserId.Length > 50 || macAddress.Equals("") || endUserId.Equals(""))
            if (macAddress.Equals("") || endUserId.Equals(""))
            {
                Logger.log("MalformedParameterException - Mac Address: " + macAddress + ", End User: " + endUserId);
                throw new MalformedParameterException("Parameters passed are not correct");
            }

            //Since we are applying a direct registration, the terminal is created as well
            //in case the terminal does not exist.
            if (!dg.terminalExists(macAddress))
            {
                if (!dg.insertTerminal(macAddress, RegistrationStatus.LOCKED))
                {
                    Logger.log("NoSuchTerminalException - Mac Address: " + macAddress + ", End User: " + endUserId);
                    throw new NoSuchTerminalException("Could not create terminal");
                }
            }

            //Check if the terminal is in use
            if (dg.isTerminalInUse(macAddress))
            {
                Logger.log("TerminalInUseException - Mac Address: " + macAddress + ", End User: " + endUserId);
                throw new TerminalInUseException(macAddress + " is in use");
            }

            //Check if the terminal is not blocked by another request
            if (dg.hasPendingRequest(macAddress))
            {
                Logger.log("OutstandingTerminalRequestException - Mac Address: " + macAddress + ", End User: " + endUserId);
                throw new OutstandingTerminalRequestException(macAddress + " is Busy");
            }

            //Check if registration is ongoing
            if (dg.isRegistrationOnGoing(macAddress))
            {
                Logger.log("OutstandingResigstrationRequestException - Mac Address: " + macAddress + ", End User: " + endUserId);
                throw new OutstandingRegistrationRequestException(macAddress + " has a registration going on");
            }

            //Check if the end user is already registered
            if (dg.isUserRegistered(endUserId))
            {
                Logger.log("EndUserAlreadyRegisteredException - Mac Address: " + macAddress + ", End User: " + endUserId);
                throw new EndUserAlreadyRegisteredException(endUserId + " is already registered");
            }

            //Check if the end user exists, if not create the user
            if (!dg.userExists(endUserId))
            {
                Logger.log("User does not exist, creating user - Mac Address: " + macAddress + ", End User: " + endUserId);
                dg.createUser(endUserId, iIspId);
            }

            //Create the registration
            if (dg.registerUser(endUserId, macAddress, iSlaId))
            {
                //Create a new ticket
                Logger.log("User registerd succesfully - Mac Address: " + macAddress + ", End User: " + endUserId);
                rt.requestStatus = RequestStatus.BUSY;
            }
            else
            {
                Logger.log("User registration failed - Mac Address: " + macAddress + ", End User: " + endUserId);
                rt.requestStatus = RequestStatus.FAILED;
                rt.failureReason = "Unable to register user";
            }

            if (!dg.generateTicket(ref rt, macAddress, TypeEnum.Registration))
            {
                Logger.log("Could not generate ticket - Mac Address: " + macAddress + ", End User: " + endUserId);
                throw new InternalException("Could not generate Ticket!");
            }
            return rt;
        }

        /// <summary>
        /// This is an asynchronous operation to remove the registration association between an end user and a terminal.
        /// There can be only one registration per end user. The end user is not completely removed from the Gateway system. 
        /// Accounting and billing information, that must be kept, will reference the end user so the Registration must be kept too. 
        /// A create registration on an already existing user will associate him to the new terminal. 
        /// See 2.1.3 for details of the asynchronous mechanism and the returned RequestTicket data type.
        /// </summary>
        /// <param name="endUserId"></param>
        /// <param name="macAddress"></param>
        /// <returns></returns>
        [WebMethod]
        public RequestTicket removeRegistration(string endUserId, string macAddress)
        {
            RequestTicket rt = new RequestTicket();
            rt.id = Guid.NewGuid().ToString();

            //Check the pre-conditions
            DataGateway dg = new DataGateway();

            Logger.log("removeRegistration called");

            //Check if the terminal is in use
            if (dg.isTerminalInUse(macAddress))
            {
                throw new TerminalInUseException(macAddress + " is in use");
            }

            //Check if the terminal is not blocked by another request
            if (dg.hasPendingRequest(macAddress))
            {
                throw new OutstandingTerminalRequestException(macAddress + " is Busy");
            }

            //Check if registration is ongoing
            if (dg.isRegistrationOnGoing(macAddress))
            {
                throw new OutstandingRegistrationRequestException(macAddress + " has a registration going on");
            }

            //Check for the correctness of the parameters
            if (endUserId.Length > 50 || macAddress.Equals("") || endUserId.Equals(""))
            {
                throw new MalformedParameterException("Parameters passed are not correct");
            }

            //Check if the terminal exists
            if (!dg.terminalExists(macAddress))
            {
                throw new NoSuchTerminalException(macAddress + " does not exist");
            }

            if (!dg.userExists(endUserId))
            {
                throw new NoSuchEndUserException(endUserId + " does not exist");
            }

            if (!dg.removeRegistration(endUserId, macAddress))
            {
                throw new TerminalMismatchException();
            }

            if (!dg.generateTicket(ref rt, macAddress, TypeEnum.Registration))
            {
                throw new InternalException("Could not generate Ticket!");
            }

            return rt;
        }

        /// <summary>
        /// This is an asynchronous operation to change the terminal used by end user and a terminal. There
        /// can be only one terminal per end user. The replacing terminal will become OPERATIONAL and 
        /// depending on the provisioning model the replaced terminal will no longer be referenced at all
        /// or pre-provisioned with a limited service level.
        /// </summary>
        /// <param name="endUserId"></param>
        /// <param name="macAddress"></param>
        /// <returns></returns>
        [WebMethod]
        public RequestTicket changeTerminal(string endUserId, string macAddress)
        {
            RequestTicket rt = new RequestTicket();
            rt.id = Guid.NewGuid().ToString();

            //Check the pre-conditions
            DataGateway dg = new DataGateway();

            Logger.log("changeTerminal called");

            if (!dg.isUserRegistered(endUserId))
            {
                throw new NoCurrentTerminalException();
            }

            //If registered get the original MacAddress
            string oriMacAddress = dg.getRegisteredTerminal(endUserId);

            //Check if the terminal is in use
            if (dg.isTerminalInUse(oriMacAddress))
            {
                throw new TerminalInUseException(oriMacAddress + " is in use");
            }

            //Check if the terminal is not blocked by another request
            if (dg.hasPendingRequest(oriMacAddress))
            {
                throw new OutstandingTerminalRequestException(macAddress + " is Busy");
            }

            //Check for the correctness of the parameters
            //if (endUserId.Length > 50 || macAddress.Equals("") || endUserId.Equals(""))
            if (macAddress.Equals("") || endUserId.Equals(""))
            {
                throw new MalformedParameterException("Parameters passed are not correct");
            }

            if (!dg.terminalExists(oriMacAddress))
            {
                throw new NoSuchTerminalException(oriMacAddress + " does not exist");
            }

            if (!dg.IsSameTerminal(endUserId, oriMacAddress))
            {
                throw new SameTerminalException();
            }

            //Check if registration is ongoing
            if (dg.isRegistrationOnGoing(oriMacAddress))
            {
                throw new OutstandingRegistrationRequestException(oriMacAddress + " has a registration going on");
            }

            if (!dg.userExists(endUserId))
            {
                throw new NoSuchEndUserException(endUserId + " does not exist");
            }

            //Change the terminal
            if (dg.changeTerminal(endUserId, macAddress))
            {
                //Delete the old terminal with the oriMacAddress
                //This also removes all associated tickets
                dg.deleteTerminal(oriMacAddress);

                //Create a new ticket
                rt.requestStatus = RequestStatus.BUSY;
            }
            else
            {
                rt.requestStatus = RequestStatus.FAILED;
                rt.failureReason = "Unable to register user";
            }

            if (!dg.generateTicket(ref rt, macAddress, TypeEnum.Change))
            {
                throw new InternalException("Could not change terminal!");
            }

            return rt;
        }

        /// <summary>
        /// This is an asynchronous operation to change the quality of service provided by the terminal 
        /// to the end user. The list of allowed SLA identifiers is provided by the ISPIF Support SAP
        /// Operator on setup of an ISP account. See 2.1.3 for details of the asynchronous mechanism and 
        /// the returned RequestTicket data type.
        /// </summary>
        /// <param name="endUserId"></param>
        /// <param name="newSlaId"></param>
        /// <returns></returns>
        [WebMethod]
        public RequestTicket changeSla(string endUserId, string newSlaId, string ispId)
        {
            RequestTicket rt = new RequestTicket();
            rt.id = Guid.NewGuid().ToString();

            DataGateway dg = new DataGateway();

            Logger.log("changeSla called");

            if (!dg.slaExists(Int32.Parse(newSlaId)))
            {
                throw new NoSuchSlaException();
            }

            if (!dg.userExists(endUserId))
            {
                throw new NoSuchEndUserException();
            }

            string macAddress = dg.getRegisteredTerminal(endUserId);

            if (macAddress == null)
            {
                throw new InternalException();
            }

            //Check if registration is ongoing
            if (dg.isRegistrationOnGoing(macAddress))
            {
                throw new OutstandingRegistrationRequestException(macAddress + " has a registration going on");
            }

            //Check if the terminal is not blocked by another request
            if (dg.hasPendingRequest(macAddress))
            {
                throw new OutstandingTerminalRequestException(macAddress + " is Busy");
            }

            if (!dg.changeSla(endUserId, Int32.Parse(newSlaId), Int32.Parse(ispId)))
            {
                rt.requestStatus = RequestStatus.BUSY;
            }
            else
            {
                rt.requestStatus = RequestStatus.FAILED;
                rt.failureReason = "Unable to change sla";
            }

            if (!dg.generateTicket(ref rt, macAddress, TypeEnum.Change))
            {
                throw new InternalException("Could not generate Ticket!");
            }

            return rt;
        }

        /// <summary>
        /// This is an asynchronous operation to de-activate, re-activate or temporarily 
        /// block the end user's terminal. See 2.1.3 for details of the asynchronous mechanism 
        /// and the returned RequestTicket data type.
        /// </summary>
        /// <param name="endUserId"></param>
        /// <param name="newStatus"></param>
        /// <returns></returns>
        [WebMethod]
        public RequestTicket changeStatus(string endUserId, RegistrationStatus newStatus, string ispId)
        {
            RequestTicket rt = new RequestTicket();
            rt.id = Guid.NewGuid().ToString();
            DataGateway dg = new DataGateway();

            Logger.log("changeStatus called for endUserId: " + endUserId);

            if (!dg.userExists(endUserId))
            {
                throw new NoSuchEndUserException();
            }
            
            //Retrieve the macAddress 
            string macAddress = dg.getRegisteredTerminal(endUserId);

            if (macAddress == null)
            {
                throw new InternalException();
            }

            //Check if registration is ongoing
            if (dg.isRegistrationOnGoing(macAddress))
            {
                throw new OutstandingRegistrationRequestException(macAddress + " has a registration going on");
            }

            //Check if the terminal is not blocked by another request
            if (dg.hasPendingRequest(macAddress))
            {
                throw new OutstandingTerminalRequestException(macAddress + " is Busy");
            }

            //Check for the correctness of the parameters
            //if (endUserId.Length > 50 || macAddress.Equals("") || endUserId.Equals(""))
            if (macAddress.Equals("") || endUserId.Equals(""))
            {
                throw new MalformedParameterException("Parameters passed are not correct");
            }

            if (dg.changeStatus(endUserId, newStatus))
            {
                if (!dg.generateTicket(ref rt, macAddress, TypeEnum.Change))
                {
                    throw new InternalException("Could not generate Ticket!");
                }
            }
            else
            {
                throw new InternalException("Could not change the sttus of terminal: " + endUserId);
            }

            return rt;
        }

        /// <summary>
        /// This is a synchronous operation to recover the current state of the identified asynchronous request. 
        /// See 2.1.3 for details of the asynchronous mechanism and the returned RequestTicket data type.
        /// </summary>
        /// <param name="requestTicketId"></param>
        /// <returns></returns>
        [WebMethod]
        public RequestTicket lookupRequestTicket(string requestTicketId)
        {
            DataGateway dg = new DataGateway();
            return dg.getRequestTicket(requestTicketId);
        }

        /// <summary>
        /// Resets user's traffic information for the current month. This may be 
        /// invoked by a normal isp (in which case he may only call it for his own terminals, where ispId is
        /// identical to his own id) or by teh admin (who may cal it for any ispId). The ispId is the ISPIF id.
        /// </summary>
        /// <param name="endUserId"></param>
        /// <param name="ispId"></param>
        [WebMethod]
        public void resetUser(string endUserId, int ispId)
        {
            DataGateway dg = new DataGateway();

            Logger.log("resetUser called");

            //Check if the end-user exists
            if (!dg.userExists(endUserId))
            {
                throw new NoSuchEndUserException();
            }

            string macAddress = dg.getRegisteredTerminal(endUserId, ispId);

            if (macAddress != null)
            {
                dg.resetVolume(macAddress);
            }
            else
            {
                throw new InternalException("Terminal does not exist");
            }
        }

        /// <summary>
        /// Resets user's traffic information for the current month. This may be 
        /// invoked by a normal isp (in which case he may only call it for his own terminals, where ispId is
        /// identical to his own id) or by teh admin (who may cal it for any ispId). The ispId is the ISPIF id.
        /// </summary>
        /// <param name="ispId"></param>
        /// <param name="sitId"></param>
        [WebMethod]
        public void resetUserBySit(string ispId, string sitId)
        {
            DataGateway dg = new DataGateway();

            int isp = Int32.Parse(ispId);
            int sit = Int32.Parse(sitId);

            Logger.log("resetUserBySit caled");

            string macAddress = dg.getRegisteredTerminalBySitId(isp, sit);

            if (macAddress != null)
            {
                dg.resetVolume(macAddress);
            }
            else
            {
                throw new InternalException("Terminal does not exist");
            }
        }
        
        /// <summary>
        /// Complete regsitrations by putting the states to SUCCESSFULL
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public int completeRegistrations()
        {
            DataGateway dg = new DataGateway();

            return dg.completeRegistrations();
        }

        /// <summary>
        /// Simulates the preprovisioning of a terminal
        /// </summary>
        /// <param name="macAddress"></param>
        /// <returns></returns>
        [WebMethod]
        public bool preProvisionTerminal(string macAddress)
        {
            DataGateway dg = new DataGateway();
            return dg.insertTerminal(macAddress, RegistrationStatus.LOCKED);
        }
    }
}
