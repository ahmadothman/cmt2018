﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace net.nimera.cmt.ISPIFEmulator
{
    [Serializable]
    public class NoSuchEndUserException : System.Exception
    {
        public NoSuchEndUserException()
        {
        }

        public NoSuchEndUserException(string message)
            : base(message)
        {
        }

        public NoSuchEndUserException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected NoSuchEndUserException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}