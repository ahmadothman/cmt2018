﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace net.nimera.cmt.ISPIFEmulator
{
    [Serializable]
    public class NoCurrentTerminalException : System.Exception
    {
        public NoCurrentTerminalException()
        {
        }

        public NoCurrentTerminalException(string message)
            : base(message)
        {
        }

        public NoCurrentTerminalException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected NoCurrentTerminalException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}