﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MandacTester.CMTThirdPartyWS;
namespace MandacTester
{
    public partial class _default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ButtonRun_Click(object sender, EventArgs e)
        {
            CMTMandacControllerWS control = new CMTMandacControllerWS();
            //double vol = control.GetRemainingVolumeForTerminal(TextBox1.Text);
            double[] vol = control.GetTerminalVolumeInfo(TextBox1.Text);
            string volStr = "";
            foreach (double d in vol)
            {
                volStr = volStr + d.ToString("0.000") + " - "; 
            }
            LabelResult.Text = volStr;
        }
    }
}