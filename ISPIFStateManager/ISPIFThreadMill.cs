﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO;
using ISPIFStateManager.net.nimera.ispifemu.IspSupportInterfaceServiceRef;

namespace ISPIFStateManager
{
    /// <summary>
    /// This class contains the main state management thread
    /// </summary>
    class ISPIFThreadMill
    {
        const int _THREAD_INTERVAL = 10000;
        bool _runFlag = true;
        string _logFilePath = "C:/Temp/ISPIFThreadMill.log";

        public bool RunFlag
        {
            get
            {
                return _runFlag;
            }

            set
            {
                _runFlag = value;
            }
        }


        public void stateManagementThread()
        {
            this.log("State management thread start");

            IspSupportInterfaceService isi = new IspSupportInterfaceService();
            while (_runFlag)
            {
                try
                {
                    int rows = isi.completeRegistrations();
                    this.log("Number of records processed: " + rows);
                }
                catch (Exception ex)
                {
                    this.log("Exception: " + ex.Message);
                }
                Thread.Sleep(_THREAD_INTERVAL);
            }

            this.log("State management thread stop");
        }

        private void log(string logMsg)
        {
            using (StreamWriter writer = new StreamWriter(_logFilePath, true))
            {
                writer.WriteLine("[" + DateTime.Now + "]" + logMsg);
            }
        }

    }
}
