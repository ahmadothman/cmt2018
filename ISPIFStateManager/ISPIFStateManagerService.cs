﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;

namespace ISPIFStateManager
{
    public partial class ISPIFStateManagerService : ServiceBase
    {
        ISPIFThreadMill _ispifThreadMill = null;
        public ISPIFStateManagerService()
        {
            InitializeComponent();
            _ispifThreadMill = new ISPIFThreadMill();

             if (!System.Diagnostics.EventLog.SourceExists("ISPIFStateManagerSource"))     
                 System.Diagnostics.EventLog.CreateEventSource("ISPIFStateManagerSource", "ISPIFStateManagerLog");     
            this.eventLogISPIFStateManager.Source = "ISPIFStateManagerSource";     
            this.eventLogISPIFStateManager.Log = "ISPIFStateManagerLog";
        }

        protected override void OnStart(string[] args)
        {
            //Start the manager main thread
            Thread mainThread = new Thread(new ThreadStart(_ispifThreadMill.stateManagementThread));
            mainThread.Start();
            eventLogISPIFStateManager.WriteEntry("Windows service start at: " + DateTime.Now.TimeOfDay);
        }

        protected override void OnStop()
        {
            _ispifThreadMill.RunFlag = false;
            eventLogISPIFStateManager.WriteEntry("Windows service stopped at: " + DateTime.Now.TimeOfDay);
        }
    }
}
