﻿namespace ISPIFStateManager
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ISPIFStateManagerProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.ISPIFStateManagerServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // ISPIFStateManagerProcessInstaller
            // 
            this.ISPIFStateManagerProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.ISPIFStateManagerProcessInstaller.Password = null;
            this.ISPIFStateManagerProcessInstaller.Username = null;
            // 
            // ISPIFStateManagerServiceInstaller
            // 
            this.ISPIFStateManagerServiceInstaller.Description = "ISPIF State Manager";
            this.ISPIFStateManagerServiceInstaller.DisplayName = "ISPIF State Manager";
            this.ISPIFStateManagerServiceInstaller.ServiceName = "ISPIFStateManager";
            this.ISPIFStateManagerServiceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.ISPIFStateManagerProcessInstaller,
            this.ISPIFStateManagerServiceInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller ISPIFStateManagerProcessInstaller;
        private System.ServiceProcess.ServiceInstaller ISPIFStateManagerServiceInstaller;
    }
}