﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using MySql.Data.MySqlClient;
using System.Configuration;
using NMSNetFlowBigData.BOCassandraLibWSRef;
using NMSNetFlowBigData.BOLogControlWSRef;
using NMSNetFlowBigData.BOAccountingControlWSRef;
using NMSNetFlowBigData.Util;
using System.Data.SqlClient;

namespace NMSNetFlowBigData
{
    class Program
    {
        static void Main(string[] args)
        {
            GetNMSNetFlowList();
            ProcessNMSNetFlowFile();
        }

        static BOCassandraLibWS _cassandraClient = new BOCassandraLibWS();
        static DateTime timeStamp = DateTime.UtcNow;
        static string timeStampString = timeStamp.ToString("yyyy-MM-dd HH:mm:ss");
        static List<NMSNetFlowFile> netFlowFiles = new List<NMSNetFlowFile>();

        //GET NetFlow files from NMS folder and put them in a NMSNetFlowFile List
        private static List<NMSNetFlowFile> GetNMSNetFlowList()
        {
            //List<NMSNetFlowFile> netFlowFiles = new List<NMSNetFlowFile>();
            string path = DateTime.Now.AddDays(-1).ToString();
            //IEnumerable<FileInfo> files = new DirectoryInfo(@"C:\Users\kstockmans\Desktop\Issues CMT\NMSNetFlowTestData\" + path).GetFiles().OrderBy(file => file.CreationTime);
            IEnumerable<FileInfo> files = new DirectoryInfo(@"C:\Users\kstockmans\Desktop\Issues CMT\NMSNetFlowTestData").GetFiles().OrderBy(file => file.CreationTime);

            foreach (FileInfo file in files)
            {
                NMSNetFlowFile netFlowFile = new NMSNetFlowFile
                {
                    filename = file.Name,
                    time_stamp = file.CreationTime
                };
                byte[] fileContent;
                using (FileStream stream = file.OpenRead())
                {
                    using (var reader = new BinaryReader(stream))
                    {
                        fileContent = reader.ReadBytes((int)stream.Length);
                    }
                }
                netFlowFile.file = fileContent;
                netFlowFiles.Add(netFlowFile);
            }

            return netFlowFiles;
        }

        //INSERT NMSNetFlowFiles in the Cassandra DB
        private static void ProcessNMSNetFlowFile()
        {
            Logger.LogInfo("Processing file: " + timeStampString, "NMS NetFlow File Import");
            //bool success = false;

            int counter = 0; //a counter to determine the number of objects

            // Get values from netFlowFiles List (by method above GetNMSNetFlowList) and insert them into Cassandra DB table nmsnetflowfiles
            try
            {
                foreach (NMSNetFlowFile nff in netFlowFiles)
                {
                    counter++;
                }

                NMSNetFlowFile[] cassandraArray = new NMSNetFlowFile[counter]; //the counter is used to determine the length of the array
                counter = 0; //the counter is reset so it can be used for selecting the right object in the array
                foreach (NMSNetFlowFile nff in netFlowFiles)
                {
                    cassandraArray[counter] = nff;
                    counter++;
                }
                _cassandraClient.InsertNMSNetFlowFiles(cassandraArray, timeStampString);
                Logger.log("Inserting data in Cassandra succeeded");
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
            }
        }

    }
}
