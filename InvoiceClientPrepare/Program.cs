﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Synchronization.Data;
using Microsoft.Synchronization.Data.SqlServer;

namespace InvoiceClientPrepare
{
    class Program
    {
        const string _SCOPE = "CMTInvoiceScope";
        const string _ClientConnectionString = "Data Source=.\\SQLEXPRESS; Initial Catalog=CMTData-Invoicing; Integrated Security=SSPI;";
        const string _ServerConnectionString = "data source=bj8nh0fsve.database.windows.net;Database=CMTData-Invoicing; User Id=evermassen; Password=arne#1200";

        static void Main(string[] args)
        {
            Program prg = new Program();
            prg.mainBody();
        }

        private void mainBody()
        {
            try
            {
                SqlConnection clientConn = new SqlConnection(_ClientConnectionString);
                SqlConnection serverConn = new SqlConnection(_ServerConnectionString);
                DbSyncScopeDescription scopeDesc =
                                    SqlSyncDescriptionBuilder.GetDescriptionForScope(_SCOPE, serverConn);

                // create server provisioning object based on the ProductsScope
                SqlSyncScopeProvisioning clientProvision = new SqlSyncScopeProvisioning(clientConn, scopeDesc);

                // starts the provisioning process
                clientProvision.Apply();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
