﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ContactForm.aspx.cs" Inherits="SatADSLContactForm.ContactForm" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table>
                <tr>
                    <td width="20%">Name:</td>
                    <td><asp:TextBox ID="TextBoxName" runat="server" width="400px"></asp:TextBox></td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="This is a required field!"
                    ControlToValidate="TextBoxName"></asp:RequiredFieldValidator>
                </tr>
                <tr>
                    <td>Company:</td>
                    <td><asp:TextBox ID="TextBoxCompany" runat="server" width="400px"></asp:TextBox></td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="This is a required field!"
                    ControlToValidate="TextBoxCompany"></asp:RequiredFieldValidator>
                </tr>
                <tr>
                    <td>Email:</td>
                    <td><asp:TextBox ID="TextBoxEmail" runat="server" width="400px"></asp:TextBox></td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="This is a required field!"
                    ControlToValidate="TextBoxEmail"></asp:RequiredFieldValidator>
                </tr>
                <tr>
                    <td>Phone:</td>
                    <td><asp:TextBox ID="TextBoxPhone" runat="server" width="400px"></asp:TextBox></td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="This is a required field!"
                    ControlToValidate="TextBoxPhone"></asp:RequiredFieldValidator>
                </tr>
                <tr>
                    <td>Website:</td>
                    <td><asp:TextBox ID="TextBoxWebsite" runat="server" width="400px"></asp:TextBox></td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="This is a required field!"
                    ControlToValidate="TextBoxWebsite"></asp:RequiredFieldValidator>
                </tr>
                <tr>
                    <td>Country:</td>
                    <td><asp:TextBox ID="TextBoxCountry" runat="server" width="400px"></asp:TextBox></td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="This is a required field!"
                    ControlToValidate="TextBoxCountry"></asp:RequiredFieldValidator>
                </tr>
                <tr>
                    <td> </td>
                    <td>
                        <asp:CheckBoxList ID="CheckBoxList1" runat="server">
                            <asp:ListItem Value="ISP">ISP</asp:ListItem>
                            <asp:ListItem Value="financial">Financial institution</asp:ListItem>
                            <asp:ListItem Value="enduser">End-User</asp:ListItem>
                            <asp:ListItem Value="other">Other</asp:ListItem>
                        </asp:CheckBoxList>
                    </td>
                </tr>
            </table>
         <asp:button runat="server" ID="ButtonSubmit" Text="Submit" OnClick="ButtonSubmit_Click"></asp:button>
         <br /><asp:Label ID="LabelConfirmation" runat="server"></asp:Label>
    </div>
    </form>
</body>
</html>
