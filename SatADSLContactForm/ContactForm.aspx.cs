﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BOControllerLibrary.Controllers;
using System.Drawing;
using System.Configuration;

namespace SatADSLContactForm
{
    public partial class ContactForm : System.Web.UI.Page
    {
        string _connectionString = ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
        string _databaseProvider = ConfigurationManager.ConnectionStrings["ApplicationServices"].ProviderName;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ButtonSubmit_Click(object sender, EventArgs e)
        {
            BONotifyController notifyController = new BONotifyController(_connectionString, _databaseProvider);

            string emailSubject = "New request for case sheets from: " + TextBoxName.Text;
            string receiverEmail = "info@satadsl.net";
            
            StringBuilder b = new StringBuilder();
            b.Append("Name: " + TextBoxName.Text).AppendLine();
            b.Append("*** Company: " + TextBoxCompany.Text).AppendLine();
            b.Append("*** Email: " + TextBoxEmail.Text).AppendLine();
            b.Append("*** Phone: " + TextBoxPhone.Text).AppendLine();
            b.Append("*** Website: " + TextBoxWebsite.Text).AppendLine();
            b.Append("*** Country: " + TextBoxCountry.Text).AppendLine();


            foreach (ListItem item in CheckBoxList1.Items)
            {
                if (item.Selected)
                {
                    b.Append("*** " + item.Text.ToString()).AppendLine();
                }
            }

            string emailBody = b.ToString();

            if (notifyController.SendMail(emailBody, emailSubject, receiverEmail))
            {
                LabelConfirmation.ForeColor = Color.Green;
                LabelConfirmation.Text = "Form succesfully submitted";
            }
            else
            {
                LabelConfirmation.ForeColor = Color.Red;
                LabelConfirmation.Text = "Submission failed";
            }
        }
    }
}