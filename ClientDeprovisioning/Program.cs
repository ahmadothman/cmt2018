﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Microsoft.Synchronization;
using Microsoft.Synchronization.Data;
using Microsoft.Synchronization.Data.SqlServer;

namespace ClientDeprovisioning
{
    class Program
    {
        const string _SCOPE = "CMTDataScope";
        const string _ClientConnectionString = "Data Source=.\\SQLEXPRESS; Initial Catalog=CMTData-Local; Integrated Security=SSPI;";

        static void Main(string[] args)
        {
            Program prg = new Program();
            prg.mainBody();
        }

        private void mainBody()
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(_ClientConnectionString))
                {
                    SqlSyncScopeDeprovisioning serverSqlDepro = new SqlSyncScopeDeprovisioning(conn);
                    serverSqlDepro.DeprovisionScope(_SCOPE);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
