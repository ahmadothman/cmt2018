﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.EnterpriseServices;
using System.IO;
using System.Linq;
using System.ServiceModel.Configuration;
using System.Text;
using System.Threading.Tasks;
using BusinessVolumeNotificationService.BOAccountingControlWSRef;
using BusinessVolumeNotificationService.BOLogControlWSRef;
using BusinessVolumeNotificationService.BOMonitorControlWSRef;
using BusinessVolumeNotificationService.BOTaskControlWSRef;
using BusinessVolumeNotificationService.NMSGatewayWSRef;
using BusinessVolumeNotificationService.Properties;
using BusinessVolumeNotificationService.Util;

namespace BusinessVolumeNotificationService
{
    class Program
    {
        private string _connectionString;
        private BOLogControlWS _boLogControl = null;
        private BOTaskControlWS _boTaskControl = null;
        private BOMonitorControlWS _boMonitorControlWs = null;
        private BOAccountingControlWS _boAccountingControlWs = null;
        private string userId;
        static void Main(string[] args)
        {
            Logger.log("Processing started: Business Notification");
            Program program= new Program();
            program.SendEmail();
            Logger.log("Processing ended: Business Notification");
            //Console.ReadLine();
        }
        public List<TerminalObject> ListOfTerminalObjects()
        {
            List<TerminalObject> terminals= new List<TerminalObject>();
            _connectionString = Properties.Settings.Default.ConnectionString;
            string query = " select t.MacAddress, t.FullName, t.IspId,t.SitId,d.EMail ,s.SlaName ,t.distributorid from terminals t inner join servicepacks s on t.slaid = s.SlaID " +
                           " inner join Distributors d on t.distributorid = d.Id " +
                           " where t.admstatus = 2 and s.ServiceClass = 2 ";
            try
            {
                using (SqlConnection conn= new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd= new SqlCommand(query,conn))
                    {
                        TerminalObject to;
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            to= new TerminalObject();
                            to.macAddress = reader.GetString(0);
                            to.termName = reader.GetString(1);
                            to.ispId = reader.GetInt32(2);
                            to.SitId = reader.GetInt32(3);
                            to.distEMail = reader.GetString(4);
                            to.slaName = reader.GetString(5);
                            to.distributorId = reader.GetInt32(6);
                            terminals.Add(to);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                
            }
            return terminals;
        }



        //Add notification to the DB so the user will never be notified twice for the same warning
        public bool RegisterSystemEmailNotif(int IspId,int SitId,int AlertType,DateTime Alerted)
        {
            int retValue = 0;
            _connectionString = Settings.Default.ConnectionString;
            string query =
                "INSERT INTO VolumeConsumptionAlerts (IspId,SitId,AlertType,Alerted) " +
                "VALUES (@IspId ,@SitId,@AlertType,@Alerted )";                                                                               
            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd=new SqlCommand(query,conn))
                    {
                        retValue = SetParam(IspId, SitId, AlertType, Alerted, cmd, retValue);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
            }
            if (retValue > 0)
                return true;
            else
                return false;
        }


        //Once the user got a new volume old notifications should be deleted
        public int DeleteSystemEmailNotif(int IspId,int SitId,int AlertType,DateTime Alerted)
        {
            int retValue = 0;
            _connectionString = Settings.Default.ConnectionString;
            string query =
                "DELETE FROM VolumeConsumptionAlerts WHERE IspId=@IspId AND SitId=@SitId AND AlertType=@AlertType";
                                                                                               
            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd=new SqlCommand(query,conn))
                    {
                        retValue = ParamDeleteNot(IspId, SitId, AlertType, Alerted, cmd, retValue);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
            }
            return retValue;
        }




        private int ParamDeleteNot(int IspId, int SitId, int AlertType, DateTime Alerted, SqlCommand cmd, int retValue)
        {
            cmd.Parameters.Add("@IspId", SqlDbType.Int).Value = IspId;
            cmd.Parameters.Add("@SitId", SqlDbType.Int).Value = SitId;
            cmd.Parameters.Add("@AlertType", SqlDbType.Int).Value = AlertType;
            cmd.Parameters.Add("@Alerted", SqlDbType.DateTime).Value = Alerted;
            cmd.CommandType = CommandType.Text;
            retValue = cmd.ExecuteNonQuery();
            return retValue;
        }



        private int SetParam(int IspId, int SitId, int AlertType, DateTime Alerted, SqlCommand cmd, int retValue)
        {
            cmd.Parameters.Add("@IspId", SqlDbType.Int).Value = IspId;
            cmd.Parameters.Add("@SitId", SqlDbType.Int).Value = SitId;
            cmd.Parameters.Add("@AlertType", SqlDbType.Int).Value = AlertType;
            cmd.Parameters.Add("@Alerted", SqlDbType.DateTime).Value = Alerted;
            cmd.CommandType = CommandType.Text;
            retValue = cmd.ExecuteNonQuery();
            return retValue;
        }

        private void SendEmail()
        {
            GetLogAccountingMonitorValues();
            NMSGatewayService nms = new NMSGatewayService();           
            List<TerminalObject> terminals = ListOfTerminalObjects();
            foreach (TerminalObject t in terminals)
            {
                try
                {                
                    NMSTerminalInfo   nmsTerminal = NmsTerminalInfo(nms, t);
                    Terminal trm = Terminal(t);
                    ServiceLevel serviceLevel = ServiceLevel(trm);
                    
                    string[] emails = t.distEMail.Split(';'); //new string[1];
                    //emails[0]="ahmad.othman@satadsl.net";
                    double CurrentValue = CheckCurrentValue(nmsTerminal, serviceLevel);
                    int SevenFivePrc = Convert.ToInt32(ConfigurationManager.AppSettings["SevenFivePrc"]);
                    int HundredPrc = Convert.ToInt32(ConfigurationManager.AppSettings["HundredPrc"]);

                    if (Int32.Parse(DateTime.Now.Hour.ToString())<=1)
                    {
                        if (((trm.SitId % 28) + 1) == DateTime.Now.Day)
                        {
                            int del = DeleteSystemEmailNotif(trm.IspId, trm.SitId, 1, DateTime.Now);
                            if (del == 1)
                            {
                                Logger.log("1st Warning Notification deleted");
                            }
                            del = DeleteSystemEmailNotif(trm.IspId, trm.SitId, 2, DateTime.Now);
                            if (del == 1)
                            {
                                Logger.log("2nd Notification deleted");
                            }
                        }
                    }

                    if (EqualOrGratherThan(CurrentValue, HundredPrc))
                    {
                        if (emails != null)
                        {
                            try
                            {

                                if (RegisterSystemEmailNotif(trm.IspId, trm.SitId, 2, DateTime.Now))
                                {
                                    this.SendMailAllVolumeChecked(emails, t.termName, t.macAddress);
                                }

                            }
                            catch (Exception ex)
                            {
                                Logger.LogError(ex);
                            }
                        }
                        else
                        {
                            Logger.log(t.macAddress+"has no email address");
                            
                        }
                    }

                    if (EqualOrGratherThan(CurrentValue, SevenFivePrc))
                    {
                        if (emails != null)
                        {
                            try
                            {

                                if (RegisterSystemEmailNotif(trm.IspId, trm.SitId, 1, DateTime.Now))
                                {
                                    this.SendMailVolumeChecked(emails, t.termName, t.macAddress);
                                }
                            }
                            catch (Exception ex)
                            {
                                Logger.LogError(ex);
                            }
                        }
                        else
                        {
                            Logger.log(t.macAddress + "has no email address");
                            
                        }
                    }
       
                   
                }
                catch (Exception ex)
                {
                    Logger.log(ex.Message);
                }                  
            }
        }
        private double CheckCurrentValue(NMSTerminalInfo nmsTerminal, ServiceLevel serviceLevel)
        {
            double CurrentValue = (((nmsTerminal.CurVolRtn) / 1000000000) / ((double) serviceLevel.HighVolumeSUM)) * 100;
            return CurrentValue;
        }
        private ServiceLevel ServiceLevel(Terminal trm)
        {
            ServiceLevel serviceLevel = _boAccountingControlWs.GetServicePack((int) trm.SlaId);
            return serviceLevel;
        }
        private Terminal Terminal(TerminalObject t)
        {
            Terminal trm = _boAccountingControlWs.GetTerminalDetailsByMAC(t.macAddress);
            return trm;
        }
        private NMSTerminalInfo NmsTerminalInfo(NMSGatewayService nms, TerminalObject t)
        {
            NMSTerminalInfo nmsTerminal = nms.getTerminalInfoByMacAddress(t.macAddress);
            return nmsTerminal;
        }
        public void GetLogAccountingMonitorValues()
        {
            userId = Settings.Default.UserID;
            _boLogControl = new BOLogControlWS();
            _boAccountingControlWs = new BOAccountingControlWS();
            _boMonitorControlWs = new BOMonitorControlWS();
        }
        private static bool EqualOrGratherThan(double CurrentValue, double NotifcationPrc)
        {
            return ( CurrentValue >= NotifcationPrc);
        }




        public bool SendMailVolumeChecked(string[] emails, string termName, string macAddress)
        {
            bool success = false;
            string body ="<p>Dear customer,</ p >" +
                         "<p>Please be advised that you have consumed 85% of the monthly volume allocated to your subscription on terminal: </br> </p> <p><b>" + termName + " MAC " + macAddress + "</b>.</br></p> <p>. We suggest you keep an eye on your consumption by looking at MySatADSL space on our website at www.satadsl.net. </p>" +
                         "<p>Please do not hesitate to contact your local distributor for further details or to purchase a manual reset.</p>" +
                         "<p></br></p>" +
                         "<p>Best regards, </br></p>" +
                         "<p>The SatADSL Team </br></p>" +
                         "<p>--------</br></p>" +
                         "<p>   </br></p>"+
                        "<p>Cher Client,</ p >" +
                         "<p>Notez que vous avez consommé 85% de votre allocation mensuelle de volume.pour le terminal: </br> </p> <p><b>" + termName + " MAC " + macAddress + "</b>.</br></p> <p>. Nous vous suggérons de surveiller votre consommation en regardant votre espace MySatADSL sur notre site Internet www.satadsl.net. </p>" +
                         "<p>N'hésitez pas à contacter votre distributeur local pour plus de détails ou pour acheter une remise à zéro manuelle des compteurs.</p>" +
                         "<p></br></p>" +
                         "<p>Cordialement, </br></p>" +
                         "<p>L'équipe SatADSL  </br></p>" +
                         "<p>--------</br></p>" +
                         "<p>   </br></p>"+
                        "<p>Estimado cliente,</ p >" +
                         "<p>Tenha em atenção que consumiu 85% do volume mensal atribuído à sua assinatura no terminal: </br> </p> <p><b>" + termName + " MAC " + macAddress + "</b>.</br></p> <p>. Sugerimos que verifique o seu consumo, consultando o espaço MySatADSL no nosso sitio internet em www.satadsl.net. </p>" +
                         "<p>Não hesite em contactar o seu distribuidor local para mais detalhes ou para comprar uma reinicialização manual.</p>" +
                         "<p></br></p>" +
                         "<p>Atentamente, </br></p>" +
                         "<p>SatADSL </br></p>" +
                         "<p>--------</br></p>" +
                         "<p>   </br></p>";

            string subject = "SatADSL volume Notification 85%  ";
            try
            {
                if (_boLogControl.SendMail(body, subject, emails))
                {
                    String.Join(",", emails);
                    Logger.log(" 85% Notification email sent to "+emails+"for terminal " +macAddress);
 
                    success = true;
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
            }
            return success;
        }



        public bool SendMailAllVolumeChecked(string[] emails, string termName, string macAddress)
        {
            bool success = false;
            string body = "<p>Dear customer,</p>" +
                          "<p>Please be advised that you have reached the limit of the monthly volume allocated to your subscription on terminal: </br> </p> <p><b>" + termName + " MAC " + macAddress + "</b>.</br></p> <p>. This means that your data rate is throttled down until next automatic reset. You can check such date on MySatADSL space on our website www.satadsl.net </p>" +
                          "<p>Please do not hesitate to contact your local distributor for further details or to purchase a manual reset.</p>" +
                          "<p></br></p>" +
                          "<p>Best regards, </br></p>" +
                          "<p>The SatADSL Team </br></p>" +
                          "<p>--------</br></p>" +
                          "<p>   </br></p>"+
                        "<p>Cher Client,</p>" +
                          "<p>Notez que vous avez atteint la limite de votre allocation de volume mensuelle pour le terminal:  </br> </p> <p><b>" + termName + " MAC " + macAddress + "</b>.</br></p> <p>. Cela signifie que votre connexion est bridée en vitesse jusqu'à la date de la prochaine remise à zéro automatique. Vous pouvez consulter cette date en regardant votre espace MySatADSL sur notre site Internet www.satadsl.net.</p>" +
                          "<p>N'hésitez pas à contacter votre distributeur local pour plus de détails ou pour acheter une remise à zéro manuelle des compteurs.</p>" +
                          "<p></br></p>" +
                          "<p>Cordialement, </br></p>" +
                          "<p>L'équipe SatADSL  </br></p>" +
                          "<p>--------</br></p>" +
                          "<p>   </br></p>"+
                        "<p>Estimado cliente,</p>" +
                          "<p>Tenha em atenção que você atingiu o limite do volume mensal atribuído à sua assinatura no terminal: </br> </p> <p><b>" + termName + " MAC " + macAddress + "</b>.</br></p> <p>. Isso significa que sua velocidade foi reduzida até à próxima reinicialização automática dos contadores de volume. Poderá verificar a data exacta de reinicialização através do espaço cliente MySatADSL no nosso sitio internet www.satadsl.net. </p>" +
                          "<p>Não hesite em contactar o seu distribuidor local para mais detalhes ou para comprar uma reinicialização manual.</p>" +
                          "<p></br></p>" +
                          "<p>Atentamente, </br></p>" +
                          "<p>SatADSL </br></p>" +
                          "<p>--------</br></p>" +
                          "<p>   </br></p>";

            string subject = "SatADSL volume Notification 100%";
            try
            {
                if (_boLogControl.SendMail(body, subject, emails))
                {
                    String.Join(",", emails);
                    Logger.log(" 100% Notification email sent to "+emails+"for terminal " +macAddress);
 
                    success = true;
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
            }

            return success;
        }


        public class TerminalObject
        {
            public string macAddress { get; set; }
            public string termName { get; set; }
            public int ispId { get; set; }
            public int SitId { get; set; }
            public string distEMail { get; set; }        
            public string slaName { get; set; }
            public int distributorId { get; set; }
        }

    }
}
