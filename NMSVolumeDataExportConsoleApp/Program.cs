﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace NMSVolumeDataExportConsoleApp
{
    /// <summary>
    /// This app will collect the current volume data from the NMS db and
    /// output a CSV-file
    /// Transferring the file to another location is not in the scope of this app.
    /// That should be handled by the script executing the app.
    /// </summary>
    class Program
    {
        const bool _DEBUG = false;
        enum OperatingSystem { Windows, Linux }
        const OperatingSystem _OS = OperatingSystem.Linux;
        const string _FILEPATH_WINDOWS = "C:/Temp/";
        const string _FILEPATH_LINUX = "/home/erwin/volume_data_script/";
        const string _connectionStringDebug = "Server=localhost;database=NMS-debug;user=root;password=ebbe130561";
        const string _connectionString = "Server=192.168.100.100;database=NMS;user=SatADSL;password=Fulvio";
        const string delimiter = ";";
        
        static void Main(string[] args)
        {
            Program prg = new Program();
            prg.mainBody(args[0]);
        }

        private void mainBody(string fileName)
        {
            List<TerminalVolume> terminalVolumes = new List<TerminalVolume>();
            string filePath = _FILEPATH_LINUX;
            string connectionString = _connectionString;
            string timeStamp = DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss");
            
            if (_OS == OperatingSystem.Windows)
            {
                filePath = _FILEPATH_WINDOWS;
            }

            if (_DEBUG)
            {
                connectionString = _connectionStringDebug;
            }

            using (MySqlConnection con = new MySqlConnection(connectionString))
            {
                TerminalVolume tv;
                con.Open();
                try
                {
                    //fetch the volume data from the NMS db
                    string queryCmd = "SELECT MacAddress, CurVolFWD, CurVolRTN, VolFWD, VolRTN, Isp, SitId, FreeZoneFWD, FreeZoneRTN FROM Terminals";
                    using (MySqlCommand command = new MySqlCommand(queryCmd, con))
                    {
                        MySqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            tv = new TerminalVolume();
                            tv.MacAddress = reader.GetInt64(0).ToString();
                            tv.AccVolFWD = reader.GetInt64(1);
                            tv.AccVolRTN = reader.GetInt64(2);
                            tv.PrevVolFWD = reader.GetInt64(3);
                            tv.PrevVolRTN = reader.GetInt64(4);
                            tv.IspId = reader.GetInt32(5);
                            tv.SitId = reader.GetInt32(6);
                            tv.FreeZoneFWD = reader.GetInt64(7);
                            tv.FreeZoneRTN = reader.GetInt64(8);
                            terminalVolumes.Add(tv);
                        }
                    }
                }
                catch (Exception ex)
                {
                    fileName = "Error.log";
                    using (StreamWriter writer = new StreamWriter(filePath + fileName, true))
                    {
                        writer.WriteLine("[" + DateTime.UtcNow.ToString() + "]" + ex.Message);
                    }
                    return;
                }
            }
            
            //write top row of CSV-file
            using (StreamWriter writer = new StreamWriter(filePath + fileName, true))
            {
                writer.WriteLine("Isp" + delimiter + "SitId" + delimiter + "Timestamp" + delimiter + "MacAddress" + delimiter + "RTVolFWD" + delimiter + "RTVolRTN" + delimiter + "CurVolFWD" + delimiter + "CurVolRTN" + delimiter + "FreeZoneFWD" + delimiter + "FreeZoneRTN");
            }

            using (MySqlConnection con = new MySqlConnection(connectionString))
            {
                try
                {
                    string updateCmd = "UPDATE Terminals SET VolFWD = @VolFWD, VolRTN = @VolRTN WHERE MacAddress = @MacAddress";
                    foreach (TerminalVolume t in terminalVolumes)
                    {
                        // realtime data is current volume minus previous volume
                        // set negative values to 0 to compensate for volume resets
                        t.RTVolFWD = t.AccVolFWD - t.PrevVolFWD;
                        if (t.RTVolFWD < 0)
                        {
                            t.RTVolFWD = 0;
                        }
                        t.RTVolRTN = t.AccVolRTN - t.PrevVolRTN;
                        if (t.RTVolRTN < 0)
                        {
                            t.RTVolRTN = 0;
                        }
                        // write current data to NMS db
                        con.Open();
                        using (MySqlCommand command = new MySqlCommand(updateCmd, con))
                        {
                            command.Parameters.AddWithValue("@MacAddress", t.MacAddress);
                            command.Parameters.AddWithValue("@VolFWD", t.AccVolFWD);
                            command.Parameters.AddWithValue("@VolRTN", t.AccVolRTN);
                            command.ExecuteNonQuery();
                        }
                        con.Close();
                        //write data to CSV-file
                        using (StreamWriter writer = new StreamWriter(filePath + fileName, true))
                        {
                            writer.WriteLine(t.IspId + delimiter + t.SitId + delimiter + timeStamp + delimiter + t.MacAddress + delimiter + t.RTVolFWD + delimiter + t.RTVolRTN + delimiter + t.AccVolFWD + delimiter + t.AccVolRTN + delimiter + t.FreeZoneFWD + delimiter + t.FreeZoneRTN);
                        }
                    }
                }
                catch (Exception ex)
                {
                    fileName = "Error.log";
                    using (StreamWriter writer = new StreamWriter(filePath + fileName, true))
                    {
                        writer.WriteLine("[" + DateTime.UtcNow.ToString() + "]" + ex.Message);
                    }
                    return;
                }
            }
        }

        /// <summary>
        /// This class is used to create a terminal volume object
        /// containing only the values necessary for the volume data
        /// </summary>
        private class TerminalVolume
        {
            public int IspId { get; set; }
            public int SitId { get; set; }
            public string MacAddress { get; set; }
            public Int64 AccVolRTN { get; set; }
            public Int64 AccVolFWD { get; set; }
            public Int64 PrevVolRTN { get; set; }
            public Int64 PrevVolFWD { get; set; }
            public Int64 RTVolRTN { get; set; }
            public Int64 RTVolFWD { get; set; }
            public Int64 FreeZoneRTN { get; set; }
            public Int64 FreeZoneFWD { get; set; }
        }
    }
}
