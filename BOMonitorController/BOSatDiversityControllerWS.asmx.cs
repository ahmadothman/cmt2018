﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Configuration;
using BOControllerLibrary.Model.Terminal;
using BOControllerLibrary.Controllers;
using BOControllerLibrary.Controllers.Interfaces;

namespace BOMonitorController
{
    /// <summary>
    /// This interface describes the methods for satellite diversity management
    /// (redundant terminal methods)
    /// This method deals only with the specifics of satellite diversity: existing terminal management
    /// methods as still used for as far as possible also for redundant terminals
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class BOSatDiversityControllerWS : System.Web.Services.WebService, IBOSatDiversityController
    {
        IBOSatDiversityController _controller;

        public BOSatDiversityControllerWS()
        {
            _controller = new BOSatDiversityController(
                connectionString: ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString,
                databaseProvider: ConfigurationManager.ConnectionStrings["ApplicationServices"].ProviderName);
        }
        
        /// <summary>
        /// Sets a terminal to active and sets the other terminal in the redundant setup to inactive
        /// </summary>
        /// <param name="macAddress">The MAC address of the terminal to be set as active</param>
        /// <returns>True if successful. Also returns true in case a non-redundant terminal is provided, or if the the terminal provided already was active</returns>
        [WebMethod]
        public bool SetActiveTerminal(string macAddress)
        {
            return _controller.SetActiveTerminal(macAddress);
        }

        /// <summary>
        /// Stores the switchover event in the CMT database
        /// </summary>
        /// <param name="switchOver">The details of the switchOver</param>
        /// <returns>True if successful</returns>
        [WebMethod]
        public bool LogSwitchOver(SwitchOver switchOver)
        {
            return _controller.LogSwitchOver(switchOver);
        }

        /// <summary>
        /// Fetches all switchover events in the database during a give timeperiod
        /// </summary>
        /// <param name="startDate">The start date of the period</param>
        /// <param name="endDate">The end date of the period</param>
        /// <returns>A list of switchover events</returns>
        [WebMethod]
        public List<SwitchOver> GetAllSwitchOvers(DateTime startDate, DateTime endDate)
        {
            return _controller.GetAllSwitchOvers(startDate, endDate);
        }

        /// <summary>
        /// Fetches all switchover events in the database during a give timeperiod
        /// </summary>
        /// <param name="macAddress">The MAC address of one of the terminals in the redundant setup</param>
        /// <param name="startDate">The start date of the period</param>
        /// <param name="endDate">The end date of the period</param>
        /// <returns>A list of switchover events</returns>
        [WebMethod]
        public List<SwitchOver> GetSwitchOversForTerminal(string macAddress, DateTime startDate, DateTime endDate)
        {
            return _controller.GetSwitchOversForTerminal(macAddress, startDate, endDate);
        }

        /// <summary>
        /// Fetches all switchover events in the database during a give timeperiod for a specific distributor
        /// </summary>
        /// <param name="startDate">The start date of the period</param>
        /// <param name="endDate">The end date of the period</param>
        /// <param name="distributorId">The ID of the distributor</param>
        /// <returns>A list of switchover events</returns>
        [WebMethod]
        public List<SwitchOver> GetSwitchOversForDistributor(DateTime startDate, DateTime endDate, int distributorId)
        {
            return _controller.GetSwitchOversForDistributor(startDate, endDate, distributorId);
        }

        /// <summary>
        /// Fetches all switchover events in the database during a give timeperiod for a specific customer
        /// </summary>
        /// <param name="startDate">The start date of the period</param>
        /// <param name="endDate">The end date of the period</param>
        /// <param name="customerId">The ID of the customer</param>
        /// <returns>A list of switchover events</returns>
        [WebMethod]
        public List<SwitchOver> GetSwitchOversForCustomer(DateTime startDate, DateTime endDate, int customerId)
        {
            return _controller.GetSwitchOversForCustomer(startDate, endDate, customerId);
        }
    }
}
