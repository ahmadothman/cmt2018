﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Configuration;
using BOControllerLibrary.Controllers.Interfaces;
using BOControllerLibrary.Controllers;
using BOControllerLibrary.Model.MultiCast;

namespace BOMonitorController
{
    /// <summary>
    /// Summary description for BOMultiCastGroupController
    /// </summary>
    [WebService(Namespace = "http://cmt.nimera.net/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class BOMultiCastGroupControllerWS : System.Web.Services.WebService, IBOMultiCastGroupController
    {
        private IBOMultiCastGroupController _controller;

        public BOMultiCastGroupControllerWS()
        {
            // in time we'll use dependency injection here for lifetime and implementation activation.
            _controller = new BOControllerLibrary.Controllers.BOMultiCastGroupController(
                connectionString: ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString,
                databaseProvider: ConfigurationManager.ConnectionStrings["ApplicationServices"].ProviderName);
        }

        /// <summary>
        /// Creates the given MultiCastGroup
        /// </summary>
        /// <remarks>
        /// This method first tries to update the Multicast group in the NMS. If this is done
        /// successfully, the Multicast group data is updated in the CMT database
        /// </remarks>
        /// <param name="mc">The Multicast group to update</param>
        /// <returns>True if the Multicast group is successfully created</returns>
        [WebMethod]
        public bool CreateMultiCastGroup(BOControllerLibrary.Model.MultiCast.MultiCastGroup mc)
        {
            return _controller.CreateMultiCastGroup(mc);
        }

        /// <summary>
        /// Updates the given Multicast group in the CMT database AND the NMS
        /// </summary>
        /// <param name="mc">The Multicast group to update</param>
        /// <returns>True if the operation succeeded</returns>
        [WebMethod]
        public bool UpdateMultiCastGroup(BOControllerLibrary.Model.MultiCast.MultiCastGroup mc)
        {
            return _controller.UpdateMultiCastGroup(mc);
        }

        /// <summary>
        /// Deletes the Multicast group from the CMT and NMS databases.
        /// </summary>
        /// <remarks>
        /// The record in the CMT is not completely deleted but on flagged as deleted
        /// </remarks>
        /// <param name="mc">The Multicast group to delete</param>
        /// <returns>True if the MC group was successfully deleted</returns>
        [WebMethod]
        public bool DeleteMultiCastGroup(BOControllerLibrary.Model.MultiCast.MultiCastGroup mc)
        {
            return _controller.DeleteMultiCastGroup(mc);
        }

        /// <summary>
        /// Retrieves a Multicast group identified by its IP address from the
        /// CMT Database
        /// </summary>
        /// <param name="IPAddress">The IP Address of the Multicast group</param>
        /// <returns>A valid Multicast group object or NULL if not found</returns>
        [WebMethod]
        public BOControllerLibrary.Model.MultiCast.MultiCastGroup GetMultiCastGroupByIP(string IPAddress)
        {
            return _controller.GetMultiCastGroupByIP(IPAddress);
        }

        /// <summary>
        /// Returns the bytes transferred for the given MC from the NMS
        /// </summary>
        /// <param name="IPAddress">The IP Address of the MC group</param>
        /// <param name="IspId">The ISP Id which identifies the NMS</param>
        /// <returns>The Bytes retrieved as an unsigned long</returns>
        [WebMethod]
        public ulong GetBytesTransferredForMC(string IPAddress, int IspId)
        {
            return _controller.GetBytesTransferredForMC(IPAddress, IspId);
        }

        /// <summary>
        /// Returns true if the Multicast group identified by its IP Address
        /// is true
        /// </summary>
        /// <param name="IPAddress">The MC IP Address</param>
        /// <param name="IspId">The ISP Id which identifies the NMS</param>
        /// <returns>True if the MC is enabled</returns>
        [WebMethod]
        public bool IsEnabled(string IPAddress, int IspId)
        {
            return _controller.IsEnabled(IPAddress, IspId);
        }

        /// <summary>
        /// Checks if the Multicast group exists in the CMT Database
        /// </summary>
        /// <param name="IPAddress">The MC IP Address</param>
        /// <returns>True if the MC exists</returns>
        [WebMethod]
        public bool MultiCastGroupExists(string IPAddress)
        {
            return _controller.MultiCastGroupExists(IPAddress);
        }

        /// <summary>
        /// Returns a list of all Multicast groups contained by the MulticastGroups table
        /// </summary>
        /// <remarks>
        /// This method only brings back the non-deleted MC groups
        /// </remarks>
        /// <returns>A list of MultiCastGroup elements. This list can be empty</returns>
        [WebMethod]
        public List<BOControllerLibrary.Model.MultiCast.MultiCastGroup> GetMultiCastGroups()
        {
            return _controller.GetMultiCastGroups();
        }

        /// <summary>
        /// Returns a list of all multicast groups connected to a specific user
        /// </summary>
        /// <param name="userId">The ID of the user</param>
        /// <returns>A list of MultiCastGroup elements. This list can be empty</returns>
        [WebMethod]
        public List<MultiCastGroup> GetMultiCastGroupsByUser(string userId)
        {
            return _controller.GetMultiCastGroupsByUser(userId);
        }

        /// <summary>
        /// Returns the MultiCastGroup by its MAC address
        /// </summary>
        /// <remarks>
        /// This value can be NULL if the MC group is not found
        /// </remarks>
        /// <param name="macAddress">The MAC address</param>
        /// <returns>The MultiCastGroup or null if not found</returns>
        [WebMethod]
        public MultiCastGroup GetMultiCastGroupByMac(string macAddress)
        {
            return _controller.GetMultiCastGroupByMac(macAddress);
        }

        /// <summary>
        /// Enables a Multicast group
        /// </summary>
        /// <param name="ipAddress"></param>
        [WebMethod]
        public Boolean EnableMultiCastGroup(string ipAddress, string macAddress, int ispId, Guid userId)
        {
            return _controller.EnableMultiCastGroup(ipAddress, macAddress, ispId, userId);
        }

        /// <summary>
        /// Disables the Multicast group
        /// </summary>
        /// <param name="ipAddress"></param>
        /// <returns></returns>
        [WebMethod]
        public Boolean DisableMultiCastGroup(string ipAddress, string macAddress, int IspId, Guid userId)
        {
            return _controller.DisableMultiCastGroup(ipAddress, macAddress, IspId, userId);
        }

        [WebMethod]
        public Boolean UpdateMultiCastGroupSchedule(BOControllerLibrary.Model.MultiCast.MultiCastGroupSchedule mcs)
        {
            return _controller.UpdateMultiCastGroupSchedule(mcs);
        }

        [WebMethod]
        public Boolean DeleteMultiCastGroupSchedule(MultiCastGroupSchedule mcs)
        {
            return _controller.DeleteMultiCastGroupSchedule(mcs);
        }

        [WebMethod]
        public Boolean CreateMultiCastGroupSchedule(BOControllerLibrary.Model.MultiCast.MultiCastGroupSchedule mcs, List<MultiCastGroupSchedule> scheduleList)
        {
            return _controller.CreateMultiCastGroupSchedule(mcs, scheduleList);
        }

        [WebMethod]
        public List<MultiCastGroupSchedule> GetMultiCastGroupSchedules()
        {
            return _controller.GetMultiCastGroupSchedules();
        }

        /// <summary>
        /// Returns all schedules for a specific multicast group
        /// </summary>
        /// <param name="id">The ID of the multicast group</param>
        /// <returns>A list of multicast group schedules. Can be null</returns>
        [WebMethod]
        public List<MultiCastGroupSchedule> GetMulticastSchedulesByMCGroup(int id)
        {
            return _controller.GetMulticastSchedulesByMCGroup(id);
        }

        [WebMethod]
        public MultiCastGroup GetMultiCastGroupByID(int id)
        {
            return _controller.GetMultiCastGroupByID(id);
        }

        [WebMethod]
        public MultiCastGroupSchedule GetMultiCastGroupScheduleByID(int id)
        {
            return _controller.GetMultiCastGroupScheduleByID(id);
        }

        /// <summary>
        /// Method for creating a recurring multicast group schedule
        /// First create the recurrent event entry and the creates the separate scheduled events
        /// </summary>
        /// <param name="mcs">The data of the first scheduled event</param>
        /// <param name="mcr">The data of the overall recurrent event</param>
        /// <returns>True if successful</returns>
        [WebMethod]
        public bool CreateRecurrentMultiCastGroupSchedule(MultiCastGroupSchedule mcs, MultiCastRecurrentEvents mcr)
        {
            return _controller.CreateRecurrentMultiCastGroupSchedule(mcs, mcr);
        }

        /// <summary>
        /// Updates a set of recurrent events in the CMT db
        /// </summary>
        /// <param name="mcs">The single multicast schedule that was edited</param>
        /// <param name="mcr">The recurrent event</param>
        /// <returns>true if successful</returns>
        [WebMethod]
        public bool UpdateRecurrentMultiCastGroupSchedule(MultiCastGroupSchedule mcs, MultiCastRecurrentEvents mcr)
        {
            return _controller.UpdateRecurrentMultiCastGroupSchedule(mcs, mcr);
        }

        [WebMethod]
        public MultiCastRecurrentEvents GetMulticastRecurrentEventById(int id)
        {
            return _controller.GetMulticastRecurrentEventById(id);
        }

        /// <summary>
        /// Method for creating a new multicast source
        /// </summary>
        /// <param name="source">The multicast source object to be added to the database</param>
        /// <returns>True if successful</returns>
        [WebMethod]
        public bool AddMultiCastSource(MultiCastSource source)
        {
            return _controller.AddMultiCastSource(source);
        }

        /// <summary>
        /// Deletes a multicast source from the database
        /// </summary>
        /// <param name="sourceID">The ID of the multicast source to be deleted</param>
        /// <returns>True if successful</returns>
        [WebMethod]
        public bool DeleteMultiCastSource(MultiCastSource src)
        {
            return _controller.DeleteMultiCastSource(src);
        }

        /// <summary>
        /// Updates a multicast source in the database
        /// </summary>
        /// <param name="source">The multicast object with the new paramaters</param>
        /// <returns>True if successful</returns>
        [WebMethod]
        public bool UpdateMultiCastSource(MultiCastSource source)
        {
            return _controller.UpdateMultiCastSource(source);
        }

        /// <summary>
        /// Fetches a specific multicast source
        /// </summary>
        /// <param name="sourceID">The ID of the multicast source to be fetched</param>
        /// <returns>A multicast source object</returns>
        [WebMethod]
        public MultiCastSource GetMultiCastSourceByID(int sourceID)
        {
            return _controller.GetMultiCastSourceByID(sourceID);
        }

        /// <summary>
        /// Fetches all multicast sources in the database
        /// </summary>
        /// <returns>A list of multicast source objects</returns>
        [WebMethod]
        public List<MultiCastSource> GetAllMultiCastSources()
        {
            return _controller.GetAllMultiCastSources();
        }

        /// <summary>
        /// Fetches all multicast sources for a specific multicast group
        /// </summary>
        /// <param name="multiCastGroupID">The ID of the multicast group for which sources should be fetched</param>
        /// <returns>A list of multicast source objects</returns>
        [WebMethod]
        public List<MultiCastSource> GetMultiCastSourcesByMCGroupID(int multiCastGroupID)
        {
            return _controller.GetMultiCastSourcesByMCGroupID(multiCastGroupID);
        }

        [WebMethod]
        public List<BOControllerLibrary.Model.MultiCast.MultiCastLog> GetMultiCastLogsBetweenDateTimes(DateTime startDate, DateTime endDate, string macAddress)
        {
            return _controller.GetMultiCastLogsBetweenDateTimes(startDate, endDate, macAddress);
        }

        [WebMethod]
        public List<BOControllerLibrary.Model.MultiCast.MultiCastLog> GetMultiCastLogsInLastHour(string macAddress) 
        {
            return _controller.GetMultiCastLogsInLastHour(macAddress);
        }

        [WebMethod]
        public BOControllerLibrary.Model.MultiCast.MultiCastLog GetLatestMultiCastLog(string macAddress) 
        {
            return _controller.GetLatestMultiCastLog(macAddress);
        }

        //[WebMethod]
        //public MultiCastLog GetLatestMultiCastLog2(string macAddress) 
        //{
        //    return _controller.GetLatestMultiCastLog2(macAddress);
        //}

        /// <summary>
        /// Gets all MultiCastGroupSchedules available in the CMT DB, between two given DateTimes.
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        /// SATCORP-52 will use this method in the NMSMultiCastLogSync console app, to get MultiCastGroupSchedules form CMT DB
        [WebMethod]
        public List<MultiCastGroupSchedule> GetAllMultiCastGroupSchedulesBetweenDateTimes(DateTime startDate, DateTime endDate) 
        {
            return _controller.GetAllMultiCastGroupSchedulesBetweenDateTimes(startDate, endDate);
        }

    }
}
