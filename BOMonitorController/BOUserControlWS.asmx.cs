﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using BOControllerLibrary.Model;
using BOControllerLibrary.Controllers;
using BOControllerLibrary.Model.Log;

namespace BOMonitorController
{
    /// <summary>
    /// Summary description for BOUserControlWS
    /// </summary>
    [WebService(Namespace = "http://cmt.nimera.net/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class BOUserControlWS : System.Web.Services.WebService
    {
        string _GetSystemUsers =
          @"SELECT u.UserId FROM aspnet_Users AS u
            WHERE	NOT EXISTS (SELECT du.UserId 
					            FROM DeletedUsers AS du
					            WHERE u.UserId = du.UserId)
            ORDER BY u.UserName";
        string _DistributorsUserInsert =
            "INSERT INTO DistributorsUsers (UserId, DistributorId) VALUES (@UserId, @DistributorId)";
        string _OrganizationsUserInsert =
            "INSERT INTO OrganizationsUsers (UserId, OrganizationId) VALUES (@UserId, @OrganizationId)";
        string _EndUsersInsert =
            "INSERT INTO EndUserUsers (UserId, EndUserId) VALUES (@UserId, @EndUserId)";
        string _SystemUserDelete =
          @"INSERT INTO DeletedUsers (UserId, UserName) VALUES (@UserId, @UserName)
            UPDATE aspnet_Membership
            SET IsLockedOut = 1, LastLockoutDate = @LastLockoutDate
            WHERE UserId = @UserId";
        string _IsUserDeleted =
            "SELECT * FROM DeletedUsers WHERE UserId = @UserId";

        BOLogController _logger = null;

        /// <summary>
        /// Default constructor
        /// </summary>
        public BOUserControlWS()
        {
            _logger = new BOLogController(
               connectionString: ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString,
               databaseProvider: ConfigurationManager.ConnectionStrings["ApplicationServices"].ProviderName);
        }

        [WebMethod]
        public List<Guid> GetUsers()
        {
            List<Guid> usersList = new List<Guid> { };

            using (SqlConnection conn = new SqlConnection(this.getConnectionString()))
            {
                conn.Open();
                try
                {
                    using (SqlCommand command = new SqlCommand(_GetSystemUsers, conn))
                    {
                        SqlDataReader reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            Guid userId = new Guid();

                            if (!reader.IsDBNull(0))
                                userId = reader.GetGuid(0);

                            usersList.Add(userId);
                        }
                    }
                }
                catch (Exception ex)
                {
                    BOLogControlWS boLogControlWS = new BOLogControlWS();
                    CmtApplicationException cmtEx = new CmtApplicationException();
                    cmtEx.ExceptionDesc = ex.Message;
                    cmtEx.ExceptionStacktrace = ex.StackTrace;
                    boLogControlWS.LogApplicationException(cmtEx);
                    usersList = null;
                }
            }

            return usersList;
        }

        /// <summary>
        /// Connects a system user to a distributor.
        /// </summary>
        /// <remarks>
        /// Users are in the Distributor role and when authenticated are
        /// linked to the given Distributor
        /// </remarks>
        /// <param name="UserId">The unique identifier of the user</param>
        /// <param name="distributorId">The related Distributor</param>
        /// <returns>True if the method succeeds</returns>
        [WebMethod]
        public bool ConnectUserToDistributor(Guid userId, int distributorId)
        {
            bool result = false;
            
            using (SqlConnection con = new SqlConnection(this.getConnectionString()))
            {
                con.Open();
                try
                {
                    using (SqlCommand command = new SqlCommand(_DistributorsUserInsert, con))
                    {
                        command.Parameters.Add("@UserId", SqlDbType.UniqueIdentifier);
                        command.Parameters["@UserId"].Value = userId;
                        command.Parameters.Add("@DistributorId", SqlDbType.Int);
                        command.Parameters["@DistributorId"].Value = distributorId;
                        int numRecords = command.ExecuteNonQuery();

                        if (numRecords == 1)
                        {
                            result = true;
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
                catch (Exception ex)
                {
                    result = false;
                    CmtApplicationException cmtException = new CmtApplicationException();
                    cmtException.ExceptionDesc = ex.Message;
                    cmtException.ExceptionDateTime = DateTime.Now;
                    cmtException.UserDescription = 
                            "ConnectUserToDistributor method failed; User=" + userId + "; Distributor=" + distributorId;
                    cmtException.ExceptionLevel = (short)ExceptionLevelEnum.Critical;
                    _logger.LogApplicationException(cmtException);
                }
            }
            return result;
        }

        /// <summary>
        /// Connects a system uer to a specific customer (organization)
        /// </summary>
        /// <param name="userId">The user Id of the system user (GUID)</param>
        /// <param name="OrgId">The unique identifier of the Organization</param>
        /// <returns>True if the method succeeds</returns>
        [WebMethod]
        public bool ConnectUserToCustomer(Guid userId, int orgId)
        {
            bool result = false;

            using (SqlConnection con = new SqlConnection(this.getConnectionString()))
            {
                con.Open();
                try
                {
                    using (SqlCommand command = new SqlCommand(_OrganizationsUserInsert, con))
                    {
                        command.Parameters.Add("@UserId", SqlDbType.UniqueIdentifier);
                        command.Parameters["@UserId"].Value = userId;
                        command.Parameters.Add("@OrganizationId", SqlDbType.Int);
                        command.Parameters["@OrganizationId"].Value = orgId;
                        int numRecords = command.ExecuteNonQuery();

                        if (numRecords == 1)
                        {
                            result = true;
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
                catch (Exception ex)
                {
                    CmtApplicationException cmtException = new CmtApplicationException();
                    cmtException.ExceptionDesc = ex.Message;
                    cmtException.ExceptionDateTime = DateTime.Now;
                    cmtException.UserDescription = "ConnectUserToCustomer method failed; UserId=" + userId + "; CustomerId=" + orgId;
                    cmtException.ExceptionLevel = (short)ExceptionLevelEnum.Critical;
                    _logger.LogApplicationException(cmtException);
                    result = false;
                }
            }
            return result;
        }

        /// <summary>
        /// Connects a system user to an End User entity
        /// </summary>
        /// <remarks>
        /// End User entities are owners of a terminal, so a user is in essence
        /// connected to a terminal
        /// </remarks>
        /// <param name="userId">The user Id of the system user</param>
        /// <param name="EndUserId">Unique identifier of the end user</param>
        /// <returns>True if the method succeeds</returns>
        [WebMethod]
        public bool ConnectUserToEndUser(Guid userId, int EndUserId)
        {
            bool result = false;

            using (SqlConnection con = new SqlConnection(this.getConnectionString()))
            {
                con.Open();
                try
                {
                    using (SqlCommand command = new SqlCommand(_EndUsersInsert, con))
                    {
                        command.Parameters.Add("@UserId", SqlDbType.UniqueIdentifier);
                        command.Parameters["@UserId"].Value = userId;
                        command.Parameters.Add("@EndUserId", SqlDbType.Int);
                        command.Parameters["@EndUserId"].Value = EndUserId;
                        int numRecords = command.ExecuteNonQuery();

                        if (numRecords == 1)
                        {
                            result = true;
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
                catch (Exception ex)
                {
                    CmtApplicationException cmtException = new CmtApplicationException();
                    cmtException.ExceptionDesc = ex.Message;
                    cmtException.ExceptionDateTime = DateTime.Now;
                    cmtException.UserDescription = "ConnectUserToEndUser method failed; UserId=" + userId + "; EndUserId=" + EndUserId;
                    cmtException.ExceptionLevel = (short)ExceptionLevelEnum.Critical;
                    _logger.LogApplicationException(cmtException);
                    result = false;
                }
            }
            return result;
        }

        /// <summary>
        /// This method deletes the user from the link tables
        /// </summary>
        /// <remarks>
        /// Link tables are:
        /// <li>
        ///     <li>EndUsersUsers</li>
        ///     <li>OrganizationUsers</li>
        ///     <li>DistributorsUsers</li>
        /// </li>
        /// </remarks>
        /// <param name="userId">The user to "deconnect"</param>
        /// <returns>True if the user is deconnected</returns>
        [WebMethod]
        public bool DeconnectUser(Guid userId)
        {
            bool result = false;

            using (SqlConnection conn = new SqlConnection(this.getConnectionString()))
            {
                SqlCommand cmd = new SqlCommand("cmtDeconnectUser", conn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.UniqueIdentifier));
                cmd.Parameters["@UserId"].Value = userId;
                conn.Open();
                try
                {
                    cmd.ExecuteNonQuery();
                    result = true;
                }
                catch (Exception ex)
                {
                    BOLogControlWS boLogControlWS = new BOLogControlWS();
                    CmtApplicationException cmtApplicationException = new CmtApplicationException(ex, "DeconnectUser failed", "Unable to delete a system user. User ID: " + userId, (short)ExceptionLevelEnum.Error);
                    boLogControlWS.LogApplicationException(cmtApplicationException);
                    result = false;
                }
            }
            return result;
        }

        /// <summary>
        /// This method adds the user to the DeletedUsers table
        /// </summary>
        /// <param name="userId">The user to "delete"</param>
        /// <returns>True if the user is "deleted"</returns>
        [WebMethod]
        public bool DeleteUser(Guid userId, string username, DateTime lastLockoutDate)
        {
            bool result = false;
            
            if (!this.IsUserDeleted(userId))
            {
                using (SqlConnection conn = new SqlConnection(this.getConnectionString()))
                {
                    conn.Open();
                    try
                    {
                        using (SqlCommand command = new SqlCommand(_SystemUserDelete, conn))
                        {
                            command.Parameters.Add("@LastLockoutDate", SqlDbType.DateTime);
                            command.Parameters["@LastLockoutDate"].Value = lastLockoutDate;
                            command.Parameters.Add("@UserId", SqlDbType.UniqueIdentifier);
                            command.Parameters["@UserId"].Value = userId;
                            command.Parameters.Add("@UserName", SqlDbType.NVarChar);
                            command.Parameters["@UserName"].Value = username;
                            int numRecords = command.ExecuteNonQuery();

                            if (numRecords == 2)
                            {
                                result = true;
                            }
                            else
                            {
                                result = false;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        CmtApplicationException cmtException = new CmtApplicationException();
                        cmtException.ExceptionDesc = ex.Message;
                        cmtException.ExceptionDateTime = DateTime.Now;
                        cmtException.UserDescription = "DeleteUser method failed; UserId=" + userId;
                        cmtException.ExceptionLevel = (short)ExceptionLevelEnum.Critical;
                        _logger.LogApplicationException(cmtException);
                        result = false;
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// This method checks if the user is deleted or not
        /// </summary>
        /// <param name="userId">The user to verify</param>
        /// <returns>True if the user is "deleted"</returns>
        [WebMethod]
        public bool IsUserDeleted(Guid userId)
        {
            bool result = true;

            using (SqlConnection conn = new SqlConnection(this.getConnectionString()))
            {
                conn.Open();
                try
                {
                    using (SqlCommand command = new SqlCommand(_IsUserDeleted, conn))
                    {
                        command.Parameters.Add("@UserId", SqlDbType.UniqueIdentifier);
                        command.Parameters["@UserId"].Value = userId;
                        object deletedUser = command.ExecuteScalar();

                        if (deletedUser != null)
                        {
                            result = true;
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
                catch (Exception ex)
                {
                    CmtApplicationException cmtException = new CmtApplicationException();
                    cmtException.ExceptionDesc = ex.Message;
                    cmtException.ExceptionDateTime = DateTime.Now;
                    cmtException.UserDescription = "DeleteUser method failed; UserId=" + userId;
                    cmtException.ExceptionLevel = (short)ExceptionLevelEnum.Critical;
                    _logger.LogApplicationException(cmtException);
                    result = true;
                }
            }

            return result;
        }

        /// <summary>
        /// Returns the connection string from the web config file
        /// </summary>
        /// <returns>The connection string</returns>
        private string getConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
        }
    }
}
