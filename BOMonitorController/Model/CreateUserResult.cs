namespace AuthenticationService.Model
{
    public class CreateUserResult
    {
        public bool Success { get; set; }
    }
}