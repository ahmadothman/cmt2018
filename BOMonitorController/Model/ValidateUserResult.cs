using System.Collections.Generic;

namespace AuthenticationService.Model
{
    public class ValidateUserResult
    {
        public bool Success { get; set; }
        public List<string> Roles { get; set; }
        public string Guid { get; set; }
    }
}