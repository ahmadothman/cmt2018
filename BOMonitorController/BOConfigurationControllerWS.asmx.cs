﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Configuration;
using BOControllerLibrary.Controllers.Interfaces;
using BOControllerLibrary.Controllers;

namespace BOMonitorController
{
    /// <summary>
    /// Summary description for BOConfigurationControllerWS
    /// </summary>
    [WebService(Namespace = "http://cmt.nimera.net/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [System.Web.Script.Services.ScriptService]
    public class BOConfigurationControllerWS : System.Web.Services.WebService
    {
        IBOConfigurationController _controller = null;

        /// <summary>
        /// Default constructor
        /// </summary>
        public BOConfigurationControllerWS()
        {
            _controller = new BOConfigurationController(
                connectionString: ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString,
                databaseProvider: ConfigurationManager.ConnectionStrings["ApplicationServices"].ProviderName);
        }

        /// <summary>
        /// Returns the value of the parameter given by paramName
        /// </summary>
        /// <param name="paramName">The name of the parameter</param>
        /// <returns>The parameter value as a String</returns>
        [WebMethod]
        public string getParameter(string paramName)
        {
           return _controller.getParameter(paramName);
        }

        /// <summary>
        /// Sets a parameter value. The parameter is created if it does not
        /// exist in the configuration table yet.
        /// </summary>
        /// <param name="paramName">The parameter name</param>
        /// <param name="value">The parameter value</param>
        /// <returns>True if the parameter update succeeded</returns>
        [WebMethod]
        public Boolean setParameter(string paramName, string value)
        {
            return _controller.setParameter(paramName, value);
        }
    }
}
