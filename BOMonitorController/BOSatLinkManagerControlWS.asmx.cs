﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Services;
using BOControllerLibrary.Controllers;
using BOControllerLibrary.Controllers.Interfaces;
using BOControllerLibrary.Model;

namespace BOMonitorController
{
    /// <summary>
    /// This controller manages Bandwidth on Demand bookings, both in the CMT database
    /// as well as over the SatLink Manager interface provided by SES
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class BOSatLinkManagerControlWS : System.Web.Services.WebService, IBOSatLinkManagerController
    {
        private BOSatLinkManagerController _controller;

        public BOSatLinkManagerControlWS()
        {
            _controller = new BOSatLinkManagerController(
                connectionString: ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString,
                databaseProvider: ConfigurationManager.ConnectionStrings["ApplicationServices"].ProviderName);
        }

        /// <summary>
        /// Gets the details of a reservation from the CMT database
        /// </summary>
        /// <param name="cmtId">The CMT ID of the reservation</param>
        /// <returns>An SLMBooking object</returns>
        [WebMethod]
        public SLMBooking GetSLMBookingById(int cmtId)
        {
            return _controller.GetSLMBookingById(cmtId);
        }

        /// <summary>
        /// Gets the details of the reservations requested by a specific distributor
        /// </summary>
        /// <param name="distributorId">The ID of the distributor in the CMT</param>
        /// <returns>A list of SLMBooking objects</returns>
        [WebMethod]
        public List<SLMBooking> GetSLMBookingsByDistributor(int distributorId)
        {
            return _controller.GetSLMBookingsByDistributor(distributorId);
        }

        /// <summary>
        /// Gets the details of the reservations requested by a specific distributor
        /// </summary>
        /// <param name="customerId">The ID of the customer in the CMT</param>
        /// <returns>A list of SLMBooking objects</returns>
        [WebMethod]
        public List<SLMBooking> GetSLMBookingsByCustomer(int customerId)
        {
            return _controller.GetSLMBookingsByCustomer(customerId);
        }

        /// <summary>
        /// Fetches reservations from the CMT database based on status
        /// </summary>
        /// <param name="status">The status name</param>
        /// <returns>A list of SLMBooking objects</returns>
        [WebMethod]
        public List<SLMBooking> GetSLMBookingsByStatus(string status)
        {
            return _controller.GetSLMBookingsByStatus(status);
        }

        /// <summary>
        /// Fetches reservations from the CMT database which are active during the given period
        /// </summary>
        /// <param name="startDate">The first date in the period</param>
        /// <param name="endDate">The last date in the period</param>
        /// <returns>A list of SLMBooking objects</returns>
        [WebMethod]
        public List<SLMBooking> GetSLMBookingsBetweenDates(DateTime startDate, DateTime endDate)
        {
            return _controller.GetSLMBookingsBetweenDates(startDate, endDate);
        }

        /// <summary>
        /// Updates a reservation in the CMT database
        /// </summary>
        /// <param name="booking">The SLMBooking object</param>
        /// <returns>True if successful</returns>
        [WebMethod]
        public bool UpdateSLMBooking(SLMBooking booking)
        {
            return _controller.UpdateSLMBooking(booking);
        }

        /// <summary>
        /// Flags a reservation as deleted in the CMT database
        /// </summary>
        /// <param name="CMTId">The CMT ID of the resevation</param>
        /// <returns>True if successful</returns>
        [WebMethod]
        public bool DeleteSLMBooking(int CMTId)
        {
            return _controller.DeleteSLMBooking(CMTId);
        }

        /// <summary>
        /// Creates an SLM booking in the hub
        /// </summary>
        /// <param name="CMTId">The CMT ID of the reservation</param>
        /// <returns>A text string with a specific error message in case the booking failed</returns>
        [WebMethod]
        public string CreateSLMBookingInHub(int CMTId)
        {
            return _controller.CreateSLMBookingInHub(CMTId);
        }

        /// <summary>
        /// This method will cancel a future booking in the SLM server
        /// /// </summary>
        /// <param name="booking">An SLMBoooking object</param>
        /// <returns>True if successfult</returns>
        [WebMethod]
        public bool CancelSLMBooking(SLMBooking booking)
        {
            return _controller.CancelSLMBooking(booking);
        }

        /// <summary>
        /// This method starts a booking in the hub and sets the correct parameters in the CMT and the NMS
        /// </summary>
        /// <param name="booking">An SLMBoooking object</param>
        /// <returns>True if successfult</returns>
        [WebMethod]
        public bool StartBooking(SLMBooking booking)
        {
            return _controller.StartBooking(booking);
        }

        /// <summary>
        /// This method resets the SLA in the NMS once the SLM session is finised
        /// </summary>
        /// <param name="booking"></param>
        /// <returns></returns>
        [WebMethod]
        public bool StopBooking(SLMBooking booking)
        {
            return _controller.StopBooking(booking);
        }

        /// <summary>
        /// Fetches bookings from the CMT with a given start time
        /// </summary>
        /// <param name="startTime">The start time</param>
        /// <returns>A list of SLMBookings</returns>
        [WebMethod]
        public List<SLMBooking> GetBookingsByStartTime(DateTime startTime)
        {
            return _controller.GetBookingsByStartTime(startTime);
        }
    }
}
