﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using System.Configuration;
using BOControllerLibrary.Model.Invoices;
using BOControllerLibrary.Controllers.Interfaces;

namespace BOMonitorController
{
    /// <summary>
    /// Summary description for BOInvoiceController
    /// </summary>
    [WebService(Namespace = "http://cmt.nimera.net/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class BOInvoiceController : System.Web.Services.WebService, IBOInvoiceController
    {
        IBOInvoiceController _controller = null;

        public BOInvoiceController()
        {
            _controller = new BOControllerLibrary.Controllers.BOInvoiceController(
                connectionString: ConfigurationManager.ConnectionStrings["InvoiceServices"].ConnectionString,
                databaseProvider: ConfigurationManager.ConnectionStrings["InvoiceServices"].ProviderName);
        }

         /// <summary>
        /// Returns the invoices for a given distributor, year and month
        /// </summary>
        /// <param name="DistributorId">The distributor Id</param>
        /// <param name="Year">The invoice year</param>
        /// <param name="Month">The invoice month</param>
        /// <returns>A list of invoice headers for a specific distirbutor</returns>
        [WebMethod]
        public List<InvoiceHeader> GetInvoicesForDistributor(int distributorId, int year, int Month)
        {
            return _controller.GetInvoicesForDistributor(distributorId, year, Month);
        }

        /// <summary>
        /// Returns the list of detail lines for given invoice
        /// </summary>
        /// <param name="Year">Invoice year</param>
        /// <param name="num">Invoice number</param>
        /// <returns>A list of invoice lines</returns>
        [WebMethod]
        public List<InvoiceLine> GetInvoiceLines(int year, int num)
        {
            return _controller.GetInvoiceLines(year, num);
        }

        /// <summary>
        /// Returns the invoice header of a given invoice
        /// </summary>
        /// <param name="Year"></param>
        /// <param name="num"></param>
        /// <returns></returns>
        [WebMethod]
        public InvoiceHeader GetInvoice(int year, int num)
        {
            return _controller.GetInvoice(year, num);
        }

        /// <summary>
        /// Get all invoices for a given year and month
        /// </summary>
        /// <param name="year"></param>
        /// <param name="Month"></param>
        /// <returns>A list of invoices for a given year and month</returns>
        [WebMethod]
        public List<InvoiceHeader> GetInvoices(int year, int Month)
        {
            return _controller.GetInvoices(year, Month);
        }

         /// <summary>
        /// Returns the invoice lines for a distributor.
        /// </summary>
        /// <remarks>
        /// Only the invoice lines which refere to a terminal acivity or service
        /// fee linked to a terminal are included in the list.
        /// </remarks>
        /// <param name="distributorId">The distributor Id</param>
        /// <param name="year">The invoice year</param>
        /// <param name="month">The invoice month</param>
        /// <returns>A DataTable object with the invoice lines</returns>
        [WebMethod]
        public DataTable GetInvoiceLinesForDistributor(int distributorId, int year, int month)
        {
            return _controller.GetInvoiceLinesForDistributor(distributorId, year, month);
        }
    }
}
