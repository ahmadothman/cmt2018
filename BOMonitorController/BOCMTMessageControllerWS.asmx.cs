﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using BOControllerLibrary.Controllers.Interfaces;
using BOControllerLibrary.Controllers;
using BOControllerLibrary.Model.CMTMessages;
using System.Configuration;

namespace BOMonitorController
{
    /// <summary>
    /// Summary description for BOCMTMessageControllerWS
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class BOCMTMessageControllerWS : System.Web.Services.WebService, IBOCMTMessageController 
    {
        private IBOCMTMessageController _controller;

        public BOCMTMessageControllerWS()
        {
            _controller = new BOCMTMessageController(
                connectionString: ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString,
                databaseProvider: ConfigurationManager.ConnectionStrings["ApplicationServices"].ProviderName);
        }

        /// <summary>
        /// This method submits a new message to the CMT database
        /// </summary>
        /// <param name="message">The CMT message</param>
        /// <returns>True if succesful</returns>
        [WebMethod]
        public bool SubmitCMTMessage(CMTMessage message)
        {
            return _controller.SubmitCMTMessage(message);
        }

        /// <summary>
        /// This method submits changes to a message in the CMT database
        /// </summary>
        /// <param name="message">The CMT message</param>
        /// <returns>True if succesful</returns>
        [WebMethod]
        public bool UpdateCMTMessage(CMTMessage message)
        {
            return _controller.UpdateCMTMessage(message);
        }

        /// <summary>
        /// Retrieves the messages for a user
        /// First messages are retrieved based on the user ID (messages sent specifically to user)
        /// Then messages are retrieved based on the user role (messages sent to role)
        /// Finally messages sent to everyone are retrieved
        /// </summary>
        /// <param name="userId">The ID of the user, can be distributor ID, VNO ID etc</param>
        /// <param name="role">The role of the user, eg distributor or organization</param>
        /// <returns>A list of CMT messages, can be empty</returns>
        [WebMethod]
        public List<CMTMessage> GetCMTMessagesForUser(string userId, string role)
        {
            return _controller.GetCMTMessagesForUser(userId, role);
        }

        /// <summary>
        /// Retrieves all the messages in the database
        /// This method is used for the admin overview
        /// </summary>
        /// <returns>A list of CMT messages, can be empty</returns>
        [WebMethod]
        public List<CMTMessage> GetAllCMTMessages()
        {
            return _controller.GetAllCMTMessages();
        }

        /// <summary>
        /// Retrieves a specific message from the database
        /// </summary>
        /// <param name="messageId">The ID of the message</param>
        /// <returns>The message object</returns>
        [WebMethod]
        public CMTMessage GetCMTMessage(string messageId)
        {
            return _controller.GetCMTMessage(messageId);
        }

        /// <summary>
        /// Method for retrieving an attachment from the database and putting it into the memorystream
        /// so that it can be used
        /// </summary>
        /// <param name="ServiceId">The ID of the message for which the attachment is retrieved</param>
        /// <returns>The image</returns>
        [WebMethod]
        public byte[] GetAttachment(string messageId)
        {
            return _controller.GetAttachment(messageId);
        }
    }
}
