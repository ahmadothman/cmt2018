﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Web.Script.Services;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using BOControllerLibrary.Controllers.Interfaces;
using BOControllerLibrary.Controllers;
using BOControllerLibrary.Model;
using BOControllerLibrary.Model.Voucher;

namespace BOMonitorController
{
    /// <summary>
    /// Summary description for BOPaymentControllerWS
    /// Contains the methods for processing a checkout
    /// In the first implementation, a distributor can choose an unreleased batch of vouchers
    /// to pay for
    /// The payment will be processed by an external provider, PayPal in the first case
    /// Payments are stored in the CMT database
    /// </summary>
    [WebService(Namespace = "http://cmt.nimera.net/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    [ScriptService]
    public class BOPaymentControllerWS : WebService, IBOPaymentController
    {
        private IBOPaymentController _controller;

        public BOPaymentControllerWS()
        {
            _controller = new BOPaymentController(
                connectionString: ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString,
                databaseProvider: ConfigurationManager.ConnectionStrings["ApplicationServices"].ProviderName);
        }

        /// <summary>
        /// Initiates the checkout procedure
        /// </summary>
        /// <param name="checkOutMethod">Determines the checkout method, in the first implemantation, only PayPal is available</param>
        /// <param name="action">Payment type in the CMT</param>
        /// <param name="distributorId">The ID of the paying distributor</param>
        /// <param name="amount">The amount to be paid</param>
        /// <param name="token">Paypal session token</param>
        [WebMethod]
        public string CheckoutStart(string checkOutMethod, string action, int distributorId, decimal amount, string cur, ref string token)
        {
            string tokenTemp = token;
            string retMsg = _controller.CheckoutStart(checkOutMethod, action, distributorId, amount, cur, ref tokenTemp);
            token = tokenTemp;
            return retMsg;
        }

        /// <summary>
        /// Cancels the checkout procedure
        /// This method has been replaced by a redirect
        /// </summary>
        [WebMethod]
        public void CheckoutCancel()
        {
            _controller.CheckoutCancel();
        }

        /// <summary>
        /// Proceeds with the user review the Paypal checkout
        /// This user still has to confirm the payment after this step
        /// </summary>
        /// <param name="token">Paypal session token</param>
        /// <param name="payerId">Paypal payer ID</param>
        /// <returns>A URL, possibly containing an error message</returns>
        [WebMethod]
        public string PaypalCheckoutProceed(string token, ref string payerId)
        {
            string payerIdTemp = payerId;
            string retMsg = _controller.PaypalCheckoutProceed(token, ref payerIdTemp);
            payerId = payerIdTemp;
            return retMsg;
        }

        /// <summary>
        /// Completes the Paypal checkout upon confirmation by the user
        /// </summary>
        /// <param name="amount">Final amount to be paid</param>
        /// <param name="payerId">Paypal payer ID</param>
        /// <param name="token">Paypal session token</param>
        /// <param name="action">Payment action</param>
        /// <param name="distributorId">The ID of the paying distributor</param>
        /// <returns>1 if succesful, else an error message</returns>
        [WebMethod]
        public string PaypalCheckoutComplete(decimal amount, string cur, string payerId, string token, string action, int distributorId)
        {
            return _controller.PaypalCheckoutComplete(amount, cur, payerId, token, action, distributorId);
        }
    }
}