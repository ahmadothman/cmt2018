﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using AuthenticationService.Model;

namespace BOMonitorController
{
    /// <summary>
    /// Summary description for BOAuthenticationControlWS
    /// </summary>
    [WebService(Namespace = "http://cmt.nimera.net/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [System.Web.Script.Services.ScriptService]
    public class BOAuthenticationControlWS : System.Web.Services.WebService
    {
        [WebMethod]
        public ValidateUserResult ValidateUser(string username, string password)
        {
            var result = new ValidateUserResult();

            if (Membership.ValidateUser(username, password))
            {
                var user = Membership.GetUser(username);
                result.Success = true;
                result.Guid = user.ProviderUserKey.ToString();
                result.Roles = new List<string>(Roles.GetRolesForUser(username));
            }
            else
            {
                result.Success = false;
            }
            return result;
        }

        [WebMethod]
        public string CreateUser(string username, string password, string email)
        {
            MembershipCreateStatus createStatus;
            Membership.CreateUser(username, password, email,
                passwordQuestion: null,
                passwordAnswer: null,
                isApproved: true,
                providerUserKey: null,
                status: out createStatus);

            return createStatus.ToString();
        }

        [WebMethod]
        public string GetGuidForUser (string username)
        {
            return Membership.GetUser(username).ProviderUserKey.ToString();
        }

        [WebMethod]
        public bool IsUserInRole (string username, string role)
        {
            return Roles.IsUserInRole(username, role);
        }
    }
}
