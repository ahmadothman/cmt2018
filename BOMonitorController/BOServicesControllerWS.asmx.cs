﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Configuration;
using BOControllerLibrary.Controllers.Interfaces;
using BOControllerLibrary.Controllers;
using BOControllerLibrary.Model.Service;

namespace BOMonitorController
{
    /// <summary>
    /// Summary description for BOServicesControllerWS
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class BOServicesControllerWS : System.Web.Services.WebService, IBOServicesController
    {
        private IBOServicesController _controller;

        public BOServicesControllerWS()
        {
            _controller = new BOServicesController(
                connectionString: ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString,
                databaseProvider: ConfigurationManager.ConnectionStrings["ApplicationServices"].ProviderName);
        }

        /// <summary>
        /// Deletes the service from the CMTDatabase.
        /// This does not physically remove the service ascx file from the project,
        /// but just removes it from the list of selectable services.
        /// </summary>
        /// <param name="service">The service to be deleted</param>
        /// <returns>True if succesful, else false</returns>
        [WebMethod]
        public bool DeleteService(Service service)
        {
            return _controller.DeleteService(service);
        }

        /// <summary>
        /// Returns the parent Service of a service.
        /// This is the service which is represented by a node one level higher
        /// than the given service. The return value can be null!
        /// </summary>
        /// <param name="service">The service for which the parent is requested</param>
        /// <returns>The parent service, can also be null</returns>
        [WebMethod]
        public Service GetParent(Service service)
        {
            return _controller.GetParent(service);
        }

        /// <summary>
        /// Returns a list of services linked to the given VNO.
        /// </summary>
        /// <param name="VNOId">The VNO ID in question</param>
        /// <returns>A list of services</returns>
        [WebMethod]
        public List<Service> GetServicesForVNO(int VNOId)
        {
            return _controller.GetServicesForVNO(VNOId);
        }

        /// <summary>
        /// Returns a typed list of services which are related as siblings
        /// to the given service. This list can be empty.
        /// </summary>
        /// <param name="service">The service for which the siblings are requested</param>
        /// <returns>A list of services</returns>
        [WebMethod]
        public List<Service> GetSiblings(Service service)
        {
            return _controller.GetSiblings(service);
        }

        /// <summary>
        /// Links a Service to a VNO. This adds a record to the ServicesVNOs table.
        /// </summary>
        /// <param name="VNOId">The VNO ID the service should be linked to</param>
        /// <param name="ServiceId">The ID of the service to be linked to the VNO</param>
        /// <returns>True if successful, else false</returns>
        [WebMethod]
        public bool LinkServiceToVNO(int VNOId, string ServiceId)
        {
            return _controller.LinkServiceToVNO(VNOId, ServiceId);
        }

        /// <summary>
        /// Removes the link between a Service and a VNO.
        /// </summary>
        /// <param name="VNOId">The VNO ID the service should be removed from</param>
        /// <param name="ServiceId">The ID of the service to be removed from the VNO</param>
        /// <returns>True if successful, else false</returns>
        [WebMethod]
        public bool RemoveServiceFromVNO(int VNOId, string ServiceId)
        {
            return _controller.RemoveServiceFromVNO(VNOId, ServiceId);
        }

        /// <summary>
        /// Updates the service in the CMT database. If the service does not exist a new service is created.
        /// </summary>
        /// <param name="service">The service to be updated/added</param>
        /// <returns>True if successful, else false</returns>
        [WebMethod]
        public bool UpdateService(Service service)
        {
            return _controller.UpdateService(service);
        }

        /// <summary>
        /// Returns the service details
        /// </summary>
        /// <param name="ServiceId">The ID of the service</param>
        /// <returns>A service</returns>
        [WebMethod]
        public Service GetService(string ServiceId)
        {
            return _controller.GetService(ServiceId);
        }

        /// <summary>
        /// Returns a list of all available (non-deleted) services in the database
        /// of a certain level.
        /// Will mainly be used to get all top level services or to get all services
        /// </summary>
        /// <param name="level">The service level (1, 2, 3, or all)</param>
        /// <returns>A list of services</returns>
        [WebMethod]
        public List<Service> GetAllAvailableServices(string level)
        {
            return _controller.GetAllAvailableServices(level);
        }

        /// <summary>
        /// Returns a list of all child services of a parent service
        /// The list can be empty
        /// </summary>
        /// <param name="parentService">The parent service</param>
        /// <returns>A list of services</returns>
        [WebMethod]
        public List<Service> GetChildren(Service service)
        {
            return _controller.GetChildren(service);
        }

        /// <summary>
        /// Method for retrieving an icon from the database and putting it into the memorystream
        /// so that it can be displayed as an image
        /// </summary>
        /// <param name="ServiceId">The ID of the service for which the icon is retrieved</param>
        /// <returns>The image</returns>
        [WebMethod]
        public Byte[] GetIcon(string ServiceId)
        {
            return _controller.GetIcon(ServiceId);
        }


        /// <summary>
        /// Returns a list of services linked to the given Role.
        /// </summary>
        /// <param name="Role">The Role in question</param>
        /// <returns>A list of services</returns>
        [WebMethod]
        public List<Service> GetServicesForRole(string RoleName)
        {
            return _controller.GetServicesForRole(RoleName);
        }

        /// <summary>
        /// Links a Service to a Role. This adds a record to the RolesServices table.
        /// </summary>
        /// <param name="RoleName">The Role the service should be linked to</param>
        /// <param name="ServiceId">The ID of the service to be linked to the VNO</param>
        /// <returns>True if succesful, else false</returns>
        [WebMethod]
        public bool LinkServiceToRole(string RoleName, string ServiceId)
        {
            return _controller.LinkServiceToRole(RoleName, ServiceId);
        }

        /// <summary>
        /// Remove a Service from a Role. This removes a record from the RolesServices table.
        /// </summary>
        /// <param name="RoleName">The Role the service should be removed from</param>
        /// <param name="ServiceId">The ID of the service to be removed from the VNO</param>
        /// <returns>True if succesful, else false</returns>
        [WebMethod]
        public bool RemoveServiceFromRole(string RoleName, string ServiceId)
        {
            return _controller.RemoveServiceFromRole(RoleName, ServiceId);
        }
    }
}
