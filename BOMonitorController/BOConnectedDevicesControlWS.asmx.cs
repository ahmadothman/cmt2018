﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Configuration;
using BOControllerLibrary.Controllers.Interfaces;
using BOControllerLibrary.Controllers;
using BOControllerLibrary.Model.Terminal;

namespace BOMonitorController
{
    /// <summary>
    /// Summary description for BOConnectedDevicesControlWS
    /// </summary>
    [WebService(Namespace = "http://cmt.nimera.net/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [System.Web.Script.Services.ScriptService]
    public class BOConnectedDevicesControlWS : System.Web.Services.WebService, IBOConnectedDevicesController
    {
        IBOConnectedDevicesController _controller;

        public BOConnectedDevicesControlWS()
        {
            _controller = new BOConnectedDevicesController(
                connectionString: ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString,
                databaseProvider: ConfigurationManager.ConnectionStrings["ApplicationServices"].ProviderName);
        }

        
        /// <summary>
        /// Adds a new connected device to a terminal
        /// </summary>
        /// <param name="device">The device object which includes the terminal identifier</param>
        /// <returns>True if successful</returns>
        [WebMethod]
        public bool AddDeviceToTerminal(ConnectedDevice device)
        {
            return _controller.AddDeviceToTerminal(device);
        }

        /// <summary>
        /// Removes a connected device from a terminal and the CMT database
        /// </summary>
        /// <param name="device">The device object which includes the terminal identifier</param>
        /// <returns>True if successful</returns>
        [WebMethod]
        public bool RemoveDeviceFromTerminal(ConnectedDevice device)
        {
            return _controller.RemoveDeviceFromTerminal(device);
        }

        /// <summary>
        /// Returns a list of devices that are connected to a specific terminal
        /// </summary>
        /// <param name="macAddress">The MAC address of the terminal</param>
        /// <returns>A list of connected devices</returns>
        [WebMethod]
        public List<ConnectedDevice> GetDevicesForTerminal(string macAddress)
        {
            return _controller.GetDevicesForTerminal(macAddress);
        }

        /// <summary>
        /// Returns the details of a specific connected device
        /// </summary>
        /// <param name="deviceId">The ID of the connected device</param>
        /// <returns>The connect device object</returns>
        [WebMethod]
        public ConnectedDevice GetDevice(string deviceId)
        {
            return _controller.GetDevice(deviceId);
        }

        /// <summary>
        /// Updates the parameters of an existing connected device
        /// </summary>
        /// <param name="device">The device to be updated, including the new parameters</param>
        /// <returns>True if successful</returns>
        [WebMethod]
        public bool UpdateDevice(ConnectedDevice device)
        {
            return _controller.UpdateDevice(device);
        }

        /// <summary>
        /// Retrieves a list of all connected devices in the CMT database
        /// </summary>
        /// <returns>The list of devices</returns>
        [WebMethod]
        public List<ConnectedDevice> GetAllDevices()
        {
            return _controller.GetAllDevices();
        }

        /// <summary>
        /// Retrieves a list of all connected device types in the CMT database
        /// </summary>
        /// <returns>The list of device types</returns>
        [WebMethod]
        public List<ConnectedDeviceType> GetAllConnectedDeviceTypes()
        {
            return _controller.GetAllConnectedDeviceTypes();
        }

        /// <summary>
        /// Returns a list of devices that are connected to a specific terminal
        /// and are of a specific typ
        /// </summary>
        /// <param name="terminalMAC">The MAC address of the terminal</param>
        /// <param name="deviceTypeID">The ID of the device type</param>
        /// <returns>A list of connected devices</returns>
        [WebMethod]
        public List<ConnectedDevice> GetConnectedDevicesForTerminalByType(string terminalMAC, int deviceTypeID)
        {
            return _controller.GetConnectedDevicesForTerminalByType(terminalMAC, deviceTypeID);
        }
    }
}
