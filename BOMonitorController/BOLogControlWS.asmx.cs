﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Web.Script.Services;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using BOControllerLibrary.Controllers;
using BOControllerLibrary.Controllers.Interfaces;
using BOControllerLibrary.Model;
using BOControllerLibrary.Model.Log;
using BOControllerLibrary.Model.Terminal;
using BOControllerLibrary.Model.User;

namespace BOMonitorController
{
    /// <summary>
    /// Main Backoffice Logging Controller 
    /// </summary>
    [WebService(Namespace = "http://cmt.nimera.net/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    [ScriptService]
    public class BOLogControlWS : WebService, IBOLogController
    {
        private IBOLogController _controller;

        public BOLogControlWS()
        {
            _controller = new BOLogController(
                connectionString: ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString,
                databaseProvider: ConfigurationManager.ConnectionStrings["ApplicationServices"].ProviderName);

        }


        /// <summary>
        /// Log all user and application exceptions.
        /// </summary>
        /// <param name="ex">The exception object: Note that it is useful to subclass System.Exception for more specific information. </param>
        [WebMethod]
        public void LogApplicationException(CmtApplicationException ex)
        {
            _controller.LogApplicationException(ex);
        }

        /// <summary>
        /// Logs the terminal activity.
        /// </summary>
        /// <param name="activity">The activity.</param>
        [WebMethod]
        public void LogTerminalActivity(TerminalActivity activity)
        {
            _controller.LogTerminalActivity(activity);
        }

        /// <summary>
        /// Log the activities of a user (Log-In, Log-Out, Session expiration)
        /// </summary>
        /// <param name="activity">The activity.</param>
        [WebMethod]
        public void LogUserActivity(UserActivity activity)
        {
            _controller.LogUserActivity(activity);
        }

        /// <summary>
        /// Return a list of activities for a given terminal
        /// </summary>
        /// <param name="macAddress">The MAC address of the terminal</param>
        /// <returns>A list of terminal activities for the specified MAC address</returns>
        [WebMethod]
        public List<TerminalActivity> GetTerminalActivity(string macAddress)
        {
            return _controller.GetTerminalActivity(macAddress);
        }


        /// <summary>
        /// Returns a list of activities for the given SitId and IspId
        /// </summary>
        /// <param name="SitId">The Site Identifier of the terminal</param>
        /// <param name="IspId">The ISP of the terminal</param>
        /// <returns></returns>
        [WebMethod]
        public List<TerminalActivityExt> GetTerminalActivityBySitId(int SitId, int IspId)
        {
            return _controller.GetTerminalActivityBySitId(SitId, IspId);
        }

        /// <summary>
        /// Return a list of activities for a given user.
        /// </summary>
        /// <param name="userId">Userid is the unique uid</param>
        /// <returns></returns>
        [WebMethod]
        public List<UserActivity> GetUserActivities(string userId)
        {
            return _controller.GetUserActivities(userId);
        }

        /// <summary>
        /// Return a list of exceptions over a given period
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        [WebMethod]
        public List<CmtApplicationException> GetApplicationExceptions(DateTime from, DateTime to)
        {
            return _controller.GetApplicationExceptions(from, to);
        }

        /// <summary>
        /// Lists all types of terminal activity. New ones are currently only supported through autogeneration (so no description yet): see storing terminal activities
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public List<ActivityType> GetTerminalActivityTypes()
        {
            return _controller.GetTerminalActivityTypes();
        }

        /// <summary>
        /// Updates a terminal activity in the terminal acitivty log
        /// </summary>
        /// <param name="activity">The modified terminal activity</param>
        /// <returns>True if the method succeeds</returns>
        [WebMethod]
        public void UpdateTerminalActivity(TerminalActivity activity)
        {
            _controller.UpdateTerminalActivity(activity);
        }

        /// <summary>
        /// Removes a terminal activity from the TerminalActivityLog table
        /// </summary>
        /// <param name="terminalActivityId">The identifier of the terminal activity</param>
        /// <returns>True if the method succeeds</returns>
        public Boolean DeleteTerminalActivity(int terminalActivityId)
        {
            return _controller.DeleteTerminalActivity(terminalActivityId);
        }

        /// <summary>
        /// Returns a TerminalActivity object given by its terminalActivityId
        /// </summary>
        /// <param name="terminalActivityId">The terminal identifier</param>
        /// <returns>A TerminalActivity object or null if not found</returns>
        [WebMethod]
        public TerminalActivity GetTerminalActivityById(int terminalActivityId)
        {
            return _controller.GetTerminalActivityById(terminalActivityId);
        }

        /// <summary>
        /// Lists all types of users activity. New ones are currently only supported through autogeneration (so no description yet): see storing user activities
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public List<ActivityType> GetUserActivityTypes()
        {
            return _controller.GetUserActivityTypes();
        }

        /// <summary>
        /// Returns a DataTable with user activities between a start and end date.
        /// </summary>
        /// <param name="startDate">The start date</param>
        /// <param name="endDate">The end date</param>
        /// <returns>A DataTable of UserActivityType records or null if an error occurred</returns>
        [WebMethod]
        public DataTable GetUserActivityBetweenDates(DateTime startDate, DateTime endDate)
        {
            string queryCmd = "SELECT aspnet_Users.UserName, UserActivityLog.ActionDate, UserActivityTypes.Action " +
                        "FROM UserActivityLog, UserActivityTypes, aspnet_Users " + 
                        "WHERE UserActivityLog.UserActivityTypeId = UserActivityTypes.Id " +
                        "AND UserActivityLog.UserId = aspnet_Users.UserId " +
                        "AND ActionDate >= @StartDate AND ActionDate <= @EndDate " +
                        "ORDER BY UserActivityLog.ActionDate DESC";

            string connectionString = ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
            
            DataTable userActivityData = new DataTable();
            userActivityData.TableName = "Activities";
            SqlConnection conn = new SqlConnection(connectionString);
            SqlDataAdapter adapter = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand(queryCmd, conn);

            SqlParameter startDateParam = new SqlParameter();
            startDateParam.ParameterName = "@StartDate";
            startDateParam.SqlDbType = SqlDbType.Date;
            startDateParam.Value = startDate.Date;
            cmd.Parameters.Add(startDateParam);

            SqlParameter endDateParam = new SqlParameter();
            endDateParam.ParameterName = "@EndDate";
            endDateParam.SqlDbType = SqlDbType.Date;
            endDateParam.Value = endDate.Date;
            cmd.Parameters.Add(endDateParam);

            adapter.SelectCommand = cmd;

            conn.Open();

            try
            {
                adapter.Fill(userActivityData);
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtException = new CmtApplicationException();
                cmtException.ExceptionDesc = ex.Message;
                cmtException.ExceptionDateTime = DateTime.Now;
                cmtException.UserDescription = "getAccumulatedViews method failed";
                _controller.LogApplicationException(cmtException);
                userActivityData = null;
            }
            finally
            {
                conn.Close();
            }

            return userActivityData;
        }

        /// <summary>
        /// Returns a DataTable with exceptions between a start and end-date
        /// </summary>
        /// <param name="startDate">Start date for the query</param>
        /// <param name="endDate">End date for the query</param>
        /// <returns>A filled dataTable or null in case of an error</returns>
        [WebMethod]
        public DataTable GetExceptionsBetweenDates(DateTime startDate, DateTime endDate, String level)
        {
            string queryCmd = "SELECT ExceptionDateTime, ExceptionLevel, ExceptionDesc, UserDescription " +
                        "FROM ExceptionActivityLog " +
                        "WHERE ExceptionDateTime >= @StartDate AND ExceptionDateTime <= @EndDate AND ExceptionLevel IN (" + level + ")";

            string connectionString = ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;

            DataTable exceptionActivityData = new DataTable();
            exceptionActivityData.TableName = "ExceptionActivityLog";
            SqlConnection conn = new SqlConnection(connectionString);
            SqlDataAdapter adapter = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand(queryCmd, conn);

            SqlParameter startDateParam = new SqlParameter();
            startDateParam.ParameterName = "@StartDate";
            startDateParam.SqlDbType = SqlDbType.Date;
            startDateParam.Value = startDate.Date;
            cmd.Parameters.Add(startDateParam);

            SqlParameter endDateParam = new SqlParameter();
            endDateParam.ParameterName = "@EndDate";
            endDateParam.SqlDbType = SqlDbType.Date;
            endDateParam.Value = endDate.Date;
            cmd.Parameters.Add(endDateParam);
            
            adapter.SelectCommand = cmd;

            conn.Open();

            try
            {
                adapter.Fill(exceptionActivityData);
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtException = new CmtApplicationException();
                cmtException.ExceptionDesc = ex.Message;
                cmtException.ExceptionDateTime = DateTime.Now;
                cmtException.UserDescription = "GetException between dates method failed";
                _controller.LogApplicationException(cmtException);
                exceptionActivityData = null;
            }
            finally
            {
                conn.Close();
            }

            return exceptionActivityData;
        }

        /// <summary>
        /// Returns a DataTable with all terminal activities between a start and end-date
        /// </summary>
        /// <param name="startDate">Start date for the query</param>
        /// <param name="endDate">End date for the query</param>
        /// <returns>A filled DataTable or null in case of an error</returns>
        [WebMethod]
        public DataTable GetTermActivitiesBetweenDates(DateTime startDate, DateTime endDate)
        {
            return _controller.GetTermActivitiesBetweenDates(startDate, endDate);
        }

        /// <summary>
        /// Returns a list of activities for a given terminal and 
        /// activity type between a start and end time
        /// </summary>
        /// <param name="startTime">The start time of the query</param>
        /// <param name="endTime">The end time of query</param>
        /// <param name="activityType">The activity type</param>
        /// <param name="macAddress">The terminals MAC address</param>
        /// <returns>A list of terminal activities</returns>
        [WebMethod]
        public List<TerminalActivity> GetTerminalActivityExt(DateTime startTime, DateTime endTime, int activityType, string macAddress)
        {
            return _controller.GetTerminalActivityExt(startTime, endTime, activityType, macAddress);
        }

        /// <summary>
        /// Returns a datatable with an account report for a given distributor
        /// </summary>
        /// <param name="startDate">Start date of the report</param>
        /// <param name="endDate">End date of the report</param>
        /// <param name="distid">The distributor Id</param>
        /// <returns>A datatable containing the report rows</returns>
        [WebMethod]
        public DataTable GetTermAccountReportForDistributor(DateTime startDate, DateTime endDate, int distid)
        {
            return _controller.GetTermAccountReportForDistributor(startDate, endDate, distid);
        }


        /// <summary>
        /// Sends a mail to the destination addresses
        /// </summary>
        /// <param name="body">The body text of the mail</param>
        /// <param name="subject">The subject of the mail</param>
        /// <param name="toAddresses">A list of to addresses</param>
        [WebMethod]
        public bool SendMail(string body, string subject, params string[] toAddresses)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
            string providerName = ConfigurationManager.ConnectionStrings["ApplicationServices"].ProviderName;
            BONotifyController boNotifyController = new BONotifyController(connectionString, providerName);

            return boNotifyController.SendMail(body, subject, toAddresses);
        }

        /// <summary>
        /// Returns a list of tickets in the CMT database for a given MAC address
        /// </summary>
        /// <param name="macAddress">The MAC address of the terminal the tickets are for</param>
        /// <returns>A list of tickets (CMT format)</returns>
        [WebMethod]
        public List<CMTTicket> GetTicketsByMacAddress(string macAddress)
        {
            return _controller.GetTicketsByMacAddress(macAddress);
        }

        /// <summary>
        /// Returns a ticket from the CMT database based on a specific ticket ID
        /// </summary>
        /// <param name="ticketId">The ID of the ticket</param>
        /// <returns>A CMT ticket object</returns>
        [WebMethod]
        public CMTTicket GetTicketById(string ticketId)
        {
            return _controller.GetTicketById(ticketId);
        }

        /// <summary>
        /// Returns the billable to which a terminal activity type is linked
        /// </summary>
        /// <param name="TerminalActivityTypeId">The ID of the terminal activity type</param>
        /// <returns>The ID of the billable</returns>
        [WebMethod]
        public int GetBillableForTerminalActivityType(int TerminalActivityTypeId)
        {
            return _controller.GetBillableForTerminalActivityType(TerminalActivityTypeId);
        }

        [WebMethod]
        public List<EventMessage> GetLatestBarixEventMessagesByMac(string macAddress)
        {
            return _controller.GetLatestBarixEventMessagesByMac(macAddress);
        }

        [WebMethod]
        public List<EventMessage> GetLatestBarixEventMessagesByUser(string userId){
            return _controller.GetLatestBarixEventMessagesByUser(userId);
        }
    }
}
