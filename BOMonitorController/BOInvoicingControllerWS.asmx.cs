﻿using BOControllerLibrary.Controllers;
using BOControllerLibrary.Controllers.Interfaces;
using BOControllerLibrary.Model.Invoicing;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using WCFInvoiceControllerService;
using WCFInvoiceControllerService.Util;

namespace BOMonitorController
{
    /// <summary>
    /// Summary description for BOInvoicingControllerWS
    /// </summary>
    [WebService(Namespace = "http://cmt.nimera.net/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    [ScriptService]
    public class BOInvoicingControllerWS : WebService, IBOInvoicingController
    {
        private IBOInvoicingController _controller;

        public BOInvoicingControllerWS()
        {
            // in time we'll use dependency injection here for lifetime and implementation activation.
            _controller = new BOControllerLibrary.Controllers.BOInvoicingController(
                connectionString: ConfigurationManager.ConnectionStrings["CMTDataInvoicing"].ConnectionString,
                databaseProvider: ConfigurationManager.ConnectionStrings["ApplicationServices"].ProviderName);
        }
        /// <summary>
        /// Returns the invoices for a given distributor
        /// These invoices don't contain their invoiceDetails
        /// </summary>
        /// <param name="distributorId">The distributor Id</param>
        /// <returns>A list of invoice headers for a specific distirbutor</returns>
        [WebMethod]
        public List<InvoiceHeader> GetInvoicesForDistributor(int distributorId)
        {
            return _controller.GetInvoicesForDistributor(distributorId);
        }

        /// <summary>
        /// Returns the unpaid invoices for a given distributor
        /// These invoices don't contain their invoiceDetails
        /// </summary>
        /// <param name="distributorId">The distributor Id</param>
        /// <returns>A list of invoice headers for a specific distirbutor</returns>
        [WebMethod]
        public List<InvoiceHeader> GetUnpaidInvoicesForDistributor(int distributorId)
        {
            return _controller.GetUnpaidInvoicesForDistributor(distributorId);
        }

        /// <summary>
        /// Returns the list of detail lines for given invoice
        /// </summary>
        /// <param name="year">Invoice year</param>
        /// <param name="num">Invoice number</param>
        /// <returns>A list of invoice lines, this list can be empty</returns>
        [WebMethod]
        public List<InvoiceDetail> GetInvoiceLines(int id)
        {
            return _controller.GetInvoiceLines(id);
        }

        /// <summary>
        /// Returns an invoice identified by its year and number
        /// This invoice also contains its invoiceDetails
        /// </summary>
        /// <param name="year">Year the invoice was issued</param>
        /// <param name="num">Sequence number of the invoice</param>
        /// <returns>An invoice header + detail lines or null if not found</returns>
        [WebMethod]
        public InvoiceHeader GetInvoice(int id)
        {
            return _controller.GetInvoice(id);
        }

        /// <summary>
        /// Get all unvalidated invoices
        /// These invoices don't contain their invoiceDetails
        /// </summary>
        /// <returns>A list of invoices headers (and details)</returns>
        [WebMethod]
        public List<InvoiceHeader> GetUnvalidatedInvoices()
        {
            return _controller.GetUnvalidatedInvoices();
        }

        /// <summary>
        /// Get all invoices for a given year and month
        /// These invoices don't contain their invoiceDetails
        /// </summary>
        /// <param name="year">The year the invoices were issued</param>
        /// <param name="month">The month the invoices were issued</param>
        /// <returns>A list of invoices headers (and details) for a given year and month</returns>
        [WebMethod]
        public List<InvoiceHeader> GetInvoices(int year, int month)
        {
            return _controller.GetInvoices(year, month);
        }

        /// <summary>
        /// Gets the IDs of the invoiceDetails for one invoiceHeader.
        /// This method could be used to get the invoiceDetails separately
        /// by looping over the retrieved IDs.
        /// This could for example be the case when the data to retrieve is too large to send to the client.
        /// </summary>
        /// <param name="invoiceYear">The year of the invoice</param>
        /// <param name="invoiceNum">The number of the invoice in that year</param>
        /// <returns>A list with the IDs of the invoice detail lines of that invoice</returns>
        [WebMethod]
        public List<int> GetInvoiceDetailIdsForInvoice(int invoiceId)
        {
            return _controller.GetInvoiceDetailIdsForInvoice(invoiceId);
        }

        /// <summary>
        /// Gets sum of transactions for invoice
        /// </summary>
        /// <param name="invoiceId">The id of the invoice</param>
        /// <returns>Sum of all transactions for this invoice</returns>
        [WebMethod]
        public InvoiceTransaction GetTransaction(int invoiceId)
        {
            return _controller.GetTransaction(invoiceId);
        }

        /// <summary>
        /// Adds a new invoice to the invoice list
        /// </summary>
        /// <remarks>
        /// An invoice contains an invoice header AND all detail lines which are allocated to the inoivce!
        /// </remarks>
        /// <param name="invoice">The invoice (invoice header + details) to add</param>
        /// <returns>The ID of the newly created invoice, or null when an error occured.</returns>
        [WebMethod]
        public int? AddInvoice(InvoiceHeader invoice)
        {
            return _controller.AddInvoice(invoice);
        }

        /// <summary>
        /// This method queries the invoice tables by means of a number of criteria. Only
        /// the criteria which are not null are taken into account.
        /// These invoices don't contain their invoiceDetails
        /// </summary
        /// <param name="year">Invoice year</param>
        /// <param name="month">Invoice month</param>
        /// <param name="invoiceNum">Invoice number</param>
        /// <param name="invoiceState">The invoice status. 0 = Unvalidated, 1 = validated
        /// 2 = Released, 3 = Paid</param>
        /// <param name="distId">The Distributor Id</param>
        /// <returns>The list of selected invoice headers. This list can be empty</returns>
        [WebMethod]
        public List<InvoiceHeader> QueryInvoices(String year, String month, String invoiceNum, String invoiceState, String distId)
        {
            return _controller.QueryInvoices(year, month, invoiceNum, invoiceState, distId);
        }

        /// <summary>
        /// This method queries the invoice tables by means of a number of criteria. Only
        /// the criteria which are not null are taken into account.
        /// An AND operator is put between the different arguments, an OR operator between the same arguments.
        /// These invoices don't contain their invoiceDetails
        /// </summary
        /// <param name="years">Invoice year</param>
        /// <param name="months">Invoice month</param>
        /// <param name="InvoiceNums">Invoice number</param>
        /// <param name="invoiceStates">The invoice status. 0 = Unvalidated, 1 = validated
        /// 2 = Released, 3 = Paid</param>
        /// <param name="distIds">The Distributor Id</param>
        /// <returns>The list of selected invoice headers. This list can be empty</returns>
        [WebMethod]
        public List<InvoiceHeader> QueryInvoicesMultipleArgs(
            List<string> years,
            List<string> months,
            List<string> invoiceNums,
            List<string> invoiceStates,
            List<string> distIds)
        {
            return _controller.QueryInvoicesMultipleArgs(years, months, invoiceNums, invoiceStates, distIds);
        }


        /// <summary>
        /// Inserts the billable in the billable table
        /// </summary>
        /// <param name="bill">The billable update to add</param>
        /// <returns>True if the operation succeeded</returns>
        [WebMethod]
        public bool AddBillable(Billable bill)
        {
            return _controller.AddBillable(bill);
        }

        /// <summary>
        /// Inserts the account in the account table
        /// </summary>
        /// <param name="acc">The account to update</param>
        /// <returns>True if the operation succeeded</returns>
        [WebMethod]
        public bool AddAccount(BOControllerLibrary.Model.Invoicing.Account acc)
        {
            return _controller.AddAccount(acc);
        }

        /// <summary>
        /// Adds a new advance Payment
        /// </summary>
        /// <param name="advancePayment">Advance Payment Value</param>
        /// <param name="distributorId">Distributor ID</param>
        /// <returns>True if the operation succeeded</returns>
        [WebMethod]
        public bool AddAdvancePayment(decimal advancePayment, int distributorId)
        {
            return _controller.AddAdvancePayment(advancePayment, distributorId);
        }

        /// <summary>
        /// Adds a transaction
        /// </summary>
        /// <param name="type">type of transaction: "credit" "debit"</param>
        /// <param name="amount">monetary amount</param>
        /// <param name="distributorId">Distributor ID</param>
        /// <param name="invoiceId">Invoice ID</param>
        /// <returns>True if the operation succeeded</returns>
        [WebMethod]
        public bool AddTransaction(string type, decimal amount, int distributorId, int invoiceId, string remark)
        {
            return _controller.AddTransaction(type, amount, distributorId, invoiceId, remark);
        }

        /// <summary>
        /// Inserts the Distributor billable in the DistBillable table
        /// </summary>
        /// <param name="distBill">The distributor billable to insert</param>
        /// <returns>True if the operation succeeded</returns>
        [WebMethod]
        public bool AddDistBillable(DistBillable distBill)
        {
            return _controller.AddDistBillable(distBill);
        }

        /// <summary>
        /// Adds an invoice detail line
        /// </summary>
        /// <param name="detailLine"></param>
        /// <returns>True if the operation succeeded</returns>
        [WebMethod]
        public bool AddInvoiceDetailLine(InvoiceDetail detailLine)
        {
            return _controller.AddInvoiceDetailLine(detailLine);
        }

        /// <summary>
        /// Modifies the given billable in the Billable table
        /// </summary>
        /// <param name="bill">The billable to update</param>
        /// <returns>True if the operation succeeded</returns>
        [WebMethod]
        public bool UpdateBillable(Billable bill)
        {
            return _controller.UpdateBillable(bill);
        }

        /// <summary>
        /// Modifies the account entity in the Account table
        /// </summary>
        /// <param name="acc">The Account entity</param>
        /// <returns>True if the operation succeeded</returns>
        [WebMethod]
        public bool UpdateAccount(BOControllerLibrary.Model.Invoicing.Account acc)
        {
            return _controller.UpdateAccount(acc);
        }

        /// <summary>
        /// Updates the DistBillable entity in the DistBillable table
        /// </summary>
        /// <param name="distBill">The DistBillable entity to update</param>
        /// <returns>True if the operation succeeded</returns>
        [WebMethod]
        public bool UpdateDistBillable(DistBillable distBill)
        {
            return _controller.UpdateDistBillable(distBill);
        }

        /// <summary>
        /// Updates the invoice in the invoiceHeader table.
        /// Only the InvoiceState and Remarks property will be updated.
        /// </summary>
        /// <param name="invoice">The invoice to update</param>
        /// <returns>True if the operation succeeded</returns>
        [WebMethod]
        public bool UpdateInvoice(InvoiceHeader invoice)
        {
            return _controller.UpdateInvoice(invoice);
        }

        /// <summary>
        /// marks invoices as paid
        /// </summary>
        /// <param name="invoiceIds">The invoice ids</param>
        /// <returns>True if the operation succeeded</returns>
        [WebMethod]
        public bool MarkInvoicesAsPaid(List<int> invoiceIds)
        {
            return _controller.MarkInvoicesAsPaid(invoiceIds);
        }

        /// <summary>
        /// Updates an invoice detail line
        /// </summary>
        /// <param name="detailLine">The invoice detail line to update</param>
        /// <returns>True if the operation succeeded</returns>
        [WebMethod]
        public bool UpdateInvoiceDetail(InvoiceDetail detailLine)
        {
            return _controller.UpdateInvoiceDetail(detailLine);
        }

        /// <summary>
        /// Deletes the invoice detail line referenced by lineId
        /// </summary>
        /// <remarks>
        /// This method effecdtively removes the line from the invoice detail
        /// </remarks>
        /// <param name="lineId">The unique identifer of the invoice detail line</param>
        /// <returns>True if the line is deleted, false otherwise</returns>
        [WebMethod]
        public bool DeleteInvoiceDetailLine(int lineId)
        {
            return _controller.DeleteInvoiceDetailLine(lineId);
        }

        /// <summary>
        /// Returns the invoice detail line referenced by lineId
        /// </summary>
        /// <param name="lineId">The unique identifier of the invoice detail line</param>
        /// <returns>The invoiceDetail object or null if not found</returns>
        [WebMethod]
        public InvoiceDetail GetInvoiceDetailLine(int lineId)
        {
            return _controller.GetInvoiceDetailLine(lineId);
        }

        /// <summary>
        /// Removes a billable from the CMT views
        /// </summary>
        /// <remarks>
        /// Remark that the entity is not deleted from the billable table but
        /// only the Deleted flag is set to "true". So in general this method 
        /// first sets the Delete flag to true and next executes the UpdateBillable
        /// method.
        /// </remarks>
        /// <param name="billableId">The unique identifier of the billable to delete</param>
        /// <returns>True if the operation succeeded</returns>
        [WebMethod]
        public bool DeleteBillable(int billableId)
        {
            return _controller.DeleteBillable(billableId);
        }

        /// <summary>
        /// Deletes an account from the accounts table
        /// </summary>
        /// <remarks>
        /// Only the Deleted flag is set. The record is not deleted from the table!
        /// </remarks>
        /// <param name="accId">The account to delete</param>
        /// <returns>True if the delete succeeded</returns>
        [WebMethod]
        public bool DeleteAccount(int accId)
        {
            return _controller.DeleteAccount(accId);
        }

        /// <summary>
        /// Deletes a given billable from the distributors billable list.
        /// </summary>
        /// <remarks>
        /// Only the Deleted flag is set. The record is not actually deleted from the table!
        /// </remarks>
        /// <param name="distId">The unique identifier of the distributor</param>
        /// <param name="billableId">The unique identifier of the billableId (not the Id of the billable in the distBillable table!)</param>
        /// <returns>True if the operation succeeded</returns>
        [WebMethod]
        public bool DeleteDistBillable(int distId, int billableId)
        {
            return _controller.DeleteDistBillable(distId, billableId);
        }

        /// <summary>
        /// This method returns all billables from the billables table
        /// which are <b>NOT</b> deleted!
        /// </summary>
        /// <returns>A list of Billables with the deleted flag set to false</returns>
        [WebMethod]
        public List<Billable> GetBillables()
        {
            return _controller.GetBillables();
        }

        /// <summary>
        /// This billable retrieves the last billable added to the billables table.
        /// </summary>
        /// <remarks>
        /// Only the last none delete billable is returned
        /// </remarks>
        /// <returns>The last  billable added or null if none</returns>
        [WebMethod]
        public Billable GetLastBillable()
        {
            return _controller.GetLastBillable();
        }

        /// <summary>
        /// Returns the last distributor billable object added to the database
        /// </summary>
        /// <returns>The last added distBillable or null if none</returns>
        [WebMethod]
        public DistBillable GetLastDistributorBillable()
        {
            return _controller.GetLastDistributorBillable();
        }


        /// <summary>
        /// Returns the billables for which the validity period is exceeded.
        /// </summary>
        /// <returns>A list of Billables which are invalid</returns>
        [WebMethod]
        public List<Billable> GetInvalidBillables()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Returns the list of distributor billables which are not deleted 
        /// </summary>
        /// <remarks>
        /// Only the non-deleted distributor billables are returned
        /// </remarks>
        /// <param name="distId">The unique identifier of the distributor</param>
        /// <returns>A list of distBillables for the given distributor</returns>
        [WebMethod]
        public List<DistBillable> GetDistBillablesForDistributor(int distId)
        {
            return _controller.GetDistBillablesForDistributor(distId);
        }


        /// <summary>
        /// This method returns a list of accounts with, for each of the accounts,
        /// the billables selected by means of (part) of the description.
        /// </summary>
        /// <remarks>
        /// The result is not a list of billables but a list of accounts with associated billables.
        /// The list of billables can be retrieved by measn of the account.billables property
        /// </remarks>
        /// <param name="desc">The description or part of the description of the billable</param>
        /// <returns>A list of selected accounts, this list can be empty</returns>
        [WebMethod]
        public List<SelectedAccount> GetBillablesByDescription(String desc)
        {
            return _controller.GetBillablesByDescription(desc);
        }


        /// <summary>
        /// Returns a list of billables for an account
        /// </summary>
        /// <param name="accId">The unique identifier of the account</param>
        /// <returns>List of billables for the specified account</returns>
        [WebMethod]
        public List<Billable> GetBillablesForAccount(int accId)
        {
            return _controller.GetBillablesForAccount(accId);
        }

        /// <summary>
        /// Returns a list of accounts
        /// </summary>
        /// <remarks>
        /// Only the none delete accounts are returned
        /// </remarks>
        /// <returns>List of account. This list can be empty</returns>
        [WebMethod]
        public List<BOControllerLibrary.Model.Invoicing.Account> GetAccounts()
        {
            return _controller.GetAccounts();
        }

        /// <summary>
        /// Returns an account identified by accId
        /// </summary>
        /// <param name="accId">Account object corresponding accId</param>
        /// <returns>A valid account</returns>
        [WebMethod]
        public BOControllerLibrary.Model.Invoicing.Account GetAccount(int accId)
        {
            return _controller.GetAccount(accId);
        }

        /// <summary>
        /// Returns the last account created
        /// </summary>
        /// <remarks>
        /// Only non deleted accounts are created
        /// </remarks>
        /// <returns>The last account created or null if not existing</returns>
        [WebMethod]
        public BOControllerLibrary.Model.Invoicing.Account GetLastAccount()
        {
            return _controller.GetLastAccount();
        }


        /// <summary>
        /// Resturns a billable identified by its bid
        /// </summary>
        /// <param name="bid">The Billable Id</param>
        /// <returns>A billable entity or an empty billable if not found</returns>
        [WebMethod]
        public Billable GetBillable(int bid)
        {
            return _controller.GetBillable(bid);
        }

        /// <summary>
        /// Returns a billable identified by its bid but with the pricing info
        /// set to the specific values set for the given distributor.
        /// </summary>
        /// <param name="bid">The Billable Id</param>
        /// <param name="distId">The unique identifier of the distributor</param>
        /// <returns>A billable entity or an empty billable if not found</returns>
        [WebMethod]
        public Billable GetBillableForDistributor(int bid, int distId)
        {
            return _controller.GetBillableForDistributor(bid, distId);
        }

        /// <summary>
        /// Returns all billables for one distributor with the correct pricing info for that distributor.
        /// The prices from his distributor billables have been taken over into the prices of the ordinary billables.
        /// Only the not-deleted billables are returned.
        /// </summary>
        /// <param name="distId">The unique identifier of the distributor to search the billables for</param>
        /// <returns>The list of billables for that distributor</returns>
        [WebMethod]
        public List<Billable> GetBillablesForDistributor(int distId)
        {
            return _controller.GetBillablesForDistributor(distId);
        }

        /// <summary>
        /// Returns a distributor billable identified by its billable id
        /// </summary>
        /// <param name="distBillableId">Identifier of the billable</param>
        /// <returns>A distBillable instance or null if not found</returns>
        [WebMethod]
        public DistBillable GetDistBillable(int distBillableId)
        {
            return _controller.GetDistBillable(distBillableId);
        }

        /// <summary>
        /// Deletes a distributor billable by its billable id
        /// </summary>
        /// <param name="distBillableId">The unique identifier of the distributor billable</param>
        /// <returns>µTrue if the operation succeeded false otherwise</returns>
        [WebMethod]
        public bool DeleteDistBillableById(int distBillableId)
        {
            return _controller.DeleteDistBillableById(distBillableId);
        }

        /// <summary>
        /// Returns true if the distributor has a distBillable for the given bid
        /// </summary>
        /// <param name="distId">The distributor identifier</param>
        /// <param name="bid">The Billable Identifier</param>
        /// <returns>True if the a distributor billable exists</returns>
        [WebMethod]
        public Boolean DistBillableExists(int distId, int bid)
        {
            return _controller.DistBillableExists(distId, bid);
        }

        /// <summary>
        /// Allows to retrieve the last invoice number used from the configurations table
        /// </summary>
        /// <remarks>
        /// When running the billing engine, the first invoice number used is this last number + 1
        /// </remarks>
        /// <returns>The last invoice number used by the billing engine</returns>
        [WebMethod]
        public int GetLastInvoiceNumber()
        {
            return _controller.GetLastInvoiceNumber();
        }

        /// <summary>
        /// Sets the last invoice number to a new value
        /// </summary>
        /// <remarks>
        /// <b>When running the billing engine, the first invoice number used is this last number + 1</b>
        /// </remarks>
        /// <returns>The last invoice number used by the billing engine</returns>
        [WebMethod]
        public Boolean SetLastInvoiceNumber(int newNum)
        {
            return _controller.SetLastInvoiceNumber(newNum);
        }

        /// <summary>
        /// Allows to retrieve a certain message used from the configurations table
        /// </summary>
        /// <returns>The message depending on the given parameter</returns>
        [WebMethod]
        public string GetInvoiceMessage(string messageParam)
        {
            return _controller.GetInvoiceMessage(messageParam);
        }

        /// <summary>
        /// Sets the message for the given parameter
        /// </summary>
        /// <returns>True if change was succesfull</returns>
        [WebMethod]
        public Boolean SetInvoiceMessage(string messageParam, string messageValue)
        {
            return _controller.SetInvoiceMessage(messageParam, messageValue);
        }

        /// <summary>
        /// Gets the current exchange rate
        /// </summary>
        /// <returns>The most recent exchange rate</returns>
        [WebMethod]
        public decimal GetCurrentExchangeRate()
        {
            return _controller.GetCurrentExchangeRate();
        }

        /// <summary>
        /// Inserts the new exchange rate in the database
        /// </summary>
        /// <returns>True if change was succesfull</returns>
        [WebMethod]
        public Boolean InsertExchangeRate(decimal exchangeRate, DateTime changedDate, Guid userId)
        {
            return _controller.InsertExchangeRate(exchangeRate, changedDate, userId);
        }

        /// <summary>
        /// Gets the current exchange rate
        /// </summary>
        /// <returns>The most recent exchange rate</returns>
        [WebMethod]
        public List<ExchangeRateHistory> GetAllExchangeRates()
        {
            return _controller.GetAllExchangeRates();
        }
    }
}
