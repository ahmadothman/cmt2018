﻿using System.Collections.Generic;
using System.Configuration;
using System.Web.Services;
using BOControllerLibrary.Controllers;
using BOControllerLibrary.Controllers.Interfaces;
using BOControllerLibrary.Model.Tasks;

namespace BOMonitorController
{
    /// <summary>
    /// Summary description for BOTaskControlWS
    /// </summary>
    [WebService(Namespace = "http://cmt.nimera.net/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class BOTaskControlWS : System.Web.Services.WebService, IBOTaskController
    {
        private IBOTaskController _controller;

        public BOTaskControlWS()
        {
            _controller = new BOTaskController(
                connectionString: ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString,
                databaseProvider: ConfigurationManager.ConnectionStrings["ApplicationServices"].ProviderName);

        }

        /// <summary>
        /// Return a list of completed tasks, in descending order
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public List<Task> GetCompletedTasks()
        {
            return _controller.GetCompletedTasks();
        }

        /// <summary>
        /// Return a list of open tasks, in descending order
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public List<Task> GetOpenTasks()
        {
            return _controller.GetOpenTasks();
        }

        /// <summary>
        /// Insert or update a task
        /// </summary>
        /// <param name="task"></param>
        /// <returns></returns>
        [WebMethod]
        public bool UpdateTask(Task task)
        {
            return _controller.UpdateTask(task);
        }

        /// <summary>
        /// Get a list of all tasktypes
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public List<TaskType> GetTaskTypes()
        {
            return _controller.GetTaskTypes();
        }

        /// <summary>
        /// Insert or update a new tasktype
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        [WebMethod]
        public bool UpdateTaskType(TaskType type)
        {
            return _controller.UpdateTaskType(type);
        }

        /// <summary>
        /// Returns the task given by its task id value
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        [WebMethod]
        public Task GetTask(int taskId)
        {
            return _controller.GetTask(taskId);
        }

        /// <summary>
        /// Returns the TaskType corresponding with the given Id
        /// </summary>
        /// <param name="taskTypeId"></param>
        /// <returns>A Task object or null otherwise</returns>
        [WebMethod]
        public TaskType GetTaskType(int taskTypeId)
        {
            return _controller.GetTaskType(taskTypeId);
        }
    }
}
