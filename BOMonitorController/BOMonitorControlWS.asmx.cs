﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Net;
using System.Configuration;
using System.Collections.Specialized;
using BOControllerLibrary.ISPIF.Model;
using BOControllerLibrary.com.astra2connect.ispif;
using BOControllerLibrary.ISPIF.Gateway.Interface;
using BOControllerLibrary.ISPIF.Gateway;
using net.nimera.cmt.BOMonitorController;

namespace net.nimera.cmt.BOMonitorController
{
    /// <summary>
    /// Main Back-Office monitoring controller.
    /// </summary>
    /// <remarks>
    /// The BOMonitorController web service contains web methods which return data about terminals, users, registrations
    /// and so forth.
    /// </remarks>
    [WebService(Namespace = "http://cmt.nimera.net/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class BOMonitorControlWS : System.Web.Services.WebService
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        public BOMonitorControlWS()
        {
        }

        #region Volume Visualization

        /// <summary>
        /// Returns a TerminalInfo object for a terminal identified by its macAddress
        /// </summary>
        /// <remarks>
        /// This information is provided by the CMT database but is retrieved from the
        /// IHubGateway implementation corresponding with the given ISP identifier.
        /// </remarks>
        /// <param name="macAddress">The Terminal MAC address</param>
        /// <returns>A TerminalInfo instance or null if the terminal could not be found</returns>
        [WebMethod]
        public TerminalInfo getTerminalInfoByMacAddress(String macAddress, int ISPId)
        {
            IHubGateWay hubGateway = HubGatewayFactory.GetHubGateway(ISPId);
            return hubGateway.getTerminalInfoByMacAddress(macAddress);
        }

        /// <summary>
        /// Returns a number of terminal details such as the BladeId etc. from the 
        /// Teleport HUB
        /// </summary>
        /// <param name="macAddress"></param>
        /// <param name="ISPId"></param>
        /// <returns></returns>
        [WebMethod]
        public TerminalInfo getTerminalDetailsFromHub(String macAddress, int ISPId)
        {
            IHubGateWay hubGateway = HubGatewayFactory.GetHubGateway(ISPId);
            return hubGateway.getTerminalDetailsFromHub(macAddress, ISPId.ToString());
        }

        /// <summary>
        /// This method retrieves the terminal information from the SES Hub
        /// This override method is used for MAC changes
        /// </summary>
        /// <param name="oldMacAddress">The old terminal MAC address. This MAC address is still in use in the CMT at this point</param>
        /// <param name="newMacAddress">The new terminal MAC address. This MAC address is already being used in the hub</param>
        /// <param name="ISPId">The ISP ID of the NMS ISP</param>
        /// <returns>A completed TerminalInfo object</returns>
        [WebMethod]
        public TerminalInfo getTerminalDetailsFromHubMacChange(string oldMacAddress, string newMacAddress, int ISPId)
        {
            IHubGateWay hubGateway = HubGatewayFactory.GetHubGateway(ISPId);
            return hubGateway.getTerminalDetailsFromHubMacChange(oldMacAddress, newMacAddress, ISPId.ToString());
        }
        
        /// <summary>
        /// This method returns a the list of RTN volume consumption over 7 days
        /// </summary>
        /// <param name="sitId"></param>
        /// <returns></returns>
        [WebMethod]
        public List<TrafficDataPoint> getTrafficViews(string macAddress, DateTime startDateTime, DateTime endDateTime, int ispId)
        {
           IHubGateWay hubGateway = HubGatewayFactory.GetHubGateway(ispId);
           return hubGateway.getTrafficViews(macAddress, startDateTime, endDateTime, ispId);
        }

        [WebMethod]
        public List<TrafficDataPoint> getAccumulatedTrafficViews(string macAddress, DateTime startDateTime, DateTime endDateTime, int ispId)
        {
            IHubGateWay hubGateway = HubGatewayFactory.GetHubGateway(ispId);
            return hubGateway.getAccumulatedTrafficViews(macAddress, startDateTime, endDateTime, ispId);
        }

        /// <summary>
        /// This method returns a list of highpriority volume consumption between a start and
        /// end date.
        /// </summary>
        /// <param name="macAddress">The terminal mac address</param>
        /// <param name="startDateTime">Start date/time of the query</param>
        /// <param name="endDateTime">End date/time of the query</param>
        /// <returns></returns>
        [WebMethod]
        public List<TrafficDataPoint> getHighPriorityTrafficViews(string macAddress, DateTime startDateTime, DateTime endDateTime, int ispId)
        {
            IHubGateWay hubGateway = HubGatewayFactory.GetHubGateway(ispId);
            return hubGateway.getHighPriorityTrafficViews(macAddress, startDateTime, endDateTime, ispId);
        }

        /// <summary>
        /// Returns the accumulated views for Return and Forward volumes since
        /// the FUP date
        /// </summary>
        /// <param name="sitId"></param>
        /// <returns></returns>
        [WebMethod]
        public List<TrafficDataPoint> getAccumulatedViews(string macAddress, int ispId)
        {
            IHubGateWay hubGateway = HubGatewayFactory.GetHubGateway(ispId);
            return hubGateway.getAccumulatedViews(macAddress, ispId);
        }

        /// <summary>
        /// Returns a list of accumulated volume data between a start and end date
        /// </summary>
        /// <remarks>
        /// The data returned is taken from the daily reports
        /// </remarks>
        /// <param name="macAddress">The MAC address of the terminal</param>
        /// <param name="ispId">The ISP identifier</param>
        /// <returns>A list of TrafficDataPoints</returns>
        [WebMethod]
        public List<TrafficDataPoint> getAccumulatedViewsBetweenDates(string macAddress, int ispId, DateTime startDate, DateTime endDate)
        {
            IHubGateWay hubGateway = HubGatewayFactory.GetHubGateway(ispId);
            return hubGateway.getAccumulatedViewsBetweenDates(macAddress, ispId, startDate, endDate);
        }


        #endregion

        #region Terminal Management

        /// <summary>
        /// Activates a terminal in the SES Hub
        /// </summary>
        /// <remarks>
        /// This method returns true or false. The activation is an asynchronous process. A return
        /// value of true means that the ISPIF could process the request. False means that the 
        /// ISPIF for one reason or another could not process the request. The TicketMaster polls the
        /// ISPIF for the final result of the activation.
        /// </remarks>
        /// <param name="endUserId">The End User identifier</param>
        /// <param name="macAddress">The terminals' MAC address</param>
        /// <param name="slaId">The Service Level Agreement identifier</param>
        /// <returns>True if the terminal activation succeeds</returns>
        [WebMethod]
        public bool TerminalActivate(string endUserId, string macAddress, long slaId, int ISPId, Guid userId)
        {
            IHubGateWay hubGateway = HubGatewayFactory.GetHubGateway(ISPId);
            return hubGateway.TerminalActivate(endUserId, macAddress, slaId, ISPId, userId);
        }

        /// <summary>
        /// Activates in the NMS by means of the NMS Gateway
        /// </summary>
        /// <remarks>
        /// This method returns true or false. The activation is an asynchronous process. A return
        /// value of true means that the NMS could process the request. False means that the 
        /// NMS for one reason or another could not process the request. The TicketMaster polls the
        /// NMS for the final result of the activation.
        /// </remarks>
        /// <param name="endUserId">The End User identifier</param>
        /// <param name="macAddress">The terminals' MAC address</param>
        /// <param name="slaId">The Service Level Agreement identifier</param>
        /// <param name="freeZone">The freezone calendar this terminal is associated with</param>
        /// <returns>True if the terminal activation succeeds</returns>
        [WebMethod]
        public bool NMSTerminalActivate(string endUserId, string macAddress, long slaId, int ISPId, Guid userId)
        {
            IHubGateWay hubGateway = HubGatewayFactory.GetHubGateway(ISPId);
            return hubGateway.NMSTerminalActivate(endUserId, macAddress, slaId, ISPId, 0, userId);
        }

        /// <summary>
        /// Activates in the NMS by means of the NMS Gateway. This method allows to pass as freeZone 
        /// calendar Identifier.
        /// </summary>
        /// <remarks>
        /// This method returns true or false. The activation is an asynchronous process. A return
        /// value of true means that the NMS could process the request. False means that the 
        /// NMS for one reason or another could not process the request. The TicketMaster polls the
        /// NMS for the final result of the activation.
        /// </remarks>
        /// <param name="endUserId">The End User identifier</param>
        /// <param name="macAddress">The terminals' MAC address</param>
        /// <param name="slaId">The Service Level Agreement identifier</param>
        /// <param name="freeZone">The freezone calendar this terminal is associated with</param>
        /// <returns>True if the terminal activation succeeds</returns>
        [WebMethod]
        public bool NMSTerminalActivateExt(string endUserId, string macAddress, long slaId, int ISPId, int freeZone, Guid userId)
        {
            IHubGateWay hubGateway = HubGatewayFactory.GetHubGateway(ISPId);
            return hubGateway.NMSTerminalActivate(endUserId, macAddress, slaId, ISPId, freeZone, userId);
        }

        /// <summary>
        /// Activates in the NMS by means of the NMS Gateway. This method allows to specify an IP
        /// address range by means of a mask.
        /// </summary>
        /// <remarks>
        /// This method returns true or false. The activation is an asynchronous process. A return
        /// value of true means that the NMS could process the request. False means that the 
        /// NMS for one reason or another could not process the request. The TicketMaster polls the
        /// NMS for the final result of the activation.
        /// This method can only be a temporary method and must be reviewed depending on the implemenations
        /// of range by the SES Hub. Today the IP address is retrieved from the HUB and it must also be possible
        /// to retrieve the IP mask by means of the terminal info object. The implementation today is a bit doubtful 
        /// and far from optimal :-(
        /// </remarks>
        /// <param name="endUserId">The End User identifier</param>
        /// <param name="macAddress">The terminals' MAC address</param>
        /// <param name="slaId">The Service Level Agreement identifier</param>
        /// <param name="ISPId">The ISP Identifier</param>
        /// <param name="ipMask">The IP Mask which determines the range</param>
        /// <param name="freeZone">The freezone calendar this terminal is associated with</param>
        /// <returns>True if the terminal activation succeeds</returns>
        [WebMethod]
        public bool NMSTerminalActivateWithRange(string endUserId, string macAddress, long slaId, int ISPId, int ipMask, Guid userId)
        {
            IHubGateWay hubGateway = HubGatewayFactory.GetHubGateway(ISPId);
            return hubGateway.NMSTerminalActivateWithRange(endUserId, macAddress, slaId, ISPId, ipMask, 0, userId);
        }

        /// <summary>
        /// Activates in the NMS by means of the NMS Gateway. This method allows to specify an IP
        /// address range by means of a mask and a freezone
        /// </summary>
        /// <remarks>
        /// This method returns true or false. The activation is an asynchronous process. A return
        /// value of true means that the NMS could process the request. False means that the 
        /// NMS for one reason or another could not process the request. The TicketMaster polls the
        /// NMS for the final result of the activation.
        /// This method can only be a temporary method and must be reviewed depending on the implemenations
        /// of range by the SES Hub. Today the IP address is retrieved from the HUB and it must also be possible
        /// to retrieve the IP mask by means of the terminal info object. The implementation today is a bit doubtful 
        /// and far from optimal :-(
        /// </remarks>
        /// <param name="endUserId">The End User identifier</param>
        /// <param name="macAddress">The terminals' MAC address</param>
        /// <param name="slaId">The Service Level Agreement identifier</param>
        /// <param name="ISPId">The ISP Identifier</param>
        /// <param name="ipMask">The IP Mask which determines the range</param>
        /// <param name="freeZone">The freezone calendar this terminal is associated with</param>
        /// <returns>True if the terminal activation succeeds</returns>
        [WebMethod]
        public bool NMSTerminalActivateWithRangeExt(string endUserId, string macAddress, long slaId, int ISPId, int ipMask, int freeZone, Guid userId)
        {
            IHubGateWay hubGateway = HubGatewayFactory.GetHubGateway(ISPId);
            return hubGateway.NMSTerminalActivateWithRange(endUserId, macAddress, slaId, ISPId, ipMask, freeZone, userId);
        }

        /// <summary>
        /// Activates a special satellite diversity terminal in the NMS
        /// </summary>
        /// <remarks>
        /// This method activates a terminal without specifying a IP Mask range! So only
        /// one IP address is allocated to the terminal.
        /// </remarks>
        /// <param name="endUserId">The endUserId of the terminal to activate</param>
        /// <param name="macAddress">The MAC address of the terminal to activate</param>
        /// <param name="ipAddress">The IP address of the terminal to be activated</param>
        /// <param name="slaId">The service pack allocated to the terminal</param>
        /// <param name="ipMask">The IP Mask used to determine the IP range</param>
        /// <param name="freeZone">The freezone calendar associated with this terminal, 0 if not associated with
        /// a freezone SLA</param>
        /// <param name="primaryMac">The MAC address of the primary terminal in the setup</param>
        /// <param name="secondaryMac">The MAC address of the secondary terminal in the setup</param>
        /// <returns>true if the activation succeeded</returns>
        [WebMethod]
        public bool NMSActivateSatelliteDiversityTerminal(string endUserId, string macAddress, string ipAddress, long slaId, int ISPId, int freeZone, string primaryMac, string secondaryMac, Guid userId)
        {
            IHubGateWay hubGateway = HubGatewayFactory.GetHubGateway(ISPId);
            return hubGateway.NMSActivateSatelliteDiversityTerminal(endUserId, macAddress, ipAddress, slaId, ISPId, freeZone, primaryMac, secondaryMac, userId);
        }

        /// <summary>
        /// Suspends a terminal in the ISPIF
        /// </summary>
        /// <remarks>
        /// As for the terminal activation, this method works asynchronous. 
        /// </remarks>
        /// <param name="endUserId">The end user identifier</param>
        /// <param name="macAddress">The MAC address</param>
        /// <returns>True if the suspension was accepted </returns>
        [WebMethod]
        public bool TerminalSuspend(string endUserId, string macAddress, int ISPId, Guid userId)
        {
            IHubGateWay hubGateway = HubGatewayFactory.GetHubGateway(ISPId);
            return hubGateway.TerminalSuspend(endUserId, macAddress, ISPId, userId);
        }

        /// <summary>
        /// Suspends a terminal in the NMS
        /// </summary>
        /// <remarks>
        /// As for the terminal activation, this method works asynchronous. 
        /// </remarks>
        /// <param name="endUserId">The end user identifier</param>
        /// <param name="macAddress">The MAC address</param>
        /// <returns>True if the suspension was accepted </returns>
        [WebMethod]
        public bool NMSTerminalSuspend(string endUserId, string macAddress, int ISPId, Guid userId)
        {
            IHubGateWay hubGateway = HubGatewayFactory.GetHubGateway(ISPId);
            return hubGateway.NMSTerminalSuspend(endUserId, macAddress, ISPId, userId);
        }

        /// <summary>
        /// Changes the weight of the terminal in the NMS
        /// </summary>
        /// <param name="endUserId">The endUserId of the terminal to activate</param>
        /// <param name="macAddress">The MAC address of the terminal to activate</param>
        /// <param name="newWeight">The new weight allocated to the terminal</param>
        /// <param name="customerId">The ID of the customer the terminal belongs to</param>
        /// <param name="currentState">The current admin state of the terminal</param>
        /// <returns>true if the application succeeded</returns>
        [WebMethod]
        public bool NMSTerminalChangeWeight(string endUserId, string macAddress, int newWeight, int customerId, AdmStatus currentState, int ispId, Guid userId)
        {
            IHubGateWay hubGateway = HubGatewayFactory.GetHubGateway(ispId);
            return hubGateway.NMSTerminalChangeWeight(endUserId, macAddress, newWeight, customerId, currentState, ispId, userId);
        }

        /// <summary>
        /// Reactivate a terminal in the ISPIF
        /// </summary>
        /// <param name="endUserId">The end user identifier</param>
        /// <param name="macAddress">The terminals' MAC address</param>
        /// <returns>True if the re-activation is accepted by the ISPIF</returns>
        [WebMethod]
        public bool TerminalReActivate(string endUserId, string macAddress, int ISPId, Guid userId)
        {
            IHubGateWay hubGateway = HubGatewayFactory.GetHubGateway(ISPId);
            return hubGateway.TerminalReActivate(endUserId, macAddress, ISPId, userId);
        }


        /// <summary>
        /// Reactivate a terminal in the NMS
        /// </summary>
        /// <param name="endUserId">The end user identifier</param>
        /// <param name="macAddress">The terminals' MAC address</param>
        /// <returns>True if the re-activation is accepted by the NMS</returns>
        [WebMethod]
        public bool NMSTerminalReActivate(string endUserId, string macAddress, int ISPId, Guid userId)
        {
            IHubGateWay hubGateway = HubGatewayFactory.GetHubGateway(ISPId);
            return hubGateway.NMSTerminalReActivate(endUserId, macAddress, ISPId, userId);
        }

        /// <summary>
        /// Decommission a terminal in the ISPIF
        /// </summary>
        /// <param name="endUserId">The end user idnetifier</param>
        /// <param name="macAddress">The terminals' MAC address</param>
        /// <returns>True if the decommissioning of the terminal succeeded</returns>
        [WebMethod]
        public bool TerminalDecommission(string endUserId, string macAddress, int ISPId, Guid userId)
        {
            IHubGateWay hubGateway = HubGatewayFactory.GetHubGateway(ISPId);
            return hubGateway.TerminalDecommission(endUserId, macAddress, ISPId, userId);
        }

        /// <summary>
        /// Decommission a terminal in the NMS
        /// </summary>
        /// <param name="endUserId">The end user idnetifier</param>
        /// <param name="macAddress">The terminals' MAC address</param>
        /// <returns>True if the decommissioning of the temrinal succeeded</returns>
        [WebMethod]
        public bool NMSTerminalDecommission(string endUserId, string macAddress, int ISPId, Guid userId)
        {
            IHubGateWay hubGateway = HubGatewayFactory.GetHubGateway(ISPId);
            return hubGateway.NMSTerminalDecommission(endUserId, macAddress, ISPId, userId);
        }

        /// <summary>
        /// Resets the users' volume usage in the ISPIF
        /// </summary>
        /// <param name="sitId">The Site identifier</param>
        /// <param name="ispId">The ISP identifier</param>
        /// <param name="macAddress">The terminals' MAC address</param>
        [WebMethod]
        public bool TerminalUserReset(string endUserId, int ispId)
        {
            IHubGateWay hubGateway = HubGatewayFactory.GetHubGateway(ispId);
            return hubGateway.TerminalUserReset(endUserId, ispId);
        }

        /// <summary>
        /// Resets the users' volume usage in the NMS
        /// </summary>
        /// <param name="sitId">The Site identifier</param>
        /// <param name="ispId">The ISP identifier</param>
        /// <param name="macAddress">The terminals' MAC address</param>
        [WebMethod]
        public bool NMSTerminalUserReset(string endUserId, int ispId)
        {
            IHubGateWay hubGateway = HubGatewayFactory.GetHubGateway(ispId);
            return hubGateway.NMSTerminalUserReset(endUserId, ispId);
        }

        /// <summary>
        /// Changes the SLA of the given endUserId in the ISPIF
        /// </summary>
        /// <param name="endUserId">The end user identifier</param>
        /// <param name="macAddress">The MAC address of the terminal</param>
        /// <param name="newSlaId">The new service pack identifier</param>
        /// <param name="currentStatus">The current status of the terminal</param>
        /// <returns>True if the change sla call is accepted by the ISPIF</returns>
        [WebMethod]
        public bool TerminalChangeSla(string endUserId, string macAddress, int newSlaId, AdmStatus currentStatus, int ISPId, Guid userId)
        {
            IHubGateWay hubGateway = HubGatewayFactory.GetHubGateway(ISPId);
            return hubGateway.TerminalChangeSla(endUserId, macAddress, newSlaId, currentStatus, ISPId, userId);
        }

        /// <summary>
        /// Changes the SLA of the given endUserId in the NMS
        /// </summary>
        /// <param name="endUserId">The end user identifier</param>
        /// <param name="macAddress">The MAC address of the terminal</param>
        /// <param name="newSlaId">The new service pack identifier</param>
        /// <param name="currentStatus">The current status of the terminal</param>
        /// <returns>True if the change sla call is accepted by the NMS</returns>
        [WebMethod]
        public bool NMSTerminalChangeSla(string endUserId, string macAddress, int newSlaId, AdmStatus currentStatus, int ISPId, Guid userId)
        {
            IHubGateWay hubGateway = HubGatewayFactory.GetHubGateway(ISPId);
            return hubGateway.NMSTerminalChangeSla(endUserId, macAddress, newSlaId, currentStatus, ISPId, userId);
        }

        
        /// <summary>
        /// Returns a CmtRequestTicket which reflects the status of the ticket. This method retrieves the
        /// ticket information from the ISPIF
        /// </summary>
        /// <param name="requestTicketId">The target request ticket identifier</param>
        /// <param name="ISPId">The ISP identifier</param>
        /// <returns>A valid CMT Request Ticket or null if the ticket does not exist</returns>
        [WebMethod]
        public CmtRequestTicket LookupRequestTicket(string requestTicketId, int ISPId)
        {
            IHubGateWay hubGateway = HubGatewayFactory.GetHubGateway(ISPId);
            return hubGateway.LookupRequestTicket(requestTicketId, ISPId);
        }


        /// <summary>
        /// Returns a CmtRequestTicket which reflects the status of the ticket. This method retrieves the
        /// ticket information from the NMS
        /// </summary>
        /// <param name="requestTicketId">The target request ticket identifier</param>
        /// <param name="ISPId">The ISP identifier</param>
        /// <returns>A valid CMT Request Ticket or null if the ticket does not exist</returns>
        [WebMethod]
        public CmtRequestTicket NMSLookupRequestTicket(string requestTicketId, int ISPId)
        {
            IHubGateWay hubGateway = HubGatewayFactory.GetHubGateway(ISPId);
            return hubGateway.NMSLookupRequestTicket(requestTicketId, ISPId);
        }

        /// <summary>
        /// Changes the MacAddress of a terminal in the ISPIF
        /// </summary>
        /// <param name="endUserId">The End User Identifier</param>
        /// <param name="newMacAddress">The new MAC address</param>
        /// <param name="ispId">The ISP Identifier</param>
        /// <returns>True if successful</returns>
        [WebMethod]
        public bool ChangeMacAddress(string endUserId, string oriMacAddress, int ispId, string newMacAddress, Guid userId)
        {
            IHubGateWay hubGateway = HubGatewayFactory.GetHubGateway(ispId);
            return hubGateway.ChangeMacAddress(endUserId, oriMacAddress, newMacAddress, ispId, userId);
        }

        /// <summary>
        /// Changes the MacAddress of a terminal in the NMS
        /// </summary>
        /// <param name="endUserId">The End User Identifier</param>
        /// <param name="newMacAddress">The new MAC address</param>
        /// <param name="ispId">The ISP Identifier</param>
        /// <returns>True if successful</returns>
        [WebMethod]
        public bool NMSChangeMacAddress(string endUserId, string oriMacAddress, int ispId, string newMacAddress, Guid userId)
        {
            IHubGateWay hubGateway = HubGatewayFactory.GetHubGateway(ispId);
            return hubGateway.NMSChangeMacAddress(endUserId, oriMacAddress, newMacAddress, ispId, userId);
        }

        /// <summary>
        /// This method allows to change the IP Address AND Mask in the SatADSL NMS
        /// </summary>
        /// <remarks>
        /// The old and new IP Masks may be identical while the old and new IP Addresses
        /// can never be identical.
        /// </remarks>
        /// <param name="endUserId">The unique end user Id</param>
        /// <param name="oldIPAddress">The original IP Address</param>
        /// <param name="oldIPMask">The original IP Mask</param>
        /// <param name="newIPAddress">The new IP Address</param>
        /// <param name="newIPMask">The new IP Mask</param>
        /// <param name="ispId">The ISP Identifier</param>
        /// <returns>True if the change was registered by NMS successfully</returns>
        [WebMethod]
        public bool NMSChangeIPAddress(string endUserId, string macAddress, string oldIPAddress, int oldIPMask,
                                                    string newIPAddress, int newIPMask, int ispId, Guid userId)
        {
            IHubGateWay hubGateway = HubGatewayFactory.GetHubGateway(ispId);
            return hubGateway.NMSChangeIPAddress(endUserId, macAddress, oldIPAddress, 
                                                oldIPMask, newIPAddress, newIPMask, ispId, userId);
        }

        /// <summary>
        /// This method allows to update the freezone information for a terminal
        /// </summary>
        /// <param name="FreeZoneId">The identifier of the freezone allocated to the terminal</param>
        /// <param name="macAddress">The MAC address of the terminal</param>
        /// <param name="FreeZoneFlag">If this flag is set to true, a freezone option is allocated to the terminal. In
        /// case this value is false, the terminal does not have the freezone option</param>
        /// <returns></returns>
        [WebMethod]
        public bool NMSModifyFreeZoneForTerminal(string endUserId, int freeZoneId, int ispId)
        {
            IHubGateWay hubGateway = HubGatewayFactory.GetHubGateway(ispId);
            return hubGateway.NMSModifyFreeZoneForTerminal(endUserId, freeZoneId);
        }

        /// <summary>
        /// Add volume to a subscription in the ISPIF
        /// </summary>
        /// <remarks>
        /// The subscription must have a voucher based SLA. This is a synchronous method.
        /// </remarks>
        /// <param name="sitId">The Site Identifier</param>
        /// <param name="volumeCode">The Volume code which defines the amount of volume to add</param>
        /// <param name="ispId">The ISP identifier</param>
        /// <returns>True if the AddVolume mehtod succeeded</returns>
        [WebMethod]
        public bool AddVolume(int sitId, int volumeCode, int ispId)
        {
            IHubGateWay hubGateway = HubGatewayFactory.GetHubGateway(ispId);
            return hubGateway.AddVolume(sitId, volumeCode, ispId);
        }

        /// <summary>
        /// Add volume to a subscription in the NMS
        /// </summary>
        /// <remarks>
        /// The subscription must have a voucher based SLA. This is a synchronous method.
        /// </remarks>
        /// <param name="sitId">The Site Identifier</param>
        /// <param name="volumeCode">The Volume code which defines the amount of volume to add</param>
        /// <param name="ispId">The ISP identifier</param>
        /// <returns>True if the AddVolume mehtod succeeded</returns>
        [WebMethod]
        public bool NMSAddVolume(int sitId, int volumeCode, int ispId)
        {
            IHubGateWay hubGateway = HubGatewayFactory.GetHubGateway(ispId);
            return hubGateway.NMSAddVolume(sitId, volumeCode, ispId);
        }

        /// <summary>
        /// Changes the ISP of a terminal in the ISPIF
        /// </summary>
        /// <param name="endUserId">The enduser id</param>
        /// <param name="oldISP">The old (actual) ISP</param>
        /// <param name="newISP">The new ISP</param>
        /// <returns>True if the ISP is successfully changed</returns>
        [WebMethod]
        public bool ChangeISP(string endUserId, int oldISP, int newISP)
        {
            IHubGateWay hubGateway = HubGatewayFactory.GetHubGateway(oldISP);
            return hubGateway.ChangeISP(endUserId, oldISP, newISP);
        }


        /// <summary>
        /// Changes the ISP of a terminal in the NMS
        /// </summary>
        /// <param name="endUserId">The enduser id</param>
        /// <param name="oldISP">The old (actual) ISP</param>
        /// <param name="newISP">The new ISP</param>
        /// <returns>True if the ISP is successfully changed</returns>
        [WebMethod]
        public bool NMSChangeISP(string endUserId, int oldISP, int newISP)
        {
            IHubGateWay hubGateway = HubGatewayFactory.GetHubGateway(oldISP);
            return hubGateway.NMSChangeISP(endUserId, oldISP, newISP);
        }

        /// <summary>
        /// Creates a new service pack in the NMS
        /// </summary>
        /// <param name="servicePack">The service pack to be created</param>
        /// <returns>True if the operation succeeded</returns>
        [WebMethod]
        public bool NMSCreateServicePack(BOControllerLibrary.NMSGatewayServiceRef.ServicePack servicePack)
        {
            IHubGateWay hubGateway = HubGatewayFactory.GetHubGateway((int)servicePack.IspId);
            return hubGateway.NMSCreateServicePack(servicePack);
        }

        /// <summary>
        /// Updates a service pack in the NMS
        /// </summary>
        /// <param name="servicePack">The service pack to be updated</param>
        /// <returns>True if the operation succeeded</returns>
        [WebMethod]
        public bool NMSUpdateServicePack(BOControllerLibrary.NMSGatewayServiceRef.ServicePack servicePack)
        {
            IHubGateWay hubGateway = HubGatewayFactory.GetHubGateway((int)servicePack.IspId);
            if (servicePack.ServiceClass == 7)
                servicePack.ServiceClass = 1;
            return hubGateway.NMSUpdateServicePack(servicePack);
        }

        /// <summary>
        /// Deletes a service pack in the NMS
        /// </summary>
        /// <param name="slaId">The identifier of the service pack to be deleted</param>
        /// <param name="ispId">The identifier of the ISP with the given SLA</param>
        /// <returns>True if the operation succeeded</returns>
        [WebMethod]
        public bool NMSDeleteServicePack(int slaId, int ispId)
        {
            IHubGateWay hubGateway = HubGatewayFactory.GetHubGateway(ispId);
            return hubGateway.NMSDeleteServicePack(slaId);
        }

        /// <summary>
        /// Checks if the SLA (which eventually needs to be deleted) has associated terminals
        /// </summary>
        /// <param name="slaId">The identifier of the SLA</param>
        /// <param name="IspId">The identifier of the ISP with the given SLA</param>
        /// <returns>True when the SLA has still associated terminals</returns>
        [WebMethod]
        public bool NMSSlaHasAssociatedTerminals(int slaId, int ispId)
        {
            IHubGateWay hubGateway = HubGatewayFactory.GetHubGateway(ispId);
            return hubGateway.NMSSlaHasAssociatedTerminals(slaId);
        }

        /// <summary>
        /// Creates a freezone in the NMS
        /// </summary>
        /// <param name="freeZone">The freezone to be created</param>
        /// <returns>True if succesful</returns>
        [WebMethod]
        public bool NMScreateFreeZone(BOControllerLibrary.NMSGatewayServiceRef.FreeZone freeZone, int ispId)
        {
            IHubGateWay hubGateway = HubGatewayFactory.GetHubGateway(ispId);
            return hubGateway.NMScreateFreeZone(freeZone);
        }

        /// <summary>
        /// Updates a freezone in the NMS
        /// </summary>
        /// <param name="freeZone">The freezone to be updated</param>
        /// <returns>True if succesful</returns>
        [WebMethod]
        public bool NMSupdateFreeZone(BOControllerLibrary.NMSGatewayServiceRef.FreeZone freeZone, int ispId)
        {
            IHubGateWay hubGateway = HubGatewayFactory.GetHubGateway(ispId);
            return hubGateway.NMSupdateFreeZone(freeZone);
        }

        /// <summary>
        /// Fetches a list of all freezones in the NMS database
        /// </summary>
        /// <returns>The list of freezones</returns>
        [WebMethod]
        public List<BOControllerLibrary.NMSGatewayServiceRef.FreeZone> NMSgetFreeZones(int ispId)
        {
            IHubGateWay hubGateway = HubGatewayFactory.GetHubGateway(ispId);
            return hubGateway.NMSgetFreeZones();
        }

        /// <summary>
        /// Fetches the details of a specific freezone from the NMS
        /// </summary>
        /// <param name="freeZoneId">The ID of the freezone</param>
        /// <returns>The freezone</returns>
        [WebMethod]
        public BOControllerLibrary.NMSGatewayServiceRef.FreeZone NMSgetFreeZone(int freeZoneId, int ispId)
        {
            IHubGateWay hubGateway = HubGatewayFactory.GetHubGateway(ispId);
            return hubGateway.NMSgetFreeZone(freeZoneId);
        }

        /// <summary>
        /// Pre-provision a terminal when using the ISPÏF emulator
        /// </summary>
        /// <param name="macAddress">The MAC address of the terminal to pre-provision</param>
        /// <returns>True if the terminal was preprovisioned successfully</returns>
        [WebMethod]
        public bool PreProvisionTerminal(string macAddress)
        {
            ISPIFEmulatorGateWay ispifEmulatorGateWay = new ISPIFEmulatorGateWay();
            return ispifEmulatorGateWay.PreProvisionTerminal(macAddress);
        }

        /// <summary>
        /// Returns RTN Value for a specific terminal
        /// </summary>
        /// <remarks>
        /// This information is provided by the HUB retrieved from the
        /// IHubGateway implementation corresponding with the given ISP identifier.
        /// </remarks>
        /// <param name="sitId">The Terminal sit id</param>
        /// <returns>A TerminalInfo instance or null if the terminal could not be found</returns>
        [WebMethod]
        public string getRTNValue(string sitId, int ISPId)
        {
            IHubGateWay hubGateway = HubGatewayFactory.GetHubGateway(ISPId);
            return hubGateway.getRTNValue(ISPId.ToString(),sitId);
        }
        #endregion

    }
}
