﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Web.Script.Services;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using BOControllerLibrary.Controllers;
using BOControllerLibrary.Model.Distributor;
using BOControllerLibrary.Model.ISP;
using BOControllerLibrary.Model.Organization;
using BOControllerLibrary.Model.Terminal;
using BOControllerLibrary.Model.User;
using BOControllerLibrary.Model;
using BOControllerLibrary.Model.VNO;
using BOControllerLibrary.Model.Log;

namespace net.nimera.cmt.BOAccountingController
{
    /// <summary>
    ///   Main Back-Office Accounting Controller 
    /// </summary>
    [WebService(Namespace = "http://cmt.nimera.net/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    [ScriptService]
    public class BOAccountingControlWS : WebService, IBOAccountingController
    {
        private IBOAccountingController _controller;
        BOLogController _logger = null;
        
        public BOAccountingControlWS()
        {
            // in time we'll use dependency injection here for lifetime and implementation activation.
            _controller = new BOControllerLibrary.Controllers.BOAccountingController(
                connectionString: ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString,
                databaseProvider: ConfigurationManager.ConnectionStrings["ApplicationServices"].ProviderName);
            _logger = new BOLogController(connectionString: ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString,
                databaseProvider: ConfigurationManager.ConnectionStrings["ApplicationServices"].ProviderName);
        }

        [WebMethod]
        public List<Terminal> GetTerminalsByDistributor(string distributorUUid)
        {
            return _controller.GetTerminalsByDistributor(distributorUUid);
        }

        [WebMethod]
        public List<Terminal> GetTerminalsByDistributorForDistributor(string distributorUUid)
        {
            return _controller.GetTerminalsByDistributorForDistributor(distributorUUid);
        }

        [WebMethod]
        public List<Terminal> GetTerminalsByDistributorId(int distributorId)
        {
            return _controller.GetTerminalsByDistributorId(distributorId);
        }

        /// <summary>
        /// Fetches the list of organizations for a distributor
        /// </summary>
        /// <param name = "distributorId">The GUID of the distributor</param>
        /// <returns>A list of organizations</returns>
        [WebMethod]
        public List<Organization> GetOrganisationsByDistributor(string distributorId)
        {
            return _controller.GetOrganisationsByDistributor(distributorId);
        }

        /// <summary>
        /// Fetches the list of customer VNOs for a distributor
        /// </summary>
        /// <param name = "distributorId">The GUID of the distributor</param>
        /// <returns>A list of organizations</returns>
        [WebMethod]
        public List<Organization> GetCustomerVNOsByDistributor(string distributorId)
        {
            return _controller.GetCustomerVNOsByDistributor(distributorId);
        }

        /// <summary>
        /// Fetches all customer VNOs
        /// </summary>
        /// <returns>A list of organizations</returns>
        [WebMethod]
        public List<Organization> GetCustomerVNOs()
        {
            return _controller.GetCustomerVNOs();
        }

        /// <summary>
        /// Fetches the list of organizations for a specific distributor
        /// </summary>
        /// <param name = "distributorId">The ID of the distributor</param>
        /// <returns>A list of organizations</returns>
        [WebMethod]
        public List<Organization> GetOrganisationsByDistributorId(string distributorId)
        {
            return _controller.GetOrganisationsByDistributorId(distributorId);
        }

        [WebMethod]
        public Terminal GetUserTerminal(int endUserId)
        {
            return _controller.GetUserTerminal(endUserId);
        }

        [WebMethod]
        public List<Terminal> GetTerminalRequests()
        {
            return _controller.GetTerminalRequests();
        }
        
        [WebMethod]
        public Distributor GetDistributorForUser(string userId)
        {
            return _controller.GetDistributorForUser(userId);
        }

        /// <summary>
        /// Returns the organization corresponding to the logged-in user.
        /// </summary>
        /// <param name="userId">Unique user id (guid)</param>
        /// <returns>The Organization entity corresponding to the user or null if not found</returns>
        [WebMethod]
        public Organization GetOrganizationForUser(string userId)
        {
           return _controller.GetOrganizationForUser(userId);
        }

        [WebMethod]
        public List<Terminal> GetTerminalsByOrganization(int organizationId)
        {
            return _controller.GetTerminalsByOrganization(organizationId);
        }

        /// <summary>
        /// Returns a list of terminals which are linked to an SLA.
        /// </summary>
        /// <param name="slaId">The SLA identifier for which</param>
        /// <returns>A list of terminals with that specific SLA</returns>
        [WebMethod]
        public List<Terminal> GetTerminalsBySla(int slaId) 
         {
            return _controller.GetTerminalsBySla(slaId);
        }

        /// <summary>
        /// Returns a terminal object identified by the given sitId
        /// </summary>
        /// <param name="sitId">The target SitId</param>
        /// <param name="ispId">The ISP identifier</param>
        /// <returns>A Terminal object or null if the SitId does not exist</returns>
        [WebMethod]
        public Terminal GetTerminalDetailsBySitId(int sitId, int ispId)
        {
            return _controller.GetTerminalDetailsBySitId(sitId, ispId);
        }

        [WebMethod]
        public Terminal GetTerminalDetailsByIp(string ipAddress)
        {
            return _controller.GetTerminalDetailsByIp(ipAddress);
        }

        [WebMethod]
        public Terminal GetTerminalDetailsByMAC(string macAddress)
        {
            return _controller.GetTerminalDetailsByMAC(macAddress);
        }

        /// <summary>
        /// Returns a list of terminal matching a complete or partly given MAC address. 
        /// </summary>
        /// <param name="macAddress">A complete or partly matching mac address</param>
        /// <returns>A list with matching terminals, this list can be empty!</returns>
        [WebMethod]
        public List<Terminal> GetTerminalsByMac(string macAddress)
        {
            return _controller.GetTerminalsByMac(macAddress);
        }

        /// <summary>
        /// Returns a terminal list corresponding 
        /// </summary>
        /// <param name="statusId">The status identifier</param>
        /// <returns>A list of matching terminals</returns>
        [WebMethod]
        public List<Terminal> GetTerminalsByStatus(int statusId)
        {
            return _controller.GetTerminalsByStatus(statusId);
        }

        [WebMethod]
        public DataTable GetSuspendedTerminals(DateTime startDate, DateTime endDate)
        {
            string queryCmd =
                "SELECT T.MacAddress AS 'MAC Address', T.FullName AS Terminal, TA.SuspensionDate, S.SlaName AS SLA, D.FullName AS Distributor " +
                "FROM Terminals AS T LEFT OUTER JOIN " +
                "(SELECT MacAddress, max(ActionDate) AS SuspensionDate FROM TerminalActivityLog WHERE TerminalActivityTypeId = 200 GROUP BY MacAddress) " +
                "AS TA ON T.MacAddress = TA.MacAddress LEFT OUTER JOIN " +
                "ServicePacks AS S ON T.SlaID = S.SlaID LEFT OUTER JOIN " +
                "Distributors AS D ON T.DistributorId = D.Id " +
                "WHERE T.AdmStatus = 1 AND (SuspensionDate >= @StartDate AND SuspensionDate <= @EndDate)" +
                "ORDER BY T.FullName";

            string connectionString = ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;

            DataTable userActivityData = new DataTable();
            userActivityData.TableName = "Activities";
            SqlConnection conn = new SqlConnection(connectionString);
            SqlDataAdapter adapter = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand(queryCmd, conn);

            SqlParameter startDateParam = new SqlParameter();
            startDateParam.ParameterName = "@StartDate";
            startDateParam.SqlDbType = SqlDbType.Date;
            startDateParam.Value = startDate.Date;
            cmd.Parameters.Add(startDateParam);

            SqlParameter endDateParam = new SqlParameter();
            endDateParam.ParameterName = "@EndDate";
            endDateParam.SqlDbType = SqlDbType.Date;
            endDateParam.Value = endDate.Date;
            cmd.Parameters.Add(endDateParam);

            adapter.SelectCommand = cmd;

            conn.Open();

            try
            {
                adapter.Fill(userActivityData);
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtException = new CmtApplicationException();
                cmtException.ExceptionDesc = ex.Message;
                cmtException.ExceptionDateTime = DateTime.Now;
                cmtException.UserDescription = "GetSuspendedTerminals method failed";
                cmtException.ExceptionStacktrace = ex.StackTrace;
                _logger.LogApplicationException(cmtException);
            }
            finally
            {
                conn.Close();
            }

            return userActivityData;
        }

        /// <summary>
        /// Returns a list of terminals corresponding to the given criterion.
        /// </summary>
        /// <param name="dt">The matching DateTime value</param>
        /// <param name="criterion">Determines the relation of the terminals ExpiryDate to the
        /// given date.
        /// <ul>
        ///     <li>Equals</li>
        ///     <li>Less then (Earlier)</li>
        ///     <li>More then (Later)</li>
        ///     <li>Less or Equal</li>
        ///     <li>More or Equal</li>
        /// </ul>
        /// </param>
        /// <returns>A list of matching terminals</returns>
        [WebMethod]
        public List<Terminal> GetTerminalsByExpiryDate(DateTime dt, string criterion)
        {
            return _controller.GetTerminalsByExpiryDate(dt, criterion);
        }

        [WebMethod]
        public Distributor GetDistributor(int distributorId)
        {
            return _controller.GetDistributor(distributorId);
        }

        [WebMethod]
        public Organization GetOrganization(int organizationId)
        {
            return _controller.GetOrganization(organizationId);
        }

        [WebMethod]
        public EndUser GetEndUser(string userId)
        {
            return _controller.GetEndUser(userId);
        }

        /// <summary>
        /// Returns the EndUser by means of its unique identifier
        /// </summary>
        /// <param name="Id">The Unique Identifier from the EndUser table</param>
        /// <returns>The EndUser corresponding with the end user id or null if not found</returns>
        [WebMethod]
        public EndUser GetEndUserById(int Id)
        {
            string endUserQueryCmd = "SELECT Id, FirstName, LastName, EMail, Phone, MobilePhone, SitId FROM EndUsers WHERE Id = @Id";
            string connectionString = ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
            EndUser endUser = null;

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();

                using (SqlCommand cmd = new SqlCommand(endUserQueryCmd, conn))
                {
                    cmd.Parameters.AddWithValue("@Id", Id);

                    try
                    {
                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            endUser = new EndUser();
                            if (!reader.IsDBNull(0))
                            {
                                endUser.Id = reader.GetInt32(0);
                            }
                            if (!reader.IsDBNull(1))
                            {
                                endUser.FirstName = reader.GetString(1);
                            }
                            if (!reader.IsDBNull(2))
                            {
                                endUser.LastName = reader.GetString(2);
                            }
                            if (!reader.IsDBNull(3))
                            {
                                endUser.Email = reader.GetString(3);
                            }
                            if (!reader.IsDBNull(4))
                            {
                                endUser.Phone = reader.GetString(4);
                            }
                            if (!reader.IsDBNull(5))
                            {
                                endUser.MobilePhone = reader.GetString(5);
                            }
                            if (!reader.IsDBNull(6))
                            {
                                endUser.SitId = (int)reader.GetInt32(6);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        CmtApplicationException cmtException = new CmtApplicationException();
                        cmtException.ExceptionDesc = ex.Message;
                        cmtException.ExceptionDateTime = DateTime.Now;
                        cmtException.UserDescription = "GetTermActivitiesBetweenDates method failed";
                        cmtException.ExceptionStacktrace = ex.StackTrace;
                        _logger.LogApplicationException(cmtException);
                    }
                }

                return endUser;
            }
            
        
        }

        /// <summary>
        /// Returns a list of dates of when manual FUP Resets were made between a selected time period
        /// </summary>
        /// <param name="SitId">SitId</param>
        /// <param name="IspId">IspId</param>
        /// <param name="startDate">Beginning of time interval</param>
        /// <param name="endDate">End of time interval</param>
        /// <returns></returns>
        [WebMethod]
        public List<DateTime> GetTerminalManualFupResets(int SitId, int IspId, DateTime startDate, DateTime endDate)
        {
            return _controller.GetTerminalManualFupResets(SitId, IspId, startDate, endDate);
        }

        /// <summary>
        /// Returns a list of all added voucher volume events made between a selected time period
        /// </summary>
        /// <param name="SitId">SitId</param>
        /// <param name="IspId">IspId</param>
        /// <param name="startDate">Beginning of time interval</param>
        /// <param name="endDate">End of time interval</param>
        /// <returns></returns>
        [WebMethod]
        public List<DateTime> GetTerminalVolumeVoucherAdds(int SitId, int IspId, DateTime startDate, DateTime endDate)
        {
            return _controller.GetTerminalVolumeVoucherAdds(SitId, IspId, startDate, endDate);
        }

        /// <summary>
        /// Returns a list of a terminal's activations
        /// </summary>
        /// <param name="SitId">SitId</param>
        /// <param name="IspId">IspId</param>
        /// <returns></returns>
        [WebMethod]
        public List<DateTime> GetTerminalActivations(int SitId, int IspId)
        {
            return _controller.GetTerminalActivations(SitId, IspId);
        }

        /// <summary>
        /// Returns a list of accounting records between a begin and end date
        /// </summary>
        /// <param name="startDate">The start date of the query</param>
        /// <param name="endDate">End date of th query</param>
        /// <returns>A DataTable object, which can be empty</returns>
        public DataTable GetAccountReport(DateTime startDate, DateTime endDate)
        {
            return _controller.GetAccountReport(startDate, endDate);
        }

        /// <summary>
        /// Returns a list of terminal activities for a given distributor
        /// </summary>
        /// <param name="startDate">The start date of the report</param>
        /// <param name="endDate">The end date of the report</param>
        /// <param name="DistributorId">The distributor Id</param>
        /// <returns>A list of terminal billable terminal activity records</returns>
        [WebMethod]
        public DataTable GetAccountReportByDistributor(DateTime startDate, DateTime endDate, int DistributorId)
        {
            return _controller.GetAccountReportByDistributor(startDate, endDate, DistributorId);
        }


        /// <summary>
        /// Returns the list of end users
        /// </summary>
        /// <returns>The list of end users</returns>
        [WebMethod]
        public List<EndUser> GetEndUsers()
        {
            string endUsersQuery = "SELECT Id, FirstName, LastName, EMail, Phone, MobilePhone, SitId FROM EndUsers WHERE Deleted != 1  ORDER BY FirstName";
            string connectionString = ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
            List<EndUser> endUserList = new List<EndUser>();


            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(endUsersQuery, conn))
                {
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        EndUser endUser = new EndUser();
                        endUser.Id = reader.GetInt32(0);
                        if (!reader.IsDBNull(1))
                        {
                            endUser.FirstName = reader.GetString(1);
                        }
                        if (!reader.IsDBNull(2))
                        {
                            endUser.LastName = reader.GetString(2);
                        }
                        if (!reader.IsDBNull(3))
                        {
                            endUser.Email = reader.GetString(3);
                        }
                        if (!reader.IsDBNull(4))
                        {
                            endUser.Phone = reader.GetString(4);
                        }
                        if (!reader.IsDBNull(5))
                        {
                            endUser.MobilePhone = reader.GetString(5);
                        }
                        if (!reader.IsDBNull(6))
                        {
                            endUser.SitId = (int)reader.GetInt32(6);
                        }

                        endUserList.Add(endUser);
                    }
                }
            }

            return endUserList;
        }

        [WebMethod]
        public bool UpdateEndUser(EndUser userData)
        {
            return _controller.UpdateEndUser(userData);
        }

        [WebMethod]
        public int UpdateEndUser2(EndUser userData)
        {
            return _controller.UpdateEndUser2(userData);
        }

        [WebMethod]
        public bool UpdateTerminal(Terminal terminalData)
        {
            return _controller.UpdateTerminal(terminalData);
        }

        [WebMethod]
        public bool UpdateOrganization(Organization organizatonData)
        {
            return _controller.UpdateOrganization(organizatonData);
        }

        [WebMethod]
        public bool UpdateDistributor(Distributor distributorData)
        {
            return _controller.UpdateDistributor(distributorData);
        }

        [WebMethod]
        public bool UpdateIsp(Isp ispData)
        {
            return _controller.UpdateIsp(ispData);
        }

        [WebMethod]
        public Isp GetISP(int ispId)
        {
            return _controller.GetISP(ispId);
        }

        [WebMethod]
        public List<Isp> GetEdgeISPs()
        {
            return _controller.GetEdgeISPs();
        }

        [WebMethod]
        public bool IsTermOwnedByDistributor(int distributorId, string macAddress)
        {
            return _controller.IsTermOwnedByDistributor(distributorId, macAddress);
        }

        [WebMethod]
        public bool IsTermOwnedByOrganization(int organizationId, string macAddress)
        {
            return _controller.IsTermOwnedByOrganization(organizationId, macAddress);
        }

        /// <summary>
        /// Get all ISP's
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public List<Isp> GetIsps()
        {
            return _controller.GetIsps();
        }

        /// <summary>
        /// Get All distributors
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public List<Distributor> GetDistributors()
        {
            return _controller.GetDistributors();
        }

        /// <summary>
        /// Get all organizations
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public List<Organization> GetOrganizations()
        {
            return _controller.GetOrganizations();
        }

        /// <summary>
        /// get all terminals. 
        /// note: should we not include paging here?
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public List<Terminal> GetTerminals()
        {
            return _controller.GetTerminals();
        }

        /// <summary>
        /// Get a list of all terminal statuses
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public List<TerminalStatus> GetTerminalStatus()
        {
            return _controller.GetTerminalStatus();
        }

        /// <summary>
        /// updateTerminalStatus (insert if not exist, update otherwise)
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        [WebMethod]
        public bool UpdateTerminalStatus(TerminalStatus status)
        {
            return _controller.UpdateTerminalStatus(status);
        }

        /// <summary>
        /// Get all available service packs/levels
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public List<ServiceLevel> GetServicePacks()
        {
            return _controller.GetServicePacks();
        }

        /// <summary>
        ///   Get all available service packs/levels for a specific VNO
        /// </summary>
        /// <param name="VNOId">The ID of the VNO</param>
        /// <returns>A list of service packs</returns>
        [WebMethod]
        public List<ServiceLevel> GetServicePacksForVNO(int VNOId)
        {
            return _controller.GetServicePacksForVNO(VNOId);
        }

        /// <summary>
        /// Returns a ServiceLevel object corresponding to the
        /// give servicePackId
        /// </summary>
        /// <param name="servicePackId"></param>
        /// <returns></returns>
        [WebMethod]
        public ServiceLevel GetServicePack(int servicePackId)
        {
            return _controller.GetServicePack(servicePackId);
        }

        /// <summary>
        /// Returns the ServiceLevel object corresponding with the given 
        /// servicePackId
        /// </summary>
        /// <param name="servicePackId">The unique ServicePack identifier</param>
        /// <returns>The corresponding ServicePack or null if not found</returns>
        [WebMethod]
        public ServiceLevel GetServicePackByName(string servicePackName)
        {
            return _controller.GetServicePackByName(servicePackName);
        }

        /// <summary>
        /// Get all available service packs/levels not assigned to a VNO
        /// </summary>
        /// <returns>A list of service packs</returns>
        [WebMethod]
        public List<ServiceLevel> GetServicePacksForAllVNOs()
        {
            return _controller.GetServicePacksForAllVNOs();
        }

        /// <summary>
        /// Get all available service packs/levels not assigned to a VNO by ISP ID
        /// </summary>
        /// <param name="ispId">The ID of the ISP</param>
        /// <returns>A list of service packs</returns>
        [WebMethod]
        public List<ServiceLevel> GetServicePacksForAllVNOsByISP(int ispId)
        {
            return _controller.GetServicePacksForAllVNOsByISP(ispId);
        }

        /// <summary>
        /// Update or Insert a service pack/level
        /// </summary>
        /// <param name="level"></param>
        /// <returns></returns>
        [WebMethod]
        public bool UpdateServicePack(ServiceLevel level)
        {
            return _controller.UpdateServicePack(level);
        }

        /// <summary>
        /// Creates a new freezone in the CMT database
        /// </summary>
        /// <param name="freeZone">The freezone to be created</param>
        /// <returns>True if the operation succeeded</returns>
        [WebMethod]
        public bool CreateFreeZone(FreeZoneCMT freeZone)
        {
            return _controller.CreateFreeZone(freeZone);
        }

        /// <summary>
        /// Updates the data of a freezone in the CMT database
        /// </summary>
        /// <param name="freeZone">The freezone to be updated</param>
        /// <returns>True if the operation succeeded</returns>
        [WebMethod]
        public bool UpdateFreeZone(FreeZoneCMT freeZone)
        {
            return _controller.UpdateFreeZone(freeZone);
        }

        /// <summary>
        /// Fetches all freezones in the CMT database
        /// </summary>
        /// <returns>The list of freezones</returns>
        [WebMethod]
        public List<FreeZoneCMT> GetFreeZones()
        {
            return _controller.GetFreeZones();
        }

        /// <summary>
        /// Returns the pricing of an activity type as a float
        /// </summary>
        /// <remarks>
        /// If the activity has not price setting the value resturned is 0.0
        /// </remarks>
        /// <param name="activityType">The activity type identifier</param>
        /// <returns>The activity type price as a float</returns>
        [WebMethod]
        public float GetActivityTypePrice(int activityType)
        {
            return _controller.GetActivityTypePrice(activityType);
        }

        [WebMethod]
        public bool DeleteTerminal (string macAddress)
        {
            return _controller.DeleteTerminal(macAddress);
        }

        [WebMethod]
        public bool DeleteDistributor (int distributorId)
        {
            return _controller.DeleteDistributor(distributorId);
        }

        [WebMethod]
        public bool DeleteEndUser (int endUserId)
        {
            return _controller.DeleteEndUser(endUserId);
        }

        [WebMethod]
        public bool DeleteOrganizations(int organizationId)
        {
            return _controller.DeleteOrganizations(organizationId);
        }

        /// <summary>
        /// Delete a service pack
        /// </summary>
        /// <param name="slaId">The identifier of the service pack that should be deleted</param>
        /// <returns>True if the deletion was succesfull</returns>
        [WebMethod]
        public bool DeleteServicePack(int slaId) 
        {
            return _controller.DeleteServicePack(slaId);
        }

        /// <summary>
        /// Adds an ISP to a distributor
        /// </summary>
        /// <remarks>
        /// This method acts on the ISPDistributors table
        /// </remarks>
        /// <param name="distributorId">The distributor identifier</param>
        /// <param name="ispId">The ISP identifier</param>
        /// <returns>True if the ISP was successfully added to the Distributor</returns>
        [WebMethod]
        public bool AddISPToDistributor(int distributorId, int ispId)
        {
            return _controller.AddISPToDistributor(distributorId, ispId);
        }

        /// <summary>
        /// This method de-allocates an ISP from a distributor
        /// </summary>
        /// <remarks>
        /// The corresponding tupple distributorId - IspId is removed from the ISPDistributors
        /// table.
        /// </remarks>
        /// <param name="distributorId">The distributor identifier</param>
        /// <param name="ispId">The ISP identifier</param>
        /// <returns>True if the de-allocation succeeded</returns>
        [WebMethod]
        public bool RemoveISPFromDistributor(int distributorId, int ispId)
        {
            return _controller.RemoveISPFromDistributor(distributorId, ispId);
        }

        /// <summary>
        /// Returns a list of ISPs allocated to a distributor
        /// </summary>
        /// <remarks>
        /// This method uses the ISPDistributors table
        /// </remarks>
        /// <param name="distributorId">The target distributor identifier</param>
        /// <returns>A list of ISPs, this list can be empty</returns>
        [WebMethod]
        public List<Isp> GetISPsForDistributor(int distributorId)
        {
            return _controller.GetISPsForDistributor(distributorId);
        }

        /// <summary>
        /// Retrieves the volume information with the most recent timestamp
        /// </summary>
        /// <param name="sitId">The SitId of the end user</param>
        /// <param name="ispId">The ISP identifier</param>
        /// <param name="ispId"> </param>
        /// <returns></returns>
        [WebMethod]
        public VolumeInformationTime GetLastRealTimeVolumeInformation(int sitId, int ispId)
        {
            return _controller.GetLastRealTimeVolumeInformation(sitId, ispId);
        }

        /// <summary>
        /// Retrieves the accumulated return and forward volume information with the 
        /// most recent timestamp.
        /// </summary>
        /// <remarks>
        /// These values are retrieved from the view which holds the realtime updated 
        /// volume consumption information. The view used is vw_volume_detail2. This method is
        /// used by teh NMSVolumeDataService to calculate the delta volume consumption values.
        /// </remarks>
        /// <param name="sitId">The Site Identifier</param>
        /// <param name="ispId">The ISP Identifier</param>
        /// <returns>
        /// The VolumeInformationTime object which contains the latest forward and return
        /// accumulated values from the realtime volume consumption table
        /// </returns>
        [WebMethod]
        public VolumeInformationTime GetLastRTAccumulatedVolumeInformation(int sitId, int ispId)
        {
            return _controller.GetLastRTAccumulatedVolumeInformation(sitId, ispId);
        }

        /// <summary>
        /// retrieves the accumulated volume information with the most recent timestamp
        /// </summary>
        /// <param name="sitId">The SitId of the end user</param>
        /// <param name="ispId">The ISP identifier</param>
        /// <returns>The VolumeInformationTime object containing the last accumulated volume information</returns>
        [WebMethod]
        public VolumeInformationTime GetLastAccumulatedVolumeInformation(int sitId, int ispId)
        {
            return _controller.GetLastAccumulatedVolumeInformation(sitId, ispId);
        }

        /// <summary>
        /// Returns the Realtime volume information between a begin and end date/time
        /// </summary>
        /// <param name="sitId">The SitId of the end user (terminal installation)</param>
        /// <param name="start">The start date/time of the query</param>
        /// <param name="end">The end date/time of the query</param>
        /// <param name="ispId">The ISP identifier</param>
        /// <returns>A list of VolumeInformationTime objects</returns>
        [WebMethod]
        public List<VolumeInformationTime> GetRealTimeVolumeInformation(int sitId, DateTime start, DateTime end, int ispId)
        {
            return _controller.GetRealTimeVolumeInformation(sitId, start, end, ispId);
        }

        /// <summary>
        /// Fetches accumulated data from: VolumeHistoryDetailImport
        /// </summary>
        /// <param name="sitId"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="ispId"></param>
        /// <returns>The 30 minutes interval information for the given sitId</returns>
        [WebMethod]
        public List<VolumeInformationTime> GetAccumulatedVolumeInformationFromRT(int sitId, DateTime start, DateTime end, int ispId)
        {
            return _controller.GetAccumulatedVolumeInformationFromRT(sitId, start, end, ispId);
        }

        /// <summary>
        /// Returns the accumulated volume information between a begin and end date/time
        /// </summary>
        /// <param name="sitId">The SitId fo the end user (terminal installation)</param>
        /// <param name="start">The start date/time of the query</param>
        /// <param name="end">The end date/time of the query</param>
        /// <param name="ispId">The ISP identifier</param>
        /// <returns>A list of VolumeInformationTime objects</returns>
        [WebMethod]
        public List<VolumeInformationTime> GetAccumulatedVolumeInformation(int sitId, DateTime start, DateTime end, int ispId)
        {
            return _controller.GetAccumulatedVolumeInformation(sitId, start, end, ispId);
        }
        
        /// <summary>
        /// Returns the service packs for a specific ISP
        /// </summary>
        /// <param name="ispId"></param>
        /// <returns></returns>
        [WebMethod]
        public List<ServiceLevel> GetServicePacksByIsp(int ispId)
        {
            return _controller.GetServicePacksByIsp(ispId);
        }

        /// <summary>
        /// Resturns the servicepacks for a specific isp but includes the 
        /// servicepacks with the Hide flag set to true.
        /// </summary>
        /// <remarks>
        /// Use this method when retrieving the service packs for an ISP in the 
        /// NOCSA CMT
        /// </remarks>
        /// <param name="ispId">The ISP id</param>
        /// <returns>A list of SLAs belonging to the ISP with the hidden serivcepacks included</returns>
        [WebMethod]
        public List<ServiceLevel> GetServicePacksByIspWithHide(int ispId)
        {
            return _controller.GetServicePacksByIspWithHide(ispId);
        }

        /// <summary>
        /// Returns true if the SLA is a business sla
        /// </summary>
        /// <param name="slaId">The SLA id as an integer</param>
        /// <returns>True if the SLA is a business sla</returns>
        [WebMethod]
        public bool IsBusinessSLA(int slaId)
        {
            return _controller.IsBusinessSLA(slaId);
        }

        /// <summary>
        /// Returns true if the SLA is a freezone sla
        /// </summary>
        /// <param name="slaId">The SLA id as an integer</param>
        /// <returns>True if the SLA is a freezone sla</returns>
        [WebMethod]
        public bool IsFreeZoneSLA(int slaId)
        {
            return _controller.IsFreeZoneSLA(slaId);
        }

        /// <summary>
        /// Returns true if the SLA is a voucher based sla
        /// </summary>
        /// <param name="slaId">The SLA id as an integer</param>
        /// <returns>True if the SLA is a voucher based sla</returns>
        [WebMethod]
        public bool IsVoucherSLA(int slaId)
        {
            return _controller.IsVoucherSLA(slaId);
        }


        /// <summary>
        /// Checks if the given SitId is unique
        /// </summary>
        /// <param name="sitId">The SitId to check</param>
        /// <param name="ispId">The unique ISP identifier</param>
        /// <returns>True if the SitId is indeed unique, false otherwise</returns>
        [WebMethod]
        public bool IsSitIdUnique(int sitId, int ispId)
        {
            return _controller.IsSitIdUnique(sitId, ispId);
        }

        /// <summary>
        /// Returns the last received priority volume data
        /// </summary>
        /// <param name="sitid">The SitId of the end user</param>
        /// <param name="ispId">The unique ISP identifier</param>
        /// <returns>A VolumeInformationTime object</returns>
        [WebMethod]
        public VolumeInformationTime GetLastPriorityVolume(int sitid, int ispId)
        {
            return _controller.GetLastPriorityVolume(sitid, ispId);
        }

        /// <summary>
        /// Returns a list of priority volumes for a sitId between begin and end date
        /// </summary>
        /// <param name="sitId">The end user SitId</param>
        /// <param name="start">The start date/time</param>
        /// <param name="end">The end date/time</param>
        /// <param name="ispId">The ISP id</param>
        /// <returns>A list of VolumeInformationTime objects</returns>
        [WebMethod]
        public List<VolumeInformationTime> GetPriorityVolumeInformation(int sitId, DateTime start, DateTime end, int ispId)
        {
            return _controller.GetPriorityVolumeInformation(sitId, start, end, ispId);
        }

        /// <summary>
        /// Returns a list of Terminals according to the given AdvancedQueryParams transport object
        /// </summary>
        /// <param name="queryParams">The AdvancedQueryParams object</param>
        /// <returns>A list of terminals</returns>
        [WebMethod]
        public List<Terminal> GetTerminalsWithAdvancedQuery(AdvancedQueryParams queryParams)
        {
            return _controller.GetTerminalsWithAdvancedQuery(queryParams);
        }

        /// <summary>
        /// This method returns the MacAddress for the given system user
        /// </summary>
        /// <remarks>
        /// This method is especially used by the White Label CMT
        /// </remarks>
        /// <param name="UserId">The Guid of the logged in system user</param>
        /// <returns>The MacAddress if found or null if not found!</returns>
        [WebMethod]
        public String GetMacAddressForSystemUser(Guid UserId)
        {
            return _controller.GetMacAddressForSystemUser(UserId);
        }

        /// <summary>
        /// Updates the MacAddress for the given user. If the user does not exist
        /// the record is created in the 
        /// </summary>
        /// <param name="SystemUserId">The SystemUserId</param>
        /// <param name="MacAddress">The MacAddress</param>
        /// <returns>True if the operation succeeded</returns>
        [WebMethod]
        public bool UpdateMacAddressForSystemUser(Guid SystemUserId, string MacAddress)
        {
            return _controller.UpdateMacAddressForSystemUser(SystemUserId, MacAddress);
        }

        /// <summary>
        /// Retrievs a specific VNO from the CMT database
        /// </summary>
        /// <param name="VNOId">The ID of the VNO to be retrieved</param>
        /// <returns>The VNO object</returns>
        [WebMethod]
        public VNO GetVNO(int VNOId)
        {
            return _controller.GetVNO(VNOId);
        }

        /// <summary>
        /// Creates/Updates a VNO entry in the CMT database
        /// </summary>
        /// <param name="vno">The VNO object to be created/updated</param>
        /// <returns>True if the operation succeeded</returns>
        [WebMethod]
        public bool UpdateVNO(VNO vno)
        {
            return _controller.UpdateVNO(vno);
        }

        /// <summary>
        /// Retrieves all the distributor objects which are marked as VNO from the CMT database
        /// </summary>
        /// <returns>A list of distributor objects</returns>
        [WebMethod]
        public List<Distributor> GetVNOs()
        {
            return _controller.GetVNOs();
        }

        /// <summary>
        ///   Get all Distributors and VNOs
        /// </summary>
        /// <returns>A list of distributor objects</returns>
        [WebMethod]
        public List<Distributor> GetDistributorsAndVNOs()
        {
            return _controller.GetDistributorsAndVNOs();
        }

        // <summary>
        /// Adds a URL in an NMS table through which the terminal can access certain functionalities
        /// The URLs are scrambled for the end-user and this method matches the clear-text URL with
        /// the scrambled one.
        /// Functionalities include traffic data and a speedtest
        /// Issue CMTBO-48
        /// </summary>
        /// <param name="clearURL">The URL in readable text</param>
        /// <param name="scrambledURL">The scrambled version of the URL</param>
        /// <returns>True if successful</returns>
        [WebMethod]
        public bool AddTerminalNMSURL(string clearURL, string scrambledURL)
        {
            return _controller.AddTerminalNMSURL(clearURL, scrambledURL);
        }

        /// <summary>
        ///   Get all active distributors 
        ///   Excludes all active distributors in the VNO list
        /// </summary>
        /// <returns>A list of active distributor objects</returns>
        [WebMethod]
        public List<Distributor> GetActiveDistributors()
        {
            return _controller.GetActiveDistributors();
        }

        /// <summary>
        ///   Get all inactive distributors
        ///   Excludes all inactive distributors in the VNO list
        /// </summary>
        /// <returns>A list of inactive distributor objects</returns>
        [WebMethod]
        public List<Distributor> GetInactiveDistributors()
        {
            return _controller.GetInactiveDistributors();
        }

        /// <summary>
        /// Gets info concerning the BadDistributor flag, and passes it through to the NMS 
        /// </summary>
        /// <remarks>
        /// Both params received through DistributorDetailsForm from the front end
        /// </remarks>
        /// <param name="distId">Distributor ID</param>
        /// <param name="badDistributor">BadDistributor flag</param>
        [WebMethod]
        public void BadDistributorChanged(int distId, bool badDistributor)
        {
            _controller.BadDistributorChanged(distId, badDistributor);
        }


        /// <summary>
        /// Gets a total amount of Volume used by a given Terminal on a given date. 
        /// By total Volume is meant: the sum of the TotalForwardedVolume column and the TotalReturnVolume in the VolumeHistory2 table.
        /// </summary>
        /// <param name="ispId">ispId of the Terminal</param>
        /// <param name="sitId">sitId of the Terminal</param>
        /// <param name="date">a date</param>
        /// <returns>a long integer or Int64</returns>
        [WebMethod]
        public long GetTotalTerminalVolumeForDate(int ispId, int sitId, DateTime date)
        {
            return _controller.GetTotalTerminalVolumeForDate(ispId, sitId, date);
        }
        
        /// <summary>
        /// Gets the country with country code and country name with a given country code.
        /// Returns null when the country could not be found.
        /// </summary>
        /// <param name="code">The country code to get the country for</param>
        /// <returns>The country with its country name and country code, or null when it could not be found</returns>
        [WebMethod]
        public Country GetCountryByCountryCode(string code) 
        {
            return _controller.GetCountryByCountryCode(code);
        }

        /// <summary>
        /// Gets the full country name with a given country code.
        /// Returns null when the country could not be found.
        /// </summary>
        /// <param name="code">The country code to get the country name for</param>
        /// <returns>The country name of a country, or null when it could not be found</returns>
        [WebMethod]
        public string GetCountryNameByCountryCode(string code) 
        {
            return _controller.GetCountryNameByCountryCode(code);
        }

        [WebMethod]
        public bool IsSatDivTerminalActive(string macAddress)
        {
            return _controller.IsSatDivTerminalActive(macAddress);
        }

        /// <summary>
        ///   Get all available service packs/levels by Billable Id
        /// </summary>
        /// <param name="billableId"></param>
        /// <returns>A list of service packs</returns>
        /// CMTINVOICE-73
        [WebMethod]
        public List<ServiceLevel> GetServicePacksByBillableId(int billableId) 
        {
            return _controller.GetServicePacksByBillableId(billableId);
        }

        //Move these methods to a separate controller in due time
        #region Methods related to the management of themes for the White Label CMT
        /// <summary>
        /// Creates or updates a White Label CMT theme. If the theme does not exist in
        /// the CMT the theme is created, otherwise the theme data is updated.
        /// </summary>
        /// <param name="theme">The theme to update or create</param>
        /// <returns>True if the operation succeeded</returns>
        [WebMethod]
        public Boolean UpdateTheme(Theme theme)
        {
            return _controller.UpdateTheme(theme);
        }

        /// <summary>
        /// Returns a theme for the given distributor.
        /// </summary>
        /// <remarks>
        /// The result can be null if no theme is allocated to the given
        /// distributor
        /// </remarks>
        /// <param name="DistId">The distributor identifier</param>
        /// <returns></returns>
        [WebMethod]
        public Theme GetThemeForDistributor(int DistId)
        {
            return _controller.GetThemeForDistributor(DistId);
        }
        #endregion



        //SDPDEV-23
        [WebMethod]
        public List<Satellite> GetSatellites()
        {
            return _controller.GetSatellites();
        }
        //SDPDEV-23
        [WebMethod]
        public List<Isp> GetISPsBySatellite(int satelliteId)
        {
            return _controller.GetISPsBySatellite(satelliteId);
        }

        
    }
}
