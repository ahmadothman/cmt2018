﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Configuration;
using BOControllerLibrary.Controllers.Interfaces;
using BOControllerLibrary.Controllers;
using BOControllerLibrary.Model.Terminal;
using BOControllerLibrary.Model.Reporting;

namespace BOMonitorController
{
    /// <summary>
    /// Webservice for the Mandac controller which will be used by third parties to connect to the CMT
    /// </summary>
    [WebService(Namespace = "http://cmt.nimera.net/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [System.Web.Script.Services.ScriptService]
    public class BOMandacControlWS : System.Web.Services.WebService, IBOMandacController
    {
        IBOMandacController _controller;

        public BOMandacControlWS()
        {
            _controller = new BOMandacController(
                connectionString: ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString,
                databaseProvider: ConfigurationManager.ConnectionStrings["ApplicationServices"].ProviderName);
        }

        /// <summary>
        /// Returns an estimation of the remaining volume for Mandac hotspot connected to a terminal.
        /// This estimation is an indication to the terminal owner if more
        /// hotspot vouchers can be issued or not.
        /// This method will be interpreted by the Mandac system
        /// </summary>
        /// <param name="mandacId">The ID of the Mandac hotspot</param>
        /// <returns>The volume estimation in GB</returns>
        [WebMethod]
        public double GetEstimatedRemainingVolumeForHotspot(string mandacId)
        {
            return _controller.GetEstimatedRemainingVolumeForHotspot(mandacId);
        }

        /// <summary>
        /// Returns the current remaining volume of a terminal
        /// </summary>
        /// <param name="mandacId">The ID of the Mandac hotspot</param>
        /// <returns>The volume in GB</returns>
        [WebMethod]
        public double GetRemainingVolumeForTerminal(string mandacId)
        {
            return _controller.GetRemainingVolumeForTerminal(mandacId);
        }

        /// <summary>
        /// Returns a list of all Mandac transactions in the CMT database
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public List<MandacTransaction> GetAllTransactions()
        {
            return _controller.GetAllTransactions();
        }

        /// <summary>
        /// This method returns the remaining validity period for the volume of a terminal
        /// </summary>
        /// <param name="mandacId">The ID of the Mandac hotspot connected to the terminal</param>
        /// <returns>The number of day the volume is still valid</returns>
        [WebMethod]
        public int GetValidityForTerminalVolume(string mandacId)
        {
            return _controller.GetValidityForTerminalVolume(mandacId);
        }
    }
}
