﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Web.Script.Services;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using BOControllerLibrary.Controllers.Interfaces;
using BOControllerLibrary.Controllers;

namespace BOMonitorController
{
    /// <summary>
    /// Summary description for BOIssuesControllerWS
    /// </summary>
    [WebService(Namespace = "http://cmt.nimera.net/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    [ScriptService]
    public class BOIssuesControllerWS : WebService, IBOIssuesController
    {
        private IBOIssuesController _controller;

        public BOIssuesControllerWS()
        {
            _controller = new BOIssuesController(
                connectionString: ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString,
                databaseProvider: ConfigurationManager.ConnectionStrings["ApplicationServices"].ProviderName);
        }

        [WebMethod]
        public List<BOControllerLibrary.Model.Issues.Issue> GetIssuesByDistributor(int distributorId)
        {
            return _controller.GetIssuesByDistributor(distributorId);
        }

        [WebMethod]
        public string SubmitNewIssue(BOControllerLibrary.Model.Issues.Issue issue)
        {
            return _controller.SubmitNewIssue(issue);
        }

        [WebMethod]
        public BOControllerLibrary.Model.Issues.JiraIssue GetJiraIssueDetails(string jiraIssueKey)
        {
            return _controller.GetJiraIssueDetails(jiraIssueKey);
        }

        [WebMethod]
        public bool SubmitComment(string commentBody, string jiraIssueKey)
        {
            return _controller.SubmitComment(commentBody, jiraIssueKey);
        }

    }
}
