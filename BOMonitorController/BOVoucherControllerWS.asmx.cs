﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Web.Services;
using System.Data;
using BOControllerLibrary.Controllers;
using BOControllerLibrary.Controllers.Interfaces;
using BOControllerLibrary.Model;
using BOControllerLibrary.Model.Voucher;

namespace BOMonitorController
{
    /// <summary>
    /// Summary description for BOVoucherControllerWS
    /// </summary>
    [WebService(Namespace = "http://cmt.nimera.net/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class BOVoucherControllerWS : System.Web.Services.WebService, IBOVoucherController
    {

        private IBOVoucherController _controller;

        /// <summary>
        /// Constructor
        /// </summary>
        public BOVoucherControllerWS()
        {
            _controller = new BOVoucherController(
                connectionString: ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString,
                databaseProvider: ConfigurationManager.ConnectionStrings["ApplicationServices"].ProviderName);
        }

        #region IBOVoucherController Members

        /// <summary>
        /// Returns a list of available (valid) vouchers for a given
        /// distributor.
        /// </summary>
        /// <param name="distributorId">The distributor identifier.</param>
        /// <returns>A list of available vouchers. This list can be empty.</returns>
        [WebMethod]
        public List<BOControllerLibrary.Model.Voucher.Voucher> GetAvailableVouchers(int distributorId)
        {
            return _controller.GetAvailableVouchers(distributorId);
        }

        /// <summary>
        /// Returns a list of VoucherBatchInfo objects
        /// </summary>
        /// <param name="distributorId"></param>
        /// <returns></returns>
        [WebMethod]
        public List<BOControllerLibrary.Model.Voucher.VoucherBatchInfo> GetAvailableVoucherBatches(int distributorId)
        {
            return _controller.GetAvailableVoucherBatches(distributorId);
        }

        /// <summary>
        /// Returns a list of VoucherBatchInfo objects
        /// </summary>
        /// <param name="distributorId"></param>
        /// <returns></returns>
        [WebMethod]
        public List<BOControllerLibrary.Model.Voucher.VoucherBatchInfo> GetVoucherBatches(int distributorId)
        {
            return _controller.GetVoucherBatches(distributorId);
        }

        /// <summary>
        /// Returns a list of VoucherBatchInfo objects for a given distributor. The list contains
        /// only released voucher batches
        /// </summary>
        /// <param name="distributorId">The distributor identifier</param>
        /// <returns>A list of VoucherBatchInfo objects</returns>
        [WebMethod]
        public List<VoucherBatchInfo> GetReleasedVoucherBatches(int distributorId)
        {
            return _controller.GetReleasedVoucherBatches(distributorId);
        }


        /// <summary>
        /// Returns a list of vouchers for the specified Batch Id
        /// </summary>
        /// <param name="batchId">The identifier of the batch</param>
        /// <returns>A list of vouchers identifief by the given batch id.</returns>
        [WebMethod]
        public List<BOControllerLibrary.Model.Voucher.Voucher> GetVouchers(int batchId)
        {
            return _controller.GetVouchers(batchId);
        }

        /// <summary>
        /// Returns a Voucher object identified by its code.
        /// </summary>
        /// <param name="code">The 13 digit code string</param>
        /// <returns>The voucher instance corresponding with the given code or
        /// null if the voucher could not be found in the database.</returns>
        [WebMethod]
        public BOControllerLibrary.Model.Voucher.Voucher GetVoucher(string code)
        {
            return _controller.GetVoucher(code);
        }

        /// <summary>
        /// Returns the meta data of the given batch
        /// </summary>
        /// <param name="batchId">A unique batch id</param>
        /// <returns>A VoucherBatch instance or null if the batch could not be found in the database</returns>
        [WebMethod]
        public BOControllerLibrary.Model.Voucher.VoucherBatch GetVoucherBatchInfo(int batchId)
        {
            return _controller.GetVoucherBatchInfo(batchId);
        }

        /// <summary>
        /// Invalidates the voucher identified by the given code.
        /// </summary>
        /// <param name="code">The code of the voucher to invalidate</param>
        /// <param name="crc">The crc string contains an Hexadecimal number</param>
        /// <param name="macAddress">The MAC address of the terminal for which the voucher was validated</param>
        /// <returns>True if the voucher was successfully invalidated</returns>
        [WebMethod]
        public bool ValidateVoucher(string code, string crc, string macAddress)
        {
            return _controller.ValidateVoucher(code, crc, macAddress);
        }

        /// <summary>
        /// Releases a voucher batch
        /// </summary>
        /// <param name="batchId">The voucher batch identifier</param>
        /// <returns>True if the mehtod succeeded</returns>
        [WebMethod]
        public bool ReleaseVoucherBatch(int batchId)
        {
            return _controller.ReleaseVoucherBatch(batchId);
        }

         /// <summary>
        /// This method puts the Release flag of the VoucherBatch table
        /// for the given voucher batch id back to false
        /// </summary>
        /// <param name="batchId">The voucher batch to unrelease</param>
        /// <returns>True if the method succeeded</returns>
        [WebMethod]
        public bool UnreleaseVoucherBatch(int batchId)
        {
            return _controller.UnreleaseVoucherBatch(batchId);
        }

        /// <summary>
        /// This method puts the Release flag of the VoucherBatch table
        /// for the given voucher batch id back to false
        /// </summary>
        /// <param name="batchId">The voucher batch to unrelease</param>
        /// <returns>True if the method succeeded</returns>
        [WebMethod]
        public int DeleteVoucherBatch(int batchId)
        {
            return _controller.DeleteVoucherBatch(batchId);
        }
        /// <summary>
        /// Returns a list of Voucher volume objects
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public List<BOControllerLibrary.Model.Voucher.VoucherVolume> GetVoucherVolumes()
        {
            return _controller.GetVoucherVolumes();
        }

        /// <summary>
        /// Invalidate the complete batch with the given batch number.
        /// </summary>
        /// <param name="batchNumber">The batch number to invalidate</param>
        /// <returns>True if the batch was successfully invalidated</returns>
        [WebMethod]
        public bool ValidateBatch(int batchId)
        {
            return _controller.ValidateBatch(batchId);
        }

        /// <summary>
        /// Creates a list of unique vouchers. The number of vouchers is given
        /// by the numVouchers parameter. The list is also stored in the CMT database (VoucherBase and
        /// Voucher tables)
        /// </summary>
        /// <param name="distributorId">The distributor who owns the vouchers</param>
        /// <param name="numVouchers">The number of vouchers to generate</param>
        /// <param name="UserId">Id of the system user who created the voucher batch</param>
        /// <param name="batchId">After conclusion of the method, batchId contains the new batch identifier</param>
        /// <returns>A list of Vouchers with length is numVouchers</returns>
        [WebMethod]
        public List<BOControllerLibrary.Model.Voucher.Voucher> CreateVouchers(int distributorId, int numVouchers, Guid userId, out int batchId, int volume)
        {
            return _controller.CreateVouchers(distributorId, numVouchers, userId, out batchId, volume);
        }

        /// <summary>
        /// Makes a voucher which was (accidently) invalidated, valid again.
        /// </summary>
        /// <remarks>
        /// This method needs a correct CRC code for the validation process to
        /// succeed.
        /// </remarks>
        /// <param name="code">The unique voucher code</param>
        /// <returns>True if the operation succeeded</returns>
        [WebMethod]
        public bool ResetVoucher(string code, string crc)
        {
            return _controller.ResetVoucher(code, crc);
        }

        /// <summary>
        /// Returns a data set with the number of voucher created for each distributor between
        /// a begin and start date.
        /// </summary>
        /// <remarks>
        /// This method should use the cmtVoucherCreation stored procedure.
        /// </remarks>
        /// <param name="startDate">Start date of the report (this date included)</param>
        /// <param name="endDate">End date of the report (this date excluded)</param>
        /// <returns>A data set with the selected voucher batches</returns>
        [WebMethod]
        public DataSet GenerateVoucherReport(DateTime startDate, DateTime endDate)
        {
            return _controller.GenerateVoucherReport(startDate, endDate);
        }

        /// <summary>
        /// Returns the voucher batch for a given code and crc
        /// </summary>
        /// <remarks>
        /// This method needs to be corrected since the VoucherBatch volume property does not
        /// contain the actual volume in MB but the volume code identifier. Maybe a join could
        /// also make this method a bit more streamlined!
        /// </remarks>
        /// <param name="code">The voucher code</param>
        /// <param name="crc">The voucher crc</param>
        /// <returns>The corresponding VoucherBatch or null if the method failed</returns>
        [WebMethod]
        public VoucherBatch GetVoucherBatch(string code, string crc)
        {
            return _controller.GetVoucherBatch(code, crc);
        }

        /// <summary>
        /// This method returns the the voucher volume code which needs to be used
        ///  in the ISPIF and NMS add volume methods.
        /// </summary>
        /// <param name="voucherVolumeId">The voucher volume Id, this value corresponds
        /// with the Id column in the VoucherVolumes table</param>
        /// <returns>The corresponding VolumeCode or null if the VolumeCode could not be found</returns>
        [WebMethod]
        public int? GetVolumeCodeById(int voucherVolumeId)
        {
            return _controller.GetVolumeCodeById(voucherVolumeId);
        }

        /// <summary>
        /// Returns a voucher volume object reference by its volume id
        /// </summary>
        /// <param name="volumeId">The VoucherVolumeId</param>
        /// <returns>The voucher volume object or null if not found</returns>
        [WebMethod]
        public VoucherVolume GetVoucherVolumeById(int volumeId)
        {
            return _controller.GetVoucherVolumeById(volumeId);
        }

        /// <summary>
        /// This method adds a new voucher volume to the CMT database
        /// </summary>
        /// <param name="vv">The voucher volume object</param>
        /// <returns>True if successful</returns>
        [WebMethod]
        public bool AddVoucherVolume(VoucherVolume vv)
        {
            return _controller.AddVoucherVolume(vv);
        }

        /// <summary>
        /// This method updates an existing voucher volume in the CMT database
        /// </summary>
        /// <param name="vv">The voucher volume object</param>
        /// <returns>True if successful</returns>
        [WebMethod]
        public bool UpdateVoucherVolume(VoucherVolume vv)
        {
            return _controller.UpdateVoucherVolume(vv);
        }

        #endregion

        /// <summary>
        /// Creates a new voucher requests
        /// </summary>
        /// <param name="vr">The voucher request to be created</param>
        /// <returns>True if successfull</returns>
        [WebMethod]
        public bool CreacteVoucherRequest(VoucherRequest vr)
        {
            return _controller.CreacteVoucherRequest(vr);
        }

        /// <summary>
        /// Updates the state of a voucher requests
        /// </summary>
        /// <param name="vr">The voucher request to be updated</param>
        /// <returns>True if successfull</returns>
        [WebMethod]
        public bool UpdateVoucherRequest(VoucherRequest vr)
        {
            return _controller.UpdateVoucherRequest(vr);
        }

        /// <summary>
        /// Fetches a voucher request from the CMT database
        /// </summary>
        /// <param name="vrId">The ID of the voucher request</param>
        /// <returns>The voucher request object</returns>
        [WebMethod]
        public VoucherRequest GetVoucherRequest(int vrId)
        {
            return _controller.GetVoucherRequest(vrId);
        }

        /// <summary>
        /// Fetches all pending voucher requests
        /// </summary>
        /// <returns>A list of voucher requests</returns>
        [WebMethod]
        public List<VoucherRequest> GetAllPendingVoucherRequests()
        {
            return _controller.GetAllPendingVoucherRequests();
        }

        /// <summary>
        /// Fetches all vouchers for a voucher report based on certain criteria
        /// All criteria are optional
        /// </summary>
        /// <param name="startDate">The begin date of the reporting period. The date is the date the voucher was validated</param>
        /// <param name="endDate">The end date of the reporting period. The date is the date the voucher was validated</param>
        /// <param name="batchId">The ID of the batch the vouchers belong to</param>
        /// <param name="released">True if only released vouchers should be included</param>
        /// <returns>A list of vouchers</returns>
        [WebMethod]
        public List<Voucher> GetVouchersForReport(DateTime startDate, DateTime endDate, int batchId, bool released)
        {
            return _controller.GetVouchersForReport(startDate, endDate, batchId, released);
        }


        /// <summary>
        /// Allows a distributor to enter his retail price for vouchers
        /// </summary>
        /// <param name="distributorId">The ID of the distributor setting the price</param>
        /// <param name="voucherVolumeId">The ID of the voucher volume for which the price is set</param>
        /// <param name="unitPriceUSD">The unit price in Euros</param>
        /// <param name="unitPriceEUR">The unit price in US Dollars</param>
        /// <returns></returns>
        [WebMethod]
        public bool SetVoucherRetailPrice(int distributorId, int voucherVolumeId, double unitPriceUSD, double unitPriceEUR)
        {
            return _controller.SetVoucherRetailPrice(distributorId, voucherVolumeId, unitPriceUSD, unitPriceEUR);
        }

        /// <summary>
        /// Fetches the voucher retail prices as set by a specific distributor
        /// </summary>
        /// <param name="distributorId">The ID of the distributor</param>
        /// <returns>A list of voucher volume objects containing the ID of the voucher volume and the retail prices of the distributor</returns>
        [WebMethod]
        public List<VoucherVolume> GetVoucherRetailPricesForDistributor(int distributorId)
        {
            return _controller.GetVoucherRetailPricesForDistributor(distributorId);
        }

        /// <summary>
        /// Flags a voucher batch as paid in the CMT database
        /// </summary>
        /// <param name="batchId">The ID of the batch to be flagged</param>
        /// <returns>True if successful</returns>
        [WebMethod]
        public bool FlagVoucherBatchAsPaid(int batchId)
        {
            return _controller.FlagVoucherBatchAsPaid(batchId);
        }

        /// <summary>
        /// Flags a voucher batch as paid in the CMT database
        /// </summary>
        /// <param name="batchId">The ID of the batch to be flagged</param>
        /// <param name="paymentMethod">Payment method</param>
        /// <returns>True if successful</returns>
        [WebMethod] 
        public bool MarkVoucherBatchAsPaid(int batchId, string paymentMethod)
        {
            return _controller.MarkVoucherBatchAsPaid(batchId, paymentMethod);
        }

        /// <summary>
        /// Flags a voucher batch as unpaid in the CMT database
        /// </summary>
        /// <param name="batchId">The ID of the batch to be flagged</param>
        /// <returns>True if successful</returns>
        [WebMethod]
        public bool FlagVoucherBatchAsUnPaid(int batchId)
        {
            return _controller.FlagVoucherBatchAsUnPaid(batchId);
        }

        /// <summary>
        /// Returns batchId for activated voucher on a specific terminal at determined time
        /// </summary>
        /// <param name="macAddress">The mac address of terminal where voucher was validated</param>
        /// <param name="vdate">validation date when voucher was validated</param>
        /// <returns>batchid</returns>
        [WebMethod] 
        public int getBatchIDforValidatedVoucher(string macAddress, DateTime vdate)
        {
            return _controller.getBatchIDforValidatedVoucher(macAddress,vdate);
        }

        /// <summary>
        /// Returns batches between specific dates and distributor could be predefined
        /// </summary>
        /// <returns>list of voucher batches</returns>
        [WebMethod]
        public List<VoucherBatchInfo> getVouchersBatchesReport(int distID,int paid, DateTime? batchesfrom, DateTime? batchesto)
        {
            return _controller.getVouchersBatchesReport(distID,paid, batchesfrom, batchesto);
        }

        /// <summary>
        /// returns number of available vouchers
        /// </summary>
        /// <returns>number of available vouchers</returns>
        [WebMethod]
        public int getAvailableVouchersforbatch(int batchID)
        {
            return _controller.getAvailableVouchersforbatch(batchID);
        }

        /// <summary>
        /// returns customized vouchervolumes for distributor
        /// </summary>
        /// <returns>List of distributor voucher volumes</returns>
        [WebMethod]
        public List<DistVoucher> GetDistVouchersforVV(int volumeId)
        {
            return _controller.GetDistVouchersforVV(volumeId);
        }


        /// <summary>
        /// returns vouchervolumes for specific distributor
        /// </summary>
        /// <returns>list of distributor voucher volumes</returns>
        [WebMethod]
        public List<DistVoucher> GetVoucherVolumesforDist(int distId)
        {
            return _controller.GetVoucherVolumesforDist(distId);
        }

        /// <summary>
        /// check if distributor voucher volume already exist
        /// </summary>
        /// <returns>Tue if exists</returns>
        [WebMethod]
        public Boolean DistVoucherExists(int distId, int vvid)
        {
            return _controller.DistVoucherExists(distId,vvid);
        }

        /// <summary>
        /// returns a specific voucher volume for a selected distributor in case the price is customized otherwise a generic price
        /// </summary>
        /// <returns>a voucher volume</returns>
        [WebMethod]
        public VoucherVolume GetVoucherVolumeByIdForDistributor(int volumeId, int DistId)
        {
            return _controller.GetVoucherVolumeByIdForDistributor(volumeId,DistId);
        }


        /// <summary>
        /// add customized price for a voucher volume
        /// </summary>
        /// <returns>True if success</returns>
        [WebMethod]
        public bool AddDistVoucher(DistVoucher dv)
        {
            return _controller.AddDistVoucher(dv);
        }


        /// <summary>
        /// update already exist customized price for a distributor 
        /// </summary>
        /// <returns>True if success</returns>
        [WebMethod]
        public bool UpdateDistVoucher(DistVoucher dv)
        {
            return _controller.UpdateDistVoucher(dv);
        }

        /// <summary>
        /// return a record from DistVouchers table identified by id 
        /// </summary>
        /// <returns>Instance of DistVoucher if exist else null</returns>
        [WebMethod]
        public DistVoucher GetDistVoucher(int distVoucherId)
        {
            return _controller.GetDistVoucher(distVoucherId);
        }
    }
}
