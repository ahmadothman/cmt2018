﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using CassandraLib;
using CassandraLib.Model;
using CassandraLib.Model.DataModel;

namespace BOMonitorController
{
    /// <summary>
    /// Summary description for BOCassandraLibWS
    /// </summary>
    [WebService(Namespace = "http://cmt.nimera.net/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [System.Web.Script.Services.ScriptService]
    public class BOCassandraLibWS : System.Web.Services.WebService
    {

        CassandraClient _cassandraClient;

        public BOCassandraLibWS()
        {
            _cassandraClient = new CassandraClient("SatADSL");
        }

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [WebMethod]
        public void InsertVolumeData(List<VolumeDataEntry> entries, string filename)
        {
            _cassandraClient.InsertVolumeData(entries, filename);
        }

        [WebMethod]
        public void InsertNMSNetFlowFiles(List<NMSNetFlowFile> files, string filename) 
        {
            _cassandraClient.InsertNMSNetFlowFiles(files, filename);
        }
    }
}
