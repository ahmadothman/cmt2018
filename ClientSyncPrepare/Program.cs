﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using Microsoft.Synchronization.Data;
using Microsoft.Synchronization.Data.SqlServer;

namespace ClientSyncPrepare
{
    class Program
    {
        const string _SCOPE = "CMTDataScope";
        //const string _ClientConnectionString = "Data Source=.\\SQLEXPRESS; Initial Catalog=CMTData-Local; Integrated Security=SSPI;";
        const string _ClientConnectionString = "server=192.168.1.141;User Id=root;Persist Security Info=True;database=CMTData-Local";
        const string _ServerConnectionString = "Data source=cmtdata.nimera.net;Database=CMTData; User Id=SatFinAfrica; Password=Lagos#1302";

        static void Main(string[] args)
        {
            Program prg = new Program();
            prg.mainBody();
        }

        private void mainBody()
        {
            try
            {
                MySqlConnection clientConn = new MySqlConnection(_ClientConnectionString);
                SqlConnection serverConn = new SqlConnection(_ServerConnectionString);
                DbSyncScopeDescription scopeDesc =
                                    SqlSyncDescriptionBuilder.GetDescriptionForScope(_SCOPE, serverConn);

                // create server provisioning object based on the ProductsScope
                SqlSyncScopeProvisioning clientProvision = new SqlSyncScopeProvisioning(clientConn, scopeDesc);
                File.WriteAllText("C:\\Temp\\SampleDeprovisionScript.txt", clientProvision.Script());
                // starts the provisioning process
                clientProvision.Apply();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
