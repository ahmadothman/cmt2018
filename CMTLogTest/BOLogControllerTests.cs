﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BOControllerLibrary.Model;
using BOControllerLibrary.Model.Log;
using BOControllerLibrary.Controllers;

namespace CMTLogTest
{
    /// <summary>
    /// This class tests some of the BOLogController methods.
    /// </summary>
    [TestClass]
    public class BOLogControllerTests
    {
        BOLogController _boLogController = new BOLogController("data source=176.34.243.152;Database=CMTData-Production; User Id=SatFinAfrica; Password=Lagos#1302", "SqlClient");

        [TestMethod]
        public void LogApplicationExceptionTest()
        {
            _boLogController.LogApplicationException(new CmtApplicationException
            {
                UserDescription = "UnitTestedMessage",
                ExceptionDesc = "Exception Message",
                ExceptionStacktrace = "Exception StackTrace",
                ExceptionDateTime = DateTime.Now,
                StateInformation = "state information here",
                ExceptionLevel = (short)ExceptionLevelEnum.Debug
            });
        }
    }
}
