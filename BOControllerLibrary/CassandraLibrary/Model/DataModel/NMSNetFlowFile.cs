﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CassandraLib.Model.DataModel
{
    public class NMSNetFlowFile
    {
        public string filename { get; set; }
        public byte[] file { get; set; }
        public DateTime time_stamp { get; set; }
    }
}
