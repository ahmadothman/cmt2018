﻿using System;

namespace CassandraLib.Model.DataModel
{
    public class VolumeDataEntry : VolumeDataEntryBase
    {
        public DateTime time_stamp { get; set; }
    }

    public class VolumeDataSummary : VolumeDataEntryBase
    {
        public string date { get; set; }
    }

    public class VolumeDataEntryBase
    {
        public int ispid { get; set; }
        public int sitid { get; set; }

        public long forwardvolume { get; set; }
        public long returnvolume { get; set; }
        public long accumulatedforwardvolume { get; set; }
        public long accumulatedreturnvolume { get; set; }
        public long freezonertn { get; set; }
        public long freezonefwd { get; set; }
    }
}