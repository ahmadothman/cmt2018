﻿using System;

namespace CassandraLib.Model.DataModel
{
    public class ImportLog
    {
        public string filename { get; set; }
        public DateTime time_stamp { get; set; }
        public int numberofrecords { get; set; }
    }
}