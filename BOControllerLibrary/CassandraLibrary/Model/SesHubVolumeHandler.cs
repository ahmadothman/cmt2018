﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using CassandraLib.Model.DataModel;
using CassandraLib.Model.Interfaces;

namespace CassandraLib.Model
{
    public class SesHubVolumeHandler : IVolumeDataHandler

    {
        public List<VolumeDataEntry> ParseData(string filename)
        {
            char delimiter = ',';
            using (var reader = new StreamReader(filename, Encoding.UTF8))
            {
                // file description header: nbr_records,ICD_version,file_tag,time_stamp,time_interval
                var fileHeader = reader.ReadLine().Split(delimiter);

                // eg 12670,0001,ACC,201201010000,1440
                var fileHeaderData = reader.ReadLine().Split(delimiter);

                // data header (the real csv column headers)
                var dataHeader = reader.ReadLine().Split(delimiter);

                // code to import the ispid, but we dont need it as we'll just pass the ispid to the procedure ;).
                //var newDataHeader = new string[dataHeader.Length + 1];
                //Array.Copy(dataHeader, 0, newDataHeader, 1, dataHeader.Length);
                //newDataHeader[0] = "IspId";

                // data header suffix, containing the data types
                var dataHeaderTypes = reader.ReadLine().Split(delimiter);

                var entries = new List<VolumeDataEntry>();

                while (!reader.EndOfStream)
                {
                    var arr = reader.ReadLine().Split(delimiter);
                    entries.Add(new VolumeDataEntry()
                    {
                        ispid = 412,
                        sitid = Int32.Parse(arr[0]),
                        time_stamp = DateTime.ParseExact(arr[1], "yyyy-MM-dd HH:mm:ss", null),
                        forwardvolume = long.Parse(arr[2]),
                        returnvolume = long.Parse(arr[3]),
                        accumulatedforwardvolume = long.Parse(arr[6]),
                        accumulatedreturnvolume = long.Parse(arr[7])
                    });
                }
                return entries;
            }
        }
    }
}