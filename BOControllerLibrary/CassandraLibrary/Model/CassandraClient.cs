﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using CassandraLib.Model;
using CassandraLib.Model.Cql;
using CassandraLib.Model.DataModel;
using CassandraLib.Model.Helpers;
using CassandraLib.Model.Interfaces;
using CassandraSharp;
using CassandraSharp.Config;
using CassandraSharp.CQLOrdinal;
using CassandraSharp.CQLPoco;

namespace CassandraLib.Model
{
    public class CassandraClient
    {
        public string ClusterName { get; set; }

        public CassandraClient(string clusterName)
        {
            XmlConfigurator.Configure();
            ClusterName = clusterName;
        }

        #region Query

        private List<T> QueryData<T>(string query)
        {
            if (String.IsNullOrEmpty(ClusterName))
                throw new Exception("You did not set the clustername. You should probably set this to SatADSL");

            try
            {
                using (ICluster cluster = ClusterManager.GetCluster(ClusterName))
                {
                    ICqlCommand cmd = cluster.CreatePocoCommand();
                    var result = cmd.Execute<T>(query).AsFuture().Result;
                    return result.ToList();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
            finally
            {
                ClusterManager.Shutdown();
            }
        }

        public List<VolumeDataEntry> QueryDataDetail(int ispId, int sitid, DateTime from, DateTime to)
        {
            return QueryData<VolumeDataEntry>(string.Format(Queries.SelectVolumeDataRecords, ispId, sitid, String.Join(",", GenerateDaysList(from, to))));
        }

        public List<VolumeDataSummary> QueryDataDaily(int ispId, int sitid, DateTime from, DateTime to)
        {
            return QueryData<VolumeDataSummary>(string.Format(Queries.SelectVolumeDataDayRecords, ispId, sitid, String.Join(",", GenerateDaysList(from, to))));
        }

        public List<VolumeDataSummary> QueryDataMonthly(int ispId, int sitid, DateTime from, DateTime to)
        {
            return QueryData<VolumeDataSummary>(string.Format(Queries.SelectVolumeDataMonthRecords, ispId, sitid, String.Join(",", GenerateDaysList(from, to, "yyyyMM"))));
        }

        private static IEnumerable<string> GenerateDaysList(DateTime from, DateTime to, string format = "yyyyMMdd")
        {
            var days = (int)Math.Ceiling(to.Subtract(from).TotalDays);
            var daysList = new List<string>();

            for (int i = 0; i <= days; i++)
            {
                daysList.Add("'" + from.AddDays(i).ToString(format) + "'");
            }
            return daysList.Distinct();
        }



        #endregion

        #region Import

        /// <summary>
        /// Insert the given volumedata entries into cassandra. Also mention the file they come from. We check via the unique filename if it has already been imported or not.
        /// There's no overwrite function yet. See the todo inside the method below for instructions if you would need it.
        /// </summary>
        /// <param name="entries"></param>
        /// <param name="filename"></param>
        public void InsertVolumeData(List<VolumeDataEntry> entries, string filename)
        {
            try
            {
                Console.WriteLine("Data read from source file complete. Now sending to cassandra.");
                var watch = new Stopwatch();
                using (ICluster cluster = ClusterManager.GetCluster(ClusterName))
                {
                    // Check if the file has not been imported yet
                    // todo: If you enable a force option, when overwriting data in cassandra, you must make sure you decrement the counters (of volumedataday/month) with the values from the importlog.
                    // note: you'll need to work with secondary indexes!
                    if (IsFileImported(cluster, filename)) return;

                    // batch them all
                    GenerateAndInsertRecords(entries, cluster);

                    // write log record
                    var queryLog = string.Format(Queries.InsertLogRecord, filename,
                        DateTime.Now.ToUnixTime() * 1000, entries.Count);
                    var cmd = cluster.CreatePocoCommand();
                    cmd.Execute(queryLog).AsFuture().Wait();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                ClusterManager.Shutdown();
            }
        }

        private static void GenerateAndInsertRecords(List<VolumeDataEntry> entries, ICluster cluster)
        {
            var watch = new Stopwatch();
            watch.Start();
            foreach (var entry in entries.Chunk(2500))
            {
                var bw = new BatchWrapper();
                var bwCounters = new BatchWrapper();
                foreach (var e in entry)
                {
                    long time = e.time_stamp.ToUnixTime() * 1000;
                    // volumedata
                    string dataQuery = string.Format(Queries.InsertVolumeRecord,
                        e.ispid, e.sitid, e.time_stamp.ToString("yyyyMMdd"), time, e.forwardvolume, e.returnvolume,
                        e.accumulatedforwardvolume, e.accumulatedreturnvolume, e.freezonertn, e.freezonefwd);
                    bw.Queries.Add(dataQuery);

                    // volumedataday
                    string dayQuery = string.Format(Queries.UpdateDayRecord,
                        e.ispid, e.sitid, e.time_stamp.ToString("yyyyMMdd"), e.forwardvolume, e.returnvolume,
                        e.accumulatedforwardvolume, e.accumulatedreturnvolume, e.freezonertn, e.freezonefwd);

                    // volumedatamonth
                    string monthQuery = string.Format(Queries.UpdateMonthRecord,
                        e.ispid, e.sitid, e.time_stamp.ToString("yyyyMM"), e.forwardvolume, e.returnvolume,
                        e.accumulatedforwardvolume, e.accumulatedreturnvolume, e.freezonertn, e.freezonefwd);

                    bwCounters.Queries.AddRange(new[] { dayQuery, monthQuery });
                }

                // todo: investigate how we can handle bw failure and deal with bwcounters and possible inconsistencies here.
                InsertRawRecords(bw.ToBatchQuery(), cluster);
                InsertRawRecords(bwCounters.ToCounterBatchQuery(), cluster);
            }
        }


        private static bool IsFileImported(ICluster cluster, string filename)
        {
            var cmd = cluster.CreatePocoCommand();
            var importLog = cmd.Execute<ImportLog>(string.Format(Queries.SelectLogRecord, filename)).AsFuture().Result;
            return importLog.Count > 0;
        }


        /// <summary>
        /// Insert the actual records in cassandra (takes a batch query string)
        /// </summary>
        /// <param name="query"></param>
        /// <param name="cluster"></param>
        private static void InsertRawRecords(string query, ICluster cluster)
        {
            long running = 0;
            //var watch = new Stopwatch();
            //watch.Start();

            var cmd = cluster.CreateOrdinalCommand().WithConsistencyLevel(ConsistencyLevel.ONE);
            Interlocked.Increment(ref running);
            cmd.Execute(query).AsFuture().ContinueWith(_ => Interlocked.Decrement(ref running));

            while (Thread.VolatileRead(ref running) > 0)
            {
                //Console.WriteLine("Running {0}", running);
                Thread.Sleep(1000);
            }
            //Console.WriteLine("insertrawrecords: " + watch.Elapsed);
        }

        //private static void GenerateNMSNetFlowFiles(List<NMSNetFlowFile> files, ICluster cluster)
        //{
        //    var watch = new Stopwatch();
        //    watch.Start();
        //    foreach (var file in files.Chunk(2500))
        //    {
        //        var bw = new BatchWrapper();
        //        var bwCounters = new BatchWrapper();
        //        foreach (var f in files)
        //        {
        //            string fileQuery = string.Format(Queries.InsertNMSNetFlowFile, f.filename, "?", f.time_stamp);
        //            bw.Queries.Add(fileQuery);

        //            bwCounters.Queries.AddRange(new[] { fileQuery });
        //        }
        //        InsertRawRecords(bw.ToBatchQuery(), cluster);
        //        InsertRawRecords(bwCounters.ToCounterBatchQuery(), cluster);
        //    }
        //}
        private static long _running;
        private static void GenerateNMSNetFlowFiles(List<NMSNetFlowFile> files, ICluster cluster)
        {
            ICqlCommand cmd = cluster.CreatePocoCommand();
            string insertQuery = Queries.InsertNMSNetFlowFile;
            IPreparedQuery<NonQuery> preparedInsert = cmd.WithConsistencyLevel(ConsistencyLevel.ONE).Prepare(insertQuery);
            foreach (NMSNetFlowFile f in files)
            {
                long running = Interlocked.Increment(ref _running);
                preparedInsert.Execute(new { filename = f.filename, file = f.file, time_stamp = f.time_stamp }).AsFuture().ContinueWith(_ => Interlocked.Decrement(ref _running));
            }
            while (Thread.VolatileRead(ref _running) > 0)
            {
                Thread.Sleep(1000);
            }
            //foreach (var item in cmd.Execute<NMSNetFlowFile>("select * from volumedata.nmsnetflowfiles").AsFuture().Result)
            //{
            //    Debug.WriteLine("{0} {1} {2}", item.time_stamp, item.filename, System.Text.Encoding.Default.GetString(item.file));
            //}
        }

        public void InsertNMSNetFlowFiles(List<NMSNetFlowFile> files, string filename) 
        {
            try
            {
                Console.WriteLine("Data read from source file complete. Now sending to cassandra.");
                var watch = new Stopwatch();
                using (ICluster cluster = ClusterManager.GetCluster(ClusterName))
                {
                    // Check if file has not been imported yet
                    if (IsFileImported(cluster, filename)) return;

                    // batch the files
                    GenerateNMSNetFlowFiles(files, cluster);

                    // write log record
                    var queryLog = string.Format(Queries.InsertLogRecord, filename,
                        DateTime.Now.ToUnixTime() * 1000, files.Count);
                    var cmd = cluster.CreatePocoCommand();
                    cmd.Execute(queryLog).AsFuture().Wait();
                }
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
            finally 
            {
                ClusterManager.Shutdown();
            }
        }


        #endregion

    }
}