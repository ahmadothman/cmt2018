﻿using System.Collections.Generic;
using System.Text;

namespace CassandraLib.Model.Helpers
{
    public class BatchWrapper
    {
        public BatchWrapper()
        {
            Queries = new List<string>();
        }

        public List<string> Queries { get; set; }

        public string ToBatchQuery()
        {
            var sb = new StringBuilder();
            sb.AppendLine("BEGIN BATCH");
                // used to be UNLOGGED BATCH, which would incline that part of the batch can succeed, and another part fails. 
            // logged batch makes sure that the entire batch fails in case of error. Which implies we can just reimport the file again.
            foreach (var query in Queries)
            {
                sb.AppendLine(query);
            }
            sb.Append("APPLY BATCH;");
            return sb.ToString();
        }

        public string ToCounterBatchQuery()
        {
            var sb = new StringBuilder();
            sb.AppendLine("BEGIN COUNTER BATCH");
            // used to be UNLOGGED BATCH, which would incline that part of the batch can succeed, and another part fails. 
            // logged batch makes sure that the entire batch fails in case of error. Which implies we can just reimport the file again.
            foreach (var query in Queries)
            {
                sb.AppendLine(query);
            }
            sb.Append("APPLY BATCH;");
            return sb.ToString();
        }
    }
}