﻿namespace CassandraLib.Model.Cql
{
    internal class Queries
    {
        public static string InsertVolumeRecord =
            "INSERT INTO volumedata.volumedata (ispid, sitid, date, time_stamp,forwardvolume, returnvolume,accumulatedforwardvolume,accumulatedreturnvolume,freezonertn,freezonefwd) " +
            "VALUES ({0}, {1}, '{2}', {3}, {4}, {5}, {6}, {7}, {8}, {9}) USING TTL 4320000; ";

        /// <summary>
        /// Example query: update volumedataday set forwardvolume = forwardvolume + 5 where ispid = 1 and sitid = 1 and date = '20131101' and time_stamp = 1383264000;
        /// todo: Set appropriate TTL for 'day' counter values. Possibly one year?
        /// </summary>
        public static string UpdateDayRecord =
            @"
                UPDATE  volumedata.volumedataday
                SET     forwardvolume = forwardvolume + {3}, 
                        returnvolume = returnvolume + {4}, 
                        accumulatedforwardvolume = accumulatedforwardvolume + {5},
                        accumulatedreturnvolume = accumulatedreturnvolume + {6},
                        freezonertn = freezonertn + {7},
                        freezonefwd = freezonefwd + {8}
                        WHERE ispid = {0} AND sitid = {1} AND date = '{2}'; 
            ";

        /// <summary>
        /// Example query: update volumedatamonth set forwardvolume = forwardvolume + 5 where ispid = 1 and sitid = 1 and date = '201311' and time_stamp = 1383264000;
        /// todo: Set appropriate TTL for 'day' counter values. Perhaps 10 years?
        /// </summary>
        public static string UpdateMonthRecord =
            @"
                UPDATE  volumedata.volumedatamonth
                SET     forwardvolume = forwardvolume + {3}, 
                        returnvolume = returnvolume + {4}, 
                        accumulatedforwardvolume = accumulatedforwardvolume + {5},
                        accumulatedreturnvolume = accumulatedreturnvolume + {6},
                        freezonertn = freezonertn + {7},
                        freezonefwd = freezonefwd + {8}
                        WHERE ispid = {0} AND sitid = {1} AND date = '{2}' ; 
            ";

        public static string InsertNMSNetFlowFile =
            "INSERT INTO volumedata.nmsnetflowfiles (filename,file,time_stamp) VALUES (?, ?, ?) ; ";

        public static string InsertLogRecord =
            @"
                INSERT INTO volumedata.importlog (filename, time_stamp, numberofrecords) VALUES ('{0}', {1}, {2}) USING TTL 8640000

            ";

        public static string SelectLogRecord =
            @"
                SELECT filename, time_stamp, numberofrecords FROM volumedata.importlog WHERE filename = '{0}'
            ";

        public static string SelectVolumeDataRecords =
            @"
               SELECT ispid, sitid, date, time_stamp, accumulatedforwardvolume, accumulatedreturnvolume, forwardvolume, returnvolume, freezonertn, freezonefwd from volumedata.volumedata WHERE ispid = {0} and sitid = {1} and date in ({2})
            ";

        public static string SelectVolumeDataDayRecords =
            @"
               SELECT ispid, sitid, date, accumulatedforwardvolume, accumulatedreturnvolume, forwardvolume, returnvolume, freezonertn, freezonefwd from volumedata.volumedataday WHERE ispid = {0} and sitid = {1} and date in ({2})
            ";

        public static string SelectVolumeDataMonthRecords =
            @"
               SELECT ispid, sitid, date, accumulatedforwardvolume, accumulatedreturnvolume, forwardvolume, returnvolume, freezonertn, freezonefwd from volumedata.volumedatamonth WHERE ispid = {0} and sitid = {1} and date in ({2})
            ";

        //        public static string SelectNMSNetFlowFile =
        //            @"
        //               SELECT filename, file, time_stamp from volumedata.nmsnetflowfiles WHERE filename = {0}
        //            ";

    }
}