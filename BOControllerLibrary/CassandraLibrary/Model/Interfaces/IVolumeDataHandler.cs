﻿using System.Collections.Generic;
using CassandraLib.Model.DataModel;

namespace CassandraLib.Model.Interfaces
{
    public interface IVolumeDataHandler
    {
        List<VolumeDataEntry> ParseData(string filename);
    }
}