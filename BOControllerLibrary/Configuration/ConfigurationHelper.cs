﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using BOControllerLibrary.Configuration;
using log4net;

namespace BOControllerLibrary
{
    public class ConfigurationHelper : IConfigurationHelper
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(ConfigurationHelper));
        /// <summary>
        /// Gets the raw value of the configuration string
        /// </summary>
        /// <param name="tag"></param>
        /// <returns></returns>
        protected static string Get(string tag)
        {
            return ConfigurationManager.AppSettings[tag];
        }

        /// <summary>
        /// The data provider for the database
        /// </summary>
        public string CmtDatabaseDataProvider
        {
            get { return ConfigurationManager.ConnectionStrings["CmtDatabase"].ProviderName;  }
        }

        /// <summary>
        /// The connection string for the database
        /// </summary>
        public string CmtDatabaseConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["CmtDatabase"].ConnectionString;
            }
        }

    }


}
