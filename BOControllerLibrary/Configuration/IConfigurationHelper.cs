﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BOControllerLibrary.Configuration
{
    interface IConfigurationHelper
    {
        string CmtDatabaseDataProvider { get;  }
        string CmtDatabaseConnectionString { get;  }
    }
}
