﻿using System;
using System.Collections.Generic;
using System.Text;
using BOControllerLibrary.Model;

namespace BOControllerLibrary.Controllers
{
    /// <summary>
    /// Warning: these interfaces are very preliminary and will probably change as the OM becomes more clear to me :).
    /// The BOLogController takes care of all logging Use Cases, such as terminal activity logging, user action logging and exception logging. 
    /// The terminal activity log is also used by the BOAccountingController for producing invoice statements at the end of the month.
    /// </summary>
    public interface IBOLogController
    {
        /*
         *  •	Log the exceptions produced by the application
            •	Log the activities done on a terminal
            •	Log the activities of a user (Log-In, Log-Out, Session expiration)
            •	Return a list of activities for a given terminal over a given period
            •	Return a list of exceptions over a given period
            •	Return a list of User activities for a given User

         */


        /// <summary>
        /// Log all user and application exceptions.
        /// </summary>
        /// <param name="ex">The exception object: Note that it is useful to subclass System.Exception for more specific information. </param>
        /// <param name="infoMessage">Additional information about the error.</param>
        /// <param name="errorCode">A error code that is used to identify the kind of exception</param>
        void LogApplicationException(Exception ex, string infoMessage, string errorCode);
        
        /// <summary>
        /// Log terminal activity
        /// </summary>
        /// <param name="activity"></param>
        /// <param name="terminalId"></param>
        void LogTerminalActivity(string activity, string terminalId);

        /// <summary>
        /// Log the activities of a user (Log-In, Log-Out, Session expiration)
        /// </summary>
        /// <param name="activity"></param>
        /// <param name="user"></param>
        void LogUserActivity(string activity, string user);

        /// <summary>
        /// Return a list of activities for a given terminal over a given period
        /// </summary>
        /// <param name="terminalId"></param>
        /// <returns></returns>
        IList<ITerminalActivity> GetTerminalActivity(string terminalId);

        /// <summary>
        /// Return a list of exceptions over a given period
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        IList<IApplicationException> GetApplicationExceptions(DateTime from, DateTime to);

        /// <summary>
        /// Return a list of activities for a given user.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        IList<IUserActivity> GetUserActivities(string userId);
    }
}
