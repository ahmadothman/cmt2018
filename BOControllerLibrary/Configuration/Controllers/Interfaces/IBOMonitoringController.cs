﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BOControllerLibrary.Controllers
{
    /// <summary>
    /// This component interacts with the ISPIF and the CMT database mainly for retrieving volume information and technical parameters related to a specific terminal. 
    /// The BOMonitorController contains the main methods for monitoring and managing terminals. 
    /// By means of BOMonitorController it is possible to Activate and De-Activate terminals and to obtain operational data from a terminal (Volume info, CNo values etc.) 
    /// </summary>
    public interface IBOMonitoringController
    {
        /*
         *  •	Get the Volume information and other terminal related data from the ISPIF
            •	Get the Volume consumption data over the last 30 days
            •	Activate a terminal
            •	De-Activate a terminal
            •	Suspend a terminal operation
            •	Resume a terminal operation
            •	Change the SLA
            •	Add additional volume for a terminal
            •	Reset the users traffic information for the current month (This is normally done during the invoicing process)

         */
    }
}
