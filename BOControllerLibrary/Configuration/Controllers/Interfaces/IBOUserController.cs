﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BOControllerLibrary.Controllers
{
    /// <summary>
    /// This is the interface to the authorization and authentication system and is responsible for user login and role management
    /// </summary>
    public interface IBOUserController
    {
        /*
         * 
         *  •	Log-in/Log-out users
            •	Add a user to a specific role
            •	Determine if a user is in a specific role
            •	Register new users
            •	Remove existing users
            •	Modify user administrative data
            •	Add a user to the Distributor Role
            •	Add a user in User or User Network Admin role to an End-User account (EndUsers table)
            •	Add a user to the Network Administrator Role
            •	Add a user to the Network Operator Role
            •	Link End-Users (EndUsers table) to a Distributor
            •	Link End-Users (EndUsers table) to an Organization
            •	Link End-Users (EndUsers table) to a single Terminal

         */
        /// <summary>
        /// Notes:	Processes a request to update the password for a membership user.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="newPassword"></param>
        /// <param name="oldPassword"></param>
        /// <returns></returns>
        bool ChangePassword(string username, string newPassword, string oldPassword);

        /// <summary>
        /// Notes:	Processes a request to update the password question and answer for a membership user.
        /// </summary>
        /// <param name="newPasswordAnswer"></param>
        /// <param name="newPasswordQuestion"></param>
        /// <param name="password"></param>
        /// <param name="userName"></param>
        /// <returns></returns>
        bool ChangePasswordQuestionAndAnswer(string newPasswordAnswer, string newPasswordQuestion, string password, string userName);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="?"></param>
        /// <param name="providerUserKey"></param>
        /// <param name="isApproved"></param>
        /// <param name="passwordAnswer"></param>
        /// <param name="passwordQuestion"></param>
        /// <param name="email"></param>
        /// <param name="password"></param>
        /// <param name="username"></param>
        /// <returns></returns>
        //IMemberShipUser CreateUser(IMemberShipCreateStatus status, object providerUserKey, bool isApproved, string passwordAnswer, string passwordQuestion, string email, string password, string username);

        /// <summary>
        /// Notes:	Removes a user from the membership data source. 
        /// </summary>
        /// <param name="deleteAllRelatedData">If deleteAllRelatedData is true, then all user data stored in the database for the Roles,
        /// Profile, and WebPart personalization is also deleted.  
        /// </param>
        /// <param name="username">The username to delete</param>
        /// <returns>true if the user was successfully deleted; otherwise, false.</returns>
        bool DeleteUser(bool deleteAllRelatedData, string username);

        /// <summary>
        /// FindUsersByEmail returns a list of membership users where the user's e-mail address matches the 
        /// supplied emailToMatch for the configuredApplicationName property. 
        /// 
        /// If your data source supports additional search capabilities, such as wildcard characters, 
        /// you can provide more extensive search capabilities for e-mail addresses.
        /// The results returned by FindUsersByEmail are constrained by the pageIndex and pageSize parameters. 
        /// </summary>
        /// <param name="totalRecords">The totalRecords parameter is an out parameter that is set to the total number of membership users that matched the emailToMatch value. 
        /// For example, if 13 users were found where emailToMatch matched part of or the entire e-mail address, 
        /// and the pageIndex value was 1 with a pageSize of 5, then the MembershipUserCollection would contain the sixth through the tenth users returned. 
        /// totalRecords would be set to 13.
        /// </param>
        /// <param name="pageSize">The pageSize parameter identifies the number of MembershipUser objects to return in theMembershipUserCollection collection. </param>
        /// <param name="pageIndex">The pageIndex parameter identifies which page of results to return where 0 identifies the first page. </param>
        /// <param name="emailToMatch"></param>
        /// <returns>A set of users which match the given E-Mail address. </returns>
        //IMemberShipUserCollection FindUsersByEmail(int totalRecords, int pageSize, int pageIndex, string emailToMatch);


    }

}
