﻿using System;
using System.Collections.Generic;
using System.Text;
using BOControllerLibrary.Controllers.Interfaces;
using BOControllerLibrary.Model;
using BOControllerLibrary.Model.Distributor;
using BOControllerLibrary.Model.ISP;
using BOControllerLibrary.Model.Organization;
using BOControllerLibrary.Model.Terminal;
using BOControllerLibrary.Model.User;

namespace BOControllerLibrary.Controllers
{
    /// <summary>
    /// This component is responsible for the accounting Use Cases specified by UC-M-270 and others.  
    /// The component is mainly used for accounting purpose such a volume consumption reporting, integration with the billing system etc
    /// </summary>
    public interface IBOAccountingController : IBODataControllerBase
    {
        /*
         *  •	Return a list of activities and volume consumption for a specific terminal over a specific period.
            •	Return a list of quality parameters (CNo values) for a specific terminal over a given period.
         */

        /// <summary>
        /// Returns a list of terminals which are serviced by a Distributor. The UserId in this case is taken from the aspnet_Users table.
        /// </summary>
        /// <param name="distributorId"></param>
        /// <returns></returns>
        List<Terminal> GetTerminalsByDistributor(string distributorId);

        /// <summary>
        /// Returns a list of organizations
        /// </summary>
        /// <param name="distributorId"></param>
        /// <returns></returns>
        List<Organization> GetOrganisationsByDistributor(string distributorId);

        /// <summary>
        /// Returns the terminal operated by a single End User
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Terminal GetUserTerminal(int endUserId);

        /// <summary>
        /// Returns the distributor which is linked to the user
        /// </summary>
        /// <param name="userId">The userid here is the UID</param>
        /// <returns></returns>
        Distributor GetDistributorForUser(string userId);

        /// <summary>
        /// Returns a list of terminals which are linked to the organization.
        /// </summary>
        /// <param name="organizationId">This is the orgid from the organizations table</param>
        /// <returns></returns>
        List<Terminal> GetTerminalsByOrganization(int organizationId);

        /// <summary>
        /// Returns the details of a terminal by its SitId value. These are the values which are stored in the CMT Database
        /// 
        /// developer notes: it's possible that we will need to use a ITerminalDetails object here instead of the ITerminal to reduce amount of traffic
        /// </summary>
        /// <param name="sitId"></param>
        /// <returns></returns>
        Terminal GetTerminalDetailsBySitId(int sitId);

        /// <summary>
        /// Returns the terminal details by its IP address
        /// 
        /// developer notes: it's possible that we will need to use a ITerminalDetails object here instead of the ITerminal to reduce amount of traffic
        /// </summary>
        /// <param name="ipAddress"></param>
        /// <returns></returns>
        Terminal GetTerminalDetailsByIp(string ipAddress);

        /// <summary>
        /// Returns the terminal details by the terminal MAC address
        /// 
        /// developer notes: it's possible that we will need to use a ITerminalDetails object here instead of the ITerminal to reduce amount of traffic
        /// </summary>
        /// <param name="macAddress"></param>
        /// <returns></returns>
        Terminal GetTerminalDetailsByMAC(string macAddress);


        /// <summary>
        /// Returns a Distributor record defined by its DistributorId
        /// </summary>
        /// <param name="distributorId"></param>
        /// <returns></returns>
        Distributor GetDistributor(int distributorId);

        /// <summary>
        /// Returns an organization by the Organization ID.
        /// </summary>
        /// <param name="organizationId"></param>
        /// <returns></returns>
        Organization GetOrganization(int organizationId);

        /// <summary>
        /// Returns an EndUser by the given UserId. 
        /// This UserId is taken from the apsnet_Users table. 
        /// If the user has an EndUser role, this method returns the details of the EndUser. 
        /// The method uses theEndUserUsers link table
        /// 
        /// If the user has no EndUser role, we'll return null.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        EndUser GetEndUser(string userId);

        /// <summary>
        /// Updates EndUser data in the EndUser table. The method should return false if the update fails. 
        /// If the EndUser does not exist yet in the EndUser table a new record MUST be inserted
        /// </summary>
        /// <param name="user"></param>
        /// <returns>true if the operation succeeded, false if it didn't</returns>
        bool UpdateEndUser(EndUser userData);

        /// <summary>
        /// Update terminal data in the terminal table. The method returns false if the update fails. 
        /// If the Terminal does not exist yet in the Terminal table a new record MUST be inserted.
        /// </summary>
        /// <param name="terminalData"></param>
        /// <returns></returns>
        bool UpdateTerminal(Terminal terminalData);

        /// <summary>
        /// Updates an organization in the Organization table. Returns false if the update fails. 
        /// If the Organization does not exist yet in the Organization table a new record MUST be inserted
        /// </summary>
        /// <param name="organizatonData"></param>
        /// <returns></returns>
        bool UpdateOrganization(Organization organizatonData);

        /// <summary>
        /// Updates a distributor in the distributor database. The method returns false if the update fails. 
        /// If the Distributor does not exist yet in the Distributor table a new record MUST be inserted.
        /// </summary>
        /// <param name="distributorData"></param>
        /// <returns></returns>
        bool UpdateDistributor(Distributor distributorData);

        /// <summary>
        /// Updates ISP data in the ISP table. The method returns false if the method fails. 
        /// If the ISP does not exist yet in the ISP table a new record MUST be inserted.
        /// </summary>
        /// <param name="ispData"></param>
        /// <returns></returns>
        bool UpdateIsp(Isp ispData);

        /// <summary>
        /// Returns an ISP record by its ISP id.
        /// </summary>
        /// <param name="ispId"></param>
        /// <returns></returns>
        Isp GetISP(int ispId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="distributorId"></param>
        /// <param name="macAddress"></param>
        /// <returns></returns>
        bool IsTermOwnedByDistributor(int distributorId, string macAddress);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="organizationId"></param>
        /// <param name="macAddress"></param>
        /// <returns></returns>
        bool isTermOwnedByOrganization(int organizationId, string macAddress);
    }
}
