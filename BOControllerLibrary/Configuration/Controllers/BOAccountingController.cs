﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using BOControllerLibrary.Model;
using BOControllerLibrary.Model.Distributor;
using BOControllerLibrary.Model.ISP;
using BOControllerLibrary.Model.Organization;
using BOControllerLibrary.Model.Terminal;
using BOControllerLibrary.Model.User;
using Dapper;
using Dapper.Contrib;
using Dapper.Contrib.Extensions;

namespace BOControllerLibrary.Controllers
{
    /// <summary>
    /// Todo: refactor the actual table names and column names to a Sql class. 
    /// </summary>
    public class BOAccountingController : BODataControllerBase, IBOAccountingController
    {
        #region Implementation of IBOAccountingController

        /// <summary>
        ///   Returns a list of terminals which are serviced by a Distributor. The UserId in this case is taken from the aspnet_Users table.
        /// </summary>
        /// <param name = "distributorId"></param>
        /// <returns></returns>
        public List<Terminal> GetTerminalsByDistributor(string distributorUserId)
        {
            const string sql = "select t.* " +
                               "from terminals t " +
                               "join organizations o on t.orgid = o.id " +
                               "join distributorsusers du on du.distributorid = o.distributorid " +
                               "where du.userid = @distributorUserId";
            using (var db = new PetaPoco.Database(ConnectionString, DataProvider))
            {
                var dbObj = db.Fetch<Terminal>(
                    sql, new { distributorUserId });
                return dbObj.ToList();
            }
        }

        /// <summary>
        ///   Returns a list of organizations
        /// </summary>
        /// <param name = "distributorId">The UID of the distributor</param>
        /// <returns></returns>
        public List<Organization> GetOrganisationsByDistributor(string distributorId)
        {
            const string sql = "select o.*, a.*, d.* " +
                               "from organizations o " +
                               "join addresses a on a.Id = o.Addressid " +  // mogelijk geen address record??
                               "join distributors d on d.id = o.distributorid " +
                               "join distributorsusers du on du.distributorid = d.id " +
                               "where du.userid = @distributorId  ";
            using (var db = new PetaPoco.Database(ConnectionString, DataProvider))
            {
                var dbObj = db.Fetch<Organization, Address, Distributor, Organization>(
                    (o, a, d) =>
                        {
                            o.Address = a;
                            o.Distributor = d;
                            return o;
                        },
                    sql, new { distributorId });
                return dbObj.ToList();
            }
        }

        /// <summary>
        ///   Returns the terminal operated by a single End User
        /// </summary>
        /// <param name = "endUserId"></param>
        /// <returns></returns>
        public Terminal GetUserTerminal(int endUserId)
        {
            const string sql = 
                "SELECT * " +
                "FROM terminals " +
                "where enduserid = @endUserId ";
            using (var db = new PetaPoco.Database(ConnectionString, DataProvider))
            {
                var dbObj = db.Fetch<Terminal>(
                    sql, new { endUserId });
                return dbObj.FirstOrDefault();
            }
        }

        /// <summary>
        ///   Returns the distributor which is linked to the user
        /// </summary>
        /// <param name="userid">The userid here is the UID</param>
        /// <returns></returns>
        public Distributor GetDistributorForUser(string userid)
        {
            const string sql = "select d.*, a.* " +
                                "from distributors d " +
                                "join addresses a on a.id = d.addressid " +
                                "join distributorsusers du on du.distributorid = d.id " +
                                "where du.userid = @userid";
            using (var db = new PetaPoco.Database(ConnectionString, DataProvider))
            {
                var dbObj = db.Fetch<Distributor, Address, Distributor>(
                    (i, a) => { i.Address = a; return i; },
                    sql, new { userid });
                return dbObj.FirstOrDefault();
            }
        }

        /// <summary>
        ///   Returns a list of terminals which are linked to the organization.
        /// </summary>
        /// <param name = "organizationId"></param>
        /// <returns></returns>
        public List<Terminal> GetTerminalsByOrganization(int organizationId)
        {
            const string sql =
                "SELECT t.* " +
                "from terminals t " +
                "join organizations o on o.id = t.orgid " +
                "where o.Id = @organizationId";
            using (var db = new PetaPoco.Database(ConnectionString, DataProvider))
            {
                var dbObj = db.Fetch<Terminal>(
                    sql, new { organizationId });
                return dbObj.ToList();
            }
        }

        /// <summary>
        ///   Returns the details of a terminal by its SitId value. These are the values which are stored in the CMT Database
        /// 
        ///   developer notes: it's possible that we will need to use a ITerminalDetails object here instead of the ITerminal to reduce amount of traffic
        /// </summary>
        /// <param name = "sitId"></param>
        /// <returns></returns>
        public Terminal GetTerminalDetailsBySitId(int sitId)
        {
            const string sql = "SELECT * FROM terminals where sitid = @sitid";
            using (var connection = CreateConnection())
            {
                return connection.Query<Terminal>(sql, new { sitid = sitId }).FirstOrDefault();
            }
        }

        /// <summary>
        ///   Returns the terminal details by its IP address
        /// 
        ///   developer notes: it's possible that we will need to use a ITerminalDetails object here instead of the ITerminal to reduce amount of traffic
        /// </summary>
        /// <param name = "ipAddress"></param>
        /// <returns></returns>
        public Terminal GetTerminalDetailsByIp(string ipAddress)
        {
            const string sql = "SELECT * FROM terminals where IPAddress = @ip";
            using (var connection = CreateConnection())
            {
                return connection.Query<Terminal>(sql, new { ip = ipAddress }).FirstOrDefault();
            }
        }

        /// <summary>
        ///   Returns the terminal details by the terminal MAC address
        /// 
        ///   developer notes: it's possible that we will need to use a ITerminalDetails object here instead of the ITerminal to reduce amount of traffic
        /// </summary>
        /// <param name = "macAddress"></param>
        /// <returns></returns>
        public Terminal GetTerminalDetailsByMAC(string macAddress)
        {
            const string sql = "SELECT * FROM terminals where macaddress = @macaddress";
            using (var connection = CreateConnection())
            {
                return connection.Query<Terminal>(sql, new { macaddress = macAddress }).FirstOrDefault();
            }
        }

        /// <summary>
        ///   Returns a Distributor record defined by its DistributorId
        /// </summary>
        /// <param name = "distributorId"></param>
        /// <returns></returns>
        public Distributor GetDistributor(int distributorId)
        {
            
            /*
                select *
                from distributors
                where distributorid = ?
             */
            const string sql = "select d.*, a.* " +
                                "from distributors d " +
                                "join addresses a on a.id = d.addressid " +
                                "where d.id = @distributorId";

            using (var db = new PetaPoco.Database(ConnectionString, DataProvider))
            {
                var dbObj = db.Fetch<Distributor, Address, Distributor>(
                    (i, a) => { i.Address = a; return i; },
                    sql, new { distributorId });
                return dbObj.FirstOrDefault();
            }

            //using (var connection = CreateConnection())
            //{
            //    return connection.Query<Distributor>(sql, new { distributorid = distributorId }).FirstOrDefault();
            //}
        }

        /// <summary>
        ///   Returns an organization by the Organization ID.
        /// </summary>
        /// <param name = "organizationId"></param>
        /// <returns></returns>
        public Organization GetOrganization(int organizationId)
        {
            using (var db = new PetaPoco.Database(ConnectionString, DataProvider))
            {
                var dbObj = db.Fetch<Organization, Address, Distributor,Organization>(
                    (i, a, d) =>
                        {
                            i.Address = a;
                            i.Distributor = d;
                            return i;
                        },
                    "SELECT o.*, a.*, d.* " +
                    "FROM Organizations as o JOIN Addresses as a on o.addressid = a.id LEFT JOIN Distributors d on o.DistributorId = d.Id where o.Id = @organizationId", new { organizationId });
                return dbObj.FirstOrDefault();
            }
        }

        /// <summary>
        ///   Returns an EndUser by the given UserId. 
        ///   This UserId is taken from the apsnet_Users table. 
        ///   If the user has an EndUser role, this method returns the details of the EndUser. 
        ///   The method uses theEndUserUsers link table
        /// 
        ///   If the user has no EndUser role, we'll return null.
        /// </summary>
        /// <param name = "userId"></param>
        /// <returns></returns>
        public EndUser GetEndUser(string userId)
        {
            const string sql = "select u.[Id],u.[FirstName],u.[LastName],u.[EMail], u.[Phone],u.[SitId],u.[OrgId] " +
                      "from endusers u " +
                      "join enduserusers userref on u.Id = userref.Enduserid " +
                      "where userref.userid = @userid";
            using (var connection = CreateConnection())
            {
                return connection.Query<EndUser>(sql, new { userid = userId }).FirstOrDefault();
            }
        }

        /// <summary>
        ///   Updates EndUser data in the EndUser table. The method should return false if the update fails. 
        ///   If the EndUser does not exist yet in the EndUser table a new record MUST be inserted
        /// </summary>
        /// <param name = "userData"></param>
        /// <returns>true if the operation succeeded, false if it didn't</returns>
        public bool UpdateEndUser(EndUser userData)
        {
            using (var db = new PetaPoco.Database(ConnectionString, DataProvider))
            {
                int spResult = db.Execute("exec cmtUpdateEndUsers @Id,@FirstName,@LastName,@Email,@Phone,@SitId,@OrgId",
                                          new
                                          {
                                              userData.Id,
                                              userData.FirstName,
                                              userData.LastName,
                                              userData.Email,
                                              userData.Phone,
                                              userData.SitId,
                                              userData.OrgId
                                          }
                    );
                if (spResult == 0 || spResult == 1)
                    return true;
                return false;
            }

        }

        /// <summary>
        ///   Update terminal data in the terminal table. The method returns false if the update fails. 
        ///   If the Terminal does not exist yet in the Terminal table a new record MUST be inserted.
        /// </summary>
        /// <param name = "terminalData"></param>
        /// <returns></returns>
        public bool UpdateTerminal(Terminal terminalData)
        {
            //using (var db = new PetaPoco.Database(ConnectionString, DataProvider))
            //{
            //    var result = db.Update("Terminals", "MacAddress", terminalData);
            //    return result > 0;
            //}

            if (terminalData.Address == null)
                terminalData.Address = new Address();

            using (var db = new PetaPoco.Database(ConnectionString, DataProvider))
            {
                int spResult = db.Execute("exec [cmtUpdateTerminal] @MacAddress,@StartDate,@ExpiryDate,@EndUserId,@MonthlyConsumed,@MonthlyConsumedReturn," +
                    "@SlaId,@AdmStatus,@SitId,@FullName,@Phone,@Fax,@Email,@Remarks,@OrgId,@IpAddress,@LatLong,@FirstActivationDate,@Blade,@IPMgm," +
                    "@IPTunnel,@SpotID,@RTPool,@FWPool,@IspId,@AddressLine1,@AddressLine2,@Location,@PostalCode,@Country",
                                          new
                                          {
                                              terminalData.MacAddress,
                                              terminalData.StartDate,
                                              terminalData.ExpiryDate,
                                              terminalData.EndUserId,
                                              terminalData.MonthlyConsumed,
                                              terminalData.MonthlyConsumedReturn,
                                              terminalData.SlaId,
                                              terminalData.AdmStatus,
                                              terminalData.SitId,
                                              terminalData.FullName,
                                              terminalData.Phone,
                                              terminalData.Fax,
                                              terminalData.Email,
                                              terminalData.Remarks,
                                              terminalData.OrgId,
                                              terminalData.IpAddress,
                                              terminalData.LatLong,
                                              terminalData.FirstActivationDate,
                                              terminalData.Blade,
                                              terminalData.IPMgm,
                                              terminalData.IPTunnel,
                                              terminalData.SpotID,
                                              terminalData.RTPool,
                                              terminalData.FWPool,
                                              terminalData.IspId,
                                              terminalData.Address.AddressLine1,
                                              terminalData.Address.AddressLine2,
                                              terminalData.Address.Location,
                                              terminalData.Address.PostalCode,
                                              terminalData.Address.Country
                                          }
                    );
                if (spResult == 0 || spResult == 1)
                    return true;
                return false;
            }
        }

        /// <summary>
        ///   Updates an organization in the Organization table. Returns false if the update fails. 
        ///   If the Organization does not exist yet in the Organization table a new record MUST be inserted
        /// </summary>
        /// <param name = "organizatonData"></param>
        /// <returns></returns>
        public bool UpdateOrganization(Organization organizatonData)
        {
            if (organizatonData.Address == null)
                organizatonData.Address = new Address();

            if (organizatonData.Distributor == null)
                organizatonData.Distributor = new Distributor();

            using (var db = new PetaPoco.Database(ConnectionString, DataProvider))
            {
                int spResult = db.Execute("exec [cmtUpdateOrganization] @Id,@FullName,@Phone,@Fax,@Email,@WebSite,@Remarks,@DistributorId,@Vat, @AddressLine1,@AddressLine2,@Location,@PostalCode,@Country",
                                          new
                                              {
                                                  organizatonData.Id,
                                                  organizatonData.FullName,
                                                  organizatonData.Phone,
                                                  organizatonData.Fax,
                                                  organizatonData.Email,
                                                  organizatonData.WebSite,
                                                  organizatonData.Remarks,
                                                  DistributorId = organizatonData.Distributor != null ? organizatonData.Distributor.Id : null,
                                                  organizatonData.Vat,
                                                  organizatonData.Address.AddressLine1,
                                                  organizatonData.Address.AddressLine2,
                                                  organizatonData.Address.Location,
                                                  organizatonData.Address.PostalCode,
                                                  organizatonData.Address.Country                                                  
                                              }
                    );
                if (spResult == 0 || spResult == 1)
                    return true;
                return false;
            }
        }

        /// <summary>
        ///   Updates a distributor in the distributor database. The method returns false if the update fails. 
        ///   If the Distributor does not exist yet in the Distributor table a new record MUST be inserted.
        /// </summary>
        /// <param name = "distributorData"></param>
        /// <returns></returns>
        public bool UpdateDistributor(Distributor distributorData)
        {
            if (distributorData.Address == null)
                distributorData.Address = new Address();

            if (distributorData.Isp == null)
                distributorData.Isp = new Isp();

            using (var db = new PetaPoco.Database(ConnectionString, DataProvider))
            {
                int spResult = db.Execute("exec [cmtUpdateDistributor] @Id,@FullName,@Phone,@Fax,@Email,@WebSite,@Remarks,@IspId,@Vat,@AddressLine1,@AddressLine2,@Location,@PostalCode,@Country",
                                          new
                                          {
                                              distributorData.Id,
                                              distributorData.FullName,
                                              distributorData.Phone,
                                              distributorData.Fax,
                                              distributorData.Email,
                                              distributorData.WebSite,
                                              distributorData.Remarks,
                                              IspId = distributorData.Isp.Id,
                                              distributorData.Vat,
                                              distributorData.Address.AddressLine1,
                                              distributorData.Address.AddressLine2,
                                              distributorData.Address.Location,
                                              distributorData.Address.PostalCode,
                                              distributorData.Address.Country

                                          }
                    );
                if (spResult == 0 || spResult == 1)
                    return true;
                return false;
            }
        }

        /// <summary>
        ///   Updates ISP data in the ISP table. The method returns false if the method fails. 
        ///   If the ISP does not exist yet in the ISP table a new record MUST be inserted.
        /// </summary>
        /// <param name = "ispData"></param>
        /// <returns></returns>
        public bool UpdateIsp(Isp ispData)
        {
            if (ispData.Address == null)
                ispData.Address = new Address();
            using (var db = new PetaPoco.Database(ConnectionString, DataProvider))
            {
                int spResult = db.Execute("exec [cmtUpdateIsp] @Id,@CompanyName,@Phone,@Fax,@Email,@Remarks,@AddressLine1,@AddressLine2,@Location,@PostalCode,@Country",
                                          new
                                              {
                                                  ispData.Id,
                                                  ispData.CompanyName,
                                                  ispData.Phone,
                                                  ispData.Fax,
                                                  ispData.Email,
                                                  ispData.Remarks,
                                                  ispData.Address.AddressLine1,
                                                  ispData.Address.AddressLine2,
                                                  ispData.Address.Location,
                                                  ispData.Address.PostalCode,
                                                  ispData.Address.Country

                                              }
                    );
                //var result = db.Update("ISPS", "ISPId", ispData);
                //return result > 0;
                if (spResult == 0 || spResult == 1)
                    return true;
                return false;
            }
        }

        /// <summary>
        ///   Returns an ISP record by its ISP id.
        /// </summary>
        /// <param name = "ispId"></param>
        /// <returns></returns>
        public Isp GetISP(int ispId)
        {
            using (var db = new PetaPoco.Database(ConnectionString, DataProvider))
            {
                var dbObj = db.Fetch<Isp, Address, Isp>(
                    (i, a) => { i.Address = a; return i; },
                    "SELECT i.*, a.*" +
                    //"a.[AddressLine1],a.[AddressLine2],a.[Location],a.[PostalCode],a.[Country] " +
                    "FROM Isps as i JOIN Addresses as a on i.addressid = a.id where i.Id = @ispId", new { ispId });
                return dbObj.FirstOrDefault();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="distributorId"></param>
        /// <param name="macAddress"></param>
        /// <returns></returns>
        public bool IsTermOwnedByDistributor(int distributorId, string macAddress)
        {
            using (var db = new PetaPoco.Database(ConnectionString, DataProvider))
            {
                    const string sql = 
                    "SELECT sitid " +
                    "from terminals t " +
                    "join organizations o on o.Id = t.sitid " +
                    "join distributors d on d.Id = o.distributorid " +
                    "where macaddress = @macAddress";
                var i = db.ExecuteScalar<int>(sql, new {macAddress});
                return i == distributorId;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="organizationId"></param>
        /// <param name="macAddress"></param>
        /// <returns></returns>
        public bool isTermOwnedByOrganization(int organizationId, string macAddress)
        {
            using (var db = new PetaPoco.Database(ConnectionString, DataProvider))
            {
                var i = db.ExecuteScalar<int>("SELECT sitid from terminals where macaddress = @macAddress", new { macAddress });
                return i == organizationId;
            }
        }

        #endregion
    }
}
