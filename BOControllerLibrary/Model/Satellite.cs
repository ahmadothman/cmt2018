﻿namespace BOControllerLibrary.Model
{
    /// <summary>
    /// This class represents a satellite
    /// </summary>
    public class Satellite
    {
        /// <summary>
        /// The satellite's unique identifier, mandatory
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// The name of the satellite, mandatory
        /// </summary>
        public string SatelliteName { get; set; }

        /// <summary>
        /// The description of the satellite, not mandatory
        /// </summary>
        public string SatelliteDescription { get; set; }
    }
}
