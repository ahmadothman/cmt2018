﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BOControllerLibrary.Model.Reporting
{
    /// <summary>
    /// Class used for the Mandac transactions as stored in the CMT database
    /// </summary>
    public class MandacTransaction
    {
        public string TransactionId { get; set; }
        public DateTime TransactionDate { get; set; }
        public double VolumePurchasedGB { get; set; }
        public double FeePaid { get; set; }
        public string CurrencyCode { get; set; }
        public string PaymentMethod { get; set; }
        public double VolumeUsed { get; set; }
        public string HotSpotId { get; set; }
        public DateTime LastUpdate { get; set; }
        public string VoucherType { get; set; }
        public DateTime ExpiryDate { get; set; }
    }
}
