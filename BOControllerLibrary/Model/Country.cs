﻿namespace BOControllerLibrary.Model
{
    public class Country : ICountry
    {
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
    }
}
