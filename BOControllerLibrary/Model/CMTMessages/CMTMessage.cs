﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BOControllerLibrary.Model.CMTMessages
{
    /// <summary>
    /// This class represents a message sent within the CMT from a NOCSA to one or more users.
    /// </summary>
    public class CMTMessage
    {
        /// <summary>
        /// A unique message ID
        /// </summary>
        public Guid MessageId { get; set; }

        /// <summary>
        /// The content of the message
        /// </summary>
        public string MessageContent { get; set; }

        /// <summary>
        /// The title of the message
        /// </summary>
        public string MessageTitle { get; set; }

        /// <summary>
        /// The time as from which the message will be displayed to users
        /// </summary>
        public DateTime StartTime { get; set; }

        /// <summary>
        /// The date after which the message no longer will be 
        /// displayed to the user(s). 
        /// </summary>
        public DateTime EndTime { get; set; }

        /// <summary>
        /// The role or user the message was sent to
        /// </summary>
        public string Recipient { get; set; }

        /// <summary>
        /// The full name of the recipient
        /// </summary>
        public string RecipientFullName { get; set; }

        /// <summary>
        /// A file attached to the message
        /// </summary>
        public string Attachment { get; set; }

        /// <summary>
        /// Flag to determine if the message has an attachment
        /// </summary>
        public bool HasAttachment { get; set; }

        /// <summary>
        /// The type of file attached to the message
        /// </summary>
        public string AttachmentType { get; set; }

        /// <summary>
        /// Determines of this message is visible for the end-user or not
        /// </summary>
        public Boolean Release { get; set; }
    }
}
