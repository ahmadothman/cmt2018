﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BOControllerLibrary.Model.Documents
{
    /// <summary>
    /// This class represents a document
    /// </summary>
    public class Document
    {
        /// <summary>
        /// The document unique identifier, mandatory
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// The name of the document, including the extension, mandatory
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The description of the document, not mandatory
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Date and time the document was uploaded, mandatory
        /// </summary>
        public DateTime DateUploaded { get; set; }

        /// <summary>
        /// Identifier of the recipient, mandatory
        /// </summary>
        /// <remarks>
        /// Either this property holds a reference to a specific user, or the name of a specific
        /// role (NOCSA, NOCOP, Distributor, DistributorReadOnly, EndUser, FinanceAdmin, MulticastServiceOperator, Organization, Suspended Distributor, VNO),
        /// or everyone if the document is meant for all users in the sytem.
        /// </remarks>
        public string Recipient { get; set; }

        /// <summary>
        /// The full name of the recipient, not mandatory
        /// </summary>
        public string RecipientFullName { get; set; }
    }
}
