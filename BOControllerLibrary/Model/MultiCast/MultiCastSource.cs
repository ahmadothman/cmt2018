﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BOControllerLibrary.Model.MultiCast
{
    /// <summary>
    /// This class represents the sources as used multicast app
    /// Sources are where the content for a multicast can be found, 
    /// as provided by the administrator of the multicast
    /// </summary>
    public class MultiCastSource
    {
        /// <summary>
        /// The unique ID of the source
        /// </summary>
        public int sourceID { get; set; }
        /// <summary>
        /// The name of the source, as provided by the user
        /// </summary>
        public string sourceName { get; set; }
        /// <summary>
        /// The actual location of the source
        /// </summary>
        public string URL { get; set; }
        /// <summary>
        /// Username required to access the source
        /// </summary>
        public string username { get; set; }
        /// <summary>
        /// Password required to access the source
        /// </summary>
        public string password { get; set; }
        /// <summary>
        /// Flag to determine if the source comes from SatADSL's internal servers
        /// </summary>
        public bool satADSLSource { get; set; }
        /// <summary>
        /// The ID of the multicast group for which the source was created
        /// </summary>
        public int multiCastGroupID { get; set; }
    }
}