﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BOControllerLibrary.Model.MultiCast
{

    /// <summary>
    /// 
    /// </summary>
    public class MultiCastLog
    {
        public int Id { get; set; }
        //public string MacAddress { get; set; }
        //public string Url { get; set; }
        public int BufferLevel { get; set; }
        public int Latency { get; set; }
        public int FrameLoss { get; set; }
        public int FrameDup { get; set; }
        public int FrameDrop { get; set; }
        //public int SoftErrorCount { get; set; }
        //public int StreamNumber { get; set; }
        public int BitRate { get; set; }
        //public int Reconnects { get; set; }
        //public int LastError { get; set; }
        //public string LastErrorDesc { get; set; }
        public short Volume { get; set; }
        //public string UpTime { get; set; }
        public DateTime LastUpdate { get; set; }
    }
}
