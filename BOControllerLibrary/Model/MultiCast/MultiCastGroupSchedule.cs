﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace BOControllerLibrary.Model.MultiCast
{
    public class MultiCastGroupSchedule
    {
        public int ID { get; set; }
        public int MultiCastGroupID { get; set; }
        public string EventName { get; set; }
        public bool RecurrentFlag { get; set; }
        public int? RecurrentEventID { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        [XmlIgnore]
        public TimeSpan StartTime { get; set; }
        [XmlIgnore]
        public TimeSpan EndTime { get; set; }
        public string Description { get; set; }
        public int SourceID { get; set; }
        [Browsable(false)]
        [XmlElement(DataType = "duration", ElementName = "StartTime")]
        public string StartTimeString
        {
            get { return XmlConvert.ToString(this.StartTime); }
            set { this.StartTime = string.IsNullOrEmpty(value) ? TimeSpan.Zero : XmlConvert.ToTimeSpan(value); }
        }
        [Browsable(false)]
        [XmlElement(DataType = "duration", ElementName = "EndTime")]
        public string EndTimeString
        {
            get { return XmlConvert.ToString(this.EndTime); }
            set { this.EndTime = string.IsNullOrEmpty(value) ? TimeSpan.Zero : XmlConvert.ToTimeSpan(value); }
        }

    }
}
