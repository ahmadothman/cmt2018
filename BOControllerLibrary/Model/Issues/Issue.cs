﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BOControllerLibrary.Model.Issues
{
    /// <summary>
    /// This class represents an issue as it is stored within the CMT.
    /// An issue has a key which is a unique identifier in the Jira database,
    /// which is where the issues are stored and managed.
    /// </summary>
    public class Issue
    {
        /// <summary> 
        /// An issue has a unique key, which is determined by Jira
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// The ID of the distributor that submitted the issue
        /// </summary>
        public int DistributorId { get; set; }

        /// <summary>
        /// The summary of the issue, as entered by the user
        /// </summary>
        public string Summary { get; set; }

        /// <summary>
        /// The description of the issue, as entered by the user
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// The time and date the issue was submitted
        /// </summary>
        public DateTime Submitted { get; set; }

        /// <summary>
        /// The status of the issue in Jira
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// The time and date of the last update on the issue
        /// </summary>
        public DateTime LastUpdate { get; set; }

        /// <summary>
        /// An optional MAC address concerned by the issue
        /// </summary>
        public string MACAddress { get; set; }

        /// <summary>
        /// The type of issue
        /// </summary>
        public string IssueType { get; set; }
    }
}
