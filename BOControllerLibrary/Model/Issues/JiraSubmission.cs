﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BOControllerLibrary.Model.Issues
{
    /// <summary>
    /// This class contains the fields necessary for submitting a new issue to Jira
    /// Jira expects a JSON call and this class can provide that call with the correct structure
    /// </summary>
    public class JiraSubmission
    {
        /// <summary>
        /// all the data are located in the fields container
        /// </summary>
        public JiraSubmissionFields fields = new JiraSubmissionFields();
    }

    /// <summary>
    /// these fields contain the actual data submitted to Jira
    /// </summary>
    public class JiraSubmissionFields
    {
        /// <summary>
        /// The Jira project the issue belongs to
        /// The projected will be the same for all issues submitted by the CMT
        /// </summary>
        public JiraSubmissionProject project = new JiraSubmissionProject();

        /// <summary>
        /// The summary of the issue, as provided by the reporter
        /// </summary>
        public string summary { get; set; }

        /// <summary>
        /// The description of the issue, as provided by the reporter
        /// </summary>
        public string description { get; set; }

        /// <summary>
        /// The type of issue submitted.
        /// For the CMT this type is currently defaulted to Bug
        /// </summary>
        public JiraSubmissionIssueType issuetype = new JiraSubmissionIssueType();

        /// <summary>
        /// A custom field for the distributor ID
        /// </summary>
        //public double customfield_10004 { get; set; }

        /// <summary>
        /// A custom field for the distributor name
        /// </summary>
        //public string customfield_10001 { get; set; }

        /// <summary>
        /// A custom field for a MAC address
        /// </summary>
        //public string customfield_10002 { get; set; }

        /// <summary>
        /// A custom field for the submitter
        /// </summary>
        //public string customfield_10003 { get; set; }
    }

    /// <summary>
    /// The Jira project the issue belongs to
    /// The secondary level is necessary for the structure of the API call
    /// </summary>
    public class JiraSubmissionProject
    {
        /// <summary>
        /// The project is identified by a unique key, as set in Jira
        /// </summary>
        public string key { get; set; }
    }

    /// <summary>
    /// The type of issue submitted
    /// The secondary level is necessary for the structure of the API call
    /// </summary>
    public class JiraSubmissionIssueType
    {
        /// <summary>
        /// The actual issue type
        /// Available options in Jira include
        /// Bug, New Feature, Task and Improvement
        /// </summary>
        public string name { get; set; }
    }
}
