﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BOControllerLibrary.Model.Issues
{
    /// <summary>
    /// This class is used for capturing the response from Jira when submitting a new issue
    /// The structure is needed to correctly handle the JSON response
    /// </summary>
    public class JiraResponse
    {
        /// <summary>
        /// A unique issue id, as provided by Jira
        /// This id is not used in the CMT
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// A unique issue key, as provided by Jira
        /// This key is used as a unique identifier by the CMT
        /// </summary>
        public string key { get; set; }

        /// <summary>
        /// An API url to view the issue
        /// This url will no be used by the CMT
        /// </summary>
        public string self { get; set; }
    }
}
