﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BOControllerLibrary.Model.Issues
{
    /// <summary>
    /// This class represents a comment that is submitted from the CMT to Jira
    /// A distributor can submit a comment on a open issue which he has reported
    /// </summary>
    public class JiraComment
    {
        // The contents of the comment
        public string body { get; set; }
        //public Visibility visibility { get; set; }
    }

    //// The visibility of the comment within Jira
    //public class Visibility
    //{
    //    // The type of limitation to the visibility, usually "role"
    //    public string type { get; set; }
    //    // The value of the limitation, usually the name of a role, eg "administrators"
    //    public string value { get; set; }
    //}
}
