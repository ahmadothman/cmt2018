﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BOControllerLibrary.Model.Issues
{
    /// <summary>
    /// This class represents an issue as it is stored Jira.
    /// The class is used for retreiving an issue from Jira and presenting it in the CMT
    /// Jira returns an issue in the JSON format and this class is needed for capturing
    /// all the fields and make the relevant fields readable.
    /// Most fields and classes will not be shown in the CMT, but are needed to capture
    /// a complete issue.
    /// All variable must be lower case in order for the API statements to work
    /// </summary>
    public class JiraIssue
    {
        /// <summary>
        /// The Expand string is required at the start of the JSON statement
        /// </summary>
        public string expand { get; set; }
        
        /// <summary>
        /// Jira provides each issue with a unique id. The CMT will not make use of this id
        /// </summary>
        public string id { get; set; }
        
        /// <summary>
        /// Jira provides each issue with an API URL
        /// </summary>
        public string self { get; set; }
        
        /// <summary>
        /// Jira also provides each issue with a unique key. 
        /// This is what the CMT uses as unique identifier.
        /// </summary>
        public string key { get; set; }
       
        /// <summary>
        /// Fields is where the main data of the tickt is located
        /// </summary>
        public JiraIssueFields fields = new JiraIssueFields();
    }

    /// <summary>
    /// Class provided by Jira
    /// Usage not yet clear
    /// </summary>
    public class JiraIssueProgress
    {
        public int progress { get; set; }
        public int total { get; set; }
    }

    /// <summary>
    /// Class provided by Jira
    /// Usage in CMT not yet clear
    /// </summary>
    public class JiraIssueTimetracking
    {
    }

    /// <summary>
    /// Class provided by Jira
    /// </summary>
    public class JiraIssueIssuetype
    {
        public string self { get; set; }
        public string id { get; set; }
        public string description { get; set; }
        public string iconUrl { get; set; }
        
        /// <summary>
        /// The name of the issue type.
        /// The CMT will use Bug.
        /// Other options include New Feature and Task
        /// </summary>
        public string name { get; set; }
        
        public bool subtask { get; set; }
    }


    /// <summary>
    /// Class provided by Jira
    /// Usage in CMT not yet clear
    /// </summary>
    public class JiraIssueVotes
    {
        public string self { get; set; }
        public int votes { get; set; }
        public bool hasVoted { get; set; }
    }

    /// <summary>
    /// Class provided by Jira
    /// Provides the information on the type of resolution
    /// </summary>
    public class JiraIssueResolution
    {
        public string self { get; set; }
        public string id { get; set; }
        public string description { get; set; }
        // the actual name of the resolution
        public string name { get; set; }
    }

    /// <summary>
    /// Class provided by Jira
    /// Usage in CMT not yet clear
    /// </summary>
    public class JiraIssueAvatarUrls
    {
        public string _16x16 { get; set; }
        public string _24x24 { get; set; }
        public string _32x32 { get; set; }
        public string _48x48 { get; set; }
    }

    /// <summary>
    /// Class provided by Jira
    /// The person that reported the issue
    /// To Jira, issues from the CMT are always reported by the Jira user CMT
    /// The actual reporter is stored within the CMT.
    /// The information in this class will be irrelevant to the CMT
    /// </summary>
    public class JiraIssueReporter
    {
        public string self { get; set; }
        public string name { get; set; }
        public string emailAddress { get; set; }
        public JiraIssueAvatarUrls avatarUrls = new JiraIssueAvatarUrls();
        public string displayName { get; set; }
        public bool active { get; set; }
    }

    /// <summary>
    /// Class provided by Jira
    /// The priority the issue has in Jira
    /// </summary>
    public class JiraIssuePriority
    {
        public string self { get; set; }
        public string iconUrl { get; set; }

        /// <summary>
        /// The actual name of the priority
        /// Jira will set new issues to Major
        /// The user managing the issue in Jira can change this
        /// depending on the issue
        /// </summary>
        public string name { get; set; }

        public string id { get; set; }
    }

    /// <summary>
    /// Class provided by Jira
    /// Number of Jira users following the issue
    /// Not relevant for the CMT at this point
    /// </summary>
    public class JiraIssueWatches
    {
        public string self { get; set; }
        public int watchCount { get; set; }
        public bool isWatching { get; set; }
    }

    /// <summary>
    /// Class provided by Jira
    /// Usage in CMT not yet clear
    /// </summary>
    public class JiraIssueWorklog
    {
        public int startAt { get; set; }
        public int maxResults { get; set; }
        public int total { get; set; }
        public List<object> worklogs { get; set; }
    }

    /// <summary>
    /// Class provided by Jira
    /// The status of the issue in Jira
    /// </summary>
    public class JiraIssueStatus
    {
        public string self { get; set; }
        public string description { get; set; }
        public string iconUrl { get; set; }
        
        /// <summary>
        /// The actual status of the issue in Jira
        /// New issues get the status Open
        /// </summary>
        public string name { get; set; }

        public string id { get; set; }
    }

    /// <summary>
    /// Class provided by Jira
    /// The user who has been assigned the issue in Jira
    /// Usage in CMT not yet clear
    /// </summary>
    public class JiraIssueAssignee
    {
        public string self { get; set; }
        public string name { get; set; }
        public string emailAddress { get; set; }
        public JiraIssueAvatarUrls avatarUrls = new JiraIssueAvatarUrls();
        public string displayName { get; set; }
        public bool active { get; set; }
    }

    /// <summary>
    /// Class provided by Jira
    /// The name of the project in Jira the issue is part of
    /// All CMT issues will be assigned to the same project, which must still be named
    /// </summary>
    public class JiraIssueProject
    {
        public string self { get; set; }
        public string id { get; set; }
        public string key { get; set; }
        public string name { get; set; }
        public JiraIssueAvatarUrls avatarUrls { get; set; }
    }

    /// <summary>
    /// Class provided by Jira
    /// Usage in CMT not yet clear
    /// </summary>
    public class JiraIssueAggregateprogress
    {
        public int progress { get; set; }
        public int total { get; set; }
    }

    /// <summary>
    /// Class provided by Jira
    /// Used for handling attachments, which is currently out of scope for the CMT
    /// </summary>
    public class JiraIssueAttachment
    {
        public string self { get; set; }
        public string id { get; set; }
        public string filename { get; set; }
        public JiraIssueAuthor author { get; set; }
        public string created { get; set; }
        public int size { get; set; }
        public string mimeType { get; set; }
        public string content { get; set; }
        public string thumbnail { get; set; }
    }

    /// <summary>
    /// Class provided by Jira
    /// Jira user that wrote a comment
    /// Usage in CMT not yet clear
    /// </summary>
    public class JiraIssueAuthor
    {
        public string self { get; set; }
        public string name { get; set; }
        public string emailAddress { get; set; }
        public JiraIssueAvatarUrls avatarUrls { get; set; }
        public string displayName { get; set; }
        public bool active { get; set; }
    }

    /// <summary>
    /// Class provided by Jira
    /// Jira user that updated a comment
    /// Usage in CMT not yet clear
    /// </summary>
    public class JiraIssueUpdateAuthor
    {
        public string self { get; set; }
        public string name { get; set; }
        public string emailAddress { get; set; }
        public JiraIssueAvatarUrls avatarUrls { get; set; }
        public string displayName { get; set; }
        public bool active { get; set; }
    }

    /// <summary>
    /// Class provided by Jira
    /// Comments made on the issue in Jira
    /// </summary>
    public class JiraIssueComments
    {
        public string self { get; set; }
        public string id { get; set; }
        
        /// <summary>
        /// Jira user that wrote the comment
        /// </summary>
        public JiraIssueAuthor author = new JiraIssueAuthor();
        
        /// <summary>
        /// The actual content of the comment
        /// </summary>
        public string body { get; set; }

        /// <summary>
        /// Jira user that updated the comment
        /// </summary>
        public JiraIssueUpdateAuthor updateAuthor = new JiraIssueUpdateAuthor();
        
        /// <summary>
        /// Time and date the comment was made
        /// </summary>
        public DateTime created { get; set; }

        /// <summary>
        /// Time and date the comment was updated
        /// </summary>
        public DateTime updated { get; set; }
    }

    /// <summary>
    /// Class provided by Jira
    /// The list of comments made on the issue in Jira
    /// </summary>
    public class JiraIssueComment
    {
        public int startAt { get; set; }
        public int maxResults { get; set; }
        public int total { get; set; }

        /// <summary>
        /// The acutal list of comments
        /// </summary>
        public List<JiraIssueComments> comments { get; set; }
    }

    /// <summary>
    /// Class provided by Jira
    /// The fields containing the bulk of the issue data
    /// Relevant fields are commented on here as well as 
    /// in their respective classes above.
    /// </summary>
    public class JiraIssueFields
    {
        /// <summary>
        /// A summary of the contents of the issue, as provided by the reporter
        /// </summary>
        public string summary { get; set; }

        public JiraIssueProgress progress = new JiraIssueProgress();
        public JiraIssueTimetracking timetracking = new JiraIssueTimetracking();

        /// <summary>
        /// The type of issue in Jira
        /// </summary>
        public JiraIssueIssuetype issuetype = new JiraIssueIssuetype();
        public JiraIssueVotes votes = new JiraIssueVotes();
        public JiraIssueResolution resolution = new JiraIssueResolution();
        public List<object> fixVersions { get; set; }
        public object resolutiondate { get; set; }
        public object timespent { get; set; }

        /// <summary>
        /// The Jira user that reported the issue
        /// </summary>
        public JiraIssueReporter reporter = new JiraIssueReporter();
        public object aggregatetimeoriginalestimate { get; set; }
        
        /// <summary>
        /// Time and date the issue was created
        /// </summary>
        public DateTime created { get; set; }
        
        /// <summary>
        ///  Time and date the issue was last updated
        /// </summary>
        public DateTime updated { get; set; }
        
        /// <summary>
        /// Text describing the issue, as provided by the reporter
        /// </summary>
        public string description { get; set; }
        
        public JiraIssuePriority priority = new JiraIssuePriority();
        public object duedate { get; set; }
        
        /// <summary>
        /// custom field containing the distributor name
        /// </summary>
        public string customfield_10001 { get; set; }
        /// <summary>
        /// custom field containing a MAC address
        /// </summary>
        public string customfield_10002 { get; set; }
        /// <summary>
        /// custom field containing the submitter
        /// </summary>
        public string customfield_10003 { get; set; }
        
        public List<object> issuelinks { get; set; }
        public JiraIssueWatches watches = new JiraIssueWatches();
        public JiraIssueWorklog worklog = new JiraIssueWorklog();
        public object customfield_10010 { get; set; }
        public List<object> subtasks { get; set; }
        
        /// <summary>
        /// The status of the issue in Jira
        /// </summary>
        public JiraIssueStatus status = new JiraIssueStatus();
        
        public object customfield_10007 { get; set; }
        public object customfield_10006 { get; set; }
        public List<object> labels { get; set; }
        public int workratio { get; set; }
        
        /// <summary>
        /// The Jira user the issue is assigned to
        /// </summary>
        public JiraIssueAssignee assignee = new JiraIssueAssignee();

        /// <summary>
        /// A list of attachments belonging to the issue
        /// </summary>
        public List<JiraIssueAttachment> attachment = new List<JiraIssueAttachment>();
       
        public object aggregatetimeestimate { get; set; }
        
        /// <summary>
        /// The Jira project the issue is part of
        /// </summary>
        public JiraIssueProject project = new JiraIssueProject();
        
        public List<object> versions { get; set; }
        public object environment { get; set; }
        public object timeestimate { get; set; }
        public JiraIssueAggregateprogress aggregateprogress = new JiraIssueAggregateprogress();
        public string lastViewed { get; set; }
        public List<object> components { get; set; }
        
        /// <summary>
        /// Comments made on the issue in Jira
        /// </summary>
        public JiraIssueComment comment = new JiraIssueComment();
        
        /// <summary>
        /// A custom field for the distributor ID
        /// </summary>
        public double customfield_10004 { get; set; }
        
        public object timeoriginalestimate { get; set; }
        public object aggregatetimespent { get; set; }
    }
}
