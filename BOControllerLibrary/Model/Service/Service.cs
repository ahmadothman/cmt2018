﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BOControllerLibrary.Model.Service
{
    /// <summary>
    /// This class represents a user service.
    /// </summary>
    public class Service
    {
        /// <summary>
        /// A unique service ID
        /// </summary>
        public Guid ServiceId { get; set; }

        /// <summary>
        /// The name of the service
        /// </summary>
        public string ServiceName { get; set; }

        /// <summary>
        /// A description of the service
        /// </summary>
        public string ServiceDescription { get; set; }

        /// <summary>
        /// The service controller url
        /// </summary>
        public string ServiceController { get; set; }
        
        /// <summary>
        /// The level of the service
        /// </summary>
        public int Level { get; set; }

        /// <summary>
        /// The ID of the parent service
        /// </summary>
        public string ParentServiceId { get; set; }

        /// <summary>
        /// A deleted flag
        /// </summary>
        public bool DeletedFlag { get; set; }

        /// <summary>
        /// The icon image of the service
        /// </summary>
        public string Icon { get; set; }
    }
}
