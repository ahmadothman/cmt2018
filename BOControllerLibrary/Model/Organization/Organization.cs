﻿using BOControllerLibrary.Model.Distributor;

namespace BOControllerLibrary.Model.Organization
{
    public class Organization : IOrganization
    {
        #region Implementation of IOrganization

        public int? Id { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string WebSite { get; set; }
        public string Remarks { get; set; }
        public string Vat { get; set; }
        public Distributor.Distributor Distributor { get; set; }
        public Address Address { get; set; }
        public bool? VNO { get; set; }
        public int? MIR { get; set; }
        public int? CIR { get; set; }
        public int? DefaultSla { get; set; }
        public int Sector { get; set; }

        #endregion

    }
}
