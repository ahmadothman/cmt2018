﻿

namespace BOControllerLibrary.Model.Organization
{
    public interface IOrganization
    {
        int? Id { get; set; }
        string FullName { get; set; }
        string Phone { get; set; }
        string Fax { get; set; }
        string Email { get; set; }
        string WebSite { get; set; }
        string Remarks { get; set; }
        string Vat { get; set; }
        Distributor.Distributor Distributor { get; set; }
        Address Address { get; set; }
        bool? VNO { get; set; }
        int? MIR { get; set; }
        int? CIR { get; set; }
        int? DefaultSla { get; set; }
        int Sector { get; set; }
    }
}
