﻿using PetaPoco;

namespace BOControllerLibrary.Model.User
{
    public class EndUser : IEndUser
    {
        #region Implementation of IEndUser

        public int? Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public int? SitId { get; set; }
        public int? OrgId { get; set; }
        public string MobilePhone { get; set; }
        #endregion
    }
}
