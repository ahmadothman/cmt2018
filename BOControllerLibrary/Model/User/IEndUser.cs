﻿namespace BOControllerLibrary.Model.User
{
    public interface IEndUser
    {
        int? Id { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        string Email { get; set; }
        string Phone { get; set; }
        int? SitId { get; set; }
        int? OrgId { get; set; }
        string MobilePhone { get; set; } 
    }
}
