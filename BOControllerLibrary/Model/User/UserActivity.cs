﻿using System;

namespace BOControllerLibrary.Model.User
{
    public class UserActivity //: IActivity<UserActivityType>
    {
        public int? Id { get; set; }
        public Guid UserId { get; set; }
        public DateTime ActionDate { get; set; }
        public string Action { get; set; }
    }
}
