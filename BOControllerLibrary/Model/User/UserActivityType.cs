﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BOControllerLibrary.Model.User
{
    public enum UserActivityType
    {
        Login,
        Logout,
        Expired,
        /// <summary>
        /// When custom you'll need to specify the custom activity type
        /// </summary>
        Custom
    }
}
