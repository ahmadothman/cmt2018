﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BOControllerLibrary.Model
{
    public class NoSuchSitIdException : Exception
    {
        public NoSuchSitIdException (string message) : base (message)
        {
        }

        public int? SitId { get; set; }
    }
}
