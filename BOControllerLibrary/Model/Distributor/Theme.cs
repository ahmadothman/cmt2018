﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOControllerLibrary.Model.Distributor
{
    /// <summary>
    /// Represents a theme of the WL CMT plugin
    /// </summary>
    public class Theme
    {
        /// <summary>
        /// Distributors Identifier
        /// </summary>
        public int DistId { get; set; }

        /// <summary>
        /// The name of the theme
        /// </summary>
        public String ThemeName { get; set; }

        /// <summary>
        /// Any introduction text provided by the distributor
        /// </summary>
        public String IntroText { get; set; }

        /// <summary>
        /// Holds the background color for the plug-in as a 32bit value
        /// </summary>
        public String BackgroundColor { get; set; }
    }
}
