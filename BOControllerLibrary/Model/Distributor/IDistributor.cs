﻿using System;
using BOControllerLibrary.Model.ISP;

namespace BOControllerLibrary.Model.Distributor
{
    public interface IDistributor
    {
        int? Id { get; set; }
        string FullName { get; set; }
        string Phone { get; set; }
        string Fax { get; set; }
        string Email { get; set; }
        string WebSite { get; set; }
        string Remarks { get; set; }
        string Vat { get; set; }
        Isp Isp { get; set; }
        Address Address { get; set; }
        string Currency { get; set; }
        bool VNO { get; set; }
        int? DefaultOrganization { get; set; }
        DateTime LastInvoiceDate { get; set; }
        int? InvoiceInterval { get; set; }
        bool BankCharges { get; set; }
        string InvoicingEmail { get; set; }
        bool Inactive { get; set; }
        bool BadDistributor { get; set; }
        int DistType { get; set; }
    }
}
