﻿using System;
using BOControllerLibrary.Model.ISP;
using System.ComponentModel;
using BOControllerLibrary.Model.ISP;

namespace BOControllerLibrary.Model.Distributor
{
    public class Distributor : IDistributor
    {
        #region Implementation of IDistributor

        private bool _bankCharges = true; // This is the default value in the database

        private bool _inactive = false; // Default value in DB

        private bool _badDistributor = false; // Default value in DB

        private int _distType = 1; // default value

        public int? Id { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string WebSite { get; set; }
        public string Remarks { get; set; }
        public string Vat { get; set; }
        public Isp Isp { get; set; }
        public Address Address { get; set; }
        public string Currency { get; set; }
        public bool VNO { get; set; }
        public int? DefaultOrganization { get; set; }
        public DateTime LastInvoiceDate { get; set; }
        public int? InvoiceInterval { get; set; }
        public string InvoicingEmail { get; set; }
        

        [DefaultValue(true)]
        public bool BankCharges
        {
            get { return _bankCharges; }
            set { _bankCharges = value; }
        }


        [DefaultValue(false)]
        public bool Inactive 
        { 
            get{return _inactive;}
            set { _inactive = value; }
        }

        [DefaultValue(false)]
        public bool BadDistributor 
        {
            get { return _badDistributor ; }
            set { _badDistributor = value; } 
        }

        [DefaultValue(1)]
        public int DistType
        {
            get { return _distType; }
            set { _distType = value; }
        }
        #endregion
    }
}
