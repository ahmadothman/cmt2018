﻿namespace BOControllerLibrary.Model.ISP
{
    public class Isp : IIsp
    {
        #region Implementation of IIsp

        public int? Id { get; set; }
        public string CompanyName { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Remarks { get; set; }
        public string Dashboard_User { get; set; }
        public string Dashboard_Password { get; set; }
        public string Ispif_User { get; set; }
        public string Ispif_Password { get; set; }
        public int ISPType { get; set; }
        public Address Address { get; set; }
        public Satellite Satellite { get; set; }
        #endregion
    }
}
