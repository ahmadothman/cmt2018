﻿namespace BOControllerLibrary.Model.ISP
{
    public interface IIsp
    {
        int? Id { get; set; }
        string CompanyName { get; set; }
        string Phone { get; set; }
        string Fax { get; set; }
        string Email { get; set; }
        string Remarks { get; set; }
        string Dashboard_User { get; set; }
        string Dashboard_Password { get; set; }
        string Ispif_User { get; set; }
        string Ispif_Password { get; set; }
        int ISPType { get; set; }
        Address Address { get; set; }
        Satellite Satellite { get; set; }
    }
}
