﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BOControllerLibrary.Model.VNO
{
    /// <summary>
    /// This class represents VNO-specific properties of a VNO object
    /// A VNO object is an extension of a Distributor object
    /// The distributor properties of the VNO are stored in a distributor object, so this class
    /// only contains properties that are additional to the distributor object.
    /// The only exception is the ID, which is the same for the distributor and the VNO object.
    /// </summary>
    public class VNO
    {
        public int Id { get; set; }
    }
}
