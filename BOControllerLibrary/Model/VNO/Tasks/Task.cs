﻿using System;

namespace BOControllerLibrary.Model.Tasks
{
    public class Task
    {
        public int? Id { get; set; }
        public string Subject { get; set; }
        public Guid UserId { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateDue { get; set; }
        public DateTime? DateCompleted { get; set; }
        public string MacAddress { get; set; }
        public TaskType Type { get; set; }
        public int DistributorId { get; set; }
        public bool Completed { get; set; }
    }
}
