﻿namespace BOControllerLibrary.Model.Tasks
{
    public class TaskType
    {
        public int? Id { get; set; }
        public string Type { get; set; }
        public bool DistInfo { get; set; }

        public override string ToString()
        {
            return string.Format("TaskType: " + string.Join(";", Id, Type, DistInfo));
        }
    }
}
