﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BOControllerLibrary.Model
{
    public interface IActivity<T>
    {
        /// <summary>
        /// When ActivityType is 'custom', this value will be used. Otherwise this is ignored
        /// </summary>
        string CustomActivity { get; set; }

        /// <summary>
        /// The event time the activity took place. For a new event, its best to not specify this. The event time is then saved to the current time in the database.
        /// </summary>
        DateTime? EventTime { get; set; }

        /// <summary>
        /// The activity type we're dealing with. When choosing custom, you must specify the customActivity field. Otherwise you will get an exception.
        /// </summary>
        T ActivityType { get; set; }
    }
}
