﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BOControllerLibrary.Model.Log
{
    /// <summary>
    /// Adds information to the alarm.
    /// </summary>
    public enum AlarmInfoEnum
    {
        /// <summary>
        /// New alarm
        /// </summary>
        New,
        /// <summary>
        /// Overwrites a previous alarm
        /// </summary>
        Overwrite,
        /// <summary>
        /// Clears a previous alarm
        /// </summary>
        Clear,
        /// <summary>
        /// Not applicable in the message context or not supported
        /// </summary>
        NotApplicable
    }
}
