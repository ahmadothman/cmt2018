﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BOControllerLibrary.Model.Log
{
    /// <summary>
    /// This class represents a ticket as stored in the CMT database
    /// Tickets are used for communicating with other systems, such as the SES hub and the NMS
    /// The format used here is from the CMT database
    /// </summary>
    public class CMTTicket
    {
        public string TicketId { get; set; }
        public DateTime CreationDate { get; set; }
        public string MacAddress { get; set; }
        public int SourceAdmState { get; set; }
        public int TargetAdmState { get; set; }
        public int TicketStatus { get; set; }
        public string FailureReason { get; set; }
        public string FailureStackTrace { get; set; }
        public int TermActivity { get; set; }
        public int newSla { get; set; }
        public int IspId { get; set; }
        public string NewMacAddress { get; set; }
        public Int16 NMSActivate { get; set; }
    }
}
