﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BOControllerLibrary.Model.Log
{
    /// <summary>
    /// Enumerates the possible error levels to be used by the 
    /// ApplicationExceptionLog function
    /// </summary>
    public enum ExceptionLevelEnum
    {
        Debug, Info, Warning, Error, Critical
    }
}
