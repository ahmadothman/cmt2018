﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BOControllerLibrary.Model.Log
{
    /// <summary>
    /// Corresponds with the message types specified in the introduction of this chapter: 
    /// Debug, Info, Warning, Error and Critical. 
    /// </summary>
    public enum AlarmLevelEnum
    {
        /// <summary>
        /// Alarm message used for debug purposes
        /// </summary>
        Debug,
        /// <summary>
        /// Pure informational alarm message
        /// </summary>
        Info,
        /// <summary>
        /// Warning about a possible problem
        /// </summary>
        Warning,
        /// <summary>
        /// Non critical error which must be fixed
        /// </summary>
        Error,
        /// <summary>
        /// Show stopper error which must be fixed
        /// </summary>
        Critical,
        /// <summary>
        /// NotApplicable, use this value if the message is not about an alarm
        /// </summary>
        NotApplicable,

        /// <summary>
        /// Emergency message
        /// </summary>
        Emergency,

        /// <summary>
        /// Alert message
        /// </summary>
        Alert,

        /// <summary>
        /// Notice message
        /// </summary>
        Notice,

    }

}
