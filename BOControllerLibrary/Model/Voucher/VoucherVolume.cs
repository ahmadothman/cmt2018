﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BOControllerLibrary.Model.Voucher
{
    /// <summary>
    /// Voucher volume entity. The list of voucher volumes serves mainly
    /// as a lookup table when creating new vouchers
    /// </summary>
    public class VoucherVolume
    {
        /// <summary>
        /// Unique Id for this Voucher volume
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The volume in MB which needs to be added to the subscription
        /// </summary>
        public int VolumeMB { get; set; }

        /// <summary>
        /// The unit price in the currency set by the currency field
        /// This is no longer used as vouchers now have two prices
        /// Keep for compatibility issues
        /// </summary>
        public double UnitPrice { get; set; }

        /// <summary>
        /// A short description of the voucher volume
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Currency code according to ISO 4217. For instance EUR for Euro and USD for
        /// US dollar
        /// This is no longer used as vouchers now have two prices
        /// Keep for compatibility issues
        /// </summary>
        public string CurrencyCode { get; set; }

        /// <summary>
        /// The code needed when calling the Addvolume ISPIF method
        /// </summary>
        public int VolumeId { get; set; }

        /// <summary>
        /// The ISP 
        /// </summary>
        public int Sla { get; set; }

        /// <summary>
        /// The period of validity of the voucher
        /// expressed in days
        /// </summary>
        public int ValidityPeriod { get; set; }

        /// <summary>
        /// The unit price in Euros
        /// This will replace the old unit price and currency code
        /// </summary>
        public double UnitPriceEUR { get; set; }

        /// <summary>
        /// The unit price in US dollars
        /// This will replace the old unit price and currency code
        /// </summary>
        public double UnitPriceUSD { get; set; }

        /// <summary>
        /// Indicates if this voucher type still exists in the CMT or not
        /// </summary>
        public bool Deleted { get; set; }
    }
}
