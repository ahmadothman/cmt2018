﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BOControllerLibrary.Model.Voucher
{
    public class VoucherRequest
    {
        /// <summary>
        /// The ID of the request
        /// </summary>
        public int RequestId { get; set; }

        /// <summary>
        /// The ID of the distributor that made the request
        /// </summary>
        public int DistributorId { get; set; }

        /// <summary>
        /// The voucher volume requested
        /// </summary>
        public int VoucherVolumeId { get; set; }

        /// <summary>
        /// The number of vouchers requested
        /// </summary>
        public int NumVouchers { get; set; }

        /// <summary>
        /// The state of the request, 0 = pending
        /// 1 = batch created, 2 = request cancelled
        /// </summary>
        public int RequestState { get; set; }

        // The Voucher request Sla
        public int Sla { get; set; }

    }
}
