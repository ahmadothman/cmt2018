﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BOControllerLibrary.Model.Voucher
{
    /// <summary>
    /// The VoucherBatchInfo class extends a VoucherBatch entity with information
    /// about the creator of the Voucher batch and the available vouchers belonging
    /// to a voucher batch.
    /// </summary>
    public class VoucherBatchInfo : VoucherBatch
    {
        /// <summary>
        /// The user name of the NOCSA who created the voucher batch
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// The available vouchers left in the batch
        /// </summary>
        public int AvailableVouchers { get; set; }

        /// <summary>
        /// Batch payment method
        /// </summary>
        public string PaymentMethod { get; set; }
    }
}
