﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BOControllerLibrary.Model.Voucher
{
    /// <summary>
    /// This class represents a Voucher. A voucher has a 13 digit
    /// voucher code and a 16 bit CRC code modelled as a ushort.
    /// the 13 digit number is represented by a character string and the
    /// CRC is calculated for the 13 character bytes which make up the 
    /// voucher code.
    /// </summary>
    public class Voucher
    {
        /// <summary>
        /// The Voucher code as a 13 character string
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// The 16 bit CRC calculated over the 13 characters. 
        /// </summary>
        public ushort Crc { get; set; }

        /// <summary>
        /// This is the seed which is used to calculate the 16 bit CRC
        /// </summary>
        public ushort CrcSeed { get; set; }

        /// <summary>
        /// Indicates if the voucher is available or used
        /// </summary>
        public bool Available { get; set; }

        /// <summary>
        /// The batch id created for this voucher
        /// </summary>
        public int BatchId { get; set; }

        /// <summary>
        /// Equals override
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(Object obj)
        {
            if (obj is Voucher)
            {
                Voucher v = (Voucher) obj;
                if (!v.Code.Equals(this.Code))
                {
                    return false;
                }
                else if (v.Crc != this.Crc)
                {
                    return false;
                }
                else if (v.Available != this.Available)
                {
                    return false;
                }
                else if (v.CrcSeed != this.CrcSeed)
                {
                    return false;
                }
                else if (v.BatchId != this.BatchId)
                {
                    return false;
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// GetHashCode override
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// The date the voucher was validated
        /// </summary>
        public DateTime DateValidated { get; set; }

        /// <summary>
        /// The MAC address of the terminal for which the voucher was validated
        /// </summary>
        public string MacAddress { get; set; }
    }
}
