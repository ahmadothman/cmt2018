﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace BOControllerLibrary.Model.Voucher
{
    /// <summary>
    /// This class respresents a Voucher batch
    /// </summary>
    public class VoucherBatch
    {
        private bool _paid = false; // This is the default value in the database

        /// <summary>
        /// A unique batch id, this value is used as an indentity column in the 
        /// VoucherBatches table
        /// </summary>
        public int BatchId { get; set; }

        /// <summary>
        /// The identifier of the distributor who owns this batch of vouchers
        /// </summary>
        public int DistributorId { get; set; }

        /// <summary>
        /// The date the batch was created
        /// </summary>
        public DateTime DateCreated { get; set; }

        /// <summary>
        /// The identifier of the user who created this batch
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        /// Volume added to the subscription in MB
        /// </summary>
        public int Volume { get; set; }

        /// <summary>
        /// Number of vouchers generated for this batch
        /// </summary>
        public int NumVouchers { get; set; }

        /// <summary>
        /// The release flag determines if a voucher batch is available
        /// to Distributors for distribution to their customers. A Voucher
        /// batch is released if the distributor has paid his voucher invoice.
        /// </summary>
        public Boolean Release { get; set; }


        /// <summary>
        /// Number of vouchers generated for this batch
        /// </summary>
        public int Sla { get; set; }

        /// <summary>
        /// The paid flag determines whether the voucher batch has been paid
        /// by the distributor. It's value is true when the batch has been paid.
        /// Its default value is false.
        /// </summary>
        [DefaultValue(false)]
        public bool Paid
        {
            get { return _paid; }
            set { _paid = value; }
        }
    }
}
