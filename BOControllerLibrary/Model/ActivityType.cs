﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BOControllerLibrary.Model
{
    public class ActivityType
    {
        public int? Id { get; set; }
        public string Action { get; set; }
        public string Description { get; set; }
        public bool? Billable { get; set; }
        public Decimal? UnitPrice { get; set; }
        public int? BillableId { get; set; }
    }
}
