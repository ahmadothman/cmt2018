﻿namespace BOControllerLibrary.Model
{
    public class Address : IAddress
    {
        public int Id { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string Location { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
    }
}
