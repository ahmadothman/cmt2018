﻿namespace BOControllerLibrary.Model
{
    public interface ICountry
    {
        string CountryCode { get; set; }
        string CountryName { get; set; }
    }
}
