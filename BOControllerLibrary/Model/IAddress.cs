﻿namespace BOControllerLibrary.Model
{
    public interface IAddress
    {
        int Id { get; set; }
        string AddressLine1 { get; set; }
        string AddressLine2 { get; set; }
        string Location { get; set; }
        string PostalCode { get; set; }
        string Country { get; set; }
    }
}
