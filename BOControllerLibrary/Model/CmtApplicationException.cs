﻿using System;
using PetaPoco;
using BOControllerLibrary.Model.Log;

namespace BOControllerLibrary.Model
{
    public class CmtApplicationException
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        public CmtApplicationException()
        {
        }

        /// <summary>
        /// Constructor which allows to set some basic parameters.
        /// </summary>
        /// <remarks>
        /// When using this constructor the ExceptionLevel is always set to Info
        /// </remarks>
        /// <param name="ex">The Exception</param>
        /// <param name="userDescription">A user description</param>
        /// <param name="stateInformation">Some state information</param>
        public CmtApplicationException(Exception ex, string userDescription, string stateInformation)
        {
            ExceptionDateTime = DateTime.UtcNow;
            ExceptionDesc = ex.Message;
            ExceptionStacktrace = ex.StackTrace;
            StateInformation = stateInformation;
            UserDescription = userDescription;
            ExceptionLevel = (short)ExceptionLevelEnum.Info;
        }

        /// <summary>
        /// Constructor which allows to set the exception parameters including the exception level
        /// </summary>
        /// <remarks>
        /// If the exception level is set to null the level becomes Info
        /// </remarks>
        /// <param name="ex">The Exception</param>
        /// <param name="userDescription">A user description</param>
        /// <param name="stateInformation">Some state information</param>
        /// <param name="exceptionLevel">The exception level</param>
        public CmtApplicationException(Exception ex, string userDescription, string stateInformation, short? exceptionLevel)
        {
            ExceptionDateTime = DateTime.UtcNow;
            ExceptionDesc = ex.Message;
            ExceptionStacktrace = ex.StackTrace;
            StateInformation = stateInformation;
            UserDescription = userDescription;
            if (exceptionLevel != null)
            {
                ExceptionLevel = (short)exceptionLevel;
            }
            else
            {
                ExceptionLevel = (short)ExceptionLevelEnum.Info;
            }
        }

        public int? Id { get; set; }

        /// <summary>
        ///   The exception time when it was logged in the database
        /// </summary>
        public DateTime ExceptionDateTime { get; set; }

        /// <summary>
        /// This is the actual Exception.Message
        /// </summary>
        public string ExceptionDesc { get; set; }

        /// <summary>
        /// Optional: Exception level code
        /// </summary>
        public Int16? ExceptionLevel { get; set; }

        /// <summary>
        /// Optional: The exception stack trace, useful for debugging
        /// </summary>
        public string ExceptionStacktrace { get; set; }

        /// <summary>
        /// Optional: A user description of what happend at the time of the exception
        /// </summary>
        public string UserDescription { get; set; }

        /// <summary>
        /// Optional: Some state info you are free to provide: usually this is the input parameter fields for the method the exception was thrown. This is particularly useful to reproduce problems easily.
        /// And being able to unit test them.
        /// </summary>
        public string StateInformation { get; set; }
    }
}
