﻿using BOControllerLibrary.Model.Invoicing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WCFInvoiceControllerService.Util
{
    /// <summary>
    /// Represents an account selected by means of the
    /// GetBillableByDescription method. This is a mere transport object 
    /// used for a specific purpose
    /// </summary>
    public class SelectedAccount
    {
        /// <summary>
        /// The Account Identifier
        /// </summary>
        public int AccountId
        {
            get;
            set;
        }

        /// <summary>
        /// The account description
        /// </summary>
        public String AccountDescription
        {
            get;
            set;
        }

        /// <summary>
        /// The list of seleted Billables
        /// </summary>
        public List<Billable> SelectedBillables
        {
            get;
            set;
        }
    }
}