﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BOControllerLibrary.Model.Distributor;

namespace BOControllerLibrary.Model.Invoices
{
    /// <summary>
    /// Contains the properties of the invoice header
    /// </summary>
    public class InvoiceHeader
    {
        public BOControllerLibrary.Model.Distributor.Distributor Distributor { get; set; }

        /// <summary>
        ///  Month - Year - Sequence number
        /// </summary>
        public int InvoiceNumber { get; set; }

        public int InvoiceYear { get; set; }

        public int InvoiceMonth { get; set; }

        public DateTime InvoiceDate { get; set; }

        public float InvoiceTotalEUR { get; set; }

        public float InvoiceTotalUSD { get; set; }

    }
}
