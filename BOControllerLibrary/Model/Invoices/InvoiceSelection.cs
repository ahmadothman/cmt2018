﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BOControllerLibrary.Model.Invoices
{
    /// <summary>
    /// The InvoiceSelection class is a used by the Invoice option the Finance role to
    /// query for one or more invoices.
    /// </summary>
    public class InvoiceSelection
    {
        /// <summary>
        /// Default constructor which initializes the class properties
        /// </summary>
        public InvoiceSelection()
        {
            Month = -1;
            InvoiceNumber = -1;
            Year = -1;
            InvoiceStatus = "";
            DistributorVNOId = -1;
        }

        /// <summary>
        /// The month of the invoice
        /// </summary>
        public int Month { get; set; }

        /// <summary>
        /// The Invoice number
        /// </summary>
        public int InvoiceNumber { get; set; }

        /// <summary>
        /// The year of the invoice
        /// </summary>
        public int Year { get; set; }

        /// <summary>
        /// The status of the invoice
        /// </summary>
        public string InvoiceStatus { get; set; }

        /// <summary>
        /// The Distributor or VNO Identifier
        /// </summary>
        public int DistributorVNOId { get; set; }
    }
}
