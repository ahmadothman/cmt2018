﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOControllerLibrary.Model.Invoicing
{
    public class ExchangeRateHistory
    {
        public int ID { get; set; }
        public DateTime? ChangedDate { get; set; }
        public decimal? ExchangeRate { get; set; }
        public Guid? UserId { get; set; }
    }
}
