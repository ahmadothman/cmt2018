﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOControllerLibrary.Model.Invoicing
{
    public class InvoiceDetail
    {
        public int ID { get; set; }
        public DateTime? ActivityDate { get; set; }
        public int? Bid { get; set; }
        public string BillableDescription { get; set; }
        public int InvoiceId { get; set; }
        public int InvoiceNum { get; set; }
        public int InvoiceYear { get; set; }
        public int Items { get; set; }
        public string MacAddress { get; set; }
        public bool PaidInAdvance { get; set; }
        public string Serial { get; set; }
        public string ServicePack { get; set; }
        public string Status { get; set; }
        public string TerminalId { get; set; }
        public decimal UnitPrice { get; set; }
    }
}
