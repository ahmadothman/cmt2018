﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOControllerLibrary.Model.Invoicing
{
    public class Configuration
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
