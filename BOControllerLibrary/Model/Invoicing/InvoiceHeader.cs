﻿using BOControllerLibrary.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOControllerLibrary.Model.Invoicing
{
    public class InvoiceHeader
    {
        public int Id { get; set; }
        public int? ActiveTerminals { get; set; }
        public decimal? AdvanceAutomatic { get; set; }
        public decimal? AdvanceManual { get; set; }
        public bool BankCharges { get; set; }
        public DateTime CreationDate { get; set; }
        public string CurrencyCode { get; set; }
        public int DistributorId { get; set; }
        public string DistributorName { get; set; }
        public decimal? ExchangeRate { get; set; }
        public decimal? FinancialInterest { get; set; }
        public int InvoiceMonth { get; set; }
        public int InvoiceNum { get; set; }
        public int InvoiceState { get; set; }
        public int InvoiceYear { get; set; }
        public bool NoIncentive { get; set; }
        public string Remarks { get; set; }
        public decimal? VAT { get; set; }

        BOInvoicingController controller = new BOInvoicingController();
        private IList<InvoiceDetail> _invoiceDetails = new List<InvoiceDetail>();
        public virtual IList<InvoiceDetail> invoiceDetails
        {
            get
            {
                return controller.GetInvoiceLines(this.Id);
            }
        }

        //public BOControllerLibrary.Model.Distributor.Distributor Distributor { get; set; }

        ///// <summary>
        /////  Month - Year - Sequence number
        ///// </summary>
        //public int InvoiceNumber { get; set; }

        //public DateTime InvoiceDate { get; set; }

        //public float InvoiceTotalEUR { get; set; }

        //public float InvoiceTotalUSD { get; set; }

        /// <summary>
        /// Returns the Invoice ID
        /// It has the following format: 0MM-YYYY-XXX-[distributor name]
        /// </summary>
        public string InvoiceId
        {
            get
            {
                return string.Format("{0:000}-{1}-{2:0000}-{3}", this.InvoiceMonth, this.InvoiceYear, this.InvoiceNum, this.DistributorName);
            }
            set
            {
                // This set is only there because otherwise, there will be a serialization error
            }
        }

        private bool _noDueBankCharges;

        private decimal _totalAmount;
        /// <summary>
        /// The sum of all the invoiceDetail prices of the invoiceHeader
        /// </summary>
        public decimal TotalAmount
        {
            get
            {
                //if (this.invoiceDetails != null)
                //{
                //    return this.invoiceDetails.Sum(detail => detail.UnitPrice * detail.Items);
                //}

                //return 0;

                if (this.invoiceDetails != null && this.invoiceDetails.Count > 0)
                {
                    _totalAmount = this.invoiceDetails.Sum(detail => detail.UnitPrice); //* detail.Items);
                }

                return _totalAmount;

                //else
                //{
                //    if (!_totalAmount.HasValue)
                //    {
                //        _totalAmount = 0;
                //    }
                //}
                
                //return (decimal)_totalAmount;
            }
            set
            {
                //// This set is only there because otherwise, there will be a serialization error
                _totalAmount = value;
            }
        }

        /*
         * CMTINVOICE-38 - 2015-01-09
         * Implemented NoIncentive
         */
        /// <summary>
        /// The incentive of the invoiceHeader.
        /// A discount on the TotalAmount when the TotalAmount is a large number.
        /// This isn't the discount, but the percentage of the TotalAmount that should be subtracted from the TotalAmount.
        /// This discount is reflected in the GrandTotal.
        /// </summary>
        public decimal Incentive
        {
            get
            {
                if ((!this.NoIncentive) && (this.FinancialInterest.Value==0))
                {
                    if (string.IsNullOrEmpty(this.CurrencyCode) || this.CurrencyCode == "EUR")
                    {
                        if (this.TotalAmount <= 2850)
                        {
                            return 0;
                        }
                        else if (this.TotalAmount >= 2851 && this.TotalAmount <= 5000)
                        {
                            return (decimal)0.03;
                        }
                        else if (this.TotalAmount >= 5001 && this.TotalAmount <= 7140)
                        {
                            return (decimal)0.04;
                        }
                        else if (this.TotalAmount >= 7141 && this.TotalAmount <= 10715)
                        {
                            return (decimal)0.05;
                        }
                        else if (this.TotalAmount >= 10716 && this.TotalAmount <= 14285)
                        {
                            return (decimal)0.055;
                        }
                        else if (this.TotalAmount >= 14286 && this.TotalAmount <= 35715)
                        {
                            return (decimal)0.06;
                        }
                        else if (this.TotalAmount >= 35716)
                        {
                            return (decimal)0.07;
                        }
                    }
                    else if (this.CurrencyCode == "USD")
                    {
                        if (this.TotalAmount <= 3999)
                        {
                            return 0;
                        }
                        else if (this.TotalAmount >= 4000 && this.TotalAmount <= 6999)
                        {
                            return (decimal)0.03;
                        }
                        else if (this.TotalAmount >= 7000 && this.TotalAmount <= 9999)
                        {
                            return (decimal)0.04;
                        }
                        else if (this.TotalAmount >= 10000 && this.TotalAmount <= 14999)
                        {
                            return (decimal)0.05;
                        }
                        else if (this.TotalAmount >= 15000 && this.TotalAmount <= 19999)
                        {
                            return (decimal)0.055;
                        }
                        else if (this.TotalAmount >= 20000 && this.TotalAmount <= 49999)
                        {
                            return (decimal)0.06;
                        }
                        else if (this.TotalAmount >= 50000)
                        {
                            return (decimal)0.07;
                        }
                    } 
                }

                return 0;
            }
            set
            {
                // This set is only there because otherwise, there will be a serialization error
            }
        }

        /// <summary>
        /// The GrandTotal of the invoiceHeader.
        /// It's the calculation of the TotalAmount with the Incentive and VAT of the invoiceHeader.
        /// </summary>
        public decimal GrandTotal
        {
            get
            {
                decimal grandTotal = this.TotalAmount * (1 - this.Incentive);
                                              
                if (this.FinancialInterest.HasValue)
                {
                    grandTotal += this.FinancialInterest.Value;
                }

                if (BankCharges)
                {
                    decimal alreadypaid = 0;

                    if (this.AdvanceAutomatic.HasValue)
                    {
                        alreadypaid += this.AdvanceAutomatic.Value;
                    }
                    if (this.AdvanceManual.HasValue && this.AdvanceManual != null)
                    {
                        alreadypaid += this.AdvanceManual.Value;
                    }


                    if (grandTotal==alreadypaid)
                        _noDueBankCharges=true;
                        

                }

                grandTotal += this.BankChargesValue;
                InvoiceTransaction transaction = controller.GetTransaction(this.Id);
                grandTotal += transaction.Amount;
                return grandTotal;
            }
            set
            {
                // This set is only there because otherwise, there will be a serialization error
            }
        }

        /*
         * CMTINVOICE-31 - 2015-01-05
         * Implemented BankCharges
         */
        /// <summary>
        /// When the property BankCharges is true, this property returns the right bank charges amount for the used currency.
        /// Otherwise, it returns 0.
        /// </summary>
        public decimal BankChargesValue
        {
            get 
            {
                if (BankCharges && !_noDueBankCharges)
                {
                    if (string.IsNullOrEmpty(this.CurrencyCode) || this.CurrencyCode == "EUR")
                    {
                        return 15;
                    }
                    else if (this.CurrencyCode == "USD")
                    {
                        return 20;
                    }
                }

                return 0;
            }
            set
            {
                // This set is only there because otherwise, there will be a serialization error
            }
        }

        /*
         * CMTINVOICE-31 - 2015-01-05
         * Implemented BankCharges
         */
        /// <summary>
        /// The amount to be paid by the distributor.
        /// This is the sum of the GrandTotal, the FinancialInterest and Advance payments of the invoiceHeader.
        /// </summary>
        public decimal ToBePaid
        {
            get
            {
                decimal toBePaid = this.GrandTotal;

                if (this.VAT > 0 && this.VAT.HasValue)
                {
                    toBePaid += toBePaid * this.VAT.Value;
                }

                if (this.AdvanceAutomatic.HasValue)
                {
                    toBePaid -= this.AdvanceAutomatic.Value;
                }

                if (this.AdvanceManual.HasValue && this.AdvanceManual != null)
                {
                    toBePaid -= this.AdvanceManual.Value;
                }

                return toBePaid;
            }
            set
            {
                // This set is only there because otherwise, there will be a serialization error
            }
        }

        /*
         * CMTINVOICE-39 - 2015-01-09
         * Implemented ExchangeRate
         */
        /// <summary>
        /// When the exchange rate has been set, this is the ToBePaid amount in the other currency (EUR).
        /// Otherwise, its value is null.
        /// </summary>
        public decimal? ToBePaidExchanged 
        {
            get 
            {
                if (this.ExchangeRate > 0 && this.ExchangeRate != null)
                {
                    return this.ToBePaid * this.ExchangeRate; //.Value;
                }

                return null;
            }
            set
            {
                // This set is only there because otherwise, there will be a serialization error
            }
        }
    }
}
