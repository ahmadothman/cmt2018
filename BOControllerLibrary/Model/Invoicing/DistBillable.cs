﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOControllerLibrary.Model.Invoicing
{
    public class DistBillable
    {
        public int Id { get; set; }
        public int Bid { get; set; }
        public bool Deleted { get; set; }
        public int DistId { get; set; }
        public DateTime LastUpdate { get; set; }
        public decimal PriceEU { get; set; }
        public decimal PriceUSD { get; set; }
        public string Remarks { get; set; }
        public Guid UserId { get; set; }
    }
}
