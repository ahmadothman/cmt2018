﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOControllerLibrary.Controllers
{
    public class ErrorLog
    {
        public int Id { get; set; }
        public DateTime ExceptionDateTime { get; set; }
        public string ExceptionDesc { get; set; }
        public int? ExceptionLevel { get; set; }
        public string ExceptionStacktrace { get; set; }
        public string StateInformation { get; set; }
        public string UserDescription { get; set; }
    }
}
