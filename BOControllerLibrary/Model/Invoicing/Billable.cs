﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOControllerLibrary.Model.Invoicing
{
    public class Billable
    {
        public int Bid { get; set; }
        public int AccountId { get; set; }
        public bool Deleted { get; set; }
        public string Description { get; set; }
        public DateTime LastUpdate { get; set; }
        public decimal PriceEU { get; set; }
        public decimal PriceUSD { get; set; }
        public Guid UserId { get; set; }
        public DateTime? ValidFromDate { get; set; }
        public DateTime? ValidToDate { get; set; }
    }
}
