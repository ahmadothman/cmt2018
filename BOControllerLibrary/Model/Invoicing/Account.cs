﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOControllerLibrary.Model.Invoicing
{
    public class Account
    {
        public int AccountId { get; set; }
        public bool Deleted { get; set; }
        public string Description { get; set; }
        public DateTime LastUpdate { get; set; }
        public string Remarks { get; set; }
        public Guid UserId { get; set; }
    }
}
