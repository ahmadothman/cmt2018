﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BOControllerLibrary.Model.Terminal
{
    
    /// <summary>
    /// This is the CMT implementation of devices that can be connected to terminals
    /// such as a network device adding functionality to the local network
    /// A device must always be connected to a terminal
    /// Multiple devices can be connected to the same terminal
    /// </summary>
    public class ConnectedDevice
    {
        /// <summary>
        /// The unique ID of the device, ie a MAC address
        /// </summary>
        public string DeviceId { get; set; }

        /// <summary>
        /// The name of the device as provided by the user
        /// </summary>
        public string DeviceName { get; set; }
        
        /// <summary>
        /// The ID of the device type
        /// </summary>
        public string DeviceType { get; set; }

        /// <summary>
        /// The MAC address of the terminal the device is connected to
        /// </summary>
        public string macAddress { get; set; }
    }
}
