﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace BOControllerLibrary.Model.Terminal
{
    [XmlInclude(typeof(Coordinate))]
    public interface ICoordinate
    {
        double? Longitude { get; set; }
        double? Latitude { get; set; }
    }
}
