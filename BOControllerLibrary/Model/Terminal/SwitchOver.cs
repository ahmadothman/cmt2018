﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BOControllerLibrary.Model.Terminal
{
    /// <summary>
    /// This class describes switchovers which represent when the active terminal in a redundant
    /// terminal setup is changed (Satellite Diversity)
    /// </summary>
    public class SwitchOver
    {
        /// <summary>
        /// The time and date of the switchover
        /// </summary>
        public DateTime timeStamp { get; set; }

        /// <summary>
        /// The MAC address of the terminal from which the switch was done
        /// </summary>
        public string fromMac { get; set; }

        /// <summary>
        /// The MAC address of the terminal to which the switch was done
        /// </summary>
        public string toMac { get; set; }
    }
}
