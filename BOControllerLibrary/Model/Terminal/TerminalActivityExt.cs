﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BOControllerLibrary.Model.Terminal
{
    /// <summary>
    /// Extended version of the TerminalAcitivity class
    /// </summary>
    /// <remarks>
    /// Use this class if the UserName must be shown
    /// </remarks>
    public class TerminalActivityExt : TerminalActivity
    {
        /// <summary>
        /// The user name of the user who performed the terminal activity
        /// </summary>
        public string UserName { get; set; }
    }
}
