﻿using System;

namespace BOControllerLibrary.Model.Terminal
{
    public interface ITerminal
    {
        string MacAddress { get; set; }
        DateTime? StartDate { get; set; }
        DateTime? ExpiryDate { get; set; }
        int? EndUserId { get; set; }
        int? MonthlyConsumed { get; set; }
        int? MonthlyConsumedReturn { get; set; }
        int? SlaId { get; set; }
        int? AdmStatus { get; set; }
        //int? StatusId { get; set; }
        int SitId { get; set; }
        string FullName { get; set; }
        string Phone { get; set; }
        string Fax { get; set; }
        string Email { get; set; }
        string Remarks { get; set; }
        int? OrgId { get; set; }
        string IpAddress { get; set; }
        Coordinate LatLong { get; set; }
        DateTime? FirstActivationDate { get; set; }
        int? Blade { get; set; }
        string IPMgm { get; set; }
        string IPTunnel { get; set; }
        int? SpotID { get; set; }
        int? RTPool { get; set; }
        int? FWPool { get; set; }
        int IspId { get; set; }
        Address Address { get; set; }
        int? DistributorId { get; set; }
        string DistributorName { get; set; }
        string SlaName { get; set; }
        bool? TestMode { get; set; }
        decimal? CNo { get; set; }
        int IPMask { get; set; }
        bool? IPRange { get; set; }
        int? FreeZone { get; set; }
        int? TermWeight { get; set; }
        bool? FreeZoneFlag { get; set; }
        bool? StaticIPFlag { get; set; }
        int? EdgeSlaID { get; set; }
        int InvoiceInterval { get; set; }
        DateTime? LastInvoiceDate { get; set; }
        bool TerminalInvoicing { get; set; }

        //SatDiversity properties
        bool? RedundantSetup { get; set; }
        bool? PrimaryTerminal { get; set; }
        string AssociatedMacAddress { get; set; }
        string VirtualTerminal { get; set; }

        //CMTFO-9
        string Modem { get; set; }
        string iLNBBUC { get; set; }
        string Antenna { get; set; }

        //SATCORP-59
        string DialogTechnology { get; set; }
        Int64 CenterFrequency { get; set; }

        Int64 SymbolRate { get; set; }
    }
}

