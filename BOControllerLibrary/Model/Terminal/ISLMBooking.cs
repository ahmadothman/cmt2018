﻿using System;

namespace BOControllerLibrary.Model
{
    public interface ISLMBooking
    {
        int CMTId { get; set; }
        int DistributorId { get; set; }
        int CustomerId { get; set; }
        string TerminalMac { get; set; }
        DateTime StartTime { get; set; }
        DateTime? EndTime { get; set; }
        int BookingSlaId { get; set; }
        int TerminalSlaId { get; set; }
        string Type { get; set; }
        string Status { get; set; }
        int? SLMId { get; set; }
    }
}
