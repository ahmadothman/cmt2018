﻿using System;

namespace BOControllerLibrary.Model.Terminal
{
    public class TerminalActivity //: IActivity<TerminalActivityType>
    {
        public int? Id { get; set;  }
        /// <summary>
        /// The UUID
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        /// The MAC address of the terminal
        /// </summary>
        public string MacAddress { get; set; }

        /// <summary>
        /// The date of the terminal activity
        /// </summary>
        public DateTime ActionDate { get; set; }

        /// <summary>
        /// A description of the terminal activity
        /// </summary>
        public string Action { get; set; }

        /// <summary>
        /// The SLA name
        /// </summary>
        public string SlaName { get; set; }

        /// <summary>
        /// The new SLA Name in case of a SLA upgrade/downgrade. For any other
        /// activity this value is set to null
        /// </summary>
        public string NewSlaName { get; set; }

        /// <summary>
        /// The terminal activity
        /// </summary>
        public int TerminalActivityId { get; set; }

        /// <summary>
        /// Indicates if this activity needs to be included in the accounting report
        /// </summary>
        public Boolean AccountingFlag { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Boolean Prepaid { get; set; }
        
        /// <summary>
        /// Contains the comment line for this activity. The comment line provides additional
        /// information when compiling the invoices from the Accounting table
        /// </summary>
        public String Comment { get; set; }

    }
}
