﻿using System;

namespace BOControllerLibrary.Model.Terminal
{
    public class VolumeInformationTime : VolumeInformation
    {
        /// <summary>
        ///   The timestamp this volume was recorded
        /// </summary>
        public DateTime TimeStamp { get; set; }
    }
}
