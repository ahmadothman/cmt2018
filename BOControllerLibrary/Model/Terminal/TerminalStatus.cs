﻿namespace BOControllerLibrary.Model.Terminal
{
    public class TerminalStatus
    {
        public int? Id { get; set; }
        public string StatusDesc { get; set; }
    }
}
