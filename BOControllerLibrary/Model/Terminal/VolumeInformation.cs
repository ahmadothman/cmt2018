﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BOControllerLibrary.Model.Terminal
{
    public class VolumeInformation
    {
        public long Forward { get; set; }
        public long Return { get; set; }
    }
}
