﻿namespace BOControllerLibrary.Model
{
    public enum TerminalActivityType
    {
        /// <summary>
        ///   UC-M-201 Terminal activation
        /// </summary>
        TerminalActivated,

        /// <summary>
        ///   UC-M-202 Terminal decommissioning
        /// </summary>
        TerminalDecommissioned,

        /// <summary>
        ///   UC-M-203 Terminal suspension
        /// </summary>
        TerminalSuspended,

        /// <summary>
        ///   UC-M-204 Terminal subscription expiry update
        /// </summary>
        SubscriptionExpiredUpdate,

        /// <summary>
        ///   UC-M-205 Terminal search
        /// </summary>
        Search,

        /// <summary>
        ///   UC-M-206 Terminal Applications log
        /// </summary>
        ApplicationLogQuery,

        /// <summary>
        ///   UC-M-207 Test Terminal subscription prolongation by NOC Operator
        /// </summary>
        ProlongedSubscription,

        /// <summary>
        /// Specify the terminal activity type yourself
        /// </summary>
        Custom,

        /// <summary>
        /// IP Address Change
        /// </summary>
        IPAddressChange
    }
}
