﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BOControllerLibrary.Model.Terminal
{
    /// <summary>
    /// For more dynamic use of connected devices, this class
    /// enables the connected device types
    /// </summary>
    public class ConnectedDeviceType
    {
        /// <summary>
        /// The unique ID of the device type
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// The name of the device type as provided by the user
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// An optional description of the device type
        /// </summary>
        public string Description { get; set; }
    }
}
