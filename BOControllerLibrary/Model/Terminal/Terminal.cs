﻿using System;
using System.ComponentModel;

namespace BOControllerLibrary.Model.Terminal
{
    public class Terminal : ITerminal
    {
        private int _invoiceInterval = 1; // This is the default value in the database
        private bool _terminalInvoicing = false; // This is the default value in the database

        #region Implementation of ITerminal

        public string MacAddress { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public int? EndUserId { get; set; }
        public int? MonthlyConsumed { get; set; }
        public int? MonthlyConsumedReturn { get; set; }
        public int? SlaId { get; set; }
        public int? AdmStatus { get; set; }
        public int SitId { get; set; }
        public string FullName { get; set; }
        public string ExtFullName { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Remarks { get; set; }
        public int? OrgId { get; set; }
        public string IpAddress { get; set; }
        public Coordinate LatLong { get; set; }
        public DateTime? FirstActivationDate { get; set; }
        public int? Blade { get; set; }
        public string IPMgm { get; set; }
        public string IPTunnel { get; set; }
        public int? SpotID { get; set; }
        public int? RTPool { get; set; }
        public int? FWPool { get; set; }
        public int IspId { get; set; }
        public Address Address { get; set; }
        public string Serial { get; set; }
        public int? AddressType { get; set; }

        public int? DistributorId { get; set; }
        public string DistributorName { get; set; }
        public string SlaName { get; set; }
        public bool? TestMode { get; set; }
        public bool? DistributorAcceptance { get; set; }
        public bool? OrganizationAcceptance { get; set; }
        public DateTime? RequestDate { get; set; }
        public bool? LateBird { get; set; }
        public decimal? CNo { get; set; }
        // In DB IPMask is nullable int
        public int IPMask { get; set; }
        public bool? IPRange { get; set; }
        public int? FreeZone { get; set; }
        public int? TermWeight { get; set; }
        public bool? FreeZoneFlag { get; set; }
        public bool? StaticIPFlag { get; set; }
        public int? EdgeSlaID { get; set; }



        [DefaultValue(1)]
        public int InvoiceInterval
        {
            get { return _invoiceInterval; }
            set { _invoiceInterval = value; }
        }

        public DateTime? LastInvoiceDate { get; set; }

        [DefaultValue(false)]
        public bool TerminalInvoicing
        {
            get { return _terminalInvoicing; }
            set { _terminalInvoicing = value; }
        }

        //SatDiversity properties
        public bool? RedundantSetup { get; set; }
        public bool? PrimaryTerminal { get; set; }
        public string AssociatedMacAddress { get; set; }
        public string VirtualTerminal { get; set; }

        //CMTFO-9
        public string Modem { get; set; }
        public string iLNBBUC { get; set; }
        public string Antenna { get; set; }

        //SATCORP-59
        public string DialogTechnology { get; set; }
        public Int64 CenterFrequency { get; set; }

        public Int64 SymbolRate { get; set; }

        #endregion
    }
}
