﻿namespace BOControllerLibrary.Model.Terminal
{
    /// <summary>
    ///   This is the service pack in the CMT database
    /// </summary>
    public class ServiceLevel
    {
        public int? SlaId { get; set; }
        public string SlaName { get; set; }
        public decimal? FupThreshold { get; set; }
        public string DrAboveFup { get; set; }
        public string SlaCommonName { get; set; }
        public int? IspId { get; set; }
        public bool? FreeZone { get; set; }
        public bool? Business { get; set; }
        public bool? Voucher { get; set; }
        public decimal? RTNFUPThreshold { get; set; }
        public float ServicePackAmt { get; set; }
        public float FreeZoneAmt { get; set; }
        public float VoIPAmt { get; set; }
        public bool VoIPFlag { get; set; }
        public bool Hide { get; set; }
        public int CIRFWD { get; set; }
        public int CIRRTN { get; set; }
        public int DRFWD { get; set; }
        public int DRRTN { get; set; }
        public int VNOId { get; set; }
        public int ServiceClass { get; set; }
        public float ServicePackAmtEUR { get; set; }
        public float FreeZoneAmtEUR { get; set; }
        public float VoIPAmtEUR { get; set; }
        public decimal? HighVolumeFWD { get; set; }
        public decimal? HighVolumeRTN { get; set; }
        public decimal? LowVolumeFWD { get; set; }
        public decimal? LowVolumeRTN { get; set; }
        public int? MinBWFWD { get; set; }
        public int? MinBWRTN { get; set; }
        public decimal? HighVolumeSUM { get; set; }
        public decimal? LowVolumeSUM { get; set; }
        public bool AggregateFlag { get; set; }
        public double SLAWeight { get; set; }
        public int EdgeISP { get; set; }
        public int EdgeSLA { get; set; }
        public int EdgeSlaOverFup { get; set; }
        public int? BillableId { get; set; }
    }
}
