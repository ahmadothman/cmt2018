﻿using System;

namespace BOControllerLibrary.Model
{
    public class SLMBooking : ISLMBooking
    {
        public int CMTId { get; set; }
        public int DistributorId { get; set; }
        public int CustomerId { get; set; }
        public string TerminalMac { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public int BookingSlaId { get; set; }
        public int TerminalSlaId { get; set; }
        public string Type { get; set; }
        public string Status { get; set; }
        public int? SLMId { get; set; }
    }
}
