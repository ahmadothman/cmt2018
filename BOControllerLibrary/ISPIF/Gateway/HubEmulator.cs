﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Data.SqlClient;
using BOControllerLibrary.Controllers;
using BOControllerLibrary.Controllers.Interfaces;
using BOControllerLibrary.Model.Terminal;
using BOControllerLibrary.Model.Log;
using BOControllerLibrary.ISPIF.Model;
using BOControllerLibrary.Model;
using System.Web.Script.Serialization;
using RestSharp;
using RestSharp.Authenticators;
using System.Net;
using System.Net.Security;
using MySql.Data.MySqlClient;
using BOControllerLibrary.ISPIF.Gateway.Interface;
//NMS webservices
using nms = BOControllerLibrary.NMSGatewayServiceRef;
using BOControllerLibrary.Model.Organization;

namespace BOControllerLibrary.ISPIF.Gateway
{
    /// <summary>
    /// This controller provides access to NetworkInovation E21B Dialog M2M interface
    /// It is used for creating and managing terminals on the Dialog hub
    /// This is the NMS emulator gateway - no traffic directed to Local NMS
    /// </summary>
    public class HubEmulator : NMSGateway
    {
        private IBOConfigurationController _configurationController;
        private IBOAccountingController _accountingController;
        private IBOLogController _logController;
        private MySqlConnectionStringBuilder mtConnection;
        private string _connectionString;
        private string _databaseProvider;
        
        /// <summary>
        /// Default constructor
        /// </summary>
        public HubEmulator()
        {
            _connectionString = ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
            _databaseProvider = ConfigurationManager.ConnectionStrings["ApplicationServices"].ProviderName;
            _accountingController = new BOAccountingController(_connectionString, _databaseProvider);
            _configurationController = new BOConfigurationController(_connectionString, _databaseProvider);
            _logController = new BOLogController(_connectionString, _databaseProvider);
            mtConnection = new MySqlConnectionStringBuilder();
            mtConnection.Server = "";
            mtConnection.UserID = "";
            mtConnection.Password = "";
            mtConnection.Database = "";
            mtConnection.Port = 24306;
        }

        /// <summary>
        /// Creates a terminal in the Dialog Hub. 
        /// This method sets the NMSActivate flag which indicates that the 
        /// TicketMaster still needs to create the terminal in the NMS.
        /// </summary>
        /// <param name="endUserId">The end user identifier related to the terminal</param>
        /// <param name="macAddress">The terminal's MAC address</param>
        /// <param name="slaId">
        /// The Service Level identifier. For this version of the IHubGateway interface
        /// the slaId is retrieved from the configuration table since the sla is NOT the sla for the 
        /// activation in the NMS!
        /// </param>
        /// <param name="ispId">The ISP id</param>
        /// <returns>true if the operation succeeds</returns>
        public override bool TerminalActivate(string endUserId, string macAddress, long slaId, int ispId, Guid userId)
        {
            string failureReason = "";
            string failureStackTrace = "";
            bool result = false;
            
            // Get the terminal details from the CMT
            Terminal cmtTerm = _accountingController.GetTerminalDetailsByMAC(macAddress);
            try
              {
                if (cmtTerm.SitId != 0 && cmtTerm.IpAddress != null)
                {
                    cmtTerm.Blade = 10;
                    _accountingController.UpdateTerminal(cmtTerm);
                    result = true;
                }
                else
                {
                    failureReason = "Missing SitID or IPAddress";
                    failureStackTrace = "";
                }
                
            }
            catch (Exception ex)
            {
                this.logException(ex, "HubEmulator.TerminalActivate");
                failureReason = ex.Message;
                failureStackTrace = ex.StackTrace;
            }

            this.insertRequestTicketWithSla(!result, macAddress, AdmStatus.Request, AdmStatus.OPERATIONAL, failureReason, failureStackTrace, TerminalActivityTypes.Activation, ispId, (int)slaId, userId);
            return result;
        }

        /// <summary>
        /// Changes the SLA of the terminal in the Dialog hub as well as in the NMS 
        /// </summary>
        /// <param name="endUserId">The identification of the terminal to activate</param>
        /// <param name="macAddress">The MAC address of the terminal to activate</param>
        /// <param name="newSlaId">The service pack allocated to the terminal</param>
        /// <param name="currentState">The current admin state of the terminal</param>
        /// <returns>true if the application succeeded</returns>
        public override bool TerminalChangeSla(string endUserId, string macAddress, int newSlaId, Model.AdmStatus currentState, int ispId, Guid userId)
        {
            string failureReason = "";
            string failureStackTrace = "";
            bool result = true;
            //this.insertRequestTicketWithSla(!result, macAddress, AdmStatus.OPERATIONAL, AdmStatus.OPERATIONAL, failureReason, failureStackTrace, TerminalActivityTypes.ChangeSla, ispId, newSlaId, userId);
            return this.NMSTerminalChangeSla(endUserId, macAddress, newSlaId, currentState, ispId, userId);

        }

        /// <summary>
        /// Decommissions a terminal in the Dialog hub. 
        /// The terminal is effectively removed from the Hub database
        /// </summary>
        /// <param name="endUserId">The identification of the terminal to activate</param>
        /// <param name="macAddress">The MAC address of the terminal to decommission</param>
        /// <returns>true if the activation succeeded</returns>
        public override bool TerminalDecommission(string endUserId, string macAddress, int ispId, Guid userId)
        {
            string failureReason = "";
            string failureStackTrace = "";
            bool result = true;
            
            //Get the terminal details form the CMT
            Terminal cmtTerm = _accountingController.GetTerminalDetailsByMAC(macAddress);
            
            this.insertRequestTicket(!result, macAddress, AdmStatus.LOCKED, AdmStatus.DECOMMISSIONED, failureReason, failureStackTrace, TerminalActivityTypes.Decommissioning, ispId, userId);
            return result;
        }

        /// <summary>
        /// Reactivates a terminal in the Dialog hub
        /// </summary>
        /// <param name="endUserId"></param>
        /// <param name="macAddress"></param>
        /// <param name="ispId"></param>
        /// <returns></returns>
        public override bool TerminalReActivate(string endUserId, string macAddress, int ispId, Guid userId)
        {
            bool result = true;
            string failureReason = "";
            string failureStackTrace = "";
            //Get the terminal details form the CMT
            Terminal cmtTerm = _accountingController.GetTerminalDetailsByMAC(macAddress);

            
            this.insertRequestTicket(!result, macAddress, AdmStatus.LOCKED, AdmStatus.OPERATIONAL, failureReason, failureStackTrace, TerminalActivityTypes.ReActivation, ispId, userId);
            return result;
        }

        /// <summary>
        /// Suspends a terminal, the status of the terminal becomes LOCKED
        /// </summary>
        /// <param name="endUserId">The identification of the terminal to activate</param>
        /// <param name="macAddress">The MAC address of the terminal to activate</param>
        /// <returns>true if the re-activation succeeded</returns>
        public override bool TerminalSuspend(string endUserId, string macAddress, int ispId, Guid userId)
        {
            bool result = true;
            string failureReason = "";
            string failureStackTrace = "";
            //Get the terminal details form the CMT
            Terminal cmtTerm = _accountingController.GetTerminalDetailsByMAC(macAddress);
            
            this.insertRequestTicket(!result, macAddress, AdmStatus.OPERATIONAL, AdmStatus.LOCKED, failureReason, failureStackTrace, TerminalActivityTypes.Suspension, ispId, userId);
            return result;
        }

        /// <summary>
        /// Changes the MacAddress in the Dialog Hub
        /// </summary>
        /// <param name="endUserId"></param>
        /// <param name="oriMacAddress"></param>
        /// <param name="newMacAddress"></param>
        /// <param name="ispId"></param>
        /// <returns></returns>
        public override bool ChangeMacAddress(string endUserId, string oriMacAddress, string newMacAddress, int ispId, Guid userId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method retrieves the terminal information from the CMT Database
        /// </summary>
        /// <param name="macAddress">The terminal MAC address</param>
        /// <param name="ispId">The ISP identifier, this value is not really used for this implementation</param>
        /// <returns>A completed TerminalInfo object</returns>
        public override TerminalInfo getTerminalDetailsFromHub(string macAddress, string ispId)
        {
            TerminalInfo ti = new TerminalInfo();
            Terminal term = _accountingController.GetTerminalDetailsByMAC(macAddress);

            //Copy the terminal data to a terminal information object
            try
            {
                if (term.Blade != null)
                    ti.BladeId = term.Blade.ToString();

                if (term.AdmStatus != null)
                    ti.Blocked = (term.AdmStatus == 2);
                if (term.FullName != null)
                    ti.EndUserId = term.FullName;

                if (term.IpAddress != null)
                    ti.IPAddress = term.IpAddress;

                ti.IspId = term.IspId.ToString();

                if (term.MacAddress != null)
                    ti.MacAddress = term.MacAddress;

                ti.SitId = term.SitId.ToString();

                if (term.SlaId != null)
                    ti.SlaId = term.SlaId.ToString();

                if (term.SlaName != null)
                    ti.SlaName = term.SlaName;

                if (term.AdmStatus != null)
                    ti.Status = term.AdmStatus.ToString();
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDateTime = DateTime.Now;
                cmtEx.ExceptionDesc = ex.Message;
                cmtEx.ExceptionLevel = (short)ExceptionLevelEnum.Error;
                cmtEx.ExceptionStacktrace = ex.StackTrace;
                cmtEx.StateInformation = "Error";
                cmtEx.UserDescription = "Copy from Terminal to TerminalInfo object failed.";
                _logController.LogApplicationException(cmtEx);
            }

            return ti;
        }

        /// <summary>
        /// Returns the C/No value. This value is retrieved from the Dialog Hub
        /// </summary>
        /// <param name="ispId">The unique ISP identifier</param>
        /// <param name="sitId">The site id</param>
        /// <returns>The C/No value as a string</returns>
        public override string getRTNValue(string ispId, string sitId)
        {
            //try
            //{
            //    using (MySqlConnection conn = new MySqlConnection(mtConnection.ToString()))
            //    using (MySqlCommand cmd = conn.CreateCommand())
            //    {
            //        conn.Open();
            //        cmd.CommandText = "SELECT Tx_Power FROM monitoring_database.modem_short where Ship_ID=@sit order by modem_short.index desc limit 1";
            //        cmd.Parameters.AddWithValue("@sit", Int32.Parse(sitId));
            //        MySqlDataReader reader = cmd.ExecuteReader();
            //        if (reader.Read())
            //        {
            //            if (!reader.IsDBNull(0))
            //            {
            //                return reader.GetInt32(0).ToString();
            //            }
            //            else
            //                return "-1";
            //        }
            //        else
            //            return "-1";
            //    }

            //}
            //catch (Exception ex)
            //{
            //    this.logException(ex, "HubEmulator.GetSNR");
            //    return "-1";

            //}
            return "-24.5";
        }

        /// <summary>
        /// Returns the Es/No value. This value is retrieved from the Dialog Hub
        /// </summary>
        /// <remarks>
        /// This value is probably available from the Eutelsat hub but needs further investigation.
        /// For the moment being we are returning 9999 to indicate a false reading.
        /// </remarks>
        /// <param name="ispId">The unique ISP identifier</param>
        /// <param name="sitId">The site id</param>
        /// <returns>The Es/No value as a string</returns>
        public override string getFWDValue(string ispId, string sitId)
        {
            //try
            //{
            //    using (MySqlConnection conn = new MySqlConnection(mtConnection.ToString()))
            //    using (MySqlCommand cmd = conn.CreateCommand())
            //    {
            //        conn.Open();
            //        cmd.CommandText = "SELECT Rx_SNR FROM monitoring_database.modem_short where Ship_ID=@sit order by modem_short.index desc limit 1";
            //        cmd.Parameters.AddWithValue("@sit", Int32.Parse(sitId));
            //        MySqlDataReader reader = cmd.ExecuteReader();
            //        if (reader.Read())
            //        {
            //            if (!reader.IsDBNull(0))
            //            {
            //                return reader.GetInt32(0).ToString();
            //            }
            //            else
            //                return "-1";
            //        }
            //        else
            //            return "-1";
            //    }

            //}
            //catch (Exception ex)
            //{
            //    this.logException(ex, "HubEmulator.Tx_Power");
            //    return "-1";

            //}
            return 11.0.ToString();
        }

       
       

        /// <summary>
        /// This method allows to update the freezone information for a terminal
        /// </summary>
        /// <param name="FreeZoneId">The identifier of the freezone allocated to the terminal</param>
        /// <param name="macAddress">The MAC address of the terminal</param>
        /// <param name="FreeZoneFlag">If this flag is set to true, a freezone option is allocated to the terminal. In
        /// case this value is false, the terminal does not have the freezone option</param>
        /// <returns></returns>
        public bool NMSModifyFreeZoneForTerminal(int FreeZoneId, string macAddress, Boolean FreeZoneFlag)
        {
            throw new NotImplementedException();
        }

        public override bool NMSTerminalActivateWithRange(string endUserId, string macAddress, long slaId, int ispId, int ipMask, int freeZone, Guid userId)
        {
            bool result = false;
            nms.NMSGatewayService nmsService = new nms.NMSGatewayService();
            Terminal term = _accountingController.GetTerminalDetailsByMAC(macAddress);


            try
            {
                TerminalInfo ti = this.getTerminalDetailsFromHub(macAddress, ispId.ToString());
                nms.RequestTicket rt;
                if (term.OrgId != null)
                {
                    Organization organization = _accountingController.GetOrganization((int)term.OrgId);
                    if (organization.VNO != null && (bool)organization.VNO)
                    {
                        rt = nmsService.createRegistrationWithVNO(endUserId, macAddress, ipMask, (int)slaId, ispId, ti.IPAddress, Int32.Parse(ti.SitId), freeZone, organization.FullName);
                    }
                    else
                    {
                        rt = nmsService.createRegistration(endUserId, macAddress, ipMask, (int)slaId, ispId, ti.IPAddress, Int32.Parse(ti.SitId), freeZone);
                    }
                }
                //Need to specify an IP Address as this address is not generated by the NMS but by the Eutelsat HUB
                else
                {
                    rt = nmsService.createRegistration(endUserId, macAddress, ipMask, (int)slaId, ispId, ti.IPAddress, Int32.Parse(ti.SitId), freeZone);
                }

                result = this.insertRequestTicketWithSla(rt, macAddress, AdmStatus.LOCKED, AdmStatus.OPERATIONAL, TerminalActivityTypes.Activation, ispId, (int)slaId, userId);
            }
            catch (Exception ex)
            {
                this.logException(ex, "HubEmulator.NMSTerminalActivateWithRange");
            }
            return result;
        }

        /// <summary>
        /// Activates a terminal in the NMS without specifying a range by means of
        /// an IP Mask value.
        /// </summary>
        /// <remarks>
        /// This method activates a terimal in the NMS without specifiying a range, thus the
        /// IP Mask is always set to /32 which means that only one IP address can be specified.
        /// </remarks>
        /// <param name="endUserId">The end user identifier</param>
        /// <param name="macAddress">The terminal MAC address</param>
        /// <param name="slaId">The SLA identifier</param>
        /// <param name="ispId">The ISP identifier</param>
        /// <returns>True if the method succeeds</returns>
        public override bool NMSTerminalActivate(string endUserId, string macAddress, long slaId, int ispId, int freeZone, Guid userId)
        {
            nms.NMSGatewayService nmsService = new nms.NMSGatewayService();
            bool result = false;

            try
            {
                //Need to specify an IP Address as this address is not generated by the NMS but by the Eutelsat HUB
                TerminalInfo ti = this.getTerminalDetailsFromHub(macAddress, ispId.ToString());
                nms.RequestTicket rt = nmsService.createRegistration(endUserId, macAddress, 32, (int)slaId, ispId, ti.IPAddress, Int32.Parse(ti.SitId), freeZone);
                result = base.insertRequestTicket(rt, macAddress, AdmStatus.Request, AdmStatus.OPERATIONAL, TerminalActivityTypes.Activation, ispId, userId);
            }
            catch (Exception ex)
            {
                this.logException(ex, "HubEmulator.NMSTerminalActivate");
            }
            return result;
        }


        /// <summary>
        /// Inserts a new ticket in the CMT database and flags it as an NMS ticket
        /// </summary>
        /// <remarks>
        /// Use this version of the insertRequestTicket method to insert a request ticket for activities
        /// on the Dialog hub in the database
        /// </remarks>
        /// <param name="failed">Has status true if the request on the Dialog hub failed</param>
        /// <param name="macAddress">The MacAddress of the target</param>
        /// <param name="sourceAdmStatus">The Source admin status</param>
        /// <param name="targetAdmStatus">The Target admin status</param>
        /// <param name="termActivityType">The terminal activity type</param>
        /// <param name="ispId">The ISP ID</param>
        /// <returns>True if the operation succeeded</returns>
        private bool insertRequestTicket(bool failed, string macAddress, AdmStatus sourceAdmStatus, AdmStatus targetAdmStatus, string failureReason, string failureStackTrace,
                                           TerminalActivityTypes termActivityType, int ispId, Guid userId)
        {
            TicketDataMgr ticketDataMgr = new TicketDataMgr();
            Guid ticketId = Guid.NewGuid();
            if (failureReason.Length > 400)
            {
                failureReason = failureReason.Substring(0, 400);
            }
            int ticketStatus = 0;
            if (failed)
            {
                ticketStatus = 1;
            }
            return ticketDataMgr.insertTicket(ticketId.ToString(), macAddress, sourceAdmStatus, targetAdmStatus, ticketStatus, failureReason, failureStackTrace, termActivityType, ispId, 1, userId);
        }

        /// <summary>
        /// Inserts a ticket  for an SLA change
        /// </summary>
        /// <remarks>
        /// Use this version of the insertRequestTicket method to insert a request ticket for activities
        /// on the Dialog hub in the database
        /// </remarks>
        /// <param name="failed">Has status true if the request on the Dialog hub failed</param>
        /// <param name="macAddress">The Mac Address</param>
        /// <param name="sourceAdmStatus">The source amdin status</param>
        /// <param name="targetAdmStatus">The target admin status</param>
        /// <param name="termActivityType">The terminal activity type</param>
        /// <param name="ispId">The ISP identifier</param>
        /// <param name="newSla">The new sla</param>
        /// <returns>True if the method succeeded</returns>
        private bool insertRequestTicketWithSla(bool failed, string macAddress, AdmStatus sourceAdmStatus, AdmStatus targetAdmStatus, string failureReason, string failureStackTrace,
                                           TerminalActivityTypes termActivityType, int ispId, int newSla, Guid userId)
        {
            TicketDataMgr ticketDataMgr = new TicketDataMgr();
            Guid ticketId = Guid.NewGuid();
            if (failureReason.Length > 400)
            {
                failureReason = failureReason.Substring(0, 400);
            }
            int ticketStatus = 0;
            if (failed)
            {
                ticketStatus = 1;
            }
            return ticketDataMgr.insertTicket(ticketId.ToString(), macAddress, sourceAdmStatus, targetAdmStatus, ticketStatus, failureReason, failureStackTrace, termActivityType, ispId, 1, newSla, userId);
        }

        /// <summary>
        /// Inserts a ticket for a MAC Address change
        /// </summary>
        /// <remarks>
        /// Use this version of the insertRequestTicket method to insert a request ticket for activities
        /// on the Dialog hub in the database
        /// </remarks>
        /// <param name="failed">Has status true if the request on the Dialog hub failed</param>
        /// <param name="macAddress">The MAC address of the terminal</param>
        /// <param name="sourceAdmStatus">The sourde administration status</param>
        /// <param name="targetAdmStatus">The target administration status</param>
        /// <param name="termActivityType">The terminal activity type</param>
        /// <param name="ispId">The ISP identifier</param>
        /// <param name="NewMacAddress">The new mac address</param>
        /// <returns></returns>
        private bool insertRequestTicketWithMac(bool failed, string macAddress, AdmStatus sourceAdmStatus, AdmStatus targetAdmStatus, string failureReason, string failureStackTrace,
                                           TerminalActivityTypes termActivityType, int ispId, string NewMacAddress, Guid userId)
        {
            TicketDataMgr ticketDataMgr = new TicketDataMgr();
            Guid ticketId = Guid.NewGuid();
            if (failureReason.Length > 400)
            {
                failureReason = failureReason.Substring(0, 400);
            }
            int ticketStatus = 0;
            if (failed)
            {
                ticketStatus = 1;
            }
            return ticketDataMgr.insertTicket(ticketId.ToString(), macAddress, sourceAdmStatus, targetAdmStatus, ticketStatus, failureReason, failureStackTrace, termActivityType, ispId, 1, NewMacAddress, userId);
        }

        /// <summary>
        /// Returns the status of a request ticket.
        /// </summary>
        /// <remarks>
        /// This method gets the ticket from the CMT database 
        /// </remarks>
        /// <param name="requestTicketId">The requestticket identfier</param>
        /// <param name="ispId">The ISP identfier</param>
        /// <returns>
        /// The corresponding CMT request ticket or null if the ticket could not 
        ///  be found
        ///  </returns>
        public override CmtRequestTicket LookupRequestTicket(string requestTicketId, int ispId)
        {
            CmtRequestTicket cmtRt = null;
            try
            {
                CMTTicket cmtTick = _logController.GetTicketById(requestTicketId);
                cmtRt = new CmtRequestTicket();
                cmtRt.id = cmtTick.TicketId;
                cmtRt.failureReason = cmtTick.FailureReason;
                cmtRt.failureStackTrace = cmtTick.FailureStackTrace;

                if (cmtTick.TicketStatus == 0)
                {
                    cmtRt.requestStatus = CmtRequestStatus.SUCCESSFUL;
                }

                if (cmtTick.TicketStatus == 1)
                {
                    cmtRt.requestStatus = CmtRequestStatus.FAILED;
                }

                if (cmtTick.TicketStatus == 2)
                {
                    cmtRt.requestStatus = CmtRequestStatus.SUCCESSFUL;
                }
            }
            catch (Exception ex)
            {
                this.logException(ex, "HubEmulator.LookupRequestTicket");
                cmtRt = null;
            }

            return cmtRt;
        }

        /// <summary>
        /// Initiates the Dialog terminal object for the activation of an SCPC terminal
        /// </summary>
        /// <param name="cmtTerm">The CMT terminal to be activated</param>
        /// <returns>A Dialog SCPC terminal for Line Up</returns>
        

        
       

       


        /// <summary>
        /// Logs an exception
        /// </summary>
        /// <param name="ex">The exception to log</param>
        /// <param name="method">The method which generated the error</param>
        public void logException(Exception ex, string method)
        {
            CmtApplicationException cmtApplicationException = new CmtApplicationException();
            cmtApplicationException.ExceptionDateTime = DateTime.Now;
            cmtApplicationException.ExceptionDesc = ex.Message;
            cmtApplicationException.ExceptionStacktrace = ex.StackTrace;
            cmtApplicationException.UserDescription = method;
            cmtApplicationException.ExceptionLevel = (short)ExceptionLevelEnum.Critical;
            _logController.LogApplicationException(cmtApplicationException);
        }
    }
}