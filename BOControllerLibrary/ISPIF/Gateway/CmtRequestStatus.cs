﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BOControllerLibrary.ISPIF.Gateway
{
    /// <summary>
    /// This enumrations holds the values which are returend by means of a CmtRequestTicket
    /// </summary>
    public enum CmtRequestStatus { BUSY, FAILED, SUCCESSFUL }
}