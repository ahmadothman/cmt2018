﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using BOControllerLibrary.IspSupportInterfaceServiceRef;
using BOControllerLibrary.Model;
using BOControllerLibrary.Controllers;
using BOControllerLibrary.Controllers.Interfaces;
using BOControllerLibrary.ISPIF.Model;
using BOControllerLibrary.Model.Log;

namespace BOControllerLibrary.ISPIF.Gateway
{
    /// <summary>
    /// Inserts a ticket in the Ticket table
    /// </summary>
    public class TicketDataMgr
    {
        string _connectionString = "";
        string _databaseProvider = "";
        private IBOLogController _controller;

        public TicketDataMgr()
        {
            _connectionString = BOControllerLibrary.Properties.Settings.Default.ConnectionString;
            _databaseProvider = BOControllerLibrary.Properties.Settings.Default.ProviderName;
            _controller = new BOLogController(_connectionString, _databaseProvider);
        }

        /// <summary>
        /// Inserts a new ticket in the ticket database
        /// </summary>
        /// <param name="ticketId">The ticket id as received from the HUB or NMS</param>
        /// <param name="macAddress">The terminals MAC address</param>
        /// <param name="sourceAdmState">The source admin status</param>
        /// <param name="targetAdmState">The target admin status</param>
        /// <param name="ticketStatus">The ticket status</param>
        /// <param name="failureReason">The reason of failure</param>
        /// <param name="failureStackTrace">The failure stack trace</param>
        /// <param name="terminalActivity"></param>
        /// <returns></returns>
        public bool insertTicket(string ticketId, string macAddress, AdmStatus sourceAdmState,
                                        AdmStatus targetAdmState, int ticketStatus,
                                        string failureReason, string failureStackTrace, 
                                        TerminalActivityTypes terminalActivity, int ispId,
                                        short NMSActivate, Guid userId)
        {
            bool result = false;
            string insertCmd = "INSERT INTO Tickets (TicketId, CreationDate, MacAddress, SourceAdmState, " +
                                    "TargetAdmState, TicketStatus, FailureReason, FailureStackTrace, TermActivity, IspId, NMSActivate, UserId) " +
                                    "VALUES (@TicketId, GetDate(), @MacAddress, @SourceAdmState, @TargetAdmState, " +
                                    "@TicketStatus, @FailureReason, @FailureStackTrace, @TermActivity, @IspId, @NMSActivate, @UserId)";

            //Fill failureReason and FailureStackTrace with empty strings if null
            if (failureReason == null)
                failureReason = "";

            if (failureStackTrace == null)
                failureStackTrace = "";

            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                conn.Open();

                try
                {
                    using (SqlCommand cmd = new SqlCommand(insertCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@TicketId", ticketId);
                        cmd.Parameters.AddWithValue("@MacAddress", macAddress);
                        cmd.Parameters.AddWithValue("@SourceAdmState", (int)sourceAdmState);
                        cmd.Parameters.AddWithValue("@TargetAdmState", (int)targetAdmState);
                        cmd.Parameters.AddWithValue("@TicketStatus", (int)ticketStatus);
                        cmd.Parameters.AddWithValue("@FailureReason", failureReason);
                        cmd.Parameters.AddWithValue("@FailureStackTrace", failureStackTrace);
                        cmd.Parameters.AddWithValue("@TermActivity", (int)terminalActivity);
                        cmd.Parameters.AddWithValue("@IspId", ispId);
                        cmd.Parameters.AddWithValue("@NMSActivate", NMSActivate);
                        cmd.Parameters.AddWithValue("@UserId", userId);

                        int numRows = cmd.ExecuteNonQuery();

                        if (numRows > 0)
                        {
                            result = true;
                        }
                        else
                        {
                            result = false;
                            CmtApplicationException cmtApplicationException = new CmtApplicationException();
                            cmtApplicationException.ExceptionDateTime = DateTime.Now;
                            cmtApplicationException.ExceptionDesc = "Could not insert a ticket for terminal: " + macAddress;
                            cmtApplicationException.ExceptionStacktrace = "";
                            cmtApplicationException.ExceptionLevel = (short)ExceptionLevelEnum.Error;
                            cmtApplicationException.UserDescription = "TicketDataMgr - insertTicket";
                            _controller.LogApplicationException(cmtApplicationException);
                        }
                    }
                }
                catch (Exception ex)
                {
                    result = false;
                    CmtApplicationException cmtApplicationException = new CmtApplicationException();
                    cmtApplicationException.ExceptionDateTime = DateTime.Now;
                    cmtApplicationException.ExceptionDesc = ex.Message;
                    cmtApplicationException.ExceptionStacktrace = ex.StackTrace;
                    cmtApplicationException.ExceptionLevel = (short)ExceptionLevelEnum.Critical;
                    cmtApplicationException.UserDescription = "TicketDataMgr - insertTicket";
                    _controller.LogApplicationException(cmtApplicationException);
                }
            }

            return result;
        }

        /// <summary>
        /// Inserts a new ticket in the ticket database with Sla
        /// </summary>
        /// <param name="ticketId"></param>
        /// <param name="macAddress"></param>
        /// <param name="sourceAdmState"></param>
        /// <param name="targetAdmState"></param>
        /// <param name="ticketStatus"></param>
        /// <param name="failureReason"></param>
        /// <param name="failureStackTrace"></param>
        /// <param name="terminalActivity"></param>
        /// <returns></returns>
        public bool insertTicket(string ticketId, string macAddress, AdmStatus sourceAdmState,
                                        AdmStatus targetAdmState, int ticketStatus,
                                        string failureReason, string failureStackTrace, 
                                        TerminalActivityTypes terminalActivity, int ispId, 
                                        short NMSActivate, int slaId, Guid userId)
        {
            bool result = false;
            string insertCmd = "INSERT INTO Tickets (TicketId, CreationDate, MacAddress, SourceAdmState, " +
                                    "TargetAdmState, TicketStatus, FailureReason, FailureStackTrace, TermActivity, IspId, NMSActivate, newSla, UserId) " +
                                    "VALUES (@TicketId, GetDate(), @MacAddress, @SourceAdmState, @TargetAdmState, " +
                                    "@TicketStatus, @FailureReason, @FailureStackTrace, @TermActivity, @IspId, @NMSActivate, @newSla, @UserId)";

            //Fill failureReason and FailureStackTrace with empty strings if null
            if (failureReason == null)
                failureReason = "";

            if (failureStackTrace == null)
                failureStackTrace = "";

            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                conn.Open();

                try
                {
                    using (SqlCommand cmd = new SqlCommand(insertCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@TicketId", ticketId);
                        cmd.Parameters.AddWithValue("@MacAddress", macAddress);
                        cmd.Parameters.AddWithValue("@SourceAdmState", (int)sourceAdmState);
                        cmd.Parameters.AddWithValue("@TargetAdmState", (int)targetAdmState);
                        cmd.Parameters.AddWithValue("@TicketStatus", (int)ticketStatus);
                        cmd.Parameters.AddWithValue("@FailureReason", failureReason);
                        cmd.Parameters.AddWithValue("@FailureStackTrace", failureStackTrace);
                        cmd.Parameters.AddWithValue("@TermActivity", (int)terminalActivity);
                        cmd.Parameters.AddWithValue("@IspId", ispId);
                        cmd.Parameters.AddWithValue("@newSla", slaId);
                        cmd.Parameters.AddWithValue("@NMSActivate", NMSActivate);
                        cmd.Parameters.AddWithValue("@UserId", userId);

                        int numRows = cmd.ExecuteNonQuery();

                        if (numRows > 0)
                        {
                            result = true;
                        }
                        else
                        {
                            result = false;
                            CmtApplicationException cmtApplicationException = new CmtApplicationException();
                            cmtApplicationException.ExceptionDateTime = DateTime.Now;
                            cmtApplicationException.ExceptionDesc = "Could not insert a ticket for terminal: " + macAddress;
                            cmtApplicationException.ExceptionStacktrace = "";
                            cmtApplicationException.ExceptionLevel = (short)ExceptionLevelEnum.Error;
                            cmtApplicationException.UserDescription = "TicketDataMgr - insertTicket";
                            _controller.LogApplicationException(cmtApplicationException);
                        }
                    }
                }
                catch (Exception ex)
                {
                    result = false;
                    CmtApplicationException cmtApplicationException = new CmtApplicationException();
                    cmtApplicationException.ExceptionDateTime = DateTime.Now;
                    cmtApplicationException.ExceptionDesc = ex.Message;
                    cmtApplicationException.ExceptionStacktrace = ex.StackTrace;
                    cmtApplicationException.ExceptionLevel = (short)ExceptionLevelEnum.Critical;
                    cmtApplicationException.UserDescription = "TicketDataMgr - insertTicket";
                    _controller.LogApplicationException(cmtApplicationException);
                }
            }

            return result;
        }

        /// <summary>
        /// Inserts a new ticket in the ticket database with a new Mac Address
        /// </summary>
        /// <param name="ticketId"></param>
        /// <param name="macAddress"></param>
        /// <param name="sourceAdmState"></param>
        /// <param name="targetAdmState"></param>
        /// <param name="ticketStatus"></param>
        /// <param name="failureReason"></param>
        /// <param name="failureStackTrace"></param>
        /// <param name="terminalActivity"></param>
        /// <returns></returns>
        public bool insertTicket(string ticketId, string macAddress, AdmStatus sourceAdmState,
                                        AdmStatus targetAdmState, int ticketStatus,
                                        string failureReason, string failureStackTrace, 
                                        TerminalActivityTypes terminalActivity, int ispId, 
                                        short NMSActivate, string NewMacAddress, Guid userId)
        {
            bool result = false;
            string insertCmd = "INSERT INTO Tickets (TicketId, CreationDate, MacAddress, SourceAdmState, " +
                                    "TargetAdmState, TicketStatus, FailureReason, FailureStackTrace, TermActivity, IspId, NewMacAddress, NMSActivate, UserId) " +
                                    "VALUES (@TicketId, GetDate(), @MacAddress, @SourceAdmState, @TargetAdmState, " +
                                    "@TicketStatus, @FailureReason, @FailureStackTrace, @TermActivity, @IspId, @NewMacAddress, @NMSActivate, @UserId)";

            //Fill failureReason and FailureStackTrace with empty strings if null
            if (failureReason == null)
                failureReason = "";

            if (failureStackTrace == null)
                failureStackTrace = "";

            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                conn.Open();

                try
                {
                    using (SqlCommand cmd = new SqlCommand(insertCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@TicketId", ticketId);
                        cmd.Parameters.AddWithValue("@MacAddress", macAddress);
                        cmd.Parameters.AddWithValue("@SourceAdmState", (int)sourceAdmState);
                        cmd.Parameters.AddWithValue("@TargetAdmState", (int)targetAdmState);
                        cmd.Parameters.AddWithValue("@TicketStatus", (int)ticketStatus);
                        cmd.Parameters.AddWithValue("@FailureReason", failureReason);
                        cmd.Parameters.AddWithValue("@FailureStackTrace", failureStackTrace);
                        cmd.Parameters.AddWithValue("@TermActivity", (int)terminalActivity);
                        cmd.Parameters.AddWithValue("@IspId", ispId);
                        cmd.Parameters.AddWithValue("@NewMacAddress", NewMacAddress);
                        cmd.Parameters.AddWithValue("@NMSActivate", NMSActivate);
                        cmd.Parameters.AddWithValue("@UserId", userId);

                        int numRows = cmd.ExecuteNonQuery();

                        if (numRows > 0)
                        {
                            result = true;
                        }
                        else
                        {
                            result = false;
                            CmtApplicationException cmtApplicationException = new CmtApplicationException();
                            cmtApplicationException.ExceptionDateTime = DateTime.Now;
                            cmtApplicationException.ExceptionDesc = "Could not insert a ticket for terminal: " + macAddress;
                            cmtApplicationException.ExceptionStacktrace = "";
                            cmtApplicationException.ExceptionLevel = (short)ExceptionLevelEnum.Error;
                            cmtApplicationException.UserDescription = "TicketDataMgr - insertTicket";
                            _controller.LogApplicationException(cmtApplicationException);
                        }
                    }
                }
                catch (Exception ex)
                {
                    result = false;
                    CmtApplicationException cmtApplicationException = new CmtApplicationException();
                    cmtApplicationException.ExceptionDateTime = DateTime.Now;
                    cmtApplicationException.ExceptionDesc = ex.Message;
                    cmtApplicationException.ExceptionStacktrace = ex.StackTrace;
                    cmtApplicationException.UserDescription = "TicketDataMgr - insertTicket";
                    cmtApplicationException.ExceptionLevel = (short)ExceptionLevelEnum.Critical;
                    _controller.LogApplicationException(cmtApplicationException);
                }
            }

            return result;
        }


        /// <summary>
        /// This method inserts a ticket for an IP address change.
        /// </summary>
        /// <remarks>
        /// At the moment an IP address change via the ISPIF is not possible. The ticket master
        /// will only execute the IP Address change on the NMS Gatway interface regardless of the NMS Activate
        /// value. Later this value will be used to peform a two step address change. For the moemnt
        /// leave this value at 0.
        /// </remarks>
        /// <param name="ticketId">The ticket Id obtained from the HUB or NMS</param>
        /// <param name="macAddress">The MAC address of the terminal</param>
        /// <param name="sourceAdmState">The source admin state (not applicable in this case)</param>
        /// <param name="targetAdmState">The target admin state (not applicable in this case)</param>
        /// <param name="ticketStatus">The ticket status as obtained from the NMS or HUB</param>
        /// <param name="failureReason">In case of a failure, the failure reason</param>
        /// <param name="failureStackTrace">In case of a failure the failure stack trace</param>
        /// <param name="terminalActivity">The terminal activity type. Must be 1002 IPAddressChange</param>
        /// <param name="ispId">The ISP identifier</param>
        /// <param name="NMSActivate">Not applicable for the moment, set to 0</param>
        /// <param name="newIPAddress">The new IP Address</param>
        /// <param name="newIPMask">The new IP Mask</param>
        /// <returns></returns>
        public bool insertTicket(string ticketId, string macAddress, AdmStatus sourceAdmState,
                                       AdmStatus targetAdmState, int ticketStatus,
                                       string failureReason, string failureStackTrace,
                                       TerminalActivityTypes terminalActivity, int ispId,
                                       short NMSActivate, string newIPAddress, int newIPMask, Guid userId)
        {
            bool result = false;
            string insertCmd = "INSERT INTO Tickets (TicketId, CreationDate, MacAddress, SourceAdmState, " +
                                    "TargetAdmState, TicketStatus, FailureReason, FailureStackTrace, TermActivity, IspId, NMSActivate, NewIPAddress, NewMask, UserId) " +
                                    "VALUES (@TicketId, GetDate(), @MacAddress, @SourceAdmState, @TargetAdmState, " +
                                    "@TicketStatus, @FailureReason, @FailureStackTrace, @TermActivity, @IspId, @NMSActivate, @NewIPAddress, @NewMask, @UserId)";

            //Fill failureReason and FailureStackTrace with empty strings if null
            if (failureReason == null)
                failureReason = "";

            if (failureStackTrace == null)
                failureStackTrace = "";

            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                conn.Open();

                try
                {
                    using (SqlCommand cmd = new SqlCommand(insertCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@TicketId", ticketId);
                        cmd.Parameters.AddWithValue("@MacAddress", macAddress);
                        cmd.Parameters.AddWithValue("@SourceAdmState", (int)sourceAdmState);
                        cmd.Parameters.AddWithValue("@TargetAdmState", (int)targetAdmState);
                        cmd.Parameters.AddWithValue("@TicketStatus", (int)ticketStatus);
                        cmd.Parameters.AddWithValue("@FailureReason", failureReason);
                        cmd.Parameters.AddWithValue("@FailureStackTrace", failureStackTrace);
                        cmd.Parameters.AddWithValue("@TermActivity", (int)terminalActivity);
                        cmd.Parameters.AddWithValue("@IspId", ispId);
                        cmd.Parameters.AddWithValue("@NewIPAddress", newIPAddress);
                        cmd.Parameters.AddWithValue("@NewMask", newIPMask);
                        cmd.Parameters.AddWithValue("@NMSActivate", NMSActivate);
                        cmd.Parameters.AddWithValue("@UserId", userId);

                        int numRows = cmd.ExecuteNonQuery();

                        if (numRows > 0)
                        {
                            result = true;
                        }
                        else
                        {
                            result = false;
                            CmtApplicationException cmtApplicationException = new CmtApplicationException();
                            cmtApplicationException.ExceptionDateTime = DateTime.Now;
                            cmtApplicationException.ExceptionDesc = "Could not insert a ticket for terminal: " + macAddress;
                            cmtApplicationException.ExceptionStacktrace = "";
                            cmtApplicationException.ExceptionLevel = (short)ExceptionLevelEnum.Error;
                            cmtApplicationException.UserDescription = "TicketDataMgr - insertTicket";
                            _controller.LogApplicationException(cmtApplicationException);
                        }
                    }
                }
                catch (Exception ex)
                {
                    result = false;
                    CmtApplicationException cmtApplicationException = new CmtApplicationException();
                    cmtApplicationException.ExceptionDateTime = DateTime.Now;
                    cmtApplicationException.ExceptionDesc = ex.Message;
                    cmtApplicationException.ExceptionStacktrace = ex.StackTrace;
                    cmtApplicationException.ExceptionLevel = (short)ExceptionLevelEnum.Critical;
                    cmtApplicationException.UserDescription = "TicketDataMgr - insertTicket";
                    _controller.LogApplicationException(cmtApplicationException);
                }
            }

            return result;
        }
    }
}