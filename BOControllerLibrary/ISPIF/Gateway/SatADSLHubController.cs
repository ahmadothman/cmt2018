﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Collections.Specialized;
using System.Net;
using BOControllerLibrary.Model;
using BOControllerLibrary.ISPIF.Model;
using CMTTerminal = BOControllerLibrary.Model.Terminal;
using BOControllerLibrary.Controllers;
using BOControllerLibrary.com.astra2connect.ispif;
using net.nimera.supportlibrary;

namespace BOControllerLibrary.ISPIF.Gateway
{
    /// <summary>
    /// Speicalization of the IISPIFController interface which retrieves data as much as possible
    /// from the local CMTData database.
    /// </summary>
    public class SatADSLHubController
    {
        string _connectionString = "";
        string _databaseProvider = "";
        const string _RTN_VIEW = "CNO_5M"; //Constant for getting the RTN value from the ISPIF DB
        const string _FWD_VIEW = "FWD_ESNO_5M"; //Constant for getting the FWD value from the ISPIF DB
        BOLogController _logger = null;
        BOConfigurationController _configController = null;
        bool _useCacheFlag = false; //Allows to switch caching on and off

        #region IISPIFController Members

        /// <summary>
        /// Default constructor
        /// </summary>
        public SatADSLHubController()
        {
            _connectionString = ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
            _databaseProvider = ConfigurationManager.ConnectionStrings["ApplicationServices"].ProviderName;
            _logger = new BOLogController(_connectionString, _databaseProvider);
            _configController = new BOConfigurationController(_connectionString, _databaseProvider);

            //Read the useCacheFlag parameter from the configuration file
            string useCacheFlagStr = _configController.getParameter("UseCacheFlag");
            if (useCacheFlagStr == null)
            {
                _useCacheFlag = true; //default behaviour
            }
            else
            {
                _useCacheFlag = useCacheFlagStr.Equals("true");
            }
        }

        /// <summary>
        /// Returns a TerminalInfo object with the data for the
        /// given macAddress
        /// </summary>
        /// <param name="macAddress">The MAC address of the terminal</param>
        /// <param name="ispId">The ISP identifier, this value is necessary to select
        /// the correct IHubGateway implementation</param>
        /// <returns>A completed TerminalInfo object. This value can be NULL!</returns>
        public Model.TerminalInfo getTerminalInfoByMacAddress(string macAddress)
        {
            TerminalInfo ti = new TerminalInfo();
            SQLServerCacheProvider cacheManager = new SQLServerCacheProvider();

            //Firt check if the terminal information is chached
            cacheStateEnum cacheState = cacheManager.isCached(macAddress);

            if (cacheState == cacheStateEnum.Cached && _useCacheFlag)
            {
                //Great data is cached, no need to go to the ISPIF
                ti = cacheManager.popFromCache(macAddress);
                ti.ErrorFlag = false;
            }
            else
            {
                try
                {

                    //First read terminal data from the CMT database
                    BOAccountingController accountingController =
                                                    new BOAccountingController(_connectionString, _databaseProvider);

                    CMTTerminal.Terminal term = accountingController.GetTerminalDetailsByMAC(macAddress);

                    //Get the SlaID
                    ServicePackQuery servicePackQuery = new ServicePackQuery();
                    ServicePackInfo si = servicePackQuery.getServicePackBySlaId(term.SlaId.ToString());

                    ti.IPAddress = term.IpAddress;
                    ti.MacAddress = macAddress;
                    ti.SitId = term.SitId.ToString();
                    ti.EndUserId = term.FullName;
                    ti.SlaId = term.SlaId.ToString();
                    ti.SlaName = si.SlaName;
                    ti.InvoiceDayOfMonth = "" + (Convert.ToInt32(ti.SitId) % 28 + 1);
                    ti.FUPThreshold = si.FUPThreshold;
                    ti.RTNFUPThreshold = si.RTNFUPThreshold;
                    ti.DRAboveFUP = si.DataRateAboveFUP;
                    ti.Id = "NA";
                    ti.IspId = term.IspId.ToString();

                    //Fill the TerminalInfo object with data from the ISPIF
                    this.getVolumeInfo(ref ti);

                    //Cache the data only if the _useCacheFlag is set to true
                    if (!ti.ErrorFlag && _useCacheFlag)
                    {
                        cacheManager.pushToCache(ti, cacheState);
                    }
                }
                catch (Exception ex)
                {
                    ti.ErrorFlag = true;
                    ti.ErrorMsg = ex.Message;
                }
            }

            return ti;
        }

        /// <summary>
        /// This method returns a TerminalInfo object completed with the data retrieved
        /// from the hub.
        /// </summary>
        /// <remarks>
        /// Use this method to synchronize the data in the CMT database with the data available
        /// from the Teleport hub. The TerminalInfo object contains only a limited amount of data:
        /// <ul>
        ///  <li>The blade Id</li>
        ///  <li>The IP address</li>
        ///  <li>The FWPool</li>
        ///  <li>The SitId</li>
        ///  <li>The SlaId</li>
        /// </ul>
        /// Remark that the status is NOT reported by this method.
        /// </remarks>
        /// <param name="macAddress">The MAC address of the terminal</param>
        /// <param name="ispId">The ISP Identifier</param>
        /// <returns>A completed terminal info object</returns>
        public Model.TerminalInfo getTerminalDetailsFromHub(string macAddress, string ispId)
        {
            IspSupportInterfaceService ispService = new IspSupportInterfaceService();
            ispService.Credentials = this.readCredentials(Int32.Parse(ispId));
            ispService.PreAuthenticate = true;
            ServicePointManager.Expect100Continue = false;
            int trialIdx = 0;
            bool loopFlag = true;
            TerminalInfo termInfo = null;

            while (loopFlag && trialIdx < 4)
            {
                try
                {
                    NbiTerminalInfo ti = ispService.getNbiInfoByMac(macAddress);
                    Registration reg = ispService.lookupRegistrationByEndUserId(ti.sitName, false, ti.nbiIspId);
                    if (ti != null)
                    {
                        termInfo = new TerminalInfo();
                        termInfo.BladeId = ti.bladeId;
                        termInfo.IPAddress = ti.ipAddress;
                        termInfo.FWPool = ti.forwardPoolId;
                        termInfo.SitId = ti.sitId;
                        termInfo.SlaName = ti.slaName;
                        termInfo.EndUserId = ti.sitName;
                        termInfo.SlaId = ti.slaId;
                        termInfo.Status = reg.status.ToString();
                        termInfo.MacAddress = macAddress;
                        loopFlag = false;
                    }

                }
                catch (Exception ex)
                {
                    trialIdx++;
                    CmtApplicationException cmtException = new CmtApplicationException();
                    cmtException.ExceptionDesc = ex.Message;
                    cmtException.ExceptionDateTime = DateTime.Now;
                    cmtException.UserDescription = "getTerminalDetailsFromHub method failed";
                    _logger.LogApplicationException(cmtException);
                }
            }
            return termInfo;
        }
        
        /// <summary>
        /// This method retrieves the C/No (return value) from the SES HUB.
        /// </summary>
        /// <param name="ispId">The ISP identifier</param>
        /// <param name="sitId">The site identifier</param>
        /// <returns></returns>
        public string getRTNValue(string ispId, string sitId)
        {
            IspSupportInterfaceService ispService = new IspSupportInterfaceService();
            ispService.Credentials = this.readCredentials(Int32.Parse(ispId));
            ispService.PreAuthenticate = true;
            ServicePointManager.Expect100Continue = false;
            int trialIdx = 0;
            bool loopFlag = true;
            string cnoValue = "";

            while (loopFlag && trialIdx < 4)
            {
                try
                {
                    //RTN View volume
                    PmtDataPoint[] pmtDataPoints = ispService.getPmtDataLastValue(_RTN_VIEW, ispId, sitId);
                    
                    if (pmtDataPoints != null)
                    {
                        cnoValue = pmtDataPoints[0].value;
                    }
                    else
                    {
                        cnoValue = "-1.0";
                    }

                    loopFlag = false;
                }
                catch (Exception ex)
                {
                    trialIdx++;
                    CmtApplicationException cmtException = new CmtApplicationException();
                    cmtException.ExceptionDesc = ex.Message;
                    cmtException.ExceptionDateTime = DateTime.Now;
                    cmtException.UserDescription = "getRTNValue method failed";
                    _logger.LogApplicationException(cmtException);
                }
            }

            return cnoValue;
        }

        /// <summary>
        /// This method retrieves the Es/No (return value) from the SES HUB.
        /// </summary>
        /// <param name="ispId">The ISP identifier</param>
        /// <param name="sitId">The site identifier</param>
        /// <returns></returns>
        public string getFWDValue(string ispId, string sitId)
        {
            IspSupportInterfaceService ispService = new IspSupportInterfaceService();
            ispService.Credentials = this.readCredentials(Int32.Parse(ispId));
            ispService.PreAuthenticate = true;
            ServicePointManager.Expect100Continue = false;
            int trialIdx = 0;
            bool loopFlag = true;
            string esnoValue = "";

            while (loopFlag && trialIdx < 4)
            {
                try
                {
                    //RTN View volume
                    PmtDataPoint[] pmtDataPoints = ispService.getPmtDataLastValue(_FWD_VIEW, ispId, sitId);

                    if (pmtDataPoints != null)
                    {
                        esnoValue = pmtDataPoints[0].value;
                    }
                    else
                    {
                        esnoValue = "-1.0";
                    }

                    loopFlag = false;
                }
                catch (Exception ex)
                {
                    trialIdx++;
                    CmtApplicationException cmtException = new CmtApplicationException();
                    cmtException.ExceptionDesc = ex.Message;
                    cmtException.ExceptionDateTime = DateTime.Now;
                    cmtException.UserDescription = "getFWDValue method failed";
                    _logger.LogApplicationException(cmtException);
                }
            }

            return esnoValue;
        }

        /// <summary>
        /// Returns a list of best effort traffic consumption
        /// </summary>
        /// <param name="macAddress">The mac address of the terminal</param>
        /// <param name="startDateTime">Start date/time of the query</param>
        /// <param name="endDateTime">End date/time of the query</param>
        /// <param name="ispId"> </param>
        /// <returns></returns>
        public List<Model.TrafficDataPoint> getTrafficViews(string macAddress, DateTime startDateTime, DateTime endDateTime, int ispId)
        {
            BOAccountingController accountingController =
                                                new BOAccountingController(_connectionString, _databaseProvider);

            CMTTerminal.Terminal term = accountingController.GetTerminalDetailsByMAC(macAddress);

            List<CMTTerminal.VolumeInformationTime> realTimeVolumeInfo =
                    accountingController.GetRealTimeVolumeInformation(term.SitId, startDateTime, endDateTime, ispId);

            List<Model.TrafficDataPoint> trafficDataPoints = new List<Model.TrafficDataPoint>();

            foreach (CMTTerminal.VolumeInformationTime vit in realTimeVolumeInfo)
            {
                TrafficDataPoint tp = new TrafficDataPoint();
                PmtDataPoint forwardPdp = new PmtDataPoint();
                forwardPdp.timestamp = DateHelper.DateToEpoch(vit.TimeStamp).ToString();
                forwardPdp.sitId = term.SitId.ToString();
                forwardPdp.value = vit.Forward.ToString();
                tp.FwdDataPoint = forwardPdp;

                PmtDataPoint returnPdp = new PmtDataPoint();
                returnPdp.timestamp = DateHelper.DateToEpoch(vit.TimeStamp).ToString();
                returnPdp.sitId = term.SitId.ToString();
                returnPdp.value = vit.Return.ToString();
                tp.RtnDataPoint = returnPdp;

                trafficDataPoints.Add(tp);
            }

            return trafficDataPoints;
        }

        /// <summary>
        /// Returns the accumulated return and forward values from the 
        /// 30 minute (real time) views.
        /// </summary>
        /// <remarks>
        /// Use this method to retrieve 30 minute accumulated value samples
        /// </remarks>
        /// <param name="macAddress"></param>
        /// <param name="startDateTime"></param>
        /// <param name="endDateTime"></param>
        /// <param name="ispId"></param>
        /// <returns></returns>
        public List<Model.TrafficDataPoint> getAccumulatedTrafficViews(string macAddress, DateTime startDateTime, DateTime endDateTime, int ispId)
        {
            BOAccountingController accountingController =
                                                new BOAccountingController(_connectionString, _databaseProvider);

            CMTTerminal.Terminal term = accountingController.GetTerminalDetailsByMAC(macAddress);

            List<CMTTerminal.VolumeInformationTime> realTimeVolumeInfo =
                    accountingController.GetAccumulatedVolumeInformationFromRT(term.SitId, startDateTime, endDateTime, ispId);

            List<Model.TrafficDataPoint> trafficDataPoints = new List<Model.TrafficDataPoint>();

            foreach (CMTTerminal.VolumeInformationTime vit in realTimeVolumeInfo)
            {
                TrafficDataPoint tp = new TrafficDataPoint();
                PmtDataPoint forwardPdp = new PmtDataPoint();
                forwardPdp.timestamp = DateHelper.DateToEpoch(vit.TimeStamp).ToString();
                forwardPdp.sitId = term.SitId.ToString();
                forwardPdp.value = vit.Forward.ToString();
                tp.FwdDataPoint = forwardPdp;

                PmtDataPoint returnPdp = new PmtDataPoint();
                returnPdp.timestamp = DateHelper.DateToEpoch(vit.TimeStamp).ToString();
                returnPdp.sitId = term.SitId.ToString();
                returnPdp.value = vit.Return.ToString();
                tp.RtnDataPoint = returnPdp;

                trafficDataPoints.Add(tp);
            }

            return trafficDataPoints;
        }

        /// <summary>
        /// Returns a list of high priority traffic consumption
        /// </summary>
        /// <param name="macAddres">The mac address of the terminal</param>
        /// <param name="startDateTime">Start date/time of the query</param>
        /// <param name="endDateTime">End date/time of the query</param>
        /// <returns></returns>
        public List<ISPIF.Model.TrafficDataPoint> getHighPriorityTrafficViews(string macAddress, DateTime startDateTime, DateTime endDateTime, int ispId)
        {
            BOAccountingController accountingController =
                                                new BOAccountingController(_connectionString, _databaseProvider);

            CMTTerminal.Terminal term = accountingController.GetTerminalDetailsByMAC(macAddress);

            List<CMTTerminal.VolumeInformationTime> highPriorityVolumeInfo =
                    accountingController.GetPriorityVolumeInformation(term.SitId, startDateTime, endDateTime, ispId);

            List<Model.TrafficDataPoint> trafficDataPoints = new List<Model.TrafficDataPoint>();

            foreach (CMTTerminal.VolumeInformationTime vit in highPriorityVolumeInfo)
            {
                TrafficDataPoint tp = new TrafficDataPoint();
                PmtDataPoint forwardPdp = new PmtDataPoint();
                forwardPdp.timestamp = DateHelper.DateToEpoch(vit.TimeStamp).ToString();
                forwardPdp.sitId = term.SitId.ToString();
                forwardPdp.value = vit.Forward.ToString();
                tp.FwdDataPoint = forwardPdp;

                PmtDataPoint returnPdp = new PmtDataPoint();
                returnPdp.timestamp = DateHelper.DateToEpoch(vit.TimeStamp).ToString();
                returnPdp.sitId = term.SitId.ToString();
                returnPdp.value = vit.Return.ToString();
                tp.RtnDataPoint = returnPdp;

                trafficDataPoints.Add(tp);
            }

            return trafficDataPoints;
        }


        /// <summary>
        /// Returns a list of accumulated volume data over one month
        /// </summary>
        /// <param name="macAddress">The MAC address of the terminal</param>
        /// <returns>A list of TrafficDataPoints</returns>
        public List<Model.TrafficDataPoint> getAccumulatedViews(string macAddress, int ispId)
        {
            BOAccountingController accountingController =
                                                new BOAccountingController(_connectionString, _databaseProvider);

            CMTTerminal.Terminal term = accountingController.GetTerminalDetailsByMAC(macAddress);

            //Determine the end date.
            DateTime endDateTime = DateTime.Now;
            DateTime startDateTime = DateTime.Now.AddDays(-30.0);

            List<CMTTerminal.VolumeInformationTime> realTimeVolumeInfo =
                    accountingController.GetAccumulatedVolumeInformation(term.SitId, startDateTime, endDateTime, ispId);

            List<Model.TrafficDataPoint> trafficDataPoints = new List<Model.TrafficDataPoint>();

            foreach (CMTTerminal.VolumeInformationTime vit in realTimeVolumeInfo)
            {
                TrafficDataPoint tp = new TrafficDataPoint();
                PmtDataPoint forwardPdp = new PmtDataPoint();
                forwardPdp.timestamp = DateHelper.DateToEpoch(vit.TimeStamp).ToString();
                forwardPdp.sitId = term.SitId.ToString();
                forwardPdp.value = vit.Forward.ToString();
                tp.FwdDataPoint = forwardPdp;

                PmtDataPoint returnPdp = new PmtDataPoint();
                returnPdp.timestamp = DateHelper.DateToEpoch(vit.TimeStamp).ToString();
                returnPdp.sitId = term.SitId.ToString();
                returnPdp.value = vit.Return.ToString();
                tp.RtnDataPoint = returnPdp;

                trafficDataPoints.Add(tp);
            }

            return trafficDataPoints;
        }


        /// <summary>
        /// Returns a list of accumulated volume data between a start and end date
        /// </summary>
        /// <param name="macAddress">The MAC address of the terminal</param>
        /// <param name="ispId">The ISP identifier</param>
        /// <returns>A list of TrafficDataPoints</returns>
        public List<Model.TrafficDataPoint> getAccumulatedViewsBetweenDates(string macAddress, int ispId, DateTime startDate, DateTime endDate)
        {
            BOAccountingController accountingController =
                                                new BOAccountingController(_connectionString, _databaseProvider);

            CMTTerminal.Terminal term = accountingController.GetTerminalDetailsByMAC(macAddress);

            List<CMTTerminal.VolumeInformationTime> realTimeVolumeInfo =
                    accountingController.GetAccumulatedVolumeInformation(term.SitId, startDate, endDate, ispId);

            List<Model.TrafficDataPoint> trafficDataPoints = new List<Model.TrafficDataPoint>();

            foreach (CMTTerminal.VolumeInformationTime vit in realTimeVolumeInfo)
            {
                TrafficDataPoint tp = new TrafficDataPoint();
                PmtDataPoint forwardPdp = new PmtDataPoint();
                forwardPdp.timestamp = DateHelper.DateToEpoch(vit.TimeStamp).ToString();
                forwardPdp.sitId = term.SitId.ToString();
                forwardPdp.value = vit.Forward.ToString();
                tp.FwdDataPoint = forwardPdp;

                PmtDataPoint returnPdp = new PmtDataPoint();
                returnPdp.timestamp = DateHelper.DateToEpoch(vit.TimeStamp).ToString();
                returnPdp.sitId = term.SitId.ToString();
                returnPdp.value = vit.Return.ToString();
                tp.RtnDataPoint = returnPdp;

                trafficDataPoints.Add(tp);
            }

            return trafficDataPoints;
        }

        #endregion

        #region Helper methods

        /// <summary>
        /// Completes the TerminalInfo object with volume data obtained from the ISPIF
        /// </summary>
        /// <param name="ti">The completed TerminalInfo object</param>
        public void getVolumeInfo(ref TerminalInfo ti)
        {
            IspSupportInterfaceService ispService = new IspSupportInterfaceService();
            ispService.Credentials = this.readCredentials(Int32.Parse(ti.IspId));
            ispService.PreAuthenticate = true;
            ServicePointManager.Expect100Continue = false;
            int trialIdx = 0;
            bool loopFlag = true;

            while (loopFlag && trialIdx < 4)
            {
                try
                {

                    //Volume info

                    
                    VolumeInfo vi = ispService.getVolumeInfoBySit(ti.SitId, ti.IspId);
                    
                    //ispService.get

                    ti.Additional = vi.additional;
                    ti.Blocked = vi.blocked;
                    ti.Consumed = vi.consumed;
                    ti.ConsumedFreeZone = vi.freezoneReturn;
                    ti.ConsumedReturn = vi.consumedReturn;
                    ti.FreeZone = vi.freezone;
                    ti.TotalAdditional = vi.totalAdditional;
                    ti.TotalAdditionalReturn = vi.totalAdditionalReturn;
                    ti.AdditionalReturn = vi.additionalReturn;

                    //ti.Additional = "0";
                    //ti.Blocked = false;
                    //ti.Consumed = "0";
                    //ti.ConsumedFreeZone = "0";
                    //ti.ConsumedReturn = "0";
                    //ti.FreeZone = "0";
                    //ti.TotalAdditional = "0";
                    //ti.TotalAdditionalReturn = "0";
                    //ti.AdditionalReturn = "0";


                    //RTN View volume
                    PmtDataPoint[] pmtDataPoints = ispService.getPmtDataLastValue(_RTN_VIEW, ti.IspId, ti.SitId);

                    if (pmtDataPoints != null)
                    {
                        ti.RTN = pmtDataPoints[0].value;
                    }
                    else
                    {
                        ti.RTN = "-1.0";
                    }

                    if (ti.RTN.Equals("-1.0"))
                    {
                        ti.Status = "Not Operational";
                    }
                    else
                    {
                        ti.Status = "Operational";
                    }

                    PmtDataPoint[] pmtDataPointsFWD = ispService.getPmtDataLastValue(_FWD_VIEW, ti.IspId, ti.SitId);

                    if (pmtDataPointsFWD != null)
                    {
                        ti.FWD = pmtDataPointsFWD[0].value;
                    }
                    else
                    {
                        ti.FWD = "-1.0";
                    }

                    NbiTerminalInfo nbiTermInfo = ispService.getNbiInfoByMac(ti.MacAddress);
                    ti.RTNPool = nbiTermInfo.returnCapacityGroupId;
                    ti.BladeId = nbiTermInfo.bladeId;
                    ti.FWPool = nbiTermInfo.forwardPoolId;

                    loopFlag = false;
                    ti.ErrorFlag = false;
                    ti.ErrorMsg = "";

                }
                catch (Exception ex)
                {
                    ti.ErrorFlag = true;
                    ti.ErrorMsg = "Terminal not available from Hub";
                    trialIdx++;

                    CmtApplicationException cmtException = new CmtApplicationException();
                    cmtException.ExceptionDesc = ex.Message;
                    cmtException.ExceptionDateTime = DateTime.Now;
                    cmtException.UserDescription = "getVolumeInfo method failed";
                    _logger.LogApplicationException(cmtException);

                }
            }
        }

        /// <summary>
        /// This method returns the ISPIF credentials for the given ISP
        /// </summary>
        /// <param name="macAddress">The target MAC Address</param>
        /// <returns>A NetworkCredential instance or null in case of an error</returns>
        private NetworkCredential readCredentials(int IspId)
        {
            //Get the credentials from the database
            string connectionString =
               ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
            string databaseProviderString =
               ConfigurationManager.ConnectionStrings["ApplicationServices"].ProviderName;

            BOControllerLibrary.Controllers.BOAccountingController boAccountingController =
                            new BOControllerLibrary.Controllers.BOAccountingController(connectionString, databaseProviderString);

            BOControllerLibrary.Model.ISP.Isp isp = boAccountingController.GetISP(IspId);
            NetworkCredential cred = new NetworkCredential(isp.Ispif_User, isp.Ispif_Password);
            return cred;
        }

        /// <summary>
        /// The ISPIF User name
        /// </summary>
        /// <remarks>
        /// This property is read-only
        /// </remarks>
        public string ISPIFUserName
        {
            get
            {
                NameValueCollection appSettings = ConfigurationManager.AppSettings;

                if (appSettings.Count == 0)
                {
                    return "";
                }
                else
                {
                    return appSettings["ISPIFUserName"];
                }
            }
        }

        /// <summary>
        /// ISPIFPwd
        /// </summary>
        public string ISPIFPwd
        {
            get
            {
                NameValueCollection appSettings = ConfigurationManager.AppSettings;

                if (appSettings.Count == 0)
                {
                    return "";
                }
                else
                {
                    return appSettings["ISPIFPwd"];
                }
            }
        }

        /// <summary>
        /// The ISP Identifier
        /// </summary>
        public string ISPId
        {
            get
            {
                NameValueCollection appSettings = ConfigurationManager.AppSettings;

                if (appSettings.Count == 0)
                {
                    return "";
                }
                else
                {
                    return appSettings["ISPId"];
                }
            }
        }

        #endregion
    }
}
