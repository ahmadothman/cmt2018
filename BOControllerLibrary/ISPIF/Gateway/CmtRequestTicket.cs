﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BOControllerLibrary.ISPIF.Gateway
{
    /// <summary>
    /// Encapsulates a Request Ticket as it is returned by the ISPIF lookupRequestTicket method
    /// </summary>
    public class CmtRequestTicket
    {
        public string id { set; get; }
        public string failureReason { set; get; }
        public CmtRequestStatus? requestStatus { set; get; }
        public string failureStackTrace { set; get; }
        public bool NMSFlag { set; get; }
    }
}