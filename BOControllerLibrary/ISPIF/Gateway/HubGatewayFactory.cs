﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BOControllerLibrary.ISPIF.Gateway.Interface;

namespace BOControllerLibrary.ISPIF.Gateway
{
    /// <summary>
    /// Returns the correct gateway implementation depending on the given
    /// ISP ID
    /// </summary>
    public class HubGatewayFactory
    {
        /// <summary>
        /// This method returns the hub gateway implementation corresponding to the
        /// given ISPId.
        /// </summary>
        /// <remarks>
        /// For a future implementation of this method we should think about a 
        /// configurable system which loads the IHubGateways dynamically
        /// </remarks>
        /// <param name="ISPId">The ISP Id which determines the hub implementaiton</param>
        /// <returns>A valid hub gateway implementation or null if none could be  found</returns>
        public static IHubGateWay GetHubGateway(int ISPId)
        {
            //const int ISPIF_601 = 601; //Eutelsat hub ISP 601
            //const int ISPIF_602 = 602; //Eutelsat hub ISP 602
            //const int ISPIF_603 = 603; //Eutelsat hub ISP 603
            //const int ISPIF_604 = 604; //Eutelsat hub ISP 604
            //const int ISPIF_605 = 605; //Eutelsat hub ISP 605
            //const int ISPIF_606 = 606; //Eutelsat hub ISP 606
            //const int ISPIF_607 = 607; //Eutelsat hub ISP 607
            //const int ISPIF_608 = 608; //Eutelsat hub ISP 608
            //const int ISPIF_609 = 609; //Eutelsat hub ISP 608
            //const int ISPIF_701 = 701; //Eutelsat hub ISP 701
            //const int ISPIF_702 = 702; //Eutelsat hub ISP 702
            //const int ISPIF_703 = 703; //Eutelsat hub ISP 703
            //const int ISPIF_704 = 704; //Eutelsat hub ISP 704
            //const int ISPIF_705 = 705; //Eutelsat hub ISP 705
            //const int ISPIF_150 = 150; //SES Hub ISP 150
            //const int ISPIF_112 = 112; //SES Hub ISP 112
            //const int ISPIF_348 = 348; //Avanti hub ISP 348
            //const int ISPIF_349 = 349; //NMS1 ISP 349
            //const int ISPIF_412 = 412; //SES Hub ISP 412
            //const int ISPIF_469 = 469; //SES NMS ISP 469
            //const int ISPIF_473 = 473; //SES Hub ISP 473 (SOHO)
            //const int ISPIF_512 = 512; //Emulator test ISP
            //const int ISPIF_411 = 411; //NMS1 ISP 411
            //const int ISPIF_476 = 476; //NMS1 ISP 476
            //const int ISPIF_415 = 415; //NMS1 ISP 410
            //const int ISPIF_416 = 416; //NMS1 ISP 416 (High speed service)
            //const int ISPIF_417 = 417; //NMS1 ISP 410
            //const int ISPIF_480 = 480; //SES Dialog hub Astra 2G
            //const int ISPIF_201 = 201; //SES Dialog hub Astra 2G

            //IHubGateWay hubGateway = null;

            //switch (ISPId)
            //{
            //    case ISPIF_150:
            //        hubGateway = new ISPIFGateway();
            //        break;

            //    case ISPIF_112:
            //        hubGateway = new ISPIFGateway();
            //        break;

            //    case ISPIF_348:
            //        hubGateway = new AvantiNBIGateway();
            //        break;

            //    case ISPIF_349:
            //        hubGateway = new AvantiNBIGateway();
            //        break;

            //    case ISPIF_411:
            //        hubGateway = new NMSGatewayAdapterExt();
            //        break;

            //    case ISPIF_412:
            //        hubGateway = new ISPIFGateway();
            //        break;

            //    case ISPIF_469:
            //        hubGateway = new NMSGatewayAdapterExt(); 
            //        break;

            //    case ISPIF_512:
            //        hubGateway = new ISPIFEmulatorGateWay();
            //        break;

            //    case ISPIF_473:
            //        hubGateway = new ISPIFGateway();
            //        break;

            //    case ISPIF_476:
            //        //hubGateway = new TalAdapter_NMS_476();
            //        hubGateway = new NMSGatewayAdapterExt();
            //        break;

            //    case ISPIF_415:
            //        //hubGateway = new NMSAdapter_415();
            //        hubGateway = new NMSGatewayAdapterExt();
            //        break;

            //    case ISPIF_416:
            //        //hubGateway = new NMSGateway_416();
            //        hubGateway = new NMSGatewayAdapterExt();
            //        break;

            //    case ISPIF_417:
            //        //hubGateway = new NMSAdapter_415();
            //        hubGateway = new NMSGatewayAdapterExt();
            //        break;

            //    case ISPIF_480:
            //        hubGateway = new DialogHubGateway(ISPId);
            //        break;

            //    case ISPIF_201:
            //        hubGateway = new DialogHubGateway(ISPId);
            //        break;

            //    case ISPIF_601:
            //        hubGateway = new EutelsatNBIGateway(ISPId);
            //        break;

            //    case ISPIF_602:
            //        hubGateway = new EutelsatNBIGateway(ISPId);
            //        break;

            //    case ISPIF_603:
            //        hubGateway = new EutelsatNBIGateway(ISPId);
            //        break;

            //    case ISPIF_604:
            //        hubGateway = new EutelsatNBIGateway(ISPId);
            //        break;

            //    case ISPIF_605:
            //        hubGateway = new EutelsatNBIGateway(ISPId);
            //        break;

            //    case ISPIF_606:
            //        hubGateway = new EutelsatNBIGateway(ISPId);
            //        break;

            //    case ISPIF_607:
            //        hubGateway = new EutelsatNBIGateway(ISPId);
            //        break;

            //    case ISPIF_608:
            //        hubGateway = new EutelsatNBIGateway(ISPId);
            //        break;

            //    case ISPIF_609:
            //        hubGateway = new EutelsatNBIGateway(ISPId);
            //        break;

            //    case ISPIF_701:
            //        hubGateway = new EutelsatNBIGateway(ISPId);
            //        break;

            //    case ISPIF_702:
            //        hubGateway = new EutelsatNBIGateway(ISPId);
            //        break;

            //    case ISPIF_703:
            //        hubGateway = new EutelsatNBIGateway(ISPId);
            //        break;

            //    case ISPIF_704:
            //        hubGateway = new EutelsatNBIGateway(ISPId);
            //        break;

            //    case ISPIF_705:
            //        hubGateway = new EutelsatNBIGateway(ISPId);
            //        break;

            //    default:
            //        hubGateway = new NMSGatewayAdapterExt();
            //        break;
            //}

            //return hubGateway;

            //SDPDEV-35
            IHubGateWay hubGateway = null;
            bool gatewayFound = false;

            if (ISPId >= 100 && ISPId <= 150)
            {
                hubGateway = new ISPIFGateway();
                gatewayFound = true;
            }
            else if (ISPId >= 151 && ISPId <= 199)
            {
                hubGateway = new MTiDirect();
                gatewayFound = true;
            }
            else if (ISPId == 200)
            {
                hubGateway = new DialogHubGateway(ISPId);
                gatewayFound = true;
            }
            else if (ISPId >= 220 && ISPId <= 240)
            {
                hubGateway = new AvantiNBIGateway();
                gatewayFound = true;
            }
            else if (ISPId >= 300 && ISPId <= 350) //Astra3B
            {
                hubGateway = new NMSGatewayAdapterExt();
                gatewayFound = true;
            }
            else if (ISPId >= 402 && ISPId < 410)
            {
                hubGateway = new NMSGatewayAdapterExt();
                gatewayFound = true;
            }
            else if (ISPId == 412 || ISPId == 410)
            {
                hubGateway = new ISPIFGateway();
                gatewayFound = true;
            }
            else if (ISPId >= 413 && ISPId <= 472)
            {
                hubGateway = new NMSGatewayAdapterExt();
                gatewayFound = true;
            }
            else if (ISPId == 473)
            {
                hubGateway = new ISPIFGateway();
                gatewayFound = true;
            }
            else if (ISPId >= 474 && ISPId <= 499)
            {
                hubGateway = new NMSGatewayAdapterExt();
                gatewayFound = true;
            }
            else if (ISPId >= 500 && ISPId <= 599)
            {
                //hubGateway = new AvantiNBIGateway();
                hubGateway = new TaliaArabsat();
                gatewayFound = true;
            }
            else if (ISPId >= 600 && ISPId <= 699)
            {
                hubGateway = new EutelsatNBIGateway(ISPId);
                gatewayFound = true;
            }
            else if (ISPId >= 700 && ISPId <= 799)
            {
                hubGateway = new HubEmulator();
                gatewayFound = true;
            }
            else if (ISPId >= 800 && ISPId <= 889)
            {
                hubGateway = new NetworkInvGateway();
                gatewayFound = true;
            }
            else if (ISPId >= 890 && ISPId <= 899)
            {
                hubGateway = new E21BNMSEmulator();
                gatewayFound = true;
            }
            else if (ISPId >= 900 && ISPId <= 999)
            {
                hubGateway = new EutelsatNBIGateway(ISPId);
                gatewayFound = true;
            }
            if (gatewayFound == false)
            {
                hubGateway = new NMSGatewayAdapterExt();
            }

            return hubGateway;
        }
    }
}