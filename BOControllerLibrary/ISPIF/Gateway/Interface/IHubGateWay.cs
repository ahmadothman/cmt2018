﻿using System;
using System.Collections.Generic;
using BOControllerLibrary.ISPIF.Gateway;
using BOControllerLibrary.ISPIF.Model;
using BOControllerLibrary.NMSGatewayServiceRef;

namespace BOControllerLibrary.ISPIF.Gateway.Interface
{
    /// <summary>
    /// Interface which is implemented by Hub Gateway components. The IHubGateway defines the methods
    /// for terminal management which must be implemented for the remote NMS and the SatADSL NMS.
    /// </summary>
    public interface IHubGateWay
    {
        #region SES HUB methods

        /// <summary>
        /// Activates a terminal. The terminal is in a Request state
        /// </summary>
        /// <remarks>
        /// The terminal should be pre-provisioned
        /// </remarks>
        /// <param name="endUserId">The endUserId of the terminal to activate</param>
        /// <param name="macAddress">The MAC address of the terminal to activate</param>
        /// <param name="slaId">The service pack allocated to the terminal</param>
        /// <returns>true if the activation succeeded</returns>
        bool TerminalActivate(string endUserId, string macAddress, long slaId, int ispId, Guid userId);

        /// <summary>
        /// Changes the SLA of the terminal
        /// </summary>
        /// <param name="endUserId">The endUserId of the terminal to activate</param>
        /// <param name="macAddress">The MAC address of the terminal to activate</param>
        /// <param name="newSlaId">The service pack allocated to the terminal</param>
        /// <param name="currentState">The current admin state of the terminal</param>
        /// <returns>true if the application succeeded</returns>
        bool TerminalChangeSla(string endUserId, string macAddress,
                    int newSlaId, BOControllerLibrary.ISPIF.Model.AdmStatus currentState, int ispId, Guid userId);
        
        /// <summary>
        /// Decommissions a terminal. The terminal is effectively removed from the Hub database
        /// </summary>
        /// <param name="endUserId">The endUserId of the terminal to activate</param>
        /// <param name="macAddress">The MAC address of the terminal to activate</param>
        /// <returns>true if the activation succeeded</returns>
        bool TerminalDecommission(string endUserId, string macAddress, int ispId, Guid userId);
        
        /// <summary>
        /// Re-activates a suspended terminal, the status of the terminal becomes
        /// OPERATIONAL
        /// </summary>
        /// <param name="endUserId">The endUserId of the terminal to activate</param>
        /// <param name="macAddress">The MAC address of the terminal to activate</param>
        /// <returns>true if the re-activation succeeded</returns>
        bool TerminalReActivate(string endUserId, string macAddress, int ispId, Guid userId);
        
        /// <summary>
        /// Suspends a terminal, the status of the terminal becomes LOCKED
        /// </summary>
        /// <param name="endUserId">The endUserId of the terminal to activate</param>
        /// <param name="macAddress">The MAC address of the terminal to activate</param>
        /// <returns>true if the re-activation succeeded</returns>
        bool TerminalSuspend(string endUserId, string macAddress, int ispId, Guid userId);
        
        /// <summary>
        /// FUP (Volume) reset
        /// </summary>
        /// <param name="endUserId">The endUserId of the terminal to activate</param>
        /// <param name="ispId">The ISP identifier</param>
        /// <returns>true if the operation succeeded</returns>
        bool TerminalUserReset(string endUserId, int ispId);

        /// <summary>
        /// Returns the status of a request ticket.
        /// </summary>
        /// <remarks>
        /// This method maps on the ISPIF lookupRequestTicket 
        /// </remarks>
        /// <param name="requestTicketId">A class which represents a Request ticket</param>
        /// <returns>In this case a CMTRequestTicket is returned by the method</returns>
        CmtRequestTicket LookupRequestTicket(string requestTicketId, int ispId);

        /// <summary>
        /// Add volume to a subscription.
        /// </summary>
        /// <remarks>
        /// The subscription must have a voucher based SLA
        /// </remarks>
        /// <param name="sitId">The SIT Identifier of the terminal</param>
        /// <param name="volumeCode">The volume code corresponding to a voucher volume code</param>
        /// <param name="ispId">The ISP Identifier</param>
        /// <returns>True if the operation succeeded</returns>
        bool AddVolume(int sitId, int volumeCode, int ispId);

        /// <summary>
        /// Changes the MacAddress of a terminal
        /// </summary>
        /// <remarks>
        /// This is an asynchronous operation which changes the data in the CMT database and
        /// the HUB!
        /// </remarks>
        /// <param name="endUserId">The id (full terminal name) of the target terminal</param>
        /// <param name="oriMacAddress">The original Mac Address</param>
        /// <param name="newMacAddress">The new mac address</param>
        /// <param name="ispId">The unique identifier of the ISP</param>
        /// <returns>True if the operation succeeded, false otherwise</returns>
        bool ChangeMacAddress(string endUserId, string oriMacAddress, string newMacAddress, int ispId, Guid userId);

        /// <summary>
        /// Returns information from the CMT database for a terminal which is identified by its MAC address.
        /// </summary>
        /// <remarks>
        /// This method returns the data which is stored in the CMT database and not from the remote hub, except
        /// for the point value. This value is retrieved from the teleport hub.
        /// </remarks>
        /// <param name="macAddress">The Terminals' MAC address</param>
        /// <returns>A TerminalInfo instance or null if the terminal does not exist</returns>
        TerminalInfo getTerminalInfoByMacAddress(string macAddress);

        /// <summary>
        /// Returns information from the remote hub for a terminal which is identified by its MAC address AND the ISP Id
        /// </summary>
        ///  <remarks>
        /// This method returns the data from the teleport hub and NOT from the CMT database. Use this method for 
        /// synchronization purposes etc.
        /// </remarks>
        /// <param name="macAddress">The MAC address of the terminal</param>
        /// <param name="ispId">The ISP Identifer of the ISP the terminal belongs to</param>
        /// <returns>A TerminalInfo instance or null if the terminal could not be found</returns>
        TerminalInfo getTerminalDetailsFromHub(string macAddress, string ispId);

        /// <summary>
        /// This method retrieves the terminal information from the SES Hub
        /// This override method is used for MAC changes
        /// </summary>
        /// <param name="oldMacAddress">The old terminal MAC address. This MAC address is still in use in the CMT at this point</param>
        /// <param name="newMacAddress">The new terminal MAC address. This MAC address is already being used in the hub</param>
        /// <param name="ispId">The ISP identifier, not used in this case as we need to use the
        /// SES HUB Isp!</param>
        /// <returns>A completed TerminalInfo object</returns>
        TerminalInfo getTerminalDetailsFromHubMacChange(string oldMacAddress, string newMacAddress, string ispId);

        /// <summary>
        /// Returns the Point value (C/No)
        /// </summary>
        /// <param name="ispId">The ISP Identifier the terminal belongs to</param>
        /// <param name="sitId">The Sit Id of the terminal</param>
        /// <returns></returns>
        string getRTNValue(string ispId, string sitId);

        /// <summary>
        /// Returns the Point value (Es/No)
        /// </summary>
        /// <param name="ispId">The ISP Identifier the terminal belongs to</param>
        /// <param name="sitId">The Sit Id of the terminal</param>
        /// <returns></returns>
        string getFWDValue(string ispId, string sitId);

        /// <summary>
        /// Returns a list of best effort traffic data
        /// </summary>
        /// <param name="macAddress">The mac address of the terminal</param>
        /// <param name="startDateTime">Start date of the query</param>
        /// <param name="endDateTime">End date of the query</param>
        /// <param name="ispId">The ISP identifier the terminal belongs to</param>
        /// <returns>A list of traffic data points</returns>
        List<TrafficDataPoint> getTrafficViews(string macAddress, DateTime startDateTime, DateTime endDateTime, int ispId);

        /// <summary>
        /// Returns a list of best effort accumulated data
        /// </summary>
        /// <remarks>
        /// These values are taken from the 30 minute volume consumption reports
        /// </remarks>
        /// <param name="macAddress"></param>
        /// <param name="startDateTime"></param>
        /// <param name="endDateTime"></param>
        /// <param name="ispId"></param>
        /// <returns></returns>
        List<Model.TrafficDataPoint> getAccumulatedTrafficViews(string macAddress, DateTime startDateTime, DateTime endDateTime, int ispId);

        /// <summary>
        /// Returns a list of high priority traffic data
        /// </summary>
        /// <param name="macAddres">The mac address of the terminal</param>
        /// <param name="startDateTime">Start date of the query</param>
        /// <param name="endDateTime">End date of the query</param>
        /// <param name="ispId">The ISP identifier the terminal belongs to</param>
        /// <returns>A list of TrafficDataPoints, this list can be empty but not null (see model for more information about DataPoints)</returns>
        List<TrafficDataPoint> getHighPriorityTrafficViews(string macAddress, DateTime startDateTime, DateTime endDateTime, int ispId);

        /// <summary>
        /// Returns a list of TrafficDataPoints of volume consumption
        /// </summary>
        /// <param name="macAddress">The mac address of the terminal</param>
        /// <param name="ispId">The ISP identifier the terminal belongs to</param>
        /// <returns>A list of TrafficDataPoints, this list can be empty but not null</returns>
        List<TrafficDataPoint> getAccumulatedViews(string macAddress, int ispId);

        /// <summary>
        /// Returns a list of accumulated volume data between a start and end date
        /// </summary>
        /// <param name="macAddress">The MAC address of the terminal</param>
        /// <param name="ispId">The ISP identifier</param>
        /// <returns>A list of TrafficDataPoints</returns>
        List<TrafficDataPoint> getAccumulatedViewsBetweenDates(string macAddress, int ispId, DateTime startDate, DateTime endDate);

        /// <summary>
        /// Changes the ISP of a terminal
        /// </summary>
        /// <param name="macAddress">The terminals MAC address</param>
        /// <param name="oldISP">The old ISP</param>
        /// <param name="newISP">The new iSP</param>
        /// <returns>True if the operation succeeded</returns>
        bool ChangeISP(string endUserId, int oldISP, int newISP);

        #endregion

        #region Multicast support methods

        /// <summary>
        /// Creates a new multicast group
        /// </summary>
        /// <param name="macAddress">The multicast MAC address, derived from the IP address</param>
        /// <param name="ipAddress">The multicast group IP address</param>
        /// <param name="multiCastName">The multicast group name</param>
        /// <param name="sourceUrl">The source URL</param>
        /// <param name="portNr">The port number</param>
        /// <param name="bandWidth">The bandwidth</param>
        /// <param name="ispId">The ISP this Multicast group belongs to</param>
        /// <returns>True if the operation succeeded</returns>
        bool CreateMulticastGroup(string macAddress, string ipAddress, string multiCastName, string sourceUrl, int portNr, long bandWidth, int ispId, Guid userId);

        /// <summary>
        /// Removes an IP address from a Multicast group
        /// </summary>
        /// <param name="ipAddress">The IP Address of the Multicast group to remove</param>
        /// <returns>True if the operation succeeded</returns>
        bool RemoveIPFromMulticast(string ipAddress);

        /// <summary>
        /// Returns the transferred bytes for the Multicast group specified by the IP Address
        /// </summary>
        /// <param name="ipAddress">The IP Address</param>
        /// <returns>The transferred bytes as a long</returns>
        ulong GetTransferredBytes(string ipAddress);

        /// <summary>
        /// Returns true if the Multicast group identified by the IP Address is enabled
        /// </summary>
        /// <param name="ipAddress">The IP Address which identifies the MC group</param>
        /// <returns>True if the MC is enabled</returns>
        bool IsEnabled(string ipAddress);

        /// <summary>
        /// Enables a MulticastGroup
        /// </summary>
        /// <param name="ipAddress">IP Address of the Multicast group to enable</param>
        /// <returns>true if the command was accepted by the NMS</returns>
        bool EnableMultiCastGroup(string ipAddres, string macAddress, int ispId, Guid userId);

        /// <summary>
        /// Disables a Multicast group
        /// </summary>
        /// <param name="ipAddress">IP Address of the Multicast group to disable</param>
        /// <returns>true if the command was accepted by the NMS</returns>
        bool DisableMultiCastGroup(string ipAddres,string macAddress, int ispId, Guid userId);

        #endregion

        #region NMS Support Methods

        /*
         * These methods are used to manage terminals by means of the NMS Gateway. If only a
         * one step procedure is needed to change the terminals' status etc. thse methods can
         * be kept empty.
         */

        /// <summary>
        /// Activates a terminal in the NMS by specifying a range of IP addresses.
        /// </summary>
        /// <param name="endUserId">The endUserId of the terminal to activate</param>
        /// <param name="macAddress">The MAC address of the terminal to activate</param>
        /// <param name="slaId">The service pack allocated to the terminal</param>
        /// <param name="ipMask">The IP Mask used to determine the IP range</param>
        /// <param name="freeZone">The freezone calendar associated with this terminal, 0 if not associated with
        /// a freezone SLA</param>
        /// <returns>true if the activation succeeded</returns>
        bool NMSTerminalActivateWithRange(string endUserId, string macAddress, long slaId, int ispId, int ipMask, int freeZone, Guid userId);

        /// <summary>
        /// Activates a terminal in the NMS. The terminal is in a Request state
        /// </summary>
        /// <remarks>
        /// This method activates a terminal without specifying a IP Mask range! So only
        /// one IP address is allocated to the terminal.
        /// </remarks>
        /// <param name="endUserId">The endUserId of the terminal to activate</param>
        /// <param name="macAddress">The MAC address of the terminal to activate</param>
        /// <param name="slaId">The service pack allocated to the terminal</param>
        /// <param name="ipMask">The IP Mask used to determine the IP range</param>
        /// <param name="freeZone">The freezone calendar associated with this terminal, 0 if not associated with
        /// a freezone SLA</param>
        /// <returns>true if the activation succeeded</returns>
        bool NMSTerminalActivate(string endUserId, string macAddress, long slaId, int ispId, int freeZone, Guid userId);

        /// <summary>
        /// Activates a special satellite diversity terminal in the NMS
        /// </summary>
        /// <remarks>
        /// This method activates a terminal without specifying a IP Mask range! So only
        /// one IP address is allocated to the terminal.
        /// </remarks>
        /// <param name="endUserId">The endUserId of the terminal to activate</param>
        /// <param name="macAddress">The MAC address of the terminal to activate</param>
        /// <param name="ipAddress">The IP address of the terminal to be activated</param>
        /// <param name="sitId">The sit ID of the terminal</param>
        /// <param name="slaId">The service pack allocated to the terminal</param>
        /// <param name="ipMask">The IP Mask used to determine the IP range</param>
        /// <param name="freeZone">The freezone calendar associated with this terminal, 0 if not associated with
        /// a freezone SLA</param>
        /// <param name="primaryMac">The MAC address of the primary terminal in the setup</param>
        /// <param name="secondaryMac">The MAC address of the secondary terminal in the setup</param>
        /// <returns>true if the activation succeeded</returns>
        bool NMSActivateSatelliteDiversityTerminal(string endUserId, string macAddress, string ipAddress, long slaId, int ispId, int freeZone, string primaryMac, string secondaryMac, Guid userId);

        /// <summary>
        /// Changes the SLA of the terminal in the NMS
        /// </summary>
        /// <param name="endUserId">The endUserId of the terminal to activate</param>
        /// <param name="macAddress">The MAC address of the terminal to activate</param>
        /// <param name="newSlaId">The service pack allocated to the terminal</param>
        /// <param name="currentState">The current admin state of the terminal</param>
        /// <returns>true if the application succeeded</returns>
        bool NMSTerminalChangeSla(string endUserId, string macAddress,
                    int newSlaId, BOControllerLibrary.ISPIF.Model.AdmStatus currentState, int ispId, Guid userId);

        /// <summary>
        /// Checks if the SLA (which eventually needs to be deleted) has associated terminals
        /// </summary>
        /// <param name="slaId">The identifier of the SLA</param>
        /// <returns>True when the SLA has still associated terminals</returns>
        bool NMSSlaHasAssociatedTerminals(int slaId);

        /// <summary>
        /// Changes the weight of the terminal in the NMS
        /// </summary>
        /// <param name="endUserId">The endUserId of the terminal to activate</param>
        /// <param name="macAddress">The MAC address of the terminal to activate</param>
        /// <param name="newWeight">The new weight allocated to the terminal</param>
        /// <param name="customerId">The ID of the customer the terminal belongs to</param>
        /// <param name="currentState">The current admin state of the terminal</param>
        /// <returns>true if the application succeeded</returns>
        bool NMSTerminalChangeWeight(string endUserId, string macAddress,
                    int newWeight, int customerId, BOControllerLibrary.ISPIF.Model.AdmStatus currentState, int ispId, Guid userId);

        /// <summary>
        /// Decommissions a terminal in the NMS. The terminal is effectively removed from the Hub database
        /// </summary>
        /// <param name="endUserId">The endUserId of the terminal to activate</param>
        /// <param name="macAddress">The MAC address of the terminal to activate</param>
        /// <returns>true if the activation succeeded</returns>
        bool NMSTerminalDecommission(string endUserId, string macAddress, int ispId, Guid userId);

        /// <summary>
        /// Re-activates a suspended terminal in the NMS, the status of the terminal becomes
        /// OPERATIONAL
        /// </summary>
        /// <param name="endUserId">The endUserId of the terminal to activate</param>
        /// <param name="macAddress">The MAC address of the terminal to activate</param>
        /// <returns>true if the re-activation succeeded</returns>
        bool NMSTerminalReActivate(string endUserId, string macAddress, int ispId, Guid userId);

        /// <summary>
        /// Suspends a terminal, the status of the terminal becomes LOCKED
        /// </summary>
        /// <param name="endUserId">The endUserId of the terminal to activate</param>
        /// <param name="macAddress">The MAC address of the terminal to activate</param>
        /// <returns>true if the re-activation succeeded</returns>
        bool NMSTerminalSuspend(string endUserId, string macAddress, int ispId, Guid userId);

        /// <summary>
        /// FUP (Volume) reset
        /// </summary>
        /// <param name="endUserId">The endUserId of the terminal to activate</param>
        /// <param name="ispId">The ISP identifier</param>
        /// <returns>true if the operation succeeded</returns>
        bool NMSTerminalUserReset(string endUserId, int ispId);

        /// <summary>
        /// Returns the status of a request ticket in the NMS.
        /// </summary>
        /// <remarks>
        /// This method maps on the NMS Gateway lookupRequestTicket 
        /// </remarks>
        /// <param name="requestTicketId"></param>
        /// <returns></returns>
        CmtRequestTicket NMSLookupRequestTicket(string requestTicketId, int ispId);

        /// <summary>
        /// Add volume to a subscription in the NMS.
        /// </summary>
        /// <remarks>
        /// The subscription must have a voucher based SLA
        /// </remarks>
        /// <param name="sitId"></param>
        /// <param name="volumeCode"></param>
        /// <param name="ispId"></param>
        /// <returns>true if the operationa succeeded</returns>
        bool NMSAddVolume(int sitId, int volumeCode, int ispId);

        /// <summary>
        /// Changes the MacAddress of a terminal in the NMS
        /// </summary>
        /// <remarks>
        /// This is an asynchronous operation which changes the data in the CMT database and
        /// the HUB!
        /// </remarks>
        /// <param name="endUserId">The id (full terminal name) of the target terminal</param>
        /// <param name="oriMacAddress">The original Mac Address</param>
        /// <param name="newMacAddress">The new mac address</param>
        /// <param name="ispId">The unique identifier of the ISP</param>
        /// <returns>true if the operation succeeded, false otherwise</returns>
        bool NMSChangeMacAddress(string endUserId, string oriMacAddress, string newMacAddress, int ispId, Guid userId);

        /// <summary>
        /// Changes the ISP of a terminal in the NMS
        /// </summary>
        /// <param name="macAddress">The terminals MAC address</param>
        /// <param name="oldISP">The old ISP</param>
        /// <param name="newISP">The new iSP</param>
        /// <returns>True if the operation succeeded</returns>
        bool NMSChangeISP(string endUserId, int oldISP, int newISP);

        /// <summary>
        /// Creates a new service pack (SLA) in the NMS
        /// </summary>
        /// <param name="servicePack">The service pack to be created</param>
        /// <returns>True if the operation succeeded</returns>
        bool NMSCreateServicePack(ServicePack servicePack);

        /// <summary>
        /// Updates an existing service pack (SLA) in the NMS
        /// </summary>
        /// <param name="servicePack">The service pack to be updated</param>
        /// <returns>True if the operation succeeded</returns>
        bool NMSUpdateServicePack(ServicePack servicePack);

        /// <summary>
        /// Deletes an existing service pack (SLA) from the NMS
        /// </summary>
        /// <param name="slaId">The identifier of the service pack to be deleted</param>
        /// <returns>True if the operation succeeded</returns>
        bool NMSDeleteServicePack(int slaId);

        /// <summary>
        /// Creates a freezone in the NMS
        /// </summary>
        /// <param name="freeZone">The freezone to be created</param>
        /// <returns>True if succesful</returns>
        bool NMScreateFreeZone(FreeZone freeZone);

        /// <summary>
        /// Updates a freezone in the NMS
        /// </summary>
        /// <param name="freeZone">The freezone to be updated</param>
        /// <returns>True if succesful</returns>
        bool NMSupdateFreeZone(FreeZone freeZone);

        /// <summary>
        /// This method allows to update the freezone information for a terminal
        /// </summary>
        /// <param name="FreeZoneId">The identifier of the freezone allocated to the terminal</param>
        /// <param name="macAddress">The MAC address of the terminal</param>
        /// <param name="FreeZoneFlag">If this flag is set to true, a freezone option is allocated to the terminal. In
        /// case this value is false, the terminal does not have the freezone option</param>
        /// <returns></returns>
        bool NMSModifyFreeZoneForTerminal(string endUserId, int freeZoneId);

        /// <summary>
        /// Fetches a list of all freezones in the NMS database
        /// </summary>
        /// <returns>The list of freezones</returns>
        List<FreeZone> NMSgetFreeZones();

        /// <summary>
        /// Fetches the details of a specific freezone from the NMS
        /// </summary>
        /// <param name="freeZoneId">The ID of the freezone</param>
        /// <returns>The freezone</returns>
        FreeZone NMSgetFreeZone(int freeZoneId);

        /// <summary>
        /// This method allows to change the IP Address AND Mask in the SatADSL NMS
        /// </summary>
        /// <remarks>
        /// The old and new IP Masks may be identical while the old and new IP Addresses
        /// can never be identical.
        /// </remarks>
        /// <param name="endUserId">The unique end user Id</param>
        /// <param name="macAddress">The terminals' MAC address</param>
        /// <param name="oldIPAddress">The original IP Address</param>
        /// <param name="oldIPMask">The original IP Mask</param>
        /// <param name="newIPAddress">The new IP Address</param>
        /// <param name="newIPMask">The new IP Mask</param>
        /// <param name="ispId">The ISP Identifier</param>
        /// <returns>True if the change was registered by NMS successfully</returns>
        bool NMSChangeIPAddress(string endUserId, string macAddress, string oldIPAddress, int oldIPMask,
                                                    string newIPAddress, int newIPMask, int ispId, Guid userId);

        #endregion


    }
}
