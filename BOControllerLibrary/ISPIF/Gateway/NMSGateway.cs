﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Net;
using System.Globalization;
using System.Data.SqlClient;

using BOControllerLibrary.ISPIF.Gateway.Interface;
using BOControllerLibrary.Controllers;
using BOControllerLibrary.Controllers.Interfaces;
using BOControllerLibrary.ISPIF.Model;
using BOControllerLibrary.Model;
using BOControllerLibrary.Model.Log;
using BOControllerLibrary.Model.Voucher;
using CMTTerminal = BOControllerLibrary.Model.Terminal;
using net.nimera.supportlibrary;

//Astra2connect webservice
using ispif = BOControllerLibrary.com.astra2connect.ispif;
using nms = BOControllerLibrary.NMSGatewayServiceRef;

namespace BOControllerLibrary.ISPIF.Gateway
{
    /// <summary>
    /// Access to the NMSHubGatewayService. This TAL adapter corresponds with ISP 411
    /// </summary>
    /// <remarks>
    /// This implementation of the IHubGateway interface is used when managing terminals in 
    /// the NMS. These kind of terminals need to be managed both in the SES hub AND in the 
    /// NMS. The procedure works as follows (terminal activiation taken as an example):
    /// <ul>
    ///     <li>First the terminal is activated in the SES HUB with an unlimited SLA. Since
    ///     this is an activation which needs a follow-up activation in the NMS the NMSActivity
    ///     status code is set to 1. This means that the TicketMaster, once the activation is 
    ///     confirmed by the ISPIF, must activate the terminal in the NMS.
    ///     </li>
    ///     <li>The TicketMaster polls the ISPIF for the result of the activation</li>
    ///     <l>In case the activation succeeds, the ticket is set to successful completion. Otherwise
    ///     the ticket is set to failure and the operation stops at this point</l>
    ///     <li>The TicketMaster reads the terminal information for the newly created terminal from
    ///     the ISPIF (GetTerminalInformationFromHub) to get the allocated IP address</li>
    ///     <li>The TicketMaster next activates the terminal in the NMS by calling the NMSTerminalActivation
    ///     method</li>
    ///     <li>The newly received ticket, this time from the NMS is stored in the Ticket table with the
    ///     NMSActivation status set to 2. This indicates that for this ticket the status must be retrieved
    ///     from the NMS and NOT from the ISPIF</li>
    ///     <li>If the ticket is successfully completed by the NMS the status of the ticket is set to
    ///     Successful and the admin status of the terminal is set to activated (unlocked)</li>
    /// </ul>
    /// This concludes the procedure for an activation of a terminal in the NMS. The process is similar for the
    /// other management activities.
    /// </remarks>
    public class NMSGateway : IHubGateWay
    {
        protected ispif.IspSupportInterfaceService _ispService = null;
        protected nms.NMSGatewayService _nmsService = null;
        protected IBOLogController _controller;
        protected IBOAccountingController _accountingController;
        protected IBOConfigurationController _configurationController;
        protected IBOVoucherController _voucherController;
        protected SatADSLHubController _satADSLHubController = null; //necessary for accessing SES terminal information

        const string _RTN_VIEW = "CNO_5M"; //Constant for getting the RTN value from the ISPIF DB
        protected const short MAX_RETRY = 5; //Retry ISPIF communication
        protected string DEF_SLA_NAME = "DefSla410";
        protected string SES_HUB_ISP = "SESIsp";

        /// <summary>
        /// Default constructor
        /// </summary>
        public NMSGateway()
            : this("DefSla410", "SESIsp")
        {
        }

        /// <summary>
        /// Constructor which allows to set the default SLA Name and the default Hub reference
        /// </summary>
        /// <remarks>
        /// The default Hub reference, links to the default hub ISP which is stored in the
        /// configuration table of the CMT Database.
        /// </remarks>
        public NMSGateway(string defaultSlaName, string hubReference)
        {
            DEF_SLA_NAME = defaultSlaName;
            SES_HUB_ISP = hubReference;
            _ispService = new ispif.IspSupportInterfaceService();
            _nmsService = new nms.NMSGatewayService();
            _controller = new BOLogController(
                connectionString: ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString,
                databaseProvider: ConfigurationManager.ConnectionStrings["ApplicationServices"].ProviderName);

            _accountingController = new BOAccountingController(connectionString: ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString,
                databaseProvider: ConfigurationManager.ConnectionStrings["ApplicationServices"].ProviderName);
            _configurationController = new BOConfigurationController(
                connectionString: ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString,
                databaseProvider: ConfigurationManager.ConnectionStrings["ApplicationServices"].ProviderName);
            _satADSLHubController = new SatADSLHubController();
            _voucherController = new BOVoucherController(
                connectionString: ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString,
                databaseProvider: ConfigurationManager.ConnectionStrings["ApplicationServices"].ProviderName);
        }

        #region IHubGateWay Members

        /// <summary>
        /// Creates a terminal in the SES Hub by means of the ISPIF gateway. 
        /// This method sets the NMSActivate flag which indicates that the 
        /// TicketMaster still needs to create the terminal in the NMS.
        /// </summary>
        /// <param name="endUserId">The end user identifier related to the terminal</param>
        /// <param name="macAddress">The terminals' MAC address</param>
        /// <param name="slaId">
        /// The Service Level identifier. For this version of the IHubGateway interface
        /// the slaId is retrieved from the configuration table since the sla is NOT the sla for the 
        /// activation in the NMS!
        /// </param>
        /// <param name="ispId">The ISP id</param>
        /// <returns>true if the operation succeeds</returns>
        public virtual bool TerminalActivate(string endUserId, string macAddress, long slaId, int ispId, Guid userId)
        {
            bool result = false;
            int retryCounter = 0;

            //set slaid to value from configuration table
            long HubSlaId = long.Parse(_configurationController.getParameter(DEF_SLA_NAME));

            //We are not using the ISPID passed by the method to activate the terminal on the
            //SES Hub but instead use the default "no limit" ISP from the configuration table
            int sesISPId = int.Parse(_configurationController.getParameter(SES_HUB_ISP));


            while (retryCounter < MAX_RETRY && !result)
            {
                try
                {
                    //Set authentication credentials and WS settings                    
                    _ispService.Credentials = this.readCredentials(sesISPId);
                    _ispService.PreAuthenticate = true;
                    ServicePointManager.Expect100Continue = false;

                    CMTTerminal.Terminal term = _accountingController.GetTerminalDetailsByMAC(macAddress);
                    ispif.RequestTicket rt = _ispService.createRegistration(endUserId, macAddress, HubSlaId.ToString(), sesISPId.ToString());
                    result = this.insertRequestTicketWithSla(rt, macAddress, AdmStatus.LOCKED, AdmStatus.OPERATIONAL, TerminalActivityTypes.Activation, ispId, (int)slaId, userId);
                }
                catch (Exception ex)
                {
                    retryCounter++;
                    this.logException(ex, "NMSGateWay.TerminalActivate");
                }
            }
            return result;
        }

        /// <summary>
        /// Changes the SLA of the terminal in the SES hub. To change the SLA in the NMS call the
        /// NMSTerminalChangeSLA method.
        /// </summary>
        /// <param name="endUserId">The endUserId of the terminal to activate</param>
        /// <param name="macAddress">The MAC address of the terminal to activate</param>
        /// <param name="newSlaId">The service pack allocated to the terminal</param>
        /// <param name="currentState">The current admin state of the terminal</param>
        /// <returns>true if the application succeeded</returns>
        public virtual bool TerminalChangeSla(string endUserId, string macAddress, int newSlaId, Model.AdmStatus currentState, int ispId, Guid userId)
        {
            return this.NMSTerminalChangeSla(endUserId, macAddress, newSlaId, currentState, ispId, userId);
        }

        /// <summary>
        /// Decommissions a terminal in the SES hub. 
        /// The terminal is effectively removed from the Hub database
        /// </summary>
        /// <param name="endUserId">The endUserId of the terminal to activate</param>
        /// <param name="macAddress">The MAC address of the terminal to activate</param>
        /// <returns>true if the activation succeeded</returns>
        public virtual bool TerminalDecommission(string endUserId, string macAddress, int ispId, Guid userId)
        {
            bool result = false;
            int retryCounter = 0;

            //We are not using the ISPID passed by the method to activate the terminal on the
            //SES Hub but instead use the default "no limit" ISP from the configuration table
            int sesISPId = int.Parse(_configurationController.getParameter(SES_HUB_ISP));

            while (retryCounter < MAX_RETRY && !result)
            {
                try
                {
                    //Set authentication credentials and WS settings                    
                    _ispService.Credentials = this.readCredentials(sesISPId);
                    _ispService.PreAuthenticate = true;
                    ServicePointManager.Expect100Continue = false;
                    ispif.RequestTicket rt = _ispService.removeRegistration(endUserId, macAddress);
                    result = this.insertRequestTicket(rt, macAddress, AdmStatus.OPERATIONAL, AdmStatus.DECOMMISSIONED, TerminalActivityTypes.Decommissioning, ispId, userId);
                }
                catch (Exception ex)
                {
                    retryCounter++;
                    this.logException(ex, "NMSGatewayService.TerminalDecommission");
                }
            }
            return result;
        }

        /// <summary>
        /// Reactivates a terminal in the SES hub
        /// </summary>
        /// <param name="endUserId"></param>
        /// <param name="macAddress"></param>
        /// <param name="ispId"></param>
        /// <returns></returns>
        public virtual bool TerminalReActivate(string endUserId, string macAddress, int ispId, Guid userId)
        {
            bool result = false;
            int retryCounter = 0;

            //We are not using the ISPID passed by the method to activate the terminal on the
            //SES Hub but instead use the default "no limit" ISP from the configuration table
            int sesISPId = int.Parse(_configurationController.getParameter(SES_HUB_ISP));

            while (retryCounter < MAX_RETRY && !result)
            {
                try
                {
                    //Set authentication credentials and WS settings                    
                    _ispService.Credentials = this.readCredentials(sesISPId);
                    _ispService.PreAuthenticate = true;
                    ServicePointManager.Expect100Continue = false;

                    ispif.RequestTicket rt = _ispService.changeStatus(endUserId, ispif.RegistrationStatus.OPERATIONAL, sesISPId.ToString());
                    result = this.insertRequestTicket(rt, macAddress, AdmStatus.LOCKED, AdmStatus.OPERATIONAL, TerminalActivityTypes.ReActivation, ispId, userId);
                }
                catch (Exception ex)
                {
                    retryCounter++;
                    this.logException(ex, "NMSGateWay.TerminalReActivate");
                }
            }
            return result;
        }

        /// <summary>
        /// Suspends a terminal, the status of the terminal becomes LOCKED
        /// </summary>
        /// <param name="endUserId">The endUserId of the terminal to activate</param>
        /// <param name="macAddress">The MAC address of the terminal to activate</param>
        /// <returns>true if the re-activation succeeded</returns>
        public virtual bool TerminalSuspend(string endUserId, string macAddress, int ispId, Guid userId)
        {
            bool result = false;
            int retryCount = 0;

            //We are not using the ISPID passed by the method to activate the terminal on the
            //SES Hub but instead use the default "no limit" ISP from the configuration table
            int sesISPId = int.Parse(_configurationController.getParameter(SES_HUB_ISP));

            while (retryCount < MAX_RETRY && !result)
            {
                try
                {
                    //Set authentication credentials and WS settings                    
                    _ispService.Credentials = this.readCredentials(sesISPId);
                    _ispService.PreAuthenticate = true;
                    ServicePointManager.Expect100Continue = false;

                    ispif.RequestTicket rt = _ispService.changeStatus(endUserId, ispif.RegistrationStatus.LOCKED, sesISPId.ToString());
                    result = this.insertRequestTicket(rt, macAddress, AdmStatus.OPERATIONAL, AdmStatus.LOCKED, TerminalActivityTypes.Suspension, ispId, userId);
                }
                catch (Exception ex)
                {
                    this.logException(ex, "NMSGateWay.TerminalSuspend");
                }
            }
            return result;
        }

        /// <summary>
        /// FUP (Volume) reset
        /// </summary>
        /// <param name="endUserId">The endUserId of the terminal to activate</param>
        /// <param name="ispId">The ISP identifier</param>
        /// <returns>true if the operation succeeded</returns>
        public virtual bool TerminalUserReset(string endUserId, int ispId)
        {
            bool result = false;
            try
            {
                this.NMSTerminalUserReset(endUserId, ispId);
                result = true;
            }
            catch (Exception ex)
            {
                this.logException(ex, "TerminalUserReset");
            }
            return result;
        }

        /// <summary>
        /// Returns the status of a request ticket.
        /// </summary>
        /// <remarks>
        /// This method maps on the ISPIF lookupRequestTicket 
        /// </remarks>
        /// <param name="requestTicketId">The requestticket identfier</param>
        /// <param name="ispId">The ISP identfier</param>
        /// <returns>
        /// The corresponding CMT request ticket or null if the ticket could not 
        ///  be found
        ///  </returns>
        public virtual CmtRequestTicket LookupRequestTicket(string requestTicketId, int ispId)
        {
            CmtRequestTicket cmtRt = null;
            int retryCounter = 0;
            bool result = false;

            while (retryCounter < MAX_RETRY && !result)
            {
                try
                {
                    //Set authentication and WS settings
                    int sesIspId = int.Parse(_configurationController.getParameter(SES_HUB_ISP));
                    _ispService.Credentials = this.readCredentials(sesIspId);
                    _ispService.PreAuthenticate = true;
                    ServicePointManager.Expect100Continue = false;
                    ispif.RequestTicket rt = _ispService.lookupRequestTicket(requestTicketId);

                    if (rt != null)
                    {
                        cmtRt = new CmtRequestTicket();
                        cmtRt.id = rt.id;
                        cmtRt.failureReason = rt.failureReason;
                        cmtRt.failureStackTrace = rt.failureStackTrace;
                        cmtRt.NMSFlag = true;

                        if (rt.requestStatus == ispif.RequestStatus.BUSY)
                        {
                            cmtRt.requestStatus = CmtRequestStatus.BUSY;
                        }

                        if (rt.requestStatus == ispif.RequestStatus.FAILED)
                        {
                            cmtRt.requestStatus = CmtRequestStatus.FAILED;
                        }

                        if (rt.requestStatus == ispif.RequestStatus.SUCCESSFUL)
                        {
                            cmtRt.requestStatus = CmtRequestStatus.SUCCESSFUL;
                        }
                    }

                    result = true;
                }
                catch (Exception ex)
                {
                    retryCounter++;
                    this.logException(ex, "ISPIFGateWay.LookupRequest");
                }
            }

            return cmtRt;
        }

        /// <summary>
        /// Adds volume to a given sitId according to the given volumeCode. This method is 
        /// not used as such for this implementation of the HubGateway. Instead the NMSAddVolume
        /// method is used.
        /// </summary>
        /// <param name="sitId">The target SitId</param>
        /// <param name="volumeCode">The volume code which determines the volume to add</param>
        /// <param name="ispId">The ISP identifier</param>
        /// <returns>true if the method succeeds</returns>
        public bool AddVolume(int sitId, int volumeCode, int ispId)
        {
            return this.NMSAddVolume(sitId, volumeCode, ispId);
        }

        /// <summary>
        /// Changes the MacAddress in the SES Hub
        /// </summary>
        /// <param name="endUserId"></param>
        /// <param name="oriMacAddress"></param>
        /// <param name="newMacAddress"></param>
        /// <param name="ispId"></param>
        /// <returns></returns>
        public virtual bool ChangeMacAddress(string endUserId, string oriMacAddress, string newMacAddress, int ispId, Guid userId)
        {
            bool result = false;

            //We are not using the ISPID passed by the method to activate the terminal on the
            //SES Hub but instead use the default "no limit" ISP from the configuration table
            int sesISPId = int.Parse(_configurationController.getParameter(SES_HUB_ISP));

            try
            {
                int retryCounter = 0;

                while (retryCounter < MAX_RETRY && !result)
                {
                    try
                    {
                        //Set authentication credentials and WS settings
                        _ispService.Credentials = this.readCredentials(sesISPId);
                        _ispService.PreAuthenticate = true;
                        ServicePointManager.Expect100Continue = false;
                        ispif.RequestTicket rt = _ispService.changeTerminal(endUserId, newMacAddress, sesISPId.ToString());
                        result = this.insertRequestTicketWithMac(rt, oriMacAddress, AdmStatus.OPERATIONAL, AdmStatus.OPERATIONAL, TerminalActivityTypes.ChangeMac, ispId, newMacAddress, userId);
                    }
                    catch (Exception ex)
                    {
                        retryCounter++;
                        this.logException(ex, "ChangeMacAddress");
                        result = false;
                    }
                }

            }
            catch (Exception ex)
            {
                this.logException(ex, "ISPIFEmulatorGateWay.ChangeMacAddress failed for: " + endUserId);
            }
            return result;
        }

        /// <summary>
        /// Changes the ISP of a terminal
        /// </summary>
        /// <param name="macAddress">The terminals MAC address</param>
        /// <param name="oldISP">The old ISP</param>
        /// <param name="newISP">The new iSP</param>
        /// <returns>True if the operation succeeded</returns>
        public virtual bool ChangeISP(string endUserId, int oldISP, int newISP)
        {
            bool result = false;

            try
            {
                //Set authentication credentials and WS settings
                _ispService.Credentials = this.readCredentials(oldISP);
                _ispService.PreAuthenticate = true;
                ServicePointManager.Expect100Continue = false;
                _ispService.reassignRegistration(endUserId, oldISP.ToString(), newISP.ToString());
                result = true;
            }
            catch (Exception ex)
            {
                this.logException(ex, "ChangeISP");
                result = false;
            }

            return result;
        }
        #endregion

        #region Multicast group support methods

        /// <summary>
        /// Creates a new multicast group
        /// </summary>
        /// <remarks>
        /// By default the Multicast group is set in the disabled state. The TicketMaster enables
        /// the Multicastgroup as soon as the ticket is returned successful
        /// </remarks>
        /// <param name="macAddress">The multicast MAC address, derived from the IP address</param>
        /// <param name="ipAddress">The multicast group IP address</param>
        /// <param name="multiCastName">The multicast group name</param>
        /// <param name="sourceUrl">The source URL</param>
        /// <param name="portNr">The port number</param>
        /// <param name="bandWidth">The bandwidth</param>
        /// <returns>True if the operation succeeded</returns>
        public bool CreateMulticastGroup(string macAddress, string ipAddress, string multiCastName, string sourceUrl, int portNr, long bandWidth, int ispId, Guid userId)
        {
            bool result = false;
            try
            {
                nms.RequestTicket rt = _nmsService.CreateMulticastGroup(macAddress, ipAddress, multiCastName, sourceUrl, portNr, bandWidth, 0);
                result = this.insertRequestTicket(rt, macAddress, AdmStatus.OPERATIONAL, AdmStatus.OPERATIONAL, TerminalActivityTypes.EnableStream, ispId, userId);
            }
            catch (Exception ex)
            {
                result = false;
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDateTime = DateTime.UtcNow;
                cmtEx.ExceptionDesc = ex.Message;
                cmtEx.ExceptionLevel = (short)ExceptionLevelEnum.Critical;
                cmtEx.ExceptionStacktrace = "Could not create Multicast group: " + ipAddress;
                cmtEx.StateInformation = "Critical error";
            }
            return result;
        }

        /// <summary>
        /// Removes an IP address from a Multicast group
        /// </summary>
        /// <param name="ipAddress">The IP Address of the Multicast group to remove</param>
        /// <returns>True if the operation succeeded</returns>
        public bool RemoveIPFromMulticast(string ipAddress)
        {
            return _nmsService.RemoveIPFromMulticast(ipAddress);
        }

        /// <summary>
        /// Returns the transferred bytes for the Multicast group specified by the IP Address
        /// </summary>
        /// <param name="ipAddress">The IP Address</param>
        /// <returns>The transferred bytes as a long</returns>
        public ulong GetTransferredBytes(string ipAddress)
        {
            return _nmsService.GetTransferredBytes(ipAddress);
        }

        /// <summary>
        /// Returns true if the Multicast group identified by the IP Address is enabled
        /// </summary>
        /// <param name="ipAddress">The IP Address which identifies the MC group</param>
        /// <returns>True if the MC is enabled</returns>
        public bool IsEnabled(string ipAddress)
        {
            nms.MultiCastInfo mci = _nmsService.GetMultiCastGroupByIp(ipAddress);
            if (mci != null)
            {
                return (mci.State == 1);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Enables a MulticastGroup
        /// </summary>
        /// <param name="ipAddress">IP Address of the Multicast group to enable</param>
        /// <returns>true if the command was accepted by the NMS</returns>
        public bool EnableMultiCastGroup(string ipAddress, string macAddress, int ispId, Guid userId)
        {
            bool result = false;
            try
            {
                nms.RequestTicket rt = _nmsService.EnableMulticastGroup(ipAddress);
                result = this.insertRequestTicket(rt, macAddress, AdmStatus.OPERATIONAL, AdmStatus.OPERATIONAL, TerminalActivityTypes.EnableStream, ispId, userId);
            }
            catch (Exception ex)
            {
                result = false;
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDateTime = DateTime.UtcNow;
                cmtEx.ExceptionDesc = ex.Message;
                cmtEx.ExceptionLevel = (short)ExceptionLevelEnum.Critical;
                cmtEx.ExceptionStacktrace = "Could not enable Multicast group: " + ipAddress;
                cmtEx.StateInformation = "Critical error";
            }
            return result;
        }

        /// <summary>
        /// Disables a Multicast group
        /// </summary>
        /// <param name="ipAddress">IP Address of the Multicast group to disable</param>
        /// <returns>true if the command was accepted by the NMS</returns>
        public bool DisableMultiCastGroup(string ipAddress, string macAddress, int ispId, Guid userId)
        {
            bool result = false;
            try
            {
                nms.RequestTicket rt = _nmsService.DisableMulticastGroup(ipAddress);
                result = this.insertRequestTicket(rt, macAddress, AdmStatus.OPERATIONAL, AdmStatus.OPERATIONAL, TerminalActivityTypes.DisableStream, ispId, userId);
            }
            catch (Exception ex)
            {
                result = false;
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDateTime = DateTime.UtcNow;
                cmtEx.ExceptionDesc = ex.Message;
                cmtEx.ExceptionLevel = (short)ExceptionLevelEnum.Critical;
                cmtEx.ExceptionStacktrace = "Could not disable Multicast group: " + ipAddress;
                cmtEx.StateInformation = "Critical error";
            }
            return result;
        }

        #endregion

        #region IISPIFController Members

        /// <summary>
        /// This method retrieves the terminal information from the NMS
        /// </summary>
        /// <param name="macAddress">The MAC address</param>
        /// <returns>A completed TerminalInfo object</returns>
        public TerminalInfo getTerminalInfoByMacAddress(string macAddress)
        {
            nms.NMSTerminalInfo nmsTerminalInfo = _nmsService.getTerminalInfoByMacAddress(macAddress);
            CMTTerminal.ServiceLevel sl = _accountingController.GetServicePack(nmsTerminalInfo.SlaId);
            CMTTerminal.Terminal term = _accountingController.GetTerminalDetailsByMAC(macAddress);
            TerminalInfo ti = new TerminalInfo();

            try
            {

                ti.SitId = term.SitId.ToString();
                ti.Additional = "0";
                ti.AdditionalReturn = "0";
                if (term.Blade != null)
                    ti.BladeId = term.Blade.ToString();
                ti.Blocked = (nmsTerminalInfo.State == 0);
                //ti.Consumed = String.Format("{0:0.00}", nmsTerminalInfo.CurVolFwd);
                ti.Consumed = "" + (long)nmsTerminalInfo.CurVolFwd;
                ti.ConsumedFreeZone = "" + (long)nmsTerminalInfo.FreeZoneRtn;
                ti.ConsumedReturn = "" + (long)nmsTerminalInfo.CurVolRtn;
                ti.DRAboveFUP = sl.DrAboveFup;
                ti.EndUserId = term.FullName;
                ti.FreeZone = "" + (long)nmsTerminalInfo.FreeZoneFwd;
                ti.FUPThreshold = (decimal)sl.FupThreshold;
                ti.FWPool = term.FWPool.ToString();
                ti.InvoiceDayOfMonth = "" + (Convert.ToInt32(ti.SitId) % 28 + 1);
                ti.IPAddress = term.IpAddress;
                ti.IspId = term.IspId.ToString();
                ti.MacAddress = macAddress;
                ti.SitId = term.SitId.ToString();
                ti.RTN = this.getRTNValue(ti.IspId, ti.SitId);
                ti.FWD = this.getFWDValue(ti.IspId, ti.SitId);
                if (sl.ServiceClass == 3 || sl.ServiceClass == 2)
                {
                    if (term.ExpiryDate != null)
                        ti.ExpiryDate = (DateTime)term.ExpiryDate;
                }
                else
                    ti.ExpiryDate = nmsTerminalInfo.ExpirationDate;

                if (ti.RTN.Equals("-1.0"))
                {
                    ti.Status = "Not Operational";
                }
                else
                {
                    ti.Status = "Operational";
                }

                if (sl.RTNFUPThreshold != null)
                {
                    ti.RTNFUPThreshold = (decimal)sl.RTNFUPThreshold;
                }
                else
                {
                    ti.RTNFUPThreshold = new decimal(0.0);
                }
                ti.RTNPool = term.RTPool.ToString();
                ti.SitId = term.SitId.ToString();
                ti.SlaId = term.SlaId.ToString();
                ti.SlaName = term.SlaName;
                ti.ErrorFlag = false;

                //added maximum volume info to keep track of SoHo terminal data
                ti.MaxVolFwd = nmsTerminalInfo.MaxVolFwd;
                ti.MaxVolRtn = nmsTerminalInfo.MaxVolRtn;
                ti.MaxVolSum = nmsTerminalInfo.MaxVolSum;
            }
            catch (Exception ex)
            {
                ti.ErrorFlag = true;
                ti.ErrorMsg = ex.Message;

            }

            return ti;
        }

        /// <summary>
        /// This method retrieves the terminal information from the SES Hub
        /// </summary>
        /// <param name="macAddress">The terminal MAC address</param>
        /// <param name="ispId">The ISP identifier, not used in this case as we need to use the
        /// SES HUB Isp!</param>
        /// <returns>A completed TerminalInfo object</returns>
        public virtual TerminalInfo getTerminalDetailsFromHub(string macAddress, string ispId)
        {
            String sesIsp = _configurationController.getParameter(SES_HUB_ISP);
            return _satADSLHubController.getTerminalDetailsFromHub(macAddress, sesIsp);
        }

        /// <summary>
        /// Returns the C/No value. This value is retrieved from the SES hub
        /// </summary>
        /// <param name="ispId">The unique ISP identifier</param>
        /// <param name="sitId">The site id</param>
        /// <returns>The C/No value as a string</returns>
        public virtual string getRTNValue(string ispId, string sitId)
        {
            string sesIspId = _configurationController.getParameter(SES_HUB_ISP).Trim();
            return _satADSLHubController.getRTNValue(sesIspId, sitId);
        }

        /// <summary>
        /// Returns the Es/No value. This value is retrieved from the SES hub
        /// </summary>
        /// <param name="ispId">The unique ISP identifier</param>
        /// <param name="sitId">The site id</param>
        /// <returns>The Es/No value as a string</returns>
        public virtual string getFWDValue(string ispId, string sitId)
        {
            string sesIspId = _configurationController.getParameter(SES_HUB_ISP).Trim();
            return _satADSLHubController.getFWDValue(sesIspId, sitId);
        }

        /// <summary>
        /// Returns a list of TrafficDataPoints
        /// </summary>
        /// <remarks>
        /// For this version the list remains empty
        /// </remarks>
        /// <param name="macAddress">The mac address of the terminal</param>
        /// <param name="startDateTime">Start date/time of the query</param>
        /// <param name="endDateTime">End date/time of the query</param>
        /// <param name="ispId"></param>
        /// <returns></returns>
        public List<TrafficDataPoint> getTrafficViews(string macAddress, DateTime startDateTime, DateTime endDateTime, int ispId)
        {
            return _satADSLHubController.getTrafficViews(macAddress, startDateTime, endDateTime, ispId);
        }

        /// <summary>
        /// Returns a list of TrafficDataPonts with accumumated volume consumption info
        /// </summary>
        /// <param name="macAddress"></param>
        /// <param name="startDateTime"></param>
        /// <param name="endDateTime"></param>
        /// <param name="ispId"></param>
        /// <returns></returns>
        public List<Model.TrafficDataPoint> getAccumulatedTrafficViews(string macAddress, DateTime startDateTime, DateTime endDateTime, int ispId)
        {
            return _satADSLHubController.getAccumulatedTrafficViews(macAddress, startDateTime, endDateTime, ispId);
        }

        public List<TrafficDataPoint> getHighPriorityTrafficViews(string macAddress, DateTime startDateTime, DateTime endDateTime, int ispId)
        {
            return _satADSLHubController.getHighPriorityTrafficViews(macAddress, startDateTime, endDateTime, ispId);
        }

        public List<TrafficDataPoint> getAccumulatedViews(string macAddress, int ispId)
        {
            return _satADSLHubController.getAccumulatedViews(macAddress, ispId);
        }

        public List<TrafficDataPoint> getAccumulatedViewsBetweenDates(string macAddress, int ispId, DateTime startDate, DateTime endDate)
        {
            return _satADSLHubController.getAccumulatedViewsBetweenDates(macAddress, ispId, startDate, endDate);
        }

        #endregion

        #region NMS Support methods

        /// <summary>
        /// Activates a terminal in the NMS.
        /// </summary>
        /// <param name="endUserId">The end user identifier</param>
        /// <param name="macAddress">The terminal MAC address</param>
        /// <param name="slaId">The SLA identifier</param>
        /// <param name="ispId">The ISP idnetifier</param>
        /// <param name="ipMask">IP range specified by means of a MASK</param>
        /// <returns>True if the method succeeds</returns>
        public virtual bool NMSTerminalActivateWithRange(string endUserId, string macAddress, long slaId, int ispId, int ipMask, int freeZone, Guid userId)
        {
            bool result = false;

            //First check if the slaId belongs to the isp. If not abort the activate as we are not allowed
            //to send non SatADSL NMS Slas to the server.
            if (this.IsCorrectSla(slaId, ispId))
            {
                try
                {
                    //Need to specify an IP Address as this address is not generated by the NMS but by the SES HUB
                    //Use the SES HUB ISP to get the terminal details
                    string sesIspId = _configurationController.getParameter(SES_HUB_ISP);
                    TerminalInfo ti = _satADSLHubController.getTerminalDetailsFromHub(macAddress, sesIspId);
                    nms.RequestTicket rt = _nmsService.createRegistration(endUserId, macAddress, ipMask, (int)slaId, ispId, ti.IPAddress, Int32.Parse(ti.SitId), freeZone);
                    result = this.insertRequestTicketWithSla(rt, macAddress, AdmStatus.LOCKED, AdmStatus.OPERATIONAL, TerminalActivityTypes.Activation, ispId, (int)slaId, userId);
                }
                catch (Exception ex)
                {
                    this.logException(ex, "NMSGateWay.TerminalActivate");
                }
            }
             else
            {
                this.logException("Sla: " + slaId + " does not belong to ISP: " + ispId, "NMSGateway.NMSTerminalActivateWithRange");
            }
            return result;
        }

        /// <summary>
        /// Activates a terminal in the NMS without specifying a range by means of
        /// an IP Mask value.
        /// </summary>
        /// <remarks>
        /// This method activates a terimal in the NMS without specifiying a range, thus the
        /// IP Mask is always set to \32 which means that only one IP address can be specified.
        /// </remarks>
        /// <param name="endUserId">The end user identifier</param>
        /// <param name="macAddress">The terminal MAC address</param>
        /// <param name="slaId">The SLA identifier</param>
        /// <param name="ispId">The ISP idnetifier</param>
        /// <returns>True if the method succeeds</returns>
        public virtual bool NMSTerminalActivate(string endUserId, string macAddress, long slaId, int ispId, int freeZone, Guid userId)
        {
            bool result = false;

            //First check if the slaId belongs to the isp. If not abort the activate as we are not allowed
            //to send non SatADSL NMS Slas to the server.
            if (this.IsCorrectSla(slaId, ispId))
            {
                try
                {
                    //Need to specify an IP Address as this address is not generated by the NMS but by the SES HUB
                    TerminalInfo ti = _satADSLHubController.getTerminalDetailsFromHub(macAddress, ispId.ToString());
                    nms.RequestTicket rt = _nmsService.createRegistration(endUserId, macAddress, 32, (int)slaId, ispId, ti.IPAddress, Int32.Parse(ti.SitId), freeZone);
                    result = this.insertRequestTicketWithSla(rt, macAddress, AdmStatus.Request, AdmStatus.OPERATIONAL, TerminalActivityTypes.Activation, ispId, (int)slaId, userId);
                }
                catch (Exception ex)
                {
                    this.logException(ex, "NMSGateWay.NMSTerminalActivate");
                }
            }
            else
            {
                this.logException("Sla: " + slaId + " does not belong to ISP: " + ispId, "NMSTerminalActivate");
            }
            return result;
        }

        /// <summary>
        /// Changes the SLA in the NMS
        /// </summary>
        /// <param name="endUserId">The End User Identifier</param>
        /// <param name="macAddress">The terminals' mac address</param>
        /// <param name="newSlaId">The new SLA identifier</param>
        /// <param name="currentState">The current administration status of the terminal</param>
        /// <param name="ispId">The ISP identifier</param>
        /// <returns>True if the method succeeds</returns>
        public bool NMSTerminalChangeSla(string endUserId, string macAddress, int newSlaId, AdmStatus currentState, int ispId, Guid userId)
        {
            bool result = false;

            //First check if the slaId belongs to the isp. If not abort the activate as we are not allowed
            //to send non SatADSL NMS Slas to the server.


            try
            {
                nms.RequestTicket rt = _nmsService.changeSla(endUserId, newSlaId.ToString(), ispId.ToString());
                result = this.insertRequestTicketWithSla(rt, macAddress, currentState, currentState, TerminalActivityTypes.ChangeSla, ispId, newSlaId, userId);
            }
            catch (Exception ex)
            {
                this.logException(ex, "NMSGateway.NMSTerminalChangeSla");
            }


            //if (this.IsCorrectSla(newSlaId, ispId))
            //{
            //    try
            //    {
            //        nms.RequestTicket rt = _nmsService.changeSla(endUserId, newSlaId.ToString(), ispId.ToString());
            //        result = this.insertRequestTicketWithSla(rt, macAddress, currentState, currentState, TerminalActivityTypes.ChangeSla, ispId, newSlaId, userId);
            //    }
            //    catch (Exception ex)
            //    {
            //        this.logException(ex, "NMSGateway.NMSTerminalChangeSla");
            //    }
            //}
            //else
            //{
            //    this.logException("The new Sla: " + newSlaId + " does not belong to ISP: " + ispId, "NMSGateway.NMSTerminalChangeSla");
            //}
            return result;
        }

        /// <summary>
        /// Changes the weight of the terminal in the NMS
        /// </summary>
        /// <param name="endUserId">The endUserId of the terminal to activate</param>
        /// <param name="macAddress">The MAC address of the terminal to activate</param>
        /// <param name="newWeight">The new weight allocated to the terminal</param>
        /// <param name="customerId">The ID of the customer the terminal belongs to</param>
        /// <param name="currentState">The current admin state of the terminal</param>
        /// <returns>true if the application succeeded</returns>
        public bool NMSTerminalChangeWeight(string endUserId, string macAddress, int newWeight, int customerId, AdmStatus currentState, int ispId, Guid userId)
        {
            bool result = false;
            try
            {
                nms.RequestTicket rt = _nmsService.changeWeight(endUserId, newWeight, customerId, ispId.ToString());
                result = this.insertRequestTicket(rt, macAddress, currentState, currentState, TerminalActivityTypes.ChangeWeight, ispId, userId);
            }
            catch (Exception ex)
            {
                this.logException(ex, "NMSGateway.TerminalChangeWeight");
            }
            return result;
        }

        /// <summary>
        /// Decommissions the terminal in the NMS.
        /// </summary>
        /// <param name="endUserId">Identifier of the end user</param>
        /// <param name="macAddress">The terminals' mac address</param>
        /// <param name="ispId">The ISP identifier</param>
        /// <returns>True if the method succeeds</returns>
        public bool NMSTerminalDecommission(string endUserId, string macAddress, int ispId, Guid userId)
        {
            bool result = false;
            try
            {
                nms.RequestTicket rt = _nmsService.removeRegistration(endUserId, macAddress);
                //nms.RequestTicket rt = new nms.RequestTicket()
                //{
                //    id = Guid.NewGuid().ToString(),
                //    failureReason = "",
                //    failureStackTrace = "",
                //    requestStatus = nms.RequestStatus.BUSY
                //};
                result = this.insertRequestTicket(rt, macAddress, AdmStatus.OPERATIONAL, AdmStatus.DECOMMISSIONED, TerminalActivityTypes.Decommissioning, ispId, userId);
            }
            catch (Exception ex)
            {
                this.logException(ex, "NMSGatewayService.TerminalDecommission");
            }
            return result;
        }

        /// <summary>
        /// Reactivation of the terminal in the NMS
        /// </summary>
        /// <param name="endUserId">The end user identifier</param>
        /// <param name="macAddress">The MAC address of the terminal</param>
        /// <param name="ispId">The ISP identifier</param>
        /// <returns>True if the method succeeds</returns>
        public bool NMSTerminalReActivate(string endUserId, string macAddress, int ispId, Guid userId)
        {
            bool result = false;
            try
            {
                nms.RequestTicket rt = _nmsService.changeStatus(endUserId, nms.RegistrationStatus.OPERATIONAL, ispId.ToString());
                result = this.insertRequestTicket(rt, macAddress, AdmStatus.LOCKED, AdmStatus.OPERATIONAL, TerminalActivityTypes.ReActivation, ispId, userId);
            }
            catch (Exception ex)
            {
                this.logException(ex, "NMSGateWay.TerminalReActivate");
            }
            return result;
        }

        /// <summary> 
        /// Suspends a terminal in the NMS
        /// </summary>
        /// <param name="endUserId">The end user identifier</param>
        /// <param name="macAddress">The MAC address of the terminal</param>
        /// <param name="ispId">The ISP identifier</param>
        /// <returns>True if the suspend succeeds</returns>
        public bool NMSTerminalSuspend(string endUserId, string macAddress, int ispId, Guid userId)
        {
            bool result = false;
            try
            {
                nms.RequestTicket rt = _nmsService.changeStatus(endUserId, nms.RegistrationStatus.LOCKED, ispId.ToString());
                result = this.insertRequestTicket(rt, macAddress, AdmStatus.OPERATIONAL, AdmStatus.LOCKED, TerminalActivityTypes.Suspension, ispId, userId);
            }
            catch (Exception ex)
            {
                this.logException(ex, "ISPIFEmulatorGateWay.TerminalSuspend");
            }
            return result;
        }

        /// <summary>
        /// Resets the terminal in the NMS
        /// </summary>
        /// <param name="endUserId">The end user id of the terminal</param>
        /// <param name="ispId"></param>
        /// <returns></returns>
        public bool NMSTerminalUserReset(string endUserId, int ispId)
        {
            bool result = false;
            try
            {
                _nmsService.resetUser(endUserId, ispId);
                result = true;
            }
            catch (Exception ex)
            {
                this.logException(ex, "NMSGatewayService.TerminalUserReset");
            }

            return result;
        }

        /// <summary>
        /// Looksup the ticket in the NMS
        /// </summary>
        /// <param name="requestTicketId">The request ticket identifier</param>
        /// <param name="ispId">The ISPIF identifier</param>
        /// <returns>A valid CmtRequestTicket or null if not found</returns>
        public CmtRequestTicket NMSLookupRequestTicket(string requestTicketId, int ispId)
        {
            CmtRequestTicket cmtRt = null;
            nms.RequestTicket rt = _nmsService.lookupRequestTicket(requestTicketId);

            if (rt != null)
            {
                cmtRt = new CmtRequestTicket();
                cmtRt.id = rt.id;
                cmtRt.failureReason = rt.failureReason;
                cmtRt.failureStackTrace = rt.failureStackTrace;
                if (rt.requestStatus == nms.RequestStatus.BUSY)
                {
                    cmtRt.requestStatus = CmtRequestStatus.BUSY;
                }

                if (rt.requestStatus == nms.RequestStatus.FAILED)
                {
                    cmtRt.requestStatus = CmtRequestStatus.FAILED;
                }

                if (rt.requestStatus == nms.RequestStatus.SUCCESSFUL)
                {
                    cmtRt.requestStatus = CmtRequestStatus.SUCCESSFUL;
                }
            }

            return cmtRt;
        }

        /// <summary>
        /// Adds volume to the NMS 
        /// </summary>
        /// <param name="sitId">The Site identifier of the end user</param>
        /// <param name="volumeCode">The volume code which determines how much volume needs to be added</param>
        /// <param name="ispId">The ISP identifier</param>
        /// <returns>True if the volume was correctly added</returns>
        public bool NMSAddVolume(int sitId, int volumeCode, int ispId)
        {
            //Correct after new implementation of voucher volumes
            CMTTerminal.Terminal term = _accountingController.GetTerminalDetailsBySitId(sitId, ispId);
            nms.NMSTerminalInfo nmsTerminalInfo = _nmsService.getTerminalInfoByMacAddress(term.MacAddress);
            VoucherVolume vv = _voucherController.GetVoucherVolumeById(volumeCode);

            //SMT-13
            if ((nmsTerminalInfo.MaxVolFwd >= 9000000000000 && vv.VolumeMB >= 9000000 && DateTime.UtcNow <= nmsTerminalInfo.ExpirationDate) ||(vv.ValidityPeriod==0))
            {
                double additionalDays = (nmsTerminalInfo.ExpirationDate - DateTime.UtcNow).TotalDays+1;
                return _nmsService.AddVolumeWithValidity(sitId, ispId, vv.VolumeMB, vv.VolumeMB, vv.ValidityPeriod+(int)additionalDays);
            }
            else
                return _nmsService.AddVolumeWithValidity(sitId, ispId, vv.VolumeMB, vv.VolumeMB, vv.ValidityPeriod);
        }

        /// <summary>
        /// Changes the MAC address in the NMS
        /// </summary>
        /// <param name="endUserId">The end user id</param>
        /// <param name="oriMacAddress">The original Mac Address</param>
        /// <param name="newMacAddress">The new Mac Address</param>
        /// <param name="ispId">The ISP identifier</param>
        /// <returns>True if the method succeeds</returns>
        public bool NMSChangeMacAddress(string endUserId, string oriMacAddress, string newMacAddress, int ispId, Guid userId)
        {
            bool result = false;
            try
            {
                nms.RequestTicket rt = _nmsService.changeTerminal(endUserId, oriMacAddress, newMacAddress);
                result = this.insertRequestTicketWithMac(rt, oriMacAddress, AdmStatus.OPERATIONAL, AdmStatus.OPERATIONAL, TerminalActivityTypes.ChangeMac, ispId, newMacAddress, userId);

            }
            catch (Exception ex)
            {
                this.logException(ex, "ISPIFEmulatorGateWay.ChangeMacAddress failed for: " + endUserId);
            }
            return result;
        }

        /// <summary>
        /// Changes the ISP of a terminal in the NMS
        /// </summary>
        /// <param name="macAddress">The terminals MAC address</param>
        /// <param name="oldISP">The old ISP</param>
        /// <param name="newISP">The new iSP</param>
        /// <returns>True if the operation succeeded</returns>
        public bool NMSChangeISP(string endUserId, int oldISP, int newISP)
        {
            throw new NotImplementedException("reassign method not implemented by the NMSGatewayService");
        }

        /// <summary>
        /// Creates a new service pack (SLA) in the NMS
        /// </summary>
        /// <param name="servicePack">The service pack to be created</param>
        /// <returns>True if the operation succeeded</returns>
        public bool NMSCreateServicePack(nms.ServicePack servicePack)
        {
            return _nmsService.CreateServicePack(servicePack);
        }

        /// <summary>
        /// Updates an existing service pack (SLA) in the NMS
        /// </summary>
        /// <param name="servicePack">The service pack to be updated</param>
        /// <returns>True if the operation succeeded</returns>
        public bool NMSUpdateServicePack(nms.ServicePack servicePack)
        {
           BOControllerLibrary.Model.ISP.Isp ispSLA = _accountingController.GetISP((int)servicePack.IspId);
            return _nmsService.UpdateServicePackwithUPlink(servicePack,ispSLA.Satellite.ID);
        }

        /// <summary>
        /// Deletes an existing service pack (SLA) from the NMS
        /// </summary>
        /// <param name="servicePack">The service pack to be deleted</param>
        /// <returns>True if the operation succeeded</returns>
        public bool NMSDeleteServicePack(int slaId)
        {
            if (_nmsService.slaExists(slaId))
            {
                if (!_nmsService.SlaHasAssociatedTerminals(slaId))
                {
                    return _nmsService.DeleteServicePack(slaId);
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Checks if the SLA (which eventually needs to be deleted) has associated terminals
        /// </summary>
        /// <param name="slaId">The identifier of the SLA</param>
        /// <returns>True when the SLA has still associated terminals</returns>
        public bool NMSSlaHasAssociatedTerminals(int slaId)
        {
            return _nmsService.SlaHasAssociatedTerminals(slaId);
        }

        /// <summary>
        /// Creates a freezone in the NMS
        /// </summary>
        /// <param name="freeZone">The freezone to be created</param>
        /// <returns>True if succesful</returns>
        public bool NMScreateFreeZone(nms.FreeZone freeZone)
        {
            return _nmsService.createFreeZone(freeZone);
        }

        /// <summary>
        /// Updates a freezone in the NMS
        /// </summary>
        /// <param name="freeZone">The freezone to be updated</param>
        /// <returns>True if succesful</returns>
        public bool NMSupdateFreeZone(nms.FreeZone freeZone)
        {
            return _nmsService.updateFreeZone(freeZone);
        }

        /// <summary>
        /// Fetches a list of all freezones in the NMS database
        /// </summary>
        /// <returns>The list of freezones</returns>
        public List<nms.FreeZone> NMSgetFreeZones()
        {
            // Rather than calling the NMS web service here, 
            // the CMT database will be used for this method
            // for a faster load
            throw new NotImplementedException();
        }

        /// <summary>
        /// Fetches the details of a specific freezone from the NMS
        /// </summary>
        /// <param name="freeZoneId">The ID of the freezone</param>
        /// <returns>The freezone</returns>
        public nms.FreeZone NMSgetFreeZone(int freeZoneId)
        {
            return _nmsService.getFreeZone(freeZoneId);
        }

        /// <summary>
        /// This method allows to change the IP Address AND Mask in the SatADSL NMS
        /// </summary>
        /// <remarks>
        /// The old and new IP Masks may be identical while the old and new IP Addresses
        /// can never be identical.
        /// </remarks>
        /// <param name="endUserId">The unique end user Id</param>
        /// <param name="oldIPAddress">The original IP Address</param>
        /// <param name="oldIPMask">The original IP Mask</param>
        /// <param name="newIPAddress">The new IP Address</param>
        /// <param name="newIPMask">The new IP Mask</param>
        /// <param name="ispId">The ISP Identifier</param>
        /// <returns>True if the change was registered by NMS successfully</returns>
        public bool NMSChangeIPAddress(string endUserId, string macAddress, string oldIPAddress, int oldIPMask,
                                                    string newIPAddress, int newIPMask, int ispId, Guid userId)
        {
            bool result = false;
            try
            {
                nms.RequestTicket rt = _nmsService.changeIPAddress(endUserId, newIPAddress, ispId.ToString(), newIPMask);
                result = this.insertRequestTicketWithIPAddress(rt, macAddress, AdmStatus.OPERATIONAL,
                    AdmStatus.OPERATIONAL, BOControllerLibrary.ISPIF.Gateway.TerminalActivityTypes.IPAddressChange, ispId, newIPAddress, newIPMask, userId);
            }
            catch (Exception ex)
            {
                this.logException(ex, "NMSGatewayService.NMSChangeIPAddress");
            }

            return result;
        }

        /// <summary>
        /// This method allows to update the freezone information for a terminal
        /// </summary>
        /// <param name="FreeZoneId">The identifier of the freezone allocated to the terminal</param>
        /// <param name="macAddress">The MAC address of the terminal</param>
        /// <param name="FreeZoneFlag">If this flag is set to true, a freezone option is allocated to the terminal. In
        /// case this value is false, the terminal does not have the freezone option</param>
        /// <returns>True if the operation succeeds false otherwise</returns>
        public bool NMSModifyFreeZoneForTerminal(string endUserId, int FreeZoneId)
        {
            return _nmsService.ChangeFreeZoneForTerminal(endUserId, FreeZoneId);
        }

        #endregion

        #region Helper methods

        /// <summary>
        /// Logs an exception
        /// </summary>
        /// <param name="ex">The exception to log</param>
        /// <param name="method">The method which generated the error</param>
        public void logException(Exception ex, string method)
        {
            CmtApplicationException cmtApplicationException = new CmtApplicationException();
            cmtApplicationException.ExceptionDateTime = DateTime.Now;
            cmtApplicationException.ExceptionDesc = ex.Message;
            cmtApplicationException.ExceptionStacktrace = ex.StackTrace;
            cmtApplicationException.UserDescription = method;
            cmtApplicationException.ExceptionLevel = (short)ExceptionLevelEnum.Critical;
            _controller.LogApplicationException(cmtApplicationException);
        }

        /// <summary>
        /// Logs and exception
        /// </summary>
        /// <param name="msg">The message to log</param>
        /// <param name="method">The method which generated the error</param>
        public void logException(string msg, string method)
        {
            CmtApplicationException cmtApplicationException = new CmtApplicationException();
            cmtApplicationException.ExceptionDateTime = DateTime.Now;
            cmtApplicationException.ExceptionDesc = msg;
            cmtApplicationException.ExceptionStacktrace = "";
            cmtApplicationException.UserDescription = method;
            cmtApplicationException.ExceptionLevel = (short)ExceptionLevelEnum.Critical;
            _controller.LogApplicationException(cmtApplicationException);
        }

        /// <summary>
        /// Inserts a new ticket in the CMT database and flags it as an NMS ticket
        /// </summary>
        /// <remarks>
        /// Use this version of the insertRequestTicket method to insert a request ticket from the
        /// SES hub in the database
        /// </remarks>
        /// <param name="rt"><b>ISPIF Request ticket</b></param>
        /// <param name="macAddress">The MacAddress of the target</param>
        /// <param name="sourceAdmStatus">The Source admin status</param>
        /// <param name="targetAdmStatus">The Target admin status</param>
        /// <param name="termActivityType">The terminal activity type</param>
        /// <returns>True if the operation succeeded</returns>
        virtual protected bool insertRequestTicket(ispif.RequestTicket rt, string macAddress, AdmStatus sourceAdmStatus, AdmStatus targetAdmStatus,
                                           TerminalActivityTypes termActivityType, int ispId, Guid userId)
        {
            TicketDataMgr ticketDataMgr = new TicketDataMgr();
            //RequestStatus reqStatus = 
            //        (RequestStatus)Enum.ToObject(typeof(RequestStatus), (int)rt.requestStatus);
            return ticketDataMgr.insertTicket(rt.id, macAddress, sourceAdmStatus, targetAdmStatus,
                                            (int)rt.requestStatus, rt.failureReason, rt.failureStackTrace, termActivityType, ispId, 1, userId);
        }

        /// <summary>
        /// Inserts a new ticket in the CMT database
        /// </summary>
        /// <remarks>
        /// Use this overloaded method to insert the request ticket from the NMS into the 
        /// Ticket table.
        /// </remarks>
        /// <param name="rt"><b>NMS Request ticket</b></param>
        /// <param name="macAddress">The MacAddress of the target</param>
        /// <param name="sourceAdmStatus">The Source admin status</param>
        /// <param name="targetAdmStatus">The Target admin status</param>
        /// <param name="termActivityType">The terminal activity type</param>
        /// <returns>True if the operation succeeded</returns>
        protected bool insertRequestTicket(nms.RequestTicket rt, string macAddress, AdmStatus sourceAdmStatus, AdmStatus targetAdmStatus,
                                           TerminalActivityTypes termActivityType, int ispId, Guid userId)
        {
            TicketDataMgr ticketDataMgr = new TicketDataMgr();
            return ticketDataMgr.insertTicket(rt.id, macAddress, sourceAdmStatus, targetAdmStatus,
                                            (int)rt.requestStatus, rt.failureReason, rt.failureStackTrace, termActivityType, ispId, 2, userId);
        }

        /// <summary>
        /// Inserts a ticket  for an SLA change
        /// </summary>
        /// <remarks>
        /// This method inserts a request ticket obtained from the SES Hub (ISPIF) into the Ticket table
        /// </remarks>
        /// <param name="rt"><b>The ISPIF Request ticket</b></param> 
        /// <param name="macAddress">The Mac Address</param>
        /// <param name="sourceAdmStatus">The source amdin status</param>
        /// <param name="targetAdmStatus">The target admin status</param>
        /// <param name="termActivityType">The terminal activity type</param>
        /// <param name="ispId">The ISP identifier</param>
        /// <param name="newSla">The new sla</param>
        /// <returns>True if the method succeeded</returns>
        protected bool insertRequestTicketWithSla(ispif.RequestTicket rt, string macAddress, AdmStatus sourceAdmStatus, AdmStatus targetAdmStatus,
                                           TerminalActivityTypes termActivityType, int ispId, int newSla, Guid userId)
        {
            TicketDataMgr ticketDataMgr = new TicketDataMgr();
            return ticketDataMgr.insertTicket(rt.id, macAddress, sourceAdmStatus, targetAdmStatus,
                                            (int)rt.requestStatus, rt.failureReason, rt.failureStackTrace, termActivityType, ispId, 1, newSla, userId);
        }

        /// <summary>
        /// Inserts a ticket with for an SLA change
        /// </summary>
        /// <remarks>
        /// This method inserts a request ticket obtained from the NMS into the Ticket table
        /// </remarks>
        /// <param name="rt"><b>The NMS Request ticket</b></param> 
        /// <param name="macAddress">The Mac Address</param>
        /// <param name="sourceAdmStatus">The source amdin status</param>
        /// <param name="targetAdmStatus">The target admin status</param>
        /// <param name="termActivityType">The terminal activity type</param>
        /// <param name="ispId">The ISP identifier</param>
        /// <param name="newSla">The new sla</param>
        /// <returns>True if the method succeeded</returns>
        protected bool insertRequestTicketWithSla(nms.RequestTicket rt, string macAddress, AdmStatus sourceAdmStatus, AdmStatus targetAdmStatus,
                                           TerminalActivityTypes termActivityType, int ispId, int newSla, Guid userId)
        {
            TicketDataMgr ticketDataMgr = new TicketDataMgr();
            return ticketDataMgr.insertTicket(rt.id, macAddress, sourceAdmStatus, targetAdmStatus,
                                            (int)rt.requestStatus, rt.failureReason, rt.failureStackTrace, termActivityType, ispId, 2, newSla, userId);
        }

        /// <summary>
        /// Inserts a ticket for a MAC Address change
        /// </summary>
        /// <remarks>
        /// This version of the method inserts a request ticket obtained from the ISPIF into the ticket table
        /// </remarks>
        /// <param name="rt"><b>The ISPIF Request ticket</b></param>
        /// <param name="macAddress">The MAC address of the terminal</param>
        /// <param name="sourceAdmStatus">The sourde administration status</param>
        /// <param name="targetAdmStatus">The target administration status</param>
        /// <param name="termActivityType">The terminal activity type</param>
        /// <param name="ispId">The ISP identifier</param>
        /// <param name="NewMacAddress">The new mac address</param>
        /// <returns></returns>
        protected bool insertRequestTicketWithMac(ispif.RequestTicket rt, string macAddress, AdmStatus sourceAdmStatus, AdmStatus targetAdmStatus,
                                           TerminalActivityTypes termActivityType, int ispId, string NewMacAddress, Guid userId)
        {
            TicketDataMgr ticketDataMgr = new TicketDataMgr();
            return ticketDataMgr.insertTicket(rt.id, macAddress, sourceAdmStatus, targetAdmStatus,
                                            (int)rt.requestStatus, rt.failureReason, rt.failureStackTrace, termActivityType, ispId, 1, NewMacAddress, userId);
        }

        /// <summary>
        /// Inserts a ticket for a MAC Address change
        /// </summary>
        /// <remarks>
        /// This version of the method inserts a request ticket obtained from the NMS into the ticket table
        /// </remarks>
        /// <param name="rt"><b>The NMS Request ticket</b></param>
        /// <param name="macAddress">The MAC address of the terminal</param>
        /// <param name="sourceAdmStatus">The sourde administration status</param>
        /// <param name="targetAdmStatus">The target administration status</param>
        /// <param name="termActivityType">The terminal activity type</param>
        /// <param name="ispId">The ISP identifier</param>
        /// <param name="NewMacAddress">The new mac address</param>
        /// <returns></returns>
        protected bool insertRequestTicketWithMac(nms.RequestTicket rt, string macAddress, AdmStatus sourceAdmStatus, AdmStatus targetAdmStatus,
                                           TerminalActivityTypes termActivityType, int ispId, string NewMacAddress, Guid userId)
        {
            TicketDataMgr ticketDataMgr = new TicketDataMgr();
            return ticketDataMgr.insertTicket(rt.id, macAddress, sourceAdmStatus, targetAdmStatus,
                                            (int)rt.requestStatus, rt.failureReason, rt.failureStackTrace, termActivityType, ispId, 2, NewMacAddress, userId);
        }

        /// <summary>
        /// Inserts a ticket for an IP Address change
        /// </summary>
        /// <remarks>
        /// This version of the method inserts a request ticket obtained from the NMS into the ticket table
        /// </remarks>
        /// <param name="rt"><b>The NMS Request ticket</b></param>
        /// <param name="macAddress">The MAC address of the terminal</param>
        /// <param name="sourceAdmStatus">The source administration status</param>
        /// <param name="targetAdmStatus">The target administration status</param>
        /// <param name="termActivityType">The target activity type which should be IP</param>
        /// <param name="ispId">The isp Identifier</param>
        /// <param name="newIPAddress">The new IP Address</param>
        /// <param name="newIPMask">The new IP Mask</param>
        /// <returns></returns>
        protected bool insertRequestTicketWithIPAddress(nms.RequestTicket rt, string macAddress, AdmStatus sourceAdmStatus, AdmStatus targetAdmStatus,
                                           TerminalActivityTypes termActivityType, int ispId, string newIPAddress, int newIPMask, Guid userId)
        {
            TicketDataMgr ticketDataMgr = new TicketDataMgr();
            return ticketDataMgr.insertTicket(rt.id, macAddress, sourceAdmStatus, targetAdmStatus, (int)rt.requestStatus,
                rt.failureReason, rt.failureStackTrace, TerminalActivityTypes.IPAddressChange,
                ispId, 2, newIPAddress, newIPMask, userId);
        }

        /// <summary>
        /// This method returns the ISPIF credentials for the ISP associated to the 
        /// given MAC address
        /// </summary>
        /// <param name="macAddress">The target MAC Address</param>
        /// <returns>A NetworkCredential instance or null in case of an error</returns>
        protected NetworkCredential readCredentials(string macAddress)
        {
            //Get the credentials from the database
            string connectionString =
               ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
            string databaseProviderString =
               ConfigurationManager.ConnectionStrings["ApplicationServices"].ProviderName;

            BOControllerLibrary.Controllers.BOAccountingController boAccountingController =
                            new BOControllerLibrary.Controllers.BOAccountingController(connectionString, databaseProviderString);

            //First get the terminal info object from the database
            BOControllerLibrary.Model.Terminal.Terminal term
                                    = boAccountingController.GetTerminalDetailsByMAC(macAddress);
            BOControllerLibrary.Model.ISP.Isp isp = boAccountingController.GetISP(term.IspId);
            NetworkCredential cred = new NetworkCredential(isp.Ispif_User, isp.Ispif_Password);
            return cred;
        }

        /// <summary>
        /// This method returns the ISPIF credentials for the given ISP
        /// </summary>
        /// <param name="macAddress">The target MAC Address</param>
        /// <returns>A NetworkCredential instance or null in case of an error</returns>
        protected NetworkCredential readCredentials(int IspId)
        {
            //Get the credentials from the database
            string connectionString =
               ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
            string databaseProviderString =
               ConfigurationManager.ConnectionStrings["ApplicationServices"].ProviderName;

            BOControllerLibrary.Controllers.BOAccountingController boAccountingController =
                            new BOControllerLibrary.Controllers.BOAccountingController(connectionString, databaseProviderString);

            BOControllerLibrary.Model.ISP.Isp isp = boAccountingController.GetISP(IspId);
            NetworkCredential cred = new NetworkCredential(isp.Ispif_User, isp.Ispif_Password);
            return cred;
        }

        /// <summary>
        /// This method checks if a given Sla indeed belongs to a the specified ISP.
        /// </summary>
        /// <param name="slaId">The SlaId as a long</param>
        /// <param name="ispId">The ISP Id as an int</param>
        /// <returns>True if the SLA belongs to the ISP</returns>
        public bool IsCorrectSla(long slaId, int ispId)
        {
            CMTTerminal.ServiceLevel sl = _accountingController.GetServicePack((int)slaId);
            return sl.IspId == ispId;
        }

        /// <summary>
        /// This method checks if a given Sla indeed belongs to a the specified ISP.
        /// </summary>
        /// <param name="slaId">The SlaId as an int</param>
        /// <param name="ispId">The ISP Id as an int</param>
        /// <returns>True if the SLA belongs to the ISP</returns>
        public bool IsCorrectSla(int slaId, int ispId)
        {
            CMTTerminal.ServiceLevel sl = _accountingController.GetServicePack(slaId);
            return sl.IspId == ispId;
        }

        #endregion



        public virtual TerminalInfo getTerminalDetailsFromHubMacChange(string oldMacAddress, string newMacAddress, string ispId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Activates a special satellite diversity terminal in the NMS
        /// </summary>
        /// <remarks>
        /// This method activates a terminal without specifying a IP Mask range! So only
        /// one IP address is allocated to the terminal.
        /// </remarks>
        /// <param name="endUserId">The endUserId of the terminal to activate</param>
        /// <param name="macAddress">The MAC address of the terminal to activate</param>
        /// <param name="ipAddress">The IP address of the terminal to be activated</param>
        /// <param name="slaId">The service pack allocated to the terminal</param>
        /// <param name="ipMask">The IP Mask used to determine the IP range</param>
        /// <param name="freeZone">The freezone calendar associated with this terminal, 0 if not associated with
        /// a freezone SLA</param>
        /// <param name="primaryMac">The MAC address of the primary terminal in the setup</param>
        /// <param name="secondaryMac">The MAC address of the secondary terminal in the setup</param>
        /// <returns>true if the activation succeeded</returns>
        public bool NMSActivateSatelliteDiversityTerminal(string endUserId, string macAddress, string ipAddress, long slaId, int ispId, int freeZone, string primaryMac, string secondaryMac, Guid userId)
        {
            bool result = false;
            int sitId = this.GetMaxSitId(ispId.ToString()) + 1;
            try
                {
                    //Create the ticket in order to activate the terminal
                    nms.RequestTicket rt = _nmsService.createRegistration(endUserId, macAddress, 32, (int)slaId, ispId, ipAddress, sitId, freeZone);
                    result = this.insertRequestTicketWithSla(rt, macAddress, AdmStatus.Request, AdmStatus.OPERATIONAL, TerminalActivityTypes.Activation, ispId, (int)slaId, userId);
                    //Add an entry in the Sat Diversity table on the NMS
                    _nmsService.AddSatDivEntry(macAddress, primaryMac, secondaryMac);
                }
                catch (Exception ex)
                {
                    this.logException(ex, "NMSGateWay.NMSActivateSatelliteDiversityTerminal");
                }
            
            return result;
        }

        /// <summary>
        /// A helper method for finding the highest Sit ID currently in the CMT db
        /// for a specific ISP
        /// </summary>
        /// <param name="ispId">The ISP ID for which the Sit ID is requested</param>
        /// <returns>The current highest Sit ID for the ISP</returns>
        private int GetMaxSitId(string ispId)
        {
            const string queryCmd = "SELECT MAX(SitId) FROM Terminals " +
                                    "WHERE IspId = @IspId";

            int sitId;
            try
            {
                using (var conn = new SqlConnection(connectionString: ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString))
                {
                    conn.Open();
                    using (var cmd = new SqlCommand(queryCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@IspId", ispId);
                        SqlDataReader reader = cmd.ExecuteReader();

                        reader.Read();
                        sitId = reader.GetInt32(0);
                    }
                }
            }
            catch (Exception ex)
            {
                this.logException(ex, "EutelsatNBIGateway.GetMaxSitId");
                throw;
            }
            return sitId;
        }
    }
}
