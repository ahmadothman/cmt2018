﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Net;
using System.Net.Security;
using System.Data.SqlClient;
using BOControllerLibrary.Controllers;
using BOControllerLibrary.Controllers.Interfaces;
using BOControllerLibrary.Model.Terminal;
using BOControllerLibrary.Model.Log;
using BOControllerLibrary.ISPIF.Model;
using BOControllerLibrary.Model;
using RestSharp;
using System.Web.Script.Serialization;


//NMS webservices
using nms = BOControllerLibrary.NMSGatewayServiceRef;

namespace BOControllerLibrary.ISPIF.Gateway
{
    /// <summary>
    /// This controller provides access to the Avanti OSS
    /// It is used for creating and managing terminals on the Avanti hub
    /// The Avanti interface handles the provisioning of terminals, but the main
    /// SLA and volume management is done by the CG NMS. Therefor, this interface is 
    /// an extenstion of the NMS Gateway and overrides certain hub-specific methods
    /// of the NMS Gateway
    /// </summary>
    public class AvantiNBIGateway : NMSGateway
    {
        private const string DEF_SLA_AV = "DefSla348";
        //private const string ISP_ID_AV = "Avanti348";
        private IBOConfigurationController _configurationController;
        private IBOAccountingController _accountingController;
        private IBOLogController _logController;
        private string _connectionString;
        private string _databaseProvider;
        private RestClient _client;
        private string _baseUrl = "http://10.226.84.24/";
        private string _username = "api_satadsl";
        private string _password = "Avanti123";
        string _hub = "120";
        string _customer = "28678";
        string _partner = "348";
        
        /// <summary>
        /// Default constructor
        /// </summary>
        public AvantiNBIGateway()
        {
            _connectionString = ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
            _databaseProvider = ConfigurationManager.ConnectionStrings["ApplicationServices"].ProviderName;
            _accountingController = new BOAccountingController(_connectionString, _databaseProvider);
            _configurationController = new BOConfigurationController(_connectionString, _databaseProvider);
            _logController = new BOLogController(_connectionString, _databaseProvider);
            _client = new RestClient();
            _client.BaseUrl = new Uri(_baseUrl);
            _client.Authenticator = new HttpBasicAuthenticator(_username, _password);
            _client.CookieContainer = new System.Net.CookieContainer();
        }
        
        /// <summary>
        /// Creates a terminal in the Avanti Hub by means of the ISPIF gateway. 
        /// This method sets the NMSActivate flag which indicates that the 
        /// TicketMaster still needs to create the terminal in the NMS.
        /// </summary>
        /// <param name="endUserId">The end user identifier related to the terminal</param>
        /// <param name="macAddress">The terminals' MAC address</param>
        /// <param name="slaId">
        /// The Service Level identifier. For this version of the IHubGateway interface
        /// the slaId is retrieved from the configuration table since the sla is NOT the sla for the 
        /// activation in the NMS!
        /// </param>
        /// <param name="ispId">The ISP id</param>
        /// <returns>true if the operation succeeds</returns>
        public override bool TerminalActivate(string endUserId, string macAddress, long slaId, int ispId, Guid userId)
        {
            bool result = false;
            string failureReason = "";
            string failureStackTrace = "";
            
            // The CMT terminal
            Terminal cmtTerm = _accountingController.GetTerminalDetailsByMAC(macAddress);

            //Get all the necessary parameters
            string hardwareId = this.GetAvantiHardwareId(macAddress).Trim();
            string ipAddress = cmtTerm.IpAddress.Trim();
            //Create the sit on the Avanti hub
            //Authentication for the Avanti server
            this.AvantiAuthentication();

            //This also links a customer to the sit
            string sitId = this.CreateSit(cmtTerm);
            
            if (sitId != "0")
            {
                //Create the hardware on the Avanti hub
                //this is currently not possible at the Avanti end so instead we look up the hardware in a table in the CMT db
                
                if (hardwareId != "0")
                {
                    //Link the hardware to the sit
                    if (this.LinkHardwareToSit(sitId, hardwareId, macAddress))
                    {
                        // Get available IP address from the Avanti hub
                        // This is done through a local database as no such feature is offered by Avanti
                        if (ipAddress != "0")
                        {
                            //Connect the IP address to the sit
                            if (this.AddIpToSit(sitId, ipAddress))
                            {
                                //Execute the changes to the sit in the hub
                                if (true)//this.ConfigureHub(sitId))
                                {
                                    cmtTerm.SitId = Convert.ToInt32(sitId);
                                    cmtTerm.IpAddress = ipAddress;
                                    cmtTerm.ExtFullName = "AVANTI_" + sitId;
                                    //cmtTerm.IPMask = 32;
                                    //cmtTerm.IPRange = true;
                                    //cmtTerm.AdmStatus = 2; //ugly hack

                                    //Save the terminal details
                                    try
                                    {
                                        _accountingController.UpdateTerminal(cmtTerm);
                                        result = true;
                                    }
                                    catch (Exception ex)
                                    {
                                        this.logException(ex, "AvantiNBIGateway.TerminalActivate");
                                        failureReason = ex.Message;
                                        failureStackTrace = ex.StackTrace;
                                    }
                                }
                                else
                                {
                                    failureReason = "Failed to configure the hub";
                                    failureStackTrace = "MAC address: " + macAddress;
                                }
                            }
                            else
                            {
                                failureReason = "Failed to add the IP address to the sit in the hub";
                                failureStackTrace = "MAC address: " + macAddress;
                            }
                        }
                        else
                        {
                            failureReason = "Failed to get an IP address";
                            failureStackTrace = "MAC address: " + macAddress;
                        }
                    }
                    else
                    {
                        failureReason = "Failed to link the hardware to the sit in the hub";
                        failureStackTrace = "MAC address: " + macAddress;
                    }
                }
                else
                {
                    failureReason = "Failed to get a hardware ID from the hub";
                    failureStackTrace = "MAC address: " + macAddress;
                }
            }
            else
            {
                failureReason = "Failed to get a sit ID from the hub";
                failureStackTrace = "MAC address: " + macAddress;
            }

            this.insertRequestTicketWithSla(!result, macAddress, AdmStatus.Request, AdmStatus.OPERATIONAL, failureReason, failureStackTrace, TerminalActivityTypes.Activation, ispId, (int)cmtTerm.SlaId, userId);
            return result;
        }

        /// <summary>
        /// Changes the SLA of the terminal in the Avanti hub. To change the SLA in the NMS call the
        /// NMSTerminalChangeSLA method.
        /// </summary>
        /// <param name="endUserId">The endUserId of the terminal to activate</param>
        /// <param name="macAddress">The MAC address of the terminal to activate</param>
        /// <param name="newSlaId">The service pack allocated to the terminal</param>
        /// <param name="currentState">The current admin state of the terminal</param>
        /// <returns>true if the application succeeded</returns>
        public override bool TerminalChangeSla(string endUserId, string macAddress, int newSlaId, Model.AdmStatus currentState, int ispId, Guid userId)
        {
            bool result = false;
            string failureReason = "SLA change succeeded";
            string failureStackTrace = "";
            //Get the terminal details form the CMT
            Terminal cmtTerm = _accountingController.GetTerminalDetailsByMAC(macAddress);
            string sitId = cmtTerm.SitId.ToString();
            ServiceLevel newSla = _accountingController.GetServicePack(newSlaId);
            //Set the parameters for the Avanti hub request
            this.AvantiAuthentication();
            //get the E-Tag for the next step
            string eTag = this.GetEtagForSit(sitId);

            //send the update to the hub
            RestRequest updateSit = new RestRequest();
            updateSit.AddHeader("If-Match", eTag);
            updateSit.AddParameter("partner", _partner);
            updateSit.AddParameter("sitNum", sitId);
            updateSit.AddParameter("ipConnectivity", "routed");
            updateSit.AddParameter("hub", _hub);
            updateSit.AddParameter("sla", Int32.Parse(newSla.SlaCommonName));
            updateSit.AddParameter("reason", "New_SLA");
            updateSit.Method = Method.PUT;
            updateSit.Resource = "services/rest/default/Sit/" + sitId;
            IRestResponse response = _client.Execute(updateSit);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                if (this.ReConfigureHub(sitId))
                {
                    result = true;
                }
                else
                {
                    failureReason = "SLA change failed in the Avanti hub";
                    failureStackTrace = "";
                }
            }
            else
            {
                failureReason = "SLA change failed in the Avanti hub";
                failureStackTrace = "";
            }

            //Create a CMT ticket for logging

            this.insertRequestTicketWithSla(!result, macAddress, currentState, currentState, failureReason, failureStackTrace, TerminalActivityTypes.ChangeSla, ispId, newSlaId, userId);

            if (result)
            {
                return this.NMSTerminalChangeSla(endUserId, macAddress, newSlaId, currentState, ispId, userId);
            }
            else
            {
                return result;
            }
        }

        /// <summary>
        /// Decommissions a terminal in the Avanti hub. 
        /// The terminal is effectively removed from the Hub database
        /// </summary>
        /// <param name="endUserId">The endUserId of the terminal to activate</param>
        /// <param name="macAddress">The MAC address of the terminal to decommission</param>
        /// <returns>true if the activation succeeded</returns>
        public override bool TerminalDecommission(string endUserId, string macAddress, int ispId, Guid userId)
        {
            bool result = false;
            string failureReason = "";
            string failureStackTrace = "";
            //Get the terminal details form the CMT
            Terminal cmtTerm = _accountingController.GetTerminalDetailsByMAC(macAddress);

            this.AvantiAuthentication();
            //Delete the terminal in the hub
            if (this.DeleteTerminalFromHub(cmtTerm.SitId.ToString()))
            {
                //Unlink the IP address in the hub
                string ipDevice = this.GetIpDeviceId(cmtTerm.SitId.ToString()).Trim();
                if (ipDevice != "0")
                {
                    if (this.RemoveIp(ipDevice))
                    {
                        if(this.UnLinkHardware(cmtTerm.SitId.ToString()))
                            result = true;
                    }
                }
                
                //ugly hack
                //cmtTerm.AdmStatus = 3;
                //_accountingController.UpdateTerminal(cmtTerm);
            }
            this.EndSession();

            this.insertRequestTicket(!result, macAddress, AdmStatus.LOCKED, AdmStatus.DECOMMISSIONED, failureReason, failureStackTrace, TerminalActivityTypes.Decommissioning, ispId, userId);
            return result;
        }

        /// <summary>
        /// Reactivates a terminal in the Avanti hub
        /// </summary>
        /// <param name="endUserId"></param>
        /// <param name="macAddress"></param>
        /// <param name="ispId"></param>
        /// <returns></returns>
        public override bool TerminalReActivate(string endUserId, string macAddress, int ispId, Guid userId)
        {
            bool result = false;
            string failureReason = "";
            string failureStackTrace = "";
            //Get the terminal details form the CMT
            Terminal cmtTerm = _accountingController.GetTerminalDetailsByMAC(macAddress);

            this.AvantiAuthentication();
            //unlock the terminal in the hub
            if (this.TerminalStateTransition(cmtTerm.SitId.ToString(), "operational"))
            {
                result = true;
                //ugly hack
                //cmtTerm.AdmStatus = 2;
                //_accountingController.UpdateTerminal(cmtTerm);
            }
            this.EndSession();

            this.insertRequestTicket(!result, macAddress, AdmStatus.LOCKED, AdmStatus.OPERATIONAL, failureReason, failureStackTrace, TerminalActivityTypes.ReActivation, ispId, userId);
            return result;
        }

        /// <summary>
        /// Suspends a terminal, the status of the terminal becomes LOCKED
        /// </summary>
        /// <param name="endUserId">The endUserId of the terminal to activate</param>
        /// <param name="macAddress">The MAC address of the terminal to activate</param>
        /// <returns>true if the re-activation succeeded</returns>
        public override bool TerminalSuspend(string endUserId, string macAddress, int ispId, Guid userId)
        {
            bool result = false;
            string failureReason = "";
            string failureStackTrace = "";
            //Get the terminal details form the CMT
            Terminal cmtTerm = _accountingController.GetTerminalDetailsByMAC(macAddress);

            this.AvantiAuthentication();
            //Lock the terminal in the hub
            if (this.TerminalStateTransition(cmtTerm.SitId.ToString(), "suspensionblock"))
            {
                result = true;
                //ugly hack
                //cmtTerm.AdmStatus = 1;
                //_accountingController.UpdateTerminal(cmtTerm);
            }
            this.EndSession();

            this.insertRequestTicket(!result, macAddress, AdmStatus.OPERATIONAL, AdmStatus.LOCKED, failureReason, failureStackTrace, TerminalActivityTypes.Suspension, ispId, userId);
            return result;
        }

        /// <summary>
        /// Changes the MacAddress in the Avanti Hub
        /// </summary>
        /// <param name="endUserId"></param>
        /// <param name="oriMacAddress"></param>
        /// <param name="newMacAddress"></param>
        /// <param name="ispId"></param>
        /// <returns></returns>
        public override bool ChangeMacAddress(string endUserId, string oriMacAddress, string newMacAddress, int ispId, Guid userId)
        {
            bool result = false;
            string failureReason = "";
            string failureStackTrace = "";
            //Get the terminal details form the CMT
            Terminal cmtTerm = _accountingController.GetTerminalDetailsByMAC(oriMacAddress);
            string sitId = cmtTerm.SitId.ToString();
            this.AvantiAuthentication();
            //First deactivate the terminal in the Avanti hub
            if (this.DeleteTerminalFromHub(sitId))
            {
                //Remove the link between the MAC address and the terminal in the hub
                if (this.UnLinkHardware(sitId))
                {
                    //Remove the link to the MAC address in the CMT db
                    //this.UnLinkHardwareInCMTDb(oriMacAddress);
                    //Fetch the hardware ID necessary for communication with Avanti
                    string hwId = this.GetAvantiHardwareId(newMacAddress).Trim();
                    if (hwId != "0")
                    {
                        //Link the new hardware in the hub
                        if (this.LinkHardwareToSit(sitId, hwId, newMacAddress))
                        {
                            //Re-activate the terminal in the hub
                            if (this.ConfigureHub(sitId))
                            {
                                result = true;
                                //ugly hack
                                //cmtTerm.MacAddress = newMacAddress;
                                //_accountingController.UpdateTerminal(cmtTerm);
                            }
                            else
                            {
                                failureReason = "Failed to re-activate the terminal in the avanti hub";
                                failureStackTrace = "MAC address: " + newMacAddress;
                            }
                        }
                        else
                        {
                            failureReason = "Failed to link the hardware to the sit in the hub";
                            failureStackTrace = "New MAC address: " + newMacAddress +" Sit ID: " + sitId;
                        }
                    }
                    else
                    {
                        failureReason = "New MAC address not found in the CMT database";
                        failureStackTrace = "MAC address: " + newMacAddress;
                    }
                }
                else
                {
                    failureReason = "Failed to unlink the hardware in the hub";
                    failureStackTrace = "MAC address: " + oriMacAddress;
                }
            }
            else
            {
                failureReason = "Failed to delete terminal from hub";
                failureStackTrace = "MAC address: " + oriMacAddress;
            }
            this.EndSession();

            this.insertRequestTicketWithMac(!result, oriMacAddress, AdmStatus.OPERATIONAL, AdmStatus.OPERATIONAL, failureReason, failureStackTrace, TerminalActivityTypes.ChangeMac, ispId, newMacAddress, userId);
            return result;
        }

        /// <summary>
        /// This method retrieves the terminal information from the CMT Database
        /// </summary>
        /// <param name="macAddress">The terminal MAC address</param>
        /// <param name="ispId">The ISP identifier, this value is not really used for this implementation</param>
        /// <returns>A completed TerminalInfo object</returns>
        public override TerminalInfo getTerminalDetailsFromHub(string macAddress, string ispId)
        {
            TerminalInfo ti = new TerminalInfo();
            Terminal term = _accountingController.GetTerminalDetailsByMAC(macAddress);

            //Copy the terminal data to a terminal information object
            try
            {
                //if (term.Blade != null)
                ti.BladeId = "1";// term.Blade.ToString();

                if (term.AdmStatus != null)
                    ti.Blocked = (term.AdmStatus == 2);
                if (term.FullName != null)
                    ti.EndUserId = term.FullName;

                if (term.IpAddress != null)
                    ti.IPAddress = term.IpAddress.Trim();

                ti.IspId = term.IspId.ToString();

                if (term.MacAddress != null)
                    ti.MacAddress = term.MacAddress;

                ti.SitId = term.SitId.ToString();

                if (term.SlaId != null)
                    ti.SlaId = term.SlaId.ToString();

                if (term.SlaName != null)
                    ti.SlaName = term.SlaName;

                if (term.AdmStatus != null)
                    ti.Status = term.AdmStatus.ToString();
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDateTime = DateTime.Now;
                cmtEx.ExceptionDesc = ex.Message;
                cmtEx.ExceptionLevel = (short)ExceptionLevelEnum.Error;
                cmtEx.ExceptionStacktrace = ex.StackTrace;
                cmtEx.StateInformation = "Error";
                cmtEx.UserDescription = "Copy from Terminal to TerminalInfo object failed.";
                _logController.LogApplicationException(cmtEx);
            }
            
            return ti;
        }

        /// <summary>
        /// Returns the C/No value. This value is retrieved from the Avanti Hub
        /// </summary>
        /// <remarks>
        /// This value is not available from the Avanti hub
        /// For the moment being we are returning 9999 to indicate a false reading.
        /// </remarks>
        /// <param name="ispId">The unique ISP identifier</param>
        /// <param name="sitId">The site id</param>
        /// <returns>The C/No value as a string</returns>
        public override string getRTNValue(string ispId, string sitId)
        {
            return "9999";
        }


        /// <summary>
        /// Returns the ESN0 value. This value is retrieved from the Avanti Hub
        /// </summary>
        /// <remarks>
        /// This value is not available from the Avanti hub but needs further investigation.
        /// For the moment being we are returning 9999 to indicate a false reading.
        /// </remarks>
        /// <param name="ispId">The unique ISP identifier</param>
        /// <param name="sitId">The site id</param>
        /// <returns>The C/No value as a string</returns>
        public override string getFWDValue(string ispId, string sitId)
        {
            return "9999";
        }
        /// <summary>
        /// Activates a terminal in the NMS.
        /// </summary>
        /// <param name="endUserId">The end user identifier</param>
        /// <param name="macAddress">The terminal MAC address</param>
        /// <param name="slaId">The SLA identifier</param>
        /// <param name="ispId">The ISP idnetifier</param>
        /// <param name="ipMask">IP range specified by means of a MASK</param>
        /// <returns>True if the method succeeds</returns>
        public override bool NMSTerminalActivateWithRange(string endUserId, string macAddress, long slaId, int ispId, int ipMask, int freeZone, Guid userId)
        {
            bool result = false;
            nms.NMSGatewayService nmsService = new nms.NMSGatewayService();
            try
            {
                //Need to specify an IP Address as this address is not generated by the NMS but by the Eutelsat HUB
                TerminalInfo ti = this.getTerminalDetailsFromHub(macAddress, ispId.ToString());
                nms.RequestTicket rt = nmsService.createRegistration(endUserId, macAddress, ipMask, (int)slaId, ispId, ti.IPAddress, Int32.Parse(ti.SitId), freeZone);
                result = this.insertRequestTicketWithSla(rt, macAddress, AdmStatus.LOCKED, AdmStatus.OPERATIONAL, TerminalActivityTypes.Activation, ispId, (int)slaId, userId);
            }
            catch (Exception ex)
            {
                this.logException(ex, "NMSGateWay.TerminalActivate");
            }
            return result;
        }

        /// <summary>
        /// Activates a terminal in the NMS without specifying a range by means of
        /// an IP Mask value.
        /// </summary>
        /// <remarks>
        /// This method activates a terimal in the NMS without specifiying a range, thus the
        /// IP Mask is always set to \32 which means that only one IP address can be specified.
        /// </remarks>
        /// <param name="endUserId">The end user identifier</param>
        /// <param name="macAddress">The terminal MAC address</param>
        /// <param name="slaId">The SLA identifier</param>
        /// <param name="ispId">The ISP identifier</param>
        /// <returns>True if the method succeeds</returns>
        public override bool NMSTerminalActivate(string endUserId, string macAddress, long slaId, int ispId, int freeZone, Guid userId)
        {
            nms.NMSGatewayService nmsService = new nms.NMSGatewayService(); 
            bool result = false;

            try
            {
                //Need to specify an IP Address as this address is not generated by the NMS but by the Avanti HUB
                //TerminalInfo ti = this.getTerminalDetailsFromHub(macAddress, ispId.ToString());
                Terminal term = _accountingController.GetTerminalDetailsByMAC(macAddress);
                nms.RequestTicket rt = nmsService.createRegistration(endUserId, macAddress, 32, (int)slaId, ispId, term.IpAddress, term.SitId, freeZone);
                result = base.insertRequestTicket(rt, macAddress, AdmStatus.Request, AdmStatus.OPERATIONAL, TerminalActivityTypes.Activation, ispId, userId);
            }
            catch (Exception ex)
            {
                this.logException(ex, "NMSGateWay.TerminalActivate");
            }
            return result;
        }

        /// <summary>
        /// This method allows to update the freezone information for a terminal
        /// </summary>
        /// <param name="FreeZoneId">The identifier of the freezone allocated to the terminal</param>
        /// <param name="macAddress">The MAC address of the terminal</param>
        /// <param name="FreeZoneFlag">If this flag is set to true, a freezone option is allocated to the terminal. In
        /// case this value is false, the terminal does not have the freezone option</param>
        /// <returns></returns>
        public bool NMSModifyFreeZoneForTerminal(int FreeZoneId, string macAddress, Boolean FreeZoneFlag)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Inserts a new ticket in the CMT database and flags it as an NMS ticket
        /// </summary>
        /// <remarks>
        /// Use this version of the insertRequestTicket method to insert a request ticket for activities
        /// on the Avanti hub in the database
        /// </remarks>
        /// <param name="failed">Has status true if the request on the Avanti hub failed</param>
        /// <param name="macAddress">The MacAddress of the target</param>
        /// <param name="sourceAdmStatus">The Source admin status</param>
        /// <param name="targetAdmStatus">The Target admin status</param>
        /// <param name="termActivityType">The terminal activity type</param>
        /// <param name="ispId">The ISP ID</param>
        /// <returns>True if the operation succeeded</returns>
        private bool insertRequestTicket(bool failed, string macAddress, AdmStatus sourceAdmStatus, AdmStatus targetAdmStatus, string failureReason, string failureStackTrace,
                                           TerminalActivityTypes termActivityType, int ispId, Guid userId)
        {
            TicketDataMgr ticketDataMgr = new TicketDataMgr();
            Guid ticketId = Guid.NewGuid();
            if (failureReason.Length > 400)
            {
                failureReason = failureReason.Substring(0, 400);
            }
            int ticketStatus = 0;
            if (failed)
            {
                ticketStatus = 1;
            }
            return ticketDataMgr.insertTicket(ticketId.ToString(), macAddress, sourceAdmStatus, targetAdmStatus, ticketStatus, failureReason, failureStackTrace, termActivityType, ispId, 1, userId);
        }

        /// <summary>
        /// Inserts a ticket  for an SLA change
        /// </summary>
        /// <remarks>
        /// Use this version of the insertRequestTicket method to insert a request ticket for activities
        /// on the Eutelsat hub in the database
        /// </remarks>
        /// <param name="failed">Has status true if the request on the Eutelsat hub failed</param>
        /// <param name="macAddress">The Mac Address</param>
        /// <param name="sourceAdmStatus">The source amdin status</param>
        /// <param name="targetAdmStatus">The target admin status</param>
        /// <param name="termActivityType">The terminal activity type</param>
        /// <param name="ispId">The ISP identifier</param>
        /// <param name="newSla">The new sla</param>
        /// <returns>True if the method succeeded</returns>
        private bool insertRequestTicketWithSla(bool failed, string macAddress, AdmStatus sourceAdmStatus, AdmStatus targetAdmStatus, string failureReason, string failureStackTrace,
                                           TerminalActivityTypes termActivityType, int ispId, int newSla, Guid userId)
        {
            TicketDataMgr ticketDataMgr = new TicketDataMgr();
            Guid ticketId = Guid.NewGuid();
            if (failureReason.Length > 400)
            {
                failureReason = failureReason.Substring(0, 400);
            } 
            int ticketStatus = 0;
            if (failed)
            {
                ticketStatus = 1;
            }
            return ticketDataMgr.insertTicket(ticketId.ToString(), macAddress, sourceAdmStatus, targetAdmStatus, ticketStatus, failureReason, failureStackTrace, termActivityType, ispId, 1, newSla, userId);
        }

        /// <summary>
        /// Inserts a ticket for a MAC Address change
        /// </summary>
        /// <remarks>
        /// Use this version of the insertRequestTicket method to insert a request ticket for activities
        /// on the Eutelsat hub in the database
        /// </remarks>
        /// <param name="failed">Has status true if the request on the Eutelsat hub failed</param>
        /// <param name="macAddress">The MAC address of the terminal</param>
        /// <param name="sourceAdmStatus">The sourde administration status</param>
        /// <param name="targetAdmStatus">The target administration status</param>
        /// <param name="termActivityType">The terminal activity type</param>
        /// <param name="ispId">The ISP identifier</param>
        /// <param name="NewMacAddress">The new mac address</param>
        /// <returns></returns>
        private bool insertRequestTicketWithMac(bool failed, string macAddress, AdmStatus sourceAdmStatus, AdmStatus targetAdmStatus, string failureReason, string failureStackTrace,
                                           TerminalActivityTypes termActivityType, int ispId, string NewMacAddress, Guid userId)
        {
            TicketDataMgr ticketDataMgr = new TicketDataMgr();
            Guid ticketId = Guid.NewGuid();
            if (failureReason.Length > 400)
            {
                failureReason = failureReason.Substring(0, 400);
            }
            int ticketStatus = 0;
            if (failed)
            {
                ticketStatus = 1;
            } 
            return ticketDataMgr.insertTicket(ticketId.ToString(), macAddress, sourceAdmStatus, targetAdmStatus, ticketStatus, failureReason, failureStackTrace, termActivityType, ispId, 1, NewMacAddress, userId);
        }

        /// <summary>
        /// Returns the status of a request ticket.
        /// </summary>
        /// <remarks>
        /// This method gets the ticket from the CMT database 
        /// </remarks>
        /// <param name="requestTicketId">The requestticket identfier</param>
        /// <param name="ispId">The ISP identfier</param>
        /// <returns>
        /// The corresponding CMT request ticket or null if the ticket could not 
        ///  be found
        ///  </returns>
        public override CmtRequestTicket LookupRequestTicket(string requestTicketId, int ispId)
        {
            CmtRequestTicket cmtRt = null;
            try
            {
                CMTTicket cmtTick = _logController.GetTicketById(requestTicketId);
                cmtRt = new CmtRequestTicket();
                cmtRt.id = cmtTick.TicketId;
                cmtRt.failureReason = cmtTick.FailureReason;
                cmtRt.failureStackTrace = cmtTick.FailureStackTrace;

                if (cmtTick.TicketStatus == 0)
                {
                    cmtRt.requestStatus = CmtRequestStatus.SUCCESSFUL;
                }

                if (cmtTick.TicketStatus == 1)
                {
                    cmtRt.requestStatus = CmtRequestStatus.FAILED;
                }

                if (cmtTick.TicketStatus == 2)
                {
                    cmtRt.requestStatus = CmtRequestStatus.SUCCESSFUL;
                }
            }
            catch (Exception ex)
            {
                this.logException(ex, "AvantiNBIGateway.LookupRequestTicket");
                cmtRt = null;
            }

            return cmtRt;
        }

        #region Avanti methods

        /// <summary>
        /// This methods connects to the Avanti hub to set up a session
        /// The call returns a cookie which is used for the authentication of
        /// subsequent calls. The cookie is stored in a container of the client
        /// </summary>
        private void AvantiAuthentication()
        {
            RestRequest session = new RestRequest();
            session.Method = Method.POST;
            session.Resource = "services/rest/Session";
            session.AddParameter("username", _username);
            session.AddParameter("password", _password);
            IRestResponse response = _client.Execute(session);

            CookieContainer cookieCon = new CookieContainer();

            if (response.StatusCode == HttpStatusCode.OK)
            {
                var cookie = response.Cookies.FirstOrDefault();
                cookieCon.Add(new Cookie(cookie.Name, cookie.Value, cookie.Path, cookie.Domain));
            }

            _client.CookieContainer = cookieCon;
        }

        /// <summary>
        /// This method ends the current session at the Avanti hub
        /// </summary>
        private void EndSession()
        {
            RestRequest logout = new RestRequest();
            logout.Resource = "logout";
            IRestResponse response = _client.Execute(logout);
        }

        /// <summary>
        /// This method is used to create a new Sit on the Avanti hub
        /// </summary>
        /// <param name="term">The terminal details to be used</param>
        /// <returns>A string containing the ID of the newly created Sit</returns>
        private string CreateSit(Terminal term)
        {
            string sitId = "0";
            //fetch the SLA details
            ServiceLevel sla = _accountingController.GetServicePack((int)term.SlaId);
            
            //create the call to Avanti
            RestRequest newSit = new RestRequest();
            newSit.Resource = "services/rest/default/Sit";
            newSit.Method = Method.POST;
            newSit.AddParameter("sla", Int32.Parse(sla.SlaCommonName));
            newSit.AddParameter("partner", _partner);
            //The sit number is a parameter required by the hub, but has no relevance to the CMT. A helper method is used to fetch the next sit number from the CMT db
            int sitNum = this.GetNextAvantiSitNumber();
            newSit.AddParameter("sitNum", sitNum);
            newSit.AddParameter("ipConnectivity", "routed");
            newSit.AddParameter("hub", _hub);

            //Execute the call
            IRestResponse response = _client.Execute(newSit);

            if (response.StatusCode == HttpStatusCode.Created)
            {
                //store the sit ID
                JavaScriptSerializer js = new JavaScriptSerializer();
                var dict = js.Deserialize<Dictionary<string, dynamic>>(response.Content);
                sitId = dict["Sit"]["id"];

                //get the E-Tag for the next step
                string eTag = this.GetEtagForSit(sitId);

                //link the default customer to the sit
                newSit.AddHeader("If-Match", eTag);
                newSit.AddParameter("customer", _customer);
                newSit.AddParameter("reason", "Linked_customer");
                newSit.Method = Method.PUT;
                newSit.Resource = "services/rest/default/Sit/" + sitId;
                response = _client.Execute(newSit);
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    sitId = "0";
                }
            }

            return sitId;
        }

        /// <summary>
        /// Links an Avanti sit to a specific hardware in the Avanti hub
        /// </summary>
        /// <param name="sitId">The ID of the sit</param>
        /// <param name="hardwareId">The ID of the hardware</param>
        /// <param name="macAddress">The macAddress of the hardware</param>
        /// <returns>True if successful</returns>
        private bool LinkHardwareToSit(string sitId, string hardwareId, string macAddress)
        {
            bool result = false;

            RestRequest link = new RestRequest();
            link.Resource = "services/rest/default/Sit/" + sitId + "/link_hardware";
            link.AddParameter("hardwareId", hardwareId);
            link.AddParameter("reason", "Linked_hardware");
            link.Method = Method.PUT;

            IRestResponse response = _client.Execute(link);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                result = true;
                //store the changes in the CMT database
                this.LinkHardwareInCMTDb(macAddress, sitId);
            }

            return result;
        }

        /// <summary>
        /// This method removes the linked hardware from a sit on the avanti hub
        /// </summary>
        /// <param name="sitId">The ID of the sit</param>
        /// <returns>True if successful</returns>
        private bool UnLinkHardware(string sitId)
        {
            bool result = false;

            RestRequest link = new RestRequest();
            link.Resource = "services/rest/default/Sit/" + sitId + "/unlink_hardware";
            link.AddParameter("reason", "Unlinked_hardware");
            link.Method = Method.PUT;

            IRestResponse response = _client.Execute(link);

            if (response.StatusCode == HttpStatusCode.Accepted)
            {
                result = true;
            }

            return result;
        }

        /// <summary>
        /// Helper method to get the E-Tag for a sit which is required when making updates to a sit object in the Avanti system
        /// </summary>
        /// <param name="sitId">the ID of the sit</param>
        /// <returns>the E-Tag</returns>
        public string GetEtagForSit(string sitId)
        {
            string eTag = "";

            RestRequest get = new RestRequest();
            get.Resource = "services/rest/default/Sit/" + sitId;
            IRestResponse response = _client.Execute(get);
            foreach (var v in response.Headers)
            {
                if (v.Name == "ETag")
                {
                    eTag = v.Value.ToString();
                }
            }

            return eTag;
        }

        /// <summary>
        /// Simply converts an IP address in a string format into a uint
        /// </summary>
        /// <param name="sIpAddress">The IP address</param>
        /// <returns>A uint</returns>
        private uint ConvertIPToLong(string sIpAddress)
        {
            byte[] addressBytes = IPAddress.Parse(sIpAddress).GetAddressBytes();
            byte[] invAddressBytes = new Byte[4];

            invAddressBytes[3] = addressBytes[0];
            invAddressBytes[2] = addressBytes[1];
            invAddressBytes[1] = addressBytes[2];
            invAddressBytes[0] = addressBytes[3];

            return BitConverter.ToUInt32(invAddressBytes, 0);
        }

        /// <summary>
        /// This methods connects an IP address (range) to a sit in the Avanti hub
        /// </summary>
        /// <param name="sitId">The ID of the sit</param>
        /// <param name="ipAddress">The IP address</param>
        /// <returns>true if successful</returns>
        public bool AddIpToSit(string sitId, string ipAddress)
        {
            bool result = false;

            RestRequest addIp = new RestRequest();
            addIp.Resource = "services/rest/default/Sit/" + sitId + "/add_ip";
            addIp.AddParameter("ipAddressType", "POP Network");
            //Convert the IP address to the correct format for the hub
            uint ip = this.ConvertIPToLong(ipAddress);
            //Avanti expects the second address in the IP range
            ip = ip + 1;
            addIp.AddParameter("ipAddress", ip.ToString());
            addIp.Method = Method.PUT;

            IRestResponse response = _client.Execute(addIp);

            if (response.StatusCode == HttpStatusCode.Created)
            {
                result = true;
                //Get the ID of the new IP device that was created in the Avanti hub
                JavaScriptSerializer js = new JavaScriptSerializer();
                var dict = js.Deserialize<Dictionary<string, dynamic>>(response.Content);
                string ipDeviceId = dict["IpDevice"]["id"];
                //store the change in the CMT database as well
               // this.LinkIPToSit(ipAddress, sitId, ipDeviceId);
            }

            return result;
        }

        /// <summary>
        /// Removes the IP from the sit in the Avanti Hub
        /// </summary>
        /// <param name="ipDevice">The ID of the IP address linke to the sit</param>
        /// <returns>true if successful</returns>
        public bool RemoveIp(string ipDevice)
        {
            bool result = false;
            string eTag = "";
            
            RestRequest get = new RestRequest();
            get.Resource = "services/rest/default/IpDevice/" + ipDevice;
            IRestResponse response = _client.Execute(get);
            foreach (var v in response.Headers)
            {
                if (v.Name == "ETag")
                {
                    eTag = v.Value.ToString();
                }
            }

            RestRequest delIp = new RestRequest();
            delIp.AddHeader("If-Match", eTag);
            delIp.Resource = "services/rest/default/IpDevice/" + ipDevice;
            delIp.AddParameter("text/plain", "reason=removing", ParameterType.RequestBody);
            //delIp.AddBody("reason=removing");
            delIp.Method = Method.DELETE;
            response = _client.Execute(delIp);

            if (response.StatusCode == HttpStatusCode.Accepted || response.StatusCode == HttpStatusCode.OK)
            {
                result = true;
            }
            
            return result;
        }

        /// <summary>
        /// This method executes the changes to the sit object in the hub
        /// </summary>
        /// <param name="sitId">The ID of the sit</param>
        /// <returns>True if successful</returns>
        public bool ConfigureHub(string sitId)
        {
            bool result = false;

            //Get the hardware model for the sit, which is required when configuring the hub
            string hw = "MDM2210"; //this.GetHardwareModel(sitId);

            if (hw != "")
            {
                RestRequest put = new RestRequest();
                put.Resource = "services/rest/default/Sit/" + sitId + "/configure_hub";
                put.Method = Method.PUT;
                put.AddParameter("hwModel", hw);

                IRestResponse response = _client.Execute(put);

                if (response.StatusCode == HttpStatusCode.Accepted)
                {
                    result = true;
                }
            }
            return result;
        }

        /// <summary>
        /// This method executes the changes to the sit object in the hub
        /// </summary>
        /// <param name="sitId">The ID of the sit</param>
        /// <returns>True if successful</returns>
        public bool ReConfigureHub(string sitId)
        {
            bool result = false;
            string eTag = this.GetEtagForSit(sitId);

            RestRequest put = new RestRequest();
            put.AddHeader("If-Match", eTag);
            put.Resource = "services/rest/default/Sit/" + sitId + "/reconfigure_hub";
            put.Method = Method.PUT;
            
            IRestResponse response = _client.Execute(put);

            if (response.StatusCode == HttpStatusCode.Accepted)
            {
                result = true;
            }
            
            return result;
        }

        /// <summary>
        /// This methods deletes the terminal from the Avanti hub
        /// </summary>
        /// <param name="sitId">The sit ID of the terminal</param>
        /// <returns>True if successful</returns>
        public bool DeleteTerminalFromHub(string sitId)
        {
            RestRequest put = new RestRequest();
            put.Resource = "services/rest/default/Sit/" + sitId + "/decommission";
            put.Method = Method.PUT;
            //put.AddParameter("reason", "decommission");

            IRestResponse response = _client.Execute(put);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Stores the hardware-sit link in the CMT db
        /// This should be done after the link has been made in the Avanti hub
        /// </summary>
        /// <param name="macAddress">the MAC address of the hardware</param>
        /// <param name="sitId">the sit ID of the terminal</param>
        /// <returns>True if successful</returns>
        private bool LinkHardwareInCMTDb(string macAddress, string sitId)
        {
            bool result = false;

            try
            {
                var var1 = new StringBuilder();
                var1.Append("UPDATE AvantiHardware \n");
                var1.Append("SET SitId = @SitId \n");
                var1.Append("WHERE MacAddress = @MacAddress");

                using (SqlConnection con = new SqlConnection(_connectionString))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(var1.ToString(), con))
                    {
                        cmd.Parameters.AddWithValue("@SitId", sitId);
                        cmd.Parameters.AddWithValue("@MacAddress", macAddress);

                        int rows = cmd.ExecuteNonQuery();

                        if (rows == 1)
                        {
                            result = true;
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "AvantiNBIGateway: Error while linking sit ID to hardware",
                                                                                   "MAC: " + macAddress + ", Sit: " + sitId, (short)ExceptionLevelEnum.Error));
                result = false;
            }

            return result;
        }

        /// <summary>
        /// Removes the hardware-sit link in the CMT db
        /// This should be done after the link has been removed in the Avanti hub
        /// </summary>
        /// <param name="macAddress">the MAC address of the hardware</param>
        /// <param name="sitId">the sit ID of the terminal</param>
        /// <returns>True if successful</returns>
        private bool UnLinkHardwareInCMTDb(string macAddress)
        {
            bool result = false;

            try
            {
                var var1 = new StringBuilder();
                var1.Append("UPDATE AvantiHardware \n");
                var1.Append("SET SitId = 0 \n");
                var1.Append("WHERE MacAddress = @MacAddress");

                using (SqlConnection con = new SqlConnection(_connectionString))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(var1.ToString(), con))
                    {
                        cmd.Parameters.AddWithValue("@MacAddress", macAddress);

                        int rows = cmd.ExecuteNonQuery();

                        if (rows == 1)
                        {
                            result = true;
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "AvantiNBIGateway: Error while unlinking sit ID from hardware",
                                                                                   macAddress, (short)ExceptionLevelEnum.Error));
                result = false;
            }

            return result;
        }

        /// <summary>
        /// This method returns the Avanti hardware ID to be used when communicating about the hardware
        /// with the Avanti hub
        /// </summary>
        /// <param name="macAddress">The MAC address of the hardware</param>
        /// <returns>The hardware ID if found, otherwise 0 is returned, meaning the MAC address doesn't exist in the CMT table of Avanti hardware</returns>
        private string GetAvantiHardwareId(string macAddress)
        {
            string hardwareId = "0";
            try
            {
                var var1 = new StringBuilder();
                var1.Append("SELECT AvantiId FROM AvantiHardware \n");
                var1.Append("WHERE MacAddress = @MacAddress");

                using (SqlConnection con = new SqlConnection(_connectionString))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(var1.ToString(), con))
                    {
                        cmd.Parameters.AddWithValue("@MacAddress", macAddress);
                        SqlDataReader reader = cmd.ExecuteReader();
                        reader.Read();
                        hardwareId = reader.GetString(0);
                    }
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "AvantiNBIGateway: Error while getting hardware ID",
                                                                                   macAddress, (short)ExceptionLevelEnum.Error));
                hardwareId = "0";
            }

            return hardwareId;
        }

        /// <summary>
        /// This methods looks up the next available IP address in the Avanti range in the CMT db
        /// </summary>
        /// <returns>An IP address if available, if not 0 is returned</returns>
        private string GetNextAvailableAvantiIP()
        {
            string ipAddr = "0";
            try
            {
                var var1 = new StringBuilder();
                var1.Append("SELECT IPAddress FROM AvantiIPAddresses \n");
                var1.Append("WHERE SitId = 0 OR SitId IS NULL");

                using (SqlConnection con = new SqlConnection(_connectionString))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(var1.ToString(), con))
                    {
                        
                        SqlDataReader reader = cmd.ExecuteReader();
                        reader.Read();
                        if (reader.HasRows)
                        {
                            ipAddr = reader.GetString(0);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "AvantiNBIGateway: Error while getting IP address",
                                                                                   "", (short)ExceptionLevelEnum.Error));
                ipAddr = "0";
            }

            return ipAddr;
        }

        /// <summary>
        /// Removes the IP-sit link in the CMT db
        /// This should be done after the link has been removed in the Avanti hub
        /// </summary>
        /// <param name="ipAddress">the IP address</param>
        /// <returns>True if successful</returns>
        private bool UnLinkIPFromSit(string ipAddress)
        {
            bool result = false;

            try
            {
                var var1 = new StringBuilder();
                var1.Append("UPDATE AvantiIPAddresses \n");
                var1.Append("SET SitId = 0 \n");
                var1.Append("WHERE IPAddress = @IPAddress");

                using (SqlConnection con = new SqlConnection(_connectionString))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(var1.ToString(), con))
                    {
                        cmd.Parameters.AddWithValue("@IPAddress", ipAddress);

                        int rows = cmd.ExecuteNonQuery();

                        if (rows == 1)
                        {
                            result = true;
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "AvantiNBIGateway: Error while unlinking sit ID from IP address",
                                                                                   ipAddress, (short)ExceptionLevelEnum.Error));
                result = false;
            }

            return result;
        }

        /// <summary>
        /// Stores the IP-sit link in the CMT db
        /// This should be done after the link has been stored in the Avanti hub
        /// </summary>
        /// <param name="ipAddress">the IP address</param>
        /// <param name="sitId">the sit ID of the terminal</param>
        /// <param name="deviceId">the ID of the IP device created in the Avanti hub</param>
        /// <returns>True if successful</returns>
        private bool LinkIPToSit(string ipAddress, string sitId, string deviceId)
        {
            bool result = false;

            try
            {
                var var1 = new StringBuilder();
                var1.Append("UPDATE AvantiIPAddresses \n");
                var1.Append("SET SitId = @SitId, DeviceId = @DeviceId \n");
                var1.Append("WHERE IPAddress = @IPAddress");

                using (SqlConnection con = new SqlConnection(_connectionString))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(var1.ToString(), con))
                    {
                        cmd.Parameters.AddWithValue("@SitId", sitId);
                        cmd.Parameters.AddWithValue("@DeviceId", deviceId);
                        cmd.Parameters.AddWithValue("@IPAddress", ipAddress);

                        int rows = cmd.ExecuteNonQuery();

                        if (rows == 1)
                        {
                            result = true;
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "AvantiNBIGateway: Error while unlinking sit ID from IP address",
                                                                                   "IP: " + ipAddress + ", Sit: " + sitId, (short)ExceptionLevelEnum.Error));
                result = false;
            }

            return result;
        }

        /// <summary>
        /// Returns the IP device ID for a given sit ID. The IP device is an Avanti object used for identifying the IP address in the hub
        /// </summary>
        /// <param name="sitId">The sit ID of the terminal to which the IP device is linked</param>
        /// <returns>An IP device ID</returns>
        public string GetIpDeviceId(string sitId)
        {
            string ipDevice = "0";
            RestRequest get = new RestRequest();
            get.Resource = "services/rest/default/IpDevice?filter=device:=:" + sitId;
            IRestResponse response = _client.Execute(get);
            try
            {
                JavaScriptSerializer js = new JavaScriptSerializer();
                var dict = js.Deserialize<Dictionary<string, dynamic>>(response.Content);
                ipDevice = dict["objects"]["IpDevice"]["id"];
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "AvantiNBIGateway: Error while getting deviceIP_ID from the hub",
                                                                                   "Sit: " + sitId, (short)ExceptionLevelEnum.Error));
            }
            //try
            //{
            //    var var1 = new StringBuilder();
            //    var1.Append("SELECT DeviceId FROM AvantiIPAddresses \n");
            //    var1.Append("WHERE SitId = @SitId");

            //    using (SqlConnection con = new SqlConnection(_connectionString))
            //    {
            //        con.Open();
            //        using (SqlCommand cmd = new SqlCommand(var1.ToString(), con))
            //        {
            //            cmd.Parameters.AddWithValue("@SitId", sitId);
            //            SqlDataReader reader = cmd.ExecuteReader();
            //            reader.Read();
            //            if (reader.HasRows)
            //            {
            //                ipDevice = reader.GetString(0);
            //            }
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{
            //    _logController.LogApplicationException(new CmtApplicationException(ex,
            //                                                                       "AvantiNBIGateway: Error while getting IP device ID",
            //                                                                       "Sit ID: " + sitId.ToString(), (short)ExceptionLevelEnum.Error));
            //    ipDevice = "0";
            //}

            return ipDevice;
        }

        /// <summary>
        /// This method locks/unlocks the terminal in the Avanti hub
        /// </summary>
        /// <param name="sitId">The sit ID of the terminal</param>
        /// <param name="transition">"suspensionblock" locks the terminal, "operational" unlocks the terminal</param>
        /// <returns>True if successful</returns>
        public bool TerminalStateTransition(string sitId, string transition)
        {
            //get the ETag
            string eTag = "";
            RestRequest get = new RestRequest();
            get.Resource = "services/rest/default/Sit/" + sitId;
            IRestResponse response = _client.Execute(get);
            foreach (var v in response.Headers)
            {
                if (v.Name == "ETag")
                {
                    eTag = v.Value.ToString();
                }
            }

            RestRequest delIp = new RestRequest();
            delIp.AddHeader("If-Match", eTag);

            RestRequest put = new RestRequest();
            put.Resource = "services/rest/default/Sit/" + sitId + "/state_transition";
            put.Method = Method.PUT;
            put.AddHeader("If-Match", eTag);
            put.AddParameter("state", transition);
            put.AddParameter("reason", "locking");

            response = _client.Execute(put);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Gets the hardware model for a specifc sit in the Avanti hub
        /// </summary>
        /// <param name="sitId">The ID of the sit</param>
        /// <returns>A string with the hardware model</returns>
        public string GetHardwareModel(string sitId)
        {
            string hw = "MDM2200";
            RestRequest get = new RestRequest();
            get.Resource = "services/rest/default/Sit/" + sitId + "/supported_hardware_model";
            IRestResponse response = _client.Execute(get);
            try
            {
                JavaScriptSerializer js = new JavaScriptSerializer();
                var dict = js.Deserialize<Dictionary<string, dynamic>>(response.Content);
                hw = dict["options"]["hwModel"]["id"];
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "AvantiNBIGateway: Error while getting hardware model from the hub",
                                                                                   "Sit: " + sitId, (short)ExceptionLevelEnum.Error));
            }
            return hw;
        }

        /// <summary>
        /// First gets the current Avanti sit number from the CMT database
        /// then updates the sit number to the new value
        /// </summary>
        /// <returns>The current sit number incremented by 1</returns>
        public int GetNextAvantiSitNumber()
        {
            int sitNum = Convert.ToInt32(_configurationController.getParameter("AvantiSIT")) + 1;
            _configurationController.setParameter("AvantiSIT", sitNum.ToString());
            return sitNum;
        }

        #endregion 
    }
}
