﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Net;
using System.Configuration;
using BOControllerLibrary.Model;
using BOControllerLibrary.Model.Log;
using BOControllerLibrary.Controllers;
using BOControllerLibrary.Controllers.Interfaces;
using BOControllerLibrary.ISPIF.Model;
using BOControllerLibrary.ISPIF.Gateway.Interface;

//Astra2connect webservice
using BOControllerLibrary.com.astra2connect.ispif;

namespace BOControllerLibrary.ISPIF.Gateway
{
    /// <summary>
    /// Terminal management by means of the astra2connect (ISPIF)
    /// framework
    /// </summary>
    public class ISPIFGateway : SatADSLHubController, IHubGateWay
    {
        private IspSupportInterfaceService _ispService = null;
        private IBOLogController _controller;
        private const short MAX_RETRY = 5; //Retry ISPIF communication

        public ISPIFGateway()
        {
            //The ISPIF support service
            _ispService = new IspSupportInterfaceService();
            _controller = new BOLogController(
                connectionString: ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString,
                databaseProvider: ConfigurationManager.ConnectionStrings["ApplicationServices"].ProviderName);
        }

        #region IHubGateWay Members

        public bool TerminalActivate(string endUserId, string macAddress, long slaId, int ispId, Guid userId)
        {
            bool result = false;
            int retryCounter = 0;

            while (retryCounter < MAX_RETRY && !result)
            {
                try
                {
                    //Set authentication credentials and WS settings
                    _ispService.Credentials = this.readCredentials(ispId);
                    _ispService.PreAuthenticate = true;
                    ServicePointManager.Expect100Continue = false;
                    RequestTicket rt = _ispService.createRegistration(endUserId, macAddress, slaId.ToString(), ispId.ToString());
                    result = this.insertRequestTicket(rt, macAddress, AdmStatus.LOCKED, AdmStatus.OPERATIONAL, TerminalActivityTypes.Activation, ispId, userId);
                }
                catch (Exception ex)
                {
                    retryCounter++;
                    this.logException(ex, "ISPIFGateWay.TerminalActivate", ExceptionLevelEnum.Critical);
                }
            }
            return result;
        }

        public bool TerminalChangeSla(string endUserId, string macAddress, int newSlaId, BOControllerLibrary.ISPIF.Model.AdmStatus currentState, int ispId, Guid userId)
        {
            bool result = false;
            int retryCounter = 0;

            while (retryCounter < MAX_RETRY && !result)
            {
                try
                {
                    //Set authentication credentials and WS settings
                    _ispService.Credentials = this.readCredentials(macAddress);
                    _ispService.PreAuthenticate = true;
                    ServicePointManager.Expect100Continue = false;
                    RequestTicket rt = _ispService.changeSla(endUserId, newSlaId.ToString(), ispId.ToString());
                    result = this.insertRequestTicketWithSla(rt, macAddress, currentState, currentState, TerminalActivityTypes.ChangeEdgeSla, ispId, newSlaId, userId);
                }
                catch (Exception ex)
                {
                    retryCounter++;
                    this.logException(ex, "ISPIFEmulatorGateWay.TerminalChangeSla", ExceptionLevelEnum.Critical);
                }
            }
            return result;
        }

        public bool TerminalDecommission(string endUserId, string macAddress, int ispId, Guid userId)
        {
            bool result = false;
            int retryCounter = 0;

            while (retryCounter < MAX_RETRY && !result)
            {
                try
                {
                    //Set authentication credentials and WS settings
                    _ispService.Credentials = this.readCredentials(macAddress);
                    _ispService.PreAuthenticate = true;
                    ServicePointManager.Expect100Continue = false;
                    RequestTicket rt = _ispService.removeRegistration(endUserId, macAddress);
                    result = this.insertRequestTicket(rt, macAddress, AdmStatus.OPERATIONAL, AdmStatus.DECOMMISSIONED, TerminalActivityTypes.Decommissioning, ispId, userId);
                }
                catch (Exception ex)
                {
                    retryCounter++;
                    this.logException(ex, "ISPIFGateWay.TerminalDecommission", ExceptionLevelEnum.Critical);
                }
            }
            return result;
        }

        public bool TerminalReActivate(string endUserId, string macAddress, int ispId, Guid userId)
        {
            bool result = false;
            int retryCounter = 0;

            while (retryCounter < MAX_RETRY && !result)
            {
                try
                {
                    //Set authentication credentials and WS settings
                    _ispService.Credentials = this.readCredentials(macAddress);
                    _ispService.PreAuthenticate = true;
                    ServicePointManager.Expect100Continue = false;
                    RequestTicket rt = _ispService.changeStatus(endUserId, RegistrationStatus.OPERATIONAL, ispId.ToString());
                    result = this.insertRequestTicket(rt, macAddress, AdmStatus.LOCKED, AdmStatus.OPERATIONAL, TerminalActivityTypes.ReActivation, ispId, userId);
                }
                catch (Exception ex)
                {
                    retryCounter++;
                    this.logException(ex, "ISPIFGateWay.TerminalReActivate", ExceptionLevelEnum.Critical);
                }
            }
            return result;
        }

        public bool TerminalSuspend(string endUserId, string macAddress, int ispId, Guid userId)
        {
            bool result = false;

            int retryCounter = 0;

            while (retryCounter < MAX_RETRY && !result)
            {
                try
                {
                    //Set authentication credentials and WS settings
                    _ispService.Credentials = this.readCredentials(macAddress);
                    _ispService.PreAuthenticate = true;
                    ServicePointManager.Expect100Continue = false;
                    RequestTicket rt = _ispService.changeStatus(endUserId, RegistrationStatus.LOCKED, ispId.ToString());
                    result = this.insertRequestTicket(rt, macAddress, AdmStatus.OPERATIONAL, AdmStatus.LOCKED, TerminalActivityTypes.Suspension, ispId, userId);
                }
                catch (Exception ex)
                {
                    retryCounter++;
                    this.logException(ex, "ISPIFGateWay.TerminalSuspend", ExceptionLevelEnum.Critical);
                }
            }
            return result;
        }

        public bool TerminalUserReset(string endUserId, int ispId)
        {
            bool result = false;
            int retryCounter = 0;

            while (retryCounter < MAX_RETRY && !result)
            {
                try
                {
                    //Set authentication credentials and WS settings
                    _ispService.Credentials = this.readCredentials(ispId);
                    _ispService.PreAuthenticate = true;
                    ServicePointManager.Expect100Continue = false;
                    _ispService.resetUser(endUserId, ispId.ToString());
                    result = true;
                }
                catch (Exception ex)
                {
                    retryCounter++;
                    this.logException(ex, "ISPIFGateWay.TerminalUserReset", ExceptionLevelEnum.Critical);
                }
            }
            return result;
        }

        /// <summary>
        /// Returns a CmtRequestTicket corresponding to the Ticket status
        /// </summary>
        /// <param name="requestTicketId"></param>
        /// <param name="ispId"></param>
        /// <returns></returns>
        public CmtRequestTicket LookupRequestTicket(string requestTicketId, int ispId)
        {
            CmtRequestTicket cmtRt = null;
            int retryCounter = 0;
            bool result = false;

            while (retryCounter < MAX_RETRY && !result)
            {
                try
                {
                    //Set authentication and WS settings
                    _ispService.Credentials = this.readCredentials(ispId);
                    _ispService.PreAuthenticate = true;
                    ServicePointManager.Expect100Continue = false;
                    RequestTicket rt = _ispService.lookupRequestTicket(requestTicketId);

                    cmtRt = new CmtRequestTicket();

                    cmtRt.id = rt.id;
                    cmtRt.failureReason = rt.failureReason;
                    cmtRt.failureStackTrace = rt.failureStackTrace;
                    if (rt.requestStatus == RequestStatus.BUSY)
                    {
                        cmtRt.requestStatus = CmtRequestStatus.BUSY;
                    }

                    if (rt.requestStatus == RequestStatus.FAILED)
                    {
                        cmtRt.requestStatus = CmtRequestStatus.FAILED;
                    }

                    if (rt.requestStatus == RequestStatus.SUCCESSFUL)
                    {
                        cmtRt.requestStatus = CmtRequestStatus.SUCCESSFUL;
                    }

                    result = true;
                }
                catch (Exception ex)
                {
                    retryCounter++;
                    this.logException(ex, "ISPIFGateWay.LookupRequest", ExceptionLevelEnum.Critical);
                }
            }

            return cmtRt;
        }


        /// <summary>
        /// Add volume to a subscription.
        /// </summary>
        /// <remarks>
        /// The subscription must have a voucher based SLA
        /// </remarks>
        /// <param name="sitId"></param>
        /// <param name="volumeCode"></param>
        /// <param name="ispId"></param>
        public bool AddVolume(int sitId, int volumeCode, int ispId)
        {
            ISPIFCommon ispifCommon = new ISPIFCommon();
            bool addVolumeStatus = ispifCommon.AddVolume(sitId, volumeCode, ispId);
            if (!addVolumeStatus)
            {
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDesc = "Add volume failed for SIT Id: " + sitId + " and ISP: " + ispId;
                cmtEx.ExceptionDateTime = DateTime.UtcNow;
                cmtEx.StateInformation = "Error";
                cmtEx.ExceptionLevel = (short)ExceptionLevelEnum.Error;
                cmtEx.UserDescription = "The the AddeVolume method failed";
                _controller.LogApplicationException(cmtEx);
            }
            return addVolumeStatus;
        }

        /// <summary>
        /// Changes the MacAddress of a terminal
        /// </summary>
        /// <param name="endUserId"></param>
        /// <param name="newMacAddress"></param>
        /// <param name="ispId"></param>
        /// <returns></returns>
        public bool ChangeMacAddress(string endUserId, string oriMacAddress, string newMacAddress, int ispId, Guid userId)
        {
            bool result = false;
            int retryCounter = 0;

            while (retryCounter < MAX_RETRY && !result)
            {
                try
                {
                    //Set authentication credentials and WS settings
                    _ispService.Credentials = this.readCredentials(ispId);
                    _ispService.PreAuthenticate = true;
                    ServicePointManager.Expect100Continue = false;
                    RequestTicket rt = _ispService.changeTerminal(endUserId, newMacAddress, ispId.ToString());
                    result = this.insertRequestTicketWithMac(rt, oriMacAddress, AdmStatus.OPERATIONAL, AdmStatus.OPERATIONAL, TerminalActivityTypes.ChangeMac, ispId, newMacAddress, userId);
                }
                catch (Exception ex)
                {
                    retryCounter++;
                    this.logException(ex, "ChangeMacAddress", ExceptionLevelEnum.Critical);
                    result = false;
                }
            }

            if (!result)
            {
                CmtApplicationException cmtAppEx = new CmtApplicationException();
                cmtAppEx.ExceptionDateTime = DateTime.Now;
                cmtAppEx.ExceptionDesc = "Could not change MAC address for terminal: " + endUserId;
                cmtAppEx.ExceptionStacktrace = "Method: ChangeMacAddress";
                cmtAppEx.UserDescription = "Check this MacAddress value on HUB";
                cmtAppEx.ExceptionLevel = (short)ExceptionLevelEnum.Critical;
                _controller.LogApplicationException(cmtAppEx);
            }

            return result;
        }

        /// <summary>
        /// Changes the ISP of a terminal
        /// </summary>
        /// <param name="macAddress">The terminals MAC address</param>
        /// <param name="oldISP">The old ISP</param>
        /// <param name="newISP">The new iSP</param>
        /// <returns>True if the operation succeeded</returns>
        public bool ChangeISP(string endUserId, int oldISP, int newISP)
        {
            ISPIFCommon ispifCommon = new ISPIFCommon();
            return ispifCommon.ChangeISP(endUserId, oldISP, newISP);
        }
    


        #endregion

        #region Helper methods
        /// <summary>
        /// Inserts a new ticket in the CMT database
        /// </summary>
        /// <param name="rt"></param>
        /// <param name="macAddress"></param>
        /// <param name="sourceAdmStatus"></param>
        /// <param name="targetAdmStatus"></param>
        /// <param name="termActivityType"></param>
        /// <returns></returns>
        private bool insertRequestTicket(RequestTicket rt, string macAddress, AdmStatus sourceAdmStatus, AdmStatus targetAdmStatus,
                                           TerminalActivityTypes termActivityType, int ispId, Guid userId)
        {
            TicketDataMgr ticketDataMgr = new TicketDataMgr();
            // RequestStatus reqStatus =
            //        (RequestStatus)Enum.ToObject(typeof(RequestStatus), (int)rt.requestStatus);
            
            return ticketDataMgr.insertTicket(rt.id, macAddress, sourceAdmStatus, targetAdmStatus,
                                            (int)rt.requestStatus, rt.failureReason, rt.failureStackTrace, termActivityType, ispId, 0, userId);
        }

        /// <summary>
        /// Inserts a new ticket in the CMT Database with a specified SLA
        /// </summary>
        /// <param name="rt">The request ticket</param>
        /// <param name="macAddress">The Mac Address of the terminal </param>
        /// <param name="sourceAdmStatus">The source administration status</param>
        /// <param name="targetAdmStatus">The target administration status</param>
        /// <param name="termActivityType">The terminal activity type</param>
        /// <param name="ispId">The ISP identifier</param>
        /// <param name="newSla">The identifier of the new SLA</param>
        /// <returns>True if the insert succeeded</returns>
        private bool insertRequestTicketWithSla(RequestTicket rt, string macAddress, AdmStatus sourceAdmStatus, AdmStatus targetAdmStatus,
                                           TerminalActivityTypes termActivityType, int ispId, int newSla, Guid userId)
        {
            TicketDataMgr ticketDataMgr = new TicketDataMgr();
            //RequestStatus reqStatus =
            //        (RequestStatus)Enum.ToObject(typeof(RequestStatus), (int)rt.requestStatus);
            
            return ticketDataMgr.insertTicket(rt.id, macAddress, sourceAdmStatus, targetAdmStatus,
                                            (int)rt.requestStatus, rt.failureReason, rt.failureStackTrace, termActivityType, ispId, 0, newSla, userId);
        }

        /// <summary>
        /// Simple exception logger
        /// </summary>
        /// <remarks>
        /// For this version the exception level is simply set to INFO
        /// </remarks>
        /// <param name="ex">The exception to log</param>
        /// <param name="method">The origin method of the exception</param>
        private void logException(Exception ex, string method)
        {
            CmtApplicationException cmtApplicationException = new CmtApplicationException();
            cmtApplicationException.ExceptionDateTime = DateTime.Now;
            cmtApplicationException.ExceptionDesc = ex.Message;
            cmtApplicationException.ExceptionStacktrace = ex.StackTrace;
            cmtApplicationException.UserDescription = method;
            _controller.LogApplicationException(cmtApplicationException);
        }

        /// <summary>
        /// Method version which allows to set an exception level
        /// </summary>
        /// <param name="ex">The exception to log</param>
        /// <param name="method">The origin method of the exception</param>
        /// <param name="exLevel">The exception level</param>
        private void logException(Exception ex, string method, ExceptionLevelEnum exLevel)
        {
            CmtApplicationException cmtApplicationException = new CmtApplicationException();
            cmtApplicationException.ExceptionDateTime = DateTime.Now;
            cmtApplicationException.ExceptionDesc = ex.Message;
            cmtApplicationException.ExceptionStacktrace = ex.StackTrace;
            cmtApplicationException.UserDescription = method;
            cmtApplicationException.ExceptionLevel = (short)exLevel;
            _controller.LogApplicationException(cmtApplicationException);
        }

        /// <summary>
        /// This method returns the ISPIF credentials for the ISP associated to the 
        /// given MAC address
        /// </summary>
        /// <param name="macAddress">The target MAC Address</param>
        /// <returns>A NetworkCredential instance or null in case of an error</returns>
        private NetworkCredential readCredentials(string macAddress)
        {
            //Get the credentials from the database
            string connectionString =
               ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
            string databaseProviderString =
               ConfigurationManager.ConnectionStrings["ApplicationServices"].ProviderName;

            BOControllerLibrary.Controllers.BOAccountingController boAccountingController = 
                            new BOControllerLibrary.Controllers.BOAccountingController(connectionString, databaseProviderString);

            //First get the terminal info object from the database
            BOControllerLibrary.Model.Terminal.Terminal term
                                    = boAccountingController.GetTerminalDetailsByMAC(macAddress);
            BOControllerLibrary.Model.ISP.Isp isp = boAccountingController.GetISP(term.IspId);
            NetworkCredential cred = new NetworkCredential(isp.Ispif_User, isp.Ispif_Password);
            return cred;
        }

        /// <summary>
        /// This method returns the ISPIF credentials for the given ISP
        /// </summary>
        /// <param name="macAddress">The target MAC Address</param>
        /// <returns>A NetworkCredential instance or null in case of an error</returns>
        private NetworkCredential readCredentials(int IspId)
        {
            //Get the credentials from the database
            string connectionString =
               ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
            string databaseProviderString =
               ConfigurationManager.ConnectionStrings["ApplicationServices"].ProviderName;

            BOControllerLibrary.Controllers.BOAccountingController boAccountingController =
                            new BOControllerLibrary.Controllers.BOAccountingController(connectionString, databaseProviderString);

            BOControllerLibrary.Model.ISP.Isp isp = boAccountingController.GetISP(IspId);
            NetworkCredential cred = new NetworkCredential(isp.Ispif_User, isp.Ispif_Password);
            return cred;
        }

        /// <summary>
        /// Inserts a ticket for a MAC Address change
        /// </summary>
        /// <param name="rt"></param>
        /// <param name="macAddress"></param>
        /// <param name="sourceAdmStatus"></param>
        /// <param name="targetAdmStatus"></param>
        /// <param name="termActivityType"></param>
        /// <param name="ispId"></param>
        /// <param name="NewMacAddress"></param>
        /// <returns></returns>
        private bool insertRequestTicketWithMac(RequestTicket rt, string macAddress, AdmStatus sourceAdmStatus, AdmStatus targetAdmStatus,
                                           TerminalActivityTypes termActivityType, int ispId, string NewMacAddress, Guid userId)
        {
            TicketDataMgr ticketDataMgr = new TicketDataMgr();
            //RequestStatus reqStatus =
            //        (RequestStatus)Enum.ToObject(typeof(RequestStatus), (int)rt.requestStatus);
            return ticketDataMgr.insertTicket(rt.id, macAddress, sourceAdmStatus, targetAdmStatus,
                                            (int)rt.requestStatus, rt.failureReason, rt.failureStackTrace, termActivityType, ispId, 0, NewMacAddress, userId);
        }
        
        #endregion

        #region NMS Support methods

        /**
         * For this version of the HubGateway these methods are not used
         */
        /// <summary>
        /// Creates a new multicast group
        /// </summary>
        /// <param name="macAddress">The multicast MAC address, derived from the IP address</param>
        /// <param name="ipAddress">The multicast group IP address</param>
        /// <param name="multiCastName">The multicast group name</param>
        /// <param name="sourceUrl">The source URL</param>
        /// <param name="portNr">The port number</param>
        /// <param name="bandWidth">The bandwidth</param>
        /// <param name="ispId">The ISP this multicast group belongs to</param>
        /// <returns>True if the operation succeeded</returns>
        public bool CreateMulticastGroup(string macAddress, string ipAddress, string multiCastName, string sourceUrl, int portNr, long bandWidth, int ispId, Guid userId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Removes an IP address from a Multicast group
        /// </summary>
        /// <param name="ipAddress">The IP Address of the Multicast group to remove</param>
        /// <returns>True if the operation succeeded</returns>
        public bool RemoveIPFromMulticast(string ipAddress)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Enables a MulticastGroup
        /// </summary>
        /// <param name="ipAddress">IP Address of the Multicast group to enable</param>
        /// <returns>true if the command was accepted by the NMS</returns>
        public bool EnableMultiCastGroup(string ipAddress, string macAddress, int ispId, Guid userId)
        {
            throw new NotImplementedException();
        }

        
        /// <summary>
        /// Disables a Multicast group
        /// </summary>
        /// <param name="ipAddress">IP Address of the Multicast group to disable</param>
        /// <returns>true if the command was accepted by the NMS</returns>
        public bool DisableMultiCastGroup(string ipAddress, string macAddress, int ispId, Guid userId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Returns the transferred bytes for the Multicast group specified by the IP Address
        /// </summary>
        /// <param name="ipAddress">The IP Address</param>
        /// <returns>The transferred bytes as a long</returns>
        public ulong GetTransferredBytes(string ipAddress)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Returns true if the Multicast group identified by the IP Address is enabled
        /// </summary>
        /// <param name="ipAddress">The IP Address which identifies the MC group</param>
        /// <returns>True if the MC is enabled</returns>
        public bool IsEnabled(string ipAddress)
        {
            throw new NotImplementedException();
        }

        public bool NMSTerminalActivateWithRange(string endUserId, string macAddress, long slaId, int ispId, int ipMask, int freeZone, Guid userId)
        {
            throw new NotImplementedException();
        }

        public bool NMSTerminalActivate(string endUserId, string macAddress, long slaId, int ispId, int freeZone, Guid userId)
        {
            throw new NotImplementedException();
        }

        public bool NMSTerminalChangeSla(string endUserId, string macAddress, int newSlaId, AdmStatus currentState, int ispId, Guid userId)
        {
            throw new NotImplementedException();
        }

        public bool NMSTerminalDecommission(string endUserId, string macAddress, int ispId, Guid userId)
        {
            throw new NotImplementedException();
        }

        public bool NMSTerminalReActivate(string endUserId, string macAddress, int ispId, Guid userId)
        {
            throw new NotImplementedException();
        }

        public bool NMSTerminalSuspend(string endUserId, string macAddress, int ispId, Guid userId)
        {
            throw new NotImplementedException();
        }

        public bool NMSTerminalUserReset(string endUserId, int ispId)
        {
            throw new NotImplementedException();
        }

        public CmtRequestTicket NMSLookupRequestTicket(string requestTicketId, int ispId)
        {
            throw new NotImplementedException();
        }

        public bool NMSAddVolume(int sitId, int volumeCode, int ispId)
        {
            throw new NotImplementedException();
        }

        public bool NMSChangeMacAddress(string endUserId, string oriMacAddress, string newMacAddress, int ispId, Guid userId)
        {
            throw new NotImplementedException();
        }

        public bool NMSChangeISP(string endUserId, int oldISP, int newISP)
        {
            throw new NotImplementedException();
        }

        public bool NMSCreateServicePack(BOControllerLibrary.NMSGatewayServiceRef.ServicePack servicePack)
        {
            return true;
        }

        public bool NMSUpdateServicePack(BOControllerLibrary.NMSGatewayServiceRef.ServicePack servicePack)
        {
            return true;
        }

        public bool NMSDeleteServicePack(int slaId)
        {
            return true;
        }

        /// <summary>
        /// Checks if the SLA (which eventually needs to be deleted) has associated terminals
        /// </summary>
        /// <param name="slaId">The identifier of the SLA</param>
        /// <returns>True when the SLA has still associated terminals</returns>
        public bool NMSSlaHasAssociatedTerminals(int slaId)
        {
            return false;
        }

        public bool NMScreateFreeZone(NMSGatewayServiceRef.FreeZone freeZone)
        {
            throw new NotImplementedException();
        }

        public bool NMSupdateFreeZone(NMSGatewayServiceRef.FreeZone freeZone)
        {
            throw new NotImplementedException();
        }

        public List<NMSGatewayServiceRef.FreeZone> NMSgetFreeZones()
        {
            throw new NotImplementedException();
        }

        public NMSGatewayServiceRef.FreeZone NMSgetFreeZone(int freeZoneId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method allows to change the IP Address AND Mask in the SatADSL NMS
        /// </summary>
        /// <remarks>
        /// The old and new IP Masks may be identical while the old and new IP Addresses
        /// can never be identical.
        /// </remarks>
        /// <param name="endUserId">The unique end user Id</param>
        /// <param name="macAddress">The terminal MAC address</param>
        /// <param name="oldIPAddress">The original IP Address</param>
        /// <param name="oldIPMask">The original IP Mask</param>
        /// <param name="newIPAddress">The new IP Address</param>
        /// <param name="newIPMask">The new IP Mask</param>
        /// <param name="ispId">Th ISP Identifier</param>
        /// <returns>True if the change was registered by NMS successfully</returns>
        public bool NMSChangeIPAddress(string endUserId, string macAddress, string oldIPAddress, int oldIPMask,
                                                    string newIPAddress, int newIPMask, int ispId, Guid userId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method allows to update the freezone information for a terminal
        /// </summary>
        /// <param name="FreeZoneId">The identifier of the freezone allocated to the terminal</param>
        /// <param name="macAddress">The MAC address of the terminal</param>
        /// <param name="FreeZoneFlag">If this flag is set to true, a freezone option is allocated to the terminal. In
        /// case this value is false, the terminal does not have the freezone option</param>
        /// <returns></returns>
        public bool NMSModifyFreeZoneForTerminal(string endUserId, int FreeZoneId)
        {
            throw new NotImplementedException();
        }

        public bool NMSTerminalChangeWeight(string endUserId, string macAddress, int newWeight, int customerId, AdmStatus currentState, int ispId, Guid userId)
        {
            return true;
        }

        #endregion




        public virtual TerminalInfo getTerminalDetailsFromHubMacChange(string oldMacAddress, string newMacAddress, string ispId)
        {
            throw new NotImplementedException();
        }


        public bool NMSActivateSatelliteDiversityTerminal(string endUserId, string macAddress, string ipAddress, long slaId, int ispId, int freeZone, string primaryMac, string secondaryMac, Guid userId)
        {
            throw new NotImplementedException();
        }
    }
}