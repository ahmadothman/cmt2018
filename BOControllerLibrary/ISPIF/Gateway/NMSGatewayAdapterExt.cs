﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Configuration;
using System.Data.SqlClient;

using BOControllerLibrary.ISPIF.Model;
using BOControllerLibrary.Model.Terminal;
using BOControllerLibrary.Model.Log;
using BOControllerLibrary.Model;
using CMTTerminal = BOControllerLibrary.Model.Terminal;


//Astra2connect webservice
using ispif = BOControllerLibrary.com.astra2connect.ispif;
using nms = BOControllerLibrary.NMSGatewayServiceRef;
using BOControllerLibrary.Model.Organization;

namespace BOControllerLibrary.ISPIF.Gateway
{
    /// <summary>
    /// This is the generic SatADSL NMSGateway adapter able to work with the 
    /// EdgeISP and EdgeSLA extensions provided by the ServicePack (ServiceLevel)
    /// provided by the calling application.
    /// </summary>
    public class NMSGatewayAdapterExt : NMSGateway
    {
        /// <summary>
        /// Default constructor. The Default SLA and Default ISP values are kept empty
        /// </summary>
        public NMSGatewayAdapterExt()
            : base("", "")
        {
        }

        #region Method overrides

        /// <summary>
        /// Creates a terminal in the SES Hub by means of the ISPIF gateway. 
        /// This method sets the NMSActivate flag which indicates that the 
        /// TicketMaster still needs to create the terminal in the NMS.
        /// </summary>
        /// <param name="endUserId">The end user identifier related to the terminal</param>
        /// <param name="macAddress">The terminals' MAC address</param>
        /// <param name="slaId">
        /// The Service Level identifier. For this version of the IHubGateway interface
        /// the slaId is retrieved from the configuration table since the sla is NOT the sla for the 
        /// activation in the NMS!
        /// </param>
        /// <param name="ispId">The ISP id</param>
        /// <returns>true if the operation succeeds</returns>
        public override Boolean TerminalActivate(string endUserId, string macAddress, long slaId, int ispId, Guid userId)
        {
            bool result = false;
            int retryCounter = 0;

            ServiceLevel sl = _accountingController.GetServicePack((int)slaId);


            //set slaid to value from the service level object (Edge SLA = Hub SLA)
            long HubSlaId = sl.EdgeSLA;

            //We are not using the ISPID passed by the method to activate the terminal on the
            //SES Hub but instead use Edge ISP stored by the ServiceLevel object
            int sesISPId = sl.EdgeISP;


            while (retryCounter < MAX_RETRY && !result)
            {
                try
                {
                    //Set authentication credentials and WS settings                    
                    _ispService.Credentials = this.readCredentials(sesISPId);
                    _ispService.PreAuthenticate = true;
                    ServicePointManager.Expect100Continue = false;

                    CMTTerminal.Terminal term = _accountingController.GetTerminalDetailsByMAC(macAddress);
                    ispif.RequestTicket rt = _ispService.createRegistration(endUserId, macAddress, HubSlaId.ToString(), sesISPId.ToString());
                    result = this.insertRequestTicketWithSla(rt, macAddress, AdmStatus.LOCKED, AdmStatus.OPERATIONAL, TerminalActivityTypes.Activation, ispId, (int)slaId, userId);
                }
                catch (Exception ex)
                {
                    retryCounter++;
                    this.logException(ex, "NMSGateWay.TerminalActivate");
                }
            }
            return result;
        }

        /// <summary>
        /// Changes the SLA of the terminal in the SES hub. To change the SLA in the NMS call the
        /// NMSTerminalChangeSLA method.
        /// </summary>
        /// <param name="endUserId">The endUserId of the terminal to activate</param>
        /// <param name="macAddress">The MAC address of the terminal to activate</param>
        /// <param name="newSlaId">The service pack allocated to the terminal</param>
        /// <param name="currentState">The current admin state of the terminal</param>
        /// <returns>true if the application succeeded</returns>
        public override bool TerminalChangeSla(string endUserId, string macAddress, int newSlaId, Model.AdmStatus currentState, int ispId, Guid userId)
        {
            bool result = false;
            int retryCounter = 0;
            int EdgeISP = 0;
            int EdgeSla = 0;

            //Since this is a secondary isp the Edge SLA must be used to change the SLA on the SES HUB
            ServiceLevel sl = _accountingController.GetServicePack(newSlaId);

            if (sl != null)
            {
                while (retryCounter < MAX_RETRY && !result)
                {
                    try
                    {
                        //Set authentication credentials and WS settings
                        _ispService.Credentials = this.readCredentials(macAddress);
                        _ispService.PreAuthenticate = true;
                        ServicePointManager.Expect100Continue = false;
                        ispif.RequestTicket rt = _ispService.changeSla(endUserId, sl.EdgeSLA.ToString(), sl.EdgeISP.ToString());
                        result = this.insertRequestTicketWithSla(rt, macAddress, currentState, currentState, TerminalActivityTypes.ChangeSla, ispId, newSlaId, userId);
                    }
                    catch (Exception ex)
                    {
                        retryCounter++;
                        this.logException(ex, "NMSGatewayAdapterExt.TerminalChangeSla");
                    }
                }
            }
            else
            {
                this.logException("Secondary SLA: " + newSlaId + " does not exist", "NMSGatewayAdapterEx.TerminalSla");
            }
            return result;
        }

        /// <summary>
        /// Decommissions a terminal in the SES hub. 
        /// The terminal is effectively removed from the Hub database
        /// </summary>
        /// <param name="endUserId">The endUserId of the terminal to activate</param>
        /// <param name="macAddress">The MAC address of the terminal to activate</param>
        /// <returns>true if the activation succeeded</returns>
        public override bool TerminalDecommission(string endUserId, string macAddress, int ispId, Guid userId)
        {
            bool result = false;
            int retryCounter = 0;

            //Get the Edge ISP for this terminal
            int sesISPId = this.GetEdgeISP(macAddress);

            while (retryCounter < MAX_RETRY && !result)
            {
                try
                {
                    //Set authentication credentials and WS settings                    
                    _ispService.Credentials = this.readCredentials(sesISPId);
                    _ispService.PreAuthenticate = true;
                    ServicePointManager.Expect100Continue = false;
                    ispif.RequestTicket rt = _ispService.removeRegistration(endUserId, macAddress);
                    result = this.insertRequestTicket(rt, macAddress, AdmStatus.OPERATIONAL, AdmStatus.DECOMMISSIONED, TerminalActivityTypes.Decommissioning, ispId, userId);
                }
                catch (Exception ex)
                {
                    retryCounter++;
                    this.logException(ex, "NMSGatewayService.TerminalDecommission");
                }
            }
            return result;
        }

        /// <summary>
        /// Reactivates a terminal in the SES hub
        /// </summary>
        /// <param name="endUserId"></param>
        /// <param name="macAddress"></param>
        /// <param name="ispId"></param>
        /// <returns></returns>
        public override bool TerminalReActivate(string endUserId, string macAddress, int ispId, Guid userId)
        {
            bool result = false;
            int retryCounter = 0;

            //Get the Edge ISP for this terminal
            int sesISPId = this.GetEdgeISP(macAddress);

            while (retryCounter < MAX_RETRY && !result)
            {
                try
                {
                    //Set authentication credentials and WS settings                    
                    _ispService.Credentials = this.readCredentials(sesISPId);
                    _ispService.PreAuthenticate = true;
                    ServicePointManager.Expect100Continue = false;

                    ispif.RequestTicket rt = _ispService.changeStatus(endUserId, ispif.RegistrationStatus.OPERATIONAL, sesISPId.ToString());
                    result = this.insertRequestTicket(rt, macAddress, AdmStatus.LOCKED, AdmStatus.OPERATIONAL, TerminalActivityTypes.ReActivation, ispId, userId);
                }
                catch (Exception ex)
                {
                    retryCounter++;
                    this.logException(ex, "NMSGateWay.TerminalReActivate");
                }
            }
            return result;
        }

        /// <summary>
        /// Suspends a terminal, the status of the terminal becomes LOCKED
        /// </summary>
        /// <param name="endUserId">The endUserId of the terminal to activate</param>
        /// <param name="macAddress">The MAC address of the terminal to activate</param>
        /// <returns>true if the re-activation succeeded</returns>
        public override bool TerminalSuspend(string endUserId, string macAddress, int ispId, Guid userId)
        {
            bool result = false;
            int retryCount = 0;

            //Get the Edge ISP for this terminal
            int sesISPId = this.GetEdgeISP(macAddress);

            while (retryCount < MAX_RETRY && !result)
            {
                try
                {
                    //Set authentication credentials and WS settings                    
                    _ispService.Credentials = this.readCredentials(sesISPId);
                    _ispService.PreAuthenticate = true;
                    ServicePointManager.Expect100Continue = false;

                    ispif.RequestTicket rt = _ispService.changeStatus(endUserId, ispif.RegistrationStatus.LOCKED, sesISPId.ToString());
                    result = this.insertRequestTicket(rt, macAddress, AdmStatus.OPERATIONAL, AdmStatus.LOCKED, TerminalActivityTypes.Suspension, ispId, userId);
                }
                catch (Exception ex)
                {
                    this.logException(ex, "NMSGateWay.TerminalSuspend");
                }
            }
            return result;
        }

        /// <summary>
        /// Returns the status of a request ticket.
        /// </summary>
        /// <remarks>
        /// This method maps on the ISPIF lookupRequestTicket 
        /// </remarks>
        /// <param name="requestTicketId">The requestticket identfier</param>
        /// <param name="ispId">This is the ISP identfier (not used in this version of the method as we need the edge ISP</param>
        /// <returns>
        /// The corresponding CMT request ticket or null if the ticket could not 
        ///  be found
        ///  </returns>
        public override CmtRequestTicket LookupRequestTicket(string requestTicketId, int ispId)
        {
            CmtRequestTicket cmtRt = null;
            int retryCounter = 0;
            bool result = false;

            while (retryCounter < MAX_RETRY && !result)
            {
                try
                {
                    //Set authentication and WS settings
                    int sesIspId = this.GetEdgeISPFromRequestId(requestTicketId);
                    _ispService.Credentials = this.readCredentials(sesIspId);
                    _ispService.PreAuthenticate = true;
                    ServicePointManager.Expect100Continue = false;
                    ispif.RequestTicket rt = _ispService.lookupRequestTicket(requestTicketId);

                    if (rt != null)
                    {
                        cmtRt = new CmtRequestTicket();
                        cmtRt.id = rt.id;
                        cmtRt.failureReason = rt.failureReason;
                        cmtRt.failureStackTrace = rt.failureStackTrace;
                        cmtRt.NMSFlag = true;

                        if (rt.requestStatus == ispif.RequestStatus.BUSY)
                        {
                            cmtRt.requestStatus = CmtRequestStatus.BUSY;
                        }

                        if (rt.requestStatus == ispif.RequestStatus.FAILED)
                        {
                            cmtRt.requestStatus = CmtRequestStatus.FAILED;
                        }

                        if (rt.requestStatus == ispif.RequestStatus.SUCCESSFUL)
                        {
                            cmtRt.requestStatus = CmtRequestStatus.SUCCESSFUL;
                        }
                    }

                    result = true;
                }
                catch (Exception ex)
                {
                    retryCounter++;
                    this.logException(ex, "ISPIFGateWay.LookupRequest");
                }
            }

            return cmtRt;
        }

       
        /// <summary>
        /// Changes the MacAddress in the SES Hub
        /// </summary>
        /// <param name="endUserId"></param>
        /// <param name="oriMacAddress"></param>
        /// <param name="newMacAddress"></param>
        /// <param name="ispId"></param>
        /// <returns></returns>
        public override bool ChangeMacAddress(string endUserId, string oriMacAddress, string newMacAddress, int ispId, Guid userId)
        {
            bool result = false;
            int sesISPId = this.GetEdgeISP(oriMacAddress);

            try
            {
                int retryCounter = 0;

                while (retryCounter < MAX_RETRY && !result)
                {
                    try
                    {
                        //Set authentication credentials and WS settings
                        _ispService.Credentials = this.readCredentials(sesISPId);
                        _ispService.PreAuthenticate = true;
                        ServicePointManager.Expect100Continue = false;
                        ispif.RequestTicket rt = _ispService.changeTerminal(endUserId, newMacAddress, sesISPId.ToString());
                        result = this.insertRequestTicketWithMac(rt, oriMacAddress, AdmStatus.OPERATIONAL, AdmStatus.OPERATIONAL, TerminalActivityTypes.ChangeMac, ispId, newMacAddress, userId);
                    }
                    catch (Exception ex)
                    {
                        retryCounter++;
                        this.logException(ex, "ChangeMacAddress");
                        result = false;
                    }
                }

            }
            catch (Exception ex)
            {
                this.logException(ex, "ISPIFEmulatorGateWay.ChangeMacAddress failed for: " + endUserId);
            }
            return result;
        }

        /// <summary>
        /// Changes the ISP of a terminal
        /// </summary>
        /// <param name="macAddress">The terminals MAC address</param>
        /// <param name="oldISP">The old ISP</param>
        /// <param name="newISP">The new iSP</param>
        /// <returns>True if the operation succeeded</returns>
        public override bool ChangeISP(string endUserId, int oldISP, int newISP)
        {
            bool result = false;

            try
            {
                //Set authentication credentials and WS settings
                _ispService.Credentials = this.readCredentials(oldISP);
                _ispService.PreAuthenticate = true;
                ServicePointManager.Expect100Continue = false;
                _ispService.reassignRegistration(endUserId, oldISP.ToString(), newISP.ToString());
                result = true;
            }
            catch (Exception ex)
            {
                this.logException(ex, "ChangeISP");
                result = false;
            }

            return result;
        }

        /// <summary>
        /// This method retrieves the terminal information from the SES Hub
        /// </summary>
        /// <param name="macAddress">The terminal MAC address</param>
        /// <param name="ispId">The ISP identifier, not used in this case as we need to use the
        /// SES HUB Isp!</param>
        /// <returns>A completed TerminalInfo object</returns>
        public override TerminalInfo getTerminalDetailsFromHub(string macAddress, string ispId)
        {
            String sesIsp = this.GetEdgeISP(macAddress).ToString();
            return _satADSLHubController.getTerminalDetailsFromHub(macAddress, sesIsp);
        }

        /// <summary>
        /// This method retrieves the terminal information from the SES Hub
        /// This override method is used for MAC changes
        /// </summary>
        /// <param name="oldMacAddress">The old terminal MAC address. This MAC address is still in use in the CMT at this point</param>
        /// <param name="newMacAddress">The new terminal MAC address. This MAC address is already being used in the hub</param>
        /// <param name="ispId">The ISP identifier, not used in this case as we need to use the
        /// SES HUB Isp!</param>
        /// <returns>A completed TerminalInfo object</returns>
        public override TerminalInfo getTerminalDetailsFromHubMacChange(string oldMacAddress, string newMacAddress, string ispId)
        {
            String sesIsp = this.GetEdgeISP(oldMacAddress).ToString();
            return _satADSLHubController.getTerminalDetailsFromHub(newMacAddress, sesIsp);
        }


        /// <summary>
        /// Returns the C/No value. This value is retrieved from the SES hub
        /// </summary>
        /// <param name="ispId">The unique ISP identifier</param>
        /// <param name="sitId">The site id</param>
        /// <returns>The C/No value as a string</returns>
        public override string getRTNValue(string ispId, string sitId)
        {
            Terminal term = _accountingController.GetTerminalDetailsBySitId(Int32.Parse(sitId), Int32.Parse(ispId));
            string sesIspId = this.GetEdgeISP(term.MacAddress).ToString();
            return _satADSLHubController.getRTNValue(sesIspId, sitId);
        }

        /// <summary>
        /// Returns the Es/No value. This value is retrieved from the SES hub
        /// </summary>
        /// <param name="ispId">The unique ISP identifier</param>
        /// <param name="sitId">The site id</param>
        /// <returns>The Es/No value as a string</returns>
        public override string getFWDValue(string ispId, string sitId)
        {
            Terminal term = _accountingController.GetTerminalDetailsBySitId(Int32.Parse(sitId), Int32.Parse(ispId));
            string sesIspId = this.GetEdgeISP(term.MacAddress).ToString();
            return _satADSLHubController.getFWDValue(sesIspId, sitId);
        }

        /// <summary>
        /// Activates a terminal in the NMS.
        /// </summary>
        /// <param name="endUserId">The end user identifier</param>
        /// <param name="macAddress">The terminal MAC address</param>
        /// <param name="slaId">The SLA identifier</param>
        /// <param name="ispId">The ISP idnetifier</param>
        /// <param name="ipMask">IP range specified by means of a MASK</param>
        /// <returns>True if the method succeeds</returns>
        public override bool NMSTerminalActivateWithRange(string endUserId, string macAddress, long slaId, int ispId, int ipMask, int freeZone, Guid userId)
        {
            bool result = false;
            Terminal term = _accountingController.GetTerminalDetailsByMAC(macAddress);

            //First check if the slaId belongs to the isp. If not abort the activate as we are not allowed
            //to send non SatADSL NMS Slas to the server.
            if (this.IsCorrectSla(slaId, ispId))
            {
                try
                {
                    //Need to specify an IP Address as this address is not generated by the NMS but by the SES HUB
                    //Use the SES HUB ISP to get the terminal details
                    string sesIspId = this.GetEdgeISP(macAddress).ToString();
                    TerminalInfo ti = _satADSLHubController.getTerminalDetailsFromHub(macAddress, sesIspId);
                    nms.RequestTicket rt;

                    if (term.OrgId != null)
                    {
                        Organization organization = _accountingController.GetOrganization((int)term.OrgId);
                        if (organization.VNO != null && (bool)organization.VNO)
                        {
                            rt = _nmsService.createRegistrationWithVNO(endUserId, macAddress, ipMask, (int)slaId, ispId, ti.IPAddress, Int32.Parse(ti.SitId), freeZone, organization.FullName);
                        }
                        else
                        {
                            rt = _nmsService.createRegistration(endUserId, macAddress, ipMask, (int)slaId, ispId, ti.IPAddress, Int32.Parse(ti.SitId), freeZone);
                        }
                    }
                    //Need to specify an IP Address as this address is not generated by the NMS but by the Eutelsat HUB
                    else
                    {
                        rt = _nmsService.createRegistration(endUserId, macAddress, ipMask, (int)slaId, ispId, ti.IPAddress, Int32.Parse(ti.SitId), freeZone);
                    }
                    // nms.RequestTicket rt = _nmsService.createRegistration(endUserId, macAddress, ipMask, (int)slaId, ispId, ti.IPAddress, Int32.Parse(ti.SitId), freeZone);
                    result = this.insertRequestTicketWithSla(rt, macAddress, AdmStatus.LOCKED, AdmStatus.OPERATIONAL, TerminalActivityTypes.Activation, ispId, (int)slaId, userId);
                }
                catch (Exception ex)
                {
                    this.logException(ex, "NMSGateWay.TerminalActivate");
                }
            }
            else
            {
                this.logException("Sla: " + slaId + " does not belong to ISP: " + ispId, "NMSGateway.NMSTerminalActivateWithRange");
            }
            return result;
        }
        
        
        #endregion

        #region Helper Methods

        /// <summary>
        /// This method returns the EdgeISP for terminal linked to a 
        /// Secondary ISP.
        /// </summary>
        /// <param name="macAddress">The terminalse MAC Address</param>
        /// <returns>The Edge ISP as an int. The method returns -1 if the EdgeISP could not be found</returns>
        protected int GetEdgeISP(string macAddress)
        {
            int edgeISP = -1;

            try
            {
                Terminal term = _accountingController.GetTerminalDetailsByMAC(macAddress);
                ServiceLevel sl = _accountingController.GetServicePack((int)term.SlaId);
                edgeISP = sl.EdgeISP;
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtExt = new CmtApplicationException();
                cmtExt.ExceptionDateTime = DateTime.Now;
                cmtExt.ExceptionDesc = "No edge ISP defined for terminal: " + macAddress;
                cmtExt.ExceptionLevel = (short)ExceptionLevelEnum.Critical;
                cmtExt.ExceptionStacktrace = "NMSGatewayAdapterExt.GetEdgeISP";
                cmtExt.StateInformation = "Critical error";
                cmtExt.UserDescription = "No Edge ISP defined for terminal, check the SLA configuration";
                _controller.LogApplicationException(cmtExt);
            }

            return edgeISP;
        }

        /// <summary>
        /// This method returns the Edge ISP for a terminal by means of the request ticket Identifier
        /// </summary>
        /// <param name="requestId">The request ticket</param>
        /// <returns>The Edge ISP, the method returns -1 if not found</returns>
        protected int GetEdgeISPFromRequestId(string requestId)
        {
            string queryCmd = "SELECT MacAddress FROM Tickets WHERE TicketId = @TicketId";
            string connectionString = ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
            int ispId = -1;
            string macAddress = "";

            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(queryCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@TicketId", requestId);
                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            macAddress = reader.GetString(0);
                            ispId = this.GetEdgeISP(macAddress);
                        }
                        else
                        {
                            CmtApplicationException cmtExt = new CmtApplicationException();
                            cmtExt.ExceptionDateTime = DateTime.Now;
                            cmtExt.ExceptionDesc = "No SLA defined for ticket: " + requestId;
                            cmtExt.ExceptionLevel = (short)ExceptionLevelEnum.Critical;
                            cmtExt.ExceptionStacktrace = "NMSGatewayAdapterExt.GetEdgeISPFromRequestId";
                            cmtExt.StateInformation = "Critical error";
                            cmtExt.UserDescription = ", check the ticket";
                            _controller.LogApplicationException(cmtExt);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtExt = new CmtApplicationException();
                cmtExt.ExceptionDateTime = DateTime.Now;
                cmtExt.ExceptionDesc = ex.Message;
                cmtExt.ExceptionLevel = (short)ExceptionLevelEnum.Critical;
                cmtExt.ExceptionStacktrace = ex.StackTrace;
                cmtExt.StateInformation = "Critical error";
                cmtExt.UserDescription = "No Edge ISP defined for terminal, check the SLA configuration";
                _controller.LogApplicationException(cmtExt);
            }

            return ispId;
        }
        #endregion

    }
}
