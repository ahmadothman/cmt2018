﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Net;
using BOControllerLibrary.Model;
using BOControllerLibrary.Model.Log;
using BOControllerLibrary.Controllers;
using BOControllerLibrary.Controllers.Interfaces;
using BOControllerLibrary.ISPIF.Model;

//Astra2Connect websrvice
using BOControllerLibrary.com.astra2connect.ispif;

namespace BOControllerLibrary.ISPIF.Gateway
{
    /// <summary>
    /// Contains common methods such as the returning credentials from the
    /// database and access to the AddVolumeBySitId ISPIF methods.
    /// </summary>
    public class ISPIFCommon
    {
        private IspSupportInterfaceService _ispService = null;
        private IBOLogController _logController;
        private const short MAX_RETRY = 5; //Retry ISPIF communication

        public ISPIFCommon()
        {
            //The ISPIF support service
            _ispService = new IspSupportInterfaceService();
            _logController = new BOLogController(
                connectionString: ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString,
                databaseProvider: ConfigurationManager.ConnectionStrings["ApplicationServices"].ProviderName);
        }

        /// <summary>
        /// Add volume to a subscription.
        /// </summary>
        /// <remarks>
        /// The subscription must have a voucher based SLA
        /// </remarks>
        /// <param name="sitId"></param>
        /// <param name="volumeCode"></param>
        /// <param name="ispId"></param>
        public bool AddVolume(int sitId, int volumeCode, int ispId)
        {
            bool result = false;

            try
            {
                //Set authentication credentials and WS settings
                _ispService.Credentials = this.readCredentials(ispId);
                _ispService.PreAuthenticate = true;
                ServicePointManager.Expect100Continue = false;
                _ispService.addAdditionalVolumeBySit(sitId.ToString(), volumeCode.ToString(), ispId.ToString());
                result = true;
            }
            catch (Exception ex)
            {
                this.logException(ex, "AddVolume", ExceptionLevelEnum.Error);
                result = false;
            }

            return result;
        }

        /// <summary>
        /// Changes the ISP of a terminal
        /// </summary>
        /// <param name="macAddress">The terminals MAC address</param>
        /// <param name="oldISP">The old ISP</param>
        /// <param name="newISP">The new iSP</param>
        /// <returns>True if the operation succeeded</returns>
        public bool ChangeISP(string endUserId, int oldISP, int newISP)
        {
            bool result = false;

            try
            {
                //Set authentication credentials and WS settings
                _ispService.Credentials = this.readCredentials(oldISP);
                _ispService.PreAuthenticate = true;
                ServicePointManager.Expect100Continue = false;
                _ispService.reassignRegistration(endUserId, oldISP.ToString(), newISP.ToString());
                result = true;
            }
            catch (Exception ex)
            {
                this.logException(ex, "ChangeISP", ExceptionLevelEnum.Critical);
                result = false;
            }

            return result;
        }

        /// <summary>
        /// Logs a CmtApplicationException
        /// </summary>
        /// <param name="ex">The original exception</param>
        /// <param name="method">The failing method name</param>
        private void logException(Exception ex, string method, ExceptionLevelEnum level)
        {
            CmtApplicationException cmtApplicationException = new CmtApplicationException();
            cmtApplicationException.ExceptionDateTime = DateTime.Now;
            cmtApplicationException.ExceptionDesc = ex.Message;
            cmtApplicationException.ExceptionStacktrace = "";
            cmtApplicationException.ExceptionLevel = (short?)level;
            cmtApplicationException.UserDescription = method;
            _logController.LogApplicationException(cmtApplicationException);
        }

        /// <summary>
        /// This method returns the ISPIF credentials for the ISP associated to the 
        /// given MAC address
        /// </summary>
        /// <param name="macAddress">The target MAC Address</param>
        /// <returns>A NetworkCredential instance or null in case of an error</returns>
        private NetworkCredential readCredentials(string macAddress)
        {
            //Get the credentials from the database
            string connectionString =
               ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
            string databaseProviderString =
               ConfigurationManager.ConnectionStrings["ApplicationServices"].ProviderName;

            BOControllerLibrary.Controllers.BOAccountingController boAccountingController =
                            new BOControllerLibrary.Controllers.BOAccountingController(connectionString, databaseProviderString);

            //First get the terminal info object from the database
            BOControllerLibrary.Model.Terminal.Terminal term
                                    = boAccountingController.GetTerminalDetailsByMAC(macAddress);
            BOControllerLibrary.Model.ISP.Isp isp = boAccountingController.GetISP(term.IspId);
            NetworkCredential cred = new NetworkCredential(isp.Ispif_User, isp.Ispif_Password);
            return cred;
        }

        /// <summary>
        /// This method returns the ISPIF credentials for the given ISP
        /// </summary>
        /// <param name="macAddress">The target MAC Address</param>
        /// <returns>A NetworkCredential instance or null in case of an error</returns>
        private NetworkCredential readCredentials(int IspId)
        {
            //Get the credentials from the database
            string connectionString =
               ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
            string databaseProviderString =
               ConfigurationManager.ConnectionStrings["ApplicationServices"].ProviderName;

            BOControllerLibrary.Controllers.BOAccountingController boAccountingController =
                            new BOControllerLibrary.Controllers.BOAccountingController(connectionString, databaseProviderString);

            BOControllerLibrary.Model.ISP.Isp isp = boAccountingController.GetISP(IspId);
            NetworkCredential cred = new NetworkCredential(isp.Ispif_User, isp.Ispif_Password);
            return cred;
        }
    }
}