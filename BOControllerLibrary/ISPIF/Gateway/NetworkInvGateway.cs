﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Data.SqlClient;
using BOControllerLibrary.Controllers;
using BOControllerLibrary.Controllers.Interfaces;
using BOControllerLibrary.Model.Terminal;
using BOControllerLibrary.Model.Log;
using BOControllerLibrary.ISPIF.Model;
using BOControllerLibrary.Model;
using System.Web.Script.Serialization;
using RestSharp;
using RestSharp.Authenticators;
using System.Net;
using System.Net.Security;
//Dialog webservices
using BOControllerLibrary.NetworkInovWebServiceRef;

//NMS webservices
using nms = BOControllerLibrary.NMSGatewayServiceRef;
using BOControllerLibrary.Model.Organization;

namespace BOControllerLibrary.ISPIF.Gateway
{
    /// <summary>
    /// This controller provides access to the SES Astra2G Dialog M2M interface
    /// It is used for creating and managing terminals on the Dialog hub
    /// The Dialog interface handles the provisioning of terminals, but the main
    /// SLA and volume management is done by the CG NMS. Therefor, this interface is 
    /// an extenstion of the NMS Gateway and overrides certain hub-specific methods
    /// of the NMS Gateway
    /// </summary>
    public class NetworkInvGateway : NMSGateway
    {
        private IBOConfigurationController _configurationController;
        private IBOAccountingController _accountingController;
        private IBOLogController _logController;
        private string _connectionString;
        private string _databaseProvider;
        private RestClient _restClient;
        private string _baseUrl = "http://10.9.42.110/";
        private string _username = "SATADSL";
        private string _password = "S@T@D5L";
        private string _domainName = "hno";
        private SoapService _soapService;

        /// <summary>
        /// Default constructor
        /// </summary>
        public NetworkInvGateway()
        {
            _connectionString = ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
            _databaseProvider = ConfigurationManager.ConnectionStrings["ApplicationServices"].ProviderName;
            _accountingController = new BOAccountingController(_connectionString, _databaseProvider);
            _configurationController = new BOConfigurationController(_connectionString, _databaseProvider);
            _logController = new BOLogController(_connectionString, _databaseProvider);
            _restClient = new RestClient();
            _restClient.BaseUrl = new Uri(_baseUrl);
            _restClient.Authenticator = new HttpBasicAuthenticator(_username, _password);
            _soapService = new SoapService();
        }

        /// <summary>
        /// Creates a terminal in the Dialog Hub. 
        /// This method sets the NMSActivate flag which indicates that the 
        /// TicketMaster still needs to create the terminal in the NMS.
        /// </summary>
        /// <param name="endUserId">The end user identifier related to the terminal</param>
        /// <param name="macAddress">The terminal's MAC address</param>
        /// <param name="slaId">
        /// The Service Level identifier. For this version of the IHubGateway interface
        /// the slaId is retrieved from the configuration table since the sla is NOT the sla for the 
        /// activation in the NMS!
        /// </param>
        /// <param name="ispId">The ISP id</param>
        /// <returns>true if the operation succeeds</returns>
        public override bool TerminalActivate(string endUserId, string macAddress, long slaId, int ispId, Guid userId)
        {
            bool result = false;
            string failureReason = "";
            string failureStackTrace = "";
            // Get the terminal details from the CMT
            Terminal cmtTerm = _accountingController.GetTerminalDetailsByMAC(macAddress);

            if (cmtTerm.DialogTechnology == "SCPC" || cmtTerm.DialogTechnology == "MxDMA")
            {
                // Initiate and serialize the Dialog terminal object
                JavaScriptSerializer js = new JavaScriptSerializer();
                string javaTerminal = "";
                if (cmtTerm.DialogTechnology == "SCPC")
                {
                    javaTerminal = js.Serialize(this.InitiateTermSCPCLineUp(cmtTerm));
                }
                else if (cmtTerm.DialogTechnology == "MxDMA")
                {
                    javaTerminal = js.Serialize(this.InitiateTermMxDMALineUp(cmtTerm));
                }

                //Create the HTTP request
                RestRequest provisioning = new RestRequest("rest/modem", Method.POST) { RequestFormat = DataFormat.Json };
                provisioning.AddParameter("application/json", javaTerminal, ParameterType.RequestBody);

                try
                {
                    //Execute the HTTP request
                    IRestResponse response = _restClient.Execute(provisioning);
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        result = true;
                        //save the sit ID and the IP address in the CMT
                        var dict = js.Deserialize<Dictionary<string, dynamic>>(response.Content);
                        cmtTerm.SitId = dict["id"]["systemId"];
                        //cmtTerm.IpAddress = dict["networkSettingses"][0]["ipv4"]["address"];
                        cmtTerm.Blade = 0; //for now
                        cmtTerm.ExtFullName = dict["id"]["systemId"] + ".NI-SATADSL";
                        _accountingController.UpdateTerminal(cmtTerm);
                    }
                    else if (response.StatusCode == 0)
                    {
                        failureReason = response.ErrorException.Message;
                        failureStackTrace = response.ErrorException.StackTrace;
                    }
                    else
                    {
                        failureReason = response.Content;
                        failureStackTrace = response.StatusCode.ToString();
                    }

                }
                catch (Exception ex)
                {
                    this.logException(ex, "NetworkInvGateway.TerminalActivate");
                    failureReason = ex.Message;
                    failureStackTrace = ex.StackTrace;
                }
            }
            else
            {
                failureReason = "Dialog technology not defined";
                failureStackTrace = "The dialog technology of the terminal has not been defined. Please select either SCPC or MxDMA on the terminal details screen.";
            }

            this.insertRequestTicketWithSla(!result, macAddress, AdmStatus.Request, AdmStatus.OPERATIONAL, failureReason, failureStackTrace, TerminalActivityTypes.Activation, ispId, (int)slaId, userId);
            return result;
        }

        /// <summary>
        /// Changes the SLA of the terminal in the Dialog hub as well as in the NMS 
        /// </summary>
        /// <param name="endUserId">The identification of the terminal to activate</param>
        /// <param name="macAddress">The MAC address of the terminal to activate</param>
        /// <param name="newSlaId">The service pack allocated to the terminal</param>
        /// <param name="currentState">The current admin state of the terminal</param>
        /// <returns>true if the application succeeded</returns>
        public override bool TerminalChangeSla(string endUserId, string macAddress, int newSlaId, Model.AdmStatus currentState, int ispId, Guid userId)
        {
            bool result = false;
            string failureReason = "";
            string failureStackTrace = "";
            //Get the terminal details form the CMT
            Terminal cmtTerm = _accountingController.GetTerminalDetailsByMAC(macAddress);
            //Get the NAME of the new SLA in order to set the correct SLA in the hub
            ServiceLevel newSLA = _accountingController.GetServicePack(newSlaId);

            if (newSLA.SlaCommonName != null)
            {
                try
                {
                    //Fetch the terminal details from the hub
                    RestRequest requestModem = new RestRequest("rest/modem/" + (long)cmtTerm.SitId, Method.GET) { RequestFormat = DataFormat.Json };
                    IRestResponse response = _restClient.Execute(requestModem);
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        //Update the service profile (=SLA) in the modem object
                        String s = response.Content.Substring(0, response.Content.IndexOf("serviceProfileId") - 1);
                        String p = response.Content.Substring(response.Content.IndexOf("serviceProfileId"));
                        String e = p.Substring(p.IndexOf("}") + 1);
                        s = s + "\"serviceProfileId\":{\"domainName\":\"" + _domainName + "\",\"name\":\"" + newSLA.SlaCommonName.Trim() + "\"}" + e;
                        //Request the SLA change in the hub
                        RestRequest requestSlaChange = new RestRequest("rest/modem/" + (long)cmtTerm.SitId, Method.PUT) { RequestFormat = DataFormat.Json };
                        requestSlaChange.AddParameter("application/json", s, ParameterType.RequestBody);
                        response = _restClient.Execute(requestSlaChange);
                        if (response.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            result = true;
                        }
                        else if (response.StatusCode == 0)
                        {
                            failureReason = response.ErrorException.Message;
                            failureStackTrace = response.ErrorException.StackTrace;
                        }
                        else
                        {
                            failureReason = response.Content;
                            failureStackTrace = response.StatusCode.ToString();
                        }
                    }
                    else if (response.StatusCode == 0)
                    {
                        failureReason = response.ErrorException.Message;
                        failureStackTrace = response.ErrorException.StackTrace;
                    }
                    else
                    {
                        failureReason = response.Content;
                        failureStackTrace = response.StatusCode.ToString();
                    }
                }
                catch (Exception ex)
                {
                    this.logException(ex, "NetworkInvGateway.TerminalChangeSla");
                    failureReason = ex.Message;
                    failureStackTrace = ex.StackTrace;
                }
            }
            else
            {
                failureReason = "SLA common name not set";
                failureStackTrace = "The common name of the SLA has not been set. Please set this in the SLA details in order to set the hub SLA to which the CMT SLA points.";
            }

            //this.insertRequestTicketWithSla(!result, macAddress, AdmStatus.OPERATIONAL, AdmStatus.OPERATIONAL, failureReason, failureStackTrace, TerminalActivityTypes.ChangeSla, ispId, newSlaId, userId);

            if (result)
            {
                return this.NMSTerminalChangeSla(endUserId, macAddress, newSlaId, currentState, ispId, userId);
            }
            else
            {
                return result;
            }
        }

        /// <summary>
        /// Decommissions a terminal in the Dialog hub. 
        /// The terminal is effectively removed from the Hub database
        /// </summary>
        /// <param name="endUserId">The identification of the terminal to activate</param>
        /// <param name="macAddress">The MAC address of the terminal to decommission</param>
        /// <returns>true if the activation succeeded</returns>
        public override bool TerminalDecommission(string endUserId, string macAddress, int ispId, Guid userId)
        {
            bool result = false;
            string failureReason = "";
            string failureStackTrace = "";
            //Get the terminal details form the CMT
            Terminal cmtTerm = _accountingController.GetTerminalDetailsByMAC(macAddress);

            try
            {
                //Delete the terminal in the hub
                RestRequest deleteRequest = new RestRequest("rest/modem/" + (long)cmtTerm.SitId, Method.DELETE) { RequestFormat = DataFormat.Json };
                IRestResponse response = _restClient.Execute(deleteRequest);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    result = true;
                }
                else if (response.StatusCode == 0)
                {
                    failureReason = response.ErrorException.Message;
                    failureStackTrace = response.ErrorException.StackTrace;
                }
                else
                {
                    failureReason = response.Content;
                    failureStackTrace = response.StatusCode.ToString();
                }

            }
            catch (Exception ex)
            {
                this.logException(ex, "NetworkInvGateway.TerminalDecommission");
                failureReason = ex.Message;
                failureStackTrace = ex.StackTrace;
            }
            this.insertRequestTicket(!result, macAddress, AdmStatus.LOCKED, AdmStatus.DECOMMISSIONED, failureReason, failureStackTrace, TerminalActivityTypes.Decommissioning, ispId, userId);
            return result;
        }

        /// <summary>
        /// Reactivates a terminal in the Dialog hub
        /// </summary>
        /// <param name="endUserId"></param>
        /// <param name="macAddress"></param>
        /// <param name="ispId"></param>
        /// <returns></returns>
        public override bool TerminalReActivate(string endUserId, string macAddress, int ispId, Guid userId)
        {
            bool result = false;
            string failureReason = "";
            string failureStackTrace = "";
            //Get the terminal details form the CMT
            Terminal cmtTerm = _accountingController.GetTerminalDetailsByMAC(macAddress);

            try
            {
                //Delete the terminal in the hub
                RestRequest unlockRequest = new RestRequest("rest/modem/" + (long)cmtTerm.SitId + "/lock", Method.DELETE) { RequestFormat = DataFormat.Json };
                IRestResponse response = _restClient.Execute(unlockRequest);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    result = true;
                }
                else if (response.StatusCode == 0)
                {
                    failureReason = response.ErrorException.Message;
                    failureStackTrace = response.ErrorException.StackTrace;
                }
                else
                {
                    failureReason = response.Content;
                    failureStackTrace = response.StatusCode.ToString();
                }
            }
            catch (Exception ex)
            {
                this.logException(ex, "NetworkInvGateway.TerminalReActivate");
                failureReason = ex.Message;
                failureStackTrace = ex.StackTrace;
            }
            this.insertRequestTicket(!result, macAddress, AdmStatus.LOCKED, AdmStatus.OPERATIONAL, failureReason, failureStackTrace, TerminalActivityTypes.ReActivation, ispId, userId);
            return result;
        }

        /// <summary>
        /// Suspends a terminal, the status of the terminal becomes LOCKED
        /// </summary>
        /// <param name="endUserId">The identification of the terminal to activate</param>
        /// <param name="macAddress">The MAC address of the terminal to activate</param>
        /// <returns>true if the re-activation succeeded</returns>
        public override bool TerminalSuspend(string endUserId, string macAddress, int ispId, Guid userId)
        {
            bool result = false;
            string failureReason = "";
            string failureStackTrace = "";
            //Get the terminal details form the CMT
            Terminal cmtTerm = _accountingController.GetTerminalDetailsByMAC(macAddress);

            try
            {
                //Delete the terminal in the hub
                RestRequest lockRequest = new RestRequest("rest/modem/" + (long)cmtTerm.SitId + "/lock", Method.POST) { RequestFormat = DataFormat.Json };
                IRestResponse response = _restClient.Execute(lockRequest);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    result = true;
                }
                else if (response.StatusCode == 0)
                {
                    failureReason = response.ErrorException.Message;
                    failureStackTrace = response.ErrorException.StackTrace;
                }
                else
                {
                    failureReason = response.Content;
                    failureStackTrace = response.StatusCode.ToString();
                }
            }
            catch (Exception ex)
            {
                this.logException(ex, "NetworkInvGateway.TerminalReActivate");
                failureReason = ex.Message;
                failureStackTrace = ex.StackTrace;
            }
            this.insertRequestTicket(!result, macAddress, AdmStatus.OPERATIONAL, AdmStatus.LOCKED, failureReason, failureStackTrace, TerminalActivityTypes.Suspension, ispId, userId);
            return result;
        }

        /// <summary>
        /// Changes the MacAddress in the Dialog Hub
        /// </summary>
        /// <param name="endUserId"></param>
        /// <param name="oriMacAddress"></param>
        /// <param name="newMacAddress"></param>
        /// <param name="ispId"></param>
        /// <returns></returns>
        public override bool ChangeMacAddress(string endUserId, string oriMacAddress, string newMacAddress, int ispId, Guid userId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method retrieves the terminal information from the CMT Database
        /// </summary>
        /// <param name="macAddress">The terminal MAC address</param>
        /// <param name="ispId">The ISP identifier, this value is not really used for this implementation</param>
        /// <returns>A completed TerminalInfo object</returns>
        public override TerminalInfo getTerminalDetailsFromHub(string macAddress, string ispId)
        {
            TerminalInfo ti = new TerminalInfo();
            Terminal term = _accountingController.GetTerminalDetailsByMAC(macAddress);
            
            //Copy the terminal data to a terminal information object
            try
            {
                if (term.Blade != null)
                    ti.BladeId = term.Blade.ToString();

                if (term.AdmStatus != null)
                    ti.Blocked = (term.AdmStatus == 2);
                if (term.FullName != null)
                    ti.EndUserId = term.FullName;

                if (term.IpAddress != null)
                    ti.IPAddress = term.IpAddress;

                ti.IspId = term.IspId.ToString();

                if (term.MacAddress != null)
                    ti.MacAddress = term.MacAddress;

                ti.SitId = term.SitId.ToString();

                if (term.SlaId != null)
                    ti.SlaId = term.SlaId.ToString();

                if (term.SlaName != null)
                    ti.SlaName = term.SlaName;

                if (term.AdmStatus != null)
                    ti.Status = term.AdmStatus.ToString();
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDateTime = DateTime.Now;
                cmtEx.ExceptionDesc = ex.Message;
                cmtEx.ExceptionLevel = (short)ExceptionLevelEnum.Error;
                cmtEx.ExceptionStacktrace = ex.StackTrace;
                cmtEx.StateInformation = "Error";
                cmtEx.UserDescription = "Copy from Terminal to TerminalInfo object failed.";
                _logController.LogApplicationException(cmtEx);
            }

            return ti;
        }

        /// <summary>
        /// Returns the C/No value. This value is retrieved from the Dialog Hub
        /// </summary>
        /// <param name="ispId">The unique ISP identifier</param>
        /// <param name="sitId">The site id</param>
        /// <returns>The C/No value as a string</returns>
        public override string getRTNValue(string ispId, string sitId)
        {
            //Initiate the connection
            string conn = this.SoapConnect();
            
            //Set the necessary parameters
            string[] parameters = new string[] {"230"};
            Terminal cmtTerm = _accountingController.GetTerminalDetailsBySitId(Convert.ToInt32(sitId), Convert.ToInt32(ispId));
            //string terminalName = cmtTerm.ExtFullName.Replace(" ", "-").Replace(":", "") + ".SATADSL";
            
            //Make the SOAP call
            //ParamValue[] paramResults = _soapService.GetParameterValueByElementName(conn, terminalName, parameters);
            ParamValue[] paramResults = _soapService.GetParameterValueByElementName(conn, cmtTerm.ExtFullName, parameters);

            //Convert the result to a string with only the number
            string valueString;
            if (paramResults.Length == 0) 
              valueString = "-1.0";
            else
                valueString = paramResults[0].Value[0][0];
            string[] result = valueString.Split(' ');
            return result[0];
        }

        /// <summary>
        /// Returns the Es/No value. This value is retrieved from the Dialog Hub
        /// </summary>
        /// <remarks>
        /// This value is probably available from the Eutelsat hub but needs further investigation.
        /// For the moment being we are returning 9999 to indicate a false reading.
        /// </remarks>
        /// <param name="ispId">The unique ISP identifier</param>
        /// <param name="sitId">The site id</param>
        /// <returns>The Es/No value as a string</returns>
        public override string getFWDValue(string ispId, string sitId)
        {
            //Initiate the connection
            string conn = this.SoapConnect();

            //Set the necessary parameters
            string[] parameters = new string[] { "213" };
            Terminal cmtTerm = _accountingController.GetTerminalDetailsBySitId(Convert.ToInt32(sitId), Convert.ToInt32(ispId));
            //string terminalName = cmtTerm.ExtFullName.Replace(" ", "-").Replace(":", "") + ".SATADSL";

            //Make the SOAP call
            ParamValue[] paramResults = _soapService.GetParameterValueByElementName(conn, cmtTerm.ExtFullName, parameters);

            //Convert the result to a string with only the number
            string valueString;
            if (paramResults.Length == 0)
                valueString = "-1.0";
            else
                valueString = paramResults[0].Value[0][0];
            string[] result = valueString.Split(' ');
            return result[0];
        }

        /// <summary>
        /// Activates a terminal in the NMS.
        /// </summary>
        /// <param name="endUserId">The end user identifier</param>
        /// <param name="macAddress">The terminal MAC address</param>
        /// <param name="slaId">The SLA identifier</param>
        /// <param name="ispId">The ISP idnetifier</param>
        /// <param name="ipMask">IP range specified by means of a MASK</param>
        /// <returns>True if the method succeeds</returns>
        public override bool NMSTerminalActivateWithRange(string endUserId, string macAddress, long slaId, int ispId, int ipMask, int freeZone, Guid userId)
        {
            bool result = false;
            nms.NMSGatewayService nmsService = new nms.NMSGatewayService();
            Terminal term = _accountingController.GetTerminalDetailsByMAC(macAddress);


            try
            {
                TerminalInfo ti = this.getTerminalDetailsFromHub(macAddress, ispId.ToString());
                nms.RequestTicket rt;
                if (term.OrgId != null)
                {
                    Organization organization = _accountingController.GetOrganization((int)term.OrgId);
                    if (organization.VNO != null && (bool)organization.VNO)
                    {
                        rt = nmsService.createRegistrationWithVNO(endUserId, macAddress, ipMask, (int)slaId, ispId, ti.IPAddress, Int32.Parse(ti.SitId), freeZone, organization.FullName);
                    }
                    else
                    {
                        rt = nmsService.createRegistration(endUserId, macAddress, ipMask, (int)slaId, ispId, ti.IPAddress, Int32.Parse(ti.SitId), freeZone);
                    }
                }
                //Need to specify an IP Address as this address is not generated by the NMS but by the Eutelsat HUB
                else
                {
                    rt = nmsService.createRegistration(endUserId, macAddress, ipMask, (int)slaId, ispId, ti.IPAddress, Int32.Parse(ti.SitId), freeZone);
                }

                result = this.insertRequestTicketWithSla(rt, macAddress, AdmStatus.LOCKED, AdmStatus.OPERATIONAL, TerminalActivityTypes.Activation, ispId, (int)slaId, userId);
            }
            catch (Exception ex)
            {
                this.logException(ex, "NetworkInvGateway.TerminalActivate");
            }
            return result;
        }

        /// <summary>
        /// Activates a terminal in the NMS without specifying a range by means of
        /// an IP Mask value.
        /// </summary>
        /// <remarks>
        /// This method activates a terimal in the NMS without specifiying a range, thus the
        /// IP Mask is always set to /32 which means that only one IP address can be specified.
        /// </remarks>
        /// <param name="endUserId">The end user identifier</param>
        /// <param name="macAddress">The terminal MAC address</param>
        /// <param name="slaId">The SLA identifier</param>
        /// <param name="ispId">The ISP identifier</param>
        /// <returns>True if the method succeeds</returns>
        public override bool NMSTerminalActivate(string endUserId, string macAddress, long slaId, int ispId, int freeZone, Guid userId)
        {
            nms.NMSGatewayService nmsService = new nms.NMSGatewayService();
            bool result = false;

            try
            {
                //Need to specify an IP Address as this address is not generated by the NMS but by the Eutelsat HUB
                TerminalInfo ti = this.getTerminalDetailsFromHub(macAddress, ispId.ToString());
                nms.RequestTicket rt = nmsService.createRegistration(endUserId, macAddress, 32, (int)slaId, ispId, ti.IPAddress, Int32.Parse(ti.SitId), freeZone);
                result = base.insertRequestTicket(rt, macAddress, AdmStatus.Request, AdmStatus.OPERATIONAL, TerminalActivityTypes.Activation, ispId, userId);
            }
            catch (Exception ex)
            {
                this.logException(ex, "NetworkInvGateway.TerminalActivate");
            }
            return result;
        }

        /// <summary>
        /// This method allows to update the freezone information for a terminal
        /// </summary>
        /// <param name="FreeZoneId">The identifier of the freezone allocated to the terminal</param>
        /// <param name="macAddress">The MAC address of the terminal</param>
        /// <param name="FreeZoneFlag">If this flag is set to true, a freezone option is allocated to the terminal. In
        /// case this value is false, the terminal does not have the freezone option</param>
        /// <returns></returns>
        public bool NMSModifyFreeZoneForTerminal(int FreeZoneId, string macAddress, Boolean FreeZoneFlag)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Inserts a new ticket in the CMT database and flags it as an NMS ticket
        /// </summary>
        /// <remarks>
        /// Use this version of the insertRequestTicket method to insert a request ticket for activities
        /// on the Dialog hub in the database
        /// </remarks>
        /// <param name="failed">Has status true if the request on the Dialog hub failed</param>
        /// <param name="macAddress">The MacAddress of the target</param>
        /// <param name="sourceAdmStatus">The Source admin status</param>
        /// <param name="targetAdmStatus">The Target admin status</param>
        /// <param name="termActivityType">The terminal activity type</param>
        /// <param name="ispId">The ISP ID</param>
        /// <returns>True if the operation succeeded</returns>
        private bool insertRequestTicket(bool failed, string macAddress, AdmStatus sourceAdmStatus, AdmStatus targetAdmStatus, string failureReason, string failureStackTrace,
                                           TerminalActivityTypes termActivityType, int ispId, Guid userId)
        {
            TicketDataMgr ticketDataMgr = new TicketDataMgr();
            Guid ticketId = Guid.NewGuid();
            if (failureReason.Length > 400)
            {
                failureReason = failureReason.Substring(0, 400);
            }
            int ticketStatus = 0;
            if (failed)
            {
                ticketStatus = 1;
            }
            return ticketDataMgr.insertTicket(ticketId.ToString(), macAddress, sourceAdmStatus, targetAdmStatus, ticketStatus, failureReason, failureStackTrace, termActivityType, ispId, 1, userId);
        }

        /// <summary>
        /// Inserts a ticket  for an SLA change
        /// </summary>
        /// <remarks>
        /// Use this version of the insertRequestTicket method to insert a request ticket for activities
        /// on the Dialog hub in the database
        /// </remarks>
        /// <param name="failed">Has status true if the request on the Dialog hub failed</param>
        /// <param name="macAddress">The Mac Address</param>
        /// <param name="sourceAdmStatus">The source amdin status</param>
        /// <param name="targetAdmStatus">The target admin status</param>
        /// <param name="termActivityType">The terminal activity type</param>
        /// <param name="ispId">The ISP identifier</param>
        /// <param name="newSla">The new sla</param>
        /// <returns>True if the method succeeded</returns>
        private bool insertRequestTicketWithSla(bool failed, string macAddress, AdmStatus sourceAdmStatus, AdmStatus targetAdmStatus, string failureReason, string failureStackTrace,
                                           TerminalActivityTypes termActivityType, int ispId, int newSla, Guid userId)
        {
            TicketDataMgr ticketDataMgr = new TicketDataMgr();
            Guid ticketId = Guid.NewGuid();
            if (failureReason.Length > 400)
            {
                failureReason = failureReason.Substring(0, 400);
            }
            int ticketStatus = 0;
            if (failed)
            {
                ticketStatus = 1;
            }
            return ticketDataMgr.insertTicket(ticketId.ToString(), macAddress, sourceAdmStatus, targetAdmStatus, ticketStatus, failureReason, failureStackTrace, termActivityType, ispId, 1, newSla, userId);
        }

        /// <summary>
        /// Inserts a ticket for a MAC Address change
        /// </summary>
        /// <remarks>
        /// Use this version of the insertRequestTicket method to insert a request ticket for activities
        /// on the Dialog hub in the database
        /// </remarks>
        /// <param name="failed">Has status true if the request on the Dialog hub failed</param>
        /// <param name="macAddress">The MAC address of the terminal</param>
        /// <param name="sourceAdmStatus">The sourde administration status</param>
        /// <param name="targetAdmStatus">The target administration status</param>
        /// <param name="termActivityType">The terminal activity type</param>
        /// <param name="ispId">The ISP identifier</param>
        /// <param name="NewMacAddress">The new mac address</param>
        /// <returns></returns>
        private bool insertRequestTicketWithMac(bool failed, string macAddress, AdmStatus sourceAdmStatus, AdmStatus targetAdmStatus, string failureReason, string failureStackTrace,
                                           TerminalActivityTypes termActivityType, int ispId, string NewMacAddress, Guid userId)
        {
            TicketDataMgr ticketDataMgr = new TicketDataMgr();
            Guid ticketId = Guid.NewGuid();
            if (failureReason.Length > 400)
            {
                failureReason = failureReason.Substring(0, 400);
            }
            int ticketStatus = 0;
            if (failed)
            {
                ticketStatus = 1;
            }
            return ticketDataMgr.insertTicket(ticketId.ToString(), macAddress, sourceAdmStatus, targetAdmStatus, ticketStatus, failureReason, failureStackTrace, termActivityType, ispId, 1, NewMacAddress, userId);
        }

        /// <summary>
        /// Returns the status of a request ticket.
        /// </summary>
        /// <remarks>
        /// This method gets the ticket from the CMT database 
        /// </remarks>
        /// <param name="requestTicketId">The requestticket identfier</param>
        /// <param name="ispId">The ISP identfier</param>
        /// <returns>
        /// The corresponding CMT request ticket or null if the ticket could not 
        ///  be found
        ///  </returns>
        public override CmtRequestTicket LookupRequestTicket(string requestTicketId, int ispId)
        {
            CmtRequestTicket cmtRt = null;
            try
            {
                CMTTicket cmtTick = _logController.GetTicketById(requestTicketId);
                cmtRt = new CmtRequestTicket();
                cmtRt.id = cmtTick.TicketId;
                cmtRt.failureReason = cmtTick.FailureReason;
                cmtRt.failureStackTrace = cmtTick.FailureStackTrace;

                if (cmtTick.TicketStatus == 0)
                {
                    cmtRt.requestStatus = CmtRequestStatus.SUCCESSFUL;
                }

                if (cmtTick.TicketStatus == 1)
                {
                    cmtRt.requestStatus = CmtRequestStatus.FAILED;
                }

                if (cmtTick.TicketStatus == 2)
                {
                    cmtRt.requestStatus = CmtRequestStatus.SUCCESSFUL;
                }
            }
            catch (Exception ex)
            {
                this.logException(ex, "NetworkInvGateway.LookupRequestTicket");
                cmtRt = null;
            }

            return cmtRt;
        }

        /// <summary>
        /// Initiates the Dialog terminal object for the activation of an SCPC terminal
        /// </summary>
        /// <param name="cmtTerm">The CMT terminal to be activated</param>
        /// <returns>A Dialog SCPC terminal for Line Up</returns>
        private DialogTerminalSCPC InitiateTermSCPCLineUp(Terminal cmtTerm)
        {
            //get the service profile related resources
            ServiceLevel sla = _accountingController.GetServicePack((int)cmtTerm.SlaId);
            string[] resources = this.GetResources(sla.SlaCommonName.Trim());
            
            // Initiate the Dialog terminal object
            DialogTerminalSCPC dialogTerm = new DialogTerminalSCPC();
            dialogTerm.id.domainName = "NI-SATADSL";
            dialogTerm.id.name = cmtTerm.ExtFullName.Replace(" ", "-").Replace(":", "");
            dialogTerm.forwardClassificationProfileId.domainName = "System";
            dialogTerm.forwardClassificationProfileId.name = "best-effort-only";
            dialogTerm.forwardPoolId.domainName = _domainName;
            dialogTerm.forwardPoolId.name = resources[0]; //LineUP_VNO STRCP-TB-MxDMA
            dialogTerm.locked = false;
            dialogTerm.macAddress = cmtTerm.MacAddress;
            dialogTerm.debugMode = false;
            dialogTerm.description = "";
            dialogTerm.encryptTraffic = true;
            dialogTerm.monitoringType = "Advanced";

            // Initiate the network settings
            NetworkSettings network = new NetworkSettings();
            network.type = "VIRTUAL";
            network.ipv4.dhcpMode = "Enabled";
            network.ipv4.address = this.SetIPAddressForHub(cmtTerm.IpAddress.Trim(), cmtTerm.IPMask);
            network.ipv4.prefixLength = cmtTerm.IPMask;
            network.ipv4.lanAddressPingable = true;
            network.networkId.domainName = _domainName;
            network.networkId.name = "Uplink-Sat2-Net-501";
            dialogTerm.networkSettingses.Add(network);

            dialogTerm.returnClassificationProfileId.domainName = "System";
            dialogTerm.returnClassificationProfileId.name = "best-effort-only";
            dialogTerm.satelliteNetworkId.domainName = _domainName;
            dialogTerm.satelliteNetworkId.name = "SatNet-2";
            dialogTerm.serviceProfileId.domainName = _domainName;
            dialogTerm.serviceProfileId.name = sla.SlaCommonName.Trim();
            dialogTerm.softwareUpdateGroup = 0;
            dialogTerm.type = "MDM3100";
            dialogTerm.returnTechnology = "HRC_SCPC";
            dialogTerm.returnPoolId.domainName = _domainName;
            dialogTerm.returnPoolId.name = "test";
            dialogTerm.hrcScpcSettings.hrcScpcReturnCapacityGroupId.domainName = _domainName;
            dialogTerm.hrcScpcSettings.hrcScpcReturnCapacityGroupId.name = "test";
            dialogTerm.hrcScpcSettings.centerFrequency = cmtTerm.CenterFrequency; //Convert.ToInt64(14090250000);
            dialogTerm.hrcScpcSettings.symbolRate = cmtTerm.SymbolRate;

            if (dialogTerm.serviceProfileId.name == "SP-SATADSL-1M_4M")
            {
                dialogTerm.serviceProfileId.domainName = "NI-SATADSL";
            }

            return dialogTerm;
        }

        /// <summary>
        /// Initiates the Dialog terminal object for the activation of an MxDMA terminal
        /// </summary>
        /// <param name="cmtTerm">The CMT terminal to be activated</param>
        /// <returns>A Dialog MxDMA terminal for Line Up</returns>
        private DialogTerminalMxDMALineUp InitiateTermMxDMALineUp(Terminal cmtTerm)
        {
            //get the service profile related resources
            ServiceLevel sla = _accountingController.GetServicePack((int)cmtTerm.SlaId);
            string[] resources = this.GetResources(sla.SlaCommonName.Trim());

            // Initiate the Dialog terminal object
            DialogTerminalMxDMALineUp dialogTerm = new DialogTerminalMxDMALineUp();
            dialogTerm.id.domainName = "NI-SATADSL";
            dialogTerm.forwardClassificationProfileId.domainName = "System";
            dialogTerm.forwardClassificationProfileId.name = "best-effort-only";
            dialogTerm.forwardPoolId.domainName = _domainName;
            dialogTerm.forwardPoolId.name = resources[0];
            dialogTerm.locked = false;
            dialogTerm.macAddress = cmtTerm.MacAddress;
            dialogTerm.debugMode = false;
            dialogTerm.description = "";
            dialogTerm.encryptTraffic = true;
            dialogTerm.monitoringType = "Advanced";

            // Initiate the network settings
            NetworkSettings network = new NetworkSettings();
            network.type = "VIRTUAL";
            network.ipv4.dhcpMode = "Enabled";
            network.ipv4.address = this.SetIPAddressForHub(cmtTerm.IpAddress.Trim(), cmtTerm.IPMask);
            network.ipv4.prefixLength = cmtTerm.IPMask;
            network.ipv4.lanAddressPingable = true;
            network.networkId.domainName = _domainName;
            network.networkId.name = "Uplink-Sat2-Net-501";
            dialogTerm.networkSettingses.Add(network);

            dialogTerm.returnClassificationProfileId.domainName = "System";
            dialogTerm.returnClassificationProfileId.name = "best-effort-only";
            dialogTerm.satelliteNetworkId.domainName = _domainName;
            dialogTerm.satelliteNetworkId.name = "SatNet-2";
            dialogTerm.serviceProfileId.domainName = _domainName;
            dialogTerm.serviceProfileId.name = sla.SlaCommonName.Trim();
            dialogTerm.softwareUpdateGroup = 0;
            dialogTerm.type = "MDM3100";
            dialogTerm.returnTechnology = "HRC_MXDMA";
            dialogTerm.hrcMxDmaSettings.returnPoolId.domainName = _domainName;
            dialogTerm.hrcMxDmaSettings.returnPoolId.name = resources[1];

            dialogTerm.lineUpSettings.nominalBandwidthHz = 438000;
            dialogTerm.lineUpSettings.nominalOutputPowerDbm = -25;
            dialogTerm.lineUpSettings.outputPower1DbCompression = 5;

            if (dialogTerm.serviceProfileId.name == "SP-SATADSL-1M_4M")
            {
                dialogTerm.serviceProfileId.domainName = "NI-SATADSL";
            }

            return dialogTerm;
        }

        /// <summary>
        /// Connects to the hub SOAP service
        /// after cleaning up old connections
        /// </summary>
        /// <returns>The connection string required for subsequent calls</returns>
        private string SoapConnect()
        {
            _soapService.CleanupConnections();
            return _soapService.Connect("localhost", _username, _password);
        }

        /// <summary>
        /// Helper method that stores Forward and Return resources
        /// as they are linked to the SLA in the hub
        /// Thin information is currently hardcoded as it cannot be readily
        /// gotten from the hub
        /// </summary>
        /// <param name="slaName">The name of the hub SLA</param>
        /// <returns>An array of strings, first the Forward resource, second the Return resource</returns>
        private string[] GetResources(string slaName)
        {
            string[] resources = new string[2];

            if (slaName == "CM-500kb_500kb" || slaName == "SP-XLNK-4CPM-512_256")
            {
                resources[0] = "CB-fwd-cpm";
                resources[1] = "4cpm-Tesr";
            }
            else if (slaName == "MIR_1024-1024_CIR_256-256")
            {
                resources[0] = "";
                resources[1] = "";
            }
            else if (slaName == "SP-10Mx10M_MxDMA" || slaName == "SP-NI-MSB-SATADSL-001" || slaName == "SP-NI-MSB-SATADSL-TEST3")
            {
                resources[0] = "TB-MSB-SAT2-NI-COM";
                resources[1] = "test-mxdma-TB-root-pool";
            }
            else
            {
                resources[0] = "TB-SATADSL-FW";
                resources[1] = "TB-SATADSL-RTN";
            }

            return resources;
        }

        /// <summary>
        /// Set the correct IP address for use in the network innovation hub.
        /// The hub expects the first USABLE IP address of the range, ie not the network address
        /// </summary>
        /// <param name="cmtIP">The IP address as stored in the CMT and used in the NMS</param>
        /// <param name="ipMask">The IP mask</param>
        /// <returns>The IP address to be used in the hub</returns>
        private string SetIPAddressForHub(string cmtIP, int ipMask)
        {
            string hubIP = cmtIP;
            if (ipMask < 32)
            {
                string[] numbers = cmtIP.Split('.');
                int numberFour = Convert.ToInt32(numbers[3]) + 1;
                numbers[3] = numberFour.ToString();

                hubIP = numbers[0];
                for (int i = 1; i <= 3; i++)
                {
                    hubIP = hubIP + "." + numbers[i];
                }
            }

            return hubIP;
        }
    }
}