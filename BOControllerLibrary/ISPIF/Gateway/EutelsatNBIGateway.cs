﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Data.SqlClient;
using BOControllerLibrary.Controllers;
using BOControllerLibrary.Controllers.Interfaces;
using BOControllerLibrary.Model.Terminal;
using BOControllerLibrary.Model.Log;
using BOControllerLibrary.ISPIF.Model;
using BOControllerLibrary.Model;

// Eutelsat webservices
using ti = BOControllerLibrary.eutelsat.NBI.TerminalService;
using ipps = BOControllerLibrary.eutelsat.NBI.Ipv4AddressPoolService;
using perf = BOControllerLibrary.eutelsat.NBI.PerformanceInterface;

//NMS webservices
using nms = BOControllerLibrary.NMSGatewayServiceRef;
using BOControllerLibrary.Model.Organization;

namespace BOControllerLibrary.ISPIF.Gateway
{
    /// <summary>
    /// This controller provides access to the Eutelsat NBI
    /// It is used for creating and managing terminals on the Eutelsat hub
    /// The Eutelsat interface handles the provisioning of terminals, but the main
    /// SLA and volume management is done by the CG NMS. Therefor, this interface is 
    /// an extenstion of the NMS Gateway and overrides certain hub-specific methods
    /// of the NMS Gateway
    /// </summary>
    public class EutelsatNBIGateway : NMSGateway
    {
        private const string DEF_SLA_ES = "DefSla16";
        private const string ISP_ID_ES = "EutelsatIsp";
        private string DEF_BLADE_ES = "EutelsatBlade";
        private string _returnCapacityGroup = "ESRtnCapacityGroupId";
        private string _forwardPoolId = "ESFwdPoolId";
        private ti.TerminalService ts;
        private ipps.Ipv4AddressPoolService ip;
        private perf.PerformanceServiceImplService ps;
        private NetworkCredential esCred;
        private IBOConfigurationController _configurationController;
        private IBOAccountingController _accountingController;
        private IBOLogController _logController;
        private string _connectionString;
        private string _databaseProvider;
        private int _eutIspId = 16;
        
        /// <summary>
        /// Default constructor
        /// </summary>
        public EutelsatNBIGateway(int ispId) : base("DefSla16", "EutelsatIsp")
        {
            _connectionString = ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
            _databaseProvider = ConfigurationManager.ConnectionStrings["ApplicationServices"].ProviderName;
            _accountingController = new BOAccountingController(_connectionString, _databaseProvider);
            _configurationController = new BOConfigurationController(_connectionString, _databaseProvider);
            _logController = new BOLogController(_connectionString, _databaseProvider);

            //Set the Eutelsat credentials
            string esUsername = "EutelsatUsername";
            string esPassword = "EutelsatPassword";
            if (ispId > 699 && ispId < 800)
            {
                esUsername = "EutelsatUsernameE70B";
                esPassword = "EutelsatPasswordE70B";
                DEF_BLADE_ES = "EutelsatBladeE70B";
                _returnCapacityGroup = "ESRtnCapGroupIdE70B";
                _forwardPoolId = "ESFwdPoolIdE70B";
                _eutIspId = 411;
            }
            if (ispId > 899 && ispId <= 999)
            {
                esUsername = "EutelsatUNE70BMUC";
                esPassword = "EutelsatPWE70BMUC";
                DEF_BLADE_ES = "EutelsatBladeMUC";
                _returnCapacityGroup = "ESRtnCapGroupIdMUC";
                _forwardPoolId = "ESFwdPoolIdMUC";
                _eutIspId = 461;
            }
            esCred = new NetworkCredential(_configurationController.getParameter(esUsername).Trim(), _configurationController.getParameter(esPassword).Trim());
            ip = new ipps.Ipv4AddressPoolService();
            ip.Credentials = esCred;
            ts = new ti.TerminalService();
            ts.Credentials = esCred;
            ps = new perf.PerformanceServiceImplService();
            ps.Credentials = esCred;
            //ps.Url = "https://172.21.3.20/nbi/performance/2.0/PerformanceInterface";
            
        }
        
        /// <summary>
        /// Creates a terminal in the Eutelsat Hub by means of the ISPIF gateway. 
        /// This method sets the NMSActivate flag which indicates that the 
        /// TicketMaster still needs to create the terminal in the NMS.
        /// </summary>
        /// <param name="endUserId">The end user identifier related to the terminal</param>
        /// <param name="macAddress">The terminal's MAC address</param>
        /// <param name="slaId">
        /// The Service Level identifier. For this version of the IHubGateway interface
        /// the slaId is retrieved from the configuration table since the sla is NOT the sla for the 
        /// activation in the NMS!
        /// </param>
        /// <param name="ispId">The ISP id</param>
        /// <returns>true if the operation succeeds</returns>
        public override bool TerminalActivate(string endUserId, string macAddress, long slaId, int ispId, Guid userId)
        {
            bool result = false;
            string failureReason = "";
            string failureStackTrace = "";
            // The Eutelsat terminal parameters object
            ti.TerminalParameters esTerm = new ti.TerminalParameters();

            // The CMT terminal
            Terminal cmtTerm = _accountingController.GetTerminalDetailsByMAC(macAddress);
            ServiceLevel cmtSLA = _accountingController.GetServicePack((int)cmtTerm.SlaId);

            // Set the necessary terminal parameters
            esTerm.bladeId = _configurationController.getParameter(DEF_BLADE_ES).Trim();
            esTerm.macAddress = macAddress;
            esTerm.name = cmtTerm.ExtFullName.Replace(" ", "-");
            esTerm.ispId = cmtSLA.EdgeISP.ToString();
            esTerm.slaId = cmtSLA.EdgeSLA.ToString();
            esTerm.returnCapacityGroupId = _configurationController.getParameter(_returnCapacityGroup).Trim();
            esTerm.forwardPoolId = _configurationController.getParameter(_forwardPoolId).Trim();
            esTerm.qosClassificationProfileId = _configurationController.getParameter("ESQoSClassification").Trim();
            esTerm.terminalNetworkConfig = new ti.TerminalProvisioningNetworkConfig();
            esTerm.terminalNetworkConfig.nativeNetworkConfig = new ti.NativeNetworkConfig();
            
            // Get available IP address from the Eutelsat hub
            try
            {
                ServicePointManager.ServerCertificateValidationCallback = delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; }; //Handle inconsistent SSL certificates of hub
                ipps.Ipv4AddressPool[] ipPools = ip.getIpv4AddressPoolsByServiceProvider(Convert.ToInt32(esTerm.ispId), true);
                bool breaker = false;
                bool exhausted = true;
                //Loop through IP pools to find the first available IP address. Break the loops when an address has been found
                foreach (ipps.Ipv4AddressPool ipPool in ipPools)
                {
                    //if (ipPool.id == "31")
                    //{
                        foreach (ipps.Ipv4AddressRange ipRange in ipPool.ipAddressRanges)
                        {
                            if (!ipRange.exhausted)
                            {
                                foreach (ipps.ipRange freeRange in ipRange.freeRanges)
                                {
                                    esTerm.terminalNetworkConfig.nativeNetworkConfig.cpeAddress = freeRange.firstIpAddress;
                                    esTerm.terminalNetworkConfig.nativeNetworkConfig.ipv4AddressPoolId = ipPool.id;
                                    breaker = true;
                                    exhausted = false;
                                    break;
                                }
                            }

                            if (breaker)
                            {
                                break;
                            }
                        }
                    //}

                    if (breaker)
                    {
                        break;
                    }
                }

                if (exhausted) //Exhausted means no IP address available in hub and that the terminal cannot be activated
                {
                    SystemException ex = new SystemException("The IP address pool on the Eutelsat hub is exhausted");
                    this.logException(ex, "EutelsatNBIGateway.TerminalActivate");
                    failureReason = ex.Message;
                    failureStackTrace = ex.StackTrace;
                }
            }
            catch (Exception ex)
            {
                this.logException(ex, "EutelsatNBIGateway.TerminalActivate");
                failureReason = ex.Message;
                failureStackTrace = ex.StackTrace;
            }

            if (failureReason == "")
            {
                //Generate a Sit ID
                esTerm.sitId = this.generatetSitId().ToString();

                try
                {
                    ServicePointManager.ServerCertificateValidationCallback = delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; }; //Handle inconsistent SSL certificates of hub
                    //Create the terminal on the Eutelsat hub
                    ti.ProvisioningRequest provReq = ts.createTerminal(esTerm);

                    if (provReq.action == "CREATE")
                    {
                        result = true;
                        cmtTerm.Blade = Convert.ToInt32(esTerm.bladeId);
                        cmtTerm.IpAddress = esTerm.terminalNetworkConfig.nativeNetworkConfig.cpeAddress.Trim();
                        cmtTerm.SitId = Convert.ToInt32(esTerm.sitId);
                        cmtTerm.SlaId = (int)slaId;
                        try
                        {
                            _accountingController.UpdateTerminal(cmtTerm);
                        }
                        catch (Exception ex)
                        {
                            this.logException(ex, "EutelsatNBIGateway.TerminalActivate");
                            failureReason = ex.Message;
                            failureStackTrace = ex.StackTrace;
                        }
                    }
                }
                catch (Exception ex)
                {
                    this.logException(ex, "EutelsatNBIGateway.TerminalActivate");
                    failureReason = ex.Message;
                    failureStackTrace = ex.StackTrace;
                }
            }
            this.insertRequestTicketWithSla(!result, macAddress, AdmStatus.Request, AdmStatus.OPERATIONAL, failureReason, failureStackTrace, TerminalActivityTypes.Activation, ispId, (int)slaId, userId);
            return result;
        }

        /// <summary>
        /// Changes the SLA of the terminal in the Eutelsat hub. To change the SLA in the NMS call the
        /// NMSTerminalChangeSLA method.
        /// </summary>
        /// <param name="endUserId">The endUserId of the terminal to activate</param>
        /// <param name="macAddress">The MAC address of the terminal to activate</param>
        /// <param name="newSlaId">The service pack allocated to the terminal</param>
        /// <param name="currentState">The current admin state of the terminal</param>
        /// <returns>true if the application succeeded</returns>
        public override bool TerminalChangeSla(string endUserId, string macAddress, int newSlaId, Model.AdmStatus currentState, int ispId, Guid userId)
        {
            bool result = false;
            string failureReason = "";
            string failureStackTrace = "";
            //Get the terminal details form the CMT
            Terminal cmtTerm = _accountingController.GetTerminalDetailsByMAC(macAddress);
            ServiceLevel cmtOldSLA = _accountingController.GetServicePack((int)cmtTerm.SlaId);
            ServiceLevel cmtNewSLA = _accountingController.GetServicePack(newSlaId);
            ti.Terminal esTerm = new ti.Terminal();

            //Get the terminal details from the Eutelsat hub
            try
            {
                ServicePointManager.ServerCertificateValidationCallback = delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; }; //Handle inconsistent SSL certificates of hub
                esTerm = ts.findTerminal(cmtOldSLA.EdgeISP, true, cmtTerm.SitId, true);
            }
            catch (Exception ex)
            {
                this.logException(ex, "EutelsatNBIGateway.TerminalChangeSla");
                failureReason = ex.Message;
                failureStackTrace = ex.StackTrace;
            }

            if (failureReason == "")
            {
                //Create the terminal parameters for the Eutelsat hub request
                ti.TerminalProvisioningParameters termPar = new ti.TerminalProvisioningParameters();

                termPar.sitId = esTerm.terminalId;
                termPar.ispId = cmtOldSLA.EdgeISP.ToString();
                termPar.slaId = cmtNewSLA.EdgeSLA.ToString();
                termPar.terminalNetworkConfig = esTerm.terminalNetworkConfig; //The network configuration must be the same, or else it will be overwritten with empty values

                //Write the changes to the hub
                try
                {
                    ServicePointManager.ServerCertificateValidationCallback = delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; }; //Handle inconsistent SSL certificates of hub
                    ti.ProvisioningRequest provReq = ts.updateTerminal(termPar);
                    if (provReq.action == "UPDATE")
                    {
                        result = true;
                    }
                }
                catch (Exception ex)
                {
                    this.logException(ex, "EutelsatNBIGateway.TerminalChangeSla");
                    failureReason = ex.Message;
                    failureStackTrace = ex.StackTrace;
                }
            }
            this.insertRequestTicketWithSla(!result, macAddress, currentState, currentState, failureReason, failureStackTrace, TerminalActivityTypes.ChangeSla, ispId, newSlaId, userId);
            return result;
        }

        /// <summary>
        /// Decommissions a terminal in the Eutelsat hub. 
        /// The terminal is effectively removed from the Hub database
        /// </summary>
        /// <param name="endUserId">The endUserId of the terminal to activate</param>
        /// <param name="macAddress">The MAC address of the terminal to decommission</param>
        /// <returns>true if the activation succeeded</returns>
        public override bool TerminalDecommission(string endUserId, string macAddress, int ispId, Guid userId)
        {
            bool result = false;
            string failureReason = "";
            string failureStackTrace = "";
            //Get the terminal details form the CMT
            Terminal cmtTerm = _accountingController.GetTerminalDetailsByMAC(macAddress);
            ServiceLevel cmtSLA = _accountingController.GetServicePack((int)cmtTerm.SlaId);

            try
            {
                //Delete the terminal in the hub
                ServicePointManager.ServerCertificateValidationCallback = delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; }; //Handle inconsistent SSL certificates of hub
                ti.ProvisioningRequest provReq = ts.deleteTerminal(cmtSLA.EdgeISP, true, cmtTerm.SitId, true);
                if (provReq.action == "DELETE")
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                this.logException(ex, "EutelsatNBIGateway.TerminalDecommission");
                failureReason = ex.Message;
                failureStackTrace = ex.StackTrace;
            }
            this.insertRequestTicket(!result, macAddress, AdmStatus.OPERATIONAL, AdmStatus.DECOMMISSIONED, failureReason, failureStackTrace, TerminalActivityTypes.Decommissioning, ispId, userId);
            return result;
        }

        /// <summary>
        /// Reactivates a terminal in the Eutelsat hub
        /// </summary>
        /// <param name="endUserId"></param>
        /// <param name="macAddress"></param>
        /// <param name="ispId"></param>
        /// <returns></returns>
        public override bool TerminalReActivate(string endUserId, string macAddress, int ispId, Guid userId)
        {
            bool result = false;
            string failureReason = "";
            string failureStackTrace = "";
            //Get the terminal details form the CMT
            Terminal cmtTerm = _accountingController.GetTerminalDetailsByMAC(macAddress);
            ServiceLevel cmtSLA = _accountingController.GetServicePack((int)cmtTerm.SlaId);

            try
            {
                //unlock the terminal in the hub
                ServicePointManager.ServerCertificateValidationCallback = delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; }; //Handle inconsistent SSL certificates of hub
                ti.ProvisioningRequest provReq = ts.unlockTerminal(cmtSLA.EdgeISP, true, cmtTerm.SitId, true);
                if (provReq.action == "UNLOCK")
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                this.logException(ex, "EutelsatNBIGateway.TerminalReActivate");
                failureReason = ex.Message;
                failureStackTrace = ex.StackTrace;
            }
            this.insertRequestTicket(!result, macAddress, AdmStatus.LOCKED, AdmStatus.OPERATIONAL, failureReason, failureStackTrace, TerminalActivityTypes.ReActivation, ispId, userId);
            return result;
        }

        /// <summary>
        /// Suspends a terminal, the status of the terminal becomes LOCKED
        /// </summary>
        /// <param name="endUserId">The endUserId of the terminal to activate</param>
        /// <param name="macAddress">The MAC address of the terminal to activate</param>
        /// <returns>true if the re-activation succeeded</returns>
        public override bool TerminalSuspend(string endUserId, string macAddress, int ispId, Guid userId)
        {
            bool result = false;
            string failureReason = "";
            string failureStackTrace = "";
            //Get the terminal details form the CMT
            Terminal cmtTerm = _accountingController.GetTerminalDetailsByMAC(macAddress);
            ServiceLevel cmtSLA = _accountingController.GetServicePack((int)cmtTerm.SlaId);

            try
            {
                //Lock the terminal in the hub
                ServicePointManager.ServerCertificateValidationCallback = delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; }; //Handle inconsistent SSL certificates of hub
                ti.ProvisioningRequest provReq = ts.lockTerminal(cmtSLA.EdgeISP, true, cmtTerm.SitId, true);
                if (provReq.action == "LOCK")
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                this.logException(ex, "EutelsatNBIGateway.TerminalSuspend");
                failureReason = ex.Message;
                failureStackTrace = ex.StackTrace;
            }
            this.insertRequestTicket(!result, macAddress, AdmStatus.OPERATIONAL, AdmStatus.LOCKED, failureReason, failureStackTrace, TerminalActivityTypes.Suspension, ispId, userId);
            return result;
        }

        /// <summary>
        /// Changes the MacAddress in the Eutelsat Hub
        /// </summary>
        /// <param name="endUserId"></param>
        /// <param name="oriMacAddress"></param>
        /// <param name="newMacAddress"></param>
        /// <param name="ispId"></param>
        /// <returns></returns>
        public override bool ChangeMacAddress(string endUserId, string oriMacAddress, string newMacAddress, int ispId, Guid userId)
        {
            bool result = false;
            string failureReason = "";
            string failureStackTrace = "";
            //Get the terminal details form the CMT
            Terminal cmtTerm = _accountingController.GetTerminalDetailsByMAC(oriMacAddress);
            ServiceLevel cmtSLA = _accountingController.GetServicePack((int)cmtTerm.SlaId);
            ti.Terminal esTerm = new ti.Terminal();

            //Get the terminal details from the Eutelsat hub
            try
            {
                ServicePointManager.ServerCertificateValidationCallback = delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; }; //Handle inconsistent SSL certificates of hub
                esTerm = ts.findTerminal(cmtSLA.EdgeISP, true, cmtTerm.SitId, true);
            }
            catch (Exception ex)
            {
                this.logException(ex, "EutelsatNBIGateway.ChangeMacAddress");
                failureReason = ex.Message;
                failureStackTrace = ex.StackTrace;
            }

            if (failureReason == "")
            {
                //Create the terminal parameters for the Eutelsat hub request
                ti.TerminalProvisioningParameters termPar = new ti.TerminalProvisioningParameters();

                termPar.sitId = esTerm.terminalId;
                termPar.ispId = cmtSLA.EdgeISP.ToString();
                termPar.macAddress = newMacAddress;
                termPar.terminalNetworkConfig = esTerm.terminalNetworkConfig; //The network configuration must be the same, or else it will be overwritten with empty values

                //Write the changes to the hub
                try
                {
                    ServicePointManager.ServerCertificateValidationCallback = delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; }; //Handle inconsistent SSL certificates of hub
                    ti.ProvisioningRequest provReq = ts.updateTerminal(termPar);
                    if (provReq.action == "UPDATE")
                    {
                        result = true;
                    }
                }
                catch (Exception ex)
                {
                    this.logException(ex, "EutelsatNBIGateway.ChangeMacAddress");
                    failureReason = ex.Message;
                    failureStackTrace = ex.StackTrace;
                }
            }
            this.insertRequestTicketWithMac(!result, oriMacAddress, AdmStatus.OPERATIONAL, AdmStatus.OPERATIONAL, failureReason, failureStackTrace, TerminalActivityTypes.ChangeMac, ispId, newMacAddress, userId);
            return result;
        }

        /// <summary>
        /// This method retrieves the terminal information from the CMT Database
        /// </summary>
        /// <param name="macAddress">The terminal MAC address</param>
        /// <param name="ispId">The ISP identifier, this value is not really used for this implementation</param>
        /// <returns>A completed TerminalInfo object</returns>
        public override TerminalInfo getTerminalDetailsFromHub(string macAddress, string ispId)
        {
            TerminalInfo ti = new TerminalInfo();
            Terminal term = _accountingController.GetTerminalDetailsByMAC(macAddress);

            //Copy the terminal data to a terminal information object
            try
            {
                if (term.Blade != null)
                    ti.BladeId = term.Blade.ToString();

                if (term.AdmStatus != null)
                    ti.Blocked = (term.AdmStatus == 2);
                if (term.FullName != null)
                    ti.EndUserId = term.FullName;

                if (term.IpAddress != null)
                    ti.IPAddress = term.IpAddress;

                ti.IspId = term.IspId.ToString();

                if (term.MacAddress != null)
                    ti.MacAddress = term.MacAddress;

                ti.SitId = term.SitId.ToString();

                if (term.SlaId != null)
                    ti.SlaId = term.SlaId.ToString();

                if (term.SlaName != null)
                    ti.SlaName = term.SlaName;

                if (term.AdmStatus != null)
                    ti.Status = term.AdmStatus.ToString();
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDateTime = DateTime.Now;
                cmtEx.ExceptionDesc = ex.Message;
                cmtEx.ExceptionLevel = (short)ExceptionLevelEnum.Error;
                cmtEx.ExceptionStacktrace = ex.StackTrace;
                cmtEx.StateInformation = "Error";
                cmtEx.UserDescription = "Copy from Terminal to TerminalInfo object failed.";
                _logController.LogApplicationException(cmtEx);
            }
            
            return ti;
        }

        /// <summary>
        /// This method retrieves the terminal information from the SES Hub
        /// This override method is used for MAC changes
        /// </summary>
        /// <param name="oldMacAddress">The old terminal MAC address. This MAC address is still in use in the CMT at this point</param>
        /// <param name="newMacAddress">The new terminal MAC address. This MAC address is already being used in the hub</param>
        /// <param name="ispId">The ISP identifier, not used in this case</param>
        /// <returns>A completed TerminalInfo object</returns>
        public override TerminalInfo getTerminalDetailsFromHubMacChange(string oldMacAddress, string newMacAddress, string ispId)
        {
            return this.getTerminalDetailsFromHub(oldMacAddress, ispId);
        }

        /// <summary>
        /// Returns the C/No value. This value is retrieved from the Eutelsat NMS
        /// </summary>
        /// <param name="ispId">The unique ISP identifier</param>
        /// <param name="sitId">The site id</param>
        /// <returns>The C/No value as a string</returns>
        public override string getRTNValue(string ispId, string sitId)
        {
            string receivedResponse = null;
            try
            {
                ServicePointManager.ServerCertificateValidationCallback = delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; }; //Handle inconsistent SSL certificates of hub
                Terminal cmtTerm = _accountingController.GetTerminalDetailsBySitId(Convert.ToInt32(sitId), Convert.ToInt32(ispId));
                ServiceLevel sla = _accountingController.GetServicePack((int)cmtTerm.SlaId);
                ti.Terminal esTerm = new ti.Terminal();
                esTerm = ts.findTerminal(sla.EdgeISP, true, cmtTerm.SitId, true);
                perf.MetricDataQuery query = new perf.MetricDataQuery();
                query.attributeCriteria = new perf.AttributeQueryCriterion[1];
                perf.AttributeQueryCriterion criterion = new perf.AttributeQueryCriterion();
                criterion.granularities = new string[] { "PT5M" };
                criterion.attribute = "returnCNoAverage";
                query.attributeCriteria[0] = criterion;
                query.lastMetricOnly = true;
                query.domain = "newtec";
                query.type = "sit";
                query.resourceKeys = new string[] { "ispId=" + _eutIspId + ",sitId=" + cmtTerm.SitId.ToString() };
                perf.AttributeMetricData[] amv = ps.findMetricData(query);
                receivedResponse = amv[0].granularityMetricDatas[0].dsv.data;
                string[] response = amv[0].granularityMetricDatas[0].dsv.data.Split(';');
                double rss;
                if (Double.TryParse(response[response.Length - 1], out rss) == true)
                    return response[response.Length - 1];
                else
                    return "-1.0";
            }
            catch (IndexOutOfRangeException ex)
            {
                Exception e = new Exception("Bad response received: " + receivedResponse, ex);
                this.logException(e, "EutelsatNBIGateway: getRTNValue");
                return "-1.0";
            }
            catch (Exception ex)
            {
                this.logException(ex, "EutelsatNBIGateway.getRTNValue: " + receivedResponse); 
                return "-1.0";
            }
        }

        /// <summary>
        /// Returns the Es/No value. This value is retrieved from the Eutelsat NMS
        /// </summary>
        /// <param name="ispId">The unique ISP identifier</param>
        /// <param name="sitId">The site id</param>
        /// <returns>The Es/No value as a string</returns>
        public override string getFWDValue(string ispId, string sitId)
        {
            try
            {
                ServicePointManager.ServerCertificateValidationCallback = delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; }; //Handle inconsistent SSL certificates of hub
                Terminal cmtTerm = _accountingController.GetTerminalDetailsBySitId(Convert.ToInt32(sitId), Convert.ToInt32(ispId));
                ServiceLevel sla = _accountingController.GetServicePack((int)cmtTerm.SlaId);
                ti.Terminal esTerm = new ti.Terminal();
                esTerm = ts.findTerminal(sla.EdgeISP, true, cmtTerm.SitId, true);
                perf.MetricDataQuery query = new perf.MetricDataQuery();
                query.attributeCriteria = new perf.AttributeQueryCriterion[1];
                perf.AttributeQueryCriterion criterion = new perf.AttributeQueryCriterion();
                criterion.attribute = "forwardEsNoAverage";
                criterion.granularities = new string[] { "PT5M" };
                query.attributeCriteria[0] = criterion;
                query.lastMetricOnly = true;
                query.domain = "newtec";
                query.type = "sit";
                query.resourceKeys = new string[] { "ispId=" + _eutIspId + ",sitId=" + cmtTerm.SitId.ToString() };
                perf.AttributeMetricData[] amv = ps.findMetricData(query);
                string[] response = amv[0].granularityMetricDatas[0].dsv.data.Split(';');

                return response[response.Length - 1];
            }
            catch (IndexOutOfRangeException ex)
            {
                Exception e = new Exception("Bad response received.", ex);
                this.logException(e, "EutelsatNBIGateway: getFWDValue");
                return "-1.0";
            }
            catch (Exception ex)
            {
                this.logException(ex, "EutelsatNBIGateway.getFWDValue");
                return "-1.0";
            }
        }

        /// <summary>
        /// Activates a terminal in the NMS.
        /// </summary>
        /// <param name="endUserId">The end user identifier</param>
        /// <param name="macAddress">The terminal MAC address</param>
        /// <param name="slaId">The SLA identifier</param>
        /// <param name="ispId">The ISP idnetifier</param>
        /// <param name="ipMask">IP range specified by means of a MASK</param>
        /// <returns>True if the method succeeds</returns>
        public override bool NMSTerminalActivateWithRange(string endUserId, string macAddress, long slaId, int ispId, int ipMask, int freeZone, Guid userId)
        {
            bool result = false;
            nms.NMSGatewayService nmsService = new nms.NMSGatewayService();
            Terminal term = _accountingController.GetTerminalDetailsByMAC(macAddress);


            try
            {
                TerminalInfo ti = this.getTerminalDetailsFromHub(macAddress, ispId.ToString());
                nms.RequestTicket rt;
                if (term.OrgId != null)
                {
                    Organization organization = _accountingController.GetOrganization((int)term.OrgId);
                    if (organization.VNO != null && (bool)organization.VNO)
                    {
                        rt = nmsService.createRegistrationWithVNO(endUserId, macAddress, ipMask, (int)slaId, ispId, ti.IPAddress, Int32.Parse(ti.SitId), freeZone, organization.FullName);
                    }
                    else
                    {
                        rt = nmsService.createRegistration(endUserId, macAddress, ipMask, (int)slaId, ispId, ti.IPAddress, Int32.Parse(ti.SitId), freeZone);
                    }
                }
                //Need to specify an IP Address as this address is not generated by the NMS but by the Eutelsat HUB
                else
                {
                    rt = nmsService.createRegistration(endUserId, macAddress, ipMask, (int)slaId, ispId, ti.IPAddress, Int32.Parse(ti.SitId), freeZone);
                }

                result = this.insertRequestTicketWithSla(rt, macAddress, AdmStatus.LOCKED, AdmStatus.OPERATIONAL, TerminalActivityTypes.Activation, ispId, (int)slaId, userId);
            }
            catch (Exception ex)
            {
                this.logException(ex, "EutelsatNBIGateway.TerminalActivate");
            }
            return result;
        }

        /// <summary>
        /// Activates a terminal in the NMS without specifying a range by means of
        /// an IP Mask value.
        /// </summary>
        /// <remarks>
        /// This method activates a terimal in the NMS without specifiying a range, thus the
        /// IP Mask is always set to /32 which means that only one IP address can be specified.
        /// </remarks>
        /// <param name="endUserId">The end user identifier</param>
        /// <param name="macAddress">The terminal MAC address</param>
        /// <param name="slaId">The SLA identifier</param>
        /// <param name="ispId">The ISP identifier</param>
        /// <returns>True if the method succeeds</returns>
        public override bool NMSTerminalActivate(string endUserId, string macAddress, long slaId, int ispId, int freeZone, Guid userId)
        {
            nms.NMSGatewayService nmsService = new nms.NMSGatewayService(); 
            bool result = false;

            try
            {
                //Need to specify an IP Address as this address is not generated by the NMS but by the Eutelsat HUB
                TerminalInfo ti = this.getTerminalDetailsFromHub(macAddress, ispId.ToString());
                nms.RequestTicket rt = nmsService.createRegistration(endUserId, macAddress, 32, (int)slaId, ispId, ti.IPAddress, Int32.Parse(ti.SitId), freeZone);
                result = base.insertRequestTicket(rt, macAddress, AdmStatus.Request, AdmStatus.OPERATIONAL, TerminalActivityTypes.Activation, ispId, userId);
            }
            catch (Exception ex)
            {
                this.logException(ex, "EutelsatNBIGateway.TerminalActivate");
            }
            return result;
        }

        /// <summary>
        /// This method allows to update the freezone information for a terminal
        /// </summary>
        /// <param name="FreeZoneId">The identifier of the freezone allocated to the terminal</param>
        /// <param name="macAddress">The MAC address of the terminal</param>
        /// <param name="FreeZoneFlag">If this flag is set to true, a freezone option is allocated to the terminal. In
        /// case this value is false, the terminal does not have the freezone option</param>
        /// <returns></returns>
        public bool NMSModifyFreeZoneForTerminal(int FreeZoneId, string macAddress, Boolean FreeZoneFlag)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Inserts a new ticket in the CMT database and flags it as an NMS ticket
        /// </summary>
        /// <remarks>
        /// Use this version of the insertRequestTicket method to insert a request ticket for activities
        /// on the Eutelsat hub in the database
        /// </remarks>
        /// <param name="failed">Has status true if the request on the Eutelsat hub failed</param>
        /// <param name="macAddress">The MacAddress of the target</param>
        /// <param name="sourceAdmStatus">The Source admin status</param>
        /// <param name="targetAdmStatus">The Target admin status</param>
        /// <param name="termActivityType">The terminal activity type</param>
        /// <param name="ispId">The ISP ID</param>
        /// <returns>True if the operation succeeded</returns>
        private bool insertRequestTicket(bool failed, string macAddress, AdmStatus sourceAdmStatus, AdmStatus targetAdmStatus, string failureReason, string failureStackTrace,
                                           TerminalActivityTypes termActivityType, int ispId, Guid userId)
        {
            TicketDataMgr ticketDataMgr = new TicketDataMgr();
            Guid ticketId = Guid.NewGuid();
            if (failureReason.Length > 400)
            {
                failureReason = failureReason.Substring(0, 400);
            }
            int ticketStatus = 0;
            if (failed)
            {
                ticketStatus = 1;
            }
            return ticketDataMgr.insertTicket(ticketId.ToString(), macAddress, sourceAdmStatus, targetAdmStatus, ticketStatus, failureReason, failureStackTrace, termActivityType, ispId, 1, userId);
        }

        /// <summary>
        /// Inserts a ticket  for an SLA change
        /// </summary>
        /// <remarks>
        /// Use this version of the insertRequestTicket method to insert a request ticket for activities
        /// on the Eutelsat hub in the database
        /// </remarks>
        /// <param name="failed">Has status true if the request on the Eutelsat hub failed</param>
        /// <param name="macAddress">The Mac Address</param>
        /// <param name="sourceAdmStatus">The source amdin status</param>
        /// <param name="targetAdmStatus">The target admin status</param>
        /// <param name="termActivityType">The terminal activity type</param>
        /// <param name="ispId">The ISP identifier</param>
        /// <param name="newSla">The new sla</param>
        /// <returns>True if the method succeeded</returns>
        private bool insertRequestTicketWithSla(bool failed, string macAddress, AdmStatus sourceAdmStatus, AdmStatus targetAdmStatus, string failureReason, string failureStackTrace,
                                           TerminalActivityTypes termActivityType, int ispId, int newSla, Guid userId)
        {
            TicketDataMgr ticketDataMgr = new TicketDataMgr();
            Guid ticketId = Guid.NewGuid();
            if (failureReason.Length > 400)
            {
                failureReason = failureReason.Substring(0, 400);
            } 
            int ticketStatus = 0;
            if (failed)
            {
                ticketStatus = 1;
            }
            return ticketDataMgr.insertTicket(ticketId.ToString(), macAddress, sourceAdmStatus, targetAdmStatus, ticketStatus, failureReason, failureStackTrace, termActivityType, ispId, 1, newSla, userId);
        }

        /// <summary>
        /// Inserts a ticket for a MAC Address change
        /// </summary>
        /// <remarks>
        /// Use this version of the insertRequestTicket method to insert a request ticket for activities
        /// on the Eutelsat hub in the database
        /// </remarks>
        /// <param name="failed">Has status true if the request on the Eutelsat hub failed</param>
        /// <param name="macAddress">The MAC address of the terminal</param>
        /// <param name="sourceAdmStatus">The sourde administration status</param>
        /// <param name="targetAdmStatus">The target administration status</param>
        /// <param name="termActivityType">The terminal activity type</param>
        /// <param name="ispId">The ISP identifier</param>
        /// <param name="NewMacAddress">The new mac address</param>
        /// <returns></returns>
        private bool insertRequestTicketWithMac(bool failed, string macAddress, AdmStatus sourceAdmStatus, AdmStatus targetAdmStatus, string failureReason, string failureStackTrace,
                                           TerminalActivityTypes termActivityType, int ispId, string NewMacAddress, Guid userId)
        {
            TicketDataMgr ticketDataMgr = new TicketDataMgr();
            Guid ticketId = Guid.NewGuid();
            if (failureReason.Length > 400)
            {
                failureReason = failureReason.Substring(0, 400);
            }
            int ticketStatus = 0;
            if (failed)
            {
                ticketStatus = 1;
            } 
            return ticketDataMgr.insertTicket(ticketId.ToString(), macAddress, sourceAdmStatus, targetAdmStatus, ticketStatus, failureReason, failureStackTrace, termActivityType, ispId, 1, NewMacAddress, userId);
        }

        /// <summary>
        /// Returns the status of a request ticket.
        /// </summary>
        /// <remarks>
        /// This method gets the ticket from the CMT database 
        /// </remarks>
        /// <param name="requestTicketId">The requestticket identfier</param>
        /// <param name="ispId">The ISP identfier</param>
        /// <returns>
        /// The corresponding CMT request ticket or null if the ticket could not 
        ///  be found
        ///  </returns>
        public override CmtRequestTicket LookupRequestTicket(string requestTicketId, int ispId)
        {
            CmtRequestTicket cmtRt = null;
            try
            {
                CMTTicket cmtTick = _logController.GetTicketById(requestTicketId);
                cmtRt = new CmtRequestTicket();
                cmtRt.id = cmtTick.TicketId;
                cmtRt.failureReason = cmtTick.FailureReason;
                cmtRt.failureStackTrace = cmtTick.FailureStackTrace;

                if (cmtTick.TicketStatus == 0)
                {
                    cmtRt.requestStatus = CmtRequestStatus.SUCCESSFUL;
                }

                if (cmtTick.TicketStatus == 1)
                {
                    cmtRt.requestStatus = CmtRequestStatus.FAILED;
                }

                if (cmtTick.TicketStatus == 2)
                {
                    cmtRt.requestStatus = CmtRequestStatus.SUCCESSFUL;
                }
            }
            catch (Exception ex)
            {
                this.logException(ex, "EutelsatNBIGateway.LookupRequestTicket");
                cmtRt = null;
            }

            return cmtRt;
        }

        #region Helper Methods

        /// <summary>
        /// A helper method for finding the highest Sit ID currently in the CMT db
        /// for a specific ISP
        /// </summary>
        /// <returns>The current highest Sit ID for the ISP</returns>
        private int generatetSitId()
        {

            int sitId;
            Random rand = new Random();
            
            const string queryCmd = "SELECT * FROM Terminals " +
                                    "WHERE SitId=@sitID";
            while (true)
            {
                sitId = rand.Next(4000000, 4194304);
                try
                {
                    using (var conn = new SqlConnection(_connectionString))
                    {
                        conn.Open();
                        using (var cmd = new SqlCommand(queryCmd, conn))
                        {
                            cmd.Parameters.AddWithValue("@sitID", sitId);
                            SqlDataReader reader = cmd.ExecuteReader();

                            if (!reader.Read())
                             return sitId;
                            
                        }
                    }
                }
                catch (Exception ex)
                {
                    this.logException(ex, "EutelsatNBIGateway.generatetSitId");
                    throw;
                }
            }
            
        }

        #endregion
   
    }
}
