﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BOControllerLibrary.ISPIF.Gateway
{
    /// <summary>
    /// This NMS Adapter supports ISP 416. The SES ISP related is identical to the base NMSGateway adapter but
    /// the default SLA on the SES NMS is 41217100
    /// </summary>
    public class NMSGateway_416 : NMSGateway
    {
        public NMSGateway_416()
            : base("DefSla416", "SESIsp")
        {
        }
    }
}
