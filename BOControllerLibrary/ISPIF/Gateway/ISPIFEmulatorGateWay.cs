﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using BOControllerLibrary.IspSupportInterfaceServiceRef;
using BOControllerLibrary.Model;
using BOControllerLibrary.Controllers;
using BOControllerLibrary.Controllers.Interfaces;
using BOControllerLibrary.ISPIF.Model;
using BOControllerLibrary.ISPIF.Gateway.Interface;

namespace BOControllerLibrary.ISPIF.Gateway
{
    /// <summary>
    /// This class represents the ISPIF Emulator. When 
    /// </summary>
    public class ISPIFEmulatorGateWay : SatADSLHubController, IHubGateWay
    {
        private IspSupportInterfaceService _ispEmulatorService = null;
        private IBOLogController _controller;

        /// <summary>
        /// Default method
        /// </summary>
        public ISPIFEmulatorGateWay()
        {
            _ispEmulatorService = new IspSupportInterfaceService();
            _controller = new BOLogController(
                connectionString: ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString,
                databaseProvider: ConfigurationManager.ConnectionStrings["ApplicationServices"].ProviderName);
        }

        /// <summary>
        /// Creates a new registration and returns a ticket which is used by the
        /// TicketMaster service to verify the activation result.
        /// </summary>
        /// <remarks>
        /// The calling method MUST create a new Ticket row in the CMTData ticket table!
        /// </remarks>
        /// <param name="endUserId"></param>
        /// <param name="macAddress"></param>
        /// <param name="slaId"></param>
        /// <returns>The ticket allocated to this request</returns>
        public bool TerminalActivate(string endUserId, string macAddress, long slaId, int ispId, Guid userId)
        {
            bool result = false;
            try
            {
                RequestTicket rt = _ispEmulatorService.createRegistration(endUserId, macAddress, slaId.ToString(), ispId.ToString());
                result = this.insertRequestTicket(rt, macAddress, AdmStatus.LOCKED, AdmStatus.OPERATIONAL, TerminalActivityTypes.Activation, ispId, userId);
            }
            catch (Exception ex)
            {
                this.logException(ex, "ISPIFEmulatorGateWay.TerminalActivate");
            }
            return result;
        }

        /// <summary>
        /// Re-activates by changing the state from Locked to Operational
        /// </summary>
        /// <param name="endUserId"></param>
        /// <returns>The ticket allocated to this request</returns>
        public bool TerminalReActivate(string endUserId, string macAddress, int ispId, Guid userId)
        {
            bool result = false;
            try
            {
                RequestTicket rt = _ispEmulatorService.changeStatus(endUserId, RegistrationStatus.OPERATIONAL, ispId.ToString());
                result = this.insertRequestTicket(rt, macAddress, AdmStatus.LOCKED, AdmStatus.OPERATIONAL, TerminalActivityTypes.ReActivation, ispId, userId);
            }
            catch (Exception ex)
            {
                this.logException(ex, "ISPIFEmulatorGateWay.TerminalReActivate");
            }
            return result;
        }

        /// <summary>
        /// Suspends a terminal by changing the terminal state from OPERATIONAL to LOCKED
        /// </summary>
        /// <param name="endUserId"></param>
        /// <returns>The ticket allocated to this request</returns>
        public bool TerminalSuspend(string endUserId, string macAddress, int ispId, Guid userId)
        {
            bool result = false;
            try
            {
                RequestTicket rt = _ispEmulatorService.changeStatus(endUserId, RegistrationStatus.LOCKED, ispId.ToString());
                result = this.insertRequestTicket(rt, macAddress, AdmStatus.OPERATIONAL, AdmStatus.LOCKED, TerminalActivityTypes.Suspension, ispId, userId);
            }
            catch (Exception ex)
            {
                this.logException(ex, "ISPIFEmulatorGateWay.TerminalSuspend");
            }
            return result;
        }

        /// <summary>
        /// Decommissions a terminal by removing the registration for the endUserId from the terminal
        /// reference by its macAddress
        /// </summary>
        /// <param name="endUserId"></param>
        /// <param name="macAddress"></param>
        /// <returns>The ticket allocated to this request</returns>
        public bool TerminalDecommission(string endUserId, string macAddress, int ispId, Guid userId)
        {
            bool result = false;
            try
            {
                RequestTicket rt = _ispEmulatorService.removeRegistration(endUserId, macAddress);
                result = this.insertRequestTicket(rt, macAddress, AdmStatus.OPERATIONAL, AdmStatus.DECOMMISSIONED, TerminalActivityTypes.Decommissioning, ispId, userId);
            }
            catch (Exception ex)
            {
                this.logException(ex, "ISPIFEmulatorGateWay.TerminalDecommission");
            }
            return result;
        }

        /// <summary>
        /// Reset the accumulated volume used by the user (FUP Reset)
        /// </summary>
        /// <param name="sitId"></param>
        /// <param name="ispId"></param>
        public bool TerminalUserReset(string endUserId, int ispId)
        {
            bool result = false;
            try
            {
                _ispEmulatorService.resetUser(endUserId, ispId);
                result = true;
            }
            catch (Exception ex)
            {
                this.logException(ex, "ISPIFEmulatorGateWay.TerminalUserReset");
            }

            return result;
        }

        /// <summary>
        /// Changes the Sla towards the specified value
        /// </summary>
        /// <param name="endUserId"></param>
        /// <param name="macAddress"></param>
        /// <param name="newSlaId"></param>
        /// <param name="currentState"></param>
        /// <returns></returns>
        public bool TerminalChangeSla(string endUserId, string macAddress, int newSlaId, AdmStatus currentState, int ispId, Guid userId)
        {
            bool result = false;
            try
            {
                RequestTicket rt = _ispEmulatorService.changeSla(endUserId, newSlaId.ToString(), ispId.ToString());
                result = this.insertRequestTicketWithSla(rt, macAddress, currentState, currentState, TerminalActivityTypes.ChangeSla, ispId, newSlaId, userId);
            }
            catch (Exception ex)
            {
                this.logException(ex, "ISPIFEmulatorGateWay.TerminalChangeSla");
            }
            return result;
        }

        /// <summary>
        /// Preprovision a terminal
        /// </summary>
        /// <param name="macAddress"></param>
        /// <returns></returns>
        public bool PreProvisionTerminal(string macAddress)
        {
            return _ispEmulatorService.preProvisionTerminal(macAddress);
        }

        /// <summary>
        /// Calls the ISPIF Emulator lookupRequestTicket method 
        /// </summary>
        /// <remarks>
        /// For this implementation, the ispId value is not used
        /// </remarks>
        /// <param name="requestTicketId"></param>
        /// <returns></returns>
        public CmtRequestTicket LookupRequestTicket(string requestTicketId, int ispId)
        {
            CmtRequestTicket cmtRt = null;
            RequestTicket rt = _ispEmulatorService.lookupRequestTicket(requestTicketId);

            if (rt != null)
            {
                cmtRt = new CmtRequestTicket();
                cmtRt.id = rt.id;
                cmtRt.failureReason = rt.failureReason;
                cmtRt.failureStackTrace = rt.failureStackTrace;
                if (rt.requestStatus == RequestStatus.BUSY)
                {
                    cmtRt.requestStatus = CmtRequestStatus.BUSY;
                }

                if (rt.requestStatus == RequestStatus.FAILED)
                {
                    cmtRt.requestStatus = CmtRequestStatus.FAILED;
                }

                if (rt.requestStatus == RequestStatus.SUCCESSFUL)
                {
                    cmtRt.requestStatus = CmtRequestStatus.SUCCESSFUL;
                }
            }

            return cmtRt;
        }

        /// <summary>
        /// Add volume to a subscription.
        /// </summary>
        /// <remarks>
        /// The subscription must have a voucher based SLA
        /// </remarks>
        /// <param name="sitId"></param>
        /// <param name="volumeCode"></param>
        /// <param name="ispId"></param>
        /// <returns>true if the operationa succeeded</returns>
        public bool AddVolume(int sitId, int volumeCode, int ispId)
        {
            ISPIFCommon ispifCommon = new ISPIFCommon();
            return ispifCommon.AddVolume(sitId, volumeCode, ispId);
        }


        /// <summary>
        /// Changes the ISP of a terminal
        /// </summary>
        /// <param name="macAddress">The terminals MAC address</param>
        /// <param name="oldISP">The old ISP</param>
        /// <param name="newISP">The new iSP</param>
        /// <returns>True if the operation succeeded</returns>
        public bool ChangeISP(string endUserId, int oldISP, int newISP)
        {
            ISPIFCommon ispifCommon = new ISPIFCommon();
            return ispifCommon.ChangeISP(endUserId, oldISP, newISP);
        }

        /// <summary>
        /// Changes the MacAddress of a terminal
        /// </summary>
        /// <param name="endUserId"></param>
        /// <param name="newMacAddress"></param>
        /// <param name="ispId"></param>
        /// <returns></returns>
        public bool ChangeMacAddress(string endUserId, string oriMacAddress, string newMacAddress, int ispId, Guid userId)
        {
            bool result = false;
            try
            {
                RequestTicket rt = _ispEmulatorService.changeTerminal(endUserId, newMacAddress);
                result = this.insertRequestTicketWithMac(rt, oriMacAddress, AdmStatus.OPERATIONAL, AdmStatus.OPERATIONAL, TerminalActivityTypes.ChangeMac, ispId, newMacAddress, userId);
          
            }
            catch (Exception ex)
            {
                this.logException(ex, "ISPIFEmulatorGateWay.ChangeMacAddress failed for: " + endUserId);
            }
            return result;

        }

        /// <summary>
        /// Inserts a new ticket in the CMT database
        /// </summary>
        /// <param name="rt"></param>
        /// <param name="macAddress"></param>
        /// <param name="sourceAdmStatus"></param>
        /// <param name="targetAdmStatus"></param>
        /// <param name="termActivityType"></param>
        /// <returns></returns>
        private bool insertRequestTicket(RequestTicket rt, string macAddress, AdmStatus sourceAdmStatus, AdmStatus targetAdmStatus,
                                           TerminalActivityTypes termActivityType, int ispId, Guid userId)
        {
            TicketDataMgr ticketDataMgr = new TicketDataMgr();
            //RequestStatus reqStatus = 
            //        (RequestStatus)Enum.ToObject(typeof(RequestStatus), (int)rt.requestStatus);
            return ticketDataMgr.insertTicket(rt.id, macAddress, sourceAdmStatus, targetAdmStatus,
                                            (int)rt.requestStatus, rt.failureReason, rt.failureStackTrace, termActivityType, ispId, 0, userId);
        }

        /// <summary>
        /// Inserts a ticket with for an SLA change
        /// </summary>
        /// <param name="rt"></param>
        /// <param name="macAddress"></param>
        /// <param name="sourceAdmStatus"></param>
        /// <param name="targetAdmStatus"></param>
        /// <param name="termActivityType"></param>
        /// <param name="ispId"></param>
        /// <param name="newSla"></param>
        /// <returns></returns>
        private bool insertRequestTicketWithSla(RequestTicket rt, string macAddress, AdmStatus sourceAdmStatus, AdmStatus targetAdmStatus,
                                           TerminalActivityTypes termActivityType, int ispId, int newSla, Guid userId)
        {
            TicketDataMgr ticketDataMgr = new TicketDataMgr();
            //RequestStatus reqStatus =
            //        (RequestStatus)Enum.ToObject(typeof(RequestStatus), (int)rt.requestStatus);
            return ticketDataMgr.insertTicket(rt.id, macAddress, sourceAdmStatus, targetAdmStatus,
                                            (int)rt.requestStatus, rt.failureReason, rt.failureStackTrace, termActivityType, ispId, 0, newSla, userId);
        }

        /// <summary>
        /// Inserts a ticket for a MAC Address change
        /// </summary>
        /// <param name="rt"></param>
        /// <param name="macAddress"></param>
        /// <param name="sourceAdmStatus"></param>
        /// <param name="targetAdmStatus"></param>
        /// <param name="termActivityType"></param>
        /// <param name="ispId"></param>
        /// <param name="NewMacAddress"></param>
        /// <returns></returns>
        private bool insertRequestTicketWithMac(RequestTicket rt, string macAddress, AdmStatus sourceAdmStatus, AdmStatus targetAdmStatus,
                                           TerminalActivityTypes termActivityType, int ispId, string NewMacAddress, Guid userId)
        {
            TicketDataMgr ticketDataMgr = new TicketDataMgr();
            //RequestStatus reqStatus =
            //        (RequestStatus)Enum.ToObject(typeof(RequestStatus), (int)rt.requestStatus);
            return ticketDataMgr.insertTicket(rt.id, macAddress, sourceAdmStatus, targetAdmStatus,
                                            (int)rt.requestStatus, rt.failureReason, rt.failureStackTrace, termActivityType, ispId, 0, NewMacAddress, userId);
        }
        
        private void logException(Exception ex, string method)
        {
            CmtApplicationException cmtApplicationException = new CmtApplicationException();
            cmtApplicationException.ExceptionDateTime = DateTime.Now;
            cmtApplicationException.ExceptionDesc = ex.Message;
            cmtApplicationException.ExceptionStacktrace = "";
            cmtApplicationException.UserDescription = method;
            _controller.LogApplicationException(cmtApplicationException);
        }

        #region NMS Support methods

        /*
         * For this HubGateway implementation these can be kept empty.
         */
        /// <summary>
        /// Creates a new multicast group
        /// </summary>
        /// <param name="macAddress">The multicast MAC address, derived from the IP address</param>
        /// <param name="ipAddress">The multicast group IP address</param>
        /// <param name="multiCastName">The multicast group name</param>
        /// <param name="sourceUrl">The source URL</param>
        /// <param name="portNr">The port number</param>
        /// <param name="bandWidth">The bandwidth</param>
        /// <param name="ispId">The ISP Id this group belongs to</param>
        /// <returns>True if the operation succeeded</returns>
        public bool CreateMulticastGroup(string macAddress, string ipAddress, string multiCastName, string sourceUrl, int portNr, long bandWidth, int ispId, Guid userId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Removes an IP address from a Multicast group
        /// </summary>
        /// <param name="ipAddress">The IP Address of the Multicast group to remove</param>
        /// <returns>True if the operation succeeded</returns>
        public bool RemoveIPFromMulticast(string ipAddress)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Returns the transferred bytes for the Multicast group specified by the IP Address
        /// </summary>
        /// <param name="ipAddress">The IP Address</param>
        /// <returns>The transferred bytes as a long</returns>
        public ulong GetTransferredBytes(string ipAddress)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Returns true if the Multicast group identified by the IP Address is enabled
        /// </summary>
        /// <param name="ipAddress">The IP Address which identifies the MC group</param>
        /// <returns>True if the MC is enabled</returns>
        public bool IsEnabled(string ipAddress)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Enables a MulticastGroup
        /// </summary>
        /// <param name="ipAddress">IP Address of the Multicast group to enable</param>
        /// <returns>true if the command was accepted by the NMS</returns>
        public bool EnableMultiCastGroup(string ipAddress, string macAddress, int ispId, Guid userId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Disables a Multicast group
        /// </summary>
        /// <param name="ipAddress">IP Address of the Multicast group to disable</param>
        /// <returns>true if the command was accepted by the NMS</returns>
        public bool DisableMultiCastGroup(string ipAddress, string macAddress, int ispId, Guid userId)
        {
            throw new NotImplementedException();
        }

        public bool NMSTerminalActivateWithRange(string endUserId, string macAddress, long slaId, int ispId, int ipMask, int freeZone, Guid userId)
        {
            throw new NotImplementedException();
        }

        public bool NMSTerminalActivate(string endUserId, string macAddress, long slaId, int ispId, int freeZone, Guid userId)
        {
            throw new NotImplementedException();
        }

        public bool NMSTerminalChangeSla(string endUserId, string macAddress, int newSlaId, AdmStatus currentState, int ispId, Guid userId)
        {
            throw new NotImplementedException();
        }

        public bool NMSTerminalDecommission(string endUserId, string macAddress, int ispId, Guid userId)
        {
            throw new NotImplementedException();
        }

        public bool NMSTerminalReActivate(string endUserId, string macAddress, int ispId, Guid userId)
        {
            throw new NotImplementedException();
        }

        public bool NMSTerminalSuspend(string endUserId, string macAddress, int ispId, Guid userId)
        {
            throw new NotImplementedException();
        }

        public bool NMSTerminalUserReset(string endUserId, int ispId)
        {
            throw new NotImplementedException();
        }

        public CmtRequestTicket NMSLookupRequestTicket(string requestTicketId, int ispId)
        {
            throw new NotImplementedException();
        }

        public bool NMSAddVolume(int sitId, int volumeCode, int ispId)
        {
            throw new NotImplementedException();
        }

        public bool NMSChangeMacAddress(string endUserId, string oriMacAddress, string newMacAddress, int ispId, Guid userId)
        {
            throw new NotImplementedException();
        }

        public bool NMSChangeISP(string endUserId, int oldISP, int newISP)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method allows to update the freezone information for a terminal
        /// </summary>
        /// <param name="FreeZoneId">The identifier of the freezone allocated to the terminal</param>
        /// <param name="macAddress">The MAC address of the terminal</param>
        /// <param name="FreeZoneFlag">If this flag is set to true, a freezone option is allocated to the terminal. In
        /// case this value is false, the terminal does not have the freezone option</param>
        /// <returns></returns>
        public bool NMSModifyFreeZoneForTerminal(string endUserId, int FreeZoneId)
        {
            throw new NotImplementedException();
        }

        public bool NMSCreateServicePack(BOControllerLibrary.NMSGatewayServiceRef.ServicePack servicePack)
        {
            return true;
        }

        public bool NMSUpdateServicePack(BOControllerLibrary.NMSGatewayServiceRef.ServicePack servicePack)
        {
            return true;
        }

        public bool NMSDeleteServicePack(int slaId)
        {
            return true;
        }

        /// <summary>
        /// Checks if the SLA (which eventually needs to be deleted) has associated terminals
        /// </summary>
        /// <param name="slaId">The identifier of the SLA</param>
        /// <returns>True when the SLA has still associated terminals</returns>
        public bool NMSSlaHasAssociatedTerminals(int slaId) 
        {
            throw new NotImplementedException();
        }

        public bool NMScreateFreeZone(NMSGatewayServiceRef.FreeZone freeZone)
        {
            throw new NotImplementedException();
        }

        public bool NMSupdateFreeZone(NMSGatewayServiceRef.FreeZone freeZone)
        {
            throw new NotImplementedException();
        }

        public List<NMSGatewayServiceRef.FreeZone> NMSgetFreeZones()
        {
            throw new NotImplementedException();
        }

        public NMSGatewayServiceRef.FreeZone NMSgetFreeZone(int freeZoneId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method allows to change the IP Address AND Mask in the SatADSL NMS
        /// </summary>
        /// <remarks>
        /// The old and new IP Masks may be identical while the old and new IP Addresses
        /// can never be identical.
        /// </remarks>
        /// <param name="endUserId">The unique end user Id</param>
        /// <param name="macAddress">The MAC address of the terminal</param>
        /// <param name="oldIPAddress">The original IP Address</param>
        /// <param name="oldIPMask">The original IP Mask</param>
        /// <param name="newIPAddress">The new IP Address</param>
        /// <param name="newIPMask">The new IP Mask</param>
        /// <param name="ispId">The ISP Identifier</param>
        /// <returns>True if the change was registered by NMS successfully</returns>
        public bool NMSChangeIPAddress(string endUserId, string macAddress, string oldIPAddress, int oldIPMask,
                                                    string newIPAddress, int newIPMask, int ispId, Guid userId)
        {
            throw new NotImplementedException();
        }
        #endregion



        public bool NMSTerminalChangeWeight(string endUserId, string macAddress, int newWeight, int customerId, AdmStatus currentState, int ispId, Guid userId)
        {
            return true;
        }


        public virtual TerminalInfo getTerminalDetailsFromHubMacChange(string oldMacAddress, string newMacAddress, string ispId)
        {
            throw new NotImplementedException();
        }


        public bool NMSActivateSatelliteDiversityTerminal(string endUserId, string macAddress, string ipAddress, long slaId, int ispId, int freeZone, string primaryMac, string secondaryMac, Guid userId)
        {
            throw new NotImplementedException();
        }
    }
}