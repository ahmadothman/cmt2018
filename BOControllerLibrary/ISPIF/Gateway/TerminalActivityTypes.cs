﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BOControllerLibrary.ISPIF.Gateway
{
    /// <summary>
    /// The activities which can be performed on a terminal. These elements correspond to the 
    /// values in the TerminalActivity table.
    /// </summary>
    public enum TerminalActivityTypes
    {
        Activation = 100, Suspension = 200, ReActivation = 300, Decommissioning = 400, Deletion = 500, ChangeSla = 600, ChangeEdgeSla = 601, FUPReset = 700, ChangeMac = 900, EnableStream = 1000, DisableStream = 1100, IPAddressChange = 1002, ChangeFreezone=1003, ChangeWeight = 1200
    }
}
