﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BOControllerLibrary.ISPIF;
using BOControllerLibrary.com.astra2connect.ispif;

namespace BOControllerLibrary.ISPIF.Model
{
    /// <summary>
    /// Represents one single Return and Forward data point.
    /// </summary>
    /// <remarks>
    /// This class is used as a transport object to move chart data from the 
    /// back-end server to the front-end.
    /// </remarks>
    public class TrafficDataPoint
    {
        PmtDataPoint _rtnDataPoint;

        /// <summary>
        /// Represents a return data point for 24 hours
        /// </summary>
        public PmtDataPoint RtnDataPoint
        {
            get
            {
                return _rtnDataPoint;
            }

            set
            {
                _rtnDataPoint = value;
            }
        }

        PmtDataPoint _fwdDataPoint;

        /// <summary>
        /// Forward volume datapoint for 24 hours
        /// </summary>
        public PmtDataPoint FwdDataPoint
        {
            get
            {
                return _fwdDataPoint;
            }

            set
            {
                _fwdDataPoint = value;
            }
        }

        PmtDataPoint _rtnDataPointCum;

        /// <summary>
        /// Return volume datapoint cummulative for 30 days
        /// </summary>
        public PmtDataPoint RtnDataPointCum
        {
            get
            {
                return _rtnDataPointCum;
            }

            set
            {
                _rtnDataPointCum = value;
            }
        }


        PmtDataPoint _fwdDataPointCum;

        /// <summary>
        /// Forward volume datapoint cummulative for 30 days
        /// </summary>
        public PmtDataPoint FwdDataPointRTN
        {
            get
            {
                return _fwdDataPointCum;
            }

            set
            {
                _fwdDataPointCum = value;
            }
        }
    }
}