﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BOControllerLibrary.ISPIF.Model
{
    /// <summary>
    /// Holds information about a ServicePack (SLA)
    /// </summary>
    public class ServicePackInfo
    {
        private int _slaId;

        /// <summary>
        /// The Sla identifier.
        /// </summary>
        /// <remarks>
        /// This is an integer value
        /// </remarks>
        public int SlaID
        {
            get
            {
                return _slaId;
            }

            set
            {
                _slaId = value;
            }
        }

        private string _slaName;

        /// <summary>
        /// The common name for the service pack
        /// </summary>
        public string SlaName
        {
            get
            {
                return _slaName;
            }

            set
            {
                _slaName = value;
            }
        }

        private decimal _fupThreshold;

        /// <summary>
        /// The file upload threshold value
        /// </summary>
        public decimal FUPThreshold
        {
            get
            {
                return _fupThreshold;
            }

            set
            {
                _fupThreshold = value;
            }
        }

        /// <summary>
        /// The return threshold value
        /// </summary>
        public decimal RTNFUPThreshold { get; set; }

        private string _dataRateAboveFUP;

        /// <summary>
        /// The data rate allocate when the FUP threshold is reached.
        /// </summary>
        public string DataRateAboveFUP
        {
            get
            {
                return _dataRateAboveFUP;
            }

            set
            {
                _dataRateAboveFUP = value;
            }
        }
    }
}