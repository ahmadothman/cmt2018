﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BOControllerLibrary.ISPIF.Model
{
    public class Isp
    {
        public string id { get; set; }
        public string name { get; set; }
        public Sla[] sla { get; set; }
    } 
}
