﻿namespace BOControllerLibrary.ISPIF.Model.Old
{
    public class Terminal
    {
        public string id { get; set; }
        public string macAddress { get; set; }
        public string ppIspId { get; set; }
        public string ppSitId { get; set; }
        public string ppSlaId { get; set; }
        public string endUserId { get; set; }
        public string regIspId { get; set; }
    }
}
