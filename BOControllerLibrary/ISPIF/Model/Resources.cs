﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BOControllerLibrary.ISPIF.Model
{
    public class Resources
    {
        public string me { get; set; }
        public bool isAdmin { get; set; }
        public Isp[] isp { get; set; }
        public Isp parent { get; set; }
    } 
}
