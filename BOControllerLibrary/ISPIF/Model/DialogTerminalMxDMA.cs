﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace BOControllerLibrary.ISPIF.Model
{
    /// <summary>
    /// This class extends the DialogTerminal class for MxDMA terminals
    /// </summary>
    class DialogTerminalMxDMA : DialogTerminal
    {
        public HrcMxDmaSettings hrcMxDmaSettings = new HrcMxDmaSettings();
        public LineUpSettings lineUpSettings = new LineUpSettings();
    }
}
