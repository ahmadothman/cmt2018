﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BOControllerLibrary.ISPIF.Model
{
    /// <summary>
    /// The TerminalInfo class contains all data related to a specific terminal.
    /// </summary>
    /// <remarks>
    /// TerminalInfo is mainly a container or tansport object, used to transfer terminal data between the BO
    /// and FO applications. The different properties are filled by means of the ISPIF get methods. This object
    /// is very similar to the Registration entity as defined by the ISPIF WSDL but comobnes registration
    /// information with volume information.
    /// </remarks>
    public class TerminalInfo
    {
        /// <summary>
        /// Internal Gateway system identifier for this terminal.
        /// </summary>
        private string _id;

        /// <summary>
        /// Gets or sets the internal gateway system identifier.
        /// </summary>
        /// <remarks>
        /// This value is normally not used by FO applications.
        /// </remarks>
        public string Id
        {
            get
            {
                return _id;
            }

            set
            {
                _id = value;
            }
        }

        /// <summary>
        /// Terminal's hardware identifier, used in IP protocols.
        /// </summary>
        private string _macAddress;

        /// <summary>
        /// Sets and gets the MacAddress, the terminal's hardware identifier.
        /// </summary>
        /// <remarks>
        /// A Mac Address is unique for a device and has the following format (example):
        /// 00:09:47:81:fb:f0 (6 hex numbers)
        /// </remarks>
        public string MacAddress
        {
            get
            {
                return _macAddress;
            }

            set
            {
                _macAddress = value;
            }
        }

        /// <summary>
        /// The end-user id who owns this terminal.
        /// </summary>
        /// <remarks>
        /// This is supposed to be a unique identifier for an end user with a maximum of 50 characters.
        /// To date however, the end user id is a concatenation of a number of fields including the 
        /// Distributor name, the End User name and the device serial numer.
        /// </remarks>
        private string _endUserId;

        /// <summary>
        /// Sets and gets the endUserId value. 
        /// </summary>
        public string EndUserId
        {
            get
            {
                return _endUserId;
            }

            set
            {
                _endUserId = value;
            }
        }


        /// <summary>
        /// NewTec A2C identifier for the ISP.
        /// </summary>
        private string _ispId;

        /// <summary>
        /// Sets or gets the ISP identifier.
        /// </summary>
        public string IspId
        {
            get
            {
                return _ispId;
            }

            set
            {
                _ispId = value;
            }
        }

        /// <summary>
        /// NewTec A2C identifier for the ISP, useful in the NewTec A2C Dashboard.
        /// </summary>
        private string _sitId;

        /// <summary>
        /// Sets and gets the A2C identifier.
        /// </summary>
        public string SitId
        {
            get
            {
                return _sitId;
            }

            set
            {
                _sitId = value;
            }
        }

        /// <summary>
        /// ISPIF Gateway identifier for the Service Level Agreement provided to the end user.
        /// </summary>
        /// <remarks>
        /// This value is a String which determines the SLA.
        /// </remarks>
        private string _slaId;

        /// <summary>
        /// Sets and gets the SLA identifier.
        /// </summary>
        public string SlaId
        {
            get
            {
                return _slaId;
            }

            set
            {
                _slaId = value;
            }
        }

        /// <summary>
        /// A RegistrationStatus which can be OPERATIONAL or LOCKED.
        /// </summary>
        private string _status;

        /// <summary>
        /// Sets and gets the status value.
        /// </summary>
        public String Status
        {
            get
            {
                return _status;
            }

            set
            {
                _status = value;
            }
        }

        /// <summary>
        /// How much forward volume overall did the user consume during this period (daytime and freezone).
        /// </summary>
        private string _consumed;

        /// <summary>
        /// Sets and gets the consumed value.
        /// </summary>
        public string Consumed
        {
            get
            {
                return _consumed;
            }

            set
            {
                _consumed = value;
            }
        }

        /// <summary>
        /// How much return volume overall did the user consume during this period (daytime and freezone).
        /// </summary>
        private string _consumedReturn;

        /// <summary>
        /// Sets and gets the ConsumedReturn value.
        /// </summary>
        /// <remarks>
        /// This value contains the overall return volume consumed by the user during this period (daytime and freezone).
        /// </remarks>
        public string ConsumedReturn
        {
            get
            {
                return _consumedReturn;
            }

            set
            {
                _consumedReturn = value;
            }
        }

        /// <summary>
        /// How much forward volume overall did the user consume during this period during the freezone.
        /// </summary>
        private string _freeZone;

        /// <summary>
        /// Gets and sets the forward freezone volume consumed.
        /// </summary>
        /// <remarks>
        /// This value indicates teh overall volume consumed for this period for this period.
        /// </remarks>
        public string FreeZone
        {
            get
            {
                return _freeZone;
            }

            set
            {
                _freeZone = value;
            }
        }

        /// <summary>
        /// How much volume did the user consume during this period during the freezone.
        /// </summary>
        private string _consumedFreeZone;

        /// <summary>
        /// Gets and sets the freezone return value consumed.
        /// </summary>
        /// <remarks>
        /// This value indicates the return volume consumed for the period during the freezone.
        /// </remarks>
        public string ConsumedFreeZone
        {
            get
            {
                return _consumedFreeZone;
            }

            set
            {
                _consumedFreeZone = value;
            }
        }

        /// <summary>
        /// How much additional forward volume did the user "buy" during this period.
        /// </summary>
        private string _additional;

        /// <summary>
        /// Gets and sets the additional volume.
        /// </summary>
        /// <return>
        /// Gets or sets the additional volume bought by the user during this volume.
        /// </return>
        public string Additional
        {
            get
            {
                return _additional;
            }

            set
            {
                _additional = value;
            }
        }

        /// <summary>
        /// How much additional volume did the user "buy" since he was created.
        /// </summary>
        private string _additionalReturn;

        /// <summary>
        /// Gets and sets the additional volume bought by the user.
        /// </summary>
        public string AdditionalReturn
        {
            get
            {
                return _additionalReturn;
            }

            set
            {
                _additionalReturn = value;
            }
        }

        /// <summary>
        /// How much additional forward volume did the user "buy" since he was created.
        /// </summary>
        private string _totalAdditional;

        /// <summary>
        /// Gets and sets the additional forward volume bought by the user.
        /// </summary>
        public string TotalAdditional
        {
            get
            {
                return _totalAdditional;
            }

            set
            {
                _totalAdditional = value;
            }
        }

        /// <summary>
        /// How much additional return volume did the user "buy" since he was created.
        /// </summary>
        public string _totalAdditionalReturn;

        /// <summary>
        /// Gets and sets teh additional volume bought b the user since he was created.
        /// </summary>
        public string TotalAdditionalReturn
        {
            get
            {
                return _totalAdditionalReturn;
            }

            set
            {
                _totalAdditionalReturn = value;
            }
        }

        /// <summary>
        /// The day of the month this user is invoiced.
        /// </summary>
        public string _invoiceDayOfMonth;

        /// <summary>
        /// Gets or sets the invoice day of month.
        /// </summary>
        public string InvoiceDayOfMonth
        {
            get
            {
                return _invoiceDayOfMonth;
            }

            set
            {
                _invoiceDayOfMonth = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private Boolean _blocked;

        /// <summary>
        /// Indicates whether the terminal is operational or blocked
        /// </summary>
        public Boolean Blocked
        {
            get
            {
                return _blocked;
            }

            set
            {
                _blocked = value;
            }
        }

        /// <summary>
        /// Set to true if an error occurred during the retrieval of the registration and volume data.
        /// </summary>
        /// <remarks>
        /// This flag is failsafe and MUST be set to false in case the operation was successfull.
        /// </remarks>
        private Boolean _errorFlag = true;

        /// <summary>
        /// Gets the error flag.
        /// </summary>
        public Boolean ErrorFlag
        {
            get
            {
                return _errorFlag;
            }

            set
            {
                _errorFlag = value;
            }
        }

        /// <summary>
        /// Provides more information about the error
        /// </summary>
        public string _errorMsg = "";

        /// <summary>
        /// Returns the error message as a string.
        /// </summary>
        /// <remarks>
        /// This value is set to an empty string if no error occured.
        /// </remarks>
        public string ErrorMsg
        {
            get
            {
                return _errorMsg;
            }
            set
            {
                _errorMsg = value;
            }
        }

        /// <summary>
        /// The terminal IP Address
        /// </summary>
        public string _ipAddress = "";

        /// <summary>
        /// The terminal IP Address
        /// </summary>
        public string IPAddress
        {
            get
            {
                return _ipAddress;
            }

            set
            {
                _ipAddress = value;
            }
        }

        public string _slaName;

        /// <summary>
        /// ServicePack (SLA) name
        /// </summary>
        public string SlaName
        {
            get
            {
                return _slaName;
            }

            set
            {
                _slaName = value;
            }
        }

        public decimal _fupThreshold;
        
        /// <summary>
        /// The File upload threshold value
        /// </summary>
        /// <remarks>
        /// The value is given in Gigabytes
        /// </remarks>
        public decimal FUPThreshold
        {
            get
            {
                return _fupThreshold;
            }

            set
            {
                _fupThreshold = value;
            }
        }

        /// <summary>
        /// The Return FUP Threshold value
        /// </summary>
        public decimal RTNFUPThreshold { get; set; }

        public string _drAboveFUP;

        /// <summary>
        /// Data rate available if the volume consumption is above FUP Threshold
        /// </summary>
        /// <remarks>
        /// This is a readable string
        /// </remarks>
        public string DRAboveFUP
        {
            get
            {
                return _drAboveFUP;
            }

            set
            {
                _drAboveFUP = value;
            }
        }

        public String _rtn; //Signal/Noise ration

        /// <summary>
        /// Gets and Sets the Signal/Noise ratio
        /// </summary>
        /// <remarks>
        /// This value is obtained from the getPMTData methods
        /// </remarks>
        public String RTN
        {
            get
            {
                return _rtn;
            }

            set
            {
                _rtn = value;
            }
        }

        public String _fwd; //Signal/Noise ration

        /// <summary>
        /// Gets and Sets the Signal/Noise ratio
        /// </summary>
        /// <remarks>
        /// This value is obtained from the getPMTData methods
        /// </remarks>
        public String FWD
        {
            get
            {
                return _fwd;
            }

            set
            {
                _fwd = value;
            }
        }

        /// <summary>
        /// The return pool value
        /// </summary>
        public String RTNPool { get; set; }

        /// <summary>
        /// The blade identifier
        /// </summary>
        public String BladeId { get; set; }

        /// <summary>
        /// Forward pool value
        /// </summary>
        public String FWPool { get; set; }

        // Added three maximum volumes to match the NMS terminal info class
        
        /// <summary>
        /// The maximum forward volume
        /// </summary>
        public float MaxVolFwd { get; set; }

        /// <summary>
        /// The maximum return volume
        /// </summary>
        public float MaxVolRtn { get; set; }

        /// <summary>
        /// The sum of the maximum forward and return volumes
        /// </summary>
        public float MaxVolSum { get; set; }

        /// <summary>
        /// Override of Equals method. This method compares two TerminalInfo
        /// instances for equality
        /// </summary>
        /// <param name="obj">An instance of a TerminalInfo class</param>
        /// <returns>true if the two instances are equals</returns>
        /// 

        //Terminal expiry date at NMS
        public DateTime ExpiryDate { get; set; }

        public override bool Equals(object obj)
        {
            TerminalInfo ti = (TerminalInfo) obj;
            return
                this._additional.Equals(ti.Additional) &&
                this._additionalReturn.Equals(ti.AdditionalReturn) &&
                this._blocked == ti.Blocked &&
                this._consumed.Equals(ti.Consumed) &&
                this._consumedFreeZone.Equals(ti.ConsumedFreeZone) &&
                this._consumedReturn.Equals(ti.ConsumedReturn) &&
                this._drAboveFUP.Equals(ti.DRAboveFUP) &&
                this._endUserId.Equals(ti.EndUserId) &&
                this._errorFlag == ti.ErrorFlag &&
                this._errorMsg.Equals(ti.ErrorMsg) &&
                this._freeZone.Equals(ti.FreeZone) &&
                this._fupThreshold.Equals(ti.FUPThreshold) &&
                this.RTNFUPThreshold.Equals(ti.RTNFUPThreshold) &&
                this._id.Equals(ti.Id) && 
                this._invoiceDayOfMonth.Equals(ti.InvoiceDayOfMonth) &&
                this._ipAddress.Equals(ti.IPAddress) &&
                this._ispId.Equals(ti.IspId) &&
                this._macAddress.Equals(ti.MacAddress) &&
                this._rtn.Equals(ti.RTN) &&
                this._fwd.Equals(ti.FWD) &&
                this._sitId.Equals(ti.SitId) &&
                this._slaId.Equals(ti.SlaId) &&
                this._slaName.Equals(ti.SlaName) &&
                this._status.Equals(ti.Status) &&
                this._totalAdditional.Equals(ti.TotalAdditional) &&
                this.TotalAdditionalReturn.Equals(ti.TotalAdditionalReturn);
                
        }

        /// <summary>
        /// Override of the GetHashCode method
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}