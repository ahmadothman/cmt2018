﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections.Specialized;
using System.Configuration;
using System.Data.SqlClient;

namespace BOControllerLibrary.ISPIF.Model
{
    /// <summary>
    /// Contains methods for handling ServicePack records
    /// </summary>
    public class ServicePackQuery
    {
        string _connectionString;
        string _servicePackFromSlaIdCmd =
            "SELECT SlaCommonName, FUPThreshold, DRAboveFUP, RTNFUPThreshold FROM dbo.ServicePacks WHERE SlaID =@SlaID";

        public ServicePackQuery()
        {
            //Fill-up the connection string
            _connectionString =
                ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
        }


        /// <summary>
        /// Returns a ServicePackInfo object corresonding to the given SLA ID
        /// </summary>
        /// <param name="slaId">The SLA ID as a Sting</param>
        /// <returns>A ServicePackInfo instance or null if the query failed</returns>
        public ServicePackInfo getServicePackBySlaId(string slaId)
        {
            //System.Diagnostics.Debugger.Break();
            ServicePackInfo si = new ServicePackInfo();
            try
            {
                using (SqlConnection con = new SqlConnection(_connectionString))
                {
                    int intSlaId = System.Convert.ToInt32(slaId);
                    SqlCommand cmd = new SqlCommand(_servicePackFromSlaIdCmd, con);
                    cmd.Parameters.AddWithValue("@SlaID", intSlaId);
                    con.Open();
                    SqlDataReader reader = cmd.ExecuteReader();

                    if (reader.HasRows)
                    {
                        reader.Read(); //Advance to first record
                        si.SlaID = intSlaId;
                        si.SlaName = (string)reader["SlaCommonName"];
                        si.FUPThreshold = (Decimal) reader["FUPThreshold"];
                        si.DataRateAboveFUP = (string)reader["DRAboveFUP"];
                        si.RTNFUPThreshold = (Decimal)reader["RTNFUPThreshold"];
                    }
                    else
                    {
                        throw new Exception("No data retrieved for SLAID: " + slaId);
                    }
                }
 
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
           
            return si;
        }
    }
}