﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BOControllerLibrary.ISPIF.Model
{
    /// <summary>
    /// This class extends the DialogTerminal class for SCPC terminals
    /// </summary>
    class DialogTerminalSCPC : DialogTerminal
    {
        public HrcScpcSettings hrcScpcSettings = new HrcScpcSettings();
    }
}
