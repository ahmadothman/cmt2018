﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BOControllerLibrary.ISPIF.Model
{
    /// <summary>
    /// This class extends the DialogTerminal class for MxDMA terminals
    /// specifically for use during the provisioning (line up) phase
    /// </summary>
    class DialogTerminalMxDMALineUp : DialogTerminal
    {
        public HrcMxDmaSettings hrcMxDmaSettings = new HrcMxDmaSettings();
        public LineUpSettings lineUpSettings = new LineUpSettings();
    }
}
