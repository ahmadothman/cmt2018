﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BOControllerLibrary.ISPIF.Model
{
    /// <summary>
    /// This class describes the basic Dialog Terminal object.
    /// The class is based on what is defined by the Dialog hub, but 
    /// is not an exact copy
    /// </summary>
    public class DialogTerminal
    {
        public networkId forwardClassificationProfileId = new networkId();
        public networkId forwardPoolId = new networkId();
        public bool locked { get; set; }
        public string macAddress { get; set; }
        public List<NetworkSettings> networkSettingses = new List<NetworkSettings>();
        public networkId returnClassificationProfileId = new networkId();
        public networkId satelliteNetworkId = new networkId();
        public networkId serviceProfileId = new networkId();
        public int softwareUpdateGroup { get; set; }
        public string type { get; set; }
        public string returnTechnology { get; set; }
        public string s2Settings { get; set; }
        public networkId returnPoolId = new networkId();
        public networkId id = new networkId();
        public bool debugMode { get; set; }
        public string description { get; set; }
        public bool encryptTraffic { get; set; }
        public string monitoringType { get; set; }
     }

    public class networkId
    {
        public string domainName { get; set; }
        public string name { get; set; }
    }

    public class LineUpSettings
    {
        public Int64 nominalBandwidthHz { get; set; }
        public float nominalOutputPowerDbm { get; set; }
        public float outputPower1DbCompression { get; set; }
        public double psd { get; set; }
    }

    public enum MonitoringType {Advanced, Standard}


    public class NetworkSettings
    {
        public string type { get; set; }
        public ipv4Settings ipv4 = new ipv4Settings();
        public networkId id = new networkId();
        public networkId networkId = new networkId();
        public int? vlanTag { get; set; }
    }

    public class ipv4Settings
    {
        public string address { get; set; }
        public string dhcpMode { get; set; }
        public bool lanAddressPingable { get; set; }
        public int prefixLength { get; set; }
        public networkId addressPoolId = new networkId();
        //public subnetRoute subnetRoutes = new subnetRoute();
    }

    public class subnetRoute
    {
        public string address { get; set; }
        public string gateway { get; set; }
        public int prefixLength { get; set; }
    }

    public class HrcScpcSettings
    {
        public Int64 centerFrequency { get; set; }
        public hrcScpcReturnCapacityGroup hrcScpcReturnCapacityGroupId = new hrcScpcReturnCapacityGroup();
        public Int64 symbolRate { get; set; }
    }

    public class hrcScpcReturnCapacityGroup
    {
        public string domainName { get; set; }
        public string name { get; set; }
        public networkId returnLinkId = new networkId();
    }

    public class HrcMxDmaSettings
    {
        public bool aupcEnabled { get; set; }
        public int aupcRange { get; set; }
        public networkId hrcMxDmaReturnCapacityGroupId = new networkId();
        public string modCod { get; set; }
        //public PowerSettings powerSettings = new PowerSettings();
        public networkId returnPoolId = new networkId();
    }

    public class HrcModCod
    {

    }

    public class PowerSettings
    {
        public float calculatedOutputPowerDbm { get; set; }
        public float outputPowerDbm { get; set; }
        public string powerControlMode { get; set; }
    }
}