﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BOControllerLibrary.ISPIF.Model
{
    /// <summary>
    /// Terminal admin states
    /// </summary>
    public enum AdmStatus
    {
        LOCKED = 1, OPERATIONAL = 2, DECOMMISSIONED = 3, Test = 4, Request = 5, Deleted = 6
    }
}
