﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BOControllerLibrary.ISPIF.Model;

namespace BOControllerLibrary.ISPIF.Model
{
    /// <summary>
    /// Provides caching feature to the TerminalInfo object.
    /// </summary>
    /// <remarks>
    /// The Data is cached into the CMT database
    /// </remarks>
    public class TerminalInfoCache
    {
        /// <summary>
        /// Checks if the terminal with the given MAC Address is cached.
        /// </summary>
        /// <param name="macAddress">Target MAC Address</param>
        /// <returns>true if the TerminalInfo object is cached</returns>
        public bool IsCached(string macAddress)
        {
            return false;
        }

        /// <summary>
        /// Pushes the TerminalInfo object to cache
        /// </summary>
        /// <param name="ti"></param>
        /// <returns></returns>
        public bool pushToCache(TerminalInfo ti)
        {
            return false;
        }

    }
}