﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BOControllerLibrary.ISPIF.Model;

namespace BOControllerLibrary.ISPIF.Model
{
    /// <summary>
    /// Enumeration which indicates the cache state
    /// </summary>
    public enum cacheStateEnum { NotCached, Cached, OutDated }

    /// <summary>
    /// Describes the functionality to be implemented by TerminalInfo Cache providers
    /// </summary>
    public interface ITICacheProvider
    {
        /// <summary>
        /// Checks if the terminal with the given macAddress is available from cache
        /// </summary>
        /// <param name="macAddress">The Terminal MAC Address</param>
        /// <returns>true if the TerminalInfo object is cached</returns>
        cacheStateEnum isCached(string macAddress);

        /// <summary>
        /// Pushes a TerminalInfo object to cache
        /// </summary>
        /// <remarks>
        /// If the TerminalInfo object already exists the entry is overwritten
        /// </remarks>
        /// <param name="ti">The TerminalInfo object to store</param>
        /// <returns>true if the operation succeeded</returns>
        bool pushToCache(TerminalInfo ti);

        /// <summary>
        /// Pops a TerminalInfo object from cache
        /// </summary>
        /// <param name="macAddress">The terminal MAC Address to retrieve from cache</param>
        /// <returns>The TerminalInfo object identified by the macAddress or null if
        /// the TerminalInfo object is not available from cache</returns>
        TerminalInfo popFromCache(string macAddress);

        /// <summary>
        /// Deletes the terminal identified by MAC Address from the chache
        /// </summary>
        /// <param name="macAddress">Terminal to delete</param>
        /// <returns>true if the method succeeded false otherwise</returns>
        Boolean deleteFromCache(string macAddress);
    }
}
