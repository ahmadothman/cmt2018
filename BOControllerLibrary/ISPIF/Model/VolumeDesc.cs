﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BOControllerLibrary.ISPIF.Model
{
    public class VolumeDesc
    {
        public string code { get; set; }
        public string addForward { get; set; }
        public string addReturn { get; set; }
    } 
}
