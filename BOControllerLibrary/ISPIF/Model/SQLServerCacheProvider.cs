﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using BOControllerLibrary.ISPIF.Model;
using BOControllerLibrary.Controllers;
using BOControllerLibrary.Model;

namespace BOControllerLibrary.ISPIF.Model
{
    public class SQLServerCacheProvider : ITICacheProvider
    {
        //The connection string
        string _connectionString;

        public SQLServerCacheProvider()
        {
            //Fill-up the connection string
            _connectionString =
                ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
        }

        #region ITICacheProvider Members

        /// <summary>
        /// Checks if the terminal with the given macAddress is available from cache
        /// </summary>
        /// <param name="macAddress">The Terminal MAC Address</param>
        /// <returns>true if the TerminalInfo object is cached</returns>
        public cacheStateEnum isCached(string macAddress)
        {
            cacheStateEnum cacheState = cacheStateEnum.NotCached;
            
            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand("cmtIsCached", conn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@MacAddress", SqlDbType.NChar, 20));
                cmd.Parameters["@MacAddress"].Value = macAddress;
                cmd.Parameters.Add(new SqlParameter("@CacheState", SqlDbType.Int));
                cmd.Parameters["@CacheState"].Direction = ParameterDirection.Output;
                conn.Open();
                try
                {
                    cmd.ExecuteNonQuery();

                    //Retrieve the CacheState
                    int state = (int)cmd.Parameters["@CacheState"].Value;

                    switch (state)
                    {
                        case 0:
                            cacheState = cacheStateEnum.NotCached;
                            break;
                        case 1:
                            cacheState = cacheStateEnum.Cached;
                            break;
                        case 2:
                            cacheState = cacheStateEnum.OutDated;
                            break;
                        default:
                            cacheState = cacheStateEnum.NotCached;
                            break;
                    }
                }
                catch (Exception ex)
                {
                    cacheState = cacheStateEnum.NotCached;
                    this.logException(ex, "isCached method failed");
                   
                }
            }


            return cacheState;
        }

        /// <summary>
        /// Pushes a TerminalInfo object to cache
        /// </summary>
        /// <remarks>
        /// If the TerminalInfo object already exists the entry is overwritten
        /// </remarks>
        /// <param name="ti">The TerminalInfo object to store</param>
        /// <returns>true if the operation succeeded</returns>
        public bool pushToCache(TerminalInfo ti)
        {
            //First check if the object is already cached or out of date
            cacheStateEnum cacheState = this.isCached(ti.MacAddress);
            bool success = false;

            switch (cacheState)
            {
                case cacheStateEnum.NotCached:
                    //Create the terminal info object in cache
                    using (SqlConnection conn = new SqlConnection(_connectionString))
                    {
                        SqlCommand cmd = new SqlCommand("cmtCreateInCache", conn);
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd = this.setTerminalInfoData(ti, cmd);
                        conn.Open();
                        try
                        {
                            cmd.ExecuteNonQuery();
                            success = true;
                        }
                        catch (Exception ex)
                        {
                            ti.ErrorFlag = true;
                            ti.ErrorMsg = ex.Message;
                            this.logException(ex, "pushToCache method failed");
                        }
                    }
                    break;

                case cacheStateEnum.OutDated:
                    //Here we just need an update
                     using (SqlConnection conn = new SqlConnection(_connectionString))
                    {
                        SqlCommand cmd = new SqlCommand("cmtUpdateCache", conn);
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd = this.setTerminalInfoData(ti, cmd);
                        conn.Open();
                        try
                        {
                            cmd.ExecuteNonQuery();
                            success = true;
                        }
                        catch (Exception ex)
                        {
                            ti.ErrorFlag = true;
                            ti.ErrorMsg = ex.Message;
                            this.logException(ex, "pushToCache method failed");
                        }
                    }
                    break;
            }
            return success;
        }

        /// <summary>
        /// Pushes a TerminalInfo object to cache but does not 
        /// </summary>
        /// <remarks>
        /// If the TerminalInfo object already exists the entry is overwritten
        /// </remarks>
        /// <param name="ti">The TerminalInfo object to store</param>
        /// <returns>true if the operation succeeded</returns>
        public bool pushToCache(TerminalInfo ti, cacheStateEnum cacheState)
        {
            //First check if the object is already cached or out of date
            bool success = false;

            switch (cacheState)
            {
                case cacheStateEnum.NotCached:
                    //Create the terminal info object in cache
                    using (SqlConnection conn = new SqlConnection(_connectionString))
                    {
                        SqlCommand cmd = new SqlCommand("cmtCreateInCache", conn);
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd = this.setTerminalInfoData(ti, cmd);
                        conn.Open();
                        try
                        {
                            cmd.ExecuteNonQuery();
                            success = true;
                        }
                        catch (Exception ex)
                        {
                            ti.ErrorFlag = true;
                            ti.ErrorMsg = ex.Message;
                            this.logException(ex, "pushToCache failed");
                          
                        }
                    }
                    break;

                case cacheStateEnum.OutDated:
                    //Here we just need an update
                    using (SqlConnection conn = new SqlConnection(_connectionString))
                    {
                        SqlCommand cmd = new SqlCommand("cmtUpdateCache", conn);
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd = this.setTerminalInfoData(ti, cmd);
                        conn.Open();
                        try
                        {
                            cmd.ExecuteNonQuery();
                            success = true;
                        }
                        catch (Exception ex)
                        {
                            ti.ErrorFlag = true;
                            ti.ErrorMsg = ex.Message;
                            this.logException(ex, "pushToCache method failed");
                        }
                    }
                    break;
            }
            return success;
        }

        /// <summary>
        /// Pops a TerminalInfo object from cache
        /// </summary>
        /// <param name="macAddress">The terminal MAC Address to retrieve from cache</param>
        /// <returns>The TerminalInfo object identified by the macAddress or null if
        /// the TerminalInfo object is not available from cache</returns>
        public TerminalInfo popFromCache(string macAddress)
        {
            TerminalInfo ti = null;
            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                SqlCommand cmd = new SqlCommand("cmtRetrieveFromCache", conn);
                cmd.Parameters.Add(new SqlParameter("@MacAddress", SqlDbType.NChar, 20));
                cmd.Parameters["@MacAddress"].Value = macAddress;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                conn.Open();
                try
                {
                    SqlDataReader sqlReader = cmd.ExecuteReader();
                    if (sqlReader.HasRows)
                    {
                        sqlReader.Read();
                        ti = this.getTerminalInfoData(sqlReader);
                    }
                    else
                    {
                        ti.ErrorFlag = true;
                        ti.ErrorMsg = "Terminal Info record not cached";
                    }
                }
                catch (Exception ex)
                {
                    ti = new TerminalInfo();
                    ti.ErrorFlag = true;
                    ti.ErrorMsg = ex.Message;
                    this.logException(ex, "popFromCache method failed");
                }
            }
            return ti;
        }

        /// <summary>
        /// Deletes the terminal identified by MAC Address from the chache
        /// </summary>
        /// <param name="macAddress">Terminal to delete</param>
        /// <returns>true if the method succeeded false otherwise</returns>
        public bool deleteFromCache(string macAddress)
        {
            bool success = false;
            string connectionString =
                            ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("cmtDeleteFromCache", conn);
                cmd.Parameters.Add(new SqlParameter("@MacAddress", SqlDbType.NChar, 20));
                cmd.Parameters["@MacAddress"].Value = macAddress;
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                conn.Open();

                try
                {
                    cmd.ExecuteNonQuery();
                    success = true;
                }
                catch (Exception ex)
                {
                    this.logException(ex, "deleteFromCache method failed");
                }

                return success;
            }
        }

        #endregion

        /// <summary>
        /// fills the SqlCommand object with the necessary data to execute the 
        /// underlying Stored Procedure.
        /// </summary>
        /// <param name="ti">The source terminal info object</param>
        /// <param name="cmd">The SqlCommand instance to complete</param>
        /// <returns>The filled SQlCommand instance</returns>
        private SqlCommand setTerminalInfoData(TerminalInfo ti, SqlCommand cmd)
        {
            cmd.Parameters.Add(new SqlParameter("@MacAddress", SqlDbType.NChar, 20));
            cmd.Parameters["@MacAddress"].Value = ti.MacAddress;
            cmd.Parameters.Add(new SqlParameter("@Additional", SqlDbType.NChar, 20));
            cmd.Parameters["@Additional"].Value = ti.Additional;
            cmd.Parameters.Add(new SqlParameter("@AdditionalReturn", SqlDbType.NChar, 20));
            cmd.Parameters["@AdditionalReturn"].Value = ti.AdditionalReturn;
            cmd.Parameters.Add(new SqlParameter("@Blocked", SqlDbType.Bit));
            cmd.Parameters["@Blocked"].Value = ti.Blocked;
            cmd.Parameters.Add(new SqlParameter("@Consumed", SqlDbType.NChar, 20));
            cmd.Parameters["@Consumed"].Value = ti.Consumed;
            cmd.Parameters.Add(new SqlParameter("@ConsumedFreeZone", SqlDbType.NChar, 20));
            cmd.Parameters["@ConsumedFreeZone"].Value = ti.ConsumedFreeZone;
            cmd.Parameters.Add(new SqlParameter("@ConsumedReturn", SqlDbType.NChar, 20));
            cmd.Parameters["@ConsumedReturn"].Value = ti.ConsumedReturn;
            cmd.Parameters.Add(new SqlParameter("@DRAboveFUP", SqlDbType.NChar, 20));
            cmd.Parameters["@DRAboveFUP"].Value = ti.DRAboveFUP;
            cmd.Parameters.Add(new SqlParameter("@EndUserId", SqlDbType.VarChar, 50));
            cmd.Parameters["@EndUserId"].Value = ti.EndUserId;
            cmd.Parameters.Add(new SqlParameter("@FreeZone", SqlDbType.NChar, 20));
            cmd.Parameters["@FreeZone"].Value = ti.FreeZone;
            cmd.Parameters.Add(new SqlParameter("@FUPThreshold", SqlDbType.NChar, 20));
            cmd.Parameters["@FUPThreshold"].Value = ti.FUPThreshold;
            cmd.Parameters.Add(new SqlParameter("@Id", SqlDbType.NChar, 20));
            cmd.Parameters["@Id"].Value = ti.Id;
            cmd.Parameters.Add(new SqlParameter("@InvoiceDayOfMonth", SqlDbType.NChar, 2));
            cmd.Parameters["@InvoiceDayOfMonth"].Value = ti.InvoiceDayOfMonth;
            cmd.Parameters.Add(new SqlParameter("@IPAddress", SqlDbType.NChar, 20));
            cmd.Parameters["@IPAddress"].Value = ti.IPAddress;
            cmd.Parameters.Add(new SqlParameter("@IspId", SqlDbType.NChar, 20));
            cmd.Parameters["@IspId"].Value = ti.IspId;
            cmd.Parameters.Add(new SqlParameter("@RTN", SqlDbType.NChar, 20));
            cmd.Parameters["@RTN"].Value = ti.RTN;
            cmd.Parameters.Add(new SqlParameter("@SitId", SqlDbType.NChar, 20));
            cmd.Parameters["@SitId"].Value = ti.SitId;
            cmd.Parameters.Add(new SqlParameter("@SlaId", SqlDbType.NChar, 10));
            cmd.Parameters["@SlaId"].Value = ti.SlaId;
            cmd.Parameters.Add(new SqlParameter("@SlaName", SqlDbType.NChar, 50));
            cmd.Parameters["@SlaName"].Value = ti.SlaName;
            cmd.Parameters.Add(new SqlParameter("@Status", SqlDbType.NChar, 20));
            cmd.Parameters["@Status"].Value = ti.Status;
            cmd.Parameters.Add(new SqlParameter("@TotalAdditional", SqlDbType.NChar, 20));
            cmd.Parameters["@TotalAdditional"].Value = ti.TotalAdditional;
            cmd.Parameters.Add(new SqlParameter("@TotalAdditionalReturn", SqlDbType.NChar, 20));
            cmd.Parameters["@TotalAdditionalReturn"].Value = ti.TotalAdditionalReturn;
            cmd.Parameters.Add(new SqlParameter("@RTPool", SqlDbType.NChar, 10));
            cmd.Parameters["@RTPool"].Value = ti.RTNPool;
            cmd.Parameters.Add(new SqlParameter("@FWPool", SqlDbType.NChar, 10));
            cmd.Parameters["@FWPool"].Value = ti.FWPool;
            cmd.Parameters.Add(new SqlParameter("@BladeId", SqlDbType.NChar, 10));
            cmd.Parameters["@BladeId"].Value = ti.BladeId;
            cmd.Parameters.Add(new SqlParameter("@RTNFUPThreshold", SqlDbType.NChar, 20));
            cmd.Parameters["@RTNFUPThreshold"].Value = ti.RTNFUPThreshold;


            return cmd;
        }
        
        /// <summary>
        /// Creates a TerminalInfo instance from the data provided by the SqlDataReader
        /// </summary>
        /// <param name="reader">SqlDataReader, result from the retrieve stored procedure</param>
        /// <returns>Completed TemrinalInfo instance</returns>
        private TerminalInfo getTerminalInfoData(SqlDataReader reader)
        {
            TerminalInfo ti = new TerminalInfo();
            ti.Additional = ((string) reader["Additional"]).Trim();
            ti.AdditionalReturn = ((string)reader["AdditionalReturn"]).Trim();
            ti.Blocked = (bool)reader["Blocked"];
            ti.Consumed = ((string)reader["Consumed"]).Trim();
            ti.ConsumedFreeZone = ((string) reader["ConsumedFreeZone"]).Trim();
            ti.ConsumedReturn = ((string)reader["ConsumedReturn"]).Trim();
            ti.DRAboveFUP = ((string)reader["DRAboveFUP"]).Trim();
            ti.EndUserId = ((string)reader["EndUserId"]).Trim();
            ti.FreeZone = ((string)reader["FreeZone"]).Trim();
            ti.FUPThreshold = Convert.ToDecimal(((string)reader["FUPThreshold"]).Trim());
            ti.Id = ((string)reader["Id"]).Trim();
            ti.InvoiceDayOfMonth = ((string)reader["InvoiceDayOfMonth"]).Trim();
            ti.IPAddress = ((string)reader["IPAddress"]).Trim();
            ti.IspId = ((string)reader["IspId"]).Trim();
            ti.MacAddress = ((string)reader["MacAddress"]).Trim();
            ti.RTN = ((string)reader["RTN"]).Trim();
            ti.SitId = ((string)reader["SitId"]).Trim();
            ti.SlaId = ((string)reader["SlaId"]).Trim();
            ti.SlaName = ((string)reader["SlaName"]).Trim();
            ti.Status = ((string)reader["Status"]).Trim();
            ti.TotalAdditional = ((string)reader["TotalAdditional"]).Trim();
            ti.TotalAdditionalReturn = ((string)reader["TotalAdditionalReturn"]).Trim();
            ti.FWPool = ((string)reader["FWPool"]).Trim();
            ti.RTNPool = ((string)reader["RTPool"]).Trim();
            ti.BladeId = ((string)reader["BladeId"]).Trim();
            ti.RTNFUPThreshold = Convert.ToDecimal(((string)reader["RTNFUPThreshold"]).Trim());
            return ti;
        }

        /// <summary>
        /// Logs an exception
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="desc"></param>
        private void logException(Exception ex, string desc)
        {
            string providerName = BOControllerLibrary.Properties.Settings.Default.ProviderName;
            BOLogController boLogController = new BOLogController(_connectionString, providerName);
            CmtApplicationException cmtApplicationException = new CmtApplicationException(ex, desc, "");
            boLogController.LogApplicationException(cmtApplicationException);
        }
    }
}