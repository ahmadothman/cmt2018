﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BOControllerLibrary.ISPIF.Model
{
    /// <summary>
    /// Returns the specified cache provider instance
    /// </summary>
    public class TICacheProviderFactory
    {
        /// <summary>
        /// Returns the ITICacheProvider implementation
        /// </summary>
        /// <param name="providerName"></param>
        /// <returns></returns>
        public static ITICacheProvider getCacheProvider(string providerName)
        {
            ITICacheProvider provider = null;

            if (providerName.Equals("SQLServerCacheProvider"))
            {
                provider = new SQLServerCacheProvider();
            }

            return provider;
        }
    }
}