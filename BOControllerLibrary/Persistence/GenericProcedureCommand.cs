﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;

namespace BOControllerLibrary.Persistence
{
    class GenericProcedureCommand
    {
        public static void ProcessCommand (string dataprovider, string connectionstring, string storedProcedureName, 
            DbParameter[] parameterCollection, Action<DbCommand> onCommandExecuted, Action<Exception> onError)
        {
            
            var fact = DbProviderFactories.GetFactory(dataprovider);
            using (var conn = fact.CreateConnection())
            {
                if (conn == null)
                    throw new Exception(string.Format("No such dataprovider found: {0}. Did you mean Sql.Data.SqlClient?", dataprovider));

                // open the database connection
                conn.ConnectionString = connectionstring;
                conn.Open();

                var cmd = conn.CreateCommand();
                cmd.CommandText = storedProcedureName;
                cmd.CommandType = CommandType.StoredProcedure;

                if (parameterCollection != null)
                    cmd.Parameters.AddRange(parameterCollection);

                var trans = conn.BeginTransaction(IsolationLevel.ReadCommitted);
                cmd.Transaction = trans;

                try
                {
                    cmd.ExecuteNonQuery();
                    trans.Commit();
                    if (onCommandExecuted != null)
                        onCommandExecuted(cmd);
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    if (onError != null)
                        onError(ex);
                }
            }

        }
    }
}
