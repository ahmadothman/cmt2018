﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace BOControllerLibrary.Exceptions
{
    /// <summary>
    /// This exception is thrown if the name of an existing document has been changed
    /// </summary>
    [Serializable]
    public class DocumentNameChangeException : System.Exception
    {
        public DocumentNameChangeException()
        {
        }

        public DocumentNameChangeException(string message)
            : base(message)
        {
        }

        public DocumentNameChangeException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected DocumentNameChangeException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
