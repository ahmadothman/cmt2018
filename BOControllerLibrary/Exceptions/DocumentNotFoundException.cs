﻿using System;
using System.Linq;
using System.Runtime.Serialization;

namespace BOControllerLibrary.Exceptions
{
    /// <summary>
    /// Thrown if a document is not found
    /// </summary>
    [Serializable]
    public class DocumentNotFoundException : System.Exception
    {
        public DocumentNotFoundException()
        {
        }

        public DocumentNotFoundException(string message)
            : base(message)
        {
        }

        public DocumentNotFoundException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected DocumentNotFoundException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
