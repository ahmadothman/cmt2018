﻿using System;
using System.Linq;
using System.Runtime.Serialization;

namespace BOControllerLibrary.Exceptions
{
    /// <summary>
    /// Thrown if the caller tries to create a document which name already exists is the document table.
    /// </summary>
    [Serializable]
    public class DuplicateDocumentException : System.Exception
    {
        public DuplicateDocumentException()
        {
        }

        public DuplicateDocumentException(string message)
            : base(message)
        {
        }

        public DuplicateDocumentException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected DuplicateDocumentException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
