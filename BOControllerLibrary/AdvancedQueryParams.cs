﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BOControllerLibrary.Model
{
    /// <summary>
    /// This class serves as a transport object for the 
    /// advanced query parameters.
    /// </summary>
    public class AdvancedQueryParams
    {
        /// <summary>
        /// The ISP Identifier
        /// </summary>
        public int IspId { get; set; }

        /// <summary>
        /// The Site identifier
        /// </summary>
        public int SitId { get; set; }

        /// <summary>
        /// The terminal full name
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Distributor Id
        /// </summary>
        public int DistributorId { get; set; }

        /// <summary>
        /// Administration state
        /// </summary>
        public int AdmState { get; set; }

        /// <summary>
        /// IP Address
        /// </summary>
        public string IpAddress { get; set; }

        /// <summary>
        /// MAC address
        /// </summary>
        public string MacAddress { get; set; }

        /// <summary>
        /// Serivce Pack Id
        /// </summary>
        public int SlaId { get; set; }

        /// <summary>
        /// Forward pool
        /// </summary>
        public int FwdPool { get; set; }

        /// <summary>
        /// Return pool
        /// </summary>
        public int RtnPool { get; set; }

        /// <summary>
        /// Expiry date
        /// </summary>
        public DateTime ExpDate { get;  set; }

        /// <summary>
        /// The ExpirationDate operand
        /// </summary>
        public string ExpDateOper { get; set; }

        /// <summary>
        /// The EMail address of the terminal
        /// </summary>
        public string EMail { get; set; }

        /// <summary>
        /// The Serial number of the terminal
        /// </summary>
        public string Serial { get; set; }
    }
}
