﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using BOControllerLibrary.Controllers.Interfaces;
using BOControllerLibrary.Model.Service;
using BOControllerLibrary.Model;
using BOControllerLibrary.Model.Log;
using System.Data;
using System.Data.SqlClient;

namespace BOControllerLibrary.Controllers
{
    /// <summary>
    /// This component handles the flexible user services
    /// It is the implementation of use case UC-VNOSO-005
    /// </summary>
    public class BOServicesController : IBOServicesController
    {

        string _connectionString = "";
        string _databaseProvider = "";
        BOLogController _logController;

        public BOServicesController(string connectionString, string databaseProvider)
        {
            _connectionString = connectionString;
            _databaseProvider = databaseProvider;
            _logController = new BOLogController(_connectionString, _databaseProvider);
        }

        /// <summary>
        /// Deletes the service from the CMTDatabase.
        /// This does not physically remove the service ascx file from the project,
        /// but just removes it from the list of selectable services.
        /// </summary>
        /// <param name="service">The service to be deleted</param>
        /// <returns>True if succesful, else false</returns>
        public bool DeleteService(Service service)
        {
            bool deleted;

            try
            {
                var var1 = new StringBuilder();
                var1.Append("UPDATE Services \n");
                var1.Append("SET DeletedFlag = 1 \n");
                var1.Append("WHERE ServiceId = @ServiceId ");

                using (SqlConnection con = new SqlConnection(_connectionString))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(var1.ToString(), con))
                    {
                        cmd.Parameters.AddWithValue("@ServiceId", service.ServiceId);

                        int rows = cmd.ExecuteNonQuery();

                        if (rows == 1)
                        {
                            deleted = true;
                        }
                        else
                        {
                            deleted = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOServicesController: Error while deleting service",
                                                                                   service.ServiceId.ToString(), (short)ExceptionLevelEnum.Error));
                deleted = false;
            }

            return deleted;
        }

        /// <summary>
        /// Returns the parent Service of a service.
        /// This is the service which is represented by a node one level higher
        /// than the given service. The return value can be null!
        /// </summary>
        /// <param name="service">The service for which the parent is requested</param>
        /// <returns>The parent service, can also be null</returns>
        public Service GetParent(Service service)
        {
            return this.GetService(service.ParentServiceId);
        }

        /// <summary>
        /// Returns a list of services linked to the given VNO.
        /// </summary>
        /// <param name="VNOId">The VNO ID in question</param>
        /// <returns>A list of services</returns>
        public List<Service> GetServicesForVNO(int VNOId)
        {
            List<Service> services = new List<Service>();
            try
            {
                string getCmd = "SELECT ServiceId FROM ServicesVNOs " +
                                "WHERE VNOId = @VNOId";
                Service service;
                string serviceId;

                using (SqlConnection con = new SqlConnection(_connectionString))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(getCmd, con))
                    {
                        cmd.Parameters.AddWithValue("@VNOId", VNOId);
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            service = new Service();
                            serviceId = reader.GetGuid(0).ToString();
                            service = this.GetService(serviceId);
                            services.Add(service);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOServicesController: Error while executing GetServicesForVNO",
                                                                                   VNOId.ToString()));
            }
            return services;
        }

        /// <summary>
        /// Returns a typed list of services which are related as siblings
        /// to the given service. This list can be empty.
        /// </summary>
        /// <param name="service">The service for which the siblings are requested</param>
        /// <returns>A list of services</returns>
        public List<Service> GetSiblings(Service service)
        {
            // Get the parent service
            Service parent = this.GetParent(service);
            
            // Get all children of parent
            List<Service> allChildren = this.GetChildren(parent);
            
            // Remove original service from list so that only the other siblings are returned
            List<Service> siblings = new List<Service>();
            foreach (Service sibling in allChildren)
            {
                if (sibling.ServiceId != service.ServiceId)
                {
                    siblings.Add(sibling);
                }
            }

            return siblings;
        }

        /// <summary>
        /// Links a Service to a VNO. This adds a record to the ServicesVNOs table.
        /// </summary>
        /// <param name="VNOId">The VNO ID the service should be linked to</param>
        /// <param name="ServiceId">The ID of the service to be linked to the VNO</param>
        /// <returns>True if successful, else false</returns>
        public bool LinkServiceToVNO(int VNOId, string ServiceId)
        {
            string insertCmd = "INSERT INTO ServicesVNOs (VNOId, ServiceId)" +
                               "VALUES (@VNOId, @ServiceId)"; 
            
            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(insertCmd, conn))
                    {
                        // write service information to database
                        cmd.Parameters.AddWithValue("@VNOId", VNOId);
                        cmd.Parameters.AddWithValue("@ServiceId", ServiceId);

                        cmd.ExecuteNonQuery();
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOServicesController: Error while executing LinkServiceToVNO", VNOId.ToString(), (short)ExceptionLevelEnum.Error));
                return false;
            }
        }

        /// <summary>
        /// Removes the link between a Service and a VNO.
        /// </summary>
        /// <param name="VNOId">The VNO ID the service should be removed from</param>
        /// <param name="ServiceId">The ID of the service to be removed from the VNO</param>
        /// <returns>True if successful, else false</returns>
        public bool RemoveServiceFromVNO(int VNOId, string ServiceId)
        {
            string deleteCmd = "DELETE FROM ServicesVNOs " +
                               "WHERE VNOId = @VNOId AND ServiceId = @ServiceId";

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(deleteCmd, conn))
                    {
                        // write service information to database
                        cmd.Parameters.AddWithValue("@VNOId", VNOId);
                        cmd.Parameters.AddWithValue("@ServiceId", ServiceId);

                        cmd.ExecuteNonQuery();
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOServicesController: Error while executing RemoveServiceFromVNO", VNOId.ToString(), (short)ExceptionLevelEnum.Error));
                return false;
            }
        }

        /// <summary>
        /// Updates the service in the CMT database. If the service does not exist a new service is created.
        /// </summary>
        /// <param name="service">The service to be updated/added</param>
        /// <returns>True if successful, else false</returns>
        public bool UpdateService(Service service)
        {
            string insertCmd = "";

            // check if service exists
            Service serviceExists = this.GetService(service.ServiceId.ToString());
            bool isNew = true;
            if (service.ServiceId == serviceExists.ServiceId)
            {
                isNew = false;
                var var1 = new StringBuilder();
                var1.Append("UPDATE Services \n");
                // check if icon has been changed
                if (!String.IsNullOrEmpty(service.Icon))
                {
                    var1.Append("SET ServiceName = @ServiceName, ServiceDescription = @ServiceDescription, ServiceController = @ServiceController, ServiceLevel = @ServiceLevel, ParentServiceId = @ParentServiceId, DeletedFlag = @DeletedFlag, Icon = @Icon, IconFileName = @IconFileName \n");
                }
                else
                {
                    var1.Append("SET ServiceName = @ServiceName, ServiceDescription = @ServiceDescription, ServiceController = @ServiceController, ServiceLevel = @ServiceLevel, ParentServiceId = @ParentServiceId, DeletedFlag = @DeletedFlag \n");
                }
                var1.Append("WHERE ServiceId = @ServiceId ");
                insertCmd = var1.ToString();
            }
            else
            {
                // check if there is an icon file
                if (!String.IsNullOrEmpty(service.Icon))
                {
                    insertCmd = "INSERT INTO Services (ServiceId, ServiceName, ServiceDescription, ServiceController, ServiceLevel, ParentServiceId, DeletedFlag, Icon, IconFileName) " +
                            "VALUES (@ServiceId, @ServiceName, @ServiceDescription, @ServiceController, @ServiceLevel, @ParentServiceId, @DeletedFlag, @Icon, @IconFileName)";
                }
                else
                {
                    insertCmd = "INSERT INTO Services (ServiceId, ServiceName, ServiceDescription, ServiceController, ServiceLevel, ParentServiceId, DeletedFlag) " +
                         "VALUES (@ServiceId, @ServiceName, @ServiceDescription, @ServiceController, @ServiceLevel, @ParentServiceId, @DeletedFlag)";
                }
            } 
            
            // update/add service
            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(insertCmd, conn))
                    {
                        // write service information to database
                        cmd.Parameters.AddWithValue("@ServiceId", service.ServiceId);
                        cmd.Parameters.AddWithValue("@ServiceName", service.ServiceName);
                        cmd.Parameters.AddWithValue("@ServiceDescription", service.ServiceDescription);
                        cmd.Parameters.AddWithValue("@ServiceController", service.ServiceController);
                        cmd.Parameters.AddWithValue("@ServiceLevel", service.Level);
                        cmd.Parameters.AddWithValue("@ParentServiceId", service.ParentServiceId);
                        cmd.Parameters.AddWithValue("@DeletedFlag", false);
                        // process the icon file, if applicable
                        if (!String.IsNullOrEmpty(service.Icon))
                        {
                            byte[] icon; 
                            using (var stream = new FileStream("C:\\temp\\" + service.Icon, FileMode.Open, FileAccess.Read))
                            {
                                using (var reader = new BinaryReader(stream))
                                {
                                    icon = reader.ReadBytes((int)stream.Length);
                                }
                            }
                            cmd.Parameters.AddWithValue("@Icon", icon);
                            cmd.Parameters.AddWithValue("@IconFileName", service.Icon);

                            // delete the uploaded file
                            try
                            {
                                File.Delete("C:\\temp\\" + service.Icon);
                            }
                            catch (Exception ex)
                            {
                                CmtApplicationException cmtEx = new CmtApplicationException();
                                cmtEx.ExceptionDesc = ex.Message;
                                cmtEx.ExceptionStacktrace = ex.StackTrace;
                                cmtEx.ExceptionLevel = (short)ExceptionLevelEnum.Error;
                                cmtEx.StateInformation = "Error when deleting service icon file";
                                cmtEx.UserDescription = "BOServicesController.UpdateService method failed while deleting a service icon file";
                            }
                        }
                        
                        cmd.ExecuteNonQuery();
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                if (isNew)
                {
                    _logController.LogApplicationException(new CmtApplicationException(ex, "BOServicesController: Error while storing new service in database", service.ServiceController, (short)ExceptionLevelEnum.Error));
                }
                else
                {
                    _logController.LogApplicationException(new CmtApplicationException(ex, "BOServicesController: Error while updating service in database", service.ServiceId.ToString(), (short)ExceptionLevelEnum.Error));
                }
                return false;
            }
        }

        /// <summary>
        /// Returns the service details
        /// </summary>
        /// <param name="ServiceId">The ID of the service</param>
        /// <returns>A service</returns>
        public Service GetService(string ServiceId)
        {
            Service service = new Service();
            try
            {
                var var1 = new StringBuilder();
                var1.Append("SELECT ServiceId, ServiceName, ServiceDescription, ServiceController, ServiceLevel, ParentServiceId, Icon, IconFileName \n");
                var1.Append("FROM Services \n");
                var1.Append("WHERE ServiceId = @ServiceId AND DeletedFlag = 0");

                using (SqlConnection con = new SqlConnection(_connectionString))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(var1.ToString(), con))
                    {
                        cmd.Parameters.AddWithValue("@ServiceId", ServiceId);
                        
     
                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            service.ServiceId = reader.GetGuid(0);
                            service.ServiceName = reader.GetString(1);
                            service.ServiceDescription = reader.GetString(2);
                            service.ServiceController = reader.GetString(3);
                            service.Level = reader.GetInt32(4);
                            service.ParentServiceId = reader.GetString(5);
                            if (!String.IsNullOrEmpty(reader.GetString(7)))
                            {
                                service.Icon = reader.GetString(7);
                            }
                            //if (service.Icon != "")
                            //{
                            //    var blob = new Byte[(reader.GetBytes(6, 0, null, 0, int.MaxValue))];
                            //    reader.GetBytes(6, 0, blob, 0, blob.Length);
                            //    using (var fs = new FileStream("C:\\temp\\icons\\" + service.Icon, FileMode.Create, FileAccess.Write))
                            //    {
                            //        fs.Write(blob, 0, blob.Length);
                            //    }
                            //    MemoryStream memoryStream = new MemoryStream();
                            //    memoryStream.Write(blob, 0, blob.Length);
                            //}
                        }
                    }
                }
            }   
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOServicesController: Error while executing GetService",
                                                                                   ServiceId));
            }

            return service;
        }

        /// <summary>
        /// Returns a list of all available (non-deleted) services in the database
        /// of a certain level.
        /// Will mainly be used to get all top level services or to get all services
        /// </summary>
        /// <param name="level">The service level (1, 2, 3, or all)</param>
        /// <returns>A list of services</returns>
        public List<Service> GetAllAvailableServices(string level)
        {
            List<Service> availableServices = new List<Service>();
            Service service;
           
            try
            {
                string var1 = "";

                if (level == "all")
                {
                    var1 = "SELECT * FROM Services WHERE DeletedFlag != 1";
                }
                else
                {
                    var1 = "SELECT * FROM Services WHERE ServiceLevel IN (@ServiceLevel) AND DeletedFlag != 1";
                }

                using (SqlConnection con = new SqlConnection(_connectionString))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(var1.ToString(), con))
                    {
                        if (level == "1" || level == "2" || level == "3")
                        {
                            cmd.Parameters.AddWithValue("@ServiceLevel", level);
                        }
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            service = new Service(); 
                            service.ServiceId = reader.GetGuid(0);
                            service.ServiceName = reader.GetString(1);
                            service.ServiceDescription = reader.GetString(2);
                            service.ServiceController = reader.GetString(3);
                            service.Level = reader.GetInt32(4);
                            service.ParentServiceId = reader.GetString(5);
                            //if (!String.IsNullOrEmpty(reader.GetString(7)))
                            //{
                            //    service.Icon = reader.GetString(7);
                            //}
                            availableServices.Add(service);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOServicesController: Error while executing GetAllAvailableServices",
                                                                                   "Service level " + level));
            }
            return availableServices;
        }

        /// <summary>
        /// Returns a list of all child services of a parent service
        /// The list can be empty
        /// </summary>
        /// <param name="parentService">The parent service</param>
        /// <returns>A list of services</returns>
        public List<Service> GetChildren(Service parentService)
        {
            List<Service> serviceChildren = new List<Service>();
            Service service;

            try
            {
                string var1 = "SELECT * FROM Services WHERE ParentServiceId = @ParentServiceId AND DeletedFlag != 1";

                using (SqlConnection con = new SqlConnection(_connectionString))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(var1.ToString(), con))
                    {
                        cmd.Parameters.AddWithValue("@ParentServiceId", parentService.ServiceId.ToString());
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            service = new Service();
                            service.ServiceId = reader.GetGuid(0);
                            service.ServiceName = reader.GetString(1);
                            service.ServiceDescription = reader.GetString(2);
                            service.ServiceController = reader.GetString(3);
                            service.Level = reader.GetInt32(4);
                            service.ParentServiceId = reader.GetString(5);
                            //if (!String.IsNullOrEmpty(reader.GetString(7)))
                            //{
                            //    service.Icon = reader.GetString(7);
                            //}
                            serviceChildren.Add(service);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOServicesController: Error while executing GetChildren",
                                                                                   parentService.ServiceId.ToString()));
            }
            return serviceChildren;
        }

        /// <summary>
        /// Method for retrieving an icon from the database and putting it into the memorystream
        /// so that it can be displayed as an image
        /// </summary>
        /// <param name="ServiceId">The ID of the service for which the icon is retrieved</param>
        /// <returns>The image</returns>
        public Byte[] GetIcon(string ServiceId)
        {
            MemoryStream memoryStream = new MemoryStream();
            Byte[] blob = new Byte[0];
            try
            {
                var var1 = new StringBuilder();
                var1.Append("SELECT Icon \n");
                var1.Append("FROM Services \n");
                var1.Append("WHERE ServiceId = @ServiceId ");
                
                using (SqlConnection con = new SqlConnection(_connectionString))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(var1.ToString(), con))
                    {
                        cmd.Parameters.AddWithValue("@ServiceId", ServiceId);
                        SqlDataReader reader = cmd.ExecuteReader();
                        reader.Read();
                        blob = new Byte[(reader.GetBytes(0, 0, null, 0, int.MaxValue))];
                        reader.GetBytes(0, 0, blob, 0, blob.Length);
                    }
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOServicesController: Error while executing GetIcon",
                                                                                   ServiceId));
            }

            return blob;
        }

        /// <summary>
        /// Returns a list of services linked to the given Role.
        /// </summary>
        /// <param name="RoleName">The Role in question</param>
        /// <returns>A list of services</returns>
        public List<Service> GetServicesForRole(string RoleName)
        {
            List<Service> services = new List<Service>();
            try
            {
                string getCmd = "SELECT ServiceId FROM ServicesRoles " +
                                "WHERE RoleName = @RoleName";
                Service service;
                string serviceId;

                using (SqlConnection con = new SqlConnection(_connectionString))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(getCmd, con))
                    {
                        cmd.Parameters.AddWithValue("@RoleName", RoleName);
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            service = new Service();
                            serviceId = reader.GetGuid(0).ToString();
                            service = this.GetService(serviceId);
                            services.Add(service);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOServicesController: Error while executing GetServicesForVNO",
                                                                                   RoleName));
            }
            return services;
        }

        /// <summary>
        /// Links a Service to a Role. This adds a record to the RolesServices table.
        /// </summary>
        /// <param name="RoleName">The Role the service should be linked to</param>
        /// <param name="ServiceId">The ID of the service to be linked to the VNO</param>
        /// <returns>True if succesful, else false</returns>
        public bool LinkServiceToRole(string RoleName, string ServiceId)
        {
            string insertCmd = "INSERT INTO ServicesRoles (RoleName, ServiceId)" +
                               "VALUES (@RoleName, @ServiceId)";

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(insertCmd, conn))
                    {
                        // write service information to database
                        cmd.Parameters.AddWithValue("@RoleName", RoleName);
                        cmd.Parameters.AddWithValue("@ServiceId", ServiceId);

                        cmd.ExecuteNonQuery();
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOServicesController: Error while executing LinkServiceToVNO", RoleName, (short)ExceptionLevelEnum.Error));
                return false;
            }
        }

        /// <summary>
        /// Remove a Service from a Role. This removes a record from the RolesServices table.
        /// </summary>
        /// <param name="RoleName">The Role the service should be removed from</param>
        /// <param name="ServiceId">The ID of the service to be removed from the VNO</param>
        /// <returns>True if succesful, else false</returns>
        public bool RemoveServiceFromRole(string RoleName, string ServiceId)
        {
            string deleteCmd = "DELETE FROM ServicesRoles " +
                              "WHERE RoleName = @RoleName AND ServiceId = @ServiceId";

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(deleteCmd, conn))
                    {
                        // write service information to database
                        cmd.Parameters.AddWithValue("@RoleName", RoleName);
                        cmd.Parameters.AddWithValue("@ServiceId", ServiceId);

                        cmd.ExecuteNonQuery();
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOServicesController: Error while executing RemoveServiceFromVNO", RoleName, (short)ExceptionLevelEnum.Error));
                return false;
            }
        }
    }
}
