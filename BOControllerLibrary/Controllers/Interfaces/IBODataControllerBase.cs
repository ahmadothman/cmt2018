﻿using System.Data;

namespace BOControllerLibrary.Controllers.Interfaces
{
    public interface IBODataControllerBase
    {
        string ConnectionString { get; set; }
        string DataProvider { get; set; }
        IDbConnection CreateConnection();
    }
}
