﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using BOControllerLibrary.Model;
using BOControllerLibrary.Controllers;
using BOControllerLibrary.Controllers.Interfaces;

namespace BOControllerLibrary.Controllers.Interfaces
{
    /// <summary>
    /// Contains the methods for processing a checkout
    /// In the first implementation, a distributor can choose an unreleased batch of vouchers
    /// to pay for
    /// The payment will be processed by an external provider, PayPal in the first case
    /// Payments are stored in the CMT database
    /// </summary>
    public interface IBOPaymentController
    {
        /// <summary>
        /// Initiates the checkout procedure
        /// </summary>
        /// <param name="checkOutMethod">Determines the checkout method, in the first implemantation, only PayPal is available</param>
        /// <param name="action">The action that should be taken</param>
        /// <param name="distributorId">The ID of the paying distributor</param>
        /// <param name="amount">The amount to be paid</param>
        /// <param name="token">Paypal session token</param>
        string CheckoutStart(string checkOutMethod, string action, int distributorId, decimal amount,string cur, ref string token);

        /// <summary>
        /// Cancels the checkout procedure
        /// This method has been replaced by a redirect
        /// </summary>
        void CheckoutCancel();

        /// <summary>
        /// Proceeds with the user review the Paypal checkout
        /// This user still has to confirm the payment after this step
        /// </summary>
        /// <param name="token">Paypal session token</param>
        /// <param name="payerId">Paypal payer ID</param>
        /// <returns>A URL, possibly containing an error message</returns>
        string PaypalCheckoutProceed(string token, ref string payerId);

        /// <summary>
        /// Completes the Paypal checkout upon confirmation by the user
        /// </summary>
        /// <param name="amount">Final amount to be paid</param>
        /// <param name="payerId">Paypal payer ID</param>
        /// <param name="token">Paypal session token</param>
        /// <param name="batchId">The ID of the voucher batch paid for</param>
        /// <param name="distributorId">The ID of the paying distributor</param>
        /// <returns>1 if succesful, else an error message</returns>
        string PaypalCheckoutComplete(decimal amount, string cur,string payerId, string token, string action, int distributorId);
    }
}
