﻿using System;
using System.Collections.Generic;
using System.Text;
using BOControllerLibrary.ISPIF.Model;

namespace BOControllerLibrary.Controllers
{
    /// <summary>
    /// This component interacts with the ISPIF and the CMT database mainly for retrieving volume information and technical parameters related to a specific terminal. 
    /// The BOMonitorController contains the main methods for monitoring and managing terminals. 
    /// By means of BOMonitorController it is possible to Activate and De-Activate terminals and to obtain operational data from a terminal (Volume info, CNo values etc.) 
    /// </summary>
    public interface IBOMonitoringController
    {
    }
}
