﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BOControllerLibrary.Model.Reporting;

namespace BOControllerLibrary.Controllers.Interfaces
{
    /// <summary>
    /// Contains methods which are necessary for the Mandac hotspot access points,
    /// which are a type of connected devices, added to terminals
    /// These methods are made available for third parties
    /// </summary>
    public interface IBOMandacController
    {
        /// <summary>
        /// Returns an estimation of the remaining volume for Mandac hotspot connected to a terminal.
        /// This estimation is an indication to the terminal owner if more
        /// hotspot vouchers can be issued or not.
        /// This method will be interpreted by the Mandac system
        /// </summary>
        /// <param name="mandacId">The ID of the Mandac hotspot</param>
        /// <returns>The volume estimation in GB</returns>
        double GetEstimatedRemainingVolumeForHotspot(string mandacId);

        /// <summary>
        /// Returns the current remaining volume of a terminal
        /// </summary>
        /// <param name="mandacId">The ID of the Mandac hotspot</param>
        /// <returns>The volume in GB</returns>
        double GetRemainingVolumeForTerminal(string mandacId);

        /// <summary>
        /// Returns a list of all Mandac transactions in the CMT database
        /// </summary>
        /// <returns></returns>
        List<MandacTransaction> GetAllTransactions();

        /// <summary>
        /// This method returns the remaining validity period for the volume of a terminal
        /// </summary>
        /// <param name="mandacId">The ID of the Mandac hotspot connected to the terminal</param>
        /// <returns>The number of day the volume is still valid</returns>
        int GetValidityForTerminalVolume(string mandacId);
    }
}
