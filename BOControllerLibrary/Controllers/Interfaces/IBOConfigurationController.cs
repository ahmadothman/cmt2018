﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BOControllerLibrary.Controllers.Interfaces
{
    /// <summary>
    /// Describes the methods necessary for working with the 
    /// configuration table of te CMT database
    /// </summary>
    public interface IBOConfigurationController
    {
        /// <summary>
        /// Returns the parameter from the config table with the
        /// give name;
        /// </summary>
        /// <param name="paramName">The parameter name</param>
        /// <returns>The parameter value or null if not found</returns>
        string getParameter(string paramName);

          /// <summary>
        /// Sets a parameter value. The parameter is created if it does not
        /// exist in the configuration table yet.
        /// </summary>
        /// <param name="paramName">The parameter name</param>
        /// <param name="value">The parameter value</param>
        /// <returns>True if the parameter update succeeded</returns>
        Boolean setParameter(string paramName, string value);
    }
}
