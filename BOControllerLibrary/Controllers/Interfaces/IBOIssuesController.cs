﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using BOControllerLibrary.Model.Issues;

namespace BOControllerLibrary.Controllers.Interfaces
{
    /// <summary>
    /// Contains the methods which are necessary for Issue controllers
    /// </summary>
    public interface IBOIssuesController
    {
        /// <summary>
        /// Returns a list of issues for a given
        /// distributor.
        /// </summary>
        /// <param name="distributorId">The distributor identifier.</param>
        /// <returns>A list of issues. This list can be empty.</returns>
        List<Issue> GetIssuesByDistributor(int distributorId);

        /// <summary>
        /// Creates a new issue as submitted by a distributor.
        /// </summary>
        /// <param name="issue">The issue to be added</param>
        /// <returns>A string containing the unique identifier of the created issue.</returns>
        string SubmitNewIssue(Issue issue);

        /// <summary>
        /// Returns a specific Jira issue, for which the data is stored in Jira.
        /// </summary>
        /// <param name="jiraIssueKey">The unique issue identifier.</param>
        /// <returns>A JiraIssue.</returns>
        JiraIssue GetJiraIssueDetails(string jiraIssueKey);

        /// <summary>
        /// A method for submitting a comment from the CMT to Jira
        /// A comment can be submitted on an existing issue
        /// In the current implementation, a distributor can only submit a comment
        /// on an open issue which he has reported. Theoretically, other options are possible,
        /// but at the moment there are no use cases requiring this.
        /// </summary>
        /// <param name="commentBody">The contents of the comment</param>
        /// <param name="issueKey">The key of the concerned issue</param>
        /// <returns>True if the operation succeeded</returns>
        bool SubmitComment(string commentBody, string jiraIssueKey);

    }
}
