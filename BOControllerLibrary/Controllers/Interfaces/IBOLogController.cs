﻿using System;
using System.Data;
using System.Collections.Generic;
using BOControllerLibrary.Model;
using BOControllerLibrary.Model.Terminal;
using BOControllerLibrary.Model.User;
using BOControllerLibrary.Model.Log;

namespace BOControllerLibrary.Controllers.Interfaces
{
    /// <summary>
    /// Warning: these interfaces are very preliminary and will probably change as the OM becomes more clear to me :).
    /// The BOLogController takes care of all logging Use Cases, such as terminal activity logging, user action logging and exception logging. 
    /// The terminal activity log is also used by the BOAccountingController for producing invoice statements at the end of the month.
    /// </summary>
    public interface IBOLogController
    {
        /// <summary>
        /// Log all user and application exceptions.
        /// </summary>
        /// <param name="ex">The exception object: Note that it is useful to subclass System.Exception for more specific information. </param>
        void LogApplicationException(CmtApplicationException ex);


        /// <summary>
        /// Logs the terminal activity.
        /// </summary>
        /// <param name="activity">The activity.</param>
        void LogTerminalActivity(TerminalActivity activity);

        /// <summary>
        /// Updates a terminal activity in the terminal acitivty log
        /// </summary>
        /// <param name="activity">The modified terminal activity</param>
        /// <returns>True if the method succeeds</returns>
        void UpdateTerminalActivity(TerminalActivity activity);

        /// <summary>
        /// Returns a TerminalActivity object given by its terminalActivityId
        /// </summary>
        /// <param name="terminalActivityId">The terminal identifier</param>
        /// <returns>A TerminalActivity object or null if not found</returns>
        TerminalActivity GetTerminalActivityById(int terminalActivityId);

        /// <summary>
        /// Log the activities of a user (Log-In, Log-Out, Session expiration)
        /// </summary>
        /// <param name="activity">The activity.</param>
        void LogUserActivity(UserActivity activity);

        /// <summary>
        /// Removes a terminal activity from the TerminalActivityLog table
        /// </summary>
        /// <param name="terminalActivityId">The identifier of the terminal activity</param>
        /// <returns>True if the method succeeds</returns>
        Boolean DeleteTerminalActivity(int terminalActivityId);

        /// <summary>
        /// Return a list of activities for a given terminal
        /// </summary>
        /// <param name="macAddress">The MAC address of the terminal</param>
        /// <returns>A list of activities, this list can be empty</returns>
        List<TerminalActivity> GetTerminalActivity(string macAddress);

        /// <summary>
        /// Returns a list of activities for the given SitId and IspId
        /// </summary>
        /// <remarks>
        /// This method returns a list of TerminalActivityExt objects. These 
        /// objects contain next to the UserId GUID, the name of the user who
        /// executed the terminal activity.<br/>
        /// <b>The query executes a left outerjoin between the TerminalActivityLog table and 
        /// the aspnet_Users table. In case a user is deleted from the Users table, the activity log
        /// record will still show up but the UserName is set to NULL</b>
        /// </remarks>
        /// <param name="SitId">The Site Identifier of the terminal</param>
        /// <param name="IspId">The ISP Identifier of the terminal</param>
        /// <returns>A list of terminal activities for the given SitId and IspId</returns>
        List<TerminalActivityExt> GetTerminalActivityBySitId(int SitId, int IspId);

        /// <summary>
        /// Returns a list of activities for a given terminal and 
        /// activity type between a start and end time
        /// </summary>
        /// <param name="startTime">The start time of the query</param>
        /// <param name="endTime">The end time of query</param>
        /// <param name="activityType">The activity type</param>
        /// <param name="macAddress">The terminals MAC address</param>
        /// <returns>A list of terminal activities</returns>
        List<TerminalActivity> GetTerminalActivityExt(DateTime startTime, DateTime endTime, int activityType, string macAddress);


        /// <summary>
        /// Return a list of activities for a given user.
        /// </summary>
        /// <param name="userId">Userid is the unique uid</param>
        /// <returns></returns>
        List<UserActivity> GetUserActivities(string userId);

        /// <summary>
        /// Return a list of exceptions over a given period
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        List<CmtApplicationException> GetApplicationExceptions(DateTime from, DateTime to);

        /// <summary>
        /// Lists all types of terminal activity. New ones are currently only supported through autogeneration (so no description yet): see storing terminal activities
        /// </summary>
        /// <returns></returns>
        List<ActivityType> GetTerminalActivityTypes();

        /// <summary>
        /// Lists all types of users activity. New ones are currently only supported through autogeneration (so no description yet): see storing user activities
        /// </summary>
        /// <returns></returns>
        List<ActivityType> GetUserActivityTypes();

        /// <summary>
        /// Returns a DataTable of terminal activities between two dates
        /// </summary>
        /// <param name="startDate">The start date for the list</param>
        /// <param name="endDate">The end date for the list</param>
        /// <returns>A DataTable object containing the result set</returns>
        DataTable GetTermActivitiesBetweenDates(DateTime startDate, DateTime endDate);

        /// <summary>
        /// Returns a datatable with an account report for a given distributor
        /// </summary>
        /// <param name="startDate">Start date of the report</param>
        /// <param name="endDate">End date of the report</param>
        /// <param name="distid">The distributor Id</param>
        /// <returns>A datatable containing the report rows</returns>
        DataTable GetTermAccountReportForDistributor(DateTime startDate, DateTime endDate, int distid);

        /// <summary>
        /// Returns a list of tickets in the CMT database for a given MAC address
        /// </summary>
        /// <param name="macAddress">The MAC address of the terminal the tickets are for</param>
        /// <returns>A list of tickets (CMT format)</returns>
        List<CMTTicket> GetTicketsByMacAddress(string macAddress);

        /// <summary>
        /// Returns a ticket from the CMT database based on a specific ticket ID
        /// </summary>
        /// <param name="ticketId">The ID of the ticket</param>
        /// <returns>A CMT ticket object</returns>
        CMTTicket GetTicketById(string ticketId);

        /// <summary>
        /// Returns the billable to which a terminal activity type is linked
        /// </summary>
        /// <param name="TerminalActivityTypeId">The ID of the terminal activity type</param>
        /// <returns>The ID of the billable</returns>
        int GetBillableForTerminalActivityType(int TerminalActivityTypeId);

        List<EventMessage> GetLatestBarixEventMessagesByMac(string macAddress);

        List<EventMessage> GetLatestBarixEventMessagesByUser(string userId);
     }
}
