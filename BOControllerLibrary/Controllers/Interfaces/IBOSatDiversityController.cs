﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BOControllerLibrary.Model.Terminal;

namespace BOControllerLibrary.Controllers.Interfaces
{
    /// <summary>
    /// This interface describes the methods for satellite diversity management
    /// (redundant terminal methods)
    /// This method deals only with the specifics of satellite diversity: existing terminal management
    /// methods as still used for as far as possible also for redundant terminals
    /// </summary>
    public interface IBOSatDiversityController
    {
        /// <summary>
        /// Sets a terminal to active and sets the other terminal in the redundant setup to inactive
        /// </summary>
        /// <param name="macAddress">The MAC address of the terminal to be set as active</param>
        /// <returns>True if successful. Also returns true in case a non-redundant terminal is provided, or if the the terminal provided already was active</returns>
        bool SetActiveTerminal(string macAddress);

        /// <summary>
        /// Stores the switchover event in the CMT database
        /// </summary>
        /// <param name="switchOver">The details of the switchOver</param>
        /// <returns>True if successful</returns>
        bool LogSwitchOver(SwitchOver switchOver);

        /// <summary>
        /// Fetches all switchover events in the database during a give timeperiod
        /// </summary>
        /// <param name="startDate">The start date of the period</param>
        /// <param name="endDate">The end date of the period</param>
        /// <returns>A list of switchover events</returns>
        List<SwitchOver> GetAllSwitchOvers(DateTime startDate, DateTime endDate);

        /// <summary>
        /// Fetches all switchover events in the database during a give timeperiod for a specific distributor
        /// </summary>
        /// <param name="startDate">The start date of the period</param>
        /// <param name="endDate">The end date of the period</param>
        /// <param name="distributorId">The ID of the distributor</param>
        /// <returns>A list of switchover events</returns>
        List<SwitchOver> GetSwitchOversForDistributor(DateTime startDate, DateTime endDate, int distributorId);

        /// <summary>
        /// Fetches all switchover events in the database during a give timeperiod for a specific customer
        /// </summary>
        /// <param name="startDate">The start date of the period</param>
        /// <param name="endDate">The end date of the period</param>
        /// <param name="customerId">The ID of the customer</param>
        /// <returns>A list of switchover events</returns>
        List<SwitchOver> GetSwitchOversForCustomer(DateTime startDate, DateTime endDate, int customerId);

        /// <summary>
        /// Fetches all switchover events in the database during a give timeperiod
        /// </summary>
        /// <param name="macAddress">The MAC address of one of the terminals in the redundant setup</param>
        /// <param name="startDate">The start date of the period</param>
        /// <param name="endDate">The end date of the period</param>
        /// <returns>A list of switchover events</returns>
        List<SwitchOver> GetSwitchOversForTerminal(string macAddress, DateTime startDate, DateTime endDate);
    }
}
