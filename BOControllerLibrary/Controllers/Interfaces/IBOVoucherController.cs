﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using BOControllerLibrary.Model.Voucher;

namespace BOControllerLibrary.Controllers.Interfaces
{
    /// <summary>
    /// Contains the methods which need to be implemented by Voucher controllers.
    /// </summary>
    public interface IBOVoucherController
    {
        /// <summary>
        /// Returns a list of available vouchers for a given
        /// distributor.
        /// </summary>
        /// <param name="distributorId">The distributor identifier.</param>
        /// <returns>A list of available vouchers. This list can be empty.</returns>
        List<Voucher> GetAvailableVouchers(int distributorId);

        /// <summary>
        /// Returns a list of VoucherBatchInfo objects for a given distributor. 
        /// </summary>
        /// <param name="distributorId">The distributor identifier</param>
        /// <returns>A list of VoucherBatchInfo objects</returns>
        List<VoucherBatchInfo> GetAvailableVoucherBatches(int distributorId);

        /// <summary>
        /// Returns a list of VoucherBatchInfo objects for a given distributor. 
        /// </summary>
        /// <param name="distributorId">The distributor identifier</param>
        /// <returns>A list of VoucherBatchInfo objects</returns>
        List<VoucherBatchInfo> GetVoucherBatches(int distributorId);

        /// <summary>
        /// Returns a list of VoucherBatchInfo objects for a given distributor. The list contains
        /// only released voucher batches
        /// </summary>
        /// <param name="distributorId">The distributor identifier</param>
        /// <returns>A list of VoucherBatchInfo objects</returns>
        List<VoucherBatchInfo> GetReleasedVoucherBatches(int distributorId);

        /// <summary>
        /// Returns a list of vouchers for the specified Batch Id
        /// </summary>
        /// <param name="batchId">The identifier of the batch</param>
        /// <returns>A list of vouchers identifief by the given batch id.</returns>
        List<Voucher> GetVouchers(int batchId);

        /// <summary>
        /// Returns a Voucher object identified by its code.
        /// </summary>
        /// <param name="code">The 13 digit code string</param>
        /// <returns>The voucher instance corresponding with the given code or
        /// null if the voucher could not be found in the database.</returns>
        Voucher GetVoucher(string code);

        /// <summary>
        /// Returns a list of Voucher volume objects
        /// </summary>
        /// <returns></returns>
        List<Model.Voucher.VoucherVolume> GetVoucherVolumes();

        /// <summary>
        /// Returns a voucher volume object reference by its volume id
        /// </summary>
        /// <param name="volumeId">The VoucherVolumeId</param>
        /// <returns>The voucher volume object or null if not found</returns>
        VoucherVolume GetVoucherVolumeById(int volumeId);


        /// <summary>
        /// Returns the meta data of the given batch
        /// </summary>
        /// <param name="batchId">A unique batch id</param>
        /// <returns>A VoucherBatch instance or null if the batch could not be found in the database</returns>
        VoucherBatch GetVoucherBatchInfo(int batchId);

        /// <summary>
        /// Validates the voucher identified by the given code.
        /// </summary>
        /// <remarks>
        /// Validation is done without calling the AddVolume ISPIF method
        /// </remarks>
        /// <param name="code">The code of the voucher to validate</param>
        /// <param name="crc">The crc string contains an Hexadecimal number</param>
        /// <param name="macAddress">The MAC address of the terminal for which the voucher was validated</param>
        /// <returns>True if the voucher was successfully validated</returns>
        bool ValidateVoucher(string code, string crc, string macAddress);

        /// <summary>
        /// Validate the complete batch with the given batch number.
        /// </summary>
        ///  /// <remarks>
        /// Validation is done without calling the AddVolume ISPIF method
        /// </remarks>
        /// <param name="batchNumber">The batch number to validate</param>
        /// <returns>True if the batch was successfully validated</returns>
        bool ValidateBatch(int batchId);

        /// <summary>
        /// Creates a list of unique vouchers. The number of vouchers is given
        /// by the numVouchers parameter. The list is also stored in the CMT database (VoucherBase and
        /// Voucher tables)
        /// </summary>
        /// <param name="distributorId">The distributor who owns the vouchers</param>
        /// <param name="numVouchers">The number of vouchers to generate</param>
        /// <param name="UserId">Id of the system user who created the voucher batch</param>
        /// <param name="batchId">After conclusion of the method, batchId contains the new batch identifier</param>
        /// <returns>A list of Vouchers with length is numVouchers</returns>
        List<Voucher> CreateVouchers(int distributorId, int numVouchers, Guid userId, out int batchId, int volumeId);

        /// <summary>
        /// Makes a voucher which was (accidently) validated, available again.
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <param name="code">The unique voucher code</param>
        /// <returns>True if the operation succeeded</returns>
        bool ResetVoucher(string code, string crc);

        /// <summary>
        /// Releases a voucher batch
        /// </summary>
        /// <param name="batchId">The voucher batch identifier</param>
        /// <returns>True if the mehtod succeeded</returns>
        bool ReleaseVoucherBatch(int batchId);

        /// <summary>
        /// This method puts the Release flag of the VoucherBatch table
        /// for the given voucher batch id back to false
        /// </summary>
        /// <param name="batchId">The voucher batch to unrelease</param>
        /// <returns>True if the method succeeded</returns>
        bool UnreleaseVoucherBatch(int batchId);

        /// <summary>
        /// This method deletes a voucher batch if non of its vouchers where validated or released
        /// </summary>
        /// <param name="batchId">The voucher batch to be deleted</param>
        /// <returns>True if the method succeeded</returns>
        int DeleteVoucherBatch(int batchId);

        /// <summary>
        /// Returns a data set with the number of voucher created for each distributor between
        /// a begin and start date.
        /// </summary>
        /// <remarks>
        /// This method should use the cmtVoucherCreation stored procedure.
        /// </remarks>
        /// <param name="startDate">Start date of the report (this date included)</param>
        /// <param name="endDate">End date of the report (this date excluded)</param>
        /// <returns>A data set with the selected voucher batches</returns>
        DataSet GenerateVoucherReport(DateTime startDate, DateTime endDate);

        /// <summary>
        /// Returns the voucher batch for a given code and crc
        /// </summary>
        /// <param name="code">The voucher code</param>
        /// <param name="crc">The voucher crc</param>
        /// <returns>The corresponding VoucherBatch or null if the method failed</returns>
        VoucherBatch GetVoucherBatch(string code, string crc);

        /// <summary>
        /// This method returns the voucher volume code which needs to be used
        ///  in the ISPIF and NMS add volume methods.
        /// </summary>
        /// <param name="voucherVolumeId">The voucher volume Id, this value corresponds
        /// with the Id column in the VoucherVolumes table</param>
        /// <returns></returns>
        int? GetVolumeCodeById(int voucherVolumeId);

        /// <summary>
        /// This method adds a new voucher volume to the CMT database
        /// </summary>
        /// <param name="vv">The voucher volume object</param>
        /// <returns>True if successful</returns>
        bool AddVoucherVolume(VoucherVolume vv);

        /// <summary>
        /// This method updates an existing voucher volume in the CMT database
        /// </summary>
        /// <param name="vv">The voucher volume object</param>
        /// <returns>True if successful</returns>
        bool UpdateVoucherVolume(VoucherVolume vv);

        /// <summary>
        /// Creates a new voucher requests
        /// </summary>
        /// <param name="vr">The voucher request to be created</param>
        /// <returns>True if successfull</returns>
        bool CreacteVoucherRequest(VoucherRequest vr);

        /// <summary>
        /// Updates the state of a voucher requests
        /// </summary>
        /// <param name="vr">The voucher request to be updated</param>
        /// <returns>True if successfull</returns>
        bool UpdateVoucherRequest(VoucherRequest vr);

        /// <summary>
        /// Fetches a voucher request from the CMT database
        /// </summary>
        /// <param name="vrId">The ID of the voucher request</param>
        /// <returns>The voucher request object</returns>
        VoucherRequest GetVoucherRequest(int vrId);

        /// <summary>
        /// Fetches all pending voucher requests
        /// </summary>
        /// <returns>A list of voucher requests</returns>
        List<VoucherRequest> GetAllPendingVoucherRequests();

        /// <summary>
        /// Fetches all vouchers for a voucher report based on certain criteria
        /// All criteria are optional
        /// </summary>
        /// <param name="startDate">The begin date of the reporting period. The date is the date the voucher was validated</param>
        /// <param name="endDate">The end date of the reporting period. The date is the date the voucher was validated</param>
        /// <param name="batchId">The ID of the batch the vouchers belong to</param>
        /// <param name="released">True if only released vouchers should be included</param>
        /// <returns>A list of vouchers</returns>
        List<Voucher> GetVouchersForReport(DateTime startDate, DateTime endDate, int batchId, bool released);

        /// <summary>
        /// Allows a distributor to enter his retail price for vouchers
        /// </summary>
        /// <param name="distributorId">The ID of the distributor setting the price</param>
        /// <param name="voucherVolumeId">The ID of the voucher volume for which the price is set</param>
        /// <param name="unitPriceUSD">The unit price in Euros</param>
        /// <param name="unitPriceEUR">The unit price in US Dollars</param>
        /// <returns>True if successful</returns>
        bool SetVoucherRetailPrice(int distributorId, int voucherVolumeId, double unitPriceUSD, double unitPriceEUR);

        /// <summary>
        /// Fetches the voucher retail prices as set by a specific distributor
        /// </summary>
        /// <param name="distributorId">The ID of the distributor</param>
        /// <returns>A list of voucher volume objects containing the ID of the voucher volume and the retail prices of the distributor</returns>
        List<VoucherVolume> GetVoucherRetailPricesForDistributor(int distributorId);

        /// <summary>
        /// Flags a voucher batch as paid in the CMT database
        /// </summary>
        /// <param name="batchId">The ID of the batch to be flagged</param>
        /// <returns>True if successful</returns>
        bool FlagVoucherBatchAsPaid(int batchId);

        /// <summary>
        /// Flags a voucher batch as paid in the CMT database
        /// </summary>
        /// <param name="batchId">The ID of the batch to be flagged</param>
        /// <param name="paymentMethod">Payment Method</param>
        /// <returns>True if successful</returns>
        bool MarkVoucherBatchAsPaid(int batchId, string paymentMethod);

        /// <summary>
        /// Flags a voucher batch as unpaid in the CMT database
        /// </summary>
        /// <param name="batchId">The ID of the batch to be flagged</param>
        /// <returns>True if successful</returns>
        bool FlagVoucherBatchAsUnPaid(int batchId);

        /// <summary>
        /// Returns batchId for activated voucher on a specific terminal at determined time
        /// </summary>
        /// <param name="macAddress">The mac address of terminal where voucher was validated</param>
        /// <param name="vdate">validation date when voucher was validated</param>
        /// <returns>batchid</returns>
        int getBatchIDforValidatedVoucher(string macAddress, DateTime vdate);


        /// <summary>
        /// Returns voucher batches according for a specific distributor if it is defined otherwise it displays the the vouchers for all distributor for last 3 month.
        /// </summary>
        /// <param name="distID">Distributor ID could be null then bathced for all distributors for the last 3 months will be fetched</param>
        /// <param name="batchesfrom">the report startdate</param>
        /// <param name="batchesto">the report endDate</param>
        /// <returns>List of voucher batched</returns>
        List<VoucherBatchInfo> getVouchersBatchesReport(int distID,int paid, DateTime? batchesfrom, DateTime? batchesto);



        /// <summary>
        /// Returns number of available vouchers for specific batch.
        /// </summary>
        /// <param name="batchID">batchid for the inquired batch</param>
        /// <returns>number of vouchers</returns>
        int getAvailableVouchersforbatch(int batchID);

        List<DistVoucher> GetDistVouchersforVV(int volumeId);

        List<DistVoucher> GetVoucherVolumesforDist(int distId);

        bool DistVoucherExists(int distId, int vvid);

        VoucherVolume GetVoucherVolumeByIdForDistributor(int volumeId, int distId);

        bool AddDistVoucher(DistVoucher dv);

        bool UpdateDistVoucher(DistVoucher dv);
        DistVoucher GetDistVoucher(int distVoucherId);
    }
}
