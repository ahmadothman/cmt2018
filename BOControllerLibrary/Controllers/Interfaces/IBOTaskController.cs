﻿using System.Collections.Generic;
using BOControllerLibrary.Model.Tasks;

namespace BOControllerLibrary.Controllers.Interfaces
{
    public interface IBOTaskController
    {
        /// <summary>
        /// Return a list of completed tasks, in descending order
        /// </summary>
        /// <returns></returns>
        List<Task> GetCompletedTasks();

        /// <summary>
        /// Return a list of open tasks, in descending order
        /// </summary>
        /// <returns></returns>
        List<Task> GetOpenTasks();

        /// <summary>
        /// Insert or update a task
        /// </summary>
        /// <param name="task"></param>
        /// <returns></returns>
        bool UpdateTask (Task task);

        /// <summary>
        /// Get a list of all tasktypes
        /// </summary>
        /// <returns></returns>
        List<TaskType> GetTaskTypes();

        /// <summary>
        /// Insert or update a new tasktype
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        bool UpdateTaskType(TaskType type);

        /// <summary>
        /// Returns the task corresponding with the
        /// given task id.
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns>A Task object or null otherwise</returns>
        Task GetTask(int taskId);

        /// <summary>
        /// Returns the TaskType corresponding with the given Id
        /// </summary>
        /// <param name="taskTypeId"></param>
        /// <returns>A Task object or null otherwise</returns>
        TaskType GetTaskType(int taskTypeId);
   
    }
}
