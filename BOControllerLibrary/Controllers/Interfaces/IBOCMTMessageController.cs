﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BOControllerLibrary.Model.CMTMessages;

namespace BOControllerLibrary.Controllers.Interfaces
{
    /// <summary>
    /// This component handles an internal messaging system for the CMT
    /// Messages can be sent from the NOCSA to a specific user, all users of
    /// a specific role or to everyone.
    /// The messages will be displayed in a list when the user logs on.
    /// </summary>
    public interface IBOCMTMessageController
    {
        /// <summary>
        /// This method submits a new message to the CMT database
        /// </summary>
        /// <param name="message">The CMT message</param>
        /// <returns>True if succesful</returns>
        bool SubmitCMTMessage(CMTMessage message);

        /// <summary>
        /// This method submits changes to a message in the CMT database
        /// </summary>
        /// <param name="message">The CMT message</param>
        /// <returns>True if succesful</returns>
        bool UpdateCMTMessage(CMTMessage message);

        /// <summary>
        /// Retrieves the messages for a user
        /// First messages are retrieved based on the user ID (messages sent specifically to user)
        /// Then messages are retrieved based on the user role (messages sent to role)
        /// Finally messages sent to everyone are retrieved
        /// </summary>
        /// <param name="userId">The ID of the user, can be distributor ID, VNO ID etc</param>
        /// <param name="role">The role of the user, eg distributor or organization</param>
        /// <returns>A list of CMT messages, can be empty</returns>
        List<CMTMessage> GetCMTMessagesForUser(string userId, string role);

        /// <summary>
        /// Retrieves all the messages in the database
        /// This method is used for the admin overview
        /// </summary>
        /// <returns>A list of CMT messages, can be empty</returns>
        List<CMTMessage> GetAllCMTMessages();

        /// <summary>
        /// Retrieves a specific message from the database
        /// </summary>
        /// <param name="messageId">The ID of the message</param>
        /// <returns>The message object</returns>
        CMTMessage GetCMTMessage(string messageId);

        /// <summary>
        /// Method for retrieving an attachment from the database and putting it into the memorystream
        /// so that it can be used
        /// </summary>
        /// <param name="messageId">The ID of the message for which the attachment is retrieved</param>
        /// <returns>The image</returns>
        Byte[] GetAttachment(string messageId);
    }
}
