﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BOControllerLibrary.Model;

namespace BOControllerLibrary.Controllers.Interfaces
{
    /// <summary>
    /// This controller manages Bandwidth on Demand bookings, both in the CMT database
    /// as well as over the SatLink Manager interface provided by SES
    /// </summary>
    public interface IBOSatLinkManagerController
    {
        /// <summary>
        /// Gets the details of a reservation from the CMT database
        /// </summary>
        /// <param name="cmtId">The CMT ID of the reservation</param>
        /// <returns>An SLMBooking object</returns>
        SLMBooking GetSLMBookingById(int cmtId);

        /// <summary>
        /// Gets the details of the reservations requested by a specific distributor
        /// </summary>
        /// <param name="distributorId">The ID of the distributor in the CMT</param>
        /// <returns>A list of SLMBooking objects</returns>
        List<SLMBooking> GetSLMBookingsByDistributor(int distributorId);

        /// <summary>
        /// Gets the details of the reservations requested by a specific distributor
        /// </summary>
        /// <param name="customerId">The ID of the customer in the CMT</param>
        /// <returns>A list of SLMBooking objects</returns>
        List<SLMBooking> GetSLMBookingsByCustomer(int customerId);

        /// <summary>
        /// Fetches reservations from the CMT database based on status
        /// </summary>
        /// <param name="status">The status name</param>
        /// <returns>A list of SLMBooking objects</returns>
        List<SLMBooking> GetSLMBookingsByStatus(string status);

        /// <summary>
        /// Fetches reservations from the CMT database which are active during the given period
        /// </summary>
        /// <param name="startDate">The first date in the period</param>
        /// <param name="endDate">The last date in the period</param>
        /// <returns>A list of SLMBooking objects</returns>
        List<SLMBooking> GetSLMBookingsBetweenDates(DateTime startDate, DateTime endDate);

        /// <summary>
        /// Updates a reservation in the CMT database
        /// </summary>
        /// <param name="booking">The SLMBooking object</param>
        /// <returns>True if successful</returns>
        bool UpdateSLMBooking(SLMBooking booking);

        /// <summary>
        /// Flags a reservation as deleted in the CMT database
        /// </summary>
        /// <param name="CMTId">The CMT ID of the resevation</param>
        /// <returns>True if successful</returns>
        bool DeleteSLMBooking(int CMTId);

        /// <summary>
        /// Creates an SLM booking in the hub
        /// </summary>
        /// <param name="CMTId">The CMT ID of the reservation</param>
        /// <returns>A text string with a specific error message in case the booking failed</returns>
        string CreateSLMBookingInHub(int CMTId);

        /// <summary>
        /// This method will cancel a future booking in the SLM server
        /// /// </summary>
        /// <param name="booking">An SLMBoooking object</param>
        /// <returns>True if successfult</returns>
        bool CancelSLMBooking(SLMBooking booking);

        /// <summary>
        /// This method starts a booking in the hub and sets the correct parameters in the CMT and the NMS
        /// </summary>
        /// <param name="booking">An SLMBoooking object</param>
        /// <returns>True if successfult</returns>
        bool StartBooking(SLMBooking booking);

        /// <summary>
        /// This method resets the SLA in the NMS once the SLM session is finised
        /// </summary>
        /// <param name="booking"></param>
        /// <returns></returns>
        bool StopBooking(SLMBooking booking);

        /// <summary>
        /// Fetches bookings from the CMT with a given start time
        /// </summary>
        /// <param name="startTime">The start time</param>
        /// <returns>A list of SLMBookings</returns>
        List<SLMBooking> GetBookingsByStartTime(DateTime startTime);
    }
}
