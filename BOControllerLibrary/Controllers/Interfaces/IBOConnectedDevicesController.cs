﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BOControllerLibrary.Model.Terminal;

namespace BOControllerLibrary.Controllers.Interfaces
{
    /// <summary>
    /// Contains the methods which are necessary for connected devices
    /// </summary>
    public interface IBOConnectedDevicesController
    {
        /// <summary>
        /// Adds a new connected device to a terminal
        /// </summary>
        /// <param name="device">The device object which includes the terminal identifier</param>
        /// <returns>True if successful</returns>
        bool AddDeviceToTerminal(ConnectedDevice device);

        /// <summary>
        /// Removes a connected device from a terminal and the CMT database
        /// </summary>
        /// <param name="device">The device object which includes the terminal identifier</param>
        /// <returns>True if successful</returns>
        bool RemoveDeviceFromTerminal(ConnectedDevice device);

        /// <summary>
        /// Returns a list of devices that are connected to a specific terminal
        /// </summary>
        /// <param name="macAddress">The MAC address of the terminal</param>
        /// <returns>A list of connected devices</returns>
        List<ConnectedDevice> GetDevicesForTerminal(string macAddress);

        /// <summary>
        /// Returns the details of a specific connected device
        /// </summary>
        /// <param name="deviceId">The ID of the connected device</param>
        /// <returns>The connect device object</returns>
        ConnectedDevice GetDevice(string deviceId);

        /// <summary>
        /// Updates the parameters of an existing connected device
        /// </summary>
        /// <param name="device">The device to be updated, including the new parameters</param>
        /// <returns>True if successful</returns>
        bool UpdateDevice(ConnectedDevice device);

        /// <summary>
        /// Retrieves a list of all connected devices in the CMT database
        /// </summary>
        /// <returns>The list of devices</returns>
        List<ConnectedDevice> GetAllDevices();

        /// <summary>
        /// Retrieves a list of all connected device types in the CMT database
        /// </summary>
        /// <returns>The list of device types</returns>
        List<ConnectedDeviceType> GetAllConnectedDeviceTypes();

        /// <summary>
        /// Returns a list of devices that are connected to a specific terminal
        /// and are of a specific typ
        /// </summary>
        /// <param name="terminalMAC">The MAC address of the terminal</param>
        /// <param name="deviceTypeID">The ID of the device type</param>
        /// <returns>A list of connected devices</returns>
        List<ConnectedDevice> GetConnectedDevicesForTerminalByType(string terminalMAC, int deviceTypeID);
    }
}
