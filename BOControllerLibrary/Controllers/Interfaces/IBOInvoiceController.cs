﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using BOControllerLibrary.Model.Invoices;

namespace BOControllerLibrary.Controllers.Interfaces
{
    /// <summary>
    /// This interface describes the methods for retrieving Invoice headers and
    /// details from the invoice database.
    /// </summary>
    public interface IBOInvoiceController
    {
        /// <summary>
        /// Returns the invoices for a given distributor, year and month
        /// </summary>
        /// <param name="DistributorId">The distributor Id</param>
        /// <param name="Year">The invoice year</param>
        /// <param name="Month">The invoice month</param>
        /// <returns>A list of invoice headers for a specific distirbutor</returns>
        List<InvoiceHeader> GetInvoicesForDistributor(int distributorId, int year, int Month);

        /// <summary>
        /// Returns the invoice lines for a distributor
        /// </summary>
        /// <param name="distributorId">The distributor Id</param>
        /// <param name="year">The invoice year</param>
        /// <param name="month">The invoice month</param>
        /// <returns></returns>
        DataTable GetInvoiceLinesForDistributor(int distributorId, int year, int month);

        /// <summary>
        /// Returns the list of detail lines for given invoice
        /// </summary>
        /// <param name="Year">Invoice year</param>
        /// <param name="num">Invoice number</param>
        /// <returns>A list of invoice lines</returns>
        List<InvoiceLine> GetInvoiceLines(int year, int num);

        /// <summary>
        /// Returns the invoice header of a given invoice
        /// </summary>
        /// <param name="Year"></param>
        /// <param name="num"></param>
        /// <returns></returns>
        InvoiceHeader GetInvoice(int year, int num);

        /// <summary>
        /// Get all invoices for a given year and month
        /// </summary>
        /// <param name="year"></param>
        /// <param name="Month"></param>
        /// <returns>A list of invoices for a given year and month</returns>
        List<InvoiceHeader> GetInvoices(int year, int Month);
    }
}
