﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BOControllerLibrary.Model.Documents;

namespace BOControllerLibrary.Controllers.Interfaces
{
    /// <summary>
    /// The DocumentController contains methods which support the management of the 
    /// document archive.
    /// </summary>
    public interface IBODocumentController
    {
        /// <summary>
        /// Creates a new document in the database. 
        /// </summary>
        /// <remarks>
        /// The name of the document should be unique. Duplicate names will
        /// result in an exception.
        /// </remarks>
        /// <param name="document">The document Dto</param>
        /// <returns>The id of the newly created document</returns>
        /// <exception cref=">BOControllerLibrary.Exceptions.DuplicateDocumentException">Thrown if the the name of the document already exists</exception>
        long CreateNewDocument(Document document);

        /// <summary>
        /// Updates the document information
        /// </summary>
        /// <remarks>
        /// It is forbidden to change the name of the document! Changing the name of the
        /// document will result in an exception.
        /// </remarks>
        /// <param name="document">The document Dto</param>
        /// <returns>True if the document could be updated</returns>
        /// <exception cref=">BOControllerLibrary.Exceptions.DuplicateDocumentException">Thrown if the name of and existing document has been changed</exception>
        Boolean UpdateDocument(Document document);

        /// <summary>
        /// This deletes the document information AND the document from the server harddisk.
        /// </summary>
        /// <param name="document">Document Dto</param>
        /// <returns>True if the action succeeded, false otherwise</returns>
        Boolean DeleteDocument(long documentId);

        /// <summary>
        /// Retrieves the document Dto from the database
        /// </summary>
        /// <remarks>
        /// The return value of this method could be null! If you need to check if the document already exists
        /// in the database it's better to use the <b>AlreadyExists</b> method.
        /// </remarks>
        /// <param name="documentId">The unique identifier of the document</param>
        /// <returns>The document Dto or null if not found</returns>
        /// <exception cref=">BOControlLibrary.Exceptions.DocumentNotFoundException">Thrown if the document is not found</exception>
        Document GetDocument(long documentId);

        /// <summary>
        /// Checks if a document with the given name already exists in the database
        /// </summary>
        /// <param name="documentName">The name of the document</param>
        /// <returns>True if the document exists in the database</returns>
        Boolean AlreadyExists(string documentName);

        /// <summary>
        /// Returns true if the name of the document has changed
        /// </summary>
        /// <remarks>
        /// In case the document does not exist yet, the method throws a a DocumentNotFoundException. So do not use this
        /// method to check if a document exists in the database. Use the <b>GetDocument</b> method instead.
        /// </remarks>
        /// <param name="document">the Document Dto</param>
        /// <returns>Returns true if the name specified by the document Dto is different from the
        /// name in the database</returns>
        /// <exception cref=">BOControlLibrary.Exceptions.DocumentNotFoundException">Thrown if the document is not found</exception>
        Boolean DocumentNameHasChanged(Document document);

        /// <summary>
        /// Retrieves the document by its unique name
        /// </summary>
        /// <param name="fileName">The filename of the document</param>
        /// <returns>A valid document instance or null if not found</returns>
        Document GetDocumentByName(string fileName);

        /// <summary>
        /// Returns a list of all methods
        /// </summary>
        /// <returns>A LIst of Documents</returns>
        List<Document> GetAllDocuments();

        /// <summary>
        /// Returns a list of documents which belong either to everyone or the role
        /// of the recipient or the specific recipient.
        /// </summary>
        /// <param name="recipient">The reference of the recipient (Distributor id etc.)</param>
        /// <returns>A list of selected documents. This list can be empty.</returns>
        List<Document> GetDocumentsForRecipient(String recipient, String role);
            

    }
}
