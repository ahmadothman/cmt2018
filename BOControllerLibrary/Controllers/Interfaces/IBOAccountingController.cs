﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using BOControllerLibrary.Controllers.Interfaces;
using BOControllerLibrary.Model;
using BOControllerLibrary.Model.Distributor;
using BOControllerLibrary.Model.ISP;
using BOControllerLibrary.Model.Organization;
using BOControllerLibrary.Model.Terminal;
using BOControllerLibrary.Model.User;
using BOControllerLibrary.Model.VNO;
using BOControllerLibrary.Model.Invoices;


namespace BOControllerLibrary.Controllers
{
    /// <summary>
    /// This component is responsible for the accounting Use Cases specified by UC-M-270 and others.  
    /// The component is mainly used for accounting purpose such a volume consumption reporting, integration with the billing system etc
    /// </summary>
    public interface IBOAccountingController 
    {
        /*
         *  •	Return a list of activities and volume consumption for a specific terminal over a specific period.
            •	Return a list of quality parameters (CNo values) for a specific terminal over a given period.
         */

        /// <summary>
        /// Returns a list of terminals which are serviced by a Distributor. The UserId in this case is taken from the aspnet_Users table.
        /// </summary>
        /// <param name="distributorId"></param>
        /// <returns></returns>
        List<Terminal> GetTerminalsByDistributor(string distributorId);

        /// <summary>
        /// Returns a list of terminals which are serviced by a Distributor. The distributorid is taken from the distributors table.
        /// </summary>
        /// <param name = "distributorId"></param>
        /// <returns></returns>
        List<Terminal> GetTerminalsByDistributorId(int distributorId);

        /// <summary>
        /// CMT-95
        ///   Returns a list of terminals which are serviced by a Distributor without decommisioned or deleted Terminals. The UserId in this case is taken from the aspnet_Users table.
        /// </summary>
        /// <param name = "distributorUserId"></param>
        /// <returns></returns>
        List<Terminal> GetTerminalsByDistributorForDistributor(string distributorUserId);

        /// <summary>
        /// Returns a list of organizations
        /// </summary>
        /// <param name="distributorId">The UID of the distributor</param>
        /// <returns></returns>
        List<Organization> GetOrganisationsByDistributor(string distributorId);

        /// <summary>
        /// Returns a list of organizations
        /// </summary>
        /// <param name="distributorId">The UID of the distributor</param>
        /// <returns></returns>
        List<Organization> GetCustomerVNOsByDistributor(string distributorId);

        /// <summary>
        /// Returns a list of organizations
        /// </summary>
        /// <returns></returns>
        List<Organization> GetCustomerVNOs();

        /// <summary>
        /// Fetches the list of organizations for a specific distributor
        /// </summary>
        /// <param name = "distributorId">The ID of the distributor</param>
        /// <returns>A list of organizations</returns>
        List<Organization> GetOrganisationsByDistributorId(string distributorId);

        /// <summary>
        /// Returns the terminal operated by a single End User
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Terminal GetUserTerminal(int endUserId);

        /// <summary>
        /// Returns the distributor which is linked to the user
        /// </summary>
        /// <param name="userId">The userid here is the UID</param>
        /// <returns></returns>
        Distributor GetDistributorForUser(string userId);

        /// <summary>
        /// Returns a list of terminals which are linked to the organization.
        /// </summary>
        /// <param name="organizationId">This is the orgid from the organizations table</param>
        /// <returns></returns>
        List<Terminal> GetTerminalsByOrganization(int organizationId);

        /// <summary>
        /// Returns a list of terminals which are linked to an SLA.
        /// </summary>
        /// <param name="slaId">The SLA identifier for which</param>
        /// <returns>A list of terminals with that specific SLA</returns>
        List<Terminal> GetTerminalsBySla(int slaId);

        /// <summary>
        /// Returns the details of a terminal by its SitId value. These are the values which are stored in the CMT Database
        /// 
        /// developer notes: it's possible that we will need to use a ITerminalDetails object here instead of the ITerminal to reduce amount of traffic
        /// </summary>
        /// <param name="sitId"></param>
        /// <returns></returns>
        Terminal GetTerminalDetailsBySitId(int sitId, int ispId);

        /// <summary>
        /// Returns the terminal details by its IP address
        /// 
        /// developer notes: it's possible that we will need to use a ITerminalDetails object here instead of the ITerminal to reduce amount of traffic
        /// </summary>
        /// <param name="ipAddress"></param>
        /// <returns></returns>
        Terminal GetTerminalDetailsByIp(string ipAddress);

        /// <summary>
        /// Returns the terminal details by the terminal MAC address
        /// 
        /// developer notes: it's possible that we will need to use a ITerminalDetails object here instead of the ITerminal to reduce amount of traffic
        /// </summary>
        /// <param name="macAddress"></param>
        /// <returns></returns>
        Terminal GetTerminalDetailsByMAC(string macAddress);

        /// <summary>
        /// Returns a list of terminal matching a complete or partly given MAC address. 
        /// </summary>
        /// <param name="macAddress">A complete or partly matching mac address</param>
        /// <returns>A list with matching terminals, this list can be empty!</returns>
        List<Terminal> GetTerminalsByMac(string macAddress);

        /// <summary>
        /// Returns a terminal list corresponding 
        /// </summary>
        /// <param name="statusId">The status identifier</param>
        /// <returns>A list of matching terminals</returns>
        List<Terminal> GetTerminalsByStatus(int statusId);

        /// <summary>
        /// Returns a list of terminals corresponding to the given criterion.
        /// </summary>
        /// <param name="dt">The matching DateTime value</param>
        /// <param name="criterion">Determines the relation of the terminals ExpiryDate to the
        /// given date.
        /// <ul>
        ///     <li>Equals</li>
        ///     <li>Less then (Earlier)</li>
        ///     <li>More then (Later)</li>
        ///     <li>Less or Equal</li>
        ///     <li>More or Equal</li>
        /// </ul>
        /// </param>
        /// <returns>A list of matching terminals</returns>
        List<Terminal> GetTerminalsByExpiryDate(DateTime dt, string criterion);

        /// <summary>
        /// Returns a list of terminals according to the list given by the advanced
        /// query parameters tarnsport object.
        /// </summary>
        /// <param name="queryParams"></param>
        /// <returns></returns>
        List<Terminal> GetTerminalsWithAdvancedQuery(AdvancedQueryParams queryParams);

        /// <summary>
        /// Returns the terminals with an adminstatus set to Request
        /// </summary>
        /// <returns>A list with terminal requests. This list can be empty</returns>
        List<Terminal> GetTerminalRequests();
     
        /// <summary>
        /// Returns a Distributor record defined by its DistributorId
        /// </summary>
        /// <param name="distributorId"></param>
        /// <returns></returns>
        Distributor GetDistributor(int distributorId);

        /// <summary>
        /// Returns an organization by the Organization ID.
        /// </summary>
        /// <param name="organizationId"></param>
        /// <returns></returns>
        Organization GetOrganization(int organizationId);

        /// <summary>
        /// Returns an EndUser by the given UserId. 
        /// This UserId is taken from the apsnet_Users table. 
        /// If the user has an EndUser role, this method returns the details of the EndUser. 
        /// The method uses theEndUserUsers link table
        /// 
        /// If the user has no EndUser role, we'll return null.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        EndUser GetEndUser(string userId);

        /// <summary>
        /// Returns a list of dates of when manual FUP Resets were made between a selected time period
        /// </summary>
        /// <param name="SitId">SitId</param>
        /// <param name="IspId">IspId</param>
        /// <param name="startDate">Beginning of time interval</param>
        /// <param name="endDate">End of time interval</param>
        /// <returns></returns>
        List<DateTime> GetTerminalManualFupResets(int SitId, int IspId, DateTime startDate, DateTime endDate);

        /// <summary>
        /// Returns a list of all added voucher volume events made between a selected time period
        /// </summary>
        /// <param name="SitId">SitId</param>
        /// <param name="IspId">IspId</param>
        /// <param name="startDate">Beginning of time interval</param>
        /// <param name="endDate">End of time interval</param>
        /// <returns></returns>
        List<DateTime> GetTerminalVolumeVoucherAdds(int SitId, int IspId, DateTime startDate, DateTime endDate);

        /// <summary>
        /// Returns a list of a terminal's activations
        /// </summary>
        /// <param name="SitId">SitId</param>
        /// <param name="IspId">IspId</param>
        /// <returns></returns>
        List<DateTime> GetTerminalActivations(int SitId, int IspId);

        /// <summary>
        /// Returns a datatable with the account rows.
        /// </summary>
        /// <remarks>
        /// Use this datatable as a source for Grids etc.
        /// </remarks>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        DataTable GetAccountReport(DateTime startDate, DateTime endDate);

        /// <summary>
        /// Similar to the GetAccount report method but returns a list for
        /// the given DistributorId
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="DistributorId"></param>
        /// <returns></returns>
        DataTable GetAccountReportByDistributor(DateTime startDate, DateTime endDate, int DistributorId);
 
        /// <summary>
        /// Updates EndUser data in the EndUser table. The method should return false if the update fails. 
        /// If the EndUser does not exist yet in the EndUser table a new record MUST be inserted
        /// </summary>
        /// <param name="user"></param>
        /// <returns>true if the operation succeeded, false if it didn't</returns>
        bool UpdateEndUser(EndUser userData);

        int UpdateEndUser2(EndUser userData);

        /// <summary>
        /// Update terminal data in the terminal table. The method returns false if the update fails. 
        /// If the Terminal does not exist yet in the Terminal table a new record MUST be inserted.
        /// </summary>
        /// <param name="terminalData"></param>
        /// <returns></returns>
        bool UpdateTerminal(Terminal terminalData);

        /// <summary>
        /// Updates an organization in the Organization table. Returns false if the update fails. 
        /// If the Organization does not exist yet in the Organization table a new record MUST be inserted
        /// </summary>
        /// <param name="organizatonData"></param>
        /// <returns></returns>
        bool UpdateOrganization(Organization organizatonData);

        /// <summary>
        /// Updates a distributor in the distributor database. The method returns false if the update fails. 
        /// If the Distributor does not exist yet in the Distributor table a new record MUST be inserted.
        /// </summary>
        /// <param name="distributorData"></param>
        /// <returns></returns>
        bool UpdateDistributor(Distributor distributorData);

        /// <summary>
        /// Updates ISP data in the ISP table. The method returns false if the method fails. 
        /// If the ISP does not exist yet in the ISP table a new record MUST be inserted.
        /// </summary>
        /// <param name="ispData"></param>
        /// <returns></returns>
        bool UpdateIsp(Isp ispData);

        /// <summary>
        /// Returns an ISP record by its ISP id.
        /// </summary>
        /// <param name="ispId"></param>
        /// <returns>The selected ISP or null if not found</returns>
        Isp GetISP(int ispId);

        /// <summary>
        /// This method returns a list of all Edge ISPs stored in the ISP table.
        /// </summary>
        /// <returns>A list of Edge ISPs. This list can be empty</returns>
        List<Isp> GetEdgeISPs();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="distributorId"></param>
        /// <param name="macAddress"></param>
        /// <returns></returns>
        bool IsTermOwnedByDistributor(int distributorId, string macAddress);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="organizationId"></param>
        /// <param name="macAddress"></param>
        /// <returns></returns>
        bool IsTermOwnedByOrganization(int organizationId, string macAddress);

        // newly added methods according to the boaccountingcontroller specification on 29/11/2011

        /// <summary>
        /// Get all ISP's
        /// </summary>
        /// <returns></returns>
        List<Isp> GetIsps();

        /// <summary>
        /// Get All distributors
        /// </summary>
        /// <returns></returns>
        List<Distributor> GetDistributors();

        /// <summary>
        /// Get all organizations
        /// </summary>
        /// <returns></returns>
        List<Organization> GetOrganizations();

        /// <summary>
        /// get all terminals. 
        /// note: should we not include paging here?
        /// </summary>
        /// <returns></returns>
        List<Terminal> GetTerminals();

        /// <summary>
        /// Get a list of all terminal statuses
        /// </summary>
        /// <returns></returns>
        List<TerminalStatus> GetTerminalStatus();

        /// <summary>
        /// updateTerminalStatus (insert if not exist, update otherwise)
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        bool UpdateTerminalStatus(TerminalStatus status);

        /// <summary>
        /// Get all available service packs/levels
        /// </summary>
        /// <returns></returns>
        List<ServiceLevel> GetServicePacks();

        /// <summary>
        ///   Get all available service packs/levels for a specific VNO
        /// </summary>
        /// <param name="VNOId">The ID of the VNO</param>
        /// <returns>A list of service packs</returns>
        List<ServiceLevel> GetServicePacksForVNO(int VNOId);

        /// <summary>
        ///   Get all available service packs/levels not assigned to a VNO by ISP ID
        /// </summary>
        /// <param name="ispId">The ID of the ISP</param>
        /// <returns>A list of service packs</returns>
        List<ServiceLevel> GetServicePacksForAllVNOsByISP(int ispId);

        /// <summary>
        ///   Get all available service packs/levels not assigned to a VNO
        /// </summary>
        /// <returns>A list of service packs</returns>
        List<ServiceLevel> GetServicePacksForAllVNOs();

        /// <summary>
        /// Returns the ServiceLevel object corresponding with the given 
        /// servicePackId
        /// </summary>
        /// <param name="servicePackId">The unique ServicePack identifier</param>
        /// <returns>The corresponding ServicePack or null if not found</returns>
        ServiceLevel GetServicePack(int servicePackId);

        /// <summary>
        /// Returns the ServiceLevel object corresponding with the given 
        /// service pack name
        /// </summary>
        /// <param name="servicePackName">The name of the service pack</param>
        /// <returns>The corresponding ServicePack or null if not found</returns>
        ServiceLevel GetServicePackByName(string servicePackName);

        /// <summary>
        /// Update or Insert a service pack/level
        /// </summary>
        /// <param name="level"></param>
        /// <returns></returns>
        bool UpdateServicePack(ServiceLevel level);

        /// <summary>
        /// Returns the pricing of an activity type as a float
        /// </summary>
        /// <remarks>
        /// If the activity has not price setting the value resturned is 0.0
        /// </remarks>
        /// <param name="activityType">The activity type identifier</param>
        /// <returns>The activity type price as a float</returns>
        float GetActivityTypePrice(int activityType);


        /// <summary>
        /// Deletes a terminal using its macaddress. 
        /// </summary>
        /// <param name="macAddress"></param>
        /// <returns>true if the deleting was succesful, false if the operation failed.</returns>
        bool DeleteTerminal(string macAddress);

        /// <summary>
        /// Delete an end user from the EndUser table
        /// </summary>
        /// <param name="endUserId">The end user identifier to delete</param>
        /// <returns>True if the action succeeded, false otherwise</returns>
        bool DeleteEndUser(int endUserId);

        /// <summary>
        /// Deletes a distributor from the Distributor table
        /// </summary>
        /// <param name="distributorId">The distributor identifier to delete</param>
        /// <returns>True if the action succeeded, false otherwise</returns>
        bool DeleteDistributor(int distributorId);

        /// <summary>
        /// Deletes an organization from the Organizations table
        /// </summary>
        /// <param name="organizationId">The id of the organization to delete</param>
        /// <returns>True if the action succeeded, false otherwise</returns>
        bool DeleteOrganizations(int organizationId);

        /// <summary>
        /// Delete a service pack
        /// </summary>
        /// <param name="slaId">The identifier of the service pack that should be deleted</param>
        /// <returns>True if the deletion was succesfull</returns>
        bool DeleteServicePack(int slaId);
       
        /// <summary>
        /// Adds an ISP to a distributor
        /// </summary>
        /// <remarks>
        /// This method acts on the ISPDistributors table
        /// </remarks>
        /// <param name="distributorId">The distributor identifier</param>
        /// <param name="ispId">The ISP identifier</param>
        /// <returns>True if the ISP was successfully added to the Distributor</returns>
        bool AddISPToDistributor(int distributorId, int ispId);

        /// <summary>
        /// This method de-allocates an ISP from a distributor
        /// </summary>
        /// <remarks>
        /// The corresponding tupple distributorId - IspId is removed from the ISPDistributors
        /// table.
        /// </remarks>
        /// <param name="distributorId">The distributor identifier</param>
        /// <param name="ispId">The ISP identifier</param>
        /// <returns>True if the de-allocation succeeded</returns>
        bool RemoveISPFromDistributor(int distributorId, int ispId);

        /// <summary>
        /// Returns a list of ISPs allocated to a distributor
        /// </summary>
        /// <remarks>
        /// This method uses the ISPDistributors table
        /// </remarks>
        /// <param name="distributorId">The target distributor identifier</param>
        /// <returns>A list of ISPs, this list can be empty</returns>
        List<Isp> GetISPsForDistributor(int distributorId);


        // new volume information part

        /// <summary>
        /// Retrieves the volume information with the most recent timestamp
        /// Fetches data from vw_volume_detail (VolumeHistoryDetailImport)
        /// </summary>
        /// <param name="sitId"></param>
        /// <returns></returns>
        VolumeInformationTime GetLastRealTimeVolumeInformation(int sitId, int ispId);

        /// <summary>
        /// Retrieves the accumulated volume information with the most recent timestamp
        /// Fetches data from vw_volume_full (volumehistoryimport)
        /// </summary>
        /// <param name="sitId"></param>
        /// <returns></returns>
        VolumeInformationTime GetLastAccumulatedVolumeInformation(int sitId, int ispId);

        /// <summary>
        /// Retrieves the accumulated return and forward volume information with the 
        /// most recent timestamp.
        /// </summary>
        /// <remarks>
        /// These values are retrieved from the view which holds the realtime updated 
        /// volume consumption information. The view used is vw_volume_detail2.
        /// </remarks>
        /// <param name="sitId">The Site Identifier</param>
        /// <param name="ispId">The ISP Identifier</param>
        /// <returns>
        /// The VolumeInformationTime object which contains the latest forward and return
        /// accumulated values from the realtime volume consumption table
        /// </returns>
        VolumeInformationTime GetLastRTAccumulatedVolumeInformation(int sitId, int ispId);

        /// <summary>
        /// Get the last High Priority volume consumption value reported for a sit Id
        /// </summary>
        /// <param name="sitid"></param>
        /// <param name="ispId"></param>
        /// <returns></returns>
        VolumeInformationTime GetLastPriorityVolume(int sitid, int ispId);

        /// <summary>
        /// Fetches from the vw_volume_detail view (VolumeHistoryDetailImport)
        /// </summary>
        /// <param name="sitId"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        List<VolumeInformationTime> GetRealTimeVolumeInformation(int sitId, DateTime start, DateTime end, int ispId);

        /// <summary>
        /// Fetches accumulated data from: VolumeHistoryDetailImport
        /// </summary>
        /// <param name="sitId"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="ispId"></param>
        /// <returns>The 30 minutes interval information for the given sitId</returns>
        List<VolumeInformationTime> GetAccumulatedVolumeInformationFromRT(int sitId, DateTime start, DateTime end, int ispId);

        /// <summary>
        /// Fetches from the vw_volume_full view (volumehistoryimport)
        /// </summary>
        /// <param name="sitId"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        List<VolumeInformationTime> GetAccumulatedVolumeInformation(int sitId, DateTime start, DateTime end, int ispId);

        /// <summary>
        /// Returns a list of high priority volume consumption data points
        /// </summary>
        /// <param name="sitId"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        List<VolumeInformationTime> GetPriorityVolumeInformation(int sitId, DateTime start, DateTime end, int ispId);

        /// <summary>
        /// Returns the organization which is linked to the user
        /// </summary>
        /// <param name="userId">The userid here is the UID</param>
        /// <returns></returns>
        Organization GetOrganizationForUser(string userId);

        /// <summary>
        /// Returns the servicepacks for a specific isp
        /// </summary>
        /// <param name="ispId">The ISP id</param>
        /// <returns>A list of SLAs belonging to the ISP with the hidden servicepacks excluded</returns>
        List<ServiceLevel> GetServicePacksByIsp(int ispId);
        
        /// <summary>
        /// Resturns the servicepacks for a specific isp but includes the 
        /// servicepacks with the Hide flag set to true.
        /// </summary>
        /// <remarks>
        /// Use this method when retrieving the service packs for an ISP in the 
        /// NOCSA CMT
        /// </remarks>
        /// <param name="ispId">The ISP id</param>
        /// <returns>A list of SLAs belonging to the ISP with the hidden serivcepacks included</returns>
        List<ServiceLevel> GetServicePacksByIspWithHide(int ispId);

        /// <summary>
        /// Returns true if the SLA is a business sla
        /// </summary>
        /// <param name="slaId">The SLA id as an integer</param>
        /// <returns>True if the SLA is a business sla</returns>
        bool IsBusinessSLA(int slaId);

        /// <summary>
        /// Returns true if the SLA is a freezone sla
        /// </summary>
        /// <param name="slaId">The SLA id as an integer</param>
        /// <returns>True if the SLA is a freezone sla</returns>
        bool IsFreeZoneSLA(int slaId);

        /// <summary>
        /// Returns true if the SLA is a voucher based sla
        /// </summary>
        /// <param name="slaId">The SLA id as an integer</param>
        /// <returns>True if the SLA is a voucher based sla</returns>
        bool IsVoucherSLA(int slaId);

        /// <summary>
        /// Checks if the given SitId is unique
        /// </summary>
        /// <param name="sitId">The SitId to check</param>
        /// <returns>True if the SitId is indeed unique, false otherwise</returns>
        bool IsSitIdUnique(int sitId, int ispId);

        /// <summary>
        /// This method returns the MacAddress for the given system user
        /// </summary>
        /// <remarks>
        /// This method is especially used by the White Label CMT
        /// </remarks>
        /// <param name="UserId">The Guid of the logged in system user</param>
        /// <returns>The MacAddress if found or null if not found!</returns>
        String GetMacAddressForSystemUser(Guid UserId);

        /// <summary>
        /// Updates the MacAddress for the given user. If the user does not exist
        /// the record is created in the 
        /// </summary>
        /// <param name="SystemUserId"></param>
        /// <param name="MacAddress"></param>
        /// <returns></returns>
        bool UpdateMacAddressForSystemUser(Guid SystemUserId, string MacAddress);

        /// These methods were moved to the BOConfigurationController
        /// <summary>
        /// Returns the parameter value from the configuration table
        /// </summary>
        /// <param name="paramName"></param>
        /// <returns></returns>
        //string GetParameter(string paramName);

        /// <summary>
        /// Updates the parameter in the configurations table
        /// </summary>
        /// <remarks>
        /// If the parameter does not exist is is created          
        /// </remarks>
        /// <param name="paramName">Name of the configuration parameter</param>
        /// <param name="value">Parameter value</param>
        /// <returns>True if the parameter was correctly updated</returns>
        //bool UpdateParameter(string paramName, string value);

        /// <summary>
        /// Fetches all freezones in the CMT database
        /// </summary>
        /// <returns>The list of freezones</returns>
        List<FreeZoneCMT> GetFreeZones();

        /// <summary>
        /// Updates the data of a freezone in the CMT database
        /// </summary>
        /// <param name="freeZone">The freezone to be updated</param>
        /// <returns>True if the operation succeeded</returns>
        bool UpdateFreeZone(FreeZoneCMT freeZone);

        /// <summary>
        /// Creates a new freezone in the CMT database
        /// </summary>
        /// <param name="freeZone">The freezone to be created</param>
        /// <returns>True if the operation succeeded</returns>
        bool CreateFreeZone(FreeZoneCMT freeZone);

        /// <summary>
        /// Retrievs a specific VNO from the CMT database
        /// </summary>
        /// <param name="VNOId">The ID of the VNO to be retrieved</param>
        /// <returns>The VNO object</returns>
        VNO GetVNO(int VNOId);

        /// <summary>
        /// Creates/Updates a VNO entry in the CMT database
        /// </summary>
        /// <param name="vno">The VNO object to be created/updated</param>
        /// <returns>True if the operation succeeded</returns>
        bool UpdateVNO(VNO vno);

        /// <summary>
        /// Retrieves all the distributor objects which are marked as VNO from the CMT database
        /// </summary>
        /// <returns>A list of distributor objects</returns>
        List<Distributor> GetVNOs();

        /// <summary>
        ///   Get all Distributors and VNOs
        /// </summary>
        /// <returns>A list of distributor objects</returns>
        List<Distributor> GetDistributorsAndVNOs();

        /// <summary>
        /// Adds a URL in an NMS table through which the terminal can access certain functionalities
        /// The URLs are scrambled for the end-user and this method matches the clear-text URL with
        /// the scrambled one.
        /// Functionalities include traffic data and a speedtest
        /// Issue CMTBO-48
        /// </summary>
        /// <param name="clearURL">The URL in readable text</param>
        /// <param name="scrambledURL">The scrambled version of the URL</param>
        /// <returns>True if successful</returns>
        bool AddTerminalNMSURL(string clearURL, string scrambledURL);

        /// <summary>
        ///   Get all inactive distributors
        ///   Excludes all inactive distributors in the VNO list
        /// </summary>
        /// <returns>A list of inactive distributor objects</returns>
        List<Distributor> GetInactiveDistributors();

        /// <summary>
        ///   Get all active distributors 
        ///   Excludes all active distributors in the VNO list
        /// </summary>
        /// <returns>A list of active distributor objects</returns>
        List<Distributor> GetActiveDistributors();

        /// <summary>
        /// Gets info concerning the BadDistributor flag, and passes it through to the NMS 
        /// </summary>
        /// <remarks>
        /// Both params received through DistributorDetailsForm from the front end
        /// </remarks>
        /// <param name="distId">Distributor ID</param>
        /// <param name="badDistributor">BadDistributor flag</param>
        void BadDistributorChanged(int distId, bool badDistributor);


        /// <summary>
        /// Gets a total amount of Volume used by a given Terminal on a given date. 
        /// By total Volume is meant: the sum of the TotalForwardedVolume column and the TotalReturnVolume in the VolumeHistory2 table.
        /// </summary>
        /// <param name="ispId">ispId of the Terminal</param>
        /// <param name="sitId">sitId of the Terminal</param>
        /// <param name="date">a date</param>
        /// <returns>a long integer or Int64</returns>
        long GetTotalTerminalVolumeForDate(int ispId, int sitId, DateTime date);

        /// <summary>
        /// Gets the country with country code and country name with a given country code.
        /// Returns null when the country could not be found.
        /// </summary>
        /// <param name="code">The country code to get the country for</param>
        /// <returns>The country with its country name and country code, or null when it could not be found</returns>
        Country GetCountryByCountryCode(string code);

        /// <summary>
        /// Gets the full country name with a given country code.
        /// Returns null when the country could not be found.
        /// </summary>
        /// <param name="code">The country code to get the country name for</param>
        /// <returns>The country name of a country, or null when it could not be found</returns>
        string GetCountryNameByCountryCode(string code);

        bool IsSatDivTerminalActive(string macAddress);

        /// <summary>
        ///   Get all available service packs/levels by Billable Id
        /// </summary>
        /// <param name="billableId"></param>
        /// <returns>A list of service packs</returns>
        /// CMTINVOICE-73
        List<ServiceLevel> GetServicePacksByBillableId(int billableId);

        //Move these methods to a separate controller in due time
        #region Methods related to the management of themes for the White Label CMT
        /// <summary>
        /// Creates or updates a White Label CMT theme. If the theme does not exist in
        /// the CMT the theme is created, otherwise the theme data is updated.
        /// </summary>
        /// <param name="theme">The theme to update or create</param>
        /// <returns>True if the operation succeeded</returns>
        Boolean UpdateTheme(Theme theme);

        /// <summary>
        /// Returns a theme for the given distributor.
        /// </summary>
        /// <remarks>
        /// The result can be null if no theme is allocated to the given
        /// distributor
        /// </remarks>
        /// <param name="DistId">The distributor identifier</param>
        /// <returns></returns>
        Theme GetThemeForDistributor(int DistId);
        #endregion

        /// <summary>
        ///   Get a list of satellites.
        /// </summary>
        /// <returns>A list of satellites.</returns>
        List<Satellite> GetSatellites();

        /// <summary>
        ///   Get a list of ISPs linked to a satellite.
        /// </summary>
        /// <returns>A list of satellites.</returns>
        List<Isp> GetISPsBySatellite(int satelliteId);

        
    }
}
