﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BOControllerLibrary.Model.Service;
using System.Drawing;

namespace BOControllerLibrary.Controllers.Interfaces
{
    /// <summary>
    /// This component handles the flexible user services
    /// It is the implementation of use case UC-VNOSO-005
    /// </summary>
    public interface IBOServicesController
    {
        
        /// <summary>
        /// Deletes the service from the CMTDatabase.
        /// This does not physically remove the service ascx file from the project,
        /// but just removes it from the list of selectable services.
        /// </summary>
        /// <param name="service">The service to be deleted</param>
        /// <returns>True if succesful, else false</returns>
        bool DeleteService(Service service);

        /// <summary>
        /// Returns the parent Service.
        /// This is the service which is represented by a node one level higher
        /// than the given service. The return value can be null!
        /// </summary>
        /// <param name="service">The service for which the parent is requested</param>
        /// <returns>The parent service, can also be null</returns>
        Service GetParent(Service service);

        /// <summary>
        /// Returns a list of services linked to the given VNO.
        /// </summary>
        /// <param name="VNOId">The VNO ID in question</param>
        /// <returns>A list of services</returns>
        List<Service> GetServicesForVNO(int VNOId);

        /// <summary>
        /// Returns a typed list of services which are related as siblings
        /// to the given service. This list can be empty.
        /// </summary>
        /// <param name="service">The service for which the siblings are requested</param>
        /// <returns>A list of services</returns>
        List<Service> GetSiblings(Service service);

        /// <summary>
        /// Links a Service to a VNO. This adds a record to the VNOProfile table.
        /// </summary>
        /// <param name="VNOId">The VNO ID the service should be linked to</param>
        /// <param name="ServiceId">The ID of the service to be linked to the VNO</param>
        /// <returns>True if succesful, else false</returns>
        bool LinkServiceToVNO(int VNOId, string ServiceId);

        /// <summary>
        /// Links a Service to a VNO. This adds a record to the VNOProfile table.
        /// </summary>
        /// <param name="VNOId">The VNO ID the service should be removed from</param>
        /// <param name="ServiceId">The ID of the service to be removed from the VNO</param>
        /// <returns>True if succesful, else false</returns>
        bool RemoveServiceFromVNO(int VNOId, string ServiceId);

        /// <summary>
        /// Updates the service in the CMT database. If the service does not exist a new service is created.
        /// </summary>
        /// <param name="service">The service to be updated</param>
        /// <returns>True if succesful, else false</returns>
        bool UpdateService(Service service);

        /// <summary>
        /// Returns the service details
        /// </summary>
        /// <param name="ServiceId">The ID of the service</param>
        /// <returns>A service</returns>
        Service GetService(string ServiceId);

        /// <summary>
        /// Returns a list of all available (non-deleted) services in the database
        /// of a certain level.
        /// Will mainly be used to get all top level services or to get all services
        /// </summary>
        /// <param name="level">The service level (1, 2, 3, or all)</param>
        /// <returns>A list of services</returns>
        List<Service> GetAllAvailableServices(string level);

        /// <summary>
        /// Returns a list of all child services of a parent service
        /// The list can be empty
        /// </summary>
        /// <param name="service">The parent service</param>
        /// <returns>A list of services</returns>
        List<Service> GetChildren(Service service);

        /// <summary>
        /// Method for retrieving an icon from the database and putting it into the memorystream
        /// so that it can be displayed as an image
        /// </summary>
        /// <param name="ServiceId">The ID of the service for which the icon is retrieved</param>
        /// <returns>The image</returns>
        Byte[] GetIcon(string ServiceId);
    }
}
