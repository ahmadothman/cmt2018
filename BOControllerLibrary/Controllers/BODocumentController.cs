﻿using System;
using System.Data.SqlClient;
using System.Linq;
using System.Collections.Generic;
using BOControllerLibrary.Model;
using BOControllerLibrary.Model.Documents;
using BOControllerLibrary.Model.Log;
using BOControllerLibrary.Exceptions;

namespace BOControllerLibrary.Controllers
{
    /// <summary>
    /// The DocumentController contains methods which support the management of the 
    /// document archive.
    /// </summary>
    public class BODocumentController : BOControllerLibrary.Controllers.Interfaces.IBODocumentController
    {
        private string _connectionString = "";
        private string _databaseProvider = "";
        private BOLogController _logController = null;

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="connectionString">The connection string to use</param>
        /// <param name="databaseProvider">The database provider</param>
        public BODocumentController(string connectionString, string databaseProvider)
        {
            _connectionString = connectionString;
            _databaseProvider = databaseProvider;
            _logController = new BOLogController(_connectionString, _databaseProvider);
        }

        /// <summary>
        /// Creates a new document in the database. 
        /// </summary>
        /// <remarks>
        /// The name of the document should be unique. Duplicate names will
        /// result in an exception.
        /// </remarks>
        /// <param name="document">The document Dto</param>
        /// <returns>True if the document could be created</returns>
        /// <exception cref="BOControllerLibrary.Exceptions.DuplicateDocumentException">Thrown if the the name of the document already exists</exception>
        public long CreateNewDocument(Model.Documents.Document document)
        {
            String insertCmd = "INSERT INTO Documents (DocumentName, DocumentDescription, DateUploaded, Recipient, RecipientFullName) " +
                               "VALUES (@DocumentName, @DocumentDescription, @DateUploaded, @Recipient, @RecipientFullName)";
            String lastIdCmd = "SELECT IDENT_CURRENT('Documents')";
            long lastId = 0;

            //Check if the document alraedy exists
            if (this.AlreadyExists(document.Name))
            {
                throw new DuplicateDocumentException("Document: " + document.Name + " already exists");
            }

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(insertCmd, conn);
                    cmd.Parameters.AddWithValue("@DocumentName", document.Name);
                    cmd.Parameters.AddWithValue("@DocumentDescription", document.Description);
                    cmd.Parameters.AddWithValue("@DateUploaded", document.DateUploaded);
                    cmd.Parameters.AddWithValue("@Recipient", document.Recipient);
                    cmd.Parameters.AddWithValue("RecipientFullName", document.RecipientFullName);
                    int numRows = cmd.ExecuteNonQuery();

                    if (numRows != 0)
                    {
                        cmd = new SqlCommand(lastIdCmd, conn);
                        lastId = Convert.ToInt64(cmd.ExecuteScalar());
                    }
                    cmd.Dispose();
                }
            }
            catch (SqlException ex)
            {
                CmtApplicationException cmtException = new CmtApplicationException();
                cmtException.ExceptionDesc = ex.Message;
                cmtException.ExceptionStacktrace = ex.StackTrace;
                cmtException.ExceptionLevel = (short)ExceptionLevelEnum.Error;
                cmtException.ExceptionDateTime = DateTime.Now;
                cmtException.UserDescription = "BODocumentController.CreateDocument method failed";
                _logController.LogApplicationException(cmtException);
            }

            return lastId;
        }

        /// <summary>
        /// Updates the document information
        /// </summary>
        /// <remarks>
        /// It is forbidden to change the name of the document! Changing the name of the
        /// document will result in an exception.
        /// </remarks>
        /// <param name="document">The document Dto</param>
        /// <returns>True if the document could be updated</returns>
        /// <exception cref="BOControllerLibrary.Exceptions.DuplicateDocumentException">Thrown if the name of and existing document has been changed</exception>
        public bool UpdateDocument(Model.Documents.Document document)
        {
            String updateCmd = "UPDATE Documents SET DocumentName = @DocumentName, DocumentDescription = @DocumentDescription, " +
                "DateUploaded = @DateUploaded, Recipient = @Recipient, RecipientFullName = @RecipientFullName " +
                "WHERE DocumentId = @DocumentId";

            bool result = false;

            //First check if the documents' name has been changed
            if (this.DocumentNameHasChanged(document))
            {
                throw new DocumentNameChangeException("Document name has been changed");
            }

            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(updateCmd, conn))
                {
                    cmd.Parameters.AddWithValue("@DocumentName", document.Name);
                    cmd.Parameters.AddWithValue("@DocumentDescription", document.Description);
                    cmd.Parameters.AddWithValue("@DateUploaded", document.DateUploaded);
                    cmd.Parameters.AddWithValue("@Recipient", document.Recipient);
                    cmd.Parameters.AddWithValue("@RecipientFullName", document.RecipientFullName);
                    cmd.Parameters.AddWithValue("@DocumentId", document.Id);

                    try
                    {
                        int numRows = cmd.ExecuteNonQuery();

                        if (numRows == 1)
                        {
                            result = true;
                        }
                    }
                    catch (SqlException sqlEx)
                    {
                        CmtApplicationException cmtException = new CmtApplicationException();
                        cmtException.ExceptionDesc = sqlEx.Message;
                        cmtException.ExceptionStacktrace = sqlEx.StackTrace;
                        cmtException.ExceptionLevel = (short)ExceptionLevelEnum.Error;
                        cmtException.ExceptionDateTime = DateTime.Now;
                        cmtException.UserDescription = "BODocumentController.UpdateDocument method failed";
                        _logController.LogApplicationException(cmtException);
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// This deletes the document information AND the document from the server harddisk.
        /// </summary>
        /// <param name="document">Document Dto</param>
        /// <returns>True if the action succeeded, false otherwise</returns>
        public bool DeleteDocument(long documentId)
        {
            String deleteCmd = "DELETE FROM Documents WHERE DocumentId = @DocumentId";
            bool result = false;

            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(deleteCmd, conn))
                {
                    cmd.Parameters.AddWithValue("@DocumentId", documentId);

                    try
                    {
                        int numRows = cmd.ExecuteNonQuery();

                        if (numRows == 1)
                        {
                            result = true;
                        }
                    }
                    catch (SqlException sqlEx)
                    {
                        CmtApplicationException cmtException = new CmtApplicationException();
                        cmtException.ExceptionDesc = sqlEx.Message;
                        cmtException.ExceptionStacktrace = sqlEx.StackTrace;
                        cmtException.ExceptionLevel = (short)ExceptionLevelEnum.Error;
                        cmtException.ExceptionDateTime = DateTime.Now;
                        cmtException.UserDescription = "BODocumentController.DeleteDocument method failed";
                        _logController.LogApplicationException(cmtException);
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Retrieves the document Dto from the database
        /// </summary>
        /// <remarks>
        /// The return value of this method could be null! If you need to check if the document already exists
        /// in the database it's better to use the <b>AlreadyExists</b> method.
        /// </remarks>
        /// <param name="documentId">The unique identifier of the document</param>
        /// <returns>The document Dto or null if not found</returns>
        /// <exception cref=">BOControlLibrary.Exceptions.DocumentNotFoundException">Thrown if the document is not found</exception>
        public Model.Documents.Document GetDocument(long documentId)
        {
            String queryCmd = "SELECT DocumentName, DocumentDescription, DateUploaded, Recipient, RecipientFullName " +
                "FROM Documents WHERE DocumentId = @DocumentId";
            Document document = null;

            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(queryCmd, conn))
                {
                    cmd.Parameters.AddWithValue("@DocumentId", documentId);

                    try
                    {
                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            document = new Document();
                            document.Id = documentId;
                            document.Name = reader.GetString(0);
                            document.Description = reader.GetString(1);
                            document.DateUploaded = reader.GetDateTime(2);
                            document.Recipient = reader.GetString(3);
                            document.RecipientFullName = reader.GetString(4);
                        }
                        else
                        {
                            throw new DocumentNotFoundException("Document with Id: " + documentId + " does not exist");
                        }
                    }
                    catch (SqlException sqlEx)
                    {
                        CmtApplicationException cmtException = new CmtApplicationException();
                        cmtException.ExceptionDesc = sqlEx.Message;
                        cmtException.ExceptionStacktrace = sqlEx.StackTrace;
                        cmtException.ExceptionLevel = (short)ExceptionLevelEnum.Error;
                        cmtException.ExceptionDateTime = DateTime.Now;
                        cmtException.UserDescription = "BODocumentController.GetDocument method failed";
                        _logController.LogApplicationException(cmtException);
                    }
                }
            }

            return document;
        }

        /// <summary>
        /// Checks if a document with the given name already exists in the database
        /// </summary>
        /// <param name="documentName">The name of the document</param>
        /// <returns>True if the document exists in the database</returns>
        public bool AlreadyExists(string documentName)
        {
            bool result = false;
            String queryCmd = "SELECT DocumentId FROM Documents WHERE DocumentName = @documentName";

            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                conn.Open();

                using (SqlCommand cmd = new SqlCommand(queryCmd, conn))
                {
                    cmd.Parameters.AddWithValue("@documentName", documentName);
                    try
                    {
                        SqlDataReader reader = cmd.ExecuteReader();
                        result = reader.HasRows;
                    }
                    catch (SqlException ex)
                    {
                        CmtApplicationException cmtException = new CmtApplicationException();
                        cmtException.ExceptionDesc = ex.Message;
                        cmtException.ExceptionStacktrace = ex.StackTrace;
                        cmtException.ExceptionLevel = (short)ExceptionLevelEnum.Error;
                        cmtException.ExceptionDateTime = DateTime.Now;
                        cmtException.UserDescription = "BODocumentController.AlreadyExists method failed";
                        _logController.LogApplicationException(cmtException);
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Returns true if the name of the document has changed
        /// </summary>
        /// <remarks>
        /// In case the document does not exist yet, the method throws a a DocumentNotFoundException. So do not use this
        /// method to check if a document exists in the database. Use the <b>GetDocument</b> method instead.
        /// </remarks>
        /// <param name="document">the Document Dto</param>
        /// <returns>Returns true if the name specified by the document Dto is different from the
        /// name in the database</returns>
        /// <exception cref=">BOControlLibrary.Exceptions.DocumentNotFoundException">Thrown if the document is not found</exception>
        public bool DocumentNameHasChanged(Model.Documents.Document document)
        {
            Document documentFromDb = null;

            try
            {
                documentFromDb = this.GetDocument(document.Id);
            }
            catch (DocumentNotFoundException dnex)
            {
                throw;
            }
            return  !document.Name.Equals(documentFromDb.Name);
        }


        /// <summary>
        /// Retrieves the document by its unique name
        /// </summary>
        /// <param name="fileName">The filename of the document</param>
        /// <returns>A valid document instance or null if not found</returns>
        public Document GetDocumentByName(string fileName)
        {
            String queryCmd = "SELECT DocumentId, DocumentName, DocumentDescription, DateUploaded, Recipient, RecipientFullName " +
                 "FROM Documents WHERE DocumentName = @DocumentName";
            Document document = null;

            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(queryCmd, conn))
                {
                    cmd.Parameters.AddWithValue("@DocumentName", fileName);

                    try
                    {
                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            document = new Document();
                            document.Id = (long)reader.GetInt32(0);
                            document.Name = reader.GetString(1);
                            document.Description = reader.GetString(2);
                            document.DateUploaded = reader.GetDateTime(3);
                            document.Recipient = reader.GetString(4);
                            document.RecipientFullName = reader.GetString(5);
                        }
                    }
                    catch (SqlException sqlEx)
                    {
                        CmtApplicationException cmtException = new CmtApplicationException();
                        cmtException.ExceptionDesc = sqlEx.Message;
                        cmtException.ExceptionStacktrace = sqlEx.StackTrace;
                        cmtException.ExceptionLevel = (short)ExceptionLevelEnum.Error;
                        cmtException.ExceptionDateTime = DateTime.Now;
                        cmtException.UserDescription = "BODocumentController.GetDocument method failed";
                        _logController.LogApplicationException(cmtException);
                    }
                }
            }

            return document;
        }

        /// <summary>
        /// Returns a list of all methods
        /// </summary>
        /// <returns>A LIst of Documents</returns>
        public List<Document> GetAllDocuments()
        {
            String queryCmd = "SELECT DocumentId, DocumentName, DocumentDescription, DateUploaded, Recipient, RecipientFullName " +
                "FROM Documents";
            Document document = null;
            List<Document> documents = new List<Document>();

            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(queryCmd, conn))
                {
                    try
                    {
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            document = new Document();
                            document.Id = reader.GetInt32(0);
                            document.Name = reader.GetString(1);
                            document.Description = reader.GetString(2);
                            document.DateUploaded = reader.GetDateTime(3);
                            document.Recipient = reader.GetString(4);
                            document.RecipientFullName = reader.GetString(5);
                            documents.Add(document);
                        }
                    }
                    catch (SqlException sqlEx)
                    {
                        CmtApplicationException cmtException = new CmtApplicationException();
                        cmtException.ExceptionDesc = sqlEx.Message;
                        cmtException.ExceptionStacktrace = sqlEx.StackTrace;
                        cmtException.ExceptionLevel = (short)ExceptionLevelEnum.Error;
                        cmtException.ExceptionDateTime = DateTime.Now;
                        cmtException.UserDescription = "BODocumentController.GetDocument method failed";
                        _logController.LogApplicationException(cmtException);
                    }
                }
            }

            return documents;
        }

        /// <summary>
        /// Returns a list of documents which belong either to everyone or the role
        /// of the recipient or the specific recipient.
        /// </summary>
        /// <param name="recipient">The reference of the recipient (Distributor id etc.)</param>
        /// <returns>A list of selected documents. This list can be empty.</returns>
        public List<Document> GetDocumentsForRecipient(String recipient, String role)
        {
            String queryCmd = "SELECT DocumentId, DocumentName, DocumentDescription, DateUploaded, Recipient, RecipientFullName " +
               "FROM Documents WHERE Recipient = 'everyone' OR Recipient = @role OR Recipient= @recipient";
            Document document = null;
            List<Document> documents = new List<Document>();

            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(queryCmd, conn))
                {
                    try
                    {
                        cmd.Parameters.AddWithValue("@role", role);
                        cmd.Parameters.AddWithValue("@recipient", recipient);
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            document = new Document();
                            document.Id = reader.GetInt32(0);
                            document.Name = reader.GetString(1);
                            document.Description = reader.GetString(2);
                            document.DateUploaded = reader.GetDateTime(3);
                            document.Recipient = reader.GetString(4);
                            document.RecipientFullName = reader.GetString(5);
                            documents.Add(document);
                        }
                    }
                    catch (SqlException sqlEx)
                    {
                        CmtApplicationException cmtException = new CmtApplicationException();
                        cmtException.ExceptionDesc = sqlEx.Message;
                        cmtException.ExceptionStacktrace = sqlEx.StackTrace;
                        cmtException.ExceptionLevel = (short)ExceptionLevelEnum.Error;
                        cmtException.ExceptionDateTime = DateTime.Now;
                        cmtException.UserDescription = "BODocumentController.GetDocument method failed";
                        _logController.LogApplicationException(cmtException);
                    }
                }
            }

            return documents;
        }
    }
}
