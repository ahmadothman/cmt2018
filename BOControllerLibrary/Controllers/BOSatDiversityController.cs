﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using BOControllerLibrary.Controllers;
using BOControllerLibrary.Controllers.Interfaces;
using BOControllerLibrary.Model.Terminal;
using BOControllerLibrary.Model.Log;
using BOControllerLibrary.Model;

namespace BOControllerLibrary.Controllers
{
    /// <summary>
    /// This controller describes the methods for satellite diversity management
    /// (redundant terminal methods)
    /// This method deals only with the specifics of satellite diversity: existing terminal manag  ement
    /// methods as still used for as far as possible also for redundant terminals
    /// </summary>
    public class BOSatDiversityController : IBOSatDiversityController
    {
        string _connectionString = "";
        string _databaseProvider = "";
        BOLogController _logController;
        BOAccountingController _accountingController;

        public BOSatDiversityController(string connectionString, string databaseProvider)
        {
            _connectionString = connectionString;
            _databaseProvider =      databaseProvider;
            _logController = new BOLogController(_connectionString, _databaseProvider);
            _accountingController = new BOAccountingController(_connectionString, _databaseProvider);
        }

        /// <summary>
        /// Sets a terminal to active and sets the other terminal in the redundant setup to inactive
        /// </summary>
        /// <param name="macAddress">The MAC address of the terminal to be set as active</param>
        /// <returns>True if successful. Also returns true in case a non-redundant terminal is provided, or if the the terminal provided already was active</returns>
        public bool SetActiveTerminal(string macAddress)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Stores the switchover event in the CMT database
        /// </summary>
        /// <param name="switchOver">The details of the switchOver</param>
        /// <returns>True if successful</returns>
        public bool LogSwitchOver(SwitchOver switchOver)
        {
            string insertCmd = "INSERT INTO SatDiversitySwitchovers (TimeStamp, FromMac, ToMac) " +
                              "VALUES (@TimeStamp, @FromMac, @ToMac)";
            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(insertCmd, conn))
                    {
                        // write device information to database
                        cmd.Parameters.AddWithValue("@TimeStamp", switchOver.timeStamp);
                        cmd.Parameters.AddWithValue("@FromMac", switchOver.fromMac);
                        cmd.Parameters.AddWithValue("@ToMac", switchOver.toMac);
                        
                        cmd.ExecuteNonQuery();
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOSatDiversityController: Error while storing new switchover event", "From terminal: " + switchOver.fromMac + " To terminal: " + switchOver.toMac, (short)ExceptionLevelEnum.Error));
                return false;
            }
        }

        /// <summary>
        /// Fetches all switchover events in the database during a give timeperiod
        /// </summary>
        /// <param name="startDate">The start date of the period</param>
        /// <param name="endDate">The end date of the period</param>
        /// <returns>A list of switchover events</returns>
        public List<SwitchOver> GetAllSwitchOvers(DateTime startDate, DateTime endDate)
        {
            List<SwitchOver> switchOvers = new List<SwitchOver>();
            string queryCmd = "SELECT TimeStamp, FromMac, ToMac FROM SatDiversitySwitchovers " +
                              "WHERE TimeStamp > @TimeStamp1 AND TimeStamp < @TimeStamp2";
            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(queryCmd, conn))
                    {
                        // write device information to database
                        cmd.Parameters.AddWithValue("@TimeStamp1", startDate);
                        cmd.Parameters.AddWithValue("@TimeStamp2", endDate.AddDays(1));

                        SqlDataReader reader = cmd.ExecuteReader();
                        SwitchOver so;
                        while (reader.Read())
                        {
                            so = new SwitchOver();
                            so.timeStamp = reader.GetDateTime(0);
                            so.fromMac = reader.GetString(1);
                            so.toMac = reader.GetString(2);
                            switchOvers.Add(so);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOSatDiversityController: Error while executing GetAllSwitchOvers", "From: " + startDate.ToShortDateString() + " To: " + endDate.ToShortDateString(), (short)ExceptionLevelEnum.Error));
            }

            return switchOvers;
        }

        /// <summary>
        /// Fetches all switchover events in the database during a give timeperiod
        /// </summary>
        /// <param name="macAddress">The MAC address of one of the terminals in the redundant setup</param>
        /// <param name="startDate">The start date of the period</param>
        /// <param name="endDate">The end date of the period</param>
        /// <returns>A list of switchover events</returns>
        public List<SwitchOver> GetSwitchOversForTerminal(string macAddress, DateTime startDate, DateTime endDate)
        {
            List<SwitchOver> switchOvers = new List<SwitchOver>();
            string queryCmd = "SELECT TimeStamp, FromMac, ToMac FROM SatDiversitySwitchovers " +
                              "WHERE TimeStamp >= @TimeStamp1 AND TimeStamp <= @TimeStamp2 AND (FromMac = @MacAddress OR ToMac = @MacAddress)";
            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand(queryCmd, conn))
                    {
                        conn.Open();
                        // write device information to database
                        cmd.Parameters.AddWithValue("@TimeStamp1", startDate);
                        cmd.Parameters.AddWithValue("@TimeStamp2", endDate.AddDays(1));
                        cmd.Parameters.AddWithValue("@MacAddress", macAddress);

                        SqlDataReader reader = cmd.ExecuteReader();
                        SwitchOver so;
                        while (reader.Read())
                        {
                            so = new SwitchOver();
                            so.timeStamp = reader.GetDateTime(0);
                            so.fromMac = reader.GetString(1);
                            so.toMac = reader.GetString(2);
                            switchOvers.Add(so);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOSatDiversityController: Error while executing GetSwitchOversForTerminal", "Terminal: " + macAddress, (short)ExceptionLevelEnum.Error));
            }

            return switchOvers;
        }

        /// <summary>
        /// Fetches all switchover events in the database during a give timeperiod for a specific distributor
        /// </summary>
        /// <param name="startDate">The start date of the period</param>
        /// <param name="endDate">The end date of the period</param>
        /// <param name="distributorId">The ID of the distributor</param>
        /// <returns>A list of switchover events</returns>
        public List<SwitchOver> GetSwitchOversForDistributor(DateTime startDate, DateTime endDate, int distributorId)
        {
            List<Terminal> terminals = _accountingController.GetTerminalsByDistributorId(distributorId);
            List<SwitchOver> switchOvers = new List<SwitchOver>();
            List<SwitchOver> termSO;
            foreach (Terminal t in terminals)
            {
                termSO = new List<SwitchOver>();
                termSO = this.GetSwitchOversForTerminal(t.MacAddress, startDate, endDate);
                foreach (SwitchOver so in termSO)
                {
                    switchOvers.Add(so);
                }
            }
            return switchOvers;
        }

        /// <summary>
        /// Fetches all switchover events in the database during a give timeperiod for a specific customer
        /// </summary>
        /// <param name="startDate">The start date of the period</param>
        /// <param name="endDate">The end date of the period</param>
        /// <param name="customerId">The ID of the customer</param>
        /// <returns>A list of switchover events</returns>
        public List<SwitchOver> GetSwitchOversForCustomer(DateTime startDate, DateTime endDate, int customerId)
        {
            List<Terminal> terminals = _accountingController.GetTerminalsByOrganization(customerId);
            List<SwitchOver> switchOvers = new List<SwitchOver>();
            List<SwitchOver> termSO;
            foreach (Terminal t in terminals)
            {
                termSO = new List<SwitchOver>();
                termSO = this.GetSwitchOversForTerminal(t.MacAddress, startDate, endDate);
                foreach (SwitchOver so in termSO)
                {
                    switchOvers.Add(so);
                }
            }
            return switchOvers;
        }
    }
}
