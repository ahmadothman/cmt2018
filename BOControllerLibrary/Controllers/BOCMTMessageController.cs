﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BOControllerLibrary.Controllers.Interfaces;
using BOControllerLibrary.Model.CMTMessages;
using BOControllerLibrary.Model.Log;
using BOControllerLibrary.Model;
using System.Data;
using System.Data.SqlClient;
using PetaPoco;
using log4net;

namespace BOControllerLibrary.Controllers
{
    /// <summary>
    /// This component handles an internal messaging system for the CMT
    /// Messages can be sent from the NOCSA to a specific user, all users of
    /// a specific role or to everyone.
    /// The messages will be displayed in a list when the user logs on.
    /// </summary>
    public class BOCMTMessageController : IBOCMTMessageController
    {
        string _connectionString = "";
        string _databaseProvider = "";
        BOLogController _logController;

        public BOCMTMessageController(string connectionString, string databaseProvider)
        {
            _connectionString = connectionString;
            _databaseProvider = databaseProvider;
            _logController = new BOLogController(_connectionString, _databaseProvider);
        }
        
        /// <summary>
        /// This method submits a new message to the CMT database
        /// </summary>
        /// <param name="message">The CMT message</param>
        /// <returns>True if succesful</returns>
        public bool SubmitCMTMessage(CMTMessage message)
        {
            string insertCmd = "INSERT INTO CMTMessages (MessageId, MessageContent, MessageTitle, StartTime, EndTime, Recipient, RecipientFullName, Release) " +
                              "VALUES (@MessageId, @MessageContent, @MessageTitle, @StartTime, @EndTime, @Recipient, @RecipientFullName, @Release)";
            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(insertCmd, conn))
                    {
                        // write service information to database
                        cmd.Parameters.AddWithValue("@MessageId", message.MessageId);
                        cmd.Parameters.AddWithValue("@MessageContent", message.MessageContent);
                        cmd.Parameters.AddWithValue("@MessageTitle", message.MessageTitle);
                        cmd.Parameters.AddWithValue("@StartTime", message.StartTime);
                        cmd.Parameters.AddWithValue("@EndTime", message.EndTime);
                        cmd.Parameters.AddWithValue("@Recipient", message.Recipient);
                        cmd.Parameters.AddWithValue("@RecipientFullName", message.RecipientFullName);
                        cmd.Parameters.AddWithValue("@Release", message.Release);
                        
                        cmd.ExecuteNonQuery();
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOMessagesController: Error while storing message in database", message.MessageId.ToString(), (short)ExceptionLevelEnum.Error));
                return false;
            }
        }

        /// <summary>
        /// This method submits changes to a message in the CMT database
        /// </summary>
        /// <param name="message">The CMT message</param>
        /// <returns>True if succesful</returns>
        public bool UpdateCMTMessage(CMTMessage message)
        {
            string updateCmd = "UPDATE CMTMessages " +
                              "SET MessageContent = @MessageContent, MessageTitle = @MessageTitle, StartTime = @StartTime, EndTime = @EndTime, Recipient = @Recipient, RecipientFullName = @RecipientFullName, " +
                              "Release = @Release WHERE MessageId = @MessageId";
 
            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(updateCmd, conn))
                    {
                        // write service information to database
                        cmd.Parameters.AddWithValue("@MessageId", message.MessageId);
                        cmd.Parameters.AddWithValue("@MessageContent", message.MessageContent);
                        cmd.Parameters.AddWithValue("@MessageTitle", message.MessageTitle);
                        cmd.Parameters.AddWithValue("@StartTime", message.StartTime);
                        cmd.Parameters.AddWithValue("@EndTime", message.EndTime);
                        cmd.Parameters.AddWithValue("@Recipient", message.Recipient);
                        cmd.Parameters.AddWithValue("@RecipientFullName", message.RecipientFullName);
                        cmd.Parameters.AddWithValue("@Release", message.Release);

                        cmd.ExecuteNonQuery();
                    }
                }
                return true;
            }
            catch (Exception ex)
            {

                _logController.LogApplicationException(new CmtApplicationException(ex, "BOMessagesController: Error while storing message in database", message.MessageId.ToString(), (short)ExceptionLevelEnum.Error));
                return false;
            }
        }

        /// <summary>
        /// Retrieves the messages for a user
        /// First messages are retrieved based on the user ID (messages sent specifically to user)
        /// Then messages are retrieved based on the user role (messages sent to role)
        /// Finally messages sent to everyone are retrieved
        /// Only messages which are currently supposed to be displayed are retrieved
        /// </summary>
        /// <param name="userId">The ID of the user, can be distributor ID, VNO ID etc</param>
        /// <param name="role">The role of the user, eg distributor or organization</param>
        /// <returns>A list of CMT messages, can be empty</returns>
        public List<CMTMessage> GetCMTMessagesForUser(string userId, string role)
        {
            string getCmd = "SELECT MessageId, MessageContent, MessageTitle, StartTime, EndTime, Recipient, RecipientFullName, Release FROM CMTMessages " +
                              "WHERE (Recipient = @UserId OR Recipient = @Role OR Recipient = @Everyone) AND StartTime < @StartTime AND EndTime > @EndTime AND Release = 1" +
                              "ORDER BY StartTime DESC ";
            List<CMTMessage> messages = new List<CMTMessage>();
            CMTMessage messageTemp;

            //By user ID
            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(getCmd, conn))
                    {
                        // write service information to database
                        cmd.Parameters.AddWithValue("@UserId", userId);
                        cmd.Parameters.AddWithValue("@Role", role);
                        cmd.Parameters.AddWithValue("@Everyone", "everyone");
                        cmd.Parameters.AddWithValue("@StartTime", DateTime.Now);
                        cmd.Parameters.AddWithValue("@EndTime", DateTime.Now);
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            messageTemp = new CMTMessage();
                            messageTemp.MessageId = reader.GetGuid(0);
                            messageTemp.MessageContent = reader.GetString(1);
                            messageTemp.MessageTitle = reader.GetString(2);
                            messageTemp.StartTime = reader.GetDateTime(3);
                            messageTemp.EndTime = reader.GetDateTime(4);
                            messageTemp.Recipient = reader.GetString(5);
                            messageTemp.RecipientFullName = reader.GetString(6);
                            messageTemp.Release = reader.GetBoolean(7);
                            messages.Add(messageTemp);
                        }
                    }
                }
                
            }
            catch (Exception ex)
            {

                _logController.LogApplicationException(new CmtApplicationException(ex, "BOMessagesController: Error while getting messages for user", userId + "-" + role, (short)ExceptionLevelEnum.Error));
            }
            return messages;
        }

        /// <summary>
        /// Retrieves all the messages in the database
        /// This method is used for the admin overview
        /// </summary>
        /// <returns>A list of CMT messages, can be empty</returns>
        public List<CMTMessage> GetAllCMTMessages()
        {
            string getCmd = "SELECT MessageId, MessageContent, MessageTitle, StartTime, EndTime, Recipient, RecipientFullName, Release FROM CMTMessages";
            List<CMTMessage> messages = new List<CMTMessage>();
            CMTMessage messageTemp;

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(getCmd, conn))
                    {
                        // write service information to database
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            messageTemp = new CMTMessage();
                            messageTemp.MessageId = reader.GetGuid(0);
                            messageTemp.MessageContent = reader.GetString(1);
                            messageTemp.MessageTitle = reader.GetString(2);
                            messageTemp.StartTime = reader.GetDateTime(3);
                            messageTemp.EndTime = reader.GetDateTime(4);
                            messageTemp.Recipient = reader.GetString(5);
                            messageTemp.RecipientFullName = reader.GetString(6);
                            messageTemp.Release = reader.GetBoolean(7);
                            messages.Add(messageTemp);
                        }
                    }
                }

            }
            catch (Exception ex)
            {

                _logController.LogApplicationException(new CmtApplicationException(ex, "BOMessagesController: Error while getting messages", "All messages", (short)ExceptionLevelEnum.Error));
            }

            return messages;
        }

        /// <summary>
        /// Retrieves a specific message from the database
        /// </summary>
        /// <param name="messageId">The ID of the message</param>
        /// <returns>The message object</returns>
        public CMTMessage GetCMTMessage(string messageId)
        {
            string getCmd = "SELECT MessageId, MessageContent, MessageTitle, StartTime, EndTime, Recipient, RecipientFullName, Release FROM CMTMessages " +
                              "WHERE MessageId = @MessageId ";
            CMTMessage message = new CMTMessage();
            
            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(getCmd, conn))
                    {
                        // write service information to database
                        cmd.Parameters.AddWithValue("@MessageId", messageId);
                        SqlDataReader reader = cmd.ExecuteReader();
                        reader.Read();
                        
                        message = new CMTMessage();
                        message.MessageId = reader.GetGuid(0);
                        message.MessageContent = reader.GetString(1);
                        message.MessageTitle = reader.GetString(2);
                        message.StartTime = reader.GetDateTime(3);
                        message.EndTime = reader.GetDateTime(4);
                        message.Recipient = reader.GetString(5);
                        message.RecipientFullName = reader.GetString(6);
                        message.Release = reader.GetBoolean(7);
                    }
                }
            }
            catch (Exception ex)
            {

                _logController.LogApplicationException(new CmtApplicationException(ex, "BOMessagesController: Error while getting CMT message", messageId, (short)ExceptionLevelEnum.Error));
            }

            return message;
        }

        /// <summary>
        /// Method for retrieving an attachment from the database and putting it into the memorystream
        /// so that it can be used
        /// </summary>
        /// <param name="messageId">The ID of the message for which the attachment is retrieved</param>
        /// <returns>The image</returns>
        public byte[] GetAttachment(string messageId)
        {
            throw new NotImplementedException();
        }
    }
}
