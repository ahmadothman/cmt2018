﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using BOControllerLibrary.Controllers;
using BOControllerLibrary.Controllers.Interfaces;
using BOControllerLibrary.Model;
using BOControllerLibrary.Model.Invoices;
using BOControllerLibrary.Model.Distributor;

namespace BOControllerLibrary.Controllers
{
    public class BOInvoiceController : IBOInvoiceController
    {
        string _connectionString = "";
        string _databaseProvider = "";
        IBOLogController _logController = null;

        public BOInvoiceController(string connectionString, string databaseProvider)
        {
            _connectionString = connectionString;
            _databaseProvider = databaseProvider;
            _logController = new BOLogController(connectionString, databaseProvider);
        }

        #region IBOInvoiceController Members

        /// <summary>
        /// Returns the invoices for a given distributor and a given month
        /// </summary>
        /// <param name="distributorId">The distributor Id</param>
        /// <param name="year">The invoice year</param>
        /// <param name="month">The invoice month</param>
        /// <returns>A list of invoice headers</returns>
        public List<InvoiceHeader> GetInvoicesForDistributor(int distributorId, int year, int month)
        {
            string selectCmd = "SELECT InvoiceHeaders.InvoiceYear, InvoiceHeaders.InvoiceNum, InvoiceHeaders.CreationDate, InvoiceHeaders.Payed, " +
	        "InvoiceHeaders.InvoiceMonth, Distributors.FullName, Distributors.AddressLine1, Distributors.AddressLine2, " +
	        "Distributors.Location, Distributors.PostalCode, Distributors.Country, SUM(InvoiceDetails.AmountUSD) AS AmountUSD, SUM(InvoiceDetails.AmountEUR) AS AmountEUR " +
            "FROM InvoiceHeaders, Distributors, InvoiceDetails " +
            "WHERE DistributorId = @DistributorId AND InvoiceHeaders.InvoiceYear = @InvYear AND " +
            "InvoiceHeaders.InvoiceMonth = @InvMonth AND InvoiceHeaders.DistributorId = Distributors.ID AND " +
            "InvoiceDetails.InvoiceYear = InvoiceHeaders.InvoiceYear AND " +
            "InvoiceDetails.InvoiceNum = InvoiceHeaders.InvoiceNum " +
            "GROUP BY InvoiceHeaders.InvoiceYear, InvoiceHeaders.InvoiceNum, InvoiceHeaders.CreationDate, InvoiceHeaders.Payed, " +
            "InvoiceHeaders.InvoiceMonth, Distributors.FullName, Distributors.AddressLine1, Distributors.AddressLine2, " +
            "Distributors.Location, Distributors.PostalCode, Distributors.Country";

            List<InvoiceHeader> invHeaders = new List<InvoiceHeader>();

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(selectCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@DistributorId", distributorId);
                        cmd.Parameters.AddWithValue("@InvYear", year);
                        cmd.Parameters.AddWithValue("@InvMonth", month);

                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            InvoiceHeader invHeader = new InvoiceHeader();
                            invHeader.Distributor = new Distributor();
                            invHeader.Distributor.Address = new Model.Address();

                            invHeader.Distributor.Id = distributorId;
                            invHeader.InvoiceYear = reader.GetInt32(0);
                            invHeader.InvoiceNumber = reader.GetInt32(1);
                            invHeader.InvoiceDate = reader.GetDateTime(2);
                            //invHeader.Payed = reader.GetBoolean(3);
                            invHeader.InvoiceMonth = reader.GetInt32(4);
                            invHeader.Distributor.FullName = reader.GetString(5);
                            invHeader.Distributor.Address.AddressLine1 = reader.GetString(6);
                            invHeader.Distributor.Address.AddressLine2 = reader.GetString(7);
                            invHeader.Distributor.Address.Location = reader.GetString(8);
                            invHeader.Distributor.Address.PostalCode = reader.GetString(9);
                            invHeader.Distributor.Address.Country = reader.GetString(10);
                            invHeader.InvoiceTotalUSD = (float)reader.GetDecimal(11);
                            invHeader.InvoiceTotalEUR = (float)reader.GetDecimal(12);

                            invHeaders.Add(invHeader);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new Model.CmtApplicationException(ex, "GetInvoiceForDistributor", ""));
            }
            return invHeaders;
        }

        /// <summary>
        /// Returns the invoice lines for a given year and invoice number
        /// </summary>
        /// <param name="year"></param>
        /// <param name="num"></param>
        /// <returns></returns>
        public List<InvoiceLine> GetInvoiceLines(int year, int num)
        {
            string selectCmd = "SELECT InvoiceYear, InvoiceNum, ItemId, ItemDescription, UnitPrice, Items, " +
                "AmountUSD, AmountEUR, InvoiceMonth, TerminalId, Serial, ServicePack, MacAddress, Status " +
                "FROM InvoiceDetails WHERE InvoiceYear = @InvoiceYear AND InvoiceNum = @InvoiceNum";

            List<InvoiceLine> invLines = new List<InvoiceLine>();

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(selectCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@InvoiceYear", year);
                        cmd.Parameters.AddWithValue("@InvoiceNum", num);

                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            InvoiceLine invLine = new InvoiceLine();
                            invLine.InvoiceYear = reader.GetInt32(0);
                            invLine.InvoiceNum = reader.GetInt32(1);
                            invLine.ItemId = reader.GetInt32(2);
                            invLine.ItemDescription = reader.GetString(3);
                            invLine.UnitPrice = (float)reader.GetDecimal(4);
                            invLine.Items = reader.GetInt32(5);
                            invLine.AmountUSD = (float)reader.GetDecimal(6);
                            invLine.AmountEUR = (float)reader.GetDecimal(7);
                            invLine.InvoiceMonth = reader.GetInt32(8);
                            invLine.TerminalId = reader.GetString(9);
                            invLine.Serial = reader.GetString(10);
                            invLine.ServicePack = reader.GetString(11);
                            invLine.MacAddress = reader.GetString(12);
                            invLine.Status = reader.GetString(13);

                            invLines.Add(invLine);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new Model.CmtApplicationException(ex, "GetInvoiceLines", ""));
            }
            return invLines;
        }

        /// <summary>
        /// Returns the invoice headr for a given year and invoice number
        /// </summary>
        /// <param name="year"></param>
        /// <param name="num"></param>
        /// <returns></returns>
        public Model.Invoices.InvoiceHeader GetInvoice(int year, int num)
        {
            string selectCmd = "SELECT InvoiceHeaders.InvoiceYear, InvoiceHeaders.InvoiceNum, InvoiceHeaders.CreationDate, InvoiceHeaders.Payed, " +
            "InvoiceHeaders.InvoiceMonth, Distributors.FullName, Distributors.AddressLine1, Distributors.AddressLine2, " +
            "Distributors.Location, Distributors.PostalCode, Distributors.Country, Distributors.ID, SUM(InvoiceDetails.AmountUSD) AS AmountUSD, SUM(InvoiceDetails.AmountEUR) AS AmountEUR " +
            "FROM InvoiceHeaders, Distributors, InvoiceDetails " +
            "WHERE InvoiceHeaders.InvoiceYear = @InvYear AND " +
            "InvoiceHeaders.InvoiceNum = @InvoiceNum AND InvoiceHeaders.DistributorId = Distributors.ID AND " +
            "InvoiceDetails.InvoiceYear = InvoiceHeaders.InvoiceYear AND " +
            "InvoiceDetails.InvoiceNum = InvoiceHeaders.InvoiceNum " +
            "GROUP BY InvoiceHeaders.InvoiceYear, InvoiceHeaders.InvoiceNum, InvoiceHeaders.CreationDate, InvoiceHeaders.Payed, " +
            "InvoiceHeaders.InvoiceMonth, Distributors.FullName, Distributors.AddressLine1, Distributors.AddressLine2, " +
            "Distributors.Location, Distributors.PostalCode, Distributors.Country, Distributors.ID";


            InvoiceHeader invHeader = null;

            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                conn.Open();

                using (SqlCommand cmd = new SqlCommand(selectCmd, conn))
                {
                    cmd.Parameters.AddWithValue("@InvoiceNum", num);
                    cmd.Parameters.AddWithValue("@InvYear", year);

                    SqlDataReader reader = cmd.ExecuteReader();

                    if (reader.Read())
                    {
                        invHeader = new InvoiceHeader();
                        invHeader.Distributor = new Distributor();
                        invHeader.Distributor.Address = new Model.Address();

                        
                        invHeader.InvoiceYear = reader.GetInt32(0);
                        invHeader.InvoiceNumber = reader.GetInt32(1);
                        invHeader.InvoiceDate = reader.GetDateTime(2);
                        //invHeader.Payed = reader.GetBoolean(3);
                        invHeader.InvoiceMonth = reader.GetInt32(4);
                        invHeader.Distributor.FullName = reader.GetString(5);
                        invHeader.Distributor.Address.AddressLine1 = reader.GetString(6);
                        invHeader.Distributor.Address.AddressLine2 = reader.GetString(7);
                        invHeader.Distributor.Address.Location = reader.GetString(8);
                        invHeader.Distributor.Address.PostalCode = reader.GetString(9);
                        invHeader.Distributor.Address.Country = reader.GetString(10);
                        invHeader.Distributor.Id = reader.GetInt32(11);
                        invHeader.InvoiceTotalUSD = (float)reader.GetDecimal(12);
                        invHeader.InvoiceTotalEUR = (float)reader.GetDecimal(13);
                    }
                }
            }

            return invHeader;
        }

        /// <summary>
        /// Returns all invoice headers for a given year and month
        /// </summary>
        /// <param name="year"></param>
        /// <param name="Month"></param>
        /// <returns></returns>
        public List<InvoiceHeader> GetInvoices(int year, int month)
        {
            string selectCmd = "SELECT InvoiceHeaders.InvoiceYear, InvoiceHeaders.InvoiceNum, InvoiceHeaders.CreationDate, InvoiceHeaders.Payed, " +
           "InvoiceHeaders.InvoiceMonth, Distributors.FullName, Distributors.AddressLine1, Distributors.AddressLine2, " +
           "Distributors.Location, Distributors.PostalCode, Distributors.Country, Distributors.ID, SUM(InvoiceDetails.AmountUSD) AS AmountUSD, SUM(InvoiceDetails.AmountEUR) AS AmountEUR " +
           "FROM InvoiceHeaders, Distributors, InvoiceDetails " +
           "WHERE InvoiceHeaders.InvoiceYear = @InvYear AND " +
           "InvoiceHeaders.InvoiceMonth = @InvMonth AND InvoiceHeaders.DistributorId = Distributors.ID AND " +
           "InvoiceDetails.InvoiceYear = InvoiceHeaders.InvoiceYear AND " +
           "InvoiceDetails.InvoiceNum = InvoiceHeaders.InvoiceNum " +
           "GROUP BY InvoiceHeaders.InvoiceYear, InvoiceHeaders.InvoiceNum, InvoiceHeaders.CreationDate, InvoiceHeaders.Payed, " +
           "InvoiceHeaders.InvoiceMonth, Distributors.FullName, Distributors.AddressLine1, Distributors.AddressLine2, " +
           "Distributors.Location, Distributors.PostalCode, Distributors.Country, Distributors.ID";

            List<InvoiceHeader> invHeaders = new List<InvoiceHeader>();

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(selectCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@InvYear", year);
                        cmd.Parameters.AddWithValue("@InvMonth", month);

                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            InvoiceHeader invHeader = new InvoiceHeader();
                            invHeader.Distributor = new Distributor();
                            invHeader.Distributor.Address = new Model.Address();
                           
                            invHeader.InvoiceYear = reader.GetInt32(0);
                            invHeader.InvoiceNumber = reader.GetInt32(1);
                            invHeader.InvoiceDate = reader.GetDateTime(2);
                            //invHeader.Payed = reader.GetBoolean(3);
                            invHeader.InvoiceMonth = reader.GetInt32(4);
                            invHeader.Distributor.FullName = reader.GetString(5);
                            invHeader.Distributor.Address.AddressLine1 = reader.GetString(6);
                            invHeader.Distributor.Address.AddressLine2 = reader.GetString(7);
                            invHeader.Distributor.Address.Location = reader.GetString(8);
                            invHeader.Distributor.Address.PostalCode = reader.GetString(9);
                            invHeader.Distributor.Address.Country = reader.GetString(10);
                            invHeader.Distributor.Id = reader.GetInt32(11); ;
                            invHeader.InvoiceTotalUSD = (float)reader.GetDecimal(12);
                            invHeader.InvoiceTotalEUR = (float)reader.GetDecimal(13);

                            invHeaders.Add(invHeader);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new Model.CmtApplicationException(ex, "GetInvoices", ""));
            }
            return invHeaders;
        }

        /// <summary>
        /// Returns the invoice lines for a distributor.
        /// </summary>
        /// <remarks>
        /// Only the invoice lines which refere to a terminal acivity or service
        /// fee linked to a terminal are included in the list.
        /// </remarks>
        /// <param name="distributorId">The distributor Id</param>
        /// <param name="year">The invoice year</param>
        /// <param name="month">The invoice month</param>
        /// <returns>A DataTable object with the invoice lines</returns>
        public DataTable GetInvoiceLinesForDistributor(int distributorId, int year, int month)
        {
            string selectCmd = "SELECT InvoiceDetails.ItemDescription, InvoiceDetails.Items, InvoiceDetails.UnitPrice, " +
                               "InvoiceDetails.AmountUSD, InvoiceDetails.AmountEUR, InvoiceDetails.TerminalId, InvoiceDetails.Serial, " +
                               "InvoiceDetails.ServicePack, InvoiceDetails.MacAddress, InvoiceDetails.Status FROM InvoiceHeaders, InvoiceDetails " +
                                "WHERE InvoiceDetails.InvoiceYear = @InvoiceYear AND InvoiceDetails.InvoiceMonth = @InvoiceMonth AND InvoiceHeaders.DistributorId = @DistributorId " +
                                "AND InvoiceHeaders.InvoiceYear = InvoiceDetails.InvoiceYear " +
                                "AND InvoiceHeaders.InvoiceNum = InvoiceDetails.InvoiceNum " +
                                "AND TerminalId != 'NA' ORDER BY InvoiceDetails.TerminalId";
           
            DataTable accountReportData = new DataTable();
           accountReportData.TableName = "InvoiceLinesByDistributor";
           SqlConnection conn = new SqlConnection(_connectionString);
           SqlDataAdapter adapter = new SqlDataAdapter();
           SqlCommand cmd = new SqlCommand(selectCmd, conn);

           SqlParameter startDateParam = new SqlParameter();
           startDateParam.ParameterName = "@InvoiceYear";
           startDateParam.SqlDbType = SqlDbType.Int;
           startDateParam.Value = year;
           cmd.Parameters.Add(startDateParam);

           SqlParameter endDateParam = new SqlParameter();
           endDateParam.ParameterName = "@InvoiceMonth";
           endDateParam.SqlDbType = SqlDbType.Int;
           endDateParam.Value = month;
           cmd.Parameters.Add(endDateParam);

           SqlParameter distributorParam = new SqlParameter();
           distributorParam.ParameterName = "@DistributorId";
           distributorParam.SqlDbType = SqlDbType.Int;
           distributorParam.Value = distributorId;
           cmd.Parameters.Add(distributorParam);

           adapter.SelectCommand = cmd;

           conn.Open();

           try
           {
               adapter.Fill(accountReportData);
           }
           catch (Exception ex)
           {
               CmtApplicationException cmtException = new CmtApplicationException();
               cmtException.ExceptionDesc = ex.Message;
               cmtException.ExceptionDateTime = DateTime.Now;
               cmtException.UserDescription = "GetTermAccountReportForDistributor method failed";
               _logController.LogApplicationException(cmtException);
               accountReportData = null;
           }
           finally
           {
               conn.Close();
           }

           return accountReportData;
        }

        #endregion
    }
}
