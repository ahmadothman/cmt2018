﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;
using BOControllerLibrary.Model;
using BOControllerLibrary.ISPIF.Model;
using BOControllerLibrary.Controllers.Interfaces;
using BOControllerLibrary.Controllers;
using log4net;
using net.nimera.CRCCalculator;
using BOControllerLibrary.Model.Log;
using BOControllerLibrary.Model.Terminal;
using BOControllerLibrary.ISPIF.Gateway.Interface;
using BOControllerLibrary.ISPIF.Gateway;

namespace BOControllerLibrary.Controllers
{
    /// <summary>
    /// Contains the methods which are necessary for connected devices
    /// </summary>
    public class BOConnectedDevicesController : IBOConnectedDevicesController
    {
        string _connectionString = "";
        string _databaseProvider = "";
        private readonly ILog _log = LogManager.GetLogger(typeof(IBOAccountingController));
        private readonly IBOLogController _logController;
        private readonly BOAccountingController _boAccountingControl;

        #region General methods

        public BOConnectedDevicesController(string connectionString, string databaseProvider)
        {
            _connectionString = connectionString;
            _databaseProvider = databaseProvider;
            _logController = new BOLogController(_connectionString, _databaseProvider);
            _boAccountingControl = new BOAccountingController(_connectionString, _databaseProvider);
        }
        
        /// <summary>
        /// Adds a new connected device to a terminal
        /// A device may only be connected to one terminal
        /// </summary>
        /// <param name="device">The device object which includes the terminal identifier</param>
        /// <returns>True if successful</returns>
        public bool AddDeviceToTerminal(ConnectedDevice device)
        {
            string insertCmd = "INSERT INTO ConnectedDevices (TerminalMacAddress, DeviceId, DeviceType, DeviceName)" +
                               "VALUES (@TerminalMacAddress, @DeviceId, @DeviceType, @DeviceName)";

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(insertCmd, conn))
                    {
                        // write device information to database
                        cmd.Parameters.AddWithValue("@TerminalMacAddress", device.macAddress);
                        cmd.Parameters.AddWithValue("@DeviceId", device.DeviceId);
                        cmd.Parameters.AddWithValue("@DeviceType", device.DeviceType);
                        cmd.Parameters.AddWithValue("@DeviceName", device.DeviceName);

                        cmd.ExecuteNonQuery();
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOConnectedDevicesController: Error while executing AddDeviceToTerminal", "Terminal: "+ device.macAddress + " Device: " + device.DeviceId, (short)ExceptionLevelEnum.Error));
                return false;
            }
        }

        /// <summary>
        /// Removes a connected device from a terminal and the CMT database
        /// </summary>
        /// <param name="device">The device object which includes the terminal identifier</param>
        /// <returns>True if successful</returns>
        public bool RemoveDeviceFromTerminal(ConnectedDevice device)
        {
            string deleteCmd = "DELETE FROM ConnectedDevices " +
                               "WHERE DeviceId = @DeviceId";

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(deleteCmd, conn))
                    {
                        // remove device from database
                        cmd.Parameters.AddWithValue("@DeviceId", device.DeviceId);
                        
                        cmd.ExecuteNonQuery();
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOConnectedDevicesController: Error while executing RemoveDeviceFromTerminal", "Device: " + device.DeviceId, (short)ExceptionLevelEnum.Error));
                return false;
            }
        }

        /// <summary>
        /// Returns a list of devices that are connected to a specific terminal
        /// </summary>
        /// <param name="macAddress">The MAC address of the terminal</param>
        /// <returns>A list of connected devices</returns>
        public List<ConnectedDevice> GetDevicesForTerminal(string macAddress)
        {
            List<ConnectedDevice> deviceList = new List<ConnectedDevice>();
            ConnectedDevice device;
            //string queryCmd = "SELECT TerminalMacAddress, DeviceId, DeviceType, DeviceName FROM ConnectedDevices " +
            //                   "WHERE TerminalMacAddress = @TerminalMacAddress";
            string queryCmd = "SELECT TerminalMacAddress, DeviceId, ConnectedDeviceTypes.Name, DeviceName FROM ConnectedDevices " +
                              "INNER JOIN ConnectedDeviceTypes ON ID = DeviceType " +
                              "WHERE TerminalMacAddress = @TerminalMacAddress";

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(queryCmd, conn))
                    {
                        // get the devices
                        cmd.Parameters.AddWithValue("@TerminalMacAddress", macAddress);
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            device = new ConnectedDevice();
                            device.macAddress = reader.GetString(0);
                            device.DeviceId = reader.GetString(1);
                            device.DeviceType = reader.GetString(2);
                            device.DeviceName = reader.GetString(3);
                            deviceList.Add(device);
                        }
                    }
                }
                
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOConnectedDevicesController: Error while executing GetDevicesForTerminal", "Terminal: " + macAddress, (short)ExceptionLevelEnum.Error));
            }
            return deviceList;
        }

        /// <summary>
        /// Returns a list of devices that are connected to a specific terminal
        /// and are of a specific typ
        /// </summary>
        /// <param name="terminalMAC">The MAC address of the terminal</param>
        /// <param name="deviceTypeID">The ID of the device type</param>
        /// <returns>A list of connected devices</returns>
        public List<ConnectedDevice> GetConnectedDevicesForTerminalByType(string terminalMAC, int deviceTypeID)
        {
            List<ConnectedDevice> deviceList = new List<ConnectedDevice>();
            ConnectedDevice device;
            //string queryCmd = "SELECT TerminalMacAddress, DeviceId, DeviceType, DeviceName FROM ConnectedDevices " +
            //                   "WHERE TerminalMacAddress = @TerminalMacAddress";
            string queryCmd = "SELECT TerminalMacAddress, DeviceId, ConnectedDeviceTypes.Name, DeviceName FROM ConnectedDevices " +
                              "INNER JOIN ConnectedDeviceTypes ON ID = DeviceType " +
                              "WHERE TerminalMacAddress = @TerminalMacAddress AND DeviceType = @DeviceType";

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(queryCmd, conn))
                    {
                        // get the devices
                        cmd.Parameters.AddWithValue("@TerminalMacAddress", terminalMAC);
                        cmd.Parameters.AddWithValue("@DeviceType", deviceTypeID);
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            device = new ConnectedDevice();
                            device.macAddress = reader.GetString(0);
                            device.DeviceId = reader.GetString(1);
                            device.DeviceType = reader.GetString(2);
                            device.DeviceName = reader.GetString(3);
                            deviceList.Add(device);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOConnectedDevicesController: Error while executing GetDevicesForTerminal", "Terminal: " + terminalMAC, (short)ExceptionLevelEnum.Error));
            }
            return deviceList;
        }

        /// <summary>
        /// Returns the details of a specific connected device
        /// </summary>
        /// <param name="deviceId">The ID of the connected device</param>
        /// <returns>The connect device object</returns>
        public ConnectedDevice GetDevice(string deviceId)
        {
            //string queryCmd = "SELECT TerminalMacAddress, DeviceId, DeviceType, DeviceName FROM ConnectedDevices " +
            //                   "WHERE DeviceId = @DeviceId";
            string queryCmd = "SELECT TerminalMacAddress, DeviceId, ConnectedDeviceTypes.Name, DeviceName FROM ConnectedDevices " +
                              "INNER JOIN ConnectedDeviceTypes ON ID = DeviceType " +
                              "WHERE DeviceId = @DeviceId";
            ConnectedDevice device = new ConnectedDevice();
                       
            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(queryCmd, conn))
                    {
                        // get the devices
                        cmd.Parameters.AddWithValue("@DeviceId", deviceId);
                        SqlDataReader reader = cmd.ExecuteReader();
                        reader.Read();
                        device.macAddress = reader.GetString(0);
                        device.DeviceId = reader.GetString(1);
                        device.DeviceType = reader.GetString(2);
                        device.DeviceName = reader.GetString(3);
                    }
                }

            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOConnectedDevicesController: Error while executing GetDevice", "Device: " + deviceId, (short)ExceptionLevelEnum.Error));
            }
            return device;
        }

        /// <summary>
        /// Helper method to get the terminal a specific device is connected to
        /// </summary>
        /// <param name="deviceId">The ID of the connected device</param>
        /// <returns>The MAC address of the terminal</returns>
        public string GetTerminalForDevice(string deviceId)
        {
            string queryCmd = "SELECT TerminalMacAddress FROM ConnectedDevices " +
                               "WHERE DeviceId = @DeviceId";
            string macAddress = "";

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(queryCmd, conn))
                    {
                        // get the devices
                        cmd.Parameters.AddWithValue("@DeviceId", deviceId);
                        SqlDataReader reader = cmd.ExecuteReader();
                        reader.Read();
                        macAddress = reader.GetString(0);
                        
                    }
                }

            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOConnectedDevicesController: Error while executing GetTerminalForDevice", "Device: " + deviceId, (short)ExceptionLevelEnum.Error));
            }
            return macAddress;
        }

        /// <summary>
        /// Updates the parameters of an existing connected device
        /// </summary>
        /// <param name="device">The device to be updated, including the new parameters</param>
        /// <returns>True if successful</returns>
        public bool UpdateDevice(ConnectedDevice device)
        {
            string updateCmd = "UPDATE ConnectedDevices " +
                               "SET TerminalMacAddress = @TerminalMacAddress, DeviceName = @DeviceName, DeviceType = @DeviceType " +
                               "WHERE DeviceId = @DeviceId";

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(updateCmd, conn))
                    {
                        // update the database database
                        cmd.Parameters.AddWithValue("@DeviceId", device.DeviceId);
                        cmd.Parameters.AddWithValue("@TerminalMacAddress", device.macAddress);
                        cmd.Parameters.AddWithValue("@DeviceName", device.DeviceName);
                        cmd.Parameters.AddWithValue("@DeviceType", device.DeviceType);

                        cmd.ExecuteNonQuery();
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOConnectedDevicesController: Error while executing UpdateDevice", "Device: " + device.DeviceId, (short)ExceptionLevelEnum.Error));
                return false;
            }
        }

        /// <summary>
        /// Retrieves a list of all connected devices in the CMT database
        /// </summary>
        /// <returns>The list of devices</returns>
        public List<ConnectedDevice> GetAllDevices()
        {
            List<ConnectedDevice> deviceList = new List<ConnectedDevice>();
            ConnectedDevice device;
            //string queryCmd = "SELECT TerminalMacAddress, DeviceId, DeviceType, DeviceName FROM ConnectedDevices";
            string queryCmd = "SELECT TerminalMacAddress, DeviceId, ConnectedDeviceTypes.Name, DeviceName FROM ConnectedDevices " +
                              "INNER JOIN ConnectedDeviceTypes ON ID = DeviceType"; 

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(queryCmd, conn))
                    {
                        // get the devices
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            device = new ConnectedDevice();
                            device.macAddress = reader.GetString(0);
                            device.DeviceId = reader.GetString(1);
                            device.DeviceType = reader.GetString(2);
                            device.DeviceName = reader.GetString(3);
                            deviceList.Add(device);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOConnectedDevicesController: Error while executing GetAllDevices", "", (short)ExceptionLevelEnum.Error));
            }
            return deviceList;
        }
        
        /// <summary>
        /// Retrieves a list of all connected device types in the CMT database
        /// </summary>
        /// <returns>The list of device types</returns>
        public List<ConnectedDeviceType> GetAllConnectedDeviceTypes()
        {
            List<ConnectedDeviceType> typeList = new List<ConnectedDeviceType>();
            ConnectedDeviceType type;
            string queryCmd = "SELECT ID, Name, Description FROM ConnectedDeviceTypes ";

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(queryCmd, conn))
                    {
                        // get the devices
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            type = new ConnectedDeviceType();
                            type.ID = reader.GetInt32(0);
                            type.Name = reader.GetString(1);
                            type.Description = reader.GetString(2);
                            typeList.Add(type);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOConnectedDevicesController: Error while executing GetAllConnectedDeviceTypes", "", (short)ExceptionLevelEnum.Error));
            }
            return typeList;
        }

        #endregion
    }
}
