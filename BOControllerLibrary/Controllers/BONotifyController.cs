﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Net.Mail;
using Amazon.SimpleEmail;
using Amazon.SimpleEmail.Model;
using BOControllerLibrary.Controllers.Interfaces;
using BOControllerLibrary.Model;
using BOControllerLibrary.Model.Log;
using log4net;

namespace BOControllerLibrary.Controllers
{
    /// <summary>
    ///   Just a helper for sending relevant mails and alerts. Could have been named NotifyHelper too.
    /// </summary>
    public class BONotifyController : BODataControllerBase
    {
        private readonly ILog _log = LogManager.GetLogger(typeof(BONotifyController));
        private readonly IBOLogController _logController;

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="connectionString">The CMT database connection string</param>
        /// <param name="databaseProvider">The database provider specified in the web config file</param>
        public BONotifyController(string connectionString, string databaseProvider)
        {
            ConnectionString = connectionString;
            DataProvider = databaseProvider;
            _logController = new BOLogController(connectionString, databaseProvider);
        }

        /// <summary>
        /// Sends a mail to flag the switch of a terminal to test mode.
        /// </summary>
        /// <param name="terminalId">The target terminal</param>
        public void SendTestModeAlert(string terminalId)
        {
            // get the emailaddresses belonging to this terminal
            // send the mail
            try
            {
                SendMail(string.Format("Testmode changed for Terminal {0}", terminalId), "TestMode alert", ConfigurationManager.AppSettings["SupportMail"]);
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex: ex, userDescription: "SendTestModeAlert", stateInformation: terminalId));
                throw;
            }
        }

        /// <summary>
        /// Sends a mail to the given toAddresses
        /// </summary>
        /// <param name="body">Body text of the mail</param>
        /// <param name="subject">The mail subject</param>
        /// <param name="toAddresses">A list of destination addresses</param>
        public bool SendMail(string body, string subject, params string[] toAddresses)
        {
            bool result = false;

            string useIt = ConfigurationManager.AppSettings["UseAmazonSes"];
            if (bool.Parse(useIt))
            {
                try
                {
                    var client = new AmazonSimpleEmailServiceClient(ConfigurationManager.AppSettings["AmazonAccessId"], ConfigurationManager.AppSettings["AmazonPrivateKey"]);
                    var msg = new SendEmailRequest();

                    // to addresses here
                    var destinationObj = new Destination(new List<string>(toAddresses));
                    msg.Source = ConfigurationManager.AppSettings["MailFromAddress"];
                    msg.ReturnPath = ConfigurationManager.AppSettings["MailFromAddress"];
                    msg.Destination = destinationObj;

                    var emailBody = new Body { Html = new Content(body) };
                    var emailMsg = new Message(new Content(subject), emailBody);
                    msg.Message = emailMsg;

                    client.SendEmail(msg);
                    result = true;
                }
                catch (Exception ex)
                {
                    result = false;
                    _logController.LogApplicationException(new CmtApplicationException(ex,
                                                    "Failed to send mail to destination via Amazon SES"
                                                    , "Failed", (short)ExceptionLevelEnum.Error));
                }
            }
            else
            {
                foreach (var str in toAddresses)
                {
                    try
                    {
                        var msg = new MailMessage(ConfigurationManager.AppSettings["MailFromAddress"], str, subject, body);
                        // we'll need to send this using Amazon SES i believe.
                        var client = new SmtpClient(ConfigurationManager.AppSettings["SMTPServer"], Int32.Parse(ConfigurationManager.AppSettings["SmtpPort"])) { EnableSsl = bool.Parse(ConfigurationManager.AppSettings["SmtpSsl"]) };
                        client.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["SMTPUserName"], ConfigurationManager.AppSettings["SMTPPassword"]);
                        client.Send(msg);
                        result = true;
                    }
                    catch (Exception ex)
                    {
                        result = false;
                        _logController.LogApplicationException(new CmtApplicationException(ex,
                                                        "Failed to send mail to destination via " + ConfigurationManager.AppSettings["SMTPServer"],
                                                        "Failed", (short)ExceptionLevelEnum.Error));
                    }
                }
            }

            return result;
        }
    }
}
