﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using BOControllerLibrary.Controllers.Interfaces;
using log4net;
using BOControllerLibrary.Model.Invoicing;
using PetaPoco;
using WCFInvoiceControllerService.Util;
using System.Data.SqlClient;

namespace BOControllerLibrary.Controllers
{
    /// <summary>
    /// The InvoiceController provides the functionality for external BSS application, to access the 
    /// invoicing data.
    /// </summary>
    public class BOInvoicingController : IBOInvoicingController
    {
      
        string _connectionString = "Data Source=192.168.100.111;Database=CMTData-Invoicing;User Id=SatFinAfrica; Password=Lagos#1302;";
        string _databaseProvider = "System.Data.SqlClient";
        private readonly ILog _log = LogManager.GetLogger(typeof(IBOAccountingController));
        private readonly IBOLogController _logController;
        private readonly BOConnectedDevicesController _boConnectedDevicesControl;

        /// <summary>
        /// 
        /// </summary>
        public BOInvoicingController()
        {
            _logController = new BOLogController(_connectionString, _databaseProvider);
            _boConnectedDevicesControl = new BOConnectedDevicesController(_connectionString, _databaseProvider);
        }

        //public BOInvoicingController(string databaseProvider)
        //{
        //    _databaseProvider = databaseProvider;
        //    _logController = new BOLogController(_connectionString, _databaseProvider);
        //    _boConnectedDevicesControl = new BOConnectedDevicesController(_connectionString, _databaseProvider);
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="databaseProvider"></param>
        public BOInvoicingController(string connectionString, string databaseProvider)
        {
            _connectionString = connectionString;
            _databaseProvider = databaseProvider;
            _logController = new BOLogController(_connectionString, _databaseProvider);
            _boConnectedDevicesControl = new BOConnectedDevicesController(_connectionString, _databaseProvider);
        }

        /// <summary>
        /// Returns the invoices for a given distributor
        /// These invoices don't contain their invoiceDetails
        /// </summary>
        /// <param name="distributorId">The distributor Id</param>
        /// <returns>A list of invoice headers for a specific distirbutor</returns>
        public List<InvoiceHeader> GetInvoicesForDistributor(int distributorId)
        {
            const string sql = @"
                                SELECT InvoiceYear, InvoiceNum, DistributorId, CreationDate, InvoiceMonth, CurrencyCode, InvoiceState, Remarks, DistributorName, ActiveTerminals, VAT, 
                                FinancialInterest, BankCharges, ExchangeRate, NoIncentive, AdvanceManual, AdvanceAutomatic, Id FROM InvoiceHeaders
                                WHERE DistributorId = @distributorId
                                ";
            var invoiceList = new List<InvoiceHeader>();

            try
            {
                using (var con = new SqlConnection(_connectionString))
                {
                    con.Open();

                    using (var cmd = new SqlCommand(sql, con))
                    {
                        cmd.Parameters.AddWithValue("@distributorId", distributorId);
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            InvoiceHeader ih = new InvoiceHeader();
                            ih.InvoiceYear = reader.GetInt32(0);
                            ih.InvoiceNum = reader.GetInt32(1);
                            ih.DistributorId = reader.GetInt32(2);
                            ih.CreationDate = reader.GetDateTime(3);
                            ih.InvoiceMonth = reader.GetInt32(4);
                            ih.CurrencyCode = reader.GetString(5);
                            ih.InvoiceState = reader.GetInt16(6);
                            if (!reader.IsDBNull(7))
                            {
                                ih.Remarks = reader.GetString(7);
                            }
                            if (!reader.IsDBNull(8))
                            {
                                ih.DistributorName = reader.GetString(8);
                            }
                            if (!reader.IsDBNull(9))
                            {
                                ih.ActiveTerminals = reader.GetInt32(9);
                            }
                            if (!reader.IsDBNull(10))
                            {
                                ih.VAT = reader.GetDecimal(10);
                            }
                            if (!reader.IsDBNull(11))
                            {
                                ih.FinancialInterest = reader.GetDecimal(11);
                            }
                            ih.BankCharges = reader.GetBoolean(12);
                            if (!reader.IsDBNull(13))
                            {
                                ih.ExchangeRate = reader.GetDecimal(13);
                            }
                            ih.NoIncentive = reader.GetBoolean(14);
                            if (!reader.IsDBNull(15))
                            {
                                ih.AdvanceManual = reader.GetDecimal(15);
                            }
                            if (!reader.IsDBNull(16))
                            {
                                ih.AdvanceAutomatic = reader.GetDecimal(16);
                            }
                            ih.Id = reader.GetInt32(17);
                            invoiceList.Add(ih);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log(ex);

                throw;
            }
            return invoiceList;
        }

        /// <summary>
        /// Returns the unpaid invoices for a given distributor
        /// These invoices don't contain their invoiceDetails
        /// </summary>
        /// <param name="distributorId">The distributor Id</param>
        /// <returns>A list of invoice headers for a specific distirbutor</returns>
        public List<InvoiceHeader> GetUnpaidInvoicesForDistributor(int distributorId)
        {
            const string sql = @"SELECT InvoiceYear, InvoiceNum, DistributorId, CreationDate, InvoiceMonth, CurrencyCode, 
                                InvoiceState, Remarks, DistributorName, ActiveTerminals, VAT, 
                                FinancialInterest, BankCharges, ExchangeRate, NoIncentive, AdvanceManual, AdvanceAutomatic, Id FROM InvoiceHeaders
                                WHERE DistributorId = @distributorId AND (InvoiceState = 2 OR InvoiceState = 3 )";
            var invoiceList = new List<InvoiceHeader>();

            try
            {
                using (var con = new SqlConnection(_connectionString))
                {
                    con.Open();

                    using (var cmd = new SqlCommand(sql, con))
                    {
                        cmd.Parameters.AddWithValue("@distributorId", distributorId);
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            InvoiceHeader ih = new InvoiceHeader();
                            ih.InvoiceYear = reader.GetInt32(0);
                            ih.InvoiceNum = reader.GetInt32(1);
                            ih.DistributorId = reader.GetInt32(2);
                            ih.CreationDate = reader.GetDateTime(3);
                            ih.InvoiceMonth = reader.GetInt32(4);
                            ih.CurrencyCode = reader.GetString(5);
                            ih.InvoiceState = reader.GetInt16(6);
                            if (!reader.IsDBNull(7))
                            {
                                ih.Remarks = reader.GetString(7);
                            }
                            if (!reader.IsDBNull(8))
                            {
                                ih.DistributorName = reader.GetString(8);
                            }
                            if (!reader.IsDBNull(9))
                            {
                                ih.ActiveTerminals = reader.GetInt32(9);
                            }
                            if (!reader.IsDBNull(10))
                            {
                                ih.VAT = reader.GetDecimal(10);
                            }
                            if (!reader.IsDBNull(11))
                            {
                                ih.FinancialInterest = reader.GetDecimal(11);
                            }
                            ih.BankCharges = reader.GetBoolean(12);
                            if (!reader.IsDBNull(13))
                            {
                                ih.ExchangeRate = reader.GetDecimal(13);
                            }
                            ih.NoIncentive = reader.GetBoolean(14);
                            if (!reader.IsDBNull(15))
                            {
                                ih.AdvanceManual = reader.GetDecimal(15);
                            }
                            if (!reader.IsDBNull(16))
                            {
                                ih.AdvanceAutomatic = reader.GetDecimal(16);
                            }
                            ih.Id = reader.GetInt32(17);
                            invoiceList.Add(ih);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log(ex);
            }
            return invoiceList;
        }

        /// <summary>
        /// Returns the list of detail lines for given invoice
        /// </summary>
        /// <param name="year">Invoice year</param>
        /// <param name="num">Invoice number</param>
        /// <returns>A list of invoice lines, this list can be empty</returns>
        public List<InvoiceDetail> GetInvoiceLines(int id)
        {

            const string sql = @"
                                SELECT Id, InvoiceYear, InvoiceNum, Bid, BillableDescription, UnitPrice, Items, TerminalId, Serial, ServicePack,
                                MacAddress, Status, ActivityDate, InvoiceId, PaidInAdvance FROM InvoiceDetails
                                WHERE InvoiceId = @id
                                ";
            var invoiceDetailsList = new List<InvoiceDetail>();

            try
            {
                using (var con = new SqlConnection(_connectionString))
                {
                    con.Open();

                    using (var cmd = new SqlCommand(sql, con))
                    {
                        cmd.Parameters.AddWithValue("@id", id);
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            InvoiceDetail detail = new InvoiceDetail();
                            detail.ID = reader.GetInt32(0);
                            detail.InvoiceYear = reader.GetInt32(1);
                            detail.InvoiceNum = reader.GetInt32(2);
                            if (!reader.IsDBNull(3))
                            {
                                detail.Bid = reader.GetInt32(3);
                            }
                            detail.BillableDescription = reader.GetString(4);
                            detail.UnitPrice = reader.GetDecimal(5);
                            detail.Items = reader.GetInt32(6);
                            if (!reader.IsDBNull(7))
                            {
                                detail.TerminalId = reader.GetString(7);
                            }
                            if (!reader.IsDBNull(8))
                            {
                                detail.Serial = reader.GetString(8);
                            }
                            if (!reader.IsDBNull(9))
                            {
                                detail.ServicePack = reader.GetString(9);
                            }
                            if (!reader.IsDBNull(10))
                            {
                                detail.MacAddress = reader.GetString(10);
                            }
                            if (!reader.IsDBNull(11))
                            {
                                detail.Status = reader.GetString(11);
                            }
                            if (!reader.IsDBNull(12))
                            {
                                detail.ActivityDate = reader.GetDateTime(12);
                            }
                            detail.InvoiceId = reader.GetInt32(13);
                            detail.PaidInAdvance = reader.GetBoolean(14);
                            invoiceDetailsList.Add(detail);
                        }
                    }
                }
                //using (var db = new Database(_connectionString, _databaseProvider))
                //{
                //    List<InvoiceDetail> dbObj = db.Fetch<InvoiceDetail>(sql, new { id });
                //    return dbObj.ToList();
                //}
            }
            catch (Exception ex)
            {
                this.Log(ex);

                throw;
            }
            return invoiceDetailsList;
        }

        /// <summary>
        /// Returns an invoice identified by its year and number
        /// This invoice also contains its invoiceDetails
        /// </summary>
        /// <param name="year">Year the invoice was issued</param>
        /// <param name="num">Sequence number of the invoice</param>
        /// <returns>An invoice header + detail lines or null if not found</returns>
        public InvoiceHeader GetInvoice(int id)
        {

            InvoiceHeader ih = new InvoiceHeader();
            const string sql = @"
                                SELECT InvoiceYear, InvoiceNum, DistributorId, CreationDate, InvoiceMonth, CurrencyCode, InvoiceState, Remarks, DistributorName, ActiveTerminals, VAT, 
                                FinancialInterest, BankCharges, ExchangeRate, NoIncentive, AdvanceManual, AdvanceAutomatic, Id FROM InvoiceHeaders
                                WHERE Id = @id
                                ";
            try
            {
                using (var con = new SqlConnection(_connectionString))
                {
                    con.Open();

                    using (var cmd = new SqlCommand(sql, con))
                    {
                        cmd.Parameters.AddWithValue("@id", id);
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            ih.InvoiceYear = reader.GetInt32(0);
                            ih.InvoiceNum = reader.GetInt32(1);
                            ih.DistributorId = reader.GetInt32(2);
                            ih.CreationDate = reader.GetDateTime(3);
                            ih.InvoiceMonth = reader.GetInt32(4);
                            ih.CurrencyCode = reader.GetString(5);
                            ih.InvoiceState = reader.GetInt16(6);
                            if (!reader.IsDBNull(7))
                            {
                                ih.Remarks = reader.GetString(7);
                            }
                            if (!reader.IsDBNull(8))
                            {
                                ih.DistributorName = reader.GetString(8);
                            }
                            if (!reader.IsDBNull(9))
                            {
                                ih.ActiveTerminals = reader.GetInt32(9);
                            }
                            if (!reader.IsDBNull(10))
                            {
                                ih.VAT = reader.GetDecimal(10);
                            }
                            if (!reader.IsDBNull(11))
                            {
                                ih.FinancialInterest = reader.GetDecimal(11);
                            }
                            ih.BankCharges = reader.GetBoolean(12);
                            if (!reader.IsDBNull(13))
                            {
                                ih.ExchangeRate = reader.GetDecimal(13);
                            }
                            ih.NoIncentive = reader.GetBoolean(14);
                            if (!reader.IsDBNull(15))
                            {
                                ih.AdvanceManual = reader.GetDecimal(15);
                            }
                            if (!reader.IsDBNull(16))
                            {
                                ih.AdvanceAutomatic = reader.GetDecimal(16);
                            }
                            ih.Id = reader.GetInt32(17);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log(ex);

                throw;
            }
            return ih;
        }

        /// <summary>
        /// Get all unvalidated invoices
        /// These invoices don't contain their invoiceDetails
        /// </summary>
        /// <returns>A list of invoices headers (and details)</returns>
        public List<InvoiceHeader> GetUnvalidatedInvoices()
        {
            try
            {
                const string sql = @"
                                    SELECT * FROM InvoiceHeaders
                                    WHERE InvoiceState = 0
                                    ";

                using (var db = new Database(_connectionString, _databaseProvider))
                {
                    List<InvoiceHeader> dbObj = db.Fetch<InvoiceHeader>(sql);
                    return dbObj.ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log(ex);

                throw;
            }
        }

        /// <summary>
        /// Get all invoices for a given year and month
        /// These invoices don't contain their invoiceDetails
        /// </summary>
        /// <param name="year">The year the invoices were issued</param>
        /// <param name="month">The month the invoices were issued</param>
        /// <returns>A list of invoices headers (and details) for a given year and month</returns>
        public List<InvoiceHeader> GetInvoices(int year, int month)
        {

            const string sql = @"
                                SELECT InvoiceYear, InvoiceNum, DistributorId, CreationDate, InvoiceMonth, CurrencyCode, InvoiceState, Remarks, DistributorName, ActiveTerminals, VAT, 
                                FinancialInterest, BankCharges, ExchangeRate, NoIncentive, AdvanceManual, AdvanceAutomatic, Id FROM InvoiceHeaders
                                WHERE InvoiceYear = @year AND InvoiceMonth = @month
                                ";

            var invoiceList = new List<InvoiceHeader>();

            try
            {
                using (var con = new SqlConnection(_connectionString))
                {
                    con.Open();

                    using (var cmd = new SqlCommand(sql, con))
                    {
                        cmd.Parameters.AddWithValue("@year", year);
                        cmd.Parameters.AddWithValue("@month", month);
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            InvoiceHeader ih = new InvoiceHeader();
                            ih.InvoiceYear = reader.GetInt32(0);
                            ih.InvoiceNum = reader.GetInt32(1);
                            ih.DistributorId = reader.GetInt32(2);
                            ih.CreationDate = reader.GetDateTime(3);
                            ih.InvoiceMonth = reader.GetInt32(4);
                            ih.CurrencyCode = reader.GetString(5);
                            ih.InvoiceState = reader.GetInt16(6);
                            if (!reader.IsDBNull(7))
                            {
                                ih.Remarks = reader.GetString(7);
                            }
                            if (!reader.IsDBNull(8))
                            {
                                ih.DistributorName = reader.GetString(8);
                            }
                            if (!reader.IsDBNull(9))
                            {
                                ih.ActiveTerminals = reader.GetInt32(9);
                            }
                            if (!reader.IsDBNull(10))
                            {
                                ih.VAT = reader.GetDecimal(10);
                            }
                            if (!reader.IsDBNull(11))
                            {
                                ih.FinancialInterest = reader.GetDecimal(11);
                            }
                            ih.BankCharges = reader.GetBoolean(12);
                            if (!reader.IsDBNull(13))
                            {
                                ih.ExchangeRate = reader.GetDecimal(13);
                            }
                            ih.NoIncentive = reader.GetBoolean(14);
                            if (!reader.IsDBNull(15))
                            {
                                ih.AdvanceManual = reader.GetDecimal(15);
                            }
                            if (!reader.IsDBNull(16))
                            {
                                ih.AdvanceAutomatic = reader.GetDecimal(16);
                            }
                            ih.Id = reader.GetInt32(17);
                            invoiceList.Add(ih);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log(ex);

                throw;
            }
            return invoiceList;
        }

        /// <summary>
        /// Gets the IDs of the invoiceDetails for one invoiceHeader.
        /// This method could be used to get the invoiceDetails separately
        /// by looping over the retrieved IDs.
        /// This could for example be the case when the data to retrieve is too large to send to the client.
        /// </summary>
        /// <param name="invoiceYear">The year of the invoice</param>
        /// <param name="invoiceNum">The number of the invoice in that year</param>
        /// <returns>A list with the IDs of the invoice detail lines of that invoice</returns>
        public List<int> GetInvoiceDetailIdsForInvoice(int invoiceId)
        {
            const string sql = @"
                                SELECT Id FROM InvoiceDetails
                                WHERE InvoiceId = @invoiceId
                                ";
            var idList = new List<int>();

            try
            {
                using (var con = new SqlConnection(_connectionString))
                {
                    con.Open();

                    using (var cmd = new SqlCommand(sql, con))
                    {
                        cmd.Parameters.AddWithValue("@invoiceId", invoiceId);
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            int id = reader.GetInt32(0);
                            idList.Add(id);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log(ex);

                throw;
            }
            return idList;
        }

        /// <summary>
        /// Gets sum of transactions for invoice
        /// </summary>
        /// <param name="invoiceId">The id of the invoice</param>
        /// <returns>Sum of all transactions for this invoice</returns>
        public InvoiceTransaction GetTransaction(int invoiceId)
        {
            //const string sql = @"SELECT Type, Amount FROM Transactions WHERE Invoice_Id = @invoiceId";
            const string sql = @"SELECT TOP 1 Type, Amount, Remark FROM Transactions WHERE Invoice_Id = @invoiceId ORDER BY Id DESC";
            //CMT-102 last value entered is the total credit/debit
            InvoiceTransaction transaction = new InvoiceTransaction() { Amount = 0, Remark = string.Empty } ;
            try
            {
                using (var con = new SqlConnection(_connectionString))
                {
                    con.Open();
                    using (var cmd = new SqlCommand(sql, con))
                    {
                        cmd.Parameters.AddWithValue("@invoiceId", invoiceId);
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            string type = reader.GetString(0);
                            transaction.Amount = reader.GetDecimal(1);
                            if (type == "credit")
                            {
                                transaction.Amount = transaction.Amount * -1;
                            }
                            if (!reader.IsDBNull(2))
                                transaction.Remark = reader.GetString(2);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log(ex);
                throw;
            }
            return transaction;
        }

        /// <summary>
        /// Adds a new invoice to the invoice list
        /// </summary>
        /// <remarks>
        /// An invoice contains an invoice header AND all detail lines which are allocated to the inoivce!
        /// </remarks>
        /// <param name="invoice">The invoice (invoice header + details) to add</param>
        /// <returns>The ID of the newly created invoice, or null when an error occured.</returns>
        public int? AddInvoice(InvoiceHeader invoice)
        {
            string sqlCmd = @"
                            INSERT INTO InvoiceHeaders (ActiveTerminals, VAT, FinancialInterest, BankCharges, InvoiceYear, InvoiceNum,
                            DistributorId, InvoiceMonth, CreationDate, CurrencyCode, InvoiceState, DistributorName, AdvanceAutomatic) 
                            VALUES (@activeTerminals, @VAT, @FinancialInterest, @BankCharges, @InvoiceYear, @InvoiceNum,
                            @DistributorId, @InvoiceMonth, @CreationDate, @CurrencyCode, @InvoiceState, @DistributorName, @Automatic)
                            ";
            string getLatestInvoiceId = @"
                                        SELECT Id from InvoiceHeaders
                                        ";

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(sqlCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@activeTerminals", invoice.ActiveTerminals);
                        cmd.Parameters.AddWithValue("@VAT", invoice.VAT);
                        cmd.Parameters.AddWithValue("@FinancialInterest", invoice.FinancialInterest);
                        cmd.Parameters.AddWithValue("@BankCharges", invoice.BankCharges);
                        cmd.Parameters.AddWithValue("@InvoiceYear", invoice.InvoiceYear);
                        cmd.Parameters.AddWithValue("@InvoiceNum", invoice.InvoiceNum);
                        cmd.Parameters.AddWithValue("@DistributorId", invoice.DistributorId);
                        cmd.Parameters.AddWithValue("@InvoiceMonth", invoice.InvoiceMonth);
                        cmd.Parameters.AddWithValue("@CreationDate", invoice.CreationDate);
                        cmd.Parameters.AddWithValue("@CurrencyCode", invoice.CurrencyCode);
                        cmd.Parameters.AddWithValue("@InvoiceState", invoice.InvoiceState);
                        cmd.Parameters.AddWithValue("@DistributorName", invoice.DistributorName);
                        cmd.Parameters.AddWithValue("@Automatic", invoice.AdvanceAutomatic);
                        cmd.ExecuteNonQuery();
                    }
                }
                using (var db = new Database(_connectionString, _databaseProvider))
                {
                    List<int> dbObj = db.Fetch<int>(getLatestInvoiceId);
                    return dbObj.Last();
                }
            }
            catch (Exception ex)
            {
                this.Log(ex);

                throw;
            }

        }

        /// <summary>
        /// This method queries the invoice tables by means of a number of criteria. Only
        /// the criteria which are not null are taken into account.
        /// These invoices don't contain their invoiceDetails
        /// </summary
        /// <param name="year">Invoice year</param>
        /// <param name="month">Invoice month</param>
        /// <param name="invoiceNum">Invoice number</param>
        /// <param name="invoiceState">The invoice status. 0 = Unvalidated, 1 = validated
        /// 2 = Released, 3 = Paid</param>
        /// <param name="distId">The Distributor Id</param>
        /// <returns>The list of selected invoice headers. This list can be empty</returns>
        public List<InvoiceHeader> QueryInvoices(String year, String month, String invoiceNum, String invoiceState, String distId)
        {
            List<InvoiceHeader> invoices = new List<InvoiceHeader>();
            String queryString = this.createLinqQuery(new List<string> { year }, new List<string> { month }, new List<string> { invoiceNum }, new List<string> { invoiceState }, new List<string> { distId });

            if (queryString != "")
            {
                try
                {
                    string sql = "SELECT * FROM InvoiceHeaders WHERE " + queryString + " ORDER BY InvoiceNum";

                    using (var db = new Database(_connectionString, _databaseProvider))
                    {
                        invoices = db.Fetch<InvoiceHeader>(sql);
                    }
                }
                catch (Exception ex)
                {
                    this.Log(ex);
                }
                return invoices;
            }
            return invoices;
        }

        /// <summary>
        /// This method queries the invoice tables by means of a number of criteria. Only
        /// the criteria which are not null are taken into account.
        /// An AND operator is put between the different arguments, an OR operator between the same arguments.
        /// These invoices don't contain their invoiceDetails
        /// </summary
        /// <param name="years">Invoice year</param>
        /// <param name="months">Invoice month</param>
        /// <param name="InvoiceNums">Invoice number</param>
        /// <param name="invoiceStates">The invoice status. 0 = Unvalidated, 1 = validated
        /// 2 = Released, 3 = Paid</param>
        /// <param name="distIds">The Distributor Id</param>
        /// <returns>The list of selected invoice headers. This list can be empty</returns>
        public List<InvoiceHeader> QueryInvoicesMultipleArgs(
            List<string> years,
            List<string> months,
            List<string> invoiceNums,
            List<string> invoiceStates,
            List<string> distIds)
        {
            List<InvoiceHeader> invoices = new List<InvoiceHeader>();
            String queryString = this.createLinqQuery(years, months, invoiceNums, invoiceStates, distIds);

            if (queryString != "")
            {
                try
                {
                    string sql = "SELECT * FROM InvoiceHeaders WHERE " + queryString + " ORDER BY InvoiceNum";

                    using (var db = new Database(_connectionString, _databaseProvider))
                    {
                        invoices = db.Fetch<InvoiceHeader>(sql);
                    }
                }
                catch (Exception ex)
                {
                    this.Log(ex);
                }
            }

            return invoices;
        }


        /// <summary>
        /// Inserts the billable in the billable table
        /// </summary>
        /// <param name="bill">The billable update to add</param>
        /// <returns>True if the operation succeeded</returns>
        public bool AddBillable(Billable bill)
        {
            bool success = false;
            string sqlCmd = "INSERT INTO Billables (Description, AccountId, PriceEU, PriceUSD, UserId, LastUpdate, Deleted, ValidFromDate, ValidToDate) " +
                            "VALUES (@description, @accountId, @priceEU, @priceUSD, @userId, @lastUpdate, @deleted, @validFromDate, @validToDate)";

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(sqlCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@description", bill.Description);
                        cmd.Parameters.AddWithValue("@accountId", bill.AccountId);
                        cmd.Parameters.AddWithValue("@priceEU", bill.PriceEU);
                        cmd.Parameters.AddWithValue("@priceUSD", bill.PriceUSD);
                        cmd.Parameters.AddWithValue("@userId", bill.UserId);
                        cmd.Parameters.AddWithValue("@lastUpdate", bill.LastUpdate);
                        cmd.Parameters.AddWithValue("@deleted", bill.Deleted);
                        cmd.Parameters.AddWithValue("@validFromDate", GetDataValue(bill.ValidFromDate));
                        cmd.Parameters.AddWithValue("@validToDate", GetDataValue(bill.ValidToDate));
                        cmd.ExecuteNonQuery();
                        success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log(ex);
            }

            return success;
        }

        /// <summary>
        /// Inserts the account in the account table
        /// </summary>
        /// <param name="acc">The account to update</param>
        /// <returns>True if the operation succeeded</returns>
        public bool AddAccount(Account acc)
        {
            bool success = false;
            string sqlCmd = "INSERT INTO Accounts (Description, Remarks, UserId, LastUpdate, Deleted) " +
                            "VALUES (@description, @remarks, @userId, @lastUpdate, @deleted)";

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(sqlCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@description", acc.Description);
                        cmd.Parameters.AddWithValue("@remarks", acc.Remarks);
                        cmd.Parameters.AddWithValue("@userId", acc.UserId);
                        cmd.Parameters.AddWithValue("@lastUpdate", acc.LastUpdate);
                        cmd.Parameters.AddWithValue("@deleted", acc.Deleted);
                        cmd.ExecuteNonQuery();
                        success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log(ex);
            }

            return success;
        }

        /// <summary>
        /// Adds a new advance Payment
        /// </summary>
        /// <param name="advancePayment">Advance Payment Value</param>
        /// <param name="distributorId">Distributor ID</param>
        /// <returns>True if the operation succeeded</returns>
        public bool AddAdvancePayment(decimal advancePayment, int distributorId)
        {
            bool success = false;
            string sqlCmd = "INSERT INTO AdvancePayments (AdvancePayment, DistributorID, CreationDate) " +
                            "VALUES (@advancePayment, @distributorId, @creationDate)";

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(sqlCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@advancePayment", advancePayment);
                        cmd.Parameters.AddWithValue("@distributorId", distributorId);
                        cmd.Parameters.AddWithValue("@creationDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm"));
                        cmd.ExecuteNonQuery();
                        success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log(ex);
            }

            return success;
        }

        /// <summary>
        /// Adds a transaction
        /// </summary>
        /// <param name="type">type of transaction: "credit" "debit"</param>
        /// <param name="amount">monetary amount</param>
        /// <param name="distributorId">Distributor ID</param>
        /// <param name="invoiceId">Invoice ID</param>
        /// <param name="remark">remark that will show up on the invoice</param>
        /// <returns>True if the operation succeeded</returns>
        public bool AddTransaction(string type, decimal amount, int distributorId, int invoiceId, string remark)
        {
            bool success = false;
            string sqlCmd = "INSERT INTO Transactions (Type, Amount, Creation_Date, Distributor_Id, Invoice_Id, Remark) " +
                            "VALUES (@type, @amount, @creationDate, @distributorId, @invoiceId, @remark)";

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(sqlCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@type", type);
                        cmd.Parameters.AddWithValue("@amount", amount);
                        cmd.Parameters.AddWithValue("@creationDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm"));
                        cmd.Parameters.AddWithValue("@distributorId", distributorId);
                        cmd.Parameters.AddWithValue("@invoiceId", invoiceId);
                        cmd.Parameters.AddWithValue("@remark", remark);
                        cmd.ExecuteNonQuery();
                        success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log(ex);
            }

            return success;
        }

        /// <summary>
        /// Inserts the Distributor billable in the DistBillable table
        /// </summary>
        /// <param name="distBill">The distributor billable to insert</param>
        /// <returns>True if the operation succeeded</returns>
        public bool AddDistBillable(DistBillable distBill)
        {
            bool success = false;
            string sqlCmd = "INSERT INTO DistBillables (Bid, DistId, PriceEU, PriceUSD, Remarks, UserId, LastUpdate, Deleted) " +
                            "VALUES (@bid, @distId, @priceEU, @priceUSD, @remarks, @userId, @lastUpdate, @deleted)";

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(sqlCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@bid", distBill.Bid);
                        cmd.Parameters.AddWithValue("@distId", distBill.DistId);
                        cmd.Parameters.AddWithValue("@priceEU", distBill.PriceEU);
                        cmd.Parameters.AddWithValue("@priceUSD", distBill.PriceUSD);
                        cmd.Parameters.AddWithValue("@remarks", distBill.Remarks);
                        cmd.Parameters.AddWithValue("@userId", distBill.UserId);
                        cmd.Parameters.AddWithValue("@lastUpdate", distBill.LastUpdate);
                        cmd.Parameters.AddWithValue("@deleted", distBill.Deleted);
                        cmd.ExecuteNonQuery();
                        success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log(ex);
            }

            return success;
        }

        /// <summary>
        /// Adds an invoice detail line
        /// </summary>
        /// <param name="detailLine"></param>
        /// <returns>True if the operation succeeded</returns>
        public bool AddInvoiceDetailLine(InvoiceDetail detailLine)
        {
            bool success = false;
            string sqlCmd = "INSERT INTO InvoiceDetails (InvoiceYear, InvoiceNum, Bid, BillableDescription, UnitPrice, Items, " +
                            "TerminalId, Serial, ServicePack, MacAddress, Status, ActivityDate, InvoiceId, PaidInAdvance) " +
                            "VALUES (@invoiceYear, @invoiceNum, @Bid, @billableDescription, @unitPrice, @items, " +
                            "@terminalId, @Serial, @ServicePack, @macAddress, @status, @activityDate, @invoiceId, @paidInAdvance) ";

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(sqlCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@invoiceYear", detailLine.InvoiceYear);
                        cmd.Parameters.AddWithValue("@invoiceNum", detailLine.InvoiceNum);
                        cmd.Parameters.AddWithValue("@bid", GetDataValue(detailLine.Bid));
                        cmd.Parameters.AddWithValue("@billableDescription", detailLine.BillableDescription);
                        cmd.Parameters.AddWithValue("@unitPrice", detailLine.UnitPrice);
                        cmd.Parameters.AddWithValue("@items", detailLine.Items);
                        cmd.Parameters.AddWithValue("@terminalId", detailLine.TerminalId);
                        cmd.Parameters.AddWithValue("@serial", GetDataValue(detailLine.Serial));
                        cmd.Parameters.AddWithValue("@servicePack", GetDataValue(detailLine.ServicePack));
                        cmd.Parameters.AddWithValue("@macAddress", GetDataValue(detailLine.MacAddress));
                        cmd.Parameters.AddWithValue("@status", GetDataValue(detailLine.Status));
                        cmd.Parameters.AddWithValue("@activityDate", GetDataValue(detailLine.ActivityDate));
                        cmd.Parameters.AddWithValue("@invoiceId", detailLine.InvoiceId);
                        cmd.Parameters.AddWithValue("@paidInAdvance", GetDataValue(detailLine.PaidInAdvance));
                        cmd.ExecuteNonQuery();
                        success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log(ex);
            }

            return success;
        }

        /// <summary>
        /// Modifies the given billable in the Billable table
        /// </summary>
        /// <param name="bill">The billable to update</param>
        /// <returns>True if the operation succeeded</returns>
        public bool UpdateBillable(Billable bill)
        {
            bool success = false;
            string sqlCmd = "UPDATE Billables SET Description = @description, AccountId = @accountId, PriceEU = @priceEU, " +
                            "PriceUSD = @priceUSD, UserId = @userId, LastUpdate = @lastUpdate, " +
                            "Deleted = @deleted, ValidFromDate = @validFromDate, ValidToDate = @validToDate " +
                            "WHERE Bid = @bid";

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(sqlCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@description", bill.Description);
                        cmd.Parameters.AddWithValue("@accountId", bill.AccountId);
                        cmd.Parameters.AddWithValue("@priceEU", bill.PriceEU);
                        cmd.Parameters.AddWithValue("@priceUSD", bill.PriceUSD);
                        cmd.Parameters.AddWithValue("@userId", bill.UserId);
                        cmd.Parameters.AddWithValue("@lastUpdate", bill.LastUpdate);
                        cmd.Parameters.AddWithValue("@deleted", bill.Deleted);
                        cmd.Parameters.AddWithValue("@validFromDate", GetDataValue(bill.ValidFromDate));
                        cmd.Parameters.AddWithValue("@validToDate", GetDataValue(bill.ValidToDate));
                        cmd.Parameters.AddWithValue("@bid", bill.Bid);
                        cmd.ExecuteNonQuery();
                        success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log(ex);
            }

            return success;
        }

        /// <summary>
        /// Modifies the account entity in the Account table
        /// </summary>
        /// <param name="acc">The Account entity</param>
        /// <returns>True if the operation succeeded</returns>
        public bool UpdateAccount(Account acc)
        {
            bool success = false;
            string sqlCmd = "UPDATE Accounts SET Description = @description, Remarks = @remarks, UserId = @userId, LastUpdate = @lastUpdate, Deleted = @deleted " +
                            "WHERE AccountId = @accountId";

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(sqlCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@description", acc.Description);
                        cmd.Parameters.AddWithValue("@remarks", acc.Remarks);
                        cmd.Parameters.AddWithValue("@userId", acc.UserId);
                        cmd.Parameters.AddWithValue("@lastUpdate", acc.LastUpdate);
                        cmd.Parameters.AddWithValue("@deleted", acc.Deleted);
                        cmd.Parameters.AddWithValue("@accountId", acc.AccountId);
                        cmd.ExecuteNonQuery();
                        success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log(ex);
            }

            return success;
        }

        /// <summary>
        /// Updates the DistBillable entity in the DistBillable table
        /// </summary>
        /// <param name="distBill">The DistBillable entity to update</param>
        /// <returns>True if the operation succeeded</returns>
        public bool UpdateDistBillable(DistBillable distBill)
        {
            bool success = false;
            string sqlCmd = "UPDATE DistBillables SET Bid = @bid, DistId = @distId, PriceEU = @priceEU, PriceUSD = @priceUSD, " +
                            "Remarks = @remarks, UserId = @userId, LastUpdate = @lastUpdate, Deleted = @deleted " +
                            "WHERE Id = @id";

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(sqlCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@bid", distBill.Bid);
                        cmd.Parameters.AddWithValue("@distId", distBill.DistId);
                        cmd.Parameters.AddWithValue("@priceEU", distBill.PriceEU);
                        cmd.Parameters.AddWithValue("@priceUSD", distBill.PriceUSD);
                        cmd.Parameters.AddWithValue("@remarks", distBill.Remarks);
                        cmd.Parameters.AddWithValue("@userId", distBill.UserId);
                        cmd.Parameters.AddWithValue("@lastUpdate", distBill.LastUpdate);
                        cmd.Parameters.AddWithValue("@deleted", distBill.Deleted);
                        cmd.Parameters.AddWithValue("@id", distBill.Id);
                        cmd.ExecuteNonQuery();
                        success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log(ex);
            }

            return success;
        }

        /// <summary>
        /// Updates the invoice in the invoiceHeader table.
        /// Only the InvoiceState and Remarks property will be updated.
        /// </summary>
        /// <param name="invoice">The invoice to update</param>
        /// <returns>True if the operation succeeded</returns>
        public bool UpdateInvoice(InvoiceHeader invoice)
        {
            bool success = false;
            string sqlCmd = "UPDATE InvoiceHeaders SET InvoiceYear = @invoiceYear, InvoiceNum = @invoiceNum, DistributorId = @distributorId, " +
                            "CreationDate = @creationDate, InvoiceMonth = @invoiceMonth, CurrencyCode = @currencyCode, InvoiceState = @invoiceState, " +
                            "Remarks = @remarks, DistributorName = @distributorName, ActiveTerminals = @activeTerminals, VAT = @vat, " +
                            "FinancialInterest = @financialInterest, BankCharges = @bankCharges, ExchangeRate = @exchangeRate, NoIncentive = @noIncentive, " +
                            "AdvanceManual = @advanceManual, AdvanceAutomatic = @advanceAutomatic " +
                            "WHERE Id = @id";

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(sqlCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@invoiceYear", invoice.InvoiceYear);
                        cmd.Parameters.AddWithValue("@invoiceNum", invoice.InvoiceNum);
                        cmd.Parameters.AddWithValue("@distributorId", invoice.DistributorId);
                        cmd.Parameters.AddWithValue("@creationDate", invoice.CreationDate);
                        cmd.Parameters.AddWithValue("@invoiceMonth", invoice.InvoiceMonth);
                        cmd.Parameters.AddWithValue("@currencyCode", invoice.CurrencyCode);
                        cmd.Parameters.AddWithValue("@invoiceState", invoice.InvoiceState);
                        cmd.Parameters.AddWithValue("@remarks", GetDataValue(invoice.Remarks));
                        cmd.Parameters.AddWithValue("@distributorName", invoice.DistributorName);
                        cmd.Parameters.AddWithValue("@activeTerminals", GetDataValue(invoice.ActiveTerminals));
                        cmd.Parameters.AddWithValue("@vat", GetDataValue(invoice.VAT));
                        cmd.Parameters.AddWithValue("@financialInterest", GetDataValue(invoice.FinancialInterest));
                        cmd.Parameters.AddWithValue("@bankCharges", GetDataValue(invoice.BankCharges));
                        cmd.Parameters.AddWithValue("@exchangeRate", GetDataValue(invoice.ExchangeRate));
                        cmd.Parameters.AddWithValue("@noIncentive", GetDataValue(invoice.NoIncentive));
                        cmd.Parameters.AddWithValue("@advanceManual", GetDataValue(invoice.AdvanceManual));
                        cmd.Parameters.AddWithValue("@advanceAutomatic", GetDataValue(invoice.AdvanceAutomatic));
                        cmd.Parameters.AddWithValue("@id", invoice.Id);
                        cmd.ExecuteNonQuery();
                        success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log(ex);
            }

            return success;
        }

        /// <summary>
        /// Marks invoices as paid
        /// </summary>
        /// <param name="invoiceIds"></param>
        /// <returns></returns>
        public bool MarkInvoicesAsPaid(List<int> invoiceIds)
        {
            if (invoiceIds.Count > 0)
            {
                string sqlCmd = "UPDATE InvoiceHeaders SET InvoiceState = 4 " +
                                "WHERE Id = @variable0";
                for (int i = 1; i < invoiceIds.Count; i++)
                {
                    sqlCmd += " OR Id = @variable" + i;
                }
                List<SqlParameter> ids = new List<SqlParameter>();
                try
                {
                    using (SqlConnection conn = new SqlConnection(_connectionString))
                    {
                        conn.Open();
                        using (SqlCommand cmd = new SqlCommand(sqlCmd, conn))
                        {
                            for (int i = 0; i < invoiceIds.Count; i++)
                            {
                                ids.Add(new SqlParameter("@variable" + i, System.Data.SqlDbType.Int) { Value = invoiceIds[i] });
                            }
                            cmd.Parameters.AddRange(ids.ToArray());
                            cmd.ExecuteNonQuery();
                        }
                    }
                    return true;
                }
                catch (Exception ex)
                {
                    this.Log(ex);
                    return false;
                }
            }
            else {
                return false;
            }
        }

        /// <summary>
        /// Updates an invoice detail line
        /// </summary>
        /// <param name="detailLine">The invoice detail line to update</param>
        /// <returns>True if the operation succeeded</returns>
        public bool UpdateInvoiceDetail(InvoiceDetail detailLine)
        {
            bool success = false;
            string sqlCmd = "UPDATE InvoiceDetails SET InvoiceYear = @invoiceYear, InvoiceNum = @invoiceNum, Bid = @bid, BillableDescription = @billableDescription, " +
                            "UnitPrice = @unitPrice, Items = @items, TerminalId = @terminalId, Serial = @serial, ServicePack = @servicePack, MacAddress = @macAddress, " +
                            "Status = @status, ActivityDate = @activityDate, InvoiceId = @invoiceId, PaidInAdvance = @paidInAdvance " +
                            "WHERE Id = @id";

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(sqlCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@invoiceYear", detailLine.InvoiceYear);
                        cmd.Parameters.AddWithValue("@invoiceNum", detailLine.InvoiceNum);
                        cmd.Parameters.AddWithValue("@bid", GetDataValue(detailLine.Bid));
                        cmd.Parameters.AddWithValue("@billableDescription", detailLine.BillableDescription);
                        cmd.Parameters.AddWithValue("@unitPrice", detailLine.UnitPrice);
                        cmd.Parameters.AddWithValue("@items", detailLine.Items);
                        cmd.Parameters.AddWithValue("@terminalId", detailLine.TerminalId);
                        cmd.Parameters.AddWithValue("@serial", GetDataValue(detailLine.Serial));
                        cmd.Parameters.AddWithValue("@servicePack", GetDataValue(detailLine.ServicePack));
                        cmd.Parameters.AddWithValue("@macAddress", GetDataValue(detailLine.MacAddress));
                        cmd.Parameters.AddWithValue("@status", GetDataValue(detailLine.Status));
                        cmd.Parameters.AddWithValue("@activityDate", GetDataValue(detailLine.ActivityDate));
                        cmd.Parameters.AddWithValue("@invoiceId", detailLine.InvoiceId);
                        cmd.Parameters.AddWithValue("@paidInAdvance", GetDataValue(detailLine.PaidInAdvance));
                        cmd.Parameters.AddWithValue("@id", detailLine.ID);
                        cmd.ExecuteNonQuery();
                        success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log(ex);
            }

            return success;
        }

        /// <summary>
        /// Deletes the invoice detail line referenced by lineId
        /// </summary>
        /// <remarks>
        /// This method effecdtively removes the line from the invoice detail
        /// </remarks>
        /// <param name="lineId">The unique identifer of the invoice detail line</param>
        /// <returns>True if the line is deleted, false otherwise</returns>
        public bool DeleteInvoiceDetailLine(int lineId)
        {
            bool success = false;
            string accDeleteCmd = "DELETE FROM InvoiceDetails WHERE Id = @id";

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(accDeleteCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@id", lineId);
                        cmd.ExecuteNonQuery();
                        success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log(ex);
            }
            return success;
        }

        /// <summary>
        /// Returns the invoice detail line referenced by lineId
        /// </summary>
        /// <param name="lineId">The unique identifier of the invoice detail line</param>
        /// <returns>The invoiceDetail object or null if not found</returns>
        public InvoiceDetail GetInvoiceDetailLine(int id)
        {
            const string sql = @"
                                SELECT Id, InvoiceYear, InvoiceNum, Bid, BillableDescription, UnitPrice, Items, TerminalId, Serial, ServicePack,
                                MacAddress, Status, ActivityDate, InvoiceId, PaidInAdvance FROM InvoiceDetails
                                WHERE Id = @id
                                ";
            InvoiceDetail detail = new InvoiceDetail();

            try
            {
                using (var con = new SqlConnection(_connectionString))
                {
                    con.Open();

                    using (var cmd = new SqlCommand(sql, con))
                    {
                        cmd.Parameters.AddWithValue("@id", id);
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            detail.ID = reader.GetInt32(0);
                            detail.InvoiceYear = reader.GetInt32(1);
                            detail.InvoiceNum = reader.GetInt32(2);
                            if (!reader.IsDBNull(3))
                            {
                                detail.Bid = reader.GetInt32(3);
                            }
                            detail.BillableDescription = reader.GetString(4);
                            detail.UnitPrice = reader.GetDecimal(5);
                            detail.Items = reader.GetInt32(6);
                            if (!reader.IsDBNull(7))
                            {
                                detail.TerminalId = reader.GetString(7);
                            }
                            if (!reader.IsDBNull(8))
                            {
                                detail.Serial = reader.GetString(8);
                            }
                            if (!reader.IsDBNull(9))
                            {
                                detail.ServicePack = reader.GetString(9);
                            }
                            if (!reader.IsDBNull(10))
                            {
                                detail.MacAddress = reader.GetString(10);
                            }
                            if (!reader.IsDBNull(11))
                            {
                                detail.Status = reader.GetString(11);
                            }
                            if (!reader.IsDBNull(12))
                            {
                                detail.ActivityDate = reader.GetDateTime(12);
                            }
                            detail.InvoiceId = reader.GetInt32(13);
                            detail.PaidInAdvance = reader.GetBoolean(14);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log(ex);

                throw;
            }
            return detail;
        }

        /// <summary>
        /// Removes a billable from the CMT views
        /// </summary>
        /// <remarks>
        /// Remark that the entity is not deleted from the billable table but
        /// only the Deleted flag is set to "true". So in general this method 
        /// first sets the Delete flag to true and next executes the UpdateBillable
        /// method.
        /// </remarks>
        /// <param name="billableId">The unique identifier of the billable to delete</param>
        /// <returns>True if the operation succeeded</returns>
        public bool DeleteBillable(int billableId)
        {
            bool success = false;
            string billDeleteCmd = "UPDATE Billables SET Deleted = 1 WHERE Bid = @bid";

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(billDeleteCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@bid", billableId);
                        cmd.ExecuteNonQuery();
                        success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log(ex);
            }
            return success;
        }

        /// <summary>
        /// Deletes an account from the accounts table
        /// </summary>
        /// <remarks>
        /// Only the Deleted flag is set. The record is not deleted from the table!
        /// </remarks>
        /// <param name="accId">The account to delete</param>
        /// <returns>True if the delete succeeded</returns>
        public bool DeleteAccount(int accId)
        {
            bool success = false;
            string accDeleteCmd = "UPDATE Accounts SET Deleted = 1 WHERE AccountId = @accountId";

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(accDeleteCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@accountId", accId);
                        cmd.ExecuteNonQuery();
                        success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log(ex);
            }
            return success;
        }

        /// <summary>
        /// Deletes a given billable from the distributors billable list.
        /// </summary>
        /// <remarks>
        /// Only the Deleted flag is set. The record is not actually deleted from the table!
        /// </remarks>
        /// <param name="distId">The unique identifier of the distributor</param>
        /// <param name="billableId">The unique identifier of the billableId (not the Id of the billable in the distBillable table!)</param>
        /// <returns>True if the operation succeeded</returns>
        public bool DeleteDistBillable(int distId, int billableId)
        {
            bool success = false;
            string accDeleteCmd = "UPDATE DistBillables SET Deleted = 1 WHERE DistId = @distId AND Bid = @bid";

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(accDeleteCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@distId", distId);
                        cmd.Parameters.AddWithValue("@bid", billableId);
                        cmd.ExecuteNonQuery();
                        success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log(ex);
            }
            return success;
        }

        /// <summary>
        /// This method returns all billables from the billables table
        /// which are <b>NOT</b> deleted!
        /// </summary>
        /// <returns>A list of Billables with the deleted flag set to false</returns>
        public List<Billable> GetBillables()
        {
            try
            {
                const string sql = @"
                                    SELECT * FROM Billables
                                    WHERE Deleted != 1
                                    ";

                using (var db = new Database(_connectionString, _databaseProvider))
                {
                    List<Billable> dbObj = db.Fetch<Billable>(sql);
                    return dbObj.ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log(ex);

                throw;
            }
        }

        /// <summary>
        /// This billable retrieves the last billable added to the billables table.
        /// </summary>
        /// <remarks>
        /// Only the last none delete billable is returned
        /// </remarks>
        /// <returns>The last  billable added or null if none</returns>
        public Billable GetLastBillable()
        {
            try
            {
                const string sql = @"
                                    SELECT * FROM Billables
                                    WHERE Deleted != 1
                                    ";

                using (var db = new Database(_connectionString, _databaseProvider))
                {
                    List<Billable> dbObj = db.Fetch<Billable>(sql);
                    return dbObj.Last();
                }
            }
            catch (Exception ex)
            {
                this.Log(ex);

                throw;
            }
        }

        /// <summary>
        /// Returns the last distributor billable object added to the database
        /// </summary>
        /// <returns>The last added distBillable or null if none</returns>
        public DistBillable GetLastDistributorBillable()
        {
            try
            {
                const string sql = @"
                                    SELECT * FROM DistBillables
                                    WHERE Deleted != 1
                                    ";

                using (var db = new Database(_connectionString, _databaseProvider))
                {
                    List<DistBillable> dbObj = db.Fetch<DistBillable>(sql);
                    return dbObj.Last();
                }
            }
            catch (Exception ex)
            {
                this.Log(ex);

                throw;
            }
        }


        /// <summary>
        /// Returns the billables for which the validity period is exceeded.
        /// </summary>
        /// <returns>A list of Billables which are invalid</returns>
        public List<Billable> GetInvalidBillables()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Returns the list of distributor billables which are not deleted 
        /// </summary>
        /// <remarks>
        /// Only the non-deleted distributor billables are returned
        /// </remarks>
        /// <param name="distId">The unique identifier of the distributor</param>
        /// <returns>A list of distBillables for the given distributor</returns>
        public List<DistBillable> GetDistBillablesForDistributor(int distId)
        {
            const string sql = "SELECT Id, Bid, DistId, PriceEU, PriceUSD, Remarks, UserId, LastUpdate, Deleted FROM DistBillables WHERE DistId = @distId AND Deleted != 1";

            var resultList = new List<DistBillable>();

            try
            {
                using (var con = new SqlConnection(_connectionString))
                {
                    con.Open();

                    using (var cmd = new SqlCommand(sql, con))
                    {
                        cmd.Parameters.AddWithValue("@distId", distId);
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            var db = new DistBillable();
                            db.Id = reader.GetInt32(0);
                            db.Bid = reader.GetInt32(1);
                            db.DistId = reader.GetInt32(2);
                            db.PriceEU = reader.GetDecimal(3);
                            db.PriceUSD = reader.GetDecimal(4);
                            if (!reader.IsDBNull(5))
                            {
                                db.Remarks = reader.GetString(5);
                            }
                            db.UserId = reader.GetGuid(6);
                            db.LastUpdate = reader.GetDateTime(7);
                            db.Deleted = reader.GetBoolean(8);

                            resultList.Add(db);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log(ex);

                throw;
            }

            return resultList;
        }


        /// <summary>
        /// This method returns a list of accounts with, for each of the accounts,
        /// the billables selected by means of (part) of the description.
        /// </summary>
        /// <remarks>
        /// The result is not a list of billables but a list of accounts with associated billables.
        /// The list of billables can be retrieved by measn of the account.billables property
        /// </remarks>
        /// <param name="desc">The description or part of the description of the billable</param>
        /// <returns>A list of selected accounts, this list can be empty</returns>
        public List<SelectedAccount> GetBillablesByDescription(String desc)
        {
            List<Account> Accounts = this.GetAccounts();
            List<SelectedAccount> SelectedAccounts = new List<SelectedAccount>();

            const string sql = @"
                                    SELECT Bid, Description, AccountId, PriceEU, PriceUSD, UserId, LastUpdate, Deleted, ValidFromDate, ValidToDate FROM Billables
                                    WHERE Description LIKE '%' + @desc + '%' AND AccountId = @accountId
                                    ";
            var billableList = new List<Billable>();

            //First select all billables which contain the given string
            try
            {
                foreach (Account acc in Accounts)
                {
                    SelectedAccount selectedAccount = new SelectedAccount();
                    selectedAccount.AccountId = acc.AccountId;
                    selectedAccount.AccountDescription = acc.Description;
                    selectedAccount.SelectedBillables = new List<Billable>();
                    using (var con = new SqlConnection(_connectionString))
                    {
                        con.Open();

                        using (var cmd = new SqlCommand(sql, con))
                        {
                            cmd.Parameters.AddWithValue("@desc", desc);
                            cmd.Parameters.AddWithValue("@accountId", acc.AccountId);
                            SqlDataReader reader = cmd.ExecuteReader();

                            while (reader.Read())
                            {
                                Billable bill = new Billable();
                                bill.Bid = reader.GetInt32(0);
                                bill.Description = reader.GetString(1);
                                bill.AccountId = reader.GetInt32(2);
                                bill.PriceEU = reader.GetDecimal(3);
                                bill.PriceUSD = reader.GetDecimal(4);
                                bill.UserId = reader.GetGuid(5);
                                bill.LastUpdate = reader.GetDateTime(6);
                                bill.Deleted = reader.GetBoolean(7);
                                if (!reader.IsDBNull(8))
                                {
                                    bill.ValidFromDate = reader.GetDateTime(8);
                                }
                                if (!reader.IsDBNull(9))
                                {
                                    bill.ValidToDate = reader.GetDateTime(9);
                                }

                                selectedAccount.SelectedBillables.Add(bill);
                            }
                        }
                    }
                    //using (var db = new Database(_connectionString, _databaseProvider))
                    //{
                    //    List<Billable> dbObj = db.Fetch<Billable>(sql, new { desc, acc.AccountId });
                    //    selectedAccount.SelectedBillables = dbObj.ToList();
                    //}
                    if (selectedAccount.SelectedBillables.Count() > 0)
                    {
                        SelectedAccounts.Add(selectedAccount);
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log(ex);
            }

            return SelectedAccounts;
        }


        /// <summary>
        /// Returns a list of billables for an account
        /// </summary>
        /// <param name="accId">The unique identifier of the account</param>
        /// <returns>List of billables for the specified account</returns>
        public List<Billable> GetBillablesForAccount(int accId)
        {
            const string sql = @"
                                    SELECT Bid, Description, AccountId, PriceEU, PriceUSD, UserId, LastUpdate, Deleted, ValidFromDate, ValidToDate FROM Billables
                                    WHERE AccountId = @accId
                                    ";
            var billableList = new List<Billable>();
            try
            {
                using (var con = new SqlConnection(_connectionString))
                {
                    con.Open();

                    using (var cmd = new SqlCommand(sql, con))
                    {
                        cmd.Parameters.AddWithValue("@accId", accId);
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            Billable bill = new Billable();
                            bill.Bid = reader.GetInt32(0);
                            bill.Description = reader.GetString(1);
                            bill.AccountId = reader.GetInt32(2);
                            bill.PriceEU = reader.GetDecimal(3);
                            bill.PriceUSD = reader.GetDecimal(4);
                            bill.UserId = reader.GetGuid(5);
                            bill.LastUpdate = reader.GetDateTime(6);
                            bill.Deleted = reader.GetBoolean(7);
                            if (!reader.IsDBNull(8))
                            {
                                bill.ValidFromDate = reader.GetDateTime(8);
                            }
                            if (!reader.IsDBNull(9))
                            {
                                bill.ValidToDate = reader.GetDateTime(9);
                            }

                            billableList.Add(bill);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log(ex);

                throw;
            }
            return billableList;
        }

        /// <summary>
        /// Returns a list of accounts
        /// </summary>
        /// <remarks>
        /// Only the none delete accounts are returned
        /// </remarks>
        /// <returns>List of account. This list can be empty</returns>
        public List<Account> GetAccounts()
        {
            try
            {
                const string sql = @"
                                    SELECT * FROM Accounts
                                    WHERE Deleted != 1
                                    ";

                using (var db = new Database(_connectionString, _databaseProvider))
                {
                    List<Account> dbObj = db.Fetch<Account>(sql);
                    return dbObj.ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log(ex);

                throw;
            }
        }

        /// <summary>
        /// Returns an account identified by accId
        /// </summary>
        /// <param name="accId">Account object corresponding accId</param>
        /// <returns>A valid account</returns>
        public Account GetAccount(int accId)
        {
            const string sql = @"
                                SELECT AccountId, Description, Remarks, UserId, LastUpdate, Deleted FROM Accounts
                                WHERE AccountId = @accId
                                ";
            Account account = new Account();
            try
            {
                using (var con = new SqlConnection(_connectionString))
                {
                    con.Open();

                    using (var cmd = new SqlCommand(sql, con))
                    {
                        cmd.Parameters.AddWithValue("@accId", accId);
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            account.AccountId = reader.GetInt32(0);
                            account.Description = reader.GetString(1);
                            if (!reader.IsDBNull(2))
                            {
                                account.Remarks = reader.GetString(2);
                            }
                            account.UserId = reader.GetGuid(3);
                            account.LastUpdate = reader.GetDateTime(4);
                            account.Deleted = reader.GetBoolean(5);
                        }
                    }
                }
                //using (var db = new Database(_connectionString, _databaseProvider))
                //{
                //    List<Account> dbObj = db.Fetch<Account>(sql, new { accId });
                //    return dbObj.FirstOrDefault();
                //}
            }
            catch (Exception ex)
            {
                this.Log(ex);

                throw;
            }
            return account;
        }

        /// <summary>
        /// Returns the last account created
        /// </summary>
        /// <remarks>
        /// Only non deleted accounts are created
        /// </remarks>
        /// <returns>The last account created or null if not existing</returns>
        public Account GetLastAccount()
        {
            try
            {
                const string sql = @"
                                    SELECT * FROM Accounts
                                    ";

                using (var db = new Database(_connectionString, _databaseProvider))
                {
                    List<Account> dbObj = db.Fetch<Account>(sql);
                    return dbObj.Last();
                }
            }
            catch (Exception ex)
            {
                this.Log(ex);

                throw;
            }
        }


        /// <summary>
        /// Returns a billable identified by its bid
        /// </summary>
        /// <param name="bid">The Billable Id</param>
        /// <returns>A billable entity or an empty billable if not found</returns>
        public Billable GetBillable(int bid)
        {
            Billable bill = new Billable();
            const string sql = @"
                                SELECT Bid, Description, AccountId, PriceEU, PriceUSD, UserId, LastUpdate, Deleted, ValidFromDate, ValidToDate FROM Billables
                                WHERE Bid = @bid
                                ";
            try
            {
                using (var con = new SqlConnection(_connectionString))
                {
                    con.Open();

                    using (var cmd = new SqlCommand(sql, con))
                    {
                        cmd.Parameters.AddWithValue("@bid", bid);
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            bill.Bid = reader.GetInt32(0);
                            bill.Description = reader.GetString(1);
                            bill.AccountId = reader.GetInt32(2);
                            bill.PriceEU = reader.GetDecimal(3);
                            bill.PriceUSD = reader.GetDecimal(4);
                            bill.UserId = reader.GetGuid(5);
                            bill.LastUpdate = reader.GetDateTime(6);
                            bill.Deleted = reader.GetBoolean(7);
                            if (!reader.IsDBNull(8))
                            {
                                bill.ValidFromDate = reader.GetDateTime(8);
                            }
                            if (!reader.IsDBNull(9))
                            {
                                bill.ValidToDate = reader.GetDateTime(9);
                            }
                        }
                    }
                }
                //using (var db = new Database(_connectionString, _databaseProvider))
                //{
                //    List<Billable> dbObj = db.Fetch<Billable>(sql, new { bid });
                //    return dbObj.FirstOrDefault();
                //}
            }
            catch (Exception ex)
            {
                this.Log(ex);

                throw;
            }
            return bill;
        }

        /// <summary>
        /// Returns a billable identified by its bid but with the pricing info
        /// set to the specific values set for the given distributor.
        /// </summary>
        /// <param name="bid">The Billable Id</param>
        /// <param name="distId">The unique identifier of the distributor</param>
        /// <returns>A billable entity or an empty billable if not found</returns>
        public Billable GetBillableForDistributor(int bid, int distId)
        {
            DistBillable db = new DistBillable();
            Billable billForDistributor = new Billable();
            const string sql1 = @"
                                SELECT Id, Bid, DistId, PriceEU, PriceUSD, Remarks, UserId, LastUpdate, Deleted FROM DistBillables 
                                WHERE DistId = @distId AND Bid = @bid AND Deleted != 1
                                ";
            const string sql2 = @"
                                SELECT Bid, Description, AccountId, PriceEU, PriceUSD, UserId, LastUpdate, Deleted, ValidFromDate, ValidToDate FROM Billables
                                WHERE Bid = @bid AND Deleted != 1
                                ";
            try
            {
                using (var con = new SqlConnection(_connectionString))
                {
                    con.Open();

                    using (var cmd = new SqlCommand(sql1, con))
                    {
                        cmd.Parameters.AddWithValue("@distId", distId);
                        cmd.Parameters.AddWithValue("@bid", bid);
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            billForDistributor.Bid = reader.GetInt32(1);
                            billForDistributor.PriceEU = reader.GetDecimal(3);
                            billForDistributor.PriceUSD = reader.GetDecimal(4);
                            billForDistributor.UserId = reader.GetGuid(6);
                            billForDistributor.Deleted = reader.GetBoolean(8);
                        }

                        con.Close();
                
                    }
                    //CMT-99
                    if (billForDistributor.Bid==0)
                    {
                        con.Open();
                        using (var cmd = new SqlCommand(sql2, con))
                        {
                            cmd.Parameters.AddWithValue("@bid", bid);
                            SqlDataReader reader = cmd.ExecuteReader();

                            while (reader.Read())
                            {
                                billForDistributor.Bid = reader.GetInt32(0);
                                billForDistributor.Description = reader.GetString(1);
                                billForDistributor.AccountId = reader.GetInt32(2);
                                billForDistributor.PriceEU = reader.GetDecimal(3);
                                billForDistributor.PriceUSD = reader.GetDecimal(4);
                                billForDistributor.UserId = reader.GetGuid(5);
                                billForDistributor.LastUpdate = reader.GetDateTime(6);
                                billForDistributor.Deleted = reader.GetBoolean(7);
                                if (!reader.IsDBNull(8))
                                {
                                    billForDistributor.ValidFromDate = reader.GetDateTime(8);
                                }
                                if (!reader.IsDBNull(9))
                                {
                                    billForDistributor.ValidToDate = reader.GetDateTime(9);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log(ex);
            }
            return billForDistributor;
        }

        /// <summary>
        /// Returns all billables for one distributor with the correct pricing info for that distributor.
        /// The prices from his distributor billables have been taken over into the prices of the ordinary billables.
        /// Only the not-deleted billables are returned.
        /// </summary>
        /// <param name="distId">The unique identifier of the distributor to search the billables for</param>
        /// <returns>The list of billables for that distributor</returns>
        public List<Billable> GetBillablesForDistributor(int distId)
        {
            List<Billable> billablesForDistributor = new List<Billable>();
            try
            {
                const string sql1 = @"
                                    SELECT * FROM Billables 
                                    WHERE Deleted != 1
                                    ";
                //                const string sql2 = @"
                //                                    SELECT * FROM Billables
                //                                    WHERE Bid = @bid AND Deleted != 1
                //                                    ";

                using (var db = new Database(_connectionString, _databaseProvider))
                {
                    billablesForDistributor = db.Fetch<Billable>(sql1);
                    List<DistBillable> distBillablesForDistributor = GetDistBillablesForDistributor(distId);
                    foreach (Billable billable in billablesForDistributor)
                    {
                        foreach (DistBillable distbillable in distBillablesForDistributor)
                        {
                            if (distbillable.Bid == billable.Bid)
                            {
                                billable.PriceEU = billable.PriceEU;
                                billable.PriceUSD = billable.PriceUSD;
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                this.Log(ex);
            }
            return billablesForDistributor;
        }

        /// <summary>
        /// Returns a distributor billable identified by its billable id
        /// </summary>
        /// <param name="distBillableId">Identifier of the billable</param>
        /// <returns>A distBillable instance or null if not found</returns>
        public DistBillable GetDistBillable(int distBillableId)
        {
            DistBillable db = new DistBillable();
            const string sql = @"
                                SELECT Id, Bid, DistId, PriceEU, PriceUSD, Remarks, UserId, LastUpdate, Deleted FROM DistBillables
                                WHERE Id = @distBillableId
                                ";

            try
            {
                using (var con = new SqlConnection(_connectionString))
                {
                    con.Open();

                    using (var cmd = new SqlCommand(sql, con))
                    {
                        cmd.Parameters.AddWithValue("@distBillableId", distBillableId);
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            db.Id = reader.GetInt32(0);
                            db.Bid = reader.GetInt32(1);
                            db.DistId = reader.GetInt32(2);
                            db.PriceEU = reader.GetDecimal(3);
                            db.PriceUSD = reader.GetDecimal(4);
                            if (!reader.IsDBNull(5))
                            {
                                db.Remarks = reader.GetString(5);
                            }
                            db.UserId = reader.GetGuid(6);
                            db.LastUpdate = reader.GetDateTime(7);
                            db.Deleted = reader.GetBoolean(8);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log(ex);

                throw;
            }
            return db;
        }

        /// <summary>
        /// Deletes a distributor billable by its billable id
        /// </summary>
        /// <param name="distBillableId">The unique identifier of the distributor billable</param>
        /// <returns>µTrue if the operation succeeded false otherwise</returns>
        public bool DeleteDistBillableById(int distBillableId)
        {
            bool success = false;
            string accDeleteCmd = "UPDATE DistBillables SET Deleted = 1 WHERE Id = @id";

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(accDeleteCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@id", distBillableId);
                        cmd.ExecuteNonQuery();
                        success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log(ex);
            }
            return success;
        }

        /// <summary>
        /// Returns true if the distributor has a distBillable for the given bid
        /// </summary>
        /// <param name="distId">The distributor identifier</param>
        /// <param name="bid">The Billable Identifier</param>
        /// <returns>True if the a distributor billable exists</returns>
        public Boolean DistBillableExists(int distId, int bid)
        {
            DistBillable db = new DistBillable();
            Boolean exists = false;
            const string sql = @"
                                    SELECT Id, Bid, DistId, PriceEU, PriceUSD, Remarks, UserId, LastUpdate, Deleted FROM DistBillables
                                    WHERE DistId = @distId AND Bid = @bid AND Deleted != 1
                                    ";
            try
            {
                using (var con = new SqlConnection(_connectionString))
                {
                    con.Open();

                    using (var cmd = new SqlCommand(sql, con))
                    {
                        cmd.Parameters.AddWithValue("@distId", distId);
                        cmd.Parameters.AddWithValue("@bid", bid);
                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                            exists = true;
                           
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log(ex);

                throw;
            }
            return exists;
        }

        /// <summary>
        /// Allows to retrieve the last invoice number used from the configurations table
        /// </summary>
        /// <remarks>
        /// When running the billing engine, the first invoice number used is this last number + 1
        /// </remarks>
        /// <returns>The last invoice number used by the billing engine</returns>
        public int GetLastInvoiceNumber()
        {
            int lastInvoiceNumber = 0;

            const string sqlGetInvoiceNum = @"SELECT * FROM Configuration WHERE Name = 'InvoiceNumber'";
            const string sqlInsertInvoiceNum = @"INSERT INTO Configuration (Name, Value) VALUES (@name, @value)";

            using (var db = new Database(_connectionString, _databaseProvider))
            {
                List<BOControllerLibrary.Model.Invoicing.Configuration> dbObj = db.Fetch<BOControllerLibrary.Model.Invoicing.Configuration>(sqlGetInvoiceNum);
                int el = dbObj.Count();
                if (el == 0)
                {
                    BOControllerLibrary.Model.Invoicing.Configuration conf = new BOControllerLibrary.Model.Invoicing.Configuration();
                    conf.Name = "InvoiceNumber";
                    conf.Value = lastInvoiceNumber.ToString();
                    using (SqlConnection conn = new SqlConnection(_connectionString))
                    {
                        conn.Open();
                        using (SqlCommand cmd = new SqlCommand(sqlInsertInvoiceNum, conn))
                        {
                            cmd.Parameters.AddWithValue("@name", conf.Name);
                            cmd.Parameters.AddWithValue("@Value", conf.Value);
                            cmd.ExecuteNonQuery();
                        }
                    }
                }
                else
                {
                    List<BOControllerLibrary.Model.Invoicing.Configuration> dbObj2 = db.Fetch<BOControllerLibrary.Model.Invoicing.Configuration>(sqlGetInvoiceNum);
                    BOControllerLibrary.Model.Invoicing.Configuration conf = dbObj2.First();
                    lastInvoiceNumber = Int32.Parse(conf.Value);
                }
            }
            return lastInvoiceNumber;
        }

        /// <summary>
        /// Sets the last invoice number to a new value
        /// </summary>
        /// <remarks>
        /// <b>When running the billing engine, the first invoice number used is this last number + 1</b>
        /// </remarks>
        /// <returns>The last invoice number used by the billing engine</returns>
        public Boolean SetLastInvoiceNumber(int newNum)
        {
            Boolean success = false;
            BOControllerLibrary.Model.Invoicing.Configuration conf = new BOControllerLibrary.Model.Invoicing.Configuration();
            bool exists;

            try
            {
                const string sqlGetInvoiceNum = @"SELECT * FROM Configuration WHERE Name = 'InvoiceNumber'";
                const string sqlInsertNameAndValue = @"INSERT INTO Configuration (Name, Value) VALUES (@name, @value)";
                const string sqlInsertValue = @"UPDATE Configuration SET Value = @value WHERE Name = 'InvoiceNumber'";

                using (var db = new Database(_connectionString, _databaseProvider))
                {
                    List<BOControllerLibrary.Model.Invoicing.Configuration> dbObj = db.Fetch<BOControllerLibrary.Model.Invoicing.Configuration>(sqlGetInvoiceNum);
                    int el = dbObj.Count();
                    if (el == 0)
                    {
                        conf.Name = "InvoiceNumber";
                        conf.Value = newNum.ToString();
                        exists = false;
                    }
                    else
                    {
                        List<BOControllerLibrary.Model.Invoicing.Configuration> dbObj2 = db.Fetch<BOControllerLibrary.Model.Invoicing.Configuration>(sqlGetInvoiceNum);
                        conf = dbObj2.First();
                        conf.Value = newNum.ToString();
                        exists = true;
                    }
                    using (SqlConnection conn = new SqlConnection(_connectionString))
                    {
                        conn.Open();
                        if (!exists)
                        {
                            using (SqlCommand cmd = new SqlCommand(sqlInsertNameAndValue, conn))
                            {
                                cmd.Parameters.AddWithValue("@name", conf.Name);
                                cmd.Parameters.AddWithValue("@Value", conf.Value);
                                cmd.ExecuteNonQuery();
                                success = true;
                            }
                        }
                        else
                        {
                            using (SqlCommand cmd = new SqlCommand(sqlInsertValue, conn))
                            {
                                cmd.Parameters.AddWithValue("@Value", conf.Value);
                                cmd.ExecuteNonQuery();
                                success = true;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log(ex);
            }
            return success;
        }

        /// <summary>
        /// Allows to retrieve a certain message used from the configurations table
        /// </summary>
        /// <returns>The message depending on the given parameter</returns>
        public string GetInvoiceMessage(string messageParam)
        {

            const string sql = @"
                                SELECT Value FROM Configuration
                                WHERE Name = @messageParam
                                ";
            string configValue = "";
            try
            {
                using (var con = new SqlConnection(_connectionString))
                {
                    con.Open();

                    using (var cmd = new SqlCommand(sql, con))
                    {
                        cmd.Parameters.AddWithValue("@messageParam", messageParam);
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            configValue = reader.GetString(0);
                        }
                    }
                }
                //using (var db = new Database(_connectionString, _databaseProvider))
                //{
                //    List<string> dbObj = db.Fetch<string>(sql, new { messageParam });
                //    return dbObj.FirstOrDefault();
                //}
            }
            catch (Exception ex)
            {
                this.Log(ex);

                throw;
            }
            return configValue;
        }

        /// <summary>
        /// Sets the message for the given parameter
        /// </summary>
        /// <returns>True if change was succesfull</returns>
        public Boolean SetInvoiceMessage(string messageParam, string messageValue)
        {
            bool success = false;
            string sqlCmd = "UPDATE Configuration SET Value = @messageValue " +
                            "WHERE Name = @messageParam";

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(sqlCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@messageValue", messageValue);
                        cmd.Parameters.AddWithValue("@messageParam", messageParam);
                        cmd.ExecuteNonQuery();
                        success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log(ex);
            }

            return success;
        }

        /// <summary>
        /// Gets the current exchange rate
        /// </summary>
        /// <returns>The most recent exchange rate</returns>
        public decimal GetCurrentExchangeRate()
        {
            try
            {
                const string sql = @"
                                    SELECT ExchangeRate FROM ExchangeRateHistory
                                    ORDER BY ChangedDate DESC
                                    ";

                using (var db = new Database(_connectionString, _databaseProvider))
                {
                    List<decimal> dbObj = db.Fetch<decimal>(sql);
                    return dbObj.First();
                }
            }
            catch (Exception ex)
            {
                this.Log(ex);

                throw;
            }
        }

        /// <summary>
        /// Inserts the new exchange rate in the database
        /// </summary>
        /// <returns>True if change was succesfull</returns>
        public Boolean InsertExchangeRate(decimal exchangeRate, DateTime changedDate, Guid userId)
        {
            bool success = false;
            string sqlCmd = "INSERT INTO ExchangeRateHistory (ExchangeRate, ChangedDate, UserId) " +
                            "VALUES (@exchangeRate, @changedDate, @userId)";

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(sqlCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@exchangeRate", exchangeRate);
                        cmd.Parameters.AddWithValue("@changedDate", changedDate);
                        cmd.Parameters.AddWithValue("@userId", userId);
                        cmd.ExecuteNonQuery();
                        success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log(ex);
            }

            return success;
        }

        /// <summary>
        /// Gets the current exchange rate
        /// </summary>
        /// <returns>The most recent exchange rate</returns>
        public List<ExchangeRateHistory> GetAllExchangeRates()
        {
            try
            {
                const string sql = @"
                                    SELECT * FROM ExchangeRateHistory
                                    ";

                using (var db = new Database(_connectionString, _databaseProvider))
                {
                    List<ExchangeRateHistory> dbObj = db.Fetch<ExchangeRateHistory>(sql);
                    return dbObj.ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log(ex);

                throw;
            }
        }

        #region Helper methods

        /// <summary>
        /// Clones an invoiceHeader into a new object, without taking the list of invoiceDetails in the new object
        /// </summary>
        /// <returns>The cloned invoiceHeader without the list of invoiceDetails</returns>
        //public InvoiceHeader CloneWithoutDetails()
        //{
        //    InvoiceHeader invoice = new InvoiceHeader()
        //    {
        //        ActiveTerminals = this.ActiveTerminals,
        //        AdvanceAutomatic = this.AdvanceAutomatic,
        //        AdvanceManual = this.AdvanceManual,
        //        BankCharges = this.BankCharges,
        //        CreationDate = this.CreationDate,
        //        CurrencyCode = this.CurrencyCode,
        //        DistributorId = this.DistributorId,
        //        DistributorName = this.DistributorName,
        //        ExchangeRate = this.ExchangeRate,
        //        FinancialInterest = this.FinancialInterest,
        //        Id = this.Id,
        //        InvoiceMonth = this.InvoiceMonth,
        //        InvoiceNum = this.InvoiceNum,
        //        InvoiceState = this.InvoiceState,
        //        InvoiceYear = this.InvoiceYear,
        //        NoIncentive = this.NoIncentive,
        //        Remarks = this.Remarks,
        //        VAT = this.VAT,
        //        //InvoiceId = this.InvoiceId,
        //        TotalAmount = this.TotalAmount
        //        //Incentive = this.Incentive,
        //        //GrandTotal = this.GrandTotal,
        //        //ToBePaid = this.ToBePaid
        //    };

        //    return invoice;
        //}

        /// <summary>
        /// This method constucts the dynamic linq query expression from the given arguments.
        /// An AND operator is put between the different arguments, an OR operator between the same arguments.
        /// </summary>
        /// <remarks>
        /// Please note that argumetns which should not be handled must be NULL!
        /// </remarks>
        /// <param name="years">The invoice years</param>
        /// <param name="months">The invoice months</param>
        /// <param name="invoiceNums">The invoice numbers</param>
        /// <param name="invoiceStates">The invoice states</param>
        /// <param name="distIds">The distribtor identifiers</param>
        /// <returns>The expression string, this String can be empty!</returns>
        private String createLinqQuery(
            List<string> years,
            List<string> months,
            List<string> invoiceNums,
            List<string> invoiceStates,
            List<string> distIds)
        {
            StringBuilder queryExpression = new StringBuilder();
            bool firstArg = true;

            QueryBuilder(queryExpression, "InvoiceYear", years, ref firstArg);
            QueryBuilder(queryExpression, "InvoiceMonth", months, ref firstArg);
            QueryBuilder(queryExpression, "InvoiceNum", invoiceNums, ref firstArg);
            QueryBuilder(queryExpression, "InvoiceState", invoiceStates, ref firstArg);
            QueryBuilder(queryExpression, "DistributorId", distIds, ref firstArg);

            return queryExpression.ToString();
        }

        /// <summary>
        /// Builds up the StringBuilder used as query for Dynamic LINQ.
        /// </summary>
        /// <param name="builder">The StringBuilder to build up the query</param>
        /// <param name="key">The key name to use in the query</param>
        /// <param name="values">The values for that key that need to be added to the query</param>
        /// <param name="firstArg">Whether this is the first argument in the query or not. At the end of this method, this parameter is set to false.</param>
        private void QueryBuilder(StringBuilder builder, string key, List<string> values, ref bool firstArg)
        {
            if (values != null && values.Count(y => y != null) > 0)
            {
                if (!firstArg)
                {
                    builder.Append(" AND "); // Add AND if another expression already exists
                }

                builder.Append("(");

                bool firstValue = true;

                foreach (string value in values)
                {
                    if (value != null)
                    {
                        if (!firstValue)
                        {
                            builder.Append(" OR "); // Add OR if another year already exists
                        }

                        builder.AppendFormat("{0} = {1}", key, value);

                        firstValue = false;
                    }
                }

                builder.Append(")");

                firstArg = false;
            }
        }

        public static object GetDataValue(object value)
        {
            if (value == null)
            {
                return DBNull.Value;
            }
            return value;
        }

        /// <summary>
        /// Logs a message to the error log table
        /// </summary>
        /// <param name="err">The error to log</param>
        private void Log(ErrorLog err)
        {
            string sqlCmd = "INSERT INTO ErrorLog (ExceptionDateTime, ExceptionDesc, ExceptionLevel, ExceptionStacktrace, UserDescription, StateInformation) " +
                            "VALUES (@exceptionDateTime, @exceptionDesc, @exceptionLevel, @exceptionStacktrace, @userDescription, @stateInformation)";
            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                conn.Open();

                using (SqlCommand cmd = new SqlCommand(sqlCmd, conn))
                {
                    cmd.Parameters.AddWithValue("@exceptionDateTime", err.ExceptionDateTime);
                    cmd.Parameters.AddWithValue("@exceptionDesc", err.ExceptionDesc);
                    cmd.Parameters.AddWithValue("@exceptionLevel", err.ExceptionLevel);
                    cmd.Parameters.AddWithValue("@exceptionStacktrace", err.ExceptionStacktrace);
                    cmd.Parameters.AddWithValue("@userDescription", err.UserDescription);
                    cmd.Parameters.AddWithValue("@stateInformation", err.StateInformation);
                    cmd.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Logs an Exception to the error log table
        /// </summary>
        /// <param name="errMsg">The error to log</param>
        private void Log(Exception ex)
        {
            ErrorLog errLog = new ErrorLog();
            errLog.ExceptionDateTime = DateTime.UtcNow;
            errLog.ExceptionDesc = ex.Message;
            errLog.ExceptionLevel = 4;
            errLog.ExceptionStacktrace = ex.StackTrace;
            this.Log(errLog);
        }
        #endregion
    }

}