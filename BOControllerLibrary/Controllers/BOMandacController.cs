﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using BOControllerLibrary.Controllers.Interfaces;
using BOControllerLibrary.Controllers;
using BOControllerLibrary.Model.Log;
using BOControllerLibrary.Model.Terminal;
using log4net;
using BOControllerLibrary.ISPIF.Gateway.Interface;
using BOControllerLibrary.ISPIF.Gateway;
using BOControllerLibrary.ISPIF.Model;
using BOControllerLibrary.Model;
using BOControllerLibrary.Model.Reporting;

namespace BOControllerLibrary.Controllers
{
    /// <summary>
    /// Contains methods which are necessary for the Mandac hotspot access points,
    /// which are a type of connected devices, added to terminals
    /// Certain methods are made available for third parties
    /// </summary>
    public class BOMandacController : IBOMandacController
    {
        string _connectionString = "";
        string _databaseProvider = "";
        private readonly ILog _log = LogManager.GetLogger(typeof(IBOAccountingController));
        private readonly IBOLogController _logController;
        private readonly BOAccountingController _boAccountingControl;
        private readonly BOConnectedDevicesController _boConnectedDevicesControl;

        #region Public methods

        public BOMandacController(string connectionString, string databaseProvider)
        {
            _connectionString = connectionString;
            _databaseProvider = databaseProvider;
            _logController = new BOLogController(_connectionString, _databaseProvider);
            _boAccountingControl = new BOAccountingController(_connectionString, _databaseProvider);
            _boConnectedDevicesControl = new BOConnectedDevicesController(_connectionString, _databaseProvider);
        }

        /// <summary>
        /// Returns an estimation of the remaining volume for Mandac hotspot connected to a terminal.
        /// This estimation is an indication to the terminal owner if more
        /// hotspot vouchers can be issued or not.
        /// This method will be interpreted by the Mandac system
        /// </summary>
        /// <param name="mandacId">The ID of the Mandac hotspot</param>
        /// <returns>The volume estimation in GB</returns>
        public double GetEstimatedRemainingVolumeForHotspot(string mandacId)
        {
            return this.EstimateRemainingVolumeForTerminal(mandacId);
        }

        /// <summary>
        /// Returns the current remaining volume of a terminal
        /// </summary>
        /// <param name="mandacId">The ID of the Mandac hotspot</param>
        /// <returns>The volume in GB</returns>
        public double GetRemainingVolumeForTerminal(string mandacId)
        {
            decimal sohoRemainingSUM = -1;
            if (!string.IsNullOrEmpty(mandacId))
            {
                try
                {
                    //Get the terminal information
                    string macAddress = _boConnectedDevicesControl.GetTerminalForDevice(mandacId);
                    Terminal term = _boAccountingControl.GetTerminalDetailsByMAC(macAddress);
                    IHubGateWay hubGateway = HubGatewayFactory.GetHubGateway(term.IspId);
                    TerminalInfo ti = hubGateway.getTerminalInfoByMacAddress(macAddress);

                    //updated calculations now using the correct parameters
                    sohoRemainingSUM = (decimal)ti.MaxVolSum - (Convert.ToDecimal(ti.Consumed) + Convert.ToDecimal(ti.ConsumedReturn));
                    sohoRemainingSUM = sohoRemainingSUM / (decimal)1000000000.0;
                    //do not display values smaller than zero, for cosmetic reasons
                    if (sohoRemainingSUM < 0)
                    {
                        sohoRemainingSUM = (decimal)0;
                    }
                }
                catch (Exception ex)
                {
                    _logController.LogApplicationException(new CmtApplicationException(ex, "BOMandacController: Error while executing GetRemainingVolumeForTerminal", ex.Message.ToString(), (short)ExceptionLevelEnum.Error));
                }
            }
            else
            {
                _logController.LogApplicationException(new CmtApplicationException(new Exception(), "BOMandacController: Error while executing GetRemainingVolumeForTerminal - Empty string provided", "Empty string provided", (short)ExceptionLevelEnum.Error));
                sohoRemainingSUM = -2;
            }

            return (double)sohoRemainingSUM;
        }

        /// <summary>
        /// Returns a list of all Mandac transactions in the CMT database
        /// </summary>
        /// <returns></returns>
        public List<MandacTransaction> GetAllTransactions()
        {
            List<MandacTransaction> transactions = new List<MandacTransaction>();
            MandacTransaction mt;
            string queryCmd = "SELECT TransactionId, TransactionDate, VolumePurchasedGB, FeePaid, CurrencyCode, PaymentMethod, VolumeUsedGB, HotSpotId, LastUpdate, VoucherType, ValidUntil FROM MandacTransactions ";

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(queryCmd, conn))
                    {
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            mt = new MandacTransaction();
                            mt.TransactionId = reader.GetString(0);
                            mt.TransactionDate = reader.GetDateTime(1);
                            mt.VolumePurchasedGB = (double)reader.GetDecimal(2);
                            mt.FeePaid = (double)reader.GetDecimal(3);
                            mt.CurrencyCode = reader.GetString(4);
                            mt.PaymentMethod = reader.GetString(5);
                            if (!reader.IsDBNull(6))
                            {
                                mt.VolumeUsed = (double)reader.GetDecimal(6);
                            }
                            mt.HotSpotId = reader.GetString(7);
                            if (!reader.IsDBNull(8))
                            {
                                mt.LastUpdate = reader.GetDateTime(8);
                            }
                            mt.VoucherType = reader.GetString(9);
                            mt.ExpiryDate = reader.GetDateTime(10);
                            transactions.Add(mt);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOMandacController: Error while executing GetAllTransactions", ex.Message.ToString(), (short)ExceptionLevelEnum.Error));
            }
            return transactions;
        }

        /// <summary>
        /// This method returns the remaining validity period for the volume of a terminal
        /// </summary>
        /// <param name="mandacId">The ID of the Mandac hotspot connected to the terminal</param>
        /// <returns>The number of day the volume is still valid</returns>
        public int GetValidityForTerminalVolume(string mandacId)
        {
            int daysLeft = 0;

            try
            {
                string macAddress = _boConnectedDevicesControl.GetTerminalForDevice(mandacId);
                Terminal term = _boAccountingControl.GetTerminalDetailsByMAC(macAddress);
                daysLeft = ((DateTime)term.ExpiryDate - DateTime.UtcNow).Days;
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOMandacController: Error while executing GetValidityForTerminalVolume", ex.Message.ToString(), (short)ExceptionLevelEnum.Error));
            }

            return daysLeft;
        }

        #endregion

        #region Helper methods

        /// <summary>
        /// A helper method used internally to estimate
        /// the remaining volume for a terminal based on historic 
        /// consumption of hotspot users and the terminal itself
        /// </summary>
        /// <param name="mandacId">The Mandac ID of the terminal</param>
        /// <returns>The volume estimation in GB</returns>
        public double EstimateRemainingVolumeForTerminal(string mandacId)
        {
            //declare the necessary variables
            string macAddress = _boConnectedDevicesControl.GetTerminalForDevice(mandacId);
            Terminal term = _boAccountingControl.GetTerminalDetailsByMAC(macAddress);
            decimal remainingVolume = 0;
            List<VolumeObject> volumes = new List<VolumeObject>();
            VolumeObject volObj;
            decimal averageUsage = 1;
            decimal totalVolumeAllocation = 0;
            const decimal buffer = 0.2M; //A buffer to account for the use of the terminal owner, ie non-hotspot traffic

            //get the available volume for the terminal
            double absRemainingVolume = this.GetRemainingVolumeForTerminal(mandacId);

            //determine the average usage by customers of this particular hotspot
            //start by fetching the actual volumes used for all expired purchases
            string queryCmd = "SELECT VolumePurchasedGB, VolumeUsedGB FROM MandacTransactions " +
                               "WHERE HotSpotId = @HotSpotId AND ValidUntil < @ValidUntil";
            
            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(queryCmd, conn))
                    {
                        // get the devices
                        cmd.Parameters.AddWithValue("@HotspotId", mandacId);
                        cmd.Parameters.AddWithValue("@ValidUntil", DateTime.Now);
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            volObj = new VolumeObject();
                            volObj.volumePurchased = reader.GetDecimal(0);
                            volObj.volumeUsed = reader.GetDecimal(1);
                            volumes.Add(volObj);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOMandacController: Error while executing EstimateRemainingVolumeForTerminal", "Terminal: " + macAddress, (short)ExceptionLevelEnum.Error));
            }

            //calculate the average
            decimal totalVolumeUsed = 0;
            decimal totalVolumePurchased = 0;
            foreach (VolumeObject vo in volumes)
            {
                totalVolumePurchased = totalVolumePurchased + vo.volumePurchased;
                totalVolumeUsed = totalVolumeUsed + vo.volumeUsed;
            }
            //avoid divide by 0
            if (totalVolumePurchased > 0)
            {
                averageUsage = totalVolumeUsed / totalVolumePurchased;
            }

            //determine the volume allocated for still active purchases
            //fetch the volumes for the purchases in question
            queryCmd = "SELECT VolumePurchasedGB, VolumeUsedGB FROM MandacTransactions " +
                               "WHERE HotspotId = @HotspotId AND ValidUntil > @ValidUntil";
            volumes = new List<VolumeObject>();
            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(queryCmd, conn))
                    {
                        // get the devices
                        cmd.Parameters.AddWithValue("@HotspotId", mandacId);
                        cmd.Parameters.AddWithValue("@ValidUntil", DateTime.Now);
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            volObj = new VolumeObject();
                            volObj.volumePurchased = reader.GetDecimal(0);
                            volObj.volumeUsed = reader.GetDecimal(1);
                            volumes.Add(volObj);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOMandacController: Error while executing EstimateRemainingVolumeForTerminal", "Terminal: " + macAddress, (short)ExceptionLevelEnum.Error));
            }
            //calculate the remaining volume allocation for the terminals and add to the total allocation
            decimal volAllocated;
            foreach (VolumeObject vo in volumes)
            {
                volAllocated = vo.volumePurchased * averageUsage;
                if (volAllocated > vo.volumeUsed)
                {
                    volAllocated = volAllocated - vo.volumeUsed;
                }
                else
                {
                    volAllocated = vo.volumePurchased - vo.volumeUsed;
                }
                totalVolumeAllocation = totalVolumeAllocation + volAllocated;
            }

            //calculate estimated remaining volume
            remainingVolume = (decimal)absRemainingVolume - totalVolumeAllocation - buffer;

            return (double)remainingVolume;
        }

        class VolumeObject
        {
            public decimal volumePurchased { get; set; }
            public decimal volumeUsed { get; set; }
        }

        #endregion
    }
}
