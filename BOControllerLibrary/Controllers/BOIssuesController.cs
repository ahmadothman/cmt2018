﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;
using BOControllerLibrary.Model;
using BOControllerLibrary.Controllers.Interfaces;
using BOControllerLibrary.Model.Issues;
using log4net;
using net.nimera.CRCCalculator;
using BOControllerLibrary.Model.Log;

namespace BOControllerLibrary.Controllers
{
    /// <summary>
    /// Contains methods for submitting and viewing issues.
    /// Issues are submitted by distributors from withing the CMT to
    /// an external Jira server, where they are managed by SatADSL.
    /// The CMT stores a minumum of information regarding each issue
    /// in order to present a list of issues when so requested by a distributor.
    /// Distributors can only see issues submitted by themselves.
    /// The issue data stored in the CMT consists of: the issue key, which is the unique id,
    /// a summary, the id of the distributor and the date of submission.
    /// The initial status of the issue is also stored, but the updating of
    /// the status must still be implemented.
    /// </summary>
    public class BOIssuesController : IBOIssuesController
    {
        string _method = "";
        string _url = "http://192.168.100.122:8080/rest/api/2/issue/";
        string _mergedCredentials = string.Format("{0}:{1}", "cmt", "cmt");
        string _jiraProjectName = "CMT";
        string _connectionString = "";
        string _databaseProvider = "";

        private readonly ILog _log = LogManager.GetLogger(typeof(IBOAccountingController));
        private readonly IBOLogController _logController;

        static BOConfigurationController _configurationController;
        
        public BOIssuesController(string connectionString, string databaseProvider)
        {
            _connectionString = connectionString;
            _databaseProvider = databaseProvider;
            _logController = new BOLogController(_connectionString, _databaseProvider);
            
            _configurationController = new BOConfigurationController(_connectionString, _databaseProvider);
        }

        /// <summary>
        /// Returns a list of issues for a given
        /// distributor.
        /// First the list is retrieved from the CMT.
        /// Then each issue is updated from Jira
        /// </summary>
        /// <param name="distributorId">The distributor identifier.</param>
        /// <returns>A list of issues. This list can be empty.</returns>
        public List<Model.Issues.Issue> GetIssuesByDistributor(int distributorId)
        {
            try
            {
                Issue issue;
                List<Issue> oldIssuesList = new List<Issue>();
                List<Issue> updatedIssuesList = new List<Issue>();
                var var1 = new StringBuilder();
                var1.Append("SELECT IssueKey, Summary, IssueDateTime, Status, IssueLastUpdate, MACAddress \n");
                var1.Append("FROM Issues \n");
                var1.Append("WHERE DistributorId = @DistributorId ");

                using (SqlConnection con = new SqlConnection(_connectionString))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(var1.ToString(), con))
                    {
                        cmd.Parameters.AddWithValue("@DistributorId", distributorId);
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            // read data from CMT
                            issue = new Issue();
                            issue.Key = reader.GetString(0);
                            issue.Summary = reader.GetString(1);
                            issue.Submitted = reader.GetDateTime(2);
                            issue.MACAddress = reader.GetString(5);

                            // read data from Jira
                            JiraIssue jiraIssue = this.GetJiraIssueDetails(issue.Key);
                            issue.Status = jiraIssue.fields.status.name;
                            issue.LastUpdate = jiraIssue.fields.updated;

                            // update data in CMT
                            this.UpdateIssue(issue);

                            oldIssuesList.Add(issue);
                        }
                    }
                }

                return oldIssuesList;
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOIssuesController: Error while executing GetIssuesByDistributorId",
                                                                                   distributorId.ToString(), (short)ExceptionLevelEnum.Error));
                throw;
            }
        }

        /// <summary>
        /// Creates a new issue as submitted by a distributor.
        /// The issue is created in Jira and 
        /// basic information is stored in the CMT table Issues
        /// The method still needs proper error handling and logging
        /// </summary>
        /// <param name="issue">The issue to be submitted</param>
        /// <returns>A string containing the unique identifier of the created issue. This is set to 0 if something goes wrong</returns>
        public string SubmitNewIssue(Issue issue)
        {
            // Create the API call
            _method = "POST";
            HttpWebRequest request = WebRequest.Create(_url) as HttpWebRequest;
            request.ContentType = "application/json";
            request.Method = _method;

            // Encode the credentials
            byte[] byteCredentials = UTF8Encoding.UTF8.GetBytes(_mergedCredentials);
            string base64Credentials = Convert.ToBase64String(byteCredentials);
            request.Headers.Add("Authorization", "Basic " + base64Credentials);

            // prepare the submission to Jira
            JiraSubmission jiraSubmission = new JiraSubmission();
            jiraSubmission.fields.project.key = _jiraProjectName;
            //jiraSubmission.fields.customfield_10004 = issue.DistributorId;

            // get the Distributor Name
            BOAccountingController boAccountingControl = new BOAccountingController(_connectionString, _databaseProvider);
            Model.Distributor.Distributor distributor = boAccountingControl.GetDistributor(issue.DistributorId);

            jiraSubmission.fields.summary = distributor.FullName + ": " + issue.Summary;
            //jiraSubmission.fields.customfield_10002 = issue.MACAddress;
            //jiraSubmission.fields.customfield_10003 = distributor.FullName;
            if (issue.MACAddress.Equals("00:00:00:00:00:00"))
                jiraSubmission.fields.description = issue.Description;
            else
                jiraSubmission.fields.description = issue.Description + " MAC:" + issue.MACAddress;
            jiraSubmission.fields.issuetype.name = issue.IssueType;
            //jiraSubmission.fields.customfield_10001 = distributor.FullName;

            // prepare the JSON call
            JavaScriptSerializer js = new JavaScriptSerializer();
            string sData = js.Serialize(jiraSubmission).ToString();
            
            // declare the response variable
            JiraResponse jiraResponse = new JiraResponse();
            
            // send the data to Jira
            try
            {
                byte[] byteData = UTF8Encoding.UTF8.GetBytes(sData);
                // Set the content length in the request headers
                request.ContentLength = byteData.Length;
                // Write data
               
                using (Stream postStream = request.GetRequestStream())
                {
                    postStream.Write(byteData, 0, byteData.Length);
                }
                
                // Get the response from Jira
                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    // Make the JSON response readable by storing it in a variable of type JiraResponse
                    StreamReader reader = new StreamReader(response.GetResponseStream());
                    string str = reader.ReadToEnd();
                    jiraResponse = (JiraResponse)js.Deserialize(str, typeof(JiraResponse));
                }
            }
            catch (Exception ex)
            {
                
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOIssuesController: Error while creating new issue in Jira", issue.DistributorId.ToString(), (short)ExceptionLevelEnum.Error));
                jiraResponse.key = "0";
            }
            
            // Write the data in the Issues table
            if (jiraResponse.key != "0")
            {
                issue.Key = jiraResponse.key;
                if (!this.StoreIssueInCMT(issue))
                {
                    jiraResponse.key = "0";
                }
            }

            // return the Jira issue key
            return jiraResponse.key;
        }

        /// <summary>
        /// Returns a specific Jira issue, for which the data is stored in Jira.
        /// The method still needs proper error handling and logging.
        /// </summary>
        /// <param name="jiraIssueKey">The unique issue identifier.</param>
        /// <returns>A JiraIssue.</returns>
        public Model.Issues.JiraIssue GetJiraIssueDetails(string jiraIssueKey)
        {
            // Create the API call
            string url = _url + jiraIssueKey;
            _method = "GET";
            HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
            request.ContentType = "application/json";
            request.Method = _method;

            // Encode the credentials
            byte[] byteCredentials = UTF8Encoding.UTF8.GetBytes(_mergedCredentials);
            string base64Credentials = Convert.ToBase64String(byteCredentials);
            request.Headers.Add("Authorization", "Basic " + base64Credentials);

            // Create variables for storing the result
            string result = string.Empty;
            JiraIssue jiraIssue = new JiraIssue();
            
            try
            {
                // Get the response from Jira
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                
                // Make the JSON response readable by storing it in a variable of type JiraIssue
                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    result = reader.ReadToEnd();
                    jiraIssue = (JiraIssue)js.Deserialize(result, typeof(JiraIssue));
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOIssuesController: GetJiraIssueDetails failed", jiraIssueKey, (short)ExceptionLevelEnum.Info));
            }

            // return the Jira issue with the data
            return jiraIssue;
        }

        /// <summary>
        /// A method for updating an issue in the CMT database
        /// </summary>
        /// <param name="issue">The issue to be updated</param>
        /// <returns>True if the operation succeeded</returns>
        public bool UpdateIssue(Issue issue)
        {
            bool result = false;
            string updateCmd = "UPDATE Issues " +
                               "SET IssueLastUpdate = @IssueLastUpdate, Status = @Status " +
                               "WHERE IssueKey = @IssueKey ";

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(updateCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@IssueKey", issue.Key);
                        cmd.Parameters.AddWithValue("@Status", issue.Status);
                        cmd.Parameters.AddWithValue("@IssueLastUpdate", issue.LastUpdate);
                        cmd.ExecuteNonQuery();
                    }
                }
                result = true;
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOIssuesController: Error while updating jiraissue in the CMT database", issue.Key, (short)ExceptionLevelEnum.Error));
            }

            return result;
        }

        /// <summary>
        /// A helper method for storing an issue in the CMT
        /// </summary>
        /// <param name="issue">The issue to be stored</param>
        /// <returns>True if the operation succeeded</returns>
        public bool StoreIssueInCMT(Issue issue)
        {
            bool result = false;
            string insertCmd = "INSERT INTO Issues (IssueKey, DistributorId, Summary, IssueDateTime, Status, IssueLastUpdate, MACAddress) " +
                                   "VALUES (@IssueKey, @DistributorId, @Summary, @IssueDateTime, @Status, @IssueLastUpdate, @MACAddress)";

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(insertCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@IssueKey", issue.Key);
                        cmd.Parameters.AddWithValue("@DistributorId", issue.DistributorId);
                        cmd.Parameters.AddWithValue("@Summary", issue.Summary);
                        cmd.Parameters.AddWithValue("@IssueDateTime", DateTime.Now);
                        cmd.Parameters.AddWithValue("@Status", "Open");
                        cmd.Parameters.AddWithValue("@IssueLastUpdate", DateTime.Now);
                        cmd.Parameters.AddWithValue("@MACAddress", issue.MACAddress);
                        cmd.ExecuteNonQuery();
                    }
                }
                result = true;
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOIssuesController: Error while storing jiraissue in the CMT database", issue.Key, (short)ExceptionLevelEnum.Error));
            }

            return result;
        }

        /// <summary>
        /// A method for submitting a comment from the CMT to Jira
        /// A comment can be submitted on an existing issue
        /// In the current implementation, a distributor can only submit a comment
        /// on an open issue which he has reported. Theoretically, other options are possible,
        /// but at the moment there are no use cases requiring this.
        /// </summary>
        /// <param name="commentBody">The contents of the comment</param>
        /// <param name="issueKey">The key of the concerned issue</param>
        /// <returns>True if the operation succeeded</returns>
        public bool SubmitComment(string commentBody, string jiraIssueKey)
        {
            // Create the API call
            string url = _url + jiraIssueKey + "/comment";
            _method = "POST";
            HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
            request.ContentType = "application/json";
            request.Method = _method;

            // Encode the credentials
            byte[] byteCredentials = UTF8Encoding.UTF8.GetBytes(_mergedCredentials);
            string base64Credentials = Convert.ToBase64String(byteCredentials);
            request.Headers.Add("Authorization", "Basic " + base64Credentials);

            // prepare the submission to Jira
            JiraComment jiraComment = new JiraComment();
            jiraComment.body = commentBody;
            
            // prepare the JSON call
            JavaScriptSerializer js = new JavaScriptSerializer();
            string sData = js.Serialize(jiraComment).ToString();

            // declare the response variable
            string responseString = "";

            // send the data to Jira
            try
            {
                byte[] byteData = UTF8Encoding.UTF8.GetBytes(sData);
                // Set the content length in the request headers
                request.ContentLength = byteData.Length;
                // Write data
                using (Stream postStream = request.GetRequestStream())
                {
                    postStream.Write(byteData, 0, byteData.Length);
                }

                // Get the response from Jira
                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    // Make the JSON response readable by storing it in a variable of type JiraResponse
                    StreamReader reader = new StreamReader(response.GetResponseStream());
                    responseString = reader.ReadToEnd();
                }

                return true;
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOIssuesController: Error while submitting a new comment to Jira", jiraIssueKey, (short)ExceptionLevelEnum.Error));
                return false;
            }
        }
    }
}
