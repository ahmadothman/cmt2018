﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using BOControllerLibrary;
using BOControllerLibrary.Model;
using BOControllerLibrary.Model.Distributor;
using BOControllerLibrary.Model.Log;
using BOControllerLibrary.Model.MultiCast;
using BOControllerLibrary.Model.Organization;
using BOControllerLibrary.Controllers.Interfaces;
using BOControllerLibrary.ISPIF.Gateway.Interface;
using BOControllerLibrary.ISPIF.Gateway;
using System.Globalization;

namespace BOControllerLibrary.Controllers
{
    /// <summary>
    /// MultiCast controller implementation
    /// </summary>
    public class BOMultiCastGroupController : IBOMultiCastGroupController
    {
        private string _connectionString = "";
        private string _dataProvider = "";
        private readonly IBOLogController _logController;

        /// <summary>
        /// Default constructor which sets the connection string and database provider
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="databaseProvider"></param>
        public BOMultiCastGroupController(string connectionString, string databaseProvider)
        {
            _connectionString = connectionString;
            _dataProvider = databaseProvider;
            _logController = new BOLogController(connectionString, databaseProvider);
        }

        /// <summary>
        /// Creates the given MultiCastGroup
        /// </summary>
        /// <remarks>
        /// This method first tries to update the Multicast group in the NMS. If this is done
        /// successfully, the Multicast group data is updated in the CMT database
        /// </remarks>
        /// <param name="mc">The Multicast group to update</param>
        /// <returns>True if the Multicast group is successfully created</returns>
        public bool CreateMultiCastGroup(MultiCastGroup mc)
        {
            bool result = false;
            IHubGateWay hubGateway = HubGatewayFactory.GetHubGateway(mc.IspId);
            if (hubGateway.CreateMulticastGroup(mc.MacAddress, mc.IPAddress, mc.Name,
                                    mc.sourceURL, mc.portNr, mc.BandWidth, mc.IspId, mc.UserId))
            {
                //The multicast group was created in the NMS so now we can create the MC in the
                //CMT Database
                if (this.UpdateMultiCastGroupInCmt(mc))
                    result = true;
                else
                {
                    result = this.CreateMultiCastGroupInCmt(mc);
                }

            }
            else
            {
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionLevel = (short)ExceptionLevelEnum.Critical;
                cmtEx.ExceptionDateTime = DateTime.UtcNow;
                cmtEx.ExceptionDesc = "Could not create the MultiCastGroup with IP: " + mc.IPAddress + " in NMS with ISPId: " + mc.IspId;
                cmtEx.ExceptionStacktrace = "CreateMultiCastGroup";
                cmtEx.UserDescription = "CreateMultiCastGroup failed";
                _logController.LogApplicationException(cmtEx);
            }

            return result;
        }

        /// <summary>
        /// Updates the given Multicast group in the CMT database AND the NMS
        /// </summary>
        /// <remarks>
        /// It is not clear for the moment how the update is done in the NMS. Probably this also needs
        /// an asynchronous approach. For the moment being the update is only done in the CMT!
        /// </remarks>
        /// <param name="mc">The Multicast group to update</param>
        /// <returns>True if the operation succeeded</returns>
        public bool UpdateMultiCastGroup(MultiCastGroup mc)
        {
            return this.UpdateMultiCastGroupInCmt(mc);
        }

        /// <summary>
        /// Deletes the Multicast group from the CMT and NMS databases.
        /// </summary>
        /// <remarks>
        /// The record in the CMT is not completely deleted but on flagged as deleted
        /// </remarks>
        /// <param name="mc">The Multicast group to delete</param>
        /// <returns>True if the MC group was successfully deleted</returns>
        public bool DeleteMultiCastGroup(MultiCastGroup mc)
        {
            bool result = false;
            string mcDeleteCmd = "UPDATE MultiCastGroups SET Deleted = 1 WHERE ID = @ID";

            IHubGateWay hubGateway = HubGatewayFactory.GetHubGateway(mc.IspId);
            if (hubGateway.RemoveIPFromMulticast(mc.IPAddress))
            {
                //The delete succeeded in the NMS now delete form CMT
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(mcDeleteCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@ID", mc.ID);
                        cmd.ExecuteNonQuery();
                    }
                }
                this.CreateEmtpyMulticastGroup(mc.NMSId);
            }
            else
            {
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionLevel = (short)ExceptionLevelEnum.Critical;
                cmtEx.ExceptionDateTime = DateTime.UtcNow;
                cmtEx.ExceptionDesc = "Could not delete the MultiCastGroup with IP: " + mc.IPAddress + " in NMS with ISPId: " + mc.IspId;
                cmtEx.ExceptionStacktrace = "DeleteMultiCastGroup";
                cmtEx.UserDescription = "DeleteMultiCastGroup failed";
                _logController.LogApplicationException(cmtEx);
            }
            return result;
        }

        public MultiCastGroup GetMultiCastGroupByID(int id)
        {
            MultiCastGroup mc = null;
            string queryCmd = "SELECT IP, MacAddress, Name, Bytes, SourceURL, PortNr, Bandwidth, State, " +
                                "IspId, FirstActivationDate, UserId, OrgId, DistributorId, Deleted, ID, NMSId, Reserved FROM MultiCastGroups WHERE ID = @ID";
            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(queryCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@ID", id);
                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            mc = new MultiCastGroup();
                            mc.IPAddress = reader.GetString(0);
                            mc.MacAddress = reader.GetString(1);
                            mc.Name = reader.GetString(2);
                            if (!reader.IsDBNull(3))
                            {
                                mc.Bytes = reader.GetInt64(3);
                            }
                            mc.sourceURL = reader.GetString(4);
                            mc.portNr = reader.GetInt32(5);
                            mc.BandWidth = reader.GetInt64(6);
                            mc.State = reader.GetBoolean(7);
                            if (!reader.IsDBNull(8))
                            {
                                mc.IspId = reader.GetInt32(8);
                            }
                            if (!reader.IsDBNull(9))
                            {
                                mc.FirstActivationDate = reader.GetDateTime(9);
                            }
                            if (!reader.IsDBNull(10))
                            {
                                mc.UserId = reader.GetGuid(10);
                            }
                            if (!reader.IsDBNull(11))
                            {
                                mc.OrganizationId = reader.GetInt32(11);
                            }
                            if (!reader.IsDBNull(12))
                            {
                                mc.DistributorId = reader.GetInt32(12);
                            }
                            mc.Deleted = reader.GetBoolean(13);
                            mc.ID = reader.GetInt32(14);
                            mc.NMSId = reader.GetInt32(15);
                            mc.Reserved = reader.GetBoolean(16);
                        }
                        else
                        {
                            CmtApplicationException cmtEx = new CmtApplicationException();
                            cmtEx.ExceptionLevel = (short)ExceptionLevelEnum.Critical;
                            cmtEx.ExceptionDateTime = DateTime.UtcNow;
                            cmtEx.ExceptionDesc = "Could not find the MultiCastGroup with ID: " + id + " in CMT Database";
                            cmtEx.ExceptionStacktrace = "GetMultiCastGroupByID";
                            cmtEx.UserDescription = "Get MultiCastGroupByID failed";
                            _logController.LogApplicationException(cmtEx);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtEx = new CmtApplicationException(ex, "Retrieval of MulticastGroup with ID: " + id + " failed", "Exception", (short)ExceptionLevelEnum.Critical);
            }

            return mc;
        }

        /// <summary>
        /// Retrieves a Multicast group identified by its IP address from the
        /// CMT Database
        /// </summary>
        /// <param name="IPAddress">The IP Address of the Multicast group</param>
        /// <returns>A valid Multicast group object or NULL if not found</returns>
        public Model.MultiCast.MultiCastGroup GetMultiCastGroupByIP(string IPAddress)
        {
            MultiCastGroup mc = null;
            string queryCmd = "SELECT IP, MacAddress, Name, Bytes, SourceURL, PortNr, Bandwidth, State, " +
                                "IspId, FirstActivationDate, UserId, OrgId, DistributorId, Deleted, ID FROM MultiCastGroups WHERE IP = @IPAddress";
            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(queryCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@IPAddress", IPAddress);
                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            mc = new MultiCastGroup();
                            mc.IPAddress = reader.GetString(0);
                            mc.MacAddress = reader.GetString(1);
                            mc.Name = reader.GetString(2);
                            if (!reader.IsDBNull(3))
                            {
                                mc.Bytes = reader.GetInt64(3);
                            }
                            mc.sourceURL = reader.GetString(4);
                            mc.portNr = reader.GetInt32(5);
                            mc.BandWidth = reader.GetInt64(6);
                            mc.State = reader.GetBoolean(7);
                            if (!reader.IsDBNull(8))
                            {
                                mc.IspId = reader.GetInt32(8);
                            }
                            if (!reader.IsDBNull(9))
                            {
                                mc.FirstActivationDate = reader.GetDateTime(9);
                            }
                            if (!reader.IsDBNull(10))
                            {
                                mc.UserId = reader.GetGuid(10);
                            }
                            if (!reader.IsDBNull(11))
                            {
                                mc.OrganizationId = reader.GetInt32(11);
                            }
                            if (!reader.IsDBNull(12))
                            {
                                mc.DistributorId = reader.GetInt32(12);
                            }
                            mc.Deleted = reader.GetBoolean(13);
                            mc.ID = reader.GetInt32(14);
                        }
                        else
                        {
                            CmtApplicationException cmtEx = new CmtApplicationException();
                            cmtEx.ExceptionLevel = (short)ExceptionLevelEnum.Critical;
                            cmtEx.ExceptionDateTime = DateTime.UtcNow;
                            cmtEx.ExceptionDesc = "Could not find the MultiCastGroup with IP: " + IPAddress + " in CMT Database";
                            cmtEx.ExceptionStacktrace = "CreateMultiCastGroup";
                            cmtEx.UserDescription = "CreateMultiCastGroup failed";
                            _logController.LogApplicationException(cmtEx);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtEx = new CmtApplicationException(ex, "Retrieval of MulticastGroup with IP: " + IPAddress + " failed", "Exception", (short)ExceptionLevelEnum.Critical);
            }

            return mc;
        }

        /// <summary>
        /// Returns the bytes transferred for the given MC from the NMS
        /// </summary>
        /// <param name="IPAddress">The IP Address of the MC group</param>
        /// <param name="IspId">The ISP Id which identifies the NMS</param>
        /// <returns>The Bytes retrieved as an unsigned long</returns>
        public ulong GetBytesTransferredForMC(string IPAddress, int IspId)
        {
            IHubGateWay hubGateway = HubGatewayFactory.GetHubGateway(IspId);
            return hubGateway.GetTransferredBytes(IPAddress);
        }

        /// <summary>
        /// Returns true if the Multicast group identified by its IP Address
        /// is true
        /// </summary>
        /// <param name="IPAddress">The MC IP Address</param>
        /// <param name="IspId">The ISP Id which identifies the NMS</param>
        /// <returns>True if the MC is enabled</returns>
        public bool IsEnabled(string IPAddress, int IspId)
        {
            IHubGateWay hubGateway = HubGatewayFactory.GetHubGateway(IspId);
            return hubGateway.IsEnabled(IPAddress);
        }

        /// <summary>
        /// Checks if the Multicast group exists in the CMT Database
        /// </summary>
        /// <param name="IPAddress">The MC IP Address</param>
        /// <returns>True if the MC exists</returns>
        public bool MultiCastGroupExists(string IPAddress)
        {
            return (this.GetMultiCastGroupByIP(IPAddress) != null);
        }

        /// <summary>
        /// Creates the Multicast group in the CMT Database
        /// </summary>
        /// <param name="mc">The MultiCast group to create</param>
        /// <returns>True if the creation succeeded</returns>
        private bool CreateMultiCastGroupInCmt(MultiCastGroup mc)
        {
            bool result = false;
            string createCmd = "INSERT INTO MultiCastGroups (IP, MacAddress, Name, Bytes, SourceURL, PortNr, Bandwidth, State, " +
                                "IspId, FirstActivationDate, UserId, OrgId, DistributorId, Deleted, NMSId, Reserved)" +
                                "VALUES(@IP, @MacAddress, @Name, @Bytes, @SourceURL, @PortNr, @Bandwidth, @State, " +
                                "@IspId, @FirstActivationDate, @UserId, @OrgId, @DistributorId, @Deleted, @NMSId, @Reserved)";

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(createCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@IP", mc.IPAddress);
                        cmd.Parameters.AddWithValue("@MacAddress", mc.MacAddress);
                        cmd.Parameters.AddWithValue("@Name", mc.Name);
                        cmd.Parameters.AddWithValue("@Bytes", mc.Bytes);
                        cmd.Parameters.AddWithValue("@SourceURL", mc.sourceURL);
                        cmd.Parameters.AddWithValue("@PortNr", mc.portNr);
                        cmd.Parameters.AddWithValue("@Bandwidth", mc.BandWidth);
                        cmd.Parameters.AddWithValue("@State", mc.State);
                        cmd.Parameters.AddWithValue("@IspId", mc.IspId);
                        cmd.Parameters.AddWithValue("@FirstActivationDate", mc.FirstActivationDate);
                        cmd.Parameters.AddWithValue("@UserId", mc.UserId);
                        cmd.Parameters.AddWithValue("@OrgId", mc.OrganizationId);
                        cmd.Parameters.AddWithValue("@DistributorId", mc.DistributorId);
                        cmd.Parameters.AddWithValue("@Deleted", mc.Deleted);
                        cmd.Parameters.AddWithValue("@NMSId", mc.NMSId);
                        cmd.Parameters.AddWithValue("@Reserved", mc.Reserved);

                        int rtn = cmd.ExecuteNonQuery();

                        if (rtn != 0)
                        {
                            result = true;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtEx = new CmtApplicationException(ex, "Creation of MulticastGroup failed", "Critical error", (short)ExceptionLevelEnum.Error);
                _logController.LogApplicationException(cmtEx);
            }

            return result;
        }

        /// <summary>
        /// Updates the Multicast group in the CMT Database
        /// </summary>
        /// <param name="mc">The MultiCast group to update</param>
        /// <returns>True if the update succeeded</returns>
        private bool UpdateMultiCastGroupInCmt(MultiCastGroup mc)
        {
            bool result = false;
            string updateCmd = "UPDATE MultiCastGroups SET IP = @IP, MacAddress = @MacAddress, Name = @Name, Bytes = @Bytes, " +
                               "SourceURL = @SourceURL, PortNr = @PortNr, Bandwidth = @Bandwidth, State = @State, IspId = @IspId, " +
                               "FirstActivationDate = @FirstActivationDate, UserId = @UserId, OrgId = @OrgId, DistributorId = @DistributorId, " +
                               "Reserved = @Reserved, NMSId = @NMSId WHERE ID = @ID";

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(updateCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@ID", mc.ID);
                        cmd.Parameters.AddWithValue("@Ip", mc.IPAddress);
                        cmd.Parameters.AddWithValue("@MacAddress", mc.MacAddress);
                        cmd.Parameters.AddWithValue("@Name", mc.Name);
                        cmd.Parameters.AddWithValue("@Bytes", mc.Bytes);
                        cmd.Parameters.AddWithValue("@SourceURL", mc.sourceURL);
                        cmd.Parameters.AddWithValue("@PortNr", mc.portNr);
                        cmd.Parameters.AddWithValue("@Bandwidth", mc.BandWidth);
                        cmd.Parameters.AddWithValue("@State", mc.State);
                        cmd.Parameters.AddWithValue("@IspId", mc.IspId);
                        cmd.Parameters.AddWithValue("@FirstActivationDate", mc.FirstActivationDate);
                        cmd.Parameters.AddWithValue("@UserId", mc.UserId);
                        cmd.Parameters.AddWithValue("@OrgId", mc.OrganizationId);
                        cmd.Parameters.AddWithValue("@DistributorId", mc.DistributorId);
                        cmd.Parameters.AddWithValue("@NMSId", mc.NMSId);
                        cmd.Parameters.AddWithValue("@Reserved", mc.Reserved);
                        //cmd.CommandType = System.Data.CommandType.StoredProcedure;

                        int rtn = cmd.ExecuteNonQuery();

                        if (rtn != 0)
                        {
                            result = true;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtEx = new CmtApplicationException(ex, "Update of MulticastGroup with ID: " + mc.ID + " failed", "Critical error", (short)ExceptionLevelEnum.Error);
                _logController.LogApplicationException(cmtEx);
            }
            return result;
        }

        /// <summary>
        /// Returns a list of all Multicast groups contained by the MulticastGroups table
        /// </summary>
        /// <remarks>
        /// This method only brings back the non-deleted MC groups
        /// </remarks>
        /// <returns>A list of MultiCastGroup elements. This list can be empty</returns>
        public List<MultiCastGroup> GetMultiCastGroups()
        {
            string queryCmd = "SELECT IP, MacAddress, Name, Bytes, SourceURL, PortNr, Bandwidth, State, " +
                                "IspId, FirstActivationDate, UserId, OrgId, DistributorId, ID, NMSId, Reserved FROM MultiCastGroups WHERE Deleted = 0";
            List<MultiCastGroup> mcList = new List<MultiCastGroup>();
            MultiCastGroup mc = null;

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(queryCmd, conn))
                    {
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            mc = new MultiCastGroup();
                            mc.IPAddress = reader.GetString(0);
                            mc.MacAddress = reader.GetString(1);
                            mc.Name = reader.GetString(2);
                            if (!reader.IsDBNull(3))
                            {
                                mc.Bytes = reader.GetInt64(3);
                            }
                            mc.sourceURL = reader.GetString(4);
                            mc.portNr = reader.GetInt32(5);
                            mc.BandWidth = reader.GetInt64(6);
                            mc.State = reader.GetBoolean(7);
                            if (!reader.IsDBNull(8))
                            {
                                mc.IspId = reader.GetInt32(8);
                            }
                            if (!reader.IsDBNull(9))
                            {
                                mc.FirstActivationDate = reader.GetDateTime(9);
                            }
                            if (!reader.IsDBNull(10))
                            {
                                mc.UserId = reader.GetGuid(10);
                            }
                            if (!reader.IsDBNull(11))
                            {
                                mc.OrganizationId = reader.GetInt32(11);
                            }
                            if (!reader.IsDBNull(12))
                            {
                                mc.DistributorId = reader.GetInt32(12);
                            }
                            mc.ID = reader.GetInt32(13);
                            if (!reader.IsDBNull(14))
                            {
                                mc.NMSId = reader.GetInt32(14);
                            }
                            if (!reader.IsDBNull(15))
                            {
                                mc.Reserved = reader.GetBoolean(15);
                            }

                            mcList.Add(mc);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtEx = new CmtApplicationException(ex, "Selection of MultiCastGroups failed", "Error", (short)ExceptionLevelEnum.Error);
                _logController.LogApplicationException(cmtEx);
            }

            return mcList;
        }

        /// <summary>
        /// Returns a list of all multicast groups connected to a specific user
        /// </summary>
        /// <param name="userId">The ID of the user</param>
        /// <returns>A list of MultiCastGroup elements. This list can be empty</returns>
        public List<MultiCastGroup> GetMultiCastGroupsByUser(string userId)
        {
            BOAccountingController boAccountingControl = new BOAccountingController(_connectionString, _dataProvider);
            Distributor dist = boAccountingControl.GetDistributorForUser(userId);
            Organization org = boAccountingControl.GetOrganizationForUser(userId);

            if (dist != null)
            {
                return this.GetMultiCastGroupsByDistributor((int)dist.Id);
            }
            else if (org != null)
            {
                return this.GetMultiCastGroupsByOrganization((int)org.Id);
            }
            else return null;
        }

        /// <summary>
        /// Returns the MultiCastGroup by its MAC address
        /// </summary>
        /// <remarks>
        /// This value can be NULL if the MC group is not found
        /// </remarks>
        /// <param name="macAddress">The MAC address</param>
        /// <returns>The MultiCastGroup or null if not found</returns>
        public MultiCastGroup GetMultiCastGroupByMac(string macAddress)
        {
            MultiCastGroup mc = null;
            string queryCmd = "SELECT IP, MacAddress, Name, Bytes, SourceURL, PortNr, Bandwidth, State, " +
                                "IspId, FirstActivationDate, UserId, OrgId, DistributorId, Deleted, ID FROM MultiCastGroups WHERE MacAddress = @MACAddress";
            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(queryCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@MACAddress", macAddress);
                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            mc = new MultiCastGroup();
                            mc.IPAddress = reader.GetString(0);
                            mc.MacAddress = reader.GetString(1);
                            mc.Name = reader.GetString(2);
                            if (!reader.IsDBNull(3))
                            {
                                mc.Bytes = reader.GetInt64(3);
                            }
                            mc.sourceURL = reader.GetString(4);
                            mc.portNr = reader.GetInt32(5);
                            mc.BandWidth = reader.GetInt64(6);
                            mc.State = reader.GetBoolean(7);
                            if (!reader.IsDBNull(8))
                            {
                                mc.IspId = reader.GetInt32(8);
                            }
                            if (!reader.IsDBNull(9))
                            {
                                mc.FirstActivationDate = reader.GetDateTime(9);
                            }
                            if (!reader.IsDBNull(10))
                            {
                                mc.UserId = reader.GetGuid(10);
                            }
                            if (!reader.IsDBNull(11))
                            {
                                mc.OrganizationId = reader.GetInt32(11);
                            }
                            if (!reader.IsDBNull(12))
                            {
                                mc.DistributorId = reader.GetInt32(12);
                            }
                            mc.Deleted = reader.GetBoolean(13);
                            mc.ID = reader.GetInt32(14);
                        }
                        else
                        {
                            CmtApplicationException cmtEx = new CmtApplicationException();
                            cmtEx.ExceptionLevel = (short)ExceptionLevelEnum.Critical;
                            cmtEx.ExceptionDateTime = DateTime.UtcNow;
                            cmtEx.ExceptionDesc = "Could not find the MultiCastGroup with MAC Address: " + macAddress + " in CMT Database";
                            cmtEx.ExceptionStacktrace = "GetMultiCastGroupByMac";
                            cmtEx.UserDescription = "GetMultiCastGroupByMac failed";
                            _logController.LogApplicationException(cmtEx);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtEx = new CmtApplicationException(ex, "Retrieval of MulticastGroup with MAC Address: " + macAddress + " failed", "Exception", (short)ExceptionLevelEnum.Critical);
            }

            return mc;
        }


        /// <summary>
        /// Enables a Multicast group
        /// </summary>
        /// <param name="ipAddress"></param>
        public Boolean EnableMultiCastGroup(string ipAddress, string macAddress, int ispId, Guid userId)
        {
            bool result = false;
            IHubGateWay hubGateway = HubGatewayFactory.GetHubGateway(ispId);
            result = hubGateway.EnableMultiCastGroup(ipAddress, macAddress, ispId, userId);
            if (!result)
            {
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionLevel = (short)ExceptionLevelEnum.Critical;
                cmtEx.ExceptionDateTime = DateTime.UtcNow;
                cmtEx.ExceptionDesc = "Could not Enable the MultiCastGroup with IP: " + ipAddress + " in NMS with ISPId: " + ispId;
                cmtEx.ExceptionStacktrace = "EnableMultiCastGroup";
                cmtEx.UserDescription = "EnableMultiCastGroup failed";
                _logController.LogApplicationException(cmtEx);
            }

            return result;
        }

        /// <summary>
        /// Disables the Multicast group
        /// </summary>
        /// <param name="ipAddress"></param>
        /// <returns></returns>
        public Boolean DisableMultiCastGroup(string ipAddress, string macAddress, int ispId, Guid userId)
        {
            bool result = false;
            IHubGateWay hubGateway = HubGatewayFactory.GetHubGateway(ispId);
            result = hubGateway.DisableMultiCastGroup(ipAddress, macAddress, ispId, userId);
            if (!result)
            {
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionLevel = (short)ExceptionLevelEnum.Critical;
                cmtEx.ExceptionDateTime = DateTime.UtcNow;
                cmtEx.ExceptionDesc = "Could not Disable the MultiCastGroup with IP: " + ipAddress + " in NMS with ISPId: " + ispId;
                cmtEx.ExceptionStacktrace = "DisableMultiCastGroup";
                cmtEx.UserDescription = "DisableMultiCastGroup failed";
                _logController.LogApplicationException(cmtEx);
            }

            return result;
        }


        /// <summary>
        /// Updates the Multicast group schedule
        /// </summary>
        /// <param name="mcs"></param>
        /// <returns></returns>
        public bool UpdateMultiCastGroupSchedule(MultiCastGroupSchedule mcs)
        {
            bool result = false;
            string updateCmdSingle = "UPDATE MultiCastSchedule " +
                                    "SET MultiCastGroupID = @MultiCastGroupID, MulticastEventName = @MulticastEventName, RecurrentFlag = @RecurrentFlag, StartDate = @StartDate, EndDate = @EndDate, StartTime = @StartTime, EndTime = @EndTime, Description = @Description, SourceId = @SourceId " +
                                    "WHERE ID = @ID";
            mcs.Description = String.IsNullOrEmpty(mcs.Description) ? "" : mcs.Description;
            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(updateCmdSingle, conn))
                    {
                        cmd.Parameters.AddWithValue("@ID", mcs.ID);
                        cmd.Parameters.AddWithValue("@MultiCastGroupID", mcs.MultiCastGroupID);
                        cmd.Parameters.AddWithValue("@MulticastEventName", mcs.EventName);
                        cmd.Parameters.AddWithValue("@RecurrentFlag", mcs.RecurrentFlag);
                        //cmd.Parameters.AddWithValue("@RecEventId", mcs.RecurrentEventID);
                        cmd.Parameters.AddWithValue("@StartDate", mcs.StartDate.Date);
                        cmd.Parameters.AddWithValue("@EndDate", mcs.EndDate.Date);
                        cmd.Parameters.AddWithValue("@StartTime", mcs.StartTime);
                        cmd.Parameters.AddWithValue("@EndTime", mcs.EndTime);
                        cmd.Parameters.AddWithValue("@Description", mcs.Description);
                        cmd.Parameters.AddWithValue("@SourceID", mcs.SourceID);
                        //cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        int rtn = cmd.ExecuteNonQuery();
                        if (rtn != 0)
                        {
                            result = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //CmtApplicationException cmtEx = new CmtApplicationException(ex, "Update of MulticastGroup with IP: " + mc.IPAddress + " failed", "Critical error", (short)ExceptionLevelEnum.Critical);
                CmtApplicationException cmtEx = new CmtApplicationException(ex, "Updating of MulticastGroupSchedule failed", "Critical error", (short)ExceptionLevelEnum.Critical);
                _logController.LogApplicationException(cmtEx);
            }
            return result;
        }

        /// <summary>
        /// Creates the Multicast group schedule
        /// </summary>
        /// <param name="mcs"></param>
        /// <param name="scheduleList"></param>
        /// <returns></returns>
        public bool CreateMultiCastGroupSchedule(MultiCastGroupSchedule mcs, List<MultiCastGroupSchedule> scheduleList = null)
        {
            bool result = false;
            scheduleList = scheduleList ?? this.GetMulticastSchedulesByMCGroup(mcs.MultiCastGroupID);
            var modelstartdatetime = mcs.StartDate.Add(mcs.StartTime);
            var modelenddatetime = mcs.StartDate.Add(mcs.EndTime);
            bool overlap = false;
            foreach (var schedule in scheduleList)
            {
                var startdatetime = new DateTime(schedule.StartDate.Year, schedule.StartDate.Month, schedule.StartDate.Day, schedule.StartTime.Hours, schedule.StartTime.Milliseconds, schedule.StartTime.Seconds);
                var enddatetime = new DateTime(schedule.EndDate.Year, schedule.EndDate.Month, schedule.EndDate.Day, schedule.EndTime.Hours, schedule.EndTime.Minutes, schedule.EndTime.Seconds);
                if (modelstartdatetime < enddatetime && startdatetime < modelenddatetime)
                {
                    overlap = true;
                    break;
                }
            }

            if (!overlap)
            {
                try
                {
                    using (SqlConnection conn = new SqlConnection(_connectionString))
                    {
                        conn.Open();
                        using (SqlCommand cmd = new SqlCommand("cmtCreateMultiCastSchedule", conn))
                        {
                            cmd.Parameters.AddWithValue("@MultiCastGroupID", mcs.MultiCastGroupID);
                            cmd.Parameters.AddWithValue("@MulticastEventName", mcs.EventName);
                            cmd.Parameters.AddWithValue("@RecurrentFlag", mcs.RecurrentFlag);
                            cmd.Parameters.AddWithValue("@RecEventId", mcs.RecurrentEventID);
                            cmd.Parameters.AddWithValue("@StartDate", mcs.StartDate.Date);
                            cmd.Parameters.AddWithValue("@EndDate", mcs.EndDate.Date);
                            cmd.Parameters.AddWithValue("@StartTime", mcs.StartTime);
                            cmd.Parameters.AddWithValue("@EndTime", mcs.EndTime);
                            cmd.Parameters.AddWithValue("@Description", mcs.Description);
                            cmd.Parameters.AddWithValue("@SourceID", mcs.SourceID);
                            cmd.CommandType = System.Data.CommandType.StoredProcedure;

                            int rtn = cmd.ExecuteNonQuery();

                            if (rtn != 0)
                            {
                                result = true;
                            }

                        }
                    }
                }
                catch (Exception ex)
                {
                    //CmtApplicationException cmtEx = new CmtApplicationException(ex, "Update of MulticastGroup with IP: " + mc.IPAddress + " failed", "Critical error", (short)ExceptionLevelEnum.Critical);
                    CmtApplicationException cmtEx = new CmtApplicationException(ex, "Creating of MulticastGroupSchedule failed", "Critical error", (short)ExceptionLevelEnum.Critical);
                    _logController.LogApplicationException(cmtEx);
                }
            }
            
            return result;
        }



        /// <summary>
        /// Method for creating a recurring multicast group schedule
        /// First create the recurrent event entry and the creates the separate scheduled events
        /// </summary>
        /// <param name="mcs">The data of the first scheduled event</param>
        /// <param name="mcr">The data of the overall recurrent event</param>
        /// <returns>True if successful</returns>
        public bool CreateRecurrentMultiCastGroupSchedule(MultiCastGroupSchedule mcs, MultiCastRecurrentEvents mcr)
        {
            bool result = false;
            //string createCmd = "INSERT INTO MultiCastRecurrentEvents (StartDate, EndDate, StartTime, EndTime, Interval) " +
            //                    "OUTPUT INSERTED.ID " +
            //                    "VALUES(@StartDate, @EndDate, @StartTime, @EndTime, @Interval)";

            try
            {
                //using (SqlConnection conn = new SqlConnection(_connectionString))
                //{
                //    conn.Open();
                //    using (SqlCommand cmd = new SqlCommand(createCmd, conn))
                //    {
                //        //create the recurrent event object in the database
                //        cmd.Parameters.AddWithValue("@StartDate", mcr.StartDate.Date);
                //        cmd.Parameters.AddWithValue("@EndDate", mcr.EndDate.Date);
                //        cmd.Parameters.AddWithValue("@StartTime", mcr.StartTime);
                //        cmd.Parameters.AddWithValue("@EndTime", mcr.EndTime);
                //        cmd.Parameters.AddWithValue("@Interval", mcr.Interval);
                //        //cmd.CommandType = System.Data.CommandType.StoredProcedure;
                //        //SqlParameter output = new SqlParameter("@ID", SqlDbType.Int);
                //        //output.Direction = ParameterDirection.Output;
                //        //cmd.Parameters.Add(output);
                //        //cmd.ExecuteNonQuery();
                //        mcr.ID = (Int32)cmd.ExecuteScalar();
                //        //get the ID of the newly created reccurent event
                //        //mcr.ID = Convert.ToInt32(output.Value);
                //    }
                //}

                var scheduled = this.GetMulticastSchedulesByMCGroup(mcs.MultiCastGroupID);

                //get the date information of the first scheduled event
                DateTime nextEventDate = mcs.StartDate.Date;
                DateTime nextEventEndDate = mcs.EndDate.Date;
                DateTime lastEventDate = mcr.EndDate.Date.AddDays(1);
                int interval = 1;
                //create scheduled events based on the interval
                if (mcr.Interval == 3) //monthly events
                {
                    while (nextEventDate < lastEventDate)
                    {
                        mcs.StartDate = nextEventDate;
                        mcs.EndDate = nextEventEndDate;
                        //mcs.RecurrentEventID = mcr.ID;
                        mcs.RecurrentEventID = null;
                        this.CreateMultiCastGroupSchedule(mcs, scheduled);
                        nextEventDate = nextEventDate.AddMonths(interval);
                        nextEventEndDate = nextEventEndDate.AddMonths(interval);
                    }
                }
                else //daily or weekly events
                {
                    if (mcr.Interval == 2)
                    {
                        //set the interval to 7 days in case of weekly events
                        interval = 7;
                    }

                    while (nextEventDate < lastEventDate)
                    {
                        mcs.StartDate = nextEventDate;
                        mcs.EndDate = nextEventEndDate;
                        //mcs.RecurrentEventID = mcr.ID;
                        mcs.RecurrentEventID = null;
                        this.CreateMultiCastGroupSchedule(mcs, scheduled);
                        nextEventDate = nextEventDate.AddDays(interval);
                        nextEventEndDate = nextEventEndDate.AddDays(interval);
                    }
                }

                result = true;
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtEx = new CmtApplicationException(ex, "Creating of MulticastGroupSchedule failed", "Critical error", (short)ExceptionLevelEnum.Error);
                _logController.LogApplicationException(cmtEx);
            }
            return result;
        }


        public List<MultiCastGroupSchedule> GetMultiCastGroupSchedules()
        {
            string queryCmd = "SELECT [ID], [MulticastGroupID], [MulticastEventName], [RecurrentFlag], [RecEventId], [StartDate], [EndDate], [StartTime], [EndTime], [Description], [SourceId]" +
                "FROM [CMTData-Production].[dbo].[MultiCastSchedule]";
            List<MultiCastGroupSchedule> mcgList = new List<MultiCastGroupSchedule>();
            MultiCastGroupSchedule schedule = null;

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(queryCmd, conn))
                    {
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            schedule = new MultiCastGroupSchedule();
                            schedule.ID = reader.GetInt32(0);
                            schedule.MultiCastGroupID = reader.GetInt32(1);
                            schedule.EventName = reader.GetString(2);
                            schedule.RecurrentFlag = reader.GetBoolean(3);
                            if (!reader.IsDBNull(4))
                            {
                                schedule.RecurrentEventID = reader.GetInt32(4);
                            }
                            schedule.StartDate = reader.GetDateTime(5);
                            schedule.EndDate = reader.GetDateTime(6);
                            schedule.StartTime = reader.GetTimeSpan(7);
                            schedule.EndTime = reader.GetTimeSpan(8);
                            if (!reader.IsDBNull(9))
                            {
                                schedule.Description = reader.GetString(9);
                            }
                            if (!reader.IsDBNull(10))
                            {
                                schedule.SourceID = reader.GetInt32(10);
                            }

                            mcgList.Add(schedule);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtEx = new CmtApplicationException(ex, "Selection of MultiCastGroupSchedules failed", "Error", (short)ExceptionLevelEnum.Error);
                _logController.LogApplicationException(cmtEx);
            }

            return mcgList;
        }

        /// <summary>
        /// Gets all MultiCastGroupSchedules available in the CMT DB, between two given DateTimes.
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        /// SATCORP-52 will use this method in the NMSMultiCastLogSync console app, to get MultiCastGroupSchedules form CMT DB
        public List<MultiCastGroupSchedule> GetAllMultiCastGroupSchedulesBetweenDateTimes(DateTime startDate, DateTime endDate)
        {
            string queryCmd = "SELECT [ID], [MulticastGroupID], [MulticastEventName], [RecurrentFlag], [RecEventId], [StartDate], [EndDate], [StartTime], [EndTime], [Description], [SourceId]" +
                "FROM [CMTData-Production].[dbo].[MultiCastSchedule] where StartDate between @startDate and @endDate";
            List<MultiCastGroupSchedule> mcgsList = new List<MultiCastGroupSchedule>();
            MultiCastGroupSchedule schedule = null;

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(queryCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@startDate", startDate);
                        cmd.Parameters.AddWithValue("@endDate", endDate);
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            schedule = new MultiCastGroupSchedule();
                            schedule.ID = reader.GetInt32(0);
                            schedule.MultiCastGroupID = reader.GetInt32(1);
                            schedule.EventName = reader.GetString(2);
                            schedule.RecurrentFlag = reader.GetBoolean(3);
                            if (!reader.IsDBNull(4))
                            {
                                schedule.RecurrentEventID = reader.GetInt32(4);
                            }
                            schedule.StartDate = reader.GetDateTime(5);
                            schedule.EndDate = reader.GetDateTime(6);
                            schedule.StartTime = reader.GetTimeSpan(7);
                            schedule.EndTime = reader.GetTimeSpan(8);
                            if (!reader.IsDBNull(9))
                            {
                                schedule.Description = reader.GetString(9);
                            }
                            if (!reader.IsDBNull(10))
                            {
                                schedule.SourceID = reader.GetInt32(10);
                            }

                            mcgsList.Add(schedule);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtEx = new CmtApplicationException(ex, "Selection of MultiCastGroupSchedules failed", "Error", (short)ExceptionLevelEnum.Error);
                _logController.LogApplicationException(cmtEx);
            }

            return mcgsList;
        }

        /// <summary>
        /// Returns all schedules for a specific multicast group
        /// </summary>
        /// <param name="id">The ID of the multicast group</param>
        /// <returns>A list of multicast group schedules. Can be null</returns>
        public List<MultiCastGroupSchedule> GetMulticastSchedulesByMCGroup(int id)
        {
            string queryCmd = "SELECT [ID], [MulticastGroupID], [MulticastEventName], [RecurrentFlag], [RecEventId], [StartDate], [EndDate], [StartTime], [EndTime], [Description], [SourceId]" +
                "FROM [CMTData-Production].[dbo].[MultiCastSchedule]" +
                "WHERE [MulticastGroupID] = @MulticastGroupID";

            List<MultiCastGroupSchedule> mcgList = new List<MultiCastGroupSchedule>();
            MultiCastGroupSchedule schedule = null;

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(queryCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@MulticastGroupID", id);
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            schedule = new MultiCastGroupSchedule();
                            schedule.ID = reader.GetInt32(0);
                            schedule.MultiCastGroupID = reader.GetInt32(1);
                            schedule.EventName = reader.GetString(2);
                            schedule.RecurrentFlag = reader.GetBoolean(3);
                            if (!reader.IsDBNull(4))
                            {
                                schedule.RecurrentEventID = reader.GetInt32(4);
                            }
                            schedule.StartDate = reader.GetDateTime(5);
                            schedule.EndDate = reader.GetDateTime(6);
                            schedule.StartTime = reader.GetTimeSpan(7);
                            schedule.EndTime = reader.GetTimeSpan(8);
                            if (!reader.IsDBNull(9))
                            {
                                schedule.Description = reader.GetString(9);
                            }
                            if (!reader.IsDBNull(10))
                            {
                                schedule.SourceID = reader.GetInt32(10);
                            }

                            mcgList.Add(schedule);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtEx = new CmtApplicationException(ex, "Selection of MultiCastGroupSchedules by MC group failed. Multicast group ID:" + id, "Error", (short)ExceptionLevelEnum.Error);
                _logController.LogApplicationException(cmtEx);
            }

            return mcgList;
        }

        public MultiCastGroupSchedule GetMultiCastGroupScheduleByID(int id)
        {
            MultiCastGroupSchedule schedule = null;
            string queryCmd = "SELECT [ID], [MulticastGroupID], [MulticastEventName], [RecurrentFlag], [RecEventId], [StartDate], [EndDate], [StartTime], [EndTime], [Description], [SourceId]" +
                "FROM [CMTData-Production].[dbo].[MultiCastSchedule] WHERE ID = @ID";
            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(queryCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@ID", id);
                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            schedule = new MultiCastGroupSchedule();
                            schedule.ID = reader.GetInt32(0);
                            schedule.MultiCastGroupID = reader.GetInt32(1);
                            schedule.EventName = reader.GetString(2);
                            schedule.RecurrentFlag = reader.GetBoolean(3);
                            if (!reader.IsDBNull(4))
                            {
                                schedule.RecurrentEventID = reader.GetInt32(4);
                            }
                            schedule.StartDate = reader.GetDateTime(5);
                            schedule.EndDate = reader.GetDateTime(6);
                            schedule.StartTime = reader.GetTimeSpan(7);
                            schedule.EndTime = reader.GetTimeSpan(8);
                            if (!reader.IsDBNull(9))
                            {
                                schedule.Description = reader.GetString(9);
                            }
                            if (!reader.IsDBNull(10))
                            {
                                schedule.SourceID = reader.GetInt32(10);
                            }
                        }
                        else
                        {
                            CmtApplicationException cmtEx = new CmtApplicationException();
                            cmtEx.ExceptionLevel = (short)ExceptionLevelEnum.Critical;
                            cmtEx.ExceptionDateTime = DateTime.UtcNow;
                            cmtEx.ExceptionDesc = "Could not find the MultiCastGroup with ID: " + id + " in CMT Database";
                            cmtEx.ExceptionStacktrace = "GetMultiCastGroupByID";
                            cmtEx.UserDescription = "Get MultiCastGroupByID failed";
                            _logController.LogApplicationException(cmtEx);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtEx = new CmtApplicationException(ex, "Retrieval of MulticastGroupSchedule with ID: " + id + " failed", "Exception", (short)ExceptionLevelEnum.Critical);
                _logController.LogApplicationException(cmtEx);
            }

            return schedule;
        }

        public bool DeleteMultiCastGroupSchedule(MultiCastGroupSchedule mcs)
        {
            bool result = false;
            string deleteCmd = "DELETE FROM MultiCastSchedule WHERE ID = @ID";

            try
            {
                //if (!(mcs.StartDate < DateTime.Now))
                //{
                    using (SqlConnection conn = new SqlConnection(_connectionString))
                    {
                        conn.Open();
                        using (SqlCommand cmd = new SqlCommand(deleteCmd, conn))
                        {
                            cmd.Parameters.AddWithValue("@ID", mcs.ID);

                            int rtn = cmd.ExecuteNonQuery();

                            if (rtn != 0)
                            {
                                result = true;
                            }
                        }
                    }
               // }
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtEx = new CmtApplicationException(ex, "Deleting of MulticastGroupSchedule failed", "Critical error", (short)ExceptionLevelEnum.Critical);
                _logController.LogApplicationException(cmtEx);
            }
            return result;
        }

        /// <summary>
        /// Updates a set of recurrent events in the CMT db
        /// </summary>
        /// <param name="mcs">The single multicast schedule that was edited</param>
        /// <param name="mcr">The recurrent event</param>
        /// <returns>true if successful</returns>
        public bool UpdateRecurrentMultiCastGroupSchedule(MultiCastGroupSchedule mcs, MultiCastRecurrentEvents mcr)
        {
            bool result = false;
            string updateCmdRecurrent = "UPDATE MultiCastRecurrentEvents " +
                                        "SET StartDate = @StartDate, EndDate = @EndDate, StartTime = @StartTime, EndTime = @EndTime, Deleted = @Deleted " +
                                        "WHERE ID = @ID";
            string updateCmdSingle = "UPDATE MultiCastSchedule " +
                                     "SET MulticastEventName = @MulticastEventName, StartTime = @StartTime, EndTime = @EndTime, Description = @Description, SourceId = @SourceId " +
                                     "WHERE RecEventId = @RecEventId";
            string deleteCmd = "DELETE FROM MultiCastSchedule " +
                                  "WHERE RecEventId = @RecEventId AND StartDate > @StartDate";
            bool delete = false;
            DateTime deleteFrom = DateTime.UtcNow;
            var scheduled = this.GetMulticastSchedulesByMCGroup(mcs.ID);
            //check if recurrent event has been deleted
            if (mcr.Deleted)
            {
                delete = true;
            }
            else
            {
                //Get the recurrent event from the database to compare with
                MultiCastRecurrentEvents oldMCR = this.GetMulticastRecurrentEventById(mcr.ID);
                //check if end date has changed
                if (mcr.EndDate < oldMCR.EndDate)
                {
                    //delete unwanted events 
                    delete = true;
                    deleteFrom = mcr.EndDate;
                }
                else if (mcr.EndDate > oldMCR.EndDate)
                {
                    //create extra events
                    //get the date information of the first scheduled event


                    int interval = 1;
                    //create scheduled events based on the interval
                    if (mcr.Interval == 3) //monthly events
                    {
                        DateTime nextEventDate = oldMCR.EndDate.Date.AddMonths(1);
                        DateTime nextEventEndDate = nextEventDate.AddDays((mcs.EndDate.Date - mcs.StartDate.Date).TotalDays);
                        while (nextEventDate < mcr.EndDate.Date)
                        {
                            mcs.StartDate = nextEventDate;
                            mcs.EndDate = nextEventEndDate;
                            mcs.RecurrentEventID = mcr.ID;
                            this.CreateMultiCastGroupSchedule(mcs, scheduled);
                            nextEventDate = nextEventDate.AddMonths(interval);
                            nextEventEndDate = nextEventEndDate.AddMonths(interval);
                        }
                    }
                    else //daily or weekly events
                    {

                        if (mcr.Interval == 2)
                        {
                            //set the interval to 7 days in case of weekly events
                            interval = 7;
                        }

                        DateTime nextEventDate = oldMCR.EndDate.Date.AddDays(interval);
                        DateTime nextEventEndDate = nextEventDate.AddDays((mcs.EndDate.Date - mcs.StartDate.Date).TotalDays);
                        while (nextEventDate < mcr.EndDate.Date)
                        {
                            mcs.StartDate = nextEventDate;
                            mcs.EndDate = nextEventEndDate;
                            mcs.RecurrentEventID = mcr.ID;
                            this.CreateMultiCastGroupSchedule(mcs, scheduled);
                            nextEventDate = nextEventDate.AddDays(interval);
                            nextEventEndDate = nextEventEndDate.AddDays(interval);
                        }
                    }
                }
            }

            try
            {
                //update the recurrent event
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(updateCmdRecurrent, conn))
                    {
                        cmd.Parameters.AddWithValue("@ID", mcr.ID);
                        cmd.Parameters.AddWithValue("@StartDate", mcr.StartDate);
                        cmd.Parameters.AddWithValue("@EndDate", mcr.EndDate);
                        cmd.Parameters.AddWithValue("@StartTime", mcr.StartTime);
                        cmd.Parameters.AddWithValue("@EndTime", mcr.EndTime);
                        cmd.Parameters.AddWithValue("@Deleted", mcr.Deleted);
                        int rtn = cmd.ExecuteNonQuery();

                        if (rtn != 0)
                        {
                            result = true;
                        }
                    }
                }

                //Delete single events if applicable
                if (delete)
                {
                    using (SqlConnection conn = new SqlConnection(_connectionString))
                    {
                        conn.Open();
                        using (SqlCommand cmd = new SqlCommand(deleteCmd, conn))
                        {
                            cmd.Parameters.AddWithValue("@RecEventId", mcr.ID);
                            cmd.Parameters.AddWithValue("@StartDate", deleteFrom);

                            int rtn = cmd.ExecuteNonQuery();

                            if (rtn != 0)
                            {
                                result = true;
                            }
                        }
                    }
                }

                //update the individual events
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(updateCmdSingle, conn))
                    {
                        cmd.Parameters.AddWithValue("@RecEventId", mcr.ID);
                        cmd.Parameters.AddWithValue("@MulticastEventName", mcs.EventName);
                        cmd.Parameters.AddWithValue("@StartTime", mcr.StartTime);
                        cmd.Parameters.AddWithValue("@EndTime", mcr.EndTime);
                        cmd.Parameters.AddWithValue("@Description", mcs.Description);
                        cmd.Parameters.AddWithValue("@SourceID", mcs.SourceID);


                        int rtn = cmd.ExecuteNonQuery();

                        if (rtn != 0)
                        {
                            result = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtEx = new CmtApplicationException(ex, "Updating of Recurrent MulticastGroupSchedule failed", "Critical error", (short)ExceptionLevelEnum.Critical);
                _logController.LogApplicationException(cmtEx);
                result = false;
            }
            return result;
        }

        /// <summary>
        /// This method fetches a recurrent event object from the CMT db
        /// </summary>
        /// <param name="id">the ID of the recurrent event</param>
        /// <returns>the recurrent event object</returns>
        public MultiCastRecurrentEvents GetMulticastRecurrentEventById(int id)
        {
            MultiCastRecurrentEvents mcr = new MultiCastRecurrentEvents();
            string queryCmd = "SELECT ID, StartDate, EndDate, StartTime, EndTime, Interval, Deleted " +
                              "FROM MultiCastRecurrentEvents WHERE ID = @ID";
            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(queryCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@ID", id);
                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            mcr.ID = reader.GetInt32(0);
                            mcr.StartDate = reader.GetDateTime(1);
                            mcr.EndDate = reader.GetDateTime(2);
                            mcr.StartTime = reader.GetTimeSpan(3);
                            mcr.EndTime = reader.GetTimeSpan(4);
                            mcr.Interval = reader.GetInt32(5);
                            if (!reader.IsDBNull(6))
                            {
                                mcr.Deleted = reader.GetBoolean(6);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtEx = new CmtApplicationException(ex, "Retrieval of MulticastGroupSchedule with ID: " + id + " failed", "Exception", (short)ExceptionLevelEnum.Critical);
                _logController.LogApplicationException(cmtEx);
            }

            return mcr;
        }

        /// <summary>
        /// Method for creating a new multicast source
        /// </summary>
        /// <param name="source">The multicast source object to be added to the database</param>
        /// <returns>True if successful</returns>
        public bool AddMultiCastSource(MultiCastSource source)
        {
            bool success = false;
            string insertCmd = "INSERT INTO MultiCastSources " +
                               "(SourceName, URL, Username, Password, SatADSLSource, MultiCastGroupID) " +
                               "VALUES (@SourceName, @URL, @Username, @Password, @SatADSLSource, @MultiCastGroupID)";

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(insertCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@SourceName", source.sourceName);
                        cmd.Parameters.AddWithValue("@URL", source.URL);
                        cmd.Parameters.AddWithValue("@Username", source.username);
                        cmd.Parameters.AddWithValue("@Password", source.password);
                        cmd.Parameters.AddWithValue("@SatADSLSource", source.satADSLSource);
                        cmd.Parameters.AddWithValue("@MultiCastGroupID", source.multiCastGroupID);

                        int rtn = cmd.ExecuteNonQuery();

                        if (rtn != 0)
                        {
                            success = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtEx = new CmtApplicationException(ex, "Adding multicast source failed", "Exception", (short)ExceptionLevelEnum.Error);
                _logController.LogApplicationException(cmtEx);
            }

            return success;
        }

        /// <summary>
        /// Deletes a multicast source from the database
        /// </summary>
        /// <param name="sourceID">The ID of the multicast source to be deleted</param>
        /// <returns>True if successful</returns>
        public bool DeleteMultiCastSource(MultiCastSource src)
        {
            bool success = false;
            string deleteCmd = "UPDATE MultiCastSources " +
                               "SET Deleted = 1 " +
                               "WHERE SourceID = @SourceID";

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(deleteCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@SourceID", src.sourceID);
                        SqlDataReader reader = cmd.ExecuteReader();
                        int rtn = cmd.ExecuteNonQuery();

                        if (rtn != 0)
                        {
                            success = true;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtEx = new CmtApplicationException(ex, "Deleting multicast source with ID: " + src.sourceID + " failed", "Exception", (short)ExceptionLevelEnum.Error);
                _logController.LogApplicationException(cmtEx);
            }

            return success;
        }

        /// <summary>
        /// Updates a multicast source in the database
        /// </summary>
        /// <param name="source">The multicast object with the new paramaters</param>
        /// <returns>True if successful</returns>
        public bool UpdateMultiCastSource(MultiCastSource source)
        {
            bool success = false;
            string deleteCmd = "UPDATE MultiCastSources " +
                               "SET SourceName = @SourceName, URL = @URL, Username = @Username, Password = @Password, SatADSLSource = @SatADSLSource, MultiCastGroupID = @MultiCastGroupID " +
                               "WHERE SourceID = @SourceID";

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(deleteCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@SourceID", source.sourceID);
                        cmd.Parameters.AddWithValue("@SourceName", source.sourceName);
                        cmd.Parameters.AddWithValue("@URL", source.URL);
                        cmd.Parameters.AddWithValue("@Username", source.username);
                        cmd.Parameters.AddWithValue("@Password", source.password);
                        cmd.Parameters.AddWithValue("@SatADSLSource", source.satADSLSource);
                        cmd.Parameters.AddWithValue("@MultiCastGroupID", source.multiCastGroupID);
                        int rtn = cmd.ExecuteNonQuery();

                        if (rtn != 0)
                        {
                            success = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtEx = new CmtApplicationException(ex, "Updating multicast source with ID: " + source.sourceID + " failed", "Exception", (short)ExceptionLevelEnum.Error);
                _logController.LogApplicationException(cmtEx);
            }

            return success;
        }

        /// <summary>
        /// Fetches a specific multicast source
        /// </summary>
        /// <param name="sourceID">The ID of the multicast source to be fetched</param>
        /// <returns>A multicast source object</returns>
        public MultiCastSource GetMultiCastSourceByID(int sourceID)
        {
            MultiCastSource source = new MultiCastSource();
            string selectCmd = "SELECT SourceID, SourceName, URL, Username, Password, SatADSLSource, MultiCastGroupID FROM MultiCastSources " +
                               "WHERE SourceID = @SourceID";

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(selectCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@SourceID", sourceID);
                        SqlDataReader reader = cmd.ExecuteReader();
                        reader.Read();

                        source.sourceID = reader.GetInt32(0);
                        source.sourceName = reader.GetString(1);
                        source.URL = reader.GetString(2);
                        source.username = reader.GetString(3);
                        source.password = reader.GetString(4);
                        source.satADSLSource = reader.GetBoolean(5);
                        source.multiCastGroupID = reader.GetInt32(6);
                    }
                }
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtEx = new CmtApplicationException(ex, "Fetching multicast source with ID: " + sourceID + " failed", "Exception", (short)ExceptionLevelEnum.Error);
                _logController.LogApplicationException(cmtEx);
            }

            return source;
        }

        /// <summary>
        /// Fetches all multicast sources in the database
        /// </summary>
        /// <returns>A list of multicast source objects</returns>
        public List<MultiCastSource> GetAllMultiCastSources()
        {
            List<MultiCastSource> sources = new List<MultiCastSource>();
            MultiCastSource source;
            string selectCmd = "SELECT SourceID, SourceName, URL, Username, Password, SatADSLSource, MultiCastGroupID FROM MultiCastSources " +
                               "WHERE Deleted = 0 OR Deleted IS NULL";

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(selectCmd, conn))
                    {
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            source = new MultiCastSource();
                            source.sourceID = reader.GetInt32(0);
                            source.sourceName = reader.GetString(1);
                            source.URL = reader.GetString(2);
                            source.username = reader.GetString(3);
                            source.password = reader.GetString(4);
                            source.satADSLSource = reader.GetBoolean(5);
                            source.multiCastGroupID = reader.GetInt32(6);
                            sources.Add(source);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtEx = new CmtApplicationException(ex, "Fetching all multicast sources failed", "Exception", (short)ExceptionLevelEnum.Error);
                _logController.LogApplicationException(cmtEx);
            }

            return sources;
        }

        /// <summary>
        /// Fetches all multicast sources for a specific multicast group
        /// </summary>
        /// <param name="multiCastGroupID">The ID of the multicast group for which sources should be fetched</param>
        /// <returns>A list of multicast source objects</returns>
        public List<MultiCastSource> GetMultiCastSourcesByMCGroupID(int multiCastGroupID)
        {
            List<MultiCastSource> sources = new List<MultiCastSource>();
            MultiCastSource source;
            string selectCmd = "SELECT SourceID, SourceName, URL, Username, Password, SatADSLSource, MultiCastGroupID FROM MultiCastSources " +
                               "WHERE MultiCastGroupID = @MultiCastGroupID AND (Deleted = 0 OR Deleted IS NULL)";

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(selectCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@MultiCastGroupID", multiCastGroupID);
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            source = new MultiCastSource();
                            source.sourceID = reader.GetInt32(0);
                            source.sourceName = reader.GetString(1);
                            source.URL = reader.GetString(2);
                            source.username = reader.GetString(3);
                            source.password = reader.GetString(4);
                            source.satADSLSource = reader.GetBoolean(5);
                            source.multiCastGroupID = reader.GetInt32(6);
                            sources.Add(source);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtEx = new CmtApplicationException(ex, "Fetching all multicast sources failed", "Exception", (short)ExceptionLevelEnum.Error);
                _logController.LogApplicationException(cmtEx);
            }

            return sources;
        }


        public List<MultiCastLog> GetMultiCastLogsBetweenDateTimes(DateTime startDate, DateTime endDate, string macAddress)
        {
            string queryCmd = "SELECT [BufferLevel],[Latency],[FrameLoss],[FrameDup],[FrameDrop],[Bitrate],[Volume],[LastUpdate]" +
                //                "FROM [CMTData-Production].[dbo].[MultiCastLog] where LastUpdate between \'" + startDate.ToString("yyyy-MM-dd HH:mm:ss") + "\' and \'" + endDate.ToString("yyyy-MM-dd HH:mm:ss") + "\'";
                "FROM [CMTData-Production].[dbo].[MultiCastLog] where LastUpdate between @startDate and @endDate and MacAddress = @macAddress";

            List<MultiCastLog> mclList = new List<MultiCastLog>();
            MultiCastLog log = null;

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(queryCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@startDate", startDate);
                        cmd.Parameters.AddWithValue("@endDate", endDate);
                        cmd.Parameters.AddWithValue("@macAddress", macAddress);
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            log = new MultiCastLog();
                            log.BufferLevel = reader.GetInt32(0);
                            log.Latency = reader.GetInt32(1);
                            log.FrameLoss = reader.GetInt32(2);
                            log.FrameDup = reader.GetInt32(3);
                            log.FrameDrop = reader.GetInt32(4);
                            log.BitRate = reader.GetInt32(5);
                            log.Volume = reader.GetInt16(6);
                            log.LastUpdate = reader.GetDateTime(7);

                            mclList.Add(log);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtEx = new CmtApplicationException(ex, "Selection of MultiCastGroupSchedules failed", "Error", (short)ExceptionLevelEnum.Error);
                _logController.LogApplicationException(cmtEx);
            }

            return mclList;
        }

        public List<MultiCastLog> GetMultiCastLogsInLastHour(string macAddress)
        {
            return GetMultiCastLogsBetweenDateTimes(DateTime.Now.AddHours(-1), DateTime.Now, macAddress);
        }

        public MultiCastLog GetLatestMultiCastLog(string macAddress)
        {
            // Get Last Id for a MultiCastLog on a certain MacAddress
            int lastEntry = GetLastMultiCastLogIdForMacAddress(macAddress);

            string queryCmd = "SELECT [BufferLevel],[Latency],[FrameLoss],[FrameDup],[FrameDrop],[Bitrate],[Volume],[LastUpdate]" +
                "FROM [CMTData-Production].[dbo].[MultiCastLog] where Id = @lastEntry";

            MultiCastLog log = null;

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(queryCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@lastEntry", lastEntry);
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            log = new MultiCastLog();
                            log.BufferLevel = reader.GetInt32(0);
                            log.Latency = reader.GetInt32(1);
                            log.FrameLoss = reader.GetInt32(2);
                            log.FrameDup = reader.GetInt32(3);
                            log.FrameDrop = reader.GetInt32(4);
                            log.BitRate = reader.GetInt32(5);
                            log.Volume = reader.GetInt16(6);
                            log.LastUpdate = reader.GetDateTime(7);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtEx = new CmtApplicationException(ex, "Selection of MultiCastGroupSchedules failed", "Error", (short)ExceptionLevelEnum.Error);
                _logController.LogApplicationException(cmtEx);
            }
            return log;
        }

        #region helper methods

        /// <summary>
        /// this method creates an empty Multicast group in the CMT database
        /// It is linked to the NMS by the NMS ID
        /// </summary>
        /// <param name="NMSId">The ID of the Multicast group in the NMS</param>
        /// <returns>true if successful</returns>
        private bool CreateEmtpyMulticastGroup(int nmsId)
        {
            bool result = false;
            string insertCmd = "INSERT INTO MultiCastGroups (State, NMSId, Deleted, Reserved) " +
                               "VALUES (@State, @NMSId, @Deleted, @Reserved)";
            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(insertCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@State", 0);
                        cmd.Parameters.AddWithValue("@NMSId", nmsId);
                        cmd.Parameters.AddWithValue("@Deleted", 0);
                        cmd.Parameters.AddWithValue("@Reserved", 0);

                        int rtn = cmd.ExecuteNonQuery();

                        if (rtn != 0)
                        {
                            result = true;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtEx = new CmtApplicationException(ex, "Creating an empty Multicast Group failed", "Critical error", (short)ExceptionLevelEnum.Error);
                _logController.LogApplicationException(cmtEx);
            }
            return result;
        }

        private int GetLastMultiCastLogIdForMacAddress(string macAddress)
        {
            string queryCmd = "SELECT MAX(Id) FROM MultiCastLog WHERE MacAddress = @macAddress";

            MultiCastLog log = null;

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(queryCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@macAddress", macAddress);
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            log = new MultiCastLog();
                            log.Id = reader.GetInt32(0);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtEx = new CmtApplicationException(ex, "Selection of MultiCastGroupSchedules failed", "Error", (short)ExceptionLevelEnum.Error);
                _logController.LogApplicationException(cmtEx);
            }
            return log.Id;
        }

        /// <summary>
        /// Helper method to get multicast groups registered to a specific distributor
        /// </summary>
        /// <param name="distributorId">The ID of the distributor</param>
        /// <returns>A list of MultiCastGroup elements. This list can be empty</returns>
        private List<MultiCastGroup> GetMultiCastGroupsByDistributor(int distributorId)
        {
            string queryCmd = "SELECT IP, MacAddress, Name, Bytes, SourceURL, PortNr, Bandwidth, State, " +
                               "IspId, FirstActivationDate, UserId, OrgId, DistributorId, ID, NMSId, Reserved FROM MultiCastGroups " +
                               "WHERE Deleted = 0 AND DistributorId = @DistributorId";

            List<MultiCastGroup> mcList = new List<MultiCastGroup>();
            MultiCastGroup mc = null;

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(queryCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@DistributorId", distributorId);
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            mc = new MultiCastGroup();
                            mc.IPAddress = reader.GetString(0);
                            mc.MacAddress = reader.GetString(1);
                            mc.Name = reader.GetString(2);
                            if (!reader.IsDBNull(3))
                            {
                                mc.Bytes = reader.GetInt64(3);
                            }
                            mc.sourceURL = reader.GetString(4);
                            mc.portNr = reader.GetInt32(5);
                            mc.BandWidth = reader.GetInt64(6);
                            mc.State = reader.GetBoolean(7);
                            if (!reader.IsDBNull(8))
                            {
                                mc.IspId = reader.GetInt32(8);
                            }
                            if (!reader.IsDBNull(9))
                            {
                                mc.FirstActivationDate = reader.GetDateTime(9);
                            }
                            if (!reader.IsDBNull(10))
                            {
                                mc.UserId = reader.GetGuid(10);
                            }
                            if (!reader.IsDBNull(11))
                            {
                                mc.OrganizationId = reader.GetInt32(11);
                            }
                            if (!reader.IsDBNull(12))
                            {
                                mc.DistributorId = reader.GetInt32(12);
                            }
                            mc.ID = reader.GetInt32(13);
                            if (!reader.IsDBNull(14))
                            {
                                mc.NMSId = reader.GetInt32(14);
                            }
                            if (!reader.IsDBNull(15))
                            {
                                mc.Reserved = reader.GetBoolean(15);
                            }

                            mcList.Add(mc);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtEx = new CmtApplicationException(ex, "Selection of MultiCastGroups failed", "Error", (short)ExceptionLevelEnum.Error);
                _logController.LogApplicationException(cmtEx);
            }

            return mcList;
        }

        /// <summary>
        /// Helper method to get multicast groups registered to a specific customer
        /// </summary>
        /// <param name="distributorId">The ID of the customer organization</param>
        /// <returns>A list of MultiCastGroup elements. This list can be empty</returns>
        private List<MultiCastGroup> GetMultiCastGroupsByOrganization(int orgId)
        {
            string queryCmd = "SELECT IP, MacAddress, Name, Bytes, SourceURL, PortNr, Bandwidth, State, " +
                               "IspId, FirstActivationDate, UserId, OrgId, DistributorId, ID, NMSId, Reserved FROM MultiCastGroups " +
                               "WHERE Deleted = 0 AND OrgId = @OrgId";

            List<MultiCastGroup> mcList = new List<MultiCastGroup>();
            MultiCastGroup mc = null;

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(queryCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@DistributorId", orgId);
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            mc = new MultiCastGroup();
                            mc.IPAddress = reader.GetString(0);
                            mc.MacAddress = reader.GetString(1);
                            mc.Name = reader.GetString(2);
                            if (!reader.IsDBNull(3))
                            {
                                mc.Bytes = reader.GetInt64(3);
                            }
                            mc.sourceURL = reader.GetString(4);
                            mc.portNr = reader.GetInt32(5);
                            mc.BandWidth = reader.GetInt64(6);
                            mc.State = reader.GetBoolean(7);
                            if (!reader.IsDBNull(8))
                            {
                                mc.IspId = reader.GetInt32(8);
                            }
                            if (!reader.IsDBNull(9))
                            {
                                mc.FirstActivationDate = reader.GetDateTime(9);
                            }
                            if (!reader.IsDBNull(10))
                            {
                                mc.UserId = reader.GetGuid(10);
                            }
                            if (!reader.IsDBNull(11))
                            {
                                mc.OrganizationId = reader.GetInt32(11);
                            }
                            if (!reader.IsDBNull(12))
                            {
                                mc.DistributorId = reader.GetInt32(12);
                            }
                            mc.ID = reader.GetInt32(13);
                            if (!reader.IsDBNull(14))
                            {
                                mc.NMSId = reader.GetInt32(14);
                            }
                            if (!reader.IsDBNull(15))
                            {
                                mc.Reserved = reader.GetBoolean(15);
                            }

                            mcList.Add(mc);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtEx = new CmtApplicationException(ex, "Selection of MultiCastGroups failed", "Error", (short)ExceptionLevelEnum.Error);
                _logController.LogApplicationException(cmtEx);
            }

            return mcList;
        }

        #endregion



    }
}
