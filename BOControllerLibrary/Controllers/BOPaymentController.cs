﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BOControllerLibrary.Controllers.Interfaces;
using BOControllerLibrary.Model.Log;
using log4net;
using BOControllerLibrary.Controllers;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using BOControllerLibrary.Model;


namespace BOControllerLibrary.Controllers
{
    /// <summary>
    /// Contains the methods for processing a checkout
    /// In the first implementation, a distributor can choose an unreleased batch of vouchers
    /// to pay for
    /// The payment will be processed by an external provider, PayPal in the first case
    /// Payments are stored in the CMT database
    /// </summary>
    public class BOPaymentController : IBOPaymentController
    {
        string _connectionString = "";
        string _databaseProvider = "";

        private readonly ILog _log = LogManager.GetLogger(typeof(IBOAccountingController));
        private readonly IBOLogController _logController;
        
        public BOPaymentController(string connectionString, string databaseProvider)
        {
            _connectionString = connectionString;
            _databaseProvider = databaseProvider;
            _logController = new BOLogController(_connectionString, _databaseProvider);
        }
        
        /// <summary>
        /// Initiates the checkout procedure
        /// </summary>
        /// <param name="checkOutMethod">Determines the checkout method, in the first implemantation, only PayPal is available</param>
        /// <param name="action">The type of payment</param>
        /// <param name="distributorId">The ID of the paying distributor</param>
        /// <param name="amount">The amount to be paid</param>
        /// <param name="token">Paypal session token</param>
        public string CheckoutStart(string checkOutMethod, string action, int distributorId, decimal amount, string cur, ref string token)
        {
            string retMsg = "";
            // intermediate token variable
            string tokenTemp = token;


            try
            {
                if (checkOutMethod == "paypal")
                {
                    BOPaypalController payPalCaller = new BOPaypalController(_connectionString, _databaseProvider);
                    //URL on the CMT-server where the next step of Paypal procedure takes place
                    string returnURL = "http://cmt.satadsl.net/FOWeb-Prod/Distributors/PaypalCheckoutReview.aspx?Action=" + action;
                    //string returnURL = "http://localhost:64619/Distributors/PaypalCheckoutReview.aspx?Action=" + action;
                    //URL on the CMT-server in case the user decides to cancel the checkout procedure
                    string cancelURL = "http://cmt.satadsl.net/FOWeb-Prod/Distributors/CheckoutCancel.aspx";
                    //string cancelURL = "http://localhost:64619/Distributors/CheckoutCancel.aspx";

                    if (amount > 0)
                    {
                        
                        string amt = amount.ToString("0.00", System.Globalization.CultureInfo.InvariantCulture);

                        bool ret = payPalCaller.ShortcutExpressCheckout(amt, cur, ref tokenTemp, ref retMsg,action, returnURL, cancelURL);
                        if (!ret)
                        {
                            retMsg = "CheckoutError.aspx?" + retMsg;
                        }
                    }
                    else
                    {
                        retMsg = "CheckoutError.aspx?ErrorCode=AmtMissing";
                    }
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOPaymentController: Error while initiating new payment procedure", "Distributor ID: " + distributorId + action, (short)ExceptionLevelEnum.Error));
            }

            token = tokenTemp;
            return retMsg;
        }

        /// <summary>
        /// Cancels the checkout procedure
        /// This method has been replaced by a redirect
        /// </summary>
        public void CheckoutCancel()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Proceeds with the user review the Paypal checkout
        /// This user still has to confirm the payment after this step
        /// </summary>
        /// <param name="token">Paypal session token</param>
        /// <param name="payerId">Paypal payer ID</param>
        /// <returns>A URL, possibly containing an error message</returns>
        public string PaypalCheckoutProceed(string token, ref string payerId)
        {
            BOPaypalController payPalCaller = new BOPaypalController(_connectionString, _databaseProvider);

            string retMsg = "";
            string payerIdTemp = payerId;
            NVPCodec decoder = new NVPCodec();

            bool ret = payPalCaller.GetCheckoutDetails(token, ref payerIdTemp, ref decoder, ref retMsg);
            if (ret)
            {
                retMsg = "~/Distributors/PaypalCheckoutComplete.aspx";
            }
            else
            {
                retMsg = "CheckoutError.aspx?" + retMsg;
            }

            payerId = payerIdTemp;
            return retMsg;
        }

        /// <summary>
        /// Completes the Paypal checkout upon confirmation by the user
        /// </summary>
        /// <param name="amount">Final amount to be paid</param>
        /// <param name="payerId">Paypal payer ID</param>
        /// <param name="token">Paypal session token</param>
        /// <param name="batchId">The ID of the voucher batch paid for</param>
        /// <param name="distributorId">The ID of the paying distributor</param>
        /// <returns>1 if succesful, else an error message</returns>
        public string PaypalCheckoutComplete(decimal amount, string cur, string payerId, string token, string action, int distributorId)
        {
            BOPaypalController payPalCaller = new BOPaypalController(_connectionString, _databaseProvider);

            string retMsg = "";
            string finalPaymentAmount = amount.ToString("0.00", System.Globalization.CultureInfo.InvariantCulture);
            NVPCodec decoder = new NVPCodec();
            
            bool ret = payPalCaller.DoCheckoutPayment(finalPaymentAmount,cur,action, token, payerId, ref decoder, ref retMsg);
            if (ret)
            {
                // Retrieve PayPal confirmation value.

                retMsg = "1";
                string paymentConfirmation = decoder["PAYMENTINFO_0_TRANSACTIONID"].ToString();

                // Store the payment in the CMT database


                this.StorePayment(amount, action, distributorId, paymentConfirmation);
                    
                    
                    
                                       

                //}
                //else
                //{
                //    retMsg = "Storing payment failed";
                //}
            }
            else
            {
                retMsg = "CheckoutError.aspx?" + retMsg;
            }

            return retMsg;
        }

        /// <summary>
        /// Stores the payment in the CMT database
        /// </summary>
        /// <param name="amount">Amount paid</param>
        /// <param name="action">Payment action</param>
        /// <param name="distributorId">ID of paying distributor</param>
        /// <param name="paymentId">Unique payment ID</param>
        public bool StorePayment(decimal amount, string action, int distributorId, string paymentId)
        {
            // Write the data in the Payments table
            string insertCmd = "INSERT INTO Payments (PaymentID, DistributorID, Action, Amount, PaymentDateTime) " +
                               "VALUES (@PaymentID, @DistributorID, @Action, @Amount, @PaymentDateTime)";

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(insertCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@PaymentID", paymentId);
                        cmd.Parameters.AddWithValue("@DistributorID", distributorId);
                        cmd.Parameters.AddWithValue("@Action", action);
                        cmd.Parameters.AddWithValue("@Amount", amount);
                        cmd.Parameters.AddWithValue("@PaymentDateTime", DateTime.Now);
                        cmd.ExecuteNonQuery();
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOPaymentController: Error while storing new payment", "Distributor ID: " + distributorId + ", action: " + action + ", payment ID: " + paymentId, (short)ExceptionLevelEnum.Critical));

                return false;
            }
        }
    }
}
