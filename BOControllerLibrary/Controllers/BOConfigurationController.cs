﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using BOControllerLibrary.Controllers.Interfaces;
using BOControllerLibrary.Model;
using BOControllerLibrary.Model.Log;

namespace BOControllerLibrary.Controllers
{
    /// <summary>
    /// Contains methods which read and write values to the configuation table
    /// </summary>
    public class BOConfigurationController : IBOConfigurationController
    {
        BOLogController _boLogController = null;
        private string _connectionString = "";
        private string _databaseProvider = "";

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="connectionString">The CMT database connection string</param>
        public BOConfigurationController(string connectionString, string databaseProvider)
        {
            _connectionString = connectionString;
            _databaseProvider = databaseProvider;
            _boLogController = new BOLogController(_connectionString, _databaseProvider);
        }

        #region IBOConfigurationController Members

        /// <summary>
        /// Reads a value from the configuration table 
        /// </summary>
        /// <param name="paramName">The parameter name to read</param>
        /// <returns>The parameter value or null if this parameter does not exist</returns>
        public string getParameter(string paramName)
        {
            string value = null;
            string queryString = "SELECT Value FROM Configuration WHERE ParamName = @ParamName";

            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                conn.Open();

                using (SqlCommand cmd = new SqlCommand(queryString, conn))
                {
                    cmd.Parameters.AddWithValue("@ParamName", paramName);
                    SqlDataReader reader = cmd.ExecuteReader();

                    if (reader.Read())
                    {
                        value = reader.GetString(0);
                    }
                }
            }

            return value;
        }

        /// <summary>
        /// Sets a parameter value. The parameter is created if it does not
        /// exist in the configuration table yet.
        /// </summary>
        /// <param name="paramName">The parameter name</param>
        /// <param name="value">The parameter value</param>
        /// <returns>True if the parameter update succeeded</returns>
        public Boolean setParameter(string paramName, string value)
        {
            Boolean success = false;
            var var1 = new StringBuilder();
            var1.Append("UPDATE Configuration \n");
            var1.Append("SET Value = @Value \n");
            var1.Append("WHERE ParamName = @ParamName");

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    //using (SqlCommand cmd = new SqlCommand("cmtUpdateParameter", conn))
                    using (SqlCommand cmd = new SqlCommand(var1.ToString(), conn))
                    {
                        cmd.Parameters.AddWithValue("@ParamName", paramName);
                        cmd.Parameters.AddWithValue("@Value", value);
                        //cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        int rtn = cmd.ExecuteNonQuery();

                        if (rtn != 0)
                        {
                            success = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtApplicationException  = new CmtApplicationException();
                cmtApplicationException.ExceptionDateTime = DateTime.Now;
                cmtApplicationException.ExceptionDesc = ex.Message;
                cmtApplicationException.ExceptionStacktrace = ex.StackTrace;
                cmtApplicationException.ExceptionLevel = (short)ExceptionLevelEnum.Error;
                _boLogController.LogApplicationException(cmtApplicationException);
            }

            return success;
        }

        #endregion
    }
}
