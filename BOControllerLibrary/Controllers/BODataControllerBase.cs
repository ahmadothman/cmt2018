﻿using System.Data;
using System.Data.SqlClient;
using BOControllerLibrary.Controllers.Interfaces;

namespace BOControllerLibrary.Controllers
{
    /// <summary>
    ///   This is just base class for some database connection info.
    /// </summary>
    public abstract class BODataControllerBase : IBODataControllerBase 
    {
        /// <summary>
        ///   The connection string that will be used in the library project
        /// </summary>
        public string ConnectionString { get; set; }

        /// <summary>
        ///   The dataprovider for the connection string
        /// </summary>
        public string DataProvider { get; set; }

        public IDbConnection CreateConnection()
        {
            IDbConnection conn = new SqlConnection(ConnectionString);
            conn.Open();
            return conn;
        }
    }
}
