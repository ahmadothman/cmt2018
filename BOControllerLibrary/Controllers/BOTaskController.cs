﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using BOControllerLibrary.Controllers.Interfaces;
using BOControllerLibrary.Model;
using BOControllerLibrary.Model.Tasks;
using BOControllerLibrary.Model.Log;
using PetaPoco;
using log4net;

namespace BOControllerLibrary.Controllers
{
    public class BOTaskController : BODataControllerBase, IBOTaskController
    {
        private readonly ILog _log = LogManager.GetLogger(typeof(BOTaskController));
        private readonly IBOLogController _logController;

        public BOTaskController(string connectionString, string databaseProvider)
        {
            ConnectionString = connectionString;
            DataProvider = databaseProvider;
            _logController = new BOLogController(connectionString, databaseProvider);
        }

        /// <summary>
        /// Return a list of completed tasks, in descending order
        /// </summary>
        /// <returns></returns>
        public List<Task> GetCompletedTasks()
        {
            return GetTasks(true);
        }

        /// <summary>
        /// Return a list of open tasks, in descending order
        /// </summary>
        /// <returns></returns>
        public List<Task> GetOpenTasks()
        {
            return GetTasks(false);
        }

        /// <summary>
        /// Insert or update a task
        /// </summary>
        /// <param name="task"></param>
        /// <returns></returns>
        public bool UpdateTask(Task task)
        {
            try
            {
                const string sql =
                    @"
                    EXECUTE [cmtUpdateTasks] 
                       @TaskId
                      ,@Subject
                      ,@UserId
                      ,@DateCreated
                      ,@DateDue
                      ,@DateCompleted
                      ,@MacAddress
                      ,@TaskType
                      ,@DistributorId
                      ,@Completed
                      ,@returnValue OUTPUT
                    ";

                _log.Debug("UpdateTask:start");
                using (var db = new Database(ConnectionString, DataProvider))
                {
                    var returnValue = new SqlParameter("returnValue", SqlDbType.Int);
                    returnValue.Direction = ParameterDirection.Output;
                    db.Execute(sql,
                                              new
                                              {
                                                  TaskId = task.Id,
                                                  task.Subject,
                                                  task.UserId,
                                                  task.DateCreated,
                                                  task.DateDue,
                                                  task.DateCompleted,
                                                  task.MacAddress,
                                                  TaskType = task.Type.Id,
                                                  task.DistributorId,
                                                  task.Completed,
                                                  returnValue
                                              }
                        );
                    int spResult = (int)returnValue.Value;
                    _log.Debug("Called cmtUpdateTasks and returned " + spResult);
                    return spResult == 0 || spResult == 1;
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOAccountingController: Error while executing UpdateTask", task.ToString(), (short)ExceptionLevelEnum.Error));
                throw;
            }
        }

        /// <summary>
        /// Get a list of all tasktypes
        /// </summary>
        /// <returns></returns>
        public List<TaskType> GetTaskTypes()
        {
            try
            {
                const string sql =
                    @"
                        select *
                        from TaskTypes
                    ";

                using (var db = new Database(ConnectionString, DataProvider))
                {
                    var dbObj = db.Fetch<TaskType>(sql);
                    return dbObj.ToList();
                }
            }
            catch (Exception ex)
            {
                _log.Error("Error during GetTaskTypes", ex);
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOAccountingController: Error while executing GetTaskTypes", ""));
                throw;
            }
        }

        /// <summary>
        /// Returns a task by its task identifier
        /// </summary>
        /// <remarks>
        /// Rewrite this method to match the other task methods
        /// </remarks>
        /// <param name="taskId">The unique task identifier</param>
        /// <returns>A task object or null if the task could not be found</returns>
        public Task GetTask(int taskId)
        {
            string query = "SELECT TaskId, Subject, UserId, DateCreated, " +
                    "DateDue, DateCompleted, MacAddress, TaskType, DistributorId, Completed " +
                    "FROM Tasks WHERE TaskId = @TaskId";
            Task task = null;

            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                try
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {
                        cmd.Parameters.AddWithValue("@TaskId", taskId);
                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            task = new Task();
                            task.Id = reader.GetInt32(0);
                            task.Subject = reader.GetString(1);
                            task.UserId = reader.GetGuid(2);

                            if (!reader.IsDBNull(3))
                            {
                                task.DateCreated = reader.GetDateTime(3);
                            }

                            if (!reader.IsDBNull(4))
                            {
                                task.DateDue = reader.GetDateTime(4);
                            }

                            if (!reader.IsDBNull(5))
                            {
                                task.DateCompleted = reader.GetDateTime(5);
                            }

                            if (!reader.IsDBNull(6))
                            {
                                task.MacAddress = reader.GetString(6);
                            }

                            task.Type = this.GetTaskType(reader.GetInt32(7));
                            task.DistributorId = reader.GetInt32(8);
                            task.Completed = reader.GetBoolean(9);

                        }
                    }
                }
                catch (Exception ex)
                {
                    CmtApplicationException cmtException = new CmtApplicationException();
                    cmtException.ExceptionDesc = ex.Message;
                    cmtException.ExceptionStacktrace= ex.StackTrace;
                    cmtException.ExceptionDateTime = DateTime.Now;
                    cmtException.UserDescription = "GetTask method failed";
                    _logController.LogApplicationException(cmtException);
                }
            }

            return task;
        }

        /// <summary>
        /// Returns the TaskType corresponding with the given Id
        /// </summary>
        /// <param name="taskTypeId"></param>
        /// <returns>A Task object or null otherwise</returns>
        public TaskType GetTaskType(int taskTypeId)
        {
            string query = "SELECT Id, Type, DistInfo FROM TaskTypes WHERE Id = @TypeId";
            TaskType taskType = null;

            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                try
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {
                        cmd.Parameters.AddWithValue("@TypeId", taskTypeId);
                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            taskType = new TaskType();
                            taskType.Id = reader.GetInt32(0);
                            taskType.Type = reader.GetString(1);
                            taskType.DistInfo = reader.GetBoolean(2);
                        }
                    }
                }
                catch (Exception ex)
                {
                    CmtApplicationException cmtException = new CmtApplicationException();
                    cmtException.ExceptionDesc = ex.Message;
                    cmtException.ExceptionDateTime = DateTime.Now;
                    cmtException.ExceptionStacktrace = ex.StackTrace;
                    cmtException.UserDescription = "GetTaskType method failed";
                    _logController.LogApplicationException(cmtException);
                }
            }

            return taskType;
        }
        
        /// <summary>
        /// Insert or update a new tasktype
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public bool UpdateTaskType(TaskType type)
        {
            try
            {
                const string sql =
                    @"
                        EXECUTE [cmtUpdateTaskTypes] 
                           @Id
                          ,@Type
                          ,@DistInfo
                          ,@returnValue OUTPUT
                    ";

                _log.Debug("UpdateTaskTypes:start");
                using (var db = new Database(ConnectionString, DataProvider))
                {
                    var returnValue = new SqlParameter("returnValue", SqlDbType.Int) { Direction = ParameterDirection.Output };
                    db.Execute(sql,
                                              new
                                              {
                                                  type.Id,
                                                  type.Type,
                                                  type.DistInfo,
                                                  returnValue
                                              }
                        );
                    int spResult = (int)returnValue.Value;
                    _log.Debug("Called cmtUpdateTaskTypes and returned " + spResult);
                    return spResult == 0 || spResult == 1;
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOAccountingController: Error while executing UpdateTaskTypes", type.ToString(), (short)ExceptionLevelEnum.Error));
                throw;
            }
        }


        #region Helpers
        public List<Task> GetTasks(bool completed)
        {
            try
            {
                const string sql =
                    @"
                        select t.TaskId [Id], t.Subject, t.UserId, t.DateCreated, t.DateDue, t.DateCompleted, t.MacAddress, t.TaskType, t.DistributorId, t.Completed, tt.*
                        from tasks t
                        join tasktypes tt on t.tasktype = tt.id
                        where Completed = @completed
                        order by t.DateCreated desc
                    ";

                using (var db = new Database(ConnectionString, DataProvider))
                {
                    var dbObj = db.Fetch<Task, TaskType, Task>(
                        (i, a) =>
                        {
                            i.Type = a;
                            return i;
                        },
                        sql, new { completed });
                    return dbObj.ToList();
                }
            }
            catch (Exception ex)
            {
                _log.Error("Error during GetCompletedTasks", ex);
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOAccountingController: Error while executing GetCompletedTasks", ""));
                throw;
            }
        }
        #endregion
    }
}
