﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Drawing;
using BOControllerLibrary.Controllers.Interfaces;
using BOControllerLibrary.Model;
using BOControllerLibrary.Model.Log;
using BOControllerLibrary.Model.Distributor;
using BOControllerLibrary.Model.ISP;
using BOControllerLibrary.Model.Organization;
using BOControllerLibrary.Model.Terminal;
using BOControllerLibrary.Model.User;
using BOControllerLibrary.Model.VNO;
using net.nimera.supportlibrary;
using PetaPoco;
using log4net;
using BOControllerLibrary.NMSGatewayServiceRef;
using CassandraLib.Model.DataModel;
using CassandraLib.Model;
using System.Configuration;

namespace BOControllerLibrary.Controllers
{
    /// <summary>
    ///  The BOAccounting Controller contains the logic 'user/terminal and general accounting' procedures.
    /// </summary>
    public class BOAccountingController : BODataControllerBase, IBOAccountingController
    {
        private readonly ILog _log = LogManager.GetLogger(typeof (IBOAccountingController));
        private readonly IBOLogController _logController;
        private const bool _debugFlag = true;
        private CassandraClient _casClient;
        private const int casTimeWindow = 60;  //using a window of 60 minutes when retrieving last entry to ensure data has been registered
        private readonly NMSGatewayService _nmsGatewayService;

        public BOAccountingController(string connectionString, string databaseProvider)
        {
            ConnectionString = connectionString;
            DataProvider = databaseProvider;
            _logController = new BOLogController(connectionString, databaseProvider);
            _nmsGatewayService = new NMSGatewayService();
            //_casClient = new CassandraClient("SatADSL");
        }

        #region Implementation of IBOAccountingController

        /// <summary>
        ///   Returns a list of terminals which are serviced by a Distributor. The UserId in this case is taken from the aspnet_Users table.
        /// </summary>
        /// <param name = "distributorUserId"></param>
        /// <returns></returns>
        public List<Terminal> GetTerminalsByDistributor(string distributorUserId)
        {
            try
            {
                const string sql = @"
                                SELECT t.*,  a.*  
                                FROM   VW_Terminals19 t 
                                JOIN addresses a ON a.id = t.addressid 
                                JOIN distributorsusers du ON du.distributorid = t.distributorid 
                                WHERE  du.userid = @distributorUserId 
                                ";

                return GetTerminalListForQuery(sql, new {distributorUserId}).ToList();
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: Error while executing GetTerminalsByDistributor",
                                                                                   distributorUserId));
                throw;
            }
        }

        /// <summary>
        /// CMT-95
        ///   Returns a list of terminals which are serviced by a Distributor without decommisioned or deleted Terminals. The UserId in this case is taken from the aspnet_Users table.
        /// </summary>
        /// <param name = "distributorUserId"></param>
        /// <returns></returns>
        public List<Terminal> GetTerminalsByDistributorForDistributor(string distributorUserId)
        {
            try
            {
                const string sql = @"
                                SELECT t.*,  a.*  
                                FROM   VW_Terminals19 t 
                                JOIN addresses a ON a.id = t.addressid 
                                JOIN distributorsusers du ON du.distributorid = t.distributorid 
                                WHERE  du.userid = @distributorUserId and t.AdmStatus in (1,2,5)
                                ";

                return GetTerminalListForQuery(sql, new { distributorUserId }).ToList();
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: Error while executing GetTerminalsByDistributor",
                                                                                   distributorUserId));
                throw;
            }
        }

        /// <summary>
        ///   Returns a list of terminals which are serviced by a Distributor. The distributorid is taken from the distributors table.
        /// </summary>
        /// <param name = "distributorId"></param>
        /// <returns></returns>
        public List<Terminal> GetTerminalsByDistributorId(int distributorId)
        {
            try
            {
                var var1 = new StringBuilder();
                var1.Append("SELECT t.*, \n");
                var1.Append("       a.* \n");
                var1.Append("FROM   VW_Terminals19 t \n");
                var1.Append("       JOIN addresses a \n");
                var1.Append("         ON a.id = t.addressid \n");
                var1.Append("WHERE  t.DistributorId = @distributorId ");

                return GetTerminalListForQuery(var1.ToString(), new {distributorId}).ToList();
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: Error while executing GetTerminalsByDistributorId",
                                                                                   distributorId.ToString(
                                                                                       CultureInfo.InvariantCulture)));
                throw;
            }
        }

        /// <summary>
        ///   Returns a list of organizations
        /// </summary>
        /// <param name = "distributorId">The UID of the distributor</param>
        /// <returns></returns>
        public List<Organization> GetOrganisationsByDistributor(string distributorId)
        {
            try
            {
                const string sql = @"
                            select o.*, a.*, d.* 
                            from vw_organizations3 o 
                            join addresses a on a.Id = o.Addressid
                            join vw_distributorsAndVNOs7 d on d.id = o.distributorid 
                            join distributorsusers du on du.distributorid = d.id 
                            where du.userid = @distributorId
                    ";
                using (var db = new Database(ConnectionString, DataProvider))
                {
                    List<Organization> dbObj = db.Fetch<Organization, Address, Distributor, Organization>(
                        (o, a, d) =>
                            {
                                o.Address = a;
                                o.Distributor = d;
                                return o;
                            },
                        sql, new {distributorId});
                    return dbObj.ToList();
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: Error while executing GetOrganizationsByDistributor",
                                                                                   distributorId));
                throw;
            }
        }

        /// <summary>
        /// Returns only customers that are customer VNOs
        /// </summary>
        /// <param name="distributorId">The UID of the distributor</param>
        /// <returns>A list of organizations</returns>
        public List<Organization> GetCustomerVNOsByDistributor(string distributorId)
        {
            try
            {
                const string sql = @"
                            select o.*, a.*, d.* 
                            from vw_customerVNOs2 o 
                            join addresses a on a.Id = o.Addressid
                            join vw_distributorsAndVNOs7 d on d.id = o.distributorid 
                            join distributorsusers du on du.distributorid = d.id 
                            where du.userid = @distributorId
                    ";
                using (var db = new Database(ConnectionString, DataProvider))
                {
                    List<Organization> dbObj = db.Fetch<Organization, Address, Distributor, Organization>(
                        (o, a, d) =>
                        {
                            o.Address = a;
                            o.Distributor = d;
                            return o;
                        },
                        sql, new { distributorId });
                    return dbObj.ToList();
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: Error while executing GetOrganizationsByDistributor",
                                                                                   distributorId));
                throw;
            }
        }

        /// <summary>
        /// Returns only customers that are customer VNOs
        /// </summary>
        /// <returns>A list of organizations</returns>
        public List<Organization> GetCustomerVNOs()
        {
            try
            {
                const string sql = @"
                            select o.*, a.*, d.* 
                            from vw_customerVNOs2 o 
                            join addresses a on a.Id = o.Addressid
                            join vw_distributorsAndVNOs3 d on d.id = o.distributorid 
                            ";

                using (var db = new Database(ConnectionString, DataProvider))
                {
                    List<Organization> dbObj = db.Fetch<Organization, Address, Distributor, Organization>(
                        (o, a, d) =>
                        {
                            o.Address = a;
                            o.Distributor = d;
                            return o;
                        },
                        sql);
                    return dbObj.ToList();
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: Error while executing GetCustomerVNOs",ex.Message));
                throw;
            }
        }

        /// <summary>
        /// Fetches the list of organizations for a specific distributor
        /// </summary>
        /// <param name = "distributorId">The ID of the distributor</param>
        /// <returns>A list of organizations</returns>
        public List<Organization> GetOrganisationsByDistributorId(string distributorId)
        {
            try
            {
                //this code appears not to work as expected, hence the re-write

//                const string sql = @"
//                            select o.*, a.*, d.* 
//                            from vw_organizations o 
//                            join addresses a on a.Id = o.Addressid
//                            join vw_distributorsAndVNOs3 d on d.id = o.distributorid 
//                            join distributorsusers du on du.distributorid = d.id 
//                            where o.distributorid = @distributorId
//                    ";
//                using (var db = new Database(ConnectionString, DataProvider))
//                {
//                    List<Organization> dbObj = db.Fetch<Organization, Address, Distributor, Organization>(
//                        (o, a, d) =>
//                        {
//                            o.Address = a;
//                            o.Distributor = d;
//                            return o;
//                        },
//                        sql, new { distributorId });
//                    return dbObj.ToList();
//                }
                List<Organization> organizations = new List<Organization>();
                string getCmd = "SELECT Id, FullName FROM Organizations " +
                                "WHERE DistributorId = @DistributorId AND (Deleted = 0 OR Deleted IS NULL)";
                Organization org;
                using (SqlConnection con = new SqlConnection(ConnectionString))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(getCmd, con))
                    {
                        cmd.Parameters.AddWithValue("@DistributorId", distributorId);
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            org = new Organization();
                            org.Id = reader.GetInt32(0);
                            org.FullName = reader.GetString(1);
                            organizations.Add(org);
                        }
                    }
                }
                return organizations;
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: Error while executing GetOrganizationsByDistributor",
                                                                                   distributorId));
                throw;
            }
        }
                
        /// <summary>
        ///   Returns the terminal operated by a single End User
        /// </summary>
        /// <param name = "endUserId"></param>
        /// <returns></returns>
        public Terminal GetUserTerminal(int endUserId)
        {
            try
            {
                const string sql =
                    "SELECT * " +
                    "FROM VW_Terminals19 t " +
                    "join addresses a on a.id = t.addressid " +
                    "where t.enduserid = @endUserId";

                return GetTerminalForQuery(sql, new {endUserId});
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: Error while executing GetUserTerminal",
                                                                                   endUserId.ToString(
                                                                                       CultureInfo.InvariantCulture)));
                throw;
            }
        }

        /// <summary>
        ///   Returns the distributor which is linked to the user
        /// </summary>
        /// <param name = "userid">The userid here is the UID</param>
        /// <returns></returns>
        public Distributor GetDistributorForUser(string userid)
        {
            try
            {
                const string sql = "select d.*, a.* " +
                                   "from vw_distributorsAndVNOs7 d " +
                                   "join addresses a on a.id = d.addressid " +
                                   "join distributorsusers du on du.distributorid = d.id " +
                                   "where du.userid = @userid";
                using (var db = new Database(ConnectionString, DataProvider))
                {
                    List<Distributor> dbObj = db.Fetch<Distributor, Address, Distributor>(
                        (i, a) =>
                            {
                                i.Address = a;
                                return i;
                            },
                        sql, new {userid});
                    return dbObj.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: Error while executing GetDistributorForUser",
                                                                                   userid));
                throw;
            }
        }

        /// <summary>
        ///   Returns a list of terminals which are linked to the organization.
        /// </summary>
        /// <param name = "organizationId"></param>
        /// <returns></returns>
        public List<Terminal> GetTerminalsByOrganization(int organizationId)
        {
            try
            {
                const string sql =
                    "SELECT t.*, a.* " +
                    "from VW_Terminals19 t " +
                    "join addresses a on a.id = t.addressid " +
                    "join vw_organizations3 o on o.id = t.orgid " +
                    "where o.Id = @organizationId";

                return GetTerminalListForQuery(sql, new {organizationId}).ToList();
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: Error while executing GetTerminalsByOrganization",
                                                                                   organizationId.ToString(
                                                                                       CultureInfo.InvariantCulture)));
                throw;
            }
        }

        /// <summary>
        /// Returns a list of terminals which are linked to an SLA.
        /// </summary>
        /// <param name="slaId">The SLA identifier for which</param>
        /// <returns>A list of terminals with that specific SLA</returns>
        public List<Terminal> GetTerminalsBySla(int slaId)
        {
            try
            {
                const string sql =
                    "SELECT t.*, a.* " +
                    "FROM VW_Terminals19 t " +
                    "INNER JOIN Addresses a on a.Id = t.AddressId " +
                    "WHERE t.SlaID = @slaId";

                return GetTerminalListForQuery(sql, new { slaId }).ToList();
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: Error while executing GetTerminalsBySla",
                                                                                   slaId.ToString(
                                                                                       CultureInfo.InvariantCulture)));
                throw;
            }
        }

        /// <summary>
        ///   Returns the details of a terminal by its SitId value. These are the values which are stored in the CMT Database
        /// 
        ///   developer notes: it's possible that we will need to use a ITerminalDetails object here instead of the ITerminal to reduce amount of traffic
        /// </summary>
        /// <param name = "sitId"></param>
        /// <param name="ispId"> </param>
        /// <returns></returns>
        public Terminal GetTerminalDetailsBySitId(int sitId, int ispId)
        {
            try
            {
                const string sql = @"
                    SELECT t.*, a.* 
                    FROM VW_Terminals19 t 
                    JOIN addresses a on a.id = t.addressid 
                    WHERE sitid = @sitId 
                    AND IspId = @ispId
                    ";

                return GetTerminalForQuery(sql, new {sitId, ispId});
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: Error while executing GetTerminalDetailsBySitId",
                                                                                   sitId.ToString(
                                                                                       CultureInfo.InvariantCulture)));
                throw;
            }
        }

        /// <summary>
        ///   Returns the terminal details by its IP address
        /// 
        ///   developer notes: it's possible that we will need to use a ITerminalDetails object here instead of the ITerminal to reduce amount of traffic
        /// </summary>
        /// <param name = "ipAddress"></param>
        /// <returns></returns>
        public Terminal GetTerminalDetailsByIp(string ipAddress)
        {
            try
            {
                const string sql =
                    "SELECT t.*, a.*  " +
                    "FROM VW_Terminals19 t " +
                    "join addresses a on a.id = t.addressid " +
                    "where t.IPAddress = @ipAddress";

                return GetTerminalForQuery(sql, new {ipAddress});
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: Error while executing GetTerminalDetailsByIp",
                                                                                   ipAddress));
                throw;
            }
        }

        /// <summary>
        ///   Returns the terminal details by the terminal MAC address
        /// 
        ///   developer notes: it's possible that we will need to use a ITerminalDetails object here instead of the ITerminal to reduce amount of traffic
        /// </summary>
        /// <param name = "macAddress"></param>
        /// <returns></returns>
        public Terminal GetTerminalDetailsByMAC(string macAddress)
        {
            try
            {
                const string sql =
                    "SELECT * " +
                    "FROM VW_Terminals19 t " +
                    "join addresses a on a.id = t.addressid " +
                    "where t.macaddress = @macAddress";

                return GetTerminalForQuery(sql, new {macAddress});
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: Error while executing GetTerminalDetailsByMac",
                                                                                   macAddress));
                throw;
            }
        }

        /// <summary>
        /// Returns a list of terminal matching a complete or partly given MAC address.
        /// </summary>
        /// <param name="macAddress">A complete or partly matching mac address</param>
        /// <returns>A list with matching terminals, this list can be empty!</returns>
        public List<Terminal> GetTerminalsByMac(string macAddress)
        {
            const string queryCmd =
                "SELECT  MacAddress, StartDate, ExpiryDate, EndUserId, SlaID, AdmStatus, SitId, FullName, IPAddress, IspId, ExtFullName, IPMask, IPRange, FreeZone " +
                "FROM Terminals WHERE MacAddress LIKE @macAddress";

            var resultList = new List<Terminal>();

            try
            {
                using (var con = new SqlConnection(ConnectionString))
                {
                    con.Open();

                    using (var cmd = new SqlCommand(queryCmd, con))
                    {
                        macAddress = "%" + macAddress + "%";
                        cmd.Parameters.AddWithValue("@macAddress", macAddress);
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            var t = new Terminal();
                            if (!reader.IsDBNull(0))
                            {
                                t.MacAddress = reader.GetString(0).Trim();
                            }

                            if (!reader.IsDBNull(1))
                            {
                                t.StartDate = reader.GetDateTime(1);
                            }

                            if (!reader.IsDBNull(2))
                            {
                                t.ExpiryDate = reader.GetDateTime(2);
                            }

                            if (!reader.IsDBNull(3))
                            {
                                t.EndUserId = reader.GetInt32(3);
                            }

                            if (!reader.IsDBNull(4))
                            {
                                t.SlaId = reader.GetInt32(4);
                            }

                            if (!reader.IsDBNull(5))
                            {
                                t.AdmStatus = reader.GetInt32(5);
                            }

                            if (!reader.IsDBNull(6))
                            {
                                t.SitId = reader.GetInt32(6);
                            }

                            if (!reader.IsDBNull(7))
                            {
                                t.FullName = reader.GetString(7).Trim();
                            }

                            if (!reader.IsDBNull(8))
                            {
                                t.IpAddress = reader.GetString(8).Trim();
                            }

                            if (!reader.IsDBNull(9))
                            {
                                t.IspId = reader.GetInt32(9);
                            }

                            if (!reader.IsDBNull(10))
                            {
                                t.ExtFullName = reader.GetString(10).Trim();
                            }

                            if (!reader.IsDBNull(11))
                            {
                                t.IPMask = reader.GetInt32(11);
                            }

                            if (!reader.IsDBNull(12))
                            {
                                t.IPRange = reader.GetBoolean(12);
                            }
                            else
                            {
                                t.IPRange = false;
                            }

                            if (!reader.IsDBNull(13))
                            {
                                t.FreeZone = reader.GetInt32(13);
                            }
                            else
                            {
                                t.FreeZone = 0;
                            }
                            resultList.Add(t);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Log an exception
                var boLogController = new BOLogController(ConnectionString, DataProvider);
                var cmtException = new CmtApplicationException
                                       {
                                           ExceptionDateTime = DateTime.Now,
                                           ExceptionDesc = ex.Message,
                                           ExceptionStacktrace = ex.StackTrace,
                                           UserDescription = "GetTerminalsByMac method failed"
                                       };
                boLogController.LogApplicationException(cmtException);
            }

            return resultList;
        }

        /// <summary>
        /// Returns a terminal list corresponding 
        /// </summary>
        /// <param name="statusId">The status identifier</param>
        /// <returns>A list of matching terminals</returns>
        public List<Terminal> GetTerminalsByStatus(int statusId)
        {
            const string queryCmd = "SELECT  MacAddress, StartDate, ExpiryDate, EndUserId, SlaID, AdmStatus, SitId, FullName, IPAddress, IspId, ExtFullName, IPMask, IPRange, FreeZone " +
                                    "FROM Terminals WHERE AdmStatus = @statusId";

            var resultList = new List<Terminal>();

            try
            {
                using (var con = new SqlConnection(ConnectionString))
                {
                    con.Open();

                    using (var cmd = new SqlCommand(queryCmd, con))
                    {
                        cmd.Parameters.AddWithValue("@statusId", statusId);
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            var t = new Terminal();
                            if (!reader.IsDBNull(0))
                            {
                                t.MacAddress = reader.GetString(0);
                            }

                            if (!reader.IsDBNull(1))
                            {
                                t.StartDate = reader.GetDateTime(1);
                            }

                            if (!reader.IsDBNull(2))
                            {
                                t.ExpiryDate = reader.GetDateTime(2);
                            }

                            if (!reader.IsDBNull(3))
                            {
                                t.EndUserId = reader.GetInt32(3);
                            }

                            if (!reader.IsDBNull(4))
                            {
                                t.SlaId = reader.GetInt32(4);
                            }

                            if (!reader.IsDBNull(5))
                            {
                                t.AdmStatus = reader.GetInt32(5);
                            }

                            if (!reader.IsDBNull(6))
                            {
                                t.SitId = reader.GetInt32(6);
                            }

                            if (!reader.IsDBNull(7))
                            {
                                t.FullName = reader.GetString(7);
                            }

                            if (!reader.IsDBNull(8))
                            {
                                t.IpAddress = reader.GetString(8);
                            }

                            if (!reader.IsDBNull(9))
                            {
                                t.IspId = reader.GetInt32(9);
                            }

                            if (!reader.IsDBNull(10))
                            {
                                t.ExtFullName = reader.GetString(10);
                            }

                            if (!reader.IsDBNull(11))
                            {
                                t.IPMask = reader.GetInt32(11);
                            }

                            if (!reader.IsDBNull(12))
                            {
                                t.IPRange = reader.GetBoolean(12);
                            }
                            else
                            {
                                t.IPRange = false;
                            }

                            if (!reader.IsDBNull(13))
                            {
                                t.FreeZone = reader.GetInt32(13);
                            }
                            else
                            {
                                t.FreeZone = 0;
                            }
                            resultList.Add(t);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Log an exception
                var boLogController = new BOLogController(ConnectionString, DataProvider);
                var cmtException = new CmtApplicationException
                                       {
                                           ExceptionDateTime = DateTime.Now,
                                           ExceptionDesc = ex.Message,
                                           ExceptionStacktrace = ex.StackTrace,
                                           UserDescription = "GetTerminalsByStatus method failed"
                                       };
                boLogController.LogApplicationException(cmtException);
            }

            return resultList;
        }

        /// <summary>
        /// Returns a list of terminals corresponding to the given criterion.
        /// </summary>
        /// <param name="dt">The matching DateTime value</param>
        /// <param name="criterion">Determines the relation of the terminals ExpiryDate to the
        /// given date.
        /// <ul>
        ///     <li>Equals</li>
        ///     <li>Less then (Earlier)</li>
        ///     <li>More then (Later)</li>
        ///     <li>Less or Equal</li>
        ///     <li>More or Equal</li>
        /// </ul>
        /// </param>
        /// <returns>A list of matching terminals</returns>
        public List<Terminal> GetTerminalsByExpiryDate(DateTime dt, string criterion)
        {
            string queryCmd =
                "SELECT  MacAddress, StartDate, ExpiryDate, EndUserId, SlaID, AdmStatus, SitId, FullName, IPAddress, IspId, ExtFullName, IPMask, IPRange, FreeZone " +
                "FROM Terminals WHERE ExpiryDate " + criterion + " @expiryDate";

            var resultList = new List<Terminal>();

            try
            {
                using (var con = new SqlConnection(ConnectionString))
                {
                    con.Open();

                    using (var cmd = new SqlCommand(queryCmd, con))
                    {
                        cmd.Parameters.AddWithValue("@expiryDate", dt);
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            var t = new Terminal();
                            if (!reader.IsDBNull(0))
                            {
                                t.MacAddress = reader.GetString(0);
                            }

                            if (!reader.IsDBNull(1))
                            {
                                t.StartDate = reader.GetDateTime(1);
                            }

                            if (!reader.IsDBNull(2))
                            {
                                t.ExpiryDate = reader.GetDateTime(2);
                            }

                            if (!reader.IsDBNull(3))
                            {
                                t.EndUserId = reader.GetInt32(3);
                            }

                            if (!reader.IsDBNull(4))
                            {
                                t.SlaId = reader.GetInt32(4);
                            }

                            if (!reader.IsDBNull(5))
                            {
                                t.AdmStatus = reader.GetInt32(5);
                            }

                            if (!reader.IsDBNull(6))
                            {
                                t.SitId = reader.GetInt32(6);
                            }

                            if (!reader.IsDBNull(7))
                            {
                                t.FullName = reader.GetString(7);
                            }

                            if (!reader.IsDBNull(8))
                            {
                                t.IpAddress = reader.GetString(8);
                            }

                            if (!reader.IsDBNull(9))
                            {
                                t.IspId = reader.GetInt32(9);
                            }

                            if (!reader.IsDBNull(10))
                            {
                                t.ExtFullName = reader.GetString(10);
                            }

                            if (!reader.IsDBNull(11))
                            {
                                t.IPMask = reader.GetInt32(11);
                            }

                            if (!reader.IsDBNull(12))
                            {
                                t.IPRange = reader.GetBoolean(12);
                            }
                            else
                            {
                                t.IPRange = false;
                            }

                            if (!reader.IsDBNull(13))
                            {
                                t.FreeZone = reader.GetInt32(13);
                            }
                            else
                            {
                                t.FreeZone = 0;
                            }
                            resultList.Add(t);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Log an exception
                var boLogController = new BOLogController(ConnectionString, DataProvider);
                var cmtException = new CmtApplicationException
                                       {
                                           ExceptionDateTime = DateTime.Now,
                                           ExceptionDesc = ex.Message,
                                           ExceptionStacktrace = ex.StackTrace,
                                           UserDescription = "GetTerminalsByStatus method failed"
                                       };
                boLogController.LogApplicationException(cmtException);
            }

            return resultList;
        }

        /// <summary>
        /// Returns a list of terminal requests
        /// </summary>
        /// <returns>A list of terminal requests, this list can be empty or null</returns>
        public List<Terminal> GetTerminalRequests()
        {
            List<Terminal> reqTerminals = new List<Terminal>();
            //try
            //{
            //    int statusId = GetTerminalStatusId("Request");
            //    reqTerminalsFull = GetTerminalsByStatus(statusId);
            //}
            //catch (Exception ex)
            //{
            //    _logController.LogApplicationException(new CmtApplicationException(ex,
            //                                                                       "BOAccountingController: Error while executing GetTerminalRequests",
            //                                                                       "", (short)ExceptionLevelEnum.Error));
            //}

            //new version to filter out secondary terminals in a redundant setup
            const string queryCmd = "SELECT  MacAddress, StartDate, ExpiryDate, EndUserId, SlaID, AdmStatus, SitId, FullName, IPAddress, IspId, ExtFullName, IPMask, IPRange, FreeZone " +
                                    "FROM Terminals where admstatus = 5 and (RedundantSetup is null or (RedundantSetup = 1 and PrimaryTerminal = 1))";
            try
            {
                using (var con = new SqlConnection(ConnectionString))
                {
                    con.Open();

                    using (var cmd = new SqlCommand(queryCmd, con))
                    {
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            var t = new Terminal();
                            if (!reader.IsDBNull(0))
                            {
                                t.MacAddress = reader.GetString(0);
                            }

                            if (!reader.IsDBNull(1))
                            {
                                t.StartDate = reader.GetDateTime(1);
                            }

                            if (!reader.IsDBNull(2))
                            {
                                t.ExpiryDate = reader.GetDateTime(2);
                            }

                            if (!reader.IsDBNull(3))
                            {
                                t.EndUserId = reader.GetInt32(3);
                            }

                            if (!reader.IsDBNull(4))
                            {
                                t.SlaId = reader.GetInt32(4);
                            }

                            if (!reader.IsDBNull(5))
                            {
                                t.AdmStatus = reader.GetInt32(5);
                            }

                            if (!reader.IsDBNull(6))
                            {
                                t.SitId = reader.GetInt32(6);
                            }

                            if (!reader.IsDBNull(7))
                            {
                                t.FullName = reader.GetString(7);
                            }

                            if (!reader.IsDBNull(8))
                            {
                                t.IpAddress = reader.GetString(8);
                            }

                            if (!reader.IsDBNull(9))
                            {
                                t.IspId = reader.GetInt32(9);
                            }

                            if (!reader.IsDBNull(10))
                            {
                                t.ExtFullName = reader.GetString(10);
                            }

                            if (!reader.IsDBNull(11))
                            {
                                t.IPMask = reader.GetInt32(11);
                            }

                            if (!reader.IsDBNull(12))
                            {
                                t.IPRange = reader.GetBoolean(12);
                            }
                            else
                            {
                                t.IPRange = false;
                            }

                            if (!reader.IsDBNull(13))
                            {
                                t.FreeZone = reader.GetInt32(13);
                            }
                            else
                            {
                                t.FreeZone = 0;
                            }
                            reqTerminals.Add(t);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Log an exception
                var boLogController = new BOLogController(ConnectionString, DataProvider);
                var cmtException = new CmtApplicationException
                {
                    ExceptionDateTime = DateTime.Now,
                    ExceptionDesc = ex.Message,
                    ExceptionStacktrace = ex.StackTrace,
                    UserDescription = "GetTerminalsRequest method failed"
                };
                boLogController.LogApplicationException(cmtException);
            }

            return reqTerminals;
        }


        /// <summary>
        ///   Returns a Distributor record defined by its DistributorId
        /// </summary>
        /// <param name = "distributorId"></param>
        /// <returns></returns>
        public Distributor GetDistributor(int distributorId)
        {
            try
            {
                const string sql =
                    @"
                        select d.*, a.* 
                        from vw_distributorsAndVNOs7 d 
                        join addresses a on a.id = d.addressid 
                        where d.id = @distributorId
                    ";

                using (var db = new Database(ConnectionString, DataProvider))
                {
                    List<Distributor> dbObj = db.Fetch<Distributor, Address, Distributor>(
                        (i, a) =>
                            {
                                i.Address = a;
                                return i;
                            },
                        sql, new {distributorId});
                    return dbObj.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: Error while executing GetDistributor",
                                                                                   distributorId.ToString(CultureInfo.InvariantCulture), (short?)ExceptionLevelEnum.Critical));
                throw;
            }
        }

        /// <summary>
        ///   Returns an organization by the Organization ID.
        /// </summary>
        /// <param name = "organizationId"></param>
        /// <returns></returns>
        public Organization GetOrganization(int organizationId)
        {
            try
            {
                const string sql =
                    @"
                        SELECT o.*, a.*, d.* 
                        FROM vw_organizations3 as o 
                        JOIN Addresses as a on o.addressid = a.id 
                        LEFT JOIN vw_distributorsAndVNOs7 d on o.DistributorId = d.Id 
                        where o.Id = @organizationId
                    ";
                using (var db = new Database(ConnectionString, DataProvider))
                {
                    List<Organization> dbObj = db.Fetch<Organization, Address, Distributor, Organization>(
                        (i, a, d) =>
                            {
                                i.Address = a;
                                i.Distributor = d;
                                return i;
                            },
                        sql, new {organizationId});
                    return dbObj.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: Error while executing GetOrganization",
                                                                                   organizationId.ToString(CultureInfo.InvariantCulture), (short?)ExceptionLevelEnum.Critical));
                throw;
            }
        }

        /// <summary>
        ///   Returns an EndUser by the given UserId. 
        ///   This UserId is taken from the apsnet_Users table. 
        ///   If the user has an EndUser role, this method returns the details of the EndUser. 
        ///   The method uses theEndUserUsers link table
        /// 
        ///   If the user has no EndUser role, we'll return null.
        /// </summary>
        /// <param name = "userId"></param>
        /// <returns></returns>
        public EndUser GetEndUser(string userId)
        {
            try
            {
                const string sql =
                    @"
                        select u.[Id],u.[FirstName],u.[LastName],u.[EMail], u.[Phone],u.[MobilePhone],u.[SitId],u.[OrgId] 
                        from vw_endusers2 u 
                        join enduserusers userref on u.Id = userref.Enduserid
                        where userref.userid = @userid
                    ";
                using (var db = new Database(ConnectionString, DataProvider))
                {
                    List<EndUser> dbObj = db.Fetch<EndUser>(sql, new {userid = userId});
                    return dbObj.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: Error while executing GetEndUser",
                                                                                   userId));
                throw;
            }
        }

        /// <summary>
        ///   Updates EndUser data in the EndUser table. The method should return false if the update fails. 
        ///   If the EndUser does not exist yet in the EndUser table a new record MUST be inserted
        /// </summary>
        /// <param name = "userData"></param>
        /// <returns>true if the operation succeeded, false if it didn't</returns>
        public bool UpdateEndUser(EndUser userData)
        {
            try
            {
                using (var db = new Database(ConnectionString, DataProvider))
                {
                    var returnValue = new SqlParameter("returnValue", SqlDbType.Int)
                                          {Direction = ParameterDirection.Output};

                    db.Execute(
                        "exec cmtUpdateEndUsers4 @Id,@FirstName,@LastName,@Email,@Phone,@MobilePhone,@SitId,@returnValue OUTPUT",
                        new
                            {
                                userData.Id,
                                userData.FirstName,
                                userData.LastName,
                                userData.Email,
                                userData.Phone,
                                userData.MobilePhone,
                                userData.SitId,
                                userData.OrgId,
                                returnValue
                            }
                        );
                    var spResult = (int) returnValue.Value;

                    if (spResult == 3)
                    {
                        var exception =
                            new NoSuchSitIdException(
                                "That SitId does not exist, so we cannot create an end user for it.")
                                {SitId = userData.SitId};
                        throw exception;
                    }
                    if (spResult == 0 || spResult == 1)
                        return true;
                    return false;
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: Error while executing UpdateEndUser",
                                                                                   userData.ToString(), (short?)ExceptionLevelEnum.Critical));
                throw;
            }
        }

        /// <summary>
        /// This method will return the newly created enduserid. Note that it will return:
        /// </summary>
        /// <param name="userData"></param>
        /// <returns>
        /// enduserid: in case of a new id.
        /// </returns>
        public int UpdateEndUser2(EndUser userData)
        {
            try
            {
                using (var db = new Database(ConnectionString, DataProvider))
                {
                    var returnValue = new SqlParameter("returnValue", SqlDbType.Int) { Direction = ParameterDirection.Output };
                    var createdId = new SqlParameter("createdId", SqlDbType.Int) { Direction = ParameterDirection.Output };

                    db.Execute(
                        "exec cmtUpdateEndUsers5 @Id,@FirstName,@LastName,@Email,@Phone,@MobilePhone,@SitId,@returnValue OUTPUT,@createdId OUTPUT",
                        new
                        {
                            userData.Id,
                            userData.FirstName,
                            userData.LastName,
                            userData.Email,
                            userData.Phone,
                            userData.MobilePhone,
                            userData.SitId,
                            userData.OrgId,
                            returnValue,
                            createdId
                        }
                        );
                    var spResult = (int)returnValue.Value;

                    if (spResult == 3)
                        throw new NoSuchSitIdException("That SitId does not exist, so we cannot create an end user for it.") { SitId = userData.SitId };

                    return (int) createdId.Value;
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,"BOAccountingController: Error while executing UpdateEndUser",
                                                                                   userData.ToString(), (short?) ExceptionLevelEnum.Critical));
                throw;
            }
        }


        /// <summary>
        ///   Update terminal data in the terminal table. The method returns false if the update fails. 
        ///   If the Terminal does not exist yet in the Terminal table a new record MUST be inserted.
        /// </summary>
        /// <param name = "terminalData"></param>
        /// <returns></returns>
        public bool UpdateTerminal(Terminal terminalData)
        {
            try
            {
                if (terminalData.Address == null)
                    terminalData.Address = new Address();


                using (var db = new Database(ConnectionString, DataProvider))
                {
                    var alertTestMode = new SqlParameter("alertTestModeChanged", SqlDbType.Bit)
                                            {Direction = ParameterDirection.Output};
                    var outputParameter = new SqlParameter("ReturnValue", SqlDbType.Int)
                                              {Direction = ParameterDirection.Output};

                    db.Execute(
                        "exec [cmtUpdateTerminal21] @MacAddress,@StartDate,@ExpiryDate,@EndUserId,@MonthlyConsumed,@MonthlyConsumedReturn," +
                        "@SlaId,@AdmStatus,@SitId,@FullName,@Phone,@Fax,@Email,@Remarks,@OrgId,@IpAddress,@Latitude,@Longitude,@FirstActivationDate,@Blade,@IPMgm," +
                        "@IPTunnel,@SpotID,@RTPool,@FWPool,@IspId,@AddressLine1,@AddressLine2,@Location,@PostalCode,@Country,@Serial,@AddressType,@DistributorId, " +
                        "@TestMode,@DistributorAcceptance, @OrganizationAcceptance, @RequestDate,@LateBird,@CNo, @ExtFullName, @IPMask, @IPRange, @FreeZone, @TermWeight, @FreeZoneFlag, @StaticIPFlag, @EdgeSlaID, @InvoiceInterval, @LastInvoiceDate, @TerminalInvoicing, @RedundantSetup, @PrimaryTerminal, @AssociatedMacAddress, @VirtualTerminal," +
                        "@alertTestModeChanged OUTPUT, @Modem, @iLNBBUC, @Antenna, @DialogTechnology, @CenterFrequency, @SymbolRate, @ReturnValue OUTPUT",
                        new
                            {
                                outputParameter,
                                terminalData.MacAddress,
                                terminalData.StartDate,
                                terminalData.ExpiryDate,
                                terminalData.EndUserId,
                                terminalData.MonthlyConsumed,
                                terminalData.MonthlyConsumedReturn,
                                terminalData.SlaId,
                                terminalData.AdmStatus,
                                terminalData.SitId,
                                terminalData.FullName,
                                terminalData.Phone,
                                terminalData.Fax,
                                terminalData.Email,
                                terminalData.Remarks,
                                terminalData.OrgId,
                                terminalData.IpAddress,
                                Latitude =
                            (terminalData.LatLong == null ? (double?) null : terminalData.LatLong.Latitude),
                                Longitude =
                            terminalData.LatLong == null ? (double?) null : terminalData.LatLong.Longitude,
                                terminalData.FirstActivationDate,
                                terminalData.Blade,
                                terminalData.IPMgm,
                                terminalData.IPTunnel,
                                terminalData.SpotID,
                                terminalData.RTPool,
                                terminalData.FWPool,
                                terminalData.IspId,
                                terminalData.Address.AddressLine1,
                                terminalData.Address.AddressLine2,
                                terminalData.Address.Location,
                                terminalData.Address.PostalCode,
                                terminalData.Address.Country,
                                terminalData.Serial,
                                terminalData.AddressType,
                                terminalData.DistributorId,
                                terminalData.TestMode,
                                terminalData.DistributorAcceptance,
                                terminalData.OrganizationAcceptance,
                                terminalData.RequestDate,
                                terminalData.LateBird,
                                terminalData.CNo,
                                terminalData.ExtFullName,
                                terminalData.IPMask,
                                terminalData.IPRange,
                                terminalData.FreeZone,
                                terminalData.TermWeight,
                                terminalData.FreeZoneFlag,
                                terminalData.StaticIPFlag,
                                terminalData.EdgeSlaID,
                                terminalData.InvoiceInterval,
                                terminalData.LastInvoiceDate,
                                terminalData.TerminalInvoicing,
                                terminalData.RedundantSetup,
                                terminalData.PrimaryTerminal,
                                terminalData.AssociatedMacAddress,
                                terminalData.VirtualTerminal,
                                alertTestModeChanged = alertTestMode,
                                terminalData.Modem,
                                terminalData.iLNBBUC,
                                terminalData.Antenna,
                                terminalData.DialogTechnology,
                                terminalData.CenterFrequency,
                                terminalData.SymbolRate,
                                ReturnValue = outputParameter
                            }
                        );

                    var spResult = (int) outputParameter.Value;
                    if ((bool) alertTestMode.Value)
                    {
                        // where's the email stored for the nocsa and nocop?
                        new BONotifyController(ConnectionString, DataProvider).
                            SendTestModeAlert(terminalData.MacAddress);
                    }

                    if (spResult == 0 || spResult == 1)
                        return true;
                    return false;
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: Error while executing UpdateTerminal",
                                                                                   terminalData.ToString(), (short?) ExceptionLevelEnum.Critical));

                _log.Error("Error during UpdateTerminal", ex);
                throw;
            }
        }

        /// <summary>
        ///   Updates an organization in the Organization table. Returns false if the update fails. 
        ///   If the Organization does not exist yet in the Organization table a new record MUST be inserted
        /// </summary>
        /// <param name = "organizatonData"></param>
        /// <returns></returns>
        public bool UpdateOrganization(Organization organizatonData)
        {
            try
            {
                if (organizatonData.Address == null)
                    organizatonData.Address = new Address();

                if (organizatonData.Distributor == null)
                    organizatonData.Distributor = new Distributor();

                using (var db = new Database(ConnectionString, DataProvider))
                {
                    var returnValue = new SqlParameter("returnValue", SqlDbType.Int)
                                          {Direction = ParameterDirection.Output};

                    db.Execute(
                        "exec [cmtUpdateOrganization5] @Id,@FullName,@Phone,@Fax,@Email,@WebSite,@Remarks,@DistributorId,@Vat,@VNO,@MIR,@CIR,@DefaultSla, @AddressLine1,@AddressLine2,@Location,@PostalCode,@Country,@Sector,@returnValue OUTPUT",
                        new
                            {
                                organizatonData.Id,
                                organizatonData.FullName,
                                organizatonData.Phone,
                                organizatonData.Fax,
                                organizatonData.Email,
                                organizatonData.WebSite,
                                organizatonData.Remarks,
                                DistributorId =
                            organizatonData.Distributor != null ? organizatonData.Distributor.Id : null,
                                organizatonData.Vat,
                                organizatonData.VNO,
                                organizatonData.MIR,
                                organizatonData.CIR,
                                organizatonData.DefaultSla,
                                organizatonData.Address.AddressLine1,
                                organizatonData.Address.AddressLine2,
                                organizatonData.Address.Location,
                                organizatonData.Address.PostalCode,
                                organizatonData.Address.Country,
                                organizatonData.Sector,
                                returnValue
                            }
                        );
                    var spResult = (int) returnValue.Value;
                    if (spResult == 0 || spResult == 1)
                        return true;
                    return false;
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: Error while executing UpdateOrganization",
                                                                                   organizatonData.ToString(), (short?) ExceptionLevelEnum.Critical));
                throw;
            }
        }

        /// <summary>
        ///   Updates a distributor in the distributor database. The method returns false if the update fails. 
        ///   If the Distributor does not exist yet in the Distributor table a new record MUST be inserted.
        /// </summary>
        /// <param name = "distributorData"></param>
        /// <returns></returns>
        public bool UpdateDistributor(Distributor distributorData)
        {
            try
            {
                if (distributorData.Address == null)
                    distributorData.Address = new Address();

                if (distributorData.Isp == null)
                    distributorData.Isp = new Isp();

                using (var db = new Database(ConnectionString, DataProvider))
                {
                    var returnValue = new SqlParameter("returnValue", SqlDbType.Int)
                                          {Direction = ParameterDirection.Output};

                    db.Execute(
                        "exec [cmtUpdateDistributor9] @Id,@FullName,@Phone,@Fax,@Email,@WebSite,@Remarks,@IspId,@Vat,@Currency,@VNO,@DefaultOrganization,@LastInvoiceDate,@InvoiceInterval,@BankCharges,@InvoicingEmail,@Inactive,@BadDistributor,@AddressLine1,@AddressLine2,@Location,@PostalCode,@Country,@DistType,@returnValue OUTPUT",
                        new
                            {
                                distributorData.Id,
                                distributorData.FullName,
                                distributorData.Phone,
                                distributorData.Fax,
                                distributorData.Email,
                                distributorData.WebSite,
                                distributorData.Remarks,
                                IspId = distributorData.Isp.Id,
                                distributorData.Vat,
                                distributorData.Currency,
                                distributorData.VNO,
                                distributorData.DefaultOrganization,
                                distributorData.LastInvoiceDate,
                                distributorData.InvoiceInterval,
                                distributorData.BankCharges,
                                distributorData.InvoicingEmail,
                                distributorData.Inactive,
                                distributorData.BadDistributor,
                                distributorData.Address.AddressLine1,
                                distributorData.Address.AddressLine2,
                                distributorData.Address.Location,
                                distributorData.Address.PostalCode,
                                distributorData.Address.Country,
                                distributorData.DistType,
                            returnValue
                            }
                        );
                    
                    var spResult = (int) returnValue.Value;

                    if (spResult == 0 || spResult == 1)
                        return true;
                    return false;
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: Error while executing UpdateDistributor",
                                                                                   distributorData.ToString(), (short?) ExceptionLevelEnum.Critical));
                throw;
            }
        }

        /// <summary>
        ///   Updates ISP data in the ISP table. The method returns false if the method fails. 
        ///   If the ISP does not exist yet in the ISP table a new record MUST be inserted.
        /// </summary>
        /// <param name = "ispData"></param>
        /// <returns></returns>
        public bool UpdateIsp(Isp ispData)
        {
            try
            {
                if (ispData.Address == null)
                    ispData.Address = new Address();

                if (!ispData.Id.HasValue)
                    throw new ArgumentException(
                        "You need to specify a new id for the ISP. These ID's are not autogenerated!");

                using (var db = new Database(ConnectionString, DataProvider))
                {
                    var returnValue = new SqlParameter("returnValue", SqlDbType.Int)
                                          {Direction = ParameterDirection.Output};

                    db.Execute(
                        "exec [cmtUpdateIsp5] @Id,@CompanyName,@Phone,@Fax,@Email,@Remarks,@Dashboard_User,@Dashboard_Password,@SatelliteId,@Ispif_User,@Ispif_Password,@AddressLine1,@AddressLine2,@Location,@PostalCode,@Country,@ISPType,@returnValue OUTPUT",
                        new
                            {
                                ispData.Id,
                                ispData.CompanyName,
                                ispData.Phone,
                                ispData.Fax,
                                ispData.Email,
                                ispData.Remarks,
                                ispData.Dashboard_User,
                                ispData.Dashboard_Password,
                                SatelliteId = ispData.Satellite.ID,
                                ispData.Ispif_User,
                                ispData.Ispif_Password,
                                ispData.Address.AddressLine1,
                                ispData.Address.AddressLine2,
                                ispData.Address.Location,
                                ispData.Address.PostalCode,
                                ispData.Address.Country,
                                ispData.ISPType,
                                returnValue
                            }
                        );
                    var spResult = (int) returnValue.Value;
                    if (spResult == 0 || spResult == 1)
                        return true;
                    return false;
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: Error while executing UpdateIsp",
                                                                                   ispData.ToString(), (short?)ExceptionLevelEnum.Critical));
                throw;
            }
        }

        /// <summary>
        ///   Returns an ISP record by its ISP id.
        /// </summary>
        /// <param name = "ispId"></param>
        /// <returns></returns>
        public Isp GetISP(int ispId)
        {
            Isp fetchedIsp;
            string queryCmd = "SELECT AddressId,SatelliteId FROM Isps where Id = @ispId ";
            try
            {
                using (var db = new Database(ConnectionString, DataProvider))
                {
                    List<Isp> dbObj = db.Fetch<Isp, Address, Satellite, Isp>(
                        (i, a, s) =>
                            {
                                i.Address = a;
                                i.Satellite = s;
                                return i;
                            },
                        @"
                                SELECT i.*, a.*, s.*
                                FROM Isps as i JOIN Addresses as a on i.addressid = a.id
                                INNER JOIN Satellites as s on i.satelliteId = s.id
                                where i.Id = @ispId
                            ", new { ispId });
                    fetchedIsp = dbObj.FirstOrDefault();
                }
                using (var conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();

                    using (var cmd = new SqlCommand(queryCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@ispId", ispId);
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            fetchedIsp.Address.Id = reader.GetInt32(0);
                            fetchedIsp.Satellite.ID = reader.GetInt32(1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: Error while executing GetIsp",
                                                                                   ispId.ToString(CultureInfo.InvariantCulture), (short?)ExceptionLevelEnum.Critical));
                throw;
            }
            return fetchedIsp;
        }

        /// <summary>
        /// This method returns a list of all Edge ISPs stored in the ISP table.
        /// </summary>
        /// <returns>A list of Edge ISPs. This list can be empty</returns>
        public List<Isp> GetEdgeISPs()
        {
            var ispType = 1;

            try
            {
                using (var db = new Database(ConnectionString, DataProvider))
                {
                    List<Isp> dbObj = db.Fetch<Isp, Address, Isp>(
                        (i, a) =>
                        {
                            i.Address = a;
                            return i;
                        },
                        @"
                                SELECT i.*, a.* 
                                FROM Isps as i JOIN Addresses as a on i.addressid = a.id 
                                where i.ISPType = @ispType", new { ispType });
                    return dbObj.ToList<Isp>();
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: Error while executing GetEdgeIsps",
                                                                                   "Could not retrieve edge ISPs", (short?)ExceptionLevelEnum.Error));
                throw;
            }
        }

        /// <summary>
        /// </summary>
        /// <param name = "distributorId"></param>
        /// <param name = "macAddress"></param>
        /// <returns></returns>
        public bool IsTermOwnedByDistributor(int distributorId, string macAddress)
        {
            try
            {
                const string sql =
                    @"
                        SELECT d.id 
                        FROM   terminals t 
                        JOIN vw_distributorsAndVNOs7 d 
                        ON d.id = t.distributorid 
                        WHERE  macaddress = @macAddress 
                    ";

                using (var db = new Database(ConnectionString, DataProvider))
                {
                    IEnumerable<int?> i = db.Query<int?>(sql, new {macAddress});
                    int? result = i.FirstOrDefault();

                    if (result == null)
                        throw new ArgumentException("The macaddress is not found in the database. Mac:" + macAddress);

                    return result.Value == distributorId;
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: Error while executing IsTermOwnedByDistributor",
                                                                                   String.Join(";",
                                                                                               distributorId.ToString(CultureInfo.InvariantCulture),
                                                                                               macAddress)));
                throw;
            }
        }

        /// <summary>
        /// </summary>
        /// <param name = "organizationId"></param>
        /// <param name = "macAddress"></param>
        /// <returns></returns>
        public bool IsTermOwnedByOrganization(int organizationId, string macAddress)
        {
            try
            {
                using (var db = new Database(ConnectionString, DataProvider))
                {
                    const string sql =
                        @"
                            SELECT orgid 
                            from vw_terminals6 
                            where macaddress = @macAddress
                        ";
                    IEnumerable<int?> i = db.Query<int?>(sql, new {macAddress});
                    int? result = i.FirstOrDefault();
                    if (result == null)
                        throw new ArgumentException("The macaddress is not found in the database. Mac:" + macAddress);

                    return result.Value == organizationId;
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: Error while executing IsTermOwnedByOrganization",
                                                                                   String.Join(";", organizationId,
                                                                                               macAddress)));
                throw;
            }
        }

        /// <summary>
        ///   Get all ISP's
        /// </summary>
        /// <returns></returns>
        public List<Isp> GetIsps()
        {
            try
            {
                const string sql =
                    @"
                        SELECT i.*, a.*
                        FROM Isps as i 
                        JOIN Addresses as a on i.addressid = a.id
                    ";
                using (var db = new Database(ConnectionString, DataProvider))
                {
                    List<Isp> dbObj = db.Fetch<Isp, Address, Isp>(
                        (i, a) =>
                            {
                                i.Address = a;
                                return i;
                            },
                        sql);
                    return dbObj.ToList();
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: Error while executing GetIsps",
                                                                                   ""));
                throw;
            }
        }

        /// <summary>
        ///   Get all distributors 
        ///   Excludes all distributors in the VNO list
        /// </summary>
        /// <returns>A list of distributor objects</returns>
        public List<Distributor> GetDistributors()
        {
            try
            {
                const string sql =
                    @"
                        select d.*, a.* 
                        from vw_distributors6 d 
                        join addresses a on a.id = d.addressid order by d.FullName
                    ";

                using (var db = new Database(ConnectionString, DataProvider))
                {
                    List<Distributor> dbObj = db.Fetch<Distributor, Address, Distributor>(
                        (i, a) =>
                            {
                                i.Address = a;
                                return i;
                            },
                        sql);
                    return dbObj.ToList();
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: Error while executing GetDistributors",
                                                                                   ""));
                throw;
            }
        }

        /// <summary>
        ///   Get all inactive distributors
        ///   Excludes all inactive distributors in the VNO list
        /// </summary>
        /// <returns>A list of inactive distributor objects</returns>
        public List<Distributor> GetInactiveDistributors()
        {
            try
            {
                const string sql =
                    @"
                        select d.* , a.*
                        from vw_distributors6 d 
                        join addresses a on a.id = d.AddressId 
                        where d.Inactive=1 order by d.FullName 
                    ";

                using (var db = new Database(ConnectionString, DataProvider))
                {
                    List<Distributor> dbObj = db.Fetch<Distributor, Address, Distributor>(
                        (i, a) =>
                        {
                            i.Address = a;
                            return i;
                        },
                        sql);
                    return dbObj.ToList();
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: Error while executing GetDistributors",
                                                                                   ""));
                throw;
            }
        }

        /// <summary>
        ///   Get all active distributors 
        ///   Excludes all active distributors in the VNO list
        /// </summary>
        /// <returns>A list of active distributor objects</returns>
        public List<Distributor> GetActiveDistributors()
        {
            try
            {
                const string sql =
                    @"
                        select d.* , a.*
                        from vw_distributors6 d 
                        join addresses a on a.id = d.AddressId 
                        where d.Inactive=0 order by d.FullName 
                    ";

                using (var db = new Database(ConnectionString, DataProvider))
                {
                    List<Distributor> dbObj = db.Fetch<Distributor, Address, Distributor>(
                        (i, a) =>
                        {
                            i.Address = a;
                            return i;
                        },
                        sql);
                    return dbObj.ToList();
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: Error while executing GetDistributors",
                                                                                   ""));
                throw;
            }
        }
        

        /// <summary>
        ///   Get all Distributors and VNOs
        /// </summary>
        /// <returns>A list of distributor objects</returns>
        public List<Distributor> GetDistributorsAndVNOs()
        {
            try
            {
                const string sql =
                    @"
                        select d.*, a.* 
                        from vw_distributorsAndVNOs7 d 
                        join addresses a on a.id = d.addressid order by d.FullName
                    ";

                using (var db = new Database(ConnectionString, DataProvider))
                {
                    List<Distributor> dbObj = db.Fetch<Distributor, Address, Distributor>(
                        (i, a) =>
                        {
                            i.Address = a;
                            return i;
                        },
                        sql);
                    return dbObj.ToList();
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: Error while executing GetDistributorsAndVNOs",
                                                                                   ""));
                throw;
            }
        }

        /// <summary>
        ///   Get all organizations
        /// </summary>
        /// <returns></returns>
        public List<Organization> GetOrganizations()
        {
            try
            {
                const string sql =
                    @"
                        SELECT o.*, a.*, d.*
                        FROM vw_Organizations3 as o 
                        JOIN Addresses as a on o.addressid = a.id 
                        LEFT JOIN vw_distributorsAndVNOs7 d on o.DistributorId = d.Id ORDER BY o.FullName
                    ";

                using (var db = new Database(ConnectionString, DataProvider))
                {
                    List<Organization> dbObj = db.Fetch<Organization, Address, Distributor, Organization>(
                        (i, a, d) =>
                            {
                                i.Address = a;
                                i.Distributor = d;
                                return i;
                            },
                        sql);
                    return dbObj.ToList();
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: Error while executing GetOrganizations",
                                                                                   ""));
                throw;
            }
        }

        /// <summary>
        ///   get all terminals. 
        ///   note: should we not include paging here?
        /// </summary>
        /// <returns></returns>
        public List<Terminal> GetTerminals()
        {
            try
            {
                const string sql =
                    @"
                        SELECT t.*, a.*  
                        FROM VW_Terminals19 t 
                        join addresses a on a.id = t.addressid
                    ";

                return GetTerminalListForQuery(sql).ToList();
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: Error while executing GetTerminals",
                                                                                   ""));
                throw;
            }
        }

        /// <summary>
        ///   Get a list of all terminal statuses
        /// </summary>
        /// <returns>A list of terminal status objects from the database</returns>
        public List<TerminalStatus> GetTerminalStatus()
        {
            try
            {
                const string sql =
                    @"
                        SELECT t.*  
                        FROM TerminalStatus t 
                    ";

                using (var db = new Database(ConnectionString, DataProvider))
                {
                    List<TerminalStatus> dbObj = db.Fetch<TerminalStatus>(sql);
                    return dbObj.ToList();
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: Error while executing GetTerminalStatus",
                                                                                   ""));
                throw;
            }
        }


        /// <summary>
        ///   updateTerminalStatus (insert if not exist, update otherwise)
        /// </summary>
        /// <param name = "status"></param>
        /// <returns></returns>
        public bool UpdateTerminalStatus(TerminalStatus status)
        {
            try
            {
                const string sql = "exec [cmtUpdateTerminalStatus3]  @Id, @Description,@returnValue OUTPUT";

                _log.Debug("UpdateTerminalStatus:start");
                using (var db = new Database(ConnectionString, DataProvider))
                {
                    var returnValue = new SqlParameter("returnValue", SqlDbType.Int)
                                          {Direction = ParameterDirection.Output};

                    db.Execute(sql,
                               new
                                   {
                                       status.Id,
                                       Description = status.StatusDesc,
                                       returnValue
                                   }
                        );
                    var spResult = (int) returnValue.Value;

                    _log.Debug("Called cmtUpdateTerminalStatus and returned " + spResult);
                    return spResult == 0;
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: Error while executing UpdateTerminalStatus",
                                                                                   status.ToString(), (short?)ExceptionLevelEnum.Critical));
                throw;
            }
        }

        /// <summary>
        ///   Get all available service packs/levels
        /// </summary>
        /// <returns></returns>
        public List<ServiceLevel> GetServicePacks()
        {
            try
            {
                const string sql =
                    @"
                        SELECT t.*  
                        FROM ServicePacks t
                        WHERE (t.Deleted IS NULL) OR (t.Deleted = 0)
                    ";

                using (var db = new Database(ConnectionString, DataProvider))
                {
                    List<ServiceLevel> dbObj = db.Fetch<ServiceLevel>(sql);
                    return dbObj.ToList();
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: Error while executing GetServicePacks",
                                                                                   ""));
                throw;
            }
        }

        /// <summary>
        ///   Get all available service packs/levels for a specific VNO
        /// </summary>
        /// <param name="VNOId">The ID of the VNO</param>
        /// <returns>A list of service packs</returns>
        public List<ServiceLevel> GetServicePacksForVNO(int VNOId)
        {
           const string queryCmd = "SELECT SlaID, SlaName, FUPThreshold, DRAboveFUP, " +
                                    "SlaCommonName, IspId, Freezone, Business, Voucher, RTNFUPThreshold, " +
                                    "ServicePackAmt, FreeZoneAmt, VoIPAmt, VoIPFlag, Hide, " +
                                    "CIRFWD, CIRRTN, DRFWD, DRRTN, VNOId, ServiceClass " +
                                    "FROM ServicePacks WHERE VNOId = @VNOId AND (Deleted IS NULL OR Deleted = 0)";
            
            ServiceLevel sl = null;
            List<ServiceLevel> servicePacks = new List<ServiceLevel>();

            try
            {
                using (var conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();

                    using (var cmd = new SqlCommand(queryCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@VNOId", VNOId);
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            sl = new ServiceLevel();
                            sl.SlaId = reader.GetInt32(0);
                            sl.SlaName = reader.GetString(1);
                            sl.FupThreshold = reader.GetDecimal(2);
                            sl.DrAboveFup = reader.GetString(3);
                            sl.SlaCommonName = reader.GetString(4);
                            sl.IspId = reader.GetInt32(5);
                            sl.FreeZone = reader.GetBoolean(6);
                            sl.Business = reader.GetBoolean(7);
                            sl.Voucher = reader.GetBoolean(8);
                            sl.RTNFUPThreshold = reader.GetDecimal(9);
                            sl.ServicePackAmt = (float)reader.GetDecimal(10);
                            sl.FreeZoneAmt = (float)reader.GetDecimal(11);
                            sl.VoIPAmt = (float)reader.GetDecimal(12);
                            sl.VoIPFlag = reader.GetBoolean(13);
                            sl.Hide = reader.GetBoolean(14);
                            sl.CIRFWD = reader.GetInt32(15);
                            sl.CIRRTN = reader.GetInt32(16);
                            sl.DRFWD = reader.GetInt32(17);
                            sl.DRRTN = reader.GetInt32(18);
                            sl.VNOId = reader.GetInt32(19);
                            sl.ServiceClass = reader.GetInt32(20);
                            servicePacks.Add(sl);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: Error while executing GetServicePacksForVNO",
                                                                                   VNOId.ToString()));
                throw;
            }
            return servicePacks;
        }

        /// <summary>
        ///   Get all available service packs/levels not assigned to a VNO
        /// </summary>
        /// <returns>A list of service packs</returns>
        public List<ServiceLevel> GetServicePacksForAllVNOs()
        {
            const string queryCmd = "SELECT SlaID, SlaName, FUPThreshold, DRAboveFUP, " +
                                     "SlaCommonName, IspId, Freezone, Business, Voucher, RTNFUPThreshold, " +
                                     "ServicePackAmt, FreeZoneAmt, VoIPAmt, VoIPFlag, Hide, " +
                                     "CIRFWD, CIRRTN, DRFWD, DRRTN, VNOId, ServiceClass " +
                                     "FROM ServicePacks WHERE (VNOId is NULL OR VNOId = 0) AND (Deleted IS NULL OR Deleted = 0)";

            ServiceLevel sl = null;
            List<ServiceLevel> servicePacks = new List<ServiceLevel>();

            try
            {
                using (var conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();

                    using (var cmd = new SqlCommand(queryCmd, conn))
                    {
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            sl = new ServiceLevel();
                            sl.SlaId = reader.GetInt32(0);
                            sl.SlaName = reader.GetString(1);
                            sl.FupThreshold = reader.GetDecimal(2);
                            sl.DrAboveFup = reader.GetString(3);
                            sl.SlaCommonName = reader.GetString(4);
                            sl.IspId = reader.GetInt32(5);
                            sl.FreeZone = reader.GetBoolean(6);
                            sl.Business = reader.GetBoolean(7);
                            sl.Voucher = reader.GetBoolean(8);
                            sl.RTNFUPThreshold = reader.GetDecimal(9);
                            sl.ServicePackAmt = (float)reader.GetDecimal(10);
                            sl.FreeZoneAmt = (float)reader.GetDecimal(11);
                            sl.VoIPAmt = (float)reader.GetDecimal(12);
                            sl.VoIPFlag = reader.GetBoolean(13);
                            sl.Hide = reader.GetBoolean(14);
                            sl.CIRFWD = reader.GetInt32(15);
                            sl.CIRRTN = reader.GetInt32(16);
                            sl.DRFWD = reader.GetInt32(17);
                            sl.DRRTN = reader.GetInt32(18);
                            sl.VNOId = reader.GetInt32(19);
                            sl.ServiceClass = reader.GetInt32(20);
                            servicePacks.Add(sl);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: Error while executing GetServicePacksForAllVNOs",
                                                                                   ""));
                throw;
            }
            return servicePacks;
        }

        /// <summary>
        ///   Get all available service packs/levels not assigned to a VNO by ISP ID
        /// </summary>
        /// <param name="ispId">The ID of the ISP</param>
        /// <returns>A list of service packs</returns>
        public List<ServiceLevel> GetServicePacksForAllVNOsByISP(int ispId)
        {
            try
            {
                const string sql =
                    @"

                    SELECT t.*  
                    FROM ServicePacks t 
                    WHERE t.Ispid = @ispId AND t.Hide = 0 AND (VNOId is NULL OR VNOId = 0) AND (t.Deleted IS NULL OR t.Deleted = 0)
                    ORDER BY t.SlaName
                    ";

                using (var db = new Database(ConnectionString, DataProvider))
                {
                    List<ServiceLevel> dbObj = db.Fetch<ServiceLevel>(sql, new { ispId });
                    return dbObj.ToList();
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: Error while executing GetServicePacks",
                                                                                   ""));
                throw;
            }
        }

        /// <summary>
        /// Returns the ServiceLevel object corresponding with the given 
        /// servicePackId
        /// </summary>
        /// <param name="servicePackId">The unique ServicePack identifier</param>
        /// <returns>The corresponding ServicePack or null if not found</returns>
        public ServiceLevel GetServicePack(int servicePackId)
        {
            const string queryCmd = "SELECT SlaID, SlaName, FUPThreshold, DRAboveFUP, " +
                                    "SlaCommonName, IspId, Freezone, Business, Voucher, RTNFUPThreshold, " +
                                    "ServicePackAmt, FreeZoneAmt, VoIPAmt, VoIPFlag, Hide, " +
                                    "CIRFWD, CIRRTN, DRFWD, DRRTN, VNOId, ServiceClass, ServicePackAmtEUR, FreeZoneAmtEUR, VoIPAmtEUR, " +
                                    "HighVolumeFWD, HighVolumeRTN, LowVolumeFWD, LowVolumeRTN, MinBWFWD, MinBWRTN, " +
                                    "LowVolumeSUM, HighVolumeSUM, AggregateFlag, SLAWeight, EdgeISP, EdgeSla, BillableId " +
                                    //"FROM ServicePacks WHERE SlaID = @SlaID AND (Deleted IS NULL OR Deleted = 0)";
                                    "FROM ServicePacks WHERE SlaID = @SlaID";
            ServiceLevel sl = null;

            try
            {
                using (var conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();

                    using (var cmd = new SqlCommand(queryCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@SlaID", servicePackId);
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            sl = new ServiceLevel();

                            if (!reader.IsDBNull(0))
                            {
                                sl.SlaId = reader.GetInt32(0);
                            }

                            sl.SlaName = reader.GetString(1);

                            if (!reader.IsDBNull(2))
                            {
                                sl.FupThreshold = reader.GetDecimal(2);
                            }

                            sl.DrAboveFup = reader.GetString(3);
                            sl.SlaCommonName = reader.GetString(4);
                            sl.IspId = reader.GetInt32(5);

                            if (!reader.IsDBNull(6))
                            {
                                sl.FreeZone = reader.GetBoolean(6);
                            }

                            if (!reader.IsDBNull(7))
                            {
                                sl.Business = reader.GetBoolean(7);
                            }

                            if (!reader.IsDBNull(8))
                            {
                                sl.Voucher = reader.GetBoolean(8);
                            }

                            if (!reader.IsDBNull(9))
                            {
                                sl.RTNFUPThreshold = reader.GetDecimal(9);
                            }

                            if (!reader.IsDBNull(10))
                            {
                                sl.ServicePackAmt = (float)reader.GetDecimal(10);
                            }

                            if (!reader.IsDBNull(11))
                            {
                                sl.FreeZoneAmt = (float)reader.GetDecimal(11);
                            }

                            if (!reader.IsDBNull(12))
                            {
                                sl.VoIPAmt = (float)reader.GetDecimal(12);
                            }

                            if (!reader.IsDBNull(13))
                            {
                                sl.VoIPFlag = reader.GetBoolean(13);
                            }

                            if (!reader.IsDBNull(14))
                            {
                                sl.Hide = reader.GetBoolean(14);
                            }

                            if (!reader.IsDBNull(15))
                            {
                                sl.CIRFWD = reader.GetInt32(15);
                            }
                            if (!reader.IsDBNull(16))
                            {
                                sl.CIRRTN = reader.GetInt32(16);
                            }
                            if (!reader.IsDBNull(17))
                            {
                                sl.DRFWD = reader.GetInt32(17);
                            }
                            if (!reader.IsDBNull(18))
                            {
                                sl.DRRTN = reader.GetInt32(18);
                            }
                            if (!reader.IsDBNull(19))
                            {
                                sl.VNOId = reader.GetInt32(19);
                            }
                            if (!reader.IsDBNull(20))
                            {
                                sl.ServiceClass = reader.GetInt32(20);
                            }
                            if (!reader.IsDBNull(21))
                            {
                                sl.ServicePackAmtEUR = (float)reader.GetDecimal(21);
                            }
                            if (!reader.IsDBNull(22))
                            {
                                sl.FreeZoneAmtEUR = (float)reader.GetDecimal(22);
                            }
                            if (!reader.IsDBNull(23))
                            {
                                sl.VoIPAmtEUR = (float)reader.GetDecimal(23);
                            }
                            if (!reader.IsDBNull(24))
                            {
                                sl.HighVolumeFWD = reader.GetDecimal(24);
                            }
                            if (!reader.IsDBNull(25))
                            {
                                sl.HighVolumeRTN = reader.GetDecimal(25);
                            }
                            if (!reader.IsDBNull(26))
                            {
                                sl.LowVolumeFWD = reader.GetDecimal(26);
                            }
                            if (!reader.IsDBNull(27))
                            {
                                sl.LowVolumeRTN = reader.GetDecimal(27);
                            }
                            if (!reader.IsDBNull(28))
                            {
                                sl.MinBWFWD = reader.GetInt32(28);
                            }
                            if (!reader.IsDBNull(29))
                            {
                                sl.MinBWRTN = reader.GetInt32(29);
                            }
                            if (!reader.IsDBNull(30))
                            {
                                sl.LowVolumeSUM = reader.GetDecimal(30);
                            }
                            if (!reader.IsDBNull(31))
                            {
                                sl.HighVolumeSUM = reader.GetDecimal(31);
                            }
                            if (!reader.IsDBNull(32))
                            {
                                sl.AggregateFlag = reader.GetBoolean(32);
                            }
                            if (!reader.IsDBNull(33))
                            {
                                sl.SLAWeight = reader.GetDouble(33);
                            }

                            if (!reader.IsDBNull(34))
                            {
                                sl.EdgeISP = reader.GetInt32(34);
                            }

                            if (!reader.IsDBNull(35))
                            {
                                sl.EdgeSLA = reader.GetInt32(35);
                            }

                            if (!reader.IsDBNull(36))
                            {
                                sl.BillableId = reader.GetInt32(36);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var boLogController = new BOLogController(ConnectionString, DataProvider);
                boLogController.LogApplicationException(new CmtApplicationException(ex,
                                                                                    "Error while retrieving ServiceLevel object for Sla: "
                                                                                    + servicePackId, "Error"));
            }

            return sl;
        }

        /// <summary>
        ///   Get all available service packs/levels by Billable Id
        /// </summary>
        /// <param name="billableId"></param>
        /// <returns>A list of service packs</returns>
        /// CMTINVOICE-73
        public List<ServiceLevel> GetServicePacksByBillableId(int billableId)
        {
            const string queryCmd = "SELECT SlaID, SlaName, FUPThreshold, DRAboveFUP, " +
                                     "SlaCommonName, IspId, Freezone, Business, Voucher, RTNFUPThreshold, " +
                                     "ServicePackAmt, FreeZoneAmt, VoIPAmt, VoIPFlag, Hide, " +
                                     "CIRFWD, CIRRTN, DRFWD, DRRTN, VNOId, ServiceClass " +
                                     "FROM ServicePacks WHERE BillableId = @BillableId";

            ServiceLevel sl = null;
            List<ServiceLevel> servicePacks = new List<ServiceLevel>();

            try
            {
                using (var conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();

                    using (var cmd = new SqlCommand(queryCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@BillableId", billableId);
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            sl = new ServiceLevel();
                            sl.SlaId = reader.GetInt32(0);
                            sl.SlaName = reader.GetString(1);
                            sl.FupThreshold = reader.GetDecimal(2);
                            sl.DrAboveFup = reader.GetString(3);
                            sl.SlaCommonName = reader.GetString(4);
                            sl.IspId = reader.GetInt32(5);
                            sl.FreeZone = reader.GetBoolean(6);
                            sl.Business = reader.GetBoolean(7);
                            sl.Voucher = reader.GetBoolean(8);
                            sl.RTNFUPThreshold = reader.GetDecimal(9);
                            sl.ServicePackAmt = (float)reader.GetDecimal(10);
                            sl.FreeZoneAmt = (float)reader.GetDecimal(11);
                            sl.VoIPAmt = (float)reader.GetDecimal(12);
                            sl.VoIPFlag = reader.GetBoolean(13);
                            sl.Hide = reader.GetBoolean(14);
                            sl.CIRFWD = reader.GetInt32(15);
                            sl.CIRRTN = reader.GetInt32(16);
                            sl.DRFWD = reader.GetInt32(17);
                            sl.DRRTN = reader.GetInt32(18);
                            sl.VNOId = reader.GetInt32(19);
                            sl.ServiceClass = reader.GetInt32(20);
                            servicePacks.Add(sl);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: Error while executing GetServicePacksByBillableId",
                                                                                   billableId.ToString()));
                throw;
            }
            return servicePacks;
        }

        /// <summary>
        ///  Update or Insert a service pack/level
        /// </summary>
        /// <param name = "level">The servicelevel to be inserted/updated</param>
        /// <returns>True if the operation succeeded</returns>
        public bool UpdateServicePack(ServiceLevel level)
        {
            try
            {
                const string sql =
                    "exec [cmtUpdateServicePack18] @SlaId, @SlaName, @FupThreshold, @DrAboveFup, @SlaCommonName, @IspId, " +
                    "@FreeZone, @Business, @Voucher, @RTNFUPThreshold, " +
                    "@ServicePackAmt, @FreeZoneAmt, @VoIPAmt, @VoIPFlag, @Hide, " +
                    "@CIRFWD, @CIRRTN, @DRFWD, @DRRTN, @VNOId, @ServiceClass, @ServicePackAmtEUR, @FreeZoneAmtEUR, @VoIPAmtEUR, " +
                    "@HighVolumeFWD, @HighVolumeRTN, @LowVolumeFWD, @LowVolumeRTN, @MinBWFWD, @MinBWRTN, " +
                    "@LowVolumeSUM, @HighVolumeSUM, @AggregateFlag, @SLAWeight, @EdgeISP, @EdgeSLA, @EdgeSlaOverFup, " +
                    "@BillableId, @returnValue OUTPUT";

                if (!level.SlaId.HasValue)
                    throw new ArgumentException(
                        "You need to define the SLAId explicitly. It currently has no auto identity.");

                _log.Debug("UpdateServicePack:start");
                using (var db = new Database(ConnectionString, DataProvider))
                {
                    var returnValue = new SqlParameter("returnValue", SqlDbType.Int)
                                          {Direction = ParameterDirection.Output};

                    db.Execute(sql,
                               new
                                   {
                                       level.SlaId,
                                       level.SlaName,
                                       level.FupThreshold,
                                       level.DrAboveFup,
                                       level.SlaCommonName,
                                       level.IspId,
                                       level.FreeZone,
                                       level.Business,
                                       level.Voucher,
                                       level.RTNFUPThreshold,
                                       level.ServicePackAmt,
                                       level.FreeZoneAmt,
                                       level.VoIPAmt,
                                       level.VoIPFlag,
                                       level.Hide,
                                       level.CIRFWD,
                                       level.CIRRTN,
                                       level.DRFWD,
                                       level.DRRTN,
                                       level.VNOId,
                                       level.ServiceClass,
                                       level.ServicePackAmtEUR,
                                       level.FreeZoneAmtEUR,
                                       level.VoIPAmtEUR,
                                       level.HighVolumeFWD,
                                       level.HighVolumeRTN,
                                       level.LowVolumeFWD,
                                       level.LowVolumeRTN,
                                       level.MinBWFWD,
                                       level.MinBWRTN,
                                       level.LowVolumeSUM,
                                       level.HighVolumeSUM,
                                       level.AggregateFlag,
                                       level.SLAWeight,
                                       level.EdgeISP,
                                       level.EdgeSLA,
                                       level.EdgeSlaOverFup,
                                       level.BillableId,
                                       returnValue
                                   }
                        );
                    var spResult = (int) returnValue.Value;
                    _log.Debug("Called cmtUpdateServicePack and returned " + spResult);
                    return spResult == 0 || spResult == 1;
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: Error while executing UpdateServicePack",
                                                                                   level.ToString(), (short?)ExceptionLevelEnum.Critical));
                throw;
            }
        }

        /// <summary>
        /// Returns the pricing of an activity type as a float
        /// </summary>
        /// <remarks>
        /// If the activity has not price setting the value resturned is 0.0
        /// </remarks>
        /// <param name="activityType">The activity type identifier</param>
        /// <returns>The activity type price as a float</returns>
        public float GetActivityTypePrice(int activityType)
        {
            string queryCmd = "SELECT UnitPrice FROM TerminalActivityTypes WHERE Id = @Id";
            float activityTypePrice = 0.0F;

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(queryCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@Id", activityType);
                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            if (!reader.IsDBNull(0))
                            {
                                activityTypePrice = (float)reader.GetDecimal(0);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtEx = new CmtApplicationException(ex, "GetActivityTypePrice failed", "");
                _logController.LogApplicationException(cmtEx);
            }

            return activityTypePrice;
        }

        /// <summary>
        /// Deletes a terminal using its macaddress. 
        /// </summary>
        /// <param name="macAddress"></param>
        /// <exception cref="NotImplementedException">If an unknown database result code is retrieved, an NotImplementedException will be thrown. In that case, just implement the return code.</exception>
        /// <returns>true if the deleting was succesful, false if the operation failed.</returns>
        public bool DeleteTerminal(string macAddress)
        {
            var outputParameter = new SqlParameter("ReturnValue", SqlDbType.Int) {Direction = ParameterDirection.Output};

            DeleteGeneric("exec @ReturnValue = cmtDeleteTerminal @macAddress", "DeleteTerminal",
                          new {ReturnValue = outputParameter, macAddress});

            var result = (int) outputParameter.Value;

            if (result == 0)
                return true;
            if (result == 1)
                return false; // not decommissioned
            if (result == 2)
                throw new Exception("An error occurred on the database. Please review the log for details.");

            throw new NotImplementedException("That result value for the deleteterminal procedure is not known.");
        }

        public bool DeleteEndUser(int endUserId)
        {
            var outputParameter = new SqlParameter("ReturnValue", SqlDbType.Int) {Direction = ParameterDirection.Output};

            DeleteGeneric("exec @ReturnValue = cmtDeleteEndUser2 @endUserId", "DeleteEndUser",
                          new {ReturnValue = outputParameter, endUserId});
            return (int) outputParameter.Value == 0;
        }

        public bool DeleteDistributor(int distributorId)
        {
            var outputParameter = new SqlParameter("ReturnValue", SqlDbType.Int) {Direction = ParameterDirection.Output};

            DeleteGeneric("exec @ReturnValue = cmtDeleteDistributor @distributorId", "DeleteDistributor",
                          new {ReturnValue = outputParameter, distributorId});
            return (int) outputParameter.Value == 0;
        }

        public bool DeleteOrganizations(int organizationId)
        {
            var outputParameter = new SqlParameter("ReturnValue", SqlDbType.Int) {Direction = ParameterDirection.Output};

            DeleteGeneric("exec @ReturnValue = cmtDeleteOrganization @organizationId", "DeleteOrganization",
                          new {ReturnValue = outputParameter, organizationId});
            return (int) outputParameter.Value == 0;
        }

        /// <summary>
        /// Delete a service pack
        /// </summary>
        /// <param name="slaId">The identifier of the service pack that should be deleted</param>
        /// <returns>True if the deletion was succesfull</returns>
        public bool DeleteServicePack(int slaId) 
        {
            SqlParameter outputParameter = new SqlParameter("ReturnValue", SqlDbType.Int) { Direction = ParameterDirection.Output };

            this.DeleteGeneric("EXECUTE @ReturnValue = cmtDeleteServicePack @slaId", "DeleteServicePack",
                new { ReturnValue = outputParameter, slaId });

            return (int)outputParameter.Value == 0;
        }

        /// <summary>
        /// Adds an ISP to a distributor
        /// </summary>
        /// <remarks>
        /// This method acts on the ISPDistributors table
        /// </remarks>
        /// <param name="distributorId">The distributor identifier</param>
        /// <param name="ispId">The ISP identifier</param>
        /// <returns>True if the ISP was successfully added to the Distributor</returns>
        public bool AddISPToDistributor(int distributorId, int ispId)
        {
            bool success = false;
            string sqlCmd = "INSERT INTO ISPDistributors (DistributorId, ISPId) VALUES (@distributorId, @ispId)";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(sqlCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@distributorId", distributorId);
                        cmd.Parameters.AddWithValue("@ispId", ispId);
                        cmd.ExecuteNonQuery();
                        success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: Error while executing AddISPToDistributor",
                                                                                   ""));
                throw;
            }

            return success;
        }

        /// <summary>
        /// This method de-allocates an ISP from a distributor
        /// </summary>
        /// <remarks>
        /// The corresponding tupple distributorId - IspId is removed from the ISPDistributors
        /// table.
        /// </remarks>
        /// <param name="distributorId">The distributor identifier</param>
        /// <param name="ispId">The ISP identifier</param>
        /// <returns>True if the de-allocation succeeded</returns>
        public bool RemoveISPFromDistributor(int distributorId, int ispId)
        {
            bool success = false;
            string sqlCmd = "DELETE FROM ISPDistributors WHERE distributorId = @distributorId AND ispId = @ispId";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(sqlCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@distributorId", distributorId);
                        cmd.Parameters.AddWithValue("@ispId", ispId);
                        cmd.ExecuteNonQuery();
                        success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                  "BOAccountingController: Error while executing RemoveISPFromDistributor",
                                                                                  ""));
                throw;
            }

            return success;
        }

        /// <summary>
        /// Returns a list of ISPs allocated to a distributor
        /// </summary>
        /// <remarks>
        /// This method uses the ISPDistributors table
        /// </remarks>
        /// <param name="distributorId">The target distributor identifier</param>
        /// <returns>A list of ISPs, this list can be empty</returns>
        public List<Isp> GetISPsForDistributor(int distributorId)
        {
            List<Isp> ispList = new List<Isp>();
            String sqlCmd = "SELECT ISPS.Id, ISPS.CompanyName, ISPS.Phone, ISPS.Fax, " +
                                "ISPS.Email, ISPS.Remarks, ISPS.dashboard_user, " +
                                "ISPS.dashboard_password, " +
                                "ISPS.ispif_user, ISPS.ispif_password, Addresses.AddressLine1, " +
                                "Addresses.AddressLine2, Addresses.Location, Addresses.PostalCode, " +
                                "Addresses.Country " +
                                "FROM ISPDistributors INNER JOIN " +
                                "ISPS ON ISPDistributors.ISPId = ISPS.Id INNER JOIN " +
                                "Addresses ON ISPS.AddressId = Addresses.Id " +
                                "WHERE (ISPDistributors.DistributorId = @distributorId)";

            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(sqlCmd, conn))
                {
                    cmd.Parameters.AddWithValue("@distributorId", distributorId);
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        Isp isp = new Isp();
                        Address addr = new Address();
                        isp.Id = reader.GetInt32(0);

                        if (!reader.IsDBNull(1))
                        {
                            isp.CompanyName = reader.GetString(1);
                        }

                        if (!reader.IsDBNull(2))
                        {
                            isp.Phone = reader.GetString(2);
                        }

                        if (!reader.IsDBNull(3))
                        {
                            isp.Fax = reader.GetString(3);
                        }

                        if (!reader.IsDBNull(4))
                        {
                            isp.Email = reader.GetString(4);
                        }

                        if (!reader.IsDBNull(5))
                        {
                            isp.Remarks = reader.GetString(5);
                        }

                        if (!reader.IsDBNull(6))
                        {
                            isp.Dashboard_User = reader.GetString(6);
                        }

                        if (!reader.IsDBNull(7))
                        {
                            isp.Dashboard_Password = reader.GetString(7);
                        }

                        if (!reader.IsDBNull(8))
                        {
                            isp.Ispif_User = reader.GetString(8);
                        }

                        if (!reader.IsDBNull(9))
                        {
                            isp.Ispif_Password = reader.GetString(9);
                        }

                        if (!reader.IsDBNull(10))
                        {
                            addr.AddressLine1 = reader.GetString(10);
                        }

                        if (!reader.IsDBNull(11))
                        {
                            addr.AddressLine2 = reader.GetString(11);
                        }

                        if (!reader.IsDBNull(12))
                        {
                            addr.Location = reader.GetString(12);
                        }

                        if (!reader.IsDBNull(13))
                        {
                            addr.PostalCode = reader.GetString(13);
                        }

                        if (!reader.IsDBNull(14))
                        {
                            addr.Country = reader.GetString(14);
                        }

                        isp.Address = addr;
                        ispList.Add(isp);
                    }
                }
            }

            return ispList;
        }


        /// <summary>
        /// Retrieves the volume information with the most recent timestamp. The return and forward values
        /// are taken from the VolumeHistoryDetail2 table. So the realtime (30 minutes, 1 minute) updated
        /// values.
        /// </summary>
        /// <param name="sitId">The Site Identifier</param>
        /// <param name="ispId">The ISP identifier</param>
        /// <returns>A VolumeInformationTime object which holds the lates values recorded in the CMT database</returns>
        public VolumeInformationTime GetLastRealTimeVolumeInformation(int sitId, int ispId)
        {
            try
            {
                const string sql =
                    @"
                        select TimeStamp, ForwardedVolume as [Forward], ReturnVolume as [Return] 
                        from vw_volume_detail2
                        where ispid = @ispId
                        and sitid = @sitId 
                        and timestamp = (select max(timestamp) from [vw_volume_detail2] where sitid = @sitId and ispId = @ispId)     
                ";

                using (var db = new Database(ConnectionString, DataProvider))
                {
                    List<VolumeInformationTime> dbObj = db.Fetch<VolumeInformationTime>(sql, new {ispId, sitId});
                    return dbObj.LastOrDefault();
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: Error while executing GetLastRealTimeVolumeInformation",
                                                                                   ""));
                throw;
            }
        }

        /// <summary>
        /// Retrieves the volume information with the most recent timestamp. The return and forward values
        /// are taken from the VolumeHistoryDetail2 table. So the realtime (30 minutes, 1 minute) updated
        /// values.
        /// This is the Cassandra version
        /// </summary>
        /// <param name="sitId">The Site Identifier</param>
        /// <param name="ispId">The ISP identifier</param>
        /// <returns>A VolumeInformationTime object which holds the lates values recorded in the CMT database</returns>
        public VolumeInformationTime GetLastRealTimeVolumeInformationCas(int sitId, int ispId)
        {
            try
            {
                DateTime nowMinX = DateTime.Now.AddMinutes(-1 * casTimeWindow); // subtract X minutes to create a window for the last entry
                VolumeDataEntry volDataEntry = _casClient.QueryDataDetail(ispId, sitId, nowMinX, DateTime.Now).LastOrDefault();
                VolumeInformationTime volInfoTime = new VolumeInformationTime();
                volInfoTime.TimeStamp = volDataEntry.time_stamp;
                volInfoTime.Forward = volDataEntry.forwardvolume;
                volInfoTime.Return = volDataEntry.returnvolume;
                return volInfoTime;
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: Error while executing GetLastRealTimeVolumeInformationCas",
                                                                                   ""));
                throw;
            }
        }

        /// <summary>
        /// Retrieves the accumulated return and forward volume information with the 
        /// most recent timestamp.
        /// </summary>
        /// <remarks>
        /// These values are retrieved from the view which holds the realtime updated 
        /// volume consumption information. The view used is vw_volume_detail2. This method is
        /// used by teh NMSVolumeDataService to calculate the delta volume consumption values.
        /// </remarks>
        /// <param name="sitId">The Site Identifier</param>
        /// <param name="ispId">The ISP Identifier</param>
        /// <returns>
        /// The VolumeInformationTime object which contains the latest forward and return
        /// accumulated values from the realtime volume consumption table
        /// </returns>
        public VolumeInformationTime GetLastRTAccumulatedVolumeInformation(int sitId, int ispId)
        {
            try
            {
                const string sql =
                    @"
                        select TimeStamp, AccumulatedForwardVolume as [Forward], AccumulatedReturnVolume as [Return] 
                        from vw_volume_detail2
                        where ispid = @ispId
                        and sitid = @sitId 
                        and timestamp = (select max(timestamp) from [vw_volume_detail2] where sitid = @sitId and ispId = @ispId)     
                ";

                using (var db = new Database(ConnectionString, DataProvider))
                {
                    List<VolumeInformationTime> dbObj = db.Fetch<VolumeInformationTime>(sql, new { ispId, sitId });
                    return dbObj.LastOrDefault();
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: Error while executing GetLastRTAccumulatedVolumeInformation",
                                                                                   ""));
                throw;
            }
        }

        /// <summary>
        /// retrieves the accumulated volume information with the most recent timestamp
        /// vw_volume_full refers to volumehistory2 and reflects the daily updated volume consumption
        /// This is the Cassandra version
        /// </summary>
        /// <param name="sitId">The SIT identifier</param>
        /// <param name="ispId">The ISP identifier</param>
        /// <returns></returns>
        public VolumeInformationTime GetLastAccumulatedVolumeInformationCas(int sitId, int ispId)
        {
            try
            {
                DateTime nowMinX = DateTime.Now.AddMinutes(-1 * casTimeWindow); // subtract X minutes to create a window for the last entry
                VolumeDataSummary volDataSummary = _casClient.QueryDataDaily(ispId, sitId, nowMinX, DateTime.Now).LastOrDefault();
                VolumeInformationTime volInfoTime = new VolumeInformationTime();
                volInfoTime.TimeStamp = Convert.ToDateTime(volDataSummary.date);
                volInfoTime.Forward = volDataSummary.accumulatedforwardvolume;
                volInfoTime.Return = volDataSummary.accumulatedreturnvolume;
                return volInfoTime;
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: Error while executing GetLastAccumulatedVolumeInformationCas",
                                                                                   ""));
                throw;
            }
        }

        /// <summary>
        /// retrieves the accumulated volume information with the most recent timestamp
        /// vw_volume_full refers to volumehistory2 and reflects the daily updated volume consumption
        /// </summary>
        /// <param name="sitId">The SIT identifier</param>
        /// <param name="ispId">The ISP identifier</param>
        /// <returns></returns>
        public VolumeInformationTime GetLastAccumulatedVolumeInformation(int sitId, int ispId)
        {
            try
            {
                const string sql =
                    @"
                        select timestamp, accumulatedforwardvolume as [Forward], accumulatedreturnvolume as [Return] 
                        from vw_volume_full3
                        where ispid = @ispId
                        and sitid = @sitId 
                        and timestamp = (select max(timestamp) from [vw_volume_full3] where sitid = @sitId and ispId = @ispId)
                    ";

                using (var db = new Database(ConnectionString, DataProvider))
                {
                    List<VolumeInformationTime> dbObj = db.Fetch<VolumeInformationTime>(sql, new {ispId, sitId});
                    return dbObj.LastOrDefault();
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: Error while executing GetLastAccumulatedVolumeInformation",
                                                                                   ""));
                throw;
            }
        }

        public VolumeInformationTime GetLastPriorityVolume(int sitId, int ispId)
        {
            try
            {
                const string sql =
                    @"
                        select timestamp, HighPriorityForwardedVolume as [Forward], HighPriorityReturnVolume as [Return] 
                        from vw_volume_full3 
                        where ispid = @ispId
                        and sitid = @sitId 
                        and timestamp = (select max(timestamp) from [vw_volume_full3] where sitid = @sitId and ispId = @ispId)
                    ";

                using (var db = new Database(ConnectionString, DataProvider))
                {
                    List<VolumeInformationTime> dbObj = db.Fetch<VolumeInformationTime>(sql, new {ispId, sitId});
                    return dbObj.LastOrDefault();
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: Error while executing GetLastPriorityVolume",
                                                                                   ""));
                throw;
            }
        }

        /// <summary>
        /// Fetches data from: VolumeHistoryDetailImport
        /// </summary>
        /// <param name="sitId"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns>The 30 minutes interval information for the given sitid</returns>
        public List<VolumeInformationTime> GetRealTimeVolumeInformation(int sitId, DateTime start, DateTime end, int ispId)
        {
            try
            {
                const string sql =
                    @"
                        select TimeStamp, ForwardedVolume as [Forward], ReturnVolume as [Return] 
                        from vw_volume_detail2 
                        where ispid = @ispId
                        and sitid = @sitId 
                        and timestamp between @start and @end 
                        order by timestamp
                    ";

                using (var db = new Database(ConnectionString, DataProvider))
                {
                    List<VolumeInformationTime> dbObj = db.Fetch<VolumeInformationTime>(sql, new {ispId, sitId, start, end});
                    return dbObj.ToList();
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: GetRealTimeVolumeInformation",
                                                                                   ""));
                throw;
            }
        }

        /// <summary>
        /// Fetches data from: VolumeHistoryDetailImport
        /// This is the Cassandra version
        /// </summary>
        /// <param name="sitId"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns>The 30 minutes interval information for the given sitid</returns>
        public List<VolumeInformationTime> GetRealTimeVolumeInformationCas(int sitId, DateTime start, DateTime end, int ispId)
        {
            try
            {
                List<VolumeDataEntry> volDataEntry = _casClient.QueryDataDetail(ispId, sitId, start, end);
                List<VolumeInformationTime> volInfoTime = new List<VolumeInformationTime>();
                VolumeInformationTime vi;
                foreach (VolumeDataEntry vd in volDataEntry)
                {
                    vi = new VolumeInformationTime();
                    vi.TimeStamp = vd.time_stamp;
                    vi.Forward = vd.forwardvolume;
                    vi.Return = vd.returnvolume;
                    volInfoTime.Add(vi);
                }
                return volInfoTime;
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: GetRealTimeVolumeInformationCas",
                                                                                   ""));
                throw;
            }
        }

        /// <summary>
        /// Fetches accumulated data from: VolumeHistoryDetailImport
        /// </summary>
        /// <param name="sitId"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="ispId"></param>
        /// <returns>The 30 minutes interval information for the given sitId</returns>
        public List<VolumeInformationTime> GetAccumulatedVolumeInformationFromRT(int sitId, DateTime start, DateTime end, int ispId)
        {
            try
            {
                const string sql =
                    @"
                        select TimeStamp, AccumulatedForwardVolume as [Forward], AccumulatedReturnVolume as [Return] 
                        from vw_volume_detail2 
                        where ispid = @ispId
                        and sitid = @sitId 
                        and timestamp between @start and @end 
                        order by timestamp
                    ";

                using (var db = new Database(ConnectionString, DataProvider))
                {
                    List<VolumeInformationTime> dbObj = db.Fetch<VolumeInformationTime>(sql, new { ispId, sitId, start, end });
                    return dbObj.ToList();
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: GetAccumulatedVolumeInformationFromRT",
                                                                                   ""));
                throw;
            }
        }

        /// <summary>
        /// Fetches accumulated data from: VolumeHistoryDetailImport
        /// This is the Cassandra version
        /// </summary>
        /// <param name="sitId"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="ispId"></param>
        /// <returns>The 30 minutes interval information for the given sitId</returns>
        public List<VolumeInformationTime> GetAccumulatedVolumeInformationFromRTCas(int sitId, DateTime start, DateTime end, int ispId)
        {
            try
            {
                List<VolumeDataEntry> volDataEntry = _casClient.QueryDataDetail(ispId, sitId, start, end);
                List<VolumeInformationTime> volInfoTime = new List<VolumeInformationTime>();
                VolumeInformationTime vi;
                foreach (VolumeDataEntry vd in volDataEntry)
                {
                    vi = new VolumeInformationTime();
                    vi.TimeStamp = vd.time_stamp;
                    vi.Forward = vd.accumulatedforwardvolume;
                    vi.Return = vd.accumulatedreturnvolume;
                    volInfoTime.Add(vi);
                }
                return volInfoTime;
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: GetAccumulatedVolumeInformationFromRTCas",
                                                                                   ""));
                throw;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sitId"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="ispId"> </param>
        /// <returns>The daily interval information for the given sitid</returns>
        public List<VolumeInformationTime> GetAccumulatedVolumeInformation(int sitId, DateTime start, DateTime end, int ispId)
        {
            try
            {
                const string sql =
                    @"

                    select timestamp, accumulatedforwardvolume as [Forward], accumulatedreturnvolume as [Return] 
                    from vw_volume_full3 
                    where ispid = @ispId
                    and sitid = @sitId
                    and timestamp between @start and @end 
                    order by timestamp

                    ";

                using (var db = new Database(ConnectionString, DataProvider))
                {
                    List<VolumeInformationTime> dbObj = db.Fetch<VolumeInformationTime>(sql, new {ispId, sitId, start, end});
                    return dbObj.ToList();
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: Error while executing GetAccumulatedVolumeInformation",
                                                                                   ""));
                throw;
            }
        }

        /// <summary>
        /// This is the Cassandra version
        /// </summary>
        /// <param name="sitId"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="ispId"> </param>
        /// <returns>The daily interval information for the given sitid</returns>
        public List<VolumeInformationTime> GetAccumulatedVolumeInformationCas(int sitId, DateTime start, DateTime end, int ispId)
        {
            try
            {
                List<VolumeDataSummary> volDataSummary = _casClient.QueryDataDaily(ispId, sitId, start, end);
                List<VolumeInformationTime> volInfoTime = new List<VolumeInformationTime>();
                VolumeInformationTime vi;
                foreach (VolumeDataSummary vd in volDataSummary)
                {
                    vi = new VolumeInformationTime();
                    vi.TimeStamp = Convert.ToDateTime(vd.date);
                    vi.Forward = vd.accumulatedforwardvolume;
                    vi.Return = vd.accumulatedreturnvolume;
                    volInfoTime.Add(vi);
                }
                return volInfoTime;
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: Error while executing GetAccumulatedVolumeInformationCas",
                                                                                   ""));
                throw;
            }
        }

        public List<VolumeInformationTime> GetPriorityVolumeInformation(int sitId, DateTime start, DateTime end, int ispId)
        {
            try
            {
                const string sql =
                    @"

                    select TimeStamp, HighPriorityForwardedVolume as [Forward], HighPriorityReturnVolume as [Return] 
                    from vw_volume_detail2 
                    where ispid = @ispId
                    and sitid = @sitId 
                    and timestamp between @start and @end 
                    order by timestamp

                    ";

                using (var db = new Database(ConnectionString, DataProvider))
                {
                    List<VolumeInformationTime> dbObj = db.Fetch<VolumeInformationTime>(sql, new {ispId, sitId, start, end});
                    return dbObj.ToList();
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: Error while executing GetPriorityVolumeInformation",
                                                                                   ""));
                throw;
            }
        }

        /// <summary>
        /// This is the Cassandra version
        /// </summary>
        /// <param name="sitId"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="ispId"></param>
        /// <returns></returns>
        public List<VolumeInformationTime> GetPriorityVolumeInformationCas(int sitId, DateTime start, DateTime end, int ispId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Returns the organization which is linked to the user
        /// </summary>
        /// <param name="userId">The userid here is the UID</param>
        /// <returns></returns>
        public Organization GetOrganizationForUser(string userId)
        {
            try
            {
                const string sql =
                    @"
                            select o.*, d.*, a.* 
                            from vw_organizations o 
                            join addresses a on a.id = o.addressid 
                            left join distributors d on o.distributorid = d.id
                            join OrganizationsUsers ou on ou.OrganizationId = o.id
                            where ou.userid = @userId
                    ";

                using (var db = new Database(ConnectionString, DataProvider))
                {
                    List<Organization> dbObj = db.Fetch<Organization, Distributor, Address, Organization>(
                        (i, d, a) =>
                        {
                            i.Address = a;
                            i.Distributor = d;
                            return i;
                        },
                        sql, new { userId });
                    return dbObj.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: Error while executing GetOrganizationForUser",
                                                                                   userId));
                throw;
            }
        }

//        /// <summary>
//        /// Returns the organization which is linked to the user
//        /// </summary>
//        /// <param name="userId">The userid here is the UID</param>
//        /// <returns></returns>
//        public Organization GetOrganizationForUser(string userId)
//        {
//            try
//            {
//                const string sql =
//                    @"
//                            select o.*, d.*, a.* 
//                            from vw_organizations3 o 
//                            join addresses a on a.id = o.addressid 
//                            left join distributors d on o.distributorid = d.id
//                            join OrganizationsUsers ou on ou.OrganizationId = o.id
//                            where ou.userid = @userId
//                    ";

//                using (var db = new Database(ConnectionString, DataProvider))
//                {
//                    List<Organization> dbObj = db.Fetch<Organization, Distributor, Address, Organization>(
//                        (i, d, a) =>
//                            {
//                                i.Address = a;
//                                i.Distributor = d;
//                                return i;
//                            },
//                        sql, new {userId});
//                    return dbObj.FirstOrDefault();
//                }
//            }
//            catch (Exception ex)
//            {
//                _logController.LogApplicationException(new CmtApplicationException(ex,
//                                                                                   "BOAccountingController: Error while executing GetOrganizationForUser",
//                                                                                   userId));
//                throw;
//            }
//        }

        /// <summary>
        /// Returns the service packs for a given ISP id
        /// </summary>
        /// <param name="ispId">The unique ISP identifier</param>
        /// <returns>A list of service levels (service packs)</returns>
        public List<ServiceLevel> GetServicePacksByIsp(int ispId)
        {
            try
            {
                const string sql =
                    @"

                    SELECT t.*  
                    FROM ServicePacks t 
                    WHERE t.Ispid = @ispId  AND t.Hide = 0 AND (t.Deleted IS NULL OR t.Deleted = 0)
                    ORDER BY t.SlaName
                    ";

                using (var db = new Database(ConnectionString, DataProvider))
                {
                    List<ServiceLevel> dbObj = db.Fetch<ServiceLevel>(sql, new {ispId});
                    return dbObj.ToList();
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: Error while executing GetServicePacks",
                                                                                   ""));
                throw;
            }
        }

        /// <summary>
        /// Resturns the servicepacks for a specific isp but includes the 
        /// servicepacks with the Hide flag set to true.
        /// </summary>
        /// <remarks>
        /// Use this method when retrieving the service packs for an ISP in the 
        /// NOCSA CMT
        /// </remarks>
        /// <param name="ispId">The ISP id</param>
        /// <returns>A list of SLAs belonging to the ISP with the hidden serivcepacks included</returns>
        public List<ServiceLevel> GetServicePacksByIspWithHide(int ispId)
        {
            try
            {
                const string sql =
                    @"

                    SELECT t.*  
                    FROM ServicePacks t 
                    WHERE t.Ispid = @ispId AND (t.Deleted IS NULL OR t.Deleted = 0)
                    ORDER BY t.SlaName
                    ";

                using (var db = new Database(ConnectionString, DataProvider))
                {
                    List<ServiceLevel> dbObj = db.Fetch<ServiceLevel>(sql, new { ispId });
                    return dbObj.ToList();
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: Error while executing GetServicePacks",
                                                                                   ""));
                throw;
            }
        }

        /// <summary>
        /// Returns true if the SLA is a business sla
        /// </summary>
        /// <param name="slaId">The SLA id as an integer</param>
        /// <returns>True if the SLA is a business sla</returns>
        public bool IsBusinessSLA(int slaId)
        {
            const string queryCmd = "SELECT Business FROM ServicePacks WHERE SlaId = @SlaId AND (Deleted IS NULL OR Deleted = 0)";
            bool isBusiness = false;

            using (var con = new SqlConnection(ConnectionString))
            {
                con.Open();
                try
                {
                    using (var command = new SqlCommand(queryCmd, con))
                    {
                        command.Parameters.AddWithValue("@SlaId", slaId);
                        SqlDataReader reader = command.ExecuteReader();

                        if (reader.Read())
                        {
                            isBusiness = reader.GetBoolean(0);
                        }
                    }
                }
                catch (Exception ex)
                {
                    //isBusiness = false; // commented because the throw below prevents the value from ever being returned anyway.
                    _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                       "BOAccountingController: Error while executing IsBusinessSLA method",
                                                                                       ""));
                    throw;
                }
            }

            return isBusiness;
        }

        /// <summary>
        /// Returns true if the SLA is a freezone sla
        /// </summary>
        /// <param name="slaId">The SLA id as an integer</param>
        /// <returns>True if the SLA is a freezone sla</returns>
        public bool IsFreeZoneSLA(int slaId)
        {
            const string queryCmd = "SELECT Freezone FROM ServicePacks WHERE SlaId = @SlaId AND (Deleted IS NULL OR Deleted = 0)";
            bool isFreeZone = false;

            using (var con = new SqlConnection(ConnectionString))
            {
                con.Open();
                try
                {
                    using (var command = new SqlCommand(queryCmd, con))
                    {
                        command.Parameters.AddWithValue("@SlaId", slaId);
                        SqlDataReader reader = command.ExecuteReader();

                        if (reader.Read())
                        {
                            isFreeZone = reader.GetBoolean(0);
                        }
                    }
                }
                catch (Exception ex)
                {
                    //isFreeZone = false; // commented because the throw below prevents the value from being returned anyway.
                    _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                       "BOAccountingController: Error while executing IsFreeZoneSLA method",
                                                                                       ""));
                    throw;
                }
            }

            return isFreeZone;
        }

        /// <summary>
        /// Returns true if the SLA is a voucher based sla
        /// </summary>
        /// <param name="slaId">The SLA id as an integer</param>
        /// <returns>True if the SLA is a voucher based sla</returns>
        public bool IsVoucherSLA(int slaId)
        {
            const string queryCmd = "SELECT Voucher FROM ServicePacks WHERE SlaId = @SlaId AND (Deleted IS NULL OR Deleted = 0)";
            bool isVoucherBased = false;

            using (var con = new SqlConnection(ConnectionString))
            {
                con.Open();
                try
                {
                    using (var command = new SqlCommand(queryCmd, con))
                    {
                        command.Parameters.AddWithValue("@SlaId", slaId);
                        SqlDataReader reader = command.ExecuteReader();

                        if (reader.Read())
                        {
                            isVoucherBased = reader.GetBoolean(0);
                        }
                    }
                }
                catch (Exception ex)
                {
                    //isVoucherBased = false; // commented because the throw below prevents the value from being returned
                    _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                       "BOAccountingController: Error while executing IsVoucher method",
                                                                                       ""));
                    throw;
                }
            }

            return isVoucherBased;
        }

        /// <summary>
        /// Returns true if the SitId is indeed unique
        /// </summary>
        /// <param name="sitId">The target SitId</param>
        /// <param name="ispId"> </param>
        /// <returns>True if the SitId is unique false otherwise</returns>
        public bool IsSitIdUnique(int sitId, int ispId)
        {
            return GetTerminalDetailsBySitId(sitId, ispId) == null;
        }

        private Terminal GetTerminalForQuery(string sql, params object[] args)
        {
            return GetTerminalListForQuery(sql, args).FirstOrDefault();
        }

        private IEnumerable<Terminal> GetTerminalListForQuery(string sql, params object[] args)
        {
            using (var db = new Database(ConnectionString, DataProvider))
            {
                List<Terminal> dbObj = db.Fetch<Terminal, Coordinate, Address, Terminal>(
                    (i, c, a) =>
                        {
                            i.LatLong = c;
                            i.Address = a;
                            return i;
                        },
                    sql, args);
                return dbObj;
            }
        }

        /// <summary>
        /// Returns the unique identifier for the given status as a String
        /// </summary>
        /// <remarks>
        /// If the status does not exist this method returns -1
        /// </remarks>
        /// <param name="status">The status as a string. This value must
        /// correspond with the value in the TerminalStatus table</param>
        /// <returns>The terminal status Id or -1 if non existing</returns>
        public int GetTerminalStatusId(string status)
        {
            const string selectQuery = "SELECT Id FROM TerminalStatus WHERE StatusDesc = @status";
            int statusId = -1;

            try
            {
                using (var conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();

                    using (var cmd = new SqlCommand(selectQuery, conn))
                    {
                        cmd.Parameters.AddWithValue("@status", status);
                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            statusId = reader.GetInt32(0);
                        }

                        reader.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: Error while executing GetTerminalStatusId",
                                                                                   ""));
                throw;
            }

            return statusId;
        }

        private void DeleteGeneric(string query, string loginfo, params object[] args)
        {
            try
            {
                _log.DebugFormat("{0}: Start", loginfo);
                using (var db = new Database(ConnectionString, DataProvider))
                {
                    int spResult = db.Execute(query, args);
                    _log.DebugFormat("Called {0} and returned " + spResult, loginfo);
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   string.Format(
                                                                                       "BOAccountingController: Error while executing {0}",
                                                                                       loginfo), String.Join(";", args), (short?)ExceptionLevelEnum.Debug));
                throw;
            }
        }

        public List<Terminal> GetTerminalsWithAdvancedQuery(AdvancedQueryParams queryParams)
        {
            string queryCmd = "SELECT FullName, MacAddress, SitId, IPAddress, SlaID, Serial, FirstActivationDate, AdmStatus, CNo, IspId FROM Terminals " +
                                "LEFT OUTER JOIN ISPS AS i ON Terminals.IspId = i.Id WHERE ";
            List<Terminal> terms = new List<Terminal>();
            bool hasParameters = false;

            if (queryParams.AdmState != -1)
            {
                 queryCmd = queryCmd + this.constructWhereClause(queryParams.AdmState, "AdmStatus", !hasParameters);
                 hasParameters = true;
            }

            if (queryParams.DistributorId != -1)
            {
                queryCmd = queryCmd + this.constructWhereClause(queryParams.DistributorId, "DistributorId", !hasParameters);
                hasParameters = true;
            }
            
            if (!queryParams.ExpDate.Equals(new DateTime(0L)))
            {
                string strExpiryDate = queryParams.ExpDate.Year + "-" + queryParams.ExpDate.Month + "-" +
                                       queryParams.ExpDate.Day + " " + queryParams.ExpDate.Hour + ":" +
                                       queryParams.ExpDate.Minute + ":" + queryParams.ExpDate.Second;
                
                queryCmd = queryCmd + this.constructWhereClause(strExpiryDate, "ExpiryDate", !hasParameters, queryParams.ExpDateOper);
                hasParameters = true;
            }

            if (queryParams.FullName != "")
            {
                queryCmd = queryCmd + this.constructWhereClause(queryParams.FullName, "FullName", !hasParameters);
                hasParameters = true;
            }

            if (queryParams.FwdPool != -1)
            {
                queryCmd = queryCmd + this.constructWhereClause(queryParams.FwdPool, "FWPool", !hasParameters);
                hasParameters = true;
            }

            if (queryParams.IpAddress != "")
            {
                queryCmd = queryCmd + this.constructWhereClause(queryParams.IpAddress, "IPAddress", !hasParameters);
                hasParameters = true;
            }

            if (queryParams.SatelliteId != -1)
            {
                queryCmd = queryCmd + this.constructWhereClause(queryParams.SatelliteId, "SatelliteId", !hasParameters);
                hasParameters = true;
            }

            if (queryParams.IspId != -1)
            {
                queryCmd = queryCmd + this.constructWhereClause(queryParams.IspId, "IspId", !hasParameters);
                hasParameters = true;
            }

            if (queryParams.MacAddress != "")
            {
                queryCmd = queryCmd + this.constructWhereClause(queryParams.MacAddress, "MacAddress", !hasParameters);
                hasParameters = true;
            }

            if (queryParams.RtnPool != -1)
            {
                queryCmd = queryCmd + this.constructWhereClause(queryParams.RtnPool, "RTPool", !hasParameters);
                hasParameters = true;
            }

            if (queryParams.SitId != -1)
            {
                queryCmd = queryCmd + this.constructWhereClause(queryParams.SitId, "Sitid", !hasParameters);
                hasParameters = true;
            }

            if (queryParams.SlaId != -1)
            {
                queryCmd = queryCmd + this.constructWhereClause(queryParams.SlaId, "SlaID", !hasParameters);
                hasParameters = true;
            }

            if (queryParams.FreeZone)
            {
                queryCmd = queryCmd + this.constructWhereClause(queryParams.FreeZone, "FreeZone", !hasParameters);
                hasParameters = true;
            }

            if (queryParams.EMail != "")
            {
                queryCmd = queryCmd + this.constructWhereClause(queryParams.EMail, "EMail", !hasParameters);
                hasParameters = true;
            }

            if (queryParams.Serial != "")
            {
                queryCmd = queryCmd + this.constructWhereClause(queryParams.Serial, "Serial", !hasParameters);
                hasParameters = true;
            }

            //SatDiversity Flags
            if (queryParams.RedundantSetup)
            {
                queryCmd = queryCmd + this.constructWhereClause(queryParams.RedundantSetup, "RedundantSetup", !hasParameters);
                queryCmd = queryCmd + this.constructWhereClause(true, "PrimaryTerminal", false);
                hasParameters = true;
            }

            //if (queryParams.PrimaryTerminal)
            //{
            //    queryCmd = queryCmd + this.constructWhereClause(queryParams.PrimaryTerminal, "PrimaryTerminal", !hasParameters);
            //    hasParameters = true;
            //}

            //Add test terminal flag.
            if (queryParams.TestTerminal)
            {
                queryCmd = queryCmd + this.constructWhereClause(queryParams.TestTerminal, "TestMode", !hasParameters);
                hasParameters = true;
            }

            // Add static IP flag
            if (queryParams.StaticIPFlag)
            {
                queryCmd = queryCmd + this.constructWhereClause(queryParams.StaticIPFlag, "StaticIPFlag", !hasParameters);
                hasParameters = true;
            }

            if (hasParameters)
            {
                queryCmd = queryCmd + " ORDER BY FullName";
                terms = this.queryTerminals(queryCmd);
            }

            return terms;
        }

        /// <summary>
        /// Returns a list of dates of when manual FUP Resets were made between a selected time period
        /// </summary>
        /// <param name="SitId">SitId</param>
        /// <param name="IspId">IspId</param>
        /// <param name="startDate">Beginning of time interval</param>
        /// <param name="endDate">End of time interval</param>
        /// <returns></returns>
        public List<DateTime> GetTerminalManualFupResets(int SitId, int IspId, DateTime startDate, DateTime endDate)
        {
            List<DateTime> fupResetList = new List<DateTime>();
            try
            {
                using (SqlConnection con = new SqlConnection(ConnectionString))
                {
                    con.Open();
                    using (SqlCommand command = new SqlCommand("SELECT TerminalActivityLog.ActionDate FROM TerminalActivityLog"
                        + " INNER JOIN Terminals AS Terminals_1 ON TerminalActivityLog.MacAddress = Terminals_1.MacAddress " +
                        "WHERE Terminals_1.SitId = @SitId AND Terminals_1.IspId = @IspId AND TerminalActivityLog.ActionDate >= @startDate AND TerminalActivityLog.ActionDate <= @endDate AND TerminalActivityLog.TerminalActivityTypeId = 700 ORDER BY TerminalActivityLog.ActionDate", con))
                    {
                        SqlParameter startDateParam = new SqlParameter();
                        startDateParam.ParameterName = "@startDate";
                        startDateParam.SqlDbType = SqlDbType.Date;
                        startDateParam.Value = startDate.Date;
                        command.Parameters.Add(startDateParam);
                        SqlParameter endDateParam = new SqlParameter();
                        endDateParam.ParameterName = "@endDate";
                        endDateParam.SqlDbType = SqlDbType.Date;
                        endDateParam.Value = endDate.Date;
                        command.Parameters.Add(endDateParam);
                        command.Parameters.AddWithValue("@SitId", SitId);
                        command.Parameters.AddWithValue("@IspId", IspId);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                fupResetList.Add(reader.GetDateTime(0));
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDesc = e.Message;
                cmtEx.ExceptionStacktrace = e.StackTrace;
                cmtEx.UserDescription = "BOAccountingController: GetTerminalManualFupResets";
                _logController.LogApplicationException(cmtEx);
            }
            return fupResetList;
        }

        /// <summary>
        /// Returns a list of all added voucher volume events made between a selected time period
        /// </summary>
        /// <param name="SitId">SitId</param>
        /// <param name="IspId">IspId</param>
        /// <param name="startDate">Beginning of time interval</param>
        /// <param name="endDate">End of time interval</param>
        /// <returns></returns>
        public List<DateTime> GetTerminalVolumeVoucherAdds(int SitId, int IspId, DateTime startDate, DateTime endDate)
        {
            List<DateTime> voucherAdds = new List<DateTime>();
            try
            {
                using (SqlConnection con = new SqlConnection(ConnectionString))
                {
                    con.Open();
                    using (SqlCommand command = new SqlCommand("SELECT TerminalActivityLog.ActionDate FROM TerminalActivityLog" + " INNER JOIN Terminals AS Terminals_1 ON TerminalActivityLog.MacAddress = Terminals_1.MacAddress " + "WHERE Terminals_1.SitId = @SitId AND Terminals_1.IspId = @IspId AND TerminalActivityLog.ActionDate >= @startDate AND TerminalActivityLog.ActionDate <= @endDate AND TerminalActivityLog.TerminalActivityTypeId = 1001 ORDER BY TerminalActivityLog.ActionDate", con))
                    {
                        SqlParameter startDateParam = new SqlParameter();
                        startDateParam.ParameterName = "@startDate";
                        startDateParam.SqlDbType = SqlDbType.Date;
                        startDateParam.Value = startDate.Date;
                        command.Parameters.Add(startDateParam);
                        SqlParameter endDateParam = new SqlParameter();
                        endDateParam.ParameterName = "@endDate";
                        endDateParam.SqlDbType = SqlDbType.Date;
                        endDateParam.Value = endDate.Date;
                        command.Parameters.Add(endDateParam);
                        command.Parameters.AddWithValue("@SitId", SitId);
                        command.Parameters.AddWithValue("@IspId", IspId);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                voucherAdds.Add(reader.GetDateTime(0));
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDesc = e.Message;
                cmtEx.ExceptionStacktrace = e.StackTrace;
                cmtEx.UserDescription = "BOAccountingController: GetTerminalVolumeVoucherAdds";
                _logController.LogApplicationException(cmtEx);
            }
            return voucherAdds;
        }

        /// <summary>
        /// Returns terminal activations
        /// </summary>
        /// <param name="SitId">SitId</param>
        /// <param name="IspId">IspId</param>
        /// <returns></returns>
        public List<DateTime> GetTerminalActivations(int SitId, int IspId)
        {
            List<DateTime> terminalActivations = new List<DateTime>();
            try
            {
                using (SqlConnection con = new SqlConnection(ConnectionString))
                {
                    con.Open();
                    using (SqlCommand command = new SqlCommand("SELECT TerminalActivityLog.ActionDate FROM TerminalActivityLog" 
                        + " INNER JOIN Terminals AS Terminals_1 ON TerminalActivityLog.MacAddress = Terminals_1.MacAddress " +
                        "WHERE Terminals_1.SitId = @SitId AND Terminals_1.IspId = @IspId AND TerminalActivityLog.TerminalActivityTypeId = 100 ORDER BY TerminalActivityLog.ActionDate", con))
                    {
                        command.Parameters.AddWithValue("@SitId", SitId);
                        command.Parameters.AddWithValue("@IspId", IspId);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                terminalActivations.Add(reader.GetDateTime(0));
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDesc = e.Message;
                cmtEx.ExceptionStacktrace = e.StackTrace;
                cmtEx.UserDescription = "BOAccountingController: GetTerminalActivation";
                _logController.LogApplicationException(cmtEx);
            }
            return terminalActivations;
        }

        /// <summary>
        /// Returns a datatable with the account rows.
        /// </summary>
        /// <remarks>
        /// Use this datatable as a source for Grids etc.
        /// </remarks>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public DataTable GetAccountReport(DateTime startDate, DateTime endDate)
        {
            string accountReportQuery = "SELECT Terminals.DistributorId, Distributors.FullName AS 'Distributor Name', TerminalActivityLog.ActionDate, " +
            "TerminalActivityLog.MacAddress, Terminals.FullName AS 'Terminal Name', Terminals.SitId, " +
            "TerminalActivityTypes.Action, TerminalActivityLog.SlaName FROM TerminalActivityLog, TerminalActivityTypes, Terminals, Distributors " +
            "WHERE ActionDate >= @StartDate AND ActionDate <= @EndDate " +
            "AND TerminalActivityLog.TerminalActivityTypeId = TerminalActivityTypes.Id " +
            "AND TerminalActivityTypes.Billable = 1 " +
            "AND TerminalActivityLog.MacAddress = Terminals.MacAddress " +
            "AND Terminals.DistributorId = Distributors.Id " +
            "ORDER BY Terminals.DistributorId, Terminals.MacAddress, TerminalActivityLog.ActionDate";

            DataTable accountReportData = new DataTable();
            SqlConnection conn = new SqlConnection(ConnectionString);
            SqlDataAdapter adapter = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand(accountReportQuery, conn);

            SqlParameter startDateParam = new SqlParameter();
            startDateParam.ParameterName = "@StartDate";
            startDateParam.SqlDbType = SqlDbType.Date;
            startDateParam.Value = startDate.Date;
            cmd.Parameters.Add(startDateParam);

            SqlParameter endDateParam = new SqlParameter();
            endDateParam.ParameterName = "@EndDate";
            endDateParam.SqlDbType = SqlDbType.Date;
            endDateParam.Value = endDate.Date;
            cmd.Parameters.Add(endDateParam);

            adapter.SelectCommand = cmd;

            conn.Open();

            try
            {
                adapter.Fill(accountReportData);
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDesc = ex.Message;
                cmtEx.ExceptionStacktrace = ex.StackTrace;
                _logController.LogApplicationException(cmtEx);
                accountReportData = null;
            }
            finally
            {
                conn.Close();
            }
         

            return accountReportData;
        }

        /// <summary>
        /// Returns a list of terminal activities for a given distributor
        /// </summary>
        /// <param name="startDate">The start date of the report</param>
        /// <param name="endDate">The end date of the report</param>
        /// <param name="DistributorId">The distributor Id</param>
        /// <returns></returns>
        public DataTable GetAccountReportByDistributor(DateTime startDate, DateTime endDate, int DistributorId)
        {
            string accountReportQuery = "SELECT Terminals.DistributorId, Distributors.FullName AS 'Distributor Name', TerminalActivityLog.ActionDate, " +
            "TerminalActivityLog.MacAddress, Terminals.FullName AS 'Terminal Name', Terminals.SitId, " +
            "TerminalActivityTypes.Action, TerminalActivityLog.SlaName FROM TerminalActivityLog, TerminalActivityTypes, Terminals, Distributors " +
            "WHERE ActionDate >= @StartDate AND ActionDate <= @EndDate " +
            "AND TerminalActivityLog.TerminalActivityTypeId = TerminalActivityTypes.Id " +
            "AND TerminalActivityTypes.Billable = 1 " +
            "AND TerminalActivityLog.MacAddress = Terminals.MacAddress " +
            "AND Terminals.DistributorId = Distributors.Id AND DistributorId = @DistributorId" +
            "ORDER BY Terminals.DistributorId, Terminals.MacAddress, TerminalActivityLog.ActionDate";

            DataTable accountReportData = new DataTable();
            SqlConnection conn = new SqlConnection(ConnectionString);
            SqlDataAdapter adapter = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand(accountReportQuery, conn);

            SqlParameter startDateParam = new SqlParameter();
            startDateParam.ParameterName = "@StartDate";
            startDateParam.SqlDbType = SqlDbType.Date;
            startDateParam.Value = startDate.Date;
            cmd.Parameters.Add(startDateParam);

            SqlParameter endDateParam = new SqlParameter();
            endDateParam.ParameterName = "@EndDate";
            endDateParam.SqlDbType = SqlDbType.Date;
            endDateParam.Value = endDate.Date;
            cmd.Parameters.Add(endDateParam);

            SqlParameter distIdParam = new SqlParameter();
            distIdParam.ParameterName = "@DistributorId";
            distIdParam.SqlDbType = SqlDbType.Int;
            distIdParam.Value = DistributorId;
            cmd.Parameters.Add(distIdParam);

            adapter.SelectCommand = cmd;

            conn.Open();

            try
            {
                adapter.Fill(accountReportData);
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDesc = ex.Message;
                cmtEx.ExceptionStacktrace = ex.StackTrace;
                _logController.LogApplicationException(cmtEx);
                accountReportData = null;
            }
            finally
            {
                conn.Close();
            }

            return accountReportData;
        }

        /// <summary>
        /// This method returns the MacAddress for the given system user
        /// </summary>
        /// <remarks>
        /// This method is especially used by the White Label CMT
        /// </remarks>
        /// <param name="UserId">The Guid of the logged in system user</param>
        /// <returns>The MacAddress if found or null if not found!</returns>
        public String GetMacAddressForSystemUser(Guid UserId)
        {
            String queryString = "SELECT MacAddress FROM SysUsersTerminals " +
                "WHERE UserId = @UserId";
            String macAddress = null;

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(queryString, conn))
                    {
                        cmd.Parameters.AddWithValue("@UserId", UserId);
                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.Read())
                        {
                            macAddress = reader.GetString(0);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.UserDescription = "GetMacAddressForSystemUser failed";
                cmtEx.ExceptionDateTime = DateTime.UtcNow;
                cmtEx.ExceptionDesc = ex.Message;
                cmtEx.ExceptionStacktrace = ex.StackTrace;
                cmtEx.ExceptionLevel = (short?)ExceptionLevelEnum.Error;
                _logController.LogApplicationException(cmtEx);
            }

            return macAddress;
        }

        /// <summary>
        /// Updates the MacAddress for the given user. If the user does not exist
        /// the record is created in the 
        /// </summary>
        /// <param name="SystemUserId"></param>
        /// <param name="MacAddress"></param>
        /// <returns></returns>
        public bool UpdateMacAddressForSystemUser(Guid SystemUserId, string MacAddress)
        {
            bool success = false;
            string storedProc = "cmtUpdateSysUsersTerminals";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();
                    
                    using (SqlCommand cmd = new SqlCommand(storedProc, conn))
                    {
                        cmd.Parameters.AddWithValue("@UserId", SystemUserId);
                        cmd.Parameters.AddWithValue("@MacAddress", MacAddress);
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.ExecuteNonQuery();
                        success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDateTime = DateTime.UtcNow;
                cmtEx.UserDescription = "UpdateMacAddressForSystemUser failed";
                cmtEx.ExceptionDesc = ex.Message;
                cmtEx.ExceptionStacktrace = ex.StackTrace;
                cmtEx.ExceptionLevel = (short?)ExceptionLevelEnum.Error;
                _logController.LogApplicationException(cmtEx);
            }

            return success;
        }

        /// <summary>
        /// Fetches all freezones in the CMT database
        /// </summary>
        /// <returns>The list of freezones</returns>
        public List<FreeZoneCMT> GetFreeZones()
        {
            try
            {
                List<FreeZoneCMT> freeZones = new List<FreeZoneCMT>();
                FreeZoneCMT freeZone;
                var var1 = new StringBuilder();
                var1.Append("SELECT FreeZoneId, FreeZoneName, CurrentActive, MonOn, MonOff, TueOn, TueOff, WedOn, WedOff, ThuOn, ThuOff, FriOn, FriOff, SatOn, SatOff, SunOn, SunOff \n");
                var1.Append("FROM FreeZones \n");

                using (SqlConnection con = new SqlConnection(ConnectionString))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(var1.ToString(), con))
                    {
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            // read data from CMT
                            freeZone = new FreeZoneCMT();
                            freeZone.Id = reader.GetInt32(0);
                            freeZone.Name = reader.GetString(1);
                            freeZone.CurrentActive = reader.GetByte(2);
                            freeZone.MonOn = reader.GetTimeSpan(3).ToString();
                            freeZone.MonOff = reader.GetTimeSpan(4).ToString();
                            freeZone.TueOn = reader.GetTimeSpan(5).ToString();
                            freeZone.TueOff = reader.GetTimeSpan(6).ToString();
                            freeZone.WedOn = reader.GetTimeSpan(7).ToString();
                            freeZone.WedOff = reader.GetTimeSpan(8).ToString();
                            freeZone.ThuOn = reader.GetTimeSpan(9).ToString();
                            freeZone.ThuOff = reader.GetTimeSpan(10).ToString();
                            freeZone.FriOn = reader.GetTimeSpan(11).ToString();
                            freeZone.FriOff = reader.GetTimeSpan(12).ToString();
                            freeZone.SatOn = reader.GetTimeSpan(13).ToString();
                            freeZone.SatOff = reader.GetTimeSpan(14).ToString();
                            freeZone.SunOn = reader.GetTimeSpan(15).ToString();
                            freeZone.SunOff = reader.GetTimeSpan(16).ToString();

                            freeZones.Add(freeZone);
                        }
                    }
                }

                return freeZones;
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: Error while executing GetFreeZones", ExceptionLevelEnum.Error.ToString()));
                throw;
            }
            
        }

        /// <summary>
        /// Updates the data of a freezone in the CMT database
        /// </summary>
        /// <param name="freeZone">The freezone to be updated</param>
        /// <returns>True if the operation succeeded</returns>
        public bool UpdateFreeZone(FreeZoneCMT freeZone)
        {
            bool result = false;

            string updateCmd =
                "UPDATE FreeZones "
                + "SET FreeZoneName = @Name, CurrentActive = @CurrentActive, MonOn = @MonOn, MonOff = @MonOff, TueOn = @TueOn, TueOff = @TueOff, WedOn = @WedOn, WedOff = @WedOff, ThuOn = @ThuOn, ThuOff = @ThuOff, FriOn = @FriOn, FriOff = @FriOff, SatOn = @SatOn, SatOff = @SatOff, SunOn = @SunOn, SunOff = @SunOff "
                + "WHERE FreeZoneId = @Id";

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                con.Open();
                try
                {
                    using (SqlCommand command = new SqlCommand(updateCmd, con))
                    {
                        command.Parameters.AddWithValue("@Id", freeZone.Id);
                        command.Parameters.AddWithValue("@Name", freeZone.Name);
                        command.Parameters.AddWithValue("@CurrentActive", freeZone.CurrentActive);
                        command.Parameters.AddWithValue("@MonOn", freeZone.MonOn);
                        command.Parameters.AddWithValue("@MonOff", freeZone.MonOff);
                        command.Parameters.AddWithValue("@TueOn", freeZone.TueOn);
                        command.Parameters.AddWithValue("@TueOff", freeZone.TueOff);
                        command.Parameters.AddWithValue("@WedOn", freeZone.WedOn);
                        command.Parameters.AddWithValue("@WedOff", freeZone.WedOff);
                        command.Parameters.AddWithValue("@ThuOn", freeZone.ThuOn);
                        command.Parameters.AddWithValue("@ThuOff", freeZone.ThuOff);
                        command.Parameters.AddWithValue("@FriOn", freeZone.FriOn);
                        command.Parameters.AddWithValue("@FriOff", freeZone.FriOff);
                        command.Parameters.AddWithValue("@SatOn", freeZone.SatOn);
                        command.Parameters.AddWithValue("@SatOff", freeZone.SatOff);
                        command.Parameters.AddWithValue("@SunOn", freeZone.SunOn);
                        command.Parameters.AddWithValue("@SunOff", freeZone.SunOff);

                        int numRows = command.ExecuteNonQuery();

                        if (numRows == 1)
                        {
                            result = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: Error while executing UpdateFreeZone", ExceptionLevelEnum.Error.ToString()));
                    throw;
                }
            }

            return result;
        }

        /// <summary>
        /// Creates a new freezone in the CMT database
        /// </summary>
        /// <param name="freeZone">The freezone to be created</param>
        /// <returns>True if the operation succeeded</returns>
        public bool CreateFreeZone(FreeZoneCMT freeZone)
        {
            bool result = false;

            string createCmd =
                "INSERT INTO FreeZones (FreeZoneId, FreeZoneName, CurrentActive, MonOn, MonOff, TueOn, TueOff, WedOn, WedOff, ThuOn, ThuOff, FriOn, FriOff, SatOn, SatOff, SunOn, SunOff)"
                + "VALUES (@Id, @Name, @CurrentActive, @MonOn, @MonOff, @TueOn, @TueOff, @WedOn, @WedOff, @ThuOn, @ThuOff, @FriOn, @FriOff, @SatOn, @SatOff, @SunOn, @SunOff)";


            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                con.Open();
                try
                {
                    using (SqlCommand command = new SqlCommand(createCmd, con))
                    {
                        command.Parameters.AddWithValue("@Id", freeZone.Id);
                        command.Parameters.AddWithValue("@Name", freeZone.Name);
                        command.Parameters.AddWithValue("@CurrentActive", freeZone.CurrentActive);
                        command.Parameters.AddWithValue("@MonOn", freeZone.MonOn);
                        command.Parameters.AddWithValue("@MonOff", freeZone.MonOff);
                        command.Parameters.AddWithValue("@TueOn", freeZone.TueOn);
                        command.Parameters.AddWithValue("@TueOff", freeZone.TueOff);
                        command.Parameters.AddWithValue("@WedOn", freeZone.WedOn);
                        command.Parameters.AddWithValue("@WedOff", freeZone.WedOff);
                        command.Parameters.AddWithValue("@ThuOn", freeZone.ThuOn);
                        command.Parameters.AddWithValue("@ThuOff", freeZone.ThuOff);
                        command.Parameters.AddWithValue("@FriOn", freeZone.FriOn);
                        command.Parameters.AddWithValue("@FriOff", freeZone.FriOff);
                        command.Parameters.AddWithValue("@SatOn", freeZone.SatOn);
                        command.Parameters.AddWithValue("@SatOff", freeZone.SatOff);
                        command.Parameters.AddWithValue("@SunOn", freeZone.SunOn);
                        command.Parameters.AddWithValue("@SunOff", freeZone.SunOff);

                        int numRows = command.ExecuteNonQuery();

                        if (numRows == 1)
                        {
                            result = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: Error while executing UpdateFreeZone", ExceptionLevelEnum.Error.ToString()));
                    throw;
                }
            }

            return result;
        }

        /// <summary>
        /// Returns the parameter value from the configuration table
        /// </summary>
        /// <param name="paramName"></param>
        /// <returns></returns>
        /*public string GetParameter(string paramName)
        {
            string paramValue = "";
            string queryCmd = "SELECT Value FROM Configuration WHERE ParamName = @ParamName";

            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                conn.Open();

                using (SqlCommand cmd = new SqlCommand(queryCmd, conn))
                {
                    cmd.Parameters.AddWithValue("@ParamName", paramName);
                    SqlDataReader reader = cmd.ExecuteReader();

                    if (reader.Read())
                    {
                        paramValue = reader.GetString(0);
                    }
                }
            }
            return paramValue;
        } */

        /// <summary>
        /// Updates the parameter in the configurations table
        /// </summary>
        /// <remarks>
        /// If the parameter does not exist is is created
        /// </remarks>
        /// <param name="paramName"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        /*public bool UpdateParameter(string paramName, string value)
        {
            string selectCmd = "SELECT COUNT(*) FROM Configuration WHERE ParamName = @ParamName";
            string updateCmd = "UPDATE Configuration SET Value = @Value WHERE ParamName = @ParamName";
            string insertCmd = "INSERT INTO Configuration (ParamName, Value) VALUES (@ParamName, @Value";

            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                conn.Open();
                SqlCommand checkParamCmd = new SqlCommand(selectCmd, conn);
                checkParamCmd.Parameters.AddWithValue("@ParamName", paramName);
                SqlDataReader reader = checkParamCmd.ExecuteReader();
                Boolean success = false;

                if (reader.Read())
                {
                    int numRows = reader.GetInt32(0);
                    reader.Close();

                    if (numRows == 0)
                    {
                        //Insert the new parameter
                        SqlCommand insertParamCmd = new SqlCommand(insertCmd, conn);
                        insertParamCmd.Parameters.AddWithValue("@ParamName", paramName);
                        insertParamCmd.Parameters.AddWithValue("@Value", value);
                        success = (insertParamCmd.ExecuteNonQuery() == 1);
                    }
                    else
                    {
                        //Update the parameter
                        SqlCommand updateParamCmd = new SqlCommand(updateCmd, conn);
                        updateParamCmd.Parameters.AddWithValue("@ParamName", paramName);
                        updateParamCmd.Parameters.AddWithValue("@Value", value);
                        success = (updateParamCmd.ExecuteNonQuery() == 1);
                    }
                }

                return success;
            }
        }*/

        /// <summary>
        /// Retrievs a specific VNO from the CMT database
        /// This method is currently not required
        /// </summary>
        /// <param name="VNOId">The ID of the VNO to be retrieved</param>
        /// <returns>The VNO object, can be null</returns>
        public VNO GetVNO(int VNOId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Creates/Updates a VNO entry in the CMT database
        /// This method is currently not required
        /// </summary>
        /// <param name="vno">The VNO object to be created/updated</param>
        /// <returns>True if the operation succeeded</returns>
        public bool UpdateVNO(VNO vno)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Retrieves all the distributor objects which are marked as VNOs from the CMT database
        /// </summary>
        /// <returns>A list of Distributor objects</returns>
        public List<Distributor> GetVNOs()
        {
            try
            {
                const string sql =
                    @"
                        select d.*, a.* 
                        from vw_VNOs d 
                        join addresses a on a.id = d.addressid order by d.FullName
                    ";

                using (var db = new Database(ConnectionString, DataProvider))
                {
                    List<Distributor> dbObj = db.Fetch<Distributor, Address, Distributor>(
                        (i, a) =>
                        {
                            i.Address = a;
                            return i;
                        },
                        sql);
                    return dbObj.ToList();
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: Error while executing GetVNOs",
                                                                                   ""));
                throw;
            }
        }

        /// <summary>
        /// Adds a URL in an NMS table through which the terminal can access certain functionalities
        /// The URLs are scrambled for the end-user and this method matches the clear-text URL with
        /// the scrambled one.
        /// Functionalities include traffic data and a speedtest
        /// Issue CMTBO-48
        /// </summary>
        /// <param name="clearURL">The URL in readable text</param>
        /// <param name="scrambledURL">The scrambled version of the URL</param>
        /// <returns>True if successful</returns>
        public bool AddTerminalNMSURL(string clearURL, string scrambledURL)
        {
            //Awaiting further info from NMS
            //return _nmsGatewayService.AddTerminalNMSURL(clearURL, scrambledURL);
            return true;
        }

        /// <summary>
        /// Gets a total amount of Volume used by a given Terminal on a given date. 
        /// By total Volume is meant: the sum of the TotalForwardedVolume column and the TotalReturnVolume in the VolumeHistory2 table.
        /// </summary>
        /// <param name="ispId">ispId of the Terminal</param>
        /// <param name="sitId">sitId of the Terminal</param>
        /// <param name="date">a date</param>
        /// <returns>a long integer or Int64</returns>
        public long GetTotalTerminalVolumeForDate(int ispId, int sitId, DateTime date)
        {
            long totalFrwdVolume = 0;
            long totalRtrnVolume = 0;
            long totalVolume = 0;
            int yy = date.Year;
            int mm = date.Month;
            int dd = date.Day;

            try
            {
                const string sqlQueryTotalFrwdVolume =
                    @"
                    select SUM(ForwardedVolume) FROM dbo.VolumeHistory2 
                    WHERE IspId = @ispId AND [SIT-ID] = @sitId 
                    AND (DATEPART(yy, time_stamp) = @yy and DATEPART(mm, time_stamp) = @mm and DATEPART(dd, time_stamp) = @dd)
                    ";

                const string sqlQueryTotalRtrnVolume =
                    @"
                    select SUM(ReturnVolume) FROM dbo.VolumeHistory2 
                    WHERE IspId = @ispId AND [SIT-ID] = @sitId 
                    AND (DATEPART(yy, time_stamp) = @yy and DATEPART(mm, time_stamp) = @mm and DATEPART(dd, time_stamp) = @dd)
                    ";

                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sqlQueryTotalFrwdVolume, conn))
                    {
                        cmd.Parameters.AddWithValue("@ispId", ispId);
                        cmd.Parameters.AddWithValue("@sitId", sitId);
                        cmd.Parameters.AddWithValue("@yy", yy);
                        cmd.Parameters.AddWithValue("@mm", mm);
                        cmd.Parameters.AddWithValue("@dd", dd);
                        totalFrwdVolume = Convert.ToInt64(cmd.ExecuteScalar());

                    }

                    using (SqlCommand cmd = new SqlCommand(sqlQueryTotalRtrnVolume, conn))
                    {
                        cmd.Parameters.AddWithValue("@ispId", ispId);
                        cmd.Parameters.AddWithValue("@sitId", sitId);
                        cmd.Parameters.AddWithValue("@yy", yy);
                        cmd.Parameters.AddWithValue("@mm", mm);
                        cmd.Parameters.AddWithValue("@dd", dd);
                        totalRtrnVolume = Convert.ToInt64(cmd.ExecuteScalar());
                    }
                    totalVolume = totalFrwdVolume + totalRtrnVolume;
                }
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.UserDescription = "GetTotalTerminalVolumeForDate failed";
                cmtEx.ExceptionDateTime = DateTime.UtcNow;
                cmtEx.ExceptionDesc = ex.Message;
                cmtEx.ExceptionStacktrace = ex.StackTrace;
                cmtEx.ExceptionLevel = (short?)ExceptionLevelEnum.Warning;
                _logController.LogApplicationException(cmtEx);
            }
            return totalVolume;
        }


        /// <summary>
        /// Returns the ServiceLevel object corresponding with the given 
        /// servicePackId
        /// </summary>
        /// <param name="servicePackName">The unique ServicePack identifier</param>
        /// <returns>The corresponding ServicePack or null if not found</returns>
        public ServiceLevel GetServicePackByName(string servicePackName)
        {
            const string queryCmd = "SELECT SlaID FROM ServicePacks WHERE SlaName = @SlaName";
            int slaId = 0;
            try
            {
                using (var conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();

                    using (var cmd = new SqlCommand(queryCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@SlaName", servicePackName);
                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            slaId = reader.GetInt32(0);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var boLogController = new BOLogController(ConnectionString, DataProvider);
                boLogController.LogApplicationException(new CmtApplicationException(ex,
                                                                                    "Error while retrieving ServiceLevel object for Sla: "
                                                                                    + servicePackName, "Error"));
            }

            return this.GetServicePack(slaId);
        }

        /// <summary>
        /// Gets the country with country code and country name with a given country code.
        /// Returns null when the country could not be found.
        /// </summary>
        /// <param name="code">The country code to get the country for</param>
        /// <returns>The country with its country name and country code, or null when it could not be found</returns>
        public Country GetCountryByCountryCode(string code)
        {
            try
            {
                const string sql =
                    @"
                        SELECT CountryCode, CountryName
                        FROM CountryISO
                        WHERE CountryCode = @code
                     ";

                using (var db = new Database(ConnectionString, DataProvider))
                {
                    return db.Fetch<Country>(sql, new { code }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: Error while executing GetCountryByCountryCode",
                                                                                   code));

                throw;
            }
        }

        /// <summary>
        /// Gets the full country name with a given country code.
        /// Returns null when the country could not be found.
        /// </summary>
        /// <param name="code">The country code to get the country name for</param>
        /// <returns>The country name of a country, or null when it could not be found</returns>
        public string GetCountryNameByCountryCode(string code)
        {
            try
            {
                return this.GetCountryByCountryCode(code).CountryName;
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: Error while executing GetCountryNameByCountryCode",
                                                                                   code));

                throw;
            }
        }

        /// <summary>
        /// Checks if a Terminal is the active Terminal in a redundant setup, by MacAddress
        /// </summary>
        /// <param name="macAddress"></param>
        /// <returns></returns>
        public bool IsSatDivTerminalActive(string macAddress)
        {
            return _nmsGatewayService.IsSatDivTerminalActive(macAddress);
        }


        #endregion

        #region Helper methods

        /// <summary>
        /// Constructs the where clause with the given parameters
        /// </summary>
        /// <param name="paramValue">The parameter value as an integer</param>
        /// <param name="colName">The Column name</param>
        /// <param name="isFirst">Indicates if this parameter is the fist</param>
        /// <param name="oper">The operand (< = >)</param>
        /// <returns>Part of the where clause</returns>
        protected string constructWhereClause(int paramValue, string colName, bool isFirst = false, string oper = "=")
        {
            string whereClause = "";

            if (paramValue != -1)
            {
                if (isFirst)
                {
                    whereClause = colName + " " + oper + " " + paramValue;
                }
                else
                {
                    whereClause = " AND " + colName + oper + " " + paramValue;
                }
            }

            return whereClause;
        }

             /// <summary>
        /// Constructs the where clause with the given parameters
        /// </summary>
        /// <param name="paramValue">The parameter value as an integer</param>
        /// <param name="colName">The Column name</param>
        /// <param name="isFirst">Indicates if this parameter is the fist</param>
        /// <param name="oper">The operand (< = >)</param>
        /// <returns>Part of the where clause</returns>
        protected string constructWhereClause(Boolean paramValue, string colName, bool isFirst=false, string oper="=")
        {
            string whereClause = "";
            short selectionParam = 0;

            if (paramValue)
                selectionParam = 1;
            else
                selectionParam = 0;
            if (paramValue)
            {
                if (isFirst)
                {
                    whereClause = "(" + colName + " IS NOT NULL AND " + colName + " != 0)";
                }
                else
                {
                    whereClause = " AND (" + colName + " IS NOT NULL AND " + colName + " != 0)";
                }
            }

            return whereClause;
        }

        /// <summary>
        /// Constructs the where clause with the given parameters
        /// </summary>
        /// <param name="paramValue">The parameter value as a string</param>
        /// <param name="colName">The column name</param>
        /// <param name="isFirst">Indicates if this parameters is the first in the WHERE clause</param>
        /// <param name="oper">The operand</param>
        /// <returns>Part of the where clause</returns>
        protected string constructWhereClause(string paramValue, string colName, bool isFirst=false, string oper="=")
        {
            if (paramValue.StartsWith("*") || paramValue.EndsWith("*"))
            {
                oper = "LIKE";
                paramValue = paramValue.Replace("*", "%");
            }

            string whereClause = "";

            if (paramValue != "")
            {
                if (isFirst)
                {
                    whereClause = colName + " " + oper + " '" + paramValue + "'";
                }
                else
                {
                    whereClause = " AND " + colName + " " + oper + " '" + paramValue + "'";
                }
            }

            return whereClause;
        }

        /// <summary>
        /// Fetches the terminals from the database.
        /// </summary>
        /// <remarks>
        /// This method is used by the GetTerminalsWithAdvancedQuery method
        /// </remarks>
        /// <param name="queryCmd">The Query Command as string</param>
        /// <returns>A list of matching terminals</returns>
        protected List<Terminal> queryTerminals(string queryCmd)
        {
            List<Terminal> termList = new List<Terminal>();
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(queryCmd, conn))
                    {
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            Terminal term = new Terminal();

                            if (reader.IsDBNull(0))
                            {
                                term.FullName = "";
                            }
                            else
                            {
                                term.FullName = reader.GetString(0);
                            }

                            term.MacAddress = reader.GetString(1);
                            term.SitId = reader.GetInt32(2);

                            if (reader.IsDBNull(3))
                            {
                                term.IpAddress = "";
                            }
                            else
                            {
                                term.IpAddress = reader.GetString(3);
                            }

                            term.SlaId = reader.GetInt32(4);

                            if (reader.IsDBNull(5))
                            {
                                term.Serial = "";
                            }
                            else
                            {
                                term.Serial = reader.GetString(5);
                            }

                            if (reader.IsDBNull(6))
                            {
                                term.StartDate = new DateTime(0L);
                            }
                            else
                            {
                                term.StartDate = reader.GetDateTime(6);
                            }

                            if (reader.IsDBNull(7))
                            {
                                term.AdmStatus = 0;
                            }
                            else
                            {
                                term.AdmStatus = reader.GetInt32(7);
                            }
                            term.CNo = reader.GetDecimal(8);
                            term.IspId= reader.GetInt32(9);
                            termList.Add(term);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtAppEx = new CmtApplicationException();
                cmtAppEx.ExceptionDesc = ex.Message;
                cmtAppEx.ExceptionStacktrace = ex.StackTrace;
                cmtAppEx.UserDescription = "Advanced query failed";
                _logController.LogApplicationException(cmtAppEx);
            }

            return termList;
        }

        /// <summary>
        /// Gets info concerning the BadDistributor flag, and passes it through to the NMS 
        /// </summary>
        /// <remarks>
        /// Both params received through DistributorDetailsForm from the front end
        /// </remarks>
        /// <param name="distId">Distributor ID</param>
        /// <param name="badDistributor">BadDistributor flag</param>
        public void BadDistributorChanged(int distId, bool badDistributor)
        {
            // Fill list with terminals belonging to wanted Distributor
            List<Terminal> terminals = this.GetTerminalsByDistributorId(distId); 
            
            foreach (Terminal term in terminals)
            {
                if (distId == term.DistributorId)
                {
                    string macAddress = term.MacAddress;
                    // Line to be uncommented after NMS Gateway Service has been updated
                    //_nmsGatewayService.ChangeBadDistributor(macAddress, badDistributor);
                }
            }
        }

        #endregion

        //These methods should be moved to a specialized controller
        #region White Label Theme support methods
        /// <summary>
        /// Creates or updates a White Label CMT theme. If the theme does not exist in
        /// the CMT the theme is created, otherwise the theme data is updated.
        /// </summary>
        /// <param name="theme">The theme to update or create</param>
        /// <returns>True if the operation succeeded</returns>
        public Boolean UpdateTheme(Theme theme)
        {
            Boolean result = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand("cmtUpdateWLThemes", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@DistId", theme.DistId);
                        cmd.Parameters.AddWithValue("@Theme", theme.ThemeName);
                        cmd.Parameters.AddWithValue("@IntroText", theme.IntroText);
                        cmd.Parameters.AddWithValue("@BackgroundColor", theme.BackgroundColor);

                        cmd.ExecuteNonQuery();
                        result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.UserDescription = ex.Message;
                cmtEx.StateInformation = "Error";
                cmtEx.ExceptionDateTime = DateTime.Now;
                cmtEx.ExceptionLevel = (short)ExceptionLevelEnum.Error;
                cmtEx.ExceptionStacktrace = ex.StackTrace;
                cmtEx.UserDescription = "BOAccountingController.UpdateTheme failed";
                _logController.LogApplicationException(cmtEx);
                result = false;
                
            }
            return result;
        }

        /// <summary>
        /// Returns a theme for the given distributor.
        /// </summary>
        /// <remarks>
        /// The result can be null if no theme is allocated to the given
        /// distributor
        /// </remarks>
        /// <param name="DistId">The distributor identifier</param>
        /// <returns>Returns the Theme connected to the given distributor. This return value
        /// can be null!</returns>
        public Theme GetThemeForDistributor(int DistId)
        {
            Theme theme = null;
            String queryString = "SELECT DistId, Theme, IntroText, BackgroundColor FROM WLThemes WHERE DistId = @DistId";
            ColorConverter colConv = new ColorConverter();

            try
            {
                this.DebugLog("Getting theme info for distributor: " + DistId, "GetThemeForDistributor");
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(queryString, conn))
                    {
                        cmd.Parameters.AddWithValue("@DistId", DistId);
                        SqlDataReader reader = cmd.ExecuteReader();
                        this.DebugLog("After ExecuteReader", "GetThemeForDistributor");
                        if (reader.Read())
                        {
                            theme = new Theme();
                            theme.DistId = DistId;
                            theme.ThemeName = reader.GetString(1);
                            if (!reader.IsDBNull(2))
                            {
                                theme.IntroText = reader.GetString(2);
                            }
                            else
                            {
                                theme.IntroText = "";
                            }

                            if (!reader.IsDBNull(3))
                            {
                                theme.BackgroundColor = reader.GetString(3).Trim();
                            }
                            else
                            {
                                theme.BackgroundColor = "FFFFFF";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionLevel = (short)ExceptionLevelEnum.Error;
                cmtEx.ExceptionDesc = ex.Message;
                cmtEx.StateInformation = "Error";
                cmtEx.UserDescription = "BOAccountingController.GetThemeForDistributor failed";
                cmtEx.ExceptionStacktrace = ex.StackTrace;
                cmtEx.ExceptionDateTime = DateTime.Now;
                _logController.LogApplicationException(cmtEx);
            }
            this.DebugLog("End of method", "GetThemeForDistributor");
            return theme;
        }
        #endregion

        internal void DebugLog(string msg, string method)
        {
            if (_debugFlag)
            {
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDesc = msg;
                cmtEx.ExceptionLevel = (short)ExceptionLevelEnum.Debug;
                cmtEx.ExceptionStacktrace = method;
                cmtEx.StateInformation = "Debug info";
                cmtEx.UserDescription = "Additional debug information";
                _logController.LogApplicationException(cmtEx);
            }
        }

        
        //SDPDEV-22
        public List<Satellite> GetSatellites()
        {
            try
            {
                const string sql =
                    @"
                        select * from Satellites
                    ";

                using (var db = new Database(ConnectionString, DataProvider))
                {
                    List<Satellite> dbObj = db.Fetch<Satellite>(sql);
                    return dbObj.ToList();
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: Error while executing GetSatellites",
                                                                                   ""));
                throw;
            }
        }

        //SDPDEV-23
        public List<Isp> GetISPsBySatellite(int satelliteId)
        {
            List<Isp> ispList = new List<Isp>();
            const string sqlCmd = "SELECT i.Id, i.CompanyName " +
                                    "FROM ISPS AS i INNER JOIN " +
                                    "Satellites AS s ON i.SatelliteId = s.ID " +
                                    "WHERE s.ID = @satelliteId";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sqlCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@satelliteId", satelliteId);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Isp isp = new Isp();

                                isp.Id = reader.GetInt32(0);

                                if (reader.IsDBNull(1))
                                {
                                    isp.CompanyName = null;
                                }
                                else
                                {
                                    isp.CompanyName = reader.GetString(1);
                                }

                                ispList.Add(isp);
                            }
                        }

                    }
                }

                return ispList;
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOAccountingController: Error while executing GetISPsBySatellite",
                                                                                   ""));
                throw;
            }
        }
    }
}