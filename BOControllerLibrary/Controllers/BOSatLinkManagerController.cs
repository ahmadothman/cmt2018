﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BOControllerLibrary.Controllers.Interfaces;
using BOControllerLibrary.Model;
using BOControllerLibrary.Model.Log;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Web.Services.Protocols;
using PetaPoco;
using log4net;
using BOControllerLibrary.SLMLinkInterfaceWSRef;
using BOControllerLibrary.SLMProvisioningInterfaceWSRef;
using BOControllerLibrary.SLMSessionInterfaceWSRef;
using BOControllerLibrary.ISPIF.Gateway;
using BOControllerLibrary.ISPIF.Model;

namespace BOControllerLibrary.Controllers
{
    /// <summary>
    /// This controller manages Bandwidth on Demand bookings, both in the CMT database
    /// as well as over the SatLink Manager interface provided by SES
    /// ISP's used are hard coded -200 for Astra2g Dialog -211 for SLM
    /// </summary>
    public class BOSatLinkManagerController : NMSGateway, IBOSatLinkManagerController
    {
        string ConnectionString = "";
        string DataProvider = "";
        string _virtualNetwork = "SatCorpAfrica";
        long _SLMHubTerminalID = (long)0;
        BOLogController _logController;
        BOAccountingController boaccountingcontrol;
        private readonly ILog _log = LogManager.GetLogger(typeof(IBOAccountingController));

        public BOSatLinkManagerController(string connectionString, string databaseProvider)
        {
            ConnectionString = connectionString;
            DataProvider = databaseProvider;
            _logController = new BOLogController(ConnectionString, DataProvider);
            boaccountingcontrol = new BOAccountingController(ConnectionString, DataProvider);
        }

        // CMTFO-190
        /// <summary>
        /// Gets the details of a reservation from the CMT database
        /// </summary>
        /// <param name="cmtId">The CMT ID of the reservation</param>
        /// <returns>An SLMBooking object</returns>
        public SLMBooking GetSLMBookingById(int cmtId)
        {
            SLMBooking booking = new SLMBooking();
            const string sqlCmd = @"
                                        SELECT * FROM SLMBookings
                                        WHERE CMTId = @CMTId
                                        ";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sqlCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@CMTId", cmtId);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                booking.CMTId = reader.GetInt32(0);
                                booking.DistributorId = reader.GetInt32(1);
                                booking.CustomerId = reader.GetInt32(2);
                                booking.TerminalMac = reader.GetString(3);
                                booking.StartTime = reader.GetDateTime(4);
                                if (!reader.IsDBNull(5))
                                {
                                    booking.EndTime = reader.GetDateTime(5);
                                }
                                booking.BookingSlaId = reader.GetInt32(6);
                                booking.TerminalSlaId = reader.GetInt32(7);
                                if (!reader.IsDBNull(8))
                                {
                                    booking.Type = reader.GetString(8);
                                }
                                if (!reader.IsDBNull(9))
                                {
                                    booking.Status = reader.GetString(9);
                                }
                                if (!reader.IsDBNull(10))
                                {
                                    booking.SLMId = reader.GetInt32(10);
                                }
                            }
                        }

                    }
                }

                return booking;
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOSatLinkManagerController: Error while executing GetSLMBookingsById",
                                                                                   ""));
                throw;
            }
        }

        // SATCORP-78
        /// <summary>
        /// Gets the details of the reservations requested by a specific distributor
        /// </summary>
        /// <param name="distributorId">The ID of the distributor in the CMT</param>
        /// <returns>A list of SLMBooking objects</returns>
        public List<SLMBooking> GetSLMBookingsByDistributor(int distributorId)
        {
            List<SLMBooking> distSLMBookings = new List<SLMBooking>();
            const string sqlCmd = @"
                                        SELECT * FROM SLMBookings
                                        WHERE DistributorId = @distributorId AND (Deleted IS NULL OR Deleted = 0)
                                        ";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sqlCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@distributorId", distributorId);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                SLMBooking booking = new SLMBooking();

                                booking.CMTId = reader.GetInt32(0);
                                booking.DistributorId = reader.GetInt32(1);
                                booking.CustomerId = reader.GetInt32(2);
                                booking.TerminalMac = reader.GetString(3);
                                booking.StartTime = reader.GetDateTime(4);
                                if (!reader.IsDBNull(5))
                                {
                                    booking.EndTime = reader.GetDateTime(5);
                                }
                                booking.BookingSlaId = reader.GetInt32(6);
                                booking.TerminalSlaId = reader.GetInt32(7);
                                if (!reader.IsDBNull(8))
                                {
                                    booking.Type = reader.GetString(8);
                                }
                                if (!reader.IsDBNull(9))
                                {
                                    booking.Status = reader.GetString(9);
                                }
                                if (!reader.IsDBNull(10))
                                {
                                    booking.SLMId = reader.GetInt32(10);
                                }

                                distSLMBookings.Add(booking);
                            }
                        }

                    }
                }

                return distSLMBookings;
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOSatLinkManagerController: Error while executing GetSLMBookingsByDistributor",
                                                                                   ""));
                throw;
            }
        }

        // SATCORP-78
        /// <summary>
        /// Gets the details of the reservations requested by a specific distributor
        /// </summary>
        /// <param name="customerId">The ID of the customer in the CMT</param>
        /// <returns>A list of SLMBooking objects</returns>
        public List<SLMBooking> GetSLMBookingsByCustomer(int customerId)
        {
            List<SLMBooking> orgSLMBookings = new List<SLMBooking>();
            const string sqlCmd = @"
                                        SELECT * FROM SLMBookings
                                        WHERE CustomerId = @customerId AND (Deleted IS NULL OR Deleted = 0)
                                        ";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sqlCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@customerId", customerId);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                SLMBooking booking = new SLMBooking();

                                booking.CMTId = reader.GetInt32(0);
                                booking.DistributorId = reader.GetInt32(1);
                                booking.CustomerId = reader.GetInt32(2);
                                booking.TerminalMac = reader.GetString(3);
                                booking.StartTime = reader.GetDateTime(4);
                                if (!reader.IsDBNull(5))
                                {
                                    booking.EndTime = reader.GetDateTime(5);
                                }
                                booking.BookingSlaId = reader.GetInt32(6);
                                booking.TerminalSlaId = reader.GetInt32(7);
                                if (!reader.IsDBNull(8))
                                {
                                    booking.Type = reader.GetString(8);
                                }
                                if (!reader.IsDBNull(9))
                                {
                                    booking.Status = reader.GetString(9);
                                }
                                if (!reader.IsDBNull(10))
                                {
                                    booking.SLMId = reader.GetInt32(10);
                                }

                                orgSLMBookings.Add(booking);
                            }
                        }

                    }
                }

                return orgSLMBookings;
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOSatLinkManagerController: Error while executing GetSLMBookingsByCustomer",
                                                                                   ""));
                throw;
            }
        }

        // SATCORP-78
        /// <summary>
        /// Fetches reservations from the CMT database based on status
        /// </summary>
        /// <param name="status">The status name</param>
        /// <returns>A list of SLMBooking objects</returns>
        public List<SLMBooking> GetSLMBookingsByStatus(string status)
        {
            List<SLMBooking> slmBookings = new List<SLMBooking>();
            const string sqlCmd = @"
                                        SELECT * FROM SLMBookings
                                        WHERE Status = @status AND (Deleted IS NULL OR Deleted = 0)
                                        ";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sqlCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@status", status);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                SLMBooking booking = new SLMBooking();

                                booking.CMTId = reader.GetInt32(0);
                                booking.DistributorId = reader.GetInt32(1);
                                booking.CustomerId = reader.GetInt32(2);
                                booking.TerminalMac = reader.GetString(3);
                                booking.StartTime = reader.GetDateTime(4);
                                if (!reader.IsDBNull(5))
                                {
                                    booking.EndTime = reader.GetDateTime(5);
                                }
                                booking.BookingSlaId = reader.GetInt32(6);
                                booking.TerminalSlaId = reader.GetInt32(7);
                                if (!reader.IsDBNull(8))
                                {
                                    booking.Type = reader.GetString(8);
                                }
                                if (!reader.IsDBNull(9))
                                {
                                    booking.Status = reader.GetString(9);
                                }
                                if (!reader.IsDBNull(10))
                                {
                                    booking.SLMId = reader.GetInt32(10);
                                }

                                slmBookings.Add(booking);
                            }
                        }

                    }
                }

                return slmBookings;
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOSatLinkManagerController: Error while executing GetSLMBookingsByStatus",
                                                                                   ""));
                throw;
            }
        }

        /// <summary>
        /// Fetches bookings from the CMT with a given start time
        /// </summary>
        /// <param name="startTime">The start time</param>
        /// <returns>A list of SLMBookings</returns>
        public List<SLMBooking> GetBookingsByStartTime(DateTime startTime)
        {
            List<SLMBooking> slmBookings = new List<SLMBooking>();
            const string sqlCmd = @"
                                        SELECT * FROM SLMBookings
                                        WHERE StartTime = @startDate 
                                        ";
            string start = startTime.ToString("yyyy-MM-dd HH:mm:ss");
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sqlCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@startDate", start);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                SLMBooking booking = new SLMBooking();

                                booking.CMTId = reader.GetInt32(0);
                                booking.DistributorId = reader.GetInt32(1);
                                booking.CustomerId = reader.GetInt32(2);
                                booking.TerminalMac = reader.GetString(3);
                                booking.StartTime = reader.GetDateTime(4);
                                if (!reader.IsDBNull(5))
                                {
                                    booking.EndTime = reader.GetDateTime(5);
                                }
                                booking.BookingSlaId = reader.GetInt32(6);
                                booking.TerminalSlaId = reader.GetInt32(7);
                                if (!reader.IsDBNull(8))
                                {
                                    booking.Type = reader.GetString(8);
                                }
                                if (!reader.IsDBNull(9))
                                {
                                    booking.Status = reader.GetString(9);
                                }
                                if (!reader.IsDBNull(10))
                                {
                                    booking.SLMId = reader.GetInt32(10);
                                }

                                slmBookings.Add(booking);
                            }
                        }

                    }
                }

                return slmBookings;
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOSatLinkManagerController: Error while executing GetBookingsByStartTime",
                                                                                   ""));
                throw;
            }
        }

        // CMTFO-191
        /// <summary>
        /// Fetches reservations from the CMT database which are active during the given period
        /// </summary>
        /// <param name="startDate">The first date in the period</param>
        /// <param name="endDate">The last date in the period</param>
        /// <returns>A list of SLMBooking objects</returns>
        public List<SLMBooking> GetSLMBookingsBetweenDates(DateTime startDate, DateTime endDate)
        {
            List<SLMBooking> slmBookings = new List<SLMBooking>();
            const string sqlCmd = @"
                                        SELECT * FROM SLMBookings
                                        WHERE (Deleted IS NULL OR Deleted = 0) 
                                        AND ((StartTime > @startDate AND StartTime < @endDate)
                                        OR (EndTime > @startDate AND EndTime < @endDate)
                                        OR (StartTime < @startDate AND EndTime > @endDate)) 
                                        ";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sqlCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@startDate", startDate);
                        cmd.Parameters.AddWithValue("@endDate", endDate);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                SLMBooking booking = new SLMBooking();

                                booking.CMTId = reader.GetInt32(0);
                                booking.DistributorId = reader.GetInt32(1);
                                booking.CustomerId = reader.GetInt32(2);
                                booking.TerminalMac = reader.GetString(3);
                                booking.StartTime = reader.GetDateTime(4);
                                if (!reader.IsDBNull(5))
                                {
                                    booking.EndTime = reader.GetDateTime(5);
                                }
                                booking.BookingSlaId = reader.GetInt32(6);
                                booking.TerminalSlaId = reader.GetInt32(7);
                                if (!reader.IsDBNull(8))
                                {
                                    booking.Type = reader.GetString(8);
                                }
                                if (!reader.IsDBNull(9))
                                {
                                    booking.Status = reader.GetString(9);
                                }
                                if (!reader.IsDBNull(10))
                                {
                                    booking.SLMId = reader.GetInt32(10);
                                }

                                slmBookings.Add(booking);
                            }
                        }

                    }
                }

                return slmBookings;
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOSatLinkManagerController: Error while executing GetSLMBookingsBetweenDates",
                                                                                   ""));
                throw;
            }
        }

        // SATCORP-78
        /// <summary>
        /// Updates a reservation in the CMT database
        /// </summary>
        /// <param name="booking">The SLMBooking object</param>
        /// <returns>True if successful</returns>
        public bool UpdateSLMBooking(SLMBooking booking)
        {
            try
            {
                using (var db = new Database(ConnectionString, DataProvider))
                {
                    var returnValue = new SqlParameter("returnValue", SqlDbType.Int) { Direction = ParameterDirection.Output };

                    db.Execute(
                        "exec [cmtUpdateSLMBooking] @CMTId,@DistributorId,@CustomerId,@TerminalMac,@StartTime,@EndTime,@BookingSlaId,@TerminalSlaId,@Type,@Status,@SLMId,@returnValue OUTPUT",
                        new
                        {
                            booking.CMTId,
                            booking.DistributorId,
                            booking.CustomerId,
                            booking.TerminalMac,
                            booking.StartTime,
                            booking.EndTime,
                            booking.BookingSlaId,
                            booking.TerminalSlaId,
                            booking.Type,
                            booking.Status,
                            booking.SLMId,
                            returnValue
                        }
                        );
                    var spResult = (int)returnValue.Value;
                    if (spResult == 0 || spResult == 1)
                        return true;
                    return false;
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   "BOSatLinkManagerController: Error while executing UpdateSLMBooking",
                                                                                   booking.ToString(), (short?)ExceptionLevelEnum.Critical));
                return false;
            }
        }

        

        /// <summary>
        /// Flags a reservation as deleted in the CMT database
        /// </summary>
        /// <param name="CMTId">The CMT ID of the resevation</param>
        /// <returns>True if successful</returns>
        public bool DeleteSLMBooking(int CMTId)
        {
            var outputParameter = new SqlParameter("ReturnValue", SqlDbType.Int) { Direction = ParameterDirection.Output };

            DeleteGeneric("exec @ReturnValue = cmtDeleteSLMBooking @CMTId", "DeleteSLMBooking",
                          new { ReturnValue = outputParameter, CMTId });
            return (int)outputParameter.Value == 0;
        }

        /// <summary>
        /// A generic deletion method
        /// </summary>
        /// <param name="query"></param>
        /// <param name="loginfo"></param>
        /// <param name="args"></param>
        private void DeleteGeneric(string query, string loginfo, params object[] args)
        {
            try
            {
                _log.DebugFormat("{0}: Start", loginfo);
                using (var db = new Database(ConnectionString, DataProvider))
                {
                    int spResult = db.Execute(query, args);
                    _log.DebugFormat("Called {0} and returned " + spResult, loginfo);
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                                                                   string.Format(
                                                                                       "BOSatLinkManagerController: Error while executing {0}",
                                                                                       loginfo), String.Join(";", args), (short?)ExceptionLevelEnum.Debug));
                throw;
            }
        }

        /// <summary>
        /// Creates an SLM booking in the hub
        /// </summary>
        /// <param name="CMTId">The CMT ID of the reservation</param>
        /// <returns>A text string with a specific error message in case the booking failed</returns>
        public string CreateSLMBookingInHub(int CMTId)
        {
            string result = "";
            SLMBooking cmtBooking = this.GetSLMBookingById(CMTId);
            long terminalSLMID = this.GetTerminalID(cmtBooking.TerminalMac);

            //Create a session request for the booking
            SessionInterface si = new SessionInterface();
            //ProvisioningInterface pi = new ProvisioningInterface();
            sessionRequest toBook = new sessionRequest();
            
            //TODO create some kind of naming convention for the session bookings
            toBook.serviceName = cmtBooking.CMTId + "-" + "Terminal-" + terminalSLMID;
            
            toBook.startTime = cmtBooking.StartTime.ToString("yyyy-MM-dd HH:mm:ss");
            toBook.endTime = ((DateTime)cmtBooking.EndTime).ToString("yyyy-MM-dd HH:mm:ss");
            toBook.virtualNetwork = _virtualNetwork;
            toBook.sessionProfile = this.GetSessionForBooking(cmtBooking);
            string[] profileName = toBook.sessionProfile.Split(' ');
            
            //Define the FWD link
            linkEndPoints lepFWD = new linkEndPoints();
            destination destFWD = new destination();
            destFWD.terminal = terminalSLMID;
            destFWD.demodulator = terminalSLMID;
            lepFWD.destination = new destination[1];
            lepFWD.destination[0] = destFWD;
            lepFWD.source = new source();
            lepFWD.source.terminal = _SLMHubTerminalID;
            lepFWD.source.modulator = _SLMHubTerminalID;
            if (profileName[2] == "FW")
            {
                toBook.link = new linkEndPoints[] { lepFWD };
            }    
            //Optionally define the RTN link. This is decided based on the session profile which has been selected
            else
            {
                linkEndPoints lepRTN = new linkEndPoints();
                destination destRTN = new destination();
                destRTN.terminal = _SLMHubTerminalID;
                destRTN.demodulator = _SLMHubTerminalID;
                lepRTN.destination = new destination[1];
                lepRTN.destination[0] = destRTN;
                lepRTN.source = new source();
                lepRTN.source.terminal = terminalSLMID;
                lepRTN.source.modulator = terminalSLMID;
                toBook.link = new linkEndPoints[] { lepFWD, lepRTN };
                
            }

            try
            {
                //Book&Start the session
                bookAndStartSessionRequest toBookAndStart = new bookAndStartSessionRequest();
                toBookAndStart.sessionRequest = toBook;
                toBookAndStart.connectReceivers = true;
                session response = si.bookAndStartSession(toBookAndStart);
                cmtBooking.SLMId = Convert.ToInt32(response.id);
                cmtBooking.Status = "Booked";
                this.UpdateSLMBooking(cmtBooking);
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            
            return result;
        }

        /// <summary>
        /// This method will cancel a future booking in the SLM server
        /// /// </summary>
        /// <param name="booking">An SLMBoooking object</param>
        /// <returns>True if successfult</returns>
        public bool CancelSLMBooking(SLMBooking booking)
        {
            bool result = false;
            SessionInterface si = new SessionInterface();
            session confirmation;
            if (booking.StartTime > DateTime.UtcNow)
            {
                try
                {
                    confirmation = si.cancelSession((long)booking.SLMId);
                    //TODO further analysis an reaction to the response in case it is unsuccessful
                    result = true;
                }
                catch (Exception ex)
                {
                    result = false;
                } 
            }

            return result;
        }

        /// <summary>
        /// This method starts a booking in the hub and sets the correct parameters in the CMT and the NMS
        /// </summary>
        /// <param name="booking">An SLMBoooking object</param>
        /// <returns>True if successfult</returns>
        public bool StartBooking(SLMBooking booking)
        {
        //    SessionInterface si = new SessionInterface();
        //    startSessionRequest toStart = new startSessionRequest();
        //    toStart.sessionId = (long)booking.SLMId;
        //    try
        //    {
        //        session confirmation = si.startSession(toStart);

                bool result = this.insertRequestTicketWithSla(booking.TerminalMac, AdmStatus.OPERATIONAL, AdmStatus.OPERATIONAL, TerminalActivityTypes.ChangeSla, 211, booking.BookingSlaId, Guid.Empty);
                if (result)
                {
                booking.Status = "Started";
                this.UpdateSLMBooking(booking);
                return this.NMSTerminalChangeSla(boaccountingcontrol.GetTerminalDetailsByMAC(booking.TerminalMac).ExtFullName, booking.TerminalMac, booking.BookingSlaId, AdmStatus.OPERATIONAL, 211, Guid.Empty);
                
                }
            
                return result;
            //}
            //catch (Exception ex)
            //{
            //    return false;
            //}
        }


        /// <summary>
        /// This method resets the SLA in the NMS once the SLM session is finised
        /// </summary>
        /// <param name="booking"></param>
        /// <returns></returns>
        public bool StopBooking(SLMBooking booking)
        {
            bool result = this.insertRequestTicketWithSla(booking.TerminalMac, AdmStatus.OPERATIONAL, AdmStatus.OPERATIONAL, TerminalActivityTypes.ChangeSla, 201, booking.TerminalSlaId, Guid.Empty);
            if (result)
            {
                booking.Status = "Finished";
                this.UpdateSLMBooking(booking);
                return this.NMSTerminalChangeSla(boaccountingcontrol.GetTerminalDetailsByMAC(booking.TerminalMac).ExtFullName, booking.TerminalMac, booking.TerminalSlaId, AdmStatus.OPERATIONAL, 201, Guid.Empty);

            }
            
            
            return result;
        }

        /// <summary>
        /// Fetches a SessionProfile object from the SLM server
        /// </summary>
        /// <param name="cmtBooking">The CMT booking object</param>
        /// <returns>The name of the SessionProfile as a string</returns>
        private string GetSessionForBooking(SLMBooking cmtBooking)
        {
            ProvisioningInterface pi = new ProvisioningInterface();
            sessionProfile[] profiles;
            try
            {
                profiles = pi.getAllSessionProfiles(new vnId());
                string slaname = boaccountingcontrol.GetServicePack(cmtBooking.BookingSlaId).SlaCommonName.Trim();
                foreach (sessionProfile sp in profiles)
                {
                    if (sp.name == slaname)
                        return sp.name;
                }
            }
            catch (Exception ex)
            {
                
                throw;
            }
            return null;
        }

        /// <summary>
        /// Matches the CMT terminal information with an SLM terminal ID
        /// </summary>
        /// <param name="mac">The MAC address of the terminal as stored in the CMT</param>
        /// <returns>The SLM terminal ID</returns>
        private long GetTerminalID(string mac)
        {
            
            
            ProvisioningInterface pi = new ProvisioningInterface();
            terminal[] slmTerminals = pi.getAllTerminals(new vnId());
            BOControllerLibrary.Model.Terminal.Terminal term = boaccountingcontrol.GetTerminalDetailsByMAC(mac);
            foreach(terminal t in slmTerminals)
            { if (t.name == term.ExtFullName)
                    return t.terminalId;
            }
            
            return (long)-1;
        }


        private bool insertRequestTicketWithSla(string macAddress, AdmStatus sourceAdmStatus, AdmStatus targetAdmStatus,
                                           TerminalActivityTypes termActivityType, int ispId, int newSla, Guid userId)
        {
            TicketDataMgr ticketDataMgr = new TicketDataMgr();
            Guid ticketId = Guid.NewGuid();
            return ticketDataMgr.insertTicket(ticketId.ToString(), macAddress, sourceAdmStatus, targetAdmStatus,
                                            0, "", "", termActivityType, ispId, 0, newSla, userId);
        }
    }
}

//The following code is necessary for the type of authentication used by the SOAP service provided by Newtec
//The same piece of code must be repeated for each interface used
namespace BOControllerLibrary.SLMLinkInterfaceWSRef
{

    //Note the use of the partial class and the CustomHeaderExtensionAttribute.
    //This allows us to add the CustomHeaderExtension
    //without changing the existing proxy code, which allows us to 
    //apply the attribute to just a single method on the proxy.
    public partial class LinkInterface : SoapHttpClientProtocol
    {
        private static string _username = "SatADSL";
        private static string _password = "S@t4dS1";
        private static string _authValue = "";

        protected override WebRequest GetWebRequest(Uri uri)
        {
            _authValue = Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(_username + ":" + _password));
            System.Net.WebRequest request = base.GetWebRequest(uri);
            request.Headers.Add("Authorization", String.Format("Basic {0}", _authValue));
            return request;
        }

    }
}

namespace BOControllerLibrary.SLMProvisioningInterfaceWSRef
{

    //Note the use of the partial class and the CustomHeaderExtensionAttribute.
    //This allows us to add the CustomHeaderExtension
    //without changing the existing proxy code, which allows us to 
    //apply the attribute to just a single method on the proxy.
    public partial class ProvisioningInterface : SoapHttpClientProtocol
    {
        private static string _username = "SatADSL";
        private static string _password = "S@t4dS1";
        private static string _authValue = "";

        protected override WebRequest GetWebRequest(Uri uri)
        {
            _authValue = Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(_username + ":" + _password));
            System.Net.WebRequest request = base.GetWebRequest(uri);
            request.Headers.Add("Authorization", String.Format("Basic {0}", _authValue));
            return request;
        }

    }
}

namespace BOControllerLibrary.SLMSessionInterfaceWSRef
{

    //Note the use of the partial class and the CustomHeaderExtensionAttribute.
    //This allows us to add the CustomHeaderExtension
    //without changing the existing proxy code, which allows us to 
    //apply the attribute to just a single method on the proxy.
    public partial class SessionInterface : SoapHttpClientProtocol
    {
        private static string _username = "SatADSL";
        private static string _password = "S@t4dS1";
        private static string _authValue = "";

        protected override WebRequest GetWebRequest(Uri uri)
        {
            _authValue = Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(_username + ":" + _password));
            System.Net.WebRequest request = base.GetWebRequest(uri);
            request.Headers.Add("Authorization", String.Format("Basic {0}", _authValue));
            return request;
        }

    }
}