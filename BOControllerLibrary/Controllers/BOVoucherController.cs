﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using BOControllerLibrary.Model;
using BOControllerLibrary.Controllers.Interfaces;
using BOControllerLibrary.Model.Voucher;
using log4net;
using net.nimera.CRCCalculator;
using BOControllerLibrary.Model.Log;


namespace BOControllerLibrary.Controllers
{
    /// <summary>
    /// <p>Contains methods for generating and managing vouchers and voucher batches.
    /// Vouchers are created in the context of a voucher batch, a set of vouchers. Before
    /// a distributor can access the newly created vouchers, a voucher batch needs to be
    /// released. This is done by the NOCSA, after the vouchers have been paid for by the
    /// distributor. Once a voucher is sold to an end-user, he or she needs to enter the code in 
    /// the CMT by means of the VBAddVolume.aspx page of the VOBasedWebApp web application.</p>
    /// <p>The voucher code consists of 13 hexadecimal characters and a 4 character CRC.</p>
    /// <p>The NOCSA has the possibility to validate a voucher or make it available again.</p>
    /// <p>The following conventions are used througout the code:
    ///     <ul>
    ///     <li>Validate a voucher - much like validating a railway ticket, the voucher is made
    ///     unavailable for use. Validation is done by entering the code in the Add Volume page or
    ///     if manually validated by the NOCSA.</li>
    ///     <li>Reset a voucher - if by accident a voucher has been validated the NOCSA is able to
    ///     reset the voucher again. After a reset a voucher is available again and can be validated as
    ///     explained in the previous section.
    ///     </li>
    ///     <li>Releasing a voucher - this is done by the NOCSA after the voucher batch has been paid by the
    ///     distributor. Only then the distributor can download the voucher batch and distribute it to its
    ///     customers</li>
    ///     </ul>
    /// </p>
    /// </summary>
    public class BOVoucherController : IBOVoucherController
    {
        const int NUM_DIGITS = 13;
        string _connectionString = "";
        string _databaseProvider = "";

        private readonly ILog _log = LogManager.GetLogger(typeof(IBOAccountingController));
        private readonly IBOLogController _logController;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="databaseProvider"></param>
        public BOVoucherController(string connectionString, string databaseProvider)
        {
            _connectionString = connectionString;
            _databaseProvider = databaseProvider;
            _logController = new BOLogController(connectionString, databaseProvider);
        }

        #region IBOVoucherController Members

        /// <summary>
        /// Returns a list of available (valid) vouchers for a given
        /// distributor.
        /// </summary>
        /// <param name="distributorId">The distributor identifier.</param>
        /// <returns>A list of available vouchers. This list can be empty.</returns>
        public List<Model.Voucher.Voucher> GetAvailableVouchers(int distributorId)
        {
            string selectCmd = "SELECT Vouchers.Code, Vouchers.Crc, Vouchers.CrcSeed, Vouchers.Available, Vouchers.BatchId, Vouchers.Sequence FROM " +
                               "VoucherBatches INNER JOIN Vouchers ON VoucherBatches.BatchId = Vouchers.BatchId " +
                               "WHERE VoucherBatches.DistributorId = @DistributorId AND " +
                               "Vouchers.Available = 1 ORDER BY Vouchers.BatchId, Vouchers.Sequence";
            List<Voucher> vouchers = null;

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(selectCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@DistributorId", distributorId);
                        SqlDataReader reader = cmd.ExecuteReader();
                        vouchers = new List<Voucher>();
                        while (reader.Read())
                        {
                            Voucher v = new Voucher();
                            v.Code = reader.GetString(0);
                            v.Crc = (ushort)reader.GetInt32(1);
                            v.CrcSeed = (ushort)reader.GetInt32(2);
                            v.Available = reader.GetBoolean(3);
                            v.BatchId = reader.GetInt32(4);
                            vouchers.Add(v);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOVoucherController: Error while returning valid vouchers for a distributor", distributorId.ToString()));
                throw;
            }

            return vouchers;
        }


        /// <summary>
        /// Returns a list of VoucherBatchInfo objects for a given distributor. The list contains
        /// only released voucher batches
        /// </summary>
        /// <param name="distributorId">The distributor identifier</param>
        /// <returns>A list of VoucherBatchInfo objects</returns>
        public List<VoucherBatchInfo> GetReleasedVoucherBatches(int distributorId)
        {
            //string selectCmd = "SELECT VoucherBatches.BatchId, VoucherBatches.DateCreated, COUNT(Vouchers.Available) AS Available, " +
            //                   "VoucherBatches.DistributorId, aspnet_Users.UserName, aspnet_Users.UserId, VoucherVolumes.VolumeMB, " +
            //                   "VoucherBatches.Release, VoucherBatches.Paid ,VoucherBatches.Sla" +
            //                   "FROM aspnet_Users INNER JOIN " +
            //                   "VoucherBatches ON aspnet_Users.UserId = VoucherBatches.UserId INNER JOIN " +
            //                   "Vouchers ON VoucherBatches.BatchId = Vouchers.BatchId INNER JOIN " +
            //                   "VoucherVolumes ON VoucherBatches.VolumeId = VoucherVolumes.Id " +
            //                   "WHERE (Vouchers.Available = 1) AND (VoucherBatches.DistributorId = @DistributorId) " +
            //                   "AND (Vouchers.Available > 0) AND (VoucherBatches.Release = 1)" +
            //                   "GROUP BY aspnet_Users.UserName, aspnet_Users.UserId, VoucherBatches.BatchId, VoucherBatches.DateCreated, VoucherBatches.DistributorId, " +
            //                   "VoucherVolumes.VolumeMB, VoucherBatches.Release, VoucherBatches.Paid " +
            //                   "ORDER BY VoucherBatches.BatchId";
            //List<VoucherBatchInfo> vbiList = null;

            //try
            //{
            //    using (SqlConnection conn = new SqlConnection(_connectionString))
            //    {
            //        conn.Open();
            //        using (SqlCommand cmd = new SqlCommand(selectCmd, conn))
            //        {
            //            cmd.Parameters.AddWithValue("@DistributorId", distributorId);
            //            SqlDataReader reader = cmd.ExecuteReader();
            //            vbiList = new List<VoucherBatchInfo>();
            //            while (reader.Read())
            //            {
            //                VoucherBatchInfo v = new VoucherBatchInfo();
            //                v.BatchId = reader.GetInt32(0);
            //                v.DateCreated = reader.GetDateTime(1);
            //                v.AvailableVouchers = reader.GetInt32(2);
            //                v.DistributorId = reader.GetInt32(3);
            //                v.UserName = reader.GetString(4);
            //                v.UserId = reader.GetGuid(5);
            //                v.Volume = reader.GetInt32(6);
            //                v.Release = reader.GetBoolean(7);
            //                v.Paid = reader.GetBoolean(8);
            //                v.Sla = reader.GetInt32(9);
            //                vbiList.Add(v);
            //            }
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{
            //    _logController.LogApplicationException(new CmtApplicationException(ex, "BOVoucherController: Error while returning available voucher info for a distributor", distributorId.ToString()));
            //    throw;
            //}

            string selectCmd = "SELECT BatchId,DistributorId,DateCreated,UserId,VolumeId,Sla,NumVouchers,Release,Paid" +
                                " FROM VoucherBatches" +
                                " where Release = 1 and DistributorId = @DistributorId";
            List<VoucherBatchInfo> vbiList = null;

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(selectCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@DistributorId", distributorId);
                        SqlDataReader reader = cmd.ExecuteReader();
                        vbiList = new List<VoucherBatchInfo>();
                        while (reader.Read())
                        {
                            VoucherBatchInfo v = new VoucherBatchInfo();
                            v.BatchId = reader.GetInt32(0);
                            v.DistributorId = reader.GetInt32(1);
                            v.DateCreated = reader.GetDateTime(2);
                            v.UserId = reader.GetGuid(3);
                            v.Volume = reader.GetInt32(4);
                            v.Sla = reader.GetInt32(5);
                            v.Release = reader.GetBoolean(7);
                            v.Paid = reader.GetBoolean(8);

                            vbiList.Add(v);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOVoucherController: Error while returning available voucher info for a distributor", distributorId.ToString()));
                throw;
            }

            return vbiList;
        }


        /// <summary>
        /// Returns a list of VoucherBatchInfo objects
        /// </summary>
        /// <param name="distributorId"></param>
        /// <returns></returns>
        public List<VoucherBatchInfo> GetAvailableVoucherBatches(int distributorId)
        {
            string selectCmd = "SELECT VoucherBatches.BatchId, VoucherBatches.DateCreated, COUNT(Vouchers.Available) AS Available, " +
                               "VoucherBatches.DistributorId, aspnet_Users.UserName, aspnet_Users.UserId, VoucherVolumes.VolumeMB, " +
                               "VoucherBatches.Release, VoucherBatches.Paid " +
                               "FROM aspnet_Users INNER JOIN " +
                               "VoucherBatches ON aspnet_Users.UserId = VoucherBatches.UserId INNER JOIN " +
                               "Vouchers ON VoucherBatches.BatchId = Vouchers.BatchId INNER JOIN " +
                               "VoucherVolumes ON VoucherBatches.VolumeId = VoucherVolumes.Id " +
                               "WHERE (Vouchers.Available = 1) AND (VoucherBatches.DistributorId = @DistributorId) " +
                               "AND (Vouchers.Available > 0) " +
                               "GROUP BY aspnet_Users.UserName, aspnet_Users.UserId, VoucherBatches.BatchId, VoucherBatches.DateCreated, VoucherBatches.DistributorId, " +
                               "VoucherVolumes.VolumeMB, VoucherBatches.Release, VoucherBatches.Paid " +
                               "ORDER BY VoucherBatches.BatchId";
            List<VoucherBatchInfo> vbiList = null;

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(selectCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@DistributorId", distributorId);
                        SqlDataReader reader = cmd.ExecuteReader();
                        vbiList = new List<VoucherBatchInfo>();
                        while (reader.Read())
                        {
                            VoucherBatchInfo v = new VoucherBatchInfo();
                            v.BatchId = reader.GetInt32(0);
                            v.DateCreated = reader.GetDateTime(1);
                            v.AvailableVouchers = reader.GetInt32(2);
                            v.DistributorId = reader.GetInt32(3);
                            v.UserName = reader.GetString(4);
                            v.UserId = reader.GetGuid(5);
                            v.Volume = reader.GetInt32(6);
                            v.Release = reader.GetBoolean(7);
                            v.Paid = reader.GetBoolean(8);
                            vbiList.Add(v);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOVoucherController: Error while returning available voucher info for a distributor", distributorId.ToString()));
                throw;
            }

            return vbiList;
        }

        /// <summary>
        /// Returns a list of VoucherBatchInfo objects
        /// </summary>
        /// <param name="distributorId"></param>
        /// <returns></returns>
        public List<VoucherBatchInfo> GetVoucherBatches(int distributorId)
        {
            string selectCmd = "SELECT VoucherBatches.BatchId, VoucherBatches.DateCreated, COUNT(Vouchers.Available) AS Available, " +
                               "VoucherBatches.DistributorId, aspnet_Users.UserName, aspnet_Users.UserId, VoucherVolumes.VolumeMB, " +
                               "VoucherBatches.Release, VoucherBatches.Paid " +
                               "FROM aspnet_Users INNER JOIN " +
                               "VoucherBatches ON aspnet_Users.UserId = VoucherBatches.UserId INNER JOIN " +
                               "Vouchers ON VoucherBatches.BatchId = Vouchers.BatchId INNER JOIN " +
                               "VoucherVolumes ON VoucherBatches.VolumeId = VoucherVolumes.Id " +
                               "WHERE (VoucherBatches.DistributorId = @DistributorId) " +
                               "GROUP BY aspnet_Users.UserName, aspnet_Users.UserId, VoucherBatches.BatchId, VoucherBatches.DateCreated, VoucherBatches.DistributorId, " +
                               "VoucherVolumes.VolumeMB, VoucherBatches.Release, VoucherBatches.Paid " +
                               "ORDER BY VoucherBatches.BatchId DESC";
            List<VoucherBatchInfo> vbiList = null;

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(selectCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@DistributorId", distributorId);
                        SqlDataReader reader = cmd.ExecuteReader();
                        vbiList = new List<VoucherBatchInfo>();
                        while (reader.Read())
                        {
                            VoucherBatchInfo v = new VoucherBatchInfo();
                            v.BatchId = reader.GetInt32(0);
                            v.DateCreated = reader.GetDateTime(1);
                            v.AvailableVouchers = reader.GetInt32(2);
                            v.DistributorId = reader.GetInt32(3);
                            v.UserName = reader.GetString(4);
                            v.UserId = reader.GetGuid(5);
                            v.Volume = reader.GetInt32(6);
                            v.Release = reader.GetBoolean(7);
                            v.Paid = reader.GetBoolean(8);
                            vbiList.Add(v);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOVoucherController: Error while returning available voucher info for a distributor", distributorId.ToString()));
                throw;
            }

            return vbiList;
        }

        /// <summary>
        /// Returns a Voucher object identified by its code.
        /// </summary>
        /// <param name="code">The 13 digit code string</param>
        /// <returns>The voucher instance corresponding with the given code or
        /// null if the voucher could not be found in the database.</returns>
        public Model.Voucher.Voucher GetVoucher(string code)
        {
            string selectCmd = "SELECT Code, Crc, CrcSeed, Available, BatchId FROM " +
                              "Vouchers WHERE Code = @Code";
            Voucher v = null;

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(selectCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@Code", code);
                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            v = new Voucher();
                            v.Code = reader.GetString(0);
                            v.Crc = (ushort)reader.GetInt32(1);
                            v.CrcSeed = (ushort)reader.GetInt32(2);
                            v.Available = reader.GetBoolean(3);
                            v.BatchId = reader.GetInt32(4);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOVoucherController: Error while retrieving voucher", code.ToString(), (short)ExceptionLevelEnum.Error));
                throw;
            }

            return v;
        }

        /// <summary>
        /// Returns a list of vouchers for the specified Batch Id
        /// </summary>
        /// <param name="batchId">The identifier of the batch</param>
        /// <returns>A list of vouchers identifief by the given batch id.</returns>
        public List<Model.Voucher.Voucher> GetVouchers(int batchId)
        {
            string selectCmd = "SELECT Code, Crc, CrcSeed, Available, BatchId FROM " +
                               "Vouchers WHERE BatchId = @BatchId AND Available = 1 ORDER BY Sequence";
            List<Voucher> vouchers = null;

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(selectCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@BatchId", batchId);
                        SqlDataReader reader = cmd.ExecuteReader();
                        vouchers = new List<Voucher>();
                        while (reader.Read())
                        {
                            Voucher v = new Voucher();
                            v.Code = reader.GetString(0);
                            v.Crc = (ushort)reader.GetInt32(1);
                            v.CrcSeed = (ushort)reader.GetInt32(2);
                            v.Available = reader.GetBoolean(3);
                            v.BatchId = reader.GetInt32(4);
                            vouchers.Add(v);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOVoucherController: Error while returning vouchers for a batch", batchId.ToString()));
                throw;
            }

            return vouchers;
        }

        /// <summary>
        /// Validates the voucher identified by the given code.
        /// </summary>
        /// <remarks>
        /// Validation is done without calling the AddVolume ISPIF method
        /// </remarks>
        /// <param name="code">The code of the voucher to validate</param>
        /// <param name="macAddress">The MAC address of the terminal for which the voucher was validated</param>
        /// <returns>True if the voucher was successfully validated</returns>
        public bool ValidateVoucher(string code, string crc, string macAddress)
        {
            string updateCmd = "UPDATE Vouchers SET Available = 0, MacAddress = @MacAddress, DateValidated = @DateValidated WHERE Code = @Code AND Crc = @Crc AND Available = 1";
            bool result = false;

            //Before doing anything else, make sure the voucher has not been validated before


            try
            {
                //Translate crc in an ushort
                int crcNum = 0;
                if (Int32.TryParse(crc, NumberStyles.AllowHexSpecifier, null, out crcNum))
                {
                    using (SqlConnection conn = new SqlConnection(_connectionString))
                    {
                        conn.Open();
                        using (SqlCommand cmd = new SqlCommand(updateCmd, conn))
                        {
                            cmd.Parameters.AddWithValue("@Code", code);
                            cmd.Parameters.AddWithValue("@Crc", crcNum);
                            cmd.Parameters.AddWithValue("@MacAddress", macAddress);
                            cmd.Parameters.AddWithValue("@DateValidated", DateTime.Now);
                            int ret = cmd.ExecuteNonQuery();
                            result = (ret > 0);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOVoucherController: Error while validating voucher", code, (short)ExceptionLevelEnum.Critical));
            }
            return result;
        }

        /// <summary>
        /// Validate the complete batch with the given batch number.
        /// </summary>
        /// <remarks>
        /// Validation is done without calling the AddVolume ISPIF method
        /// </remarks>
        /// <param name="batchNumber">The batch number to validate</param>
        /// <returns>True if the batch was successfully validated</returns>
        public bool ValidateBatch(int batchId)
        {
            string invalidateBatchCmd = "UPDATE Vouchers SET Available = 0 WHERE BatchId = @BatchId";
            bool result = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(invalidateBatchCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@BatchId", batchId);

                        int ret = cmd.ExecuteNonQuery();
                        result = (ret != 0);
                    }
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOVoucherController: Error while invalidating batch", batchId.ToString(), (short)ExceptionLevelEnum.Critical));
                throw;
            }

            return result;
        }

        /// <summary>
        /// Creates a list of unique vouchers. The number of vouchers is given
        /// by the numVouchers parameter.
        /// </summary>
        /// <param name="distributorId">The distributor who owns the vouchers</param>
        /// <param name="numVouchers">The number of vouchers to generate</param>
        /// <param name="UserId">Id of the system user who created the voucher batch</param>
        /// <param name="batchId">After conclusion of the method, batchId contains the new batch identifier</param>
        /// <returns>A list of Vouchers with length is numVouchers</returns>
        public List<Model.Voucher.Voucher> CreateVouchers(int distributorId, int numVouchers, Guid userId, out int batchId, int volumeId)
        {
            VoucherBatch vb = new VoucherBatch();
            vb.DistributorId = distributorId;
            vb.DateCreated = DateTime.UtcNow;
            vb.UserId = userId;
            vb.Volume = volumeId;
            vb.Sla = this.GetVoucherVolumeById(volumeId).Sla;
            vb.NumVouchers = numVouchers;

            List<Voucher> vouchers = new List<Voucher>();
            batchId = -1;

            try
            {
                //Store voucher batch data in database.
                vb.BatchId = this.StoreVoucherBatch(vb);
                batchId = vb.BatchId;

                //Create numVouchers, vouchers
                for (int i = 0; i < numVouchers; i++)
                {
                    Voucher v = new Voucher();
                    v.Code = this.CreateCode();
                    v.CrcSeed = this.CreateCrcSeed();
                    v.Crc = this.CalculateCrc(v);
                    v.BatchId = vb.BatchId;
                    v.Available = true;

                    //Dump newly created voucher to database
                    this.StoreVoucher(v);

                    vouchers.Add(v);
                }

                //Add an entry to the activity log
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex,
                                    "BOVoucherController: Error while creating vouchers",
                                    vb.DistributorId.ToString(), (short)ExceptionLevelEnum.Error));
            }

            return vouchers;
        }

        /// <summary>
        /// Makes a voucher which was (accidently) validated, available again.
        /// </summary>
        /// <param name="code">The unique voucher code</param>
        /// <returns>True if the operation succeeded</returns>
        public bool ResetVoucher(string code, string crc)
        {
            string updateCmd = "UPDATE Vouchers SET Available = 1 WHERE Code = @Code AND Crc = @Crc AND Available = 0";
            bool result = false;

            try
            {
                //Translate crc in an int
                int crcNum = 0;
                if (Int32.TryParse(crc, NumberStyles.AllowHexSpecifier, null, out crcNum))
                {
                    using (SqlConnection conn = new SqlConnection(_connectionString))
                    {
                        conn.Open();
                        using (SqlCommand cmd = new SqlCommand(updateCmd, conn))
                        {
                            cmd.Parameters.AddWithValue("@Code", code);
                            cmd.Parameters.AddWithValue("@Crc", crcNum);
                            int ret = cmd.ExecuteNonQuery();
                            result = (ret > 0);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOVoucherController: Error while validating voucher", code, (short)ExceptionLevelEnum.Error));
                throw;
            }
            return result;
        }

        /// <summary>
        /// This method puts the Release flag of the VoucherBatch table
        /// for the given voucher batch id back to false
        /// </summary>
        /// <param name="batchId">The voucher batch to unrelease</param>
        /// <returns>True if the method succeeded</returns>
        public bool UnreleaseVoucherBatch(int batchId)
        {
            bool success = false;
            string updateCmd = "UPDATE VoucherBatches SET Release = 0 WHERE BatchId = @BatchId";

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(updateCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@BatchId", batchId);
                        int rtn = cmd.ExecuteNonQuery();
                        if (rtn != 0)
                        {
                            success = true;
                        }
                        else
                        {
                            success = false;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOVoucherController: Error while unreleasing voucher batch", batchId.ToString(), (short)ExceptionLevelEnum.Error));
                success = false;
            }

            return success;
        }


        /// <summary>
        /// This method deletes a voucher batch if non of its vouchers where validated or released
        /// </summary>
        /// <param name="batchId">The voucher batch to be deleted</param>
        /// <returns>True if the method succeeded</returns>
        public int DeleteVoucherBatch(int batchId)
        {


            string deleteVoucherCmd = "Delete from Vouchers WHERE Available=1 and BatchId = @BatchId";
            string deleteBatchCmd = "Delete from VoucherBatches WHERE Release=0 and Paid=0 and BatchId = @BatchId";
            VoucherBatch vb = new VoucherBatch();
            vb = GetVoucherBatchInfo(batchId);
            if (!vb.Paid)
            {
                if (!vb.Release)
                {
                    if (GetVouchers(batchId).Count == vb.NumVouchers)
                    {
                        try
                        {
                            using (SqlConnection conn = new SqlConnection(_connectionString))
                            {
                                conn.Open();
                                using (SqlCommand cmd = new SqlCommand(deleteVoucherCmd, conn))
                                {
                                    cmd.Parameters.AddWithValue("@BatchId", batchId);
                                    cmd.ExecuteNonQuery();

                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            _logController.LogApplicationException(new CmtApplicationException(ex, "BOVoucherController: Error while unreleasing voucher batch", batchId.ToString(), (short)ExceptionLevelEnum.Error));
                            return -1;
                        }

                        try
                        {
                            using (SqlConnection conn = new SqlConnection(_connectionString))
                            {
                                conn.Open();
                                using (SqlCommand cmd = new SqlCommand(deleteBatchCmd, conn))
                                {
                                    cmd.Parameters.AddWithValue("@BatchId", batchId);
                                    int rtn = cmd.ExecuteNonQuery();
                                    return 0;//Batch deleted successfully

                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            _logController.LogApplicationException(new CmtApplicationException(ex, "BOVoucherController: Error while unreleasing voucher batch", batchId.ToString(), (short)ExceptionLevelEnum.Error));
                            return -1;
                        }
                    }
                    else
                        return 1;
                }
                else
                    return 2;
            }
            else
                return 3;
        }

        /// <summary>
        /// Releases a voucher batch
        /// </summary>
        /// <param name="batchId">The voucher batch identifier</param>
        /// <returns>True if the mehtod succeeded</returns>
        public bool ReleaseVoucherBatch(int batchId)
        {
            bool success = false;
            string updateCmd = "UPDATE VoucherBatches SET Release = 1 WHERE BatchId = @BatchId";

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(updateCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@BatchId", batchId);
                        int rtn = cmd.ExecuteNonQuery();
                        if (rtn != 0)
                        {
                            success = true;
                        }
                        else
                        {
                            success = false;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOVoucherController: Error while releasing voucher batch", batchId.ToString(), (short)ExceptionLevelEnum.Critical));
                success = false;
            }

            return success;
        }

        /// <summary>
        /// Flags a voucher batch as paid in the CMT database
        /// </summary>
        /// <param name="batchId">The ID of the batch in question</param>
        /// <returns>True if successful</returns>
        public bool FlagVoucherBatchAsPaid(int batchId)
        {
            bool success = false;
            string updateCmd = "UPDATE VoucherBatches SET Paid = 1 WHERE BatchId = @BatchId";

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(updateCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@BatchId", batchId);
                        int rtn = cmd.ExecuteNonQuery();
                        if (rtn != 0)
                        {
                            success = true;
                        }
                        else
                        {
                            success = false;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOVoucherController: Error while flagging voucher batch as paid", batchId.ToString(), (short)ExceptionLevelEnum.Error));
                success = false;
            }

            return success;
        }

        /// <summary>
        /// Flags a voucher batch as paid in the CMT database
        /// </summary>
        /// <param name="batchId">The ID of the batch in question</param>
        /// <param name="paymentMethod">payment method</param>
        /// <returns>True if successful</returns>
        public bool MarkVoucherBatchAsPaid(int batchId, string paymentMethod)
        {
            bool success = false;
            string updateCmd = "UPDATE VoucherBatches SET Paid = 1, PaymentMethod = @PaymentMethod WHERE BatchId = @BatchId";
            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(updateCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@BatchId", batchId);
                        cmd.Parameters.AddWithValue("@PaymentMethod", paymentMethod);
                        int rtn = cmd.ExecuteNonQuery();
                        if (rtn != 0)
                        {
                            success = true;
                        }
                        else
                        {
                            success = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOVoucherController: Error while flagging voucher batch as paid", batchId.ToString(), (short)ExceptionLevelEnum.Error));
                success = false;
            }
            return success;
        }

        /// <summary>
        /// Flags a voucher batch as unpaid in the CMT database
        /// </summary>
        /// <param name="batchId">The ID of the batch to be flagged</param>
        /// <returns>True if successful</returns>
        public bool FlagVoucherBatchAsUnPaid(int batchId)
        {
            bool success = false;
            string updateCmd = "UPDATE VoucherBatches SET Paid = 0 WHERE BatchId = @BatchId";

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(updateCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@BatchId", batchId);
                        int rtn = cmd.ExecuteNonQuery();
                        if (rtn != 0)
                        {
                            success = true;
                        }
                        else
                        {
                            success = false;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOVoucherController: Error while flagging voucher batch as unpaid", batchId.ToString(), (short)ExceptionLevelEnum.Error));
                success = false;
            }

            return success;
        }

        /// <summary>
        /// Returns the meta data of the given batch
        /// </summary>
        /// <param name="batchId">A unique batch id</param>
        /// <returns>A VoucherBatch instance or null if the batch could not be found in the database</returns>
        public Model.Voucher.VoucherBatch GetVoucherBatchInfo(int batchId)
        {
            string selectCmd = "SELECT VoucherBatches.DistributorId, VoucherBatches.DateCreated, VoucherBatches.UserId, VoucherBatches.VolumeId, VoucherBatches.Release, VoucherBatches.NumVouchers, VoucherBatches.Paid, VoucherBatches.Sla FROM " +
                               "VoucherBatches INNER JOIN VoucherVolumes ON VoucherBatches.VolumeId = VoucherVolumes.Id WHERE BatchId = @BatchId";

            VoucherBatch vb = null;

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(selectCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@BatchId", batchId);

                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            vb = new VoucherBatch();

                            vb.BatchId = batchId;
                            vb.DateCreated = reader.GetDateTime(1);
                            vb.DistributorId = reader.GetInt32(0);
                            vb.UserId = reader.GetGuid(2);
                            vb.Volume = reader.GetInt32(3);
                            vb.Release = reader.GetBoolean(4);
                            vb.NumVouchers = reader.GetInt32(5);
                            vb.Paid = reader.GetBoolean(6);
                            if (!reader.IsDBNull(7))
                            {
                                vb.Sla = reader.GetInt32(7);
                            }

                        }

                    }
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOVoucherController: Error while retrieving VoucherBatch", vb.BatchId.ToString()));
            }
            return vb;
        }

        /// <summary>
        /// Returns a list of Voucher volume objects
        /// </summary>
        /// <returns></returns>
        public List<Model.Voucher.VoucherVolume> GetVoucherVolumes()
        {
            //string voucherCmd = "SELECT Id, VolumeMB, UnitPrice, Description, CurrencyCode, ValidityPeriod, VolumeCode, UnitPriceEUR, UnitPriceUSD, Deleted FROM VoucherVolumes ORDER BY VolumeMB";
            string voucherCmd = "SELECT Id, VolumeMB, UnitPrice, Description, CurrencyCode, ValidityPeriod, VolumeCode, UnitPriceEUR, UnitPriceUSD, Deleted, Sla FROM VoucherVolumes where Deleted IS NULL OR Deleted = 0 ORDER BY VolumeMB";
            List<VoucherVolume> voucherVolumeList = new List<VoucherVolume>();

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(voucherCmd, conn))
                    {
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            VoucherVolume vv = new VoucherVolume();
                            vv.Id = reader.GetInt32(0);
                            vv.VolumeMB = reader.GetInt32(1);
                            vv.UnitPrice = (double)reader.GetDecimal(2);
                            vv.Description = reader.GetString(3);
                            vv.CurrencyCode = reader.GetString(4);
                            if (!reader.IsDBNull(5))
                            {
                                vv.ValidityPeriod = reader.GetInt32(5);
                            }
                            vv.VolumeId = reader.GetInt32(6);
                            if (!reader.IsDBNull(7))
                            {
                                vv.UnitPriceEUR = (double)reader.GetDecimal(7);
                            }
                            if (!reader.IsDBNull(8))
                            {
                                vv.UnitPriceUSD = (double)reader.GetDecimal(8);
                            }
                            if (!reader.IsDBNull(9))
                            {
                                vv.Deleted = reader.GetBoolean(9);
                            }
                            if (!reader.IsDBNull(10))
                            {
                                vv.Sla = reader.GetInt32(10);
                            }
                            voucherVolumeList.Add(vv);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                BOLogController boLogController = new BOLogController(_connectionString, _databaseProvider);
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDateTime = DateTime.Now;
                cmtEx.ExceptionDesc = ex.Message;
                cmtEx.ExceptionStacktrace = ex.StackTrace;
                cmtEx.UserDescription = "Could not retrieve Voucher Volumes";
                boLogController.LogApplicationException(cmtEx);
            }

            return voucherVolumeList;

        }

        /// <summary>
        /// Returns the list of distributor billables which are not deleted 
        /// </summary>
        /// <remarks>
        /// Only the non-deleted distributor billables are returned
        /// </remarks>
        /// <param name="distId">The unique identifier of the distributor</param>
        /// <returns>A list of distBillables for the given distributor</returns>
        public List<Model.Voucher.DistVoucher> GetDistVouchersforVV(int volumeId)
        {
            const string sql = "SELECT Id, Vvid, DistId, PriceEU, PriceUSD, Remarks, UserId, LastUpdate, Deleted FROM DistVouchers WHERE Vvid = @volumeId AND Deleted != 1";

            var resultList = new List<DistVoucher>();

            try
            {
                using (var con = new SqlConnection(_connectionString))
                {
                    con.Open();

                    using (var cmd = new SqlCommand(sql, con))
                    {
                        cmd.Parameters.AddWithValue("@volumeId", volumeId);
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            var db = new DistVoucher();
                            db.Id = reader.GetInt32(0);
                            db.Vvid = reader.GetInt32(1);
                            db.DistId = reader.GetInt32(2);
                            db.PriceEU = reader.GetDecimal(3);
                            db.PriceUSD = reader.GetDecimal(4);
                            if (!reader.IsDBNull(5))
                            {
                                db.Remarks = reader.GetString(5);
                            }
                            db.UserId = reader.GetGuid(6);
                            db.LastUpdate = reader.GetDateTime(7);
                            db.Deleted = reader.GetBoolean(8);

                            resultList.Add(db);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                BOLogController boLogController = new BOLogController(_connectionString, _databaseProvider);
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDateTime = DateTime.Now;
                cmtEx.ExceptionDesc = ex.Message;
                cmtEx.ExceptionStacktrace = ex.StackTrace;
                cmtEx.UserDescription = "Could not retrieve distributor Voucher Volumes for VV";
                boLogController.LogApplicationException(cmtEx);
            }

            return resultList;
        }



        /// <summary>
        /// Returns the list of distributor billables which are not deleted 
        /// </summary>
        /// <remarks>
        /// Only the non-deleted distributor billables are returned
        /// </remarks>
        /// <param name="distId">The unique identifier of the distributor</param>
        /// <returns>A list of distBillables for the given distributor</returns>
        public List<Model.Voucher.DistVoucher> GetVoucherVolumesforDist(int distId)
        {
            const string sql = "SELECT Id, Vvid, DistId, PriceEU, PriceUSD, Remarks, UserId, LastUpdate, Deleted FROM DistBillables WHERE DistId = @distId AND Deleted != 1";

            var resultList = new List<DistVoucher>();

            try
            {
                using (var con = new SqlConnection(_connectionString))
                {
                    con.Open();

                    using (var cmd = new SqlCommand(sql, con))
                    {
                        cmd.Parameters.AddWithValue("@distId", distId);
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            var db = new DistVoucher();
                            db.Id = reader.GetInt32(0);
                            db.Vvid = reader.GetInt32(1);
                            db.DistId = reader.GetInt32(2);
                            db.PriceEU = reader.GetDecimal(3);
                            db.PriceUSD = reader.GetDecimal(4);
                            if (!reader.IsDBNull(5))
                            {
                                db.Remarks = reader.GetString(5);
                            }
                            db.UserId = reader.GetGuid(6);
                            db.LastUpdate = reader.GetDateTime(7);
                            db.Deleted = reader.GetBoolean(8);

                            resultList.Add(db);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                BOLogController boLogController = new BOLogController(_connectionString, _databaseProvider);
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDateTime = DateTime.Now;
                cmtEx.ExceptionDesc = ex.Message;
                cmtEx.ExceptionStacktrace = ex.StackTrace;
                cmtEx.UserDescription = "Could not retrieve Voucher Volumes for distibutor";
                boLogController.LogApplicationException(cmtEx);
            }

            return resultList;
        }


        /// <summary>
        /// Returns true if the distributor has a distBillable for the given bid
        /// </summary>
        /// <param name="distId">The distributor identifier</param>
        /// <param name="bid">The Billable Identifier</param>
        /// <returns>True if the a distributor billable exists</returns>
        public Boolean DistVoucherExists(int distId, int vvid)
        {
            DistVoucher db = new DistVoucher();
            Boolean exists = false;
            const string sql = @"
                                    SELECT Id, Bid, DistId, PriceEU, PriceUSD, Remarks, UserId, LastUpdate, Deleted FROM DistVouchers
                                    WHERE DistId = @distId AND Vvid = @vvid AND Deleted != 1
                                    ";
            try
            {
                using (var con = new SqlConnection(_connectionString))
                {
                    con.Open();

                    using (var cmd = new SqlCommand(sql, con))
                    {
                        cmd.Parameters.AddWithValue("@distId", distId);
                        cmd.Parameters.AddWithValue("@vvid", vvid);
                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                            exists = true;

                    }
                }
            }
            catch (Exception ex)
            {
                BOLogController boLogController = new BOLogController(_connectionString, _databaseProvider);
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDateTime = DateTime.Now;
                cmtEx.ExceptionDesc = ex.Message;
                cmtEx.ExceptionStacktrace = ex.StackTrace;
                cmtEx.ExceptionLevel = (short)ExceptionLevelEnum.Error;
                cmtEx.UserDescription = "Error @ DistVoucherExists";
                boLogController.LogApplicationException(cmtEx);
            }
            return exists;
        }
        /// <summary>
        /// Returns a voucher volume object reference by its id
        /// </summary>
        /// <param name="volumeId">The VoucherVolumeId</param>
        /// <returns>The voucher volume object or null if not found</returns>
        public VoucherVolume GetVoucherVolumeByIdForDistributor(int volumeId,int DistId)
        {
            string voucherCmd = "SELECT Id, VolumeMB, UnitPrice, Description, CurrencyCode, ValidityPeriod, VolumeCode, UnitPriceEUR, UnitPriceUSD, Deleted, Sla FROM VoucherVolumes WHERE Id = @Id";
            string disvoucherCmd= @"
                                    SELECT Id, Vvid, DistId, PriceEU, PriceUSD, Remarks, UserId, LastUpdate, Deleted FROM DistBillables
                                    WHERE DistId = @distId AND Vvid = @volumeId AND Deleted != 1
                                    ";
            VoucherVolume vv = null;

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(voucherCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@Id", volumeId);
                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            vv = new VoucherVolume();
                            vv.Id = reader.GetInt32(0);
                            vv.VolumeMB = reader.GetInt32(1);
                            vv.UnitPrice = (double)reader.GetDecimal(2);
                            vv.Description = reader.GetString(3);
                            vv.CurrencyCode = reader.GetString(4);
                            vv.ValidityPeriod = reader.GetInt32(5);
                            vv.VolumeId = reader.GetInt32(6);
                            if (!reader.IsDBNull(7) != null)
                            {
                                vv.UnitPriceEUR = (double)reader.GetDecimal(7);
                            }
                            if (!reader.IsDBNull(8) != null)
                            {
                                vv.UnitPriceUSD = (double)reader.GetDecimal(8);
                            }
                            if (!reader.IsDBNull(9))
                            {
                                vv.Deleted = reader.GetBoolean(9);
                            }
                            vv.Sla = reader.GetInt32(10);
                        }
                    }
                }
                if (DistVoucherExists(DistId, volumeId))
                {
                    using (SqlConnection conn = new SqlConnection(_connectionString))
                    {
                        conn.Open();

                        using (SqlCommand cmd = new SqlCommand(disvoucherCmd, conn))
                        {
                            cmd.Parameters.AddWithValue("@distId", DistId);
                            cmd.Parameters.AddWithValue("@vvid", volumeId);

                            SqlDataReader reader = cmd.ExecuteReader();

                            if (reader.Read())
                            {
                                if (!reader.IsDBNull(3) != null)
                                {
                                    vv.UnitPriceEUR = (double)reader.GetDecimal(3);
                                }
                                if (!reader.IsDBNull(4) != null)
                                {
                                    vv.UnitPriceUSD = (double)reader.GetDecimal(4);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                BOLogController boLogController = new BOLogController(_connectionString, _databaseProvider);
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDateTime = DateTime.Now;
                cmtEx.ExceptionDesc = ex.Message;
                cmtEx.ExceptionStacktrace = ex.StackTrace;
                cmtEx.ExceptionLevel = (short)ExceptionLevelEnum.Error;
                cmtEx.UserDescription = "Could not retrieve Voucher Volume by id for distributor";
                boLogController.LogApplicationException(cmtEx);
            }

            return vv;
        }

        /// <summary>
        /// Returns a voucher volume object reference by its id
        /// </summary>
        /// <param name="volumeId">The VoucherVolumeId</param>
        /// <returns>The voucher volume object or null if not found</returns>
        public VoucherVolume GetVoucherVolumeById(int volumeId)
        {
            string voucherCmd = "SELECT Id, VolumeMB, UnitPrice, Description, CurrencyCode, ValidityPeriod, VolumeCode, UnitPriceEUR, UnitPriceUSD, Deleted, Sla FROM VoucherVolumes WHERE Id = @Id";
            VoucherVolume vv = null;

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(voucherCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@Id", volumeId);
                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            vv = new VoucherVolume();
                            vv.Id = reader.GetInt32(0);
                            vv.VolumeMB = reader.GetInt32(1);
                            vv.UnitPrice = (double)reader.GetDecimal(2);
                            vv.Description = reader.GetString(3);
                            vv.CurrencyCode = reader.GetString(4);
                            vv.ValidityPeriod = reader.GetInt32(5);
                            vv.VolumeId = reader.GetInt32(6);
                            if (!reader.IsDBNull(7) != null)
                            {
                                vv.UnitPriceEUR = (double)reader.GetDecimal(7);
                            }
                            if (!reader.IsDBNull(8) != null)
                            {
                                vv.UnitPriceUSD = (double)reader.GetDecimal(8);
                            }
                            if (!reader.IsDBNull(9))
                            {
                                vv.Deleted = reader.GetBoolean(9);
                            }
                            vv.Sla = reader.GetInt32(10);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                BOLogController boLogController = new BOLogController(_connectionString, _databaseProvider);
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDateTime = DateTime.Now;
                cmtEx.ExceptionDesc = ex.Message;
                cmtEx.ExceptionStacktrace = ex.StackTrace;
                cmtEx.ExceptionLevel = (short)ExceptionLevelEnum.Error;
                cmtEx.UserDescription = "Could not retrieve Voucher Volumes";
                boLogController.LogApplicationException(cmtEx);
            }

            return vv;
        }

        /// <summary>
        /// Stores a newly created voucher batch into the VoucherBatch table
        /// </summary>
        /// <param name="vb">The Voucher</param>
        /// <returns>The batch identifier as an integer or -1 if the insert fails</returns>
        private int StoreVoucherBatch(VoucherBatch vb)
        {
            int batchId = -1;
            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand("cmtCreateVoucherBatch2", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@DistributorId", vb.DistributorId);
                        cmd.Parameters.AddWithValue("@UserId", vb.UserId);
                        cmd.Parameters.AddWithValue("@DateCreated", vb.DateCreated);
                        cmd.Parameters.AddWithValue("@VolumeId", vb.Volume);
                        cmd.Parameters.AddWithValue("@Sla", vb.Sla);
                        cmd.Parameters.AddWithValue("@NumVouchers", vb.NumVouchers);

                        batchId = Convert.ToInt32(cmd.ExecuteScalar());
                    }
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOVoucherController: Error while storing voucher batch data", vb.DistributorId.ToString(), (short)ExceptionLevelEnum.Error));
                throw;
            }

            return batchId;
        }

        /// <summary>
        /// Create a 13 digit unique code
        /// </summary>
        /// <returns>A 13 digit random code</returns>
        private string CreateCode()
        {
            bool isUnique = false;
            StringBuilder code = null;
            int previousNum = 0;

            int seed = (int)DateTime.Now.Ticks;
            Random random = new Random(seed);

            while (!isUnique)
            {
                code = new StringBuilder();

                for (int i = 0; i < NUM_DIGITS; i++)
                {
                    int num = 0;

                    do
                    {
                        if (i == 0)
                        {
                            num = random.Next(1, 9);
                        }
                        else
                        {
                            num = random.Next(0, 9);
                        }
                    } while (num == previousNum);

                    code.Append(num);
                    previousNum = num;
                }

                isUnique = this.IsCodeUnique(code.ToString());
            }

            return code.ToString();
        }

        /// <summary>
        /// Checks if the generated code is unique
        /// </summary>
        /// <param name="code">The 13 digit generated code</param>
        /// <returns>True if the code is unique, false otherwise</returns>
        private bool IsCodeUnique(string code)
        {
            bool isUnique = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand("cmtIsVoucherUnique", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@VoucherCode", code);

                        var returnParameter = cmd.Parameters.Add("@IsUnique", SqlDbType.Bit);
                        returnParameter.Direction = ParameterDirection.Output;

                        cmd.ExecuteNonQuery();
                        isUnique = (bool)cmd.Parameters["@IsUnique"].Value;
                    }
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOVoucherController: cmtIsVoucherUnique stored procedure failed", "", (short)ExceptionLevelEnum.Error));
                throw;
            }

            return isUnique;
        }

        /// <summary>
        /// Creates a 16 bit seed value and returns the value as an ushort
        /// </summary>
        /// <returns></returns>
        private ushort CreateCrcSeed()
        {
            int seed = (int)DateTime.Now.Ticks / 10000;
            Random random = new Random(seed);
            return (ushort)random.Next(0x0001, 0xFFFF);
        }

        /// <summary>
        /// Calculates a 16 bit crc value for the given code
        /// </summary>
        /// <param name="v">The voucher containing the code</param>
        /// <returns>A 16 bit CRC value</returns>
        private ushort CalculateCrc(Voucher v)
        {
            Byte[] codeBytes = new System.Text.UTF8Encoding().GetBytes(v.Code);
            return Crc16.FastCrc16Calc(v.CrcSeed, codeBytes);
        }

        /// <summary>
        /// Adds the Voucher to the Voucher table
        /// </summary>
        /// <param name="v">Voucher to store</param>
        /// <param name="batchId">The Voucher batch id</param>
        /// <param name="distributorId">The identifier of the distributor who owns the voucher</param>
        /// <returns>True if the insert succeeds</returns>
        public bool StoreVoucher(Voucher v)
        {
            string insertCmd = "INSERT INTO Vouchers (Code, Crc, CrcSeed, Available, Batchid) " +
                               "VALUES (@Code, @Crc, @CrcSeed, @Available, @BatchId)";
            bool result = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(insertCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@Code", v.Code);
                        cmd.Parameters.AddWithValue("@Crc", (int)v.Crc);
                        cmd.Parameters.AddWithValue("@CrcSeed", (int)v.CrcSeed);
                        cmd.Parameters.AddWithValue("@Available", true);
                        cmd.Parameters.AddWithValue("@BatchId", v.BatchId);

                        int ret = cmd.ExecuteNonQuery();

                        if (ret == 1)
                        {
                            result = true;
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOVoucherController: Error while storing voucher", v.Code, (short)ExceptionLevelEnum.Error));
                result = false;
            }

            return result;

        }


        /// <summary>
        /// Returns a data set with the number of voucher created for each distributor between
        /// a begin and start date.
        /// </summary>
        /// <remarks>
        /// This method should use the cmtVoucherCreation stored procedure.
        /// </remarks>
        /// <param name="startDate">Start date of the report (this date included)</param>
        /// <param name="endDate">End date of the report (this date excluded)</param>
        /// <returns>A data set with the selected voucher batches</returns>
        public DataSet GenerateVoucherReport(DateTime startDate, DateTime endDate)
        {
            DataSet voucherReport = new DataSet("VoucherReportDataSet");
            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("cmtVoucherCreationReport", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@StartDate", startDate);
                        cmd.Parameters.AddWithValue("@EndDate", endDate);
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(voucherReport, "VoucherReportTable");
                    }
                }
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDesc = ex.Message;
                cmtEx.ExceptionStacktrace = ex.StackTrace;
                cmtEx.UserDescription = "The GenerateVoucherReport failed";
                _logController.LogApplicationException(cmtEx);
            }

            return voucherReport;
        }

        /// <summary>
        /// Returns the voucher batch for a given code and crc
        /// </summary>
        /// <remarks>
        /// This method needs to be corrected since the VoucherBatch volume property does not
        /// contain the actual volume in MB but the volume code identifier. Maybe a join could
        /// also make this method a bit more streamlined!
        /// </remarks>
        /// <param name="code">The voucher code</param>
        /// <param name="crc">The voucher crc</param>
        /// <returns>The corresponding VoucherBatch or null if the method failed</returns>
        public VoucherBatch GetVoucherBatch(string code, string crc)
        {
            VoucherBatch vb = null;
            int batchId = 0;

            //Convert hex string to an integer
            int crcNum = 0;
            Int32.TryParse(crc, NumberStyles.AllowHexSpecifier, null, out crcNum);

            string selectVoucherIdCmdStr = "SELECT BatchId FROM Vouchers WHERE Code = @Code AND Crc = @Crc";
            string selectVoucherBatchCmdStr = "SELECT VoucherBatches.DistributorId, VoucherBatches.DateCreated, VoucherBatches.UserId, VoucherBatches.VolumeId, VoucherBatches.NumVouchers, " +
                         "VoucherBatches.Release, VoucherBatches.Paid " +
                         "FROM VoucherBatches " +
                         "WHERE (VoucherBatches.BatchId = @BatchId)";

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    SqlCommand selectVoucherIdCmd = new SqlCommand(selectVoucherIdCmdStr, conn);

                    selectVoucherIdCmd.Parameters.AddWithValue("@Code", code);
                    selectVoucherIdCmd.Parameters.AddWithValue("@Crc", crcNum);

                    SqlDataReader voucherReader = selectVoucherIdCmd.ExecuteReader();

                    if (voucherReader.Read())
                    {
                        batchId = voucherReader.GetInt32(0);
                        voucherReader.Close();

                        SqlCommand selectVoucherBatchCmd = new SqlCommand(selectVoucherBatchCmdStr, conn);
                        selectVoucherBatchCmd.Parameters.AddWithValue("@BatchId", batchId);

                        SqlDataReader voucherBatchReader = selectVoucherBatchCmd.ExecuteReader();

                        if (voucherBatchReader.Read())
                        {
                            vb = new VoucherBatch();
                            vb.BatchId = batchId;
                            vb.DistributorId = voucherBatchReader.GetInt32(0);
                            vb.DateCreated = voucherBatchReader.GetDateTime(1);
                            vb.UserId = voucherBatchReader.GetGuid(2);
                            vb.Volume = voucherBatchReader.GetInt32(3);
                            vb.NumVouchers = voucherBatchReader.GetInt32(4);
                            vb.Release = voucherBatchReader.GetBoolean(5);
                            vb.Paid = voucherBatchReader.GetBoolean(6);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDesc = ex.Message;
                cmtEx.ExceptionStacktrace = ex.StackTrace;
                cmtEx.ExceptionDateTime = DateTime.Now;
                cmtEx.UserDescription = "GetVoucherBatch method failed";
                _logController.LogApplicationException(cmtEx);
            }

            return vb;
        }

        /// <summary>
        /// This method returns the the voucher volume code which needs to be used
        ///  in the ISPIF and NMS add volume methods.
        /// </summary>
        /// <param name="voucherVolumeId">The voucher volume Id, this value corresponds
        /// with the Id column in the VoucherVolumes table</param>
        /// <returns>The corresponding VolumeCode or null if the VolumeCode could not be found</returns>
        public int? GetVolumeCodeById(int voucherVolumeId)
        {
            int? volumeCode = null;
            string selectCmd = "SELECT VolumeCode FROM VoucherVolumes WHERE Id = @VolumeId";

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(selectCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@VolumeId", voucherVolumeId);
                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            volumeCode = reader.GetInt32(0);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDateTime = DateTime.UtcNow;
                cmtEx.ExceptionDesc = ex.Message;
                cmtEx.ExceptionStacktrace = ex.StackTrace;
                cmtEx.UserDescription = "GetVolumeCodeById failed";
                _logController.LogApplicationException(cmtEx);
            }

            return volumeCode;
        }

        /// <summary>
        /// This method adds a new voucher volume to the CMT database
        /// </summary>
        /// <param name="vv">The voucher volume object</param>
        /// <returns>True if successful</returns>
        public bool AddVoucherVolume(VoucherVolume vv)
        {
            string insertCmd = "INSERT INTO VoucherVolumes (Id, VolumeMB, UnitPrice, Description, CurrencyCode, VolumeCode, VolumeFWD, VolumeRTN, ValidityPeriod, UnitPriceEUR, UnitPriceUSD ,Sla) " +
                               "VALUES (@Id, @VolumeMB, @UnitPrice, @Description, @CurrencyCode, @VolumeCode, @VolumeFWD, @VolumeRTN, @ValidityPeriod, @UnitPriceEUR, @UnitPriceUSD, @Sla)";
            bool result = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(insertCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@Id", vv.Id);
                        cmd.Parameters.AddWithValue("@VolumeMB", vv.VolumeMB);
                        cmd.Parameters.AddWithValue("@UnitPrice", vv.UnitPrice);
                        cmd.Parameters.AddWithValue("@Description", vv.Description);
                        cmd.Parameters.AddWithValue("@CurrencyCode", vv.CurrencyCode);
                        cmd.Parameters.AddWithValue("@VolumeCode", vv.VolumeId);
                        cmd.Parameters.AddWithValue("@VolumeFWD", 0);
                        cmd.Parameters.AddWithValue("@VolumeRTN", 0);
                        cmd.Parameters.AddWithValue("@ValidityPeriod", vv.ValidityPeriod);
                        cmd.Parameters.AddWithValue("@UnitPriceEUR", vv.UnitPriceEUR);
                        cmd.Parameters.AddWithValue("@UnitPriceUSD", vv.UnitPriceUSD);
                        cmd.Parameters.AddWithValue("@Sla", vv.Sla);

                        cmd.ExecuteNonQuery();
                        result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOVoucherController: Error while adding voucher volume object", vv.Id.ToString(), (short)ExceptionLevelEnum.Error));
                result = false;
            }

            return result;
        }

        /// <summary>
        /// This method updates an existing voucher volume in the CMT database
        /// </summary>
        /// <param name="vv">The voucher volume object</param>
        /// <returns>True if successful</returns>
        public bool UpdateVoucherVolume(VoucherVolume vv)
        {
            string updateCmd = "UPDATE VoucherVolumes " +
                                "SET UnitPriceEUR = @UnitPriceEUR, UnitPriceUSD = @UnitPriceUSD, Description = @Description, Deleted = @Deleted " +
                               "WHERE Id = @Id";
            bool result = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(updateCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@Id", vv.Id);
                        cmd.Parameters.AddWithValue("@UnitPriceEUR", vv.UnitPriceEUR);
                        cmd.Parameters.AddWithValue("@UnitPriceUSD", vv.UnitPriceUSD);
                        cmd.Parameters.AddWithValue("@Description", vv.Description);
                        cmd.Parameters.AddWithValue("@CurrencyCode", vv.CurrencyCode);
                        cmd.Parameters.AddWithValue("@Deleted", vv.Deleted);

                        cmd.ExecuteNonQuery();
                        result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOVoucherController: Error while updating voucher volume object", vv.Id.ToString(), (short)ExceptionLevelEnum.Error));
                result = false;
            }

            return result;
        }

        /// <summary>
        /// Creates a new voucher requests
        /// </summary>
        /// <param name="vr">The voucher request to be created</param>
        /// <returns>True if successfull</returns>
        public bool CreacteVoucherRequest(VoucherRequest vr)
        {
            string insertCmd = "INSERT INTO VoucherRequests (RequestId, DistributorId, VoucherVolumeId, NumVouchers, RequestState, Sla) " +
                               "VALUES (@RequestId, @DistributorId, @VoucherVolumeId, @NumVouchers, @RequestState, @Sla)";
            bool result = false;
            int requestId = this.GetMaxRequestId() + 1;

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(insertCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@RequestId", requestId);
                        cmd.Parameters.AddWithValue("@DistributorId", vr.DistributorId);
                        cmd.Parameters.AddWithValue("@VoucherVolumeId", vr.VoucherVolumeId);
                        cmd.Parameters.AddWithValue("@NumVouchers", vr.NumVouchers);
                        cmd.Parameters.AddWithValue("@Sla", vr.Sla);
                        cmd.Parameters.AddWithValue("@RequestState", 0);

                        cmd.ExecuteNonQuery();
                        result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOVoucherController: Error while creating new voucher request", "Request ID: " + requestId + ", Distributor ID: " + vr.DistributorId, (short)ExceptionLevelEnum.Error));
                result = false;
            }

            return result;
        }

        /// <summary>
        /// Updates the state of a voucher requests
        /// </summary>
        /// <param name="vr">The voucher request to be updated</param>
        /// <returns>True if successful</returns>
        public bool UpdateVoucherRequest(VoucherRequest vr)
        {
            string updateCmd;

            if (vr.RequestState == 2)
            {
                updateCmd = "DELETE FROM VoucherRequests " +
                                  "WHERE RequestId = @RequestId";
            }
            else
            {
                updateCmd = "UPDATE VoucherRequests " +
                                   "SET RequestState = @RequestState " +
                                  "WHERE RequestId = @RequestId";
            }
            bool result = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(updateCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@RequestId", vr.RequestId);
                        cmd.Parameters.AddWithValue("@RequestState", vr.RequestState);

                        cmd.ExecuteNonQuery();
                        result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOVoucherController: Error while updating voucher request", vr.RequestId.ToString(), (short)ExceptionLevelEnum.Error));
                result = false;
            }

            return result;
        }

        /// <summary>
        /// Fetches a voucher request from the CMT database
        /// </summary>
        /// <param name="vrId">The ID of the voucher request</param>
        /// <returns>The voucher request object</returns>
        public VoucherRequest GetVoucherRequest(int vrId)
        {
            string queryCmd = "SELECT RequestId, DistributorId, VoucherVolumeId, NumVouchers, RequestState FROM VoucherRequests " +
                              "WHERE RequestId = @RequestId";

            VoucherRequest vr = new VoucherRequest();

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(queryCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@RequestId", vrId);
                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            vr.RequestId = reader.GetInt32(0);
                            vr.DistributorId = reader.GetInt32(1);
                            vr.VoucherVolumeId = reader.GetInt32(2);
                            vr.NumVouchers = reader.GetInt32(3);
                            vr.RequestState = reader.GetInt32(4);

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOVoucherController: Error while fetching voucher request", vrId.ToString(), (short)ExceptionLevelEnum.Error));
            }

            return vr;
        }

        /// <summary>
        /// Fetches all pending voucher requests
        /// </summary>
        /// <returns>A list of voucher requests</returns>
        public List<VoucherRequest> GetAllPendingVoucherRequests()
        {
            string queryCmd = "SELECT RequestId, DistributorId, VoucherVolumeId, NumVouchers, RequestState FROM VoucherRequests " +
                              "WHERE RequestState = @RequestState";

            VoucherRequest vr;
            List<VoucherRequest> vrList = new List<VoucherRequest>();

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(queryCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@RequestState", 0);
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            vr = new VoucherRequest();
                            vr.RequestId = reader.GetInt32(0);
                            vr.DistributorId = reader.GetInt32(1);
                            vr.VoucherVolumeId = reader.GetInt32(2);
                            vr.NumVouchers = reader.GetInt32(3);
                            vr.RequestState = reader.GetInt32(4);
                            vrList.Add(vr);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOVoucherController: Error while fetching voucher requests", ex.Message, (short)ExceptionLevelEnum.Error));
            }

            return vrList;
        }

        /// <summary>
        /// Fetches all vouchers for a voucher report based on certain criteria
        /// </summary>
        /// <param name="startDate">The begin date of the reporting period. The date is the date the voucher was validated</param>
        /// <param name="endDate">The end date of the reporting period. The date is the date the voucher was validated</param>
        /// <param name="batchId">The ID of the batch the vouchers belong to. Optional</param>
        /// <param name="released">True if only released vouchers should be included. Currently not used</param>
        /// <returns>A list of vouchers</returns>
        public List<Voucher> GetVouchersForReport(DateTime startDate, DateTime endDate, int batchId, bool released)
        {
            string queryCmd = "SELECT Code, Available, BatchId, DateValidated, MacAddress FROM Vouchers " +
                              "WHERE DateValidated > @StartDate AND DateValidated < @EndDate";
            //if (released)
            //{
            //    queryCmd += " AND Available = 1";
            //}
            //else
            //{
            //    queryCmd += " AND Available = 0";
            //}

            if (batchId > 0)
            {
                queryCmd += " AND BatchId = @BatchId";
            }

            List<Voucher> voucherList = new List<Voucher>();
            Voucher v;

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(queryCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@StartDate", startDate);
                        cmd.Parameters.AddWithValue("@EndDate", endDate);
                        if (batchId > 0)
                        {
                            cmd.Parameters.AddWithValue("@BatchId", batchId);
                        }
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            v = new Voucher();
                            v.Code = reader.GetString(0);
                            v.Available = reader.GetBoolean(1);
                            v.BatchId = reader.GetInt32(2);
                            v.DateValidated = reader.GetDateTime(3);
                            v.MacAddress = reader.GetString(4);
                            voucherList.Add(v);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOVoucherController: Error while fetching vouchers for report", ex.Message, (short)ExceptionLevelEnum.Error));
            }

            return voucherList;
        }

        #endregion

        /// <summary>
        /// A helper method for finding the highest voucher request ID currently in the CMT db
        /// </summary>
        /// <returns>The current highest voucher request ID</returns>
        private int GetMaxRequestId()
        {
            const string queryCmd = "SELECT MAX(RequestId) FROM VoucherRequests";

            int requestId = 0;
            try
            {
                using (var conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (var cmd = new SqlCommand(queryCmd, conn))
                    {
                        SqlDataReader reader = cmd.ExecuteReader();

                        reader.Read();
                        requestId = reader.GetInt32(0);
                    }
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOVoucherController: error in method GetMaxRequestId", ex.Message, (short)ExceptionLevelEnum.Error));
            }
            return requestId;
        }

        /// <summary>
        /// Allows a distributor to enter his retail price for vouchers
        /// </summary>
        /// <param name="distributorId">The ID of the distributor setting the price</param>
        /// <param name="voucherVolumeId">The ID of the voucher volume for which the price is set</param>
        /// <param name="unitPriceUSD">The unit price in Euros</param>
        /// <param name="unitPriceEUR">The unit price in US Dollars</param>
        /// <returns></returns>
        public bool SetVoucherRetailPrice(int distributorId, int voucherVolumeId, double unitPriceUSD, double unitPriceEUR)
        {
            bool success = false;

            //first check if record already exists
            bool update = false;
            string queryCmd = "SELECT * FROM VoucherRetailPrices WHERE DistributorId = @DistributorId AND VoucherVolumeId = @VoucherVolumeId";
            try
            {
                using (var conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (var cmd = new SqlCommand(queryCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@DistributorId", distributorId);
                        cmd.Parameters.AddWithValue("@VoucherVolumeId", voucherVolumeId);
                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            update = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOVoucherController: error in method SetVoucherRetailPrice", ex.Message, (short)ExceptionLevelEnum.Error));
            }

            string sqlCmd;
            if (update)
            {
                sqlCmd = "UPDATE VoucherRetailPrices " +
                         "SET UnitPriceUSD = @UnitPriceUSD, UnitPriceEUR = @UnitPriceEUR " +
                         "WHERE DistributorId = @DistributorId AND VoucherVolumeId = @VoucherVolumeId";
            }
            else
            {
                sqlCmd = "INSERT INTO VoucherRetailPrices " +
                         "(DistributorId, VoucherVolumeId, UnitPriceUSD, UnitPriceEUR) " +
                         "VALUES (@DistributorId, @VoucherVolumeId, @UnitPriceUSD, @UnitPriceEUR)";
            }

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sqlCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@DistributorId", distributorId);
                        cmd.Parameters.AddWithValue("@VoucherVolumeId", voucherVolumeId);
                        cmd.Parameters.AddWithValue("@UnitPriceUSD", unitPriceUSD);
                        cmd.Parameters.AddWithValue("@UnitPriceEUR", unitPriceEUR);

                        cmd.ExecuteNonQuery();
                        success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOVoucherController: error in method SetVoucherRetailPrice", ex.Message, (short)ExceptionLevelEnum.Error));
                success = false;
            }

            return success;
        }

        /// <summary>
        /// Fetches the voucher retail prices as set by a specific distributor
        /// </summary>
        /// <param name="distributorId">The ID of the distributor</param>
        /// <returns>A list of voucher volume objects containing the ID of the voucher volume and the retail prices of the distributor</returns>
        public List<VoucherVolume> GetVoucherRetailPricesForDistributor(int distributorId)
        {
            List<VoucherVolume> vvList = new List<VoucherVolume>();
            VoucherVolume vv;
            string queryCmd = "SELECT VoucherVolumeId, UnitPriceUSD, UnitPriceEUR FROM VoucherRetailPrices WHERE DistributorId = @DistributorId";
            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(queryCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@DistributorId", distributorId);

                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            vv = new VoucherVolume();
                            vv.Id = reader.GetInt32(0);
                            if (!reader.IsDBNull(1))
                            {
                                vv.UnitPriceUSD = (double)reader.GetDecimal(1);
                            }
                            if (!reader.IsDBNull(2))
                            {
                                vv.UnitPriceEUR = (double)reader.GetDecimal(2);
                            }
                            vvList.Add(vv);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOVoucherController: Error while fetching retail prices for distributor " + distributorId, ex.Message, (short)ExceptionLevelEnum.Error));
            }

            return vvList;
        }

        /// <summary>
        /// Returns batchId for activated voucher on a specific terminal at determined time
        /// </summary>
        /// <param name="macAddress">The mac address of terminal where voucher was validated</param>
        /// <param name="vdate">validation date when voucher was validated</param>
        /// <returns>batchid</returns>
        public int getBatchIDforValidatedVoucher(string macAddress, DateTime vdate)
        {
            string queryCmd = "SELECT batchid FROM Vouchers " +
                              "WHERE MacAddress like @mac and Convert(Date,DateValidated)=Convert(Date,@vd)";
            int result = -1;
            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(queryCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@mac", macAddress);
                        cmd.Parameters.AddWithValue("@vd", vdate);
                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            result = reader.GetInt32(0);

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOVoucherController: Error while fetching voucher info for mac address", macAddress, (short)ExceptionLevelEnum.Error));
            }

            return result;
        }


        /// <summary>
        /// Returns BatchInfo list for a specific period
        /// </summary>
        /// <param name="distID">The distributor ID</param>
        /// <param name="paid">paid</param>
        /// <param name="batchesfrom">start date to get batches info</param>
        /// <param name="batchesto">end date to get batches info</param>
        /// <returns>list of voucher batch infor</returns>
        public List<VoucherBatchInfo> getVouchersBatchesReport(int distID, int paid, DateTime? batchesfrom, DateTime? batchesto)
        {
            DateTime defaultstart = DateTime.Now.AddDays(-90);
            DateTime defaultend = DateTime.Now;
            string distributorOptional = "And VoucherBatches.DistributorId = @DistributorId ";
            string paidStatus = "";
            if (distID == 0)
                distributorOptional = " ";
            if (paid == 0 || paid == 1)
            {   // 0 = unpaid 1 = paid 2 = All 
                paidStatus = " And VoucherBatches.Paid=" + paid + " ";
            }
            else if (paid == 3) // released
            {
                paidStatus = " And VoucherBatches.Release=1 ";
            }
            else if (paid == 4) // release and paid
            {
                paidStatus = " And VoucherBatches.Paid=1 And VoucherBatches.Release=1 ";
            }
            else if (paid == 5) // release and unpaid
            {
                paidStatus = " And VoucherBatches.Paid=0 And VoucherBatches.Release=1 ";
            }

            string selectCmd = "SELECT VoucherBatches.BatchId, VoucherBatches.DateCreated, COUNT(Vouchers.Code) AS NumVouchers, " +
                               "VoucherBatches.DistributorId, aspnet_Users.UserName, aspnet_Users.UserId, VoucherVolumes.Id, " +
                               "VoucherBatches.Release, VoucherBatches.Paid, VoucherBatches.PaymentMethod " +
                               "FROM aspnet_Users INNER JOIN " +
                               "VoucherBatches ON aspnet_Users.UserId = VoucherBatches.UserId INNER JOIN " +
                               "Vouchers ON VoucherBatches.BatchId = Vouchers.BatchId INNER JOIN " +
                               "VoucherVolumes ON VoucherBatches.VolumeId = VoucherVolumes.Id " +
                               "WHERE VoucherBatches.DateCreated>=@batchesfrom and VoucherBatches.DateCreated<=@batchesto " +
                               distributorOptional + paidStatus +
                               "GROUP BY aspnet_Users.UserName, aspnet_Users.UserId, VoucherBatches.BatchId, VoucherBatches.DateCreated, VoucherBatches.DistributorId, " +
                               "VoucherVolumes.Id, VoucherBatches.Release, VoucherBatches.Paid, VoucherBatches.PaymentMethod " +
                               "ORDER BY VoucherBatches.BatchId";
            List<VoucherBatchInfo> vbiList = null;

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(selectCmd, conn))
                    {
                        if (distID != 0)
                            cmd.Parameters.AddWithValue("@DistributorId", distID);

                        if (batchesfrom != null && batchesto != null)
                        {
                            cmd.Parameters.AddWithValue("@batchesfrom", batchesfrom);
                            cmd.Parameters.AddWithValue("@batchesto", batchesto);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@batchesfrom", defaultstart);
                            cmd.Parameters.AddWithValue("@batchesto", defaultend);
                        }
                        SqlDataReader reader = cmd.ExecuteReader();
                        vbiList = new List<VoucherBatchInfo>();
                        while (reader.Read())
                        {
                            VoucherBatchInfo v = new VoucherBatchInfo();
                            v.BatchId = reader.GetInt32(0);
                            v.DateCreated = reader.GetDateTime(1);
                            v.NumVouchers = reader.GetInt32(2);
                            v.DistributorId = reader.GetInt32(3);
                            v.UserName = reader.GetString(4);
                            v.UserId = reader.GetGuid(5);
                            v.Volume = reader.GetInt32(6);
                            v.Release = reader.GetBoolean(7);
                            v.Paid = reader.GetBoolean(8);
                            if (!reader.IsDBNull(9))
                                v.PaymentMethod = reader.GetString(9);
                            else
                                v.PaymentMethod = "Unknown";

                           vbiList.Add(v);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOVoucherController: Error while returning voucher info ", "getVouchersBatchesReport"));
                throw;
            }

            return vbiList;
        }


        /// <summary>
        /// Returns available vouchers for a batch
        /// </summary>
        /// <param name="batchID">The Batch ID</param>
        /// <returns>list of voucher batch infor</returns>
        public int getAvailableVouchersforbatch(int batchID)
        {
            string queryCmd = "SELECT voucherbatches.BatchId,count(Available) " +
                "FROM VoucherBatches inner join Vouchers on VoucherBatches.BatchId = Vouchers.BatchId " +
                "where Available = 1 and voucherbatches.BatchId = @batchID " +
                "group by voucherbatches.BatchId";
            int result = 0;
            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(queryCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@batchID", batchID);

                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            result = reader.GetInt32(1);

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOVoucherController: Error while fetching available vouchers for batch", batchID.ToString(), (short)ExceptionLevelEnum.Error));
            }

            return result;
        }


        /// <summary>
        /// Inserts the Distributor Voucher volume in the DistVouchers table
        /// </summary>
        /// <param name="dv">The distributor voucher volume to insert</param>
        /// <returns>True if the operation succeeded</returns>
        public bool AddDistVoucher(DistVoucher dv)
        {
            bool success = false;
            string sqlCmd = "INSERT INTO DistVouchers (Vvid, DistId, PriceEU, PriceUSD, Remarks, UserId, LastUpdate, Deleted) " +
                            "VALUES (@vvid, @distId, @priceEU, @priceUSD, @remarks, @userId, @lastUpdate, @deleted)";
            if (!DistVoucherExists(dv.DistId, dv.Vvid))
            {
                try
                {
                    using (SqlConnection conn = new SqlConnection(_connectionString))
                    {
                        conn.Open();

                        using (SqlCommand cmd = new SqlCommand(sqlCmd, conn))
                        {
                            cmd.Parameters.AddWithValue("@vvid", dv.Vvid);
                            cmd.Parameters.AddWithValue("@distId", dv.DistId);
                            cmd.Parameters.AddWithValue("@priceEU", dv.PriceEU);
                            cmd.Parameters.AddWithValue("@priceUSD", dv.PriceUSD);
                            cmd.Parameters.AddWithValue("@remarks", dv.Remarks);
                            cmd.Parameters.AddWithValue("@userId", dv.UserId);
                            cmd.Parameters.AddWithValue("@lastUpdate", dv.LastUpdate);
                            cmd.Parameters.AddWithValue("@deleted", dv.Deleted);
                            cmd.ExecuteNonQuery();
                            success = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    _logController.LogApplicationException(new CmtApplicationException(ex, "BOVoucherController: Error while fetching available vouchers for batch", dv.Vvid.ToString(), (short)ExceptionLevelEnum.Error));

                }
            }

            return success;
        }

        /// <summary>
        /// Updates the DistVoucher entity in the DistVouchers table
        /// </summary>
        /// <param name="dv">The DistVoucher entity to update</param>
        /// <returns>True if the operation succeeded</returns>
        public bool UpdateDistVoucher(DistVoucher dv)
        {
            bool success = false;
            string sqlCmd = "UPDATE DistVouchers SET Vvid = @vvid, DistId = @distId, PriceEU = @priceEU, PriceUSD = @priceUSD, " +
                            "Remarks = @remarks, UserId = @userId, LastUpdate = @lastUpdate, Deleted = @deleted " +
                            "WHERE Id = @id";

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(sqlCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@vvid", dv.Vvid);
                        cmd.Parameters.AddWithValue("@distId", dv.DistId);
                        cmd.Parameters.AddWithValue("@priceEU", dv.PriceEU);
                        cmd.Parameters.AddWithValue("@priceUSD", dv.PriceUSD);
                        cmd.Parameters.AddWithValue("@remarks", dv.Remarks);
                        cmd.Parameters.AddWithValue("@userId", dv.UserId);
                        cmd.Parameters.AddWithValue("@lastUpdate", dv.LastUpdate);
                        cmd.Parameters.AddWithValue("@deleted", dv.Deleted);
                        cmd.Parameters.AddWithValue("@id", dv.Id);
                        cmd.ExecuteNonQuery();
                        success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                _logController.LogApplicationException(new CmtApplicationException(ex, "BOVoucherController: Error while fetching available vouchers for batch", dv.Vvid.ToString(), (short)ExceptionLevelEnum.Error));
            }

            return success;
        }



        /// <summary>
        /// Returns a distributor voucher volume identified by its  id
        /// </summary>
        /// <param name="distVoucherId">Identifier of the customized voucher volume</param>
        /// <returns>A distVoucher instance or null if not found</returns>
        public DistVoucher GetDistVoucher(int distVoucherId)
        {
            DistVoucher db = new DistVoucher();
            const string sql = @"
                                SELECT Id, Vvid, DistId, PriceEU, PriceUSD, Remarks, UserId, LastUpdate, Deleted FROM DistVouchers
                                WHERE Id = @distVoucherId
                                ";

            try
            {
                using (var con = new SqlConnection(_connectionString))
                {
                    con.Open();

                    using (var cmd = new SqlCommand(sql, con))
                    {
                        cmd.Parameters.AddWithValue("@distVoucherId", distVoucherId);
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            db.Id = reader.GetInt32(0);
                            db.Vvid = reader.GetInt32(1);
                            db.DistId = reader.GetInt32(2);
                            db.PriceEU = reader.GetDecimal(3);
                            db.PriceUSD = reader.GetDecimal(4);
                            if (!reader.IsDBNull(5))
                            {
                                db.Remarks = reader.GetString(5);
                            }
                            db.UserId = reader.GetGuid(6);
                            db.LastUpdate = reader.GetDateTime(7);
                            db.Deleted = reader.GetBoolean(8);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                _logController.LogApplicationException(new CmtApplicationException(ex, "BOVoucherController: Error while fetching DistVoucher: ", distVoucherId.ToString(), (short)ExceptionLevelEnum.Error));

            }
            return db;
        }
    }
}
