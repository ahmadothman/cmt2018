﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using BOControllerLibrary.Controllers.Interfaces;
using BOControllerLibrary.Model;
using BOControllerLibrary.Model.Log;
using BOControllerLibrary.Model.Terminal;
using BOControllerLibrary.Model.User;
using PetaPoco;
using log4net;
using System.Collections;

namespace BOControllerLibrary.Controllers
{
    public class BOLogController : BODataControllerBase, IBOLogController
    {
        private readonly ILog _log = LogManager.GetLogger(typeof(IBOLogController));

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="connectionString">The application connections string</param>
        /// <param name="databaseProvider">The application database provider</param>
        public BOLogController(string connectionString, string databaseProvider)
        {
            ConnectionString = connectionString;
            DataProvider = databaseProvider;
        }

        #region Implementation of IBOLogController

        /// <summary>
        ///   Log all user and application exceptions. This is the ExceptionActivityLog table in the CMT database.
        /// </summary>
        public void LogApplicationException(CmtApplicationException ex)
        {
            const string query = "INSERT INTO ExceptionActivityLog (ExceptionDateTime, ExceptionDesc, ExceptionLevel, ExceptionStacktrace, UserDescription, StateInformation) " +
                                     "VALUES (@ExceptionDateTime, @ExceptionDesc, @ExceptionLevel, @ExceptionStacktrace, @UserDescription, @StateInformation) ";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {

                        if (ex.ExceptionLevel == null)
                            ex.ExceptionLevel = (short)ExceptionLevelEnum.Info;

                        cmd.Parameters.AddWithValue("@ExceptionDateTime", ex.ExceptionDateTime);
                        if (ex.ExceptionDesc.Length > 400)
                        {
                            cmd.Parameters.AddWithValue("@ExceptionDesc", ex.ExceptionDesc.Substring(0, 400));
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@ExceptionDesc", ex.ExceptionDesc);
                        }
                        if (ex.ExceptionLevel == null)
                        {
                            ex.ExceptionLevel = 1; // Set to info in this case
                        }
                        cmd.Parameters.AddWithValue("@ExceptionLevel", ex.ExceptionLevel);

                        if (ex.ExceptionStacktrace == null)
                        {
                            ex.ExceptionStacktrace = "";
                        }
                        cmd.Parameters.AddWithValue("@ExceptionStacktrace", ex.ExceptionStacktrace);

                        if (ex.UserDescription == null)
                        {
                            ex.UserDescription = "";
                        }
                        cmd.Parameters.AddWithValue("@UserDescription", ex.UserDescription);

                        if (ex.StateInformation == null)
                        {
                            ex.StateInformation = "";
                        }
                        cmd.Parameters.AddWithValue("@StateInformation", ex.StateInformation);

                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception exception)
            {
                _log.Debug(exception.Message);
            }
        }

        /// <summary>
        ///   Logs the terminal activity.
        /// </summary>
        /// <param name = "activity">The activity.</param>
        public void LogTerminalActivity(TerminalActivity activity)
        {
            _log.Debug("LogTerminalActivity:start");
            using (var db = new Database(ConnectionString, DataProvider))
            {
                var spResult = db.Execute("exec [cmtInsertTerminalActivityLog4] @UserId, @MacAddress, @ActionDate, @Action, @SlaName, @NewSlaName, @AccountingFlag, @Prepaid, @Comment",
                     new
                         {
                             UserId = activity.UserId
                             ,
                             activity.MacAddress
                             ,
                             activity.ActionDate
                             ,
                             activity.Action
                             ,
                             activity.SlaName
                             ,
                             activity.NewSlaName
                             ,
                             activity.AccountingFlag
                             ,
                             activity.Prepaid
                             ,
                             activity.Comment
                             
                             
                         }
                    );
                _log.Debug("Called cmtInsertTerminalActivityLog and returned " + spResult);
            }

            _log.Debug("LogTerminalActivity:end");
        }

        /// <summary>
        ///   Log the activities of a user (Log-In, Log-Out, Session expiration)
        /// </summary>
        /// <param name = "activity">The activity.</param>
        public void LogUserActivity(UserActivity activity)
        {
            _log.Debug("LogUserActivity:start");
            using (var db = new Database(ConnectionString, DataProvider))
            {
                var spResult = db.Execute("exec [cmtInsertUserActivityLog2]   @UserId, @ActionDate, @Action",
                     new
                     {
                         activity.UserId,
                         activity.ActionDate,
                         activity.Action
                     }
                    );
                _log.Debug("Called cmtInsertUserActivityLog and returned " + spResult);
            }

            _log.Debug("LogUserActivity:end");
        }

        /// <summary>
        ///   Return a list of activities for a given terminal
        /// </summary>
        /// <param name = "macAddress">The MAC address of the terminal</param>
        /// <returns>A list of terminal activities for the given MAC address</returns>
        public List<TerminalActivity> GetTerminalActivity(string macAddress)
        {
            /*
                    public string UserId { get; set; }
        public string MacAddress { get; set; }
        public DateTime ActionDate { get; set; }
        public string Action { get; set; }
        */
            const string sql = "select t.* " +
                               "from VW_TerminalActivityLog t " +
                               "where t.MacAddress = @macAddress order by t.Id desc";

            using (var db = new Database(ConnectionString, DataProvider))
            {
                var dbObj = db.Fetch<TerminalActivity>(sql, new { macAddress });
                return dbObj;
            }
        }

        /// <summary>
        ///   Return a list of exceptions over a given period
        /// </summary>
        /// <param name = "from"></param>
        /// <param name = "to"></param>
        /// <returns></returns>
        public List<CmtApplicationException> GetApplicationExceptions(DateTime from, DateTime to)
        {
            const string sql = "select t.* " +
                               "from ExceptionActivityLog t " +
                               "where t.ExceptionDateTime between @from and @to";

            using (var db = new Database(ConnectionString, DataProvider))
            {
                var dbObj = db.Fetch<CmtApplicationException>(sql, new { from, to });
                return dbObj;
            }
        }

        /// <summary>
        ///   Return a list of activities for a given user.
        /// </summary>
        /// <param name = "userId"></param>
        /// <returns></returns>
        public List<UserActivity> GetUserActivities(string userId)
        {
            const string sql = "select t.* " +
                               "from VW_UserActivityLog t " +
                               "where t.userid = @userId";

            using (var db = new Database(ConnectionString, DataProvider))
            {
                var dbObj = db.Fetch<UserActivity>(sql, new { userId });
                return dbObj;
            }
        }


        /// <summary>
        /// Returns a list of activities for the given SitId and IspId
        /// </summary>
        /// <remarks>
        /// This method returns a list of TerminalActivityExt objects. These 
        /// objects contain next to the UserId GUID, the name of the user who
        /// executed the terminal activity.<br/>
        /// <b>The query executes a left outerjoin between the TerminalActivityLog table and 
        /// the aspnet_Users table. In case a user is deleted from the Users table, the activity log
        /// record will still show up but the UserName is set to NULL</b>
        /// </remarks>
        /// <param name="SitId">The Site Identifier of the terminal</param>
        /// <param name="IspId">The ISP Identifier of the terminal</param>
        /// <returns>A list of terminal activities for the given SitId and IspId</returns>
        public List<TerminalActivityExt> GetTerminalActivityBySitId(int SitId, int IspId)
        {
            List<TerminalActivityExt> termActivityList = new List<TerminalActivityExt>();
            string queryCmd = "SELECT TerminalActivityLog.Id, TerminalActivityLog.UserId, TerminalActivityLog.MacAddress, TerminalActivityLog.ActionDate, TerminalActivityLog.TerminalActivityTypeId, " +
                         "TerminalActivityLog.SlaName, TerminalActivityLog.NewSlaName, TerminalActivityLog.AccountingFlag, " +
                         "TerminalActivityTypes.Action, aspnet_Users.UserName, TerminalActivityLog.Comment " +
                         "FROM TerminalActivityLog INNER JOIN " +
                         "Terminals AS Terminals_1 ON TerminalActivityLog.MacAddress = Terminals_1.MacAddress INNER JOIN " +
                         "TerminalActivityTypes ON TerminalActivityLog.TerminalActivityTypeId = TerminalActivityTypes.Id LEFT OUTER JOIN " +
                         "aspnet_Users ON TerminalActivityLog.UserId = aspnet_Users.UserId " +
                         "WHERE (Terminals_1.SitId = @SitId) AND (Terminals_1.IspId = @IspId) " +
                         "ORDER BY TerminalActivityLog.Id DESC";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(queryCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@SitId", SitId);
                        cmd.Parameters.AddWithValue("@IspId", IspId);
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            TerminalActivityExt termActivityExt = new TerminalActivityExt();
                            termActivityExt.Id = reader.GetInt32(0);
                            termActivityExt.UserId = reader.GetGuid(1);
                            termActivityExt.MacAddress = reader.GetString(2);
                            termActivityExt.ActionDate = reader.GetDateTime(3);
                            termActivityExt.TerminalActivityId = reader.GetInt32(4);

                            if (!reader.IsDBNull(5))
                            {
                                termActivityExt.SlaName = reader.GetString(5);
                            }
                            else
                            {
                                termActivityExt.SlaName = "";
                            }

                            if (!reader.IsDBNull(6))
                            {
                                termActivityExt.NewSlaName = reader.GetString(6);
                            }
                            else
                            {
                                termActivityExt.NewSlaName = "";
                            }

                            termActivityExt.AccountingFlag = reader.GetBoolean(7);
                            termActivityExt.Action = reader.GetString(8);

                            if (!reader.IsDBNull(9))
                            {
                                termActivityExt.UserName = reader.GetString(9);
                            }
                            else
                            {
                                termActivityExt.UserName = "";
                            }

                            if (!reader.IsDBNull(10))
                            {
                                termActivityExt.Comment = reader.GetString(10).Trim();
                            }
                            else
                            {
                                termActivityExt.Comment = "";
                            }

                            termActivityList.Add(termActivityExt);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDesc = ex.Message;
                cmtEx.ExceptionDateTime = DateTime.Now;
                cmtEx.ExceptionStacktrace = ex.StackTrace;
                cmtEx.UserDescription = "Retrieving the TerminalActivityExt list failed";
                cmtEx.ExceptionLevel = (short)ExceptionLevelEnum.Error;
                this.LogApplicationException(cmtEx);
            }
            return termActivityList;
        }

        /// <summary>
        ///   Lists all types of terminal activity. New ones are currently only supported through autogeneration (so no description yet): see storing terminal activities
        /// </summary>
        /// <returns></returns>
        public List<ActivityType> GetTerminalActivityTypes()
        {
            const string sql = "select t.* " +
                               "from TerminalActivityTypes t ";
            using (var db = new Database(ConnectionString, DataProvider))
            {
                var dbObj = db.Fetch<ActivityType>(sql);
                return dbObj;
            }
        }

        /// <summary>
        ///   Lists all types of users activity. New ones are currently only supported through autogeneration (so no description yet): see storing user activities
        /// </summary>
        /// <returns></returns>
        public List<ActivityType> GetUserActivityTypes()
        {
            const string sql = "select t.* " +
                               "from UserActivityTypes t ";
            using (var db = new Database(ConnectionString, DataProvider))
            {
                var dbObj = db.Fetch<ActivityType>(sql);
                return dbObj;
            }
        }

        /// <summary>
        /// Returns a list of terminal activities between two dates
        /// </summary>
        /// <param name="startDate">The start date for the list</param>
        /// <param name="endDate">The end date for the list</param>
        /// <returns>A datatabel containing the result set</returns>
        public DataTable GetTermActivitiesBetweenDates(DateTime startDate, DateTime endDate)
        {
            string accountReportQuery = "SELECT TerminalActivityLog.Id, Terminals.DistributorId, Distributors.FullName AS 'Distributor Name', TerminalActivityLog.ActionDate, " +
            "TerminalActivityLog.MacAddress, Terminals.FullName AS 'Terminal Name', Terminals.SitId, " +
            "TerminalActivityTypes.Action, TerminalActivityLog.AccountingFlag, TerminalActivityLog.Comment FROM TerminalActivityLog, TerminalActivityTypes, Terminals, Distributors " +
            "WHERE ActionDate >= @StartDate AND ActionDate <= @EndDate " +
            "AND TerminalActivityLog.TerminalActivityTypeId = TerminalActivityTypes.Id " +
            "AND TerminalActivityLog.MacAddress = Terminals.MacAddress " +
            "AND Terminals.DistributorId = Distributors.Id " +
            "ORDER BY TerminalActivityLog.Id DESC";

            DataTable accountReportData = new DataTable();
            accountReportData.TableName = "AccountReport";
            SqlConnection conn = new SqlConnection(ConnectionString);
            SqlDataAdapter adapter = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand(accountReportQuery, conn);

            SqlParameter startDateParam = new SqlParameter();
            startDateParam.ParameterName = "@StartDate";
            startDateParam.SqlDbType = SqlDbType.Date;
            startDateParam.Value = startDate.Date;
            cmd.Parameters.Add(startDateParam);

            SqlParameter endDateParam = new SqlParameter();
            endDateParam.ParameterName = "@EndDate";
            endDateParam.SqlDbType = SqlDbType.Date;
            endDateParam.Value = endDate.Date;
            cmd.Parameters.Add(endDateParam);

            adapter.SelectCommand = cmd;

            conn.Open();

            try
            {
                adapter.Fill(accountReportData);
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtException = new CmtApplicationException();
                cmtException.ExceptionDesc = ex.Message;
                cmtException.ExceptionDateTime = DateTime.Now;
                cmtException.UserDescription = "GetTermActivitiesBetweenDates method failed";
                cmtException.ExceptionLevel = (short)ExceptionLevelEnum.Error;
                this.LogApplicationException(cmtException);
                accountReportData = null;
            }
            finally
            {
                conn.Close();
            }

            return accountReportData;
        }

        /// <summary>
        /// Returns a list of activities for a given terminal and 
        /// activity type between a start and end time
        /// </summary>
        /// <param name="startTime">The start time of the query</param>
        /// <param name="endTime">The end time of query</param>
        /// <param name="activityType">The activity type</param>
        /// <param name="macAddress">The terminals MAC address</param>
        /// <returns>A list of terminal activities</returns>
        public List<TerminalActivity> GetTerminalActivityExt(DateTime startTime, DateTime endTime, int activityType, string macAddress)
        {
            List<TerminalActivity> termActivities = new List<TerminalActivity>();
            string queryCmd = "SELECT Id, UserId, MacAddress, ActionDate, SlaName, NewSlaName, AccountingFlag, Comment FROM TerminalActivityLog " +
                              "WHERE TerminalActivityTypeId = @ActivityType AND MacAddress = @MacAddress AND " +
                              "AccountingFlag=1 AND ActionDate >= @StartTime AND ActionDate <= @EndTime";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(queryCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("ActivityType", activityType);
                        cmd.Parameters.AddWithValue("@MacAddress", macAddress);
                        cmd.Parameters.AddWithValue("@StartTime", startTime);
                        cmd.Parameters.AddWithValue("@EndTime", endTime);

                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            TerminalActivity termActivity = new TerminalActivity();
                            termActivity.Id = reader.GetInt32(0);
                            termActivity.UserId = reader.GetGuid(1);
                            termActivity.MacAddress = reader.GetString(2);
                            termActivity.ActionDate = reader.GetDateTime(3);

                            if (!reader.IsDBNull(4))
                            {
                                termActivity.SlaName = reader.GetString(4);
                            }
                            else
                            {
                                termActivity.SlaName = "";
                            }

                            if (!reader.IsDBNull(5))
                            {
                                termActivity.NewSlaName = reader.GetString(5);
                            }
                            else
                            {
                                termActivity.NewSlaName = "";
                            }
                            termActivity.AccountingFlag = reader.GetBoolean(6);

                            if (!reader.IsDBNull(7))
                            {
                                termActivity.Comment = reader.GetString(7).Trim();
                            }
                            else
                            {
                                termActivity.Comment = "";
                            }

                            termActivities.Add(termActivity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDateTime = DateTime.Now;
                cmtEx.ExceptionDesc = ex.Message;
                cmtEx.UserDescription = "GetTerminalActivityExt failed";
                cmtEx.ExceptionLevel = (short)ExceptionLevelEnum.Error;
                this.LogApplicationException(cmtEx);
            }

            return termActivities;
        }

        /// <summary>
        /// Updates a terminal activity in the terminal acitivty log
        /// </summary>
        /// <param name="activity">The modified terminal activity</param>
        /// <returns>True if the method succeeds</returns>
        public void UpdateTerminalActivity(TerminalActivity activity)
        {
            //Boolean success = false;
            //string updateCmd = "UPDATE TerminalActivityLog SET UserId = @UserId, " +
            //    "MacAddress = @MacAddress, ActionDate = @ActionDate, " +
            //    "TerminalActivityTypeId = @TerminalActivityTypeId, SlaName = @SlaName, " +
            //    "NewSlaName = @NewSlaName, AccountingFlag = @AccountingFlag, Comment = @Comment WHERE Id = @Id";

            //try
            //{
            //    using (SqlConnection conn = new SqlConnection(ConnectionString))
            //    {
            //        conn.Open();

            //        using (SqlCommand cmd = new SqlCommand(updateCmd, conn))
            //        {
            //            cmd.Parameters.AddWithValue("@UserId", activity.UserId);
            //            cmd.Parameters.AddWithValue("@MacAddress", activity.MacAddress);
            //            cmd.Parameters.AddWithValue("@ActionDate", activity.ActionDate);
            //            cmd.Parameters.AddWithValue("@TerminalActivityTypeId", (int)activity.TerminalActivityId);
            //            cmd.Parameters.AddWithValue("@SlaName", activity.SlaName);
            //            cmd.Parameters.AddWithValue("@NewSlaName", activity.NewSlaName);
            //            cmd.Parameters.AddWithValue("@Id", activity.Id);
            //            cmd.Parameters.AddWithValue("@AccountingFlag", activity.AccountingFlag);
            //            cmd.Parameters.AddWithValue("@Comment", activity.Comment);
            //            cmd.ExecuteNonQuery();
            //            success = true;
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{
            //    CmtApplicationException cmtEx = new CmtApplicationException();
            //    cmtEx.ExceptionDateTime = DateTime.Now;
            //    cmtEx.ExceptionDesc = ex.Message;
            //    cmtEx.UserDescription = "UpdateTerminalActivity failed";
            //    cmtEx.ExceptionLevel = (short)ExceptionLevelEnum.Critical;
            //    this.LogApplicationException(cmtEx);
            //}

            //return success;
            _log.Debug("LogTerminalActivity:start");
            using (var db = new Database(ConnectionString, DataProvider))
            {
                var spResult = db.Execute("exec [cmtUpdateTerminalActivityLog] @Id, @UserId, @MacAddress, @ActionDate, @TerminalActivityId, @SlaName, @NewSlaName, @AccountingFlag, @Comment, @Prepaid",
                     new
                     {
                         Id = activity.Id
                         ,
                         UserId = activity.UserId
                         ,
                         activity.MacAddress
                         ,
                         activity.ActionDate
                         ,
                         activity.TerminalActivityId
                         ,
                         activity.SlaName
                         ,
                         activity.NewSlaName
                         ,
                         activity.AccountingFlag
                         ,
                         activity.Comment
                         ,
                         activity.Prepaid


                     }
                    );
                _log.Debug("Called cmtUpdateTerminalActivityLog and returned " + spResult);
            }

            _log.Debug("LogTerminalActivity:end");
        }

        /// <summary>
        /// Removes a terminal activity from the TerminalActivityLog table
        /// </summary>
        /// <param name="terminalActivityId">The identifier of the terminal activity</param>
        /// <returns>True if the method succeeds</returns>
        public Boolean DeleteTerminalActivity(int terminalActivityId)
        {
            string deleteCommand = "DELETE FROM TerminalActivityLog WHERE Id = @Id";
            Boolean success = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(deleteCommand, conn))
                    {
                        cmd.Parameters.AddWithValue("@Id", terminalActivityId);
                        cmd.ExecuteNonQuery();
                        success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDateTime = DateTime.Now;
                cmtEx.ExceptionDesc = ex.Message;
                cmtEx.UserDescription = "DeleteTerminalActivity failed";
                cmtEx.ExceptionLevel = (short)ExceptionLevelEnum.Error;
                this.LogApplicationException(cmtEx);
            }

            return success;
        }


        /// <summary>
        /// Returns a TerminalActivity object given by its terminalActivityId
        /// </summary>
        /// <param name="terminalActivityId">The terminal identifier</param>
        /// <returns>A TerminalActivity object or null if not found</returns>
        public TerminalActivity GetTerminalActivityById(int terminalActivityId)
        {
            TerminalActivity terminalActivity = null;
            string queryCmd = "SELECT UserId, MacAddress, ActionDate, TerminalActivityTypeId, SlaName, NewSlaName, AccountingFlag, Comment " +
                              "FROM TerminalActivityLog WHERE Id = @Id";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(queryCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@Id", terminalActivityId);
                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.Read())
                        {
                            terminalActivity = new TerminalActivity();
                            terminalActivity.UserId = reader.GetGuid(0);
                            terminalActivity.MacAddress = reader.GetString(1).Trim();
                            terminalActivity.ActionDate = reader.GetDateTime(2);
                            terminalActivity.TerminalActivityId = reader.GetInt32(3);

                            if (!reader.IsDBNull(4))
                            {
                                terminalActivity.SlaName = reader.GetString(4).Trim();
                            }
                            else
                            {
                                terminalActivity.SlaName = "";
                            }

                            if (!reader.IsDBNull(5))
                            {
                                terminalActivity.NewSlaName = reader.GetString(5).Trim();
                            }
                            else
                            {
                                terminalActivity.NewSlaName = "";
                            }

                            if (!reader.IsDBNull(7))
                            {
                                terminalActivity.Comment = reader.GetString(7).Trim();
                            }
                            else
                            {
                                terminalActivity.Comment = "";
                            }

                            terminalActivity.Id = terminalActivityId;
                            terminalActivity.AccountingFlag = reader.GetBoolean(6);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDateTime = DateTime.Now;
                cmtEx.ExceptionDesc = ex.Message;
                cmtEx.UserDescription = "GetTerminalActivityById failed";
                cmtEx.ExceptionLevel = (short)ExceptionLevelEnum.Error;
                this.LogApplicationException(cmtEx);
            }
            return terminalActivity;
        }

        /// <summary>
        /// Returns a datatable with an account report for a given distributor
        /// </summary>
        /// <param name="startDate">Start date of the report</param>
        /// <param name="endDate">End date of the report</param>
        /// <param name="distid">The distributor Id</param>
        /// <returns>A datatable containing the report rows</returns>
        public DataTable GetTermAccountReportForDistributor(DateTime startDate, DateTime endDate, int distid)
        {
            string accountReportQuery = "SELECT Terminals.DistributorId, Distributors.FullName AS 'Distributor Name', TerminalActivityLog.ActionDate, " +
            "TerminalActivityLog.MacAddress, Terminals.FullName AS 'Terminal Name', Terminals.SitId, " +
            "TerminalActivityTypes.Action, TerminalActivityLog.SlaName, TerminalActivityLog.Comment FROM TerminalActivityLog, TerminalActivityTypes, Terminals, Distributors " +
            "WHERE ActionDate >= @StartDate AND ActionDate <= @EndDate " +
            "AND Terminals.DistributorId = @DistributorId " +
            "AND TerminalActivityLog.TerminalActivityTypeId = TerminalActivityTypes.Id " +
            "AND TerminalActivityTypes.Billable = 1 " +
            "AND TerminalActivityLog.MacAddress = Terminals.MacAddress " +
            "AND Terminals.DistributorId = Distributors.Id " +
            "AND TerminalActivityLog.AccountingFlag = 1 " +
            "ORDER BY Terminals.DistributorId, Terminals.MacAddress, TerminalActivityLog.ActionDate";


            DataTable accountReportData = new DataTable();
            accountReportData.TableName = "AccountReportByDistributor";
            SqlConnection conn = new SqlConnection(ConnectionString);
            SqlDataAdapter adapter = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand(accountReportQuery, conn);

            SqlParameter startDateParam = new SqlParameter();
            startDateParam.ParameterName = "@StartDate";
            startDateParam.SqlDbType = SqlDbType.Date;
            startDateParam.Value = startDate.Date;
            cmd.Parameters.Add(startDateParam);

            SqlParameter endDateParam = new SqlParameter();
            endDateParam.ParameterName = "@EndDate";
            endDateParam.SqlDbType = SqlDbType.Date;
            endDateParam.Value = endDate.Date;
            cmd.Parameters.Add(endDateParam);

            SqlParameter distributorParam = new SqlParameter();
            distributorParam.ParameterName = "@DistributorId";
            distributorParam.SqlDbType = SqlDbType.Int;
            distributorParam.Value = distid;
            cmd.Parameters.Add(distributorParam);

            adapter.SelectCommand = cmd;

            conn.Open();

            try
            {
                adapter.Fill(accountReportData);
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtException = new CmtApplicationException();
                cmtException.ExceptionDesc = ex.Message;
                cmtException.ExceptionDateTime = DateTime.Now;
                cmtException.UserDescription = "GetTermAccountReportForDistributor method failed";
                cmtException.ExceptionLevel = (short)ExceptionLevelEnum.Error;
                this.LogApplicationException(cmtException);
                accountReportData = null;
            }
            finally
            {
                conn.Close();
            }

            return accountReportData;
        }

        #endregion

        /// <summary>
        /// Returns a list of tickets in the CMT database for a given MAC address
        /// </summary>
        /// <param name="macAddress">The MAC address of the terminal the tickets are for</param>
        /// <returns>A list of tickets (CMT format)</returns>
        public List<CMTTicket> GetTicketsByMacAddress(string macAddress)
        {
            List<CMTTicket> tickets = new List<CMTTicket>();
            CMTTicket ticket;

            string listCmd = "SELECT TicketId, CreationDate, MacAddress, SourceAdmState, " +
                "TargetAdmState, TicketStatus, FailureReason, FailureStackTrace, " +
                "TermActivity, newSla, IspId, NewMacAddress, NMSActivate, NewIPAddress, " +
                "NewMask FROM Tickets " +
                "WHERE MacAddress = @MacAddress ORDER BY CreationDate DESC ";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(listCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@MacAddress", macAddress);
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            ticket = new CMTTicket();
                            ticket.TicketId = reader.GetString(reader.GetOrdinal("TicketId"));
                            ticket.CreationDate = reader.GetDateTime(reader.GetOrdinal("CreationDate"));
                            ticket.MacAddress = reader.GetString(reader.GetOrdinal("MacAddress"));
                            ticket.SourceAdmState = reader.GetInt32(reader.GetOrdinal("SourceAdmState"));
                            ticket.TargetAdmState = reader.GetInt32(reader.GetOrdinal("TargetAdmState"));
                            ticket.TicketStatus = reader.GetInt32(reader.GetOrdinal("TicketStatus"));
                            ticket.FailureReason = reader.GetString(reader.GetOrdinal("FailureReason"));
                            ticket.FailureStackTrace = reader.GetString(reader.GetOrdinal("FailureStackTrace"));
                            ticket.TermActivity = reader.GetInt32(reader.GetOrdinal("TermActivity"));
                            if (!reader.IsDBNull(9))
                            {
                                ticket.newSla = reader.GetInt32(reader.GetOrdinal("newSla"));
                            }
                            if (!reader.IsDBNull(10))
                            {
                                ticket.IspId = reader.GetInt32(reader.GetOrdinal("IspId"));
                            }
                            if (!reader.IsDBNull(11))
                            {
                                ticket.NewMacAddress = reader.GetString(reader.GetOrdinal("NewMacAddress"));
                            }
                            //if (!reader.IsDBNull(12))
                            //{
                            //    ticket.NMSActivate = reader.GetBoolean(12);
                            //}

                            tickets.Add(ticket);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtException = new CmtApplicationException();
                cmtException.ExceptionDesc = ex.Message;
                cmtException.ExceptionDateTime = DateTime.Now;
                cmtException.UserDescription = "GetTicketsByMacAddress method failed";
                cmtException.ExceptionLevel = (short)ExceptionLevelEnum.Error;
                this.LogApplicationException(cmtException);
            }

            return tickets;
        }

        /// <summary>
        /// Returns a ticket from the CMT database based on a specific ticket ID
        /// </summary>
        /// <param name="ticketId">The ID of the ticket</param>
        /// <returns>A CMT ticket object</returns>
        public CMTTicket GetTicketById(string ticketId)
        {
            CMTTicket ticket = new CMTTicket();

            string getCmd = "SELECT * FROM Tickets " +
                             "WHERE TicketId = @TicketId ";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(getCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@TicketId", ticketId);
                        SqlDataReader reader = cmd.ExecuteReader();

                        reader.Read();
                        ticket.TicketId = reader.GetString(0);
                        ticket.CreationDate = reader.GetDateTime(1);
                        ticket.MacAddress = reader.GetString(2);
                        ticket.SourceAdmState = reader.GetInt32(3);
                        ticket.TargetAdmState = reader.GetInt32(4);
                        ticket.TicketStatus = reader.GetInt32(5);
                        ticket.FailureReason = reader.GetString(6);
                        ticket.FailureStackTrace = reader.GetString(7);
                        ticket.TermActivity = reader.GetInt32(8);
                        if (!reader.IsDBNull(9))
                        {
                            ticket.newSla = reader.GetInt32(9);
                        }
                        if (!reader.IsDBNull(10))
                        {
                            ticket.IspId = reader.GetInt32(10);
                        }
                        if (!reader.IsDBNull(11))
                        {
                            ticket.NewMacAddress = reader.GetString(11);
                        }
                        if (!reader.IsDBNull(12))
                        {
                            ticket.NMSActivate = reader.GetInt16(12);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtException = new CmtApplicationException();
                cmtException.ExceptionDesc = ex.Message;
                cmtException.ExceptionDateTime = DateTime.Now;
                cmtException.UserDescription = "GetTicketsByMacAddress method failed";
                cmtException.ExceptionLevel = (short)ExceptionLevelEnum.Error;
                this.LogApplicationException(cmtException);
            }

            return ticket;
        }

        /// <summary>
        /// Returns the billable to which a terminal activity type is linked
        /// </summary>
        /// <param name="TerminalActivityTypeId">The ID of the terminal activity type</param>
        /// <returns>The ID of the billable</returns>
        public int GetBillableForTerminalActivityType(int TerminalActivityTypeId)
        {
            int bId = 0;
            string queryCmd = "SELECT BillableId FROM TerminalActivityTypes " +
                              "WHERE Id = @Id";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(queryCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@Id", TerminalActivityTypeId);
                        SqlDataReader reader = cmd.ExecuteReader();
                        reader.Read();
                        if (!reader.IsDBNull(0))
                        {
                            bId = reader.GetInt32(0);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtException = new CmtApplicationException();
                cmtException.ExceptionDesc = ex.Message;
                cmtException.ExceptionDateTime = DateTime.Now;
                cmtException.UserDescription = "GetBillableForTerminalActivityType method failed";
                cmtException.ExceptionLevel = (short)ExceptionLevelEnum.Error;
                this.LogApplicationException(cmtException);
            }

            return bId;
        }

        public List<EventMessage> GetLatestBarixEventMessagesByMac(string macAddress)
        {
            List<EventMessage> eventMessageList = new List<EventMessage>();
            string queryCmd = "SELECT TOP 10 [ResourceId], [AlarmLevel], [GenTimeStamp], [AlarmDescription] " +
                                "FROM [CMTData-Production].[dbo].[EventMessages] " +
                                "WHERE ApplicationId = 'Barix' AND ResourceId = @ResourceId ORDER BY GenTimeStamp DESC";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(queryCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@ResourceId", macAddress);
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            EventMessage message = new EventMessage();
                            message.ResourceId = reader.GetString(0);
                            message.AlarmLevel = (AlarmLevelEnum)reader.GetInt16(1);
                            message.GenTimestamp = reader.GetDateTime(2);
                            message.AlarmDescription = reader.GetString(3);

                            eventMessageList.Add(message);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDesc = ex.Message;
                cmtEx.ExceptionDateTime = DateTime.UtcNow;
                cmtEx.ExceptionStacktrace = ex.StackTrace;
                cmtEx.UserDescription = "Retrieving the EventMessage list failed";
                cmtEx.ExceptionLevel = (short)ExceptionLevelEnum.Error;
                this.LogApplicationException(cmtEx);
            }
            return eventMessageList;
        }

        public List<EventMessage> GetLatestBarixEventMessagesByUser(string userId)
        {
            List<EventMessage> eventMessageList = new List<EventMessage>();
            string queryGetDistributorById = "SELECT TOP 1 du.DistributorId FROM DistributorsUsers AS du WHERE du.UserId = @userId";
            //string queryGetTerminalsByDist = "SELECT t.MacAddress FROM Terminals AS t WHERE t.DistributorId = @distributorId";
            string queryGetTerminalsByDist = "SELECT TOP 10 em.MacAddress, em.FrameDup, em.LastUpdate, em.Url " +
            "FROM MultiCastLog AS em " +
            "WHERE em.MacAddress IN (SELECT DeviceId " +
            "FROM Terminals inner join ConnectedDevices on Terminals.MacAddress = ConnectedDevices.TerminalMacAddress " +
            "where Terminals.DistributorId = @distributorId ) ORDER BY em.LastUpdate DESC ";
            //string queryGetConnectedDevicesFromDist = "SELECT cd.DeviceId FROM ConnectedDevices AS cd WHERE cd.DeviceType = '1'	AND cd.TerminalMacAddress IN ({0})";
            //string queryGetEventMessagesForConnectedDevicesFromDist = "SELECT TOP 10 em.[ResourceId], em.[AlarmLevel], em.[GenTimeStamp], em.[AlarmDescription] " +
            //                       "FROM [CMTData-Production].[dbo].[EventMessages] AS em WHERE em.ApplicationId = 'Barix' AND em.ResourceId IN ({0}) ORDER BY em.GenTimeStamp DESC;";

            int? distId = null;
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();
                 
                    using (SqlCommand cmd = new SqlCommand(queryGetDistributorById, conn))
                    {
                        cmd.Parameters.AddWithValue("@userId", userId);
                        distId = (int)cmd.ExecuteScalar();
                    }
                }



                if (distId.HasValue)
                {
                    using (SqlConnection conn = new SqlConnection(ConnectionString))
                    {
                        conn.Open();
                        using (SqlCommand cmd = new SqlCommand(queryGetTerminalsByDist, conn))
                        {
                            cmd.Parameters.AddWithValue("@distributorId", distId.Value);
                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {

                                while (reader.Read())
                                {
                                    EventMessage message = new EventMessage();
                                    message.ResourceId = reader.GetString(0);
                                    message.AlarmLevel = AlarmLevelEnum.Info;
                                    message.GenTimestamp = reader.GetDateTime(2);
                                    message.AlarmDescription = reader.GetString(3);

                                    eventMessageList.Add(message);
                                }

                            }
                        }
                    }


                }
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDesc = ex.Message;
                cmtEx.ExceptionDateTime = DateTime.UtcNow;
                cmtEx.ExceptionStacktrace = ex.StackTrace;
                cmtEx.UserDescription = "Retrieving the EventMessage list failed";
                cmtEx.ExceptionLevel = (short)ExceptionLevelEnum.Error;
                this.LogApplicationException(cmtEx);
            }
            return eventMessageList;
        }
    }
}
