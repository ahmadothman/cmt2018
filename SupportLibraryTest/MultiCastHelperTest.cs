﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using net.nimera.supportlibrary;

namespace SupportLibraryTest
{
    [TestClass]
    public class MultiCastHelperTest
    {
        [TestMethod]
        public void CreateMacFromIP()
        {
            string macAddress = MultiCastHelper.CreateMacFromIP("228.64.1.1");
            Assert.AreEqual(macAddress, "01:00:5e:40:01:01");
        }
    }
}
