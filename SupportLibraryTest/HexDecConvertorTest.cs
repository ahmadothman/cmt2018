﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using net.nimera.supportlibrary;

namespace SupportLibraryTest
{
    [TestClass]
    public class HexDecConvertorTest
    {
        [TestMethod]
        public void ConvertToIntTest()
        {
           int result = HexDecConvertor.ConvertToInt("FFFFFF");
           Assert.AreEqual(result, 16777215);
        }

        [TestMethod]
        public void ConvertToHexTest()
        {
            string result = HexDecConvertor.ConvertToHex(16777215);
            Assert.AreEqual(result, "FFFFFF");
        }
    }
}
