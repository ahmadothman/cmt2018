﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OleDb;
using System.Data.SqlClient;
using Microsoft.Synchronization.Data;
using Microsoft.Synchronization.Data.SqlServer;

namespace InvoiceServerPrepare
{
    /// <summary>
    /// This application prepares the CMTData database for syncing
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            Program prg = new Program();
            prg.mainBody();
        }

        private void mainBody()
        {
            SqlConnection conn = null;

            try
            {
                using (conn = new SqlConnection("data source=bj8nh0fsve.database.windows.net;Database=CMTData-Invoicing; User Id=evermassen; Password=arne#1200"))
                {
                    // define a new scope named ProductsScope
                    DbSyncScopeDescription scopeDesc = new DbSyncScopeDescription("CMTInvoiceScope");
                    
                    // get the description of the tables from the CMTData database and add to the scope
                    DbSyncTableDescription distributorsTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("Distributors", conn);
                    DbSyncTableDescription invoiceDetailsTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("InvoiceDetails", conn);
                    DbSyncTableDescription invoiceHeadersTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("InvoiceHeaders", conn);
                    
                    
                    //Add tables to the sync scope
                    scopeDesc.Tables.Add(distributorsTableDesc);
                    scopeDesc.Tables.Add(invoiceDetailsTableDesc);
                    scopeDesc.Tables.Add(invoiceHeadersTableDesc);

                    // create a server scope provisioning object based on the ProductScope
                    SqlSyncScopeProvisioning serverProvision = new SqlSyncScopeProvisioning(conn, scopeDesc);

                    // skipping the creation of table since table already exists on server
                    serverProvision.SetCreateTableDefault(DbSyncCreationOption.Skip);

                    // start the provisioning process
                    serverProvision.Apply();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
