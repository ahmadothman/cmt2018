﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using FO_WebRole.BOAccountingControlWSRef;

namespace FO_WebRole
{
    public partial class AddTerminal : System.Web.UI.UserControl
    {
        BOAccountingControlWS _boAccountingControlWS = new BOAccountingControlWS();
        const string _HEXBYTE = "<0|1|2|3|4|5|6|7|8|9|a|b|c|d|e|f><0|1|2|3|4|5|6|7|8|9|a|b|c|d|e|f>";
        Theme _theme = null;

        protected void Page_Load(object sender, EventArgs e)
        {

            RadMaskedTextBoxMacAddress.Mask =
                   _HEXBYTE + "<:>" + _HEXBYTE + "<:>" + _HEXBYTE + "<:>" + _HEXBYTE + "<:>" + _HEXBYTE + "<:>" +
                   _HEXBYTE;
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            //Load theme for distributor
            if (Session["CurrentTheme"] != null)
            {
                _theme = (Theme)Session["CurrentTheme"];
                Page.Theme = _theme.ThemeName;
            }
        }

        protected void RadButtonSubmit_Click(object sender, EventArgs e)
        {
            //Upload the entered Mac Address
            MembershipUser user = Membership.GetUser();
            Guid userKey = (Guid)user.ProviderUserKey;
            String macAddress = RadMaskedTextBoxMacAddress.Text;

            if (macAddress.Equals("00:00:00:00:00:00"))
            {
                LabelStatus.Text = "Mac Address cannot be: 00:00:00:00:00:00";
            }
            else
            {
                if (_boAccountingControlWS.UpdateMacAddressForSystemUser(userKey, macAddress))
                {
                    Response.Redirect("Default.aspx");
                }
            }
        }
    }
}