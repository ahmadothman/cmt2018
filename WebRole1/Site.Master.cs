﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FO_WebRole.BOAccountingControlWSRef;

namespace FO_WebRole
{
    public partial class Site : System.Web.UI.MasterPage
    {
        BOAccountingControlWS _boAccountingControl = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            string bgColor = "";

            //Grab the distributor Id from the request
            if (Request.Params["Key"] != null)
            {
                _boAccountingControl = new BOAccountingControlWS();
                Theme theme = _boAccountingControl.GetThemeForDistributor(Int32.Parse((string)Request.Params["Key"]));
                if (theme != null)
                {
                    if (theme.BackgroundColor.Length == 8)
                    {
                        bgColor = "#" + theme.BackgroundColor.Substring(2, 6);
                    }
                    else
                    {
                        bgColor = "#ffffff";
                    }

                    //Set default background color
                    PageBody.Attributes.Add("bgcolor", bgColor);
                    Session["PageBackgroundColor"] = theme.BackgroundColor;
                }
            }
        }
    }
}