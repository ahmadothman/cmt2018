﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TerminalStatusTab.ascx.cs"
    Inherits="FO_WebRole.TerminalStatusTab" %>
<table border="0" cellspacing="5px" class="termDetails">
    <tr id="RowForwardVolumeAllocation">
        <td>
            <asp:Label ID="LabelTxtForwardVolumeAllocation" runat="server" Text="Forward Volume Allocation:"></asp:Label>
        </td>
        <td>
            <asp:Label ID="LabelVolumeAllocation" runat="server"></asp:Label>
        </td>
    </tr>
    <tr id="RowReturnVolumeAllocation">
        <td>
            <asp:Label ID="LabelTxtReturnVolumeAllocation" runat="server" Text="Return Volume Allocation:"></asp:Label>
        </td>
        <td>
            <asp:Label ID="LabelReturnVolumeAllocation" runat="server"></asp:Label>
        </td>
    </tr>
    <tr id="RowVolumeAllocationSum">
        <td>
            <asp:Label ID="LabelTxtVolumeAllocationSum" runat="server" Text="Volume Allocation:"></asp:Label>
        </td>
        <td>
            <asp:Label ID="LabelVolumeAllocationSum" runat="server"></asp:Label>
        </td>
    </tr>
    <tr id="RowForwardVolumeConsumed">
        <td>
            <asp:Label ID="LabelVolume" runat="server" Text="Forward Volume Consumption:"></asp:Label>
        </td>
        <td>
            <asp:Label ID="LabelVolumeConsumed" runat="server"></asp:Label>
        </td>
    </tr>
    <tr id="RowReturnVolumeConsumed">
        <td>
            <asp:Label ID="LabelReturnVolume" runat="server" Text="Return Volume Consumption:"></asp:Label>
        </td>
        <td>
            <asp:Label ID="LabelReturnVolumeConsumed" runat="server"></asp:Label>
        </td>
    </tr>
    <tr id="RowVolumeConsumedSum">
        <td>
            <asp:Label ID="LabelTxtVolumeConsumedSum" runat="server" Text="Volume Consumption:"></asp:Label>
        </td>
        <td>
            <asp:Label ID="LabelVolumeConsumedSum" runat="server"></asp:Label>
        </td>
    </tr>
    <tr id="RowFUPResetDay">
        <td>
            <asp:Label ID="LabelFUPReset" runat="server">FUP Reset Day:</asp:Label>
        </td>
        <td>
            <asp:Label ID="LabelFUPResetDay" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>Generated on:
        </td>
        <td>
            <asp:Label ID="LabelTimeStamp" runat="server"></asp:Label>
        </td>
    </tr>
</table>
<asp:Panel ID="PanelFreeZone" runat="server" Visible="False" BackColor="#F1FEF3">
    <table border="0" cellspacing="5px" class="termDetails">
        <tr>
            <td width="200px" id="RowForwardFreeZoneConsumed">
                <asp:Label ID="LabelTxtForwardFreeZoneConsumed" runat="server" Text="Forward Freezone Consumed:"></asp:Label>
            </td>
            <td>
                <asp:Label ID="LabelFreezoneConsumed" runat="server"></asp:Label>
            </td>
        </tr>
        <tr id="RowReturnFreeZoneConsumed">
            <td>
                <asp:Label ID="LabelTxtReturnFreeZoneConsumed" runat="server" Text="Return Freezone Consumed:"></asp:Label>
            </td>
            <td>
                <asp:Label ID="LabelRTNFreezoneConsumed" runat="server"></asp:Label>
            </td>
        </tr>
        <tr id="RowFreeZoneConsumedSum">
            <td>
                <asp:Label ID="LabelTxtFreeZoneConsumedSum" runat="server" Text="Freezone Consumed:"></asp:Label>
            </td>
            <td>
                <asp:Label ID="LabelFreeZoneConsumedSum" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Panel>
<div class="chartPane">
    <asp:Chart ID="ChartForwardVolume" runat="server" BackImageAlignment="Center">
        <Series>
        </Series>
        <ChartAreas>
            <asp:ChartArea Name="ChartAreaForward">
            </asp:ChartArea>
        </ChartAreas>
    </asp:Chart>
    <asp:Chart ID="ChartReturnVolume" runat="server" BackImageAlignment="Center">
        <Series>
        </Series>
        <ChartAreas>
            <asp:ChartArea Name="ChartAreaReturn">
            </asp:ChartArea>
        </ChartAreas>
    </asp:Chart>
    <asp:Chart ID="ChartVolumeConsumedSum" runat="server" BackImageAlignment="Center" Visible="False">
        <Series>
        </Series>
        <ChartAreas>
            <asp:ChartArea Name="ChartAreaSum">
            </asp:ChartArea>
        </ChartAreas>
    </asp:Chart>
</div>

