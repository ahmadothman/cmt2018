﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Drawing;
using Telerik.Web.UI;
using FO_WebRole.BOAccountingControlWSRef;
using FO_WebRole.BOMonitorControlWSRef;

namespace FO_WebRole
{
    public partial class ShowTerminalDetails : System.Web.UI.UserControl
    {
        string _macAddress = "";
        const string _HEXBYTE = "<0|1|2|3|4|5|6|7|8|9|a|b|c|d|e|f><0|1|2|3|4|5|6|7|8|9|a|b|c|d|e|f>";
        BOAccountingControlWS _boAccountingControl = new BOAccountingControlWS();
        BOMonitorControlWS _boMonitoringControl = new BOMonitorControlWS();
        int _serviceClass = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            RadMaskedTextBoxMACAddress.Mask =
                   _HEXBYTE + "<:>" + _HEXBYTE + "<:>" + _HEXBYTE + "<:>" + _HEXBYTE + "<:>" + _HEXBYTE + "<:>" +
                   _HEXBYTE;
            if (!IsPostBack)
            {
                //Get the MAC address for this user and show the teminal details
                MembershipUser user = Membership.GetUser();
                Guid userKey = (Guid)user.ProviderUserKey;
                _macAddress = _boAccountingControl.GetMacAddressForSystemUser(userKey);
                //We must fetch the terminal information first  to get to the ISP identifier

                RadMaskedTextBoxMACAddress.Text = _macAddress;

                if (_macAddress != null)
                {
                    //Get to the Service class
                    Terminal term = _boAccountingControl.GetTerminalDetailsByMAC(_macAddress);
                    if (term != null)
                    {
                        try
                        {
                            TerminalInfo ti = _boMonitoringControl.getTerminalInfoByMacAddress(_macAddress, term.IspId);
                            ServiceLevel sla = _boAccountingControl.GetServicePack(Convert.ToInt32(ti.SlaId));
                            if (sla.ServiceClass == 3 || sla.ServiceClass == 1)
                            {
                                RadTabStripTerminalView.Tabs[1].Visible = false;
                            }

                            //Store the MAC address in a session variable
                            Session["MacAddress"] = _macAddress;

                            //Load the tab view
                            RadTab terminalsTab = RadTabStripTerminalView.FindTabByText("Terminal");
                            AddPageView(terminalsTab);
                        }
                        catch
                        {
                            PanelContent.Controls.Clear();
                            PanelContent.Controls.Add(new Label
                            {
                                Text = "Terminal information not available. Please contact support.",
                                ForeColor = Color.Red
                            });
                        }
                    }
                    else
                    {
                        PanelContent.Controls.Clear();
                        PanelContent.Controls.Add(new Label
                        {
                            Text = "Terminal with given MAC address not found.",
                            ForeColor = Color.Red
                        });
                    }
                }
                else
                {
                    PanelContent.Controls.Clear();
                    PanelContent.Controls.Add(new Label
                    {
                        Text = "No MAC address has been found for this user.",
                        ForeColor = Color.Red
                    });
                }
            }
        }

        private void AddPageView(RadTab tab)
        {
            
                RadPageView pageView = new RadPageView();
                pageView.ID = tab.Text;
                RadMultiPageTerminalView.PageViews.Add(pageView);
                pageView.CssClass = "pageView";
                tab.PageViewID = pageView.ID;
        }

        protected void RadTabStripTerminalView_TabClick(object sender, Telerik.Web.UI.RadTabStripEventArgs e)
        {
            AddPageView(e.Tab);
            e.Tab.PageView.Selected = true;
        }

        protected void RadMultiPageTerminalView_PageViewCreated(object sender, Telerik.Web.UI.RadMultiPageEventArgs e)
        {
            Control control = null;

            //Add content to the page view depending on the selected tab
            if (e.PageView.ID.Equals("Terminal"))
            {
                if (_macAddress != null)
                {
                    //Load the TerminalDetailTab control
                    TerminalDetailTab terminalDetailTab =
                        (TerminalDetailTab)Page.LoadControl("TerminalDetailTab.ascx");
                    terminalDetailTab.MacAddress = Session["MacAddress"].ToString();
                    control = terminalDetailTab;
                    control.ID = e.PageView.ID + "_userControl";
                }
            }
            else if (e.PageView.ID.Equals("Status"))
            {
                if (_macAddress != null)
                {
                    //Load the TerminalStatusTab control
                    TerminalStatusTab terminalStatusTab =
                        (TerminalStatusTab)Page.LoadControl("TerminalStatusTab.ascx");
                    terminalStatusTab.MacAddress = Session["MacAddress"].ToString();
                    control = terminalStatusTab;
                    control.ID = e.PageView.ID + "_userControl";
                }
            }
            else if (e.PageView.ID.Equals("Speedtest"))
            {
                if (_macAddress != null)
                {
                    //Load the TerminalNMSSpeedtestTab control
                    TerminalNMSSpeedtestTab terminalNMSSpeedtestTab =
                        (TerminalNMSSpeedtestTab)Page.LoadControl("TerminalNMSSpeedtestTab.ascx");
                    terminalNMSSpeedtestTab.MacAddress = Session["MacAddress"].ToString();
                    control = terminalNMSSpeedtestTab;
                    control.ID = e.PageView.ID + "_userControl";
                }
            }
            else
            {
                control = Page.LoadControl("BlankUserControl.ascx");
                control.ID = e.PageView.ID + "_userControl";
            }

            if (control != null)
            {
                e.PageView.Controls.Add(control);
            }
        }

        protected void RadButtonLogOff_Click(object sender, EventArgs e)
        {
            String distKey = "0";
            if (Session["DistKey"] != null)
            {
                distKey = Session["DistKey"].ToString();
            }
          
            Session.Abandon();
            FormsAuthentication.SignOut();
            Response.Redirect("Default.aspx?Key=" + distKey );
        } 

        protected void RadButtonChangeMac_Click(object sender, EventArgs e)
        {
            //Upload the entered Mac Address
            MembershipUser user = Membership.GetUser();
            Guid userKey = (Guid)user.ProviderUserKey;
            String macAddress = RadMaskedTextBoxMACAddress.Text;
            Terminal term = _boAccountingControl.GetTerminalDetailsByMAC(macAddress);

            if (macAddress.Equals("00:00:00:00:00:00") || term == null)
            {
                //LabelStatus.Text = "Mac Address cannot be: 00:00:00:00:00:00";
                RadMaskedTextBoxMACAddress.ForeColor = Color.Red;
                PanelContent.Controls.Clear();
                PanelContent.Controls.Add(new Label
                {
                    Text = "Terminal information not available. Please contact support.",
                    ForeColor = Color.Red
                });
            }
            else
            {
                if (_boAccountingControl.UpdateMacAddressForSystemUser(userKey, macAddress))
                {
                    Response.Redirect("Default.aspx");
                }
            }
        }

    }
}