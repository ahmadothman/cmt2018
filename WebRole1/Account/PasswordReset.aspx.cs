﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FO_WebRole.BOLogControlWSRef;
using FO_WebRole.BOAccountingControlWSRef;
using System.Web.Security;
using System.Drawing;


namespace FO_WebRole.Account
{
    public partial class PasswordReset : System.Web.UI.Page
    {
        BOAccountingControlWS _boAccountingControl = null;
        Theme _theme = null;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            //Load theme for distributor
            if (Session["CurrentTheme"] != null)
            {
                _theme = (Theme)Session["CurrentTheme"];
                Page.Theme = _theme.ThemeName;
            }
        }

        protected void RadButtonReset_Click(object sender, EventArgs e)
        {
            BOLogControlWS logControl = new BOLogControlWS();
            //Reset the password if the username is existing
            string userName = RadTextBoxUserName.Text;

            //Load the Membership info
            MembershipUserCollection storedUsers = Membership.GetAllUsers();
            MembershipUser storedUser = storedUsers[userName];

            if (storedUser != null)
            {
                try
                {
                    storedUser.IsApproved = true;
                    storedUser.UnlockUser();
                    string newPassword = storedUser.ResetPassword();

                    string bodyText = "Dear customer, <br/><br/> Your password has been reset. " +
                    "For your next logon into the CMT please use the following password: " +
                    "<b><br/><br/>" + newPassword + "</b><br/><br/>" + "Please change your password as soon as possible " +
                    "by means of the Change Password option in the CMT menu.<br/><br/>" +
                    "Best regards,<br/><br/>" +
                    "Your SatADSL support team";

                    string[] toAddress = { storedUser.Email };

                    if (logControl.SendMail(bodyText, "Password change", toAddress))
                    {
                        LabelStatus.Visible = true;
                        LabelStatus.ForeColor = Color.DarkGreen;
                        LabelStatus.Text = "Customer password successfully reset!";
                    }
                    else
                    {
                        LabelStatus.Visible = true;
                        LabelStatus.ForeColor = Color.Red;
                        LabelStatus.Text = "Sorry but I could not E-Mail your new password!";
                    }
                }
                catch (Exception ex)
                {
                    CmtApplicationException cmtEx = new CmtApplicationException();
                    cmtEx.ExceptionDateTime = DateTime.UtcNow;
                    cmtEx.ExceptionDesc = ex.Message;
                    cmtEx.ExceptionLevel = 2; //Warning
                    LabelStatus.Text = "Your account seems to be blocked, please contact your distributor";
                }
            }
            else
            {
                LabelStatus.Visible = true;
                LabelStatus.ForeColor = Color.LightGoldenrodYellow;
                LabelStatus.Text = "This is not a registered user name!";
            }
        }
    }
}