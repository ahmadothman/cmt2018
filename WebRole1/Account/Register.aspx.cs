﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Web.Security;
using FO_WebRole.BOLogControlWSRef;
using FO_WebRole.BOAccountingControlWSRef;

namespace FO_WebRole.Account
{
    public partial class Register : System.Web.UI.Page
    {
        BOLogControlWS _boLogController = null;
        BOAccountingControlWS _boAccountingController = null;
        Theme _theme = null;


        protected void Page_Load(object sender, EventArgs e)
        {
            _boLogController = new BOLogControlWS();
        }

        protected void Page_PreInit(object sender, EventArgs e)
        {
            //Load theme for distributor
            if (Session["CurrentTheme"] != null)
            {
                _theme = (Theme)Session["CurrentTheme"];
                Page.Theme = _theme.ThemeName;
            }
        }

        protected void RadButtonRegister_Click(object sender, EventArgs e)
        {
            string userPwd = RadTextBoxPassword.Text;
            string repeatPwd = RadTextBoxPassword.Text;
            MembershipUser user = null;
            MembershipCreateStatus status;

            if (!userPwd.Equals(repeatPwd))
            {
                LabelStatus.ForeColor = Color.Red;
                LabelStatus.Text = "Password and Password Repeat don't match!";
            }
            else
            {
                user = Membership.CreateUser(RadTextBoxUserName.Text, repeatPwd, RadTextBoxEMail.Text, "void", "void", true, out status);

                if (user == null)
                {
                    LabelStatus.ForeColor = Color.Red;
                    LabelStatus.Text = "Unable to create user: " + status.ToString();
                    CmtApplicationException cmtEx = new CmtApplicationException();
                    cmtEx.ExceptionDateTime = DateTime.Now;
                    cmtEx.ExceptionDesc = "Unable to create system user: " + status.ToString();
                    cmtEx.ExceptionStacktrace = "White label CMT, user registration";
                    cmtEx.ExceptionLevel = 3; //Error
                    _boLogController.LogApplicationException(cmtEx);
                }
                else
                {
                    //Create the user in the database
                    if (Membership.ValidateUser(RadTextBoxUserName.Text, repeatPwd))
                    {
                        FormsAuthentication.RedirectFromLoginPage(RadTextBoxUserName.Text, false);
                    }
                }
            }
        }
    }
}