﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="FO_WebRole.Account.Register" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="main">
                <h3>
                    User Registration:</h3>
                <table>
                    <tr>
                        <td>
                            <asp:Label CssClass="Label" ID="LabelUserName" runat="server" Text="User name:"></asp:Label>
                        </td>
                        <td>
                            <telerik:RadTextBox ID="RadTextBoxUserName" runat="server" Width="200px">
                            </telerik:RadTextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorUserName" runat="server" ErrorMessage="You must specify a user name!" ControlToValidate="RadTextBoxUserName" ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label CssClass="Label" ID="LabelPassword" runat="server" Text="Password:"></asp:Label>
                        </td>
                        <td>
                            <telerik:RadTextBox ID="RadTextBoxPassword" runat="server" TextMode="Password">
                            </telerik:RadTextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="You must specify a password!" ControlToValidate="RadTextBoxPassword" ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label CssClass="Label" ID="LabelRepeatPassword" runat="server" Text="Repeat Password:"></asp:Label>
                        </td>
                        <td>
                            <telerik:RadTextBox ID="RadTextBoxPwdRepeat" runat="server" TextMode="Password">
                            </telerik:RadTextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please repeat your password!" ControlToValidate="RadTextBoxPwdRepeat" ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label CssClass="Label" ID="LabelFirstName" runat="server" Text="First Name"></asp:Label>
                        </td>
                        <td>
                            <telerik:RadTextBox ID="RadTextBoxFName" runat="server" Width="250px">
                            </telerik:RadTextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Please specify your first name!" ControlToValidate="RadTextBoxFName" ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label CssClass="Label" ID="LabelLastName" runat="server" Text="Last Name:"></asp:Label>
                        </td>
                        <td>
                            <telerik:RadTextBox ID="RadTextBoxLName" runat="server" Width="250px">
                            </telerik:RadTextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Please specify your last name!" ControlToValidate="RadTextBoxLName" ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label CssClass="Label" ID="LabelPhone" runat="server" Text="Phone:"></asp:Label>
                        </td>
                        <td>
                            <telerik:RadTextBox ID="RadTextBoxPhone" runat="server" Width="200px">
                            </telerik:RadTextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Please specify your main phone number!" ControlToValidate="RadTextBoxPhone" ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label CssClass="Label" ID="LabelMobilePhone" runat="server" Text="Mobile Phone:"></asp:Label>
                        </td>
                        <td>
                            <telerik:RadTextBox ID="RadTextBoxMobilePhone" runat="server" Width="200px">
                            </telerik:RadTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label CssClass="Label" ID="LabelEMail" runat="server" Text="E-Mail:"></asp:Label>
                        </td>
                        <td>
                            <telerik:RadTextBox ID="RadTextBoxEMail" runat="server" Width="250px">
                            </telerik:RadTextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Please specify your E-Mail address!" ControlToValidate="RadTextBoxEMail" ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <telerik:RadButton ID="RadButtonRegister" runat="server" 
                                Text="Complete registration" onclick="RadButtonRegister_Click">
                            </telerik:RadButton>
                        </td>
                        <td>
                            <asp:Label CssClass="Label" ID="LabelStatus" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
</asp:Content>
