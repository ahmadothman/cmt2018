﻿<%@ Page Title="Log in" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="FO_WebRole.Account.Login" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <article>
        <asp:Label CssClass="Label" ID="LabelArticleContent" runat="server"></asp:Label>
    </article>
    <div class="LoginForm">
        <asp:HyperLink ID="HyperLinkRegistrationMessage" runat="server" Text="User registration"></asp:HyperLink>
        <table>
            <tr>
                <td>
                    <asp:Localize ID="LabelName" runat="server" meta:resourcekey="LabelNameResource" Text="User Name:"></asp:Localize></td>
            </tr>
            <tr>
                <td>
                    <telerik:RadTextBox ID="RadTextBoxUserName" runat="server"></telerik:RadTextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorUserName" runat="server" ErrorMessage="Please provide your username" ControlToValidate="RadTextBoxUserName" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Localize ID="LocalizePassword" runat="server" meta:resourcekey="LabelPasswordResource" Text="Password:"></asp:Localize></td>
            </tr>
            <tr>
                <td>
                    <telerik:RadTextBox ID="RadTextBoxPassword" runat="server" TextMode="Password"></telerik:RadTextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorPassword" runat="server" ErrorMessage="Please provide your password" ControlToValidate="RadTextBoxPassword" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:CheckBox ID="CheckBoxKeepLoggedIn" runat="server" Text="Keep me logged in" />
                </td>
            </tr>
        </table>
        <asp:Button ID="ButtonLogin" runat="server" Text="Login" OnClick="ButtonLogin_Click" />
        <br />
    </div>
    <div class="ErrorMessage">
        <asp:Label ID="LabelLoginFailed" runat="server" Text="Sorry but your user name or password did not match!" Visible="False"></asp:Label><br />
        <asp:HyperLink ID="HyperLinkPasswordForgotten" runat="server" NavigateUrl="~/Account/PasswordReset.aspx" Visible="False" Text="Password forgotten?">
        </asp:HyperLink>
    </div>
</asp:Content>
