﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Web.UI.DataVisualization.Charting;
using System.Globalization;
using System.Web.Security;
using FO_WebRole.BOMonitorControlWSRef;
using FO_WebRole.BOAccountingControlWSRef;
using FO_WebRole.BOLogControlWSRef;
using net.nimera.supportlibrary;

namespace FO_WebRole
{
    public partial class TerminalStatusTab : System.Web.UI.UserControl
    {
        string _macAddress;
        BOAccountingControlWS _boAccountingControl = null;
        BOMonitorControlWS _boMonitoringControl = null;

        /// <summary>
        /// MacAddress which must be set before loading the component!
        /// </summary>
        public string MacAddress
        {
            get { return _macAddress; }
            set { _macAddress = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _boAccountingControl = new FO_WebRole.BOAccountingControlWSRef.BOAccountingControlWS();
            _boMonitoringControl = new FO_WebRole.BOMonitorControlWSRef.BOMonitorControlWS();
            this.initTab(_macAddress);
        }

        /// <summary>
        /// Initializes the tab page
        /// </summary>
        /// <param name="macAddress">Target macAddress</param>
        public void initTab(string macAddress)
        {
            CultureInfo culture = new CultureInfo("en-US");

            //We must fetch the terminal information first  to get to the ISP identifier
            Terminal term = _boAccountingControl.GetTerminalDetailsByMAC(macAddress);
            TerminalInfo ti = _boMonitoringControl.getTerminalInfoByMacAddress(macAddress, term.IspId);
            ServiceLevel sla = _boAccountingControl.GetServicePack(Convert.ToInt32(ti.SlaId));

            if (_boAccountingControl.IsVoucherSLA(Int32.Parse(ti.SlaId)))
            {
                this.showForwardGraphVoucherAbs(ti);
                this.showReturnGraphVoucherAbs(ti);
            }
            else
            {
                //Visualize data depending on the Service Class.
                if (sla.ServiceClass == 1) // SoHo SLA
                {
                    this.showForwardGraph(ti);
                    this.showReturnGraph(ti);
                }
                else if (sla.ServiceClass == 2) // Business SLA
                {
                    this.showVolumeConsumptionGraph(ti);
                }
                else if (sla.ServiceClass == 3) // Corporate SLA
                {
                    this.Parent.Visible = false;
                    //this.showForwardGraph(ti);
                    //this.showReturnGraph(ti);
                }
                else //Default case
                {
                    this.showForwardGraph(ti);
                    this.showReturnGraph(ti);
                }
            }
        }

        /// <summary>
        /// This method displays the volume consumption data in case the forward and return
        /// volumes are combined, as is the case for the Business SLA service class.
        /// </summary>
        /// <param name="ti">The Terminal information object</param>
        public void showVolumeConsumptionGraph(TerminalInfo ti)
        {
            decimal percentConsumed = 0.0m;

            //Hide all non-relevant labels
            LabelTxtForwardVolumeAllocation.Visible = false;
            LabelVolumeAllocation.Visible = false;
            LabelTxtReturnVolumeAllocation.Visible = false;
            LabelReturnVolumeAllocation.Visible = false;
            LabelVolume.Visible = false;
            LabelVolumeConsumed.Visible = false;
            LabelReturnVolume.Visible = false;
            LabelReturnVolumeConsumed.Visible = false;
            LabelTxtForwardFreeZoneConsumed.Visible = false;
            LabelFreezoneConsumed.Visible = false;
            LabelTxtReturnFreeZoneConsumed.Visible = false;
            LabelRTNFreezoneConsumed.Visible = false;
            ChartForwardVolume.Visible = false;
            ChartReturnVolume.Visible = false;

            ServiceLevel sla = _boAccountingControl.GetServicePack(Convert.ToInt32(ti.SlaId));

            //Check if this is a freezone SLA
            bool freeZoneFlag = _boAccountingControl.IsFreeZoneSLA(Int32.Parse(ti.SlaId));
            LabelFUPResetDay.Text = ti.InvoiceDayOfMonth;

            decimal forwardConsumedGB = Convert.ToInt64(ti.Consumed) / (decimal)1000000000.0;
            decimal returnConsumedGB = Convert.ToInt64(ti.ConsumedReturn) / (decimal)1000000000.0;
            decimal totalVolumeConsumedGB = forwardConsumedGB + returnConsumedGB;

            decimal totalVolumeAllocatedGB = (decimal)sla.HighVolumeSUM;
            LabelVolumeAllocationSum.Text = "" + totalVolumeAllocatedGB.ToString("0.000") + " GB";

            if (totalVolumeAllocatedGB > 0)
            {
                percentConsumed = (totalVolumeConsumedGB / totalVolumeAllocatedGB) * (decimal)100.0;
                LabelVolumeConsumedSum.Text = "" + totalVolumeConsumedGB.ToString("0.000") + " GB" + " (" + percentConsumed.ToString("0.00") + " %)";
            }
            else
            {
                LabelVolumeConsumedSum.Text = "" + totalVolumeConsumedGB.ToString("0.000") + " GB";
            }

            LabelTimeStamp.Text = DateTime.Now.ToUniversalTime() + " UTC";

            ChartVolumeConsumedSum.Width = 320;
            ChartVolumeConsumedSum.Titles.Add("Volume Consumption Status");
            ChartVolumeConsumedSum.Titles[0].Font = new Font("Utopia", 16);
            ChartVolumeConsumedSum.ChartAreas[0].BackColor = Color.LightSalmon;
            ChartVolumeConsumedSum.ChartAreas[0].BackSecondaryColor = Color.LightGreen;
            ChartVolumeConsumedSum.ChartAreas[0].BackGradientStyle = System.Web.UI.DataVisualization.Charting.GradientStyle.TopBottom;
            ChartVolumeConsumedSum.ChartAreas[0].Area3DStyle.Enable3D = false;

            ChartVolumeConsumedSum.Series.Add(new Series("VolumeSeries"));

            ChartVolumeConsumedSum.Series["VolumeSeries"].ChartType = SeriesChartType.StackedColumn;
            ChartVolumeConsumedSum.Series["VolumeSeries"].BorderWidth = 3;
            ChartVolumeConsumedSum.Series["VolumeSeries"].ShadowOffset = 2;
            ChartVolumeConsumedSum.Series["VolumeSeries"].XValueType = ChartValueType.Double;

            if (freeZoneFlag)
            {
                PanelFreeZone.Visible = true;
                decimal freeZoneGB = Convert.ToInt64(ti.FreeZone) / (decimal)1000000000.0;
                decimal returnFreeZoneGB = Convert.ToInt64(ti.ConsumedFreeZone) / (decimal)1000000000.0;
                decimal freeZoneSum = freeZoneGB + returnFreeZoneGB;

                LabelFreeZoneConsumedSum.Text = "" + freeZoneSum.ToString("0.000") + " GB";

                ChartVolumeConsumedSum.Series.Add(new Series("FreeZoneSeries"));
                ChartVolumeConsumedSum.Series["FreeZoneSeries"].ChartType = SeriesChartType.StackedColumn;
                ChartVolumeConsumedSum.Series["FreeZoneSeries"].BorderWidth = 3;
                ChartVolumeConsumedSum.Series["FreeZoneSeries"].ShadowOffset = 2;
                ChartVolumeConsumedSum.Series["FreeZoneSeries"].XValueType = ChartValueType.Double;
                ChartVolumeConsumedSum.Series["FreeZoneSeries"].Color = Color.LightGray;

                string[] X2Values = new String[] { "Free Zone", };
                List<decimal> Y2Values = new List<decimal>();
                Y2Values.Add(freeZoneSum);
                ChartVolumeConsumedSum.Series["FreeZoneSeries"].Points.DataBindXY(X2Values, "Volume", Y2Values, "GB");
                ChartVolumeConsumedSum.Series["FreeZoneSeries"].LegendText = "Freezone Volume in GB";
            }
            else
            {
                PanelFreeZone.Visible = false;
            }

            List<decimal> YValues = new List<decimal>();

            Color chartColor = Color.Gray;

            if (percentConsumed <= 80.0m)
            {
                chartColor = Color.Green;
            }
            else if (percentConsumed > 80.0m && percentConsumed < 100.0m)
            {
                chartColor = Color.Orange;
            }
            else
            {
                chartColor = Color.Red;
            }

            ChartVolumeConsumedSum.Series["VolumeSeries"].Color = chartColor;
            YValues.Add(totalVolumeConsumedGB);

            string[] XValues = new String[] { "Total Volume" };
            ChartVolumeConsumedSum.Series["VolumeSeries"].Points.DataBindXY(XValues, "Volume", YValues, "GB");
            ChartVolumeConsumedSum.Series["VolumeSeries"].LegendText = "Consumed Volume in GB";
            ChartVolumeConsumedSum.Series.Add(new Series("FUPThreshold"));
            ChartVolumeConsumedSum.Series["FUPThreshold"].ChartType = SeriesChartType.Line;
            ChartVolumeConsumedSum.Series["FUPThreshold"].BorderWidth = 3;

            if (percentConsumed < 100)
            {
                ChartVolumeConsumedSum.Series["FUPThreshold"].Color = Color.Red;
            }
            else
            {
                ChartVolumeConsumedSum.Series["FUPThreshold"].Color = Color.Wheat;
            }

            if (totalVolumeAllocatedGB != 0.0m)
            {
                ChartVolumeConsumedSum.Series["FUPThreshold"].LegendText = "Volume Allocated in GB";
                ChartVolumeConsumedSum.Series["FUPThreshold"].AxisLabel = "Allocated Volume";
                ChartVolumeConsumedSum.Series["FUPThreshold"].MarkerStyle = MarkerStyle.Diamond;
                ChartVolumeConsumedSum.Series["FUPThreshold"].MarkerSize = 15;
                ChartVolumeConsumedSum.Series["FUPThreshold"].IsValueShownAsLabel = true;
                ChartVolumeConsumedSum.Series["FUPThreshold"].LabelForeColor = Color.Yellow;
                ChartVolumeConsumedSum.Series["FUPThreshold"].LabelBackColor = Color.Black;

                List<decimal> FUPValues = new List<decimal>();
                FUPValues.Add(Decimal.Round(totalVolumeAllocatedGB, 2));
                ChartVolumeConsumedSum.Series["FUPThreshold"].Points.DataBindY(FUPValues);
            }

            ChartVolumeConsumedSum.Legends.Add(new Legend("Volume in GB"));
            ChartVolumeConsumedSum.Visible = true;
        }

        /// <summary>
        /// Shows the consumed forward volume
        /// </summary>
        /// <param name="ti">The soruce TerminalInfo entity</param>
        public void showForwardGraph(TerminalInfo ti)
        {
            //Hide all non-relevant rows
            LabelTxtVolumeAllocationSum.Visible = false;
            LabelVolumeAllocationSum.Visible = false;
            LabelTxtVolumeConsumedSum.Visible = false;
            LabelVolumeConsumedSum.Visible = false;
            LabelTxtFreeZoneConsumedSum.Visible = false;
            LabelFreeZoneConsumedSum.Visible = false;
            ChartVolumeConsumedSum.Visible = false;

            //Check if this is a freezone SLA
            bool freeZoneFlag = _boAccountingControl.IsFreeZoneSLA(Int32.Parse(ti.SlaId));

            LabelFUPResetDay.Text = ti.InvoiceDayOfMonth;

            decimal consumedGB = Convert.ToInt64(ti.Consumed) / (decimal)1000000000.0;

            decimal FUPThresholdBytes = ti.FUPThreshold * (decimal)1000000000.0;
            //FUPThresholdBytes = FUPThresholdBytes + Convert.ToDecimal(ti.Additional);

            Decimal FUPThresholdBytesGB = FUPThresholdBytes / (decimal)1000000000.0;
            if (FUPThresholdBytes == 0.0m)
            {
                LabelVolumeAllocation.Text = "NA";
            }
            else
            {
                LabelVolumeAllocation.Text = FUPThresholdBytesGB.ToString("#.00") + " GB";
            }
            LabelTimeStamp.Text = DateTime.Now.ToUniversalTime() + " UTC";
            double percentConsumed = 0.0;
            Decimal percentConsumedDec = (decimal)0.0;

            if (FUPThresholdBytes != (decimal)0.0)
            {
                percentConsumed = (Convert.ToDouble(ti.Consumed) / Convert.ToDouble(FUPThresholdBytes)) * 100.0;
                percentConsumedDec = new Decimal(percentConsumed);
            }

            ChartForwardVolume.Width = 320;
            ChartForwardVolume.Titles.Add("Forward Volume Status");
            ChartForwardVolume.Titles[0].Font = new Font("Utopia", 16);
            ChartForwardVolume.ChartAreas[0].BackColor = Color.LightSalmon;
            ChartForwardVolume.ChartAreas[0].BackSecondaryColor = Color.LightGreen;
            ChartForwardVolume.ChartAreas[0].BackGradientStyle = System.Web.UI.DataVisualization.Charting.GradientStyle.TopBottom;
            ChartForwardVolume.ChartAreas[0].Area3DStyle.Enable3D = false;

            ChartForwardVolume.Series.Add(new Series("FVolumeSeries"));

            ChartForwardVolume.Series["FVolumeSeries"].ChartType = SeriesChartType.StackedColumn;
            ChartForwardVolume.Series["FVolumeSeries"].BorderWidth = 3;
            ChartForwardVolume.Series["FVolumeSeries"].ShadowOffset = 2;
            ChartForwardVolume.Series["FVolumeSeries"].XValueType = ChartValueType.Double;

            if (freeZoneFlag)
            {
                PanelFreeZone.Visible = true;
                decimal freeZoneGB = Convert.ToInt64(ti.FreeZone) / (decimal)1000000000.0;

                //Take free zone consumption into account
                consumedGB = consumedGB - freeZoneGB;

                //avoid divide by zero
                if (FUPThresholdBytes != (decimal)0.0)
                {
                    percentConsumed = (Convert.ToDouble(consumedGB) / Convert.ToDouble(FUPThresholdBytes)) * 100.0;
                }

                LabelFreezoneConsumed.Text = "" + freeZoneGB.ToString("0.000") + " GB";

                ChartForwardVolume.Series.Add(new Series("FreeZoneSeries"));
                ChartForwardVolume.Series["FreeZoneSeries"].ChartType = SeriesChartType.StackedColumn;
                ChartForwardVolume.Series["FreeZoneSeries"].BorderWidth = 3;
                ChartForwardVolume.Series["FreeZoneSeries"].ShadowOffset = 2;
                ChartForwardVolume.Series["FreeZoneSeries"].XValueType = ChartValueType.Double;
                ChartForwardVolume.Series["FreeZoneSeries"].Color = Color.LightGray;

                string[] X2Values = new String[] { "Free Zone", };
                List<decimal> Y2Values = new List<decimal>();
                Y2Values.Add(freeZoneGB);
                ChartForwardVolume.Series["FreeZoneSeries"].Points.DataBindXY(X2Values, "Volume", Y2Values, "GB");
                ChartForwardVolume.Series["FreeZoneSeries"].LegendText = "Freezone in GB";
            }
            else
            {
                PanelFreeZone.Visible = false;
            }

            List<decimal> YValues = new List<decimal>();

            Color chartColor = Color.Gray;

            if (percentConsumed <= 80.0)
            {
                chartColor = Color.Green;
            }
            else if (percentConsumed > 80.0 && percentConsumed < 100.0)
            {
                chartColor = Color.Orange;
            }
            else
            {
                chartColor = Color.Red;
            }

            ChartForwardVolume.Series["FVolumeSeries"].Color = chartColor;
            YValues.Add(consumedGB);
            LabelVolumeConsumed.Text = "" + consumedGB.ToString("0.000") + " GB";

            string[] XValues = new String[] { "Forward Volume" };
            ChartForwardVolume.Series["FVolumeSeries"].Points.DataBindXY(XValues, "Volume", YValues, "GB");
            ChartForwardVolume.Series["FVolumeSeries"].LegendText = "Volumes in GB";
            ChartForwardVolume.Series.Add(new Series("FUPThreshold"));
            ChartForwardVolume.Series["FUPThreshold"].ChartType = SeriesChartType.Line;
            ChartForwardVolume.Series["FUPThreshold"].BorderWidth = 3;

            if (percentConsumed < 100)
            {
                ChartForwardVolume.Series["FUPThreshold"].Color = Color.Red;
            }
            else
            {
                ChartForwardVolume.Series["FUPThreshold"].Color = Color.Wheat;
            }

            if (FUPThresholdBytes != (decimal)0.0)
            {
                ChartForwardVolume.Series["FUPThreshold"].LegendText = "FUP Threshold in GB";
                ChartForwardVolume.Series["FUPThreshold"].AxisLabel = "FUP Threshold";
                ChartForwardVolume.Series["FUPThreshold"].MarkerStyle = MarkerStyle.Diamond;
                ChartForwardVolume.Series["FUPThreshold"].MarkerSize = 15;
                ChartForwardVolume.Series["FUPThreshold"].IsValueShownAsLabel = true;
                ChartForwardVolume.Series["FUPThreshold"].LabelForeColor = Color.Yellow;
                ChartForwardVolume.Series["FUPThreshold"].LabelBackColor = Color.Black;

                List<decimal> FUPValues = new List<decimal>();
                FUPValues.Add(Decimal.Round(FUPThresholdBytesGB, 2));
                ChartForwardVolume.Series["FUPThreshold"].Points.DataBindY(FUPValues);
            }

            Legend volumeLegend = new Legend("VolLegend");
            ChartForwardVolume.Legends.Add(new Legend("Volume in GB"));
        }

        /// <summary>
        /// Shows the consumed return volume
        /// </summary>
        /// <param name="ti">The source TerminalInfo entity</param>
        public void showReturnGraph(TerminalInfo ti)
        {
            //Hide all non-relevant rows
            LabelTxtVolumeAllocationSum.Visible = false;
            LabelVolumeAllocationSum.Visible = false;
            LabelTxtVolumeConsumedSum.Visible = false;
            LabelVolumeConsumedSum.Visible = false;
            LabelTxtFreeZoneConsumedSum.Visible = false;
            LabelFreeZoneConsumedSum.Visible = false;
            ChartVolumeConsumedSum.Visible = false;

            //Check if this is a freezone SLA
            bool freeZoneFlag = _boAccountingControl.IsFreeZoneSLA(Int32.Parse(ti.SlaId));

            LabelFUPResetDay.Text = ti.InvoiceDayOfMonth;

            decimal consumedGB = Convert.ToInt64(ti.ConsumedReturn) / (decimal)1000000000.0;

            decimal RTNFUPThresholdBytes = ti.RTNFUPThreshold * (decimal)1000000000.0;
            //RTNFUPThresholdBytes = RTNFUPThresholdBytes + Convert.ToDecimal(ti.AdditionalReturn);

            Decimal RTNFUPThresholdBytesGB = RTNFUPThresholdBytes / (decimal)1000000000.0;

            if (RTNFUPThresholdBytes == 0.0m)
            {
                LabelReturnVolumeAllocation.Text = "NA";
            }
            else
            {
                LabelReturnVolumeAllocation.Text = RTNFUPThresholdBytesGB.ToString("#.00") + " GB";
            }

            double percentConsumed = 0.0;

            LabelTimeStamp.Text = DateTime.Now.ToUniversalTime() + " UTC";

            //avoid divide by zero
            if (RTNFUPThresholdBytes != (decimal)0.0)
            {
                percentConsumed = (Convert.ToDouble(ti.ConsumedReturn) / Convert.ToDouble(RTNFUPThresholdBytes)) * 100.0;
                Decimal percentConsumedDec = new Decimal(percentConsumed);
            }

            ChartReturnVolume.Width = 320;
            ChartReturnVolume.Titles.Add("Return Volume Status");
            ChartReturnVolume.Titles[0].Font = new Font("Utopia", 16);
            //ChartReturnVolume.BackSecondaryColor = Color.WhiteSmoke;
            //ChartReturnVolume.BackGradientStyle = System.Web.UI.DataVisualization.Charting.GradientStyle.DiagonalRight;
            ChartReturnVolume.ChartAreas[0].BackColor = Color.LightSalmon;
            ChartReturnVolume.ChartAreas[0].BackSecondaryColor = Color.LightGreen;
            ChartReturnVolume.ChartAreas[0].BackGradientStyle = System.Web.UI.DataVisualization.Charting.GradientStyle.TopBottom;
            ChartReturnVolume.ChartAreas[0].Area3DStyle.Enable3D = false;

            ChartReturnVolume.Series.Add(new Series("RVolumeSeries"));

            ChartReturnVolume.Series["RVolumeSeries"].ChartType = SeriesChartType.StackedColumn;
            ChartReturnVolume.Series["RVolumeSeries"].BorderWidth = 3;
            ChartReturnVolume.Series["RVolumeSeries"].ShadowOffset = 2;
            ChartReturnVolume.Series["RVolumeSeries"].XValueType = ChartValueType.Double;

            if (freeZoneFlag)
            {
                decimal freeZoneGB = Convert.ToInt64(ti.ConsumedFreeZone) / (decimal)1000000000.0;

                //Take free zone consumption into account
                consumedGB = consumedGB - freeZoneGB;

                //avoid divide by zero
                if (RTNFUPThresholdBytes != (decimal)0.0)
                {
                    percentConsumed = (Convert.ToDouble(consumedGB) / Convert.ToDouble(RTNFUPThresholdBytesGB)) * 100.0;
                }

                LabelRTNFreezoneConsumed.Text = "" + freeZoneGB.ToString("0.000") + " GB";

                ChartReturnVolume.Series.Add(new Series("FreeZoneSeries"));
                ChartReturnVolume.Series["FreeZoneSeries"].ChartType = SeriesChartType.StackedColumn;
                ChartReturnVolume.Series["FreeZoneSeries"].BorderWidth = 3;
                ChartReturnVolume.Series["FreeZoneSeries"].ShadowOffset = 2;
                ChartReturnVolume.Series["FreeZoneSeries"].XValueType = ChartValueType.Double;
                ChartReturnVolume.Series["FreeZoneSeries"].Color = Color.LightGray;

                string[] X2Values = new String[] { "Free Zone", };
                List<decimal> Y2Values = new List<decimal>();
                Y2Values.Add(freeZoneGB);
                ChartReturnVolume.Series["FreeZoneSeries"].Points.DataBindXY(X2Values, "Volume", Y2Values, "GB");
                ChartReturnVolume.Series["FreeZoneSeries"].LegendText = "Freezone in GB";
            }

            List<decimal> YValues = new List<decimal>();

            Color chartColor = Color.Gray;

            if (percentConsumed <= 80.0)
            {
                chartColor = Color.Green;
            }
            else if (percentConsumed > 80.0 && percentConsumed < 100.0)
            {
                chartColor = Color.Orange;
            }
            else
            {
                chartColor = Color.Red;
            }

            ChartReturnVolume.Series["RVolumeSeries"].Color = chartColor;
            YValues.Add(consumedGB);
            LabelReturnVolumeConsumed.Text = "" + consumedGB.ToString("0.000") + " GB";

            string[] XValues = new String[] { "Return Volume" };
            ChartReturnVolume.Series["RVolumeSeries"].Points.DataBindXY(XValues, "Volume", YValues, "GB");
            ChartReturnVolume.Series["RVolumeSeries"].LegendText = "Volumes in GB";
            ChartReturnVolume.Series.Add(new Series("FUPThreshold"));
            ChartReturnVolume.Series["FUPThreshold"].ChartType = SeriesChartType.Line;
            ChartReturnVolume.Series["FUPThreshold"].BorderWidth = 3;

            if (percentConsumed < 100)
            {
                ChartReturnVolume.Series["FUPThreshold"].Color = Color.Red;
            }
            else
            {
                ChartReturnVolume.Series["FUPThreshold"].Color = Color.Wheat;
            }

            if (RTNFUPThresholdBytes != (decimal)0.0)
            {
                ChartReturnVolume.Series["FUPThreshold"].LegendText = "FUP Threshold in GB";
                ChartReturnVolume.Series["FUPThreshold"].AxisLabel = "FUP Threshold";
                ChartReturnVolume.Series["FUPThreshold"].MarkerStyle = MarkerStyle.Diamond;
                ChartReturnVolume.Series["FUPThreshold"].MarkerSize = 15;
                ChartReturnVolume.Series["FUPThreshold"].IsValueShownAsLabel = true;
                ChartReturnVolume.Series["FUPThreshold"].LabelForeColor = Color.Yellow;
                ChartReturnVolume.Series["FUPThreshold"].LabelBackColor = Color.Black;

                List<decimal> FUPValues = new List<decimal>();
                //Scaling
                FUPValues.Add(Decimal.Round(RTNFUPThresholdBytesGB, 2));
                ChartReturnVolume.Series["FUPThreshold"].Points.DataBindY(FUPValues);
            }

            Legend volumeLegend = new Legend("VolLegend");
            ChartReturnVolume.Legends.Add(new Legend("Volume in GB"));

        }

        /// <summary>
        /// Shows the consumed forward volume for a voucher SLA
        /// This version uses absolute values instead of %
        /// </summary>
        /// <param name="ti">The soruce TerminalInfo entity</param>
        public void showForwardGraphVoucherAbs(TerminalInfo ti)
        {
            //Hide all non-relevant rows
            LabelTxtVolumeAllocationSum.Visible = false;
            LabelVolumeAllocationSum.Visible = false;
            LabelTxtVolumeConsumedSum.Visible = false;
            LabelVolumeConsumedSum.Visible = false;
            LabelTxtFreeZoneConsumedSum.Visible = false;
            LabelFreeZoneConsumedSum.Visible = false;
            ChartVolumeConsumedSum.Visible = false;

            //Calculate volume allocation
            decimal FUPThresholdBytes = ti.FUPThreshold * (decimal)1000000000.0 + Convert.ToDecimal(ti.Additional);
            //Calculate remaining volume
            decimal remainingGB = ((FUPThresholdBytes - Convert.ToInt64(ti.Consumed) + Convert.ToDecimal(ti.FreeZone)) / (decimal)1000000000.0);

            //Convert numbers
            Decimal FUPThresholdBytesGB = FUPThresholdBytes / (decimal)1000000000.0;
            Decimal percentRemainingDec = (decimal)0.0;

            if (FUPThresholdBytes != (decimal)0.0)
            {
                percentRemainingDec = (remainingGB / FUPThresholdBytesGB) * (decimal)100.0;
            }

            //Create chart
            ChartForwardVolume.Width = 320;
            ChartForwardVolume.Titles.Add("Forward Volume Status");
            ChartForwardVolume.Titles[0].Font = new Font("Utopia", 16);
            ChartForwardVolume.ChartAreas[0].BackColor = Color.LightGreen;
            ChartForwardVolume.ChartAreas[0].BackSecondaryColor = Color.LightSalmon;
            ChartForwardVolume.ChartAreas[0].BackGradientStyle = System.Web.UI.DataVisualization.Charting.GradientStyle.TopBottom;
            ChartForwardVolume.ChartAreas[0].Area3DStyle.Enable3D = false;
            ChartForwardVolume.Series.Add(new Series("FVolumeSeries"));
            ChartForwardVolume.Series["FVolumeSeries"].ChartType = SeriesChartType.StackedColumn;
            ChartForwardVolume.Series["FVolumeSeries"].BorderWidth = 3;
            ChartForwardVolume.Series["FVolumeSeries"].ShadowOffset = 2;
            ChartForwardVolume.Series["FVolumeSeries"].XValueType = ChartValueType.Double;

            //Create freezone area
            if (_boAccountingControl.IsFreeZoneSLA(Int32.Parse(ti.SlaId)))
            {
                PanelFreeZone.Visible = true;
                decimal freeZoneGB = Convert.ToInt64(ti.FreeZone) / (decimal)1000000000.0;
                LabelFreezoneConsumed.Text = "" + freeZoneGB.ToString("0.000") + " GB";
            }
            else
            {
                PanelFreeZone.Visible = false;
            }

            //Plot chart
            List<decimal> YValues = new List<decimal>();

            Color chartColor = Color.Gray;

            if (percentRemainingDec >= (decimal)20.0)
            {
                chartColor = Color.Green;
            }
            else if (percentRemainingDec < (decimal)80.0 && percentRemainingDec > (decimal)0.0)
            {
                chartColor = Color.Orange;
            }
            else
            {
                chartColor = Color.Red;
            }

            ChartForwardVolume.Series["FVolumeSeries"].Color = chartColor;
            YValues.Add(remainingGB);

            string[] XValues = new String[] { "Forward Volume" };
            ChartForwardVolume.Series["FVolumeSeries"].Points.DataBindXY(XValues, "Volume", YValues, "GB");
            ChartForwardVolume.Series["FVolumeSeries"].LegendText = "Remaining volume in GB";

            ChartForwardVolume.ChartAreas[0].AxisY.Minimum = 0;
            ChartForwardVolume.ChartAreas[0].AxisY.Maximum = 10;
            if (remainingGB > 10)
            {
                ChartForwardVolume.ChartAreas[0].AxisY.Maximum = Convert.ToInt32(remainingGB) + 2;
            }

            Legend volumeLegend = new Legend("VolLegend");
            ChartForwardVolume.Legends.Add(new Legend("Remaining volume in GB"));

            //Fill up labels
            LabelVolumeAllocation.Visible = false;
            LabelTxtForwardVolumeAllocation.Visible = false;
            LabelTimeStamp.Text = DateTime.Now.ToUniversalTime() + " UTC";
            LabelVolumeConsumed.Text = "" + remainingGB.ToString("0.00") + " GB";
            LabelVolume.Text = "Forward Volume Remaining:";
            LabelFUPReset.Visible = false;
        }

        /// <summary>
        /// Shows the consumed return volume for a voucher SLA
        /// This version uses absolute values instead of % 
        /// </summary>
        /// <param name="ti">The soruce TerminalInfo entity</param>
        public void showReturnGraphVoucherAbs(TerminalInfo ti)
        {
            //Hide all non-relevant rows
            LabelTxtVolumeAllocationSum.Visible = false;
            LabelVolumeAllocationSum.Visible = false;
            LabelTxtVolumeConsumedSum.Visible = false;
            LabelVolumeConsumedSum.Visible = false;
            LabelTxtFreeZoneConsumedSum.Visible = false;
            LabelFreeZoneConsumedSum.Visible = false;
            ChartVolumeConsumedSum.Visible = false;

            //Calculate volume allocation
            decimal FUPThresholdBytes = ti.RTNFUPThreshold * (decimal)1000000000.0 + Convert.ToDecimal(ti.AdditionalReturn);
            //Calculate remaining volume
            decimal remainingGB = ((FUPThresholdBytes - Convert.ToInt64(ti.ConsumedReturn) + Convert.ToDecimal(ti.ConsumedFreeZone)) / (decimal)1000000000.0);

            //Convert numbers
            Decimal FUPThresholdBytesGB = FUPThresholdBytes / (decimal)1000000000.0;
            Decimal percentRemainingDec = (decimal)0.0;

            if (FUPThresholdBytes != (decimal)0.0)
            {
                percentRemainingDec = (remainingGB / FUPThresholdBytesGB) * (decimal)100.0;
            }

            //Create chart
            ChartReturnVolume.Width = 320;
            ChartReturnVolume.Titles.Add("Return Volume Status");
            ChartReturnVolume.Titles[0].Font = new Font("Utopia", 16);
            ChartReturnVolume.ChartAreas[0].BackColor = Color.LightGreen;
            ChartReturnVolume.ChartAreas[0].BackSecondaryColor = Color.LightSalmon;
            ChartReturnVolume.ChartAreas[0].BackGradientStyle = System.Web.UI.DataVisualization.Charting.GradientStyle.TopBottom;
            ChartReturnVolume.ChartAreas[0].Area3DStyle.Enable3D = false;
            ChartReturnVolume.Series.Add(new Series("RVolumeSeries"));
            ChartReturnVolume.Series["RVolumeSeries"].ChartType = SeriesChartType.StackedColumn;
            ChartReturnVolume.Series["RVolumeSeries"].BorderWidth = 3;
            ChartReturnVolume.Series["RVolumeSeries"].ShadowOffset = 2;
            ChartReturnVolume.Series["RVolumeSeries"].XValueType = ChartValueType.Double;

            //Create freezone area
            if (_boAccountingControl.IsFreeZoneSLA(Int32.Parse(ti.SlaId)))
            {
                decimal freeZoneGB = Convert.ToInt64(ti.ConsumedFreeZone) / (decimal)1000000000.0;
                LabelRTNFreezoneConsumed.Text = "" + freeZoneGB.ToString("0.000") + " GB";
            }

            //Plot chart
            List<decimal> YValues = new List<decimal>();

            Color chartColor = Color.Gray;

            if (percentRemainingDec >= (decimal)20.0)
            {
                chartColor = Color.Green;
            }
            else if (percentRemainingDec < (decimal)80.0 && percentRemainingDec > (decimal)0.0)
            {
                chartColor = Color.Orange;
            }
            else
            {
                chartColor = Color.Red;
            }

            ChartReturnVolume.Series["RVolumeSeries"].Color = chartColor;
            YValues.Add(remainingGB);

            string[] XValues = new String[] { "Return Volume" };
            ChartReturnVolume.Series["RVolumeSeries"].Points.DataBindXY(XValues, "Volume", YValues, "GB");
            ChartReturnVolume.Series["RVolumeSeries"].LegendText = "Remaining volume in GB";

            ChartReturnVolume.ChartAreas[0].AxisY.Minimum = 0;
            ChartReturnVolume.ChartAreas[0].AxisY.Maximum = ChartForwardVolume.ChartAreas[0].AxisY.Maximum;

            Legend volumeLegend = new Legend("VolLegend");
            ChartReturnVolume.Legends.Add(new Legend("Remaining volume in GB"));

            //Fill up labels
            LabelReturnVolumeAllocation.Visible = false;
            LabelTxtReturnVolumeAllocation.Visible = false;
            LabelTimeStamp.Text = DateTime.Now.ToUniversalTime() + " UTC";
            LabelReturnVolumeConsumed.Text = "" + remainingGB.ToString("0.00") + " GB";
            LabelReturnVolume.Text = "Return Volume Remaining:";
        }
    }
}