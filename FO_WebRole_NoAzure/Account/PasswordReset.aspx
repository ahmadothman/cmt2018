﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="True" CodeBehind="PasswordReset.aspx.cs" Inherits="FO_WebRole_NoAzure.Account.PasswordReset" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="ResetFormText">
        <asp:Localize ID="LocalizePasswordReset" runat="server" meta:resourcekey="LabelPasswordResource"
            Text="Forgot your password? Enter your registered username and click on the reset button to reset your password. 
        You will receive an E-Mail, sent to the address you used during the registration process, with a temporary password. 
        Change this password as soon as possible."></asp:Localize><br />
        </div>
    <div class="ResetFormData">
        User Name:
        <telerik:RadTextBox ID="RadTextBoxUserName" runat="server"></telerik:RadTextBox><br />
        <br />
        <telerik:RadButton ID="RadButtonReset" runat="server" Text="Reset" OnClick="RadButtonReset_Click"></telerik:RadButton>
    </div>
    <div>
        <asp:Label CssClass="Label" ID="LabelStatus" runat="server" Text=""></asp:Label>
    </div>
</asp:Content>
