﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using FO_WebRole_NoAzure.BOAccountingControlWSRef;

namespace FO_WebRole_NoAzure.Account
{
    public partial class Login : Page
    {
        BOAccountingControlWS _boAccountingControl = null;
        Theme _theme = null;

        protected void Page_PreInit(object sender, EventArgs e)
        {
            _boAccountingControl = new BOAccountingControlWS();
            //Load theme for distributor
            if (Request.Params["Key"] != null)
            {
                Session.Add("DistKey", Request.Params["Key"]); //Saveguard the distributor key for later
                _theme = _boAccountingControl.GetThemeForDistributor(Int32.Parse(Session["DistKey"].ToString()));
                if (_theme != null && !_theme.ThemeName.Equals("None"))
                {
                    Page.Theme = _theme.ThemeName;

                    //Add theme to session variables
                    Session.Add("CurrentTheme", _theme);
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (_theme != null)
            {
                LabelArticleContent.Text = _theme.IntroText;
            }

            //HyperLinkRegistrationMessage.NavigateUrl = "~/Account/Register.aspx?Key=" + Request.Params["Key"];
            //HyperLinkPasswordForgotten.NavigateUrl = "~/Account/PasswordReset.aspx?Key=" + Request.Params["Key"];

            HyperLinkRegistrationMessage.NavigateUrl = "~/Account/Register.aspx";
            HyperLinkPasswordForgotten.NavigateUrl = "~/Account/PasswordReset.aspx";
        }

        protected void ButtonLogin_Click(object sender, EventArgs e)
        {
            //Validate user
            if (Membership.ValidateUser(RadTextBoxUserName.Text, RadTextBoxPassword.Text))
            {
                FormsAuthentication.RedirectFromLoginPage(RadTextBoxUserName.Text, CheckBoxKeepLoggedIn.Checked);
            }
            else
            {
                LabelLoginFailed.Visible = true;
                HyperLinkPasswordForgotten.Visible = true;
            }
        }
    }
}