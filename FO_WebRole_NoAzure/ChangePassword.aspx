﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ChangePassword.aspx.cs" Inherits="FO_WebRole_NoAzure.ChangePassword" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            height: 25px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:HyperLink ID="HyperLinkReturn" runat="server" Text="Return" NavigateUrl="~/Default.aspx"></asp:HyperLink>
    <asp:ChangePassword ID="ChangeUserPassword" runat="server">
    <ChangePasswordTemplate>
        <table cellpadding="10" cellspacing="5">
            <tr>
                <td>
                    Old Password:
                </td>
                <td>
                    <asp:TextBox ID="CurrentPassword" runat="server" TextMode="Password" />
                </td>
            </tr>
            <tr>
                <td>
                    New Password:
                </td>
                <td>
                    <asp:TextBox ID="NewPassword" runat="server" TextMode="Password" />
                </td>
            </tr>
            <tr>
                <td>
                    Confirmation:
                </td>
                <td>
                    <asp:TextBox ID="ConfirmNewPassword" runat="server" TextMode="Password" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="ChangePasswordPushButton" runat="server" CommandName="ChangePassword" Text="Change Password" />
                </td>
                <td>
                    <asp:Literal ID="FailureText" runat="server" EnableViewState="false" />
                </td>
            </tr>
        </table>
    </ChangePasswordTemplate>
    <SuccessTemplate>
        Your password has been changed!
    </SuccessTemplate>
</asp:ChangePassword>
</asp:Content>
