﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using FO_WebRole_NoAzure.BOAccountingControlWSRef;

namespace FO_WebRole_NoAzure
{
    public partial class Default : System.Web.UI.Page
    {
        BOAccountingControlWS _boAccountingControl = null;
        Theme _theme = null;

        protected void Page_PreInit(object sender, EventArgs e)
        {
            //Load theme for distributor
            if (Session["CurrentTheme"] != null)
            {
                _theme = (Theme)Session["CurrentTheme"];
                Page.Theme = _theme.ThemeName;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _boAccountingControl = new BOAccountingControlWS();

                   
            //Load either the AddTerminal or ShowTerminalDetails user control
            MembershipUser user = Membership.GetUser();
            Guid userKey =  (Guid)user.ProviderUserKey;

            String macAddress = _boAccountingControl.GetMacAddressForSystemUser(userKey);

            if (macAddress != null)
            {
                //Show the terminal details user control
                ShowTerminalDetails showTerminalDetails = 
                        (ShowTerminalDetails)this.LoadControl("ShowTerminalDetails.ascx"); 
                PlaceHolderMainContent.Controls.Add(showTerminalDetails);
                
            }
            else
            {
                //Show the register terminals user control
                AddTerminal addTerminal =
                        (AddTerminal)this.LoadControl("AddTerminal.ascx");
                PlaceHolderMainContent.Controls.Add(addTerminal);
            }
        }
    }
}