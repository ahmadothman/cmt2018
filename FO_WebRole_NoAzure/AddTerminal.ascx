﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="AddTerminal.ascx.cs" Inherits="FO_WebRole_NoAzure.AddTerminal" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<div class="AddTerminal">
    <table>
        <tr>
            <td colspan="2">
                <asp:Localize ID="LocalizeAddTerminalMessage" runat="server"
                    meta:resourcekey="LabelAddTerminalMessage"
                    Text="You do not have a terminal registered yet.<br/>Please enter your MacAddress and push the submit button" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelMacAddress" runat="server" Text="Your Mac Address:"></asp:Label>
            </td>
            <td>
                <telerik:RadMaskedTextBox ID="RadMaskedTextBoxMacAddress" runat="server"
                    ControlToValidate="RadMaskedTextBoxMacAddress">
                </telerik:RadMaskedTextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorMacAddress" runat="server" 
                    ErrorMessage="Please specify your Mac Address" ControlToValidate="RadMaskedTextBoxMacAddress"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <telerik:RadButton ID="RadButtonSubmit" runat="server" Text="Submit" OnClick="RadButtonSubmit_Click"></telerik:RadButton>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Label ID="LabelStatus" runat="server" CssClass="Error"></asp:Label>
            </td>
        </tr>
    </table>
</div>
