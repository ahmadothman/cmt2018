﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="ShowTerminalDetails.ascx.cs" Inherits="FO_WebRole_NoAzure.ShowTerminalDetails" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script type="text/javascript">
    function onTabSelecting(sender, args) {
        if (args.get_tab().get_pageViewID()) {
            args.get_tab().set_postBack(false);
        }
    }
</script>
<div class="ChangeMac">
    <telerik:RadButton ID="RadButtonChangeMac" runat="server" Text="Change MAC Address" OnClick="RadButtonChangeMac_Click"></telerik:RadButton>
    <telerik:RadMaskedTextBox ID="RadMaskedTextBoxMACAddress" runat="server"></telerik:RadMaskedTextBox>
    <telerik:RadButton ID="RadButtonLogOff" runat="server" Text="Logout" OnClick="RadButtonLogOff_Click"></telerik:RadButton>
</div>
<asp:Panel runat="server" ID="PanelContent">
    <div class="TerminalDetailsTab">
        <telerik:RadTabStrip ID="RadTabStripTerminalView" runat="server"
            MultiPageID="RadMultiPageTerminalView"
            SelectedIndex="0" OnClientTabSelecting="onTabSelecting" OnTabClick="RadTabStripTerminalView_TabClick">
            <Tabs>
                <telerik:RadTab runat="server" Text="Terminal" Selected="True">
                </telerik:RadTab>
                <telerik:RadTab  runat="server" Text="Status" Selected="False">
                </telerik:RadTab>
                <telerik:RadTab  runat="server" Text="Speedtest" Selected="False">
                </telerik:RadTab>
            </Tabs>
        </telerik:RadTabStrip>
    </div>
    <telerik:RadMultiPage ID="RadMultiPageTerminalView" runat="server"
        BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="0px" Height="520px"
        Width="650px" ScrollBars="Auto" SelectedIndex="0" OnPageViewCreated="RadMultiPageTerminalView_PageViewCreated">
    </telerik:RadMultiPage>
</asp:Panel>