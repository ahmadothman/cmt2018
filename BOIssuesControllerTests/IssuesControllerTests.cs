﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BOControllerLibrary.Controllers;
using BOControllerLibrary.Model.Issues;
using System.Collections.Generic;
using BOControllerLibrary.Model.Terminal;
using BOControllerLibrary.Model.Service;
using BOControllerLibrary.Model.Organization;


namespace BOIssuesControllerTests
{
    [TestClass]
    public class IssuesControllerTests
    {
        BOIssuesController _boIssuesController = new BOIssuesController("data source=hn9t0oqpt6.database.windows.net; Database=CMTData; User Id=SatFinAfrica; Password=Lagos#1302", "System.Data.SqlClient");
        BOLogController boLogController = new BOLogController("data source=hn9t0oqpt6.database.windows.net; Database=CMTData; User Id=SatFinAfrica; Password=Lagos#1302", "System.Data.SqlClient");
        BOServicesController serviceController = new BOServicesController("data source=hn9t0oqpt6.database.windows.net; Database=CMTData; User Id=SatFinAfrica; Password=Lagos#1302", "System.Data.SqlClient");
        BOAccountingController accountingController = new BOAccountingController("data source=hn9t0oqpt6.database.windows.net; Database=CMTData; User Id=SatFinAfrica; Password=Lagos#1302", "System.Data.SqlClient");

        [TestMethod]
        public void createNewIssue()
        {
            string newIssueKey = "";
            string summary = "test 4";
            string description = "This is another test from the CMT";
            //newIssueKey = _boIssuesController.SubmitNewIssue(summary, description, "Bug", 216);
            Assert.IsNotNull(newIssueKey);
        }

        [TestMethod]
        public void retrieveJiraIssue()
        {
            string issueKey = "SPT-1";
            JiraIssue jiraIssue = new JiraIssue();
            jiraIssue = _boIssuesController.GetJiraIssueDetails(issueKey);

            Assert.AreEqual(issueKey, jiraIssue.key);
            Assert.IsNotNull(jiraIssue.self);
        }

        [TestMethod]
        public void retrieveIssueList()
        {
            List<Issue> issues = new List<Issue>();
            int distributorId = 216;
            int rows = 1;

            issues = _boIssuesController.GetIssuesByDistributor(distributorId);

            Assert.AreEqual(issues.Count, rows);
        }


        [TestMethod]
        public void newComment()
        {
            string key = "CMTSUPPORT-1";
            string comment = "This is a test comment from the CMT";

            bool result = _boIssuesController.SubmitComment(comment, key);

            Assert.IsTrue(result);
        }

    }
}
