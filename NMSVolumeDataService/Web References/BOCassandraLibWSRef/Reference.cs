﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34011
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This source code was auto-generated by Microsoft.VSDesigner, Version 4.0.30319.34011.
// 
#pragma warning disable 1591

namespace NMSVolumeDataService.BOCassandraLibWSRef {
    using System;
    using System.Web.Services;
    using System.Diagnostics;
    using System.Web.Services.Protocols;
    using System.Xml.Serialization;
    using System.ComponentModel;
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.33440")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name="BOCassandraLibWSSoap", Namespace="http://cmt.nimera.net/")]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(VolumeDataEntryBase))]
    public partial class BOCassandraLibWS : System.Web.Services.Protocols.SoapHttpClientProtocol {
        
        private System.Threading.SendOrPostCallback HelloWorldOperationCompleted;
        
        private System.Threading.SendOrPostCallback InsertVolumeDataOperationCompleted;
        
        private bool useDefaultCredentialsSetExplicitly;
        
        /// <remarks/>
        public BOCassandraLibWS() {
            this.Url = global::NMSVolumeDataService.Properties.Settings.Default.NMSVolumeDataService_BOCassandraLibWSRef_BOCassandraLibWS;
            if ((this.IsLocalFileSystemWebService(this.Url) == true)) {
                this.UseDefaultCredentials = true;
                this.useDefaultCredentialsSetExplicitly = false;
            }
            else {
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        public new string Url {
            get {
                return base.Url;
            }
            set {
                if ((((this.IsLocalFileSystemWebService(base.Url) == true) 
                            && (this.useDefaultCredentialsSetExplicitly == false)) 
                            && (this.IsLocalFileSystemWebService(value) == false))) {
                    base.UseDefaultCredentials = false;
                }
                base.Url = value;
            }
        }
        
        public new bool UseDefaultCredentials {
            get {
                return base.UseDefaultCredentials;
            }
            set {
                base.UseDefaultCredentials = value;
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        /// <remarks/>
        public event HelloWorldCompletedEventHandler HelloWorldCompleted;
        
        /// <remarks/>
        public event InsertVolumeDataCompletedEventHandler InsertVolumeDataCompleted;
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://cmt.nimera.net/HelloWorld", RequestNamespace="http://cmt.nimera.net/", ResponseNamespace="http://cmt.nimera.net/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public string HelloWorld() {
            object[] results = this.Invoke("HelloWorld", new object[0]);
            return ((string)(results[0]));
        }
        
        /// <remarks/>
        public void HelloWorldAsync() {
            this.HelloWorldAsync(null);
        }
        
        /// <remarks/>
        public void HelloWorldAsync(object userState) {
            if ((this.HelloWorldOperationCompleted == null)) {
                this.HelloWorldOperationCompleted = new System.Threading.SendOrPostCallback(this.OnHelloWorldOperationCompleted);
            }
            this.InvokeAsync("HelloWorld", new object[0], this.HelloWorldOperationCompleted, userState);
        }
        
        private void OnHelloWorldOperationCompleted(object arg) {
            if ((this.HelloWorldCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.HelloWorldCompleted(this, new HelloWorldCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://cmt.nimera.net/InsertVolumeData", RequestNamespace="http://cmt.nimera.net/", ResponseNamespace="http://cmt.nimera.net/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public void InsertVolumeData(VolumeDataEntry[] entries, string filename) {
            this.Invoke("InsertVolumeData", new object[] {
                        entries,
                        filename});
        }
        
        /// <remarks/>
        public void InsertVolumeDataAsync(VolumeDataEntry[] entries, string filename) {
            this.InsertVolumeDataAsync(entries, filename, null);
        }
        
        /// <remarks/>
        public void InsertVolumeDataAsync(VolumeDataEntry[] entries, string filename, object userState) {
            if ((this.InsertVolumeDataOperationCompleted == null)) {
                this.InsertVolumeDataOperationCompleted = new System.Threading.SendOrPostCallback(this.OnInsertVolumeDataOperationCompleted);
            }
            this.InvokeAsync("InsertVolumeData", new object[] {
                        entries,
                        filename}, this.InsertVolumeDataOperationCompleted, userState);
        }
        
        private void OnInsertVolumeDataOperationCompleted(object arg) {
            if ((this.InsertVolumeDataCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.InsertVolumeDataCompleted(this, new System.ComponentModel.AsyncCompletedEventArgs(invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        public new void CancelAsync(object userState) {
            base.CancelAsync(userState);
        }
        
        private bool IsLocalFileSystemWebService(string url) {
            if (((url == null) 
                        || (url == string.Empty))) {
                return false;
            }
            System.Uri wsUri = new System.Uri(url);
            if (((wsUri.Port >= 1024) 
                        && (string.Compare(wsUri.Host, "localHost", System.StringComparison.OrdinalIgnoreCase) == 0))) {
                return true;
            }
            return false;
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://cmt.nimera.net/")]
    public partial class VolumeDataEntry : VolumeDataEntryBase {
        
        private System.DateTime time_stampField;
        
        /// <remarks/>
        public System.DateTime time_stamp {
            get {
                return this.time_stampField;
            }
            set {
                this.time_stampField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(VolumeDataEntry))]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://cmt.nimera.net/")]
    public partial class VolumeDataEntryBase {
        
        private int ispidField;
        
        private int sitidField;
        
        private long forwardvolumeField;
        
        private long returnvolumeField;
        
        private long accumulatedforwardvolumeField;
        
        private long accumulatedreturnvolumeField;
        
        /// <remarks/>
        public int ispid {
            get {
                return this.ispidField;
            }
            set {
                this.ispidField = value;
            }
        }
        
        /// <remarks/>
        public int sitid {
            get {
                return this.sitidField;
            }
            set {
                this.sitidField = value;
            }
        }
        
        /// <remarks/>
        public long forwardvolume {
            get {
                return this.forwardvolumeField;
            }
            set {
                this.forwardvolumeField = value;
            }
        }
        
        /// <remarks/>
        public long returnvolume {
            get {
                return this.returnvolumeField;
            }
            set {
                this.returnvolumeField = value;
            }
        }
        
        /// <remarks/>
        public long accumulatedforwardvolume {
            get {
                return this.accumulatedforwardvolumeField;
            }
            set {
                this.accumulatedforwardvolumeField = value;
            }
        }
        
        /// <remarks/>
        public long accumulatedreturnvolume {
            get {
                return this.accumulatedreturnvolumeField;
            }
            set {
                this.accumulatedreturnvolumeField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.33440")]
    public delegate void HelloWorldCompletedEventHandler(object sender, HelloWorldCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.33440")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class HelloWorldCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal HelloWorldCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public string Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((string)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.33440")]
    public delegate void InsertVolumeDataCompletedEventHandler(object sender, System.ComponentModel.AsyncCompletedEventArgs e);
}

#pragma warning restore 1591