﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.ServiceProcess;
using System.Threading.Tasks;

namespace NMSVolumeDataService
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : System.Configuration.Install.Installer
    {
        private ServiceProcessInstaller serviceProcessInstaller;
        private ServiceInstaller serviceInstaller;
        
        public ProjectInstaller()
        {
            serviceProcessInstaller = new ServiceProcessInstaller();
            serviceInstaller = new ServiceInstaller();
            // Here you can set properties on serviceProcessInstaller
            //or register event handlers
            serviceProcessInstaller.Account = ServiceAccount.LocalService;
         
            serviceInstaller.ServiceName = "NMSVolumeDataService";
            this.Installers.AddRange(new Installer[] {
                serviceProcessInstaller, serviceInstaller });
        }


        private void serviceInstaller1_AfterInstall(object sender, InstallEventArgs e)
        {
            ServiceController sc = new ServiceController("NMSVolumeDataService Starter");
            sc.Start();

        }
    }
}
