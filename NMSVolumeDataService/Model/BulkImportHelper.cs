﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace NMSVolumeDataService.Model
{
    public class BulkImportHelper
    {
        /// <summary>
        /// Create the structure of the datatable, filling it is your responsibility
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="arr"></param>
        /// <returns></returns>
        public static DataTable CreateDataTable(string tableName, params string[] arr)
        {
            var table = new DataTable(tableName);
            var columns = CreateStringDataColumns(arr);

            if (columns == null)
                throw new ArgumentException("Columns should not be null here");

            table.Columns.AddRange(columns);

            return table;
        }

        public static DataColumn[] CreateStringDataColumns(params string[] arr)
        {
            if (arr == null || arr.Length == 0)
                return null;
            var list = new List<DataColumn>(arr.Length);
            list.AddRange(arr.Select(s => new DataColumn(s, typeof(string))));
            return list.ToArray();
        }

        public static bool WriteToDataBase(string connectionString, bool truncateTarget, bool wrapInTransaction, DataTable table)
        {
            if (table == null)
                throw new ArgumentException("Datatable was null");

            try
            {
                using (var conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlTransaction trans = null;
                    if (wrapInTransaction)
                        trans = conn.BeginTransaction(IsolationLevel.ReadCommitted);

                    if (truncateTarget)
                    {
                        try
                        {
                            var cmd = new SqlCommand
                            {
                                //Transaction = trans, 
                                Connection = conn,
                                CommandText = string.Format("truncate table {0}", table.TableName)
                            };
                            {

                                if (wrapInTransaction)
                                    cmd.Transaction = trans;
                                cmd.ExecuteNonQuery();
                            }


                        }
                        catch (Exception e)
                        {
                            if (wrapInTransaction)
                                trans.Rollback();

                            //Log.Error("Error during save to database - transaction rollbacked", e);
                            return false;
                        }
                    }

                    try
                    {

                        var sbc = new SqlBulkCopy(conn, SqlBulkCopyOptions.Default, trans) { DestinationTableName = table.TableName, BulkCopyTimeout = 240 };
                        sbc.WriteToServer(table);

                        if (wrapInTransaction)
                            trans.Commit();
                        return true;
                    }
                    catch (Exception sbcEx)
                    {
                        if (wrapInTransaction)
                            trans.Rollback();
                        //Log.Error("Error during save to database - bcp. transaction rollbacked", sbcEx);
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                //Log.Error("Error during WriteToDatabase. Did you supply correct connection string ? (One that supports sqlbulkcopy.. that is a MsSqlConnectionString instead of a generic)", ex);
                return false;
            }
        }

    }
}
