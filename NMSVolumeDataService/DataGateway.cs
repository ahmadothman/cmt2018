﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using NMSVolumeDataService.BOLogControlWSRef;
using NMSVolumeDataService.BOAccountingControlWSRef;
using NMSVolumeDataService.Util;

namespace NMSVolumeDataService
{
    /// <summary>
    /// Contains the data support methods
    /// </summary>
    class DataGateway
    {
        string _connectionString = "";
        string _dataPath = "";
        BOLogControlWS _logController = null;
        BOAccountingControlWS _accountingController = null;

        /// <summary>
        /// The default constructor
        /// </summary>
        public DataGateway()
        {
            _connectionString = Properties.Settings.Default.ConnectionString;
            _logController = new BOLogControlWS();
            _accountingController = new BOAccountingControlWS();
        }

        /// <summary>
        /// Processes the Real Time log file
        /// </summary>
        /// <param name="fileName">Log file including full path</param>
        /// <returns>True if the import succeeded, false otherwise</returns>
        public bool ProcessRTFile(string fileName)
        {
            Logger.LogInfo("Processing file: " + fileName, "Realtime NMS Volume Import");
            bool success = false;
            char delimiter = Properties.Settings.Default.CsvInputDelimiter;
            string[] dataFields;

            try
            {
                //Parse the file and insert each line inthe VolumeHistoryDetail2 table
                using (FileStream stream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    using (var reader = new StreamReader(stream, Encoding.UTF8))
                    {
                        //First read header
                        reader.ReadLine();

                        while (!reader.EndOfStream)
                        {
                            try
                            {
                                dataFields = reader.ReadLine().Split(delimiter);
 
                                int ispId = Int32.Parse(dataFields[0]);
                                int sitId = Int32.Parse(dataFields[1]);
                                long returnVolume = Int64.Parse(dataFields[5]);
                                long forwardVolume = Int64.Parse(dataFields[4]);
                                long totalReturnVolume = Int64.Parse(dataFields[7]);
                                long totalForwardVolume = Int64.Parse(dataFields[6]);
                                long freeZoneReturn = Int64.Parse(dataFields[9]);
                                long freeZoneForward = Int64.Parse(dataFields[8]);
                                
                                //Calculations are now handled by NMS server
                                //Volume information take from the realtime updated volume information table
                                //VolumeInformationTime vi =_accountingController.GetLastRTAccumulatedVolumeInformation(sitId, ispId);
                                //if (vi != null)
                                //{
                                //    returnVolume = Int64.Parse(dataFields[7]) - vi.Return;
                                //    forwardVolume = Int64.Parse(dataFields[6]) - vi.Forward;
                                //} 

                                this.insertData(dataFields, "VolumeHistoryDetail2", forwardVolume, returnVolume, totalForwardVolume, totalReturnVolume, freeZoneForward, freeZoneReturn);
                            }
                            catch (SqlException sqlEx)
                            {
                                Logger.log(sqlEx.Message);
                            }
                        }

                        success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
            }

            return success;
        }

        /// <summary>
        /// Processes the Daily log file
        /// </summary>
        /// <param name="fileName">Log file including full path</param>
        /// <returns>True if the import succeeded, false otherwise</returns>
        public bool ProcessDailyFile(string fileName)
        {
            Logger.LogInfo("Processing file: " + fileName, "Daily NMS Volume Import");
            bool success = false;
            char delimiter = Properties.Settings.Default.CsvInputDelimiter;
            string[] dataFields;

            try
            {
                //Parse the file and insert each line inthe VolumeHistoryDetail2 table
                using (FileStream stream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    using (var reader = new StreamReader(stream, Encoding.UTF8))
                    {
                        //First read header
                        reader.ReadLine();

                        while (!reader.EndOfStream)
                        {
                            try
                            {
                                dataFields = reader.ReadLine().Split(delimiter);

                                //Calculate return and forward delta volumes and the return and
                                //forward total volumes. 
                                int ispId = Int32.Parse(dataFields[0]);
                                int sitId = Int32.Parse(dataFields[1]);
                                long returnVolume = 0L;
                                long forwardVolume = 0L;
                                long totalReturnVolume = 0L;
                                long totalForwardVolume = 0L;

                                //Volume information taken from daily updated table
                                VolumeInformationTime vi = _accountingController.GetLastAccumulatedVolumeInformation(sitId, ispId);
                                if (vi != null)
                                {
                                    returnVolume = Int64.Parse(dataFields[7]) - vi.Return;
                                    forwardVolume = Int64.Parse(dataFields[6]) - vi.Forward;
                                }

                                this.insertData(dataFields, "VolumeHistory2", forwardVolume, returnVolume, totalForwardVolume, totalReturnVolume, 0L, 0L);
                            }
                            catch (SqlException sqlEx)
                            {
                                Logger.log(sqlEx.Message);
                            }
                        }

                        success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
            }

            return success;
        }

        /// <summary>
        /// Insert the data fields into the VolumeHistoryDetail2 table
        /// </summary>
        /// <param name="dataFields">String array of data fields</param>
        private void insertData(string[] dataFields, string tableName, long forwardedVolume, long returnVolume, long totalForwardedVolume, long totalReturnVolume, long freeZoneForward, long freeZoneReturn)
        {
            string insertCmd = "INSERT INTO " + tableName + " (IspId, [SIT-ID], time_stamp, " +
                    "ForwardedVolume, ReturnVolume, ForwardedPackets, ReturnPackets, " +
                    "AccumulatedForwardVolume, AccumulatedReturnVolume, HighPriorityForwardedVolume, " +
                    "HighPriorityReturnVolume, HighPriorityForwardedPackets, HighPriorityReturnPackets, " +
                    "RealTimeForwardedVolume, RealTimeReturnVolume, RealTimeForwardedPackets, " +
                    "RealTimeReturnPackets, TotalForwardedVolume, TotalReturnVolume, " + 
                    "TotalForwardedPackets, TotalReturnPackets, FreeZoneFWD, FreeZoneRTN) " +
                    "VALUES (@IspId, @SIT_ID, @time_stamp, @ForwardedVolume, @ReturnVolume, 0, 0, " +
                    "@AccumulatedForwardVolume, @AccumulatedReturnVolume, 0, 0, 0, 0, 0, 0, 0, 0, @TotalForwardedVolume, @TotalReturnVolume, 0, 0, @FreeZoneFWD, @FreeZoneRTN)";

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    
                    using (SqlCommand cmd = new SqlCommand(insertCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@IspId", Int32.Parse(dataFields[0]));
                        cmd.Parameters.AddWithValue("@SIT_ID", Int32.Parse(dataFields[1]));
                        cmd.Parameters.AddWithValue("@time_stamp", dataFields[2]);
                        cmd.Parameters.AddWithValue("@AccumulatedForwardVolume", Int64.Parse(dataFields[6]));
                        cmd.Parameters.AddWithValue("@AccumulatedReturnVolume", Int64.Parse(dataFields[7]));
                        cmd.Parameters.AddWithValue("@ForwardedVolume", forwardedVolume);
                        cmd.Parameters.AddWithValue("@ReturnVolume", returnVolume);
                        cmd.Parameters.AddWithValue("@TotalForwardedVolume", totalForwardedVolume);
                        cmd.Parameters.AddWithValue("@TotalReturnVolume", totalReturnVolume);
                        cmd.Parameters.AddWithValue("@FreeZoneFWD", freeZoneForward);
                        cmd.Parameters.AddWithValue("@FreeZoneRTN", freeZoneReturn);

                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
            }
        }
    }
}
