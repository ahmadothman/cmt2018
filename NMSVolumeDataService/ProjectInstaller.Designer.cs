﻿namespace NMSVolumeDataService
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.NMSVolumeDataServiceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.NMSVolumeDataServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // NMSVolumeDataServiceProcessInstaller
            // 
            this.NMSVolumeDataServiceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.NMSVolumeDataServiceProcessInstaller.Password = null;
            this.NMSVolumeDataServiceProcessInstaller.Username = null;
            // 
            // NMSVolumeDataServiceInstaller
            // 
            this.NMSVolumeDataServiceInstaller.Description = "Imports NMS Volume consumption files";
            this.NMSVolumeDataServiceInstaller.DisplayName = "NMS Volume Data Service";
            this.NMSVolumeDataServiceInstaller.ServiceName = "NMSVolumeDataService";
            this.NMSVolumeDataServiceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            this.NMSVolumeDataServiceInstaller.AfterInstall += new System.Configuration.Install.InstallEventHandler(this.serviceInstaller1_AfterInstall);
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.NMSVolumeDataServiceProcessInstaller,
            this.NMSVolumeDataServiceInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller NMSVolumeDataServiceProcessInstaller;
        private System.ServiceProcess.ServiceInstaller NMSVolumeDataServiceInstaller;
    }
}