﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Web;
using System.Globalization;

namespace NMSVolumeDataService.Util
{
    public class Convertor
    {
        /// <summary>
        /// Converts a MAC address represented in the nn:nn:nn:nn:nn:nn
        /// format to a long.
        /// </summary>
        /// <param name="sMacAddress">Mac address as a string</param>
        /// <returns>Long representation of the MAC address</returns>
        public static long ConvertMacToLong(string sMacAddress)
        {
            string filteredString = "";
            String[] macTokens = sMacAddress.Split(':');

            foreach (string token in macTokens)
            {
                filteredString = filteredString + token;
            }

            return long.Parse(filteredString, NumberStyles.HexNumber);
        }

        /// <summary>
        /// Converts a long value to a MAC address string formatted as 
        /// nn:nn:nn:nn:nn:nn.
        /// </summary>
        /// <param name="iMacAddress">Mac Address as a long</param>
        /// <returns>The mac address as a formatted string</returns>
        public static string ConvertLongToMac(long iMacAddress)
        {
            string hexString = iMacAddress.ToString("X");
            string zeroString = "";
            string sMacAddress = "";

            //Pad with 0's if necessary
            for (int i = 0; i < (12 - hexString.Length); i++)
            {
                zeroString = zeroString + "0";
            }
            hexString = zeroString + hexString;

            for (int j = 1; j <= hexString.Length; j++)
            {
                sMacAddress = sMacAddress + hexString.ToCharArray()[j-1];
                if (j % 2 == 0 && j != 12)
                {
                    sMacAddress = sMacAddress + ":";
                }
            }

            return sMacAddress.ToLower();
        }

        /// <summary>
        /// Simply converts an IP address in a string format into a uint
        /// </summary>
        /// <param name="sIpAddress"></param>
        /// <returns></returns>
        public static uint ConvertIPToLong(string sIpAddress)
        {
            byte[] addressBytes = IPAddress.Parse(sIpAddress).GetAddressBytes();
            byte[] invAddressBytes = new Byte[4];

            invAddressBytes[3] = addressBytes[0];
            invAddressBytes[2] = addressBytes[1];
            invAddressBytes[1] = addressBytes[2];
            invAddressBytes[0] = addressBytes[3];
            
            return BitConverter.ToUInt32(invAddressBytes, 0); 
        }

        /// <summary>
        /// Converts an IP Address given as a long into a string
        /// </summary>
        /// <param name="iIpAddress"></param>
        /// <returns></returns>
        public static string ConvertLongToIP(long iIpAddress)
        {
            return new IPAddress(iIpAddress).ToString(); 

        }
    }
}