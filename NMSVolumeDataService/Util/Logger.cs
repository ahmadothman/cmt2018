﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using NMSVolumeDataService.BOLogControlWSRef;

namespace NMSVolumeDataService.Util
{
    /// <summary>
    /// Simple logger methods
    /// </summary>
    public class Logger
    {
        /// <summary>
        /// Logs some info in the database
        /// </summary>
        /// <param name="msg">The message to log</param>
        /// <param name="method">The method which originated the msg</param>
        public static void LogInfo(String msg, string method)
        {
            BOLogControlWS logger = new BOLogControlWS();
            CmtApplicationException cmtEx = new CmtApplicationException();
            cmtEx.ExceptionDesc = msg;
            cmtEx.ExceptionDateTime = DateTime.Now;
            cmtEx.ExceptionLevel = 1; //Info
            cmtEx.ExceptionStacktrace = method;
            cmtEx.StateInformation = "Info";
            cmtEx.UserDescription = "NMSVolumeDataService info";
            logger.LogApplicationException(cmtEx);
        }

        /// <summary>
        /// Logs and exception
        /// </summary>
        /// <param name="ex">Exception information</param>
        public static void LogError(Exception ex)
        {
            BOLogControlWS logger = new BOLogControlWS();
            CmtApplicationException cmtEx = new CmtApplicationException();
            cmtEx.ExceptionDateTime = DateTime.Now;
            cmtEx.ExceptionDesc = ex.Message;
            cmtEx.ExceptionLevel = 2;
            cmtEx.ExceptionStacktrace = ex.StackTrace;
            cmtEx.StateInformation = "Warning";
            cmtEx.UserDescription = "NMSVolumeDataService, error";
            logger.LogApplicationException(cmtEx);
        }

        /// <summary>
        /// Simple log method
        /// As copied from the TicketMasterService
        /// </summary>
        /// <param name="logMsg">The message to be logged</param>
        public static void log(string logMsg)
        {
            using (StreamWriter writer = new StreamWriter(Properties.Settings.Default.LogFile, true))
            {
                writer.WriteLine("[" + DateTime.Now + "]" + logMsg);
            }
        }
    }
}
