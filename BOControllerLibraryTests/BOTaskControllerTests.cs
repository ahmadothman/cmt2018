﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BOControllerLibrary.Controllers;
using BOControllerLibrary.Controllers.Interfaces;
using BOControllerLibrary.Model.Tasks;
using NUnit.Framework;

namespace BOControllerLibraryTests
{
    public class BOTaskControllerTests
    {
        private IBOTaskController _controller;

        [SetUp]
        public void Setup()
        {
            _controller = new BOTaskController(connectionString: @"data source=cmtdata.nimera.net;Database=CMTData; User Id=SatFinAfrica; Password=Lagos#1302",
                                                     databaseProvider: "System.Data.SqlClient");
        }

        [TearDown]
        public void RemoveSamples()
        {
        }


        /// <summary>
        /// Return a list of completed tasks, in descending order
        /// </summary>
        /// <returns></returns>
        [Test]
        public void GetCompletedTasks()
        {
            var tasks = _controller.GetCompletedTasks();
            Assert.IsNotNull(tasks);
        }

        /// <summary>
        /// Return a list of open tasks, in descending order
        /// </summary>
        /// <returns></returns>
        [Test]
        public void GetOpenTasks()
        {
            var tasks = _controller.GetOpenTasks();
            Assert.IsNotNull(tasks);
        }

        /// <summary>
        /// Insert a task
        /// </summary>
        /// <param name="task"></param>
        /// <returns></returns>
        //[Test]
        public void UpdateTask()
        {
            // first create a tasktype
            Task t = new Task()
                         {
                             Id = 1,
                             Completed = true,
                             DateCompleted = null,
                             DateCreated = DateTime.Now,
                             DateDue = DateTime.Now.AddDays(1),
                             DistributorId = 200,
                             MacAddress = "MACMACMAC",
                             Subject = "Subject",
                             Type = new TaskType() {Id = 1},
                             UserId = new Guid("8E1FA213-8FEA-439B-9CB4-D7BD89092E85")

                         };

            var result = _controller.UpdateTask(t);
            Assert.IsTrue(result);
        }

        /// <summary>
        /// Get a list of all tasktypes
        /// </summary>
        /// <returns></returns>
        [Test]
        public void GetTaskTypes()
        {
            var types = _controller.GetTaskTypes();
            Assert.IsNotNull(types);
        }

        /// <summary>
        /// Insert or update a new tasktype
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        //[Test]
        public void UpdateTaskType()
        {
            TaskType type = new TaskType();
            type.DistInfo = false;
            type.Type = "unit test";

            var result = _controller.UpdateTaskType(type);
            Assert.IsNotNull(result);
        }
    }
}
