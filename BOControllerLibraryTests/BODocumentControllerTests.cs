using System;
using System.Linq;
using System.Data.SqlClient; 
using ArtOfTest.WebAii.TestTemplates;
using BOControllerLibrary.Controllers;
using BOControllerLibrary.Exceptions;
using BOControllerLibrary.Model.Documents;
using NUnit.Framework;

namespace BOControllerLibraryTests
{
    /// <summary>
    /// Summary description for BODocumentControllerTests
    /// </summary>
    [TestFixture]
    public class BODocumentControllerTests : BaseTest
    {
        Document _sampleDocument;
        BODocumentController _boDocumentController;

        #region [Setup / TearDown]

        /// <summary>
        /// Initialization for each test.
        /// </summary>
        [SetUp]
        public void MyTestInitialize()
        {
            #region WebAii Initialization

            // Initializes WebAii manager to be used by the test case.
            // If a WebAii configuration section exists, settings will be
            // loaded from it. Otherwise, will create a default settings
            // object with system defaults.
            //
            // Note: We are passing in a delegate to the NUnit's TestContext.Out.
            // WriteLine() method. This way any logging
            // done from WebAii (i.e. Manager.Log.WriteLine()) is
            // automatically logged to same output as NUnit.
            //
            // If you do not care about unifying the log, then you can simply
            // initialize the test by calling Initialize() with no parameters;
            // that will cause the log location to be picked up from the config
            // file if it exists or will use the default system settings.
            // You can also use Initialize(LogLocation) to set a specific log
            // location for this test.

            // Pass in 'true' to recycle the browser between test methods
            Initialize(false, new TestContextWriteLine(Console.Out.WriteLine));

            // If you need to override any other settings coming from the
            // config section or you don't have a config section, you can
            // comment the 'Initialize' line above and instead use the
            // following:

            /*

            // This will get a new Settings object. If a configuration
            // section exists, then settings from that section will be
            // loaded

            Settings settings = GetSettings();

            // Override the settings you want. For example:
            settings.Web.DefaultBrowser = BrowserType.FireFox;

            // Now call Initialize again with your updated settings object
            Initialize(settings, new TestContextWriteLine(Console.Out.WriteLine));

            */

            #endregion

            //Create a sample document
            _sampleDocument = new Document();
            _sampleDocument.Name = "MyTestDocument.pdf";
            _sampleDocument.Recipient = "everyone";
            _sampleDocument.DateUploaded = DateTime.Now;
            _sampleDocument.Description = "Just a test document";
            _sampleDocument.RecipientFullName = "everyone";
            
            //Instantiate the document controller
            _boDocumentController = 
                new BODocumentController("data source=192.168.1.122;Database=CMTData-Production; User Id=SatFinAfrica; Password=Lagos#1302", 
                    "System.Data.SqlClient");
        }

        /// <summary>
        /// Clean up after each test.
        /// </summary>
        [TearDown]
        public void MyTestCleanUp()
        {
            //
            // Place any additional cleanup here
            //

            #region WebAii CleanUp

            // Shuts down WebAii manager and closes all browsers currently running
            this.CleanUp();

            #endregion
        }

        /// <summary>
        /// Called after all tests in this class are executed.
        /// </summary>
        [TestFixtureTearDown]
        public void FixtureCleanup()
        {
            // This will shut down all browsers if
            // recycleBrowser is turned on. Else
            // will do nothing.
            ShutDown();
        }

        #endregion

        [Test]
        public void AlreadyExistsTest()
        {
            //Create the document record
            try
            {
                long docId = _boDocumentController.CreateNewDocument(_sampleDocument);
                Assert.IsTrue(_boDocumentController.AlreadyExists(_sampleDocument.Name), "Document does not exist");

                //Remove the document again
                _boDocumentController.DeleteDocument(docId);
            }
            catch (DuplicateDocumentException dex)
            {
                Assert.Fail(dex.Message);
            }
            catch (SqlException sqlEx)
            {
                Assert.Fail(sqlEx.Message);
            }
        }


        [Test]
        public void DocumentNameHasChangedTest()
        {
            //Create the document record
            try
            {
                _sampleDocument.Id = _boDocumentController.CreateNewDocument(_sampleDocument);
                
                //Change the name of the document
                _sampleDocument.Name = "Just another name for this document";
                Assert.IsTrue(_boDocumentController.DocumentNameHasChanged(_sampleDocument), "DocumentNameHasChanged did not function");

                //Remove the document again
                _boDocumentController.DeleteDocument(_sampleDocument.Id);
            }
            catch (DuplicateDocumentException dex)
            {
                Assert.Fail(dex.Message);
            }
            catch (SqlException sqlEx)
            {
                Assert.Fail(sqlEx.Message);
            }

        }


        [Test]
        public void UpdateDocumentTest()
        {
            try
            {
                //Create the document
                _sampleDocument.Id = _boDocumentController.CreateNewDocument(_sampleDocument);

                _sampleDocument.Description = "New description";
                Assert.IsTrue(_boDocumentController.UpdateDocument(_sampleDocument), "Update failed");

                _sampleDocument.Name = "Try with another name";

                Assert.Catch(UpdateWithChangedName, "DuplicateDocumentException not thrown");

                _boDocumentController.DeleteDocument(_sampleDocument.Id);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }

        }

        /// <summary>
        /// The TestDelegate
        /// </summary>
        void UpdateWithChangedName()
        {
            _boDocumentController.UpdateDocument(_sampleDocument);
        }
    
    }
}
