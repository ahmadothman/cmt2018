﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using BOControllerLibrary.Controllers;
using BOControllerLibrary.Model;
using BOControllerLibrary.Model.Distributor;
using BOControllerLibrary.Model.ISP;
using BOControllerLibrary.Model.Organization;
using BOControllerLibrary.Model.Terminal;
using BOControllerLibrary.Model.User;
using NUnit.Framework;

namespace BOControllerLibraryTests
{
    [TestFixture]
    internal class BOAccountingControllerTests 
    {
        private IBOAccountingController _controller;
        private const int IspId = 413;

        [SetUp]
        public void Setup()
        {
            _controller = new BOAccountingController(connectionString: @"data source=CMT-STAGING;Database=CMTData-Production; User Id=SatFinAfrica; Password=Lagos#1302",
                                                     databaseProvider: "System.Data.SqlClient");
            //_controller = new BOAccountingController(connectionString: @"data source=.\SQLEXPRESS;Database=CMT; User Id=SatFinAfrica; Password=Lagos#1302",
            //                                         databaseProvider: "System.Data.SqlClient");
        }

        [TearDown]
        public void RemoveSamples ()
        {
            
            
        }

        [Test]
        public void GetTerminalsByDistributor()
        {
            var data = _controller.GetTerminalsByDistributor("4F1EC26E-EE01-42D7-BE0A-012C8E0AB363").FirstOrDefault();
            Assert.IsNotNull(data);
            Assert.IsNotNull(data.Address);
            Assert.IsNotNull(data.LatLong);
            Assert.Greater(data.SitId, 0);
            Assert.Greater(data.IspId, 0);            

        }
        
        [Test]
        public void GetOrganisationsByDistributor()
        {
            var data = _controller.GetOrganisationsByDistributor("4F1EC26E-EE01-42D7-BE0A-012C8E0AB363");
            Assert.IsNotNull(data);

            var first = data.FirstOrDefault();
            Assert.IsNotNull(first);
            Assert.IsNotNull(first.Id);
        }
        
        [Test]
        public void GetUserTerminal()
        {
            var data = _controller.GetUserTerminal(1);
            Assert.IsNotNull(data);
            Assert.IsNotNull(data.Address);
            Assert.IsNotNull(data.LatLong);
            Assert.Greater(data.SitId, 0);
            Assert.Greater(data.IspId, 0);
            Assert.IsNotNull(data.TestMode);
        }
        
        [Test]
        public void GetDistributorForUser()
        {
            var data = _controller.GetDistributorForUser("4F1EC26E-EE01-42D7-BE0A-012C8E0AB363");
            Assert.IsNotNull(data);
            Assert.IsNotNull(data.Id);
        }
        
        [Test]
        public void GetTerminalsByOrganization()
        {
            var data = _controller.GetTerminalsByOrganization(302).FirstOrDefault();
            Assert.IsNotNull(data);
            Assert.IsNotNull(data.Address);
            Assert.IsNotNull(data.LatLong);
            Assert.Greater(data.SitId, 0);
            Assert.Greater(data.IspId, 0);
        }
        
        [Test]
        public void GetTerminalDetailsBySitId()
        {
            var data = _controller.GetTerminalDetailsBySitId(121, IspId);
            Assert.IsNotNull(data);
            Assert.IsNotNull(data.Address);
            Assert.IsNotNull(data.LatLong);
            Assert.Greater(data.SitId, 0);
            Assert.Greater(data.IspId, 0);
        }
        
        [Test]
        public void GetTerminalDetailsByIp()
        {
            var data = _controller.GetTerminalDetailsByIp("41.138.241.5");
            Assert.IsNotNull(data);
            Assert.IsNotNull(data.Address);
            Assert.IsNotNull(data.LatLong);
            Assert.Greater(data.SitId, 0);
            Assert.Greater(data.IspId, 0);
        }
        
        [Test]
        public void GetTerminalDetailsByMac()
        {
            var data = _controller.GetTerminalDetailsByMAC("MACMACMAC");
            Assert.IsNotNull(data);
            Assert.IsNotNull(data.Address);
            Assert.IsNotNull(data.LatLong);
            Assert.Greater(data.SitId, 0);
            Assert.Greater(data.IspId, 0);
            Assert.IsFalse(data.LateBird.Value);
            Assert.IsNotNull(data.RequestDate);
            Assert.IsTrue(data.DistributorAcceptance.Value);
            Assert.IsFalse(data.OrganizationAcceptance.Value);
        }
        
        [Test]
        public void GetDistributor()
        {
            var data = _controller.GetDistributor(175);
            Assert.IsNotNull(data);
            Assert.IsNotNull(data.Id);
        }

        [Test]
        public void GetDistributorById ()
        {
            var data = _controller.GetTerminalsByDistributorId(176);
            Assert.IsNotNull(data);
            Assert.Greater(data.Count, 0);

            var first = data.First();
            Assert.IsNotNullOrEmpty(first.MacAddress);
        }
        
        
        [Test]
        public void GetOrganization()
        {
            var data = _controller.GetOrganization(31);
            Assert.IsNotNull(data);
            Assert.AreEqual(31, data.Id);
        }
        
        [Test]
        public void GetEndUser()
        {
            var data = _controller.GetEndUser("3E378F94-0D92-41D0-A6D4-69294F0C3207");
            Assert.IsNotNull(data);
            Assert.AreEqual(17, data.Id);
        }
        
        /// <summary>
        /// OK
        /// </summary>
        //[Test]
        public void UpdateEndUser()
        {
            var user = new EndUser
                           {
                               Phone = "+32 456 234 1",
                               SitId = 1,
                               Email = "goed@nimera.net",
                               FirstName = "Updated first new name",
                               LastName = "Vanbelle"
                           };

            var result = _controller.UpdateEndUser2(user);
            Assert.Greater(result, 0);
        }
        
        [Test]
        public void UpdateTerminal()
        {
            var controller = new BOAccountingController(connectionString: @"data source=127.0.0.1,60153;Database=CMTData-Production; User Id=SatFinAfrica; Password=Lagos#1302",
                                         databaseProvider: "System.Data.SqlClient");


            var terminal = controller.GetTerminalDetailsByMAC("06:99:ef:dd:42:42");

            var terminal2 = controller.GetTerminalDetailsBySitId(916001, 412);
            terminal.IspId = 112;
            //terminal2.AdmStatus = 6;

            Assert.IsTrue(controller.UpdateTerminal(terminal));
        }

        /// <summary>
        /// OK
        /// </summary>
        //[Test]
        public void UpdateOrganization()
        {
            var organization = new Organization
                                   {
                                       Address = new Address(),
                                       Distributor = new Distributor(),
                                       Email = "updatedmail@nimera.net",
                                       FullName = "test organization"
                                   };
            organization.Address.Country = "Albania";
            Assert.IsTrue(_controller.UpdateOrganization(organization));
        }
        
        /// <summary>
        /// OK
        /// </summary>
        //[Test]
        public void UpdateDistributor()
        {
            var distributor = new Distributor
                                  {
                                      FullName = "unit tested 2",
                                      Vat = "BE 88772920",
                                      Id = 242
                                      //Address = new Address {Country = "Nigeria"}
                                  };
            Assert.IsTrue(_controller.UpdateDistributor(distributor));
        }
        
        /// <summary>
        /// OK
        /// </summary>
        [Test]
        public void UpdateIsp()
        {
            var isp = new Isp
                          {
                              Id = 999,
                              CompanyName = "updated company",
                              ISPType = 1,
                              Address = new Address()
                                            {
                                                AddressLine1 = "address 1 yeah"
                                            }
                          };
            Assert.IsTrue(_controller.UpdateIsp(isp));
        }

        /// <summary>
        /// OK
        /// </summary>
        [Test]
        public void GetISP()
        {
            var isp = _controller.GetISP(112);
            Assert.IsNotNull(isp);
            Assert.AreEqual(112, isp.Id);
            Assert.IsNotNull(isp.Ispif_User);
            Assert.IsNotNull(isp.Ispif_Password);
            Assert.IsNotNull(isp.Dashboard_Password);
            Assert.IsNotNull(isp.Dashboard_User);
            Assert.IsNotNull(isp.Address);
        }

        //Tested and ok
        [Test]
        public void getEdgeISPTest()
        {
            List<Isp> edgeIsps = _controller.GetEdgeISPs();
            Assert.IsNotEmpty(edgeIsps);
        }

        [Test]
        public void IsTerminalOwnedByOrganizationTests ()
        {
            int? test = 12;
            var result = _controller.IsTermOwnedByOrganization(test.Value, "00:06:39:81:f6:f5");
            Assert.IsTrue(result);
        }

        [Test]
        public void IsTerminalOwnedByDistributorUsingTerminalDistributorId ()
        {
            //int? test = 176;
            //var result = _controller.IsTermOwnedByDistributor(test.Value, "MACMACMAC"); // unit test mac
            //Assert.IsTrue(result);


            int? test = 224;
            var result = _controller.IsTermOwnedByDistributor(test.Value, "00:06:39:82:4c:36");
            Assert.IsTrue(result);
        }

        [Test]
        public void GetIspsTests ()
        {
            var resultList = _controller.GetIsps();
            Assert.IsNotNull(resultList);
            Assert.Greater(resultList.Count, 0);

            var first = resultList.First();
            Assert.IsNotNull(first.Address);
            Assert.IsNotNull(first.Id);
        }

        [Test]
        public void GetDistributors ()
        {
            var resultList = _controller.GetDistributors();
            Assert.IsNotNull(resultList);
            Assert.Greater(resultList.Count, 0);

            var first = resultList.First();
            Assert.IsNotNull(first.Id);
            Assert.IsNotNull(first.Address);
        }

        [Test]
        public void GetOrganizations ()
        {
            var resultList = _controller.GetOrganizations();
            Assert.IsNotNull(resultList);
            Assert.Greater(resultList.Count, 0);

            var first = resultList.First();
            Assert.IsNotNull(first.Id);
            Assert.IsNotNull(first.Address);
            Assert.IsNotNull(first.FullName);
        }

        [Test]
        public void GetTerminals ()
        {
            var resultList = _controller.GetTerminals();
            Assert.IsNotNull(resultList);
            Assert.Greater(resultList.Count, 0);

            var first = resultList.First();
            Assert.IsNotNullOrEmpty(first.MacAddress);
            Assert.IsNotNull(first.Address);
            Assert.IsNotNull(first.SitId);
            Assert.IsNotNullOrEmpty(first.SlaName);
            Assert.IsNotNullOrEmpty(first.DistributorName);
            Assert.IsNotNull(first.TestMode);
            
        }

        [Test]
        public void GetTerminalStatus()
        {
            var resultList = _controller.GetTerminalStatus();
            Assert.IsNotNull(resultList);
            Assert.Greater(resultList.Count, 0);

            var first = resultList.First();
            Assert.IsNotNull(first.Id);
            Assert.IsNotNullOrEmpty(first.StatusDesc);
        }

        //[Test]
        public void UpdateTerminalStatus ()
        {
            // Tested manually (insert/update).
            var result = _controller.UpdateTerminalStatus(new TerminalStatus()
                                                              {
                                                                  Id = 8,
                                                                  StatusDesc = "UnittestUpdated"
                                                              });
        }

        //Tested and ok
        [Test]
        public void GetServicePacks()
        {
            var resultList = _controller.GetServicePacks();
            Assert.IsNotNull(resultList);
            Assert.Greater(resultList.Count, 0);

            var first = resultList.First();
            Assert.IsNotNull(first.SlaId);
            Assert.IsNotNull(first.SlaName);
        }

        //Tested and ok
        [Test]
        public void UpdateServicePacks()
        {
            var result = _controller.UpdateServicePack(new ServiceLevel()
                                                           {
                                                               DrAboveFup = "64/16     ",
                                                               FupThreshold = (decimal)0.50,
                                                               SlaCommonName = "UnitTest",
                                                               SlaName = "UnitTest2",
                                                               SlaId    = 99999,
                                                               IspId =  112,
                                                               EdgeISP = 411,
                                                               EdgeSLA = 411411,
                                                               Voucher = false,
                                                               VoIPFlag = false,
                                                               Hide = false,
                                                               RTNFUPThreshold = (decimal)0.50
                                                           });

            Assert.IsTrue(result);
        }

        //Tested and ok
        [Test]
        public void GetServicePackByServicePackIdTest()
        {
            this.UpdateServicePacks();
            ServiceLevel sl = _controller.GetServicePack(99999);
            Assert.IsNotNull(sl);
            Assert.AreEqual(sl.SlaId, 99999);
            Assert.AreEqual(sl.EdgeISP, 411);
            Assert.AreEqual(sl.EdgeSLA, 411411);
        }

        //[Test] // tested and ok
        public void DeleteEndUser ()
        {
            var result = _controller.DeleteEndUser(1);
            Assert.IsTrue(result);
        }

        //[Test] // tested and ok
        public void DeleteDistributor ()
        {
            var result = _controller.DeleteDistributor(241);
            Assert.IsTrue(result);
        }

        //[Test] // tested and working
        public void DeleteOrganization()
        {
            var result = _controller.DeleteOrganizations(31);
            Assert.IsTrue(result);
        }

        //[Test]
        public void DeleteTerminal ()
        {
            var result = _controller.DeleteTerminal("MACMACMAC");
            Assert.IsTrue(result);
        }

        [Test]
        public void GetAccumulatedVolumeInformationTests ()
        {
            var result = _controller.GetAccumulatedVolumeInformation(100100, DateTime.Now.AddDays(-10), DateTime.Now, IspId);
            Assert.IsNotNull(result);
        }


        [Test]
        public void GetRealtimeVolumeInformationTests()
        {
            var result = _controller.GetRealTimeVolumeInformation(100112, DateTime.Now.AddDays(-10), DateTime.Now, IspId);
            Assert.IsNotNull(result);
        }

        [Test]
        public void GetLastRealtimeVolumeInformationTests()
        {
            var result = _controller.GetLastRealTimeVolumeInformation(100112, IspId);
            Assert.IsNotNull(result);
        }

        [Test]
        public void GetLastAccumulatedVolumeInformationTests()
        {
            var result = _controller.GetLastAccumulatedVolumeInformation(100100, IspId);
            Assert.IsNotNull(result);
        }

        [Test]
        public void GetLastRealtimeVolumeInformationFailTests()
        {
            var result = _controller.GetLastRealTimeVolumeInformation(-1, IspId);
            Assert.IsNull(result);
        }

        [Test]
        public void GetOrganizationForUserTests ()
        {
            // test real one
            var result = _controller.GetOrganizationForUser("7E8D072C-68CC-4909-8FC3-6FEBB21FCA54");
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Distributor);
            //Assert.IsNotNull(result.Distributor.Id);

            // test fake one
            Assert.IsNull(_controller.GetOrganizationForUser("0E0D000C-68CC-4909-8FC3-6FEBB21FCA00"));
        }

        //Tested and ok
        [Test]
        public void GetServicePacksByIsp ()
        {
            var result = _controller.GetServicePacksByIsp(413);
            Assert.IsNotNull(result);
            Assert.Greater(result.Count, 0);

            var pack1 = result[0];

            Assert.IsNotNull(pack1);
            Assert.IsNotNull(pack1.IspId);
        }


        [Test]
        public void IsSitIdUnique()
        {
            int sitId = 1; //Does not exist in the database
            Assert.IsTrue(_controller.IsSitIdUnique(sitId, IspId), "IsSitIdUnique returns false!");

        }
    }
}
