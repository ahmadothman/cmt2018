﻿using System;
using System.Configuration;
using System.Linq;
using BOControllerLibrary.Controllers;
using BOControllerLibrary.Controllers.Interfaces;
using BOControllerLibrary.Model;
using BOControllerLibrary.Model.Terminal;
using BOControllerLibrary.Model.User;
using NUnit.Framework;

namespace BOControllerLibraryTests
{
    [TestFixture]
    internal class BOLogControllerTests
    {
        private IBOLogController _controller;

        [SetUp]
        public void Setup()
        {
            _controller = new BOLogController(connectionString: @"data source=cmtdata.nimera.net;Database=CMTData; User Id=SatFinAfrica; Password=Lagos#1302",
                                                     databaseProvider: "System.Data.SqlClient");
        }

        [TearDown]
        public void RemoveSamples()
        {
        }

        [Test]
        public void TestLogApplicationException()
        {
            _controller.LogApplicationException(new CmtApplicationException
                                                    {
                                                        UserDescription = "UnitTestedMessage",
                                                        ExceptionDesc = "Exception Message",
                                                        ExceptionStacktrace = "Exception StackTrace",
                                                        ExceptionDateTime = DateTime.Now,
                                                        StateInformation = "state information here"
                                                    });
        }

        //[Test]
        public void TestLogTerminalActivity()
        {
            _controller.LogTerminalActivity(new TerminalActivity
                                                {
                                                    Action = "UnitTestAction",
                                                    ActionDate = DateTime.Now,
                                                    MacAddress = "MACMACMAC",
                                                    UserId = new Guid("4F1EC26E-EE01-42D7-BE0A-012C8E0AB363")
                                                });
        }


        //[Test]
        public void TestLogUserActivity()
        {
            _controller.LogUserActivity(new UserActivity
                                            {
                                                Action = "UnitTestAction",
                                                ActionDate = DateTime.Now,
                                                UserId = new Guid("4F1EC26E-EE01-42D7-BE0A-012C8E0AB363")
                                            });
        }


        [Test]
        public void TestGetTerminalActivity()
        {
            var list = _controller.GetTerminalActivity("06:66:66:66:66:66");
            Assert.IsNotNull(list);
            Assert.Greater(list.Count, 0);

            var first = list.First();
            Assert.IsNotNullOrEmpty(first.Action);
            Assert.IsNotNull(first.UserId);
            Assert.IsNotNull(first.Id);
            Assert.IsNotNullOrEmpty(first.MacAddress);
        }


        [Test]
        public void TestGetApplicationExceptions()
        {
            var list = _controller.GetApplicationExceptions(DateTime.Now.AddMonths(-1), DateTime.Now.AddDays(1));
            var first = list.First();
            Assert.IsNotNull(list);
            Assert.IsNotNull(first);

            Assert.IsNotNull(first.ExceptionDesc);
            Assert.IsNotNull(first.ExceptionDateTime);
            Assert.IsNotNull(first.Id);
        }


        [Test]
        public void TestGetUserActivities()
        {
            var list = _controller.GetUserActivities("4F1EC26E-EE01-42D7-BE0A-012C8E0AB363");
            Assert.IsNotNull(list);
            Assert.Greater(list.Count, 0);

            var first = list.First();
            Assert.IsNotNullOrEmpty(first.Action);
            Assert.IsNotNull(first.UserId);
            Assert.IsNotNull(first.Id);
        }

        [Test]
        public void TestGetTerminalActivityTypes()
        {
            var list = _controller.GetTerminalActivityTypes();
            Assert.IsNotNull(list);
            Assert.Greater(list.Count, 0);

            var first = list.First();
            Assert.IsNotNull(first.Id);
            Assert.IsNotNull(first.Action);
            Assert.IsNotNull(first.Billable);
        }

        [Test]
        public void TestGetUserActivityTypes()
        {
            var list = _controller.GetUserActivityTypes();
            Assert.IsNotNull(list);
            Assert.Greater(list.Count, 0);

            var first = list.First();
            Assert.IsNotNull(first.Id);
            Assert.IsNotNull(first.Action);
        }
    }
}
