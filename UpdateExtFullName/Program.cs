﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UpdateExtFullName.BOAccountingControlWSRef;

namespace UpdateExtFullName
{
    class Program
    {
        static void Main(string[] args)
        {
            Program prg = new Program();
            prg.mainBody();
        }

        private void mainBody()
        {
            BOAccountingControlWS BOAccountingController = new BOAccountingControlWS();
            Terminal[] terminals = BOAccountingController.GetTerminals();

            try
            {
                foreach (Terminal t in terminals)
                {
                    Console.Out.Write(".");
                    t.ExtFullName = t.FullName;
                    if (!BOAccountingController.UpdateTerminal(t))
                    {
                        Console.Out.WriteLine("Could not update terminal: " + t.FullName);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine(ex.Message);
            }
        }
    }
}
