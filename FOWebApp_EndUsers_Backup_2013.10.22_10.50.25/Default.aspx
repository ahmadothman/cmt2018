﻿<%@ Page Title="" Language="C#" MasterPageFile="~/design/EndUserSite.Master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="FOWebApp_EndUsers.Default" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="ContentBody" ContentPlaceHolderID="BodyContentPlaceHolder" runat="server">
    <asp:PlaceHolder ID="PlaceHolderHeader" runat="server"></asp:PlaceHolder>
    <div id="main-container">
        <div class="wrapper clearfix">
            <div id="path">
                <span>You are here:</span> <a href="http://www.satadsl.net/index.html">Home</a> &gt; <a href="#">My SatADSL</a>
            </div>
             <div id="main">
                <asp:PlaceHolder ID="PlaceHolderBody" runat="server"></asp:PlaceHolder>
            </div>
         </div>
     </div>
     <div id="footer-container">
        <footer class="wrapper clearfix">
            <div id="footerLinks">
				<span id="copyright">&copy;2012, 2013 SatADSL</span>
				<a href="http://www.satadsl.net/Disclaimer.html">Disclaimer</a>
                 <a href="http://www.nimera.net" target="_blank">Created by Nimera Mobile ICT</a>
			</div>
        </footer>
	 </div>
</asp:Content>
