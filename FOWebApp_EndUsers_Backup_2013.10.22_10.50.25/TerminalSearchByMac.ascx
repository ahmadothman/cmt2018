﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TerminalSearchByMac.ascx.cs"
    Inherits="FOWebApp_EndUsers.TerminalSearchByMac" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script type="text/javascript">
    function onTabSelecting(sender, args) {
        if (args.get_tab().get_pageViewID()) {
            args.get_tab().set_postBack(false);
        }
    }

    function ChangePassword(sender, args) {
            //Open the password reset window
            var oWnd = radopen("Account/ChangePasswordEU.aspx", "RadWindowPasswordChange");
            return false;
        }
</script>
<telerik:RadAjaxPanel ID="RadAjaxPanelTerminalSearchByMac" runat="server" Height="550px" Width="650px" LoadingPanelID="RadAjaxLoadingPanel1">
<div id="searchDiv">
    <table cellpadding="5px">
        <tr>
            <td colspan="3">
                <!-- Validations -->
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="RadMaskedTextBoxMacAddress"
                    ErrorMessage="Please enter a valid MAC Address" Display="Dynamic">
                </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelValue" runat="server" Text="Mac Address:"></asp:Label>
            </td>
            <td>
                <telerik:RadMaskedTextBox ID="RadMaskedTextBoxMacAddress" runat="server" Skin="WebBlue">
                
                </telerik:RadMaskedTextBox>
            </td>
            <td>
                <asp:ImageButton ID="ImageButtonSearch" runat="server" CommandName="Search" 
                    ImageAlign="Left" ImageUrl="~/Images/find.png" 
                    ToolTip="Search Terminal Details" onclick="ImageButtonSearch_Click" />
            </td>
            <td>
                <asp:ImageButton ID="ImageButtonChangePassword" runat="server" 
                    CausesValidation="False" ImageUrl="~/Images/key.png" 
                    ToolTip="Change your password" OnClientClick="javascript:return ChangePassword();" />
            </td>
        </tr>
    </table>
</div>
<br />
<!-- Terminal view Rad panel -->
<telerik:RadTabStrip ID="RadTabStripTerminalView" runat="server" 
    MultiPageID="RadMultiPageTerminalView" Skin="WebBlue" 
    SelectedIndex="0" OnClientTabSelecting="onTabSelecting" 
    ontabclick="RadTabStripTerminalView_TabClick">
    <Tabs>
        <telerik:RadTab runat="server" Text="Terminal" Selected="True">
        </telerik:RadTab>
        <telerik:RadTab runat="server" Text="Status">
        </telerik:RadTab>
    </Tabs>
</telerik:RadTabStrip>
<telerik:RadMultiPage ID="RadMultiPageTerminalView" runat="server" 
    BorderColor="#CCCCCC" BorderStyle="Solid" BorderWidth="1px" Height="440px" 
    Width="650px" ScrollBars="Auto" SelectedIndex="0" OnPageViewCreated="RadMultiPageTerminalView_PageViewCreated">
</telerik:RadMultiPage>
</telerik:RadAjaxPanel>
<div style="float: right" id="suNav">
    <img src="Images/techie.gif" alt="Terminal Search" />
</div>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Default" Width="440" Height="850">
</telerik:RadAjaxLoadingPanel>
<telerik:RadWindowManager ID="RadWindowManagerTerminalSearch" runat="server" 
    Skin="WebBlue">
    <Windows>
        <telerik:RadWindow ID="RadWindowPasswordChange" runat="server" Width="700px" Modal="True" Opacity="70" IconUrl="~/Images/key_edit.png">
        </telerik:RadWindow>
    </Windows>
</telerik:RadWindowManager>




