﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Web.Security;
using System.Web.UI.DataVisualization.Charting;
using FOWebApp_EndUsers.net.nimera.cmt.BOMonitorControlWSRef;
using FOWebApp_EndUsers.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp_EndUsers.net.nimera.cmt.BOLogControlWSRef;

namespace FOWebApp_EndUsers
{
    public partial class TerminalAccumulatedTab : System.Web.UI.UserControl
    {
        string _macAddress;

        /// <summary>
        /// MacAddress which must be set before loading the component!
        /// </summary>
        public string MacAddress
        {
            get { return _macAddress; }
            set { _macAddress = value; }
        }


        int _dayOfFUP;

        /// <summary>
        /// Day of FUP reset. This marks the begin date of the chart
        /// </summary>
        public int DayOfFUP
        {
            get { return _dayOfFUP; }
            set { _dayOfFUP = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!MacAddress.Equals(""))
            {
                this.initTab();
            }
        }
        
        /// <summary>
        /// Initializes the tab
        /// </summary>
        public void initTab()
        {
            //Read 24H of RTN volume
            BOMonitorControlWS boMonitorControl =
                new net.nimera.cmt.BOMonitorControlWSRef.BOMonitorControlWS();

            BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();

            try
            {
                //Get the SitId for the given MAC address
                Terminal term = boAccountingControl.GetTerminalDetailsByMAC(_macAddress);

                //Retrieve FUP limit
                TerminalInfo termInfo = boMonitorControl.getTerminalInfoByMacAddress(_macAddress, term.IspId);

                TrafficDataPoint[] trafficDataPoints = boMonitorControl.getAccumulatedViews(_macAddress, term.IspId);
               
                //Load data into chart
                ChartAccumulated.Width = 680;
                ChartAccumulated.Height = 400;
                ChartAccumulated.Titles.Add("1 Month Accumulated Volumes for SitId: " + term.SitId);
                ChartAccumulated.Titles.Add("Date and time created: " + DateTime.UtcNow + " UTC");
                ChartAccumulated.Titles[0].Font = new Font("Utopia", 16);
                ChartAccumulated.BackSecondaryColor = Color.WhiteSmoke;
                ChartAccumulated.BackGradientStyle = System.Web.UI.DataVisualization.Charting.GradientStyle.DiagonalRight;
                ChartAccumulated.ChartAreas[0].BackColor = Color.AliceBlue;
                ChartAccumulated.ChartAreas[0].BackSecondaryColor = Color.GhostWhite;
                ChartAccumulated.ChartAreas[0].BackGradientStyle = System.Web.UI.DataVisualization.Charting.GradientStyle.TopBottom;
                ChartAccumulated.ChartAreas[0].Area3DStyle.Enable3D = false;
                ChartAccumulated.ChartAreas[0].AxisX.Interval = 2;
                ChartAccumulated.ChartAreas[0].AxisX.Title = "Date";
                ChartAccumulated.ChartAreas[0].AxisY.Title = "MB";

                //Add Return Volumes

                ChartAccumulated.Series.Add(new Series("RVolumeSeries"));
                ChartAccumulated.Series["RVolumeSeries"].BorderWidth = 3;
                ChartAccumulated.Series["RVolumeSeries"].ShadowOffset = 2;
                ChartAccumulated.Series["RVolumeSeries"].XValueType = ChartValueType.Int64;
                ChartAccumulated.Series["RVolumeSeries"].Color = Color.Green;


                List<long> RYValues = new List<long>();
                List<string> RXValues = new List<string>();

                //Load data into YValues
                for (int i = 0; i < trafficDataPoints.Length; i++)
                {
                    //Values are shown in MB
                    RYValues.Add(Int64.Parse(trafficDataPoints[i].RtnDataPoint.value)/1000000);

                    DateTime offset = new DateTime(1970, 1, 1, 0, 0, 0);
                    long epochTicks = Int64.Parse(trafficDataPoints[i].RtnDataPoint.timestamp);
                    DateTime dt = offset.AddMilliseconds(epochTicks);

                    string dateTick = "";

                    if (dt.Day < 10)
                        dateTick = dateTick + "0" + dt.Day + "/";
                    else
                        dateTick = dateTick + dt.Day + "/";

                    if (dt.Month < 10)
                        dateTick = dateTick + "0" + dt.Month + "/";
                    else
                        dateTick = dateTick + dt.Month + "/";

                    dateTick = dateTick + dt.Year;

                    RXValues.Add(dateTick);
                }

                ChartAccumulated.Series["RVolumeSeries"].Points.DataBindXY(RXValues, "Time", RYValues, "MB");
                ChartAccumulated.Series["RVolumeSeries"].LegendText = "RT Best Effort";
                ChartAccumulated.Series["RVolumeSeries"].ChartType = SeriesChartType.Line;

                //Add Forward volumes
                ChartAccumulated.Series.Add(new Series("FVolumeSeries"));
                ChartAccumulated.Series["FVolumeSeries"].BorderWidth = 3;
                ChartAccumulated.Series["FVolumeSeries"].ShadowOffset = 2;
                ChartAccumulated.Series["FVolumeSeries"].XValueType = ChartValueType.Int64;
                ChartAccumulated.Series["FVolumeSeries"].Color = Color.Red;

                List<long> FYValues = new List<long>();
                List<string> FXValues = new List<string>();

                //Load data into YValues
                for (int i = 0; i < trafficDataPoints.Length; i++)
                {
                    //Values are shown in MB
                    FYValues.Add(Int64.Parse(trafficDataPoints[i].FwdDataPoint.value) / 1000000);

                    DateTime offset = new DateTime(1970, 1, 1, 0, 0, 0);
                    long epochTicks = Int64.Parse(trafficDataPoints[i].FwdDataPoint.timestamp);
                    DateTime dt = offset.AddMilliseconds(epochTicks);

                    string dateTick = "";

                    if (dt.Day < 10)
                        dateTick = dateTick + "0" + dt.Day + "/";
                    else
                        dateTick = dateTick + dt.Day + "/";

                    if (dt.Month < 10)
                        dateTick = dateTick + "0" + dt.Month + "/";
                    else
                        dateTick = dateTick + dt.Month + "/";

                    dateTick = dateTick + dt.Year;

                    FXValues.Add(dateTick);
                }

                ChartAccumulated.Series["FVolumeSeries"].Points.DataBindXY(FXValues, "Time", FYValues, "MB");
                ChartAccumulated.Series["FVolumeSeries"].LegendText = "FW Best Effort";
                ChartAccumulated.Series["FVolumeSeries"].ChartType = SeriesChartType.Line;

                //Add FUP Threshold
                ChartAccumulated.Series.Add(new Series("FUPThresholdSeries"));
                ChartAccumulated.Series["FUPThresholdSeries"].BorderWidth = 3;
                ChartAccumulated.Series["FUPThresholdSeries"].ShadowOffset = 2;
                ChartAccumulated.Series["FUPThresholdSeries"].XValueType = ChartValueType.Int64;
                ChartAccumulated.Series["FUPThresholdSeries"].Color = Color.BlueViolet;

                List<long> FUPYValues = new List<long>();
                List<string> FUPXValues = new List<string>();

                //Load data into YValues
                for (int i = 0; i < trafficDataPoints.Length; i++)
                {
                    FUPYValues.Add((long)termInfo.FUPThreshold * 1000);

                    DateTime offset = new DateTime(1970, 1, 1, 0, 0, 0);
                    long epochTicks = Int64.Parse(trafficDataPoints[i].FwdDataPoint.timestamp);
                    DateTime dt = offset.AddMilliseconds(epochTicks);

                    string dateTick = "";

                    if (dt.Day < 10)
                        dateTick = dateTick + "0" + dt.Day + "/";
                    else
                        dateTick = dateTick + dt.Day + "/";

                    if (dt.Month < 10)
                        dateTick = dateTick + "0" + dt.Month + "/";
                    else
                        dateTick = dateTick + dt.Month + "/";

                    dateTick = dateTick + dt.Year;

                    FUPXValues.Add(dateTick);
                }

                ChartAccumulated.Series["FUPThresholdSeries"].Points.DataBindXY(FUPXValues, "Time", FUPYValues, "MB");
                ChartAccumulated.Series["FUPThresholdSeries"].LegendText = "FUP Threshold";
                ChartAccumulated.Series["FUPThresholdSeries"].ChartType = SeriesChartType.Line;


                ChartAccumulated.Legends.Add(new Legend("Volume in MB"));
            }
            catch (Exception ex)
            {
                BOLogControlWS boLogControlWS = new BOLogControlWS();
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDateTime = DateTime.Now;
                cmtEx.ExceptionDesc = ex.Message;
                cmtEx.UserDescription = "Method: TerminalAccumulatedTab.PageLoad";
                cmtEx.StateInformation = "No data points avaialble";
                boLogControlWS.LogApplicationException(cmtEx);
            }
        }
    }
}