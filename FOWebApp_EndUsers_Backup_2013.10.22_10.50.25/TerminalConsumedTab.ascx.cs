﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Web.Security;
using System.Web.UI.DataVisualization.Charting;
using System.IO;
using FOWebApp_EndUsers.net.nimera.cmt.BOMonitorControlWSRef;
using FOWebApp_EndUsers.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp_EndUsers.net.nimera.cmt.BOLogControlWSRef;
using net.nimera.supportlibrary;

namespace FOWebApp_EndUsers
{
    public partial class TerminalConsumedTab : System.Web.UI.UserControl
    {
        string _macAddress;

        /// <summary>
        /// MacAddress which must be set before loading the component!
        /// </summary>
        public string MacAddress
        {
            get { return _macAddress; }
            set { _macAddress = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!MacAddress.Equals(""))
            {
                this.initTab();
            }
        }

        /// <summary>
        /// Initialize the tab
        /// </summary>
        public void initTab()
        {
            //Read 7 days of RTN volume
            BOMonitorControlWS boMonitorControl =
                new net.nimera.cmt.BOMonitorControlWSRef.BOMonitorControlWS();

            BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();

            try
            {
                //Get the SitId for the given MAC address
                Terminal term = boAccountingControl.GetTerminalDetailsByMAC(_macAddress);

                DateTime startDateTime = DateTime.Now.AddDays(-7.0);
                DateTime endDateTime = DateTime.Now;
                TrafficDataPoint[] trafficDataPoints = boMonitorControl.getTrafficViews(_macAddress, startDateTime, endDateTime, term.IspId);

                //Load data into chart
                ChartConsumed.Width = 680;
                ChartConsumed.Height = 400;
                ChartConsumed.Titles.Add("7 Day Average Bitrate for SitId " + term.SitId);
                ChartConsumed.Titles.Add("Date and time created: " + DateTime.UtcNow + " UTC");
                ChartConsumed.Titles[0].Font = new Font("Utopia", 12);
                ChartConsumed.BackSecondaryColor = Color.WhiteSmoke;
                ChartConsumed.BackGradientStyle = System.Web.UI.DataVisualization.Charting.GradientStyle.DiagonalRight;
                ChartConsumed.ChartAreas[0].BackColor = Color.AliceBlue;
                ChartConsumed.ChartAreas[0].BackSecondaryColor = Color.GhostWhite;
                ChartConsumed.ChartAreas[0].BackGradientStyle = System.Web.UI.DataVisualization.Charting.GradientStyle.TopBottom;
                ChartConsumed.ChartAreas[0].Area3DStyle.Enable3D = false;
                ChartConsumed.ChartAreas[0].AxisX.Interval = 24;
                ChartConsumed.ChartAreas[0].AxisX.Title = "Time";
                ChartConsumed.ChartAreas[0].AxisY.Title = "kbps";

                //Add Return Volumes
                this.RtTrafficGraph(term, trafficDataPoints);
                

                //Add Forward volumes
                this.FwTrafficGraph(term, trafficDataPoints);
                

                //Check if we need to show high priority traffic
                if (boAccountingControl.IsBusinessSLA((int)term.SlaId))
                {
                    //Load data
                    TrafficDataPoint[] HighPriorityDataPoints = boMonitorControl.getHighPriorityTrafficViews(_macAddress, startDateTime, endDateTime, term.IspId);
                    this.FwHighPriorityTrafficGraph(term, HighPriorityDataPoints);
                    this.RtHighPriorityTrafficgraph(term, HighPriorityDataPoints);
                }


                ChartConsumed.Legends.Add(new Legend("Bitrate in kbps"));
            }
            catch (Exception ex)
            {
                BOLogControlWS boLogControlWS = new BOLogControlWS();
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDateTime = DateTime.Now;
                cmtEx.ExceptionDesc = ex.Message;
                cmtEx.UserDescription = "Method: TerminalConsumedTab.InitTab";
                cmtEx.StateInformation = "No data points avaialble";
                boLogControlWS.LogApplicationException(cmtEx);
            }
        }

        /// <summary>
        /// Initiates the forward high priority traffic graph
        /// </summary>
        /// <param name="term">Target terminal</param>
        private void FwHighPriorityTrafficGraph(Terminal term, TrafficDataPoint[] trafficDataPoints)
        {
            ChartConsumed.Series.Add(new Series("HiPriFVolumeSeries"));
            ChartConsumed.Series["HiPriFVolumeSeries"].BorderWidth = 3;
            ChartConsumed.Series["HiPriFVolumeSeries"].ShadowOffset = 2;
            ChartConsumed.Series["HiPriFVolumeSeries"].XValueType = ChartValueType.Int64;
            ChartConsumed.Series["HiPriFVolumeSeries"].Color = Color.LightGoldenrodYellow;

            List<long> FYValues = new List<long>();
            List<string> FXValues = new List<string>();

            DateTime previousDt = DateTime.MinValue;
            int secondsDelta = 0;
            long dataPointValue = 0;

            //Load data into YValues
            for (int i = 0; i < trafficDataPoints.Length; i++)
            {
                dataPointValue = Int64.Parse(trafficDataPoints[i].FwdDataPoint.value);
                DateTime offset = new DateTime(1970, 1, 1, 0, 0, 0);
                long epochTicks = Int64.Parse(trafficDataPoints[i].FwdDataPoint.timestamp);
                DateTime dt = offset.AddMilliseconds(epochTicks);

                //Calculate average
                if (previousDt != DateTime.MinValue)
                {
                    secondsDelta = dt.Subtract(previousDt).Minutes;
                }
                else
                {
                    secondsDelta = 30; //This is an assumption, could not be true
                }

                //Check for a possible divide by 0
                if (secondsDelta != 0)
                {
                    dataPointValue = ((dataPointValue / (secondsDelta * 60)) * 8) / 1000;
                    previousDt = dt;

                    string strMinute = dt.Minute.ToString();
                    if (dt.Minute < 10)
                        strMinute = "0" + strMinute;

                    FYValues.Add(dataPointValue);
                    FXValues.Add(dt.Year + "/" + dt.Month + "/" + dt.Day + " " + dt.Hour + ":" + strMinute);

                }
            }
            ChartConsumed.Series["HiPriFVolumeSeries"].Points.DataBindXY(FXValues, "Bitrate", FYValues, "kbps");
            ChartConsumed.Series["HiPriFVolumeSeries"].LegendText = "FW High Priority";
            ChartConsumed.Series["HiPriFVolumeSeries"].ChartType = SeriesChartType.Line;
        }

        /// <summary>
        /// Initiates the return high priority traffic graph
        /// </summary>
        /// <param name="term">Target terminal</param>
        private void RtHighPriorityTrafficgraph(Terminal term, TrafficDataPoint[] trafficDataPoints)
        {
            ChartConsumed.Series.Add(new Series("HiPriRVolumeSeries"));
            ChartConsumed.Series["HiPriRVolumeSeries"].BorderWidth = 3;
            ChartConsumed.Series["HiPriRVolumeSeries"].ShadowOffset = 2;
            ChartConsumed.Series["HiPriRVolumeSeries"].XValueType = ChartValueType.Int64;
            ChartConsumed.Series["HiPriRVolumeSeries"].Color = Color.BlueViolet;


            List<long> RYValues = new List<long>();
            List<string> RXValues = new List<string>();

            DateTime previousDt = DateTime.MinValue;
            int secondsDelta = 0;
            long dataPointValue = 0;

            //Load data into YValues
            for (int i = 0; i < trafficDataPoints.Length; i++)
            {
                dataPointValue = Int64.Parse(trafficDataPoints[i].RtnDataPoint.value);
                long epochTicks = Int64.Parse(trafficDataPoints[i].RtnDataPoint.timestamp);
                DateTime dt = DateHelper.EpochToDateTime(epochTicks);

                //Calculate average
                if (previousDt != DateTime.MinValue)
                {
                    secondsDelta = dt.Subtract(previousDt).Minutes;
                }
                else
                {
                    secondsDelta = 30; //This is an assumption, could not be true
                }

                //Check for a possible divide by 0
                if (secondsDelta != 0)
                {
                    dataPointValue = ((dataPointValue / (secondsDelta * 60)) * 8) / 1000;
                    previousDt = dt;

                    string strMinute = dt.Minute.ToString();
                    if (dt.Minute < 10)
                        strMinute = "0" + strMinute;



                    RYValues.Add(dataPointValue);
                    RXValues.Add(dt.Year + "/" + dt.Month + "/" + dt.Day + " " + dt.Hour + ":" + strMinute);

                }
            }
            ChartConsumed.Series["HiPriRVolumeSeries"].Points.DataBindXY(RXValues, "Bitrate", RYValues, "kbps");
            ChartConsumed.Series["HiPriRVolumeSeries"].LegendText = "RT High Priority";
            ChartConsumed.Series["HiPriRVolumeSeries"].ChartType = SeriesChartType.Line;
        }

        /// <summary>
        /// Initiates the forward best effort traffic graph
        /// </summary>
        /// <param name="term"></param>
        private void FwTrafficGraph(Terminal term, TrafficDataPoint[] trafficDataPoints)
        {
            ChartConsumed.Series.Add(new Series("FVolumeSeries"));
            ChartConsumed.Series["FVolumeSeries"].BorderWidth = 3;
            ChartConsumed.Series["FVolumeSeries"].ShadowOffset = 2;
            ChartConsumed.Series["FVolumeSeries"].XValueType = ChartValueType.Int64;
            ChartConsumed.Series["FVolumeSeries"].Color = Color.Red;

            List<long> FYValues = new List<long>();
            List<string> FXValues = new List<string>();

            DateTime previousDt = DateTime.MinValue;
            int secondsDelta = 0;
            long dataPointValue = 0;

            //Load data into YValues
            for (int i = 0; i < trafficDataPoints.Length; i++)
            {
                dataPointValue = Int64.Parse(trafficDataPoints[i].FwdDataPoint.value);
                DateTime offset = new DateTime(1970, 1, 1, 0, 0, 0);
                long epochTicks = Int64.Parse(trafficDataPoints[i].FwdDataPoint.timestamp);
                DateTime dt = offset.AddMilliseconds(epochTicks);

                //Calculate average
                if (previousDt != DateTime.MinValue)
                {
                    secondsDelta = dt.Subtract(previousDt).Minutes;
                }
                else
                {
                    secondsDelta = 30; //This is an assumption, could not be true
                }

                //Check for a possible divide by 0
                if (secondsDelta != 0)
                {
                    dataPointValue = ((dataPointValue / (secondsDelta * 60)) * 8) / 1000;
                    previousDt = dt;

                    string strMinute = dt.Minute.ToString();
                    if (dt.Minute < 10)
                        strMinute = "0" + strMinute;

                    FYValues.Add(dataPointValue);
                    FXValues.Add(dt.Year + "/" + dt.Month + "/" + dt.Day + " " + dt.Hour + ":" + strMinute);

                }
            }
            ChartConsumed.Series["FVolumeSeries"].Points.DataBindXY(FXValues, "Bitrate", FYValues, "kbps");
            ChartConsumed.Series["FVolumeSeries"].LegendText = "FW Best Effort";
            ChartConsumed.Series["FVolumeSeries"].ChartType = SeriesChartType.Line;
        }

        /// <summary>
        /// Initiates the return best effort traffic graph
        /// </summary>
        /// <param name="term"></param>
        private void RtTrafficGraph(Terminal term, TrafficDataPoint[] trafficDataPoints)
        {
            ChartConsumed.Series.Add(new Series("RVolumeSeries"));
            ChartConsumed.Series["RVolumeSeries"].BorderWidth = 3;
            ChartConsumed.Series["RVolumeSeries"].ShadowOffset = 2;
            ChartConsumed.Series["RVolumeSeries"].XValueType = ChartValueType.Int64;
            ChartConsumed.Series["RVolumeSeries"].Color = Color.Green;


            List<long> RYValues = new List<long>();
            List<string> RXValues = new List<string>();

            DateTime previousDt = DateTime.MinValue;
            int secondsDelta = 0;
            long dataPointValue = 0;

            //Load data into YValues
            for (int i = 0; i < trafficDataPoints.Length; i++)
            {
                dataPointValue = Int64.Parse(trafficDataPoints[i].RtnDataPoint.value);
                long epochTicks = Int64.Parse(trafficDataPoints[i].RtnDataPoint.timestamp);
                DateTime dt = DateHelper.EpochToDateTime(epochTicks);

                //Calculate average
                if (previousDt != DateTime.MinValue)
                {
                    secondsDelta = dt.Subtract(previousDt).Minutes;
                }
                else
                {
                    secondsDelta = 30; //This is an assumption, could not be true
                }

                //Check for a possible divide by 0
                if (secondsDelta != 0)
                {
                    dataPointValue = ((dataPointValue / (secondsDelta * 60)) * 8) / 1000;
                    previousDt = dt;

                    string strMinute = dt.Minute.ToString();
                    if (dt.Minute < 10)
                        strMinute = "0" + strMinute;



                    RYValues.Add(dataPointValue);
                    RXValues.Add(dt.Year + "/" + dt.Month + "/" + dt.Day + " " + dt.Hour + ":" + strMinute);

                }
            }
            ChartConsumed.Series["RVolumeSeries"].Points.DataBindXY(RXValues, "Bitrate", RYValues, "kbps");
            ChartConsumed.Series["RVolumeSeries"].LegendText = "RT Best Effort";
            ChartConsumed.Series["RVolumeSeries"].ChartType = SeriesChartType.Line;
        }
    
    }
}