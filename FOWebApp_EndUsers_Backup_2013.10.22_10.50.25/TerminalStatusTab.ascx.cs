﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Web.UI.DataVisualization.Charting;
using System.Globalization;
using System.Web.Security;
using FOWebApp_EndUsers.net.nimera.cmt.BOMonitorControlWSRef;
using FOWebApp_EndUsers.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp_EndUsers.net.nimera.cmt.BOLogControlWSRef;

namespace FOWebApp_EndUsers
{
    public partial class TerminalStatusTab : System.Web.UI.UserControl
    {
        string _macAddress;

        /// <summary>
        /// MacAddress which must be set before loading the component!
        /// </summary>
        public string MacAddress
        {
            get { return _macAddress; }
            set { _macAddress = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!MacAddress.Equals(""))
            {
                this.initTab(_macAddress);
            }
        }

        /// <summary>
        /// Initializes the tab page
        /// </summary>
        /// <param name="macAddress">Target macAddress</param>
        public void initTab(string macAddress)
        {
            CultureInfo culture = new CultureInfo("en-US");
            BOMonitorControlWS boMonitorControl =
                new net.nimera.cmt.BOMonitorControlWSRef.BOMonitorControlWS();

            BOAccountingControlWS boAccountingControl =
                new net.nimera.cmt.BOAccountingControlWSRef.BOAccountingControlWS();

            Terminal term = boAccountingControl.GetTerminalDetailsByMAC(macAddress);
            if (term != null)
            {
                TerminalInfo ti = boMonitorControl.getTerminalInfoByMacAddress(macAddress, term.IspId);
                this.showForwardGraph(ti);
                this.showReturnGraph(ti);
            }
        }

        /// <summary>
        /// Shows the consumed forward volume
        /// </summary>
        /// <param name="ti">The soruce TerminalInfo entity</param>
        public void showForwardGraph(TerminalInfo ti)
        {
            bool freeZoneFlag = this.IsFreeZone(Int32.Parse(ti.SlaId)); //Check if this is freezone sla
            LabelFUPResetDay.Text = ti.InvoiceDayOfMonth;

            decimal consumedGB = Convert.ToInt64(ti.Consumed) / (decimal)1000000000.0;

            decimal FUPThresholdBytes = ti.FUPThreshold * (decimal)1000000000.0;
            FUPThresholdBytes = FUPThresholdBytes + Convert.ToDecimal(ti.Additional);

            Decimal FUPThresholdBytesGB = FUPThresholdBytes / (decimal)1000000000.0;
            LabelVolumeAllocation.Text = FUPThresholdBytesGB.ToString("#.00") + " GB";


            LabelTimeStamp.Text = DateTime.Now.ToUniversalTime() + " UTC";

            //double percentConsumed = (Convert.ToDouble(ti.Consumed) / Convert.ToDouble(FUPThresholdBytes)) * 100.0;
            //Decimal percentConsumedDec = new Decimal(percentConsumed);
            Decimal percentConsumed = (Convert.ToDecimal(ti.Consumed) / (FUPThresholdBytes + Convert.ToDecimal(ti.Additional))) * 100.0M;

            ChartForwardVolume.Width = 300;
            ChartForwardVolume.Titles.Add("Forward Volume Status");
            ChartForwardVolume.Titles[0].Font = new Font("Utopia", 16);
            ChartForwardVolume.ChartAreas[0].BackColor = Color.LightSalmon;
            ChartForwardVolume.ChartAreas[0].BackSecondaryColor = Color.LightGreen;
            ChartForwardVolume.ChartAreas[0].BackGradientStyle = System.Web.UI.DataVisualization.Charting.GradientStyle.TopBottom;
            ChartForwardVolume.ChartAreas[0].Area3DStyle.Enable3D = false;

            if (ChartForwardVolume.Series.FindByName("FVolumeSeries") == null)
            {
                ChartForwardVolume.Series.Add(new Series("FVolumeSeries"));
            }

            ChartForwardVolume.Series["FVolumeSeries"].ChartType = SeriesChartType.StackedColumn;
            ChartForwardVolume.Series["FVolumeSeries"].BorderWidth = 3;
            ChartForwardVolume.Series["FVolumeSeries"].ShadowOffset = 2;
            ChartForwardVolume.Series["FVolumeSeries"].XValueType = ChartValueType.Double;

            if (freeZoneFlag)
            {
                PanelFreeZone.Visible = true;
                decimal freeZoneGB = Convert.ToInt64(ti.FreeZone) / (decimal)1000000000.0;

                //Take free zone consumption into account
                consumedGB = consumedGB - freeZoneGB;
                percentConsumed = (consumedGB / ti.FUPThreshold) * 100.0M;

                LabelFreezoneConsumed.Text = "" + freeZoneGB.ToString("0.000") + " GB";

                if (ChartForwardVolume.Series.FindByName("FreeZoneSeries") == null)
                {
                    ChartForwardVolume.Series.Add(new Series("FreeZoneSeries"));
                }
                ChartForwardVolume.Series["FreeZoneSeries"].ChartType = SeriesChartType.StackedColumn;
                ChartForwardVolume.Series["FreeZoneSeries"].BorderWidth = 3;
                ChartForwardVolume.Series["FreeZoneSeries"].ShadowOffset = 2;
                ChartForwardVolume.Series["FreeZoneSeries"].XValueType = ChartValueType.Double;
                ChartForwardVolume.Series["FreeZoneSeries"].Color = Color.LightGray;

                string[] X2Values = new String[] { "Free Zone", };
                List<decimal> Y2Values = new List<decimal>();
                Y2Values.Add(freeZoneGB);
                ChartForwardVolume.Series["FreeZoneSeries"].Points.DataBindXY(X2Values, "Volume", Y2Values, "GB");
                ChartForwardVolume.Series["FreeZoneSeries"].LegendText = "Freezone in GB";
            }
            else
            {
                PanelFreeZone.Visible = true;
            }

            List<decimal> YValues = new List<decimal>();

            Color chartColor = Color.Gray;

            if (percentConsumed <= 80.0M)
            {
                chartColor = Color.Green;
            }
            else if (percentConsumed > 80.0M && percentConsumed < 100.0M)
            {
                chartColor = Color.Orange;
            }
            else
            {
                chartColor = Color.Red;
            }

            ChartForwardVolume.Series["FVolumeSeries"].Color = chartColor;
            YValues.Add(consumedGB);
            LabelVolumeConsumed.Text = "" + consumedGB.ToString("0.000") + " GB";

            string[] XValues = new String[] { "Forward Volume" };
            ChartForwardVolume.Series["FVolumeSeries"].Points.DataBindXY(XValues, "Volume", YValues, "GB");
            ChartForwardVolume.Series["FVolumeSeries"].LegendText = "Volumes in GB";

            if (ChartForwardVolume.Series.FindByName("FUPThreshold") == null)
            {
                ChartForwardVolume.Series.Add(new Series("FUPThreshold"));
            }
            ChartForwardVolume.Series["FUPThreshold"].ChartType = SeriesChartType.Line;
            ChartForwardVolume.Series["FUPThreshold"].BorderWidth = 3;

            if (percentConsumed < 100)
            {
                ChartForwardVolume.Series["FUPThreshold"].Color = Color.Red;
            }
            else
            {
                ChartForwardVolume.Series["FUPThreshold"].Color = Color.Wheat;
            }
            ChartForwardVolume.Series["FUPThreshold"].LegendText = "FUP Threshold in GB";
            ChartForwardVolume.Series["FUPThreshold"].AxisLabel = "FUP Threshold";
            ChartForwardVolume.Series["FUPThreshold"].MarkerStyle = MarkerStyle.Diamond;
            ChartForwardVolume.Series["FUPThreshold"].MarkerSize = 15;
            ChartForwardVolume.Series["FUPThreshold"].IsValueShownAsLabel = true;
            ChartForwardVolume.Series["FUPThreshold"].LabelForeColor = Color.Yellow;
            ChartForwardVolume.Series["FUPThreshold"].LabelBackColor = Color.Black;

            List<decimal> FUPValues = new List<decimal>();
            FUPValues.Add(ti.FUPThreshold);
            ChartForwardVolume.Series["FUPThreshold"].Points.DataBindY(FUPValues);

            Legend volumeLegend = new Legend("VolLegend");
            if (ChartForwardVolume.Legends.FindByName("Volume in GB") == null)
            {
                ChartForwardVolume.Legends.Add(new Legend("Volume in GB"));
            }
        }

        /// <summary>
        /// Shows the consumed return volume
        /// </summary>
        /// <param name="ti">The source TerminalInfo entity</param>
        public void showReturnGraph(TerminalInfo ti)
        {
            bool freeZoneFlag = this.IsFreeZone(Int32.Parse(ti.SlaId)); //Check if this is freezone sla
            LabelFUPResetDay.Text = ti.InvoiceDayOfMonth;

            LabelReturnVolumeAllocation.Text = ti.RTNFUPThreshold + " GB";

            decimal consumedGB = Convert.ToInt64(ti.ConsumedReturn) / (decimal)1000000000.0;
            decimal RTNFUPThresholdBytes = ti.RTNFUPThreshold * (decimal)1000000000.0;
            RTNFUPThresholdBytes = RTNFUPThresholdBytes + Convert.ToDecimal(ti.AdditionalReturn);

            Decimal RTNFUPThresholdBytesGB = RTNFUPThresholdBytes / (decimal)1000000000.0;
          
            LabelTimeStamp.Text = DateTime.Now.ToUniversalTime() + " UTC";
            double percentConsumed = 0.0;

            //Avoid divide by zero
            if (RTNFUPThresholdBytes != (decimal)0.0)
            {
                percentConsumed = (Convert.ToDouble(ti.ConsumedReturn) / Convert.ToDouble(RTNFUPThresholdBytes)) * 100.0;
                Decimal percentConsumedDec = new Decimal(percentConsumed);
            }

            ChartReturnVolume.Width = 300;
            ChartReturnVolume.Titles.Add("Return Volume Status");
            ChartReturnVolume.Titles[0].Font = new Font("Utopia", 16);
            //ChartReturnVolume.BackSecondaryColor = Color.WhiteSmoke;
            //ChartReturnVolume.BackGradientStyle = System.Web.UI.DataVisualization.Charting.GradientStyle.DiagonalRight;
            ChartReturnVolume.ChartAreas[0].BackColor = Color.LightSalmon;
            ChartReturnVolume.ChartAreas[0].BackSecondaryColor = Color.LightGreen;
            ChartReturnVolume.ChartAreas[0].BackGradientStyle = System.Web.UI.DataVisualization.Charting.GradientStyle.TopBottom;
            ChartReturnVolume.ChartAreas[0].Area3DStyle.Enable3D = false;

            if (ChartReturnVolume.Series.FindByName("RVolumeSeries") == null)
            {
                ChartReturnVolume.Series.Add(new Series("RVolumeSeries"));
            }

            ChartReturnVolume.Series["RVolumeSeries"].ChartType = SeriesChartType.StackedColumn;
            ChartReturnVolume.Series["RVolumeSeries"].BorderWidth = 3;
            ChartReturnVolume.Series["RVolumeSeries"].ShadowOffset = 2;
            ChartReturnVolume.Series["RVolumeSeries"].XValueType = ChartValueType.Double;

            if (freeZoneFlag)
            {
                decimal freeZoneGB = Convert.ToInt64(ti.ConsumedFreeZone) / (decimal)1000000000.0;

                //Take free zone consumption into account
                consumedGB = consumedGB - freeZoneGB;
                percentConsumed = (Convert.ToDouble(consumedGB) / Convert.ToDouble(ti.RTNFUPThreshold)) * 100.0;
                LabelRTNFreezoneConsumed.Text = "" + freeZoneGB.ToString("0.000") + " GB";

                if (ChartReturnVolume.Series.FindByName("FreeZoneSeries") == null)
                {
                    ChartReturnVolume.Series.Add(new Series("FreeZoneSeries"));
                }
                ChartReturnVolume.Series["FreeZoneSeries"].ChartType = SeriesChartType.StackedColumn;
                ChartReturnVolume.Series["FreeZoneSeries"].BorderWidth = 3;
                ChartReturnVolume.Series["FreeZoneSeries"].ShadowOffset = 2;
                ChartReturnVolume.Series["FreeZoneSeries"].XValueType = ChartValueType.Double;
                ChartReturnVolume.Series["FreeZoneSeries"].Color = Color.LightGray;

                string[] X2Values = new String[] { "Free Zone", };
                List<decimal> Y2Values = new List<decimal>();
                Y2Values.Add(freeZoneGB);
                ChartReturnVolume.Series["FreeZoneSeries"].Points.DataBindXY(X2Values, "Volume", Y2Values, "GB");
                ChartReturnVolume.Series["FreeZoneSeries"].LegendText = "Freezone in GB";
            }
          
            List<decimal> YValues = new List<decimal>();

            Color chartColor = Color.Gray;

            if (percentConsumed <= 80.0)
            {
                chartColor = Color.Green;
            }
            else if (percentConsumed > 80.0 && percentConsumed < 100.0)
            {
                chartColor = Color.Orange;
            }
            else
            {
                chartColor = Color.Red;
            }

            ChartReturnVolume.Series["RVolumeSeries"].Color = chartColor;
            YValues.Add(consumedGB);
            LabelReturnVolumeConsumed.Text = "" + consumedGB.ToString("0.000") + " GB";

            string[] XValues = new String[] { "Return Volume" };
            ChartReturnVolume.Series["RVolumeSeries"].Points.DataBindXY(XValues, "Volume", YValues, "GB");
            ChartReturnVolume.Series["RVolumeSeries"].LegendText = "Volumes in GB";
            if (ChartReturnVolume.Series.FindByName("FUPThreshold") == null)
            {
                ChartReturnVolume.Series.Add(new Series("FUPThreshold"));
            }
            ChartReturnVolume.Series["FUPThreshold"].ChartType = SeriesChartType.Line;
            ChartReturnVolume.Series["FUPThreshold"].BorderWidth = 3;

            if (percentConsumed < 100)
            {
                ChartReturnVolume.Series["FUPThreshold"].Color = Color.Red;
            }
            else
            {
                ChartReturnVolume.Series["FUPThreshold"].Color = Color.Wheat;
            }
            ChartReturnVolume.Series["FUPThreshold"].LegendText = "FUP Threshold in GB";
            ChartReturnVolume.Series["FUPThreshold"].AxisLabel = "FUP Threshold";
            ChartReturnVolume.Series["FUPThreshold"].MarkerStyle = MarkerStyle.Diamond;
            ChartReturnVolume.Series["FUPThreshold"].MarkerSize = 15;
            ChartReturnVolume.Series["FUPThreshold"].IsValueShownAsLabel = true;
            ChartReturnVolume.Series["FUPThreshold"].LabelForeColor = Color.Yellow;
            ChartReturnVolume.Series["FUPThreshold"].LabelBackColor = Color.Black;

            List<decimal> FUPValues = new List<decimal>();
            FUPValues.Add(ti.RTNFUPThreshold);
            ChartReturnVolume.Series["FUPThreshold"].Points.DataBindY(FUPValues);

            Legend volumeLegend = new Legend("VolLegend");
            if (ChartReturnVolume.Legends.FindByName("Volume in GB") == null)
            {
                ChartReturnVolume.Legends.Add(new Legend("Volume in GB"));
            }
        }


        /// <summary>
        /// Checks if a given SLA offers a freezone option
        /// </summary>
        /// <param name="slaID">The identifier of the SLA to check</param>
        /// <returns>true if the SLA offers a freezone option</returns>
        public bool IsFreeZone(int slaID)
        {
            BOAccountingControlWS boAccountingControl =
                   new net.nimera.cmt.BOAccountingControlWSRef.BOAccountingControlWS();
            if (boAccountingControl.IsFreeZoneSLA(slaID))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}