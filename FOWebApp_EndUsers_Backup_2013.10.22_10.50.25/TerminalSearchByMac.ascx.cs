﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace FOWebApp_EndUsers
{
    public partial class TerminalSearchByMac : System.Web.UI.UserControl
    {
        const string _HEXBYTE = "<0|1|2|3|4|5|6|7|8|9|a|b|c|d|e|f><0|1|2|3|4|5|6|7|8|9|a|b|c|d|e|f>";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
               if (Request.Cookies["SatADSLMAC"] != null)
                {
                    RadMaskedTextBoxMacAddress.Text = Server.HtmlEncode(Request.Cookies["SatADSLMAC"].Value);
                    //Load the tab view
                    RadTab terminalsTab = RadTabStripTerminalView.FindTabByText("Terminal");
                    AddPageView(terminalsTab);
                }
            }

            RadMaskedTextBoxMacAddress.Mask =
              _HEXBYTE + "<:>" + _HEXBYTE + "<:>" + _HEXBYTE + "<:>" + _HEXBYTE + "<:>" + _HEXBYTE + "<:>" +
              _HEXBYTE;
        }

        private void AddPageView(RadTab tab)
        {
            RadPageView pageView = new RadPageView();
            pageView.ID = tab.Text;
            RadMultiPageTerminalView.PageViews.Add(pageView);
            pageView.CssClass = "pageView";
            tab.PageViewID = pageView.ID;
        }

        protected void RadMultiPageTerminalView_PageViewCreated(object sender, RadMultiPageEventArgs e)
        {
            Control control = null;
            //Add content to the page view depending on the selected tab
            if (e.PageView.ID.Equals("Terminal"))
            {
                //Load the TerminalDetailTab control
                TerminalDetailTab terminalDetailTab =
                    (TerminalDetailTab)Page.LoadControl("TerminalDetailTab.ascx");
                terminalDetailTab.MacAddress = RadMaskedTextBoxMacAddress.Text;
                control = terminalDetailTab;
                control.ID = e.PageView.ID + "_userControl";
            }
            else if (e.PageView.ID.Equals("Status"))
            {
                //Load the TerminalDetailTab control
                TerminalStatusTab terminalStatusTab =
                    (TerminalStatusTab)Page.LoadControl("TerminalStatusTab.ascx");
                terminalStatusTab.MacAddress = RadMaskedTextBoxMacAddress.Text;

                control = terminalStatusTab;
                control.ID = e.PageView.ID + "_userControl";
            }
            else
            {
                control = Page.LoadControl("TestUserControl.ascx");
                control.ID = e.PageView.ID + "_userControl";
            }

            e.PageView.Controls.Add(control);
        }

        protected void RadTabStripTerminalView_TabClick(object sender, Telerik.Web.UI.RadTabStripEventArgs e)
        {
            AddPageView(e.Tab);
            e.Tab.PageView.Selected = true;
        }

        protected void ImageButtonSearch_Click(object sender, ImageClickEventArgs e)
        {
            //Initialize the tabview with the provided tab address
            string macAddress = RadMaskedTextBoxMacAddress.Text;
            if (!macAddress.Equals(""))
            {
                //Set as a cookie
                HttpCookie httpCookie = new HttpCookie("SatADSLMAC", macAddress);
                httpCookie.Expires = DateTime.Now.AddDays(10);
                Response.Cookies.Add(httpCookie);

                //Update the terminal details tab
                RadTab terminalsTab = RadTabStripTerminalView.FindTabByText("Terminal");

                if (terminalsTab.PageView == null)
                {
                    this.AddPageView(terminalsTab);
                }
                else
                {
                    TerminalDetailTab terminalDetailTab =
                                (TerminalDetailTab)terminalsTab.PageView.Controls[0];
                    terminalDetailTab.MacAddress = macAddress;
                    terminalDetailTab.LoadPageContent();
                }

                //Update the status tab
                RadTab statusTab = RadTabStripTerminalView.FindTabByText("Status");

                if (statusTab.PageView == null)
                {
                    this.AddPageView(statusTab);
                }
                else
                {
                    TerminalStatusTab statusDetailTab =
                                (TerminalStatusTab)statusTab.PageView.Controls[0];
                    statusDetailTab.MacAddress = macAddress;
                    statusDetailTab.initTab(macAddress);
                }
            }
        }
    }
}