﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using FOWebApp_EndUsers.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp_EndUsers.net.nimera.cmt.BOLogControlWSRef;

namespace FOWebApp_EndUsers
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Insert a message in the UserActivity log
            BOLogControlWS boLogControlWS = new BOLogControlWS();

            //Get the authenticated user
            MembershipUser myObject = Membership.GetUser();
            if (myObject != null)
            {
                string userId = myObject.ProviderUserKey.ToString();
                UserActivity userActivity = new UserActivity();
                userActivity.UserId = new Guid(userId);
                userActivity.ActionDate = DateTime.Now;
                userActivity.Action = "End user log On";
                boLogControlWS.LogUserActivity(userActivity);
            }
            else
            {
                //Close the session and present login screen
                Session.Abandon();
                FormsAuthentication.SignOut();
                Response.Redirect("Default.aspx");
            }

            Control headerControl = Page.LoadControl("~/DefaultHeader.ascx");
            PlaceHolderHeader.Controls.Clear();
            PlaceHolderHeader.Controls.Add(headerControl);

            Control control = Page.LoadControl("~/TerminalSearchByMac.ascx");
            PlaceHolderBody.Controls.Clear();
            PlaceHolderBody.Controls.Add(control);
        }
    }
}