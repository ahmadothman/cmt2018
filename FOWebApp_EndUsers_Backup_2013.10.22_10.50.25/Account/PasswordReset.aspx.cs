﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Security;
using System.Drawing;
using System.Web.UI.WebControls;
using FOWebApp_EndUsers.net.nimera.cmt.BOLogControlWSRef;

namespace FOWebApp_EndUsers.Account
{
    public partial class PasswordReset : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void RadButtonReset_Click(object sender, EventArgs e)
        {
            BOLogControlWS logControl = new BOLogControlWS();
            //Reset the password if the username is existing
            string userName = TextBoxUserName.Text;

            //Load the Membership info
            MembershipUserCollection storedUsers = Membership.GetAllUsers();
            MembershipUser storedUser = storedUsers[userName];

            if (storedUser != null)
            {
                storedUser.IsApproved = true;
                storedUser.UnlockUser();
                string newPassword = storedUser.ResetPassword();

                string bodyText = "Dear customer, <br/><br/> Your password has been reset. " +
                "For your next logon into the CMT please use the following password: " +
                "<b><br/><br/>" + newPassword + "</b><br/><br/>" + "Please change your password as soon as possible " +
                "by means of the Change Password option in the CMT menu.<br/><br/>" +
                "Best regards,<br/><br/>" +
                "Your SatADSL support team";

                string[] toAddress = { storedUser.Email };

                if (logControl.SendMail(bodyText, "Password change", toAddress))
                {
                    LabelStatus.Visible = true;
                    LabelStatus.ForeColor = Color.LightGreen;
                    LabelStatus.Text = "Customer password successfully reset!";
                }
                else
                {
                    LabelStatus.Visible = true;
                    LabelStatus.ForeColor = Color.LightGoldenrodYellow;
                    LabelStatus.Text = "Sorry but I could E-Mail your new password!";
                }
            }
            else
            {
                LabelStatus.Visible = true;
                LabelStatus.ForeColor = Color.LightGoldenrodYellow;
                LabelStatus.Text = "This is not a registered user name!";
            }
        }
    }
}