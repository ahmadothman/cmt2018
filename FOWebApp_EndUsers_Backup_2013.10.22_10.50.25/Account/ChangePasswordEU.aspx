﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ChangePasswordEU.aspx.cs"
    Inherits="FOWebApp_EndUsers.Account.ChangePasswordEU" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Change Password</title>
    <link href="~/Styles/PopUpWindowsStyles.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .style1
        {
            width: 243px;
        }
    </style>
</head>
<body background="../Images/BlueEarth.jpg">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div>
            <asp:ChangePassword ID="ChangePassword" runat="server" 
                ChangePasswordFailureText="Password change failed">
            <ChangePasswordTemplate>
                <table cellpadding="10" cellspacing="5">
                    <tr>
                        <td>
                            Old Password:
                        </td>
                        <td class="style1">
                            <asp:TextBox ID="CurrentPassword" runat="server" TextMode="Password">
                            </asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            New Password:
                        </td>
                        <td class="style1">
                            <asp:TextBox ID="NewPassword" runat="server" TextMode="Password"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Repeat Password:
                        </td>
                        <td class="style1">
                            <asp:TextBox ID="ConfirmNewPassword" runat="server" TextMode="Password"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                           <telerik:RadButton ID="ChangePasswordPushButton" CommandName="ChangePassword" 
                                runat="server" Text="Confirm" 
                                ToolTip="Confirm you password change" ButtonType="StandardButton" 
                                Skin="WebBlue">
                            </telerik:RadButton>
                        </td>
                        <td class="style1">
                            <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                        </td>
                    </tr>
                </table>
            </ChangePasswordTemplate>
            <SuccessTemplate>
                <br /><br />&nbsp
                <asp:Literal ID="Literal1" runat="server">Your password has been changed!</asp:Literal>
            </SuccessTemplate>
        </asp:ChangePassword>
    </div>
    </form>
</body>
</html>
