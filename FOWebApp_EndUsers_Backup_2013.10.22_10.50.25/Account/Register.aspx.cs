﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using FOWebApp_EndUsers.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp_EndUsers.net.nimera.cmt.BOLogControlWSRef;
using FOWebApp_EndUsers.net.nimera.cmt.BOUserControlWS;

namespace FOWebApp_EndUsers.Account
{
    public partial class Register : System.Web.UI.Page
    {
        BOAccountingControlWS _boAccountingController;
        BOLogControlWS _boLogController;
        BOUserControlWS _boUserController;

        protected void Page_Load(object sender, EventArgs e)
        {
            _boAccountingController = new BOAccountingControlWS();
            _boLogController = new BOLogControlWS();
            _boUserController = new BOUserControlWS();

            //Add the about control to the content pane
            Control headerControl = Page.LoadControl("~/DefaultHeader.ascx");
            PlaceHolderHeader.Controls.Clear();
            PlaceHolderHeader.Controls.Add(headerControl);
        }

        protected void RegisterUser_CreatedUser(object sender, EventArgs e)
        {
            //FormsAuthentication.SetAuthCookie(RegisterUser.UserName, false /* createPersistentCookie */);

            /*
            string continueUrl = RegisterUser.ContinueDestinationPageUrl;
            if (String.IsNullOrEmpty(continueUrl))
            {
                continueUrl = "~/";
            }
            Response.Redirect(continueUrl);
             */
        }
       
        protected void RadButtonRegister_Click(object sender, EventArgs e)
        {
            string userPwd = RadTextBoxPassword.Text;
            string repeatPwd = RadTextBoxPassword.Text;
            MembershipUser user = null;
            MembershipCreateStatus status;

            if (!userPwd.Equals(repeatPwd))
            {
                LabelStatus.ForeColor = Color.Red;
                LabelStatus.Text = "Password and Password Repeat don't match!";
            }
            else
            {
                user = Membership.CreateUser(RadTextBoxUserName.Text, repeatPwd, RadTextBoxEMail.Text, "void", "void", true, out status);

                if (user == null)
                {
                    LabelStatus.ForeColor = Color.Red;
                    LabelStatus.Text = "Unable to create user: " + status.ToString();
                    CmtApplicationException cmtEx = new CmtApplicationException();
                    cmtEx.ExceptionDateTime = DateTime.Now;
                    cmtEx.ExceptionDesc = "Unable to create system user: " + status.ToString();
                    cmtEx.ExceptionStacktrace = "FOWebApp_EndUsers, user registration";
                    _boLogController.LogApplicationException(cmtEx);
                }
                else
                {
                    //Create the user in the database

                    if (Membership.ValidateUser(RadTextBoxUserName.Text, repeatPwd))
                    {
                        FormsAuthentication.RedirectFromLoginPage(RadTextBoxUserName.Text, false);
                    }
                }
            }
        }

    }
}
