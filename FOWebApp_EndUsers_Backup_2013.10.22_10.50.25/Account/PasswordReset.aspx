﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PasswordReset.aspx.cs" Inherits="FOWebApp_EndUsers.Account.PasswordReset" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Password Reset</title>
    <link href="../Styles/PopUpWindowsStyles.css" rel="stylesheet" type="text/css" />
</head>
<body background="../Images/BlueEarth.jpg">
    <form id="formMain" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <div>
            <asp:Literal ID="LiteralMessage" runat="server">
                Enter your registered username and click on the reset button to
                reset your password. You will receive an E-Mail, sent to the 
                address you used during the registration process, with a temporary
                password. Change this password as soon as possible.
            </asp:Literal> 
            <table cellpadding="10" cellspacing="5">
                <tr>
                    <td>
                        <asp:Literal ID="LiteralUserName" runat="server">User name:</asp:Literal>
                    </td>
                    <td>
                        <asp:TextBox ID="TextBoxUserName" runat="server" Width="237px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <telerik:RadButton ID="RadButtonReset" runat="server" Text="Reset Password" 
                            Skin="WebBlue" onclick="RadButtonReset_Click">
                        </telerik:RadButton>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Label ID="LabelStatus" runat="server"></asp:Label> 
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
