﻿<%@ Page Title="Register" Language="C#" MasterPageFile="~/Design/EndUserSite.Master"
    AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="FOWebApp_EndUsers.Account.Register" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="ContentBody" ContentPlaceHolderID="BodyContentPlaceHolder" runat="server">
    <asp:PlaceHolder ID="PlaceHolderHeader" runat="server"></asp:PlaceHolder>
    <div id="main-container">
        <div class="wrapper clearfix">
            <div id="path">
                <span>You are here:</span> <a href="http://www.satadsl.net/index.html">Home</a>
                &gt; <a href="#">My SatADSL</a>
            </div>
            <div id="main">
                <h3>
                    Register as a MySatADSL user!</h3>
                <table>
                    <tr>
                        <td style="border-bottom: 0px; border-top: 1px; vertical-align: middle; font-size: 12px">
                            Username:
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px; vertical-align: middle; font-size: 12px">
                            <telerik:RadTextBox ID="RadTextBoxUserName" runat="server" Width="200px">
                            </telerik:RadTextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorUserName" runat="server" ErrorMessage="You must specify a user name!" ControlToValidate="RadTextBoxUserName" ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td style="border-bottom: 0px; border-top: 0px; vertical-align: middle; font-size: 12px">
                            Password:
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px; vertical-align: middle; font-size: 12px">
                            <telerik:RadTextBox ID="RadTextBoxPassword" runat="server" TextMode="Password">
                            </telerik:RadTextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="You must specify a password!" ControlToValidate="RadTextBoxPassword" ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td style="border-bottom: 0px; border-top: 0px; vertical-align: middle; font-size: 12px ">
                            Repeat password:
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px; vertical-align: middle; font-size: 12px  ">
                            <telerik:RadTextBox ID="RadTextBoxPwdRepeat" runat="server" TextMode="Password">
                            </telerik:RadTextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please repeat your password!" ControlToValidate="RadTextBoxPwdRepeat" ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td style="border-bottom: 0px; border-top: 0px; vertical-align: middle; font-size: 12px ">
                            First Name:
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px; vertical-align: middle; font-size: 12px ">
                            <telerik:RadTextBox ID="RadTextBoxFName" runat="server" Width="250px">
                            </telerik:RadTextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Please specify your first name!" ControlToValidate="RadTextBoxFName" ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td style="border-bottom: 0px; border-top: 0px; vertical-align: middle; font-size: 12px ">
                            Last Name:
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px; vertical-align: middle; font-size: 12px ">
                            <telerik:RadTextBox ID="RadTextBoxLName" runat="server" Width="250px">
                            </telerik:RadTextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Please specify your last name!" ControlToValidate="RadTextBoxLName" ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td style="border-bottom: 0px; border-top: 0px; vertical-align: middle; font-size: 12px ">
                            Phone:
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px; vertical-align: middle; font-size: 12px ">
                            <telerik:RadTextBox ID="RadTextBoxPhone" runat="server" Width="200px">
                            </telerik:RadTextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Please specify your main phone number!" ControlToValidate="RadTextBoxPhone" ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td style="border-bottom: 0px; border-top: 0px; vertical-align: middle; font-size: 12px ">
                            Mobile Phone:
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px; vertical-align: middle; font-size: 12px ">
                            <telerik:RadTextBox ID="RadTextBoxMobilePhone" runat="server" Width="200px">
                            </telerik:RadTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="border-bottom: 0px; border-top: 0px; vertical-align: middle; font-size: 12px ">
                            E-Mail:
                        </td>
                        <td style="border-bottom: 0px; border-top: 0px; vertical-align: middle; font-size: 12px ">
                            <telerik:RadTextBox ID="RadTextBoxEMail" runat="server" Width="250px">
                            </telerik:RadTextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Please specify your E-Mail address!" ControlToValidate="RadTextBoxEMail" ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td style="border-bottom: 1px; border-top: 0px; vertical-align: middle; font-size: 12px  ">
                            <telerik:RadButton ID="RadButtonRegister" runat="server" 
                                Text="Complete registration" onclick="RadButtonRegister_Click">
                            </telerik:RadButton>
                        </td>
                        <td style="font-size: 12px">
                            <asp:Label ID="LabelStatus" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div id="footer-container">
        <footer class="wrapper clearfix">
            <div id="footerLinks">
				<span id="copyright">&copy;2012 SatADSL</span>
				<a href="http://www.satadsl.net/Disclaimer.html">Disclaimer</a>
                 <a href="http://www.nimera.net" target="_blank">Created by Nimera Mobile ICT</a>
			</div>
        </footer>
    </div>
</asp:Content>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="head">
    <style type="text/css">
        .style1
        {
            width: 160px;
        }
    </style>
</asp:Content>
