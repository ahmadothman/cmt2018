﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Drawing;
using System.Web.Security;
using Telerik.Web.UI;
using FOWebApp_EndUsers.net.nimera.cmt.BOMonitorControlWSRef;
using FOWebApp_EndUsers.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp_EndUsers.net.nimera.cmt.BOLogControlWSRef;

namespace FOWebApp_EndUsers
{
    public partial class TerminalDetailTab : System.Web.UI.UserControl
    {
        string _macAddress;

        /// <summary>
        /// MacAddress which must be set before loading the component!
        /// </summary>
        public string MacAddress
        {
            get { return _macAddress; }
            set { _macAddress = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!IsPostBack)
            {
                this.LoadPageContent();
            }
        }

        /// <summary>
        /// Checks if a given SLA offers a freezone option
        /// </summary>
        /// <param name="slaID">The identifier of the SLA to check</param>
        /// <returns>true if the SLA offers a freezone option</returns>
        public bool IsFreeZone(int slaID)
        {
            BOAccountingControlWS boAccountingControl =
                   new net.nimera.cmt.BOAccountingControlWSRef.BOAccountingControlWS();
            if (boAccountingControl.IsFreeZoneSLA(slaID))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Loads the page content
        /// </summary>
        public void LoadPageContent()
        {
            bool freeZoneFlag = false;
            CultureInfo culture = new CultureInfo("en-US");
            BOMonitorControlWS boMonitorControl =
                new net.nimera.cmt.BOMonitorControlWSRef.BOMonitorControlWS();

            BOAccountingControlWS boAccountingControl =
                new net.nimera.cmt.BOAccountingControlWSRef.BOAccountingControlWS();

            MembershipUser user = Membership.GetUser();

            Terminal term = boAccountingControl.GetTerminalDetailsByMAC(_macAddress);
            if (term != null)
            {
                TerminalInfo ti = boMonitorControl.getTerminalInfoByMacAddress(_macAddress, term.IspId);

                if (ti.ErrorFlag)
                {
                    Label6.Visible = true;
                    LabelError.Visible = true;
                    LabelError.Text = ti.ErrorMsg;
                }
                else
                {
                    //Read terminal accounting data
                    Terminal terminal = boAccountingControl.GetTerminalDetailsByMAC(_macAddress);
                    //Load ti properties into fields.
                    Label6.Visible = false;
                    LabelError.Visible = false;
                    LabelSiteId.Text = ti.SitId;
                    LabelSiteName.ForeColor = Color.Black;
                    LabelSiteName.Text = ti.EndUserId;
                    LabelMacAddress.Text = ti.MacAddress;

                    TerminalStatus[] termStatusList = boAccountingControl.GetTerminalStatus();
                    LabelAdmStatus.Text = termStatusList[(int)--terminal.AdmStatus].StatusDesc.Trim();

                    if (LabelAdmStatus.Text.Equals("Locked"))
                    {
                        LabelAdmStatus.ForeColor = Color.Red;
                    }
                    else
                    {
                        LabelAdmStatus.ForeColor = Color.Green;
                    }

                    decimal consumedGB = Convert.ToInt64(ti.Consumed) / (decimal)1000000000.0;
                    decimal returnConsumedGB = Convert.ToInt64(ti.ConsumedReturn) / (decimal)1000000000.0;

                    freeZoneFlag = this.IsFreeZone(Int32.Parse(ti.SlaId));
                    if (freeZoneFlag)
                    {
                        decimal freeZoneGB = Convert.ToInt64(ti.FreeZone) / (decimal)1000000000.0;
                        decimal returnFreeZoneGB = Convert.ToInt64(ti.ConsumedFreeZone) / (decimal)1000000000.0;
                        consumedGB = consumedGB - freeZoneGB;
                        returnConsumedGB = returnConsumedGB - returnFreeZoneGB;

                        LabelFreeZoneVolume.Text = "" + freeZoneGB.ToString("0.000") + " GB";
                        LabelRTNFreeZoneVolume.Text = "" + returnFreeZoneGB.ToString("0.000") + " GB";
                        PanelFreeZone.Visible = true;
                    }
                    else
                    {
                        PanelFreeZone.Visible = false;
                    }

                    LabelFConsumed.Text = "" + consumedGB.ToString("0.000") + " GB";
                    LabelRTNConsumed.Text = "" + returnConsumedGB.ToString("0.000") + " GB";
                    LabelServicePackage.Text = ti.SlaName;
                    decimal FUPThresholdBytes = ti.FUPThreshold * 1000000000.0M;
                    decimal RTNFUPThresholdBytes = ti.RTNFUPThreshold * 1000000000.0M;
                    decimal additionalVolumeForward = Convert.ToDecimal(ti.Additional);
                    decimal additionalVolumeReturn = Convert.ToDecimal(ti.AdditionalReturn);
                    decimal percentConsumed = 0.0M;
                    decimal returnPercentConsumed = 0.0M;

                    if (!freeZoneFlag)
                    {
                        percentConsumed = (Convert.ToDecimal(ti.Consumed) / (FUPThresholdBytes + additionalVolumeForward)) * 100.0M;
                        returnPercentConsumed = (Convert.ToDecimal(ti.ConsumedReturn) / (RTNFUPThresholdBytes + additionalVolumeReturn)) * 100.0M;
                    }
                    else
                    {
                        percentConsumed = ((Convert.ToDecimal(ti.Consumed) - Convert.ToDecimal(ti.FreeZone)) / (FUPThresholdBytes + additionalVolumeForward)) * 100.0M;
                        returnPercentConsumed = ((Convert.ToDecimal(ti.ConsumedReturn) - Convert.ToDecimal(ti.ConsumedFreeZone)) / (RTNFUPThresholdBytes + additionalVolumeForward)) * 100.0M;
                    }
                    
                    LabelVolumeAllocation.Text = Decimal.Round((FUPThresholdBytes + additionalVolumeForward)/1000000000.0M, 2) + " GB";
                    LabelReturnVolumeAllocation.Text = Decimal.Round((RTNFUPThresholdBytes + additionalVolumeReturn)/1000000000.0M, 2) + " GB";
                    LabelFConsumed.Text += " (" + percentConsumed.ToString("#.00") + " %)";
                    LabelRTNConsumed.Text += " (" + returnPercentConsumed.ToString("#.00") + " %)";
                    LabelResetDayOfMOnth.Text = ti.InvoiceDayOfMonth;

                    double rtnNum = Double.Parse(ti.RTN, culture);
                    LabelRTN.Text = rtnNum.ToString("#.0") + " dBHz";

                    LabelRTN.ForeColor = this.CNOColorValidator(rtnNum, Int32.Parse(ti.IspId));
                    LabelStatus.Text = ti.Status;

                    if (ti.Status == "Operational")
                    {
                        LabelStatus.ForeColor = Color.Green;
                    }
                    else
                    {
                        LabelStatus.ForeColor = Color.Red;
                    }

                    if (terminal.ExpiryDate != null)
                    {
                        LabelExpiryDate.Text = terminal.ExpiryDate.ToString();

                        if (terminal.ExpiryDate < DateTime.Now)
                        {
                            LabelExpiryDate.ForeColor = Color.Red;
                        }
                        else
                        {
                            LabelExpiryDate.ForeColor = Color.Green;
                        }
                    }
                    else
                    {
                        LabelExpiryDate.Text = "Not Set";
                        LabelExpiryDate.ForeColor = Color.Orange;
                    }
                }
            }
            else
            {
                LabelSiteName.ForeColor = Color.Red;
                this.ClearPageContent();
                LabelSiteName.Text = "Terminal not found";
            }
        }

        /// <summary>
        /// Clears the page content
        /// </summary>
        public void ClearPageContent()
        {
            LabelAdmStatus.Text = "";
            LabelError.Text = "";
            LabelExpiryDate.Text = "";
            LabelFConsumed.Text = "";
            LabelFreeZoneVolume.Text = "";
            LabelMacAddress.Text = "";
            LabelResetDayOfMOnth.Text = "";
            LabelReturnVolumeAllocation.Text = "";
            LabelRTN.Text = "";
            LabelRTNConsumed.Text = "";
            LabelRTNFreeZoneVolume.Text = "";
            LabelServicePackage.Text = "";
            LabelSiteId.Text = "";
            LabelSiteName.Text = "";
            LabelStatus.Text = "";
            LabelVolumeAllocation.Text = "";
        }

        /// <summary>
        /// Determines the color of the CNO pointing label depending on the 
        /// Return number value and the ISP.
        /// </summary>
        /// <remarks>
        /// The return value levels which determine the label color depend on the
        /// ISP. Later we should thing on a more flexible and "plugable" strategy to
        /// implement this feature.
        /// </remarks>
        /// <param name="rtnNum">The return number value</param>
        /// <param name="ispId">The ISP identifier</param>
        /// <returns>A Color value depending on the given rtnNum value and isp</returns>
        protected Color CNOColorValidator(double rtnNum, int ispId)
        {
            Color labelColor;

            if (ispId == 112)
            {
                if (rtnNum < 55.2)
                {
                    labelColor = Color.Red;
                }
                else if (rtnNum >= 55.2 && rtnNum < 58.4)
                {
                    labelColor = Color.DarkOrange;
                }
                else
                {
                    labelColor = Color.DarkGreen;
                }
                LabelRTN.ToolTip = "Above 58.4 dBHz, the terminal uses the most efficient return pool. " +
                    "Between 55.2 dBHz and 58.4 dBHz, the terminal must use a less efficient return pool. " +
                    "Below 55.2 dBHz the signal is too weak.";
            }
            else if (ispId == 412)
            {
                if (rtnNum < 52.7)
                {
                    labelColor = Color.Red;
                }
                else if (rtnNum >= 52.7 && rtnNum < 55.8)
                {
                    labelColor = Color.DarkOrange;
                }
                else
                {
                    labelColor = Color.DarkGreen;
                }
                LabelRTN.ToolTip = "Above 55.8 dBHz, the terminal uses the most efficient return pool. " +
                    "Between 52.7 dBHz and 55.8 dBHz, the terminal must use a less efficient return pool. " +
                    "Below 52.7 dBHz the signal is too weak.";
            }
            else
            {
                //General case
                if (rtnNum < 55.2)
                {
                    labelColor = Color.Red;
                }
                else if (rtnNum >= 55.2 && rtnNum < 58.4)
                {
                    labelColor = Color.DarkOrange;
                }
                else
                {
                    labelColor = Color.DarkGreen;
                }
            }


            return labelColor;
        }
    
    }
}