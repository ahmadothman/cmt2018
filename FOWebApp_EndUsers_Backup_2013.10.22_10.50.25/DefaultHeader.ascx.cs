﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using FOWebApp_EndUsers.net.nimera.cmt.BOAccountingControlWSRef;

namespace FOWebApp_EndUsers
{
    public partial class DefaultHeader : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BOAccountingControlWS boAccountingControlWS = new BOAccountingControlWS();

            //Set the SitId necessary for loading the terminal details
            MembershipUser myObject = Membership.GetUser();

            //Check if logged-on
            if (myObject != null)
            {
                HyperLinkLogOff.Visible = true;
                string userId = myObject.ProviderUserKey.ToString();
                LabelName.Visible = true;
                LabelName.Text = "You are logged on as  " + myObject.UserName;
            }
            else
            {
                LabelName.Visible = false;
                HyperLinkLogOff.Visible = false;
            }

        }
    }
}