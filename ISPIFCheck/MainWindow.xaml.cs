﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net;
using System.IO;
using ISPIFCheck.com.astra2connect.ispif;

namespace ISPIFCheck
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string _configFileName = "Config.txt";
        string _appFolderPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Nimera\\ISPIFChecker";
        private IspSupportInterfaceService _ispService = null;
        FileInfo _fi;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!Directory.Exists(_appFolderPath))
            {
                Directory.CreateDirectory(_appFolderPath);
            }

            _configFileName = _appFolderPath + "\\" + _configFileName;

            _ispService = new IspSupportInterfaceService();
            ServicePointManager.Expect100Continue = false;
            _fi = new FileInfo(_configFileName);
            if (_fi.Exists)
            {
                StreamReader reader = new StreamReader(_configFileName);
                textBoxUserName.Text = reader.ReadLine();
                textBoxPassword.Text = reader.ReadLine();
                reader.Close();
            }

        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            if (textBoxMacAddress.Text.Equals(""))
            {
                MessageBox.Show("Please enter a MAC address", "Missing input", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }

            if (textBoxUserName.Text.Equals(""))
            {
                MessageBox.Show("Please enter a user name", "Missing input", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }

            if (textBoxPassword.Text.Equals(""))
            {
                MessageBox.Show("Please enter a password", "Missing input", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }

            string macAddress = textBoxMacAddress.Text;
            StreamWriter writer = new StreamWriter(_configFileName, false);
            writer.WriteLine(textBoxUserName.Text);
            writer.WriteLine(textBoxPassword.Text);
            writer.Close();

            //Clean form
            textBoxIP.Text = "";
            textBoxSLAName.Text = "";
            textBoxName.Text = "";
            textBoxStatus.Text = "";
            textBlockError.Text = "";
            textBoxSitId.Text = "";
            TextBoxVolumeLeft.Text = "";
            textBoxNewSLA.Text = "";

            try
            {
                if (!macAddress.Equals(""))
                {
                    _ispService.Credentials = new NetworkCredential(textBoxUserName.Text, textBoxPassword.Text);
                    _ispService.PreAuthenticate = true;
                    NbiTerminalInfo termInfo = _ispService.getNbiInfoByMac(macAddress);
                    Registration reg = _ispService.lookupRegistrationByEndUserId(termInfo.sitName, false, termInfo.nbiIspId);
                    try
                    {
                        VolumeInfo vi = _ispService.getVolumeInfoBySit(termInfo.sitId, termInfo.ispifIspId);
                        double volumeConsumed = Double.Parse(vi.consumed) + Double.Parse(vi.consumedReturn);
                        double freeZoneConsumed = Double.Parse(vi.freezone) + Double.Parse(vi.freezoneReturn);
                        double additionalVolume = Double.Parse(vi.additional) + Double.Parse(vi.additionalReturn);
                        double volumeLeft = 1073741824.0 - (volumeConsumed - freeZoneConsumed - additionalVolume);
                        TextBoxVolumeLeft.Text = volumeLeft.ToString();
                    }
                    catch (Exception ex2)
                    {
                        textBlockError.Text = "Could not retrieve volume information ("  + ex2.Message + ")";
                    }

                    textBoxIP.Text = termInfo.ipAddress;
                    textBoxSLAName.Text = reg.slaId;
                    textBoxSitId.Text = termInfo.sitId;
                    textBoxStatus.Text = reg.status.Value.ToString();
                    textBoxName.Text = termInfo.sitName;

                    if (!reg.status.Value.ToString().Equals("Decommissioned"))
                    {
                        buttonDecommission.IsEnabled = true;
                    }
                    else
                    {
                        buttonDecommission.IsEnabled = false;
                    }

                    if (reg.status.Value.ToString().Equals("OPERATIONAL"))
                    {
                        buttonManage.IsEnabled = true;
                        buttonManage.Content = "Suspend";
                    }

                    if (reg.status.Value.ToString().Equals("LOCKED"))
                    {
                        buttonManage.IsEnabled = true;
                        buttonManage.Content = "Re-Activate";
                    }

                    buttonChangeSLA.IsEnabled = true;
                    buttonChangeSLA.Focus();

                }
            }
            catch (Exception ex)
            {
                textBlockError.Text = ex.Message;
            }
        }

        private void buttonDecommission_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Are you sure you want to decommission this terminal?", "Take care",
                MessageBoxButton.OKCancel);


            if (result == MessageBoxResult.OK)
            {
                //Decommission the terminal
                //Set authentication credentials and WS settings
                _ispService.Credentials = new NetworkCredential(textBoxUserName.Text, textBoxPassword.Text);
                _ispService.PreAuthenticate = true;
                ServicePointManager.Expect100Continue = false;

                try
                {
                    RequestTicket rt = _ispService.removeRegistration(textBoxName.Text, textBoxMacAddress.Text);
                    MessageBox.Show(rt.requestStatus.Value.ToString() + " " + rt.failureReason, "Result");
                }
                catch (Exception ex)
                {
                    textBlockError.Text = ex.Message;
                }
            }
        }

        private void buttonManage_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Are you sure you want to " + buttonManage.Content + " this terminal?", "Take care",
               MessageBoxButton.OKCancel);

            if (result == MessageBoxResult.OK)
            {
                if (buttonManage.Content.Equals("Suspend"))
                {
                    //Suspend the terminal
                    this.SuspendTerminal();
                }

                if (buttonManage.Content.Equals("Re-Activate"))
                {
                    //Re-activate the terminal
                    this.ReActivateTerminal();
                }
            }
        }

        /// <summary>
        /// Suspends a terminal
        /// </summary>
        private void SuspendTerminal()
        {
            _ispService.Credentials = new NetworkCredential(textBoxUserName.Text, textBoxPassword.Text);
            _ispService.PreAuthenticate = true;
            ServicePointManager.Expect100Continue = false;

            try
            {
                NbiTerminalInfo termInfo = _ispService.getNbiInfoByMac(textBoxMacAddress.Text);
                RequestTicket rt = _ispService.changeStatus(textBoxName.Text, RegistrationStatus.LOCKED, termInfo.nbiIspId);
                MessageBox.Show(rt.requestStatus.Value.ToString() + " " + rt.failureReason, "Result");    
            }
            catch (Exception ex)
            {
                textBlockError.Text = ex.Message;
            }
        }

        /// <summary>
        /// Reactivates a terminal
        /// </summary>
        private void ReActivateTerminal()
        {
            _ispService.Credentials = new NetworkCredential(textBoxUserName.Text, textBoxPassword.Text);
            _ispService.PreAuthenticate = true;
            ServicePointManager.Expect100Continue = false;

            try
            {
                NbiTerminalInfo termInfo = _ispService.getNbiInfoByMac(textBoxMacAddress.Text);
                RequestTicket rt = _ispService.changeStatus(textBoxName.Text, RegistrationStatus.OPERATIONAL, termInfo.nbiIspId);
                MessageBox.Show(rt.requestStatus.Value.ToString() + " " + rt.failureReason, "Result");
            }
            catch (Exception ex)
            {
                textBlockError.Text = ex.Message;
            }
        }

        private void buttonDumpSLA_Click(object sender, RoutedEventArgs e)
        {

        }

        private void buttonChangeSLA_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Are you sure you want to change the SLA of this terminal?", "Take care",
               MessageBoxButton.OKCancel);

            if (result == MessageBoxResult.OK)
            {
                //Set authentication credentials and WS settings
                _ispService.Credentials = new NetworkCredential(textBoxUserName.Text, textBoxPassword.Text);
                _ispService.PreAuthenticate = true;

                try
                {
                    ServicePointManager.Expect100Continue = false;
                    NbiTerminalInfo termInfo = _ispService.getNbiInfoByMac(textBoxMacAddress.Text);
                    RequestTicket rt = _ispService.changeSla(textBoxName.Text, textBoxNewSLA.Text, termInfo.nbiIspId);
                    MessageBox.Show(rt.requestStatus.Value.ToString() + " " + rt.failureReason, "Result");

                }
                catch (Exception ex)
                {
                    textBlockError.Text = ex.Message;
                }
            }
        }
    }
}
