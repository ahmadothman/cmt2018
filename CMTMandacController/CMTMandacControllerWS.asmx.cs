﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using CMTMandacController.BOMandacControlWSRef;

namespace CMTMandacController
{
    /// <summary>
    /// Webservice for the CMTMandacControllerWS which is available to third parties
    /// </summary>
    [WebService(Namespace = "http://cmt.nimera.net/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class CMTMandacControllerWS : System.Web.Services.WebService
    {
        BOMandacControlWS _controller;

        public CMTMandacControllerWS()
        {
            _controller = new BOMandacControlWS();
        }

        /// <summary>
        /// Provides an estimation of the remaining volume for Mandac hotspot connected to a terminal.
        /// This estimation is an indication to the terminal owner if more
        /// hotspot vouchers can be issued or not.
        /// This method will be interpreted by the Mandac system
        /// </summary>
        /// <param name="mandacId">The ID of the Mandac hotspot</param>
        /// <returns>The volume estimation in GB</returns>
        [WebMethod]
        public double GetEstimatedRemainingVolumeForHotspot(string mandacId)
        {
            return _controller.GetEstimatedRemainingVolumeForHotspot(mandacId);
        }

        /// <summary>
        /// Provides the current remaining volume of a terminal in absolute numbersuj
        /// </summary>
        /// <param name="mandacId">The ID of the Mandac hotspot</param>
        /// <returns>The volume in GB</returns>
        [WebMethod]
        public double GetRemainingVolumeForTerminal(string mandacId)
        {
            return _controller.GetRemainingVolumeForTerminal(mandacId);
        }
    }
}
