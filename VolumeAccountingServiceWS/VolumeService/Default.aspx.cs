﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VolumeAccountingService.Models;

namespace VolumeService
{
    public partial class Default : System.Web.UI.Page
    {
        private IVolumeDatabaseService _service = new VolumeDatabaseService();

        protected void Page_Load(object sender, EventArgs e)
        {
            int ispId = 112; // default ispid
            if (Request.Params["ispid"] != null)
            {
                ispId = Int32.Parse(Request.Params["ispid"]);
            }
            
            if (Request.Params["q"] != null)
            {
                string type = Request.Params["q"].ToLowerInvariant();

                if (type == "fwd")
                {
                    var results = _service.GetTopNResults("fwd", 20);
                    DumpOutput(results);
                }
                else if (type == "rtn")
                {
                    var results = _service.GetTopNResults("rtn", 20);
                    DumpOutput(results);
                }
            }
        }

        private void DumpOutput(IList<IVolumeReport> results)
        {
            if (results == null || results.Count == 0)
                Response.Write("No output could be returned.");

            Response.ContentType = "text/csv";
            Response.AddHeader("Content-Disposition", "attachment; filename=results.csv");
            Response.Write(String.Join(";",
                "sitid",
                "fwd_allowed_mb",
                "fwd_current_mb",
                "fwd_traffic_left_mb",
                "fwd_usage_percentage",
                "rtn_allowed_mb",
                "rtn_current_mb",
                "rtn_traffic_left_mb",
                "rtn_usage_percentage"
                ));
            Response.Write(Environment.NewLine);
            Response.Write(String.Join(Environment.NewLine, results.Select(t => 
                String.Join(";", 
                t.sitid,
                t.fwd_allowed_mb,
                t.fwd_current_mb,
                t.fwd_traffic_left_mb,
                t.fwd_usage_percentage,
                t.rtn_allowed_mb,
                t.rtn_current_mb,
                t.rtn_traffic_left_mb,
                t.rtn_usage_percentage
                ))));
        }
    }
}