﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LoadISPDistributors.BOAccountingControlWSRef;

namespace LoadISPDistributors
{
    class Program
    {
        BOAccountingControlWS _boAccountingControlWS = new BOAccountingControlWS();

        static void Main(string[] args)
        {
            Program prog = new Program();
            prog.mainBody();
        }

        private void mainBody()
        {
            Distributor[] distributors = _boAccountingControlWS.GetDistributors();

            foreach (Distributor dist in distributors)
            {
                _boAccountingControlWS.AddISPToDistributor((int)dist.Id, 112);
                _boAccountingControlWS.AddISPToDistributor((int)dist.Id, 412);

            }
        }
    }
}
