﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BOControllerLibrary.NMSGatewayServiceRef;
using System.Web.Services;

namespace NMSGatewayUnitTest
{
    [TestClass]
    public class UnitTest1
    {
        NMSGatewayService nms = new NMSGatewayService();
        
        [TestMethod]
        public void CreateServicePack()
        {
            ServicePack servicePack = new ServicePack();

            servicePack.SlaId = 4;
            servicePack.SlaName = "SLA Test ";
            servicePack.FupThreshold = 5000000;
            servicePack.DrAboveFup = 100000;
            servicePack.SlaCommonName = servicePack.SlaName;
            servicePack.IspId = 469;
            servicePack.Business = false;
            servicePack.FreeZone = false;
            servicePack.Voucher = false;
            servicePack.RTNFUPThreshold = 1000000;
            servicePack.ServicePackAmt = 20;
            servicePack.FreeZoneAmt = 0;
            servicePack.VoIPAmt = 0;
            servicePack.VoIPFlag = false;
            servicePack.Hide = false;
            servicePack.DRAboveFupFWD = 10000;
            servicePack.DRFWD = 3000000;
            servicePack.DRRTN = 200000;
            servicePack.CIRFWD = 2000000;
            servicePack.CIRRTN = 100000;
            
            bool result = nms.CreateServicePack(servicePack);
        }

        [TestMethod]
        public void UpdateServicePack()
        {
            ServicePack servicePack = new ServicePack();

            servicePack.SlaId = 3;
            servicePack.SlaName = "SLA UPDATED";
            servicePack.FupThreshold = 0;
            servicePack.DrAboveFup = 0;
            servicePack.SlaCommonName = servicePack.SlaName;
            servicePack.IspId = 469;
            servicePack.Business = false;
            servicePack.FreeZone = false;
            servicePack.Voucher = false;
            servicePack.RTNFUPThreshold = 0;
            servicePack.ServicePackAmt = 99;
            servicePack.FreeZoneAmt = 0;
            servicePack.VoIPAmt = 0;
            servicePack.VoIPFlag = false;
            servicePack.Hide = false;
            servicePack.DRAboveFupFWD = 0;
            servicePack.DRFWD = 0;
            servicePack.DRRTN = 0;
            servicePack.CIRFWD = 0;
            servicePack.CIRRTN = 0;

            bool result = nms.UpdateServicePack(servicePack);
        }

        [TestMethod]
        public void DeleteServicePack()
        {
            ServicePack servicePack = new ServicePack();

            servicePack.SlaId = 2;
            servicePack.SlaName = "SLA UPDATED";
            servicePack.FupThreshold = 0;
            servicePack.DrAboveFup = 0;
            servicePack.SlaCommonName = servicePack.SlaName;
            servicePack.IspId = 469;
            servicePack.Business = false;
            servicePack.FreeZone = false;
            servicePack.Voucher = false;
            servicePack.RTNFUPThreshold = 0;
            servicePack.ServicePackAmt = 99;
            servicePack.FreeZoneAmt = 0;
            servicePack.VoIPAmt = 0;
            servicePack.VoIPFlag = false;
            servicePack.Hide = false;
            servicePack.DRAboveFupFWD = 0;
            servicePack.DRFWD = 0;
            servicePack.DRRTN = 0;
            servicePack.CIRFWD = 0;
            servicePack.CIRRTN = 0;

            bool result = nms.DeleteServicePack(servicePack);
        }
    }
}
