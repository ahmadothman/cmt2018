﻿using System;
using IOAccountingService.Model;
using IOAccountingService.Properties;
using PetaPoco;
using log4net;

namespace IOAccountingService
{
    internal static class Program
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof (Program));

        /// <summary>
        ///   The main entry point for the application.
        /// </summary>
        private static void Main(params string[] args)
        {
            // 112 - hourly
            //args = new[]
            //           {
            //               "https://sap3.astra2connect.com/rt/ACC-P1M/ACC-P1D-{1}-{0:yyyyMM}.csv", 
            //                
            //               "112", 
            //               "201209172030",
            //               "true", 
            //               "-240", 
            //               "VolumeHistoryImport", 
            //               "cmtImportVolumeHistory2", 
            //               "Sea_Space", 
            //               "neliande"
            //           };

            // 112 - detail
            //args = new[]
            //           {
            //               "https://sap3.astra2connect.com/rt/ACC-PT30M/ACC-PT30M-{1}-{0:yyyyMMddHHmm}.csv", 
            //               "112", 
            //               "201209172030",
            //               "true", 
            //               "-240", 
            //               "VolumeHistoryDetailImport", 
            //               "cmtImportVolumeHistoryDetail2", 
            //               "Sea_Space", 
            //               "neliande"
            //           };
            

            // 412 - full
            //args = new[]
            //           {
            //               "https://sap4.astra2connect.com/rt/ACC-P1M/ACC-P1D-{1}-{0:yyyyMM}.csv", 
            //               "412", 
            //               "201209172030",
            //               "true", 
            //               "-240", 
            //               "VolumeHistoryImport", 
            //               "cmtImportVolumeHistory2", 
            //               "SatADSL", 
            //               "tenissix"
            //           };

            // 412 - detail - does not work
            //args = new[]
            //           {
            //               "https://sap4.astra2connect.com/rt/ACC-PT30M/ACC-PT30M-{1}-{0:yyyyMMddHHmm}.csv", 
            //               "412", 
            //               "201209172030",
            //               "true", 
            //               "-240", 
            //               "VolumeHistoryDetailImport", 
            //               "cmtImportVolumeHistoryDetail2", 
            //               "SatADSL", 
            //               "tenissix"
            //           };


            // 150 foutieve record count.. FULL
            //args = new[]
            //           {
            //               "https://sap3.astra2connect.com/rt/PLR/PLR-{1}-{0:yyyyMM}.csv",
            //               "150", 
            //               "201209172030",
            //               "true", 
            //               "-240", 
            //               "VolumeHistoryImport", 
            //               "cmtImportVolumeHistory2", 
            //               "Sea_Space2", 
            //               "v3JWGFfF"
            //           };

            // 150 foutieve record count.. DETAIL
            args = new[]
                       {
                           "https://sap3.astra2connect.com/rt/PLR/PLR-{1}-{0:yyyyMMddHHmm}.csv",
                           "150", 
                           "201209172030",
                           "true", 
                           "-240", 
                           "VolumeHistoryDetailImport", 
                           "cmtImportVolumeHistoryDetail2", 
                           "Sea_Space2", 
                           "v3JWGFfF"
                       };

            //#if DEBUG
            Log.Info("Program started with : " + String.Join(" ", args));

            if (args.Length == 0)
            {
                Console.WriteLine("\nParameters:\n");
                Console.WriteLine(
                    "0: URL \n\tURL that contains timestring: \n\teg https://sap3.astra2connect.com/rt/ACC-P1M/ACC-P1D-112-{0:yyyyMM}.csv \n");
                Console.WriteLine("1: IspId \n\tThe IspId that we'll be using.");
                Console.WriteLine("2: DateTime \n\tDateTime we use to process. Usually the current time.\n");
                Console.WriteLine("3: Truncate \n\tTruncate the import table or not? True will truncate\n");
                Console.WriteLine(
                    "4: TimeOffset \n\tTimeOffset in minutes. Can be both positive or negative. \n\tThis will be applied to the given datetime (arg 1). Usual values are -240 or -120\n");
                Console.WriteLine("5: TableName \n\tThe name of the table to import (and optionally truncate) to\n");
                Console.WriteLine(
                    "6: StoredProc\n\tThe stored procedure name that we'll execute after import. \n\tUsually to move the data from import to the destination table. And to clear some data there.\n");
                Console.WriteLine("7: StoredProc\n\tThe username to connect to the service with\n");
                Console.WriteLine("8: StoredProc\n\tThe password\n");

                return;
            }

            // args:
            // 0 : url with timeframe embedded
            // 1 : ispid
            // 2 : current datetime in yyyyMMddHHmm
            // 3 : bool truncate
            // 4 : offset, in minutes (positive or negative)
            // 5 : name of table to import the data to
            // 6 : name of an optional stored procedure to run after import.
            var dateTime = DateTime.ParseExact(args[2], "yyyyMMddHHmm", null).AddMinutes(int.Parse(args[4]));
            var ispid = Int32.Parse(args[1]);
            var truncate = bool.Parse(args[3]);
            var url = string.Format(args[0], dateTime, ispid);
            string username = args[7];
            string password = args[8];
            //var url = HttpHelper.BuildTimeString(args[0], dateTime);
            var tableName = args[5];

            Log.Debug("using url : " + url);

            var cs = new IOAccounting()
                         {
                             UserName = username,
                             Password = password
                         };

            if (url.ToLower().StartsWith("http"))
                cs.ParseCSvFromUrl(url, tableName, truncate, ispid);
            else
            {
                cs.ParseCsvFromFile(url, tableName, truncate, ispid);
            }

            // execute this post stored procedure
            // add a parameter to the procedure, the ispid
            using (var db = new Database(Settings.Default.Database, Settings.Default.DatabaseProvider))
            {
                db.Execute(string.Format("exec [{0}] {1}", args[6], ispid));
            }
        }


    }
}
