﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IOAccountingService.Model
{
    public enum ImportMode
    {
        /// <summary>
        /// Fetches the full month
        /// </summary>
        Month = 0,
        /// <summary>
        /// Fetches in periods of half hours
        /// </summary>
        Halfhour = 1
    }
}
