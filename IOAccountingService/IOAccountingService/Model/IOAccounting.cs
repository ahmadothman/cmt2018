﻿using System;
using System.IO;
using System.Text;
using IOAccountingService.Properties;
using log4net;

namespace IOAccountingService.Model
{
    public class IOAccounting
    {
        public string UserName { get; set;  }
        public string Password { get; set; }

        private static readonly ILog Log = LogManager.GetLogger(typeof(IOAccounting));

        public void ParseCSvFromUrl(string url, string tableName, bool truncate, int ispId)
        {
            Log.Debug("Parse from url");
            var helper = new HttpHelper();
            var arr = helper.Fetch(url, UserName, Password);

            if (arr!=null)
                ParseCsvFromByteArray(arr, tableName, truncate, ispId);
            else 
                Log.Info("No data was retrieved, so nothing is going to be imported");
        }

        /// <summary>
        ///   Parse the data from a file on disk
        /// </summary>
        /// <param name = "filename"></param>
        public void ParseCsvFromFile(string filename, string tableName, bool truncate, int ispId)
        {
            Log.Debug("Parse from file");
            using (var stream = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                ParseCsvFromStream2(stream, tableName, truncate, ispId);
            }
        }

        /// <summary>
        ///   Parse the csv file from a UTF-8 encoded byte array
        /// </summary>
        /// <param name = "csvData"></param>
        public void ParseCsvFromByteArray(byte[] csvData, string tableName, bool truncate, int ispId)
        {
            Log.Debug("Parse from byte array");
            using (var stream = new MemoryStream(csvData))
            {
                ParseCsvFromStream2(stream, tableName, truncate, ispId);
            }
        }

        public void ParseCsvFromStream2 (Stream stream, string tableName, bool truncate, int ispId)
        {
            Log.Debug("Processing the stream");
            char delimiter = Settings.Default.CsvInputDelimiter;
            using (var reader = new StreamReader(stream, Encoding.UTF8))
            {
                // file description header: nbr_records,ICD_version,file_tag,time_stamp,time_interval
                var fileHeader = reader.ReadLine().Split(delimiter);

                // eg 12670,0001,ACC,201201010000,1440
                var fileHeaderData = reader.ReadLine().Split(delimiter);

                // data header (the real csv column headers)
                var dataHeader = reader.ReadLine().Split(delimiter);

                // code to import the ispid, but we dont need it as we'll just pass the ispid to the procedure ;).
                //var newDataHeader = new string[dataHeader.Length + 1];
                //Array.Copy(dataHeader, 0, newDataHeader, 1, dataHeader.Length);
                //newDataHeader[0] = "IspId";

                // data header suffix, containing the data types
                var dataHeaderTypes = reader.ReadLine().Split(delimiter);

                // get the datatable we need
                var table = BulkImportHelper.CreateDataTable(tableName, dataHeader);
                //var table = BulkImportHelper.CreateDataTable(tableName, newDataHeader);

                // now we can finally start parsing the file itself
                while (!reader.EndOfStream)
                {
                    var row = table.NewRow();
                    var arr = reader.ReadLine().Split(delimiter);

                    for (int i = 0; i < arr.Length; i++)
                    {
                        row[i] = arr[i];
                    }

                    table.Rows.Add(row);
                }
                BulkImportHelper.WriteToDataBase(Settings.Default.Database, truncate, true, table);
            } 
        }


    }
}
