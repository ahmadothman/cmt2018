﻿using System;
using System.Net;
using System.Text;
using System.Threading;
using IOAccountingService.Properties;
using log4net;

namespace IOAccountingService.Model
{
    public class HttpHelper
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof (HttpHelper));

        public byte[] Fetch(string url, string username, string password)
        {
            int trials = 0;
            while (trials < Settings.Default.NumberOfRetries)
            {
                try
                {
                    Log.Debug("Going for trial run: " + trials);
                    using (var webClient = new WebClient())
                    {
                        webClient.Credentials = new NetworkCredential(username, password);
                        ServicePointManager.Expect100Continue = false; //Necessary to work around a Tomcat bug
                        ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                        var data = webClient.DownloadData(url);
                        return data;
                    }
                }
                catch (Exception ex)
                {
                    Log.Warn("Failed to download", ex);
                }
                Thread.Sleep(2000);
                trials++;
            }

            Log.Error("Giving up, didn't manage to download the file at url " + url);
            return null;
        }


        /// <summary>
        /// Builds a filename for the given parameters
        /// </summary>
        /// <param name="fixedPart">ACC-PT30M-112-   : (mind the last dash). 112 is the isp parameter here</param>
        /// <param name="time">A datetime representing a half hour part (eg 11:00, 11:30, 12:00 and so on</param>
        /// <returns></returns>
        public string BuildFileName(string fixedPart, DateTime time)
        {
            /*
             * b.	File name: ACC-PT30M-112-201201101000.csv where
                i.	ACC-PT30M-112- is a fixed part (112 is the ISP Id)
                ii.	20120110 is the Date (YYYYMMDD)
                iii.	1000 is the hour (10:00), so the next hour will be 1030, next 1100 etc.
             */
            if (time.Minute == 30 || time.Minute == 00)
                return fixedPart + time.ToString("yyyyMMddHHmm");
            throw new Exception("Invalid time frame given. Must be .30 or .00 minutes.");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fixedPart"></param>
        /// <param name="time"></param>
        /// <returns></returns>
        public string BuildAccumulatedFileName(string fixedPart, DateTime time)
        {
            /*
             * c.	File name: ACC-P1D-112-201201.csv
                i.	ACC-P1D-112- is a fixed part (112 is the ISP Id)
                ii.	201201 is the Year and Month YYYYMM.
             */
            return fixedPart + time.ToString("yyyyMM") + ".csv";
        }

        public static string BuildMonthString (string url, DateTime chosenDate)
        {
            return string.Format(url, chosenDate.ToString("yyyyMM"));
        }
        public static string BuildHourMinuteString (string url, DateTime chosenDate)
        {
            return string.Format(url, chosenDate.ToString("yyyyMMddHHmm"));
        }

        //public static DateTime BuildTimeString(string url, DateTime chosenDate)
        //{
        //    return string.Format(url, chosenDate);
        //}
    }
}
