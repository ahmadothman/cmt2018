﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SLMBookingManager.BOSatLinkManagerControlWSRef;

namespace SLMBookingManager
{
    class Program
    {
        BOSatLinkManagerControlWS _slm;

        static void Main(string[] args)
        {
            Program prg = new Program();
            prg.ManageBookings();
        }

        void ManageBookings()
        {//When starting this service in business make sure that the timestamp is set to UTC time and all customers follow the same
            DateTime timeStamp = DateTime.UtcNow;
            DateTime bookingTime = new DateTime(timeStamp.Year, timeStamp.Month, timeStamp.Day, timeStamp.Hour, timeStamp.Minute, 0);
            //bookingTime = new DateTime(timeStamp.Year, timeStamp.Month, 4, 13, 30, 0);
            _slm = new BOSatLinkManagerControlWS();
            this.StartBookings(bookingTime);
            this.EndBookings(bookingTime);
        }

        void StartBookings(DateTime startTime)
        {
            SLMBooking[] bookings = _slm.GetBookingsByStartTime(startTime);

            foreach (SLMBooking b in bookings)
            {
                if (b.Status == "Booked")
                {
                    _slm.StartBooking(b);
                }
            }

            //SLMBooking book = _slm.GetSLMBookingById(11);
            //_slm.StartBooking(book);
        }

        void EndBookings(DateTime endTime)
        {
            SLMBooking[] bookings = _slm.GetSLMBookingsByStatus("Started");

            foreach (SLMBooking b in bookings)
            {
                if (b.EndTime == endTime)
                {
                    _slm.StopBooking(b);
                }
            }

            //SLMBooking book = _slm.GetSLMBookingById(11);
            //_slm.StopBooking(book);
        }
    }
}
