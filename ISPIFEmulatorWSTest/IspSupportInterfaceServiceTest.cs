﻿using net.nimera.cmt.ISPIFEmulator;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;

namespace ISPIFEmulatorWSTest
{


    /// <summary>
    ///This is a test class for IspSupportInterfaceServiceTest and is intended
    ///to contain all IspSupportInterfaceServiceTest Unit Tests
    ///</summary>
    [TestClass()]
    public class IspSupportInterfaceServiceTest
    {
        //Test data
        string testUserId = "ABBS test 1";
        string testMacAddress = "00:06:39:83:1c:d9";
        int testSlaId = 1120;
        int testSitId = 999999;

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for createRegistration
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Projects\\SatFinAfrica\\CustomerManagementTool\\ISPIFEmulator", "/")]
        [UrlToTest("http://localhost:52995/")]
        public void createRegistrationTest()
        {
            //System.Diagnostics.Debugger.Break();
            DataGateway dg = new DataGateway();
            //Assert.IsTrue(dg.insertTerminal(testMacAddress, RegistrationStatus.LOCKED), "Could not insert test terminal");

            IspSupportInterfaceService target = new IspSupportInterfaceService();
            string endUserId = testUserId;
            string macAddress = testMacAddress;
            long slaId = testSlaId;
            RequestTicket rt = target.createRegistration(endUserId, macAddress, slaId);
            Assert.IsTrue(rt.requestStatus == RequestStatus.BUSY, "Actual status of ticket = " + rt.requestStatus);

            //Remove test user
            Assert.IsTrue(dg.deleteUser(testUserId), "Could not delete test user");

            //Clean-up tickets
            Assert.IsTrue(dg.cleanUpTickets(), "Could not clean up the Tickets");

            //Remove test terminal
            Assert.IsTrue(dg.deleteTerminal(testMacAddress), "Could not delete test terminal");
        }

        /// <summary>
        ///A test for changeSla
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Projects\\SatFinAfrica\\CustomerManagementTool\\ISPIFEmulator", "/")]
        [UrlToTest("http://localhost:52995/")]
        public void changeSlaTest()
        {
            //System.Diagnostics.Debugger.Break();
            int newSla = 3001;

            IspSupportInterfaceService target = new IspSupportInterfaceService();

            //Prepare terminals and users
            DataGateway dg = new DataGateway();
            Assert.IsTrue(dg.insertTerminal(testMacAddress, RegistrationStatus.LOCKED), "Could not insert test terminal");
            Assert.IsTrue(dg.createUser(testUserId, 112), "Could not create user");

            //Create a registration
            target.createRegistration(testUserId, testMacAddress, testSlaId);

            //Complete the registration
            dg.completeRegistrations();

            RequestTicket rt = target.changeSla(testUserId, newSla);
            Assert.IsTrue(rt.requestStatus == RequestStatus.BUSY, "Could not change Sla");

            //Remove test user
            Assert.IsTrue(dg.deleteUser(testUserId), "Could not delete test user");

            //Clean-up tickets
            Assert.IsTrue(dg.cleanUpTickets(), "Could not clean up the Tickets");

            //Remove test terminal
            Assert.IsTrue(dg.deleteTerminal(testMacAddress), "Could not delete test terminal");
        }

        /// <summary>
        ///A test for changeStatus
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Projects\\SatFinAfrica\\CustomerManagementTool\\ISPIFEmulator", "/")]
        [UrlToTest("http://localhost:52995/")]
        public void changeStatusTest()
        {
            //System.Diagnostics.Debugger.Break();
            RegistrationStatus newRegStatus = RegistrationStatus.OPERATIONAL;

            IspSupportInterfaceService target = new IspSupportInterfaceService();

            //Prepare terminals and users
            DataGateway dg = new DataGateway();
            Assert.IsTrue(dg.insertTerminal(testMacAddress, RegistrationStatus.LOCKED), "Could not insert test terminal");
            Assert.IsTrue(dg.createUser(testUserId, 112), "Could not create user");

            //Create a registration
            target.createRegistration(testUserId, testMacAddress, testSlaId);

            //Complete the registration
            dg.completeRegistrations();

            RequestTicket rt = target.changeStatus(testUserId, newRegStatus);
            Assert.IsTrue(rt.requestStatus == RequestStatus.BUSY, "Could not change Status");

            //Remove test user
            Assert.IsTrue(dg.deleteUser(testUserId), "Could not delete test user");

            //Clean-up tickets
            Assert.IsTrue(dg.cleanUpTickets(), "Could not clean up the Tickets");

            //Remove test terminal
            Assert.IsTrue(dg.deleteTerminal(testMacAddress), "Could not delete test terminal");
        }

        /// <summary>
        ///A test for changeTerminal
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Projects\\SatFinAfrica\\CustomerManagementTool\\ISPIFEmulator", "/")]
        [UrlToTest("http://localhost:52995/")]
        public void changeTerminalTest()
        {
            //System.Diagnostics.Debugger.Break();
            IspSupportInterfaceService target = new IspSupportInterfaceService();
            string testMacAddress2 = "00:23:56:69:88";

            //Prepare terminals and users
            DataGateway dg = new DataGateway();
            Assert.IsTrue(dg.insertTerminal(testMacAddress2, RegistrationStatus.LOCKED), "Could not insert test terminal 2");
            Assert.IsTrue(dg.insertTerminal(testMacAddress, RegistrationStatus.LOCKED), "Could not insert test terminal 1");
            Assert.IsTrue(dg.createUser(testUserId, 112), "Could not create user");

            string endUserId = testUserId;
            string macAddress = testMacAddress2;

            //Register user for first terminal
            target.createRegistration(testUserId, testMacAddress, testSlaId);

            //Complete registrations as we cannot change a terminal registration for an ongoing registration
            dg.completeRegistrations();

            RequestTicket rt = target.changeTerminal(endUserId, macAddress);
            Assert.IsTrue(rt.requestStatus == RequestStatus.BUSY, "Actual status of ticket = " + rt.requestStatus);

            //Remove test user
            Assert.IsTrue(dg.deleteUser(testUserId), "Could not delete test user");

            //Clean-up tickets
            Assert.IsTrue(dg.cleanUpTickets(), "Could not clean up the Tickets");

            //Remove test terminal
            Assert.IsTrue(dg.deleteTerminal(testMacAddress), "Could not delete test terminal");

            //Remove test terminal2
            Assert.IsTrue(dg.deleteTerminal(testMacAddress2), "Could not delete test terminal");
        }

        /// <summary>
        ///A test for lookupRequestTicket
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Projects\\SatFinAfrica\\CustomerManagementTool\\ISPIFEmulator", "/")]
        [UrlToTest("http://localhost:52995/")]
        public void lookupRequestTicketTest()
        {
            //System.Diagnostics.Debugger.Break();
            IspSupportInterfaceService target = new IspSupportInterfaceService();

            DataGateway dg = new DataGateway();
            Assert.IsTrue(dg.insertTerminal(testMacAddress, RegistrationStatus.LOCKED), "Could not insert test terminal");
            Assert.IsTrue(dg.createUser(testUserId, 112), "Could not create user");

            //Create a registration
            RequestTicket rt = target.createRegistration(testUserId, testMacAddress, testSlaId);

            RequestTicket rtReturn = target.lookupRequestTicket(rt.id);
            Assert.IsTrue(rtReturn.requestStatus == RequestStatus.BUSY, "Actual status of ticket = " + rtReturn.requestStatus);

            //Remove test user
            Assert.IsTrue(dg.deleteUser(testUserId), "Could not delete test user");

            //Clean-up tickets
            Assert.IsTrue(dg.cleanUpTickets(), "Could not clean up the Tickets");

            //Remove test terminal
            Assert.IsTrue(dg.deleteTerminal(testMacAddress), "Could not delete test terminal");
        }

        /// <summary>
        ///A test for removeRegistration
        ///</summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Projects\\SatFinAfrica\\CustomerManagementTool\\ISPIFEmulator", "/")]
        [UrlToTest("http://localhost:52995/")]
        public void removeRegistrationTest()
        {
            //System.Diagnostics.Debugger.Break();

            DataGateway dg = new DataGateway();

            //Insert the test terminal
            Assert.IsTrue(dg.insertTerminal(testMacAddress, RegistrationStatus.LOCKED), "Could not insert test terminal");

            IspSupportInterfaceService target = new IspSupportInterfaceService();

            //Prepare the test first
            RequestTicket rt = target.createRegistration(testUserId, testMacAddress, testSlaId);
            Assert.IsTrue(rt.requestStatus == RequestStatus.BUSY, "Create registration failed");

            //Complete the registration
            Assert.IsTrue(dg.completeRegistrations() == 1, "Could not complete the registration");

            string endUserId = testUserId;
            string macAddress = testMacAddress;
            rt = target.removeRegistration(endUserId, macAddress);

            Assert.IsTrue(rt.requestStatus == RequestStatus.BUSY, "Actual status of ticket = " + rt.requestStatus);

            //Remove test user
            Assert.IsTrue(dg.deleteUser(testUserId), "Could not delete test user");

            //Clean-up tickets
            Assert.IsTrue(dg.cleanUpTickets(), "Could not clean up the Tickets");

            //Remove test terminal
            Assert.IsTrue(dg.deleteTerminal(testMacAddress), "Could not delete test terminal");
        }

        /// <summary>
        ///A test for resetUser
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Projects\\SatFinAfrica\\CustomerManagementTool\\ISPIFEmulator", "/")]
        [UrlToTest("http://localhost:52248/")]
        public void resetUserTest()
        {
            IspSupportInterfaceService target = new IspSupportInterfaceService(); // TODO: Initialize to an appropriate value
            
            //Prepare terminals and users
            DataGateway dg = new DataGateway();
            Assert.IsTrue(dg.insertTerminal(testMacAddress, RegistrationStatus.LOCKED), "Could not insert test terminal");
            Assert.IsTrue(dg.createUser(testUserId, 112), "Could not create user");

            //Create a registration
            target.createRegistration(testUserId, testMacAddress, testSlaId);

            target.resetUser(testUserId, 112);

            //Read the terminal information for the testMacAddress
            Assert.IsTrue(dg.getTerminalVolume(testMacAddress) == 0.0, "Volume is not 0.0");

            //Remove test user
            Assert.IsTrue(dg.deleteUser(testUserId), "Could not delete test user");

            //Clean-up tickets
            Assert.IsTrue(dg.cleanUpTickets(), "Could not clean up the Tickets");

            //Remove test terminal
            Assert.IsTrue(dg.deleteTerminal(testMacAddress), "Could not delete test terminal");
   
        }

        /// <summary>
        ///A test for resetUserBySit
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Projects\\SatFinAfrica\\CustomerManagementTool\\ISPIFEmulator", "/")]
        [UrlToTest("http://localhost:52248/")]
        public void resetUserBySitTest()
        {
            IspSupportInterfaceService target = new IspSupportInterfaceService(); // TODO: Initialize to an appropriate value

            //Prepare terminals and users
            DataGateway dg = new DataGateway();
            Assert.IsTrue(dg.insertTerminal(testMacAddress, RegistrationStatus.LOCKED), "Could not insert test terminal");
            Assert.IsTrue(dg.createUserWithSitId(testUserId, 112, testSitId), "Could not create user");

            //Create a registration
            target.createRegistration(testUserId, testMacAddress, testSlaId);

            target.resetUserBySit("" + 112, testSitId.ToString());

            //Read the terminal information for the testMacAddress
            Assert.IsTrue(dg.getTerminalVolume(testMacAddress) == 0.0, "Volume is not 0.0");

            //Remove test user
            Assert.IsTrue(dg.deleteUser(testUserId), "Could not delete test user");

            //Clean-up tickets
            Assert.IsTrue(dg.cleanUpTickets(), "Could not clean up the Tickets");

            //Remove test terminal
            Assert.IsTrue(dg.deleteTerminal(testMacAddress), "Could not delete test terminal");
        }
    }
}
