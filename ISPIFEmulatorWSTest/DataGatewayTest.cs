﻿using net.nimera.cmt.ISPIFEmulator;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;

namespace ISPIFEmulatorWSTest
{

    /// <summary>
    ///This is a test class for DataGatewayTest and is intended
    ///to contain all DataGatewayTest Unit Tests
    ///</summary>
    [TestClass()]
    public class DataGatewayTest
    {
        //Test data
        string testUserId = "ABBS test 1";
        string testMacAddress = "00:06:39:83:1c:d9";
        int testSlaId = 1120;


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for createUser
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Projects\\SatFinAfrica\\CustomerManagementTool\\ISPIFEmulator", "/")]
        [UrlToTest("http://localhost:52995/")]
        public void createUserTest()
        {
            DataGateway target = new DataGateway();
            string userId = testUserId;
            bool expected = true;
            bool actual;
            actual = target.createUser(userId, 112);
            Assert.AreEqual(expected, actual);

            //Remove user again
            Assert.IsTrue(target.deleteUser(testUserId), "Could not delete user");
        }

        /// <summary>
        ///A test for deleteTerminal
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Projects\\SatFinAfrica\\CustomerManagementTool\\ISPIFEmulator", "/")]
        [UrlToTest("http://localhost:52995/")]
        public void deleteTerminalTest()
        {
            DataGateway target = new DataGateway(); 

            //Create the terminal first
            target.insertTerminal(testMacAddress);

            string macAddress = testMacAddress; 
            bool expected = true; 
            bool actual;
            actual = target.deleteTerminal(macAddress);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for generateTicket
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Projects\\SatFinAfrica\\CustomerManagementTool\\ISPIFEmulator", "/")]
        [UrlToTest("http://localhost:52995/")]
        public void generateTicketTest()
        {
            DataGateway target = new DataGateway();

            //Insert terminal
            target.insertTerminal(testMacAddress);

            RequestTicket rt = new RequestTicket();
            rt.id = Guid.NewGuid().ToString();

            RequestTicket rtExpected = new RequestTicket();
            string macAddress = testMacAddress;
            target.generateTicket(ref rt, macAddress, TypeEnum.Registration);
            Assert.IsTrue(rt.requestStatus == RequestStatus.BUSY);

            //Clean-up the Ticket table
            Assert.IsTrue(target.cleanUpTickets(), "Clean-up of Tickets failed");

            //Remove terminal after test
            Assert.IsTrue(target.deleteTerminal(macAddress), "Could not remove test terminal");
        }

        /// <summary>
        ///A test for hasPendingRequest
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Projects\\SatFinAfrica\\CustomerManagementTool\\ISPIFEmulator", "/")]
        [UrlToTest("http://localhost:52995/")]
        public void hasPendingRequestTest()
        {
            bool result = false;

            DataGateway target = new DataGateway();

            //Insert the test terminal
            result = target.insertTerminal(testMacAddress, RegistrationStatus.LOCKED);
            Assert.IsTrue(result, "Could not insert test terminal");

            //Generate a ticket for this terminal
            RequestTicket rt = new RequestTicket();
            rt.id = Guid.NewGuid().ToString();
            result = target.generateTicket(ref rt, testMacAddress, TypeEnum.Registration);

            Assert.IsTrue(result, "Could not create ticket");

            bool expected = true;
            bool actual;
            actual = target.hasPendingRequest(testMacAddress);
            Assert.AreEqual(expected, actual);

            //Clean-up ticket table
            result = target.cleanUpTickets();
            Assert.IsTrue(result, "Could not clean-up the Tickets table");

            //Finally remove the test terminal from the Terminals table
            result = target.deleteTerminal(testMacAddress);
            Assert.IsTrue(result, "Could not delete test terminal");
        }

        /// <summary>
        ///A test for insertTerminal
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Projects\\SatFinAfrica\\CustomerManagementTool\\ISPIFEmulator", "/")]
        [UrlToTest("http://localhost:52995/Default.aspx")]
        public void insertTerminalTest()
        {
            DataGateway target = new DataGateway();
            string macAddress = testMacAddress;
            int slaId = testSlaId;
            bool expected = true; 
            bool actual;
            actual = target.insertTerminal(macAddress);
            Assert.AreEqual(expected, actual);

            Assert.IsTrue(target.deleteTerminal(testMacAddress), "Could not delete test terminal");
        }

        /// <summary>
        ///A test for isTerminalInUse
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Projects\\SatFinAfrica\\CustomerManagementTool\\ISPIFEmulator", "/")]
        [UrlToTest("http://localhost:52995/")]
        public void isTerminalInUseTest()
        {
            DataGateway target = new DataGateway();

            //First insert the test terminal. Status will become OPERATIONAL
            Assert.IsTrue(target.insertTerminal(testMacAddress), "Could not insert test terminal");
            string macAddress = testMacAddress;
            bool expected = true;
            bool actual;
            actual = target.isTerminalInUse(macAddress);
            Assert.AreEqual(expected, actual);

            //Remove test terminal again
            Assert.IsTrue(target.deleteTerminal(testMacAddress), "Could not delete test terminal");
        }

        /// <summary>
        ///A test for isUserRegistered
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Projects\\SatFinAfrica\\CustomerManagementTool\\ISPIFEmulator", "/")]
        [UrlToTest("http://localhost:52995/")]
        public void isUserRegisteredTest()
        {
            DataGateway target = new DataGateway();

            //Insert the test terminal
            Assert.IsTrue(this.InsertTestTerminal(), "Could not insert test terminal");

            //Insert the test suer
            Assert.IsTrue(target.createUser(testUserId, 112), "Could not insert the test user");

            //Register user
            Assert.IsTrue(target.registerUser(testUserId, testMacAddress, testSlaId), "Could not register user");

            string userId = testUserId;
            bool expected = true;
            bool actual;
            actual = target.isUserRegistered(userId);
            Assert.AreEqual(expected, actual);

            //Remove the test user
            Assert.IsTrue(target.deleteUser(testUserId), "Could not delete test user");

            //Delete test terminal
            Assert.IsTrue(this.deleteTestTerminal(), "Could not delete test terminal");
        }

        /// <summary>
        ///A test for isUserRegisteredForTerminal
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Projects\\SatFinAfrica\\CustomerManagementTool\\ISPIFEmulator", "/")]
        [UrlToTest("http://localhost:52995/")]
        public void isUserRegisteredForTerminalTest()
        {
            DataGateway target = new DataGateway();

            //Insert test terminal
            Assert.IsTrue(this.InsertTestTerminal(), "Could not insert test terminal");

            //Insert test user
            Assert.IsTrue(target.createUser(testUserId, 112), "Could not create test user");

            //Register user
            Assert.IsTrue(target.registerUser(testUserId, testMacAddress, testSlaId), "Could not register user");

            string macAddress = testMacAddress;
            string userId = testUserId;
            bool expected = true;
            bool actual;
            actual = target.isUserRegisteredForTerminal(macAddress, userId);
            Assert.AreEqual(expected, actual);

            //Remove test user
            Assert.IsTrue(target.deleteUser(testUserId), "Could not delete test user");

            //Delete test terminal
            Assert.IsTrue(this.deleteTestTerminal(), "Could not delete test terminal");
        }

        /// <summary>
        ///A test for registerUser
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Projects\\SatFinAfrica\\CustomerManagementTool\\ISPIFEmulator", "/")]
        [UrlToTest("http://localhost:52995/")]
        public void registerUserTest()
        {
            //System.Diagnostics.Debugger.Break();
            DataGateway target = new DataGateway();

            //Insert test terminal
            Assert.IsTrue(this.InsertTestTerminal(), "Could not insert test terminal");

            //Create the test user
            Assert.IsTrue(target.createUser(testUserId, 112), "Could not create test user");

            string userId = testUserId;
            string macAddress = testMacAddress;
            bool expected = true;
            bool actual;
            actual = target.registerUser(userId, macAddress, testSlaId);
            Assert.AreEqual(expected, actual);

            //Delete the test user
            Assert.IsTrue(target.deleteUser(testUserId), "Could not delete test user");

            //Delete the test terminal
            Assert.IsTrue(this.deleteTestTerminal(), "Could not delete test terminal");
        }

        /// <summary>
        ///A test for slaExists
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Projects\\SatFinAfrica\\CustomerManagementTool\\ISPIFEmulator", "/")]
        [UrlToTest("http://localhost:52995/")]
        public void slaExistsTest()
        {
            DataGateway target = new DataGateway();
            int slaId = testSlaId;
            bool expected = true;
            bool actual;
            actual = target.slaExists(slaId);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for terminalExists
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Projects\\SatFinAfrica\\CustomerManagementTool\\ISPIFEmulator", "/")]
        [UrlToTest("http://localhost:52995/")]
        public void terminalExistsTest()
        {
            DataGateway target = new DataGateway();

            //Insert the test teminal
            Assert.IsTrue(this.InsertTestTerminal(), "Could not insert test terminal");

            string macAddress = testMacAddress;
            bool expected = true;
            bool actual;
            actual = target.terminalExists(macAddress);
            Assert.AreEqual(expected, actual);

            //Remove test terminal
            Assert.IsTrue(this.deleteTestTerminal(), "Could not remove test terminal");
        }

        /// <summary>
        ///A test for userExists
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Projects\\SatFinAfrica\\CustomerManagementTool\\ISPIFEmulator", "/")]
        [UrlToTest("http://localhost:52995/")]
        public void userExistsTest()
        {
            DataGateway target = new DataGateway();

            Assert.IsTrue(target.createUser(testUserId, 112), "Could not create user");
            string userId = testUserId;
            bool expected = true;
            bool actual;
            actual = target.userExists(userId);
            Assert.AreEqual(expected, actual);
            Assert.IsTrue(target.deleteUser(testUserId), "Could not delete user");
        }

        /// <summary>
        ///A test for deleteUser
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Projects\\SatFinAfrica\\CustomerManagementTool\\ISPIFEmulator", "/")]
        [UrlToTest("http://localhost:52995/")]
        public void deleteUserTest()
        {
            DataGateway target = new DataGateway();

            //Insert the user first
            target.createUser(testUserId, 112);

            string userId = testUserId;
            bool expected = true;
            bool actual;
            actual = target.deleteUser(userId);
            Assert.AreEqual(expected, actual);
        }
    
        /// <summary>
        ///A test for getRegisteredTerminal
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Projects\\SatFinAfrica\\CustomerManagementTool\\ISPIFEmulator", "/")]
        [UrlToTest("http://localhost:49285/")]
        public void getRegisteredTerminalTest()
        {
            //System.Diagnostics.Debugger.Break();
            DataGateway target = new DataGateway();
            string userId = testUserId;
            string expected = testMacAddress;

            //Insert a test record
            target.createUser(testUserId, 112);
            target.registerUser(testUserId, testMacAddress, testSlaId);

            string actual = target.getRegisteredTerminal(userId).Trim();
            Assert.AreEqual(expected, actual);

            //Remove test user
            Assert.IsTrue(target.deleteUser(testUserId), "Could not delete test user");

            //Clean-up tickets
            Assert.IsTrue(target.cleanUpTickets(), "Could not clean up the Tickets");
        }

        /// <summary>
        ///A test for changeStatus
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Projects\\SatFinAfrica\\CustomerManagementTool\\ISPIFEmulator", "/")]
        [UrlToTest("http://localhost:58851/")]
        public void changeStatusTest()
        {
            DataGateway target = new DataGateway();

            //Insert test terminal
            Assert.IsTrue(this.InsertTestTerminal(), "Could not insert test terminal");

            //Create the test user
            Assert.IsTrue(target.createUser(testUserId, 112), "Could not create test user");

            //Register user for this terminal
            target.registerUser(testUserId, testMacAddress, testSlaId);

            string userId = testUserId;
            RegistrationStatus status = RegistrationStatus.OPERATIONAL;
            Assert.IsTrue(target.changeStatus(userId, status), "Could not change status");

            //Remove test user
            Assert.IsTrue(target.deleteUser(testUserId), "Could not delete test user");
            
            //Clean-up
            //Remove test terminal
            Assert.IsTrue(target.deleteTerminal(testMacAddress), "Could not delete test terminal");
        }

        /// <summary>
        ///A test for changeSla
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Projects\\SatFinAfrica\\CustomerManagementTool\\ISPIFEmulator", "/")]
        [UrlToTest("http://localhost:58851/")]
        public void changeSlaTest()
        {
            DataGateway target = new DataGateway();

            //Insert test terminal
            Assert.IsTrue(this.InsertTestTerminal(), "Could not insert test terminal");

            //Create the test user
            Assert.IsTrue(target.createUser(testUserId, 112), "Could not create test user");

            string userId = testUserId;
            long slaId = testSlaId;
            bool expected = true; 
            bool actual;
            actual = target.changeSla(userId, slaId);
            Assert.AreEqual(expected, actual);

            //Delete the test user
            Assert.IsTrue(target.deleteUser(testUserId), "Could not delete test user");

            //Delete the test terminal
            Assert.IsTrue(this.deleteTestTerminal(), "Could not delete test terminal");
        }

        /// <summary>
        ///A test for getRequestTicket
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Projects\\SatFinAfrica\\CustomerManagementTool\\ISPIFEmulator", "/")]
        [UrlToTest("http://localhost:58851/")]
        public void getRequestTicketTest()
        {
            //System.Diagnostics.Debugger.Break();
            DataGateway target = new DataGateway();
            
            //Insert test terminal
            Assert.IsTrue(this.InsertTestTerminal(), "Could not insert test terminal");

            RequestTicket rt = new RequestTicket();
            rt.id = Guid.NewGuid().ToString();

            Assert.IsTrue(target.generateTicket(ref rt, testMacAddress, TypeEnum.Change), "Could not create ticket");

            RequestTicket rtResult = target.getRequestTicket(rt.id);

            Assert.IsTrue(rt.id.Equals(rtResult.id), "getRequestTicket failed");

            //Clean-up tickets
            Assert.IsTrue(target.cleanUpTickets(), "Could not clean up the Tickets");

            //Delete the test terminal
            Assert.IsTrue(this.deleteTestTerminal(), "Could not delete test terminal");
        }

        #region Helper methods
        private bool InsertTestTerminal()
        {
            DataGateway dg = new DataGateway();
            return dg.insertTerminal(testMacAddress, RegistrationStatus.LOCKED);
        }

        private bool deleteTestTerminal()
        {
            DataGateway dg = new DataGateway();
            return dg.deleteTerminal(testMacAddress);
        }
        #endregion

        /// <summary>
        ///A test for resetVolume
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Projects\\SatFinAfrica\\CustomerManagementTool\\ISPIFEmulator", "/")]
        [UrlToTest("http://localhost:52248/")]
        public void resetVolumeTest()
        {
            DataGateway target = new DataGateway(); // TODO: Initialize to an appropriate value

            Assert.IsTrue(target.insertTerminal(testMacAddress), "Could not create the test terminal");

            //Reset the volume
            Assert.IsTrue(target.resetVolume(testMacAddress), "resetVolume failed");

            //Delete the test terminal
            Assert.IsTrue(this.deleteTestTerminal(), "Could not delete test terminal");
        }
    }
}
