﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using MandacDataImportService.BOLogControlWSRef;
using MandacDataImportService.Util;

namespace MandacDataImportService
{
    class DataGateway
    {
        string _connectionString = "";
        BOLogControlWS _logController = null;

        /// <summary>
        /// The default constructor
        /// </summary>
        public DataGateway()
        {
            _connectionString = Properties.Settings.Default.ConnectionString;
            _logController = new BOLogControlWS();
        }

        /// <summary>
        /// Processes the data file
        /// </summary>
        /// <param name="fileName">Log file including full path</param>
        /// <returns>True if the import succeeded, false otherwise</returns>
        public bool ProcessFile(string fileName)
        {
            Logger.LogInfo("Processing file: " + fileName, "Mandac Voucher Data Import");
            bool success = false;
            char delimiter = Properties.Settings.Default.CsvInputDelimiter;
            string[] dataFields;
            VolumeObject volObj = new VolumeObject();

            try
            {
                //Parse the file and insert each line inthe VolumeHistoryDetail2 table
                using (FileStream stream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    using (var reader = new StreamReader(stream, Encoding.UTF8))
                    {
                        //First read header
                        reader.ReadLine();

                        while (!reader.EndOfStream)
                        {
                            try
                            {
                                dataFields = reader.ReadLine().Split(delimiter);
                                volObj.TransactionId = dataFields[0];
                                volObj.TransactionDate = DateTime.Parse(dataFields[1]);
                                volObj.VolumePurchasedGB = Decimal.Parse(dataFields[2]) / 1000;
                                dataFields[3] = dataFields[3].Replace('.', ',');
                                volObj.FeePaid = Decimal.Parse(dataFields[3]);
                                volObj.CurrencyCode = dataFields[4];
                                volObj.PaymentMethod = dataFields[5];
                                volObj.VolumeUsedGB = Decimal.Parse(dataFields[6]) / 10000000;
                                volObj.HotSpotId = this.GetHotspotTextId(int.Parse(dataFields[7]));
                                volObj.LastUpdate = DateTime.UtcNow;
                                volObj.VoucherType = "";
                                volObj.ValidUntil = DateTime.Parse(dataFields[8]);
                                
                                if (!this.dataExists(volObj))
                                {
                                    this.insertData(volObj);
                                }
                            }
                            catch (SqlException sqlEx)
                            {
                                Logger.log(sqlEx.Message);
                            }
                        }

                        success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
            }

            return success;
        }

        /// <summary>
        /// Helper method that checks if a transaction already exists in the database
        /// </summary>
        /// <param name="transactionId">The ID of the transaction</param>
        /// <returns>True if the ID already exists in the table</returns>
        private bool dataExists(VolumeObject volObj)
        {
            bool dataExists = false;

            string queryCmd = "SELECT VolumeUsedGB FROM MandacTransactions WHERE TransactionId = @TransactionId";
            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(queryCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@TransactionId", volObj.TransactionId);
                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.Read())
                        {
                            dataExists = true;
                            volObj.VolumeUsedGB = volObj.VolumeUsedGB + reader.GetDecimal(0);
                            this.updateData(volObj.TransactionId, volObj.VolumeUsedGB);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
            }
        
            return dataExists;
        }

        /// <summary>
        /// Updates the value of the Volume Used
        /// </summary>
        /// <param name="transactionId">The ID of the transaction to be updated</param>
        /// <param name="volumeUsed">The volume in GB</param>
        private void updateData(string transactionId, decimal volumeUsedGB)
        {
            string updateCmd = "UPDATE MandacTransactions " +
                               "SET VolumeUsedGB = @VolumeUsedGB, LastUpdate = @LastUpdate " +
                               "WHERE TransactionId = @TransactionId";

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(updateCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@TransactionId", transactionId);
                        cmd.Parameters.AddWithValue("@VolumeUsedGB", volumeUsedGB);
                        cmd.Parameters.AddWithValue("@LastUpdate", DateTime.UtcNow);

                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
            }
        }

        /// <summary>
        /// Inserts a new line of data fields into the table
        /// </summary>
        /// <param name="dataFields">String array of data fields</param>
        private void insertData(VolumeObject volObj)
        {
            string insertCmd = "INSERT INTO MandacTransactions (TransactionId, " +
                    "TransactionDate, VolumePurchasedGB, FeePaid, CurrencyCode, PaymentMethod, " +
                    "VolumeUsedGB, HotSpotId, LastUpdate, VoucherType, ValidUntil)" +
                    "VALUES (@TransactionId, " +
                    "@TransactionDate, @VolumePurchasedGB, @FeePaid, @CurrencyCode, @PaymentMethod, " +
                    "@VolumeUsedGB, @HotSpotId, @LastUpdate, @VoucherType, @ValidUntil)";

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(insertCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@TransactionId", volObj.TransactionId);
                        cmd.Parameters.AddWithValue("@TransactionDate", volObj.TransactionDate);
                        cmd.Parameters.AddWithValue("@VolumePurchasedGB", volObj.VolumePurchasedGB);
                        cmd.Parameters.AddWithValue("@FeePaid", volObj.FeePaid);
                        cmd.Parameters.AddWithValue("@CurrencyCode", volObj.CurrencyCode);
                        cmd.Parameters.AddWithValue("@PaymentMethod", volObj.PaymentMethod);
                        cmd.Parameters.AddWithValue("@VolumeUsedGB", volObj.VolumeUsedGB);
                        cmd.Parameters.AddWithValue("@HotSpotId", volObj.HotSpotId);
                        cmd.Parameters.AddWithValue("@LastUpdate", volObj.LastUpdate);
                        cmd.Parameters.AddWithValue("@VoucherType", volObj.VoucherType);
                        cmd.Parameters.AddWithValue("@ValidUntil", volObj.ValidUntil);

                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
            }
        }

        private string GetHotspotTextId(int numericId)
        {
            string textId = "unknown";

            if (numericId == 1)
            {
                textId = "Kanel_3";
            }
            else if (numericId == 2)
            {
                textId = "Kanel_4";
            }
            else if (numericId == 3)
            {
                textId = "Kanel_5";
            }
            else if (numericId == 4)
            {
                textId = "Kanel_6";
            }

            return textId;
        }

        private class VolumeObject
        {
            public string TransactionId { get; set; }
            public DateTime TransactionDate { get; set; }
            public decimal VolumePurchasedGB { get; set; }
            public decimal FeePaid { get; set; }
            public string CurrencyCode { get; set; }
            public string PaymentMethod { get; set; }
            public decimal VolumeUsedGB { get; set; }
            public string HotSpotId { get; set; }
            public DateTime LastUpdate { get; set; }
            public string VoucherType { get; set; }
            public DateTime ValidUntil { get; set; }
        }
    }
}
