﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using MandacDataImportService.Util;

namespace MandacDataImportService
{
    partial class MandacDataImportService : ServiceBase
    {
        DataGateway _dg;

        string _dataFolder = "";
        string _pathSuccess = "";
        string _pathFail = "";
        
        public MandacDataImportService()
        {
            InitializeComponent();
            _dg = new DataGateway();
            _dataFolder = Properties.Settings.Default.DataFolder;

            if (!_dataFolder.EndsWith("/"))
            {
                _dataFolder = _dataFolder + "/";
            }

            _pathSuccess = Properties.Settings.Default.SuccessPath;

            if (!_pathSuccess.EndsWith("/"))
            {
                _pathSuccess = _pathSuccess + "/";
            }

            _pathFail = Properties.Settings.Default.FailPath;

            if (!_pathFail.EndsWith("/"))
            {
                _pathFail = _pathFail + "/";
            }
        }

        protected override void OnStart(string[] args)
        {
            Logger.log("Service started");

            FileSystemWatcher watcher = new FileSystemWatcher(_dataFolder, "*.csv");

            //Watch for changes in LastAccess and LastWrite times, and
            //the renaming of files or directories.
            watcher.NotifyFilter = NotifyFilters.LastAccess
                                 | NotifyFilters.LastWrite
                                 | NotifyFilters.FileName
                                 | NotifyFilters.DirectoryName;

            // Event handlers
            watcher.Changed += new FileSystemEventHandler(OnChanged);
            watcher.Created += new FileSystemEventHandler(OnCreated);

            // Begin watching.
            watcher.EnableRaisingEvents = true;
        }

        protected override void OnStop()
        {
            Logger.log("Service stopped");
        }

        /// <summary>
        /// This event is fired when a file in the folder is changed. In the typical use case
        /// changed means the writing of the file into the folder is finalized.
        /// So far all this does is create a log entry, but possibly this eventy should trigger 
        /// the processing of the file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void OnChanged(object sender, FileSystemEventArgs e)
        {
            //Only for debug purposes
            //Logger.log(e.Name + " | changed");
        }

        /// <summary>
        /// This event fires when a new file is added to folder
        /// Currently this triggers the processing of the file, but perhaps the OnCreated event should be the trigger
        /// When processing is finished, the file is moved to another folder
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void OnCreated(object sender, FileSystemEventArgs e)
        {
            Boolean processingResult = false;
            Logger.log(e.Name + " | created");
            //string[] fileNameTokens = e.Name.Split('-');
            //if (fileNameTokens[0].Equals("RT"))
            //{
                processingResult = _dg.ProcessFile(e.FullPath);
            //}
            //else
            //{
            //    Logger.log(e.Name + " | Not a valid file name");
            //}

            if (processingResult)
            {
                Logger.log(e.Name + " | successfully processed");
                try
                {
                    // Move the file.
                    File.Move(e.FullPath, _pathSuccess + e.Name);
                }
                catch (Exception ex)
                {
                    Logger.log("Moving failed: " + ex.ToString());
                }
            }
            else
            {
                Logger.log(e.Name + " | processing failed");
                try
                {
                    // Move the file.
                    File.Move(e.FullPath, _pathFail + e.Name);

                }
                catch (Exception ex)
                {
                    Logger.log("Moving failed: " + ex.ToString());
                }
            }
        }
    }
}
