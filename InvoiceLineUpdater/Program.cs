﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InvoiceLineUpdater.BOInvoicingControllerWSRef;
using InvoiceLineUpdater.BOAccountingControlWSRef;

namespace InvoiceLineUpdater
{
    class Program
    {
        static void Main(string[] args)
        {
            Program prg = new Program();
            prg.UpdateInvoiceLines(30461, 31250); //31250
        }

        void UpdateInvoiceLines(int startId, int endId)
        {
            BOInvoicingControllerWS invoiceControl = new BOInvoicingControllerWS();
            BOAccountingControlWS accountingControl = new BOAccountingControlWS();
            int qq = 0;
            for (int i = startId; i <= endId; i++)
            {
                InvoiceDetail id = invoiceControl.GetInvoiceDetailLine(i);
                if (id.MacAddress != null)
                {
                    qq++; 
                    Terminal term = accountingControl.GetTerminalDetailsByMAC(id.MacAddress);
                    ServiceLevel sla = accountingControl.GetServicePack((int)term.SlaId);
                    id.Serial = term.Serial;
                    id.ServicePack = sla.SlaName.Trim();
                    invoiceControl.UpdateInvoiceDetail(id);
                    Console.WriteLine(qq + ". MAC: " + id.MacAddress + " Serial: " + id.Serial + " SLA: " + id.ServicePack);
                }
            }
            Console.WriteLine("Done. " + qq + " records updated");
        }
    }
}
