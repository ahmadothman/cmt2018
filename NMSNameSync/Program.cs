﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using MySql.Data.MySqlClient;
using NMSNameSync.Util;
using NMSNameSync.BOAccountingControlWSRef;

namespace NMSNameSync
{
    class Program
    {
        const bool _DEBUG = false;
        enum OperatingSystem { Windows, Linux }
        const OperatingSystem _OS = OperatingSystem.Linux;
        const string _FILEPATH_WINDOWS = "C:/Temp/";
        const string _FILEPATH_LINUX = "/home/daniel/";
        const string _connectionString = "Server=192.168.100.100;database=NMS;user=SatADSL;password=Fulvio";
        private string _logFile = "NMSNameSync.log";
        
        static void Main(string[] args)
        {
            Program prg = new Program();
            prg.mainBody();
        }

        private void mainBody()
        {
            if (_OS == OperatingSystem.Windows)
            {
                _logFile = _FILEPATH_WINDOWS + _logFile;
            }
            else
            {
                _logFile = _FILEPATH_LINUX + _logFile;
            }
            
            Logger.log("Synchronisation started", _logFile);

            BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();
            Terminal[] cmtTerminalList = boAccountingControl.GetTerminals();
            
            foreach (Terminal term in cmtTerminalList)
            {
                if (term.AdmStatus == 1 || term.AdmStatus == 2)
                {
                    try
                    {
                        if (term.ExtFullName.Trim() != this.GetTerminalExternalName(this.ConvertMacToLong(term.MacAddress)))
                        {
                            if (!_DEBUG)
                            {
                                if (this.SetTerminalExternalName(this.ConvertMacToLong(term.MacAddress), term.ExtFullName))
                                {
                                    Logger.log("Synced external name for MAC: " + term.MacAddress + " Name: " + term.ExtFullName, _logFile);
                                }
                                else
                                {
                                    Logger.log("Failed to sync external name for MAC: " + term.MacAddress, _logFile);
                                }
                            }
                            else
                            {
                                Logger.log("(DEBUG)Synced external name for MAC: " + term.MacAddress + " Name: " + term.ExtFullName, _logFile);
                            }
                        }

                        if (term.FullName.Trim() != this.GetTerminalFullName(this.ConvertMacToLong(term.MacAddress)))
                        {
                            if (!_DEBUG)
                            {
                                if (this.SetTerminalFullName(this.ConvertMacToLong(term.MacAddress), term.FullName))
                                {
                                    Logger.log("Synced full name for MAC: " + term.MacAddress + " Name: " + term.FullName, _logFile);
                                }
                                else
                                {
                                    Logger.log("Failed to sync full name for MAC: " + term.MacAddress, _logFile);
                                }
                            }
                            else
                            {
                                Logger.log("(DEBUG)Synced full name for MAC: " + term.MacAddress + " Name: " + term.FullName, _logFile);
                            }
                        }

                    }
                    catch (Exception)
                    {
                        Logger.log("Sync failed for terminal: " + term.MacAddress, _logFile);
                    }
                }
            }
            Logger.log("Synchronzation finished", _logFile);
        }

        /// <summary>
        /// Gets the external name of a terminal in the NMS
        /// </summary>
        /// <param name="macAddress">the MAC address of the terminal</param>
        /// <returns>true if successfull</returns>
        public string GetTerminalExternalName(long macAddress)
        {
            string updateCmd = "SELECT Id FROM Terminals WHERE MacAddress = @MacAddress";
            string termName = null;

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_connectionString))
                {
                    conn.Open();

                    using (MySqlCommand cmd = new MySqlCommand(updateCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@MacAddress", macAddress);
                        MySqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            termName = reader.GetString(0);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex.Message, _logFile);
            }

            return termName.Trim();
        }

        /// <summary>
        /// Gets the full name of a terminal in the NMS
        /// </summary>
        /// <param name="macAddress">the MAC address of the terminal</param>
        /// <returns>true if successfull</returns>
        public string GetTerminalFullName(long macAddress)
        {
            string updateCmd = "SELECT IdFrontEnd FROM Terminals WHERE MacAddress = @MacAddress";
            string termName = null;

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_connectionString))
                {
                    conn.Open();

                    using (MySqlCommand cmd = new MySqlCommand(updateCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@MacAddress", macAddress);
                        MySqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            termName = reader.GetString(0);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex.Message, _logFile);
            }

            return termName.Trim();
        }

        /// <summary>
        /// Sets the external name of a terminal in the NMS
        /// </summary>
        /// <param name="termName">The name to be set in the NMS</param>
        /// <param name="macAddress">the MAC address of the terminal</param>
        /// <returns>true if successfull</returns>
        public bool SetTerminalExternalName(long macAddress, string terminalName)
        {
            string updateCmd = "UPDATE Terminals SET Id = @Id WHERE MacAddress = @MacAddress";
            bool success = false;

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_connectionString))
                {
                    conn.Open();

                    using (MySqlCommand cmd = new MySqlCommand(updateCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@MacAddress", macAddress);
                        cmd.Parameters.AddWithValue("@Id", terminalName);
                        cmd.ExecuteNonQuery();

                        success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex.Message, _logFile);
            }

            return success;
        }

        /// <summary>
        /// Sets the full name of a terminal in the NMS
        /// </summary>
        /// <param name="termName">The name to be set in the NMS</param>
        /// <param name="macAddress">the MAC address of the terminal</param>
        /// <returns>true if successfull</returns>
        public bool SetTerminalFullName(long macAddress, string terminalName)
        {
            string updateCmd = "UPDATE Terminals SET IdFrontEnd = @IdFrontEnd WHERE MacAddress = @MacAddress";
            bool success = false;

            try
            {
                using (MySqlConnection conn = new MySqlConnection(_connectionString))
                {
                    conn.Open();

                    using (MySqlCommand cmd = new MySqlCommand(updateCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@MacAddress", macAddress);
                        cmd.Parameters.AddWithValue("@IdFrontEnd", terminalName);
                        cmd.ExecuteNonQuery();

                        success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.log(ex.Message, _logFile);
            }

            return success;
        }

        /// <summary>
        /// Converts a MAC address represented in the nn:nn:nn:nn:nn:nn
        /// format to a long.
        /// </summary>
        /// <param name="sMacAddress">Mac address as a string</param>
        /// <returns>Long representation of the MAC address</returns>
        public long ConvertMacToLong(string sMacAddress)
        {
            string filteredString = "";
            String[] macTokens = sMacAddress.Split(':');

            foreach (string token in macTokens)
            {
                filteredString = filteredString + token;
            }

            return long.Parse(filteredString, NumberStyles.HexNumber);
        }
    }
}
