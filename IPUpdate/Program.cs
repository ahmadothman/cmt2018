﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using IPUpdate.BOAccountingControlRef;

namespace IPUpdate
{
    /// <summary>
    /// This application updates the IP addresses of a terminal depending on a given
    /// CSV file.
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            Program prg = new Program();
            prg.mainBody(args);
        }

        protected void mainBody(string[] args)
        {
            string inputLine = "";
            string macAddress = "";
            string[] inputTokens = null;
            int processedCounter = 0;
            int unProcessedCounter = 0;
            BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();

            if (args.Length != 1)
            {
                Console.WriteLine("Usage: IPUpdate [path to inputfile]");
            }
            else
            {
                Console.WriteLine("Starting IPUpdate with input file: " + args[0]);
                Console.WriteLine("Terminals which could not be processed:");
                Console.WriteLine("---------------------------------------");
                try
                {
                    //Read input file
                    StreamReader inputFile = new StreamReader(args[0]);

                    //Discard the first line
                    inputFile.ReadLine();
                    
                    while (!inputFile.EndOfStream)
                    {
                        inputLine = inputFile.ReadLine();
                        inputTokens = inputLine.Split(';');
                        macAddress = inputTokens[3]; //Update for MAC Address location in file

                        Terminal term = boAccountingControl.GetTerminalDetailsByMAC(macAddress);

                        if (term == null)
                        {
                            Console.WriteLine(macAddress + "; " + inputTokens[4].Trim()); //Update for IP Address location in file
                            unProcessedCounter++;
                        }
                        else
                        {
                            term.IpAddress = inputTokens[4].Trim();
                            boAccountingControl.UpdateTerminal(term);
                            processedCounter++;
                        }
                    }

                    Console.WriteLine( processedCounter + " Processed records");
                    Console.WriteLine(unProcessedCounter + " Unprocessed records");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }
    }
}
