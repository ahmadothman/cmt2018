﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OleDb;
using System.Data.SqlClient;
using Microsoft.Synchronization.Data;
using Microsoft.Synchronization.Data.SqlServer;

namespace SyncPrepare
{
    /// <summary>
    /// This application prepares the CMTData database for syncing
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            Program prg = new Program();
            prg.mainBody();
        }

        private void mainBody()
        {
            SqlConnection conn = null;

            try
            {
                using (conn = new SqlConnection("Data source=cmtdata.nimera.net;Database=CMTData; User Id=SatFinAfrica; Password=Lagos#1302"))
                {
                    // define a new scope named ProductsScope
                    DbSyncScopeDescription scopeDesc = new DbSyncScopeDescription("CMTDataScope");

                    // get the description of the tables from the CMTData database and add to the scope
                    DbSyncTableDescription addressTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("Addresses", conn);
                    DbSyncTableDescription distributorsTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("Distributors", conn);
                    DbSyncTableDescription ispsTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("ISPS", conn);
                    DbSyncTableDescription organizationsTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("Organizations", conn);
                    DbSyncTableDescription servicePacksTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("ServicePacks", conn);
                    DbSyncTableDescription terminalActivityTypesTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("TerminalActivityTypes", conn);
                    DbSyncTableDescription terminalActivityLogTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("TerminalActivityLog", conn);
                    DbSyncTableDescription terminalsTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("Terminals", conn);
                   

                    //Solve the geometry problem
                    foreach (var item in terminalsTableDesc.NonPkColumns)
                    {
                        if (item.Type.Contains("geography"))
                        {
                            item.ParameterName += "_was_geography";
                            item.Type = "nvarchar";
                            item.Size = "max";
                            item.SizeSpecified = true;
                        }
                    }

                    DbSyncTableDescription cnoValuesTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("CNOValues", conn);
                    DbSyncTableDescription ispDistributorsTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("ISPDistributors", conn);
                    DbSyncTableDescription tasksTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("Tasks", conn);
                    DbSyncTableDescription taskTypesTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("TaskTypes", conn);
                    DbSyncTableDescription exceptionActivityLogTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("ExceptionActivityLog", conn);
                    DbSyncTableDescription distributorUsersTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("DistributorsUsers", conn);
                    DbSyncTableDescription userActivityLogTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("UserActivityLog", conn);
                    DbSyncTableDescription userActivityTypesTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("UserActivityTypes", conn);
                    DbSyncTableDescription voucherBatchesTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("VoucherBatches", conn);
                    DbSyncTableDescription vouchersTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("Vouchers", conn);
                    DbSyncTableDescription voucherVolumesTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("VoucherVolumes", conn);
                    DbSyncTableDescription configurationTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("Configuration", conn);
                    DbSyncTableDescription ticketsTableDesc = SqlSyncDescriptionBuilder.GetDescriptionForTable("Tickets", conn);

                    //Add tables to the sync scope
                    scopeDesc.Tables.Add(addressTableDesc);
                    scopeDesc.Tables.Add(distributorsTableDesc);
                    scopeDesc.Tables.Add(ispsTableDesc);
                    scopeDesc.Tables.Add(organizationsTableDesc);
                    scopeDesc.Tables.Add(servicePacksTableDesc);
                    scopeDesc.Tables.Add(terminalActivityTypesTableDesc);
                    scopeDesc.Tables.Add(terminalActivityLogTableDesc);
                    scopeDesc.Tables.Add(terminalsTableDesc);
                    scopeDesc.Tables.Add(cnoValuesTableDesc);
                    scopeDesc.Tables.Add(ispDistributorsTableDesc);
                    scopeDesc.Tables.Add(tasksTableDesc);
                    scopeDesc.Tables.Add(taskTypesTableDesc);
                    scopeDesc.Tables.Add(exceptionActivityLogTableDesc);
                    scopeDesc.Tables.Add(distributorUsersTableDesc);
                    scopeDesc.Tables.Add(userActivityLogTableDesc);
                    scopeDesc.Tables.Add(userActivityTypesTableDesc);
                    scopeDesc.Tables.Add(voucherBatchesTableDesc);
                    scopeDesc.Tables.Add(vouchersTableDesc);
                    scopeDesc.Tables.Add(voucherVolumesTableDesc);
                    scopeDesc.Tables.Add(configurationTableDesc);
                    scopeDesc.Tables.Add(ticketsTableDesc);


                    // create a server scope provisioning object based on the ProductScope
                    SqlSyncScopeProvisioning serverProvision = new SqlSyncScopeProvisioning(conn, scopeDesc);

                    // skipping the creation of table since table already exists on server
                    serverProvision.SetCreateTableDefault(DbSyncCreationOption.Skip);

                    // start the provisioning process
                    serverProvision.Apply();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
