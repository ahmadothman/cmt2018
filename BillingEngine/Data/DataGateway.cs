﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using BillingEngine.Model;
using BillingEngine.Util;
using BillingEngine.BOLogControllerRef;

namespace BillingEngine.Data
{
    /// <summary>
    /// Contains mainly helper methods for CMTData-Invoicing table access
    /// </summary>
    public class DataGateway
    {
        private string _connectionString;
        private BOLogControlWS _logController;
        
        /// <summary>
        /// Default constructor
        /// </summary>
        public DataGateway()
        {
            //_connectionString = BillingEngine.Default.InvoiceConnectionString;
            //_logController = new BOLogControlWS();
        }

        /// <summary>
        /// Checks if the distributor exists, if not add it.
        /// </summary>
        /// <param name="dist">The distributor to update or insert</param>
        /// <returns>true if the operation succeeded</returns>
        public Boolean UpdateDistributor(BillingDistributor dist)
        {
            Boolean success = false;
            int recordsUpdated = 0;
            int recordsInserted = 0;
            int numRows = 0;
            int rowsAffected = 0;
            string distQuery = "SELECT count(*) FROM Distributors WHERE Id = @Id";
            string updateSql = "UPDATE Distributors SET FullName =  @FullName, Phone = @Phone, " +
                               "Fax = @Fax, Vat = @Vat, AddressLine1 = @AddressLine1, " +
                               "AddressLine2 = @AddressLine2, Location = @Location, PostalCode = @PostalCode, " +
                               "Country = @Country, EMail = @EMail WHERE Id = @Id";
            string insertSql = "INSERT INTO Distributors (Id, FullName, Phone, Fax, Vat, AddressLine1, " +
                               "AddressLine2, Location, PostalCode, Country, EMail) VALUES (@Id, @FullName, @Phone, " +
                               "@Fax, @Vat, @AddressLine1, @AddressLine2, @Location, @PostalCode, " +
                               "@Country, @EMail)";

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(distQuery, conn);
                    cmd.Parameters.AddWithValue("@Id", dist.Id);
                    SqlDataReader reader = cmd.ExecuteReader();

                    if (reader.Read())
                    {
                        numRows = reader.GetInt32(0);
                    }
                    reader.Close();

                    if (numRows == 1)
                    {
                        //Distributor exists, update
                        cmd = new SqlCommand(updateSql, conn);

                        if (dist.FullName != null)
                            cmd.Parameters.AddWithValue("@FullName", dist.FullName);
                        else
                            cmd.Parameters.AddWithValue("@FullName", "");

                        if (dist.Phone != null)
                            cmd.Parameters.AddWithValue("@Phone", dist.Phone);
                        else
                            cmd.Parameters.AddWithValue("@Phone", "");

                        if (dist.Fax != null)
                            cmd.Parameters.AddWithValue("@Fax", dist.Fax);
                        else
                            cmd.Parameters.AddWithValue("@Fax", "");

                        if (dist.Vat != null)
                            cmd.Parameters.AddWithValue("@Vat", dist.Vat);
                        else
                            cmd.Parameters.AddWithValue("@Vat", "");

                        if (dist.AddressLine1 != null)
                            cmd.Parameters.AddWithValue("@AddressLine1", dist.AddressLine1);
                        else
                            cmd.Parameters.AddWithValue("@AddressLine1", "");

                        if (dist.AddressLine2 != null)
                            cmd.Parameters.AddWithValue("@AddressLine2", dist.AddressLine2);
                        else
                            cmd.Parameters.AddWithValue("@AddressLine2", "");

                        if (dist.Location != null)
                            cmd.Parameters.AddWithValue("@Location", dist.Location);
                        else
                            cmd.Parameters.AddWithValue("@Location", "");

                        if (dist.PostalCode != null)
                            cmd.Parameters.AddWithValue("@PostalCode", dist.PostalCode);
                        else
                            cmd.Parameters.AddWithValue("@PostalCode", "");

                        if (dist.Country != null)
                            cmd.Parameters.AddWithValue("@Country", dist.Country);
                        else
                            cmd.Parameters.AddWithValue("@Country", "");

                        if (dist.EMail != null)
                            cmd.Parameters.AddWithValue("@EMail", dist.EMail);
                        else
                            cmd.Parameters.AddWithValue("@EMail", "");

                        cmd.Parameters.AddWithValue("@Id", dist.Id);

                        rowsAffected = cmd.ExecuteNonQuery();

                        if (rowsAffected != 1)
                        {
                            CmtApplicationException cmtEx = new CmtApplicationException();
                            cmtEx.ExceptionDesc = "BillingEngine - Could not update distirbutor: " + dist.Id + ", " + dist.FullName;
                            cmtEx.ExceptionDateTime = DateTime.Now;
                            cmtEx.UserDescription = "UpdateDistributor method of BillingEngine failed";
                            _logController.LogApplicationException(cmtEx);

                        }
                        else
                        {
                            recordsUpdated++;
                            success = true;
                        }
                    }
                    else
                    {
                        //Distributor does not exist, insert
                        cmd = new SqlCommand(insertSql, conn);

                        if (dist.FullName != null)
                            cmd.Parameters.AddWithValue("@FullName", dist.FullName);
                        else
                            cmd.Parameters.AddWithValue("@FullName", "");

                        if (dist.Phone != null)
                            cmd.Parameters.AddWithValue("@Phone", dist.Phone);
                        else
                            cmd.Parameters.AddWithValue("@Phone", "");

                        if (dist.Fax != null)
                            cmd.Parameters.AddWithValue("@Fax", dist.Fax);
                        else
                            cmd.Parameters.AddWithValue("@Fax", "");

                        if (dist.Vat != null)
                            cmd.Parameters.AddWithValue("@Vat", dist.Vat);
                        else
                            cmd.Parameters.AddWithValue("@Vat", "");

                        if (dist.AddressLine1 != null)
                            cmd.Parameters.AddWithValue("@AddressLine1", dist.AddressLine1);
                        else
                            cmd.Parameters.AddWithValue("@AddressLine1", "");

                        if (dist.AddressLine2 != null)
                            cmd.Parameters.AddWithValue("@AddressLine2", dist.AddressLine2);
                        else
                            cmd.Parameters.AddWithValue("@AddressLine2", "");

                        if (dist.Location != null)
                            cmd.Parameters.AddWithValue("@Location", dist.Location);
                        else
                            cmd.Parameters.AddWithValue("@Location", "");

                        if (dist.PostalCode != null)
                            cmd.Parameters.AddWithValue("@PostalCode", dist.PostalCode);
                        else
                            cmd.Parameters.AddWithValue("@PostalCode", "");

                        if (dist.Country != null)
                            cmd.Parameters.AddWithValue("@Country", dist.Country);
                        else
                            cmd.Parameters.AddWithValue("@Country", "");

                        if (dist.EMail != null)
                            cmd.Parameters.AddWithValue("@EMail", dist.EMail);
                        else
                            cmd.Parameters.AddWithValue("@EMail", "");

                        cmd.Parameters.AddWithValue("@Id", dist.Id);

                        rowsAffected = cmd.ExecuteNonQuery();

                        if (rowsAffected != 1)
                        {
                            CmtApplicationException cmtEx = new CmtApplicationException();
                            cmtEx.ExceptionDesc = "BillingEngine - Could not insert distirbutor: " + dist.Id + ", " + dist.FullName;
                            cmtEx.ExceptionDateTime = DateTime.Now;
                            cmtEx.UserDescription = "UpdateDistributor method of BillingEngine failed";
                            _logController.LogApplicationException(cmtEx);

                        }
                        else
                        {
                            recordsInserted++;
                            success = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDesc = "BillingEngine - Exception: " + ex.Message;
                cmtEx.ExceptionStacktrace = ex.StackTrace;
                cmtEx.UserDescription = "UpdateDistributor method of BillingEngine failed";
                _logController.LogApplicationException(cmtEx);
            }

            return success;
        }

        /// <summary>
        /// Inserts a new invoice header
        /// </summary>
        /// <param name="invHeader">The invoice header to insert</param>
        /// <returns>True if the insert succeed, false otherwilse</returns>
        public Boolean InsertInvoiceHeader(InvoiceHeader invHeader)
        {
            string insertCmd = "INSERT INTO InvoiceHeaders (InvoiceYear, InvoiceNum, DistributorId, CreationDate, Payed, InvoiceMonth) " +
                               "VALUES (@Year, @InvoiceNum, @DistributorId, @CreationDate, @Payed, @InvoiceMonth)";
            Boolean success = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(insertCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@Year", invHeader.InvoiceYear);
                        cmd.Parameters.AddWithValue("@InvoiceNum", invHeader.InvoiceNumber);
                        cmd.Parameters.AddWithValue("@DistributorId", invHeader.DistributorId);
                        cmd.Parameters.AddWithValue("@CreationDate", invHeader.InvoiceDate);
                        cmd.Parameters.AddWithValue("@Payed", false);
                        cmd.Parameters.AddWithValue("@InvoiceMonth", invHeader.InvoiceMonth);

                        success = cmd.ExecuteNonQuery() == 1;
                    }
                }
            }
            catch (Exception ex)
            {
                success = false;
                Logger.Log(ex.Message, true);
                Logger.Log("Inserting invoice header for invoice: " + invHeader.InvoiceMonth + "-" + invHeader.InvoiceYear + "-" + invHeader.InvoiceNumber + " failed!", true);
            }

            return success;  
        }


        public Boolean InsertInvoiceLines(List<InvoiceLine> invLines)
        {
            Boolean success = false;

            try
            {
                foreach (InvoiceLine invLine in invLines)
                {
                    this.InsertInvoiceLines(invLine);
                }
            }
            catch (Exception ex)
            {
                success = false;
                Logger.Log(ex.Message, true);
                Logger.Log("Inserting invoice lines failed", true);
            }

            return success;
        }

        private Boolean InsertInvoiceLines(InvoiceLine invLine)
        {
            string insertCmd = "INSERT INTO InvoiceDetails (InvoiceYear, InvoiceNum, ItemId, ItemDescription, UnitPrice, " +
                               "Items, AmountUSD, AmountEUR, InvoiceMonth, TerminalId, Serial, ServicePack, MacAddress, Status) " +
                               "VALUES (@InvoiceYear, @InvoiceNum, @ItemId, " +
                               "@ItemDescription, @UnitPrice, @Items, @AmountUSD, @AmountEUR, @InvoiceMonth, @TerminalId, " +
                               "@Serial, @ServicePack, @MacAddress, @Status)";

            Boolean success = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(insertCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@InvoiceYear", invLine.InvoiceYear);
                        cmd.Parameters.AddWithValue("@InvoiceNum", invLine.InvoiceNum);
                        cmd.Parameters.AddWithValue("@ItemId", invLine.ItemId);
                        cmd.Parameters.AddWithValue("@ItemDescription", invLine.ItemDescription.Trim());
                        cmd.Parameters.AddWithValue("@UnitPrice", invLine.UnitPrice);
                        cmd.Parameters.AddWithValue("@Items", invLine.Items);
                        cmd.Parameters.AddWithValue("@AmountUSD", invLine.AmountUSD);
                        cmd.Parameters.AddWithValue("@AmountEUR", invLine.AmountEUR);
                        cmd.Parameters.AddWithValue("@InvoiceMonth", invLine.InvoiceMonth);
                        cmd.Parameters.AddWithValue("@TerminalId", invLine.TerminalId.Trim());
                        cmd.Parameters.AddWithValue("@Serial", invLine.Serial.Trim());
                        cmd.Parameters.AddWithValue("@ServicePack", invLine.ServicePack.Trim());
                        cmd.Parameters.AddWithValue("@MacAddress", invLine.MacAddress);
                        cmd.Parameters.AddWithValue("@Status", invLine.Status.Trim());
                        success = (cmd.ExecuteNonQuery() == 1);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return success;
        }
    
    }
}
