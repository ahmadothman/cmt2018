﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BillingEngine.Controller.Interface;
using BillingEngine.BOAccountingControllerRef;
using BillingEngine.BOConfigurationControllerRef;
using BillingEngine.Data;
using BillingEngine.Model;
using BillingEngine.Util;
using BillingEngine;
using BillingEngine.CurrencyConvertorRef;
using BillingEngine.BOInvoicingControllerWSRef;

namespace BillingEngine.Controller
{
    public class BillingController : IBillingController
    {
        const int _UnlockedTerminalStatus = 2; 
        private DataGateway _dg;
        private CurrencyConvertor _currencyConvertor;
        private BOAccountingControlWS _boAccountingController;
        private BOConfigurationControllerWS _boConfigurationController;
        private BOInvoicingControllerWS _invoiceController;
        private const int _bankDetailsBId = 1234;
        private short _invState = 0; //Initial state = unvalidated

        public BillingController()
        {
            _dg = new DataGateway();
            _currencyConvertor = new CurrencyConvertor();
            _boAccountingController = new BOAccountingControlWS();
            _boConfigurationController = new BOConfigurationControllerWS();
            _invoiceController = new BOInvoicingControllerWS();
        }

        #region IBillingController Members

        /// <summary>
        /// Updates the Distributors table of the invvoice database
        /// </summary>
        /// <remarks>
        /// Synopsis:
        /// - For each distributor call the invoice line compiler
        /// - If the number of lines is > 0 create an invoice header and update database
        /// </remarks>
        /// <param name="Distributors">List of distributors from the CMTData database</param>
        /// <returns>True if the operation succeeded</returns>
        public bool SynchronizeDistributors(Distributor[] Distributors)
        {
            Boolean success = false;
            BillingDistributor bd = new BillingDistributor();

            Logger.Log("Synchronize Distributors start");

            foreach (Distributor dist in Distributors)
            {
                bd.AddressLine1 = dist.Address.AddressLine1;
                bd.AddressLine2 = dist.Address.AddressLine2;
                bd.Country = dist.Address.Country;
                bd.EMail = dist.Email;
                bd.Fax = dist.Fax;
                bd.FullName = dist.FullName;
                bd.Id = (int) dist.Id;
                bd.Location = dist.Address.Location;
                bd.Phone = dist.Phone;
                bd.PostalCode = dist.Address.PostalCode;
                bd.Vat = dist.Vat;

                success = _dg.UpdateDistributor(bd);
            }

            Logger.Log("Synchronize Distributors end");
            return success;
        }

        /// <summary>
        /// Effectively creates invoices
        /// This method is used for a monthly recurring job.
        /// The method takes the distributors' situation into account, ie. Last Invoice Date and Invoice Interval
        /// </summary>
        /// <param name="Distributors">An array of distributors</param>
        /// <param name="monthNum">The month to be invoiced</param>
        /// <param name="year">The year of the month to be invoiced</param>
        /// <returns>True if succesfull</returns>
        public bool CreateInvoices(Distributor[] Distributors, int monthNum, int year)
        {
            Boolean success = true;
            //Double conversionRate = _currencyConvertor.ConversionRate(Currency.USD, Currency.EUR);
            InvoiceCompiler invCompiler = new InvoiceCompiler();
            int invoiceNum = 1;
            DateTime currDate = DateTime.Now;

            //when working with specific distributors
            //int[] distIds = new int[] { 317 };
            //Distributors = new Distributor[distIds.Length];
            //for (int i = 0; i < distIds.Length; i++)
            //{
            //    Distributors[i] = _boAccountingController.GetDistributor(distIds[i]);
            //}

            Logger.Log("Create invoices start. Month: " + monthNum + ", " + "Year: " + year);
            
            foreach (Distributor dist in Distributors)
            {
                if (dist.Id != 216 && dist.Id != 282 && dist.Id != 175 && !dist.BadDistributor) //no invoice for the SatADSL distributor (216) and the AAA To be deleted (175) and no invoice for bad distributor as well.
                {
                    //Check when the distributor is due to be invoiced
                    int nextInvoiceMonth = dist.LastInvoiceDate.Month + (int)dist.InvoiceInterval;
                    int nextInvoiceYear = dist.LastInvoiceDate.Year;
                    //Compensate for new year
                    if (nextInvoiceMonth > 12)
                    {
                        nextInvoiceMonth = nextInvoiceMonth - 12;
                        nextInvoiceYear = nextInvoiceYear + 1;
                    }
                    decimal totalAmtOfInvoiceLines = (decimal)0;
                    BillingEngine.BOInvoicingControllerWSRef.InvoiceHeader invHeader = new BillingEngine.BOInvoicingControllerWSRef.InvoiceHeader();
                    List<BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail> invoiceDetails = new List<BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail>();



                    //Create invoice lines for monthly fees if distributor is due to be invoiced for the current month 
                    if ((nextInvoiceMonth == monthNum && nextInvoiceYear == year) || (int)dist.InvoiceInterval == 1)
                    {
                        //Take into account that some distributors are invoiced for multiple months in advance
                        //This will generate monthly fees for terminals that follow the distributor's periodicity
                        for (int months = 1; months <= (int)dist.InvoiceInterval; months++)
                        {
                            int invoiceMonth = monthNum + months - 1;
                            int invoiceYear = year;
                            if (invoiceMonth > 12)
                            {
                                invoiceMonth = invoiceMonth - 12;
                                invoiceYear = invoiceYear + 1;
                            }
                            List<BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail> activeTerminals = invCompiler.ActiveTerminalsInvoiceLines((int)dist.Id, invoiceMonth, invoiceYear, dist.Currency, invoiceNum);
                            foreach (BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail id in activeTerminals)
                            {
                                invoiceDetails.Add(id);
                                totalAmtOfInvoiceLines = totalAmtOfInvoiceLines + id.UnitPrice;
                            }
                        }

                        //Generate monthly fees for terminals that follow specific periodicity
                        List<BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail> activeTerminalsTermInvoicing = invCompiler.ActiveTerminalsTerminalInvoicing((int)dist.Id, monthNum, year, dist.Currency, invoiceNum);
                        foreach (BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail id in activeTerminalsTermInvoicing)
                        {
                            invoiceDetails.Add(id);
                            totalAmtOfInvoiceLines = totalAmtOfInvoiceLines + id.UnitPrice;
                        }

                        //Create the special invoice lines for the past month (activations, FUP resets etc)
                        for (int months = 1; months <= (int)dist.InvoiceInterval; months++)
                        {
                            int invoiceMonth = monthNum - months;
                            int invoiceYear = year;
                            if (invoiceMonth < 1)
                            {
                                invoiceMonth = invoiceMonth + 12;
                                invoiceYear = invoiceYear - 1;
                            }
                            List<BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail> invoiceLinesTemp = invCompiler.GenerateInvoiceLines((int)dist.Id, invoiceMonth, invoiceYear, dist.Currency, invoiceNum);
                            foreach (BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail id in invoiceLinesTemp)
                            {
                                invoiceDetails.Add(id);
                                totalAmtOfInvoiceLines = totalAmtOfInvoiceLines + id.UnitPrice;
                            }

                            if (months > 1)
                            {
                                invoiceLinesTemp = new List<BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail>();
                                invoiceLinesTemp = invCompiler.PastActivatedTerminalsInvoiceLines((int)dist.Id, invoiceMonth, invoiceYear, dist.Currency, invoiceNum, months - 1);
                                foreach (BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail id in invoiceLinesTemp)
                                {
                                    invoiceDetails.Add(id);
                                    totalAmtOfInvoiceLines = totalAmtOfInvoiceLines + id.UnitPrice;
                                }
                            }
                        }

                        //Fetch the number of active terminals for the distributor
                        AdvancedQueryParams advQueryParams = new AdvancedQueryParams();
                        advQueryParams.AdmState = _UnlockedTerminalStatus;
                        advQueryParams.DistributorId = (int)dist.Id;
                        advQueryParams.IspId = -1;
                        advQueryParams.SitId = -1;
                        advQueryParams.SlaId = -1;
                        advQueryParams.FullName = "";
                        advQueryParams.IpAddress = "";
                        advQueryParams.MacAddress = "";
                        advQueryParams.FwdPool = -1;
                        advQueryParams.RtnPool = -1;
                        advQueryParams.ExpDate = new DateTime(0L);
                        advQueryParams.ExpDateOper = "";
                        advQueryParams.EMail = "";
                        advQueryParams.Serial = "";
                        //CMT-99
                        advQueryParams.SatelliteId = -1;
                        advQueryParams.TestTerminal = false;

                        Terminal[] nbrTerminals = _boAccountingController.GetTerminalsWithAdvancedQuery(advQueryParams);
                        Logger.Log("Active terminals for dist " + dist.Id + ": " + nbrTerminals.Length);
                        Logger.Log("Total amount: " + totalAmtOfInvoiceLines);

                        //SMT-15
                        for (int i = 0; i < nbrTerminals.Length; i++)
                        {
                            ServiceLevel sl = _boAccountingController.GetServicePack((int)nbrTerminals[i].SlaId);
                            if (sl == null)
                            {
                                if (_boAccountingController.GetISP(nbrTerminals[i].IspId).CompanyName.IndexOf("MULTICAST", StringComparison.OrdinalIgnoreCase) >= 0)
                                    nbrTerminals = nbrTerminals.Where(val => val != nbrTerminals[i]).ToArray();
                            }
                            else
                            {
                                if (_boAccountingController.GetISP(nbrTerminals[i].IspId).CompanyName.IndexOf("MULTICAST", StringComparison.OrdinalIgnoreCase) >= 0 || sl.ServiceClass == 5 || sl.ServiceClass == 6)
                                    nbrTerminals = nbrTerminals.Where(val => val != nbrTerminals[i]).ToArray();
                            }
                        }

                        //Only create an invoice if there is an actual cost to be paid
                        if (invoiceDetails.Count > 0 && nbrTerminals.Length > 0 && totalAmtOfInvoiceLines > 0)
                        {
                            invHeader.ActiveTerminals = nbrTerminals.Length;

                            //Add VAT for distributors based in Belgium
                            invHeader.VAT = 0;
                            if (dist.Address.Country == "Belgium" || dist.Address.Country == "BE")
                            {
                                invHeader.VAT = 0.21m;
                            }

                            //Add financial interest
                            BillingEngine.BOInvoicingControllerWSRef.InvoiceHeader[] distInvoices = _invoiceController.GetInvoicesForDistributor((int)dist.Id);
                            decimal outstandingPayments = (decimal)0;
                            foreach (BillingEngine.BOInvoicingControllerWSRef.InvoiceHeader ih in distInvoices)
                            {
                                //add up the total amount of unpaid invoices
                                if (ih.InvoiceState != 4) //Paid
                                {
                                    outstandingPayments = outstandingPayments + ih.ToBePaid;
                                }
                            }
                            invHeader.FinancialInterest = 0.015m * outstandingPayments;

                            //Add bank charges
                            invHeader.BankCharges = dist.BankCharges;


                            //Create the invoice header and dump the invoice to the database
                            Logger.Log("Invoice lines for dist " + dist.Id + ": " + invoiceDetails.Count);

                            invHeader.InvoiceYear = currDate.Year;
                            invHeader.InvoiceNum = invoiceNum;
                            invHeader.DistributorId = (int)dist.Id;
                            invHeader.InvoiceMonth = currDate.Month; //the invoice month is the month of creation, not the previous month
                            invHeader.CreationDate = new DateTime(currDate.Year, currDate.Month, 1);
                            invHeader.CurrencyCode = dist.Currency;
                            invHeader.InvoiceState = _invState;
                            //invHeader.Remarks = "Auto-generated by the Billing Controller";
                            invHeader.DistributorName = dist.FullName;

                            Decimal paidInAdvance = (Decimal)0;
                            foreach (BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail id in invoiceDetails)
                            {
                                if (id.PaidInAdvance)
                                {
                                    paidInAdvance = paidInAdvance + id.UnitPrice;
                                }
                            }

                            invHeader.AdvanceAutomatic = paidInAdvance;

                            //Store the new invoice
                                int? invoiceId = _invoiceController.AddInvoice(invHeader); 
                            if (invoiceId != null)
                            {
                                Logger.Log("Created new invoice for dist " + dist.Id);
                                
                                //Increment the invoice number
                                invoiceNum++;

                                

                                //Store the invoice details
                                foreach (BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail id in invoiceDetails)
                                {
                                    try
                                    {
                                        id.InvoiceId = (int)invoiceId;
                                        _invoiceController.AddInvoiceDetailLine(id);
                                    }
                                    catch (Exception ex)
                                    {
                                        Logger.Log("Saving invoice detail failed");
                                    }
                                }

                                //Store the new last invoice month for the distributor
                                //The day is set to 1 as only the year and month are relevant
                                dist.LastInvoiceDate = DateTime.Now;
                                if (!_boAccountingController.UpdateDistributor(dist))
                                {
                                    Logger.Log("Saving last invoice date failed for distributor: " + dist.Id + ", " + dist.FullName);
                                }
                            }
                            else
                            {
                                Logger.Log("Adding invoice failed for dist " + dist.Id);
                                success = false;
                            }
                        }
                        else
                        {
                            Logger.Log("No invoice required for dist " + dist.Id);
                        }
                    }
                    else
                    {
                        Logger.Log("Dist " + dist.Id + " should not be invoiced this month");
                    }
                }
            }

            Logger.Log("Create invoices stop");

            return success;
        }

        /// <summary>
        /// Effectively creates invoices
        /// This method is used for a specific month.
        /// The method does not take the distributors' situation into account, ie. Last Invoice Date and Invoice Interval
        /// </summary>
        /// <param name="Distributors">An array of distributors</param>
        /// <param name="monthNum">The (last) month to be invoiced</param>
        /// <param name="year">The year of the month ot be invoiced</param>
        /// <returns>True if succesfull</returns>
        public bool CreateInvoicesForSpecificMonth(Distributor[] Distributors, int monthNum, int year)
        {
            bool success = true;
            return success;
        }

        private decimal GetIncentive(decimal totalAmount, string currency)
        {
            decimal incentive = 0;

            if (currency == "EUR")
            {
                if (totalAmount <= 2850)
                {
                    incentive = 0;
                }
                else if (totalAmount >= 2851 && totalAmount <= 5000)
                {
                    incentive = (decimal)0.03;
                }
                else if (totalAmount >= 5001 && totalAmount <= 7140)
                {
                    incentive = (decimal)0.04;
                }
                else if (totalAmount >= 7141 && totalAmount <= 10715)
                {
                    incentive = (decimal)0.05;
                }
                else if (totalAmount >= 10716 && totalAmount <= 14285)
                {
                    incentive = (decimal)0.055;
                }
                else if (totalAmount >= 14286 && totalAmount <= 35715)
                {
                    incentive = (decimal)0.06;
                }
                else if (totalAmount >= 35716)
                {
                    incentive = (decimal)0.07;
                }

            }
            else if (currency == "USD")
            {
                if (totalAmount <= 3999)
                {
                    incentive = 0;
                }
                else if (totalAmount >= 4000 && totalAmount <= 6999)
                {
                    incentive = (decimal)0.03;
                }
                else if (totalAmount >= 7000 && totalAmount <= 9999)
                {
                    incentive = (decimal)0.04;
                }
                else if (totalAmount >= 10000 && totalAmount <= 14999)
                {
                    incentive = (decimal)0.05;
                }
                else if (totalAmount >= 15000 && totalAmount <= 19999)
                {
                    incentive = (decimal)0.055;
                }
                else if (totalAmount >= 20000 && totalAmount <= 49999)
                {
                    incentive = (decimal)0.06;
                }
                else if (totalAmount >= 50000)
                {
                    incentive = (decimal)0.07;
                }
            }

            return incentive;
        }
        
        #endregion
    }
}
