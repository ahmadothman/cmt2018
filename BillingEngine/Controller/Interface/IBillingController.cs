﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BillingEngine.BOAccountingControllerRef;

namespace BillingEngine.Controller.Interface
{
    public interface IBillingController
    {
        /// <summary>
        /// Updates the Distributors table of the invvoice databaase
        /// </summary>
        /// <param name="Distributors">List of distributors from the CMTData database</param>
        /// <returns>True if the operation succeeded</returns>
        Boolean SynchronizeDistributors(Distributor[] Distributors);

        /// <summary>
        /// Creates the invoices per distributor
        /// </summary>
        /// <param name="Distributors">The list of distributors</param>
        /// <param name="monthNum">The number of the month to invoice</param>
        /// <param name="year">The year of the invoice period</param>
        /// <returns></returns>
        Boolean CreateInvoices(Distributor[] Distributors, int monthNum, int year);
    }
}
