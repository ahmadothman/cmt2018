﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BillingEngine.Util;
using BillingEngine.Controller;
using BillingEngine.BOAccountingControllerRef;

namespace BillingEngine
{
    /// <summary>
    /// Creates the invoices from the TerminalActivityLog table
    /// </summary>
    class Program
    {
        BillingController _billingController = new BillingController();
        BOAccountingControlWS _accountingController = new BOAccountingControlWS();

        static void Main(string[] args)
        {
            Program prg = new Program();
            prg.mainBody(args);
        }

        /// <summary>
        /// Synopsis:
        ///     1. Update the Distributors table in the CMTData-Invoicing database
        ///     2. Get the last invoice number from the configuration table
        ///     3. For each of the distributors check the number of records in the activation table
        ///        and create a invoice header if the number of records is 1 or more.
        ///     4. Add the invoice lines to the InvoiceDetail table with the correct invoice Year/Number
        ///     5. Do until all distributors are handled.
        /// </summary>
        private void mainBody(string[] args)
        {
            int invoiceMonth = 0;
            int invoiceYear = 0;
            DateTime currDate = DateTime.Now;

            if (args.Length == 0)
            {
                if (currDate.Month == 1)
                {
                    invoiceMonth = 12;
                    invoiceYear = currDate.Year - 1; //Invoice period is previous year
                }
                else
                {
                    invoiceMonth = currDate.Month - 1;
                    invoiceYear = currDate.Year; //Invoice year is current year
                }

                this.CreateInvoices(currDate.Month, currDate.Year);
            }
            else
            {
                if (args.Length == 2)
                {
                    //First parameter is month, second parameter is year
                    invoiceMonth = Int32.Parse(args[0]);
                    invoiceYear = Int32.Parse(args[1]);

                    if (invoiceMonth > 0 && invoiceMonth < 12)
                    {
                        this.CreateInvoicesForSpecificMonth(invoiceMonth, invoiceYear);
                    }
                    else
                    {
                        Logger.Log("Specified month value not correct!");
                    }
                }
                else
                {
                    Logger.Log("Usage: BillingEngine [Month] [Year]");
                    Logger.Log("Program End");
                }
            }
        }

        /// <summary>
        /// Creates the invoices
        /// </summary>
        /// <param name="invoiceMonth"></param>
        /// <param name="invoiceYear"></param>
        private void CreateInvoices(int invoiceMonth, int invoiceYear)
        {
            Logger.Log("BillingEngine start", true);

            //Synchronize the distributors
            Distributor[] distributors = _accountingController.GetDistributors();
            //if (!_billingController.SynchronizeDistributors(distributors))
            //{
            //    Logger.Log("SynhronizeDistributors failed");
            //}
            //else
            //{
                _billingController.CreateInvoices(distributors, invoiceMonth, invoiceYear);
            //}

            Logger.Log("BillingEngine end", true);
        }

        /// <summary>
        /// Creates the invoices
        /// </summary>
        /// <param name="invoiceMonth"></param>
        /// <param name="invoiceYear"></param>
        private void CreateInvoicesForSpecificMonth(int invoiceMonth, int invoiceYear)
        {
            Logger.Log("BillingEngine start", true);

            Distributor[] distributors = _accountingController.GetDistributors();
            _billingController.CreateInvoicesForSpecificMonth(distributors, invoiceMonth, invoiceYear);
            
            Logger.Log("BillingEngine end", true);
        }
    }
}
