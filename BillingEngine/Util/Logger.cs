﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using BillingEngine.BOLogControllerRef;

namespace BillingEngine.Util
{
    /// <summary>
    /// Simple logger methods
    /// </summary>
    public class Logger
    {
        public static void Log(string msg, Boolean boLog = false)
        {
            if (boLog)
            {
                BOLogControlWS boLogControl = new BOLogControlWS();
                UserActivity userActivity = new UserActivity();
                userActivity.Action = msg;
                userActivity.ActionDate = DateTime.Now;
                //userActivity.UserId = Guid.Parse(BillingEngine.Default.AppId);
                boLogControl.LogUserActivity(userActivity);
            }

            Console.WriteLine("[" + DateTime.Now + "] BillingEngine - " + msg);
            
            //Also log to file
            string logFilePath = Properties.Settings.Default.LogFile;
            using (StreamWriter writer = new StreamWriter(logFilePath, true))
            {
                writer.WriteLine("[" + DateTime.Now + "]" + msg);
            }
        }
    }
}
