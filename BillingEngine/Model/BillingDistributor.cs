﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BillingEngine.Model
{
    /// <summary>
    /// Represent a Distributor in the CMTData-Invoice database
    /// </summary>
    public class BillingDistributor
    {
        /// <summary>
        /// The distributors' identifier (from CMTData)
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The distributors' fullname
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// The distributors' phone number
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// The distributors' fax number
        /// </summary>
        public string Fax { get; set; }

        /// <summary>
        /// The distributors' Vat
        /// </summary>
        public string Vat { get; set; }

        /// <summary>
        /// The distributors' first address line
        /// </summary>
        public string AddressLine1 { get; set; }

        /// <summary>
        /// The distributors second address line
        /// </summary>
        public string AddressLine2 { get; set; }

        /// <summary>
        /// The distributors' location
        /// </summary>
        public string Location { get; set; }

        /// <summary>
        /// The distributors' postal code
        /// </summary>
        public string PostalCode { get; set; }

        /// <summary>
        /// The distributors' country
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// The distributors' E-Mail
        /// </summary>
        public string EMail { get; set; }
    }
}
