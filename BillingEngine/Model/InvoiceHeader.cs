﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BillingEngine.Model
{
    /// <summary>
    /// Contains the properties of the invoice header
    /// </summary>
    public class InvoiceHeader
    {
        public int DistributorId { get; set; }

        /// <summary>
        ///  Month - Year - Sequence number
        /// </summary>
        public int InvoiceNumber { get; set; }

        public int InvoiceYear { get; set; }

        public int InvoiceMonth { get; set; }

        public DateTime InvoiceDate { get; set; }

    }
}
