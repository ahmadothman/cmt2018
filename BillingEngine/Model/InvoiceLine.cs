﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BillingEngine.Model
{
    /// <summary>
    /// Represents a single invoice line
    /// </summary>
    public class InvoiceLine
    {
        public int InvoiceYear { get; set; }
        public int InvoiceNum { get; set; }
        public int InvoiceMonth { get; set; }
        public int ItemId { get; set; }
        public string ItemDescription { get; set; }
        public float UnitPrice { get; set; }
        public int Items { get; set; }
        public float AmountUSD { get; set; }
        public float AmountEUR { get; set; }
        public string TerminalId { get; set; }
        public string ServicePack { get; set; }
        public string Serial { get; set; }
        public string MacAddress { get; set; }
        public string Status { get; set; }
    }
}
