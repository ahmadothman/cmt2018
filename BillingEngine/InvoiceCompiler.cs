﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BillingEngine.Model;
using BillingEngine.Util;
using BillingEngine.BOAccountingControllerRef;
using BillingEngine.BOLogControllerRef;
using BillingEngine.BOVoucherControllerRef;
using BillingEngine.BOInvoicingControllerWSRef;

namespace BillingEngine
{
    /// <summary>
    /// Compiles the invoice detail lines for a given distributor for a given month
    /// </summary>
    public class InvoiceCompiler
    {
        const int _UnlockedTerminalStatus = 2;
        const int _LockedTerminalStatus = 1;
        const int _DecommissionedTerminalStatus = 3;
        const int _TerminalSuspension = 200;
        const int _TerminalActivation = 100;
        const int _TerminalFUPReset = 700;
        const int _TerminalChangeSLA = 600;
        const int _TerminalReActivation = 300;
        const int _FreeZoneBillable = 1228;
        const int _VoIPBillable = 1233;
        const int _ActivationBillable = 1239;
        const int _ActivationBillableSoHo = 4013;
        const int _MultipleIPBillable = 4014;
        const int _ActivationBillableVNO = 4584;
        const int _ActivationBillableMUL = 4585;


        /// <summary>
        /// Generates the actual invoice lines for a given distirbutor for a given month/year
        /// </summary>
        /// Synopsis:
        /// 1. Get all the activated terminals for the given distributor and add them to the 
        ///    list of invoice lines
        /// 2. Get all FUP resets from the activity list and adds them to the list of invoice
        ///    lines 
        /// 3. Get all activations from the activity list and adds them to the list of invoice
        ///    lines
        /// 4. Get the suspensions from the activity list and calculate te prorata credits for 
        ///    suspensions of more than 15 days
        /// 5. Get the re-activations from the activity list and calculate the prorata debits/credits
        ///    for requests before the 10th of the month
        /// 6. Get the SLA upgrades/downgrades from the activity list and calculate the prorata debits/credits
        ///    for requests before the 10th of the month
        /// <remarks>
        /// </remarks>
        /// <param name="DistId">The distributor ID</param>
        /// <param name="Month">The month for which the invoice is generated</param>
        /// <param name="Year">The year for which the invoice is generated</param>
        /// <param name="Currency">The currency in which the distributor is invoiced</param>
        /// <param name="InvoiceNum">The number of the invoice for which the lines are generated</param>
        /// <returns></returns>
        public List<BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail> GenerateInvoiceLines(int DistId, int Month, int Year, string Currency, int InvoiceNum)
        {
            List<BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail> invoiceLines = new List<BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail>();

            //Create invoice lines for active terminals
            this.ActivationInvoiceLines(DistId, Month, Year, Currency, InvoiceNum, ref invoiceLines);
            this.FUPResetInvoiceLines(DistId, Month, Year, Currency, InvoiceNum, ref invoiceLines);
            this.SuspensionInvoiceLines(DistId, Month, Year, Currency, InvoiceNum, ref invoiceLines);
            this.ReActivationInvoiceLines(DistId, Month, Year, Currency, InvoiceNum, ref invoiceLines);
            this.SLAChangeInvoiceLines(DistId, Month, Year, Currency, InvoiceNum, ref invoiceLines);
            this.VoucherBatchInvoiceLines(DistId, Month, Year, Currency, InvoiceNum, ref invoiceLines);
            return invoiceLines;
        }

       
        /// <summary>
        /// Adds the activation fees to the invoice lines
        /// </summary>
        /// <param name="DistId">The distributor ID</param>
        /// <param name="Month">The month for which the invoice is generated</param>
        /// <param name="Year">The year for which the invoice is generated</param>
        /// <param name="Currency">The currency in which the distributor is invoiced</param>
        /// <param name="invoiceLines"></param>
        /// <returns></returns>
        private void ActivationInvoiceLines(int DistId, int Month, int Year, string Currency, int InvoiceNum, ref List<BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail> invoiceLines)
        {
            BOAccountingControlWS boAccountingController = new BOAccountingControlWS();
            BOLogControlWS boLogController = new BOLogControlWS();
            BOInvoicingControllerWS invoiceController = new BOInvoicingControllerWS();

            //Get the billable for new activations
            Billable actBillable = invoiceController.GetBillable(_ActivationBillable);
            Billable actBillableSoho = invoiceController.GetBillable(_ActivationBillableSoHo);
            Billable actBillableMUL = invoiceController.GetBillable(_ActivationBillableMUL);
            Billable actBillableVNO = invoiceController.GetBillable(_ActivationBillableVNO);
            //Get the price information for the distributor
            Billable distBillableAct = invoiceController.GetBillableForDistributor(actBillable.Bid, DistId);
            Billable distBillableActSoHo = invoiceController.GetBillableForDistributor(actBillableSoho.Bid, DistId);
            Billable distBillableActMUL = invoiceController.GetBillableForDistributor(actBillableMUL.Bid, DistId);
            Billable distBillableActVNO = invoiceController.GetBillableForDistributor(actBillableVNO.Bid, DistId);
            decimal amt = distBillableAct.PriceEU;
            decimal amtSoHo = distBillableActSoHo.PriceEU;
            decimal amtMUL = distBillableActMUL.PriceEU;
            decimal amtVNO = distBillableActVNO.PriceEU;
            //Set the price to Euro if applicable
            if (Currency == "USD")
            {
                amt = distBillableAct.PriceUSD;
                amtSoHo = distBillableActSoHo.PriceUSD;
                amtMUL = distBillableActMUL.PriceUSD;
                amtVNO = distBillableActVNO.PriceUSD;
            }
            
            //Get the terminals belonging to the given distributor
            Terminal[] terminals = boAccountingController.GetTerminalsByDistributorId(DistId);

            DateTime startDate = new DateTime(Year, Month, 1);
            DateTime endDate = new DateTime(Year, Month, DateTime.DaysInMonth(Year, Month), 23, 59, 59, 998);

            foreach (Terminal term in terminals)
            {
                TerminalActivity[] termActivities =
                    boLogController.GetTerminalActivityExt(startDate, endDate, _TerminalActivation, term.MacAddress);

                foreach(TerminalActivity termActivity in termActivities)
                {
                    ServiceLevel sl = boAccountingController.GetServicePack(Convert.ToInt32(termActivity.SlaName)); // use the SLA of the activation, which may differ from the curren SLA of the terminal
                    //Invoice line for activation fee
                    BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail ilActivation = new BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail();
                    ilActivation.InvoiceYear = Year;
                    ilActivation.InvoiceNum = InvoiceNum;
                    ilActivation.Bid = distBillableAct.Bid;
                    ilActivation.BillableDescription = "Activation";
                    ilActivation.UnitPrice = amt;
                    if (!termActivity.AccountingFlag || (bool)term.TestMode)
                    {
                        ilActivation.UnitPrice = 0;
                        ilActivation.BillableDescription = "Activation (non-billable)";
                    }
                    else if (sl.ServiceClass == 1 || sl.ServiceClass == 7) //SoHo terminals
                    {
                        ilActivation.UnitPrice = amtSoHo;
                        ilActivation.BillableDescription = "Activation SoHo terminal";
                        if (termActivity.Prepaid == true)
                        {
                            ilActivation.PaidInAdvance = true;
                            ilActivation.BillableDescription = "Activation SoHo terminal (Prepaid)";
                        }
                    }
                    else if (sl.ServiceClass == 5) //Multicast Service invoicing terminal activation
                    {
                        ilActivation.UnitPrice = amtMUL;
                        ilActivation.BillableDescription = "Activation Multicast Service";


                    }
                    else if (sl.ServiceClass == 6) //VNO service invoicing terminal activation 
                    {
                        ilActivation.UnitPrice = amtVNO;
                        ilActivation.BillableDescription = "Activation VNO service";
                    }
                    else if (sl.ServiceClass == 7) //Monthly SoHo terminals
                    {
                        ilActivation.UnitPrice = amtSoHo;
                        ilActivation.BillableDescription = "Activation Monthly SoHo terminal";
                    }
                    ilActivation.Items = 1;
                    ilActivation.TerminalId = term.FullName;
                    ilActivation.Serial = term.Serial;
                    ilActivation.ServicePack = sl.SlaName;
                    ilActivation.MacAddress = term.MacAddress;
                    ilActivation.Status = "Active";
                    ilActivation.ActivityDate = termActivity.ActionDate;
                    invoiceLines.Add(ilActivation);

                    //add invoice line for multiple IP addresses
                    if (term.IPRange != null)
                    {
                        if ((bool)term.IPRange)
                        {
                            int noOfAddresses = this.ipMaskToAddresses(term.IPMask) - 3;
                            if (noOfAddresses > 1)
                            {
                                BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail ilIP = new BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail();
                                ilIP.InvoiceYear = Year;
                                ilIP.InvoiceNum = InvoiceNum;
                                ilIP.Bid = distBillableAct.Bid;
                                ilIP.BillableDescription = "Activation multiple IPs";
                                if (Currency == "EUR")
                                {
                                    ilIP.UnitPrice = 35; 
                                }
                                else if (Currency == "USD")
                                {
                                    ilIP.UnitPrice = 50;
                                } 
                                if (!termActivity.AccountingFlag || (bool)term.TestMode)
                                {
                                    ilIP.UnitPrice = 0;
                                    ilIP.BillableDescription = "Activation multiple IPs (non-billable)";
                                }
                                ilIP.UnitPrice = (noOfAddresses - 1) * ilIP.UnitPrice;
                                ilIP.Items = 1;
                                ilIP.TerminalId = term.FullName;
                                ilIP.Serial = term.Serial;
                                ilIP.ServicePack = sl.SlaName;
                                ilIP.MacAddress = term.MacAddress;
                                ilIP.Status = "Active";
                                ilIP.ActivityDate = termActivity.ActionDate;
                                invoiceLines.Add(ilIP);
                            }
                        }
                    }

                    //Invoice line for monthly fee, pro-rate from the activation date
                    if (sl.ServiceClass != 1)
                    {
                        Billable distBillableSLA = invoiceController.GetBillableForDistributor((int)sl.BillableId, DistId);
                        decimal amtSLA = distBillableSLA.PriceEU;
                        if (Currency == "USD")
                        {
                            amtSLA = distBillableSLA.PriceUSD;
                        }
                        BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail ilMonthlyFee = new BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail();
                        ilMonthlyFee.InvoiceYear = Year;
                        ilMonthlyFee.InvoiceNum = InvoiceNum; 
                        ilMonthlyFee.Bid = distBillableSLA.Bid;
                        ilMonthlyFee.BillableDescription = "Monthly fee, pro-rata";
                        decimal daysInMonth = (decimal)(DateTime.DaysInMonth(termActivity.ActionDate.Year, termActivity.ActionDate.Month));
                        ilMonthlyFee.UnitPrice = amtSLA * ((daysInMonth - (decimal)termActivity.ActionDate.Day) / daysInMonth)+ amtSLA*((DateTime.Now.Year - termActivity.ActionDate.Year)*12+ DateTime.Now.Month - termActivity.ActionDate.Month-1);
                        if (!termActivity.AccountingFlag || (bool)term.TestMode)
                        {
                            ilMonthlyFee.UnitPrice = 0;
                            ilMonthlyFee.BillableDescription = "Monthly fee (non-billable)";
                        }
                        ilMonthlyFee.Items = 1;
                        ilMonthlyFee.TerminalId = term.FullName;
                        ilMonthlyFee.Serial = term.Serial;
                        ilMonthlyFee.ServicePack = sl.SlaName;
                        ilMonthlyFee.MacAddress = term.MacAddress;
                        ilMonthlyFee.Status = "Active";
                        ilMonthlyFee.ActivityDate = termActivity.ActionDate;
                        invoiceLines.Add(ilMonthlyFee);
                    }
                }
            }
        }

        /// <summary>
        /// Adds the suspensions to the invoice lines if terminal is still suspended at the end of the month
        /// For Corporate terminals, credit is given for the unused days that are left in the month
        /// </summary>
        /// <param name="DistId">The distributor ID</param>
        /// <param name="Month">The month for which the invoice is generated</param>
        /// <param name="Year">The year for which the invoice is generated</param>
        /// <param name="invoiceLines"></param>
        /// <returns></returns>
        private void SuspensionInvoiceLines(int DistId, int Month, int Year, string Currency, int InvoiceNum, ref List<BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail> invoiceLines)
        {
            BOAccountingControlWS boAccountingController = new BOAccountingControlWS();
            BOLogControlWS boLogController = new BOLogControlWS();
            BOInvoicingControllerWS invoiceController = new BOInvoicingControllerWS();

            //Get the terminals belonging to the given distributor
            Terminal[] terminals = boAccountingController.GetTerminalsByDistributorId(DistId);

            DateTime startDate = new DateTime(Year, Month, 1);
            DateTime endDate = new DateTime(Year, Month, DateTime.DaysInMonth(Year, Month), 23, 59, 59, 998);

            foreach (Terminal term in terminals)
            {
                TerminalActivity[] termActivities = boLogController.GetTerminalActivityExt(startDate, endDate, _TerminalSuspension, term.MacAddress);
                if (termActivities.Length > 0)
                {
                    ServiceLevel sl = boAccountingController.GetServicePack((int)term.SlaId);
                    if (sl == null)
                    {
                        sl = new ServiceLevel();
                        sl.ServiceClass = 0;
                    }

                    if (sl.ServiceClass == 2 || sl.ServiceClass == 5 || sl.ServiceClass == 6) // Business terminals /VNO Service /Multicast Service
                    {
                        foreach (TerminalActivity termActivity in termActivities)
                        {
                            //create the invoice line
                            BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail ilSuspension = new BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail();
                            ilSuspension.ActivityDate = termActivity.ActionDate;
                            ilSuspension.InvoiceYear = Year;
                            ilSuspension.InvoiceNum = InvoiceNum;
                            ilSuspension.BillableDescription = "Suspension";
                            ilSuspension.UnitPrice = 0;
                            ilSuspension.Items = 1;
                            ilSuspension.TerminalId = term.FullName;
                            ilSuspension.Serial = term.Serial;
                            ilSuspension.ServicePack = sl.SlaName;
                            ilSuspension.MacAddress = term.MacAddress;
                            ilSuspension.Status = "Suspended";
                            invoiceLines.Add(ilSuspension);
                        }
                    }
                    else if (sl.ServiceClass == 3) //Corporate terminals
                    {
                        foreach (TerminalActivity termActivity in termActivities)
                        {
                            //initialize the invoice line
                            BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail ilSuspension = new BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail();
                            ilSuspension.ActivityDate = termActivity.ActionDate;
                            ilSuspension.InvoiceYear = Year;
                            ilSuspension.InvoiceNum = InvoiceNum;
                            ilSuspension.BillableDescription = "Suspension";
                            ilSuspension.Items = 1;
                            ilSuspension.TerminalId = term.FullName;
                            ilSuspension.Serial = term.Serial;
                            ilSuspension.ServicePack = sl.SlaName;
                            ilSuspension.MacAddress = term.MacAddress;
                            ilSuspension.Status = "Suspended";

                            Billable distBillableSLA = invoiceController.GetBillableForDistributor((int)sl.BillableId, DistId);
                            decimal amtSLA = distBillableSLA.PriceEU;
                            if (Currency == "USD")
                            {
                                amtSLA = distBillableSLA.PriceUSD;
                            }
                            if (term.TestMode != null)
                            {
                                if ((bool)term.TestMode)
                                {
                                    amtSLA = 0;
                                }
                            }
                            //Suspensions are credited with the remaining days of the month
                            DateTime suspDate = (DateTime)ilSuspension.ActivityDate;
                            decimal daysInMonth = (decimal)(DateTime.DaysInMonth(Year, Month));
                            ilSuspension.UnitPrice = -1 * amtSLA * ((daysInMonth - (decimal)suspDate.Day) / daysInMonth);
                            invoiceLines.Add(ilSuspension);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Creates invoice lines from the FUP reset activity lines for a given month and distributor
        /// The price for a FUP reset is the SLA monthly fee divided by 2
        /// </summary>
        /// <param name="DistId">The distributor Id</param>
        /// <param name="Month">Invoice month</param>
        /// <param name="Year">Invoice year</param>
        /// <param name="Currency">The currency in which the distributor is invoiced</param>
        /// <param name="invoiceLines">The invoice lines</param>
        private void FUPResetInvoiceLines(int DistId, int Month, int Year, string Currency, int InvoiceNum, ref List<BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail> invoiceLines)
        {
            BOAccountingControlWS boAccountingController = new BOAccountingControlWS();
            BOLogControlWS boLogController = new BOLogControlWS();
            BOInvoicingControllerWS invoiceController = new BOInvoicingControllerWS();

            //Get the terminals belonging to the given distributor
            Terminal[] terminals = boAccountingController.GetTerminalsByDistributorId(DistId);

            DateTime startDate = new DateTime(Year, Month, 1);
            DateTime endDate = new DateTime(Year, Month, DateTime.DaysInMonth(Year, Month), 23, 59, 59, 998);

            foreach (Terminal term in terminals)
            {
                TerminalActivity[] termActivities = boLogController.GetTerminalActivityExt(startDate, endDate, _TerminalFUPReset, term.MacAddress);

                foreach (TerminalActivity termActivity in termActivities)
                {
                    BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail ilFUPReset = new BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail();
                    Terminal invTerm = boAccountingController.GetTerminalDetailsByMAC(term.MacAddress);

                    //Get the default price for the SLA
                    ServiceLevel sl = boAccountingController.GetServicePack((int)term.SlaId);
                    Billable slaBillable = invoiceController.GetBillable((int)sl.BillableId);
                    //Get the price information for the distributor
                    Billable distBillable = invoiceController.GetBillableForDistributor(slaBillable.Bid, DistId);

                    ilFUPReset.InvoiceYear = Year;
                    ilFUPReset.InvoiceNum = InvoiceNum;
                    ilFUPReset.Bid = distBillable.Bid;
                    ilFUPReset.BillableDescription = "Reset";
                    if (Currency == "USD")
                    {
                        ilFUPReset.UnitPrice = distBillable.PriceUSD / 2;
                    }
                    else
                    {
                        ilFUPReset.UnitPrice = distBillable.PriceEU / 2;
                    }
                    if (!termActivity.AccountingFlag || (bool)invTerm.TestMode)
                    {
                        ilFUPReset.UnitPrice = 0;
                        ilFUPReset.BillableDescription = "Reset (non-billable)";
                    }
                    ilFUPReset.Items = 1;
                    ilFUPReset.TerminalId = invTerm.FullName;
                    ilFUPReset.Serial = invTerm.Serial;
                    ilFUPReset.ServicePack = sl.SlaName;
                    ilFUPReset.MacAddress = invTerm.MacAddress;
                    ilFUPReset.Status = "Active";
                    ilFUPReset.ActivityDate = termActivity.ActionDate;
                    invoiceLines.Add(ilFUPReset);
                }
            }
        }

        /// <summary>
        /// Creates invoice lines from the re-activation activity lines for a given month and distributor
        /// Only the earliest re-activation of a terminal is taken into account
        /// </summary>
        /// <param name="DistId">The distributor Id</param>
        /// <param name="Month">Invoice month</param>
        /// <param name="Year">Invoice year</param>
        /// <param name="Currency">The currency in which the distributor is invoiced</param>
        /// <param name="invoiceLines">The invoice lines</param>
        private void ReActivationInvoiceLines(int DistId, int Month, int Year, string Currency, int InvoiceNum, ref List<BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail> invoiceLines)
        {
            BOAccountingControlWS boAccountingController = new BOAccountingControlWS();
            BOLogControlWS boLogController = new BOLogControlWS();
            BOInvoicingControllerWS invoiceController = new BOInvoicingControllerWS();

            //Get the terminals belonging to the given distributor
            Terminal[] terminals = boAccountingController.GetTerminalsByDistributorId(DistId);

            DateTime startDate = new DateTime(Year, Month, 1);
            DateTime endDate = new DateTime(Year, Month, DateTime.DaysInMonth(Year, Month), 23, 59, 59, 998);

            foreach (Terminal term in terminals)
            {
                TerminalActivity[] termActivities = boLogController.GetTerminalActivityExt(startDate, endDate, _TerminalReActivation, term.MacAddress);

                if (termActivities.Length > 0)
                {
                    //Get the default price for the SLA
                    ServiceLevel sl = boAccountingController.GetServicePack((int)term.SlaId);
                    Billable slaBillable = invoiceController.GetBillable((int)sl.BillableId);
                    //Get the price information for the distributor
                    Billable distBillable = invoiceController.GetBillableForDistributor(slaBillable.Bid, DistId);
                    decimal amt = distBillable.PriceEU;
                    //Set the price to USD if applicable
                    if (Currency == "USD")
                    {
                        amt = distBillable.PriceUSD;
                    }

                    //Initialize the invoice line
                    BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail ilReActivation = new BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail();
                    ilReActivation.InvoiceYear = Year;
                    ilReActivation.InvoiceNum = InvoiceNum;
                    ilReActivation.Bid = distBillable.Bid;
                    ilReActivation.BillableDescription = "Re-activation";
                    ilReActivation.Items = 1;
                    ilReActivation.TerminalId = term.FullName;
                    ilReActivation.Serial = term.Serial;
                    ilReActivation.ServicePack = sl.SlaName;
                    ilReActivation.MacAddress = term.MacAddress;
                    ilReActivation.Status = "Reactivated";
                    ilReActivation.ActivityDate = endDate;

                    //Make sure the earliest reactivation is used 
                    foreach (TerminalActivity termActivity in termActivities)
                    {
                        if (termActivity.ActionDate <= ilReActivation.ActivityDate)
                        {
                            ilReActivation.ActivityDate = termActivity.ActionDate;
                        }
                    }

                    //only charge the days that were actually used
                    if (term.TestMode != null)
                    {
                        if ((bool)term.TestMode)
                        {
                            amt = 0;
                            ilReActivation.BillableDescription = "Re-activation (non-billable)";
                        }
                    }
                    
                    if (sl.ServiceClass == 1) //SoHo terminals
                    {
                        amt = 0;
                        ilReActivation.BillableDescription = "Re-activation SoHo terminal";
                    }
                    DateTime dateReActivation = (DateTime)ilReActivation.ActivityDate;

                    if (sl.ServiceClass == 7) // for Monthly SoHo a full amount will be counted instead of pro-rata
                    {
                        ilReActivation.UnitPrice = amt * ((DateTime.Now.Year - dateReActivation.Year) * 12 + DateTime.Now.Month - dateReActivation.Month);
                    }
                    else
                    {
                        decimal daysInMonth = (decimal)(DateTime.DaysInMonth(dateReActivation.Year, dateReActivation.Month));
                        ilReActivation.UnitPrice = amt * ((daysInMonth - (decimal)dateReActivation.Day) / daysInMonth) + amt * ((DateTime.Now.Year - dateReActivation.Year) * 12 + DateTime.Now.Month - dateReActivation.Month - 1);
                    }
                    invoiceLines.Add(ilReActivation);
                }
            }
        }

        /// <summary>
        /// Create monthly invoice lines for active terminals which follow the distributor's invoicing periodicity
        /// </summary>
        /// <param name="DistId">The Distributor ID</param>
        /// <param name="Month">The invoice month</param>
        /// <param name="Year">The invoice year</param>
        /// <param name="Currency">The currency in which the distributor is invoiced</param>
        /// <param name="invoiceLines">The invoice lines</param>
        /// <returns>A list of invoice details</returns>
        public List<BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail> ActiveTerminalsInvoiceLines(int DistId, int Month, int Year, string Currency, int InvoiceNum)
        {
            BOAccountingControlWS boAccountingController = new BOAccountingControlWS();
            BOInvoicingControllerWS invoiceController = new BOInvoicingControllerWS();

            AdvancedQueryParams advQueryParams = new AdvancedQueryParams();
            advQueryParams.AdmState = _UnlockedTerminalStatus;
            advQueryParams.DistributorId = DistId;
            advQueryParams.IspId = -1;
            advQueryParams.SitId = -1;
            advQueryParams.SlaId = -1;
            advQueryParams.FullName = "";
            advQueryParams.IpAddress = "";
            advQueryParams.MacAddress = "";
            advQueryParams.FwdPool = -1;
            advQueryParams.RtnPool = -1;
            advQueryParams.ExpDate = new DateTime(0L);
            advQueryParams.ExpDateOper = "";
            advQueryParams.EMail = "";
            advQueryParams.Serial = "";
            advQueryParams.SatelliteId = -1;
            List<BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail> invoiceLines = new List<BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail>();

            Terminal[] activeTerminals = boAccountingController.GetTerminalsWithAdvancedQuery(advQueryParams);

            if (activeTerminals.Length > 0)
            {
                foreach (Terminal term in activeTerminals)
                {
                    BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail ilMonthlyFee = new BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail();
                    Terminal invTerm = boAccountingController.GetTerminalDetailsByMAC(term.MacAddress);

                    //check if terminal is due to be invoiced
                    if (!invTerm.TerminalInvoicing)
                    {
                        //Add a line for the monthly fee.
                        //Get the default price for the SLA
                        ServiceLevel sl = boAccountingController.GetServicePack((int)invTerm.SlaId);

                        if (sl.ServiceClass != 1)
                        {
                            Billable defBillable = invoiceController.GetBillable((int)sl.BillableId);
                            //Get the price information for the distributor
                            Billable distBillable = invoiceController.GetBillableForDistributor(defBillable.Bid, DistId);
                            decimal amt = distBillable.PriceEU;
                            //Set the price to USD if applicable
                            if (Currency == "USD")
                            {
                                amt = distBillable.PriceUSD;
                            }

                            ilMonthlyFee.InvoiceYear = Year;
                            ilMonthlyFee.InvoiceNum = InvoiceNum;
                            ilMonthlyFee.Bid = distBillable.Bid;
                            ilMonthlyFee.BillableDescription = "Monthly fee";
                            ilMonthlyFee.UnitPrice = amt;
                            if (invTerm.TestMode != null)
                            {
                                if ((bool)invTerm.TestMode)
                                {
                                    ilMonthlyFee.UnitPrice = 0;
                                    ilMonthlyFee.BillableDescription = "Monthly fee (non-billable)";
                                }
                            }
                            ilMonthlyFee.Items = 1;
                            ilMonthlyFee.TerminalId = invTerm.FullName;
                            ilMonthlyFee.Serial = invTerm.Serial;
                            ilMonthlyFee.ServicePack = sl.SlaName;
                            ilMonthlyFee.MacAddress = invTerm.MacAddress;
                            ilMonthlyFee.Status = "Active";
                            ilMonthlyFee.ActivityDate = new DateTime(Year, Month, 1);
                            invoiceLines.Add(ilMonthlyFee);

                            //add invoice line for multiple IP addresses
                            if (invTerm.IPRange != null)
                            {
                                if ((bool)invTerm.IPRange)
                                {
                                    int noOfAddresses = this.ipMaskToAddresses(invTerm.IPMask) - 3;
                                    if (noOfAddresses > 1)
                                    {
                                        distBillable = invoiceController.GetBillableForDistributor(_MultipleIPBillable, DistId); 
                                        BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail ilIP = new BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail();
                                        ilIP.InvoiceYear = Year;
                                        ilIP.InvoiceNum = InvoiceNum;
                                        ilIP.Bid = distBillable.Bid;
                                        ilIP.BillableDescription = "Multiple IPs";
                                        if (Currency == "EUR")
                                        {
                                            ilIP.UnitPrice = distBillable.PriceEU;
                                        }
                                        else if (Currency == "USD")
                                        {
                                            ilIP.UnitPrice = distBillable.PriceUSD;
                                        }
                                        if ((bool)invTerm.TestMode)
                                        {
                                            ilIP.UnitPrice = 0;
                                            ilIP.BillableDescription = "Multiple IPs (non-billable)";
                                        }
                                        ilIP.UnitPrice = (noOfAddresses - 1) * ilIP.UnitPrice;
                                        ilIP.Items = 1;
                                        ilIP.TerminalId = invTerm.FullName;
                                        ilIP.Serial = invTerm.Serial;
                                        ilIP.ServicePack = sl.SlaName;
                                        ilIP.MacAddress = invTerm.MacAddress;
                                        ilIP.Status = "Active";
                                        ilIP.ActivityDate = new DateTime(Year, Month, 1);
                                        invoiceLines.Add(ilIP);
                                    }
                                }
                            }
                        }
                        else
                        {
                            ilMonthlyFee.InvoiceYear = Year;
                            ilMonthlyFee.InvoiceNum = InvoiceNum;
                            ilMonthlyFee.BillableDescription = "Monthly fee";
                            ilMonthlyFee.UnitPrice = 0;
                            ilMonthlyFee.Items = 1;
                            ilMonthlyFee.TerminalId = invTerm.FullName;
                            ilMonthlyFee.Serial = invTerm.Serial;
                            ilMonthlyFee.ServicePack = sl.SlaName;
                            ilMonthlyFee.MacAddress = invTerm.MacAddress;
                            ilMonthlyFee.Status = "Active";
                            ilMonthlyFee.ActivityDate = new DateTime(Year, Month, 1);
                            invoiceLines.Add(ilMonthlyFee);
                        }
                    }
                }
            }

            return invoiceLines;
        }

        /// <summary>
        /// Create invoice lines for active terminals which follow individual periodicity
        /// </summary>
        /// <param name="DistId">The Distributor ID</param>
        /// <param name="Month">The invoice month</param>
        /// <param name="Year">The invoice year</param>
        /// <param name="Currency">The currency in which the distributor is invoiced</param>
        /// <param name="invoiceLines">The invoice lines</param>
        /// <returns>A list of invoice details</returns>
        public List<BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail> ActiveTerminalsTerminalInvoicing(int DistId, int Month, int Year, string Currency, int InvoiceNum)
        {
            BOAccountingControlWS boAccountingController = new BOAccountingControlWS();
            BOInvoicingControllerWS invoiceController = new BOInvoicingControllerWS();

            AdvancedQueryParams advQueryParams = new AdvancedQueryParams();
            advQueryParams.AdmState = _UnlockedTerminalStatus;
            advQueryParams.DistributorId = DistId;
            advQueryParams.IspId = -1;
            advQueryParams.SitId = -1;
            advQueryParams.SlaId = -1;
            advQueryParams.FullName = "";
            advQueryParams.IpAddress = "";
            advQueryParams.MacAddress = "";
            advQueryParams.FwdPool = -1;
            advQueryParams.RtnPool = -1;
            advQueryParams.ExpDate = new DateTime(0L);
            advQueryParams.ExpDateOper = "";
            advQueryParams.EMail = "";
            advQueryParams.Serial = "";
            advQueryParams.SatelliteId = -1;
            List<BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail> invoiceLines = new List<BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail>();

            Terminal[] activeTerminals = boAccountingController.GetTerminalsWithAdvancedQuery(advQueryParams);

            if (activeTerminals.Length > 0)
            {
                foreach (Terminal term in activeTerminals)
                {
                    
                    BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail ilMonthlyFee = new BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail();
                    Terminal invTerm = boAccountingController.GetTerminalDetailsByMAC(term.MacAddress);

                    if (invTerm.TerminalInvoicing)
                    {
                        //calculate the next terminal invoicing month
                        int nextInvoiceMonth = 0;
                        int nextInvoiceYear = 0;
                        if (invTerm.TerminalInvoicing)
                        {
                            DateTime lastInvoiceDate;
                            if (invTerm.LastInvoiceDate != null)
                            {
                                lastInvoiceDate = (DateTime)(invTerm.LastInvoiceDate);
                            }
                            else
                            {
                                lastInvoiceDate = DateTime.Now.AddMonths(-1);
                            }
                            
                            nextInvoiceMonth = lastInvoiceDate.Month + invTerm.InvoiceInterval;
                            nextInvoiceYear = lastInvoiceDate.Year;
                            //compensate for new year
                            if (nextInvoiceMonth > 12)
                            {
                                nextInvoiceMonth = nextInvoiceMonth - 12;
                                nextInvoiceYear = nextInvoiceYear + 1;
                            }
                        }

                        //check if terminal is due to be invoiced
                        if (nextInvoiceMonth == Month && nextInvoiceYear == Year)
                        {
                            //loop through all the months of the interval
                            //works also if interval = 1
                            for (int i = 0; i < invTerm.InvoiceInterval; i++)
                            {
                                ilMonthlyFee = new BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail();
                                DateTime activityDate = new DateTime(Year, Month, 1).AddMonths(i);
                                //Add a line for the monthly fee.
                                //Get the default price for the SLA
                                ServiceLevel sl = boAccountingController.GetServicePack((int)invTerm.SlaId);

                                if (sl.ServiceClass != 1) //Business and Corporate terminals
                                {
                                    Billable defBillable = invoiceController.GetBillable((int)sl.BillableId);
                                    //Get the price information for the distributor
                                    Billable distBillable = invoiceController.GetBillableForDistributor(defBillable.Bid, DistId);
                                    decimal amt = distBillable.PriceEU;
                                    //Set the price to USD if applicable
                                    if (Currency == "USD")
                                    {
                                        amt = distBillable.PriceUSD;
                                    }

                                    ilMonthlyFee.InvoiceYear = Year;
                                    ilMonthlyFee.InvoiceNum = InvoiceNum;
                                    ilMonthlyFee.Bid = distBillable.Bid;
                                    ilMonthlyFee.BillableDescription = "Monthly fee";
                                    ilMonthlyFee.UnitPrice = amt;
                                    if (invTerm.TestMode != null)
                                    {
                                        if ((bool)invTerm.TestMode)
                                        {
                                            ilMonthlyFee.UnitPrice = 0;
                                            ilMonthlyFee.BillableDescription = "Monthly fee (non-billable)";
                                        }
                                    }
                                    ilMonthlyFee.Items = 1;
                                    ilMonthlyFee.TerminalId = invTerm.FullName;
                                    ilMonthlyFee.Serial = invTerm.Serial;
                                    ilMonthlyFee.ServicePack = sl.SlaName;
                                    ilMonthlyFee.MacAddress = invTerm.MacAddress;
                                    ilMonthlyFee.Status = "Active";
                                    ilMonthlyFee.ActivityDate = activityDate;
                                    invoiceLines.Add(ilMonthlyFee);

                                    
                                    //add invoice line for multiple IP addresses
                                    if (invTerm.IPRange != null)
                                    {
                                        if ((bool)invTerm.IPRange)
                                        {
                                            int noOfAddresses = this.ipMaskToAddresses(invTerm.IPMask) - 3;
                                            if (noOfAddresses > 1)
                                            {
                                                distBillable =  invoiceController.GetBillableForDistributor(_MultipleIPBillable, DistId);
                                                BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail ilIP = new BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail();
                                                ilIP.InvoiceYear = Year;
                                                ilIP.InvoiceNum = InvoiceNum;
                                                ilIP.Bid = distBillable.Bid;
                                                ilIP.BillableDescription = "Multiple IPs";
                                                if (Currency == "EUR")
                                                {
                                                    ilIP.UnitPrice = distBillable.PriceEU;
                                                }
                                                else if (Currency == "USD")
                                                {
                                                    ilIP.UnitPrice = distBillable.PriceUSD;
                                                }
                                                if ((bool)invTerm.TestMode)
                                                {
                                                    ilIP.UnitPrice = 0;
                                                    ilIP.BillableDescription = "Multiple IPs (non-billable)";
                                                }
                                                ilIP.UnitPrice = (noOfAddresses - 1) * ilIP.UnitPrice;
                                                ilIP.Items = 1;
                                                ilIP.TerminalId = invTerm.FullName;
                                                ilIP.Serial = invTerm.Serial;
                                                ilIP.ServicePack = sl.SlaName;
                                                ilIP.MacAddress = invTerm.MacAddress;
                                                ilIP.Status = "Active";
                                                ilIP.ActivityDate = activityDate;
                                                invoiceLines.Add(ilIP);
                                            }
                                        }
                                    }
                                }
                                else //SoHo terminals
                                {
                                    ilMonthlyFee.InvoiceYear = Year;
                                    ilMonthlyFee.InvoiceNum = InvoiceNum;
                                    ilMonthlyFee.BillableDescription = "SoHo terminal";
                                    ilMonthlyFee.UnitPrice = 0;
                                    ilMonthlyFee.Items = 1;
                                    ilMonthlyFee.TerminalId = invTerm.FullName;
                                    ilMonthlyFee.Serial = invTerm.Serial;
                                    ilMonthlyFee.ServicePack = sl.SlaName;
                                    ilMonthlyFee.MacAddress = invTerm.MacAddress;
                                    ilMonthlyFee.Status = "Active";
                                    ilMonthlyFee.ActivityDate = activityDate;
                                    invoiceLines.Add(ilMonthlyFee);
                                }
                            }

                            //store the new invoice date
                            invTerm.LastInvoiceDate = DateTime.Now;
                            try
                            {
                                boAccountingController.UpdateTerminal(invTerm);
                            }
                            catch
                            {
                                Logger.Log("Unable to store invoice date for terminal: " + invTerm.MacAddress);
                            }
                        }
                    }
                }
            }

            return invoiceLines;
        }

        /// <summary>
        /// Create invoice lines for terminals that were activated in months prior to the past month
        /// for distributors that aren't invoiced monthly
        /// </summary>
        /// <param name="DistId">The Distributor ID</param>
        /// <param name="Month">The month in which the terminal was activated</param>
        /// <param name="Year">The year in which the terminal was activated</param>
        /// <param name="Currency">The currency in which the distributor is invoiced</param>
        /// <param name="invoiceLines">The invoice lines</param>
        /// <param name="InvoiceNum">The invoice number</param>
        /// <param name="numOfMonths">The number of months in the past for which the terminal must be invoiced</param>
        /// <returns>A list of invoice details</returns>
        public List<BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail> PastActivatedTerminalsInvoiceLines(int DistId, int Month, int Year, string Currency, int InvoiceNum, int numOfMonths)
        {
            BOAccountingControlWS boAccountingController = new BOAccountingControlWS();
            BOLogControlWS boLogController = new BOLogControlWS();
            BOInvoicingControllerWS invoiceController = new BOInvoicingControllerWS();
            List<BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail> invoiceLines = new List<BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail>();
            Terminal[] terminals = boAccountingController.GetTerminalsByDistributorId(DistId);

            DateTime startDate = new DateTime(Year, Month, 1);
            DateTime endDate = new DateTime(Year, Month, DateTime.DaysInMonth(Year, Month), 23, 59, 59, 998);

            foreach (Terminal term in terminals)
            {
                TerminalActivity[] termActivities =
                    boLogController.GetTerminalActivityExt(startDate, endDate, _TerminalActivation, term.MacAddress);

                foreach (TerminalActivity termActivity in termActivities)
                {
                    if (term.AdmStatus == _UnlockedTerminalStatus)
                    {
                        for (int i = 0; i < numOfMonths; i++)
                        {
                            int billingMonth = Month + i + 1;
                            int billingYear = Year;
                            if (billingMonth > 12)
                            {
                                billingMonth = billingMonth - 12;
                                billingYear = billingYear + 1;
                            }
                            //Add a line for the monthly fee.
                            BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail ilMonthlyFee = new BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail();
                            //Get the default price for the SLA
                            ServiceLevel sl = boAccountingController.GetServicePack((int)term.SlaId);

                            if (sl.ServiceClass != 1)
                            {
                                Billable defBillable = invoiceController.GetBillable((int)sl.BillableId);
                                //Get the price information for the distributor
                                Billable distBillable = invoiceController.GetBillableForDistributor(defBillable.Bid, DistId);
                                decimal amt = distBillable.PriceEU;
                                //Set the price to USD if applicable
                                if (Currency == "USD")
                                {
                                    amt = distBillable.PriceUSD;
                                }

                                ilMonthlyFee.InvoiceYear = Year;
                                ilMonthlyFee.InvoiceNum = InvoiceNum;
                                ilMonthlyFee.Bid = distBillable.Bid;
                                ilMonthlyFee.BillableDescription = "Monthly fee";
                                ilMonthlyFee.UnitPrice = amt;
                                if (term.TestMode != null)
                                {
                                    if ((bool)term.TestMode)
                                    {
                                        ilMonthlyFee.UnitPrice = 0;
                                        ilMonthlyFee.BillableDescription = "Monthly fee (non-billable)";
                                    }
                                }
                                ilMonthlyFee.Items = 1;
                                ilMonthlyFee.TerminalId = term.FullName;
                                ilMonthlyFee.Serial = term.Serial;
                                ilMonthlyFee.ServicePack = sl.SlaName;
                                ilMonthlyFee.MacAddress = term.MacAddress;
                                ilMonthlyFee.Status = "Active";
                                ilMonthlyFee.ActivityDate = new DateTime(billingYear, billingMonth, 1);
                                invoiceLines.Add(ilMonthlyFee);
                               
                                //add invoice line for multiple IP addresses
                                if (term.IPRange != null)
                                {
                                    if ((bool)term.IPRange)
                                    {
                                        int noOfAddresses = this.ipMaskToAddresses(term.IPMask) - 3;
                                        if (noOfAddresses > 1)
                                        {
                                            distBillable = invoiceController.GetBillableForDistributor(_MultipleIPBillable, DistId);
                                            BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail ilIP = new BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail();
                                            ilIP.InvoiceYear = Year;
                                            ilIP.InvoiceNum = InvoiceNum;
                                            ilIP.Bid = distBillable.Bid;
                                            ilIP.BillableDescription = "Multiple IPs";
                                            if (Currency == "EUR")
                                            {
                                                ilIP.UnitPrice = distBillable.PriceEU;
                                            }
                                            else if (Currency == "USD")
                                            {
                                                ilIP.UnitPrice = distBillable.PriceUSD;
                                            }
                                            if ((bool)term.TestMode)
                                            {
                                                ilIP.UnitPrice = 0;
                                                ilIP.BillableDescription = "Multiple IPs (non-billable)";
                                            }
                                            ilIP.UnitPrice = (noOfAddresses - 1) * ilIP.UnitPrice;
                                            ilIP.Items = 1;
                                            ilIP.TerminalId = term.FullName;
                                            ilIP.Serial = term.Serial;
                                            ilIP.ServicePack = sl.SlaName;
                                            ilIP.MacAddress = term.MacAddress;
                                            ilIP.Status = "Active";
                                            ilIP.ActivityDate = new DateTime(billingYear, billingMonth, 1);
                                            invoiceLines.Add(ilIP);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                ilMonthlyFee.InvoiceYear = Year;
                                ilMonthlyFee.InvoiceNum = InvoiceNum;
                                ilMonthlyFee.BillableDescription = "Monthly fee";
                                ilMonthlyFee.UnitPrice = 0;
                                ilMonthlyFee.Items = 1;
                                ilMonthlyFee.TerminalId = term.FullName;
                                ilMonthlyFee.Serial = term.Serial;
                                ilMonthlyFee.ServicePack = sl.SlaName;
                                ilMonthlyFee.MacAddress = term.MacAddress;
                                ilMonthlyFee.Status = "Active";
                                ilMonthlyFee.ActivityDate = new DateTime(billingYear, billingMonth, 1);
                                invoiceLines.Add(ilMonthlyFee);
                            }
                        }
                    }
                }
            }

            return invoiceLines;
        }

        /// <summary>
        /// Creates invoice lines for SLA change activity lines for a given month and distributor
        /// All SLA changes for a terminal is taken into account
        /// </summary>
        /// <param name="DistId">The distributor Id</param>
        /// <param name="Month">Invoice month</param>
        /// <param name="Year">Invoice year</param>
        /// <param name="Currency">The currency in which the distributor is invoiced</param>
        /// <param name="invoiceLines">The invoice lines</param>
        private void SLAChangeInvoiceLines(int DistId, int Month, int Year, string Currency, int InvoiceNum, ref List<BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail> invoiceLines)
        {
            BOAccountingControlWS boAccountingController = new BOAccountingControlWS();
            BOLogControlWS boLogController = new BOLogControlWS();
            BOInvoicingControllerWS invoiceController = new BOInvoicingControllerWS();

            //Get the terminals belonging to the given distributor
            Terminal[] terminals = boAccountingController.GetTerminalsByDistributorId(DistId);

            DateTime startDate = new DateTime(Year, Month, 1);
            DateTime endDate = new DateTime(Year, Month, DateTime.DaysInMonth(Year, Month), 23, 59, 59, 998);

            foreach (Terminal term in terminals)
            {
                TerminalActivity[] termActivities = boLogController.GetTerminalActivityExt(startDate, endDate, _TerminalChangeSLA, term.MacAddress);
                //one SLA change during the month
                if (termActivities.Length == 1)
                {
                    decimal amt = 0;
                    try
                    {
                        ServiceLevel oldSLA = null;
                        ServiceLevel newSLA = null;
                        oldSLA = boAccountingController.GetServicePack(Convert.ToInt32(termActivities[0].SlaName.Trim()));
                        newSLA = boAccountingController.GetServicePack(Convert.ToInt32(termActivities[0].NewSlaName.Trim()));

                        //Try the alternative methods for fetching the SLAs
                        if (oldSLA == null)
                        {
                            oldSLA = boAccountingController.GetServicePackByName(termActivities[0].SlaName.Trim());
                        }
                        if (newSLA == null)
                        {
                            newSLA = boAccountingController.GetServicePackByName(termActivities[0].NewSlaName.Trim());
                        }

                        //get the price information for the distributor
                        Billable distBillableOld = invoiceController.GetBillableForDistributor((int)oldSLA.BillableId, DistId);
                        Billable distBillableNew = invoiceController.GetBillableForDistributor((int)newSLA.BillableId, DistId);

                        //Determine credit or debit for the SLA change
                        if (oldSLA.ServiceClass == 1) //SoHo terminal
                        {
                            if (newSLA.ServiceClass != 1) //Upgrade to Business or Corporate
                            {
                                amt = distBillableNew.PriceEU;
                                if (Currency == "USD")
                                {
                                    amt = distBillableNew.PriceUSD;
                                }
                            }
                            else //no change of ISP
                            {
                                amt = 0;
                            }
                        }
                        else if (oldSLA.ServiceClass == 2) //Business terminal
                        {
                            if (newSLA.ServiceClass == 1) //Downgrade to SoHo terminal
                            {
                                amt = 0; //no credit for Business terminals
                            }
                            else if (newSLA.ServiceClass == 3) //Upgrade to Corporate terminal
                            {
                                amt = distBillableNew.PriceUSD - distBillableOld.PriceUSD;
                                if (Currency == "EUR")
                                {
                                    amt = distBillableNew.PriceEU - distBillableOld.PriceEU;
                                }
                                if (amt < 0) //no credit for Business terminals
                                {
                                    amt = 0;
                                }
                            }
                            else //no change of Service Level
                            {
                                amt = distBillableNew.PriceUSD - distBillableOld.PriceUSD;
                                if (Currency == "EUR")
                                {
                                    amt = distBillableNew.PriceEU - distBillableOld.PriceEU;
                                }
                                if (amt < 0) //no credit for Business terminals
                                {
                                    amt = 0;
                                }
                            }
                        }
                        else if (oldSLA.ServiceClass == 3) //Corporate terminal
                        {
                            if (newSLA.ServiceClass == 1 || newSLA.ServiceClass == 2) //Downgrade to SoHo or Business
                            {
                                amt = distBillableNew.PriceUSD - distBillableOld.PriceUSD;
                                if (Currency == "EUR")
                                {
                                    amt = distBillableNew.PriceEU - distBillableOld.PriceEU;
                                }                                  
                            }
                            else //no change of Service Level
                            {
                                amt = distBillableNew.PriceUSD - distBillableOld.PriceUSD;
                                if (Currency == "EUR")
                                {
                                    amt = distBillableNew.PriceEU - distBillableOld.PriceEU;
                                }
                            }
                        }

                        //Create the invoice line
                        BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail ilSLA = new BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail();
                        ilSLA.InvoiceYear = Year;
                        ilSLA.InvoiceNum = InvoiceNum;
                        ilSLA.Bid = distBillableNew.Bid;
                        ilSLA.BillableDescription = "SLA change";
                        if (term.TestMode != null)
                        {
                            if ((bool)term.TestMode)
                            {
                                amt = 0;
                                ilSLA.BillableDescription = ilSLA.BillableDescription + " (non-billable)";
                            }
                        }

                        ilSLA.Items = 1;
                        ilSLA.TerminalId = term.FullName;
                        ilSLA.Serial = term.Serial;
                        ilSLA.ServicePack = newSLA.SlaName;
                        ilSLA.MacAddress = term.MacAddress;
                        ilSLA.Status = "Active";
                        ilSLA.ActivityDate = termActivities[0].ActionDate;
                        //calculate the pro-rate amount (credit or debit)
                        DateTime actDate = (DateTime)ilSLA.ActivityDate;
                        decimal daysInMonth = (decimal)(DateTime.DaysInMonth(Year, Month));
                        ilSLA.UnitPrice = amt * ((daysInMonth - (decimal)actDate.Day) / daysInMonth)+amt*((DateTime.Now.Year-actDate.Year)*12+ DateTime.Now.Month-actDate.Month-1);
                        invoiceLines.Add(ilSLA);
                    }
                    catch (Exception ex)
                    {
                        Logger.Log("Error storing SLA change: " + ex.Message);
                    }
                }
                //Multiple SLA changes during the same month
                else if (termActivities.Length > 1)
                {
                    try
                    {
                        //find out the most expensive SLA
                        ServiceLevel oriSLA = null;
                        ServiceLevel newSLA = null;
                        //try fetching the SLAs. This may fail due to incorrect info in the terminal acitvity
                        oriSLA = boAccountingController.GetServicePack(Convert.ToInt32(termActivities[0].SlaName.Trim()));
                        newSLA = boAccountingController.GetServicePack(Convert.ToInt32(termActivities[0].NewSlaName.Trim()));
                        
                        //Try the alternative methods for fetching the SLAs
                        if (oriSLA == null)
                        {
                            oriSLA = boAccountingController.GetServicePackByName(termActivities[0].SlaName.Trim());
                        }
                        
                        if (newSLA == null)
                        {
                            newSLA = boAccountingController.GetServicePackByName(termActivities[0].NewSlaName.Trim());
                        }

                        Billable bHigh = invoiceController.GetBillableForDistributor((int)oriSLA.BillableId, DistId);
                        Billable bOri = bHigh;
                        Billable bTemp;
                        foreach (TerminalActivity ta in termActivities)
                        {
                            ServiceLevel sla = boAccountingController.GetServicePack(Convert.ToInt32(ta.NewSlaName.Trim()));
                            if (sla == null)
                            {
                                sla = boAccountingController.GetServicePackByName(ta.NewSlaName.Trim());
                            }
                            bTemp = invoiceController.GetBillableForDistributor((int)sla.BillableId, DistId);
                            
                            if (bTemp.PriceUSD > bHigh.PriceUSD)
                            {
                                bHigh = bTemp;
                            }
                        }

                        decimal amt = bHigh.PriceUSD - bOri.PriceUSD;
                        if (Currency == "EUR")
                        {
                            amt = bHigh.PriceEU - bOri.PriceEU;
                        }
                        if (amt < 0)
                        {
                            if (newSLA.ServiceClass != 3)
                            {
                                //no credit for business or SoHo services
                                amt = 0;
                            }
                        }
                        //add an invoice line for the first SLA change, including the cost for the rest of the month
                        BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail ilSLA = new BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail();
                        ilSLA.InvoiceYear = Year;
                        ilSLA.InvoiceNum = InvoiceNum;
                        ilSLA.Bid = bHigh.Bid;
                        ilSLA.BillableDescription = "SLA change";
                        if (term.TestMode != null)
                        {
                            if ((bool)term.TestMode)
                            {
                                amt = 0;
                                ilSLA.BillableDescription = ilSLA.BillableDescription + " (non-billable)";
                            }
                        }
                        ilSLA.Items = 1;
                        ilSLA.TerminalId = term.FullName;
                        ilSLA.Serial = term.Serial;
                        ilSLA.ServicePack = newSLA.SlaName;
                        ilSLA.MacAddress = term.MacAddress;
                        ilSLA.Status = "Active";
                        ilSLA.ActivityDate = termActivities[0].ActionDate;
                        //calculate the pro-rate amount (credit or debit)
                        DateTime actDate = (DateTime)ilSLA.ActivityDate;
                        decimal daysInMonth = (decimal)(DateTime.DaysInMonth(Year, Month));
                        ilSLA.UnitPrice = amt * ((daysInMonth - (decimal)actDate.Day) / daysInMonth)+ amt * ((DateTime.Now.Year - actDate.Year) * 12 + DateTime.Now.Month - actDate.Month - 1); ;
                        invoiceLines.Add(ilSLA);
                        
                        //add invoice lines for all other SLA changes
                        for (int i = 1; i < termActivities.Length; i++)
                        {
                            newSLA = boAccountingController.GetServicePack(Convert.ToInt32(termActivities[i].NewSlaName.Trim()));
                            if (newSLA == null)
                            {
                                newSLA = boAccountingController.GetServicePackByName(termActivities[i].NewSlaName.Trim());
                            }
                            ilSLA = new BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail();
                            ilSLA.InvoiceYear = Year;
                            ilSLA.InvoiceNum = InvoiceNum;
                            ilSLA.Bid = bHigh.Bid;
                            ilSLA.BillableDescription = "SLA change";
                            ilSLA.UnitPrice = 0;
                            ilSLA.Items = 1;
                            ilSLA.TerminalId = term.FullName;
                            ilSLA.Serial = term.Serial;
                            ilSLA.ServicePack = newSLA.SlaName;
                            ilSLA.MacAddress = term.MacAddress;
                            ilSLA.Status = "Active";
                            ilSLA.ActivityDate = termActivities[i].ActionDate;
                            invoiceLines.Add(ilSLA);
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Log("Error storing SLA change: " + ex.Message);
                    }
                }
            }
        }

        /// <summary>
        /// Creates invoice lines for paid voucher batches lines for a given month and distributor
        /// Also creates invoice lines for any unpaid voucher batches
        /// </summary>
        /// <param name="DistId">The distributor Id</param>
        /// <param name="Month">Invoice month</param>
        /// <param name="Year">Invoice year</param>
        /// <param name="Currency">The currency in which the distributor is invoiced</param>
        /// <param name="invoiceLines">The invoice lines</param>
        private void VoucherBatchInvoiceLines(int DistId, int Month, int Year, string Currency, int InvoiceNum, ref List<BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail> invoiceLines)
        {
            BOVoucherControllerWS voucherController = new BOVoucherControllerWS();
            BOAccountingControlWS boAccountingController = new BOAccountingControlWS();

            DateTime InvoicingDate = new DateTime(Year, Month, 1);

                VoucherBatch[] vBatches = voucherController.getVouchersBatchesReport(DistId,2,InvoicingDate,InvoicingDate.AddMonths(1).AddDays(-1));
           
            foreach (VoucherBatch vb in vBatches)
            {
                VoucherBatch vbi = voucherController.GetVoucherBatchInfo(vb.BatchId);
                VoucherVolume vv = voucherController.GetVoucherVolumeById(vbi.Volume);
                if (vbi.Paid)
                {
                    // Add the invoice line
                    BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail ilVB = new BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail();
                    ilVB.InvoiceYear = Year;
                    ilVB.InvoiceNum = InvoiceNum;
                    if ((vv.Description.IndexOf("unlimited", StringComparison.OrdinalIgnoreCase) >= 0) || (vv.Description.IndexOf("unl", StringComparison.OrdinalIgnoreCase) >= 0))
                        ilVB.TerminalId = "Unlimited Vouchers batch Qt(" + vbi.NumVouchers + ") " + vv.ValidityPeriod + " days";
                    else
                        ilVB.TerminalId = "SoHo Vouchers batch Qt(" + vbi.NumVouchers + ") " + vv.ValidityPeriod + " days";
                    ilVB.BillableDescription = "PAID";
                    if (Currency == "EUR")
                    {
                        ilVB.UnitPrice = Convert.ToDecimal(vv.UnitPriceEUR * vbi.NumVouchers);
                    }
                    else
                    {
                        ilVB.UnitPrice = Convert.ToDecimal(vv.UnitPriceUSD * vbi.NumVouchers);
                    }
                    ilVB.Items = 1;
                    ilVB.Serial = vb.BatchId.ToString();
                    ilVB.ServicePack = boAccountingController.GetServicePack(vbi.Sla).SlaName;
                    ilVB.Status = "PAID";
                    ilVB.PaidInAdvance = true;
                    ilVB.ActivityDate = vb.DateCreated;
                    invoiceLines.Add(ilVB);
                }
                else if (!vbi.Paid && vbi.Release)
                {
                    // Add the invoice line
                    BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail ilVB = new BillingEngine.BOInvoicingControllerWSRef.InvoiceDetail();
                    ilVB.InvoiceYear = Year;
                    ilVB.InvoiceNum = InvoiceNum;
                    if ((vv.Description.IndexOf("unlimited", StringComparison.OrdinalIgnoreCase) >= 0) || (vv.Description.IndexOf("unl", StringComparison.OrdinalIgnoreCase) >= 0))
                        ilVB.TerminalId = "Unlimited Vouchers batch Qt(" + vbi.NumVouchers + ") " + vv.ValidityPeriod + " days";
                    else
                        ilVB.TerminalId = "SoHo Vouchers batch Qt(" + vbi.NumVouchers + ") " + vv.ValidityPeriod + " days";

                    ilVB.BillableDescription = "ORDERED";
                    if (Currency == "EUR")
                    {
                        ilVB.UnitPrice = Convert.ToDecimal(vv.UnitPriceEUR * vbi.NumVouchers);
                    }
                    else
                    {
                        ilVB.UnitPrice = Convert.ToDecimal(vv.UnitPriceUSD * vbi.NumVouchers);
                    }
                    ilVB.Items = 1;
                    ilVB.Status = "ORDERED";
                    ilVB.Serial = vb.BatchId.ToString();
                    ilVB.ServicePack = boAccountingController.GetServicePack(vbi.Sla).SlaName;
                    ilVB.PaidInAdvance = false;
                    ilVB.ActivityDate = vb.DateCreated;
                    invoiceLines.Add(ilVB);

                    //// Flag the voucher batch as paid
                    //if (!voucherController.FlagVoucherBatchAsPaid(vb.BatchId))
                    //{
                    //    Logger.Log("Failed to flag voucher batch " + vb.BatchId + " as paid in the CMT.");
                    //}
                }
                else
                {
                    if(voucherController.DeleteVoucherBatch(vbi.BatchId)!=0)
                        Logger.Log("Error Deleting Batch despite the voucher in not released and not paid id:" +vbi.BatchId.ToString()) ;
                }
                    

            }
        }

        private int ipMaskToAddresses(int ipMask)
        {
            int noOfAddress = 0;

            if (ipMask == 32)
            {
                noOfAddress = 1;
            }
            else if (ipMask == 31)
            {
                noOfAddress = 2;
            }
            else if (ipMask == 30)
            {
                noOfAddress = 4;
            }
            else if (ipMask == 29)
            {
                noOfAddress = 8;
            }
            else if (ipMask == 28)
            {
                noOfAddress = 16;
            }
            else if (ipMask == 27)
            {
                noOfAddress = 32;
            }
            else if (ipMask == 26)
            {
                noOfAddress = 64;
            }

            return noOfAddress;
        }

    }
}
