﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Net;
using System.IO;
using RestSharp;
using System.Linq;
using System.Web.Script.Serialization;
using Newtonsoft.Json;

namespace AvantiAPITest
{
    public class AvantiAPIGateway
    {
        string _baseUrl = "http://oss-api.freenoc.lan/";
        string _username = "dandersson";
        string _password = "Avanti123";
        string _command = "services/rest/default/Sit";
        string _session = "services/rest/Session";
        string _logout = "logout";
        string _defaultSla = "151";
        string _hub = "45";
        string _ipConnectivity = "routed";
        string _partner = "348";
        string _customer = "28678";
        RestClient _client;

        public AvantiAPIGateway()
        {
            _client = new RestClient();
            _client.BaseUrl = _baseUrl;
            _client.Authenticator = new HttpBasicAuthenticator(_username, _password);
            _client.CookieContainer = new System.Net.CookieContainer();
        }

        private void AvantiAuthorization()
        {
            RestRequest auth = new RestRequest();
            auth.Method = Method.POST;
            auth.Resource = _session;
            auth.AddParameter("username", _username);
            auth.AddParameter("password", _password);
            IRestResponse response = _client.Execute(auth);

            CookieContainer cookieCon = new CookieContainer();

            if (response.StatusCode == HttpStatusCode.OK)
            {
                var cookie = response.Cookies.FirstOrDefault();
                cookieCon.Add(new Cookie(cookie.Name, cookie.Value, cookie.Path, cookie.Domain));
            }

            _client.CookieContainer = cookieCon;
        }
        
        public string CreateSit()
        {
            string result = "";
            //create the sit
            RestRequest newSit = new RestRequest();
            newSit.Resource = _command;
            newSit.Method = Method.POST;
            newSit.AddParameter("sla", _defaultSla);
            newSit.AddParameter("partner", _partner);
            newSit.AddParameter("sitNum", "12389");
            newSit.AddParameter("ipConnectivity", _ipConnectivity);
            newSit.AddParameter("hub", _hub);

            IRestResponse response = _client.Execute(newSit);
            //get the sit id
            JavaScriptSerializer js = new JavaScriptSerializer();
            var dict = js.Deserialize<Dictionary<string, dynamic>>(response.Content);
            result = dict["Sit"]["id"];

            return result;
        }

        /// <summary>
        /// This method ends the current session at the Avanti hub
        /// </summary>
        private void EndSession()
        {
            RestRequest logout = new RestRequest();
            logout.Resource = "logout";
            IRestResponse response = _client.Execute(logout);
        }

        public void LinkHardware(string sitId, string hwId)
        {
            this.AvantiAuthorization();
            
            RestRequest link = new RestRequest();
            link.Resource = _command + "/" + sitId + "/link_hardware";
            link.AddParameter("hardwareId", hwId);
            link.AddParameter("reason", "Linked_hardware");
            link.Method = Method.PUT;

            IRestResponse respose = _client.Execute(link);
            this.EndSession();
        }

        public void UnLinkHardware(string sitId)
        {
            
            RestRequest link = new RestRequest();
            link.Resource = _command + "/" + sitId + "/unlink_hardware";
            link.AddParameter("reason", "Unlinked_hardware");
            link.Method = Method.PUT;

            IRestResponse respose = _client.Execute(link);
        }

        public void LinkCustomer(string sitId)
        {
            this.AvantiAuthorization();
            string eTag = this.GetEtag(sitId);
            RestRequest newSit = new RestRequest();
            newSit.AddHeader("If-Match", eTag);
            newSit.AddParameter("sla", _defaultSla);
            newSit.AddParameter("customer", _customer);
            newSit.AddParameter("partner", _partner);
            newSit.AddParameter("sitNum", sitId);
            newSit.AddParameter("ipConnectivity", _ipConnectivity);
            newSit.AddParameter("hub", _hub); 
            newSit.AddParameter("reason", "Linked_customer");
            newSit.Method = Method.PUT;
            newSit.Resource = _command + "/" + sitId;

            IRestResponse response = _client.Execute(newSit);

            this.EndSession();
        }

        public string GetEtag(string sitId)
        {
            string eTag = "";

            RestRequest get = new RestRequest();
            get.Resource = _command + "/" + sitId;
            IRestResponse response = _client.Execute(get);
            foreach (var v in response.Headers)
            {
                if (v.Name == "ETag")
                {
                    eTag = v.Value.ToString();
                }
            }
            
            return eTag;
        }

        public bool AddIpToSit(string sitId, string ipAddress)
        {
            bool result = false;

            RestRequest addIp = new RestRequest();
            addIp.Resource = _command + "/" + sitId + "/add_ip";
            addIp.AddParameter("ipAddressType", "POP Network");
            addIp.AddParameter("ipAddress", ipAddress);
            addIp.Method = Method.PUT;

            IRestResponse respose = _client.Execute(addIp);

            if (respose.StatusCode == HttpStatusCode.Created)
            {
                result = true;
            }

            return result;
        }

        public string GetSit(string sitId)
        {
            this.AvantiAuthorization();
            RestRequest getSit = new RestRequest();
            getSit.Resource = "services/rest/default/Sit/" + sitId;
            
            IRestResponse response = _client.Execute(getSit);

            this.EndSession();
            return response.Content;
        }

        public string GetIps()
        {
            this.AvantiAuthorization();
            RestRequest getIPs = new RestRequest();
            getIPs.Resource = "services/rest/default/IpDevice";

            IRestResponse response = _client.Execute(getIPs);

            this.EndSession();
            return response.Content;
        }

        public string  RemoveIp(string ipDevice)
        {
            string eTag = "";
            this.AvantiAuthorization();

            RestRequest get = new RestRequest();
            get.Resource = "services/rest/default/IpDevice/" + ipDevice;
            IRestResponse response = _client.Execute(get);
            foreach (var v in response.Headers)
            {
                if (v.Name == "ETag")
                {
                    eTag = v.Value.ToString();
                }
            }

            RestRequest delIp = new RestRequest();
            delIp.AddHeader("If-Match", eTag);
            delIp.Resource = "services/rest/default/IpDevice/" + ipDevice;
            delIp.AddParameter("text/plain", "reason=removing", ParameterType.RequestBody);
            //delIp.AddBody("reason=removing");
            delIp.Method = Method.DELETE;
            response = _client.Execute(delIp);
            this.EndSession();

            return response.ResponseStatus.ToString();
        }

        /// <summary>
        /// Simply converts an IP address in a string format into a uint
        /// </summary>
        /// <param name="sIpAddress"></param>
        /// <returns></returns>
        public uint ConvertIPToLong(string sIpAddress)
        {
            byte[] addressBytes = IPAddress.Parse(sIpAddress).GetAddressBytes();
            byte[] invAddressBytes = new Byte[4];

            invAddressBytes[3] = addressBytes[0];
            invAddressBytes[2] = addressBytes[1];
            invAddressBytes[1] = addressBytes[2];
            invAddressBytes[0] = addressBytes[3];

            return BitConverter.ToUInt32(invAddressBytes, 0);
        }

        public string GetHardwareModel(string sitId)
        {
            string hw = "";
            RestRequest get = new RestRequest();
            get.Resource = _command + "/" + sitId + "/supported_hardware_model";
            IRestResponse response = _client.Execute(get);
            JavaScriptSerializer js = new JavaScriptSerializer();
            var dict = js.Deserialize<Dictionary<string, dynamic>>(response.Content);
            hw = dict["options"]["hwModel"]["id"];

            return hw;
        }

        public bool ConfigureHub(string sitId)
        {
            bool result = false;
            string hw = this.GetHardwareModel(sitId);

            RestRequest put = new RestRequest();
            put.Resource = _command + "/" + sitId + "/configure_hub";
            put.Method = Method.PUT;
            put.AddParameter("hwModel", hw);

            IRestResponse response = _client.Execute(put);

            if (response.StatusCode == HttpStatusCode.Accepted)
            {
                result = true;
            }

            return result;
        }

        public bool FullTest()
        {
            bool result = false;
            this.AvantiAuthorization();
            
            //Create the sit 
            string sitId = "39576"; // this.CreateSit();

            if (sitId != "0")
            {
                //Link the sit to a customer
                //this.LinkCustomer(sitId);
                
                //Create the hardware on the Avanti hub
                //this is currently not possible at the Avanti end
                string hardwareId = "82682";

                //Link the hardware to the sit
                //this.LinkHardware(sitId, hardwareId);
                
                // Get available IP address from the Avanti hub
                // This will have to be done through a local database as no such feature is offered by Avanti
                string ipAddress = this.ConvertIPToLong("37.208.59.133").ToString();
                this.AddIpToSit(sitId, ipAddress);
                //{
                    this.ConfigureHub(sitId);
                //    result = true;
                //}
            }

            return result;
        }

        public bool LockTerminal(string sitId)
        {
            this.AvantiAuthorization();

            //get the ETag
            string eTag = "";
            RestRequest get = new RestRequest();
            get.Resource = _command + "/" + sitId;
            IRestResponse response = _client.Execute(get);
            foreach (var v in response.Headers)
            {
                if (v.Name == "ETag")
                {
                    eTag = v.Value.ToString();
                }
            }

            RestRequest delIp = new RestRequest();
            delIp.AddHeader("If-Match", eTag);
            
            RestRequest put = new RestRequest();
            put.Resource = _command + "/" + sitId + "/state_transition";
            put.Method = Method.PUT;
            put.AddHeader("If-Match", eTag);
            put.AddParameter("state", "suspensionblock");
            put.AddParameter("reason", "locking");
            
            response = _client.Execute(put);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool UnLockTerminal(string sitId)
        {
            this.AvantiAuthorization();

            //get the ETag
            string eTag = "";
            RestRequest get = new RestRequest();
            get.Resource = _command + "/" + sitId;
            IRestResponse response = _client.Execute(get);
            foreach (var v in response.Headers)
            {
                if (v.Name == "ETag")
                {
                    eTag = v.Value.ToString();
                }
            }

            RestRequest delIp = new RestRequest();
            delIp.AddHeader("If-Match", eTag);

            RestRequest put = new RestRequest();
            put.Resource = _command + "/" + sitId + "/state_transition";
            put.Method = Method.PUT;
            put.AddHeader("If-Match", eTag);
            put.AddParameter("state", "operational");
            put.AddParameter("reason", "unlocking");

            response = _client.Execute(put);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool DecommissionTerminal(string sitId)
        {
            RestRequest put = new RestRequest();
            put.Resource = _command + "/" + sitId + "/delete_hub";
            put.Method = Method.PUT;
            put.AddParameter("reason", "decommission");

            IRestResponse response = _client.Execute(put);
            
            if (response.StatusCode == HttpStatusCode.OK)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool HardwareSwap(string sitId, string hwId)
        {
            bool result = false;

            this.AvantiAuthorization();
            this.DecommissionTerminal(sitId);
            this.UnLinkHardware(sitId);
            this.LinkHardware(sitId, hwId);
            this.ConfigureHub(sitId);

            return result;
        }
    }
}