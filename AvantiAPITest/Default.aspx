﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="AvantiAPITest.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Avanti API test</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table cellspacing="10" cellpadding="5">
            <tr>
                <td><asp:TextBox ID="TextBox1" runat="server"></asp:TextBox></td>
                <td><asp:Button ID="ButtonGetHubInfo" runat="server" Text="Get hub info" OnClick="ButtonGetHubInfo_Click" /></td>
            </tr>
            <tr>
                <td><asp:Label ID="LabelHubInfo" runat="server"></asp:Label></td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
