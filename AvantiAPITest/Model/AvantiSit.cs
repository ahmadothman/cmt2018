﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace AvantiAPITest.Model
{
    public class AvantiSit
    {
        public Sit Sit = new Sit();
    }

    public class Attributes
    {
        public string id { get; set; }
        public string name { get; set; }
        public string activeUrl { get; set; }
    }

    public class Attributes2
    {
        public string count { get; set; }
    }

    public class Fields
    {
        [JsonProperty("@attributes")]
        public Attributes2 attributes = new Attributes2();
        public string id { get; set; }
        public string textId { get; set; }
        public string callSign { get; set; }
        public string type { get; set; }
        public string sla { get; set; }
        public string customer { get; set; }
        public string monitored { get; set; }
        public string bespokeConfig { get; set; }
        public string updatedAt { get; set; }
        public string remarks { get; set; }
        public string partner { get; set; }
        public string sitNum { get; set; }
        public string ipConnectivity { get; set; }
        public string dhcp { get; set; }
        public string hub { get; set; }
        public string state { get; set; }
        public string lastStateChange { get; set; }
        public object activationDate { get; set; }
        public string operationalCertification { get; set; }
    }

    public class Attributes3
    {
        public string count { get; set; }
    }

    public class Operations
    {
        [JsonProperty("@attributes")]
        public Attributes3 attributes = new Attributes3();
        public string edit { get; set; }
    }

    public class Sit
    {
        [JsonProperty("@attributes")]
        public Attributes attributes = new Attributes();
        public Fields fields = new Fields();
        public Operations operations = new Operations();
    }
}