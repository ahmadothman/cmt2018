﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AvantiAPITest
{
    public partial class Default : System.Web.UI.Page
    {
        AvantiAPIGateway _gateway;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            _gateway = new AvantiAPIGateway();
            //_gateway.LinkCustomer("38840");
            //LabelHubInfo.Text = _gateway.GetIps();
            //_gateway.UnLinkHardware("38323");
            _gateway.LinkHardware("40243", "82686");
            //_gateway.AddIpToSit("38841", "37.208.59.132");
            //_gateway.RemoveIp("82548");
            //LabelHubInfo.Text = _gateway.GetSit("38850");
            //_gateway.FullTest();
            //_gateway.ConfigureHub("39070");
            //LabelHubInfo.Text = _gateway.RemoveIp("81210");
            //LabelHubInfo.Text = _gateway.GetIps();
            //_gateway.UnLockTerminal("39576"); 
            //_gateway.DecommissionTerminal("39576");
            //_gateway.HardwareSwap("39576", "82683");
            
        }

        protected void ButtonGetHubInfo_Click(object sender, EventArgs e)
        {
            LabelHubInfo.Text = _gateway.ConvertIPToLong(TextBox1.Text).ToString();
        }
    }
}