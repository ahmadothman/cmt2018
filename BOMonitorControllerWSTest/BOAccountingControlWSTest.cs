﻿using net.nimera.cmt.BOAccountingController;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using BOControllerLibrary.Model.Terminal;
using System.Collections.Generic;

namespace BOMonitorControllerWSTest
{
    
    
    /// <summary>
    ///This is a test class for BOAccountingControlWSTest and is intended
    ///to contain all BOAccountingControlWSTest Unit Tests
    ///</summary>
    [TestClass()]
    public class BOAccountingControlWSTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for GetTerminalsByStatus
        ///</summary>
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Projects\\SatFinAfrica\\CustomerManagementTool\\BOMonitorController", "/")]
        [UrlToTest("http://localhost:49601/")]
        public void GetTerminalsByStatusTest()
        {
            BOAccountingControlWS target = new BOAccountingControlWS();
            int statusId = 1; 
            List<Terminal> termList = target.GetTerminalsByStatus(statusId);
            Assert.IsTrue(termList.Count > 0, "Terminal list contains: " + termList.Count + " records");
        }

        /// <summary>
        ///A test for GetTerminalsByExpiryDate
        ///</summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Projects\\SatFinAfrica\\CustomerManagementTool\\BOMonitorController", "/")]
        [UrlToTest("http://localhost:49601/")]
        public void GetTerminalsByExpiryDateTest()
        {
            BOAccountingControlWS target = new BOAccountingControlWS(); 
            DateTime dt = new DateTime(2012, 02, 03);
            string criterion = "=";
            List<Terminal> termList = target.GetTerminalsByExpiryDate(dt, criterion);
            Assert.IsTrue(termList.Count > 0, "Terminal list contains: " + termList.Count + " records");

            dt = new DateTime(2013, 1, 1);
            criterion = "<";
            termList = target.GetTerminalsByExpiryDate(dt, criterion);
            Assert.IsTrue(termList.Count > 0, "Terminal list contains: " + termList.Count + " records");
        }

        /// <summary>
        ///A test for GetTerminalsByMac
        ///</summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Projects\\SatFinAfrica\\CustomerManagementTool\\BOMonitorController", "/")]
        [UrlToTest("http://localhost:49601/")]
        public void GetTerminalsByMacTest()
        {
            //System.Diagnostics.Debugger.Break();
            BOAccountingControlWS target = new BOAccountingControlWS();
            string macAddress = "00:05:99:99:99:99";
            List<Terminal> termList = target.GetTerminalsByMac(macAddress);
            Assert.IsTrue(termList.Count == 1, "Terminal list contains: " + termList.Count + " records");

            macAddress = "00:06";
            termList = target.GetTerminalsByMac(macAddress);
            Assert.IsTrue(termList.Count > 1, "Terminal list contains: " + termList.Count + " records");
        }
    }
}
