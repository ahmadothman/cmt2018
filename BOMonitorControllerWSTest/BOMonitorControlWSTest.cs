﻿using System;
using System.Net;
using System.Threading;
using System.Configuration;
using System.Data.SqlClient;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using BOMonitorController.com.astra2connect.ispif;
using BOControllerLibrary.Controllers;
using BOControllerLibrary.ISPIF.Model;
using BOControllerLibrary.ISPIF.Gateway;
using net.nimera.cmt.BOMonitorController;

namespace BOMonitorControllerWSTest
{

    /// <summary>
    ///This is a test class for BOMonitorControlWSTest and is intended
    ///to contain all BOMonitorControlWSTest Unit Tests
    ///</summary>
    [TestClass()]
    public class BOMonitorControlWSTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes

        static BOAccountingController _boAccountingController = null;
        static ISPIFEmulatorGateWay _ispifController = null;
        static int _testISP = 112;
        static string _endUserId = "Ipersat - Ipersat - SN:12IUYYTT";
        static string _macAddress = "00:05:99:99:99:99";

        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
            string connectionString =
             ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
            string databaseProviderString =
               ConfigurationManager.ConnectionStrings["ApplicationServices"].ProviderName;

            _boAccountingController =
                                  new BOAccountingController(connectionString, databaseProviderString);

            _ispifController = new ISPIFEmulatorGateWay();
        }
        
        //Use ClassCleanup to run code after all tests in a class have run
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            //Remove the test terminal form the CMTData database
            string deleteFromTerminalsQuery = "DELETE FROM Terminals WHERE MacAddress = @macAddress";
            string deleteFromTerminalActivityQuery = "DELETE FROM TerminalActivityLog WHERE MacAddress = @macAddress";
            string connectionString =
                       ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();
                try
                {
                    using (SqlCommand command = new SqlCommand(deleteFromTerminalActivityQuery, con))
                    {
                        //Delete from terminals activity log
                        command.Parameters.AddWithValue("@macAddress", _macAddress);
                        int numRows = command.ExecuteNonQuery();
                    }

                    using (SqlCommand command = new SqlCommand(deleteFromTerminalsQuery, con))
                    {
                        //Delete from terminals
                        command.Parameters.AddWithValue("@macAddress", _macAddress);
                        int numRows = command.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    //TO DO: Send an error message to the log file
                }
            }
            
        }
        
        //Use TestInitialize to run code before running each test
        [TestInitialize()]
        public void MyTestInitialize()        
        {
        }
        
        //Use TestCleanup to run code after each test has run
        [TestCleanup()]
        public void MyTestCleanup()
        {
        }
        
        #endregion


        /// <summary>
        ///A test for getTerminalInfoByMacAddress
        ///</summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Projects\\SatFinAfrica\\CustomerManagementTool\\BOMonitorController", "/")]
        [UrlToTest("http://localhost:1213/Default.aspx")]
        public void getTerminalInfoByMacAddressTest()
        {
            //System.Diagnostics.Debugger.Break();
            BOMonitorControlWS target = new BOMonitorControlWS();
            string macAddress = "00:06:39:82:2f:e6";
            int ispId = 112;
            TerminalInfo tiExpected = new TerminalInfo();
            tiExpected.SitId = "124002";

            TerminalInfo tiActual = target.getTerminalInfoByMacAddress(macAddress, ispId);

            Assert.IsFalse(tiActual.ErrorFlag, tiActual.ErrorMsg);
            Assert.AreEqual(tiExpected.SitId, tiActual.SitId);
        }

        /// <summary>
        ///A test for ISPIFPwd
        ///</summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Projects\\SatFinAfrica\\CustomerManagementTool\\BOMonitorController", "/")]
        [UrlToTest("http://localhost:1213/Default.aspx")]
        public void ISPIFPwdTest()
        {
            BOControllerLibrary.Model.ISP.Isp isp = _boAccountingController.GetISP(_testISP);
            Assert.AreEqual(isp.Ispif_Password, "seaa2cspace");
            
        }

        /// <summary>
        ///A test for ISPIFUserName
        ///</summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Projects\\SatFinAfrica\\CustomerManagementTool\\BOMonitorController", "/")]
        [UrlToTest("http://localhost:1213/Default.aspx")]
        public void ISPIFUserNameTest()
        {
            BOControllerLibrary.Model.ISP.Isp isp = _boAccountingController.GetISP(_testISP);
            Assert.AreEqual(isp.Ispif_User, "sea_space");
        }

        /// <summary>
        ///A test for ISPId
        ///</summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Projects\\SatFinAfrica\\CustomerManagementTool\\BOMonitorController", "/")]
        [UrlToTest("http://localhost:1213/Default.aspx")]
        public void ISPIdTest()
        {
            BOControllerLibrary.Model.ISP.Isp isp = _boAccountingController.GetISP(_testISP);
            Assert.AreEqual(isp.Id, _testISP);
        }

        
        /// <summary>
        ///A test for IsFreeZone
        ///</summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Projects\\SatFinAfrica\\CustomerManagementTool\\BOMonitorController", "/")]
        [UrlToTest("http://localhost:1213/")]
        [DeploymentItem("BOMonitorController.dll")]
        public void IsFreeZoneTest()
        {
            int slaID = 1113; 
            bool result = _boAccountingController.IsFreeZoneSLA(slaID);
            Assert.IsTrue(result);
            slaID = 1101;
            result = _boAccountingController.IsFreeZoneSLA(slaID);
            Assert.IsFalse(result);
        }

        /// <summary>
        ///A test for IsBusiness
        ///</summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Projects\\SatFinAfrica\\CustomerManagementTool\\BOMonitorController", "/")]
        [UrlToTest("http://localhost:1213/")]
        [DeploymentItem("BOMonitorController.dll")]
        public void IsBusinessTest()
        {
            int slaID = 1123;
            bool result = _boAccountingController.IsBusinessSLA(slaID);
            Assert.IsTrue(result);
            slaID = 1120;
            result = _boAccountingController.IsBusinessSLA(slaID);
            Assert.IsFalse(result);
        }


        /// <summary>
        ///A test for TerminalActivate
        ///</summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Projects\\SatFinAfrica\\CustomerManagementTool\\BOMonitorController", "/")]
        [UrlToTest("http://localhost:1213/")]
        public void TerminalManagementTest()
        {
            //System.Diagnostics.Debugger.Break();
            BOMonitorControlWS target = new BOMonitorControlWS();
            long slaId = 1109;
            long newSlaId = 3001;
            bool expected = true;
            bool actual;

            actual = target.TerminalActivate(_endUserId, _macAddress, slaId, _testISP);
            Assert.AreEqual(expected, actual, "Could not activate terminal");

            //Hold for 60 seconds to allow the ISPIF to validate the last request
            Thread.Sleep(60000);

            actual = target.TerminalUserReset(_endUserId, _testISP);
            Assert.AreEqual(expected, actual, "Could not reset user");

            Thread.Sleep(60000);

            actual = target.TerminalChangeSla(_endUserId, _macAddress, (int)newSlaId, AdmStatus.OPERATIONAL, _testISP);
            Assert.AreEqual(expected, actual, "Could not change Sla");

            Thread.Sleep(60000);

            actual = target.TerminalSuspend(_endUserId, _macAddress, _testISP);
            Assert.AreEqual(expected, actual, "Could not suspend terminal");

            Thread.Sleep(60000);

            actual = target.TerminalReActivate(_endUserId, _macAddress, _testISP);
            Assert.AreEqual(expected, actual, "Could not re-activate terminal");

            Thread.Sleep(60000);

            actual = target.TerminalDecommission(_endUserId, _macAddress, _testISP);
            Assert.AreEqual(expected, actual, "Could not decomission terminal");
        }
    }
}
