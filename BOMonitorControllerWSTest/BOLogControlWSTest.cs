﻿using BOMonitorController;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using BOControllerLibrary.Model.Terminal;

namespace BOMonitorControllerWSTest
{
    
    
    /// <summary>
    ///This is a test class for BOLogControlWSTest and is intended
    ///to contain all BOLogControlWSTest Unit Tests
    ///</summary>
    [TestClass()]
    public class BOLogControlWSTest
    {


        private TestContext _testContextInstance;
        private TerminalActivity _terminalActivity;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return _testContextInstance;
            }
            set
            {
                _testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        [TestInitialize()]
        public void MyTestInitialize()
        {
           
        }
        
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for UpdateTerminalActivity
        ///</summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Projects\\CMT-Samoa\\BOMonitorController", "/")]
        [UrlToTest("http://localhost:9090/")]
        public void UpdateTerminalActivityTest()
        {
            _terminalActivity = new TerminalActivity();
            _terminalActivity.Id = 363;
            _terminalActivity.ActionDate = DateTime.Now;
            _terminalActivity.Action = "Activation";
            _terminalActivity.MacAddress = "00:06:39:81:fd:59";
            _terminalActivity.SlaName = "D11 2048/128 VOIP_FREE";
            _terminalActivity.TerminalActivityId = 100;
            _terminalActivity.UserId = new Guid("feb02d02-438d-4a7e-9c31-93d7d11ffa34");

            BOLogControlWS target = new BOLogControlWS();
            Assert.IsTrue(target.UpdateTerminalActivity(_terminalActivity), "Update of terminal activity failed");
        }

        /// <summary>
        ///A test for GetTerminalActivityById
        ///</summary>
        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Projects\\CMT-Samoa\\BOMonitorController", "/")]
        [UrlToTest("http://localhost:9090/")]
        public void GetTerminalActivityByIdTest()
        {
            _terminalActivity = new TerminalActivity();
            _terminalActivity.Id = 363;
            _terminalActivity.ActionDate = DateTime.Now;
            _terminalActivity.Action = "Activation";
            _terminalActivity.MacAddress = "00:06:39:81:fd:59";
            _terminalActivity.SlaName = "D11 2048/128 VOIP_FREE";
            _terminalActivity.TerminalActivityId = 100;
            _terminalActivity.UserId = new Guid("feb02d02-438d-4a7e-9c31-93d7d11ffa34");

            //System.Diagnostics.Debugger.Break();

            BOLogControlWS target = new BOLogControlWS();
            int terminalActivityId = 363;
            TerminalActivity actual = target.GetTerminalActivityById(terminalActivityId);
            Assert.IsNotNull(actual, "Could not retrieve terminal activity");
            Assert.AreEqual(_terminalActivity.Id, actual.Id, "Id not equal");
            Assert.AreEqual(_terminalActivity.MacAddress, actual.MacAddress, "MacAddress not equal");
            Assert.AreEqual(_terminalActivity.SlaName, actual.SlaName, "SlaName not equal");
            Assert.AreEqual(_terminalActivity.TerminalActivityId, actual.TerminalActivityId, "Terminal Activity Id not equal");
            Assert.AreEqual(_terminalActivity.UserId, actual.UserId, "UserId not equal");
        }

        [TestMethod()]
        [HostType("ASP.NET")]
        [AspNetDevelopmentServerHost("D:\\Projects\\CMT-Samoa\\BOMonitorController", "/")]
        [UrlToTest("http://localhost:9090/")]
        public void DeleteTerminalActivityTest()
        {
            BOLogControlWS target = new BOLogControlWS();
            int terminalActivityId = 363;

            _terminalActivity = new TerminalActivity();
            _terminalActivity.Id = terminalActivityId;
            _terminalActivity.ActionDate = DateTime.Now;
            _terminalActivity.Action = "Activation";
            _terminalActivity.MacAddress = "00:06:39:81:fd:59";
            _terminalActivity.SlaName = "D11 2048/128 VOIP_FREE";
            _terminalActivity.TerminalActivityId = 100;
            _terminalActivity.UserId = new Guid("feb02d02-438d-4a7e-9c31-93d7d11ffa34");

            target.UpdateTerminalActivity(_terminalActivity);
            Assert.IsTrue(target.DeleteTerminalActivity((int)_terminalActivity.Id), "Delete failed");
        }
    
    }
}
