﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using VolumeAccountingServiceSoHo.BOLogControlWSRef;
using VolumeAccountingServiceSoHo.NMSGatewayServiceWSRef;
using VolumeAccountingServiceSoHo.Util;

namespace VolumeAccountingServiceSoHo
{
    class Program
    {
        private string _connectionString;
        private float _volThreshold;
        private int _days;
        private BOLogControlWS _boLogControl;

        static void Main(string[] args)
        {
            Program prg = new Program();
            prg.mainBody();
        }

        private void mainBody()
        {
            Logger.log("Processing started");
            _connectionString = Properties.Settings.Default.ConnectionString;
            _volThreshold = Properties.Settings.Default.VolumeThresholdBytes;
            _boLogControl = new BOLogControlWS();
            _days = Properties.Settings.Default.NotificationBeforeDays;

            //get unlocked terminals with a SoHo SLA
            string queryCmd = "select MacAddress, t.FullName, t.ispid, d.EMail ,s.SlaName from terminals t join servicepacks s on t.slaid = s.SlaID " +
                              "join Distributors d on t.distributorid = d.Id " +
                              "where t.admstatus = 2 and s.ServiceClass = 1";

            List<terminalObject> terminals = new List<terminalObject>();

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(queryCmd, conn))
                    {
                        // get the terminals
                        terminalObject to;
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            to = new terminalObject();
                            to.macAddress = reader.GetString(0);
                            to.termName = reader.GetString(1);
                            to.ispId = reader.GetInt32(2);
                            to.distEMail = reader.GetString(3);
                            to.slaName = reader.GetString(4);
                            terminals.Add(to);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
            }

            //check the remaining volume for each terminal
            NMSGatewayService nmsGateway = new NMSGatewayService();
            NMSTerminalInfo nmsTi;

            foreach (terminalObject t in terminals)
            {
                try
                {
                    string[] recipient = t.distEMail.Split(';');
                    nmsTi = nmsGateway.getTerminalInfoByMacAddress(t.macAddress);
                    t.remainingVolume = nmsTi.MaxVolSum - nmsTi.CurVolSum;
                    //if the volume is below threshold an email should be sent
                    //only send new notification if last notification is older than X days
                    if (!((t.slaName.IndexOf("unlimited", StringComparison.OrdinalIgnoreCase) >= 0) || (t.slaName.IndexOf("UNL", StringComparison.OrdinalIgnoreCase) >= 0)))
                    {
                        if (t.remainingVolume < _volThreshold && this.getLastNotificationForTerminal(t.macAddress) < DateTime.UtcNow.AddDays(-7))
                        {
                            if (this.sendMailVN(recipient, t.termName, t.macAddress))
                            {
                                //store the notifications sent
                                this.storeNotification(t.macAddress);
                            }
                        }
                    }
                    if (nmsTi.ExpirationDate.Date == DateTime.UtcNow.AddDays(_days).Date)
                    {
                        this.sendMailEN(recipient, t.termName, t.macAddress);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex);
                }
            }

            Logger.log("Processing finished");
        }

        private DateTime getLastNotificationForTerminal(string macAddress)
        {
            DateTime lastNotification = new DateTime(1970, 01, 01);

            string queryCmd = "SELECT TOP 1 TimeStamp FROM VolumeConsumptionAlertsSoHo " +
                              "WHERE MacAddress = @MacAddress " +
                              "ORDER BY TimeStamp DESC";
            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(queryCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@MacAddress", macAddress);
                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.Read())
                        {
                            lastNotification = reader.GetDateTime(0);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
            }

            return lastNotification;
        }

        private void storeNotification(string macAddress)
        {
            string insertCmd = "INSERT INTO VolumeConsumptionAlertsSoHo (TimeStamp, MacAddress)" +
                               "VALUES (@TimeStamp, @MacAddress)";

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(insertCmd, conn))
                    {
                        // write device information to database
                        cmd.Parameters.AddWithValue("@TimeStamp", DateTime.UtcNow);
                        cmd.Parameters.AddWithValue("@MacAddress", macAddress);

                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
            }
        }

        private bool sendMailVN(string[] recipient, string termName, string macAddress)
        {
            bool success = false;
            string body = "<p>Dear distributor, </br></p>" +
                          "<p>Please be advised that the volume on terminal: </br> </p> <p><b>" + termName + " MAC " + macAddress + "</b>.</br></p> <p>is almost zero. </br></p>" +
                          "<p>When the volume is used up, the terminal can no longer access the internet. </br></p>" +
                          "<p>You can renew your subscription by means of a voucher. </br></p>" +
                          "<p>Best regards, </br></p>" +
                          "<p>The SatADSL Team </br></p>" +
                          "<p>--------</br></p>" +
                          "<p>   </br></p>" +
                          "<p>Caro Distribuidor, </br></p>" +
                          "<p>Por favor tome em consideração que o volume do terminal: </br> </p> <p><b>" + termName + " MAC " + macAddress + "</b>.</br></p> <p>é quase zero. </br></p>" +
                          "<p>Quando o volume esgotar, o terminal perderá o acesso à internet. </br></p>" +
                          "<p>Poderá renovar a sua subscrição adquirindo um voucher. </br></p>" +
                          "<p>Os nossos melhores cumprimentos, </br></p>" +
                          "<p>A Equipa Satadsl </br></p>" +
                          "<p>--------</br></p>" +
                          "<p>   </br></p>" +
                          "<p>Cher Partenaire,</br></p>" +
                          "<p>Veuillez noter que le volume du terminal: </br> </p> <p><b>" + termName + " MAC " + macAddress + "</b>.</br></p> <p>est presque épuisé.</br></p>" +
                          "<p>Un fois ce volume utilisé, le terminal n'aura plus accès à Internet.</br></p>" +
                          "<p>Vous pouvez rétablir la connectivité en achetant une recharge de volume.</br></p>" +
                          "<p>Meilleures salutation, </br></p>" +
                          "<p>L'équipe SatADSL </br></p>";




            string subject = "SatADSL Volume Notification";
            string[] toAddresses = recipient;
            try
            {
                if (_boLogControl.SendMail(body, subject, toAddresses))
                {
                    Logger.log("Volume Notification Email sent to " + String.Join(";", recipient) + " for terminal " + macAddress);
                    success = true;
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
            }

            return success;
        }

        private bool sendMailEN(string[] recipient, string termName, string macAddress)
        {
            bool success = false;
            string body = "<p>Dear distributor, </br></p>" +
                          "<p>Please be advised that the volume on terminal: </br> </p> <p><b>" + termName + " MAC " + macAddress + "</b>.</br></p> <p>will reach expiration date in " + _days + " days. </br></p>" +
                          "<p>When the expiration date exceeded, the terminal can no longer access the internet. </br></p>" +
                          "<p>You can renew your subscription by means of a voucher. </br></p>" +
                          "<p>Best regards, </br></p>" +
                          "<p>The SatADSL Team </br></p>" +
                          "<p>--------</br></p>" +
                          "<p>   </br></p>" +
                          "<p>Caro Distribuidor, </br></p>" +
                          "<p>Por favor tome em consideração que o volume do terminal: </br> </p> <p><b>" + termName + " MAC " + macAddress + "</b>.</br></p> <p>atingirá a data de expiração dentro de " + _days + " dias. </br></p>" +
                          "<p>Quando o expiração esgotar, o terminal perderá o acesso à internet. </br></p>" +
                          "<p>Poderá renovar a sua subscrição adquirindo um voucher. </br></p>" +
                          "<p>Os nossos melhores cumprimentos, </br></p>" +
                          "<p>A Equipa Satadsl </br></p>" +
                          "<p>--------</br></p>" +
                          "<p>   </br></p>" +
                          "<p>Cher Partenaire,</br></p>" +
                          "<p>Veuillez noter que le volume du terminal: </br> </p> <p><b>" + termName + " MAC " + macAddress + "</b>.</br></p> <p>arrivera à expiration dans " + _days + " jours. </br></p>" +
                          "<p>Un fois ce expiration utilisé, le terminal n'aura plus accès à Internet.</br></p>" +
                          "<p>Vous pouvez rétablir la connectivité en achetant une recharge de volume.</br></p>" +
                          "<p>Meilleures salutation, </br></p>" +
                          "<p>L'équipe SatADSL </br></p>";

            string subject = "SatADSL Expiry Date Notification";
            string[] toAddresses = recipient;
            try
            {
                if (_boLogControl.SendMail(body, subject, toAddresses))
                {
                    Logger.log("Expiration Email sent to " + String.Join(";", recipient) + " for terminal " + macAddress);
                    success = true;
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
            }

            return success;
        }
        private class terminalObject
        {
            public string macAddress { get; set; }
            public string termName { get; set; }
            public int ispId { get; set; }
            public string distEMail { get; set; }
            public float remainingVolume { get; set; }
            public string slaName { get; set; }
        }
    }
}
