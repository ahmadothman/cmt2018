﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EutelSatNBITest.Model;
using ti = EutelSatNBITest.eutelSat.NBI.TerminalInterface;
using ip = EutelSatNBITest.eutelSat.NBI.Ipv4AddressPoolService;
using sl = EutelSatNBITest.eutelSat.NBI.SlaService;
using fr = EutelSatNBITest.eutelSat.NBI.ForwardResourceService;
using rr = EutelSatNBITest.eutelSat.NBI.ReturnResourcesService;
using perf = EutelSatNBITest.eutelsat.NBI.PerformanceInterface;
using System.Security.Cryptography.X509Certificates;

namespace EutelSatNBITest
{
    public partial class Default : System.Web.UI.Page
    {
        public NetworkCredential esCred;
        public ti.TerminalService ts;
        public ip.Ipv4AddressPoolService ipp;
        public sl.SlaService ss;
        public fr.ForwardResourcesService fwd;
        public rr.ReturnResourcesService rtn;
        public perf.PerformanceServiceImplService ps;
        public const int ispId = 16;
        public const int slaId = 108;
        public const int bladeId = 1;

        protected void Page_Load(object sender, EventArgs e)
        {
            ts = new ti.TerminalService();
            ipp = new ip.Ipv4AddressPoolService();
            ss = new sl.SlaService();
            fwd = new fr.ForwardResourcesService();
            rtn = new rr.ReturnResourcesService();
            ps = new perf.PerformanceServiceImplService();
            esCred = new NetworkCredential("satadsl_vno", "430vZiju");
            ts.Credentials = esCred;
            ipp.Credentials = esCred;
            ss.Credentials = esCred;
            fwd.Credentials = esCred;
            rtn.Credentials = esCred;
            ps.Credentials = esCred;
            //ps.Url = "https://172.21.3.20/nbi/performance/2.0/PerformanceInterface";
        }

        protected void ButtonCount_Click(object sender, EventArgs e)
        {
            ti.TerminalSearchCriteria searchCriteria = new ti.TerminalSearchCriteria();
            searchCriteria.ispId = ispId.ToString();
            ServicePointManager.ServerCertificateValidationCallback = delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
            int result = ts.countTerminals(searchCriteria);
            
            LabelCount.Text = result.ToString();

        }

        protected void ButtonTerminalDetails_Click(object sender, EventArgs e)
        {
            int sitId = Convert.ToInt32(TextBoxSitID.Text);
            ServicePointManager.ServerCertificateValidationCallback = delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
            ti.Terminal term = ts.findTerminal(ispId, true, sitId, true);
            LabelTerminalDetails.Text = "MAC: " + term.macAddress + ", IP: " + term.terminalNetworkConfig.nativeNetworkConfig.cpeAddress;
        }

        protected void ButtonListTerminals_Click(object sender, EventArgs e)
        {
            ti.TerminalSearchCriteria searchCriteria = new ti.TerminalSearchCriteria();
            searchCriteria.ispId = ispId.ToString();
            ServicePointManager.ServerCertificateValidationCallback = delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
            ti.Terminal[] terminals = ts.findTerminals(searchCriteria, 0, 20);
            
            DataGridTerminalList.DataSource = terminals;
            DataGridTerminalList.DataBind();
        }

        protected void ButtonCreateTerminal_Click(object sender, EventArgs e)
        {
            ti.TerminalParameters term = new ti.TerminalParameters();

            term.bladeId = bladeId.ToString();
            term.sitId = TextBoxNewSit.Text;
            term.macAddress = TextBoxNewMac.Text;
            term.name = TextBoxNewName.Text;
            term.ispId = ispId.ToString();
            term.slaId = slaId.ToString();
            term.returnCapacityGroupId = "101";
            term.forwardPoolId = "103";
            term.qosClassificationProfileId = "1";
            term.terminalNetworkConfig = new ti.TerminalProvisioningNetworkConfig();
            term.terminalNetworkConfig.nativeNetworkConfig = new ti.NativeNetworkConfig();
            term.terminalNetworkConfig.nativeNetworkConfig.cpeAddress = this.getAvailableIPAddress();
            term.terminalNetworkConfig.nativeNetworkConfig.ipv4AddressPoolId = "1";
            ServicePointManager.ServerCertificateValidationCallback = delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
            ti.ProvisioningRequest provReq = ts.createTerminal(term);

            LabelNewTerminal.Text = "Order ID: " + provReq.orderId + ", State: " + provReq.state + ", Scheduled date: " + provReq.scheduledDate;
        }

        protected void ButtonUpdateTerminal_Click(object sender, EventArgs e)
        {
            ServicePointManager.ServerCertificateValidationCallback = delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
            ti.Terminal term = ts.findTerminal(ispId, true, Convert.ToInt32(TextBoxUpdateSit.Text), true);
            
            ti.TerminalProvisioningParameters termPar = new ti.TerminalProvisioningParameters();

            termPar.sitId = term.terminalId;
            termPar.ispId = ispId.ToString();
            termPar.macAddress = TextBoxUpdateMac.Text;
            termPar.terminalNetworkConfig = term.terminalNetworkConfig;
            ServicePointManager.ServerCertificateValidationCallback = delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
            ti.ProvisioningRequest provReq = ts.updateTerminal(termPar);

            LabelUpdateTerminal.Text = "Order ID: " + provReq.orderId + ", State: " + provReq.state + ", Scheduled date: " + provReq.scheduledDate;
        }

        protected void ButtonChangeSLA_Click(object sender, EventArgs e)
        {
            ServicePointManager.ServerCertificateValidationCallback = delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
            ti.Terminal term = ts.findTerminal(ispId, true, Convert.ToInt32(TextBoxSLASit.Text), true);

            ti.TerminalProvisioningParameters termPar = new ti.TerminalProvisioningParameters();

            termPar.sitId = term.terminalId;
            termPar.ispId = ispId.ToString();
            termPar.slaId = TextBoxSLASLA.Text;
            termPar.terminalNetworkConfig = term.terminalNetworkConfig;
            ServicePointManager.ServerCertificateValidationCallback = delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
            ti.ProvisioningRequest provReq = ts.updateTerminal(termPar);

            LabelChangeSLA.Text = "Order ID: " + provReq.orderId + ", State: " + provReq.state + ", Scheduled date: " + provReq.scheduledDate;
        }

        protected void ButtonGetProcessedRequests_Click(object sender, EventArgs e)
        {
            DateTime startDate = new DateTime(2014, 1, 1);
            ServicePointManager.ServerCertificateValidationCallback = delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
            ti.provisioningRequestTerminalProvisioningLogEntry[] prLog = ts.findProcessedTerminalRequests(ispId, true, startDate, true, DateTime.Now, true);

            List<ti.ProvisioningRequest> provReqs = new List<ti.ProvisioningRequest>();
            List<ti.TerminalProvisioningLog> termProvLog = new List<ti.TerminalProvisioningLog>();

            foreach (ti.provisioningRequestTerminalProvisioningLogEntry pr in prLog)
            {
                provReqs.Add(pr.provisioningRequest);
                termProvLog.Add(pr.terminalProvisioningLog);
            }

            LabelProvisioningRequests.Text = "Provisioning requests:";
            DataGridProvisioningRequests.DataSource = provReqs;
            DataGridProvisioningRequests.DataBind();
            //LabelTerminalProvisioningLog.Text = "Terminal provisioning log:";
            //DataGridTerminalProvisioningLog.DataSource = termProvLog;
            //DataGridTerminalProvisioningLog.DataBind();
        }

        protected void ButtonLock_Click(object sender, EventArgs e)
        {
            ServicePointManager.ServerCertificateValidationCallback = delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
            ti.ProvisioningRequest provReq = ts.lockTerminal(ispId, true, Convert.ToInt32(TextBoxLock.Text), true);
            LabelLock.Text = "Order ID: " + provReq.orderId + ", State: " + provReq.state + ", Scheduled date: " + provReq.scheduledDate;
        }

        protected void ButtonUnLock_Click(object sender, EventArgs e)
        {
            ServicePointManager.ServerCertificateValidationCallback = delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
            ti.ProvisioningRequest provReq = ts.unlockTerminal(ispId, true, Convert.ToInt32(TextBoxUnlock.Text), true);
            LabelUnlock.Text = "Order ID: " + provReq.orderId + ", State: " + provReq.state + ", Scheduled date: " + provReq.scheduledDate;
        }

        protected void ButtonGetIPPools_Click(object sender, EventArgs e)
        {
            ServicePointManager.ServerCertificateValidationCallback = delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
            ip.Ipv4AddressPool[] ipPools = ipp.getIpv4AddressPoolsByServiceProvider(ispId, true);
            List<string> ipAddresses = new List<string>();
            bool exhausted = true;
            foreach (ip.Ipv4AddressPool ipPool in ipPools)
            {
                foreach (ip.Ipv4AddressRange ipRange in ipPool.ipAddressRanges)
                {
                    if (!ipRange.exhausted)
                    {
                        exhausted = false;
                        foreach (ip.ipRange freeRange in ipRange.freeRanges)
                        {
                            ipAddresses.Add("Pool: " + ipPool.id + ", IP range: " + ipRange.rangeIndex + ", IPs: " + freeRange.firstIpAddress + " - " + freeRange.lastIpAddress);
                        }
                    }
                }
            }

            if (!exhausted)
            {
                int steps = 0;
                bool breaker = false;
                foreach (ip.Ipv4AddressPool ipPool in ipPools)
                {
                    steps++;
                    foreach (ip.Ipv4AddressRange ipRange in ipPool.ipAddressRanges)
                    {
                        steps++; 
                        if (!ipRange.exhausted)
                        {
                            foreach (ip.ipRange freeRange in ipRange.freeRanges)
                            {
                                steps++; 
                                LabelFirstIP.Text = ("1st IP: " + freeRange.firstIpAddress + " Pool: " + ipPool.id + " Steps: " + steps);
                                breaker = true;
                                break;
                            }
                        }

                        if (breaker)
                        {
                            break;
                        }
                    }

                    if (breaker)
                    {
                        break;
                    }
                }

                DataGridIPPools.DataSource = ipAddresses;
                DataGridIPPools.DataBind();
            }
            else
            {
                LabelFirstIP.Text = "IP pool exhausted";
            }
            
        }

        protected string getAvailableIPAddress()
        {
            ServicePointManager.ServerCertificateValidationCallback = delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
            ip.Ipv4AddressPool[] ipPools = ipp.getIpv4AddressPoolsByServiceProvider(ispId, true);
            bool exhausted = true;
            foreach (ip.Ipv4AddressPool ipPool in ipPools)
            {
                foreach (ip.Ipv4AddressRange ipRange in ipPool.ipAddressRanges)
                {
                    if (!ipRange.exhausted)
                    {
                        exhausted = false;
                    }
                }
            }

            if (!exhausted)
            {
                return ipPools[0].ipAddressRanges[0].freeRanges[0].firstIpAddress;
            }
            else
            {
                return "exhausted";
            }
        }

        protected void ButtonGetSLAs_Click(object sender, EventArgs e)
        {
            ServicePointManager.ServerCertificateValidationCallback = delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
            sl.Sla[] slas = ss.findSlaByServiceProvider(ispId, true);
            DataGridSLAs.DataSource = slas;
            DataGridSLAs.DataBind();
        }

        protected void ButtonGetFwdResources_Click(object sender, EventArgs e)
        {
            ServicePointManager.ServerCertificateValidationCallback = delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
            fr.ForwardLink[] links = fwd.getAllForwardResourcesByServiceProvider(ispId, true);
            DataGridFWD.DataSource = links[0].forwardPool;
            DataGridFWD.DataBind();
        }

        protected void ButtonGetRTNResources_Click(object sender, EventArgs e)
        {
            ServicePointManager.ServerCertificateValidationCallback = delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
            rr.ReturnLink[] links = rtn.getAllReturnResourcesByServiceProvider(ispId, true);
            DataGridRTN.DataSource = links[0].returnCapacityGroup;
            DataGridRTN.DataBind();
        }

        protected void ButtonDeleteTerminal_Click(object sender, EventArgs e)
        {
            ServicePointManager.ServerCertificateValidationCallback = delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
            ti.ProvisioningRequest provReq = ts.deleteTerminal(ispId, true, Convert.ToInt32(TextBoxDelete.Text), true);
            LabelDelete.Text = "Action: " + provReq.action + ", Order ID: " + provReq.orderId + ", State: " + provReq.state;
        
        }

        protected void ButtonGetEsN0_Click(object sender, EventArgs e)
        {
            string sitId = "4190940";
            ServicePointManager.ServerCertificateValidationCallback = delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; }; //Handle inconsistent SSL certificates of hub
            perf.MetricDataQuery query = new perf.MetricDataQuery();
            query.attributeCriteria = new perf.AttributeQueryCriterion[1];
            perf.AttributeQueryCriterion criterion = new perf.AttributeQueryCriterion();
            criterion.attribute = "returnCNoAverage";
            criterion.granularities = new string[] { "PT5M" };
            query.attributeCriteria[0] = criterion;
            query.lastMetricOnly = true;
            query.domain = "newtec";
            query.type = "sit";
            //query.intervalStart = DateTime.Now.AddDays(-1).Ticks;
            //query.intervalEnd = DateTime.Now.Ticks;
            //query.intervalStartSpecified = true;
            //query.intervalEndSpecified = true;
            query.resourceKeys = new string[] { "ispId=16,sitId=" + sitId };
            try
            {
                perf.AttributeMetricData[] response = ps.findMetricData(query);
                string[] values = response[0].granularityMetricDatas[0].dsv.data.Split(';');
                LabelPointing.Text = values[2];
            }
            catch (Exception ex)
            {
                LabelPointing.Text = ex.Message;
            }
            
        }

    }
}