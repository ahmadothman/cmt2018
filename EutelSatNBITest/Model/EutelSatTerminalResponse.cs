﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EutelSatNBITest.Model
{
    public class EutelSatTerminalResponse
    {
        public int blade { get; set; }
        public string failureCause { get; set; }
        public string orderId { get; set; }
        public DateTime processedDate { get; set; }
        public DateTime requestDate { get; set; }
        public DateTime scheduledDate { get; set; }
        public string state { get; set; }
        public EutelSatSubSystemServiceOrders subSystemServiceOrders = new EutelSatSubSystemServiceOrders();
        public string user { get; set; }
        public string action { get; set; }
        public string provisionedElementDescription { get; set; }
        public string provisionedElementType { get; set; }
        public string uniqueProvisionedElementId { get; set; }
    }

    public class EutelSatSubSystemServiceOrders
    {
        public string bladeId { get; set; }
        public string failureCause { get; set; }
        public string state { get; set; }
        public string subSystemName { get; set; }
    }
}