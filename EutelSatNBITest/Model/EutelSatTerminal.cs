﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EutelSatNBITest.Model
{
    public class EutelSatTerminal
    {
        public string bladeId { get; set; }
        public string forwardPoolId { get; set; }
        public string ipsId { get; set; }
        public string macAddress { get; set; }
        public string name { get; set; }
        public string qosClassificationProfileId { get; set; }
        public string returnCapacityGroupId { get; set; }
        public string sitId { get; set; }
        public string slaId { get; set; }
        public EutelSatTerminalNetworkConfig terminalNetworkConfig = new EutelSatTerminalNetworkConfig();
    }

    public class EutelSatTerminalNetworkConfig
    {
        public EutelSatNativeNetworkConfig nativeNetworkConfig = new EutelSatNativeNetworkConfig();
        public int networkId { get; set; }
        public EutelSatVirtualNetworkConfig virtualNetworkConfig = new EutelSatVirtualNetworkConfig();
    }

    public class EutelSatNativeNetworkConfig
    {
        public string cpeAddress { get; set; }
        public string ipv4AddressPoolId  { get; set; }
        public string ipv6LanAddress  { get; set; }
        public string ipv6LanPrefixLength  { get; set; }
        public string ipv6PrefixPoolId  { get; set; }
        public string ipv6RoutedPrefixAddress  { get; set; }
        public string ipv6RoutedPrefixLength  { get; set; }
    }

    public class EutelSatVirtualNetworkConfig
    {
        public string ipv4DhcpMode { get; set; }
        public string ipv4LanAddress { get; set; }
        public bool ipv4LanAddressPingable { get; set; }
        public string ipv4LanPrefixLength { get; set; }
        public string ipv6LanAddress { get; set; }
        public bool ipv6LanAddressPingable { get; set; }
        public string ipv6LanPrefixLength { get; set; }
        public string ipv6RoutedPrefixAddress { get; set; }
        public string ipv6RoutedPrefixLength { get; set; }
    }
}