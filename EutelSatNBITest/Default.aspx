﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="EutelSatNBITest.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table>
            <tr>
                <td>
                    <asp:button runat="server" ID="ButtonCount" Text="Count Terminals" OnClick="ButtonCount_Click" />
                </td>
                <td>
                    <asp:label runat="server" ID="LabelCount"></asp:label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button runat="server" ID="ButtonListTerminals" Text="List all terminals" OnClick="ButtonListTerminals_Click" />
                </td>
                <td>
                    <asp:DataGrid runat="server" ID="DataGridTerminalList" AutoGenerateColumns="False">
                        <Columns>
                            <asp:BoundColumn DataField="terminalId" HeaderText="Sit ID"></asp:BoundColumn>
                            <asp:BoundColumn DataField="macAddress" HeaderText="MAC address"></asp:BoundColumn>
                            <asp:BoundColumn DataField="locked" HeaderText="Locked"></asp:BoundColumn>
                            <asp:BoundColumn DataField="provisioned" HeaderText="Provisioned"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </td>
            </tr>
            <tr>
                <td>
                    Get MAC address for Sit ID: <asp:TextBox runat="server" ID="TextBoxSitID" Width="50px"></asp:TextBox>
                    <asp:Button runat="server" ID="ButtonTerminalDetails" Text="Get MAC address" OnClick="ButtonTerminalDetails_Click" />
                </td>
                <td>
                    <asp:Label runat="server" ID="LabelTerminalDetails"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Create new terminal:
                    Name: <asp:textbox runat="server" ID="TextBoxNewName"></asp:textbox>
                    Sit ID: <asp:textbox runat="server" ID="TextBoxNewSit"></asp:textbox>
                    MAC address: <asp:textbox runat="server" ID="TextBoxNewMac"></asp:textbox>
                    <asp:Button runat="server" ID="ButtonCreateTerminal" Text="Create terminal" OnClick="ButtonCreateTerminal_Click" />
                </td>
                <td>
                    <asp:Label ID="LabelNewTerminal" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Delete terminal with Sit ID: <asp:TextBox runat="server" ID="TextBoxDelete"></asp:TextBox> <asp:Button runat="server" ID="ButtonDeleteTerminal" Text="Delete Terminal" OnClick="ButtonDeleteTerminal_Click" />
                </td>
                <td>
                    <asp:Label ID="LabelDelete" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Update terminal:
                    Sit ID: <asp:textbox runat="server" ID="TextBoxUpdateSit"></asp:textbox>
                    MAC address: <asp:textbox runat="server" ID="TextBoxUpdateMac"></asp:textbox>
                    <asp:Button runat="server" ID="ButtonUpdateTerminal" Text="Update terminal" OnClick="ButtonUpdateTerminal_Click" />
                </td>
                <td>
                    <asp:Label ID="LabelUpdateTerminal" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Change SLA:
                    Sit ID: <asp:textbox runat="server" ID="TextBoxSLASit"></asp:textbox>
                    New SLA: <asp:textbox runat="server" ID="TextBoxSLASLA"></asp:textbox>
                    <asp:Button runat="server" ID="ButtonChangeSLA" Text="Change SLA" OnClick="ButtonChangeSLA_Click" />
                </td>
                <td>
                    <asp:Label ID="LabelChangeSLA" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button runat="server" ID="ButtonGetProcessedRequests" Text="Get processed requests" onclick="ButtonGetProcessedRequests_Click"/>
                </td>
                <td>
                    <asp:label runat="server" ID="LabelProvisioningRequests"></asp:label>
                    <asp:DataGrid runat="server" ID="DataGridProvisioningRequests"></asp:DataGrid>
                    <asp:label runat="server" ID="LabelTerminalProvisioningLog"></asp:label>
                    <asp:DataGrid runat="server" ID="DataGridTerminalProvisioningLog"></asp:DataGrid>
                </td>
            </tr>
            <tr>
                <td>
                    Lock terminal with Sit-ID: <asp:TextBox runat="server" ID="TextBoxLock"></asp:TextBox> <asp:Button ID="ButtonLock" runat="server" Text="Lock terminal" OnClick="ButtonLock_Click" />
                </td>
                <td>
                    <asp:Label runat="server" ID="LabelLock"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    Unlock terminal with Sit-ID: <asp:TextBox runat="server" ID="TextBoxUnlock"></asp:TextBox> <asp:Button ID="ButtonUnlock" runat="server" Text="Unlock terminal" OnClick="ButtonUnLock_Click" />
                </td>
                <td>
                    <asp:Label runat="server" ID="LabelUnlock"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="ButtonGetIPPools" runat="server" Text="Get IP pools" OnClick="ButtonGetIPPools_Click" />
                </td>
                <td>
                    <asp:DataGrid ID="DataGridIPPools" runat="server"></asp:DataGrid><br /><asp:Label ID="LabelIPPool" runat="server"></asp:Label><br /><asp:Label ID="LabelFirstIP" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="ButtonGetSLAs" runat="server" Text="Get SLAs" OnClick="ButtonGetSLAs_Click" />
                </td>
                <td>
                    <asp:DataGrid ID="DataGridSLAs" runat="server"></asp:DataGrid>
                    <asp:Label ID="LabelSLAs" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="ButtonGetFwdResources" runat="server" Text="Get FWD resources" OnClick="ButtonGetFwdResources_Click" />
                </td>
                <td>
                    <asp:DataGrid ID="DataGridFWD" runat="server"></asp:DataGrid>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="ButtonGetRTNResources" runat="server" Text="Get RTN resources" OnClick="ButtonGetRTNResources_Click" />
                </td>
                <td>
                    <asp:DataGrid ID="DataGridRTN" runat="server"></asp:DataGrid>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="ButtonGetEsN0" runat="server" Text="Get Term pointing" OnClick="ButtonGetEsN0_Click" />
                </td>
                <td>
                    <asp:Label ID="LabelPointing" runat="server"></asp:Label>
                </td>
            </tr>
        </table>        
    </div>
    </form>
</body>
</html>


