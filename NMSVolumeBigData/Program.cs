﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using MySql.Data.MySqlClient;
using System.Configuration;
using NMSVolumeBigData.BOLogControlWSRef;
using NMSVolumeBigData.BOAccountingControlWSRef;
using NMSVolumeBigData.Util;
using NMSVolumeBigData.BOCassandraLibWSRef;
using System.Data.SqlClient;

namespace NMSVolumeBigData
{
    class Program
    {
        static void Main(string[] args)
        {
            Logger.log("Program started");
            GetTerminalVolumeList();
            ProcessRTFile();
        }

        //const bool _DEBUG = false;
        const bool _DEBUG = false;
        enum OperatingSystem { Windows, Linux }
        const OperatingSystem _OS = OperatingSystem.Linux;
        const string _FILEPATH_WINDOWS = "C:/Temp/";
        const string _FILEPATH_LINUX = "C:/Temp/";
        //const string _connectionStringDebugNMS = @"Server=localhost\SQLEXPRESS;database=NMS-debug;integrated security=SSPI;"; //;user=root;password=ebbe130561";
        const string _connectionStringDebugNMS = "Server=192.168.1.141;database=NMS-debug;user=bigdata;password=bigdata;";
        const string _connectionStringNMS = "Server=192.168.100.100;database=NMS;user=SatADSL;password=Fulvio";
        const string _connectionSTringCMT = "data source=CMT-STAGING;Database=CMTData-Production; User Id=SatFinAfrica; Password=Lagos#1302";
        //string _connectionString = "";
        static DateTime timeStamp = DateTime.UtcNow;
        static string timeStampString = timeStamp.ToString("yyyy-MM-dd HH:mm:ss");
        static List<TerminalVolume> terminalVolumes = new List<TerminalVolume>();

        //BOLogControlWS _logController = null;
        //BOAccountingControlWS _accountingController = null;
        static BOCassandraLibWS _cassandraClient = new BOCassandraLibWS();

        //static void Main(string[] args)
        //{
        //    Program prg = new Program();
        //    prg.mainBody(args[0]);
        //}

        //GET Terminal Data from NMS
        //private List<TerminalVolume> mainBody(string fileName)

        private static List<TerminalVolume> GetTerminalVolumeList()
        {
            string filePath = _FILEPATH_LINUX;
            string connectionString = _connectionStringNMS;

            if (_OS == OperatingSystem.Windows)
            {
                filePath = _FILEPATH_WINDOWS;
            }

            if (_DEBUG)
            {
                connectionString = _connectionStringDebugNMS;
            }

            using (MySqlConnection con = new MySqlConnection(connectionString))
            {
                TerminalVolume tv;
                con.Open();
                try
                {
                    //fetch the volume data from the NMS db
                    string queryCmd = "SELECT MacAddress, CurVolFWD, CurVolRTN, VolFWD, VolRTN, Isp, SitId, FreeZoneFWD, FreeZoneRTN FROM Terminals";
                    using (MySqlCommand command = new MySqlCommand(queryCmd, con))
                    {
                        MySqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            tv = new TerminalVolume();
                            tv.MacAddress = reader.GetInt64(0).ToString();
                            tv.AccVolFWD = reader.GetInt64(1);
                            tv.AccVolRTN = reader.GetInt64(2);
                            tv.PrevVolFWD = reader.GetInt64(3);
                            tv.PrevVolRTN = reader.GetInt64(4);
                            tv.IspId = reader.GetInt32(5);
                            tv.SitId = reader.GetInt32(6);
                            tv.FreeZoneFWD = reader.GetInt64(7);
                            tv.FreeZoneRTN = reader.GetInt64(8);
                            terminalVolumes.Add(tv);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex);
                }
            }
            Logger.log("Fetched data from NMS");

            //UPDATE the NMS DataBase - this has currently been disabled in this version as it is done by a parallell app
            //using (MySqlConnection con = new MySqlConnection(connectionString))
            //{
            //    try
            //    {
            //        string updateCmd = "UPDATE Terminals SET VolFWD = @VolFWD, VolRTN = @VolRTN WHERE MacAddress = @MacAddress";
            //        foreach (TerminalVolume t in terminalVolumes)
            //        {
            //            // realtime data is current volume minus previous volume
            //            // set negative values to 0 to compensate for volume resets
            //            t.RTVolFWD = t.AccVolFWD - t.PrevVolFWD;
            //            if (t.RTVolFWD < 0)
            //            {
            //                t.RTVolFWD = 0;
            //            }
            //            t.RTVolRTN = t.AccVolRTN - t.PrevVolRTN;
            //            if (t.RTVolRTN < 0)
            //            {
            //                t.RTVolRTN = 0;
            //            }
            //            // write current data to NMS db
            //            con.Open();
            //            using (MySqlCommand command = new MySqlCommand(updateCmd, con))
            //            {
            //                command.Parameters.AddWithValue("@MacAddress", t.MacAddress);
            //                command.Parameters.AddWithValue("@VolFWD", t.AccVolFWD);
            //                command.Parameters.AddWithValue("@VolRTN", t.AccVolRTN);
            //                command.ExecuteNonQuery();
            //            }
            //            con.Close();
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        Logger.LogError(ex);
            //    }
            //}
            return terminalVolumes;
        }

        //Updata Cassandra BigData DB
        //public bool ProcessRTFile(string fileName)
        private static bool ProcessRTFile()
        {
            Logger.LogInfo("Processing file: " + timeStampString, "Realtime NMS Volume Import");
            bool success = false;
            //char delimiter = Properties.Settings.Default.CsvInputDelimiter;
            //string[] dataFields;

            List<VolumeDataEntry> cassandraList = new List<VolumeDataEntry>();
            VolumeDataEntry vde;
            int counter = 0; //a counter to determine the number of objects

            // Get values from terminalVolumes List (by method above GetTerminalVolumeList) and insert them into VolumeHistoryDetail2 table in CMT
            try
            {
                //writing data to CMT db is disabled in this version 
                
                foreach (TerminalVolume tv in terminalVolumes)
                {

                    //Set the Cassandra object and add it to the list
                    vde = new VolumeDataEntry();
                    vde.sitid = tv.SitId;
                    vde.ispid = tv.IspId;
                    vde.time_stamp = timeStamp;
                    vde.forwardvolume = tv.RTVolFWD;
                    vde.returnvolume = tv.RTVolRTN;
                    vde.accumulatedforwardvolume = tv.AccVolFWD;
                    vde.accumulatedreturnvolume = tv.AccVolRTN;
                    vde.freezonertn = tv.FreeZoneRTN;
                    vde.freezonefwd = tv.FreeZoneFWD;
                    cassandraList.Add(vde);
                    counter++;
                }

                Logger.log("Cassandra object has been prepared");
                success = true;

                try
                {
                    //Insert the data in the Cassandra table
                    VolumeDataEntry[] cassandraArray = new VolumeDataEntry[counter]; //the counter is used to determine the length of the array
                    counter = 0; //the counter is reset so it can be used for selecting the right object in the array
                    foreach (VolumeDataEntry v in cassandraList)
                    {
                        cassandraArray[counter] = v;
                        counter++;
                    }

                    _cassandraClient.InsertVolumeData(cassandraArray, timeStampString);
                    Logger.log("Inserting data in Cassandra succeeded");
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex);
                    Logger.log("Inserting data in Cassandra failed");
                }

            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                Logger.log("Inserting data in CMT db failed");
            }

            return success;
        }

        //Update CMT DB
        private static void InsertData(TerminalVolume tv, string tableName)
        {
            string insertCmd = "INSERT INTO " + tableName + " (IspId, [SIT-ID], time_stamp, " +
                    "ForwardedVolume, ReturnVolume, ForwardedPackets, ReturnPackets, " +
                    "AccumulatedForwardVolume, AccumulatedReturnVolume, HighPriorityForwardedVolume, " +
                    "HighPriorityReturnVolume, HighPriorityForwardedPackets, HighPriorityReturnPackets, " +
                    "RealTimeForwardedVolume, RealTimeReturnVolume, RealTimeForwardedPackets, " +
                    "RealTimeReturnPackets, TotalForwardedVolume, TotalReturnVolume, " +
                    "TotalForwardedPackets, TotalReturnPackets, FreeZoneFWD, FreeZoneRTN) " +
                    "VALUES (@IspId, @SIT_ID, @time_stamp, @ForwardedVolume, @ReturnVolume, 0, 0, " +
                    "@AccumulatedForwardVolume, @AccumulatedReturnVolume, 0, 0, 0, 0, 0, 0, 0, 0, @TotalForwardedVolume, @TotalReturnVolume, 0, 0, @FreeZoneFWD, @FreeZoneRTN)";

            try
            {
                using (SqlConnection conn = new SqlConnection(_connectionSTringCMT))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand(insertCmd, conn))
                    {
                        cmd.Parameters.AddWithValue("@IspId", tv.IspId);
                        cmd.Parameters.AddWithValue("@SIT_ID", tv.SitId);
                        cmd.Parameters.AddWithValue("@time_stamp", timeStamp);
                        cmd.Parameters.AddWithValue("@AccumulatedForwardVolume", tv.AccVolFWD);
                        cmd.Parameters.AddWithValue("@AccumulatedReturnVolume", tv.AccVolRTN);
                        cmd.Parameters.AddWithValue("@ForwardedVolume", tv.AccVolFWD);
                        cmd.Parameters.AddWithValue("@ReturnVolume", tv.AccVolRTN);
                        cmd.Parameters.AddWithValue("@TotalForwardedVolume", tv.RTVolFWD);
                        cmd.Parameters.AddWithValue("@TotalReturnVolume", tv.RTVolRTN);
                        cmd.Parameters.AddWithValue("@FreeZoneFWD", tv.FreeZoneFWD);
                        cmd.Parameters.AddWithValue("@FreeZoneRTN", tv.FreeZoneRTN);

                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                Logger.log("Inserting data into CMT db failed");
            }
        }
        /// <summary>
        /// This class is used to create a terminal volume object
        /// containing only the values necessary for the volume data
        /// </summary>
        private class TerminalVolume
        {
            public int IspId { get; set; }
            public int SitId { get; set; }
            public string MacAddress { get; set; }
            public Int64 AccVolRTN { get; set; }
            public Int64 AccVolFWD { get; set; }
            public Int64 PrevVolRTN { get; set; }
            public Int64 PrevVolFWD { get; set; }
            public Int64 RTVolRTN { get; set; }
            public Int64 RTVolFWD { get; set; }
            public Int64 FreeZoneRTN { get; set; }
            public Int64 FreeZoneFWD { get; set; }
        }
    }
}
