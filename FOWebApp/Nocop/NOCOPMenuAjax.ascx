﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NOCOPMenuAjax.ascx.cs" Inherits="FOWebApp.Nocop.NOCOPMenuAjax" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<telerik:RadTreeView ID="RadTreeViewDistributorMenu" runat="server"
    OnNodeClick="RadTreeViewDistributorMenu_NodeClick" Skin="Metro"
    ValidationGroup="Administrator main menu">
    <Nodes>
        <telerik:RadTreeNode runat="server" Expanded="True"
            ImageUrl="~/Images/earth_preferences.png" Owner="RadTreeViewDistributorMenu"
            Text="NOCOP Menu" Font-Bold="True" Value="n_Tasks">
            <Nodes>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/businesspeople.png"
                    Owner="" Text="Distributors" Value="n_Distributors" ToolTip="Distributor Management">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Owner="" Text="New distributor"
                            Value="n_New_Distributor" ToolTip="Create a new Distributor">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" Owner="" Text="Terminals" Value="n_Terminals"
                    ImageUrl="~/Images/Terminal.png" ToolTip="Terminal Management">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Owner="" Text="New Terminal" Value="n_New_Terminal" ToolTip="Create a new Terminal"></telerik:RadTreeNode>
                        <telerik:RadTreeNode runat="server" Owner="" Text="Search By MAC"
                            Value="n_Search_By_MAC" Visible="False">
                        </telerik:RadTreeNode>
                        <telerik:RadTreeNode runat="server" Owner="" Text="Search By SitId"
                            Value="n_Search_By_SitId" Visible="False">
                        </telerik:RadTreeNode>
                        <telerik:RadTreeNode runat="server" Text="Search"
                            Value="n_Advanced_Search" ToolTip="Find a Terminal with and advanced query">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/customersNew.png"
                    Owner="" Text="Customers" Value="n_Customers" ToolTip="Customer Management">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Owner="" Text="New Customer"
                            Value="n_New_Customer" ToolTip="Create a new Customer">
                        </telerik:RadTreeNode>
                        <telerik:RadTreeNode runat="server" Owner="" Text="Delete Customer"
                            Value="n_Delete_Customer" ToolTip="Remove a Customer">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/customers.png"
                    Text="End Users" Value="n_EndUsers" ToolTip="End User Management">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Text="New End User" Value="n_NewEndUser" ToolTip="Create a new End User">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/form_blue.png" Owner=""
                    Text="Requests" Value="n_Requests" ToolTip="Waiting activation requests">
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/lock_ok.png" Text="Change Password"
                    ToolTip="Change your password" Value="n_Change_Password">
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/report.png"
                    Text="Reports" Value="n_Reports" ToolTip="Operational reports">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Text="Activations"
                            Value="n_Reports_Activations" ToolTip="Activations done over a period of time">
                        </telerik:RadTreeNode>
                        <telerik:RadTreeNode runat="server" Text="Accounting"
                            Value="n_Reports_Accounting" ToolTip="Accounting report over a period of time">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/books_blue_preferences.png" Text="Documents" ToolTip="Available Documents" Value="n_Library">
                </telerik:RadTreeNode>
            </Nodes>
        </telerik:RadTreeNode>
        <telerik:RadTreeNode ID="AboutNode" runat="server" ImageUrl="~/Images/information.png"
            Text="About" Value="n_About" ToolTip="Information about this application">
        </telerik:RadTreeNode>
        <telerik:RadTreeNode runat="server" ImageUrl="~/Images/exit.png" Text="Log Off"
            ToolTip="Close the session" Value="n_log_off">
        </telerik:RadTreeNode>
    </Nodes>
</telerik:RadTreeView>
