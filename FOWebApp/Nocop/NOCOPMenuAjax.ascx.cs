﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using Telerik.Web.UI;
using FOWebApp.Nocsa;

namespace FOWebApp.Nocop
{
    public partial class NOCOPMenuAjax : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //Set the user name as root node of the treeview
                if (HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    string user = HttpContext.Current.User.Identity.Name;
                    RadTreeViewDistributorMenu.Nodes[0].Text = "Welcome " + user /*+ " - Tasks"*/;
                }

                AboutNode.Visible = !(Boolean)Session["WhiteLabel"]; //No about node in case of a non-
                //branded CMT
            }

            //Load the active component into the Main content placeholder
            RadTreeNode selectedNode = RadTreeViewDistributorMenu.SelectedNode;

            if (selectedNode != null)
            {
                //Get the MainContentPanel
                Control sheetContainer = scanForControl("SheetContentPlaceHolder", this);

                if (sheetContainer != null)
                {
                    Control contentContainer = sheetContainer.FindControl("MainContentPlaceHolder");

                    if (contentContainer != null)
                    {
                        contentContainer.Controls.Clear();
                        Control contentControl = this.LoadUserControl(selectedNode.Value);
                        if (contentControl != null)
                        {
                            contentControl.ID = selectedNode.Value + "_ID";
                            contentContainer.Controls.Add(contentControl);
                        }
                    }
                }
            }
        }

        protected void RadTreeViewDistributorMenu_NodeClick(object sender, Telerik.Web.UI.RadTreeNodeEventArgs e)
        {

        }

        protected Control scanForControl(string controlId, Control root)
        {
            Control resCtrl = root.FindControl(controlId);

            if (resCtrl == null)
            {
                root = root.Parent;
                if (root != null)
                {
                    resCtrl = this.scanForControl(controlId, root.Parent);
                }
            }

            return resCtrl;
        }

        protected Control LoadUserControl(string nodeSelector)
        {
            Control control = null;
            if (nodeSelector.Equals("n_ISP"))
            {
                control = Page.LoadControl("~/Nocsa/ISPList.ascx");
            } 
            else if (nodeSelector.Equals("n_New_ISP"))
            {
                control = Page.LoadControl("~/Nocsa/ISPDetails.ascx");
            }
            else if (nodeSelector.Equals("n_log_off"))
            {
                Boolean branding = (Boolean)Session["WhiteLabel"]; //Keep the session variable

                //Close the session
                Session.Abandon();
                FormsAuthentication.SignOut();
                if (branding)
                {
                    Response.Redirect("Default.aspx?Branding=Wl");
                }
                else
                {
                    Response.Redirect("Default.aspx");
                }
            }
            else if (nodeSelector.Equals("n_Distributors"))
            {
                control = Page.LoadControl("~/Nocsa/DistributorList.ascx");
            }
            else if (nodeSelector.Equals("n_New_Distributor"))
            {
                DistributorDetailsForm df = 
                    (DistributorDetailsForm) Page.LoadControl("~/Nocsa/DistributorDetailsForm.ascx");
                control = df;
            }
            else if (nodeSelector.Equals("n_Search_Distributor_By_Name"))
            {
                control = Page.LoadControl("TestUserControl.ascx");
            }
            else if (nodeSelector.Equals("n_Terminals"))
            {
                control = Page.LoadControl("~/Nocsa/TerminalList.ascx"); ;
            }
            else if (nodeSelector.Equals("n_New_Terminal"))
            {
                control = Page.LoadControl("~/Nocsa/CreateNewTerminal.ascx");
            }
            else if (nodeSelector.Equals("n_Search_By_MAC"))
            {
                control = Page.LoadControl("TerminalSearchByMac.ascx");
            }
            else if (nodeSelector.Equals("n_Search_By_SitId"))
            {
                control = Page.LoadControl("TerminalSearchBySitId.ascx");
            }
            else if (nodeSelector.Equals("n_Advanced_Search"))
            {
                control = Page.LoadControl("~/Nocsa/AdvancedSearch2.ascx");
            }
            else if (nodeSelector.Equals("n_Customers"))
            {
                control = Page.LoadControl("~/Nocsa/CustomerList.ascx");
            }
            else if (nodeSelector.Equals("n_Search_Customer_By_Name"))
            {
                control = Page.LoadControl("TestUserControl.ascx");
            }
            else if (nodeSelector.Equals("n_New_Customer"))
            {
                control = Page.LoadControl("~/Nocsa/NewCustomerForm.ascx");
            }
            else if (nodeSelector.Equals("n_EndUsers"))
            {
                control = Page.LoadControl("~/Nocsa/EndUserList.ascx");
            }
            else if (nodeSelector.Equals("n_NewEndUser"))
            {
                control = Page.LoadControl("~/Nocsa/NewEndUser.ascx");
            }
            else if (nodeSelector.Equals("n_Requests"))
            {
                control = Page.LoadControl("~/Nocsa/TerminalActivationRequestList.ascx");
            }
            else if (nodeSelector.Equals("n_Activations"))
            {
                control = Page.LoadControl("TestUserControl.ascx");
            }
            else if (nodeSelector.Equals("n_Suspensions"))
            {
                control = Page.LoadControl("TestUserControl.ascx");
            }
            else if (nodeSelector.Equals("n_Change_Password"))
            {
                control = Page.LoadControl("ChangePassword.ascx");
            }
            else if (nodeSelector.Equals("n_Logs"))
            {
                control = Page.LoadControl("TestUserControl.ascx");
            }
            else if (nodeSelector.Equals("n_Usage_Log"))
            {
                control = Page.LoadControl("~/Nocsa/UserActivityLogList.ascx");
            }
            else if (nodeSelector.Equals("n_Activity_Log"))
            {
                control = Page.LoadControl("TestUserControl.ascx");
            }
            else if (nodeSelector.Equals("n_Error_Log"))
            {
                control = Page.LoadControl("TestUserControl.ascx");
            }
            else if (nodeSelector.Equals("n_Users"))
            {
                control = Page.LoadControl("~/Nocsa/UserMaintenanceList.ascx");
            }
            else if (nodeSelector.Equals("n_New_User"))
            {
                control = Page.LoadControl("~/Nocsa/CreateUser.ascx");
            }
            else if (nodeSelector.Equals("n_Search_User_By_Name"))
            {
                control = Page.LoadControl("TestUserControl.ascx");
            }
            else if (nodeSelector.Equals("n_About"))
            {
                control = Page.LoadControl("AboutControl.ascx");
            }
            else if (nodeSelector.Equals("n_Tasks"))
            {
                control = Page.LoadControl("~/Nocsa/TaskList.ascx");
            }
            else if (nodeSelector.Equals("n_Reports_Accounting"))
            {
                control = Page.LoadControl("~/Nocsa/AccountReport.ascx");
            }
            else if (nodeSelector == "n_Library")
            {
                control = Page.LoadControl("~/DocumentLibrary.ascx");
            }
            else
            {
                control = Page.LoadControl("BlankControl.ascx");
            }

            return control;
        }
    }
}