﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TerminalNotFound.ascx.cs" Inherits="FOWebApp.TerminalNotFound" %>


<div style="padding: 30px">
    <p>
        <span style="border-spacing: 20px; padding: 10px">Error message:</span><span style="font-weight: bold; color: #ff0000">Terminal not found</span>
    </p>
    
</div>