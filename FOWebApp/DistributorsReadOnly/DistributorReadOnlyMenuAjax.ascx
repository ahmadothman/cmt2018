﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DistributorReadOnlyMenuAjax.ascx.cs" Inherits="FOWebApp.DistributorsReadOnly.DistributorReadOnlyMenuAjax" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<telerik:RadTreeView ID="RadTreeViewDistributorReadOnlyMenu" runat="server"
    OnNodeClick="RadTreeViewDistributorReadOnlyMenu_NodeClick" Skin="Metro"
    ValidationGroup="DistributorsReadOnly main menu">
    <Nodes>
        <telerik:RadTreeNode runat="server" Expanded="True"
            ImageUrl="~/Images/earth_preferences.png" Owner="RadTreeViewDistributorReadOnlyMenu"
            Text="Distributor Read-Only Menu" Font-Bold="True">
            <Nodes>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/Terminal.png"
                    Owner="" Text="Terminals" Value="n_Terminals" ToolTip="Manage and Monitor Terminals">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Owner="" Text="Search by MAC"
                            Value="n_Search_By_Mac" ToolTip="Find a terminal by means of its MAC Address">
                        </telerik:RadTreeNode>
                        <telerik:RadTreeNode runat="server" Owner="" Text="Search by SitId"
                            Value="n_Search_By_SitId" ToolTip="Find a terminal by means of its SitId">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/Account.png" Text="Account"
                    ToolTip="Show account information" Value="n_account">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Text="Change Password"
                            Value="n_Change_Password" ToolTip="Change your password">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
            </Nodes>
        </telerik:RadTreeNode>
        <telerik:RadTreeNode ID="AboutNode" runat="server" ImageUrl="~/Images/information.png"
            Text="About" Value="n_about" ToolTip="About this application">
        </telerik:RadTreeNode>
        <telerik:RadTreeNode runat="server" ImageUrl="~/Images/exit.png" Text="Log Off"
            ToolTip="Close the session" Value="n_log_off">
        </telerik:RadTreeNode>
    </Nodes>
</telerik:RadTreeView>
