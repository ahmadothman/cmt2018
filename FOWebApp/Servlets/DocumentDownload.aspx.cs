﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Configuration;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;

namespace FOWebApp.Servlets
{
    public partial class DocumentDownload : System.Web.UI.Page
    {
        BOLogControlWS _boLogController = null;

        public DocumentDownload()
        {
            _boLogController = new BOLogControlWS();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.HttpMethod.Equals("GET"))
            {
                //Retrieve the parameters from the URL (batch id)
                try
                {
                    string docName = Request.Params["DocumentName"];

                    //Return the document
                    this.DownloadDocument(docName);
                    Response.End();
                  
                }
                catch (Exception ex)
                {
                    CmtApplicationException cmtEx = new CmtApplicationException();
                    cmtEx.ExceptionDesc = ex.Message;
                    cmtEx.ExceptionStacktrace = ex.StackTrace;
                    cmtEx.UserDescription = "AccountReportDownload  servlet failed!";
                    _boLogController.LogApplicationException(cmtEx);
                }
            }
        }

        private void DownloadDocument(string docName)
        {
            //Downloads the document from the document store
            String path = ConfigurationManager.AppSettings["RadAsyncUploadDoc.TargetFolder"] + "/";
            String fullFileName = path + docName;

            FileInfo fi = new FileInfo(fullFileName);
            if (!fi.Exists)
            {
                Response.StatusCode = 500;
                Response.StatusDescription = "File: " + fullFileName + " not found";
            }
            else
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + fi.Name);
                Response.AddHeader("Content-Length", fi.Length.ToString());
                String extension = fi.Extension;
                if (extension.Equals("pdf"))
                {
                    Response.ContentType = "application/pdf";
                }
                else if (extension.Equals("doc"))
                {
                    Response.ContentType = "application/msword";
                }
                else if (extension.Equals("docx"))
                {
                    Response.ContentType = "application/msword";
                }
                else if (extension.Equals("xls"))
                {
                    Response.ContentType = "application/excel";
                }
                else if (extension.Equals("xls"))
                {
                    Response.ContentType = "application/excel";
                }
                else
                {
                    Response.ContentType = "application/octet-stream";
                }
                Response.Flush();
                Response.TransmitFile(fi.FullName);
            }
        }
        
    }
}