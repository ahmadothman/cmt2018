﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FOWebApp.Servlets
{
    public partial class MultiCastUserData : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            /* 
             This servlet checks at the login screen, if a user is in Multicast Service Operator roll.
             If so, the user will be redirected to the MultiCastViewer app. If not, acces will be denied.
             FOWebApp/Default.aspx.cs gets the URL to which the user will be redirected from the FOWebApp/Web.config/<appSettings>
             (!) In the FOWebApp/Servlets/LogOut.aspx.cs there is a reference to this comment, if this class were to be removed, copy the comment to LogOut (!)
            */

            if (Request.IsAuthenticated && Roles.IsUserInRole("MulticastServiceOperator"))
            {
                string userId = Membership.GetUser().ProviderUserKey.ToString();
                string successUrl = Request.QueryString["successUrl"];
                if (!string.IsNullOrWhiteSpace(successUrl))
                {
                    Response.Redirect(string.Format("{0}&userId={1}", successUrl, userId), true);
                }
                else
                {
                    Response.Write(userId);
                }
            }
            else
            {
                string accessDeniedUrl = Request.QueryString["accessDeniedUrl"];
                if (!string.IsNullOrWhiteSpace(accessDeniedUrl))
                {
                    Response.Redirect(accessDeniedUrl, true);
                }
                else
                {
                    Response.StatusCode = 400; //bad request
                    Response.Write("error");
                }
            }
        }
    }
}