﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;
using FOWebApp.Data;

namespace FOWebApp.Servlets
{
    public partial class AccountReportDownload : System.Web.UI.Page
    {
        BOLogControlWS _boLogController = null;
        DataHelper _dataHelper = null;

        public AccountReportDownload()
        {
            _boLogController = new BOLogControlWS();
            _dataHelper = new DataHelper();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.HttpMethod.Equals("GET"))
            {
                //Retrieve the parameters from the URL (batch id)
                try
                {
                    string startDate = Request.Params["start"];
                    string endDate = Request.Params["end"];
                    this.DownloadAccountingData(startDate, endDate);
                }
                catch (Exception ex)
                {
                    CmtApplicationException cmtEx = new CmtApplicationException();
                    cmtEx.ExceptionDesc = ex.Message;
                    cmtEx.ExceptionStacktrace = ex.StackTrace;
                    cmtEx.UserDescription = "AccountReportDownload  servlet failed!";
                    _boLogController.LogApplicationException(cmtEx);
                    Response.StatusCode = 400; //Bad request
                }
            }
        }


        protected void DownloadAccountingData(string startDate, string endDate)
        {

        }
    }
}