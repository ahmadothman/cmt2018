﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FOWebApp.Servlets
{
    public partial class LogOut : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Comment for the MultiCast servlets can be found in FOWebApp/Servlets/MultiastUserData.aspx.cs

            //Boolean branding = (Boolean)Session["WhiteLabel"]; //Keep the session variable
            
            //Close the session
            Session.Abandon();
            FormsAuthentication.SignOut();
            //if (branding)
            //{
            //    Response.Redirect("~/Default.aspx?Branding=Wl");
            //}
            //else
            //{
                Response.Redirect("~/Default.aspx");
            //}
        }
    }
}