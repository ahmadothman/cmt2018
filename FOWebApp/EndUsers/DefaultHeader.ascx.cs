﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;

namespace FOWebApp.EndUsers
{
    public partial class DefaultHeader : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BOAccountingControlWS boAccountingControlWS = new BOAccountingControlWS();

            //Set the SitId necessary for loading the terminal details
            MembershipUser myObject = Membership.GetUser();
            string userId = myObject.ProviderUserKey.ToString();

            EndUser endUser = boAccountingControlWS.GetEndUser(userId);

            if (endUser != null)
            {
                LabelName.Text = endUser.FirstName + " " + endUser.LastName;
            }

        }
    }
}