﻿<%@ Page Title="" Language="C#" MasterPageFile="~/design/EndUserSite.Master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="FOWebApp.EndUsers.Default" %>
<asp:Content ID="ContentBody" ContentPlaceHolderID="BodyContentPlaceHolder" runat="server">
    <asp:PlaceHolder ID="PlaceHolderHeader" runat="server"></asp:PlaceHolder>
    <div id="main-container">
        <div class="wrapper clearfix">
            <div id="path">
                <span>You are here:</span> <a href="http://www.satadsl.net/index.html">Home</a> &gt; <a href="#">My SatADSL</a>
            </div>
            <aside>
				<h2>Our offer</h2>
				<table cellspacing="0">
					<tr>
						<th>Service Package<br>recommended price to end users</th>
						<th class="datarate">Maximum<br />Data rate<br>Down/Up (kbps)
					</tr>
					<tr>
						<td>Home <span>$49/month</span></td>
						<td class="datarate">128 / 32</td>
					</tr>
					<tr>
						<td>Starter <span>$89/month</span></td>
						<td class="datarate">256 / 64</td>
					</tr>
					<tr>
						<td>Basic <span>$119/month</span></td>
						<td class="datarate">512 / 96</td>
					</tr>
					<tr>
						<td>Expert <span>$179/month</span></td>
						<td class="datarate">1024 / 128</td>
					</tr>
					<tr>
						<td>Pro <span>$299/month</span></td>
						<td class="datarate">2048 / 128</td>
					</tr>
				</table>
				<a href="http://www.satadsl.net/prices.html" class="button">View all prices</a>
			</aside>
             <div id="main">
                <asp:PlaceHolder ID="PlaceHolderBody" runat="server"></asp:PlaceHolder>
            </div>
         </div>
     </div>
     <div id="footer-container">
        <footer class="wrapper clearfix">
            <div id="footerLinks">
				<span id="copyright">&copy;2012 SatADSL</span>
				<a href="#">Disclaimer</a>
				<a href="#" id="jobsLink">Jobs</a>
			</div>
        </footer>
	 </div>
</asp:Content>
