﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;

namespace FOWebApp.EndUsers
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BOAccountingControlWS boAccountingControlWS = new BOAccountingControlWS();

            //Add the about control to the content pane
            Control aboutControl = Page.LoadControl("~/EndUsers/DefaultHeader.ascx");
            PlaceHolderHeader.Controls.Clear();
            PlaceHolderHeader.Controls.Add(aboutControl);

            TerminalTabView tabViewControl = (TerminalTabView)Page.LoadControl("~/EndUsers/TerminalTabView.ascx");

            //Set the SitId necessary for loading the terminal details
            MembershipUser myObject = Membership.GetUser();
            string userId = myObject.ProviderUserKey.ToString();

            EndUser endUser = boAccountingControlWS.GetEndUser(userId);

            if (endUser != null)
            {

                tabViewControl.SitId = (int)endUser.SitId;
                PlaceHolderBody.Controls.Clear();
                PlaceHolderBody.Controls.Add(tabViewControl);
            }
        }
    }
}