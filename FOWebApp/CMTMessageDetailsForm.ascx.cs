﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Drawing;
using Telerik.Web.UI;
using FOWebApp.net.nimera.cmt.BOMonitorControlWSRef;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;
using FOWebApp.net.nimera.cmt.BOCMTMessageControllerWSRef;

namespace FOWebApp
{
    public partial class CMTMessageDetailsForm : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {   
        }

        public void componentDataInit(string messageId)
        {
            message = _messageControl.GetCMTMessage(messageId);
            LabelRecipient.Text = message.RecipientFullName;
            RadDateTimePickerStartTime.SelectedDate = message.StartTime;
            RadDateTimePickerEndTime.SelectedDate = message.EndTime;
            TextBoxTitle.Text = message.MessageTitle;
            LabelContent2.Text = message.MessageContent;
            CheckBoxRelease.Checked = message.Release;

            if (Roles.IsUserInRole("NOC Administrator"))
            {
                LabelContent1.Visible = false;
                LabelContent2.Visible = false;
                LabelContent3.Visible = true;
                RadEditorMessage.Visible = true;
                RadEditorMessage.Content = message.MessageContent;
                RadDateTimePickerEndTime.Enabled = true;
                RadDateTimePickerStartTime.Enabled = true;
                TextBoxTitle.ReadOnly = false;
                ButtonUpdate.Visible = true;
                ButtonUpdate.Enabled = true;
            }
        }

        protected void ButtonUpdate_Click(object sender, EventArgs e)
        {
            message.MessageTitle = TextBoxTitle.Text;
            message.MessageContent = RadEditorMessage.Content;
            message.EndTime = (DateTime)RadDateTimePickerEndTime.SelectedDate;
            message.StartTime = (DateTime)RadDateTimePickerStartTime.SelectedDate;
            message.Release = CheckBoxRelease.Checked;

            if (_messageControl.UpdateCMTMessage(message))
            {
                LabelResult.Text = "Message succesfully updated";
                LabelResult.ForeColor = Color.Green;
            }
            else
            {
                LabelResult.Text = "Updating message failed";
                LabelResult.ForeColor = Color.Red;
            }
        }

        public BOCMTMessageControllerWS _messageControl = new BOCMTMessageControllerWS();
        public BOAccountingControlWS _boAccountingControlWS = new BOAccountingControlWS();
        public CMTMessage message;
    }
}