﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CustomerListForDistributor.ascx.cs"
    Inherits="FOWebApp.CustomerListForDistributor" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script type="text/javascript">
    function RowSelected(sender, eventArgs) {
        var grid = sender;

        var detailTables = grid.get_detailTables();
        if (detailTables.length == 1) {
            var dataItems = detailTables[0].get_selectedItems();
            if (dataItems.length != 0) {
                var macAddress = dataItems[0].get_element().cells[1].innerHTML;
                var oWnd = radopen("TerminalDetailsForm.aspx?mac=" + macAddress, "RadWindowDetails");
            }
        }
        else {
            //The master table has been clicked
            var masterTable = grid.get_masterTableView();
            var dataItems = masterTable.get_selectedItems();
            if (dataItems.length != 0) {
                var custId = dataItems[0].get_element().cells[1].innerHTML;
                var oWnd = radopen("CustomerDetailsForm.aspx?id=" + custId, "RadWindowDetails");
            }
        }
    }
</script>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="RadGridCustomers">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGridCustomers" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
    <telerik:RadGrid ID="RadGridCustomers" runat="server" AllowPaging="True" AllowSorting="True"
        AutoGenerateColumns="False" AutoGenerateHierarchy="True" 
    CellSpacing="0" EnableAriaSupport="True"
        GridLines="None" Skin="Metro" Width="900px"
        OnDetailTableDataBind="RadGridCustomers_DetailTableDataBind" 
    onneeddatasource="RadGridCustomers_NeedDataSource" 
    ondatabinding="RadGridCustomers_DataBinding" 
    ondatabound="RadGridCustomers_DataBound" 
    onitemcommand="RadGridCustomers_ItemCommand" 
    onpageindexchanged="RadGridCustomers_PageIndexChanged" 
    onpagesizechanged="RadGridCustomers_PageSizeChanged" 
    ViewStateMode="Enabled" onitemdatabound="RadGridCustomers_ItemDataBound" 
    onsortcommand="RadGridCustomers_SortCommand" 
    ondeletecommand="RadGridCustomers_DeleteCommand">
    <GroupingSettings CaseSensitive="false" /> 
        <ClientSettings EnableRowHoverStyle="true">
            <Selecting AllowRowSelect="True" />
            <ClientEvents OnRowSelected="RowSelected" />
        </ClientSettings>
        <MasterTableView AllowPaging="True" CommandItemDisplay="Top" CommandItemSettings-ShowAddNewRecordButton="False"
            AllowFilteringByColumn="True" DataKeyNames="Id" PageSize="20">
            <DetailTables>
                <telerik:GridTableView runat="server" Name="TerminalsTableView" AllowCustomPaging="false" 
                            AllowSorting="true" AllowPaging="true">
                    <CommandItemSettings ExportToPdfText="Export to PDF" />
                    <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
                    </RowIndicatorColumn>
                    <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
                    </ExpandCollapseColumn>
                    <Columns>
                        <telerik:GridBoundColumn DataField="FullName" FilterControlAltText="Filter FullNameColumn column"
                            HeaderText="Full Name" UniqueName="FullNameColumn" ShowFilterIcon="False">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="MacAddress" FilterControlAltText="Filter MacAddressColumn column"
                            HeaderText="MAC Address" MaxLength="12" ReadOnly="True" Resizable="False" UniqueName="MacAddressColumn"
                            CurrentFilterFunction="Contains">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="SitId" FilterControlAltText="Filter SitIdColumn column"
                            HeaderText="Site ID" UniqueName="SitIdColumn">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="IpAddress" FilterControlAltText="Filter IpAddressColumn column"
                            HeaderText="IP Address" ReadOnly="True" UniqueName="IpAddressColumn">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn AllowSorting="True" DataField="AdmStatus" FilterControlAltText="Filter AdmStatusColumn column"
                            HeaderText="Status" ReadOnly="True" UniqueName="AdmStatusColumn">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="StartDate" FilterControlAltText="Filter StartDateColumn column"
                            HeaderText="Start Date" ReadOnly="True" UniqueName="StartDateColumn" DataFormatString="{0:dd/MM/yyyy}">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn AllowSorting="False" DataField="ExpiryDate" DataType="System.DateTime"
                            FilterControlAltText="Filter ExpiryDateColumn column" HeaderText="Expiry Date"
                            ReadOnly="True" UniqueName="ExpiryDateColumn" DataFormatString="{0:dd/MM/yyyy}">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="CNo" 
                            FilterControlAltText="Filter CNo column" HeaderText="C\No" UniqueName="CNo" 
                            Visible="False">
                        </telerik:GridBoundColumn>
                    </Columns>
                    <EditFormSettings>
                        <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                        </EditColumn>
                    </EditFormSettings>

<PagerStyle AlwaysVisible="True"></PagerStyle>
                </telerik:GridTableView>
            </DetailTables>
            <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
            <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
                <HeaderStyle Width="20px"></HeaderStyle>
            </RowIndicatorColumn>
            <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" Visible="True">
                <HeaderStyle Width="20px"></HeaderStyle>
            </ExpandCollapseColumn>
            <Columns>
                <telerik:GridBoundColumn DataField="Id" DataType="System.Int32" FilterControlAltText="Filter IdColumn column"
                    HeaderText="Id" UniqueName="IdColumn" AllowFiltering="False">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="FullName" FilterControlAltText="Filter FullNameColumn column"
                    HeaderText="Customer organisation" UniqueName="FullNameColumn" ReadOnly="True" ShowFilterIcon="False" AutoPostBackOnFilter="True">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Phone" FilterControlAltText="Filter PhoneColumn column"
                    HeaderText="Phone" UniqueName="PhoneColumn" ReadOnly="True" ShowFilterIcon="False" AutoPostBackOnFilter="True">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Fax" FilterControlAltText="Filter FaxColumn column"
                    HeaderText="Fax" UniqueName="FaxColumn" ReadOnly="True" AutoPostBackOnFilter="True" ShowFilterIcon="False">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Email" FilterControlAltText="Filter EmailColumn column"
                    HeaderText="E-Mail" UniqueName="EmailColumn" ReadOnly="True" ShowFilterIcon="False" AutoPostBackOnFilter="True">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="WebSite" FilterControlAltText="Filter WebSiteColumn column"
                    HeaderText="WebSite" UniqueName="WebSiteColumn" ReadOnly="True" AutoPostBackOnFilter="True" ShowFilterIcon="False">
                </telerik:GridBoundColumn>
                <telerik:GridButtonColumn HeaderText="Delete" ImageUrl="~/Images/delete.png" ConfirmText="Are you sure you want to delete this customer organisation?"
                UniqueName="DeleteColumn" ButtonType="ImageButton" HeaderButtonType="PushButton"
                Text="Delete" CommandName="Delete">
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </telerik:GridButtonColumn>
            </Columns>
            <EditFormSettings>
                <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                </EditColumn>
            </EditFormSettings>
            <PagerStyle AlwaysVisible="True" />
        </MasterTableView>
        <PagerStyle AlwaysVisible="True" />
        <FilterMenu EnableImageSprites="False">
        </FilterMenu>
        <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default">
        </HeaderContextMenu>
    </telerik:RadGrid>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" Skin="Metro">
        <Windows>
            <telerik:RadWindow ID="RadWindowDetails" runat="server" Modal="False" NavigateUrl="TerminalDetailsForm.aspx"
                Animation="Fade" Opacity="100" Skin="Metro" Title="Terminal Details" AutoSize="False"
                Width="800px" Height= "600px" KeepInScreenBounds="True" EnableShadow="True" EnableAriaSupport="True"
                VisibleStatusbar="True">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>

