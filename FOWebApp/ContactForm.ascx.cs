﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Configuration;
using System.Drawing;
using System.Text;
using FOWebApp.App_Code;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;

namespace FOWebApp
{
    public partial class ContactForm : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ButtonSend_Click(object sender, EventArgs e)
        {
            BOLogControlWS boLogControlWS = new BOLogControlWS();
            StringBuilder message = new StringBuilder();
            
            if (HttpContext.Current.User.IsInRole("Distributor"))
            {
                //Get the logged in user and determine its role
                MembershipUser myObject = Membership.GetUser();
                string userId = myObject.ProviderUserKey.ToString();

                BOAccountingControlWS boAccountingControlWS = new BOAccountingControlWS();
                Distributor dist = boAccountingControlWS.GetDistributorForUser(userId);
                message.Append("<div style=\"font-weight: bold\">");
                message.Append("Distributor: ");
                message.Append(dist.FullName);
                message.Append("<br/>");
                message.Append("Distirbutor Id: ");
                message.Append(dist.Id);
                message.Append("</div><br/><br/>");
            }

            string[] mailAddresses = { ConfigurationManager.AppSettings["supportMail"] };

            message.Append("Sender E-Mail Address: ");
            message.Append(TextBoxEMail.Text);
            message.Append("<br/><br/><div style=\"font-style: italic\">");
            message.Append(RadTextBoxMessage.Text);
            message.Append("</div>");


            if (boLogControlWS.SendMail(message.ToString(), DropDownListSubject.SelectedValue, mailAddresses))
            {
                LabelResult.Visible = true;
                LabelResult.ForeColor = Color.DarkGreen;
                LabelResult.Text = "Mail sent successfully";

                //Send acknowledgement mail
                message.Clear();
                message.Append("Your mail was successfully sent to the CMT NOCSA!<br/><br/>");
                message.Append("Content:<br/><br/><div style=\"font-style: italic\">");
                message.Append(RadTextBoxMessage.Text);
                message.Append("</div>");
                message.Append("<br/><br/>");
                message.Append("Best regards,<br/><br/>");
                message.Append("Your SatADSL support team");
                string[] sourceEMailAddresses = TextBoxEMail.Text.Split(';');
                boLogControlWS.SendMail(message.ToString(), DropDownListSubject.SelectedValue, sourceEMailAddresses);
            }
            else
            {
                LabelResult.Visible = true;
                LabelResult.ForeColor = Color.Red;
                LabelResult.Text = "Sending mail failed!";
            }
        }
    }
}