﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CMTMessages.ascx.cs" Inherits="FOWebApp.CMTMessages" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script type="text/javascript">
    function RowSelected(sender, eventArgs) {
        var grid = sender;
        var masterDataView = grid.get_masterTableView();
        var rowNum = eventArgs.get_itemIndexHierarchical();
        var row = masterDataView.get_dataItems()[rowNum];
        var id = masterDataView.getCellByColumnUniqueName(row, "ID").innerHTML;

        //Initialize and show the issue details window
        var oWnd = radopen("CMTMessageDetailsPage.aspx?id=" + id, "RadWindowMessageDetails");
    }
</script>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="RadGridMessages">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGridMessages" />
                <telerik:AjaxUpdatedControl ControlID="RadWindowManager1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Metro"/>
<telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" Height="200px" 
    Width="467px" HorizontalAlign="NotSet" 
    LoadingPanelID="RadAjaxLoadingPanel1">
<telerik:RadGrid ID="RadGridMessages" runat="server" AllowSorting="True"
    AutoGenerateColumns="False" CellSpacing="0" GridLines="None" Skin="Metro"
    ShowStatusBar="True" Width="992px" 
        onitemdatabound="RadGridMessages_ItemDataBound" 
        onpagesizechanged="RadGridMessages_PageSizeChanged" 
        PageSize="50" onitemcommand="RadGridMessages_ItemCommand" EnableRowHoverStyle="true">
        <GroupingSettings CaseSensitive="false" /> 
    <ClientSettings>
        <Selecting AllowRowSelect="True" />
        <ClientEvents OnRowSelected="RowSelected" />
    </ClientSettings>
    <MasterTableView CommandItemDisplay="Top" CommandItemSettings-ShowExportToExcelButton="False" 
        CommandItemSettings-ShowExportToCsvButton="False" RowIndicatorColumn-Visible="True" RowIndicatorColumn-Resizable="True" RowIndicatorColumn-AndCurrentFilterFunction="Contains" CommandItemSettings-ShowAddNewRecordButton="False" ShowFooter="False">
        <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
        </RowIndicatorColumn>
        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
        </ExpandCollapseColumn>
        <SortExpressions>
            <telerik:GridSortExpression FieldName="EndTime" SortOrder="Ascending" />
        </SortExpressions>
        <Columns>
            <telerik:GridBoundColumn DataField="MessageId" 
                FilterControlAltText="Filter IDColumn column" HeaderText="Message ID" 
                UniqueName="ID" ShowFilterIcon="False" AutoPostBackOnFilter="True" Visible="true">
                <ItemStyle Wrap="True" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="MessageTitle" 
                FilterControlAltText="Filter TitleColumn column" HeaderText="Message Title" 
                UniqueName="MessageTitle" ShowFilterIcon="False" AutoPostBackOnFilter="True" >
                <ItemStyle Wrap="True" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="StartTime" FilterControlAltText="Filter MessageStartTimeColumn column"
                HeaderText="Visible from" ReadOnly="True" Resizable="False" 
                UniqueName="StartTime" CurrentFilterFunction="Contains" DataFormatString="{0:dd/MM/yyyy}" DataType="System.DateTime"
                ShowFilterIcon="False" AutoPostBackOnFilter="True">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="EndTime" FilterControlAltText="Filter MessageEndTimeColumn column"
                HeaderText="Visible until" ReadOnly="True" Resizable="False" 
                UniqueName="EndTime" CurrentFilterFunction="Contains" DataFormatString="{0:dd/MM/yyyy}" DataType="System.DateTime"
                ShowFilterIcon="False" AutoPostBackOnFilter="True">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="RecipientFullName" FilterControlAltText="Filter RecipientFullNameColumn column"
                HeaderText="Recipient(s)" ReadOnly="True" UniqueName="RecipientFullName" 
                ShowFilterIcon="False" AutoPostBackOnFilter="True">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Release" FilterControlAltText="Filter Release column" HeaderText="Release" UniqueName="Release">
            </telerik:GridBoundColumn>
        </Columns>
        <EditFormSettings>
            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
            </EditColumn>
        </EditFormSettings>
    </MasterTableView>
    <FilterMenu EnableImageSprites="False">
    </FilterMenu>
    <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default">
    </HeaderContextMenu>
</telerik:RadGrid>
<telerik:RadWindowManager ID="RadWindowManager1" runat="server" Skin="Metro" 
        DestroyOnClose="True">
    <Windows>
        <telerik:RadWindow ID="RadWindowMessageDetails" runat="server" 
            NavigateUrl="CMTMessageDetailsPage.aspx" Animation="Fade" Opacity="100" Skin="Metro" 
            Title="Message Details" AutoSize="False" Width="1000px" Height="700px" KeepInScreenBounds="True" EnableShadow="True" EnableAriaSupport="True" VisibleStatusbar="True" Modal="False" DestroyOnClose="False">
        </telerik:RadWindow>
    </Windows>
</telerik:RadWindowManager>
</telerik:RadAjaxPanel>