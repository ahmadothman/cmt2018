﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Threading;
using System.Globalization;

namespace FOWebApp.Account
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //RegisterHyperLink.NavigateUrl = "Register.aspx?ReturnUrl=" + HttpUtility.UrlEncode(Request.QueryString["ReturnUrl"]);
            if (Request.Params["Branding"] != null)
            {
                if (Request.Params["Branding"].Equals("Wl"))
                {
                    ImageLogo.Visible = false;
                }
                else
                {
                    ImageLogo.Visible = true;
                }
            }
        }

        protected override void InitializeCulture()
        {
            if (Request.Form["ImageButtonEnglish"] != null)
            {
                UICulture = "en";
                Culture = "en";

                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("en");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("en");
            }

            if (Request.Form["ImageButtonFrench"] != null)
            {
                 UICulture = "fr";
                 Culture = "fr";

                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("fr");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("fr");
            }
            base.InitializeCulture();
        }

        protected void LoginUser_LoginError(object sender, EventArgs e)
        {
            MembershipUser userInfo = Membership.GetUser(LoginUser.UserName);

            if (userInfo == null) // Check if Username exists
            {
                LoginUser.FailureText = GetLocalResourceObject("FailureTextResourceUserNonExistent").ToString();
            }
            else if (!userInfo.IsApproved) // Check if account has not yet been approved
            {
                LoginUser.FailureText = GetLocalResourceObject("FailureTextResourceNotApproved").ToString();
            }
            else if (userInfo.IsLockedOut) // Check if account has been locked
            {
                LoginUser.FailureText = GetLocalResourceObject("FailureTextResourceUserLockedOut").ToString();
            }
            else // Otherwise the password is incorrect
            {
                LoginUser.FailureText = GetLocalResourceObject("FailureTextResourcePasswordInvalid").ToString();
            }
        }
    }
}
