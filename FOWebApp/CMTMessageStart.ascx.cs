﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Security;
using System.Drawing;
using Telerik.Web.UI;
using FOWebApp.net.nimera.cmt.BOMonitorControlWSRef;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;
using FOWebApp.net.nimera.cmt.BOCMTMessageControllerWSRef;


namespace FOWebApp
{
    public partial class CMTMessageStart : System.Web.UI.UserControl
    {
        Dictionary<int, string> sattellites = new Dictionary<int, string>()
                {
                    //{ 1, "" },
                    { 2, "ASTRA-2G" },
                    { 3, "ASTRA-3B" },
                    { 4, "ASTRA-4A" },
                    //{ 5, "" },
                    { 6, "EUTELSAT-E16A" },
                   /* { 7, "E70B" },*/ //
                    { 8, "NI-E21B" }
                };
        protected void Page_Load(object sender, EventArgs e)
        {
            BOCMTMessageControllerWS boMessageControl = new BOCMTMessageControllerWS();
            //CMTMessage message = boCMTMessageControl.GetCMTMessage("EFEA2DD8-6F50-4C76-BF9C-4476424E9D2B");
            BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();
            MembershipUser myObject = Membership.GetUser();
            string userId = myObject.ProviderUserKey.ToString();
            CMTMessage[] messages;

            if (Roles.IsUserInRole("VNO"))
            {
                int distributorId = Convert.ToInt32(boAccountingControl.GetDistributorForUser(userId).Id);
                messages = boMessageControl.GetCMTMessagesForUser(distributorId.ToString(), "VNOs");
            }
            else if (Roles.IsUserInRole("Distributor") || Roles.IsUserInRole("DistributorReadOnly"))
            {
                int distributorId = Convert.ToInt32(boAccountingControl.GetDistributorForUser(userId).Id);
                messages = boMessageControl.GetCMTMessagesForUser(distributorId.ToString(), "distributors");

                //var isps = boAccountingControl.GetISPsForDistributor(distributorId).Select(x => x.Satellite.ID);
                //var satellites = boAccountingControl.GetSatellites().Where(x => isps.Contains(x.ID)).Select(x => x.SatelliteName);
                //foreach (var satellite in satellites)
                //{
                //    graphsHtml += "<iframe src=\"http://nms.satadsl.net:83/graph/satellitePie.php?satellite=" + satellite + "&period=from=-30d\" frameborder=\"0\" scrolling=\"no\" height=\"400\" width=\"400\"></iframe>";
                //}

                var ispIds = boAccountingControl.GetISPsForDistributor(distributorId).Select(x => x.Id / 100).Distinct();
                string graphsHtml = "";
                foreach (var id in ispIds)
                {
                    string satellite;
                    if (sattellites.TryGetValue(Convert.ToInt32(id), out satellite))
                    {
                        graphsHtml += "<iframe src=\"http://nms.satadsl.net:83/graph/satelliteOnlinePie.php?satellite=" + satellite + "&period=from=-30d\" frameborder=\"0\" scrolling=\"no\" height=\"400\" width=\"400\"></iframe>";
                    }
                }
                divGraph.InnerHtml = graphsHtml;
            }
            else if (Roles.IsUserInRole("Customer") || Roles.IsUserInRole("Organization"))
            {
                int customerId = Convert.ToInt32(boAccountingControl.GetOrganizationForUser(userId).Id);
                messages = boMessageControl.GetCMTMessagesForUser(customerId.ToString(), "customers");
            }
            else if (Roles.IsUserInRole("FinanceAdmin"))
            {
                //TO DO: retrieve messages for FinanceAdmin
                messages = null;
            }
            else if (Roles.IsUserInRole("Suspended Distributor"))
            {
                int distributorId = Convert.ToInt32(boAccountingControl.GetDistributorForUser(userId).Id);
                messages = boMessageControl.GetCMTMessagesForUser(distributorId.ToString(), "suspendedDistributors");
            }
            else
            {
                int endUserId = Convert.ToInt32(boAccountingControl.GetEndUser(userId).Id);
                messages = boMessageControl.GetCMTMessagesForUser(endUserId.ToString(), "endusers");
            }

            if (messages != null && messages.Length == 0)
            {
                divTable.InnerHtml = "";
            }
            else
            {
                bool hr = false;
                string tablestring = "<table cellpadding='10' cellspacing='5' width='60%'>";
                foreach (CMTMessage message in messages)
                {
                    if (hr)
                    {
                        tablestring = tablestring + "<tr><td colspan=\"3\"><hr></td></tr>";
                    }
                    tablestring = tablestring + "<tr><td><h4> Subject: " + message.MessageTitle + "</h4></td>";
                    tablestring = tablestring + "<td><b>From:</b> " + message.StartTime.ToShortDateString() + "</td>";
                    tablestring = tablestring + "<td><b>To:</b> " + message.EndTime.ToShortDateString() + "</td></tr>";
                    tablestring = tablestring + "<tr><td colspan=\"3\">" + message.MessageContent + "</td></tr>";
                    hr = true;
                }
                tablestring = tablestring + "</table>";

                divTable.InnerHtml = tablestring;
            }
        }
    }
}