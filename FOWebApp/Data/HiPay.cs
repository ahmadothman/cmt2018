﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HiPay
{
    public class Reason
    {
        public string code { get; set; }
        public string message { get; set; }
    }

    public class PaymentMethod
    {
        public string token { get; set; }
        public string brand { get; set; }
        public string pan { get; set; }
        public string cardHolder { get; set; }
        public string cardExpiryMonth { get; set; }
        public string cardExpiryYear { get; set; }
        public string issuer { get; set; }
        public string country { get; set; }
    }

    public class FraudScreening
    {
        public string scoring { get; set; }
        public string result { get; set; }
        public string review { get; set; }
    }

    public class Order
    {
        public string id { get; set; }
        public string dateCreated { get; set; }
        public string attempts { get; set; }
        public string amount { get; set; }
        public string shipping { get; set; }
        public string tax { get; set; }
        public string decimals { get; set; }
        public string currency { get; set; }
        public string customerId { get; set; }
        public string language { get; set; }
        public string email { get; set; }
    }

    public class DebitAgreement
    {
        public string id { get; set; }
        public string status { get; set; }
    }

    public class HiPayOrderRequestResponse
    {
        public string state { get; set; }
        public Reason reason { get; set; }
        public string forwardUrl { get; set; }
        public string test { get; set; }
        public string mid { get; set; }
        public string attemptId { get; set; }
        public string authorizationCode { get; set; }
        public string transactionReference { get; set; }
        public string dateCreated { get; set; }
        public string dateUpdated { get; set; }
        public string dateAuthorized { get; set; }
        public string status { get; set; }
        public string message { get; set; }
        public string authorizedAmount { get; set; }
        public string capturedAmount { get; set; }
        public string refundedAmount { get; set; }
        public string creditedAmount { get; set; }
        public string decimals { get; set; }
        public string currency { get; set; }
        public string ipAddress { get; set; }
        public string ipCountry { get; set; }
        public string deviceId { get; set; }
        public string cdata1 { get; set; }
        public string cdata2 { get; set; }
        public string cdata3 { get; set; }
        public string cdata4 { get; set; }
        public string cdata5 { get; set; }
        public string cdata6 { get; set; }
        public string cdata7 { get; set; }
        public string cdata8 { get; set; }
        public string cdata9 { get; set; }
        public string cdata10 { get; set; }
        public string avsResult { get; set; }
        public string cvcResult { get; set; }
        public string eci { get; set; }
        public string paymentProduct { get; set; }
        public PaymentMethod paymentMethod { get; set; }
        public string threeDSecure { get; set; }
        public FraudScreening fraudScreening { get; set; }
        public Order order { get; set; }
        public DebitAgreement debitAgreement { get; set; }
    }
}