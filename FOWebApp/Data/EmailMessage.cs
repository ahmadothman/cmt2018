﻿using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace FOWebApp.Data
{
    /// <summary>
    /// Email message with its recipients, subject and body according to message type
    /// </summary>
    public class EmailMessage
    {
        /// <summary>
        /// Message type
        /// </summary>
        public enum MessageType
        {
            ///<summary>Message for suspension of SOHO terminals</summary>
            SohoTerminalSuspension
        };
        /// <summary>
        /// Addresses to send the email to
        /// </summary>
        public List<string> recipients { get; set; }
        /// <summary>
        /// Email subject
        /// </summary>
        public string subject { get; }
        /// <summary>
        /// Email body
        /// </summary>
        public string body { get; }

        /// <summary>
        /// Constructor for different email messages according to type
        /// </summary>
        /// <param name="messageType">Type of message</param>
        /// <param name="terminals">Terminals for which action has been taken</param>
        public EmailMessage(MessageType messageType, IEnumerable<Terminal> terminals)
        {
            switch (messageType)
            {
                case MessageType.SohoTerminalSuspension:
                    {
                        this.recipients = new List<string>(ConfigurationManager.AppSettings["SohoTerminalSuspensionEmailRecipients"].Split(';'));
                        this.subject = "SatADSL terminal suspension";
                        this.body = "<p>Dear distributor,</br></p>" +
                      "<p>Please be advised that the following terminals have been suspended due to prolonged inactivity.</p>" +
                      "<p>In order to reactivate these terminals please contact our support team.</p>" +
                      "<p>--------</p>" +
                      "<p>Cher partenaire,</br></p>" +
                      "<p>Veuillez noter que les terminaux suivants ont été suspendus en raison d'une inactivité prolongée.</p>" +
                      "<p>Si vous désirez réactiver ces terminaux, veuillez contacter notre équipe de support.</p>" +
                      "<p>--------</p>" +
                      "<p>Caro distribuidor,</br></p>" +
                      "<p>Por favor tome em consideração que os seguintes terminais foram suspensos devido a inactividade prolongada.</p>" +
                      "<p>De forma a reactivar os terminais por favor contacte a nossa equipa de suporte.</p>" +
                      "<p>--------</p>" +
                      "<p><b> " + String.Join("</br>", terminals.Select(x => x.MacAddress + " - " + x.ExtFullName)) +
                      "</b></br></p>" +
                      "<p>Best regards,</br></p>" +
                      "<p>The SatADSL team </br></p>";
                    }
                    break;
                default:
                    break;
            }
        }
    }
}