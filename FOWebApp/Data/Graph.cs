﻿using DotNet.Highcharts;
using DotNet.Highcharts.Enums;
using DotNet.Highcharts.Helpers;
using DotNet.Highcharts.Options;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;
using PaloAltoQueries;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web.Script.Serialization;
using System.Xml.Serialization;

namespace FOWebApp.Data
{
    public class Graph
    {
        public string target { get; set; }
        public List<List<object>> datapoints { get; set; }
    }

    public static class HighchartsGraph
    {
        /// <summary>
        /// Generates volume traffic classification Highcharts graph between a start and
        /// end date.
        /// </summary>
        /// <param name="macAddress">terminal's Mac address</param>
        /// <param name="startDate">Start date for the graph</param>
        /// <param name="EndDate">End Date for the graph</param>
        public static string BuildTrafficClassification(string macAddress, DateTime startDate, DateTime endDate)
        {
            try
            {
                var _boAccountingControl = new BOAccountingControlWS();
                Terminal terminal = _boAccountingControl.GetTerminalDetailsByMAC(macAddress);
                int wait = Convert.ToInt32(ConfigurationManager.AppSettings["PaloAltoSingleQueryWait"]) * 1000;
                string terminalIpAddress = terminal.IpAddress.Trim() + "/" + terminal.IPMask;
                if (terminal.IPMask < 32)
                {
                    wait = Convert.ToInt32(ConfigurationManager.AppSettings["PaloAltoMultipleQueryWait"]) * 1000;
                }
                string query = PaloAlto.GetTrafficBySource(terminalIpAddress, startDate, endDate);
                string queryResponse;
                string jobResponse;
                System.Net.ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(delegate { return true; });
                System.Net.ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;

                using (WebClient client = new WebClient())
                {
                    queryResponse = client.DownloadString(query);
                }
                var reportResponse = queryResponse.XmlDeserializeFromString<PaloAltoReportResponse.Response>();
                var jobResult = new PaloAltoReportResult.Response();
                if (reportResponse.Status == "success")
                {
                    string getResult = PaloAlto.GetJobResult(reportResponse.Result.Job);
                    for (int i = 0; i < 5; i++)
                    {
                        using (WebClient client = new WebClient())
                        {
                            jobResponse = client.DownloadString(getResult);
                        }
                        jobResult = jobResponse.XmlDeserializeFromString<PaloAltoReportResult.Response>();
                        if (jobResult.Status == "success" && jobResult.Result.Job.Status == "FIN")
                        {
                            break;
                        }
                        else if (i == 4)
                        {
                            throw new Exception("PaloAlto get job result timeout. Number of retries exceeded.");
                        }
                        Thread.Sleep(wait);
                    }
                }
                var apps = jobResult.Result.Report.Entry.Select(x => x.App).ToList();
                List<Tuple<string, string, string>> appJobIds = new List<Tuple<string, string, string>>();
                foreach (var app in apps)
                {
                    string reportQuery = PaloAlto.GetTrafficByAppInSource(terminalIpAddress, app, startDate, endDate);
                    string response;
                    using (WebClient client = new WebClient())
                    {
                        response = client.DownloadString(reportQuery);
                    }
                    var appResponse = response.XmlDeserializeFromString<PaloAltoReportResponse.Response>();
                    appJobIds.Add(new Tuple<string, string, string>(appResponse.Result.Job, appResponse.Status, app));
                }
                List<PaloAltoReportResult.Response> appJobResults = new List<PaloAltoReportResult.Response>();
                foreach (var job in appJobIds)
                {
                    if (job.Item2 == "success")
                    {
                        string getResult = PaloAlto.GetJobResult(job.Item1);
                        for (int i = 0; i < 5; i++)
                        {
                            using (WebClient client = new WebClient())
                            {
                                jobResponse = client.DownloadString(getResult);
                            }
                            var appJobResult = jobResponse.XmlDeserializeFromString<PaloAltoReportResult.Response>();
                            if (appJobResult.Status == "success" && appJobResult.Result.Job.Status == "FIN")
                            {
                                appJobResults.Add(appJobResult);
                                break;
                            }
                            //else if (i == 4)
                            //{
                            //throw new Exception("PaloAlto get job result timeout. Number of retries exceeded.");
                            //}
                            Thread.Sleep(wait);
                        }
                    }
                }

                List<DotNet.Highcharts.Options.Point> points = new List<DotNet.Highcharts.Options.Point>();
                foreach (var appJobResult in appJobResults)
                {
                    var orderedAppJobResult = appJobResult.Result.Report.Entry.OrderBy(x => Convert.ToDateTime(x.Slabbedformattedreceive_time));

                    var point = new DotNet.Highcharts.Options.Point();
                    point.Color = System.Drawing.Color.FromName("colors[0]");

                    string appName = appJobIds.FirstOrDefault(x => x.Item1 == appJobResult.Result.Job.Id).Item3;
                    point.Y = Convert.ToDouble(jobResult.Result.Report.Entry.FirstOrDefault(x => x.App == appName).Bytes);
                    var drilldown = new Drilldown();
                    drilldown.Color = System.Drawing.Color.FromName("colors[0]");
                    drilldown.Name = appName;


                    drilldown.Categories = orderedAppJobResult.Select(x => Convert.ToDateTime(x.Slabbedformattedreceive_time).ToString("dd/MM/yyyy HH:mm")).ToArray();

                    List<double> sum = orderedAppJobResult.Select(x => Convert.ToDouble(x.Bytes_sent) + Convert.ToDouble(x.Bytes_received)).ToList();
                    drilldown.Data = new DotNet.Highcharts.Helpers.Data(sum.Cast<object>().ToArray());
                    point.Drilldown = drilldown;
                    points.Add(point);

                }
                DotNet.Highcharts.Helpers.Data data = new DotNet.Highcharts.Helpers.Data(points.ToArray());



                var subtitleEndDate = endDate.Date > DateTime.Now.Date ? endDate.AddDays(-1) : endDate.Date;
                //string subtitle = "from " + startDate.ToString("dd/MM/yyyy") + " until " + subtitleEndDate.ToString("dd/MM/yyyy");
                Highcharts chart = new Highcharts("chart")
                .InitChart(new Chart { DefaultSeriesType = ChartTypes.Column, Margin = new[] { 50, 50, 100, 80 } })
                .SetTitle(new Title { Text = "Traffic Classification" })
                .SetSubtitle(new Subtitle { Text = startDate.ToString("dd/MM/yyyy") + " - " + subtitleEndDate.ToString("dd/MM/yyyy") + " Click column to view detailed info" })
                .SetYAxis(new YAxis
                {
                    Min = 0,
                    Title = new YAxisTitle { Text = "Bytes" }
                })
                .SetLegend(new DotNet.Highcharts.Options.Legend { Enabled = true, Align = HorizontalAligns.Right, VerticalAlign = VerticalAligns.Top, BorderWidth = 0, X = -30, Y = 25, Layout = Layouts.Vertical })
                .SetTooltip(new Tooltip { Formatter = "TooltipFormatter" })
                .SetPlotOptions(new PlotOptions
                {
                    Column = new PlotOptionsColumn
                    {
                        Point = new PlotOptionsColumnPoint { Events = new PlotOptionsColumnPointEvents { Click = "ColumnPointClick" } },
                        DataLabels = new PlotOptionsColumnDataLabels
                        {
                            Enabled = true,
                            Rotation = -90,
                            Color = ColorTranslator.FromHtml("#FFFFFF"),
                            Align = HorizontalAligns.Right,
                            Y = 10,
                            Formatter = "function() { return nFormatter(this.y); }",
                            Style = "fontSize: '10px',fontFamily: 'Verdana, sans-serif',textShadow: '0 0 5px black'"
                        }
                    },
                    Line = new PlotOptionsLine
                    {
                        Cursor = Cursors.Pointer,
                        Marker = new PlotOptionsLineMarker
                        {
                            Symbol = "diamond"
                        },
                        Point = new PlotOptionsLinePoint { Events = new PlotOptionsLinePointEvents { Click = "ColumnPointClick" } },
                        DataLabels = new PlotOptionsLineDataLabels
                        {
                            Enabled = true,
                            Color = System.Drawing.ColorTranslator.FromHtml("#808080"),
                            Formatter = "function() { return nFormatter(this.y); }",
                            Style = "fontWeight: 'bold'"
                        }
                    }
                });

                XAxis xAxis = new XAxis();
                xAxis.Categories = apps.ToArray();
                xAxis.Labels = new XAxisLabels()
                {
                    Rotation = -45,
                    Align = HorizontalAligns.Right,
                    Style = "fontSize: '13px',fontFamily: 'Verdana, sans-serif'"
                };

                Series series = new Series();
                series.Name = "Volume";
                series.Data = data;
                //series.Color = System.Drawing.Color.White;
                chart.SetXAxis(xAxis);
                chart.SetSeries(series);


                chart.SetExporting(new Exporting { Enabled = true });
                chart.AddJavascripFunction(
                        "TooltipFormatter",
                        @"var point = this.point, s = this.x +':<b>'+ nFormatter(this.y);
                      if (point.drilldown) {
                        s += ' bytes</b><br/>Click to view '+ point.category +' usage';
                      } else {
                        s += ' bytes</b><br/>Click to return to overview';
                      }
                      return s;"
                    );
                chart.AddJavascripFunction(
                        "ColumnPointClick",
                        @"var drilldown = this.drilldown;
                      if (drilldown) { // drill down
                        //chart.yAxis[0].setExtremes(0,parseInt(drilldownMax));
                        chart.yAxis[0].axisTitle.attr({
                            text: 'Bytes'
                        });
                        setChart(drilldown.name, drilldown.categories, drilldown.data.data, drilldown.color, 'line');
                      } else { // restore
                        chart.yAxis[0].axisTitle.attr({
                            text: 'Bytes'
                        });
                        //chart.yAxis[0].setExtremes(0,100);
                        setChart(name, categories, data.data, 'white', 'column');
                      }"
                    );
                chart.AddJavascripFunction(
                        "setChart",
                        @"chart.xAxis[0].setCategories(categories);
                      chart.series[0].remove();
                      chart.addSeries({
                         type: type,
                         name: name,
                         data: data,
                         color: color || 'white'
                      });",
                        "name", "categories", "data", "color", "type"
                    );
                chart.AddJavascripFunction(
                        "nFormatter",
                        @"if (num >= 1000000000) {
                            return (num / 1000000000).toFixed(1).replace(/\.0$/, '') + 'G';
                         }
                         if (num >= 1000000) {
                            return (num / 1000000).toFixed(1).replace(/\.0$/, '') + 'M';
                         }
                         if (num >= 1000) {
                            return (num / 1000).toFixed(1).replace(/\.0$/, '') + 'K';
                         }
                         return num;",
                        "num"
                    );
                const string NAME = "Volume";
                chart.AddJavascripVariable("colors", "Highcharts.getOptions().colors");
                chart.AddJavascripVariable("name", "'{0}'".FormatWith(NAME));
                //chart.AddJavascripVariable("drilldownMax", "'{0}'".FormatWith(highestPing.Max().ToString()));
                chart.AddJavascripVariable("categories", JsonSerializer.Serialize(apps.ToArray()));
                chart.AddJavascripVariable("data", JsonSerializer.Serialize(data));
                return chart.ToJavaScriptString();
            }
            catch (Exception ex)
            {
                BOLogControlWS boLogControlWS = new BOLogControlWS();
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDateTime = DateTime.Now;
                cmtEx.ExceptionDesc = ex.Message;
                cmtEx.UserDescription = "GenerateTrafficClassificationChart";
                cmtEx.StateInformation = "";
                cmtEx.ExceptionLevel = 3;
                boLogControlWS.LogApplicationException(cmtEx);
                throw new Exception("The traffic classification system is currently taking too long to respond.");
            }
        }

        /// <summary>
        /// Generates volume traffic classification Highcharts graph between a start and
        /// end date.
        /// </summary>
        /// <param name="macAddresses">terminals</param>
        /// <param name="startDate">Start date for the graph</param>
        /// <param name="EndDate">End Date for the graph</param>
        public static string BuildTrafficClassification(IEnumerable<Terminal> terminals, DateTime startDate, DateTime endDate)
        {
            try
            {
                var _boAccountingControl = new BOAccountingControlWS();
                int wait = Convert.ToInt32(ConfigurationManager.AppSettings["PaloAltoMultipleQueryWait"]) * 1000;
                List<string> terminalIpAddresses = terminals.Select(x => x.IpAddress.Trim() + "/" + x.IPMask).ToList();
                string query = PaloAlto.GetTrafficBySource(terminalIpAddresses, startDate, endDate);
                string queryResponse;
                string jobResponse;
                System.Net.ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(delegate { return true; });
                System.Net.ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;

                using (WebClient client = new WebClient())
                {
                    queryResponse = client.DownloadString(query);
                }
                var reportResponse = queryResponse.XmlDeserializeFromString<PaloAltoReportResponse.Response>();
                var jobResult = new PaloAltoReportResult.Response();
                if (reportResponse.Status == "success")
                {
                    string getResult = PaloAlto.GetJobResult(reportResponse.Result.Job);
                    for (int i = 0; i < 5; i++)
                    {
                        using (WebClient client = new WebClient())
                        {
                            jobResponse = client.DownloadString(getResult);
                        }
                        jobResult = jobResponse.XmlDeserializeFromString<PaloAltoReportResult.Response>();
                        if (jobResult.Status == "success" && jobResult.Result.Job.Status == "FIN")
                        {
                            break;
                        }
                        else if (i == 4)
                        {
                            throw new Exception("PaloAlto get job result timeout. Number of retries exceeded.");
                        }
                        Thread.Sleep(wait);
                    }
                }
                var apps = jobResult.Result.Report.Entry.Select(x => x.App).ToList();
                List<Tuple<string, string, string>> appJobIds = new List<Tuple<string, string, string>>();
                foreach (var app in apps)
                {
                    string reportQuery = PaloAlto.GetTrafficByAppInSource(terminalIpAddresses, app, startDate, endDate);
                    string response;
                    using (WebClient client = new WebClient())
                    {
                        response = client.DownloadString(reportQuery);
                    }
                    var appResponse = response.XmlDeserializeFromString<PaloAltoReportResponse.Response>();
                    appJobIds.Add(new Tuple<string, string, string>(appResponse.Result.Job, appResponse.Status, app));
                }
                List<PaloAltoReportResult.Response> appJobResults = new List<PaloAltoReportResult.Response>();
                foreach (var job in appJobIds)
                {
                    if (job.Item2 == "success")
                    {
                        string getResult = PaloAlto.GetJobResult(job.Item1);
                        for (int i = 0; i < 5; i++)
                        {
                            using (WebClient client = new WebClient())
                            {
                                jobResponse = client.DownloadString(getResult);
                            }
                            var appJobResult = jobResponse.XmlDeserializeFromString<PaloAltoReportResult.Response>();
                            if (appJobResult.Status == "success" && appJobResult.Result.Job.Status == "FIN")
                            {
                                appJobResults.Add(appJobResult);
                                break;
                            }
                            //else if (i == 4)
                            //{
                            //throw new Exception("PaloAlto get job result timeout. Number of retries exceeded.");
                            //}
                            Thread.Sleep(wait);
                        }
                    }
                }

                List<DotNet.Highcharts.Options.Point> points = new List<DotNet.Highcharts.Options.Point>();
                foreach (var appJobResult in appJobResults)
                {
                    var orderedAppJobResult = appJobResult.Result.Report.Entry.OrderBy(x => Convert.ToDateTime(x.Slabbedformattedreceive_time));

                    var point = new DotNet.Highcharts.Options.Point();
                    point.Color = System.Drawing.Color.FromName("colors[0]");

                    string appName = appJobIds.FirstOrDefault(x => x.Item1 == appJobResult.Result.Job.Id).Item3;
                    point.Y = Convert.ToDouble(jobResult.Result.Report.Entry.FirstOrDefault(x => x.App == appName).Bytes);
                    var drilldown = new Drilldown();
                    drilldown.Color = System.Drawing.Color.FromName("colors[0]");
                    drilldown.Name = appName;

                    drilldown.Categories = orderedAppJobResult.Select(x => Convert.ToDateTime(x.Slabbedformattedreceive_time).ToString("dd/MM/yyyy HH:mm")).ToArray();

                    List<double> sum = orderedAppJobResult.Select(x => Convert.ToDouble(x.Bytes_sent) + Convert.ToDouble(x.Bytes_received)).ToList();
                    drilldown.Data = new DotNet.Highcharts.Helpers.Data(sum.Cast<object>().ToArray());
                    point.Drilldown = drilldown;
                    points.Add(point);

                }
                DotNet.Highcharts.Helpers.Data data = new DotNet.Highcharts.Helpers.Data(points.ToArray());
                var subtitleEndDate = endDate.Date > DateTime.Now.Date ? endDate.AddDays(-1) : endDate.Date;
                //string subtitle = "from " + startDate.ToString("dd/MM/yyyy") + " until " + subtitleEndDate.ToString("dd/MM/yyyy");
                Highcharts chart = new Highcharts("chart")
                .InitChart(new Chart { DefaultSeriesType = ChartTypes.Column, Margin = new[] { 50, 50, 100, 80 } })
                .SetTitle(new Title { Text = "Traffic Classification" })
                .SetSubtitle(new Subtitle { Text = startDate.ToString("dd/MM/yyyy") /*+ " - " + subtitleEndDate.ToString("dd/MM/yyyy")*/ + " Click column to view detailed info" })
                .SetYAxis(new YAxis
                {
                    Min = 0,
                    Title = new YAxisTitle { Text = "Bytes" }
                })
                .SetLegend(new DotNet.Highcharts.Options.Legend { Enabled = true, Align = HorizontalAligns.Right, VerticalAlign = VerticalAligns.Top, BorderWidth = 0, X = -30, Y = 25, Layout = Layouts.Vertical })
                .SetTooltip(new Tooltip { Formatter = "TooltipFormatter" })
                .SetPlotOptions(new PlotOptions
                {
                    Column = new PlotOptionsColumn
                    {
                        Point = new PlotOptionsColumnPoint { Events = new PlotOptionsColumnPointEvents { Click = "ColumnPointClick" } },
                        DataLabels = new PlotOptionsColumnDataLabels
                        {
                            Enabled = true,
                            Rotation = -90,
                            Color = ColorTranslator.FromHtml("#FFFFFF"),
                            Align = HorizontalAligns.Right,
                            Y = 10,
                            Formatter = "function() { return nFormatter(this.y); }",
                            Style = "fontSize: '10px',fontFamily: 'Verdana, sans-serif',textShadow: '0 0 5px black'"
                        }
                    },
                    Line = new PlotOptionsLine
                    {
                        Cursor = Cursors.Pointer,
                        Marker = new PlotOptionsLineMarker
                        {
                            Symbol = "diamond"
                        },
                        Point = new PlotOptionsLinePoint { Events = new PlotOptionsLinePointEvents { Click = "ColumnPointClick" } },
                        DataLabels = new PlotOptionsLineDataLabels
                        {
                            Enabled = true,
                            Color = System.Drawing.ColorTranslator.FromHtml("#808080"),
                            Formatter = "function() { return nFormatter(this.y); }",
                            Style = "fontWeight: 'bold'"
                        }
                    }
                });

                XAxis xAxis = new XAxis();
                xAxis.Categories = apps.ToArray();
                xAxis.Labels = new XAxisLabels()
                {
                    Rotation = -45,
                    Align = HorizontalAligns.Right,
                    Style = "fontSize: '13px',fontFamily: 'Verdana, sans-serif'"
                };

                Series series = new Series();
                series.Name = "Volume";
                series.Data = data;
                //series.Color = System.Drawing.Color.White;
                chart.SetXAxis(xAxis);
                chart.SetSeries(series);


                chart.SetExporting(new Exporting { Enabled = true });
                chart.AddJavascripFunction(
                        "TooltipFormatter",
                        @"var point = this.point, s = this.x +':<b>'+ this.y;
                      if (point.drilldown) {
                        s += ' bytes</b><br/>Click to view '+ point.category +' usage';
                      } else {
                        s += ' bytes</b><br/>Click to return to overview';
                      }
                      return s;"
                    );
                chart.AddJavascripFunction(
                        "ColumnPointClick",
                        @"var drilldown = this.drilldown;
                      if (drilldown) { // drill down
                        //chart.yAxis[0].setExtremes(0,parseInt(drilldownMax));
                        chart.yAxis[0].axisTitle.attr({
                            text: 'Bytes'
                        });
                        setChart(drilldown.name, drilldown.categories, drilldown.data.data, drilldown.color, 'line');
                      } else { // restore
                        chart.yAxis[0].axisTitle.attr({
                            text: 'Bytes'
                        });
                        //chart.yAxis[0].setExtremes(0,100);
                        setChart(name, categories, data.data, 'white', 'column');
                      }"
                    );
                chart.AddJavascripFunction(
                        "setChart",
                        @"chart.xAxis[0].setCategories(categories);
                      chart.series[0].remove();
                      chart.addSeries({
                         type: type,
                         name: name,
                         data: data,
                         color: color || 'white'
                      });",
                        "name", "categories", "data", "color", "type"
                    );
                chart.AddJavascripFunction(
                        "nFormatter",
                        @"if (num >= 1000000000) {
                            return (num / 1000000000).toFixed(1).replace(/\.0$/, '') + 'G';
                         }
                         if (num >= 1000000) {
                            return (num / 1000000).toFixed(1).replace(/\.0$/, '') + 'M';
                         }
                         if (num >= 1000) {
                            return (num / 1000).toFixed(1).replace(/\.0$/, '') + 'K';
                         }
                         return num;",
                        "num"
                    );
                const string NAME = "Volume";
                chart.AddJavascripVariable("colors", "Highcharts.getOptions().colors");
                chart.AddJavascripVariable("name", "'{0}'".FormatWith(NAME));
                chart.AddJavascripVariable("categories", JsonSerializer.Serialize(apps.ToArray()));
                chart.AddJavascripVariable("data", JsonSerializer.Serialize(data));
                return chart.ToJavaScriptString();
            }
            catch (Exception ex)
            {
                BOLogControlWS boLogControlWS = new BOLogControlWS();
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDateTime = DateTime.Now;
                cmtEx.ExceptionDesc = ex.Message;
                cmtEx.UserDescription = "GenerateTrafficClassificationChart";
                cmtEx.StateInformation = "";
                cmtEx.ExceptionLevel = 3;
                boLogControlWS.LogApplicationException(cmtEx);
                throw new Exception("The traffic classification system is currently taking too long to respond.");
            }
        }

        /// <summary>
        /// Generates pie chart traffic classication
        /// </summary>
        /// <param name="macAddress">terminal's mac address</param>
        /// <param name="startDate">Start date</param>
        /// <param name="endDate">End date</param>
        /// <returns></returns>
        public static string BuildTrafficClassificationPieChart(string macAddress, DateTime startDate, DateTime endDate)
        {
            try
            {
                var boAccountingControl = new BOAccountingControlWS();
                Terminal terminal = boAccountingControl.GetTerminalDetailsByMAC(macAddress);
                int wait = Convert.ToInt32(ConfigurationManager.AppSettings["PaloAltoSingleQueryWait"]) * 1000;
                string terminalIpAddress = terminal.IpAddress.Trim() + "/" + terminal.IPMask;
                if (terminal.IPMask < 32)
                {
                    wait = Convert.ToInt32(ConfigurationManager.AppSettings["PaloAltoMultipleQueryWait"]) * 1000;
                }
                string query = PaloAlto.GetTrafficBySource(terminalIpAddress, startDate, endDate);
                string queryResponse;
                System.Net.ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(delegate { return true; });
                System.Net.ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
                using (WebClient client = new WebClient())
                {
                    queryResponse = client.DownloadString(query);
                }
                var reportResponse = queryResponse.XmlDeserializeFromString<PaloAltoReportResponse.Response>();
                if (reportResponse.Status == "success")
                {
                    string getResult = PaloAlto.GetJobResult(reportResponse.Result.Job);
                    for (int i = 0; i < 5; i++)
                    {
                        string jobResponse;
                        using (WebClient client = new WebClient())
                        {
                            jobResponse = client.DownloadString(getResult);
                        }
                        var jobResult = jobResponse.XmlDeserializeFromString<PaloAltoReportResult.Response>();
                        if (jobResult.Status == "success" && jobResult.Result.Job.Status == "FIN")
                        {
                            Dictionary<string, double> namePercentagePairs = new Dictionary<string, double>();
                            double totalBytes = jobResult.Result.Report.Entry.Sum(x => Convert.ToDouble(x.Bytes));
                            foreach (var entry in jobResult.Result.Report.Entry)
                            {
                                namePercentagePairs.Add(entry.App, (Convert.ToDouble(entry.Bytes) * 100) / totalBytes);
                            }
                            return BuildPieChart(namePercentagePairs, startDate, endDate);
                        }
                        else if (i == 4)
                        {
                            throw new Exception("PaloAlto get job result timeout. Number of retries exceeded.");
                        }
                        Thread.Sleep(wait);
                    }
                }
                throw new Exception("Palo Alto not enqueing report jobs");
            }
            catch (Exception ex)
            {
                BOLogControlWS boLogControlWS = new BOLogControlWS();
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDateTime = DateTime.Now;
                cmtEx.ExceptionDesc = ex.Message;
                cmtEx.UserDescription = "GenerateTrafficClassificationPieChart";
                cmtEx.StateInformation = "";
                cmtEx.ExceptionLevel = 3;
                boLogControlWS.LogApplicationException(cmtEx);
                throw new Exception("The traffic classification system is currently taking too long to respond.");
            }
        }

        /// <summary>
        /// Generates pie chart traffic classication
        /// </summary>
        /// <param name="terminals">Terminals</param>
        /// <param name="startDate">Start date</param>
        /// <param name="endDate">End date</param>
        /// <returns></returns>
        public static string BuildTrafficClassificationPieChart(IEnumerable<Terminal> terminals, DateTime startDate, DateTime endDate)
        {
            try
            {
                int wait = Convert.ToInt32(ConfigurationManager.AppSettings["PaloAltoMultipleQueryWait"]) * 1000;
                List<string> terminalIpAddresses = terminals.Select(x => x.IpAddress.Trim() + "/" + x.IPMask).ToList();
                string query = PaloAlto.GetTrafficBySource(terminalIpAddresses, startDate, endDate);
                string queryResponse;
                System.Net.ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(delegate { return true; });
                System.Net.ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
                using (WebClient client = new WebClient())
                {
                    queryResponse = client.DownloadString(query);
                }
                var reportResponse = queryResponse.XmlDeserializeFromString<PaloAltoReportResponse.Response>();
                if (reportResponse.Status == "success")
                {
                    string getResult = PaloAlto.GetJobResult(reportResponse.Result.Job);
                    for (int i = 0; i < 5; i++)
                    {
                        string jobResponse;
                        using (WebClient client = new WebClient())
                        {
                            jobResponse = client.DownloadString(getResult);
                        }
                        var jobResult = jobResponse.XmlDeserializeFromString<PaloAltoReportResult.Response>();
                        if (jobResult.Status == "success" && jobResult.Result.Job.Status == "FIN")
                        {
                            Dictionary<string, double> namePercentagePairs = new Dictionary<string, double>();
                            double totalBytes = jobResult.Result.Report.Entry.Sum(x => Convert.ToDouble(x.Bytes));
                            foreach (var entry in jobResult.Result.Report.Entry)
                            {
                                namePercentagePairs.Add(entry.App, (Convert.ToDouble(entry.Bytes) * 100) / totalBytes);
                            }
                            return BuildPieChart(namePercentagePairs, startDate, endDate);
                        }
                        else if (i == 4)
                        {
                            throw new Exception("PaloAlto get job result timeout. Number of retries exceeded.");
                        }
                        Thread.Sleep(wait);
                    }
                }
                throw new Exception("Palo Alto not enqueing report jobs");
            }
            catch (ArgumentNullException)
            {
                throw new Exception("No data available for this time period");
            }
            catch (Exception ex)
            {
                BOLogControlWS boLogControlWS = new BOLogControlWS();
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDateTime = DateTime.Now;
                cmtEx.ExceptionDesc = ex.Message;
                cmtEx.UserDescription = "GenerateTrafficClassificationPieChart";
                cmtEx.StateInformation = "";
                cmtEx.ExceptionLevel = 3;
                boLogControlWS.LogApplicationException(cmtEx);
                throw new Exception("The traffic classification system is currently taking too long to respond");
            }
        }

        /// <summary>
        /// Generates an Highcharts pie chart with percentage values for each slice
        /// </summary>
        /// <param name="namePercentagePairs">Dictionary in which key is the legend and value its percentage</param>
        /// <param name="startDate">Start date</param>
        /// <param name="endDate">End date</param>
        /// <returns></returns>
        public static string BuildPieChart(Dictionary<string, double> namePercentagePairs, DateTime startDate, DateTime endDate)
        {
            if (namePercentagePairs.Count < 1)
            {
                throw new ArgumentNullException();
            }
            Highcharts chart = new Highcharts("chart")
                .InitChart(new Chart { PlotShadow = false })
                .SetTitle(new Title { Text = "Traffic classification" })
                .SetSubtitle(new Subtitle { Text = startDate.ToString("dd/MM/yyyy") + " to " + endDate.ToString("dd/MM/yyyy") })
                .SetTooltip(new Tooltip { Formatter = "function() { return '<b>'+ this.point.name +'</b>: '+ this.percentage.toFixed(2) +' %'; }" })
                .SetPlotOptions(new PlotOptions
                {
                    Pie = new PlotOptionsPie
                    {
                        AllowPointSelect = true,
                        Cursor = Cursors.Pointer,
                        DataLabels = new PlotOptionsPieDataLabels
                        {
                            Color = ColorTranslator.FromHtml("#000000"),
                            ConnectorColor = ColorTranslator.FromHtml("#000000"),
                            Formatter = "function() { return '<b>'+ this.point.name +'</b>: '+ this.percentage.toFixed(2) +' %'; }"
                        }
                    }
                });
            List<object> pieData = new List<object>();
            foreach (var pair in namePercentagePairs)
            {
                if (pair.Value > 0.01)
                {
                    pieData.Add(new object[] { pair.Key, pair.Value });
                }
            }
            var series = new Series
            {
                Type = ChartTypes.Pie,
                Name = "Apps",
                Data = new DotNet.Highcharts.Helpers.Data(pieData.ToArray())
            };
            chart.SetSeries(series);
            return chart.ToJavaScriptString();
        }

        /// <summary>
        /// Generates an Highcharts graph with accumulated volume for forward and return, between a start and
        /// end date.
        /// </summary>
        /// <param name="terminalIpAddresses">terminals' IP addresses</param>
        /// <param name="startDate">Start date for the graph</param>
        /// <param name="EndDate">End Date for the graph</param>
        public static string BuildFwdRtnVolume(IEnumerable<Terminal> terminals, DateTime startDate, DateTime endDate)
        {
            try
            {
                List<DateTime> fupResets = new List<DateTime>();
                Highcharts chart = new Highcharts("chart").SetOptions(new GlobalOptions { Global = new DotNet.Highcharts.Options.Global { UseUTC = true } });
                chart.InitChart(new DotNet.Highcharts.Options.Chart { ZoomType = ZoomTypes.X, SpacingRight = 20 });
                string title = "Accumulated Volume";
                chart.SetTitle(new DotNet.Highcharts.Options.Title { Text = title });
                chart.SetSubtitle(new Subtitle { Text = "Click and drag in the plot area to zoom in" });
                var xAxis = new XAxis
                {
                    Type = AxisTypes.Datetime,
                    MinRange = 1800000,
                    Title = new XAxisTitle { Text = "" }
                };
                chart.SetXAxis(xAxis);
                chart.SetTooltip(new Tooltip { Shared = true });
                chart.SetLegend(new DotNet.Highcharts.Options.Legend { Enabled = true, Align = HorizontalAligns.Center, VerticalAlign = VerticalAligns.Bottom, BorderWidth = 0 });
                chart.SetPlotOptions(new PlotOptions
                {
                    Area = new PlotOptionsArea
                    {
                        FillColor = null,
                        FillOpacity = 0.0,
                        LineWidth = 1,
                        Marker = new PlotOptionsAreaMarker
                        {
                            Enabled = false,
                            States = new PlotOptionsAreaMarkerStates
                            {
                                Hover = new PlotOptionsAreaMarkerStatesHover
                                {
                                    Enabled = true,
                                    Radius = 5
                                }
                            }
                        },
                        Shadow = false,
                        States = new PlotOptionsAreaStates { Hover = new PlotOptionsAreaStatesHover { LineWidth = 1 } },
                        PointStart = new PointStart(startDate)
                    },
                    Series = new PlotOptionsSeries
                    {
                        ConnectNulls = true
                    }
                });

                var javaScriptSerializer = new JavaScriptSerializer();
                javaScriptSerializer.MaxJsonLength = Int32.MaxValue;
                decimal? volumeThreshold = 0;

                var fwdDatapoints = new List<List<object>>();
                var rtnDatapoints = new List<List<object>>();

                string fwdRtnGraphUrl = Graphite.GetFwdRtnVolumeQuery(terminals, startDate, endDate);
                string fwdRtnJsonGraphData;
                using (WebClient client = new WebClient())
                {
                    fwdRtnJsonGraphData = client.DownloadString(fwdRtnGraphUrl);
                }
                var graphdata = javaScriptSerializer.Deserialize<List<Graph>>(fwdRtnJsonGraphData);
                var FWDTargets = graphdata.FirstOrDefault(x => x.target.Contains("FWD"));
                var RTNTargets = graphdata.FirstOrDefault(x => x.target.Contains("RTN"));
                if (FWDTargets != null)
                {
                    fwdDatapoints.AddRange(FWDTargets.datapoints);
                }
                if (RTNTargets != null)
                {
                    rtnDatapoints.AddRange(RTNTargets.datapoints);
                }
                if (FWDTargets == null && RTNTargets == null)
                {
                    throw new ArgumentNullException();
                }
                chart.SetSeries(new[]
                   {
                                new DotNet.Highcharts.Options.Series
                                    {
                                        Type = ChartTypes.Area,
                                        Name = "Forward Volume",
                                        Data = new DotNet.Highcharts.Helpers.Data(fwdDatapoints.Select( y => y.Swap(0, 1).ToList()).ToList().To2DArray()),
                                    },
                                new DotNet.Highcharts.Options.Series
                                    {
                                        Type = ChartTypes.Area,
                                        Name = "Return Volume",
                                        Data = new DotNet.Highcharts.Helpers.Data(rtnDatapoints.Select( y => y.Swap(0, 1).ToList()).ToList().To2DArray())
                                    },
                            });
                var yAxis = new YAxis
                {
                    Title = new YAxisTitle { Text = "Bytes" },
                    Min = 0,
                    MinRange = Convert.ToDouble(volumeThreshold),
                    StartOnTick = false,
                    EndOnTick = false
                };
                chart.SetYAxis(yAxis);
                return chart.ToJavaScriptString();
            }
            catch (ArgumentNullException)
            {
                throw new Exception("No data available for this time period");
            }
            catch (Exception ex)
            {
                BOLogControlWS boLogControlWS = new BOLogControlWS();
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDateTime = DateTime.Now;
                cmtEx.ExceptionDesc = ex.Message;
                cmtEx.UserDescription = "BuildFwdRtnVolume";
                cmtEx.StateInformation = "";
                cmtEx.ExceptionLevel = 3;
                boLogControlWS.LogApplicationException(cmtEx);
                throw new Exception("The volume accounting system is currently taking too long to respond");
            }
        }

        /// <summary>
        /// Generates an Highcharts line graph with forward and return traffic, between a start and end date
        /// </summary>
        /// <param name="terminals"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public static string BuildFwdRtnTraffic(IEnumerable<Terminal> terminals, DateTime startDate, DateTime endDate)
        {
            try
            {
                string graphUrl = Graphite.GetFwdRtnTrafficQuery(terminals, startDate, endDate);
                var javaScriptSerializer = new JavaScriptSerializer();
                javaScriptSerializer.MaxJsonLength = Int32.MaxValue;
                string jsonGraphData;
                using (WebClient client = new WebClient())
                {
                    jsonGraphData = client.DownloadString(graphUrl);
                }
                var graphdata = javaScriptSerializer.Deserialize<List<Graph>>(jsonGraphData);
                Highcharts chart = new Highcharts("chart").SetOptions(new GlobalOptions { Global = new DotNet.Highcharts.Options.Global { UseUTC = true } });
                chart.InitChart(new Chart { ZoomType = ZoomTypes.X, SpacingRight = 20 });
                chart.SetTitle(new Title { Text = "Traffic" });
                chart.SetSubtitle(new Subtitle { Text = "Click and drag in the plot area to zoom in" });
                chart.SetXAxis(new XAxis
                {
                    Type = AxisTypes.Datetime,
                    MinRange = 1800000,
                    Title = new XAxisTitle { Text = "" }
                });
                chart.SetYAxis(new YAxis
                {
                    Title = new YAxisTitle { Text = "BW (bit/s)" },
                    Min = 0.0,
                    StartOnTick = false,
                    EndOnTick = false
                });
                chart.SetTooltip(new Tooltip { Shared = true });
                chart.SetLegend(new Legend { Enabled = true, Align = HorizontalAligns.Center, VerticalAlign = VerticalAligns.Bottom, BorderWidth = 0 });
                chart.SetPlotOptions(new PlotOptions
                {
                    Area = new PlotOptionsArea
                    {
                        FillColor = null,
                        FillOpacity = 0.0,
                        LineWidth = 1,
                        Marker = new PlotOptionsAreaMarker
                        {
                            Enabled = false,
                            States = new PlotOptionsAreaMarkerStates
                            {
                                Hover = new PlotOptionsAreaMarkerStatesHover
                                {
                                    Enabled = true,
                                    Radius = 5
                                }
                            }

                        },
                        Shadow = false,
                        States = new PlotOptionsAreaStates { Hover = new PlotOptionsAreaStatesHover { LineWidth = 1 } },
                        PointStart = new PointStart(startDate)
                    }
                });
                var fwdDatapoints = new List<List<object>>();
                var rtnDatapoints = new List<List<object>>();
                var FWDTargets = graphdata.FirstOrDefault(x => x.target.Contains("FWD"));
                var RTNTargets = graphdata.FirstOrDefault(x => x.target.Contains("RTN"));
                if (FWDTargets != null)
                {
                    fwdDatapoints.AddRange(FWDTargets.datapoints);
                }
                if (RTNTargets != null)
                {
                    rtnDatapoints.AddRange(RTNTargets.datapoints);
                }
                if (FWDTargets == null && RTNTargets == null)
                {
                    throw new ArgumentNullException();
                }
                chart.SetSeries(new[]
                     {  new Series
                            {
                                Type = ChartTypes.Area,
                                Name = "Forward",
                                Data = new DotNet.Highcharts.Helpers.Data(fwdDatapoints.Select( y => y.Swap(0, 1).ToList()).ToList().To2DArray())
                            },
                            new Series
                            {
                                Type = ChartTypes.Area,
                                Name = "Return",
                                Data = new DotNet.Highcharts.Helpers.Data(rtnDatapoints.Select( y => y.Swap(0, 1).ToList()).ToList().To2DArray())
                            }
                         });
                return chart.ToJavaScriptString();

            }
            catch (ArgumentNullException)
            {
                throw new Exception("No data available for this time period");
            }
            catch (Exception ex)
            {
                BOLogControlWS boLogControlWS = new BOLogControlWS();
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDateTime = DateTime.Now;
                cmtEx.ExceptionDesc = ex.Message;
                cmtEx.UserDescription = "BuildFwdRtnTraffic";
                cmtEx.StateInformation = "";
                cmtEx.ExceptionLevel = 3;
                boLogControlWS.LogApplicationException(cmtEx);
                throw new Exception("The traffic accounting system is currently taking too long to respond");

            }
        }
    }

    public static class Graphite
    {
        public static string GetFwdRtnVolumeQuery(IEnumerable<Terminal> terminals, DateTime startDate, DateTime endDate)
        {
            string graphite = ConfigurationManager.AppSettings["Graphite"];
            List<string> terminalIpAddresses = new List<string>();
            foreach (var terminal in terminals)
            {
                if (terminal.IPMask < 32)
                {
                    int exponent = 32 - terminal.IPMask;
                    int numberOfHosts = Convert.ToInt32(Math.Pow(2, exponent));
                    var splitOctets = terminal.IpAddress.Trim().Split('.').ToList();
                    int lastOctet = Convert.ToInt32(splitOctets.Last());
                    splitOctets.RemoveAt(splitOctets.Count - 1);
                    for (int i = 1; i < numberOfHosts - 1; i++)
                    {
                        string ipAddress = String.Join(".", splitOctets) + "." + (lastOctet + i);
                        terminalIpAddresses.Add(ipAddress);
                    }
                }
                else
                {
                    terminalIpAddresses.Add(terminal.IpAddress.Trim());
                }
            }
            var fwdIpAddresses = terminalIpAddresses.Select(x => "Term." + x + ".FWD");
            var rtnIpAddresses = terminalIpAddresses.Select(x => "Term." + x + ".RTN");
            string graphiteFwdIpAddresses = String.Join(",", fwdIpAddresses);
            string graphiteRtnIpAddresses = String.Join(",", rtnIpAddresses);
            string graphUrl = graphite + "/render?target=integral(sumSeries(";
            graphUrl += graphiteFwdIpAddresses + "))&target=integral(sumSeries(";
            graphUrl += graphiteRtnIpAddresses + "))";
            graphUrl += "&from=" + startDate.ToString("HH:mm_yyyyMMdd") +
                        "&until=" + endDate.ToString("HH:mm_yyyyMMdd") +
                        "&format=json";
            return graphUrl;
        }

        public static string GetFwdRtnTrafficQuery(IEnumerable<Terminal> terminals, DateTime startDate, DateTime endDate)
        {
            string graphite = ConfigurationManager.AppSettings["Graphite"];
            List<string> terminalIpAddresses = new List<string>();
            foreach (var terminal in terminals)
            {
                if (terminal.IPMask < 32)
                {
                    int exponent = 32 - terminal.IPMask;
                    int numberOfHosts = Convert.ToInt32(Math.Pow(2, exponent));
                    var splitOctets = terminal.IpAddress.Trim().Split('.').ToList();
                    int lastOctet = Convert.ToInt32(splitOctets.Last());
                    splitOctets.RemoveAt(splitOctets.Count - 1);
                    for (int i = 1; i < numberOfHosts - 1; i++)
                    {
                        string ipAddress = String.Join(".", splitOctets) + "." + (lastOctet + i);
                        terminalIpAddresses.Add(ipAddress);
                    }
                }
                else
                {
                    terminalIpAddresses.Add(terminal.IpAddress.Trim());
                }
            }
            var fwdIpAddresses = terminalIpAddresses.Select(x => "Term." + x + ".FWD");
            var rtnIpAddresses = terminalIpAddresses.Select(x => "Term." + x + ".RTN");
            string graphiteFwdIpAddresses = String.Join(",", fwdIpAddresses);
            string graphiteRtnIpAddresses = String.Join(",", rtnIpAddresses);
            string graphUrl = graphite + "/render?target=scale(sumSeries(";
            graphUrl += graphiteFwdIpAddresses + "),0.125)&target=scale(sumSeries(";
            graphUrl += graphiteRtnIpAddresses + "),0.125)";
            graphUrl += "&from=" + startDate.ToString("HH:mm_yyyyMMdd") +
                        "&until=" + endDate.ToString("HH:mm_yyyyMMdd") +
                        "&format=json";
            return graphUrl;
        }
    }

    public static class ExtensionMethods
    {
        public static string ToJavaScriptString(this Highcharts chart)
        {
            Match match = Regex.Match(chart.ToHtmlString(), @"<script type='text/javascript'>(.*)</script>", RegexOptions.Singleline);
            return match.Groups[1].Value;
        }

        public static IList<T> Swap<T>(this IList<T> list, int indexA, int indexB)
        {
            T tmp = list[indexA];
            list[indexA] = (T)Convert.ChangeType(Convert.ToInt64(list[indexB]) * 1000, typeof(T)); // highcharts uses milliseconds
            list[indexB] = tmp;
            return list;
        }

        public static T[,] To2DArray<T>(this List<List<T>> lst)
        {

            if ((lst == null) || (lst.Any(subList => subList.Any() == false)))
                throw new ArgumentException("Input list is not properly formatted with valid data");

            int index = 0;
            int subindex;

            return lst.Aggregate(new T[lst.Count(), lst.Max(sub => sub.Count())],
                             (array, subList) =>
                             {
                                 subindex = 0;
                                 subList.ForEach(itm => array[index, subindex++] = itm);
                                 ++index;
                                 return array;
                             });
        }

        public static double ToUnixTimestamp(this DateTime dateTime)
        {
            return (dateTime -
                   new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc)).TotalSeconds;
        }

        public static DateTime UnixTimeStampToDateTime(this double unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }

        public static IEnumerable<T> TakeLast<T>(this IEnumerable<T> source, int N)
        {
            return source.Skip(Math.Max(0, source.Count() - N));
        }

        /// <summary>
        /// Get string value after [first] a.
        /// </summary>
        public static string Before(this string value, string a)
        {
            int posA = value.IndexOf(a);
            if (posA == -1)
            {
                return "";
            }
            return value.Substring(0, posA);
        }

        /// <summary>
        /// Get string value after [last] a.
        /// </summary>
        public static string After(this string value, string a)
        {
            int posA = value.LastIndexOf(a);
            if (posA == -1)
            {
                return "";
            }
            int adjustedPosA = posA + a.Length;
            if (adjustedPosA >= value.Length)
            {
                return "";
            }
            return value.Substring(adjustedPosA);
        }

        public static string XmlSerializeToString(this object objectInstance)
        {
            var serializer = new XmlSerializer(objectInstance.GetType());
            var sb = new StringBuilder();

            using (TextWriter writer = new StringWriter(sb))
            {
                serializer.Serialize(writer, objectInstance);
            }

            return sb.ToString();
        }

        public static T XmlDeserializeFromString<T>(this string objectData)
        {
            return (T)XmlDeserializeFromString(objectData, typeof(T));
        }

        public static object XmlDeserializeFromString(this string objectData, Type type)
        {
            var serializer = new XmlSerializer(type);
            object result;

            using (TextReader reader = new StringReader(objectData))
            {
                result = serializer.Deserialize(reader);
            }

            return result;
        }
    }
}