﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Xml.Serialization;
using System.Linq;

namespace PaloAltoReportResponse
{
    [XmlRoot(ElementName = "msg")]
    public class Msg
    {
        [XmlElement(ElementName = "line")]
        public string Line { get; set; }
    }

    [XmlRoot(ElementName = "result")]
    public class Result
    {
        [XmlElement(ElementName = "msg")]
        public Msg Msg { get; set; }
        [XmlElement(ElementName = "job")]
        public string Job { get; set; }
    }

    [XmlRoot(ElementName = "response")]
    public class Response
    {
        [XmlElement(ElementName = "result")]
        public Result Result { get; set; }
        [XmlAttribute(AttributeName = "status")]
        public string Status { get; set; }
    }

    
}

namespace PaloAltoReportResult
{
    [XmlRoot(ElementName = "job")]
    public class Job
    {
        [XmlElement(ElementName = "tenq")]
        public string Tenq { get; set; }
        [XmlElement(ElementName = "tdeq")]
        public string Tdeq { get; set; }
        [XmlElement(ElementName = "tlast")]
        public string Tlast { get; set; }
        [XmlElement(ElementName = "status")]
        public string Status { get; set; }
        [XmlElement(ElementName = "percent")]
        public string Percent { get; set; }
        [XmlElement(ElementName = "ndev")]
        public string Ndev { get; set; }
        [XmlElement(ElementName = "nskip")]
        public string Nskip { get; set; }
        [XmlElement(ElementName = "nreq")]
        public string Nreq { get; set; }
        [XmlElement(ElementName = "nresp")]
        public string Nresp { get; set; }
        [XmlElement(ElementName = "nproc")]
        public string Nproc { get; set; }
        [XmlElement(ElementName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "entry")]
    public class Entry
    {
        [XmlElement(ElementName = "app")]
        public string App { get; set; }
        [XmlElement(ElementName = "bytes")]
        public string Bytes { get; set; }
        [XmlElement(ElementName = "hostname")]
        public string Hostname { get; set; }
        [XmlElement(ElementName = "vsys")]
        public Vsys Vsys { get; set; }
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }
        ////////////////////////////////////////////////////////////
        [XmlElement(ElementName = "slabbed-formatted-receive_time")]
        public string Slabbedformattedreceive_time { get; set; }
        [XmlElement(ElementName = "bytes_sent")]
        public string Bytes_sent { get; set; }
        [XmlElement(ElementName = "bytes_received")]
        public string Bytes_received { get; set; }
    }

    [XmlRoot(ElementName = "report")]
    public class Report
    {
        [XmlElement(ElementName = "entry")]
        public List<Entry> Entry { get; set; }
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }
        [XmlAttribute(AttributeName = "logtype")]
        public string Logtype { get; set; }
        [XmlAttribute(AttributeName = "start")]
        public string Start { get; set; }
        [XmlAttribute(AttributeName = "start-epoch")]
        public string Startepoch { get; set; }
        [XmlAttribute(AttributeName = "end")]
        public string End { get; set; }
        [XmlAttribute(AttributeName = "end-epoch")]
        public string Endepoch { get; set; }
        [XmlAttribute(AttributeName = "generated-at")]
        public string Generatedat { get; set; }
        [XmlAttribute(AttributeName = "generated-at-epoch")]
        public string Generatedatepoch { get; set; }
        [XmlAttribute(AttributeName = "range")]
        public string Range { get; set; }
    }

    [XmlRoot(ElementName = "vsys")]
    public class Vsys
    {
        [XmlElement(ElementName = "entry")]
        public Entry Entry { get; set; }
    }

    [XmlRoot(ElementName = "devices")]
    public class Devices
    {
        [XmlElement(ElementName = "entry")]
        public Entry Entry { get; set; }
    }

    [XmlRoot(ElementName = "meta")]
    public class Meta
    {
        [XmlElement(ElementName = "devices")]
        public Devices Devices { get; set; }
    }

    [XmlRoot(ElementName = "result")]
    public class Result
    {
        [XmlElement(ElementName = "job")]
        public Job Job { get; set; }
        [XmlElement(ElementName = "report")]
        public Report Report { get; set; }
        [XmlElement(ElementName = "meta")]
        public Meta Meta { get; set; }
    }

    [XmlRoot(ElementName = "response")]
    public class Response
    {
        [XmlElement(ElementName = "result")]
        public Result Result { get; set; }
        [XmlAttribute(AttributeName = "status")]
        public string Status { get; set; }
    }
}

namespace PaloAltoQueries
{
    public static class PaloAlto
    {
        public static string GetTrafficBySource(string ipAddress, DateTime startDate, DateTime endDate)
        {
            string target = ConfigurationManager.AppSettings["PaloAlto"];
            string key = ConfigurationManager.AppSettings["PaloAltoAPIKey"];
            string reportUrl = target + @"/api/?type=report&reporttype=dynamic&reportname=custom-dynamic-report&cmd=<type><trsum><sortby>bytes</sortby><aggregate-by><member>app</member></aggregate-by><values><member>bytes</member></values></trsum></type><start-time>" + startDate.ToString("yyyy/MM/dd HH:mm:ss") + "</start-time><end-time>" + endDate.ToString("yyyy/MM/dd HH:mm:ss") + "</end-time><topn>10</topn><topm>10</topm><query>(addr.src in " + ipAddress + ")</query>&key=" + key;
            return reportUrl;
        }

        public static string GetTrafficBySource(IEnumerable<string> ipAddresses, DateTime startDate, DateTime endDate)
        {
            string target = ConfigurationManager.AppSettings["PaloAlto"];
            string key = ConfigurationManager.AppSettings["PaloAltoAPIKey"];
            string reportUrl = target + @"/api/?type=report&reporttype=dynamic&reportname=custom-dynamic-report&cmd=<type><trsum><sortby>bytes</sortby><aggregate-by><member>app</member></aggregate-by><values><member>bytes</member></values></trsum></type><start-time>" + startDate.ToString("yyyy/MM/dd HH:mm:ss") + "</start-time><end-time>" + endDate.ToString("yyyy/MM/dd HH:mm:ss") + "</end-time><topn>10</topn><topm>10</topm><query>" + ipAddresses.JoinWithOr() + "</query>&key=" + key;
            return reportUrl;
        }

        public static string GetTrafficByAppInSource(string ipAddress, string app, DateTime startDate, DateTime endDate)
        {
            string target = ConfigurationManager.AppSettings["PaloAlto"];
            string key = ConfigurationManager.AppSettings["PaloAltoAPIKey"];
            string reportUrl = target + @"/api/?type=report&reporttype=dynamic&reportname=custom-dynamic-report &cmd=<type><trsum><aggregate-by><member>slabbed-formatted-receive_time</member></aggregate-by><values><member>bytes_sent</member><member>bytes_received</member></values><sortby>slabbed-formatted-receive_time</sortby></trsum></type><start-time>" + startDate.ToString("yyyy/MM/dd HH:mm:ss") + "</start-time><end-time>" + endDate.ToString("yyyy/MM/dd HH:mm:ss") + "</end-time><delta>86400</delta><no-resolve>yes</no-resolve><topn>30</topn><query>((app eq " + app + ")) AND((src in " + ipAddress.Trim() + "))</query>&key=" + key;
            return reportUrl;
        }

        public static string GetTrafficByAppInSource(IEnumerable<string> ipAddresses, string app, DateTime startDate, DateTime endDate)
        {
            string target = ConfigurationManager.AppSettings["PaloAlto"];
            string key = ConfigurationManager.AppSettings["PaloAltoAPIKey"];
            string reportUrl = target + @"/api/?type=report&reporttype=dynamic&reportname=custom-dynamic-report &cmd=<type><trsum><aggregate-by><member>slabbed-formatted-receive_time</member></aggregate-by><values><member>bytes_sent</member><member>bytes_received</member></values><sortby>slabbed-formatted-receive_time</sortby></trsum></type><start-time>" + startDate.ToString("yyyy/MM/dd HH:mm:ss") + "</start-time><end-time>" + endDate.ToString("yyyy/MM/dd HH:mm:ss") + "</end-time><delta>86400</delta><no-resolve>yes</no-resolve><topn>30</topn><query>((app eq " + app + "))AND(" + ipAddresses.JoinWithOr() + ")</query>&key=" + key;
            return reportUrl;
        }

        public static string GetJobResult(string job)
        {
            string target = ConfigurationManager.AppSettings["PaloAlto"];
            string key = ConfigurationManager.AppSettings["PaloAltoAPIKey"];
            string jobUrl = target + @"/api/?type=report&action=get&job-id=" + job + "&key=" + key;
            return jobUrl;
        }

        private static string JoinWithOr(this IEnumerable<string> ipAddresses)
        {
            var ips = ipAddresses.Select(x => "(addr.src in " + x + ")");
            return string.Join("or", ips);
        }
    }
}
