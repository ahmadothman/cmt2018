using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using FOWebApp.Data;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string page = Request.Params["pg"];
        if (page != null)
        {
            if (page.Equals("about"))
            {
                //Add the about control to the content pane
                Control aboutControl = Page.LoadControl("AboutControl.ascx");
                MainContentPlaceHolder.Controls.Clear();
                MainContentPlaceHolder.Controls.Add(aboutControl);
            }
        }

        if (!IsPostBack)
        {
            //Insert a message in the UserActivity log
            BOLogControlWS boLogControlWS = new BOLogControlWS();

            //Remove if ticket #38 is solved
            DataHelper dh = new DataHelper();

            //Get the authenticated user
            MembershipUser myObject = Membership.GetUser();
            if (myObject != null)
            {
                string userId = myObject.ProviderUserKey.ToString();
                UserActivity userActivity = new UserActivity();
                userActivity.UserId = new Guid(userId);
                userActivity.ActionDate = DateTime.Now;
                userActivity.Action = "1";

                //Remove if ticket #38 is solved and use the BOLogControlWS method
                dh.insertUserActivity(userActivity);
                
                //This method throws and acception
                //See ticket #38
                //boLogControlWS.LogUserActivity(userActivity);
            }
            else
            {
                //Close the session and present login screen
                Session.Abandon();
                FormsAuthentication.SignOut();
                Response.Redirect("Default.aspx");
            }
        }
        Control control = null;
        //Show the correct list depending on the role of the user
        if (HttpContext.Current.User.IsInRole("Distributor"))
        {
            control = Page.LoadControl("CMTMessageStart.ascx");
        }
        
        if (HttpContext.Current.User.IsInRole("DistributorReadOnly"))
        {
            control = Page.LoadControl("CMTMessageStart.ascx");
        }

        if (HttpContext.Current.User.IsInRole("Suspended Distributor"))
        {
            control = Page.LoadControl("CMTMessageStart.ascx");
        }

        if (HttpContext.Current.User.IsInRole("VNO"))
        {
            control = Page.LoadControl("CMTMessageStart.ascx");
        }

        if (HttpContext.Current.User.IsInRole("NOC Administrator"))
        {
            control = Page.LoadControl("~/Nocsa/TaskList.ascx");
        }

        if (HttpContext.Current.User.IsInRole("NOC Operator"))
        {
            control = Page.LoadControl("~/Nocsa/TaskList.ascx");
        }

        if (HttpContext.Current.User.IsInRole("EndUser"))
        {
            Response.Redirect("http://cmt.satadsl.net/FOWebEU");
        }
        
        if (HttpContext.Current.User.IsInRole("MulticastServiceOperator"))
        {
            //Gets the URL from Web.config <appSettings>
            string multiCastViewerUrl = System.Web.Configuration.WebConfigurationManager.AppSettings["MultiCastViewerUrl"];
            Response.Redirect(multiCastViewerUrl);
        }
         
        if (HttpContext.Current.User.IsInRole("Organization"))
        {
            control = Page.LoadControl("CMTMessageStart.ascx");
        }

        if (control != null)
            MainContentPlaceHolder.Controls.Add(control);
    }
    protected void ButtonLogout_Click(object sender, EventArgs e)
    {
        Session.Abandon();
        FormsAuthentication.SignOut();
        Response.Redirect("Default.aspx");
    }
}
