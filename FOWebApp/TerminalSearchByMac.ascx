﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TerminalSearchByMac.ascx.cs"
    Inherits="FOWebApp.TerminalSearchByMac" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<telerik:RadScriptBlock ID="RadScriptBlockSearchClick" runat="server">
    <script type="text/javascript">
        function SearchClicked(sender, eventArgs) {
            // jquery to get the content of the TextBox, filled in by the client
            var macAddress = $telerik.$('#<%= RadMaskedTextBoxMacAddress.ClientID %>').val();

            //Initialize and show the terminal details window
            var openTerminalDetails = radopen("TerminalDetailsForm.aspx?mac=" + macAddress, "RadWindowTerminalDetails");
        }
    </script>

</telerik:RadScriptBlock>

<div id="searchDiv">
    <table>
        <tr>
            <td>
                <asp:Label ID="LabelValue" runat="server" Text="Mac Address:"></asp:Label>
            </td>
            <td>
                <telerik:RadMaskedTextBox ID="RadMaskedTextBoxMacAddress" runat="server" Skin="Metro">
                </telerik:RadMaskedTextBox>
            </td>
            <td>
                <!--Return false in javascript prevents the default action, that the object executes. Default action after the function is going back to the server for a postback, 
                    this prevents the postback.-->
                <!--In fact, you are doing a POST to the server, and you GET something back, and that is what's displayed, so if the is not in the code behind (it only exists here in the 
                    front end, it's not given back or posted back, so it won't be visible.-->
                <asp:Button ID="ButtonSearch" runat="server" Text="Search" OnClientClick="SearchClicked(); return false;" />
            </td>
            <td>
                <!-- Validations -->
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="RadMaskedTextBoxMacAddress"
                    ErrorMessage="Please enter a valid MAC Address" Display="Dynamic">
                </asp:RequiredFieldValidator>
            </td>
        </tr>
    </table>
</div>
<telerik:RadWindowManager ID="RadWindowManager1" runat="server" Animation="None" AutoSize="False" Width="1000px" Height="700px"
    KeepInScreenBounds="True" EnableShadow="True" EnableAriaSupport="True" VisibleStatusbar="True" Modal="True" DestroyOnClose="True" ShowContentDuringLoad="false">
    <Windows>
        <telerik:RadWindow ID="RadWindowTerminalDetails" Title="Terminal Details" runat="server" NavigateUrl="TerminalDetailsForm.aspx">
        </telerik:RadWindow>
    </Windows>
</telerik:RadWindowManager>
