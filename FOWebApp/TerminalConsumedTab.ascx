﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TerminalConsumedTab.ascx.cs" Inherits="FOWebApp.TerminalConsumedTab" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<div style="padding-top: 5px; padding-bottom: 2px">
Select period: 
    <telerik:RadComboBox ID="RadComboBoxPeriodSelection" runat="server" 
    Skin="Metro" AutoPostBack="False">
    <Items>
        <telerik:RadComboBoxItem runat="server" Text="1 Week" Value="7" />
        <telerik:RadComboBoxItem runat="server" Text="2 Weeks" Value="14" />
        <telerik:RadComboBoxItem runat="server" Text="1 Month" Value="30" />
        <telerik:RadComboBoxItem runat="server" Text="2 Months" Value="60" />
    </Items>
</telerik:RadComboBox>
&nbsp&nbsp
    <telerik:RadButton ID="RadButtonRefresh" runat="server" Text="Show Chart" 
        onclick="RadButtonRefresh_Click">
    </telerik:RadButton>
&nbsp&nbsp&nbsp
    <telerik:RadDatePicker ID="RadDatePickerStartDate" runat="server">
    </telerik:RadDatePicker>
    <telerik:RadDatePicker ID="RadDatePickerEndDate" runat="server">
    </telerik:RadDatePicker>
&nbsp&nbsp
    <telerik:RadButton ID="RadButtonDateSelection" runat="server" Text="Show Chart" 
        onclick="RadButtonDateSelection_Click">
    </telerik:RadButton>
</div>
<div>
<asp:Chart ID="ChartConsumed" runat="server" Visible="False">
    <Series>
    </Series>
    <ChartAreas>
        <asp:ChartArea Name="ChartAreaConsumed">
        </asp:ChartArea>
    </ChartAreas>
</asp:Chart>
</div>
<div>
    Right click to E-Mail, save or print this graph!
</div>

