﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Text;
using System.Security.Cryptography;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOMonitorControlWSRef;

namespace FOWebApp.HelperMethods
{
    public static class ExtensionMethods
    {
        /// <summary>
        /// Extension Method to scramble stings to MD5Hash
        /// </summary>
        /// <remarks>
        /// Adding this keyword to the parameter, makes an Extension Method of the method.
        /// Scramble a MacAddress with a method: TerminalDetailsHelper.CalculateMD5Hash(MacAddress)
        /// Scramble a MacAddress with an Extension Method: MacAddress.CalculateMD5Hash()
        /// </remarks>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string CalculateMD5Hash(this string input)
        {
            // step 1, calculate MD5 hash from input
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }

    }
}