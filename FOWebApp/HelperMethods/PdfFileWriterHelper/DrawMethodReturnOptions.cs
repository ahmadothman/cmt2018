﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FOWebApp.HelperMethods.PdfFileWriterHelper
{
    /// <summary>
    /// The different return values a custom draw method for a PDF document can have
    /// </summary>
    public enum DrawMethodReturnOptions
    {
        Success = 0,
        NewPageRequired = 1
    }
}