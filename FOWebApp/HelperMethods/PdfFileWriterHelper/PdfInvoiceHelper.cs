﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using PdfFileWriter;
using FOWebApp.net.nimera.cmt.BOInvoicingControllerWSRef;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOConfigurationControlWSRef;
using System.Globalization;

namespace FOWebApp.HelperMethods.PdfFileWriterHelper
{
    public class PdfInvoiceHelper
    {
        #region Fields & Properties

        // A4 page size in point: 595x842
        private const double PageWidth = 595;
        private const double PageHeight = 842;
        private const double MarginX = 50;
        private const double MarginY = 70;

        private PdfFont _calibriNormal;
        private PdfFont _calibriBold;
        private PdfFont _calibriItalic;
        private PdfFont _calibriBoldItalic;

        private const double CalibriNormalFontSize = 10;
        private const double CalibriTableFontSize = 6;

        private Color BlueSatAdsl = Color.FromArgb(0, 133, 180);
        private Color RedMessage = Color.Red;
        private Color GrayBackGround = Color.FromArgb(204, 204, 204);

        private PdfDocument _document;
        private PdfPage _page;
        private PdfContents _contents;
        private bool newPage = false;

        // This field is not used at the moment
        private int _pageNumber = 1;

        private string _currencySymbol = "€";
        private decimal _subTotal = 0;
        private decimal _tempSubTotal = 0;
        private List<decimal> _subTotalList = new List<decimal>();
        private int _subTotalCounter = 1;

        private List<DateTime> monthNames;
        private int months = 1;
        private int nextMonth = 0;

        // Fields used for the table
        private const double TableLineWidth = 0.5;
        private const double TableCellMargin = 1;
        // Width of all the column values together should be PageWidth - MarginX * 2
        // 133 + 85 + 47 + 50 + 60 + 50 + 29 + 12 + 29 = 595 - 50 * 2 = 495
        // 133 + 85 + 47 + 50 + 60 + 50 + 70 = 595 - 50 * 2 = 495
        // When the '- tableLineWidth / 2' isn't used on the leading and trailing column,
        // the table will be wider than the width of the page by the amount of the tableLineWidth
        private double[] TableColWidths = new double[]
        {
            133 - TableLineWidth / 2,
            85,
            47,
            50,
            60,
            50,
            //29,
            //12,
            //29 - TableLineWidth / 2
            70 - TableLineWidth / 2
        };

        const int StartCustomColumnTitleIndex = 4;
        const int EndCustomColumnTitleIndex = 5;
        const int StartCustomColumnValueIndex = 6;
        const int EndCustomColumnValueIndex = 6;

        const int StartTotalsRectangleCustomIndex = 1;
        const int EndTotalsRectangleCustomIndex = 4;
        const int StartTotalsTitleCustomIndex = 1;
        const int EndTotalsTitleCustomIndex = 3;
        const int StartTotalsValueCustomIndex = 4;
        const int EndTotalsValueCustomIndex = 4;

        private BOAccountingControlWS _accountingController = new BOAccountingControlWS();
        private BOConfigurationControllerWS _configurationController = new BOConfigurationControllerWS();
        private BOInvoicingControllerWS _invoiceController = new BOInvoicingControllerWS();

        private HttpContext Context { get; set; }
        private InvoiceHeader Invoice { get; set; }
        List<InvoiceDetail> invoiceDetails = new List<InvoiceDetail>();
        InvoiceTransaction transaction;
        InvoiceHeader[] unpaidInvoices;
        decimal unpaidInvoicesTotal;

        #endregion

        #region Constructors

        public PdfInvoiceHelper()
        {
            Context = HttpContext.Current;
        }

        #endregion

        #region Methods

        #region Generate Method

        /// <summary>
        /// Generate the PDF document for a given invoice
        /// </summary>
        /// <param name="invoice">The invoice for which the document should be generated</param>
        public void Generate(InvoiceHeader invoice)
        {
            if (invoice == null)
            {
                throw new ArgumentNullException("invoice", "The invoice parameter can't be null.");
            }

            Invoice = invoice;

            int[] invoiceDetailIds = _invoiceController.GetInvoiceDetailIdsForInvoice(Invoice.Id);
            foreach (int id in invoiceDetailIds)
            {
                invoiceDetails.Add(_invoiceController.GetInvoiceDetailLine(id));
            }

            switch (Invoice.CurrencyCode)
            {
                case "EUR":
                    _currencySymbol = "€";
                    break;
                case "USD":
                    _currencySymbol = "$";
                    break;
            }

            transaction = _invoiceController.GetTransaction(Invoice.Id);
            unpaidInvoices = _invoiceController.GetUnpaidInvoicesForDistributor(Invoice.DistributorId);
            unpaidInvoicesTotal = (decimal)unpaidInvoices.Where(x => x.ToBePaidExchanged.HasValue).Select(x => x.ToBePaidExchanged).DefaultIfEmpty(0).Sum() + unpaidInvoices.Where(x => !x.ToBePaidExchanged.HasValue).Select( x => x.ToBePaid).DefaultIfEmpty(0).Sum();

            // Step 1: Create one document object PdfDocument.

            _document = new PdfDocument(PaperType.A4, false, UnitOfMeasure.Point);

            // Step 2: Create resource objects such as fonts or images (i.e. PdfFont or PdfImage).

            DefineFontResources();

            // Step 3: Create page object PdfPage.

            _page = new PdfPage(_document);

            // Step 4: Create contents object PdfContents.

            _contents = new PdfContents(_page);

            // Step 5: Add text and graphics to the contents object (using PdfContents methods).

            TableCustomColumnText.TableColWidths = TableColWidths;

            DrawFooterLineAndFooter();

            double posY = PageHeight - MarginY; // This variable is used to store the position on the Y-axis where needs to be started with in the next method

            DrawHeadWrapper(ref posY);
            DrawTotalsWrapper(ref posY);
            DrawFinalContentWrapper(ref posY);
            posY = DrawNewPage();
            DrawInvoiceDetailsPageTitle(ref posY);
            posY -= 15;
            DrawTableWrapper(ref posY);

            // Step 6: Create your PDF document file by calling CreateFile method of PdfDocument.
            string filename = Invoice.InvoiceId.Replace("/", "-");
            filename = Invoice.InvoiceId.Replace("&", "-");
            _document.CreateFile(string.Format("{0}\\{1}.pdf", Path.GetFullPath(Path.Combine(Context.Server.MapPath("~"), "../../CMTInvoices")), filename));
            //_document.CreateFile(string.Format("{0}\\{1}.pdf", "c:/temp",filename));
        }

        #endregion

        #region General Helper Methods

        /// <summary>
        /// Gets the name of the month as string, specified by the number of that month
        /// </summary>
        /// <param name="monthNumber">The number of the month to get the name for</param>
        /// <returns>The name of the month as string, or an empty string if the month could not be found</returns>
        private string GetMonthNameByNumber(int monthNumber)
        {
            switch (monthNumber)
            {
                case 1:
                    return "January";
                case 2:
                    return "February";
                case 3:
                    return "March";
                case 4:
                    return "April";
                case 5:
                    return "May";
                case 6:
                    return "June";
                case 7:
                    return "July";
                case 8:
                    return "August";
                case 9:
                    return "September";
                case 10:
                    return "October";
                case 11:
                    return "November";
                case 12:
                    return "December";
                default:
                    return string.Empty;
            }
        }

        #endregion

        #region PDF Initializer Methods

        /// <summary>
        /// Initializes the different variations of the fonts
        /// </summary>
        private void DefineFontResources()
        {
            string fontNameCalibri = "Calibri";

            _calibriNormal = new PdfFont(_document, fontNameCalibri, FontStyle.Regular, true);
            _calibriBold = new PdfFont(_document, fontNameCalibri, FontStyle.Bold, true);
            _calibriItalic = new PdfFont(_document, fontNameCalibri, FontStyle.Italic, true);
            _calibriBoldItalic = new PdfFont(_document, fontNameCalibri, FontStyle.Bold | FontStyle.Italic, true);

            // substitute one character for another
            // PdfFileWriter supports characters 32 to 126 and 160 to 255
            // if a font has a character outside these ranges that is required by the application,
            // you can replace an unused character with this character
            // Note: space (32) and non breaking space (160) cannot be replaced
            _calibriNormal.CharSubstitution(8364, 8364, 161); // euro
            _calibriBold.CharSubstitution(8364, 8364, 161); // euro
            _calibriItalic.CharSubstitution(8364, 8364, 161); // euro
            _calibriBoldItalic.CharSubstitution(8364, 8364, 161); // euro
        }

        #endregion

        #region PDF Draw Wrapper Methods

        /// <summary>
        /// Draws the logo, addresses, invoice number, invoice data and invoice number message, starting at the specified position
        /// </summary>
        /// <param name="posY">
        /// The top of the content which needs to be drawn
        /// At the end of the execution of this method,
        /// this parameter will be set to the bottom (with eventually some margin) of the content
        /// </param>
        private void DrawHeadWrapper(ref double posY)
        {
            if (DrawLogo(ref posY) == DrawMethodReturnOptions.NewPageRequired)
            {
                posY = DrawNewPage();
                DrawLogo(ref posY);

                // When DrawLogo returns the NewPageRequired value
                // A new page will be drawn and posY will be set back to its initial value
                // When DrawLogo returns the NewPageRequired value a second time
                // I.e. the logo is too large to fit on one page
                // The logo will not be drawn
                // This is analogous with the other draw methods
            }
            posY -= 20;

            if (DrawAddresses(ref posY) == DrawMethodReturnOptions.NewPageRequired)
            {
                posY = DrawNewPage();
                DrawAddresses(ref posY);
            }
            posY -= 20;

            // When the invoice number can't be drawn on the first page (which is normally never the case)
            // The invoice number won't be drawn (because it's in the header of every following page)
            DrawInvoiceNumberFirstPage(ref posY);
            posY -= 15;

            if (DrawInvoiceData(ref posY) == DrawMethodReturnOptions.NewPageRequired)
            {
                posY = DrawNewPage();
                DrawInvoiceData(ref posY);
            }
            posY -= 5;

            if (DrawInvoiceNumberMessage(ref posY) == DrawMethodReturnOptions.NewPageRequired)
            {
                posY = DrawNewPage();
                DrawInvoiceNumberMessage(ref posY);
            }
            posY -= 15;
        }

        /// <summary>
        /// Draws the whole invoice details table, starting at the specified position
        /// </summary>
        /// <param name="posY">
        /// The top of the content which needs to be drawn
        /// At the end of the execution of this method,
        /// this parameter will be set to the bottom (with eventually some margin) of the content
        /// </param>
        private void DrawTableWrapper(ref double posY)
        {
            if (DrawTableHeader(ref posY) == DrawMethodReturnOptions.NewPageRequired)
            {
                posY = DrawNewPage();
                DrawTableHeader(ref posY);
            }
            bool noEntries = true;
            DateTime monthlyFeesDate = new DateTime(Invoice.CreationDate.Year, Invoice.CreationDate.Month, 1);
            monthNames = new List<DateTime>();
            monthNames.Add(monthlyFeesDate);
            IEnumerable<InvoiceDetail> monthsInvoiceDetails = invoiceDetails;
                     
            var periodicity = _accountingController.GetDistributor(Invoice.DistributorId).InvoiceInterval;
            //if (periodicity > 1)
            //{
               // monthsInvoiceDetails = invoiceDetails.Where(x => x.ActivityDate.Value.Month >= monthlyFeesDate.Month);
            IEnumerable<InvoiceDetail> previousMonthsInvoiceDetails = null;
            if (monthlyFeesDate.Month == 1)
            {
                monthsInvoiceDetails = invoiceDetails.Where(x => x.ActivityDate.Value.Month >= 1 && x.ActivityDate.Value.Month < 1+periodicity);
                previousMonthsInvoiceDetails = invoiceDetails.Where(x => x.ActivityDate.Value.Month <= 12 && x.ActivityDate.Value.Month >= 12 - periodicity && x.ActivityDate.Value.Year <= monthlyFeesDate.Year);
            }
            else
            {
                previousMonthsInvoiceDetails = invoiceDetails.Where(x => ((x.ActivityDate.Value.Month < monthlyFeesDate.Month && x.ActivityDate.Value.Year <= monthlyFeesDate.Year)||(x.ActivityDate.Value.Month > monthlyFeesDate.Month && x.ActivityDate.Value.Year < monthlyFeesDate.Year)));
                monthsInvoiceDetails = invoiceDetails.Where(x => x.ActivityDate.Value.Month >= monthlyFeesDate.Month && x.ActivityDate.Value.Year >= monthlyFeesDate.Year);
            }

            foreach (var invoice in previousMonthsInvoiceDetails.OrderBy(d => d.ActivityDate).ThenBy(d => d.TerminalId))
                {
                    bool newPageRequired = false;
                    if (DrawInvoiceDetail(ref posY, invoice, newPageRequired) == DrawMethodReturnOptions.NewPageRequired)
                    {
                        posY = DrawNewPage();
                        newPageRequired = true;
                        DrawTableHeader(ref posY);
                        DrawInvoiceDetail(ref posY, invoice, newPageRequired);
                    }
                }
                DrawTableFooterWrapperPreviousMonths(ref posY); 
            //}
            
            foreach (InvoiceDetail detail in monthsInvoiceDetails.OrderBy(d => d.ActivityDate).ThenBy(d => d.TerminalId))
            {
                bool newPageRequired = false;
                detail.UnitPrice = Math.Round(detail.UnitPrice);
                if (detail.ActivityDate.HasValue)
                {
                    if (detail.ActivityDate.Value.Year >= monthlyFeesDate.Year && detail.ActivityDate.Value.Month > monthlyFeesDate.Month)
                    {
                        if (!noEntries)
                        {
                            DrawTableFooterWrapper(ref posY);
                        }
                        monthlyFeesDate = monthlyFeesDate.AddMonths(1);
                        monthNames.Add(monthlyFeesDate);
                        months++;
                    }
                }
                if (DrawInvoiceDetail(ref posY, detail, newPageRequired) == DrawMethodReturnOptions.NewPageRequired)
                {
                    posY = DrawNewPage();
                    newPageRequired = true;
                    DrawTableHeader(ref posY);
                    DrawInvoiceDetail(ref posY, detail, newPageRequired);
                }
                noEntries = false;
            }
            DrawTableFooterWrapper(ref posY);
        }

        /// <summary>
        /// Draws bottom content of the table, starting at the specified position
        /// </summary>
        /// <param name="posY">
        /// The top of the content which needs to be drawn
        /// At the end of the execution of this method,
        /// this parameter will be set to the bottom (with eventually some margin) of the content
        /// </param>
        private void DrawTableFooterWrapper(ref double posY)
        {
            if (DrawTableSubTotalRow(ref posY, newPage) == DrawMethodReturnOptions.NewPageRequired)
            {
                newPage = true;
                posY = DrawNewPage();
                DrawTableSubTotalRow(ref posY, newPage);
                newPage = false;
            }

            if (DrawTableActiveTerminalsRow(ref posY) == DrawMethodReturnOptions.NewPageRequired)
            {
                posY = DrawNewPage();
                DrawTableActiveTerminalsRow(ref posY);
            }
            posY -= TableLineWidth; // So the next content appears under the table bottom line
            posY -= 12;
        }

        /// <summary>
        /// Draws bottom content of the table for previous months adjustement, starting at the specified position
        /// </summary>
        /// <param name="posY">
        /// The top of the content which needs to be drawn
        /// At the end of the execution of this method,
        /// this parameter will be set to the bottom (with eventually some margin) of the content
        /// </param>
        private void DrawTableFooterWrapperPreviousMonths(ref double posY)
        {
            if (DrawTableSubTotalRowPreviousMonths(ref posY, newPage) == DrawMethodReturnOptions.NewPageRequired)
            {
                newPage = true;
                posY = DrawNewPage();
                DrawTableSubTotalRowPreviousMonths(ref posY, newPage);
                newPage = false;
            }

            if (DrawTableActiveTerminalsRow(ref posY) == DrawMethodReturnOptions.NewPageRequired)
            {
                posY = DrawNewPage();
                DrawTableActiveTerminalsRow(ref posY);
            }
            posY -= TableLineWidth; // So the next content appears under the table bottom line
            posY -= 12;
        }

        /// <summary>
        /// Draws the different totals, starting at the specified position
        /// </summary>
        /// <param name="posY">
        /// The top of the content which needs to be drawn
        /// At the end of the execution of this method,
        /// this parameter will be set to the bottom (with eventually some margin) of the content
        /// </param>
        private void DrawTotalsWrapper(ref double posY)
        {
            double marginBetweenTotals = 12;

            if (DrawTotalPrice(ref posY) == DrawMethodReturnOptions.NewPageRequired)
            {
                posY = DrawNewPage();
                DrawTotalPrice(ref posY);
            }
            posY -= TableLineWidth; // So the next content appears under the table bottom line
            posY -= marginBetweenTotals;

            if (DrawIncentive(ref posY) == DrawMethodReturnOptions.NewPageRequired)
            {
                posY = DrawNewPage();
                DrawIncentive(ref posY);
            }
            posY -= TableLineWidth; // So the next content appears under the table bottom line
            posY -= marginBetweenTotals;

            if (DrawFinancialInterest(ref posY) == DrawMethodReturnOptions.NewPageRequired)
            {
                posY = DrawNewPage();
                DrawFinancialInterest(ref posY);
            }
            posY -= TableLineWidth; // So the next content appears under the table bottom line
            posY -= marginBetweenTotals;

            if (DrawBankCharges(ref posY) == DrawMethodReturnOptions.NewPageRequired)
            {
                posY = DrawNewPage();
                DrawBankCharges(ref posY);
            }
            posY -= TableLineWidth; // So the next content appears under the table bottom line
            posY -= marginBetweenTotals;

            for (int i = 0; i < months && months != 1; i++)
            {
                if (DrawTotalForMonths(ref posY, i) == DrawMethodReturnOptions.NewPageRequired)
                {
                    posY = DrawNewPage();
                    nextMonth--;
                    DrawTotalForMonths(ref posY, i);
                }
            }
            //_subTotalCounter = 0;
            //_subTotalList = null;

            posY -= TableLineWidth; // So the next content appears under the table bottom line
            posY -= marginBetweenTotals;

            if (DrawTransactions(ref posY) == DrawMethodReturnOptions.NewPageRequired)
            {
                posY = DrawNewPage();
                DrawTransactions(ref posY);
            }
            posY -= TableLineWidth; // So the next content appears under the table bottom line
            posY -= marginBetweenTotals;

            if (DrawGrandTotal(ref posY) == DrawMethodReturnOptions.NewPageRequired)
            {
                posY = DrawNewPage();
                DrawGrandTotal(ref posY);
            }
            posY -= TableLineWidth; // So the next content appears under the table bottom line
            posY -= marginBetweenTotals;

            if (DrawVat(ref posY) == DrawMethodReturnOptions.NewPageRequired)
            {
                posY = DrawNewPage();
                DrawVat(ref posY);
            }
            posY -= TableLineWidth; // So the next content appears under the table bottom line
            posY -= marginBetweenTotals;

            if (DrawPaidInAdvance(ref posY) == DrawMethodReturnOptions.NewPageRequired)
            {
                posY = DrawNewPage();
                DrawPaidInAdvance(ref posY);
            }
            posY -= TableLineWidth; // So the next content appears under the table bottom line
            posY -= marginBetweenTotals;

            if (DrawToBePaid(ref posY) == DrawMethodReturnOptions.NewPageRequired)
            {
                posY = DrawNewPage();
                DrawToBePaid(ref posY);
            }
            posY -= TableLineWidth; // So the next content appears under the table bottom line
            posY -= marginBetweenTotals;

            if (DrawToBePaidExchanged(ref posY) == DrawMethodReturnOptions.NewPageRequired)
            {
                posY = DrawNewPage();
                DrawToBePaidExchanged(ref posY);
            }

            posY -= TableLineWidth; // So the next content appears under the table bottom line
            posY -= marginBetweenTotals;

            if (DrawUnpaidInvoicesToBePaid(ref posY) == DrawMethodReturnOptions.NewPageRequired)
            {
                posY = DrawNewPage();
                DrawUnpaidInvoicesToBePaid(ref posY);
            }

            posY -= TableLineWidth; // So the next content appears under the table bottom line
            posY -= 20;
        }

        /// <summary>
        /// Draws final content to be shown, starting at the specified position
        /// </summary>
        /// <param name="posY">
        /// The top of the content which needs to be drawn
        /// At the end of the execution of this method,
        /// this parameter will be set to the bottom (with eventually some margin) of the content
        /// </param>
        private void DrawFinalContentWrapper(ref double posY)
        {
            double marginBetweenFinalContent = 10;

            if (DrawPaymentDetailsMessage(ref posY) == DrawMethodReturnOptions.NewPageRequired)
            {
                posY = DrawNewPage();
                DrawPaymentDetailsMessage(ref posY);
            }
            posY -= marginBetweenFinalContent;

            //if (DrawInvoiceNumberFooterMessage(ref posY) == DrawMethodReturnOptions.NewPageRequired)
            //{
            //    posY = DrawNewPage();
            //    DrawInvoiceNumberFooterMessage(ref posY);
            //}
            //posY -= marginBetweenFinalContent;

            if (DrawBankDetailsMessage(ref posY) == DrawMethodReturnOptions.NewPageRequired)
            {
                posY = DrawNewPage();
                DrawBankDetailsMessage(ref posY);
            }
            posY -= marginBetweenFinalContent;

            if (DrawTermsAndConditions(ref posY) == DrawMethodReturnOptions.NewPageRequired)
            {
                posY = DrawNewPage();
                DrawTermsAndConditions(ref posY);
            }
            posY -= marginBetweenFinalContent;

            if (DrawThankYouMessage(ref posY) == DrawMethodReturnOptions.NewPageRequired)
            {
                posY = DrawNewPage();
                DrawThankYouMessage(ref posY);
            }

        }

        #endregion

        #region PDF Draw Content Methods

        #region PDF Header & Footer Methods

        /// <summary>
        /// Draws the footer, with its footer line, on the page
        /// </summary>
        private void DrawFooterLineAndFooter()
        {
            _contents.SaveGraphicsState();

            const double marginFooterLine = 35;
            const double topFooterLine = MarginY - 10;
            _contents.DrawLine(marginFooterLine, topFooterLine, PageWidth - marginFooterLine, topFooterLine, .5);

            const double footerBoxWidth = PageWidth - MarginX * 2;
            TextBox footerBox = new TextBox(footerBoxWidth);

            //footerBox.AddText(_calibriBold, CalibriNormalFontSize, "Bank details ac. 1: ");
            footerBox.AddText(_calibriNormal, CalibriNormalFontSize, string.Format("{0} - IBAN: {1} - BIC: {2}\n",
                _configurationController.getParameter("BankAccount1Address").Trim(),
                _configurationController.getParameter("BankAccount1Iban").Trim(),
                _configurationController.getParameter("BankAccount1Bic").Trim()));

            //footerBox.AddText(_calibriBold, CalibriNormalFontSize, "Bank details ac. 2: ");
            footerBox.AddText(_calibriNormal, CalibriNormalFontSize, string.Format("{0} - IBAN: {1} - BIC: {2}",
                _configurationController.getParameter("BankAccount2Address").Trim(),
                _configurationController.getParameter("BankAccount2Iban").Trim(),
                _configurationController.getParameter("BankAccount2Bic").Trim()));

            footerBox.Terminate();

            double footerBoxHeight = footerBox.BoxHeight;

            _contents.Translate(MarginX, topFooterLine - footerBoxHeight - 2); // The '-2' is to make it appear under the line

            double posYtop = footerBoxHeight;
            _contents.DrawText(0, ref posYtop, 0, 0, footerBox);

            _contents.RestoreGraphicsState();
        }

        /// <summary>
        /// Draws the invoice number in the header of the page, by using DrawInvoiceNumberFirstPage()
        /// </summary>
        private void DrawInvoiceNumberHeader()
        {
            double posYtop = PageHeight - MarginY + 37;
            DrawInvoiceNumberFirstPage(ref posYtop);

            // When DrawInvoiceNumberFirstPage returns the value NewPageRequired (which normally shouldn't happen)
            // No header is drawn
        }

        #endregion

        /// <summary>
        /// Creates a new page with new contents.
        /// It also draws the header and the footer on that page and increase the local variable _pageNumber.
        /// Returns the new position on the Y axis for the new content.
        /// </summary>
        /// <returns>The new position on the Y axis for the new content.</returns>
        private double DrawNewPage()
        {
            _page = new PdfPage(_document);
            _contents = new PdfContents(_page);

            DrawInvoiceNumberHeader();
            DrawFooterLineAndFooter();

            _pageNumber++;

            return PageHeight - MarginY;
        }

        /// <summary>
        /// Draws the logo of SatADSL.
        /// This is done at the position of posYstart
        /// </summary>
        /// <param name="posYstart">
        /// The top of the content which needs to be drawn
        /// At the end of the execution of this method,
        /// this parameter will be set to the bottom (with eventually some margin) of the content
        /// </param>
        /// <returns>A value of the DrawMethodReturnOptions enum</returns>
        private DrawMethodReturnOptions DrawLogo(ref double posYstart)
        {
            _contents.SaveGraphicsState();

            PdfImage logo = new PdfImage(_document, Context.Server.MapPath("/Images/LogoSatADSL2.jpg"));
            SizeD logoSize = logo.ImageSize(130, 45);
            double posYbottom = posYstart - logoSize.Height;

            if (posYbottom < MarginY)
            {
                _contents.RestoreGraphicsState();

                return DrawMethodReturnOptions.NewPageRequired;
            }

            _contents.Translate(MarginX, posYbottom);

            _contents.DrawImage(logo, 0, 0, logoSize.Width, logoSize.Height);

            _contents.RestoreGraphicsState();

            posYstart = posYbottom;

            return DrawMethodReturnOptions.Success;
        }

        /// <summary>
        /// Draws the addresses of SatADSL and the distributor.
        /// This is done at the position of posYstart
        /// </summary>
        /// <param name="posYstart">
        /// The top of the content which needs to be drawn
        /// At the end of the execution of this method,
        /// this parameter will be set to the bottom (with eventually some margin) of the content
        /// </param>
        /// <returns>A value of the DrawMethodReturnOptions enum</returns>
        private DrawMethodReturnOptions DrawAddresses(ref double posYstart)
        {
            _contents.SaveGraphicsState();

            const double satAdslBoxWidth = (PageWidth - 2 * MarginX) / 2 - 10;
            TextBox satAdslBox = new TextBox(satAdslBoxWidth);

            satAdslBox.AddText(_calibriBold, CalibriNormalFontSize, BlueSatAdsl, string.Format("{0}\n", _configurationController.getParameter("CompanyName").Trim()));
            satAdslBox.AddText(_calibriNormal, CalibriNormalFontSize, string.Format("{0} {1}\n{2} {3}, {4}\nVAT: {5}",
                _configurationController.getParameter("AddressLine1").Trim(),
                _configurationController.getParameter("AddressLine2").Trim(),
                _configurationController.getParameter("PostCode").Trim(),
                _configurationController.getParameter("Location").Trim(),
                _configurationController.getParameter("Country").Trim(),
                _configurationController.getParameter("VAT").Trim()));

            satAdslBox.Terminate();

            double satAdslBoxHeight = satAdslBox.BoxHeight;

            const double distributorBoxWidth = (PageWidth - 2 * MarginX) / 2;
            TextBox distributorBox = new TextBox(distributorBoxWidth);

            Distributor distributor = _accountingController.GetDistributor(Invoice.DistributorId);

            if (distributor != null)
            {
                if (!string.IsNullOrEmpty(distributor.FullName))
                {
                    distributorBox.AddText(_calibriBold, CalibriNormalFontSize, distributor.FullName);
                }

                if (distributor.Address != null)
                {
                    if (!string.IsNullOrEmpty(distributor.Address.AddressLine1))
                    {
                        distributorBox.AddText(_calibriNormal, CalibriNormalFontSize, string.Format("\n{0}", distributor.Address.AddressLine1));
                    }

                    if (!string.IsNullOrEmpty(distributor.Address.AddressLine2))
                    {
                        distributorBox.AddText(_calibriNormal, CalibriNormalFontSize, string.Format("\n{0}", distributor.Address.AddressLine2));
                    }

                    if (!string.IsNullOrEmpty(distributor.Address.Location) && !string.IsNullOrEmpty(distributor.Address.Country))
                    {
                        distributorBox.AddText(_calibriNormal, CalibriNormalFontSize, string.Format("\n{0}, {1}", distributor.Address.Location, _accountingController.GetCountryNameByCountryCode(distributor.Address.Country)));
                    }
                    else if (!string.IsNullOrEmpty(distributor.Address.Location))
                    {
                        distributorBox.AddText(_calibriNormal, CalibriNormalFontSize, string.Format("\n{0}", distributor.Address.Location));
                    }
                    else if (!string.IsNullOrEmpty(distributor.Address.Country))
                    {
                        distributorBox.AddText(_calibriNormal, CalibriNormalFontSize, string.Format("\n{0}", _accountingController.GetCountryNameByCountryCode(distributor.Address.Country)));
                    }
                }

                if (distributor.Vat != null)
                {
                    distributorBox.AddText(_calibriNormal, CalibriNormalFontSize, string.Format("\nVAT: {0}", distributor.Vat));
                }
            }

            distributorBox.Terminate();

            double distributorBoxHeight = distributorBox.BoxHeight;

            double posYbottom = posYstart - Math.Max(satAdslBoxHeight, distributorBoxHeight);

            if (posYbottom < MarginY)
            {
                _contents.RestoreGraphicsState();

                return DrawMethodReturnOptions.NewPageRequired;
            }

            double posYtopSatAdsl = posYstart;
            _contents.DrawText(MarginX, ref posYtopSatAdsl, posYtopSatAdsl - satAdslBoxHeight, 0, satAdslBox);

            double posYtopDistributor = posYstart;
            _contents.DrawText(PageWidth - MarginX - distributorBox.BoxWidth, ref posYtopDistributor, posYtopDistributor - distributorBoxHeight, 0, distributorBox);

            _contents.RestoreGraphicsState();

            posYstart = posYbottom;

            return DrawMethodReturnOptions.Success;
        }

        /// <summary>
        /// Draws the invoice number with rectangle on the first page of the invoice
        /// (or actually also any other page, but on every other page, the invoice number is also drawn in the header).
        /// This is done at the position of posYstart.
        /// </summary>
        /// <param name="posYstart">
        /// The top of the content which needs to be drawn
        /// At the end of the execution of this method,
        /// this parameter will be set to the bottom (with eventually some margin) of the content
        /// </param>
        /// <returns>A value of the DrawMethodReturnOptions enum</returns>
        private DrawMethodReturnOptions DrawInvoiceNumberFirstPage(ref double posYstart)
        {
            _contents.SaveGraphicsState();

            // Something that is important to note when drawing a rectangle,
            // is that the height of that rectangle starts halfway the bottom line and ends halfway the top line.
            // Similarly, the width of the rectangle starts halfway the left line and ends halfway the right line.
            // Therefore, the origin of the rectangle is in the middle of the left bottom corner of the outer rectangle (with border)
            // and the left bottom corner of the inner rectangle (without border)
            // The same applies to all lines (draw origin is halfway the width of the line)

            const double rectangleMargin = 1;
            const double rectangleLineWidth = .5;
            // The width of the inner rectangle + half the width of the left and the right line together
            const double rectangleWidth = PageWidth - MarginX * 2 - rectangleLineWidth;

            string text = string.Format("INVOICE NUMBER: {0}", Invoice.InvoiceId);
            double boxWidth = Math.Min(_calibriBold.TextWidth(CalibriNormalFontSize, text), rectangleWidth - rectangleLineWidth - rectangleMargin * 2);
            TextBox box = new TextBox(boxWidth);

            box.AddText(_calibriBold, CalibriNormalFontSize, text);
            box.Terminate();

            double boxHeight = box.BoxHeight;

            // The height of the inner rectangle + half the width of the bottom and the top line together
            double rectangleHeight = boxHeight + rectangleMargin * 2 + rectangleLineWidth;

            double posYbottomBox = posYstart - rectangleLineWidth - rectangleMargin - boxHeight;
            // The bottom of the outer rectangle
            double posYbottomRectangle = posYstart - rectangleHeight - rectangleLineWidth;

            if (posYbottomRectangle < MarginY)
            {
                _contents.RestoreGraphicsState();

                return DrawMethodReturnOptions.NewPageRequired;
            }

            _contents.SetLineWidth(rectangleLineWidth);
            _contents.DrawRectangle(MarginX + rectangleLineWidth / 2, posYbottomRectangle + rectangleLineWidth / 2,
                rectangleWidth, rectangleHeight, PaintOp.CloseStroke);

            _contents.Translate(PageWidth / 2 - boxWidth / 2, posYbottomBox);

            double posYtop = boxHeight;
            _contents.DrawText(0, ref posYtop, 0, 0, box);

            _contents.RestoreGraphicsState();

            posYstart = posYbottomRectangle;

            return DrawMethodReturnOptions.Success;
        }
        /// <summary>
        /// Draws the invoice details title with rectangle on the second of the invoice
        /// (or actually also any other page, but on every other page, the invoice number is also drawn in the header).
        /// This is done at the position of posYstart.
        /// </summary>
        /// <param name="posYstart">
        /// The top of the content which needs to be drawn
        /// At the end of the execution of this method,
        /// this parameter will be set to the bottom (with eventually some margin) of the content
        /// </param>
        /// <returns>A value of the DrawMethodReturnOptions enum</returns>
        private DrawMethodReturnOptions DrawInvoiceDetailsPageTitle(ref double posYstart)
        {
            _contents.SaveGraphicsState();

            // Something that is important to note when drawing a rectangle,
            // is that the height of that rectangle starts halfway the bottom line and ends halfway the top line.
            // Similarly, the width of the rectangle starts halfway the left line and ends halfway the right line.
            // Therefore, the origin of the rectangle is in the middle of the left bottom corner of the outer rectangle (with border)
            // and the left bottom corner of the inner rectangle (without border)
            // The same applies to all lines (draw origin is halfway the width of the line)

            const double rectangleMargin = 1;
            const double rectangleLineWidth = .5;
            // The width of the inner rectangle + half the width of the left and the right line together
            const double rectangleWidth = PageWidth - MarginX * 2 - rectangleLineWidth;

            string text = "Invoice Details: ";
            double boxWidth = Math.Min(_calibriBold.TextWidth(CalibriNormalFontSize, text), rectangleWidth - rectangleLineWidth - rectangleMargin * 2);
            TextBox box = new TextBox(boxWidth);

            box.AddText(_calibriBold, CalibriNormalFontSize, text);
            box.Terminate();

            double boxHeight = box.BoxHeight;

            // The height of the inner rectangle + half the width of the bottom and the top line together
            double rectangleHeight = boxHeight + rectangleMargin * 2 + rectangleLineWidth;

            double posYbottomBox = posYstart - rectangleLineWidth - rectangleMargin - boxHeight;
            // The bottom of the outer rectangle
            double posYbottomRectangle = posYstart - rectangleHeight - rectangleLineWidth;

            if (posYbottomRectangle < MarginY)
            {
                _contents.RestoreGraphicsState();

                return DrawMethodReturnOptions.NewPageRequired;
            }

            _contents.SetLineWidth(rectangleLineWidth);
            _contents.DrawRectangle(MarginX + rectangleLineWidth / 2, posYbottomRectangle + rectangleLineWidth / 2,
                rectangleWidth, rectangleHeight, PaintOp.CloseStroke);

            _contents.Translate(PageWidth / 2 - boxWidth / 2, posYbottomBox);

            double posYtop = boxHeight;
            _contents.DrawText(0, ref posYtop, 0, 0, box);

            _contents.RestoreGraphicsState();

            posYstart = posYbottomRectangle;

            return DrawMethodReturnOptions.Success;
        }

        /// <summary>
        /// Draws the invoice data (like the invoice date) with rectangle
        /// This is done at the position of posYstart.
        /// </summary>
        /// <param name="posYstart">
        /// The top of the content which needs to be drawn
        /// At the end of the execution of this method,
        /// this parameter will be set to the bottom (with eventually some margin) of the content
        /// </param>
        /// <returns>A value of the DrawMethodReturnOptions enum</returns>
        private DrawMethodReturnOptions DrawInvoiceData(ref double posYstart)
        {
            _contents.SaveGraphicsState();

            const double rectangleMargin = 3;
            const double rectangleLineWidth = .5;
            // The width of the inner rectangle + half the width of the left and the right line together
            const double rectangleWidth = PageWidth - MarginX * 2 - rectangleLineWidth;

            string invoiceDateTitleText = "Date:";
            string invoiceDateText = string.Format("{0:00} {1} {2}", Invoice.CreationDate.Day, GetMonthNameByNumber(Invoice.CreationDate.Month), Invoice.CreationDate.Year);
            string referenceTitleText = "Reference:";
            string referenceText = string.Format("Fees {0} {1}", GetMonthNameByNumber(Invoice.CreationDate.Month), Invoice.CreationDate.Year);
            referenceText += string.Format(" - {0} {1}", GetMonthNameByNumber(invoiceDetails.OrderBy(d => d.ActivityDate).Last().ActivityDate.Value.Month), invoiceDetails.OrderBy(d => d.ActivityDate).Last().ActivityDate.Value.Year);
            string dueDateTitleText = "Due date:";
            string dueDateText = "Date of invoice";
            string vatText = "VAT exempted according to VAT Code Art. 21§ 2";

            const double titleWidth = 70;
            const double textWidth = (rectangleWidth - rectangleLineWidth) / 2 - rectangleMargin - titleWidth;
            const double vatWidth = (rectangleWidth - rectangleLineWidth) / 2 - rectangleMargin;

            TextBox invoiceDateTitleBox = GetTextBoxWithText(invoiceDateTitleText, _calibriNormal, CalibriNormalFontSize, titleWidth);
            TextBox invoiceDateBox = GetTextBoxWithText(invoiceDateText, _calibriNormal, CalibriNormalFontSize, textWidth);
            TextBox referenceTitleBox = GetTextBoxWithText(referenceTitleText, _calibriNormal, CalibriNormalFontSize, titleWidth);
            TextBox referenceBox = GetTextBoxWithText(referenceText, _calibriBold, CalibriNormalFontSize, textWidth);
            TextBox dueDateTitleBox = GetTextBoxWithText(dueDateTitleText, _calibriNormal, CalibriNormalFontSize, titleWidth);
            TextBox dueDateBox = GetTextBoxWithText(dueDateText, _calibriNormal, CalibriNormalFontSize, textWidth);
            TextBox vatBox = GetTextBoxWithText(vatText, _calibriNormal, CalibriNormalFontSize, vatWidth);

            double marginBetweenLines = 7;

            // The height of the inner rectangle + half the width of the bottom and the top line together
            double rectangleHeight = invoiceDateTitleBox.BoxHeight + referenceTitleBox.BoxHeight + marginBetweenLines + rectangleMargin * 2 + rectangleLineWidth;

            // The bottom of the outer rectangle
            double posYbottomRectangle = posYstart - rectangleHeight - rectangleLineWidth;

            if (posYbottomRectangle < MarginY)
            {
                _contents.RestoreGraphicsState();

                return DrawMethodReturnOptions.NewPageRequired;
            }

            _contents.SetLineWidth(rectangleLineWidth);
            _contents.DrawRectangle(MarginX + rectangleLineWidth / 2, posYbottomRectangle + rectangleLineWidth / 2,
                rectangleWidth, rectangleHeight, PaintOp.CloseStroke);

            double posXleft = MarginX + rectangleLineWidth + rectangleMargin;
            double posYtopTitle1 = posYstart - rectangleLineWidth - rectangleMargin;
            _contents.DrawText(posXleft, ref posYtopTitle1, posYtopTitle1 - invoiceDateTitleBox.BoxHeight, 0, invoiceDateTitleBox);
            posYtopTitle1 -= marginBetweenLines;
            _contents.DrawText(posXleft, ref posYtopTitle1, posYtopTitle1 - referenceTitleBox.BoxHeight, 0, referenceTitleBox);

            posXleft += titleWidth;
            double posYtopData1 = posYstart - rectangleLineWidth - rectangleMargin;
            _contents.DrawText(posXleft, ref posYtopData1, posYtopData1 - invoiceDateBox.BoxHeight, 0, invoiceDateBox);
            posYtopData1 -= marginBetweenLines;
            _contents.DrawText(posXleft, ref posYtopData1, posYtopData1 - referenceBox.BoxHeight, 0, referenceBox);

            posXleft = PageWidth / 2;
            double posYtopTitle2 = posYstart - rectangleLineWidth - rectangleMargin;
            _contents.DrawText(posXleft, ref posYtopTitle2, posYtopTitle2 - dueDateTitleBox.BoxHeight, 0, dueDateTitleBox);
            //CMTINVOICE-51: Hide VAT specific text on invoice for Belgian distributors
            Distributor distributor = _accountingController.GetDistributor(Invoice.DistributorId);
            if (!((distributor.Address.Country == "Belgium" || distributor.Address.Country == "BE") && (Invoice.VAT.HasValue && Invoice.VAT.Value != 0m)))
            {
                posYtopTitle2 -= marginBetweenLines;
                _contents.DrawText(posXleft, ref posYtopTitle2, posYtopTitle2 - vatBox.BoxHeight, 0, vatBox);

            }
            //CMTINVOICE-51 END
            posXleft += titleWidth;
            double posYtopData2 = posYstart - rectangleLineWidth - rectangleMargin;
            _contents.DrawText(posXleft, ref posYtopData2, posYtopData2 - dueDateBox.BoxHeight, 0, dueDateBox);

            _contents.RestoreGraphicsState();

            posYstart = posYbottomRectangle;

            return DrawMethodReturnOptions.Success;
        }

        /// <summary>
        /// Draws the message to warn the distributor that it must mention the invoice number on his transaction
        /// This is done at the position of posYstart.
        /// </summary>
        /// <param name="posYstart">
        /// The top of the content which needs to be drawn
        /// At the end of the execution of this method,
        /// this parameter will be set to the bottom (with eventually some margin) of the content
        /// </param>
        /// <returns>A value of the DrawMethodReturnOptions enum</returns>
        private DrawMethodReturnOptions DrawInvoiceNumberMessage(ref double posYstart)
        {
            _contents.SaveGraphicsState();

            string text = _invoiceController.GetInvoiceMessage("ImportantMessage");

            TextBox box = new TextBox(Math.Min(_calibriBold.TextWidth(CalibriNormalFontSize, text), PageWidth - 2 * MarginX));

            box.AddText(_calibriBold, CalibriNormalFontSize, RedMessage, text);
            box.Terminate();

            double posYbottomBox = posYstart - box.BoxHeight;

            if (posYbottomBox < MarginY)
            {
                _contents.RestoreGraphicsState();

                return DrawMethodReturnOptions.NewPageRequired;
            }

            _contents.Translate(PageWidth / 2 - box.BoxWidth / 2, posYbottomBox);

            double posYtop = box.BoxHeight;
            _contents.DrawText(0, ref posYtop, 0, 0, box);

            _contents.RestoreGraphicsState();

            posYstart = posYbottomBox;

            return DrawMethodReturnOptions.Success;
        }

        /// <summary>
        /// Draws the table header of the invoiceDetails table.
        /// This is done at the position of posYstart
        /// </summary>
        /// <param name="posYstart">
        /// The top of the content which needs to be drawn
        /// At the end of the execution of this method,
        /// this parameter will be set to the bottom of the content
        /// </param>
        /// <returns>A value of the DrawMethodReturnOptions enum</returns>
        private DrawMethodReturnOptions DrawTableHeader(ref double posYstart)
        {
            TextBox[] boxes = GetColumnTextBoxesWithText(_calibriBold,
                "Customer Reference Name",
                "Service Profile",
                "Serial Number",
                "MAC Address",
                "Action",
                "Activity Date",
                "Price");//,
                         //"#",
                         //"Total");

            return DrawTableRow(ref posYstart, GrayBackGround, boxes);
        }

        /// <summary>
        /// Draws one invoiceDetail as a row in the table.
        /// This is done at the position of posYstart
        /// </summary>
        /// <param name="posYstart">
        /// The top of the content which needs to be drawn
        /// At the end of the execution of this method,
        /// this parameter will be set to the bottom of the content
        /// </param>
        /// <param name="detail">The detail that should be drawn in the row</param>
        /// <returns>A value of the DrawMethodReturnOptions enum</returns>
        private DrawMethodReturnOptions DrawInvoiceDetail(ref double posYstart, InvoiceDetail detail, bool newPageRequired)
        {
            string terminalIdShortened = string.Empty;

            if (detail.TerminalId != null)
            {
                terminalIdShortened = detail.TerminalId.Length > 40 ? detail.TerminalId.Substring(0, 40) + "..." : detail.TerminalId;
            }

            TextBox[] boxes = GetColumnTextBoxesWithText(
                terminalIdShortened,
                detail.ServicePack,
                detail.Serial,
                detail.MacAddress,
                detail.BillableDescription,
                detail.ActivityDate.HasValue ? detail.ActivityDate.Value.ToString("dd/MM/yyyy HH:mm") : string.Empty,
                string.Format("{0}{1:0}", _currencySymbol, Math.Round(detail.UnitPrice)));//,

            //detail.Items.ToString(),
            //string.Format("{0}{1}", _currencySymbol, detail.UnitPrice * detail.Items));
            if (!newPageRequired)
                _subTotal += detail.UnitPrice;

            return DrawTableRow(ref posYstart, boxes);
        }

        /// <summary>
        /// Draws the total price as a row in the table.
        /// This is done at the position of posYstart
        /// </summary>
        /// <param name="posYstart">
        /// The top of the content which needs to be drawn
        /// At the end of the execution of this method,
        /// this parameter will be set to the bottom of the content
        /// </param>
        /// <returns>A value of the DrawMethodReturnOptions enum</returns>
        private DrawMethodReturnOptions DrawTableSubTotalRow(ref double posYstart, bool newPage)
        {
            TableCustomColumnText[] totalText;
            if (!newPage)
            {
                totalText = new TableCustomColumnText[]
                {
                    new TableCustomColumnText(_calibriBold)
                    {
                        Text = "SUBTOTAL:",
                        StartColumnIndex = StartCustomColumnTitleIndex,
                        EndColumnIndex = EndCustomColumnTitleIndex
                    },
                    new TableCustomColumnText(_calibriBold)
                    {
                        Text = string.Format("{0}{1:0}", _currencySymbol, Math.Round(_subTotal)),
                        StartColumnIndex = StartCustomColumnValueIndex,
                        EndColumnIndex = EndCustomColumnValueIndex
                    }
                };
                _subTotalList.Add(_subTotal);
                _tempSubTotal = _subTotal;
                _subTotalCounter++;
                _subTotal = 0;
            }
            else
            {
                totalText = new TableCustomColumnText[]
                {
                    new TableCustomColumnText(_calibriBold)
                    {
                        Text = "SUBTOTAL:",
                        StartColumnIndex = StartCustomColumnTitleIndex,
                        EndColumnIndex = EndCustomColumnTitleIndex
                    },
                    new TableCustomColumnText(_calibriBold)
                    {
                        Text = string.Format("{0}{1:0}", _currencySymbol, Math.Round(_tempSubTotal)),
                        StartColumnIndex = StartCustomColumnValueIndex,
                        EndColumnIndex = EndCustomColumnValueIndex
                    }
                };
                _tempSubTotal = 0;
                _subTotalCounter++;
            }

            return DrawTableCustomRow(ref posYstart, totalText);
        }

        /// <summary>
        /// Draws the total price as a row in the table for previous months.
        /// This is done at the position of posYstart
        /// </summary>
        /// <param name="posYstart">
        /// The top of the content which needs to be drawn
        /// At the end of the execution of this method,
        /// this parameter will be set to the bottom of the content
        /// </param>
        /// <returns>A value of the DrawMethodReturnOptions enum</returns>
        private DrawMethodReturnOptions DrawTableSubTotalRowPreviousMonths(ref double posYstart, bool newPage)
        {
            TableCustomColumnText[] totalText;
            if (!newPage)
            {
                totalText = new TableCustomColumnText[]
                {
                    new TableCustomColumnText(_calibriBold)
                    {
                        Text = "Previous Months Adjustment:",
                        StartColumnIndex = StartCustomColumnTitleIndex,
                        EndColumnIndex = EndCustomColumnTitleIndex
                    },
                    new TableCustomColumnText(_calibriBold)
                    {
                        Text = string.Format("{0}{1:0}", _currencySymbol, Math.Round(_subTotal)),
                        StartColumnIndex = StartCustomColumnValueIndex,
                        EndColumnIndex = EndCustomColumnValueIndex
                    }
                };
                _subTotalList.Add(_subTotal);
                _tempSubTotal = _subTotal;
                _subTotalCounter++;
                _subTotal = 0;
            }
            else
            {
                totalText = new TableCustomColumnText[]
                {
                    new TableCustomColumnText(_calibriBold)
                    {
                        Text = "Previous Months Adjustment:",
                        StartColumnIndex = StartCustomColumnTitleIndex,
                        EndColumnIndex = EndCustomColumnTitleIndex
                    },
                    new TableCustomColumnText(_calibriBold)
                    {
                        Text = string.Format("{0}{1:0}", _currencySymbol, Math.Round(_tempSubTotal)),
                        StartColumnIndex = StartCustomColumnValueIndex,
                        EndColumnIndex = EndCustomColumnValueIndex
                    }
                };
                _tempSubTotal = 0;
                _subTotalCounter++;
            }

            return DrawTableCustomRow(ref posYstart, totalText);
        }

        /// <summary>
        /// Draws the total price as a row in the table.
        /// This is done at the position of posYstart
        /// </summary>
        /// <param name="posYstart">
        /// The top of the content which needs to be drawn
        /// At the end of the execution of this method,
        /// this parameter will be set to the bottom of the content
        /// </param>
        /// <returns>A value of the DrawMethodReturnOptions enum</returns>
        private DrawMethodReturnOptions DrawTableTotalRow(ref double posYstart)
        {
            TableCustomColumnText[] totalText = new TableCustomColumnText[]
            {
                new TableCustomColumnText(_calibriBold)
                {
                    Text = "TOTAL:",
                    StartColumnIndex = StartCustomColumnTitleIndex,
                    EndColumnIndex = EndCustomColumnTitleIndex
                },
                new TableCustomColumnText(_calibriBold)
                {
                    Text = string.Format("{0}{1:0}", _currencySymbol, Math.Round(Invoice.TotalAmount)),
                    StartColumnIndex = StartCustomColumnValueIndex,
                    EndColumnIndex = EndCustomColumnValueIndex
                }
            };

            return DrawTableCustomRow(ref posYstart, totalText);
        }

        /// <summary>
        /// Draws the number of active terminals as a row in the table.
        /// This is done at the position of posYstart
        /// </summary>
        /// <param name="posYstart">
        /// The top of the content which needs to be drawn
        /// At the end of the execution of this method,
        /// this parameter will be set to the bottom of the content
        /// </param>
        /// <returns>A value of the DrawMethodReturnOptions enum</returns>
        private DrawMethodReturnOptions DrawTableActiveTerminalsRow(ref double posYstart)
        {
            TableCustomColumnText[] activeTerminalsText = new TableCustomColumnText[]
            {
                new TableCustomColumnText(_calibriBold)
                {
                    Text = "Nr. of Active Terminals:",
                    StartColumnIndex = StartCustomColumnTitleIndex,
                    EndColumnIndex = EndCustomColumnTitleIndex
                },
                new TableCustomColumnText(_calibriBold)
                {
                    Text = Invoice.ActiveTerminals.HasValue ? Invoice.ActiveTerminals.Value.ToString() : "0",
                    StartColumnIndex = StartCustomColumnValueIndex,
                    EndColumnIndex = EndCustomColumnValueIndex
                }
            };

            return DrawTableCustomRow(ref posYstart, activeTerminalsText);
        }

        /// <summary>
        /// Draws the Total Price row
        /// This is done at the position of posYstart
        /// </summary>
        /// <param name="posYstart">
        /// The top of the content which needs to be drawn
        /// At the end of the execution of this method,
        /// this parameter will be set to the bottom of the content
        /// </param>
        /// <returns>A value of the DrawMethodReturnOptions enum</returns>
        private DrawMethodReturnOptions DrawTotalPrice(ref double posYstart)
        {
            TableCustomColumnText[] totalPriceRectangles = new TableCustomColumnText[]
            {
                new TableCustomColumnText()
                {
                     StartColumnIndex = StartTotalsRectangleCustomIndex,
                     EndColumnIndex = EndTotalsRectangleCustomIndex
                }
            };

            TableCustomColumnText[] totalPriceText = new TableCustomColumnText[]
            {
                new TableCustomColumnText(_calibriBold)
                {
                    Text = "TOTAL:",
                    StartColumnIndex = StartTotalsTitleCustomIndex,
                    EndColumnIndex = EndTotalsTitleCustomIndex
                },
                new TableCustomColumnText(_calibriBold)
                {
                    Text = string.Format("{0}{1:0}", _currencySymbol, Math.Round(Invoice.TotalAmount)),
                    StartColumnIndex = StartTotalsValueCustomIndex,
                    EndColumnIndex = EndTotalsValueCustomIndex
                }
            };

            return DrawTableCustomRectanglesAndText(ref posYstart, GrayBackGround, totalPriceRectangles, totalPriceText);
        }

        /// <summary>
        /// Draws the Incentive row
        /// This is done at the position of posYstart
        /// </summary>
        /// <param name="posYstart">
        /// The top of the content which needs to be drawn
        /// At the end of the execution of this method,
        /// this parameter will be set to the bottom of the content
        /// </param>
        /// <returns>A value of the DrawMethodReturnOptions enum</returns>
        private DrawMethodReturnOptions DrawIncentive(ref double posYstart)
        {
            TableCustomColumnText[] incentiveRectangles = new TableCustomColumnText[]
            {
                new TableCustomColumnText()
                {
                     StartColumnIndex = StartTotalsRectangleCustomIndex,
                     EndColumnIndex = EndTotalsRectangleCustomIndex
                }
            };

            TableCustomColumnText[] incentiveText = new TableCustomColumnText[]
            {
                new TableCustomColumnText(_calibriBold)
                {
                    Text = string.Format( (Invoice.Incentive > 0) ? "Incentive ({0:P1}):":"Incentive ({0:P0}):", Math.Round(Invoice.Incentive) ),
                    StartColumnIndex = StartTotalsTitleCustomIndex,
                    EndColumnIndex = EndTotalsTitleCustomIndex
                },
                new TableCustomColumnText(_calibriBold)
                {
                    Text = string.Format("- {0}{1:0}", _currencySymbol, Math.Round(Invoice.TotalAmount * Invoice.Incentive)),
                    StartColumnIndex = StartTotalsValueCustomIndex,
                    EndColumnIndex = EndTotalsValueCustomIndex
                }
            };

            return DrawTableCustomRectanglesAndText(ref posYstart, incentiveRectangles, incentiveText);
        }

        /// <summary>
        /// Draws the VAT row
        /// This is done at the position of posYstart
        /// </summary>
        /// <param name="posYstart">
        /// The top of the content which needs to be drawn
        /// At the end of the execution of this method,
        /// this parameter will be set to the bottom of the content
        /// </param>
        /// <returns>A value of the DrawMethodReturnOptions enum</returns>
        private DrawMethodReturnOptions DrawVat(ref double posYstart)
        {
            TableCustomColumnText[] vatRectangles = new TableCustomColumnText[]
            {
                new TableCustomColumnText()
                {
                     StartColumnIndex = StartTotalsRectangleCustomIndex,
                     EndColumnIndex = EndTotalsRectangleCustomIndex
                }
            };

            TableCustomColumnText[] vatText = new TableCustomColumnText[]
            {
                new TableCustomColumnText(_calibriBold)
                {
                    Text = string.Format("VAT ({0:P0}):", Invoice.VAT.HasValue ? Math.Round(Invoice.VAT.Value) : 0),
                    StartColumnIndex = StartTotalsTitleCustomIndex,
                    EndColumnIndex = EndTotalsTitleCustomIndex
                },
                new TableCustomColumnText(_calibriBold)
                {
                    Text = string.Format("{0}{1:0}", _currencySymbol, Math.Round(Invoice.TotalAmount * (Invoice.VAT.HasValue ? Invoice.VAT.Value : 0))),
                    StartColumnIndex = StartTotalsValueCustomIndex,
                    EndColumnIndex = EndTotalsValueCustomIndex
                }
            };

            return DrawTableCustomRectanglesAndText(ref posYstart, vatRectangles, vatText);
        }

        /// <summary>
        /// Draws the Grand Total row
        /// This is done at the position of posYstart
        /// </summary>
        /// <param name="posYstart">
        /// The top of the content which needs to be drawn
        /// At the end of the execution of this method,
        /// this parameter will be set to the bottom of the content
        /// </param>
        /// <returns>A value of the DrawMethodReturnOptions enum</returns>
        private DrawMethodReturnOptions DrawGrandTotal(ref double posYstart)
        {
            TableCustomColumnText[] grandTotalRectangles = new TableCustomColumnText[]
            {
                new TableCustomColumnText()
                {
                     StartColumnIndex = StartTotalsRectangleCustomIndex,
                     EndColumnIndex = EndTotalsRectangleCustomIndex
                }
            };

            TableCustomColumnText[] grandTotalText = new TableCustomColumnText[]
            {
                new TableCustomColumnText(_calibriBold)
                {
                    Text = "GRAND TOTAL:",
                    StartColumnIndex = StartTotalsTitleCustomIndex,
                    EndColumnIndex = EndTotalsTitleCustomIndex
                },
                new TableCustomColumnText(_calibriBold)
                {
                    Text = string.Format("{0}{1:0}", _currencySymbol, Math.Round(Invoice.GrandTotal)),
                    StartColumnIndex = StartTotalsValueCustomIndex,
                    EndColumnIndex = EndTotalsValueCustomIndex
                }
            };

            return DrawTableCustomRectanglesAndText(ref posYstart, GrayBackGround, grandTotalRectangles, grandTotalText);
        }

        /// <summary>
        /// Draws the Financial Interest row
        /// This is done at the position of posYstart
        /// </summary>
        /// <param name="posYstart">
        /// The top of the content which needs to be drawn
        /// At the end of the execution of this method,
        /// this parameter will be set to the bottom of the content
        /// </param>
        /// <returns>A value of the DrawMethodReturnOptions enum</returns>
        private DrawMethodReturnOptions DrawFinancialInterest(ref double posYstart)
        {
            TableCustomColumnText[] financialInterestRectangles = new TableCustomColumnText[]
            {
                new TableCustomColumnText()
                {
                     StartColumnIndex = StartTotalsRectangleCustomIndex,
                     EndColumnIndex = EndTotalsRectangleCustomIndex
                }
            };

            TableCustomColumnText[] financialInterestText = new TableCustomColumnText[]
            {
                new TableCustomColumnText(_calibriBold)
                {
                    Text = "Financial Interest (1,5% on all open previous invoices):",
                    StartColumnIndex = StartTotalsTitleCustomIndex,
                    EndColumnIndex = EndTotalsTitleCustomIndex
                },
                new TableCustomColumnText(_calibriBold)
                {
                    Text = string.Format("{0}{1:0}", _currencySymbol, Invoice.FinancialInterest.HasValue ? Math.Round(Invoice.FinancialInterest.Value) : 0),
                    StartColumnIndex = StartTotalsValueCustomIndex,
                    EndColumnIndex = EndTotalsValueCustomIndex
                }
            };

            return DrawTableCustomRectanglesAndText(ref posYstart, financialInterestRectangles, financialInterestText);
        }

        /// <summary>
        /// Draws the Bank Charges row
        /// This is done at the position of posYstart
        /// </summary>
        /// <param name="posYstart">
        /// The top of the content which needs to be drawn
        /// At the end of the execution of this method,
        /// this parameter will be set to the bottom of the content
        /// </param>
        /// <returns>A value of the DrawMethodReturnOptions enum</returns>
        private DrawMethodReturnOptions DrawBankCharges(ref double posYstart)
        {
            TableCustomColumnText[] bankChargesRectangles = new TableCustomColumnText[]
            {
                new TableCustomColumnText()
                {
                     StartColumnIndex = StartTotalsRectangleCustomIndex,
                     EndColumnIndex = EndTotalsRectangleCustomIndex
                }
            };

            TableCustomColumnText[] bankChargesText = new TableCustomColumnText[]
            {
                new TableCustomColumnText(_calibriBold)
                {
                    Text = "Bank Charges:",
                    StartColumnIndex = StartTotalsTitleCustomIndex,
                    EndColumnIndex = EndTotalsTitleCustomIndex
                },
                new TableCustomColumnText(_calibriBold)
                {
                    Text = string.Format("{0}{1:0}", _currencySymbol, Math.Round(Invoice.BankChargesValue)),
                    StartColumnIndex = StartTotalsValueCustomIndex,
                    EndColumnIndex = EndTotalsValueCustomIndex
                }
            };

            return DrawTableCustomRectanglesAndText(ref posYstart, bankChargesRectangles, bankChargesText);
        }

        /// <summary>
        /// Draws the Paid in Advance row
        /// This is done at the position of posYstart
        /// </summary>
        /// <param name="posYstart">
        /// The top of the content which needs to be drawn
        /// At the end of the execution of this method,
        /// this parameter will be set to the bottom of the content
        /// </param>
        /// <returns>A value of the DrawMethodReturnOptions enum</returns>
        private DrawMethodReturnOptions DrawPaidInAdvance(ref double posYstart)
        {
            TableCustomColumnText[] paidInAdvanceRectangles = new TableCustomColumnText[]
            {
                new TableCustomColumnText()
                {
                     StartColumnIndex = StartTotalsRectangleCustomIndex,
                     EndColumnIndex = EndTotalsRectangleCustomIndex
                }
            };

            decimal paidInAdvance = (Invoice.AdvanceAutomatic ?? 0) + (Invoice.AdvanceManual ?? 0);

            TableCustomColumnText[] paidInAdvanceText = new TableCustomColumnText[]
            {
                new TableCustomColumnText(_calibriBold)
                {
                    Text = "Services already paid:",
                    StartColumnIndex = StartTotalsTitleCustomIndex,
                    EndColumnIndex = EndTotalsTitleCustomIndex
                },
                new TableCustomColumnText(_calibriBold)
                {
                    Text = string.Format("- {0}{1:0}", _currencySymbol, Math.Round(paidInAdvance)),
                    StartColumnIndex = StartTotalsValueCustomIndex,
                    EndColumnIndex = EndTotalsValueCustomIndex
                }
            };

            return DrawTableCustomRectanglesAndText(ref posYstart, paidInAdvanceRectangles, paidInAdvanceText);
        }

        /// <summary>
        /// Draws the Transactions row
        /// This is done at the position of posYstart
        /// </summary>
        /// <param name="posYstart">
        /// The top of the content which needs to be drawn
        /// At the end of the execution of this method,
        /// this parameter will be set to the bottom of the content
        /// </param>
        /// <returns>A value of the DrawMethodReturnOptions enum</returns>
        private DrawMethodReturnOptions DrawTransactions(ref double posYstart)
        {
            TableCustomColumnText[] transactionsRectangles = new TableCustomColumnText[]
            {
                new TableCustomColumnText()
                {
                     StartColumnIndex = StartTotalsRectangleCustomIndex,
                     EndColumnIndex = EndTotalsRectangleCustomIndex
                }
            };

            TableCustomColumnText[] transactionsText = new TableCustomColumnText[]
            {
                new TableCustomColumnText(_calibriBold)
                {
                    Text = String.IsNullOrEmpty(transaction.Remark) ? "Credit/Debit: " : "Credit/Debit (" + transaction.Remark + "):",
                    StartColumnIndex = StartTotalsTitleCustomIndex,
                    EndColumnIndex = EndTotalsTitleCustomIndex
                },
                new TableCustomColumnText(_calibriBold)
                {
                    Text = string.Format("{0}{1:0}", _currencySymbol, Math.Round(transaction.Amount)),
                    StartColumnIndex = StartTotalsValueCustomIndex,
                    EndColumnIndex = EndTotalsValueCustomIndex
                }
            };
            return DrawTableCustomRectanglesAndText(ref posYstart, transactionsRectangles, transactionsText);
        }

        /// <summary>
        /// Draws the Paid in Advance row
        /// This is done at the position of posYstart
        /// </summary>
        /// <param name="posYstart">
        /// The top of the content which needs to be drawn
        /// At the end of the execution of this method,
        /// this parameter will be set to the bottom of the content
        /// </param>
        /// <returns>A value of the DrawMethodReturnOptions enum</returns>
        private DrawMethodReturnOptions DrawTotalForMonths(ref double posYstart, int i)
        {
            TableCustomColumnText[] totalForMonthsRectangles = new TableCustomColumnText[]
            {
                new TableCustomColumnText()
                {
                     StartColumnIndex = StartTotalsRectangleCustomIndex,
                     EndColumnIndex = EndTotalsRectangleCustomIndex
                }
            };

            decimal subtotalForMonth = (Invoice.ToBePaid * ((_subTotalList[i] / Invoice.TotalAmount) * 100)) / 100; //Invoice.ToBePaid / months;
            TableCustomColumnText[] paidInAdvanceText = new TableCustomColumnText[]
            {
                new TableCustomColumnText(_calibriBold)
                {
                    //Text = "Total for " + CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(monthNames[nextMonth]) + ":",
                    Text = "Total for " + monthNames[nextMonth].ToString("MMMM", new CultureInfo("en-GB")) + ":",
                    StartColumnIndex = StartTotalsTitleCustomIndex,
                    EndColumnIndex = EndTotalsTitleCustomIndex
                },
                new TableCustomColumnText(_calibriBold)
                {
                    Text = string.Format("{0}{1:0}", _currencySymbol, Math.Round(subtotalForMonth)),
                    StartColumnIndex = StartTotalsValueCustomIndex,
                    EndColumnIndex = EndTotalsValueCustomIndex
                }
            };
            nextMonth++;

            return DrawTableCustomRectanglesAndText(ref posYstart, totalForMonthsRectangles, paidInAdvanceText);
        }

        /// <summary>
        /// Draws the To Be Paid row
        /// This is done at the position of posYstart
        /// </summary>
        /// <param name="posYstart">
        /// The top of the content which needs to be drawn
        /// At the end of the execution of this method,
        /// this parameter will be set to the bottom of the content
        /// </param>
        /// <returns>A value of the DrawMethodReturnOptions enum</returns>
        private DrawMethodReturnOptions DrawToBePaid(ref double posYstart)
        {
            TableCustomColumnText[] toBePaidRectangles = new TableCustomColumnText[]
            {
                new TableCustomColumnText()
                {
                     StartColumnIndex = StartTotalsRectangleCustomIndex,
                     EndColumnIndex = EndTotalsRectangleCustomIndex
                }
            };

            TableCustomColumnText[] toBePaidText = new TableCustomColumnText[]
            {
                new TableCustomColumnText(_calibriBold)
                {
                    Text = "TO BE PAID:",
                    StartColumnIndex = StartTotalsTitleCustomIndex,
                    EndColumnIndex = EndTotalsTitleCustomIndex
                },
                new TableCustomColumnText(_calibriBold)
                {
                    Text = string.Format("{0}{1:0}", _currencySymbol, Math.Round(Invoice.ToBePaid)),
                    StartColumnIndex = StartTotalsValueCustomIndex,
                    EndColumnIndex = EndTotalsValueCustomIndex
                }
            };

            return DrawTableCustomRectanglesAndText(ref posYstart, GrayBackGround, toBePaidRectangles, toBePaidText);
        }

        /// <summary>
        /// Draws the To Be Paid Exchanged row
        /// This is done at the position of posYstart
        /// </summary>
        /// <param name="posYstart">
        /// The top of the content which needs to be drawn
        /// At the end of the execution of this method,
        /// this parameter will be set to the bottom of the content
        /// </param>
        /// <returns>A value of the DrawMethodReturnOptions enum</returns>
        private DrawMethodReturnOptions DrawToBePaidExchanged(ref double posYstart)
        {
            if (Invoice.ToBePaidExchanged.HasValue)
            {
                TableCustomColumnText[] toBePaidExchangedRectangles = new TableCustomColumnText[]
                {
                    new TableCustomColumnText()
                    {
                         StartColumnIndex = StartTotalsRectangleCustomIndex,
                         EndColumnIndex = EndTotalsRectangleCustomIndex
                    }
                };

                TableCustomColumnText[] toBePaidExchangedText = new TableCustomColumnText[]
                {
                    new TableCustomColumnText(_calibriBold)
                    {
                        Text = "TO BE PAID (EUR):",
                        StartColumnIndex = StartTotalsTitleCustomIndex,
                        EndColumnIndex = EndTotalsTitleCustomIndex
                    },
                    new TableCustomColumnText(_calibriBold)
                    {
                        Text = string.Format("€{0:0}", Math.Round(Invoice.ToBePaidExchanged.Value)),
                        StartColumnIndex = StartTotalsValueCustomIndex,
                        EndColumnIndex = EndTotalsValueCustomIndex
                    }
                };

                return DrawTableCustomRectanglesAndText(ref posYstart, GrayBackGround, toBePaidExchangedRectangles, toBePaidExchangedText);
            }

            return DrawMethodReturnOptions.Success;
        }

        /// <summary>
        /// Draws the unpaid invoices to be paid
        /// This is done at the position of posYstart
        /// </summary>
        /// <param name="posYstart">
        /// The top of the content which needs to be drawn
        /// At the end of the execution of this method,
        /// this parameter will be set to the bottom of the content
        /// </param>
        /// <returns>A value of the DrawMethodReturnOptions enum</returns>
        private DrawMethodReturnOptions DrawUnpaidInvoicesToBePaid(ref double posYstart)
        {
            if (unpaidInvoices.Length > 1)
            {
                TableCustomColumnText[] UnpaidInvoicesToBePaidRectangles = new TableCustomColumnText[]
                {
                    new TableCustomColumnText()
                    {
                         StartColumnIndex = StartTotalsRectangleCustomIndex,
                         EndColumnIndex = EndTotalsRectangleCustomIndex
                    }
                };

                TableCustomColumnText[] UnpaidInvoicesToBePaidText = new TableCustomColumnText[]
                {
                new TableCustomColumnText(_calibriBold)
                {
                    Text = "OPEN UNPAID INVOICES (total):",
                    StartColumnIndex = StartTotalsTitleCustomIndex,
                    EndColumnIndex = EndTotalsTitleCustomIndex
                },
                new TableCustomColumnText(_calibriBold)
                {
                    Text = string.Format("€{0:0}", Math.Round(unpaidInvoicesTotal)),
                    StartColumnIndex = StartTotalsValueCustomIndex,
                    EndColumnIndex = EndTotalsValueCustomIndex
                }
                };

                return DrawTableCustomRectanglesAndText(ref posYstart, GrayBackGround, UnpaidInvoicesToBePaidRectangles, UnpaidInvoicesToBePaidText);
            }
            return DrawMethodReturnOptions.Success;
        }

        /// <summary>
        /// Draws the 'Payment Details' message at the bottom of the page.
        /// This is done at the position of posYstart
        /// </summary>
        /// <param name="posYstart">
        /// The top of the content which needs to be drawn
        /// At the end of the execution of this method,
        /// this parameter will be set to the bottom of the content
        /// </param>
        /// <returns>A value of the DrawMethodReturnOptions enum</returns>
        private DrawMethodReturnOptions DrawPaymentDetailsMessage(ref double posYstart)
        {
            _contents.SaveGraphicsState();

            string textBold = _invoiceController.GetInvoiceMessage("DetailsMessage");

            TextBox box = new TextBox(Math.Min(_calibriBold.TextWidth(CalibriNormalFontSize, textBold),
                PageWidth - 2 * MarginX));

            box.AddText(_calibriBold, CalibriNormalFontSize, DrawStyle.Underline, RedMessage, textBold);
            box.Terminate();

            double posYbottomBox = posYstart - box.BoxHeight;

            if (posYbottomBox < MarginY)
            {
                _contents.RestoreGraphicsState();

                return DrawMethodReturnOptions.NewPageRequired;
            }

            _contents.Translate(PageWidth / 2 - box.BoxWidth / 2, posYbottomBox);

            double posYtop = box.BoxHeight;
            _contents.DrawText(0, ref posYtop, 0, 0, box);

            _contents.RestoreGraphicsState();

            posYstart = posYbottomBox;

            return DrawMethodReturnOptions.Success;
        }

        /// <summary>
        /// Draws the 'Invoice Number' message at the bottom of the page.
        /// This is done at the position of posYstart
        /// </summary>
        /// <param name="posYstart">
        /// The top of the content which needs to be drawn
        /// At the end of the execution of this method,
        /// this parameter will be set to the bottom of the content
        /// </param>
        /// <returns>A value of the DrawMethodReturnOptions enum</returns>
        private DrawMethodReturnOptions DrawInvoiceNumberFooterMessage(ref double posYstart)
        {
            _contents.SaveGraphicsState();

            string text = _invoiceController.GetInvoiceMessage("ImportantMessage");

            TextBox box = new TextBox(Math.Min(_calibriNormal.TextWidth(CalibriNormalFontSize, text), PageWidth - 2 * MarginX));

            box.AddText(_calibriNormal, CalibriNormalFontSize, text);
            box.Terminate();

            double posYbottomBox = posYstart - box.BoxHeight;

            if (posYbottomBox < MarginY)
            {
                _contents.RestoreGraphicsState();

                return DrawMethodReturnOptions.NewPageRequired;
            }

            _contents.Translate(PageWidth / 2 - box.BoxWidth / 2, posYbottomBox);

            double posYtop = box.BoxHeight;
            _contents.DrawText(0, ref posYtop, 0, 0, box);

            _contents.RestoreGraphicsState();

            posYstart = posYbottomBox;

            return DrawMethodReturnOptions.Success;
        }

        /// <summary>
        /// Draws the 'Bank Details' message at the bottom of the page.
        /// This is done at the position of posYstart
        /// </summary>
        /// <param name="posYstart">
        /// The top of the content which needs to be drawn
        /// At the end of the execution of this method,
        /// this parameter will be set to the bottom of the content
        /// </param>
        /// <returns>A value of the DrawMethodReturnOptions enum</returns>
        private DrawMethodReturnOptions DrawBankDetailsMessage(ref double posYstart)
        {
            _contents.SaveGraphicsState();

            string text = _invoiceController.GetInvoiceMessage("InformativeMessage");

            TextBox box = new TextBox(Math.Min(_calibriNormal.TextWidth(CalibriNormalFontSize, text), PageWidth - 2 * MarginX));

            box.AddText(_calibriNormal, CalibriNormalFontSize, text);
            box.Terminate();

            double posYbottomBox = posYstart - box.BoxHeight;

            if (posYbottomBox < MarginY)
            {
                _contents.RestoreGraphicsState();

                return DrawMethodReturnOptions.NewPageRequired;
            }

            _contents.Translate(PageWidth / 2 - box.BoxWidth / 2, posYbottomBox);

            double posYtop = box.BoxHeight;
            _contents.DrawText(0, ref posYtop, 0, 0, box);

            _contents.RestoreGraphicsState();

            posYstart = posYbottomBox;

            return DrawMethodReturnOptions.Success;
        }

        /// <summary>
        /// Draws the 'Thank you' message at the bottom of the page.
        /// This is done at the position of posYstart
        /// </summary>
        /// <param name="posYstart">
        /// The top of the content which needs to be drawn
        /// At the end of the execution of this method,
        /// this parameter will be set to the bottom of the content
        /// </param>
        /// <returns>A value of the DrawMethodReturnOptions enum</returns>
        private DrawMethodReturnOptions DrawThankYouMessage(ref double posYstart)
        {
            _contents.SaveGraphicsState();

            string text = _invoiceController.GetInvoiceMessage("ThanksMessage");

            TextBox box = new TextBox(Math.Min(_calibriBold.TextWidth(CalibriNormalFontSize, text), PageWidth - 2 * MarginX));

            box.AddText(_calibriBold, CalibriNormalFontSize, BlueSatAdsl, text);
            box.Terminate();

            double posYbottomBox = posYstart - box.BoxHeight;

            if (posYbottomBox < MarginY)
            {
                _contents.RestoreGraphicsState();

                return DrawMethodReturnOptions.NewPageRequired;
            }

            _contents.Translate(PageWidth / 2 - box.BoxWidth / 2, posYbottomBox);

            double posYtop = box.BoxHeight;
            _contents.DrawText(0, ref posYtop, 0, 0, box);

            _contents.RestoreGraphicsState();

            posYstart = posYbottomBox;

            return DrawMethodReturnOptions.Success;
        }

        /// <summary>
        /// Draws the Terms And Conditions at the bottom of the page.
        /// This is done at the position of posYstart
        /// </summary>
        /// <param name="posYstart">
        /// The top of the content which needs to be drawn
        /// At the end of the execution of this method,
        /// this parameter will be set to the bottom of the content
        /// </param>
        /// <returns>A value of the DrawMethodReturnOptions enum</returns>
        private DrawMethodReturnOptions DrawTermsAndConditions(ref double posYstart)
        {
            _contents.SaveGraphicsState();

            string text = _invoiceController.GetInvoiceMessage("TermsAndConditions");

            TextBox box = new TextBox(Math.Min(_calibriBold.TextWidth(CalibriNormalFontSize, text), PageWidth - 2 * MarginX));

            box.AddText(_calibriBold, CalibriNormalFontSize, text);
            box.Terminate();

            double posYbottomBox = posYstart - box.BoxHeight;

            if (posYbottomBox < MarginY)
            {
                _contents.RestoreGraphicsState();

                return DrawMethodReturnOptions.NewPageRequired;
            }

            _contents.Translate(PageWidth / 2 - box.BoxWidth / 2, posYbottomBox);

            double posYtop = box.BoxHeight;
            _contents.DrawText(0, ref posYtop, 0, 0, box);

            _contents.RestoreGraphicsState();

            posYstart = posYbottomBox;

            return DrawMethodReturnOptions.Success;
        }

        #endregion

        #region PDF Draw Content Helper Methods

        /// <summary>
        /// Gets a terminated textbox with a specified text, shown in a specified font in a specified fontSize
        /// </summary>
        /// <param name="text">The text to show in the textbox</param>
        /// <param name="font">The font to use for the text</param>
        /// <param name="fontSize">The size to use for the text with the specified font</param>
        /// <param name="width">The width of the textbox</param>
        /// <returns>A terminated textbox with a specified text, shown in a specified font in a specified fontSize</returns>
        private TextBox GetTextBoxWithText(string text, PdfFont font, double fontSize, double width)
        {
            text = text ?? string.Empty; // Otherwise, there can be a NullReferenceException when using TextWidth

            TextBox box = new TextBox(width);

            box.AddText(font, fontSize, text);
            box.Terminate();

            return box;
        }

        /// <summary>
        /// Gets the maximal BoxHeight in an array of given TextBoxes
        /// </summary>
        /// <param name="boxes">The TextBoxes to determine the maximal BoxHeight for</param>
        /// <returns>The maximal BoxHeight of the given TextBoxes</returns>
        private double GetTextBoxesMaxHeight(params TextBox[] boxes)
        {
            if (boxes != null)
            {
                return boxes.Max(box => box.BoxHeight);
            }

            return 0;
        }

        #region PDF Draw Table Helper Methods

        /// <summary>
        /// Gets the TextBoxes that are needed to show in one row of the table as an array.
        /// The text will be displayed in the _calibriNormal font.
        /// An ArgumentException will be thrown when the length of the textStrings array is larger than there are columns in the table.
        /// </summary>
        /// <param name="textStrings">The text that should appear in the TextBoxes</param>
        /// <returns>The TextBoxes for one row as an array</returns>
        private TextBox[] GetColumnTextBoxesWithText(params string[] textStrings)
        {
            return GetColumnTextBoxesWithText(_calibriNormal, textStrings);
        }

        /// <summary>
        /// Gets the TextBoxes that are needed to show in one row of the table as an array.
        /// An ArgumentException will be thrown when the length of the textStrings array is larger than there are columns in the table.
        /// </summary>
        /// <param name="font">The font the text should be printed in</param>
        /// <param name="textStrings">The text that should appear in the TextBoxes</param>
        /// <returns>The TextBoxes for one row as an array</returns>
        private TextBox[] GetColumnTextBoxesWithText(PdfFont font, params string[] textStrings)
        {
            if (textStrings.Length > TableColWidths.Length)
            {
                throw new ArgumentException("There can't be more TextBoxes with text than there are columns defined in TableColWidths", "boxes");
            }

            List<TextBox> boxes = new List<TextBox>();

            for (int i = 0; i < textStrings.Length; i++)
            {
                string text = textStrings[i] ?? string.Empty;

                double boxWidth = Math.Min(font.TextWidth(CalibriTableFontSize, text), TableColWidths[i] - TableLineWidth - TableCellMargin * 2);
                TextBox box = new TextBox(boxWidth);

                box.AddText(font, CalibriTableFontSize, text);
                box.Terminate();

                boxes.Add(box);
            }

            return boxes.ToArray();
        }

        /// <summary>
        /// Draws a table row at the given position with the given TextBoxes.
        /// This method can be used for the header as well for the details.
        /// </summary>
        /// <param name="posYstart">The position on the page of the top of the row</param>
        /// <param name="boxes">The boxes that need to be drawn in the row</param>
        /// <returns>A value of the DrawMethodReturnOptions enum</returns>
        private DrawMethodReturnOptions DrawTableRow(ref double posYstart, params TextBox[] boxes)
        {
            return DrawTableRow(ref posYstart, Color.White, boxes);
        }

        /// <summary>
        /// Draws a table row at the given position with the given TextBoxes.
        /// This method can be used for the header as well for the details.
        /// </summary>
        /// <param name="posYstart">The position on the page of the top of the row</param>
        /// <param name="boxes">The boxes that need to be drawn in the row</param>
        /// <param name="rowBackGroundColor">The background color the row should have</param>
        /// <returns>A value of the DrawMethodReturnOptions enum</returns>
        private DrawMethodReturnOptions DrawTableRow(ref double posYstart, Color rowBackGroundColor, params TextBox[] boxes)
        {
            _contents.SaveGraphicsState();

            double boxColMaxHeight = GetTextBoxesMaxHeight(boxes);

            // The height of the inner rectangle + half the width of the bottom and the top line together
            double rowHeight = boxColMaxHeight + TableCellMargin * 2 + TableLineWidth;

            double posYbottomRowBox = posYstart - TableLineWidth - TableCellMargin - boxColMaxHeight;
            // The bottom of the outer rectangle
            double posYbottomRowRectangle = posYstart - rowHeight - TableLineWidth;

            if (posYbottomRowRectangle < MarginY)
            {
                _contents.RestoreGraphicsState();

                return DrawMethodReturnOptions.NewPageRequired;
            }

            _contents.SetLineWidth(TableLineWidth);
            _contents.SetColorNonStroking(rowBackGroundColor);
            DrawRectanglesForRow(posYbottomRowRectangle, rowHeight);

            _contents.RestoreGraphicsState();
            _contents.SaveGraphicsState();

            DrawTextBoxesForRow(posYbottomRowBox, boxColMaxHeight, boxes);

            _contents.RestoreGraphicsState();

            // '+ tableLineWidth' so the lines of the rectangles overlap
            posYstart = posYbottomRowRectangle + TableLineWidth;

            return DrawMethodReturnOptions.Success;
        }

        /// <summary>
        /// Draws a table row with custom columns at the given position with the given TextBoxes.
        /// </summary>
        /// <param name="posYstart">The position on the page of the top of the row</param>
        /// <param name="customBoxes">The boxes that need to be drawn in the row, with their StartColumnIndex and EndColumnIndex</param>
        /// <returns>A value of the DrawMethodReturnOptions enum</returns>
        private DrawMethodReturnOptions DrawTableCustomRow(ref double posYstart, params TableCustomColumnText[] customBoxes)
        {
            return DrawTableCustomRow(ref posYstart, Color.White, customBoxes);
        }

        /// <summary>
        /// Draws a table row with custom columns at the given position with the given TextBoxes.
        /// </summary>
        /// <param name="posYstart">The position on the page of the top of the row</param>
        /// <param name="rowBackGroundColor">The background color the row should have</param>
        /// <param name="customBoxes">The boxes that need to be drawn in the row, with their StartColumnIndex and EndColumnIndex</param>
        /// <returns>A value of the DrawMethodReturnOptions enum</returns>
        private DrawMethodReturnOptions DrawTableCustomRow(ref double posYstart, Color rowBackGroundColor, params TableCustomColumnText[] customBoxes)
        {
            _contents.SaveGraphicsState();

            double boxColMaxHeight = GetTextBoxesMaxHeight(customBoxes.Select(customBox => customBox.Box).ToArray());

            // The height of the inner rectangle + half the width of the bottom and the top line together
            double rowHeight = boxColMaxHeight + TableCellMargin * 2 + TableLineWidth;

            double posYbottomRowBox = posYstart - TableLineWidth - TableCellMargin - boxColMaxHeight;
            // The bottom of the outer rectangle
            double posYbottomRowRectangle = posYstart - rowHeight - TableLineWidth;

            if (posYbottomRowRectangle < MarginY)
            {
                _contents.RestoreGraphicsState();

                return DrawMethodReturnOptions.NewPageRequired;
            }

            _contents.SetLineWidth(TableLineWidth);
            _contents.SetColorNonStroking(rowBackGroundColor);
            DrawCustomRectanglesForRow(posYbottomRowRectangle, rowHeight, customBoxes);

            _contents.RestoreGraphicsState();
            _contents.SaveGraphicsState();

            DrawCustomTextBoxesForRow(posYbottomRowBox, boxColMaxHeight, customBoxes);

            _contents.RestoreGraphicsState();

            // '+ tableLineWidth' so the lines of the rectangles overlap
            posYstart = posYbottomRowRectangle + TableLineWidth;

            return DrawMethodReturnOptions.Success;
        }

        /// <summary>
        /// Draws a table row with custom columns at the given position for the rectangles and with the given TextBoxes.
        /// </summary>
        /// <param name="posYstart">The position on the page of the top of the row</param>
        /// <param name="customRectangles">The objects that contain the positions where the rectangles for that row need to be drawn</param>
        /// <param name="customBoxes">The boxes that need to be drawn in the row, with their StartColumnIndex and EndColumnIndex</param>
        /// <returns>A value of the DrawMethodReturnOptions enum</returns>
        private DrawMethodReturnOptions DrawTableCustomRectanglesAndText(ref double posYstart, TableCustomColumnText[] customRectangles, TableCustomColumnText[] customBoxes)
        {
            return DrawTableCustomRectanglesAndText(ref posYstart, Color.White, customRectangles, customBoxes);
        }

        /// <summary>
        /// Draws a table row with custom columns at the given position for the rectangles and with the given TextBoxes.
        /// </summary>
        /// <param name="posYstart">The position on the page of the top of the row</param>
        /// <param name="rowBackGroundColor">The background color the row should have</param>
        /// <param name="customRectangles">The objects that contain the positions where the rectangles for that row need to be drawn</param>
        /// <param name="customBoxes">The boxes that need to be drawn in the row, with their StartColumnIndex and EndColumnIndex</param>
        /// <returns>A value of the DrawMethodReturnOptions enum</returns>
        private DrawMethodReturnOptions DrawTableCustomRectanglesAndText(ref double posYstart, Color rowBackGroundColor, TableCustomColumnText[] customRectangles, TableCustomColumnText[] customBoxes)
        {
            _contents.SaveGraphicsState();

            double boxColMaxHeight = GetTextBoxesMaxHeight(customBoxes.Select(customBox => customBox.Box).ToArray());

            // The height of the inner rectangle + half the width of the bottom and the top line together
            double rowHeight = boxColMaxHeight + TableCellMargin * 2 + TableLineWidth;

            double posYbottomRowBox = posYstart - TableLineWidth - TableCellMargin - boxColMaxHeight;
            // The bottom of the outer rectangle
            double posYbottomRowRectangle = posYstart - rowHeight - TableLineWidth;

            if (posYbottomRowRectangle < MarginY)
            {
                _contents.RestoreGraphicsState();

                return DrawMethodReturnOptions.NewPageRequired;
            }

            _contents.SetLineWidth(TableLineWidth);
            _contents.SetColorNonStroking(rowBackGroundColor);
            DrawCustomRectanglesForRow(posYbottomRowRectangle, rowHeight, customRectangles);

            _contents.RestoreGraphicsState();
            _contents.SaveGraphicsState();

            DrawCustomTextBoxesForRow(posYbottomRowBox, boxColMaxHeight, customBoxes);

            _contents.RestoreGraphicsState();

            // '+ tableLineWidth' so the lines of the rectangles overlap
            posYstart = posYbottomRowRectangle + TableLineWidth;

            return DrawMethodReturnOptions.Success;
        }

        /// <summary>
        /// Draws the rectangles for one row of the table at the specified place and with the specified height
        /// </summary>
        /// <param name="posYbottomOuterRectangle">The position of the page of the bottom of the outer rectangle</param>
        /// <param name="rowHeight">The height of the row (also the height of the rectangle)</param>
        private void DrawRectanglesForRow(double posYbottomOuterRectangle, double rowHeight)
        {
            double colWidths = 0;

            for (int i = 0; i < TableColWidths.Length; i++)
            {
                _contents.DrawRectangle(MarginX + TableLineWidth / 2 + colWidths, posYbottomOuterRectangle + TableLineWidth / 2, TableColWidths[i], rowHeight, PaintOp.CloseFillStroke);
                colWidths += TableColWidths[i];
            }
        }

        /// <summary>
        /// Draws the rectangles for one row of the table at the specified place and with the specified height
        /// and with their custom widths, specified by the StartColumnIndex and EndColumnIndex
        /// </summary>
        /// <param name="posYbottomOuterRectangle">The position of the page of the bottom of the outer rectangle</param>
        /// <param name="rowHeight">The height of the row (also the height of the rectangle)</param>
        /// <param name="customWidths">The TableCustomColumnText objects that contain the StartColumnIndex and EndColumnIndex, to determine the width of the rectangles</param>
        private void DrawCustomRectanglesForRow(double posYbottomOuterRectangle, double rowHeight, params TableCustomColumnText[] customWidths)
        {
            foreach (TableCustomColumnText customWidth in customWidths)
            {
                _contents.DrawRectangle(MarginX + TableLineWidth / 2 + TableColWidths.Where((colWidth, index) => index < customWidth.StartColumnIndex).Sum(),
                    posYbottomOuterRectangle + TableLineWidth / 2, TableColWidths.Where((colWidth, index) => index >= customWidth.StartColumnIndex && index <= customWidth.EndColumnIndex).Sum(),
                    rowHeight, PaintOp.CloseFillStroke);
            }
        }

        /// <summary>
        /// Draws the given TextBoxes in one row at the specified position on the page and with the given MaxHeight.
        /// When the height of the TextBox is smaller than the MaxHeight, the TextBox will appear in the middle of the row.
        /// An ArgumentException will be thrown when the length of the TextBox array is larger than there are columns in the table.
        /// </summary>
        /// <param name="posYbottomBoxRow">The position on the page of the bottom of the (largest) TextBox</param>
        /// <param name="boxMaxHeight">The maximal height of the TextBoxes</param>
        /// <param name="boxes">The TextBoxes to be drawn</param>
        private void DrawTextBoxesForRow(double posYbottomBoxRow, double boxMaxHeight, params TextBox[] boxes)
        {
            if (boxes.Length > TableColWidths.Length)
            {
                throw new ArgumentException("There can't be more TextBoxes than there are columns defined in TableColWidths", "boxes");
            }

            double colWidths = 0;

            for (int i = 0; i < boxes.Length; i++)
            {
                TextBox box = boxes[i];
                double posYtopBox = posYbottomBoxRow + boxMaxHeight / 2 + box.BoxHeight / 2;

                _contents.DrawText(MarginX + colWidths + TableLineWidth + TableCellMargin, ref posYtopBox, posYbottomBoxRow + boxMaxHeight / 2 - box.BoxHeight / 2, 0, box);

                colWidths += TableColWidths[i];
            }
        }

        /// <summary>
        /// Draws the given TextBoxes in one row at the specified position on the page and with the given MaxHeight.
        /// When the height of the TextBox is smaller than the MaxHeight, the TextBox will appear in the middle of the row.
        /// The width of the TextBoxes will be determined by their corresponding StartColumnIndex and EndColumnIndex.
        /// </summary>
        /// <param name="posYbottomBoxRow">The position on the page of the bottom of the (largest) TextBox</param>
        /// <param name="boxMaxHeight">The maximal height of the TextBoxes</param>
        /// <param name="customBoxes">The custom TextBoxes to be drawn</param>
        private void DrawCustomTextBoxesForRow(double posYbottomBoxRow, double boxMaxHeight, params TableCustomColumnText[] customBoxes)
        {
            foreach (TableCustomColumnText customBox in customBoxes)
            {
                double posYtopCustomBox = posYbottomBoxRow + boxMaxHeight / 2 + customBox.Box.BoxHeight / 2;

                _contents.DrawText(MarginX + TableColWidths.Where((colWidth, index) => index < customBox.StartColumnIndex).Sum() + TableLineWidth + TableCellMargin,
                    ref posYtopCustomBox, posYbottomBoxRow + boxMaxHeight / 2 - customBox.Box.BoxHeight / 2, 0, customBox.Box);
            }
        }

        #endregion

        #endregion

        #endregion

        #region Nested Types

        #region PDF Draw Table Helper Types

        /// <summary>
        /// This class can be used when you want to print text in a cell that should have the width of multiple columns together.
        /// </summary>
        private class TableCustomColumnText
        {
            private int _startColumnIndex;
            private int _endColumnIndex;

            public string Text { get; set; }

            public static IEnumerable<double> TableColWidths { get; set; }

            public int StartColumnIndex
            {
                get { return _startColumnIndex; }
                set
                {
                    if (value < 0 || value >= TableColWidths.Count())
                    {
                        throw new ArgumentException("The start column index may not be larger than the specified maximal column index or smaller than 0.");
                    }

                    _startColumnIndex = value;
                }
            }

            public int EndColumnIndex
            {
                get { return _endColumnIndex; }
                set
                {
                    if (value < 0 || value >= TableColWidths.Count())
                    {
                        throw new ArgumentException("The end column index may not be larger than the specified maximal column index or smaller than 0.");
                    }

                    _endColumnIndex = value;
                }
            }

            public PdfFont Font { get; set; }

            public TextBox Box
            {
                get
                {
                    string text = Text ?? string.Empty;

                    double boxWidth = Math.Min(Font.TextWidth(CalibriTableFontSize, text),
                        TableColWidths.Where((colWidth, index) => index >= StartColumnIndex && index <= EndColumnIndex).Sum() - TableLineWidth - TableCellMargin * 2);
                    TextBox box = new TextBox(boxWidth);

                    box.AddText(Font, CalibriTableFontSize, text);
                    box.Terminate();

                    return box;
                }
            }

            /// <summary>
            /// A constructor for the TableCustomColumnText class.
            /// </summary>     
            public TableCustomColumnText()
            {

            }

            /// <summary>
            /// A constructor for the TableCustomColumnText class.
            /// The font to print the text in is required (when printing text).
            /// </summary>
            /// <param name="font">The font to print the text in</param>           
            public TableCustomColumnText(PdfFont font)
            {
                Font = font;
            }
        }

        #endregion

        #endregion
    }
}