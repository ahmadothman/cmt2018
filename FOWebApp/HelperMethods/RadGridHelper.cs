﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using Telerik.Web.UI;

namespace FOWebApp.HelperMethods
{
    /// <summary>
    /// A class that provides helper methods for the Telerik RadGrid
    /// </summary>
    public class RadGridHelper
    {
        private readonly string _sessionKeyStringExpanded;

        public HttpSessionState SessionState { get; set; }
        public string SessionKeyString { get; set; }

        protected string SessionKeyStringExpanded
        {
            get
            {
                return _sessionKeyStringExpanded;
            }
        }

        /// <summary>
        /// The constructor of the RadGridHelper class.
        /// A key for the session are needed
        /// </summary>
        /// <param name="sessionKeyString">The key to use for storing the variables in the HttpSessionState</param>
        public RadGridHelper(string sessionKeyString)
        {
            if (string.IsNullOrWhiteSpace(sessionKeyString))
            {
                throw new ArgumentNullException("sessionKeyString", "The sessionKeyString must contain a value");
            }

            SessionState = HttpContext.Current.Session;
            SessionKeyString = sessionKeyString;

            _sessionKeyStringExpanded = SessionKeyString + "_ExpandedIndexes";
        }

        /// <summary>
        /// Saves the expanded state of the GridItem in the Session
        /// </summary>
        /// <param name="itemIndex">
        /// The hierarchical index of the GridItem to save the expanded state for
        /// This parameter can't be null.
        /// </param>
        public void SaveExpandedState(string itemIndex)
        {
            if (itemIndex == null)
            {
                throw new ArgumentNullException("itemIndex", "The itemIndex parameter can't be null.");
            }

            IEnumerable<string> expandedIndexes = SessionState[SessionKeyStringExpanded] as IEnumerable<string>;

            if (expandedIndexes == null)
            {
                SessionState[SessionKeyStringExpanded] = new List<string>()
                {
                    itemIndex
                };
            }
            else
            {
                List<string> expandedIndexesList = expandedIndexes.ToList();
                expandedIndexesList.Add(itemIndex);
                expandedIndexesList.Sort();

                SessionState[SessionKeyStringExpanded] = expandedIndexesList;
            }
        }

        /// <summary>
        /// Clears the expanded state of the GridItem in the Session
        /// </summary>
        /// <param name="itemIndex">
        /// The hierarchical index of the GridItem to clear the expanded state for
        /// This parameter can't be null.
        /// </param>
        public void ClearExpandedState(string itemIndex)
        {
            if (itemIndex == null)
            {
                throw new ArgumentNullException("itemIndex", "The itemIndex parameter can't be null.");
            }

            IEnumerable<string> expandedIndexes = SessionState[SessionKeyStringExpanded] as IEnumerable<string>;

            if (expandedIndexes != null)
            {
                List<string> expandedIndexesList = expandedIndexes.ToList();

                if (expandedIndexesList.Contains(itemIndex))
                {
                    expandedIndexesList.Remove(itemIndex);

                    SessionState[SessionKeyStringExpanded] = expandedIndexesList;
                }
            }
        }

        /// <summary>
        /// Loads the expanded state of the grid from the Session
        /// </summary>
        /// <param name="grid">
        /// The grid to load the expanded state for.
        /// This parameter can't be null.
        /// </param>
        public void LoadExpandedState(RadGrid grid)
        {
            if (grid == null)
            {
                throw new ArgumentNullException("grid", "The grid parameter can't be null.");
            }

            IEnumerable<string> expandedIndexes = SessionState[SessionKeyStringExpanded] as IEnumerable<string>;

            if (expandedIndexes != null)
            {
                foreach (string index in expandedIndexes)
                {
                    grid.Items[index].Expanded = true;
                }
            }
        }
    }
}