﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Text;
using System.Security.Cryptography;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOMonitorControlWSRef;

namespace FOWebApp.HelperMethods
{
    public static class TerminalDetailsHelper
    {
        /// <summary>
        /// When defined, this method returns the company name and the id of a given isp as string
        /// </summary>
        /// <param name="isp">The internet service provider for which the name and id should be returned</param>
        /// <returns>The company name and the id of the isp as a string</returns>
        public static string IspNameAndIdToString(Isp isp)
        {
            if (isp != null)
            {
                return string.Format("{0}, ID: {1}", isp.CompanyName, isp.Id);
            }
            else
            {
                return "Unknown";
            }
        }

        /// <summary>
        /// Returns the appropriate color according to the given operational status
        /// </summary>
        /// <param name="status">The operational status of which the color should be determined</param>
        /// <returns>The color that the operational status should have</returns>
        public static Color OperationalStatusColorValidator(string status)
        {
            if (status == "Operational")
            {
                return Color.Green;
            }
            else
            {
                return Color.Red;
            }
        }

        /// <summary>
        /// Returns the appropriate color according to the given administrative status
        /// </summary>
        /// <param name="status">The administrative status of which the color should be determined</param>
        /// <returns>The color that the operational status should have</returns>
        public static Color AdministrativeStatusColorValidator(string status)
        {
            if (status == "Locked")
            {
                return Color.Red;
            }
            else
            {
                return Color.Green;
            }
        }

        /// <summary>
        /// Calculates the volume allocation (in GB) of a terminal based on the TerminalInfo object
        /// </summary>
        /// <param name="terminalInfo">The TerminalInfo object of the terminal for which the volume allocation should be calculated</param>
        /// <param name="forward">
        /// True when the forward volume needs to be calculated, false when the return volume needs to be calculated.
        /// The default value is true.
        /// </param>
        /// <returns>The calculated volume allocation in GB</returns>
        public static decimal CalculateVolumeAllocation(TerminalInfo terminalInfo, bool forward = true)
        {
            if (forward)
            {
                decimal fwdFupThresholdBytes = terminalInfo.FUPThreshold * (decimal)1000000000.0 + decimal.Parse(terminalInfo.Additional);
                return fwdFupThresholdBytes / (decimal)1000000000.0;
            }
            else
            {
                decimal rtnFupThresholdBytes = terminalInfo.RTNFUPThreshold * (decimal)1000000000.0 + decimal.Parse(terminalInfo.AdditionalReturn);
                return rtnFupThresholdBytes / (decimal)1000000000.0;
            }
        }

        /// <summary>
        /// Calculates the volume consumption of a terminal in GB
        /// </summary>
        /// <param name="terminalInfo">The TerminalInfo object of the terminal for which the volume consumption should be calculated</param>
        /// <param name="forward">
        /// True when the forward volume needs to be represented, false when the return volume needs to be represented.
        /// The default value is true.
        /// </param>
        /// <returns>The calculated volume consumption in GB.</returns>
        public static decimal CalculateVolumeConsumption(TerminalInfo terminalInfo, bool forward = true)
        {
            if (forward)
            {
                return (long.Parse(terminalInfo.Consumed) - long.Parse(terminalInfo.FreeZone)) / (decimal)1000000000.0;
            }
            else
            {
                return (long.Parse(terminalInfo.ConsumedReturn) - long.Parse(terminalInfo.ConsumedFreeZone)) / (decimal)1000000000.0;
            }
        }

        /// <summary>
        /// Returns the CNO value formatted as a string
        /// </summary>
        /// <param name="rtnValue">The value that should be formatted</param>
        /// <param name="culture">The culture that specifies how the value should be formatted</param>
        /// <param name="ispId">The ISP identifier, which is used to determine the tooltip and the color</param>
        /// <param name="foreColor">The ForeColor of the label containing the CNO value.</param>
        /// <param name="tooltip">The corresponding tooltip of the label containing the CNO value.</param>
        /// <returns>The CNO value, formatted as a string</returns>
        public static string GetCnoValueToString(string rtnValue, double dRRTN, CultureInfo culture, int ispId, out Color foreColor, out string tooltip)
        {
            double rtnNum;

            if (double.TryParse(rtnValue, NumberStyles.Float, culture, out rtnNum))
            {
                foreColor = CnoColorValidator(rtnNum, dRRTN, ispId, out tooltip);
                return string.Format("{0:#.0} ", rtnNum);
            }

            foreColor = Color.Red;
            tooltip = "Could not obtain the RTN value from the Hub.";
            return string.Format("{0:#.0} ", -1.0);
        }

        /// <summary>
        /// Determines the color of the CNO pointing label depending on the 
        /// Return number value and the ISP.
        /// </summary>
        /// <remarks>
        /// The return value levels which determine the label color depend on the
        /// ISP.
        /// </remarks>
        /// <param name="rtnNum">The return number value</param>
        /// <param name="ispId">The ISP identifier</param>
        /// <param name="tooltip">The corresponding tooltip to the ISP</param>
        /// <returns>A Color value depending on the given rtnNum value and isp</returns>
        public static Color CnoColorValidator(double rtnNum, int ispId, out string tooltip)
        {
            Color labelColor;

            //if (ispId == 112)
            //{
            //    if (rtnNum < 55.2)
            //    {
            //        labelColor = Color.Red;
            //    }
            //    else if (rtnNum >= 55.2 && rtnNum < 58.4)
            //    {
            //        labelColor = Color.DarkOrange;
            //    }
            //    else
            //    {
            //        labelColor = Color.DarkGreen;
            //    }

            //    tooltip = "Above 58.4 dBHz, the terminal uses the most efficient return pool. " +
            //        "Between 55.2 dBHz and 58.4 dBHz, the terminal must use a less efficient return pool. " +
            //        "Below 55.2 dBHz the signal is too weak.";
            //}
            //else if (ispId == 412)
            //{
            if (ispId > 700 && ispId < 800)
            {
                if (rtnNum < 62)
                {
                    labelColor = Color.Red;
                }
                else if (rtnNum >= 62 && rtnNum < 66)
                {
                    labelColor = Color.DarkOrange;
                }
                else
                {
                    labelColor = Color.DarkGreen;
                }

                tooltip = "Above 66 dBHz, the terminal uses the most efficient return pool. " +
                "Between 62 dBHz and 66 dBHz, the terminal must use a less efficient return pool. " +
                "Below 62 dBHz the signal is too weak.";
            }
            else if (ispId > 500 && ispId < 600)
            {
                if (rtnNum < 10)
                {
                    labelColor = Color.Red;
                }
                else if (rtnNum >= 10 && rtnNum < 12)
                {
                    labelColor = Color.DarkOrange;
                }
                else
                {
                    labelColor = Color.DarkGreen;
                }

                tooltip = "Above 12 dBHz, the terminal uses the most efficient return pool. " +
                "Between 10 dBHz and 12 dBHz, the terminal must use a less efficient return pool. " +
                "Below 10 dBHz the signal is too weak.";
            }
            else
            {      
            if (rtnNum < 52.7)
            {
                labelColor = Color.Red;
            }
            else if (rtnNum >= 52.7 && rtnNum < 55.8)
            {
                labelColor = Color.DarkOrange;
            }
            else
            {
                labelColor = Color.DarkGreen;
            }

            tooltip = "Above 55.8 dBHz, the terminal uses the most efficient return pool. " +
                "Between 52.7 dBHz and 55.8 dBHz, the terminal must use a less efficient return pool. " +
                "Below 52.7 dBHz the signal is too weak.";
            }
            //else // General case
            //{
            //    if (rtnNum < 55.2)
            //    {
            //        labelColor = Color.Red;
            //    }
            //    else if (rtnNum >= 55.2 && rtnNum < 58.4)
            //    {
            //        labelColor = Color.DarkOrange;
            //    }
            //    else
            //    {
            //        labelColor = Color.DarkGreen;
            //    }

            //    tooltip = string.Empty;
            //}

            return labelColor;
        }

        /// <summary>
        /// Adds colour coding based on CNo values and return SLA DataRate DRRTN
        /// </summary>
        /// <param name="rtnNum">The Return number value</param>
        /// <param name="dRRTN">The return SLA DateRate</param>
        /// <param name="ispId">ISP Identifier</param>
        /// <param name="tooltip">Tooltip</param>
        /// <returns>Colour conding depending on CNo, return SLA DataRate and IspId</returns>
        public static Color CnoColorValidator(double rtnNum, double dRRTN, int ispId, out string tooltip)
        {
            Color labelColor;
            if (ispId > 700 && ispId < 800)
            {
                if (rtnNum < 62)
                {
                    labelColor = Color.Red;
                }
                else if (rtnNum >= 62 && rtnNum < 66)
                {
                    labelColor = Color.DarkOrange;
                }
                else
                {
                    labelColor = Color.DarkGreen;
                }

                tooltip = "Above 66 dBHz, the terminal uses the most efficient return pool. " +
                "Between 62 dBHz and 66 dBHz, the terminal must use a less efficient return pool. " +
                "Below 62 dBHz the signal is too weak.";
            }

           else
           {
                if (dRRTN <= 256)
                {
                    if (rtnNum < 52.71)
                    {
                        labelColor = Color.Red;
                    }
                    else if (rtnNum >= 52.71 && rtnNum <= 59.63)
                    {
                        labelColor = Color.DarkOrange;
                    }
                    else
                    {
                        labelColor = Color.DarkGreen;
                    }
                    tooltip = "Above 59.63 dBHz, the terminal uses the most efficient return pool. " +
                                    "Between 52.71 dBHz and 59.63 dBHz, the terminal must use a less efficient return pool. " +
                                    "Below 52.71 dBHz the signal is too weak.";
                }
                else
                {
                    if (rtnNum < 58.73)
                    {
                        labelColor = Color.Red;
                    }
                    else if (rtnNum >= 58.73 && rtnNum < 61.74)
                    {
                        labelColor = Color.DarkOrange;
                    }
                    else
                    {
                        labelColor = Color.DarkGreen;
                    }
                    tooltip = "Above 61.74 dBHz, the terminal uses the most efficient return pool. " +
                                    "Between 58.73 dBHz and 61.74 dBHz, the terminal must use a less efficient return pool. " +
                                    "Below 58.73 dBHz the signal is too weak.";
                }
            }  
            return labelColor;
        }

        /// <summary>
        /// Returns the CNO value formatted as a string
        /// </summary>
        /// <param name="fwdValue">The value that should be formatted</param>
        /// <param name="culture">The culture that specifies how the value should be formatted</param>
        /// <param name="ispId">The ISP identifier, which is used to determine the tooltip and the color</param>
        /// <param name="foreColor">The ForeColor of the label containing the CNO value.</param>
        /// <param name="tooltip">The corresponding tooltip of the label containing the CNO value.</param>
        /// <returns>The CNO value, formatted as a string</returns>
        public static string GetEsNoValueToString(string fwdValue, CultureInfo culture, int ispId, out Color foreColor, out string tooltip)
        {
            double fwdNum;

            if (double.TryParse(fwdValue, NumberStyles.Float, culture, out fwdNum))
            {
                foreColor = EsNoColorValidator(fwdNum, ispId, out tooltip);
                return string.Format("{0:#.0} ", fwdNum);
            }

            foreColor = Color.Red;
            tooltip = "Could not obtain the FWD value from the Hub.";
            return string.Format("{0:#.0} ", -1.0);
        }

        /// <summary>
        /// Determines the color of the CNO pointing label depending on the 
        /// Return number value and the ISP.
        /// </summary>
        /// <remarks>
        /// The return value levels which determine the label color depend on the
        /// ISP.
        /// </remarks>
        /// <param name="fwdNum">The return number value</param>
        /// <param name="ispId">The ISP identifier</param>
        /// <param name="tooltip">The corresponding tooltip to the ISP</param>
        /// <returns>A Color value depending on the given rtnNum value and isp</returns>
        public static Color EsNoColorValidator(double fwdNum, int ispId, out string tooltip)
        {
            Color labelColor;

            if (fwdNum < 8)
            {
                labelColor = Color.Red;
            }
            else if (fwdNum >= 8 && fwdNum < 11.9)
            {
                labelColor = Color.DarkOrange;
            }
            else
            {
                labelColor = Color.DarkGreen;
            }

            tooltip = "Above 12 dBHz, the terminal uses the most efficient forward pool. " +
                "Between 8 dBHz and 11.9 dBHz, the terminal must use a less efficient forward pool. " +
                "Below 8 dBHz the signal is too weak.";
            
            return labelColor;
        }

        public static string GetExpiryDateToString(DateTime? expiryDate, out Color foreColor)
        {
            if (expiryDate != null)
            {
                if (expiryDate < DateTime.Now)
                {
                    foreColor = Color.Red;
                }
                else
                {
                    foreColor = Color.Green;
                }

                return ((DateTime)expiryDate).ToString("dd/MM/yyyy HH:mm");
            }
            else
            {
                foreColor = Color.Orange;
                return "Not set";
            }
        }

    }
}