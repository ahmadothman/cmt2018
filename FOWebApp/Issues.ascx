﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Issues.ascx.cs" Inherits="FOWebApp.Issues" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script type="text/javascript">
    function RowSelected(sender, eventArgs) {
        var grid = sender;
        var masterDataView = grid.get_masterTableView();
        var rowNum = eventArgs.get_itemIndexHierarchical();
        var row = masterDataView.get_dataItems()[rowNum];
        var key = masterDataView.getCellByColumnUniqueName(row, "Key").innerHTML;

        //Initialize and show the issue details window
        var oWnd = radopen("Distributors/IssueDetailsPage.aspx?key=" + key, "RadWindowIssueDetails");
    }
</script>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="RadGridIssues">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGridIssues" />
                <telerik:AjaxUpdatedControl ControlID="RadWindowManager1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Metro"/>
<telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" Height="200px" 
    Width="467px" HorizontalAlign="NotSet" 
    LoadingPanelID="RadAjaxLoadingPanel1">
<telerik:RadGrid ID="RadGridIssues" runat="server" AllowSorting="True"
    AutoGenerateColumns="False" CellSpacing="0" GridLines="None" Skin="Metro"
    ShowStatusBar="True" Width="992px" AllowFilteringByColumn="False" 
        onitemdatabound="RadGridIssues_ItemDataBound" 
        onpagesizechanged="RadGridIssues_PageSizeChanged" 
        PageSize="50" onitemcommand="RadGridIssues_ItemCommand">
        <GroupingSettings CaseSensitive="false" /> 
    <ClientSettings>
        <Selecting AllowRowSelect="True" />
        <ClientEvents OnRowSelected="RowSelected" />
    </ClientSettings>
    <MasterTableView CommandItemDisplay="Top" CommandItemSettings-ShowExportToExcelButton="False" 
        CommandItemSettings-ShowExportToCsvButton="False" RowIndicatorColumn-Visible="True" RowIndicatorColumn-Resizable="True" RowIndicatorColumn-AndCurrentFilterFunction="Contains" CommandItemSettings-ShowAddNewRecordButton="False" ShowFooter="False">
        <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
        </RowIndicatorColumn>
        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
        </ExpandCollapseColumn>
        <Columns>
            <telerik:GridBoundColumn DataField="Key" 
                FilterControlAltText="Filter KeyColumn column" HeaderText="Issue key" 
                UniqueName="Key" ShowFilterIcon="False" AutoPostBackOnFilter="True">
                <ItemStyle Wrap="True" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Summary" FilterControlAltText="Filter SummaryColumn column"
                HeaderText="Issue summary" ReadOnly="True" Resizable="False" 
                UniqueName="Summary" CurrentFilterFunction="Contains" 
                ShowFilterIcon="False" AutoPostBackOnFilter="True">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="MACAddress" FilterControlAltText="Filter MACAddressColumn column"
                HeaderText="MAC address" ReadOnly="True" Resizable="False" 
                UniqueName="MACAddress" CurrentFilterFunction="Contains" 
                ShowFilterIcon="False" AutoPostBackOnFilter="True">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Submitted" FilterControlAltText="Filter IssueDateTimeColumn column"
                HeaderText="Submitted on" ReadOnly="True" Resizable="False" 
                UniqueName="Submitted" CurrentFilterFunction="Contains" DataFormatString="{0:dd/MM/yyyy}" DataType="System.DateTime"
                ShowFilterIcon="False" AutoPostBackOnFilter="True">
            </telerik:GridBoundColumn>
             <telerik:GridBoundColumn DataField="LastUpdate" FilterControlAltText="Filter IssueLastUpdateColumn column"
                HeaderText="Last update" ReadOnly="True" Resizable="False" 
                UniqueName="LastUpdate" CurrentFilterFunction="Contains" DataFormatString="{0:dd/MM/yyyy}" DataType="System.DateTime"
                ShowFilterIcon="False" AutoPostBackOnFilter="True">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Status" FilterControlAltText="Filter StatusColumn column"
                HeaderText="Status" ReadOnly="True" UniqueName="Status" 
                ShowFilterIcon="False" AutoPostBackOnFilter="True">
            </telerik:GridBoundColumn>
        </Columns>
        <EditFormSettings>
            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
            </EditColumn>
        </EditFormSettings>
    </MasterTableView>
    <FilterMenu EnableImageSprites="False">
    </FilterMenu>
    <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default">
    </HeaderContextMenu>
</telerik:RadGrid>
<p>
    Number of issues in this list:
    <asp:Label ID="LabelNumRows" runat="server" Font-Bold="True"></asp:Label>
</p>
<telerik:RadWindowManager ID="RadWindowManager1" runat="server" Skin="Metro" 
        DestroyOnClose="True">
    <Windows>
        <telerik:RadWindow ID="RadWindowIssueDetails" runat="server" 
            NavigateUrl="Distributors/IssueDetailsPage.aspx" Animation="Fade" Opacity="100" Skin="Metro" 
            Title="Issue Details" AutoSize="False" Width="1000px" Height="700px" KeepInScreenBounds="True" EnableShadow="True" EnableAriaSupport="True" VisibleStatusbar="True" Modal="False" DestroyOnClose="False">
        </telerik:RadWindow>
    </Windows>
</telerik:RadWindowManager>
</telerik:RadAjaxPanel>