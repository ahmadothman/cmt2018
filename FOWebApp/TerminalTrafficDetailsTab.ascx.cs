﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOConnectedDevicesControlWSRef;
using SnmpSharpNet;

namespace FOWebApp
{
    /// <summary>
    /// Allows the user to select traffic details for a specific application (traffic protocol)
    /// </summary>
    public partial class TerminalTrafficDetailsTab : System.Web.UI.UserControl
    {
        public String MacAddress { get; set; }

        BOAccountingControlWS _boAccountingControl = new BOAccountingControlWS();
        BOConnectedDevicesControlWS boconnectedDevicesControl = new BOConnectedDevicesControlWS();
        ConnectedDevice[] cd;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(MacAddress))
            {
                cd = boconnectedDevicesControl.GetConnectedDevicesForTerminalByType(MacAddress, 3);

                OctetString community = new OctetString("CMT-Access");

                // Define agent parameters class
                AgentParameters param = new AgentParameters(community);
                // Set SNMP version to 1 (or 2)
                param.Version = SnmpVersion.Ver2;
                // Construct the agent address object
                // IpAddress class is easy to use here because
                //  it will try to resolve constructor parameter if it doesn't
                //  parse to an IP address
                IpAddress agent = new IpAddress(cd[0].DeviceId);

                // Construct target
                UdpTarget target = new UdpTarget((System.Net.IPAddress)agent, 161, 2000, 1);
                SnmpV2Packet result = null;
                // Pdu class used for all requests
                Pdu pdu = new Pdu(PduType.Get);
                pdu.VbList.Add("1.3.6.1.2.1.1.5.0"); //sysName
                pdu.VbList.Add("1.3.6.1.2.1.1.1.0"); //OSVersion
                pdu.VbList.Add("1.3.6.1.2.1.1.3.0"); //sysUpTime
                pdu.VbList.Add(".1.3.6.1.4.1.14988.1.1.6.1.0"); //dhcpLeaseCount
                pdu.VbList.Add("1.3.6.1.2.1.1.4.0"); //sysContact
                pdu.VbList.Add("1.3.6.1.2.1.1.6.0"); //DeviceName

                //pdu.VbList.Add(".1.3.6.1.2.1.9999.1.1.6.4.1.8"); //sysDescr

                //pdu.VbList.Add(".1.3.6.1.2.1.2.2.1.6.1"); //sysObjectID


                


                // Make SNMP request
                //string baseOid = ".1.3.6.1.2.1.9999.1.1.6.4.1.7";
                //int clID = 250;
                //do {
                //    clID++;
                //    pdu.VbList.Clear();
                //    pdu.VbList.Add(baseOid +clID);
                //    try {
                //        result = (SnmpV2Packet)target.Request(pdu, param);
                //    }
                //    catch(Exception ex)
                //    {
                //        result = null; 
                //    }
                //} while (result.Pdu.VbList[0].Value.Type != SnmpConstants.SMI_NOSUCHINSTANCE || result==null);


                //int usersCount = clID - 251;



                //pdu.VbList.Clear();
                //    pdu.VbList.Add(".1.3.6.1.2.1.2.2.1.2.1");
                //    pdu.VbList.Add(".1.3.6.1.2.1.2.2.1.6.1");
                //    pdu.VbList.Add(".1.3.6.1.2.1.2.2.1.8.1");
                //    pdu.VbList.Add(".1.3.6.1.2.1.9999.1.1.6.4.1.7");
                try
                    {
                        result = (SnmpV2Packet)target.Request(pdu, param);
                        LabelDeviceName.Text = result.Pdu.VbList[0].Value.ToString();
                        LabelOSverion.Text = result.Pdu.VbList[1].Value.ToString();
                        LabelUpTime.Text = result.Pdu.VbList[2].Value.ToString(); ;
                        LabelUsers.Text = result.Pdu.VbList[3].Value.ToString();
                        LabelContact.Text= result.Pdu.VbList[4].Value.ToString();
                        LabelLocation.Text = result.Pdu.VbList[5].Value.ToString();
                }
                    catch (Exception ex)
                    {
                        LabelDeviceNotConn.Text = "Mikrotik router is not reachable";
                        LabelUsers.Text = "";
                        LabelOSverion.Text = "";
                    }


                

                target.Close();

            }

        }
    }
}