<%@ Page Language="C#" MasterPageFile="~/design/CMTSite.master" ValidateRequest="false"
    AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="_Default" Title="Customer Management Tool" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="art" TagName="DefaultMenu" Src="DefaultMenu.ascx" %>
<%@ Register TagPrefix="art" TagName="DefaultHeader" Src="DefaultHeader.ascx" %>
<%@ Register TagPrefix="art" TagName="DefaultSidebar1" Src="DefaultSidebar1.ascx" %>
<asp:Content ID="PageTitle" ContentPlaceHolderID="TitleContentPlaceHolder" runat="Server">
    SatADSL Customer Management Tool
</asp:Content>
<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeaderContentPlaceHolder" runat="Server" EnableViewState="True" Visible="False">
    <art:DefaultHeader ID="DefaultHeader" runat="server" />
</asp:Content>
<asp:Content ID="SideBar1" ContentPlaceHolderID="Sidebar1ContentPlaceHolder" runat="Server">
    <art:DefaultSidebar1 ID="DefaultSidebar1Content" runat="server" />
</asp:Content>
<asp:Content ID="SheetContent" ContentPlaceHolderID="SheetContentPlaceHolder" runat="Server">
    <script type="text/javascript">
        function onRequestStart(sender, args) {
            //alert("ImageButtonQuery = " + args.get_eventTarget().indexOf("ImageButtonQuery")
            //    + "ImageButtonExcelExport = " + args.get_eventTarget().indexOf("ImageButtonExcelExport")
            //    + " ImageButtonCSVExport = " + args.get_eventTarget().indexOf("ImageButtonCSVExport"));
            if (args.get_eventTarget().indexOf("ImageButtonExcelExport") != -1 ||
                args.get_eventTarget().indexOf("ImageButtonCSVExport") != -1 ||
                args.get_eventTarget().indexOf("ImageButtonQuery") != -1) {
                args.set_enableAjax(false);
            }
        }
    </script>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" ClientEvents-OnRequestStart="onRequestStart"
        DefaultLoadingPanelID="RadAjaxLoadingPanel1">
        <ClientEvents OnRequestStart="onRequestStart" />
    </telerik:RadAjaxManager>
    <asp:Panel ID="MainContentPlaceHolder" runat="server" CssClass="contentPane">
    </asp:Panel>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Metro">
    </telerik:RadAjaxLoadingPanel>
</asp:Content>

