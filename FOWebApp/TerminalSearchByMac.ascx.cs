﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;

namespace FOWebApp
{
    public partial class TerminalSearchByMac : System.Web.UI.UserControl
    {
        const string _HEXBYTE = "<0|1|2|3|4|5|6|7|8|9|a|b|c|d|e|f><0|1|2|3|4|5|6|7|8|9|a|b|c|d|e|f>";


        protected void Page_Load(object sender, EventArgs e)
        {
            RadMaskedTextBoxMacAddress.Mask =
              _HEXBYTE + ":" + _HEXBYTE + ":" + _HEXBYTE + ":" + _HEXBYTE + ":" + _HEXBYTE + ":" +
              _HEXBYTE;
        }

    }
}