﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Web.Security;
using System.Web.UI.DataVisualization.Charting;
using System.IO;
using FOWebApp.net.nimera.cmt.BOMonitorControlWSRef;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;
using net.nimera.supportlibrary;
using FOWebApp.net.nimera.cmt.BOSatDiversityControllerWSRef;

namespace FOWebApp
{
    public partial class TerminalConsumedTab : System.Web.UI.UserControl
    {
        string _macAddress;
        string _associatedMacAddress;

        /// <summary>
        /// MacAddress which must be set before loading the component!
        /// </summary>
        public string MacAddress
        {
            get { return _macAddress; }
            set { _macAddress = value; }
        }

        public string AssociatedMacAddress 
        {
            get {return _associatedMacAddress ; }
            set { _associatedMacAddress = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// Initiates the forward high priority traffic graph
        /// </summary>
        /// <param name="term">Target terminal</param>
        private void FwHighPriorityTrafficGraph(Terminal term, TrafficDataPoint[] trafficDataPoints)
        {
            

            ChartConsumed.Series.Add(new Series("HiPriFVolumeSeries"));
            ChartConsumed.Series["HiPriFVolumeSeries"].BorderWidth = 3;
            ChartConsumed.Series["HiPriFVolumeSeries"].ShadowOffset = 2;
            ChartConsumed.Series["HiPriFVolumeSeries"].XValueType = ChartValueType.Int64;
            ChartConsumed.Series["HiPriFVolumeSeries"].Color = Color.LightGoldenrodYellow;

            List<long> FYValues = new List<long>();
            List<string> FXValues = new List<string>();

            DateTime previousDt = DateTime.MinValue;
            int secondsDelta = 0;
            long dataPointValue = 0;

            
            //Load data into YValues
            for (int i = 0; i < trafficDataPoints.Length; i++)
            {
                dataPointValue = Int64.Parse(trafficDataPoints[i].FwdDataPoint.value);
                DateTime offset = new DateTime(1970, 1, 1, 0, 0, 0);
                long epochTicks = Int64.Parse(trafficDataPoints[i].FwdDataPoint.timestamp);
                DateTime dt = offset.AddMilliseconds(epochTicks);

                //Calculate average
                if (previousDt != DateTime.MinValue)
                {
                    secondsDelta = dt.Subtract(previousDt).Minutes;
                }
                else
                {
                    secondsDelta = 30; //This is an assumption, could not be true
                }

                //Check for a possible divide by 0
                if (secondsDelta != 0)
                {
                    dataPointValue = ((dataPointValue / (secondsDelta * 60)) * 8) / 1000;
                    previousDt = dt;

                    FYValues.Add(dataPointValue);
                    FXValues.Add(this.ConvertDateTime(dt));

                }
            }
            ChartConsumed.Series["HiPriFVolumeSeries"].Points.DataBindXY(FXValues, "Bitrate", FYValues, "kbps");
            ChartConsumed.Series["HiPriFVolumeSeries"].LegendText = "FW High Priority";
            ChartConsumed.Series["HiPriFVolumeSeries"].ChartType = SeriesChartType.Line;
        }

        /// <summary>
        /// Initiates the return high priority traffic graph
        /// </summary>
        /// <param name="term">Target terminal</param>
        private void RtHighPriorityTrafficgraph(Terminal term, TrafficDataPoint[] trafficDataPoints)
        {
            ChartConsumed.Series.Add(new Series("HiPriRVolumeSeries"));
            ChartConsumed.Series["HiPriRVolumeSeries"].BorderWidth = 3;
            ChartConsumed.Series["HiPriRVolumeSeries"].ShadowOffset = 2;
            ChartConsumed.Series["HiPriRVolumeSeries"].XValueType = ChartValueType.Int64;
            ChartConsumed.Series["HiPriRVolumeSeries"].Color = Color.BlueViolet;


            List<long> RYValues = new List<long>();
            List<string> RXValues = new List<string>();

            DateTime previousDt = DateTime.MinValue;
            int secondsDelta = 0;
            long dataPointValue = 0;

                for (int i = 0; i < trafficDataPoints.Length; i++)
                {

                    dataPointValue = Int64.Parse(trafficDataPoints[i].RtnDataPoint.value);

                    long epochTicks = Int64.Parse(trafficDataPoints[i].RtnDataPoint.timestamp);
                    DateTime dt = DateHelper.EpochToDateTime(epochTicks);

                    //Calculate average
                    if (previousDt != DateTime.MinValue)
                    {
                        secondsDelta = dt.Subtract(previousDt).Minutes;
                    }
                    else
                    {
                        secondsDelta = 30; //This is an assumption, could not be true
                    }

                    //Check for a possible divide by 0
                    if (secondsDelta != 0)
                    {
                        dataPointValue = ((dataPointValue / (secondsDelta * 60)) * 8) / 1000;
                        previousDt = dt;

                        RYValues.Add(dataPointValue);
                        RXValues.Add(this.ConvertDateTime(dt));

                    }
                } 
            //}
            ChartConsumed.Series["HiPriRVolumeSeries"].Points.DataBindXY(RXValues, "Bitrate", RYValues, "kbps");
            ChartConsumed.Series["HiPriRVolumeSeries"].LegendText = "RT High Priority";
            ChartConsumed.Series["HiPriRVolumeSeries"].ChartType = SeriesChartType.Line;
        }

        /// <summary>
        /// Initiates the forward best effort traffic graph
        /// </summary>
        /// <param name="term"></param>
        private void FwTrafficGraph(Terminal term, TrafficDataPoint[] trafficDataPoints)
        {
            ChartConsumed.Series.Add(new Series("FVolumeSeries"));
            ChartConsumed.Series["FVolumeSeries"].BorderWidth = 3;
            ChartConsumed.Series["FVolumeSeries"].ShadowOffset = 2;
            ChartConsumed.Series["FVolumeSeries"].XValueType = ChartValueType.Int64;
            ChartConsumed.Series["FVolumeSeries"].Color = Color.Red;

            List<long> FYValues = new List<long>();
            List<string> FXValues = new List<string>();

            DateTime previousDt = DateTime.MinValue;
            int secondsDelta = 0;
            long dataPointValue = 0;

            //Load data into YValues
            for (int i = 0; i < trafficDataPoints.Length; i++)
            {
                dataPointValue = Int64.Parse(trafficDataPoints[i].FwdDataPoint.value);
                DateTime offset = new DateTime(1970, 1, 1, 0, 0, 0);
                long epochTicks = Int64.Parse(trafficDataPoints[i].FwdDataPoint.timestamp);
                DateTime dt = offset.AddMilliseconds(epochTicks);

                //Calculate average
                if (previousDt != DateTime.MinValue)
                {
                    secondsDelta = dt.Subtract(previousDt).Minutes;
                }
                else
                {
                    secondsDelta = 30; //This is an assumption, could not be true
                }

                //Check for a possible divide by 0
                if (secondsDelta != 0)
                {
                    dataPointValue = ((dataPointValue / (secondsDelta * 60)) * 8) / 1000;
                    previousDt = dt;

                    FYValues.Add(dataPointValue);
                    FXValues.Add(this.ConvertDateTime(dt));

                }
            }
            ChartConsumed.Series["FVolumeSeries"].Points.DataBindXY(FXValues, "Bitrate", FYValues, "kbps");
            ChartConsumed.Series["FVolumeSeries"].LegendText = "FW Best Effort";
            ChartConsumed.Series["FVolumeSeries"].ChartType = SeriesChartType.Line;
        }

        /// <summary>
        /// Initiates the return best effort traffic graph
        /// </summary>
        /// <param name="term"></param>
        private void RtTrafficGraph(Terminal term, TrafficDataPoint[] trafficDataPoints)
        {
            ChartConsumed.Series.Add(new Series("RVolumeSeries"));
            ChartConsumed.Series["RVolumeSeries"].BorderWidth = 3;
            ChartConsumed.Series["RVolumeSeries"].ShadowOffset = 2;
            ChartConsumed.Series["RVolumeSeries"].XValueType = ChartValueType.Int64;
            ChartConsumed.Series["RVolumeSeries"].Color = Color.Green;


            List<long> RYValues = new List<long>();
            List<string> RXValues = new List<string>();

            DateTime previousDt = DateTime.MinValue;
            int secondsDelta = 0;
            long dataPointValue = 0;

            //Load data into YValues
            for (int i = 0; i < trafficDataPoints.Length; i++)
            {
                dataPointValue = Int64.Parse(trafficDataPoints[i].RtnDataPoint.value);
                long epochTicks = Int64.Parse(trafficDataPoints[i].RtnDataPoint.timestamp);
                DateTime dt = DateHelper.EpochToDateTime(epochTicks);

                //Calculate average
                if (previousDt != DateTime.MinValue)
                {
                    secondsDelta = dt.Subtract(previousDt).Minutes;
                }
                else
                {
                    secondsDelta = 30; //This is an assumption, could not be true
                }

                //Check for a possible divide by 0
                if (secondsDelta != 0)
                {
                    dataPointValue = ((dataPointValue / (secondsDelta * 60)) * 8) / 1000;
                    previousDt = dt;

                    RYValues.Add(dataPointValue);
                    RXValues.Add(this.ConvertDateTime(dt));

                }
            }
            ChartConsumed.Series["RVolumeSeries"].Points.DataBindXY(RXValues, "Bitrate", RYValues, "kbps");
            ChartConsumed.Series["RVolumeSeries"].LegendText = "RT Best Effort";
            ChartConsumed.Series["RVolumeSeries"].ChartType = SeriesChartType.Line;
        }

        protected void RadButtonRefresh_Click(object sender, EventArgs e)
        {
            DateTime endDate = DateTime.Now.Date;
            int numDays = Int32.Parse(RadComboBoxPeriodSelection.SelectedValue);
            DateTime startDate = endDate.AddDays(numDays * -1);
            this.GenerateChart(startDate, endDate);
        }

        protected void RadDatePickerEndDate_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
        {
            DateTime startDate = (DateTime)RadDatePickerStartDate.SelectedDate;
            DateTime endDate = (DateTime)RadDatePickerEndDate.SelectedDate;

            if (startDate != null && endDate != null)
            {
                this.GenerateChart(startDate, endDate);
            }
        }

        /// <summary>
        /// Generates the chart
        /// </summary>
        /// <param name="startDate">Start date of the chart</param>
        /// <param name="endDate">End date of the chart</param>
        /// <param name="title">Title for the chart</param>
        protected void GenerateChart(DateTime startDate, DateTime endDate)
        {
            // Include end date in the calculation to create the chart
            endDate = endDate.AddDays(1);

            ChartConsumed.Visible = true;

            BOMonitorControlWS boMonitorControl =
                new net.nimera.cmt.BOMonitorControlWSRef.BOMonitorControlWS();

            BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();

            try
            {
                //Get the SitId for the given MAC address
                
                Terminal term = boAccountingControl.GetTerminalDetailsByMAC(_macAddress);
                TrafficDataPoint[] trafficDataPoints = boMonitorControl.getTrafficViews(_macAddress, startDate, endDate, term.IspId);
                Int64 switchOverCap = 0;

                if (term.RedundantSetup == true)
                {
                    Terminal associatedTerm = boAccountingControl.GetTerminalDetailsByMAC(term.AssociatedMacAddress);
                    TrafficDataPoint[] associatedTrafficDataPoints = boMonitorControl.getTrafficViews(term.AssociatedMacAddress, startDate, endDate, term.IspId);
                    for (int i = 0; i < associatedTrafficDataPoints.Length; i++)
                    {
                        trafficDataPoints[i].RtnDataPoint.value = (Convert.ToInt64(trafficDataPoints[i].RtnDataPoint.value) + Convert.ToInt64(associatedTrafficDataPoints[i].RtnDataPoint.value)).ToString();
                        trafficDataPoints[i].FwdDataPoint.value = (Convert.ToInt64(trafficDataPoints[i].FwdDataPoint.value) + Convert.ToInt64(associatedTrafficDataPoints[i].FwdDataPoint.value)).ToString();
                        if (Convert.ToInt64(trafficDataPoints[i].RtnDataPoint.value) > switchOverCap)
                        {
                            switchOverCap = Convert.ToInt64(trafficDataPoints[i].RtnDataPoint.value);
                        }
                    }
                }

                //Load data into chart
                ChartConsumed.Width = 950;
                ChartConsumed.Height = 500;
                ChartConsumed.Titles.Add("Average Bitrate for SitId " + term.SitId);
                ChartConsumed.Titles.Add("Date and time created: " + DateTime.UtcNow + " UTC");
                ChartConsumed.Titles[0].Font = new Font("Utopia", 12);
                ChartConsumed.BackSecondaryColor = Color.WhiteSmoke;
                ChartConsumed.BackGradientStyle = System.Web.UI.DataVisualization.Charting.GradientStyle.DiagonalRight;
                ChartConsumed.ChartAreas[0].BackColor = Color.AliceBlue;
                ChartConsumed.ChartAreas[0].BackSecondaryColor = Color.GhostWhite;
                ChartConsumed.ChartAreas[0].BackGradientStyle = System.Web.UI.DataVisualization.Charting.GradientStyle.TopBottom;
                ChartConsumed.ChartAreas[0].Area3DStyle.Enable3D = false;
                ChartConsumed.ChartAreas[0].AxisX.Interval = trafficDataPoints.Length / 12;

                if (ChartConsumed.ChartAreas[0].AxisX.Interval < 1.0)
                {
                    ChartConsumed.ChartAreas[0].AxisX.Interval = 1.0;
                }

                ChartConsumed.ChartAreas[0].AxisX.LabelStyle.Angle = 45;
                ChartConsumed.ChartAreas[0].AxisX.Title = "Time";
                ChartConsumed.ChartAreas[0].AxisY.Title = "kbps";

                //Add Return Volumes
                this.RtTrafficGraph(term, trafficDataPoints);


                //Add Forward volumes
                this.FwTrafficGraph(term, trafficDataPoints);

                //SatDiversity. Add SwitchOvers
                if (term.RedundantSetup == true)
                {
                    this.SwitchOverGraph(trafficDataPoints, switchOverCap, startDate, endDate); 
                }

                //Check if we need to show high priority traffic
                if (boAccountingControl.IsBusinessSLA((int)term.SlaId))
                {
                    //Load data
                    TrafficDataPoint[] HighPriorityDataPoints = boMonitorControl.getHighPriorityTrafficViews(_macAddress, startDate, endDate, term.IspId);
                    this.FwHighPriorityTrafficGraph(term, HighPriorityDataPoints);
                    this.RtHighPriorityTrafficgraph(term, HighPriorityDataPoints);
                }


                ChartConsumed.Legends.Add(new Legend("Bitrate in kbps"));
            }
            catch (Exception ex)
            {
                BOLogControlWS boLogControlWS = new BOLogControlWS();
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDateTime = DateTime.Now;
                cmtEx.ExceptionDesc = ex.Message;
                cmtEx.UserDescription = "Method: TerminalConsumedTab.InitTab";
                cmtEx.StateInformation = "No data points avaialble";
                boLogControlWS.LogApplicationException(cmtEx);
            }
        }

        private void SwitchOverGraph(TrafficDataPoint[] trafficDataPoints, Int64 switchOverCap, DateTime startDate, DateTime endDate)
        {

            BOSatDiversityControllerWS _boSatDiversityControl = new BOSatDiversityControllerWS();

            #region SatDiversityGraph
            
                //--SatDiversity SwitchOver Graph Start
                //get the switchovers
                SwitchOver[] switchOverList = _boSatDiversityControl.GetSwitchOversForTerminal(_macAddress, startDate, endDate);
                //prepare a custom switchover list for the graphs
                SwitchOverDrawing[] soList = new SwitchOverDrawing[switchOverList.Length];
                for (int i = 0; i < soList.Length; i++)
                {
                    soList[i] = new SwitchOverDrawing();
                    soList[i].timeStamp = switchOverList[i].timeStamp;
                    soList[i].inChart = false;
                }
                //Lists for the switchover graph
                List<long> SwitchOverYvalues = new List<long>();
                List<string> SwitchOverXvalues = new List<string>();
                //change the scale of the switchover marker, but make it a little larger than the rest of the graph
                switchOverCap = (long)(switchOverCap / 800000);
                bool switchOver;
                for (int i = 0; i < trafficDataPoints.Length; i++)
                {
                    //add an x value first
                    DateTime offset = new DateTime(1970, 1, 1, 0, 0, 0);
                    long epochTicks = Int64.Parse(trafficDataPoints[i].FwdDataPoint.timestamp);
                    DateTime dt = offset.AddMilliseconds(epochTicks);

                    string dateTick = "";

                    if (dt.Day < 10)
                        dateTick = dateTick + "0" + dt.Day + "/";
                    else
                        dateTick = dateTick + dt.Day + "/";

                    if (dt.Month < 10)
                        dateTick = dateTick + "0" + dt.Month + "/";
                    else
                        dateTick = dateTick + dt.Month + "/";

                    dateTick = dateTick + dt.Year;

                    SwitchOverXvalues.Add(dateTick);

                    //loop through the switchovers to see if one should be added
                    switchOver = false;
                    for (int j = 0; j < soList.Length; j++)
                    {
                        if (soList[j].timeStamp.ToShortDateString() == dateTick && !soList[j].inChart)
                        {
                            switchOver = true;
                            soList[j].inChart = true;
                            j = soList.Length + 1;
                        }
                    }
                    //add a switchover marker
                    if (switchOver)
                    {
                        SwitchOverYvalues.Add(switchOverCap);
                        SwitchOverXvalues.Add(dateTick);
                    }

                    //always add a 0 marker to make sure the line is at 0 again
                    SwitchOverYvalues.Add(0);
                }

                //Draw the SwitchOver chart
                ChartConsumed.Series.Add(new Series("SwitchOverSeries"));
                ChartConsumed.Series["SwitchOverSeries"].BorderWidth = 1;
                ChartConsumed.Series["SwitchOverSeries"].ShadowOffset = 1;
                ChartConsumed.Series["SwitchOverSeries"].XValueType = ChartValueType.Int64;
                ChartConsumed.Series["SwitchOverSeries"].Color = Color.Blue;
                ChartConsumed.Series["SwitchOverSeries"].Points.DataBindXY(SwitchOverXvalues, "Time", SwitchOverYvalues, "MB");
                ChartConsumed.Series["SwitchOverSeries"].LegendText = "Switchover Point";
                ChartConsumed.Series["SwitchOverSeries"].ChartType = SeriesChartType.Line;
            
            #endregion
        }

        protected void RadButtonDateSelection_Click(object sender, EventArgs e)
        {
            DateTime startDate = (DateTime)RadDatePickerStartDate.SelectedDate;
            DateTime endDate = (DateTime)RadDatePickerEndDate.SelectedDate;

            if (startDate != null && endDate != null)
            {
                this.GenerateChart(startDate, endDate);
            }
        }

        /// <summary>
        /// Converts the date into a string
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        protected string ConvertDateTime(DateTime dt)
        {
            string dateTick = "";

            if (dt.Day < 10)
                dateTick = dateTick + "0" + dt.Day + "/";
            else
                dateTick = dateTick + dt.Day + "/";

            if (dt.Month < 10)
                dateTick = dateTick + "0" + dt.Month + "/";
            else
                dateTick = dateTick + dt.Month + "/";

            dateTick = dateTick + dt.Year;

            if (dt.Hour < 10)
                dateTick = dateTick + " " + "0" + dt.Hour + ":";
            else
                dateTick = dateTick + " " + dt.Hour + ":";

            if (dt.Minute < 10)
                dateTick = dateTick + "0" + dt.Minute;
            else
                dateTick = dateTick + dt.Minute;

            return dateTick;
        }

        private class SwitchOverDrawing
        {
            public DateTime timeStamp { get; set; }
            public bool inChart { get; set; }
        }
    }
}