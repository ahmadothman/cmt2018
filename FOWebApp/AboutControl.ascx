﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AboutControl.ascx.cs" Inherits="FOWebApp.AboutControl" %>
<style>
    #AboutFooter {
       font-size: 8px;
       position: fixed;
       bottom: 0; 
       width:100%; 
       text-align: center
    }
</style>
<div class="picturespace">
    <asp:Image ID="Image1" runat="server" Width="300px" ImageUrl="~/Images/LogoSatADSL.jpg" />
</div>
<div class="infospace">
    <div class="art-blockheader"><b>Customer Management Tool. You have the following roles allocated:</b></div>
    <div>
        <p>
            <asp:Label ID="LabelRoles" runat="server"></asp:Label>
        </p>
    </div>
    <p>
        Version: 
        <asp:Label ID="LabelVersion" runat="server"></asp:Label>
    </p>
    <p>
        Release date:
        <asp:Label ID="LabelReleaseDate" runat="server"></asp:Label>
    </p>
    <p>
        SatADSL (c)
        <asp:Label ID="LabelCopyright" runat="server"></asp:Label>
    </p>
    <p>
        For more information about SatADSL and our Services please do not hesitate
        to visit our <a href="http://www.satadsl.net" target="_blank">website</a>.
    </p>
    <p id="AboutFooter">       
        <a href="http://www.axialis.com/free/icons">Icons</a> by <a href="http://www.axialis.com">Axialis Team</a>
    </p>
</div>

