<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="DefaultHeader.ascx.cs" Inherits="DefaultHeader" %>
<div class="logotext">
    <div class="title">Customer Management Tool</div>
    <%--<div class="titleLong">Customer Management Tool</div>--%>
    <%  
        if (HttpContext.Current.User.IsInRole("Distributor"))
        {
    %>
    <div class="subtitle">
        Distributor 
        <%--<asp:Label ID="LabelVersionDistr" runat="server" Font-Italic="True"></asp:Label>--%>
    </div>
    <%
        }
    %>
    <%  
        if (HttpContext.Current.User.IsInRole("NOC Administrator"))
        {
    %>
    <div class="subtitle">
        Network Administrator 
        <%--<asp:Label ID="LabelVersionNocsa" runat="server" Font-Italic="True"></asp:Label>--%>
    </div>
    <%
        }
    %>
    <%
        if (HttpContext.Current.User.IsInRole("NOC Operator"))
        {
    %>
    <div class="subtitle">
        Network Operator 
        <%--<asp:Label ID="LabelVersionNocop" runat="server" Font-Italic="True"></asp:Label>--%>
    </div>
    <%
        }
    %>
    <%
        if (HttpContext.Current.User.IsInRole("FinanceAdmin"))
        {
    %>
    <div class="subtitle">
        Finance Administrator 
        <%--<asp:Label ID="LabelVersionFinanceAdmin" runat="server" Font-Italic="True"></asp:Label>--%>
    </div>
    <%
        }
    %>
    <%
        if (HttpContext.Current.User.IsInRole("Organization"))
        {
    %>
    <div class="subtitle">
        Organization 
        <%--<asp:Label ID="LabelVersionOrg" runat="server" Font-Italic="True"></asp:Label>--%>
    </div>
    <%
        }
    %>
</div>
<div class="logo">
    <!-- <img src="Images/LogoSatADSL.jpg" alt="SatADSL Logo" height="80px"/> -->
    <%--<asp:Image ID="Logo" runat="server" ImageUrl="Images/LogoSatADSL.jpg" AlternateText="SatADSL Logo" Height="80px" ImageAlign="Left" />--%>
    <a href="http://www.satadsl.net/" target="_blank">
        <asp:Image ID="Logo" runat="server" ImageUrl="~/Images/logo_satadsl.png" AlternateText="SatADSL" ImageAlign="Left" />
    </a>
</div>

