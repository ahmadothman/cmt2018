﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SoHo.ascx.cs" Inherits="FOWebApp.ServiceLines.SoHo" %>
<h1>SoHo service</h1>
<table>
    <tr>
        <td>
            <h2>Bandwidth</h2>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelForward" runat="server">Forward speed:</asp:Label>
        </td>
        <td>
            <telerik:RadNumericTextBox runat="server" ID="RadNumericTextBoxForwardSpeed" InputType="Number" ShowSpinButtons="true">
                <NumberFormat ZeroPattern="n" DecimalDigits="0"></NumberFormat>
            </telerik:RadNumericTextBox>Kbps
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelReturn" runat="server">Return speed:</asp:Label>
        </td>
        <td>
            <telerik:RadNumericTextBox runat="server" ID="RadNumericTextBoxReturnSpeed" InputType="Number" ShowSpinButtons="true">
                <NumberFormat ZeroPattern="n" DecimalDigits="0"></NumberFormat>
            </telerik:RadNumericTextBox>Kbps
        </td>
    </tr>
    <tr>
        <td>
            <h2>Options</h2>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelWeight" runat="server">Weight:</asp:Label>
        </td>
        <td>
            <telerik:RadNumericTextBox ID="RadNumericTextBoxWeight" runat="server" MinValue="0" MaxValue="100" Culture="en-US" ShowSpinButtons="True" Skin="Metro" Width="100px" Value="0">
                <IncrementSettings Step="1" />
                <NumberFormat AllowRounding="true" KeepTrailingZerosOnFocus="true" DecimalDigits="2" />
            </telerik:RadNumericTextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Please specify the weight" ControlToValidate="radnumerictextboxweight"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelForVNO" runat="server">For VNO:</asp:Label>
        </td>
        <td>
            <telerik:RadComboBox ID="RadComboBoxVNOId" runat="server" Width="345px" ExpandDirection="Up" Skin="Metro"></telerik:RadComboBox>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelHide" runat="server">Hide for distributor:</asp:Label></td>
        <td>
            <asp:CheckBox ID="CheckBoxHide" runat="server" ToolTip="This flag determines if the ServicePack is visible to the distributor or not" />
        </td>
    </tr>
    <tr>
        <td></td>
        <td style="text-align:right;">
            <telerik:RadButton runat="server" ID="RadButtonPrevious" Text="Previous"></telerik:RadButton>
            <telerik:RadButton runat="server" ID="RadButtonFinish" Text="Finish"></telerik:RadButton>
        </td>
    </tr>
</table>

