﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SpecialTailored.aspx.cs" Inherits="FOWebApp.ServiceLines.SpecialTailored1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" Height="100%" Width="100%">
            <div>
                <h1>Special Tailored Service</h1>
                <table cellpadding="2" cellspacing="5">
                    <tr>
                        <td>Sla ID:</td>
                        <td>
                            <%--<asp:Label runat="server" ID="LabelSlaID"></asp:Label>--%>
                            <asp:TextBox runat="server" ID="TextBoxSlaID" type="number" Width="100"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="You must provide an SLA ID"
                                ControlToValidate="TextBoxSlaID" SetFocusOnError="true" ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>Name:</td>
                        <td>
                            <asp:TextBox ID="TextBoxName" runat="server" Columns="50" Width="300px">
                            </asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="You must provide an SLA Name!"
                                ControlToValidate="TextBoxName" SetFocusOnError="True" ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>Common Name:</td>
                        <td>
                            <asp:TextBox ID="TextBoxSlaCommonName" runat="server" Columns="50" Width="300px">
                            </asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <h2>Bandwidth</h2>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="LabelCIRFWD" runat="server">CIR forward:</asp:Label></td>
                        <td>
                            <telerik:RadNumericTextBox RenderMode="Lightweight" ID="RadNumericTextBoxCIRFWD" runat="server" ShowSpinButtons="True" Culture="nl-BE" Skin="WebBlue" Value="0" Width="125px">
                                <NumberFormat ZeroPattern="n" DecimalDigits="0"></NumberFormat>
                            </telerik:RadNumericTextBox><asp:Label ID="LabelKbps5" runat="server"> Kbps</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="LabelCIRRTN" runat="server">CIR return:</asp:Label></td>
                        <td>
                            <telerik:RadNumericTextBox ID="RadNumericTextBoxCIRRTN" runat="server" ShowSpinButtons="True" Culture="nl-BE" Skin="WebBlue" Value="0" Width="125px">
                                <NumberFormat ZeroPattern="n" DecimalDigits="0"></NumberFormat>
                            </telerik:RadNumericTextBox><asp:Label ID="LabelKbps6" runat="server"> Kbps</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="LabelForwardSpeed" runat="server">Forward speed:</asp:Label></td>
                        <td>
                            <telerik:RadNumericTextBox ID="RadNumericTextBoxForwardDefault" runat="server" ShowSpinButtons="True" Culture="nl-BE" Skin="WebBlue" Value="0" Width="125px">
                                <NumberFormat ZeroPattern="n" DecimalDigits="0"></NumberFormat>
                            </telerik:RadNumericTextBox><asp:Label ID="LabelKbps1" runat="server"> Kbps</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="LabelReturnSpeed" runat="server">Return speed:</asp:Label></td>
                        <td>
                            <telerik:RadNumericTextBox ID="RadNumericTextBoxReturnDefault" runat="server" ShowSpinButtons="True" Culture="nl-BE" Skin="WebBlue" Value="0" Width="125px">
                                <NumberFormat ZeroPattern="n" DecimalDigits="0"></NumberFormat>
                            </telerik:RadNumericTextBox><asp:Label ID="LabelKbps2" runat="server"> Kbps</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="LabelMinBWFWD" runat="server">Minimum forward bandwidth:</asp:Label></td>
                        <td>
                            <telerik:RadNumericTextBox ID="RadNumericTextBoxMinBWFWD" runat="server" ShowSpinButtons="True" Culture="nl-BE" Skin="WebBlue" Value="0" Width="125px">
                                <NumberFormat ZeroPattern="n" DecimalDigits="0"></NumberFormat>
                            </telerik:RadNumericTextBox><asp:Label ID="LabelKbps7" runat="server"> Kbps</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="LabelMinBWRTN" runat="server">Minimum return bandwidth:</asp:Label></td>
                        <td>
                            <telerik:RadNumericTextBox ID="RadNumericTextBoxMinBWRTN" runat="server" ShowSpinButtons="True" Culture="nl-BE" Skin="WebBlue" Value="0" Width="125px">
                                <NumberFormat ZeroPattern="n" DecimalDigits="0"></NumberFormat>
                            </telerik:RadNumericTextBox><asp:Label ID="LabelKbps8" runat="server"> Kbps</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="LabelFWDSpeedAboveFUP" runat="server">Forward speed above FUP:</asp:Label></td>
                        <td>
                            <telerik:RadNumericTextBox ID="RadNumericTextBoxForwardAboveFup" runat="server" ShowSpinButtons="True" Culture="nl-BE" Skin="WebBlue" Value="0" Width="125px">
                                <NumberFormat ZeroPattern="n" DecimalDigits="0"></NumberFormat>
                            </telerik:RadNumericTextBox><asp:Label ID="LabelKbps3" runat="server"> Kbps</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="LabelRTNSpeedAboveFUP" runat="server">Return speed Above FUP:</asp:Label></td>
                        <td>
                            <telerik:RadNumericTextBox ID="RadNumericTextBoxReturnAboveFup" runat="server" ShowSpinButtons="True" Culture="nl-BE" Skin="WebBlue" Value="0" Width="125px">
                                <NumberFormat ZeroPattern="n" DecimalDigits="0"></NumberFormat>
                            </telerik:RadNumericTextBox><asp:Label ID="LabelKbps4" runat="server"> Kbps</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <h2>Volume</h2>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="LabelForwardFUP" runat="server">Forward FUP:</asp:Label></td>
                        <td>
                            <telerik:RadNumericTextBox ID="RadNumericTextBoxForwardFUP" runat="server" Culture="en-US" ToolTip="Value in GB" ShowSpinButtons="True" Skin="WebBlue" Value="0" Width="125px">
                                <IncrementSettings Step="0.01" />
                                <NumberFormat ZeroPattern="n"></NumberFormat>
                            </telerik:RadNumericTextBox>
                            <asp:Label ID="LabelGB1" runat="server">GB</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="LabelReturnFUP" runat="server">Return FUP:</asp:Label></td>
                        <td>
                            <telerik:RadNumericTextBox ID="RadNumericTextBoxReturnFUP" Culture="en-US" runat="server" ToolTip="Value in GB" ShowSpinButtons="True" Skin="WebBlue" Value="0" Width="125px">
                                <IncrementSettings Step="0.01" />
                                <NumberFormat ZeroPattern="n"></NumberFormat>
                            </telerik:RadNumericTextBox>
                            <asp:Label ID="LabelGB2" runat="server">GB</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="LabelVolumeAgg" runat="server">NMS Volume type:</asp:Label></td>
                        <td>
                            <asp:RadioButton runat="server" Checked="false" ID="RadioButtonVolAgg" Text="Aggregated volumes" GroupName="VolGroup" />&nbsp;<asp:RadioButton runat="server" Checked="false" ID="RadioButtonVolSep" Text="Separate volumes" GroupName="VolGroup" />
                            <div id="selectType"><a style="color: red">Please select a type</a></div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <h2>Options</h2>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="LabelWeight" runat="server">Weight:</asp:Label></td>
                        <td>
                            <telerik:RadNumericTextBox ID="RadNumericTextBoxWeight" runat="server" MinValue="0" MaxValue="100" Culture="en-US" ShowSpinButtons="True" Skin="WebBlue" Width="100px" Value="0">
                                <IncrementSettings Step="1" />
                                <NumberFormat AllowRounding="true" KeepTrailingZerosOnFocus="true" DecimalDigits="2" />
                            </telerik:RadNumericTextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Please specify the weight" ControlToValidate="radnumerictextboxweight"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="LabelForVNO" runat="server">For VNO:</asp:Label></td>
                        <td>
                            <telerik:RadComboBox ID="RadComboBoxVNOId" runat="server" Width="345px" ExpandDirection="Up"></telerik:RadComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="LabelHide" runat="server">Hide for distributor:</asp:Label></td>
                        <td>
                            <asp:CheckBox ID="CheckBoxHide" runat="server" ToolTip="This flag determines if the ServicePack is visible to the distributor or not" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="LabelFreezone2" runat="server">Free Zone:</asp:Label></td>
                        <td>
                            <asp:CheckBox ID="CheckBoxFreeZone" runat="server" ToolTip="Check if this service pack contains the Freezone option" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="LabelVoIP2" runat="server">VoIP:</asp:Label></td>
                        <td>
                            <asp:CheckBox ID="CheckBoxVoIP" runat="server" ToolTip="Check if this service pack contains a VoIP option" />
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td style="text-align:right;">
                            <telerik:RadButton runat="server" ID="RadButtonFinish" Text="Finish" OnClick="RadButtonFinish_Click"></telerik:RadButton>
                            <asp:Label runat="server" id="LabelResult"></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
        </telerik:RadAjaxPanel>
    </form>
</body>
</html>
