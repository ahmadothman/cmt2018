﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SpecialTailored.ascx.cs" Inherits="FOWebApp.ServiceLines.SpecialTailored" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<h1>Special Tailored Service</h1>
<table cellpadding="2" cellspacing="5">
    <tr>
        <td>
            <h2>Bandwidth</h2>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelCIRFWD" runat="server">CIR forward:</asp:Label></td>
        <td>
            <telerik:RadNumericTextBox ID="RadNumericTextBoxCIRFWD" runat="server" ShowSpinButtons="True" Culture="nl-BE" Skin="Metro" Value="0" Width="125px">
                <NumberFormat ZeroPattern="n" DecimalDigits="0"></NumberFormat>
            </telerik:RadNumericTextBox><asp:Label ID="LabelKbps5" runat="server"> Kbps</asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelCIRRTN" runat="server">CIR return:</asp:Label></td>
        <td>
            <telerik:RadNumericTextBox ID="RadNumericTextBoxCIRRTN" runat="server" ShowSpinButtons="True" Culture="nl-BE" Skin="Metro" Value="0" Width="125px">
                <NumberFormat ZeroPattern="n" DecimalDigits="0"></NumberFormat>
            </telerik:RadNumericTextBox><asp:Label ID="LabelKbps6" runat="server"> Kbps</asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelForwardSpeed" runat="server">Forward speed:</asp:Label></td>
        <td>
            <telerik:RadNumericTextBox ID="RadNumericTextBoxForwardDefault" runat="server" ShowSpinButtons="True" Culture="nl-BE" Skin="Metro" Value="0" Width="125px">
                <NumberFormat ZeroPattern="n" DecimalDigits="0"></NumberFormat>
            </telerik:RadNumericTextBox><asp:Label ID="LabelKbps1" runat="server"> Kbps</asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelReturnSpeed" runat="server">Return speed:</asp:Label></td>
        <td>
            <telerik:RadNumericTextBox ID="RadNumericTextBoxReturnDefault" runat="server" ShowSpinButtons="True" Culture="nl-BE" Skin="Metro" Value="0" Width="125px">
                <NumberFormat ZeroPattern="n" DecimalDigits="0"></NumberFormat>
            </telerik:RadNumericTextBox><asp:Label ID="LabelKbps2" runat="server"> Kbps</asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelMinBWFWD" runat="server">Minimum forward bandwidth:</asp:Label></td>
        <td>
            <telerik:RadNumericTextBox ID="RadNumericTextBoxMinBWFWD" runat="server" ShowSpinButtons="True" Culture="nl-BE" Skin="Metro" Value="0" Width="125px">
                <NumberFormat ZeroPattern="n" DecimalDigits="0"></NumberFormat>
            </telerik:RadNumericTextBox><asp:Label ID="LabelKbps7" runat="server"> Kbps</asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelMinBWRTN" runat="server">Minimum return bandwidth:</asp:Label></td>
        <td>
            <telerik:RadNumericTextBox ID="RadNumericTextBoxMinBWRTN" runat="server" ShowSpinButtons="True" Culture="nl-BE" Skin="Metro" Value="0" Width="125px">
                <NumberFormat ZeroPattern="n" DecimalDigits="0"></NumberFormat>
            </telerik:RadNumericTextBox><asp:Label ID="LabelKbps8" runat="server"> Kbps</asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelFWDSpeedAboveFUP" runat="server">Forward speed above FUP:</asp:Label></td>
        <td>
            <telerik:RadNumericTextBox ID="RadNumericTextBoxForwardAboveFup" runat="server" ShowSpinButtons="True" Culture="nl-BE" Skin="Metro" Value="0" Width="125px">
                <NumberFormat ZeroPattern="n" DecimalDigits="0"></NumberFormat>
            </telerik:RadNumericTextBox><asp:Label ID="LabelKbps3" runat="server"> Kbps</asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelRTNSpeedAboveFUP" runat="server">Return speed Above FUP:</asp:Label></td>
        <td>
            <telerik:RadNumericTextBox ID="RadNumericTextBoxReturnAboveFup" runat="server" ShowSpinButtons="True" Culture="nl-BE" Skin="Metro" Value="0" Width="125px">
                <NumberFormat ZeroPattern="n" DecimalDigits="0"></NumberFormat>
            </telerik:RadNumericTextBox><asp:Label ID="LabelKbps4" runat="server"> Kbps</asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <h2>Volume</h2>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelForwardFUP" runat="server">Forward FUP:</asp:Label></td>
        <td>
            <telerik:RadNumericTextBox ID="RadNumericTextBoxForwardFUP" runat="server" Culture="en-US" ToolTip="Value in GB" ShowSpinButtons="True" Skin="Metro" Value="0" Width="125px">
                <IncrementSettings Step="0.01" />
                <NumberFormat ZeroPattern="n"></NumberFormat>
            </telerik:RadNumericTextBox>
            <asp:Label ID="LabelGB1" runat="server">GB</asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelReturnFUP" runat="server">Return FUP:</asp:Label></td>
        <td>
            <telerik:RadNumericTextBox ID="RadNumericTextBoxReturnFUP" Culture="en-US" runat="server" ToolTip="Value in GB" ShowSpinButtons="True" Skin="Metro" Value="0" Width="125px">
                <IncrementSettings Step="0.01" />
                <NumberFormat ZeroPattern="n"></NumberFormat>
            </telerik:RadNumericTextBox>
            <asp:Label ID="LabelGB2" runat="server">GB</asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelVolumeAgg" runat="server">NMS Volume type:</asp:Label></td>
        <td>
            <asp:RadioButton runat="server" Checked="false" ID="RadioButtonVolAgg" Text="Aggregated volumes" GroupName="VolGroup" />&nbsp;<asp:RadioButton runat="server" Checked="false" ID="RadioButtonVolSep" Text="Separate volumes" GroupName="VolGroup" />
            <div id="selectType"><a style="color: red">Please select a type</a></div>
        </td>
    </tr>
    <tr>
        <td>
            <h2>Options</h2>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelWeight" runat="server">Weight:</asp:Label></td>
        <td>
            <telerik:RadNumericTextBox ID="RadNumericTextBoxWeight" runat="server" MinValue="0" MaxValue="100" Culture="en-US" ShowSpinButtons="True" Skin="Metro" Width="100px" Value="0">
                <IncrementSettings Step="1" />
                <NumberFormat AllowRounding="true" KeepTrailingZerosOnFocus="true" DecimalDigits="2" />
            </telerik:RadNumericTextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Please specify the weight" ControlToValidate="radnumerictextboxweight"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelForVNO" runat="server">For VNO:</asp:Label></td>
        <td>
            <telerik:RadComboBox ID="RadComboBoxVNOId" runat="server" Width="345px" ExpandDirection="Up" Skin="Metro"></telerik:RadComboBox>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelHide" runat="server">Hide for distributor:</asp:Label></td>
        <td>
            <asp:CheckBox ID="CheckBoxHide" runat="server" ToolTip="This flag determines if the ServicePack is visible to the distributor or not" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelFreezone2" runat="server">Free Zone:</asp:Label></td>
        <td>
            <asp:CheckBox ID="CheckBoxFreeZone" runat="server" ToolTip="Check if this service pack contains the Freezone option" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelVoIP2" runat="server">VoIP:</asp:Label></td>
        <td>
            <asp:CheckBox ID="CheckBoxVoIP" runat="server" ToolTip="Check if this service pack contains a VoIP option" />
        </td>
    </tr>
</table>
<asp:Button runat="server" id="ButtonFinish" Text="Finish" OnClick="ButtonFinish_Click" />
<asp:Label runat="server" id="LabelResult"></asp:Label>