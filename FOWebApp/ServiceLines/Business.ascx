﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Business.ascx.cs" Inherits="FOWebApp.ServiceLines.Business" %>
<h2>Business Service</h2>
<table>
    <tr>
        <td>
            <h2>Bandwidth</h2>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelForwardSpeed" runat="server">Forward speed:</asp:Label></td>
        <td>
            <telerik:RadNumericTextBox ID="RadNumericTextBoxForwardDefault" runat="server" ShowSpinButtons="True" Culture="nl-BE" Skin="Metro" Value="0" Width="125px">
                <NumberFormat ZeroPattern="n" DecimalDigits="0"></NumberFormat>
            </telerik:RadNumericTextBox><asp:Label ID="LabelKbps1" runat="server"> Kbps</asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelReturnSpeed" runat="server">Return speed:</asp:Label></td>
        <td>
            <telerik:RadNumericTextBox ID="RadNumericTextBoxReturnDefault" runat="server" ShowSpinButtons="True" Culture="nl-BE" Skin="Metro" Value="0" Width="125px">
                <NumberFormat ZeroPattern="n" DecimalDigits="0"></NumberFormat>
            </telerik:RadNumericTextBox><asp:Label ID="LabelKbps2" runat="server"> Kbps</asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelMinBWFWD" runat="server">Minimum forward bandwidth:</asp:Label></td>
        <td>
            <telerik:RadNumericTextBox ID="RadNumericTextBoxMinBWFWD" runat="server" ShowSpinButtons="True" Culture="nl-BE" Skin="Metro" Value="0" Width="125px">
                <NumberFormat ZeroPattern="n" DecimalDigits="0"></NumberFormat>
            </telerik:RadNumericTextBox><asp:Label ID="LabelKbps7" runat="server"> Kbps</asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelMinBWRTN" runat="server">Minimum return bandwidth:</asp:Label></td>
        <td>
            <telerik:RadNumericTextBox ID="RadNumericTextBoxMinBWRTN" runat="server" ShowSpinButtons="True" Culture="nl-BE" Skin="Metro" Value="0" Width="125px">
                <NumberFormat ZeroPattern="n" DecimalDigits="0"></NumberFormat>
            </telerik:RadNumericTextBox><asp:Label ID="LabelKbps8" runat="server"> Kbps</asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <h2>Volume</h2>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelVolumeAgg" runat="server">NMS Volume type:</asp:Label></td>
        <td>
            <asp:RadioButton runat="server" Checked="false" value="1" onclick="DisplayElement(this)" ID="RadioButtonVolAgg" Text="Aggregated volumes" GroupName="VolGroup" />&nbsp;<asp:RadioButton runat="server" Checked="false" value="2" onclick="DisplayElement(this)" ID="RadioButtonVolSep" Text="Separate volumes" GroupName="VolGroup" />
            <div id="selectType"><a style="color: red">Please select a type</a></div>
        </td>
    </tr>
    <tr>
        <td>
            <h2>Options</h2>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelWeight" runat="server">Weight:</asp:Label></td>
        <td>
            <telerik:RadNumericTextBox ID="RadNumericTextBoxWeight" runat="server" MinValue="0" MaxValue="100" Culture="en-US" ShowSpinButtons="True" Skin="Metro" Width="100px" Value="0">
                <IncrementSettings Step="1" />
                <NumberFormat AllowRounding="true" KeepTrailingZerosOnFocus="true" DecimalDigits="2" />
            </telerik:RadNumericTextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Please specify the weight" ControlToValidate="radnumerictextboxweight"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelForVNO" runat="server">For VNO:</asp:Label></td>
        <td>
            <telerik:RadComboBox ID="RadComboBoxVNOId" runat="server" Width="345px" ExpandDirection="Up" Skin="Metro"></telerik:RadComboBox>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelHide" runat="server">Hide for distributor:</asp:Label></td>
        <td>
            <asp:CheckBox ID="CheckBoxHide" runat="server" ToolTip="This flag determines if the ServicePack is visible to the distributor or not" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelFreezone2" runat="server">Free Zone:</asp:Label></td>
        <td>
            <asp:CheckBox ID="CheckBoxFreeZone" runat="server" ToolTip="Check if this service pack contains the Freezone option" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="LabelVoIP2" runat="server">VoIP:</asp:Label></td>
        <td>
            <asp:CheckBox ID="CheckBoxVoIP" runat="server" ToolTip="Check if this service pack contains a VoIP option" />
        </td>
    </tr>
    <tr>
        <td></td>
        <td style="text-align:right;">
            <telerik:RadButton runat="server" ID="RadButtonPrevious" Text="Previous"></telerik:RadButton>
            <telerik:RadButton runat="server" ID="RadButtonFinish" Text="Finish"></telerik:RadButton>
        </td>
    </tr>
</table>