﻿using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOMonitorControlWSRef;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FOWebApp.ServiceLines
{
    public partial class SpecialTailored1 : System.Web.UI.Page
    {
        BOAccountingControlWS _boAccountingControlWS;

        protected void Page_Load(object sender, EventArgs e)
        {
            _boAccountingControlWS = new BOAccountingControlWS();
            //LabelSlaID.Text = Request.Params["isp"].ToString();
        }

        protected void RadButtonFinish_Click(object sender, EventArgs e)
        {
            if (HttpContext.Current.User.IsInRole("VNO"))
            {
                MembershipUser myObject = Membership.GetUser();
                string userId = myObject.ProviderUserKey.ToString();
                Session["VNO"] = (_boAccountingControlWS.GetDistributorForUser(userId).Id).ToString();
            }
            else
            {
                Session["VNO"] = RadComboBoxVNOId.SelectedValue.ToString();
            }

            //Create a new Service pack
            ServiceLevel serviceLevel = new ServiceLevel();
            //serviceLevel.SlaId = Int32.Parse(Session["ServicePackID"].ToString());
            serviceLevel.SlaName = TextBoxName.Text;
            if (TextBoxSlaCommonName.Text != null)
            {
                serviceLevel.SlaCommonName = TextBoxSlaCommonName.Text;
            }

            serviceLevel.IspId = Int32.Parse(Request.Params["isp"].ToString());
            serviceLevel.ServiceClass = Int32.Parse(Request.Params["SL"].ToString());

            if (serviceLevel.ServiceClass == 1)
            {
                serviceLevel.Voucher = true;
                serviceLevel.Business = false;
            }
            else if (serviceLevel.ServiceClass == 2)
            {
                serviceLevel.Business = true;
                serviceLevel.Voucher = false;
            }
            else if (serviceLevel.ServiceClass == 3)
            {
                serviceLevel.Business = false;
                serviceLevel.Voucher = false;
            }
            serviceLevel.DRFWD = Int32.Parse(RadNumericTextBoxForwardDefault.Value.ToString());
            serviceLevel.DRRTN = Int32.Parse(RadNumericTextBoxReturnDefault.Value.ToString());
            if (RadComboBoxVNOId.Text != "")
            {
                serviceLevel.VNOId = Int32.Parse(RadComboBoxVNOId.SelectedItem.Value);
            }
            serviceLevel.Hide = CheckBoxHide.Checked;

            serviceLevel.CIRFWD = Int32.Parse(RadNumericTextBoxCIRFWD.Value.ToString());
            serviceLevel.CIRRTN = Int32.Parse(RadNumericTextBoxCIRRTN.Value.ToString());
            serviceLevel.MinBWFWD = Int32.Parse(RadNumericTextBoxMinBWFWD.Value.ToString());
            serviceLevel.MinBWRTN = Int32.Parse(RadNumericTextBoxMinBWRTN.Value.ToString());

            if (RadNumericTextBoxForwardAboveFup.Text != "" && RadNumericTextBoxReturnAboveFup.Text != "")
                serviceLevel.DrAboveFup = RadNumericTextBoxForwardAboveFup.Text + "/" + RadNumericTextBoxReturnAboveFup.Text;
            else
                serviceLevel.DrAboveFup = "0/0";
            

            //serviceLevel.ServicePackAmt = (float)(Convert.ToInt32(Session["BaseAmount"].ToString())) / 100;
            //serviceLevel.FreeZoneAmt = (float)(Convert.ToInt32(Session["FreeZoneAmount"].ToString())) / 100;
            //serviceLevel.VoIPAmt = (float)(Convert.ToInt32(Session["VoIPAmount"].ToString())) / 100;
            //serviceLevel.ServicePackAmtEUR = (float)(Convert.ToInt32(Session["BaseAmountEUR"].ToString())) / 100;
            //serviceLevel.FreeZoneAmtEUR = (float)(Convert.ToInt32(Session["FreeZoneAmountEUR"].ToString())) / 100;
            //erviceLevel.VoIPAmtEUR = (float)(Convert.ToInt32(Session["VoIPAmountEUR"].ToString())) / 100;
            serviceLevel.SLAWeight = (double)Convert.ToDouble(RadNumericTextBoxWeight.Value.ToString());

            //serviceLevel.LowVolumeFWD = (decimal)(Convert.ToInt32(Session["LowVolFWD"].ToString())) / 100;
            //serviceLevel.LowVolumeRTN = (decimal)(Convert.ToInt32(Session["LowVolRTN"].ToString())) / 100;
            //serviceLevel.HighVolumeFWD = (decimal)(Convert.ToInt32(Session["HighVolFWD"].ToString())) / 100;
            //serviceLevel.HighVolumeRTN = (decimal)(Convert.ToInt32(Session["HighVolRTN"].ToString())) / 100;
            //serviceLevel.HighVolumeSUM = (decimal)(Convert.ToInt32(Session["HighVolSUM"].ToString())) / 100;
            //serviceLevel.LowVolumeSUM = (decimal)(Convert.ToInt32(Session["LowVolSUM"].ToString())) / 100;

            if (RadioButtonVolAgg.ToString() == "1")
            {
                serviceLevel.AggregateFlag = true;
            }
            else
            {
                serviceLevel.AggregateFlag = false;
            }

            //if (Session["EdgeISP"] != null)
            //{
            //    serviceLevel.EdgeISP = (int)Int32.Parse(Session["EdgeISP"].ToString());
            //}

            //if (Session["EdgeSLA"] != null)
            //{
            //    serviceLevel.EdgeSLA = (int)Int32.Parse(Session["EdgeSLA"].ToString());
            //}

            //The SerivcePack class is used for the NMS
            ServicePack servicePack = this.serviceLevelToServicePack(serviceLevel);
            BOMonitorControlWS boMonitorControlWS = new BOMonitorControlWS();

            if (boMonitorControlWS.NMSUpdateServicePack(servicePack)) //will return true for all non-NMS SLA's as well
            {
                if (_boAccountingControlWS.UpdateServicePack(serviceLevel)) //writes the SLA to the CMT database
                {
                    LabelResult.ForeColor = Color.DarkGreen;
                    LabelResult.Text = "Servicepack update succeeded";
                }
                else
                {
                    LabelResult.ForeColor = Color.Red;
                    LabelResult.Text = "Servicepack update failed";
                }
            }
            else
            {
                LabelResult.ForeColor = Color.Red;
                LabelResult.Text = "Servicepack update failed";
            }
        }

        private ServicePack serviceLevelToServicePack(ServiceLevel serviceLevel)
        {
            ServicePack servicePack = new ServicePack();

            servicePack.SlaId = serviceLevel.SlaId;
            servicePack.SlaName = serviceLevel.SlaName;
            servicePack.SlaCommonName = serviceLevel.SlaCommonName;
            servicePack.IspId = serviceLevel.IspId;
            servicePack.ServiceClass = serviceLevel.ServiceClass;
            //all NMS speeds must be transformed from kbps to bits/sec
            servicePack.DRFWD = serviceLevel.DRFWD * 1000;
            servicePack.DRRTN = serviceLevel.DRRTN * 1000;
            servicePack.CIRFWD = serviceLevel.CIRFWD * 1000;
            servicePack.CIRRTN = serviceLevel.CIRRTN * 1000;
            servicePack.MinBWFWD = serviceLevel.MinBWFWD * 1000;
            servicePack.MinBWRTN = serviceLevel.MinBWRTN * 1000;
            //NMS volumes must be transformed from GB to bytes
            servicePack.HighVolumeSUM = Convert.ToInt64(serviceLevel.HighVolumeSUM * 1000000000);
            servicePack.LowVolumeSUM = Convert.ToInt64(serviceLevel.LowVolumeSUM * 1000000000);
            servicePack.Weight = serviceLevel.SLAWeight;


            return servicePack;
        }
    }
}