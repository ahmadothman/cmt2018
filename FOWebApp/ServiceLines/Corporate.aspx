﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Corporate.aspx.cs" Inherits="FOWebApp.ServiceLines.Corporate1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" Height="100%" Width="100%">
            <div>
                <h1>Corporate Service</h1>
                <table cellpadding="2" cellspacing="5">
                    <tr>
                        <td>Name:</td>
                        <td>
                            <asp:TextBox ID="TextBoxName" runat="server" Columns="50" Width="300px">
                            </asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="You must provide an SLA Name!"
                                ControlToValidate="TextBoxName" SetFocusOnError="True"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>Common Name:</td>
                        <td>
                            <asp:TextBox ID="TextBoxSlaCommonName" runat="server" Columns="50" Width="300px">
                            </asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <h2>Bandwidth</h2>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="LabelCIRFWD" runat="server">CIR forward:</asp:Label></td>
                        <td>
                            <telerik:RadNumericTextBox ID="RadNumericTextBoxCIRFWD" runat="server" ShowSpinButtons="True" Culture="nl-BE" Skin="WebBlue" Value="0" Width="125px">
                                <NumberFormat ZeroPattern="n" DecimalDigits="0"></NumberFormat>
                            </telerik:RadNumericTextBox><asp:Label ID="LabelKbps5" runat="server"> Kbps</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="LabelCIRRTN" runat="server">CIR return:</asp:Label></td>
                        <td>
                            <telerik:RadNumericTextBox ID="RadNumericTextBoxCIRRTN" runat="server" ShowSpinButtons="True" Culture="nl-BE" Skin="WebBlue" Value="0" Width="125px">
                                <NumberFormat ZeroPattern="n" DecimalDigits="0"></NumberFormat>
                            </telerik:RadNumericTextBox><asp:Label ID="LabelKbps6" runat="server"> Kbps</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="LabelForwardSpeed" runat="server">Forward speed:</asp:Label></td>
                        <td>
                            <telerik:RadNumericTextBox ID="RadNumericTextBoxForwardDefault" runat="server" ShowSpinButtons="True" Culture="nl-BE" Skin="WebBlue" Value="0" Width="125px">
                                <NumberFormat ZeroPattern="n" DecimalDigits="0"></NumberFormat>
                            </telerik:RadNumericTextBox><asp:Label ID="LabelKbps1" runat="server"> Kbps</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="LabelReturnSpeed" runat="server">Return speed:</asp:Label></td>
                        <td>
                            <telerik:RadNumericTextBox ID="RadNumericTextBoxReturnDefault" runat="server" ShowSpinButtons="True" Culture="nl-BE" Skin="WebBlue" Value="0" Width="125px">
                                <NumberFormat ZeroPattern="n" DecimalDigits="0"></NumberFormat>
                            </telerik:RadNumericTextBox><asp:Label ID="LabelKbps2" runat="server"> Kbps</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <h2>Options</h2>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="LabelWeight" runat="server">Weight:</asp:Label>
                        </td>
                        <td>
                            <telerik:RadNumericTextBox ID="RadNumericTextBoxWeight" runat="server" MinValue="0" MaxValue="100" Culture="en-US" ShowSpinButtons="True" Skin="WebBlue" Width="100px" Value="0">
                                <IncrementSettings Step="1" />
                                <NumberFormat AllowRounding="true" KeepTrailingZerosOnFocus="true" DecimalDigits="2" />
                            </telerik:RadNumericTextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Please specify the weight" ControlToValidate="radnumerictextboxweight"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="LabelForVNO" runat="server">For VNO:</asp:Label>
                        </td>
                        <td>
                            <telerik:RadComboBox ID="RadComboBoxVNOId" runat="server" Width="345px" ExpandDirection="Up"></telerik:RadComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="LabelHide" runat="server">Hide for distributor:</asp:Label></td>
                        <td>
                            <asp:CheckBox ID="CheckBoxHide" runat="server" ToolTip="This flag determines if the ServicePack is visible to the distributor or not" />
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td style="text-align:right;">
                            <telerik:RadButton runat="server" ID="RadButtonFinish" Text="Finish" OnClick="RadButtonFinish_Click"></telerik:RadButton>
                            <asp:Label runat="server" id="LabelResult"></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
        </telerik:RadAjaxPanel>
    </form>
</body>
</html>
