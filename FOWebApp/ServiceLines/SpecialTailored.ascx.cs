﻿using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOMonitorControlWSRef;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace FOWebApp.ServiceLines
{
    public partial class SpecialTailored : System.Web.UI.UserControl
    {
        public BOAccountingControlWS _boAccountingControlWS;

        protected void Page_Load(object sender, EventArgs e)
        {
            _boAccountingControlWS = new BOAccountingControlWS();

            //Load the VNO list
            RadComboBoxVNOId.Items.Clear();
            Distributor[] VNOList = _boAccountingControlWS.GetVNOs();
            RadComboBoxVNOId.DataValueField = "Id";
            RadComboBoxVNOId.DataTextField = "FullName";
            RadComboBoxVNOId.DataSource = VNOList;
            RadComboBoxVNOId.DataBind();
            RadComboBoxVNOId.Items.Insert(0, new RadComboBoxItem(""));
        }

        protected void ButtonFinish_Click(object sender, EventArgs e)
        {
            // automatically set VNO value if user is VNO
            if (HttpContext.Current.User.IsInRole("VNO"))
            {
                MembershipUser myObject = Membership.GetUser();
                string userId = myObject.ProviderUserKey.ToString();
                Session["VNO"] = (_boAccountingControlWS.GetDistributorForUser(userId).Id).ToString();
            }
            else
            {
                Session["VNO"] = RadComboBoxVNOId.SelectedValue.ToString();
            }

            //Create a new Service pack
            ServiceLevel serviceLevel = new ServiceLevel();
            serviceLevel.SlaId = Int32.Parse(Session["ServicePackID"].ToString());
            serviceLevel.SlaName = Session["ServicePackName"].ToString();
            serviceLevel.FupThreshold = Decimal.Parse(RadNumericTextBoxForwardFUP.ToString());
            serviceLevel.RTNFUPThreshold = Decimal.Parse(RadNumericTextBoxReturnFUP.ToString());
            serviceLevel.DRFWD = Int32.Parse(RadNumericTextBoxForwardDefault.ToString());
            serviceLevel.DRRTN = Int32.Parse(RadNumericTextBoxReturnDefault.ToString());
            serviceLevel.DrAboveFup = RadNumericTextBoxForwardAboveFup.ToString() + "/" + RadNumericTextBoxReturnAboveFup.ToString();
            serviceLevel.CIRFWD = Int32.Parse(RadNumericTextBoxCIRFWD.ToString());
            serviceLevel.CIRRTN = Int32.Parse(RadNumericTextBoxCIRRTN.ToString());
            //if (Session["ServicePackCommonName"].ToString() != null)
            //{
            //    serviceLevel.SlaCommonName = Session["ServicePackCommonName"].ToString();
            //}
            serviceLevel.IspId = Int32.Parse(Session["ISP"].ToString());
            serviceLevel.ServiceClass = Int32.Parse(Session["ServiceLine"].ToString());

            //if (serviceLevel.ServiceClass == 1)
            //{
            //    serviceLevel.Voucher = true;
            //    serviceLevel.Business = false;
            //}
            //else if (serviceLevel.ServiceClass == 2)
            //{
            //    serviceLevel.Business = true;
            //    serviceLevel.Voucher = false;
            //}
            //else if (serviceLevel.ServiceClass == 3)
            //{
            //    serviceLevel.Business = false;
            //    serviceLevel.Voucher = false;
            //}

            serviceLevel.VNOId = Int32.Parse(RadComboBoxVNOId.SelectedItem.Value);
            serviceLevel.FreeZone = CheckBoxFreeZone.Checked;
            serviceLevel.Hide = CheckBoxHide.Checked;
            serviceLevel.VoIPFlag = CheckBoxVoIP.Checked;

            //serviceLevel.ServicePackAmt = (float)(Convert.ToInt32(Session["BaseAmount"].ToString())) / 100;
            //serviceLevel.FreeZoneAmt = (float)(Convert.ToInt32(Session["FreeZoneAmount"].ToString())) / 100;
            //serviceLevel.VoIPAmt = (float)(Convert.ToInt32(Session["VoIPAmount"].ToString())) / 100;
            //serviceLevel.ServicePackAmtEUR = (float)(Convert.ToInt32(Session["BaseAmountEUR"].ToString())) / 100;
            //serviceLevel.FreeZoneAmtEUR = (float)(Convert.ToInt32(Session["FreeZoneAmountEUR"].ToString())) / 100;
            //erviceLevel.VoIPAmtEUR = (float)(Convert.ToInt32(Session["VoIPAmountEUR"].ToString())) / 100;
            serviceLevel.SLAWeight = (double)Convert.ToDouble(RadNumericTextBoxWeight.ToString());

            serviceLevel.MinBWFWD = int.Parse(RadNumericTextBoxMinBWFWD.ToString());
            serviceLevel.MinBWRTN = int.Parse(RadNumericTextBoxMinBWRTN.ToString());
            //serviceLevel.LowVolumeFWD = (decimal)(Convert.ToInt32(Session["LowVolFWD"].ToString())) / 100;
            //serviceLevel.LowVolumeRTN = (decimal)(Convert.ToInt32(Session["LowVolRTN"].ToString())) / 100;
            //serviceLevel.HighVolumeFWD = (decimal)(Convert.ToInt32(Session["HighVolFWD"].ToString())) / 100;
            //serviceLevel.HighVolumeRTN = (decimal)(Convert.ToInt32(Session["HighVolRTN"].ToString())) / 100;
            //serviceLevel.HighVolumeSUM = (decimal)(Convert.ToInt32(Session["HighVolSUM"].ToString())) / 100;
            //serviceLevel.LowVolumeSUM = (decimal)(Convert.ToInt32(Session["LowVolSUM"].ToString())) / 100;
            if (RadioButtonVolAgg.ToString() == "1")
            {
                serviceLevel.AggregateFlag = true;
            }
            else
            {
                serviceLevel.AggregateFlag = false;
            }

            //if (Session["EdgeISP"] != null)
            //{
            //    serviceLevel.EdgeISP = (int)Int32.Parse(Session["EdgeISP"].ToString());
            //}

            //if (Session["EdgeSLA"] != null)
            //{
            //    serviceLevel.EdgeSLA = (int)Int32.Parse(Session["EdgeSLA"].ToString());
            //}

            //The SerivcePack class is used for the NMS
            ServicePack servicePack = null;//this.serviceLevelToServicePack(serviceLevel);
            BOMonitorControlWS boMonitorControlWS = new BOMonitorControlWS();

            if (boMonitorControlWS.NMSUpdateServicePack(servicePack)) //will return true for all non-NMS SLA's as well
            {
                if (_boAccountingControlWS.UpdateServicePack(serviceLevel)) //writes the SLA to the CMT database
                {
                    LabelResult.ForeColor = Color.DarkGreen;
                    LabelResult.Text = "Servicepack update succeeded";
                }
                else
                {
                    LabelResult.ForeColor = Color.Red;
                    LabelResult.Text = "Servicepack update failed";
                }
            }
            else
            {
                LabelResult.ForeColor = Color.Red;
                LabelResult.Text = "Servicepack update failed";
            }
        }

    }
}