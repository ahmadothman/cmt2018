﻿using Hangfire;
using Hangfire.Common;
using Hangfire.Dashboard;
using Hangfire.States;
using Hangfire.Storage;
using Microsoft.Owin;
using Owin;
using System;
using System.Web.Security;

[assembly: OwinStartup(typeof(FOWebApp.StartupHangfire))]

namespace FOWebApp
{
    public class ProlongExpirationTimeAttribute : JobFilterAttribute, IApplyStateFilter
    {
        public void OnStateUnapplied(ApplyStateContext context, IWriteOnlyTransaction transaction)
        {
            context.JobExpirationTimeout = TimeSpan.FromDays(7);
        }

        public void OnStateApplied(ApplyStateContext context, IWriteOnlyTransaction transaction)
        {
            context.JobExpirationTimeout = TimeSpan.FromDays(7);
        }
    }

    public class StartupHangfire
    {
        public void Configuration(IAppBuilder app)
        {
            var dashboardOptions = new DashboardOptions
            {
                Authorization = new[] { new CmtAuthorizationFilter() }
            };
            // Map Dashboard to the `http://<your-app>/hangfire` URL.
            app.UseHangfireDashboard("/jobs", dashboardOptions);
        }
    }

    public class CmtAuthorizationFilter : IDashboardAuthorizationFilter
    {
        public bool Authorize(DashboardContext context)
        {
            // Allow all authenticated users to see the Dashboard (potentially dangerous).
            return Roles.IsUserInRole("NOC Administrator");
        }
    }
}
