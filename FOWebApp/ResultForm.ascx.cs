﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FOWebApp
{
    public partial class ResultForm : System.Web.UI.UserControl
    {
        string _resultMsg = "";

        public string ResultMsg
        {
            get { return _resultMsg; }
            set { _resultMsg = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            LabelResult.Text = _resultMsg;
        }
    }
}