﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TrafficClassificationTab.ascx.cs" Inherits="FOWebApp.TrafficClassificationTab" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<style type="text/css">
    .graphChart {
        height: 500px;
        width: 875px;
    }
</style>

<div style="padding-top: 5px; padding-bottom: 2px">
    <telerik:RadDatePicker ID="RadDatePickerStartDate" runat="server" Skin="Metro">
        <DateInput DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy" runat="server" />
    </telerik:RadDatePicker>
    <telerik:RadDatePicker ID="RadDatePickerEndDate" runat="server" Skin="Metro" Visible="true">
        <DateInput DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy" runat="server" />
    </telerik:RadDatePicker>
    <telerik:RadButton ID="RadButtonDateSelection" runat="server" Text="Show Chart" OnClick="RadButtonDateSelection_Click" Skin="Metro">
    </telerik:RadButton>
</div>
<div id='errorMessage' runat="server"></div>
<div id='chart_container' class="graphChart"></div>
<telerik:RadScriptBlock ID="RadScriptBlockHighchart" runat="server">
    <script type="text/javascript" src="../Scripts/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="../Scripts/highcharts.js"></script>
    <script type="text/javascript" src="../Scripts/exporting.js"></script>
    <script type="text/javascript" src="../Scripts/offline-exporting.js"></script>
   <%-- <script type="text/javascript">
        function TooltipFormatter() {
            var points = '<table class="tip"><caption>' + this.x + '</caption><tbody>';
            sum = 0;
            $.each(this.points, function (i, point) {
                points += '<tr><th style="color: ' + point.series.color + '">' + point.series.name + ': </th>'
                      + '<td style="text-align: right">' + Highcharts.numberFormat((point.y/1000), 1) + '</td></tr>';
                sum += point.y;
            });
            points += '<tr><th>Total: </th>'
            + '<td style="text-align:right"><b>' + Highcharts.numberFormat((sum/1000), 1) + '</b></td></tr>'
            + '</tbody></table>';
            return points;
        }
    </script>--%>
</telerik:RadScriptBlock>

