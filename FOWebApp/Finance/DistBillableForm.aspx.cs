﻿using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOInvoicingControllerWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace FOWebApp.Finance
{
    public partial class DistBillableForm : System.Web.UI.Page
    {
        private BOInvoicingControllerWS _invoiceController = new BOInvoicingControllerWS();
        private BOAccountingControlWS _accountingController = new BOAccountingControlWS();
        protected DistBillable _distBillable = new DistBillable();
        protected Billable _billableOfDistBillable = new Billable();
        protected net.nimera.cmt.BOInvoicingControllerWSRef.Account _accountOfBillable = new net.nimera.cmt.BOInvoicingControllerWSRef.Account();

        protected void Page_Init(object sender, EventArgs e)
        {
            int distBillableId = int.Parse(Request.QueryString["id"]);

            _distBillable = _invoiceController.GetDistBillable(distBillableId);

            _billableOfDistBillable = _invoiceController.GetBillable(_distBillable.Bid);

            _accountOfBillable = _invoiceController.GetAccount(_billableOfDistBillable.AccountId);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LiteralDistributor.Text = _accountingController.GetDistributor(_distBillable.DistId).FullName;
                RadNumericTextBoxPriceUsd.Value = (double)_distBillable.PriceUSD;
                RadNumericTextBoxPriceEur.Value = (double)_distBillable.PriceEU;
                RadTextBoxRemarks.Text = _distBillable.Remarks;
                
                //CMTINVOICE-73: Show linked SLAs when viewing billable details
                IEnumerable<ServiceLevel> slas = _accountingController.GetServicePacksByBillableId(_distBillable.Bid);
                IEnumerable<ListItem> listItems = slas.Select(sla => 
                    {
                        string ispName = (_accountingController.GetISP(sla.IspId.Value)).CompanyName;

                        return new ListItem 
                        {
                            Text = string.Format("{0} - {1} ({2})", ispName, sla.SlaName, sla.SlaId)
                        };
                    });
                //check error message
                //listItems = new ListItem[]{};

                //foreach (ServiceLevel sla in slas)
                //{
                //    string slaName = sla.SlaName;
                //    string slaId = sla.SlaId.ToString();
                //    string ispName = (_accountingController.GetISP(sla.IspId.Value)).CompanyName;
                //    string output = string.Format("{0} - {1} ({2})", ispName, slaName, slaId);
                //}

                if (listItems.Any())
                {
                    BulletedList slaList = new BulletedList { DataSource = listItems };
                    slaList.DataBind();
                    PanelLinkedSlas.Controls.Add(slaList); 
                }
                else
                {
                    LabelNoLinkedSlas.Visible = true;
                }

            }
        }

        protected void RadButtonSubmit_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                DistBillable updatedDistBillable = new DistBillable()
                {
                    Id = _distBillable.Id,
                    Bid = _distBillable.Bid,
                    DistId = _distBillable.DistId,
                    PriceUSD = (decimal)RadNumericTextBoxPriceUsd.Value,
                    PriceEU = (decimal)RadNumericTextBoxPriceEur.Value,
                    Remarks = RadTextBoxRemarks.Text,
                    UserId = new Guid(Membership.GetUser().ProviderUserKey.ToString()),
                    LastUpdate = DateTime.UtcNow,
                    Deleted = false
                };

                try
                {
                    if (_invoiceController.UpdateDistBillable(updatedDistBillable))
                    {
                        LabelResult.ForeColor = Color.DarkGreen;
                        LabelResult.Text = "Distributor billable updated succesfully";
                    }
                    else
                    {
                        LabelResult.ForeColor = Color.DarkRed;
                        LabelResult.Text = "Distributor billable could not be updated";
                    }
                }
                catch (Exception ex)
                {
                    BOLogControlWS boLogControlWS = new BOLogControlWS();

                    CmtApplicationException cmtApplicationException = new CmtApplicationException()
                    {
                        ExceptionDateTime = DateTime.Now,
                        ExceptionDesc = ex.Message,
                        ExceptionStacktrace = ex.StackTrace,
                        ExceptionLevel = 3,
                        UserDescription = "DistBillableForm: Update distributor billable"
                    };

                    boLogControlWS.LogApplicationException(cmtApplicationException);

                    LabelResult.ForeColor = Color.DarkRed;
                    LabelResult.Text = "Distributor billable could not be updated";
                }
            }
        }
    }
}