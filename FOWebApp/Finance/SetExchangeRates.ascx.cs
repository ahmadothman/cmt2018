﻿using FOWebApp.net.nimera.cmt.BOInvoicingControllerWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace FOWebApp.Finance
{
	public partial class SetExchangeRates : System.Web.UI.UserControl
	{
        private BOInvoicingControllerWS _invoiceController = new BOInvoicingControllerWS();
        private BOLogControlWS _logController = new BOLogControlWS();

		protected void Page_Load(object sender, EventArgs e)
		{
            RadNumericTextBoxExchangeRate.Text = _invoiceController.GetCurrentExchangeRate().ToString();
            HiddenFieldExchangeRate.Value = RadNumericTextBoxExchangeRate.Text;
		}



        protected void RadButtonSetExchangeRate_Click(object sender, EventArgs e)
        {
            try
            {
                decimal exchangeRate = Convert.ToDecimal(RadNumericTextBoxExchangeRate.Text);
                DateTime changedDate = DateTime.Now;
                MembershipUser myObject = Membership.GetUser();
                string userId = myObject.ProviderUserKey.ToString();

                _invoiceController.InsertExchangeRate(exchangeRate, changedDate, new Guid(userId));
                InvoiceHeader[] invoices = _invoiceController.GetUnvalidatedInvoices();
                foreach (InvoiceHeader header in invoices.Where(h => h.CurrencyCode != "EUR"))
                {
                    header.ExchangeRate = exchangeRate;
                    _invoiceController.UpdateInvoice(header);
                }

                LabelResult.ForeColor = Color.Green;
                LabelResult.Text = "Exchange rate saved succesfully";
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtApplicationException = new CmtApplicationException()
                {
                    ExceptionDateTime = DateTime.Now,
                    ExceptionDesc = ex.Message,
                    ExceptionStacktrace = ex.StackTrace,
                    ExceptionLevel = 3,
                    UserDescription = "InvoicingConfiguration: Save configuration settings"
                };

                _logController.LogApplicationException(cmtApplicationException);
                LabelResult.ForeColor = Color.Red;
                LabelResult.Text = "Failed to save the exchange rate";
            }
        }

        protected void RadGridExchangeRateHistory_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            ((RadGrid)sender).MasterTableView.DataSource = _invoiceController.GetAllExchangeRates().Select(rate => 
            {
                Guid userId = new Guid(rate.UserId.ToString());
                MembershipUser user = Membership.GetUser(userId);
                string userName = user != null ? user.UserName : string.Empty;

                return new 
                {
                    //rate.ID,
                    rate.ExchangeRate,
                    rate.ChangedDate,
                    UserName = userName
                };
            });
        }

        /*protected void RadGridExchangeRateHistory_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem item = e.Item as GridDataItem;
                TableCell cell = item["UserName"];
                exchangeRateHistory[] exchangeRates = _invoiceController.GetAllExchangeRates();
                
                foreach (exchangeRateHistory rate in exchangeRates)
                {
                    Guid userId = new Guid(rate.UserId.ToString());
                    MembershipUser user = Membership.GetUser(userId);

                    if (user != null)
                        cell.Text = user.UserName;
                    else
                        cell.Text = "";
                }
            }
        }*/
	}
}