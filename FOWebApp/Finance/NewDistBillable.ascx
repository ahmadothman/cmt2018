﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NewDistBillable.ascx.cs" Inherits="FOWebApp.Finance.NewDistBillable" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<telerik:RadScriptBlock ID="RadScriptBlockNewDistBillable" runat="server">
    <script>
        function associatedBillableText(sender, eventArgs) {
            eventArgs.IsValid = false;

            var comboBox = $find(sender.controltovalidate)
            var selectedBillable = comboBox.get_selectedItem();

            if (selectedBillable != undefined && selectedBillable != null) {
                var selectedBillableId = selectedBillable.get_value();

                if (selectedBillableId != undefined && selectedBillableId != null && selectedBillableId != '') {
                    eventArgs.IsValid = true;
                }
            }
        }

        function associatedBillableIndexChanged(sender, eventArgs) {
            var customValidator = $telerik.$('[id$="CustomValidatorAssociatedBillabe"]')[0];
            window.ValidatorValidate(customValidator);

            var selectedItem = sender.get_selectedItem();
            var billablePricePanel = $find('<%= RadXmlHttpPanelBillablePrice.ClientID %>');
            billablePricePanel.set_value(selectedItem.get_value());
        }
    </script>
</telerik:RadScriptBlock>

<telerik:RadToolTip ID="RadToolTipDisabledPrice" runat="server" ShowEvent="OnMouseOver"
    HideEvent="LeaveTargetAndToolTip" TargetControlID="ImagePriceTooltip" Position="BottomCenter"
    RelativeTo="Element" Text="You must select a billable before you can enter the price">
</telerik:RadToolTip>

<table class="table-spaced">
    <tr>
        <td class="requiredLabel">Associated Billable</td>
        <td>
            <telerik:RadComboBox ID="RadComboBoxAssociatedBillable" runat="server" Width="500"
                AllowCustomText="true" DropDownWidth="500" Height="300" EnableLoadOnDemand="true"
                OnItemsRequested="RadComboBoxAssociatedBillable_ItemsRequested" MaxLength="50"
                EmptyMessage="Select a billable..." OnClientSelectedIndexChanged="associatedBillableIndexChanged" Skin="Metro">
            </telerik:RadComboBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorAssociatedBillable" runat="server"
                ControlToValidate="RadComboBoxAssociatedBillable" ErrorMessage="Please select a billable" Display="Dynamic">
            </asp:RequiredFieldValidator>
            <asp:CustomValidator ID="CustomValidatorAssociatedBillabe" runat="server" ControlToValidate="RadComboBoxAssociatedBillable"
                Display="Dynamic" ErrorMessage="Please select a billable from the dropdownlist instead of using custom text"
                ClientValidationFunction="associatedBillableText" OnServerValidate="CustomValidatorAssociatedBillabe_ServerValidate">
            </asp:CustomValidator>
        </td>
    </tr>
    <tr>
        <td class="requiredLabel">Distributor
        </td>
        <td>
            <telerik:RadDropDownList ID="RadDropDownListDistributor" runat="server"
                DefaultMessage="Select a distributor..." Width="500" DropDownWidth="500" DropDownHeight="250">
            </telerik:RadDropDownList>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorDistributor" runat="server"
                ControlToValidate="RadDropDownListDistributor" ErrorMessage="Please select a distributor" Display="Dynamic"></asp:RequiredFieldValidator></td>
    </tr>
    <tr>
        <td class="requiredLabel">Price
        </td>
        <td>
            <telerik:RadXmlHttpPanel ID="RadXmlHttpPanelBillablePrice" runat="server" Enabled="false"
                EnableClientScriptEvaluation="true" OnServiceRequest="RadXmlHttpPanelBillablePrice_ServiceRequest">
                <telerik:RadNumericTextBox ID="RadNumericTextBoxPriceUsd" runat="server" Type="Currency" Width="200" Value="0">
                    <NegativeStyle ForeColor="Red" />
                    <NumberFormat AllowRounding="true" KeepTrailingZerosOnFocus="true" DecimalDigits="2"
                        GroupSeparator=" " GroupSizes="3"
                        ZeroPattern="$ n" PositivePattern="$ n" NegativePattern="$ -n" />
                </telerik:RadNumericTextBox>
                <telerik:RadNumericTextBox ID="RadNumericTextBoxPriceEur" runat="server" Type="Currency" Width="200" Value="0">
                    <NegativeStyle ForeColor="Red" />
                    <NumberFormat AllowRounding="true" KeepTrailingZerosOnFocus="true" DecimalDigits="2"
                        GroupSeparator=" " GroupSizes="3"
                        ZeroPattern="€ n" PositivePattern="€ n" NegativePattern="€ -n" />
                </telerik:RadNumericTextBox>
            </telerik:RadXmlHttpPanel>
            <asp:Image ID="ImagePriceTooltip" runat="server" ImageUrl="~/Images/help2.png" AlternateText="Price Help" Style="cursor: help;" />
        </td>
        <td></td>
    </tr>
    <tr>
        <td>Remarks
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxRemarks" runat="server" Columns="100" Rows="8" TextMode="MultiLine"
                Resize="Vertical">
            </telerik:RadTextBox>
        </td>
        <td></td>
    </tr>
    <tr>
        <td>
            <telerik:RadButton ID="RadButtonSubmit" runat="server" Text="Submit" OnClick="RadButtonSubmit_Click"></telerik:RadButton>
        </td>
        <td colspan="2">
            <asp:Label ID="LabelResult" runat="server"></asp:Label>
        </td>
    </tr>
</table>
