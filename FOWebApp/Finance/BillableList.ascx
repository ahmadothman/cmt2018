﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BillableList.ascx.cs" Inherits="FOWebApp.Finance.BillableList" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<telerik:RadScriptBlock ID="RadScriptBlockBillableList" runat="server">
    <script>
        function RowSelected(sender, eventArgs) {
            switch (eventArgs.get_tableView().get_name()) {
                case 'Billables':
                    var billableId = eventArgs.getDataKeyValue('Bid');
                    var oWnd = radopen('<%= this.ResolveUrl("~/Finance/BillableForm.aspx") %>?id=' + billableId, 'RadWindowModifyBillable');

                    break;
                default:
                    console.log('Not implemented!');
                    break;
            }
        }

        function deleteBillableConfirm(sender, eventArgs) {
            var processDelete = Function.createDelegate(sender, function (shouldSubmit) {
                if (shouldSubmit === true) {
                    this.click();
                }
            });

            eventArgs.set_cancel(true);

            radconfirm('Are you sure you want to delete this billable?', processDelete, 300, 200, null, 'Delete Billable');
        }

        function deleteAlertCallback(arg) {
            // Maybe do something here when user clicked 'OK'
        }
    </script>
</telerik:RadScriptBlock>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxyBillableList" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="RadGridBillableList">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGridBillableList" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<%-- Using the loading panel in Default.aspx --%>
<%--<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanelBillableList" runat="server"></telerik:RadAjaxLoadingPanel>--%>

<telerik:RadGrid ID="RadGridBillableList" runat="server" OnNeedDataSource="RadGridBillableList_NeedDataSource"
    AutoGenerateColumns="false" OnItemCommand="RadGridBillableList_ItemCommand" Skin="Metro">
    <ClientSettings EnableAlternatingItems="true" EnableRowHoverStyle="true">
        <Resizing AllowColumnResize="true" EnableRealTimeResize="true" ClipCellContentOnResize="false" ResizeGridOnColumnResize="false" />
        <Selecting AllowRowSelect="true" />
        <ClientEvents OnRowSelected="RowSelected" />
    </ClientSettings>
    <MasterTableView Name="Billables" DataKeyNames="Bid" ClientDataKeyNames="Bid" CommandItemDisplay="Top">
        <Columns>
            <telerik:GridBoundColumn UniqueName="Bid"
                HeaderText="ID" DataField="Bid">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn UniqueName="Description"
                HeaderText="Description" DataField="Description">
            </telerik:GridBoundColumn>
            <telerik:GridTemplateColumn UniqueName="Price" HeaderText="Price">
                <ItemTemplate>
                    <%# Eval("PriceUSD", "$ {0:0.00}")  %> / <%# Eval("PriceEU", "€ {0:0.00}") %>
                </ItemTemplate>
            </telerik:GridTemplateColumn>
            <%--<telerik:GridTemplateColumn UniqueName="Valid" HeaderText="Valid">
                <ItemTemplate>
                    <asp:Literal ID="LiteralValidFrom" runat="server" Text='<%# Eval("ValidFromDate", "from {0:dd/MM/yyyy}") %>' />
                    <asp:Literal ID="LiteralValidTo" runat="server" Text='<%# Eval("ValidToDate", "until {0:dd/MM/yyyy}") %>' />
                </ItemTemplate>
            </telerik:GridTemplateColumn>--%>
            <telerik:GridTemplateColumn UniqueName="DeleteBillable" HeaderText="Delete" Exportable="false">
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                <ItemTemplate>
                    <telerik:RadButton ID="RadButtonBillableDelete" runat="server" ToolTip="Delete the billable"
                        Text="Delete" OnClientClicking="deleteBillableConfirm"
                        Width="24" Height="24" CommandName="DeleteBillable" CommandArgument='<%# Eval("Bid") %>'>
                        <Image ImageUrl="~/Images/delete.png" />
                    </telerik:RadButton>
                </ItemTemplate>
            </telerik:GridTemplateColumn>
        </Columns>
        <SortExpressions>
            <telerik:GridSortExpression FieldName="Bid" SortOrder="Ascending" />
        </SortExpressions>
        <CommandItemSettings ShowAddNewRecordButton="false" ShowExportToCsvButton="true" />
    </MasterTableView>
    <ExportSettings ExportOnlyData="false" IgnorePaging="true" OpenInNewWindow="true" FileName="Billables">
    </ExportSettings>
</telerik:RadGrid>

<telerik:RadWindowManager ID="RadWindowManagerBillableList" runat="server" Modal="true" Animation="None"
    DestroyOnClose="true" KeepInScreenBounds="true" EnableShadow="true" VisibleStatusbar="true" Width="850"
    Height="655" ShowContentDuringLoad="false" Skin="Metro">
    <Windows>
        <telerik:RadWindow ID="RadWindowModifyBillable" runat="server" Title="Modify Billable">
        </telerik:RadWindow>
    </Windows>
</telerik:RadWindowManager>
