﻿using FOWebApp.HelperMethods;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOInvoicingControllerWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;
using System;
using System.Drawing;
using Telerik.Web.UI;

namespace FOWebApp.Finance
{
    public partial class DistributorList : System.Web.UI.UserControl
    {
        private BOInvoicingControllerWS _invoiceController = new BOInvoicingControllerWS();
        private BOAccountingControlWS _accountingController = new BOAccountingControlWS();
        private RadGridHelper _radGridHelper = new RadGridHelper("F_DistributorList");

        protected void RadGridDistributorList_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            ((RadGrid)sender).MasterTableView.DataSource = _accountingController.GetDistributors();
        }

        protected void RadGridDistributorList_DetailTableDataBind(object sender, Telerik.Web.UI.GridDetailTableDataBindEventArgs e)
        {
            switch (e.DetailTableView.Name)
            {
                case "DistBillables":

                    GridDataItem dataItem = e.DetailTableView.ParentItem;

                    e.DetailTableView.DataSource =
                        _invoiceController.GetDistBillablesForDistributor((int)dataItem.GetDataKeyValue("Id"));
                    break;
            }
        }

        protected void RadGridDistributorList_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case RadGrid.ExpandCollapseCommandName:
                    if (!e.Item.Expanded)
                    {
                        _radGridHelper.SaveExpandedState(e.Item.ItemIndexHierarchical);
                    }
                    else
                    {
                        _radGridHelper.ClearExpandedState(e.Item.ItemIndexHierarchical);
                    }

                    break;
                case "DeleteDistBillable":
                case "DeleteDistributor":
                    int id = int.Parse(e.CommandArgument.ToString());

                    Color alertForeColor = Color.Empty;
                    string alertText;

                    try
                    {
                        bool deletionResult = false;
                        alertForeColor = Color.DarkRed;
                        alertText = "Deletion failed";

                        switch (e.CommandName)
                        {
                            case "DeleteDistBillable":
                                deletionResult = _invoiceController.DeleteDistBillableById(id);
                                break;
                            case "DeleteDistributor":
                                deletionResult = _accountingController.DeleteDistributor(id);
                                break;
                        }

                        if (deletionResult)
                        {
                            alertForeColor = Color.DarkGreen;
                            alertText = "Deletion succesful";

                            _radGridHelper.ClearExpandedState(e.Item.ItemIndexHierarchical);

                            RadGridDistributorList.DataSource = null; // Need to do this, otherwise the NeedDataSource event is not triggered on rebind
                            RadGridDistributorList.Rebind();
                        }
                    }
                    catch (Exception ex)
                    {
                        BOLogControlWS boLogControlWS = new BOLogControlWS();

                        CmtApplicationException cmtApplicationException = new CmtApplicationException()
                        {
                            ExceptionDateTime = DateTime.Now,
                            ExceptionDesc = ex.Message,
                            ExceptionStacktrace = ex.StackTrace,
                            ExceptionLevel = 3,
                            UserDescription = "DistributorList: Delete distributor / distBillable"
                        };

                        boLogControlWS.LogApplicationException(cmtApplicationException);

                        alertForeColor = Color.DarkRed;
                        alertText = "Deletion failed";
                    }

                    RadWindowManagerDistributorList.RadAlert(string.Format("<span style=\"color: {0}\">{1}</span>",
                        ColorTranslator.ToHtml(alertForeColor), alertText), 300, 200, "Deletion Result", "deleteAlertCallback");

                    break;
            }
        }

        protected void RadGridDistributorList_DataBound(object sender, EventArgs e)
        {
            _radGridHelper.LoadExpandedState(sender as RadGrid);
        }
    }
}