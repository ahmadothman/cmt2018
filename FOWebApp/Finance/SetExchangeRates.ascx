﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SetExchangeRates.ascx.cs" Inherits="FOWebApp.Finance.SetExchangeRates" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<telerik:RadScriptBlock ID="RadScriptBlockInvoiceList" runat="server">
    <script>
        function clientExecuteQuery() {
            $telerik.$(<%= LabelResult.ClientID %>)
                .html('Executing query...')
                .css('color', 'DarkBlue');
        }
    </script>
</telerik:RadScriptBlock>

<h1>Set exchange rate</h1>
<table class="table-spaced">
    <tr>
        <asp:Label ID="LabelInformation" runat="server" Text="Here you can set the exchange rate for all unvalidated invoices."></asp:Label>
    </tr>
    <tr ID="ExchangeRateRow" runat="server">
        <td class="italic">Exchange Rate</td>
        <td>
            1 USD =
            <telerik:RadNumericTextBox ID="RadNumericTextBoxExchangeRate" runat="server" Type="Number" Width="100" MinValue="0">
                <NumberFormat AllowRounding="true" DecimalDigits="5" GroupSeparator=" " GroupSizes="3" />
            </telerik:RadNumericTextBox>
            EUR
        </td>
    </tr>
    <tr>
        <td>
            <telerik:RadButton ID="RadButtonSetExchangeRate" runat="server" Text="Set exchange rate" 
                OnClientClicked="clientExecuteQuery" OnClick="RadButtonSetExchangeRate_Click"></telerik:RadButton>
        </td>
        <td>
            <asp:Label ID="LabelResult" runat="server"></asp:Label>
        </td>
    </tr>
    <asp:HiddenField ID="HiddenFieldExchangeRate" runat="server" />
</table>

<telerik:RadAjaxManager ID="RadAjaxManagerExchangeRateHistory" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="RadGridExchangeRateHistory">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGridExchangeRateHistory" LoadingPanelID="RadAjaxLoadingPanelExchangeRateHistory" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManager>

<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanelExchangeRateHistory" runat="server" Skin="Metro"></telerik:RadAjaxLoadingPanel>

<telerik:RadGrid ID="RadGridExchangeRateHistory" runat="server" 
    AutoGenerateColumns="false" AllowSorting="True" Width="500" 
    OnNeedDataSource="RadGridExchangeRateHistory_NeedDataSource">
    <MasterTableView Name="ExchangeRateHistory" CommandItemDisplay="Top">
        <Columns>
            <telerik:GridBoundColumn UniqueName="ExchangeRate"
                HeaderText="Exchange Rate" DataField="ExchangeRate"
                HeaderStyle-Width="100px">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn UniqueName="ChangedDate"
                HeaderText="Set on" DataField="ChangedDate"
                DataFormatString="{0:dd/MM/yyyy HH:mm:ss}"
                HeaderStyle-Width="150px">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn UniqueName="UserName" 
                HeaderText="User name" DataField="UserName">
            </telerik:GridBoundColumn>
        </Columns>
        <SortExpressions>
            <telerik:GridSortExpression FieldName="ChangedDate" SortOrder="Descending" />
        </SortExpressions>
        <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
    </MasterTableView>
</telerik:RadGrid>