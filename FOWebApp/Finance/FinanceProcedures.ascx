﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FinanceProcedures.ascx.cs" Inherits="FOWebApp.Finance.FinanceProcedures" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Panel ID="Panel1" runat="server" BorderStyle="Solid" GroupingText="Billing procedure">
    <table>
        <tr>
            <td>Last invoice number:
                <asp:TextBox ID="TextBoxLastInvoiceNumber" runat="server"></asp:TextBox>
            </td>
            <td>
                <telerik:RadButton ID="RadButtonBilling" runat="server" Text="Start billing process" ForeColor="Red"></telerik:RadButton>
            </td>
            <td>
                <telerik:RadButton ID="RadButtonRollBack" runat="server" Text="Rollback last billing process" ForeColor="Red"></telerik:RadButton>
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:Panel ID="Panel2" runat="server" BorderStyle="Solid" GroupingText="Invoice release procedure">
    <table>
        <tr>
            <td>Number of verified invoices:
                <asp:Label ID="LabelVerifiedInvoices" runat="server"></asp:Label>
            </td>
            <td>
                <telerik:RadButton ID="RadButtonRelease" runat="server" Text="Release verified invoices"></telerik:RadButton>
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:Panel ID="Panel3" runat="server" BorderStyle="Solid" GroupingText="Export">
    <table>
        <tr>
            <td>Year: 
                <telerik:RadComboBox ID="RadComboBoxYear" runat="server" Skin="Metro" EmptyMessage="None">
                    <Items>
                        <telerik:RadComboBoxItem runat="server" Text="2011" Value="2011" />
                        <telerik:RadComboBoxItem runat="server" Text="2012" Value="2012" />
                        <telerik:RadComboBoxItem runat="server" Text="2013" Value="2013" />
                        <telerik:RadComboBoxItem runat="server" Text="2014" Value="2014" />
                        <telerik:RadComboBoxItem runat="server" Text="2015" Value="2015" />
                        <telerik:RadComboBoxItem runat="server" Text="2016" Value="2016" />
                        <telerik:RadComboBoxItem runat="server" Text="2017" Value="2017" />
                        <telerik:RadComboBoxItem runat="server" Text="2018" Value="2018" />
                        <telerik:RadComboBoxItem runat="server" Text="2019" Value="2019" />
                        <telerik:RadComboBoxItem runat="server" Text="2020" Value="2020" />
                        <telerik:RadComboBoxItem runat="server" Text="2021" Value="2021" />
                        <telerik:RadComboBoxItem runat="server" Text="2022" Value="2022" />
                        <telerik:RadComboBoxItem runat="server" Text="2023" Value="2023" />
                        <telerik:RadComboBoxItem runat="server" Text="2024" Value="2024" />
                        <telerik:RadComboBoxItem runat="server" Text="2025" Value="2025" />
                        <telerik:RadComboBoxItem runat="server" Text="2026" Value="2026" />
                        <telerik:RadComboBoxItem runat="server" Text="2027" Value="2027" />
                        <telerik:RadComboBoxItem runat="server" Text="2028" Value="2028" />
                        <telerik:RadComboBoxItem runat="server" Text="2029" Value="2029" />
                        <telerik:RadComboBoxItem runat="server" Text="2030" Value="2030" />
                        <telerik:RadComboBoxItem runat="server" Text="2031" Value="2031" />
                        <telerik:RadComboBoxItem runat="server" Text="2032" Value="2032" />
                    </Items>
                </telerik:RadComboBox>
            </td>
            <td>
                Month:
                <telerik:RadComboBox ID="RadComboBoxMonth" runat="server" Skin="Metro" EmptyMessage="None">
                <Items>
                    <telerik:RadComboBoxItem runat="server" Text="January" Value="1" />
                    <telerik:RadComboBoxItem runat="server" Text="February" Value="2" />
                    <telerik:RadComboBoxItem runat="server" Text="March" Value="3" />
                    <telerik:RadComboBoxItem runat="server" Text="April" Value="4" />
                    <telerik:RadComboBoxItem runat="server" Text="May" Value="5" />
                    <telerik:RadComboBoxItem runat="server" Text="June" Value="6" />
                    <telerik:RadComboBoxItem runat="server" Text="July" Value="7" />
                    <telerik:RadComboBoxItem runat="server" Text="August" Value="8" />
                    <telerik:RadComboBoxItem runat="server" Text="September" Value="9" />
                    <telerik:RadComboBoxItem runat="server" Text="October" Value="10" />
                    <telerik:RadComboBoxItem runat="server" Text="November" Value="11" />
                    <telerik:RadComboBoxItem runat="server" Text="December" Value="12" />
                </Items>
            </telerik:RadComboBox>
            </td>
            <td>
                <telerik:RadButton ID="RadButtonExport" runat="server" Text="Export as CSV file"></telerik:RadButton>
            </td>
        </tr>
    </table>
</asp:Panel>


