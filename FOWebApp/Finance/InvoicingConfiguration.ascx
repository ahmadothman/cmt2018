﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InvoicingConfiguration.ascx.cs" Inherits="FOWebApp.Finance.InvoicingConfiguration" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<table class="table-spaced">
    <%--<tr>
        <td>Last Invoice Number:</td>
        <td>
            <telerik:RadNumericTextBox ID="RadNumericTextBoxLastInvoiceNumber" runat="server" Width="50" NumberFormat-DecimalDigits="0"></telerik:RadNumericTextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorLastInvoiceNumber" runat="server"
                ControlToValidate="RadNumericTextBoxLastInvoiceNumber" ErrorMessage="This field cannot be empty" Display="Dynamic"></asp:RequiredFieldValidator>
        </td>
    </tr>--%>
    <tr>
        <td>Company Name:</td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxCompanyName" runat="server" Width="200"></telerik:RadTextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorCompanyName" runat="server"
                ControlToValidate="RadTextBoxCompanyName" ErrorMessage="This field cannot be empty" Display="Dynamic"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>Address Line 1:</td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxAddressLine1" runat="server" Width="250"></telerik:RadTextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorAddressLine1" runat="server"
                ControlToValidate="RadTextBoxAddressLine1" ErrorMessage="This field cannot be empty" Display="Dynamic"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>Address Line 2:</td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxAddressLine2" runat="server" Width="250"></telerik:RadTextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorAddressLine2" runat="server"
                ControlToValidate="RadTextBoxAddressLine2" ErrorMessage="This field cannot be empty" Display="Dynamic"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>Postcode:</td>
        <td>
            <telerik:RadNumericTextBox ID="RadNumericTextBoxPostcode" runat="server" Width="70" 
                NumberFormat-DecimalDigits="0" NumberFormat-GroupSeparator=""></telerik:RadNumericTextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorPostcode" runat="server"
                ControlToValidate="RadNumericTextBoxPostcode" ErrorMessage="This field cannot be empty" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="RangeValidatorPostcode" runat="server" MinimumValue="1000" MaximumValue="9999"
                ControlToValidate="RadNumericTextBoxPostcode" ErrorMessage="Please enter a number between 1000 and 9999" Display="Dynamic"></asp:RangeValidator>
        </td>
    </tr>
    <tr>
        <td>Location:</td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxLocation" runat="server" Width="250"></telerik:RadTextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorLocation" runat="server"
                ControlToValidate="RadTextBoxLocation" ErrorMessage="This field cannot be empty" Display="Dynamic"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>Country:</td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxCountry" runat="server" Width="150"></telerik:RadTextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorCountry" runat="server"
                ControlToValidate="RadTextBoxCountry" ErrorMessage="This field cannot be empty" Display="Dynamic"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>VAT:</td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxVAT" runat="server" Width="150" Style="text-transform: uppercase;"></telerik:RadTextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorVAT" runat="server"
                ControlToValidate="RadTextBoxVAT" ErrorMessage="This field cannot be empty" Display="Dynamic"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr >
        <td colspan="3"></td>
    </tr>
    <tr>
        <td>Details message:</td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxDetailsMessage" runat="server" Width="400"></telerik:RadTextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorDetailsMessage" runat="server"
                ControlToValidate="RadTextBoxDetailsMessage" ErrorMessage="This field cannot be empty" Display="Dynamic"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>Important message:</td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxImportantMessage" runat="server" Width="400"></telerik:RadTextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorImportantMessage" runat="server"
                ControlToValidate="RadTextBoxImportantMessage" ErrorMessage="This field cannot be empty" Display="Dynamic"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>Informative message:</td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxInformativeMessage" runat="server" Width="400"></telerik:RadTextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorInformativeMessage" runat="server"
                ControlToValidate="RadTextBoxInformativeMessage" ErrorMessage="This field cannot be empty" Display="Dynamic"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>Terms and Conditions:</td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxTermsAndConditions" runat="server" Width="400"></telerik:RadTextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorTermsAndConditions" runat="server"
                ControlToValidate="RadTextBoxTermsAndConditions" ErrorMessage="This field cannot be empty" Display="Dynamic"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>Thank you message:</td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxThanksMessage" runat="server" Width="400"></telerik:RadTextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorThanksMessage" runat="server"
                ControlToValidate="RadTextBoxThanksMessage" ErrorMessage="This field cannot be empty" Display="Dynamic"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            <telerik:RadButton ID="RadButtonSaveConfiguration" runat="server" Text="Save Configuration Settings" OnClick="RadButtonSaveConfiguration_Click"></telerik:RadButton>
        </td>
        <td colspan="2">
            <asp:Label ID="LabelResult" runat="server"></asp:Label>
        </td>
    </tr>
</table>
