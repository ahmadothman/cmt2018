﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using FOWebApp.net.nimera.cmt.BOInvoicingControllerWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;

namespace FOWebApp.Finance
{
    public partial class NewAccount : System.Web.UI.UserControl
    {
        BOInvoicingControllerWS _invoiceController = new BOInvoicingControllerWS();

        protected void CustomValidatorDescription_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (args.Value.Length > 60)
            {
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }
        }

        protected void RadButtonSubmit_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                FOWebApp.net.nimera.cmt.BOInvoicingControllerWSRef.Account newAccount = new FOWebApp.net.nimera.cmt.BOInvoicingControllerWSRef.Account()
                {
                    Description = RadTextBoxDescription.Text,
                    Remarks = RadTextBoxRemarks.Text,
                    UserId = new Guid(Membership.GetUser().ProviderUserKey.ToString()),
                    LastUpdate = DateTime.UtcNow,
                    Deleted = false
                };

                try
                {
                    if (_invoiceController.AddAccount(newAccount))
                    {
                        LabelResult.ForeColor = Color.DarkGreen;
                        LabelResult.Text = "Account added succesfully";
                    }
                    else
                    {
                        LabelResult.ForeColor = Color.DarkRed;
                        LabelResult.Text = "Account could not be added";
                    }
                }
                catch (Exception ex)
                {
                    BOLogControlWS boLogControlWS = new BOLogControlWS();

                    CmtApplicationException cmtApplicationException = new CmtApplicationException() 
                    {
                        ExceptionDateTime = DateTime.Now,
                        ExceptionDesc = ex.Message,
                        ExceptionStacktrace = ex.StackTrace,
                        ExceptionLevel = 3,
                        UserDescription = "NewAccount: Create new billables account"
                    };

                    boLogControlWS.LogApplicationException(cmtApplicationException);

                    LabelResult.ForeColor = Color.DarkRed;
                    LabelResult.Text = "Account could not be added";
                }
            }
        }
    }
}