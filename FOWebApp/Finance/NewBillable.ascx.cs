﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using FOWebApp.net.nimera.cmt.BOInvoicingControllerWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;
using Telerik.Web.UI;

namespace FOWebApp.Finance
{
    public partial class NewBillable : System.Web.UI.UserControl
    {
        BOInvoicingControllerWS _invoiceController = new BOInvoicingControllerWS();

        protected void Page_Load(object sender, EventArgs e) 
        {
            // Fill the RadDropDownListAccount with data

            RadDropDownListAccount.DataSource = _invoiceController.GetAccounts();

            RadDropDownListAccount.DataValueField = "AccountId";
            RadDropDownListAccount.DataTextField = "Description";

            RadDropDownListAccount.DataBind();

            //RadDatePickerValidFrom.DataBind();
            //RadDatePickerValidTo.DataBind();
        }        
        
        protected void CustomValidatorDescription_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (args.Value.Length > 250)
            {
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }
        }

        protected void RadButtonSubmit_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                Billable newBillable = new Billable()
                {
                    Description = RadTextBoxDescription.Text,
                    AccountId = int.Parse(RadDropDownListAccount.SelectedValue),
                    PriceUSD = (decimal)RadNumericTextBoxPriceUsd.Value,
                    PriceEU = (decimal)RadNumericTextBoxPriceEur.Value,
                    UserId = new Guid(Membership.GetUser().ProviderUserKey.ToString()),
                    LastUpdate = DateTime.UtcNow,
                    Deleted = false
                    //ValidFromDate = RadDatePickerValidFrom.SelectedDate,
                    //ValidToDate = RadDatePickerValidTo.SelectedDate
                };

                try
                {
                    if (_invoiceController.AddBillable(newBillable))
                    {
                        LabelResult.ForeColor = Color.DarkGreen;
                        LabelResult.Text = "Billable added succesfully";
                    }
                    else
                    {
                        LabelResult.ForeColor = Color.DarkRed;
                        LabelResult.Text = "Billable could not be added";
                    }
                }
                catch (Exception ex)
                {
                    BOLogControlWS boLogControlWS = new BOLogControlWS();

                    CmtApplicationException cmtApplicationException = new CmtApplicationException()
                    {
                        ExceptionDateTime = DateTime.Now,
                        ExceptionDesc = ex.Message,
                        ExceptionStacktrace = ex.StackTrace,
                        ExceptionLevel = 3,
                        UserDescription = "NewBillable: Create new billable"
                    };

                    boLogControlWS.LogApplicationException(cmtApplicationException);

                    LabelResult.ForeColor = Color.DarkRed;
                    LabelResult.Text = "Billable could not be added";
                }
            }
        }
    }
}