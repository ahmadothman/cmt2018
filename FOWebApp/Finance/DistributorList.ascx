﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DistributorList.ascx.cs" Inherits="FOWebApp.Finance.DistributorList" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<telerik:RadScriptBlock ID="RadScriptBlockDistributorList" runat="server">
    <script>
        function RowSelected(sender, eventArgs) {
            switch (eventArgs.get_tableView().get_name()) {
                case 'Distributors':
                    var distributorId = eventArgs.getDataKeyValue('Id');
                    var oWnd = radopen('<%= this.ResolveUrl("~/Nocsa/DistributorDetailsPage.aspx") %>?Id=' + distributorId, 'RadWindowModifyDistributor');

                    break;
                case 'DistBillables':
                    var distBillableId = eventArgs.getDataKeyValue('Id');
                    var oWnd = radopen('<%= this.ResolveUrl("~/Finance/DistBillableForm.aspx") %>?id=' + distBillableId, 'RadWindowModifyDistBillable');

                    break;
                default:
                    console.log('Not implemented!');
                    break;
            }
        }

        function deleteDistributorConfirm(sender, eventArgs) {
            var processDelete = Function.createDelegate(sender, function (shouldSubmit) {
                if (shouldSubmit === true) {
                    this.click();
                }
            });

            eventArgs.set_cancel(true);

            radconfirm('Are you sure you want to delete this distributor?', processDelete, 300, 200, null, 'Delete Distributor');
        }

        function deleteDistBillableConfirm(sender, eventArgs) {
            var processDelete = Function.createDelegate(sender, function (shouldSubmit) {
                if (shouldSubmit === true) {
                    this.click();
                }
            });

            eventArgs.set_cancel(true);

            radconfirm('Are you sure you want to delete this distributor billable?', processDelete, 300, 200, null, 'Delete Distributor Billable');
        }

        function deleteAlertCallback(arg) {
            // Maybe do something here when user clicked 'OK'
        }
    </script>
</telerik:RadScriptBlock>

<telerik:RadAjaxManager ID="RadAjaxManagerDistributorList" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="RadGridDistributorList">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGridDistributorList" LoadingPanelID="RadAjaxLoadingPanelDistributorList" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManager>

<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanelDistributorList" runat="server" Skin="Metro"></telerik:RadAjaxLoadingPanel>

<telerik:RadGrid ID="RadGridDistributorList" runat="server" OnNeedDataSource="RadGridDistributorList_NeedDataSource"
    OnDetailTableDataBind="RadGridDistributorList_DetailTableDataBind" OnDataBound="RadGridDistributorList_DataBound"
    OnItemCommand="RadGridDistributorList_ItemCommand" AutoGenerateColumns="false" Skin="Metro">
    <ClientSettings EnableAlternatingItems="true" EnableRowHoverStyle="true">
        <Resizing AllowColumnResize="true" EnableRealTimeResize="true" ClipCellContentOnResize="false" ResizeGridOnColumnResize="false" />
        <Selecting AllowRowSelect="true" />
        <ClientEvents OnRowSelected="RowSelected" />
    </ClientSettings>
    <MasterTableView Name="Distributors" DataKeyNames="Id" ClientDataKeyNames="Id" CommandItemDisplay="Top">
        <DetailTables>
            <telerik:GridTableView Name="DistBillables" HierarchyLoadMode="ServerOnDemand" DataKeyNames="Id" ClientDataKeyNames="Id">
                <ParentTableRelation>
                    <telerik:GridRelationFields MasterKeyField="Id" DetailKeyField="DistId" />
                </ParentTableRelation>
                <Columns>
                    <telerik:GridBoundColumn UniqueName="DistBillableId"
                        HeaderText="ID" DataField="Id" Visible="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn UniqueName="BillableId"
                        HeaderText="BillableId" DataField="Bid">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn UniqueName="Description"
                        HeaderText="Description" DataField="Billable.Description">
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn UniqueName="Price" HeaderText="Price">
                        <ItemTemplate>
                            <%# Eval("PriceUSD", "$ {0:0.00}")  %> / <%# Eval("PriceEU", "€ {0:0.00}") %>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn UniqueName="DeleteDistBillable" HeaderText="Delete">
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        <ItemTemplate>
                            <telerik:RadButton ID="RadButtonDistBillableDelete" runat="server" ToolTip="Delete the distributor billable"
                                Text="Delete" OnClientClicking="deleteDistBillableConfirm"
                                Width="24" Height="24" CommandName="DeleteDistBillable" CommandArgument='<%# Eval("Id") %>'>
                                <Image ImageUrl="~/Images/delete.png" />
                            </telerik:RadButton>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                </Columns>
                <SortExpressions>
                    <telerik:GridSortExpression FieldName="Id" SortOrder="Ascending" />
                </SortExpressions>
            </telerik:GridTableView>
        </DetailTables>
        <Columns>
            <telerik:GridBoundColumn UniqueName="DistributorId"
                HeaderText="ID" DataField="Id">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn UniqueName="DistributorName"
                HeaderText="Distributor Name" DataField="FullName">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn UniqueName="Phone"
                HeaderText="Phone" DataField="Phone">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn UniqueName="Fax"
                HeaderText="Fax" DataField="Fax">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn UniqueName="Email"
                HeaderText="E-mail" DataField="Email">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn UniqueName="Website"
                HeaderText="Website" DataField="WebSite">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn UniqueName="Vat"
                HeaderText="VAT" DataField="Vat">
            </telerik:GridBoundColumn>
            <telerik:GridTemplateColumn UniqueName="DeleteDistributor" HeaderText="Delete">
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                <ItemTemplate>
                    <telerik:RadButton ID="RadButtonDistributorDelete" runat="server" ToolTip="Delete the distributor"
                        Text="Delete" OnClientClicking="deleteDistributorConfirm"
                        Width="24" Height="24" CommandName="DeleteDistributor" CommandArgument='<%# Eval("Id") %>'>
                        <Image ImageUrl="~/Images/delete.png" />
                    </telerik:RadButton>
                </ItemTemplate>
            </telerik:GridTemplateColumn>
        </Columns>
        <SortExpressions>
            <telerik:GridSortExpression FieldName="FullName" SortOrder="Ascending" />
        </SortExpressions>
        <CommandItemSettings ShowAddNewRecordButton="false" />
    </MasterTableView>
</telerik:RadGrid>

<telerik:RadWindowManager ID="RadWindowManagerDistributorList" runat="server" Modal="true" Animation="None"
    DestroyOnClose="true" KeepInScreenBounds="true" EnableShadow="true" VisibleStatusbar="true" Width="850"
    Height="655" ShowContentDuringLoad="false" Skin="Metro">
    <Windows>
        <telerik:RadWindow ID="RadWindowModifyDistributor" runat="server" Title="Modify Distributor">
        </telerik:RadWindow>
        <telerik:RadWindow ID="RadWindowModifyDistBillable" runat="server" Title="Modify Distributor Billable">
        </telerik:RadWindow>
    </Windows>
</telerik:RadWindowManager>