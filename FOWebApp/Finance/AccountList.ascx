﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AccountList.ascx.cs" Inherits="FOWebApp.Finance.AccountList" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<telerik:RadScriptBlock ID="RadScriptBlockAccountList" runat="server">
    <script>
        function RowSelected(sender, eventArgs) {
            switch (eventArgs.get_tableView().get_name()) {
                case 'Accounts':
                    var accountId = eventArgs.getDataKeyValue('AccountId');
                    var oWnd = radopen('<%= this.ResolveUrl("~/Finance/AccountForm.aspx") %>?id=' + accountId, 'RadWindowModifyAccount');

                    break;
                case 'Billables':
                    var billableId = eventArgs.getDataKeyValue('Bid');
                    var oWnd = radopen('<%= this.ResolveUrl("~/Finance/BillableForm.aspx") %>?id=' + billableId, 'RadWindowModifyBillable');

                    break;
                default:
                    console.log('Not implemented!');
                    break;
            }
        }

        function deleteAccountConfirm(sender, eventArgs) {
            var processDelete = Function.createDelegate(sender, function (shouldSubmit) {
                if (shouldSubmit === true) {
                    this.click();
                }
            });

            eventArgs.set_cancel(true);

            radconfirm('Are you sure you want to delete this account?', processDelete, 300, 200, null, 'Delete Account');
        }

        function deleteBillableConfirm(sender, eventArgs) {
            var processDelete = Function.createDelegate(sender, function (shouldSubmit) {
                if (shouldSubmit === true) {
                    this.click();
                }
            });

            eventArgs.set_cancel(true);

            radconfirm('Are you sure you want to delete this billable?', processDelete, 300, 200, null, 'Delete Billable');
        }

        function deleteAlertCallback(arg) {
            // Maybe do something here when user clicked 'OK'
        }
    </script>
</telerik:RadScriptBlock>

<telerik:RadAjaxManager ID="RadAjaxManagerAccountList" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="RadGridAccountList">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGridAccountList" LoadingPanelID="RadAjaxLoadingPanelAccountList" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManager>

<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanelAccountList" runat="server" Skin="Metro"></telerik:RadAjaxLoadingPanel>

<telerik:RadGrid ID="RadGridAccountList" runat="server" OnNeedDataSource="RadGridAccountList_NeedDataSource"
    OnDetailTableDataBind="RadGridAccountList_DetailTableDataBind" OnDataBound="RadGridAccountList_DataBound"
    OnItemCommand="RadGridAccountList_ItemCommand" AutoGenerateColumns="false" Skin="Metro">
    <%-- AllowSorting="true" ShowGroupPanel="true" AllowFilteringByColumn="true"--%>
    <ClientSettings EnableAlternatingItems="true" EnableRowHoverStyle="true">
        <%-- AllowDragToGroup="true"--%>
        <Resizing AllowColumnResize="true" EnableRealTimeResize="true" ClipCellContentOnResize="false" ResizeGridOnColumnResize="false" />
        <%--<Scrolling AllowScroll="true" UseStaticHeaders="true" SaveScrollPosition="true"></Scrolling>--%>
        <Selecting AllowRowSelect="true" />
        <ClientEvents OnRowSelected="RowSelected" />
    </ClientSettings>
    <%--<GroupingSettings CaseSensitive="false" />--%><%-- ShowUnGroupButton="true"--%>
    <MasterTableView Name="Accounts" DataKeyNames="AccountId" ClientDataKeyNames="AccountId" CommandItemDisplay="Top">
        <DetailTables>
            <telerik:GridTableView Name="Billables" HierarchyLoadMode="ServerOnDemand" DataKeyNames="Bid" ClientDataKeyNames="Bid">
                <%-- AllowFilteringByColumn="false"--%>
                <ParentTableRelation>
                    <telerik:GridRelationFields MasterKeyField="AccountId" DetailKeyField="AccountId" />
                </ParentTableRelation>
                <Columns>
                    <telerik:GridBoundColumn UniqueName="Bid"
                        HeaderText="ID" DataField="Bid">
                    </telerik:GridBoundColumn>
                    <%-- AutoPostBackOnFilter="true" ShowFilterIcon="false" CurrentFilterFunction="EqualTo"--%>
                    <telerik:GridBoundColumn UniqueName="Description"
                        HeaderText="Description" DataField="Description">
                    </telerik:GridBoundColumn>
                    <%-- AutoPostBackOnFilter="true" ShowFilterIcon="false" CurrentFilterFunction="Contains"--%>
                    <telerik:GridTemplateColumn UniqueName="Price" HeaderText="Price">
                        <ItemTemplate>
                            <%# Eval("PriceUSD", "$ {0:0.00}")  %> / <%# Eval("PriceEU", "€ {0:0.00}") %>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <%-- AutoPostBackOnFilter="true" ShowFilterIcon="false" CurrentFilterFunction="Contains"--%>
                    <%--<telerik:GridTemplateColumn UniqueName="Valid" HeaderText="Valid">
                        <ItemTemplate>
                            <asp:Literal ID="LiteralValidFrom" runat="server" Text='<%# Eval("ValidFromDate", "from {0:dd/MM/yyyy}") %>' />
                            <asp:Literal ID="LiteralValidTo" runat="server" Text='<%# Eval("ValidToDate", "until {0:dd/MM/yyyy}") %>' />
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>--%>
                    <%-- AutoPostBackOnFilter="true" ShowFilterIcon="false" CurrentFilterFunction="Contains"--%>
                    <telerik:GridTemplateColumn UniqueName="DeleteBillable" HeaderText="Delete">
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        <ItemTemplate>
                            <telerik:RadButton ID="RadButtonBillableDelete" runat="server" ToolTip="Delete the billable"
                                Text="Delete" OnClientClicking="deleteBillableConfirm"
                                Width="24" Height="24" CommandName="DeleteBillable" CommandArgument='<%# Eval("Bid") %>'>
                                <Image ImageUrl="~/Images/delete.png" />
                            </telerik:RadButton>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                </Columns>
                <SortExpressions>
                    <telerik:GridSortExpression FieldName="Bid" SortOrder="Ascending" />
                </SortExpressions>
            </telerik:GridTableView>
        </DetailTables>
        <Columns>
            <telerik:GridBoundColumn UniqueName="AccountId"
                HeaderText="ID" DataField="AccountId">
            </telerik:GridBoundColumn>
            <%-- AutoPostBackOnFilter="true" ShowFilterIcon="false" CurrentFilterFunction="EqualTo"--%>
            <telerik:GridBoundColumn UniqueName="Description"
                HeaderText="Description" DataField="Description">
            </telerik:GridBoundColumn>
            <%-- AutoPostBackOnFilter="true" ShowFilterIcon="false" CurrentFilterFunction="Contains"--%>
            <telerik:GridTemplateColumn UniqueName="DeleteAccount" HeaderText="Delete">
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                <ItemTemplate>
                    <telerik:RadButton ID="RadButtonAccountDelete" runat="server" ToolTip="Delete the account"
                        Text="Delete" OnClientClicking="deleteAccountConfirm"
                        Width="24" Height="24" CommandName="DeleteAccount" CommandArgument='<%# Eval("AccountId") %>'>
                        <Image ImageUrl="~/Images/delete.png" />
                    </telerik:RadButton>
                </ItemTemplate>
            </telerik:GridTemplateColumn>
        </Columns>
        <SortExpressions>
            <telerik:GridSortExpression FieldName="AccountId" SortOrder="Ascending" />
        </SortExpressions>
        <CommandItemSettings ShowAddNewRecordButton="false" />
    </MasterTableView>
</telerik:RadGrid>

<telerik:RadWindowManager ID="RadWindowManagerAccountList" runat="server" Modal="true" Animation="None"
    DestroyOnClose="true" KeepInScreenBounds="true" EnableShadow="true" VisibleStatusbar="true" Width="850"
    Height="655" ShowContentDuringLoad="false">
    <Windows>
        <telerik:RadWindow ID="RadWindowModifyAccount" runat="server" Title="Modify Account">
        </telerik:RadWindow>
        <telerik:RadWindow ID="RadWindowModifyBillable" runat="server" Title="Modify Billable">
        </telerik:RadWindow>
    </Windows>
</telerik:RadWindowManager>
