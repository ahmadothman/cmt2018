﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DistBillableForm.aspx.cs" Inherits="FOWebApp.Finance.DistBillableForm" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html lang="en">
<head>
    <title>Modify Distributor Billable</title>
    <link href="<%= this.ResolveUrl("~/CMTStyle.css") %>" rel="stylesheet" />
</head>
<body>
    <form id="formModifyDistBillable" runat="server">
        <telerik:RadScriptManager ID="RadScriptManagerDistBillableForm" runat="server"></telerik:RadScriptManager>

        <table class="table-spaced">
            <tr>
                <td>BillableId</td>
                <td>
                    <%= _distBillable.Bid %>
                </td>
            </tr>
            <tr>
                <td>Associated Billable</td>
                <td>
                    <%= _billableOfDistBillable.Description %>
                </td>
            </tr>
            <tr>
                <td>Account of Associated Billable</td>
                <td>
                    <%= _accountOfBillable.Description %>
                </td>
            </tr>
            <tr>
                <td>Distributor
                </td>
                <td>
                    <asp:Literal ID="LiteralDistributor" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="requiredLabel">Price
                </td>
                <td>
                    <telerik:RadNumericTextBox ID="RadNumericTextBoxPriceUsd" runat="server" Type="Currency" Width="200">
                        <NegativeStyle ForeColor="Red" />
                        <NumberFormat AllowRounding="true" KeepTrailingZerosOnFocus="true" DecimalDigits="2"
                            GroupSeparator=" " GroupSizes="3"
                            ZeroPattern="$ n" PositivePattern="$ n" NegativePattern="$ -n" />
                    </telerik:RadNumericTextBox>
                    <telerik:RadNumericTextBox ID="RadNumericTextBoxPriceEur" runat="server" Type="Currency" Width="200">
                        <NegativeStyle ForeColor="Red" />
                        <NumberFormat AllowRounding="true" KeepTrailingZerosOnFocus="true" DecimalDigits="2"
                            GroupSeparator=" " GroupSizes="3"
                            ZeroPattern="€ n" PositivePattern="€ n" NegativePattern="€ -n" />
                    </telerik:RadNumericTextBox>
                </td>
            </tr>
            <tr>
                <td>Remarks
                </td>
                <td>
                    <telerik:RadTextBox ID="RadTextBoxRemarks" runat="server" Columns="100" Rows="8" TextMode="MultiLine"
                        Resize="Vertical">
                    </telerik:RadTextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <telerik:RadButton ID="RadButtonSubmit" runat="server" Text="Submit" OnClick="RadButtonSubmit_Click" Skin="Metro"></telerik:RadButton>
                </td>
                <td>
                    <asp:Label ID="LabelResult" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Associated SLA(s):</td>
            </tr>
        </table>
        <asp:Panel ID="PanelLinkedSlas" runat="server">
            <asp:Label ID="LabelNoLinkedSlas" runat="server" Visible="false" Font-Italic="True">No linked SLAs found.</asp:Label>
        </asp:Panel>
    </form>
</body>
</html>
