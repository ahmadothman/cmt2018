﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FOWebApp.net.nimera.cmt.BOInvoicingControllerWSRef;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;

namespace FOWebApp.Finance
{
    public partial class InvoiceList : System.Web.UI.UserControl
    {
        private BOInvoicingControllerWS _invoiceController = new BOInvoicingControllerWS();
        private BOAccountingControlWS _accountingController = new BOAccountingControlWS();
        decimal totalGrandTotal = 0;
        decimal totalCreditDebit = 0;
        decimal totalFinancialInter = 0;
        decimal totalIncentive = 0;
        decimal totalBankCharges = 0;
        decimal totalPendingAmounts = 0;
        int nrOfBilledTerminals = 0;

        List<int> invoiceIds = new List<int>();

        protected void Page_Load(object sender, EventArgs e)
        {
            RadDropDownListDistributor.DataSource = _accountingController.GetDistributorsAndVNOs();
            RadDropDownListDistributor.DataValueField = "Id";
            RadDropDownListDistributor.DataTextField = "FullName";
            RadDropDownListDistributor.DataBind();
            AddInitialValueToDropDownList(RadDropDownListDistributor);
            // CMTFO-122: Allows the list to be sorted. Will catch NullReference at first load.
            try
            {
                RadDropDownListMonth.SelectedText = Session["Month"].ToString();
                RadDropDownListYear.SelectedText = Session["Year"].ToString();
                RadDropDownListDistributor.SelectedText = Session["Distributor"].ToString();
                RadDropDownListStatus.SelectedText = Session["Status"].ToString();
            }
            catch { }
        }

        protected void RadButtonFilter_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                try
                {
                    RadGridInvoiceList.DataSource = null; // Need to do this, otherwise the NeedDataSource event is not triggered on rebind
                    totalGrandTotal = 0;
                    totalPendingAmounts = 0;
                    totalCreditDebit = 0;
                     totalFinancialInter = 0;
                     totalIncentive = 0;
                     totalBankCharges = 0;
                    nrOfBilledTerminals = 0;
                    RadGridInvoiceList.Rebind();

                    LabelResult.ForeColor = Color.DarkGreen;
                    LabelResult.Text = "Query execution completed";
                    LabelTotalGrandTotal.Text =string.Format("{0:n0}", totalGrandTotal) + "€";
                    LabelNrOfBilledTerminals.Text = nrOfBilledTerminals.ToString();
                    if (nrOfBilledTerminals > 0)
                    {
                        LabelAverageRevenuePerTerminal.Text =  string.Format("{0:n0}", (totalGrandTotal / nrOfBilledTerminals)) + "€";
                    }
                    LabelTotalLiabilities.Text= string.Format("{0:n0}", totalCreditDebit) + "€";
                    LabelTotalFinancialInt.Text= string.Format("{0:n0}", totalFinancialInter) + "€";
                    LabelTotalIncentive.Text= string.Format("{0:n0}", totalIncentive*-1) + "€";
                    LabelTotalBankCharges.Text= string.Format("{0:n0}", totalBankCharges) + "€";
                    LabelTotalPendingAmounts.Text= string.Format("{0:n0}", totalPendingAmounts) + "€";

                    // CMTFO-122: Put filter values in sessions. Allows users to continue with the same list after visiting other pages.
                    Session["Month"] = RadDropDownListMonth.SelectedText;
                    Session["Year"] = RadDropDownListYear.SelectedText;
                    Session["Distributor"] = RadDropDownListDistributor.SelectedText;
                    Session["Status"] = RadDropDownListStatus.SelectedText;
                }
                catch (Exception ex)
                {
                    LabelResult.ForeColor = Color.DarkRed;
                    LabelResult.Text = "Query execution failed";
                }
            }
        }

        protected void RadGridInvoiceList_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            // Null needs to be given explicitily as a parameter so the method knows this property doesn't need to be searched for
            ((RadGrid)sender).MasterTableView.DataSource = _invoiceController.QueryInvoices(
                !(string.IsNullOrEmpty(RadDropDownListYear.SelectedValue) || RadDropDownListYear.SelectedValue == "-1") ? RadDropDownListYear.SelectedValue : null,
                !(string.IsNullOrEmpty(RadDropDownListMonth.SelectedValue) || RadDropDownListMonth.SelectedValue == "-1") ? RadDropDownListMonth.SelectedValue : null,
                !string.IsNullOrEmpty(RadNumericTextBoxInvoiceNumber.Text) ? RadNumericTextBoxInvoiceNumber.Text : null,
                !(string.IsNullOrEmpty(RadDropDownListStatus.SelectedValue) || RadDropDownListStatus.SelectedValue == "-1") ? RadDropDownListStatus.SelectedValue : null,
                !(string.IsNullOrEmpty(RadDropDownListDistributor.SelectedValue) || RadDropDownListDistributor.SelectedValue == "-1") ? RadDropDownListDistributor.SelectedValue : null
                );
        }

        protected void RadGridInvoiceList_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem dataItem = (GridDataItem)e.Item;

                TableCell invoiceMonthCell = dataItem["InvoiceMonth"];
                TableCell invoiceStateCell = dataItem["InvoiceState"];
                TableCell total = dataItem["ToBePaid"]; // Grand total
                TableCell totalInEur = dataItem["ToBePaidInEur"]; // Grand total in Eur
                TableCell toBePaidinEur = dataItem["ToBePaidFinal"]; // to be paid
                TableCell pendingAmount = dataItem["PendingAmount"]; // Open invoice / not paid yet

                InvoiceHeader ih = (InvoiceHeader)e.Item.DataItem;
                invoiceIds.Add(ih.Id);
                int distID = ih.DistributorId;
                invoiceMonthCell.Text = _accountingController.GetDistributor(distID).InvoiceInterval.ToString();
                ih = _invoiceController.GetInvoice(ih.Id);

                //decimal transactions = _invoiceController.GetTransactions(ih.Id);
                total.Text = ih.CurrencyCode + " " + Math.Round(ih.GrandTotal);

                totalInEur.Text = String.Format("{0:0}", Math.Round(ih.GrandTotal /*+ transactions*/) * (ih.ExchangeRate != null ? ih.ExchangeRate : 1));

                if (ih.CurrencyCode == "EUR")
                {
                    toBePaidinEur.Text = String.Format("{0:0}", ih.ToBePaid);
                    totalGrandTotal += ih.GrandTotal;
                    totalCreditDebit +=_invoiceController.GetTransaction(ih.Id).Amount;
                    if(ih.FinancialInterest.HasValue)
                    totalFinancialInter += (decimal)ih.FinancialInterest;
                    
                    totalIncentive += ih.TotalAmount * ih.Incentive;
                    
                    
                }
                else
                {
                    decimal exRate = 0;
                    exRate = ih.ToBePaidExchanged ?? _invoiceController.GetCurrentExchangeRate();
                    toBePaidinEur.Text = String.Format("{0:0}", exRate);
                    totalGrandTotal = totalGrandTotal + (ih.GrandTotal * _invoiceController.GetCurrentExchangeRate());
                    totalCreditDebit += _invoiceController.GetTransaction(ih.Id).Amount * _invoiceController.GetCurrentExchangeRate();
                    if (ih.FinancialInterest.HasValue)
                        totalFinancialInter += (decimal)ih.FinancialInterest * _invoiceController.GetCurrentExchangeRate();

                    totalIncentive +=ih.TotalAmount * ih.Incentive * _invoiceController.GetCurrentExchangeRate();
                    
                }
                pendingAmount.Text = toBePaidinEur.Text;

                totalPendingAmounts += Decimal.Parse(toBePaidinEur.Text);


                if (ih.BankCharges)
                    totalBankCharges += ih.BankChargesValue;

                nrOfBilledTerminals += Convert.ToInt32(ih.ActiveTerminals);

                switch (invoiceStateCell.Text)
                {
                    case "0":
                        invoiceStateCell.Text = "Unvalidated";
                        break;
                    case "1":
                        invoiceStateCell.Text = "Validated";
                        break;
                    case "2":
                        invoiceStateCell.Text = "Released";
                        break;
                    case "3":
                        invoiceStateCell.Text = "Accepted";
                        break;
                    case "4":
                        invoiceStateCell.Text = "Paid";
                        invoiceStateCell.BackColor = Color.LightGreen;
                        pendingAmount.Text = "0";
                        totalPendingAmounts-= Decimal.Parse(toBePaidinEur.Text); 
                        break;
                }
            }
        }

        /// <summary>
        /// Inserts a DropDownListItem with an emtpy string as text and -1 as value in the RadDropDownList at index 0
        /// </summary>
        /// <param name="comboBox">The RadDropDownList in which to insert the DropDownListItem</param>
        private void AddInitialValueToDropDownList(RadDropDownList dropDownList)
        {
            DropDownListItem item = new DropDownListItem(string.Empty, "-1");
            dropDownList.Items.Insert(0, item);
        }

        protected void RadGridInvoiceList_SortCommand(object sender, GridSortCommandEventArgs e)
        {
            try
            {
                RadGridInvoiceList.DataSource = null; // Need to do this, otherwise the NeedDataSource event is not triggered on rebind
                RadGridInvoiceList.Rebind();

                LabelResult.ForeColor = Color.DarkGreen;
                LabelResult.Text = "Query execution completed";
            }
            catch (Exception)
            {
                LabelResult.ForeColor = Color.DarkRed;
                LabelResult.Text = "Query execution failed";
            }
        }

        //protected void RadButtonConfirmAllPaid_Click(object sender, EventArgs e)
        //{
        //    RadWindowManagerInvoiceList.RadConfirm("Are you sure you want to mark all displayed invoices as paid?", "confirmCallBackFn", 300, 100, null, "Mark all invoices as paid");
        //}

        protected void MarkAllInvoicesPaid(object sender, EventArgs e)
        {
            _invoiceController.MarkInvoicesAsPaid(invoiceIds.ToArray());
            RadGridInvoiceList.DataSource = null;
            RadGridInvoiceList.Rebind();
        }
    }
}