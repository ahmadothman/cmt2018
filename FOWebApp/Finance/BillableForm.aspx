﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BillableForm.aspx.cs" Inherits="FOWebApp.Finance.BillableForm" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html lang="en">
<head>
    <title>Modify Billable</title>
    <link href="<%= this.ResolveUrl("~/CMTStyle.css") %>" rel="stylesheet" />
</head>
<body>
    <form id="formModifyBillable" runat="server">
        <telerik:RadScriptManager ID="RadScriptManagerBillableForm" runat="server"></telerik:RadScriptManager>

        <telerik:RadScriptBlock ID="RadScriptBlockBillableForm" runat="server">
            <script>
                function DescriptionMaxLength(sender, eventArgs) {
                    if (eventArgs.Value.length > 250) {
                        eventArgs.IsValid = false;
                    } else {
                        eventArgs.IsValid = true;
                    }
                }
            </script>
        </telerik:RadScriptBlock>

        <table class="table-spaced">
            <tr>
                <td>ID</td>
                <td><%= _billable.Bid %></td>
                <td></td>
            </tr>
            <tr>
                <td class="requiredLabel">Description
                </td>
                <td>
                    <telerik:RadTextBox ID="RadTextBoxDescription" runat="server" TextMode="MultiLine"
                        Columns="100" Rows="2" MaxLength="250" Resize="None">
                    </telerik:RadTextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorDescription" runat="server"
                        ControlToValidate="RadTextBoxDescription" ErrorMessage="Please enter a description" Display="Dynamic"></asp:RequiredFieldValidator>
                    <asp:CustomValidator ID="CustomValidatorDescription" runat="server" ControlToValidate="RadTextBoxDescription"
                        Display="Dynamic" ErrorMessage="Please provide a description of 250 characters or less"
                        ClientValidationFunction="DescriptionMaxLength" OnServerValidate="CustomValidatorDescription_ServerValidate"></asp:CustomValidator>
                </td>
            </tr>
            <tr>
                <td class="requiredLabel">Account
                </td>
                <td>
                    <telerik:RadDropDownList ID="RadDropDownListAccount" runat="server" Width="500"
                        DropDownWidth="500" DropDownHeight="250">
                    </telerik:RadDropDownList>
                </td>
                <td></td>
            </tr>
            <tr>
                <td class="requiredLabel">Price
                </td>
                <td>
                    <telerik:RadNumericTextBox ID="RadNumericTextBoxPriceUsd" runat="server" Type="Currency" Width="200">
                        <NegativeStyle ForeColor="Red" />
                        <NumberFormat AllowRounding="true" KeepTrailingZerosOnFocus="true" DecimalDigits="2"
                            GroupSeparator=" " GroupSizes="3"
                            ZeroPattern="$ n" PositivePattern="$ n" NegativePattern="$ -n" />
                    </telerik:RadNumericTextBox>
                    <telerik:RadNumericTextBox ID="RadNumericTextBoxPriceEur" runat="server" Type="Currency" Width="200">
                        <NegativeStyle ForeColor="Red" />
                        <NumberFormat AllowRounding="true" KeepTrailingZerosOnFocus="true" DecimalDigits="2"
                            GroupSeparator=" " GroupSizes="3"
                            ZeroPattern="€ n" PositivePattern="€ n" NegativePattern="€ -n" />
                    </telerik:RadNumericTextBox>
                </td>
                <td></td>
            </tr>
            <%--<tr>
                <td>Valid</td>
                <td><%= (_billable.ValidFromDate.HasValue) ? string.Format("from {0:dd/MM/yyyy}", _billable.ValidFromDate) : string.Empty %>
                    <%= (_billable.ValidToDate.HasValue) ? string.Format("until {0:dd/MM/yyyy}", _billable.ValidToDate) : string.Empty %>
                    <%= ((!_billable.ValidFromDate.HasValue) && (!_billable.ValidToDate.HasValue)) ? "No dates specified" : string.Empty %>
                </td>
            </tr>--%>
            <tr>
                <td>
                    <telerik:RadButton ID="RadButtonSubmit" runat="server" Text="Submit" OnClick="RadButtonSubmit_Click"></telerik:RadButton>
                </td>
                <td colspan="2">
                    <asp:Label ID="LabelResult" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Associated SLA(s):</td>
            </tr>
        </table>
        <asp:Panel ID="PanelLinkedSlas" runat="server">
            <asp:Label ID="LabelNoLinkedSlas" runat="server" Visible="false" Font-Italic="True">No linked SLAs found.</asp:Label>
        </asp:Panel>
    </form>
</body>
</html>
