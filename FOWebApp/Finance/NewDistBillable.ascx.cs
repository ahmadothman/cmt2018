﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FOWebApp.net.nimera.cmt.BOInvoicingControllerWSRef;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;

namespace FOWebApp.Finance
{
    public partial class NewDistBillable : System.Web.UI.UserControl
    {
        BOInvoicingControllerWS _invoiceController = new BOInvoicingControllerWS();
        BOAccountingControlWS _accountingController = new BOAccountingControlWS();

        protected void Page_Load(object sender, EventArgs e)
        {
            // Fill the RadDropDownListDistributor with data

            RadDropDownListDistributor.DataSource = _accountingController.GetDistributors();

            RadDropDownListDistributor.DataValueField = "Id";
            RadDropDownListDistributor.DataTextField = "FullName";

            // Databind the controls

            RadDropDownListDistributor.DataBind();
        }

        protected void RadComboBoxAssociatedBillable_ItemsRequested(object sender, RadComboBoxItemsRequestedEventArgs e)
        {
            RadComboBox radComboBoxAssociatedBillable = (RadComboBox)sender;

            // Clear items that were cached in the ViewState
            radComboBoxAssociatedBillable.Items.Clear();

            foreach (SelectedAccount account in _invoiceController.GetBillablesByDescription(e.Text))
            {
                RadComboBoxItem accountItem = new RadComboBoxItem(account.AccountDescription, account.AccountId.ToString());
                accountItem.Enabled = false;
                accountItem.ForeColor = Color.White;
                accountItem.BackColor = Color.DarkGray;

                radComboBoxAssociatedBillable.Items.Add(accountItem);

                foreach (Billable billableInAccount in account.SelectedBillables)
                {
                    RadComboBoxItem invoiceDetailItem = new RadComboBoxItem((billableInAccount.Bid + " - " + billableInAccount.Description), billableInAccount.Bid.ToString());

                    radComboBoxAssociatedBillable.Items.Add(invoiceDetailItem);
                }
            }
        }

        protected void CustomValidatorAssociatedBillabe_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (!string.IsNullOrEmpty(RadComboBoxAssociatedBillable.SelectedValue))
            {
                args.IsValid = true;
            }
            else
            {
                args.IsValid = false;
            }
        }

        protected void RadXmlHttpPanelBillablePrice_ServiceRequest(object sender, RadXmlHttpPanelEventArgs e)
        {
            if (e.Value != "false")
            {
                Billable billableWithPrice = _invoiceController.GetBillable(int.Parse(e.Value));

                RadNumericTextBoxPriceUsd.Value = (double)billableWithPrice.PriceUSD;
                RadNumericTextBoxPriceEur.Value = (double)billableWithPrice.PriceEU;

                ((RadXmlHttpPanel)sender).Enabled = true;
            }
            else
            {
                RadNumericTextBoxPriceUsd.Value = 0;
                RadNumericTextBoxPriceEur.Value = 0;

                ((RadXmlHttpPanel)sender).Enabled = false;
            }
        }

        protected void RadButtonSubmit_Click(object sender, EventArgs e)
        {
            if (Page.IsValid) 
            {
                DistBillable newDistBillable = new DistBillable()
                {
                    Bid = int.Parse(RadComboBoxAssociatedBillable.SelectedValue),
                    DistId = int.Parse(RadDropDownListDistributor.SelectedValue),
                    PriceUSD = (decimal)RadNumericTextBoxPriceUsd.Value,
                    PriceEU = (decimal)RadNumericTextBoxPriceEur.Value,
                    Remarks = RadTextBoxRemarks.Text,
                    UserId = new Guid(Membership.GetUser().ProviderUserKey.ToString()),
                    LastUpdate = DateTime.UtcNow,
                    Deleted = false
                };

                if (_invoiceController.DistBillableExists(newDistBillable.DistId, newDistBillable.Bid))
                {
                    LabelResult.ForeColor = Color.DarkRed;
                    LabelResult.Text = "There already exists a distributor billable for this distributor and this billable";

                    return;
                }

                try
                {
                    if (_invoiceController.AddDistBillable(newDistBillable))
                    {
                        LabelResult.ForeColor = Color.DarkGreen;
                        LabelResult.Text = "Distributor billable added succesfully";
                    }
                    else
                    {
                        LabelResult.ForeColor = Color.DarkRed;
                        LabelResult.Text = "Distributor billable could not be added";
                    }
                }
                catch (Exception ex)
                {
                    BOLogControlWS boLogControlWS = new BOLogControlWS();

                    CmtApplicationException cmtApplicationException = new CmtApplicationException()
                    {
                        ExceptionDateTime = DateTime.Now,
                        ExceptionDesc = ex.Message,
                        ExceptionStacktrace = ex.StackTrace,
                        ExceptionLevel = 3,
                        UserDescription = "NewDistBillable: Create new distributor billable"
                    };

                    boLogControlWS.LogApplicationException(cmtApplicationException);

                    LabelResult.ForeColor = Color.DarkRed;
                    LabelResult.Text = "Distributor billable could not be added";
                }
            }
        }
    }
}