﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NewBillable.ascx.cs" Inherits="FOWebApp.Finance.NewBillable" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<telerik:RadScriptBlock ID="RadScriptBlockNewBillalbe" runat="server">
    <script>
        function DescriptionMaxLength(sender, eventArgs) {
            if (eventArgs.Value.length > 250) {
                eventArgs.IsValid = false;
            } else {
                eventArgs.IsValid = true;
            }
        }
    </script>
</telerik:RadScriptBlock>

<table class="table-spaced">
    <tr>
        <td class="requiredLabel">Description
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxDescription" runat="server" TextMode="MultiLine"
                Columns="100" Rows="2" MaxLength="250" Resize="None">
            </telerik:RadTextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorDescription" runat="server"
                ControlToValidate="RadTextBoxDescription" ErrorMessage="Please enter a description" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:CustomValidator ID="CustomValidatorDescription" runat="server" ControlToValidate="RadTextBoxDescription"
                Display="Dynamic" ErrorMessage="Please provide a description of 250 characters or less"
                ClientValidationFunction="DescriptionMaxLength" OnServerValidate="CustomValidatorDescription_ServerValidate"></asp:CustomValidator>
        </td>
    </tr>
    <tr>
        <td class="requiredLabel">Account
        </td>
        <td>
            <telerik:RadDropDownList ID="RadDropDownListAccount" runat="server"
               DefaultMessage="Select an account..." Width="500" DropDownWidth="500" DropDownHeight="250">
            </telerik:RadDropDownList>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorAccount" runat="server"
                ControlToValidate="RadDropDownListAccount" ErrorMessage="Please select an account" Display="Dynamic"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td class="requiredLabel">Price
        </td>
        <td>
            <telerik:RadNumericTextBox ID="RadNumericTextBoxPriceUsd" runat="server" Type="Currency" Width="200" Value="0">
                <NegativeStyle ForeColor="Red" />
                <NumberFormat AllowRounding="true" KeepTrailingZerosOnFocus="true" DecimalDigits="2"
                    GroupSeparator=" " GroupSizes="3"
                    ZeroPattern="$ n" PositivePattern="$ n" NegativePattern="$ -n" />
            </telerik:RadNumericTextBox>
            <telerik:RadNumericTextBox ID="RadNumericTextBoxPriceEur" runat="server" Type="Currency" Width="200" Value="0">
                <NegativeStyle ForeColor="Red" />
                <NumberFormat AllowRounding="true" KeepTrailingZerosOnFocus="true" DecimalDigits="2"
                    GroupSeparator=" " GroupSizes="3"
                    ZeroPattern="€ n" PositivePattern="€ n" NegativePattern="€ -n" />
            </telerik:RadNumericTextBox>
        </td>
        <td></td>
    </tr>
    <%--<tr>
        <td>Valid
        </td>
        <td>from
            <telerik:RadDatePicker ID="RadDatePickerValidFrom" runat="server"
                MinDate="<%# DateTime.Today %>" Width="200">
            </telerik:RadDatePicker>
            until
            <telerik:RadDatePicker ID="RadDatePickerValidTo" runat="server"
                MinDate="<%# DateTime.Today %>" Width="200">
            </telerik:RadDatePicker>
        </td>
        <td>
            <asp:CompareValidator ID="CompareValidatorValidDates" runat="server" ControlToValidate="RadDatePickerValidTo"
                ControlToCompare="RadDatePickerValidFrom" Operator="GreaterThanEqual" Type="Date"
                ErrorMessage="The second date must be larger than or equal to the first date"></asp:CompareValidator>
        </td>
    </tr>--%>
    <tr>
        <td>
            <telerik:RadButton ID="RadButtonSubmit" runat="server" Text="Submit" OnClick="RadButtonSubmit_Click"></telerik:RadButton>
        </td>
        <td colspan="2">
            <asp:Label ID="LabelResult" runat="server"></asp:Label>
        </td>
    </tr>
</table>
