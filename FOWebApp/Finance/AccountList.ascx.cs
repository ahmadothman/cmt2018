﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FOWebApp.HelperMethods;
using FOWebApp.net.nimera.cmt.BOInvoicingControllerWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;
using FOWebApp.net.nimera.cmt.BOInvoicingControllerWSRef;

namespace FOWebApp.Finance
{
    public partial class AccountList : System.Web.UI.UserControl
    {
        private BOInvoicingControllerWS _invoiceController = new BOInvoicingControllerWS();
        private RadGridHelper _radGridHelper = new RadGridHelper("F_AccountList");

        protected void RadGridAccountList_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            ((RadGrid)sender).MasterTableView.DataSource = _invoiceController.GetAccounts();
        }

        protected void RadGridAccountList_DetailTableDataBind(object sender, GridDetailTableDataBindEventArgs e)
        {
            switch (e.DetailTableView.Name)
            {
                case "Billables":

                    GridDataItem dataItem = e.DetailTableView.ParentItem;

                    e.DetailTableView.DataSource =
                        _invoiceController.GetBillablesForAccount((int)dataItem.GetDataKeyValue("AccountId"));
                    break;
            }
        }

        protected void RadGridAccountList_ItemCommand(object sender, GridCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case RadGrid.ExpandCollapseCommandName:
                    if (!e.Item.Expanded)
                    {
                        _radGridHelper.SaveExpandedState(e.Item.ItemIndexHierarchical);
                    }
                    else
                    {
                        _radGridHelper.ClearExpandedState(e.Item.ItemIndexHierarchical);
                    }

                    break;
                case "DeleteBillable":
                case "DeleteAccount":
                    int id = int.Parse(e.CommandArgument.ToString());

                    Color alertForeColor = Color.Empty;
                    string alertText;

                    try
                    {
                        bool deletionResult = false;
                        alertForeColor = Color.DarkRed;
                        alertText = "Deletion failed";

                        switch (e.CommandName)
                        {
                            case "DeleteBillable":
                                deletionResult = _invoiceController.DeleteBillable(id);
                                break;
                            case "DeleteAccount":
                                if (_invoiceController.GetBillablesForAccount(id).Length > 0)
                                {
                                    alertText = "Account has still associated billables";
                                }
                                else
                                {
                                    deletionResult = _invoiceController.DeleteAccount(id);
                                }

                                break;
                        }

                        if (deletionResult)
                        {
                            alertForeColor = Color.DarkGreen;
                            alertText = "Deletion succesful";

                            _radGridHelper.ClearExpandedState(e.Item.ItemIndexHierarchical);

                            RadGridAccountList.DataSource = null; // Need to do this, otherwise the NeedDataSource event is not triggered on rebind
                            RadGridAccountList.Rebind();
                        }
                    }
                    catch (Exception ex)
                    {
                        BOLogControlWS boLogControlWS = new BOLogControlWS();

                        CmtApplicationException cmtApplicationException = new CmtApplicationException()
                        {
                            ExceptionDateTime = DateTime.Now,
                            ExceptionDesc = ex.Message,
                            ExceptionStacktrace = ex.StackTrace,
                            ExceptionLevel = 3,
                            UserDescription = "AccountList: Delete account / billable"
                        };

                        boLogControlWS.LogApplicationException(cmtApplicationException);

                        alertForeColor = Color.DarkRed;
                        alertText = "Deletion failed";
                    }

                    RadWindowManagerAccountList.RadAlert(string.Format("<span style=\"color: {0}\">{1}</span>",
                        ColorTranslator.ToHtml(alertForeColor), alertText), 300, 200, "Deletion Result", "deleteAlertCallback");

                    break;
            }
        }

        protected void RadGridAccountList_DataBound(object sender, EventArgs e)
        {
            _radGridHelper.LoadExpandedState(sender as RadGrid);
        }
    }
}