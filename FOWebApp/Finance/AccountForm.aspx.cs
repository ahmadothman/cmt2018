﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using FOWebApp.net.nimera.cmt.BOInvoicingControllerWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;

namespace FOWebApp.Finance
{
    public partial class AccountForm : System.Web.UI.Page
    {
        private BOInvoicingControllerWS _invoiceController = new BOInvoicingControllerWS();
        protected FOWebApp.net.nimera.cmt.BOInvoicingControllerWSRef.Account _account = new FOWebApp.net.nimera.cmt.BOInvoicingControllerWSRef.Account();

        protected void Page_Init(object sender, EventArgs e)
        {
            int accountId = int.Parse(Request.QueryString["id"]);

            _account = _invoiceController.GetAccount(accountId);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // Fill the textboxes with data

                RadTextBoxDescription.Text = _account.Description;
                RadTextBoxRemarks.Text = _account.Remarks;
            }
        }

        protected void CustomValidatorDescription_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (args.Value.Length > 60)
            {
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }
        }

        protected void RadButtonSubmit_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                FOWebApp.net.nimera.cmt.BOInvoicingControllerWSRef.Account updatedAccount = new FOWebApp.net.nimera.cmt.BOInvoicingControllerWSRef.Account()
                {
                    AccountId = _account.AccountId, 
                    Description = RadTextBoxDescription.Text,
                    Remarks = RadTextBoxRemarks.Text,
                    UserId = new Guid(Membership.GetUser().ProviderUserKey.ToString()),
                    LastUpdate = DateTime.UtcNow
                };

                try
                {
                    if (_invoiceController.UpdateAccount(updatedAccount))
                    {
                        LabelResult.ForeColor = Color.DarkGreen;
                        LabelResult.Text = "Account updated succesfully";
                    }
                    else
                    {
                        LabelResult.ForeColor = Color.DarkRed;
                        LabelResult.Text = "Account could not be updated";
                    }
                }
                catch (Exception ex)
                {
                    BOLogControlWS boLogControlWS = new BOLogControlWS();

                    CmtApplicationException cmtApplicationException = new CmtApplicationException()
                    {
                        ExceptionDateTime = DateTime.Now,
                        ExceptionDesc = ex.Message,
                        ExceptionStacktrace = ex.StackTrace,
                        ExceptionLevel = 3,
                        UserDescription = "AccountForm: Update billables account"
                    };

                    boLogControlWS.LogApplicationException(cmtApplicationException);

                    LabelResult.ForeColor = Color.DarkRed;
                    LabelResult.Text = "Account could not be updated";
                }
            }
        }
    }
}