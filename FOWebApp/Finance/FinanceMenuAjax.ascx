﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FinanceMenuAjax.ascx.cs" Inherits="FOWebApp.Finance.FinanceMenuAjax" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<telerik:RadTreeView ID="RadTreeViewDistributorMenu" runat="server"
    OnNodeClick="RadTreeViewDistributorMenu_NodeClick" Skin="Metro"
    ValidationGroup="Administrator main menu">
    <Nodes>
        <telerik:RadTreeNode runat="server" Expanded="True"
            ImageUrl="~/Images/earth_preferences.png" Owner="RadTreeViewDistributorMenu"
            Text="Finance Office Menu" Font-Bold="True" Value="n_Main" ToolTip="Finance Officer CMT Menu">
            <Nodes>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/invoice.png" Text="Invoices"
                    ToolTip="Invoices management" Value="n_Invoices">
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/magician.png"
                    Owner="" Text="VNOs" Value="n_VNOs" ToolTip="Virtual Network Operator Management">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Owner="" Text="New VNO"
                            Value="n_New_VNO" ToolTip="Create a new VNO">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/businesspeople.png"
                    Owner="" Text="Distributors" Value="n_Distributors" ToolTip="Distributor Management">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Owner="" Text="New distributor"
                            Value="n_New_Distributor" ToolTip="Create a new Distributor">
                        </telerik:RadTreeNode>
                        <telerik:RadTreeNode runat="server" Text="New Distributor Billable" ToolTip="Add a new distributor specific billable" Value="n_NewDistBillable">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" Owner="" Text="Terminals" Value="n_Terminals"
                    ImageUrl="~/Images/Terminal.png" ToolTip="Terminal Management">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Text="Search"
                            Value="n_Advanced_Search" ToolTip="Advanced Terminal Search">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/radio.png" Text="Multicast Groups" ToolTip="Multicast group management" Value="n_Multicast">
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/lock_ok.png" Text="Change Password"
                    ToolTip="Change your password" Value="n_Change_Password">
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/Logs.png" Owner=""
                    Text="Logs" Value="n_Logs" ToolTip="Logs and Reports">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Owner="" Text="Activity Log"
                            Value="n_Activity_Log" ToolTip="Log of Terminal activities">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/report.png"
                    Text="Reports" Value="n_Reports" ToolTip="Operational Reports">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Text="Accounting"
                            Value="n_Reports_Accounting" ToolTip="Accounting report over a period of time">
                        </telerik:RadTreeNode>
                        <telerik:RadTreeNode runat="server" Text="Vouchers"
                            Value="n_VoucherReports" ToolTip="Reports on vouchers">
                            <Nodes>
                                <telerik:RadTreeNode runat="server" Text="Validated Vouchers"
                                    Value="n_VoucherReport" ToolTip="Validated vouchers">
                                </telerik:RadTreeNode>
                                <telerik:RadTreeNode runat="server" Text="Voucher Batches"
                                    Value="n_VoucherBatchesReport" ToolTip="Voucher batches per Distributor">
                                </telerik:RadTreeNode>
                            </Nodes>
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <%--<telerik:RadTreeNode runat="server" ImageUrl="~/Images/gauge.png" Text="Dashboard" Value="n_Dashboard">
                </telerik:RadTreeNode>--%>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/ticket_blue.png"
                    Text="Vouchers" Value="n_Vouchers" ToolTip="Voucher creation and Management">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Text="Voucher requests"
                            Value="n_VoucherRequests" ToolTip="View voucher requests from distributors">
                        </telerik:RadTreeNode>
                        <telerik:RadTreeNode runat="server" Text="Create Vouchers"
                            Value="n_CreateVouchers" ToolTip="Create a new Voucher batch">
                        </telerik:RadTreeNode>
                        <telerik:RadTreeNode runat="server" Text="Batches Report"
                            Value="n_Available" ToolTip="Check on the available vouchers for a Distributor">
                        </telerik:RadTreeNode>
                        <telerik:RadTreeNode runat="server" Text="Validate voucher"
                            Value="n_ValidateVoucher" ToolTip="Validate an individual voucher, take care this does not unlock the terminal!">
                        </telerik:RadTreeNode>
                        <telerik:RadTreeNode runat="server" Text="Reset voucher"
                            Value="n_ResetVoucher" ToolTip="Reset a voucher so it can be validated again">
                        </telerik:RadTreeNode>
                        <telerik:RadTreeNode runat="server" Text="Voucher volumes"
                            Value="n_VoucherVolumes" ToolTip="View and modify voucher volumes">
                            <Nodes>
                                <telerik:RadTreeNode runat="server" Text="New voucher volume"
                                    Value="n_NewVoucherVolume" ToolTip="Add a new voucher volume">
                                </telerik:RadTreeNode>
                            </Nodes>
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" Text="CMT Messages" ImageUrl="~/Images/message.jpg" Value="n_CMTMessages" ToolTip="Send messages to CMT users">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Text="New Message" Value="n_NewCMTMessage" ToolTip="Submit a new message">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <%--<telerik:RadTreeNode runat="server" ImageUrl="~/Images/gear_run.png" Text="Procedures" Value="n_Procedures">
                </telerik:RadTreeNode>--%>
                <telerik:RadTreeNode runat="server" Text="Documents" ToolTip="Available Documents" Value="n_Library" ImageUrl="~/Images/books_blue_preferences.png">
                </telerik:RadTreeNode>

            </Nodes>
        </telerik:RadTreeNode>
        <telerik:RadTreeNode runat="server" ImageUrl="~/Images/configuration.png"
            Text="Configuration" Value="n_Configuration" ToolTip="CMT OSS Configurations">
            <Nodes>
                <telerik:RadTreeNode runat="server" Text="Service Packs"
                    Value="n_Service_Packs" ToolTip="Create and Manage Service Packs">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Text="Service Pack Wizard"
                            Value="n_ServicePackWizard" ToolTip="Create a new Service Pack with the Service Pack wizard">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <telerik:RadTreeNode Text="Invoicing" Value="n_Configuration_Invoicing"
                    ToolTip="Configure Different Invoicing Options">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Text="Set Exchange Rates"
                            Value="n_SetExchangeRates" ToolTip="Set the Exchange rate for all invoices which are unvalidated">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" Text="Billables"
                    Value="n_Billables" ToolTip="Add or modify Billables">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Text="New Billable" ToolTip="Add a new Billable" Value="n_NewBillable">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" Text="Accounts" ToolTip="Modify an Account" Value="n_Accounts">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Text="New Account" ToolTip="Add a new account" Value="n_NewAccount">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
            </Nodes>
        </telerik:RadTreeNode>
         <telerik:RadTreeNode runat="server" ImageUrl="~/Images/information.png"
            Text="About" Value="n_About" ToolTip="Shows About information">
        </telerik:RadTreeNode>
        <telerik:RadTreeNode runat="server" ImageUrl="~/Images/exit.png" Text="Log Off"
            ToolTip="Close the session" Value="n_log_off">
        </telerik:RadTreeNode>
    </Nodes>
</telerik:RadTreeView>
