﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InvoiceList.ascx.cs" Inherits="FOWebApp.Finance.InvoiceList" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<style type="text/css">
    .auto-style1 {
        width: 100%;
    }
    .auto-style2 {
        width: 264px;
        font-size: medium;
    }
</style>

<telerik:RadScriptBlock ID="RadScriptBlockInvoiceList" runat="server">
    <script>
        function clientExecuteQuery() {
            $telerik.$(<%= LabelResult.ClientID %>)
                .html('Executing query...')
                .css('color', 'DarkBlue');
        }

        function rowSelected(sender, eventArgs) {
            switch (eventArgs.get_tableView().get_name()) {
                case 'Invoices':
                    var invoiceId = eventArgs.getDataKeyValue('Id');
                    var oWnd = radopen('<%= this.ResolveUrl("~/Finance/InvoiceDetails.aspx") %>?id=' + invoiceId, 'RadWindowInvoiceDetails');

                    break;
                default:
                    console.log('Not implemented!');
                    break;
            }
        }

       function confirmMarkAllPaid() {
           if (confirm("Are you sure you want to mark all displayed invoices as paid?")) {
               document.getElementById('<%= MarkAllInvoicesPaidButton.ClientID %>').click();
               return false;
           }
        }
    </script>
</telerik:RadScriptBlock>

<table class="table-spaced">
    <tr>
        <td>Invoice Number
        </td>
        <td>
            <telerik:RadDropDownList ID="RadDropDownListMonth" runat="server" Width="150" DefaultMessage="Select a month..."
                DropDownWidth="150" DropDownHeight="265" Skin="Metro">
                <Items>
                    <telerik:DropDownListItem runat="server" Text="" Value="-1" />
                    <telerik:DropDownListItem runat="server" Text="001" Value="1" />
                    <telerik:DropDownListItem runat="server" Text="002" Value="2" />
                    <telerik:DropDownListItem runat="server" Text="003" Value="3" />
                    <telerik:DropDownListItem runat="server" Text="004" Value="4" />
                    <telerik:DropDownListItem runat="server" Text="005" Value="5" />
                    <telerik:DropDownListItem runat="server" Text="006" Value="6" />
                    <telerik:DropDownListItem runat="server" Text="007" Value="7" />
                    <telerik:DropDownListItem runat="server" Text="008" Value="8" />
                    <telerik:DropDownListItem runat="server" Text="009" Value="9" />
                    <telerik:DropDownListItem runat="server" Text="010" Value="10" />
                    <telerik:DropDownListItem runat="server" Text="011" Value="11" />
                    <telerik:DropDownListItem runat="server" Text="012" Value="12" />
                </Items>
            </telerik:RadDropDownList>
        </td>
        <td>&ndash;</td>
        <td>
            <telerik:RadDropDownList ID="RadDropDownListYear" runat="server" Width="150" DefaultMessage="Select a year..."
                DropDownWidth="150" DropDownHeight="300" Skin="Metro">
                <Items>
                    <telerik:DropDownListItem runat="server" Text="" Value="-1" />
                    <telerik:DropDownListItem runat="server" Text="2011" Value="2011" />
                    <telerik:DropDownListItem runat="server" Text="2012" Value="2012" />
                    <telerik:DropDownListItem runat="server" Text="2013" Value="2013" />
                    <telerik:DropDownListItem runat="server" Text="2014" Value="2014" />
                    <telerik:DropDownListItem runat="server" Text="2015" Value="2015" />
                    <telerik:DropDownListItem runat="server" Text="2016" Value="2016" />
                    <telerik:DropDownListItem runat="server" Text="2017" Value="2017" />
                    <telerik:DropDownListItem runat="server" Text="2018" Value="2018" />
                    <telerik:DropDownListItem runat="server" Text="2019" Value="2019" />
                    <telerik:DropDownListItem runat="server" Text="2020" Value="2020" />
                    <telerik:DropDownListItem runat="server" Text="2021" Value="2021" />
                    <telerik:DropDownListItem runat="server" Text="2022" Value="2022" />
                    <telerik:DropDownListItem runat="server" Text="2023" Value="2023" />
                    <telerik:DropDownListItem runat="server" Text="2024" Value="2024" />
                    <telerik:DropDownListItem runat="server" Text="2025" Value="2025" />
                    <telerik:DropDownListItem runat="server" Text="2026" Value="2026" />
                    <telerik:DropDownListItem runat="server" Text="2027" Value="2027" />
                    <telerik:DropDownListItem runat="server" Text="2028" Value="2028" />
                    <telerik:DropDownListItem runat="server" Text="2029" Value="2029" />
                    <telerik:DropDownListItem runat="server" Text="2030" Value="2030" />
                    <telerik:DropDownListItem runat="server" Text="2031" Value="2031" />
                    <telerik:DropDownListItem runat="server" Text="2032" Value="2032" />
                </Items>
            </telerik:RadDropDownList>
        </td>
        <td>&ndash;</td>
        <td>
            <telerik:RadNumericTextBox ID="RadNumericTextBoxInvoiceNumber" runat="server" Type="Number" Width="150"
                MinValue="1" Skin="Metro">
                <NumberFormat AllowRounding="true" KeepTrailingZerosOnFocus="true" DecimalDigits="0" />
            </telerik:RadNumericTextBox>
        </td>
        <td>&ndash;</td>
        <td>
            <telerik:RadDropDownList ID="RadDropDownListDistributor" runat="server" DefaultMessage="Select a distributor or VNO..."
                Width="300" DropDownWidth="300" DropDownHeight="300" Skin="Metro">
            </telerik:RadDropDownList>
        </td>
    </tr>
    <tr>
        <td>Invoice Status
        </td>
        <td colspan="2">
            <telerik:RadDropDownList ID="RadDropDownListStatus" runat="server" Width="150" DefaultMessage="Select a status..."
                DropDownWidth="150" DropDownHeight="125" Skin="Metro">
                <Items>
                    <telerik:DropDownListItem runat="server" Text="" Value="-1" />
                    <telerik:DropDownListItem runat="server" Text="Unvalidated" Value="0" />
                    <telerik:DropDownListItem runat="server" Text="Validated" Value="1" />
                    <telerik:DropDownListItem runat="server" Text="Released" Value="2" />
                    <telerik:DropDownListItem runat="server" Text="Accepted" Value="3" />
                    <telerik:DropDownListItem runat="server" Text="Paid" Value="4" />
                </Items>
            </telerik:RadDropDownList>
        </td>
        <td colspan="1">
            <telerik:RadButton ID="RadButtonFilter" runat="server" Text="Search"
                OnClientClicked="clientExecuteQuery" OnClick="RadButtonFilter_Click" Height="22px" Skin="Metro">
            </telerik:RadButton>

            <%--<telerik:RadButton ID="RadButtonClearFilter" runat="server" Text="Clear filter" 
                OnClientClicked="clientExecuteQuery" OnClick="RadButtonClearFilter_Click" Height="22px">
            </telerik:RadButton>--%>
        </td>
        <td colspan="2">
            <asp:Label ID="LabelResult" runat="server"></asp:Label>
        </td>
        <td colspan="2" style="text-align:right">
            <button id="confirmbutton" type="button" onclick="confirmMarkAllPaid(); return false;" hidden="hidden">Mark all as Paid</button>
            <%-- <telerik:RadButton ID="RadButtonConfirmAllPaid" runat="server" Text="Mark all as Paid" OnClick="RadButtonConfirmAllPaid_Click" Height="22px" Skin="Metro">
            </telerik:RadButton>--%>
            <asp:Button ID="MarkAllInvoicesPaidButton" Text="" runat="server" OnClick="MarkAllInvoicesPaid" style="display:none;"/>
        </td>
    </tr>
</table>

<hr />

<telerik:RadAjaxManager ID="RadAjaxManagerInvoiceList" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="RadGridInvoiceList">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGridInvoiceList" LoadingPanelID="RadAjaxLoadingPanelInvoiceList" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManager>

<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanelInvoiceList" runat="server" Skin="Metro"></telerik:RadAjaxLoadingPanel>

<telerik:RadGrid ID="RadGridInvoiceList" runat="server" OnNeedDataSource="RadGridInvoiceList_NeedDataSource"
    AutoGenerateColumns="False" OnItemDataBound="RadGridInvoiceList_ItemDataBound" AllowSorting="True" OnSortCommand="RadGridInvoiceList_SortCommand" CellSpacing="0" GridLines="None" Skin="Metro">
    <ClientSettings EnableAlternatingItems="true" EnableRowHoverStyle="true">
        <Resizing AllowColumnResize="true" EnableRealTimeResize="true" ClipCellContentOnResize="false" ResizeGridOnColumnResize="false" />
        <Selecting AllowRowSelect="true" />
        <ClientEvents OnRowSelected="rowSelected" />
    </ClientSettings>
    <MasterTableView Name="Invoices" DataKeyNames="Id"
        ClientDataKeyNames="Id" CommandItemDisplay="Top">
        <Columns>
            <telerik:GridBoundColumn UniqueName="InvoiceId"
                HeaderText="Invoice Number" DataField="InvoiceId">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn UniqueName="InvoiceMonth"
                HeaderText="Periodicity" DataField="InvoiceMonth">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn UniqueName="DistributorName"
                HeaderText="Distributor Name" DataField="DistributorName">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn UniqueName="InvoiceState"
                HeaderText="Invoice Status" DataField="InvoiceState">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn UniqueName="ActiveTerminals"
                HeaderText="Active Terminals" DataField="ActiveTerminals"
                HeaderStyle-Width="50px">
                <HeaderStyle Width="50px"></HeaderStyle>
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn UniqueName="CreationDate"
                HeaderText="Creation Date" DataField="CreationDate"
                DataFormatString="{0:dd/MM/yyyy}">
            </telerik:GridBoundColumn>
            <telerik:GridTemplateColumn UniqueName="ToBePaid" HeaderText="Grand Total">
                <ItemTemplate>
                    <%# Eval("CurrencyCode") %>
                    <%# Eval("ToBePaid", "{0:0}") %>
                </ItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridTemplateColumn UniqueName="ToBePaidInEur" HeaderText="Grand Total €">
                <ItemTemplate>
                    <%# String.Format("{0:0}", Convert.ToDecimal(Eval("ToBePaid")) * (Eval("ExchangeRate") != null ? Convert.ToDecimal(Eval("ExchangeRate")) : 1)) %>
                </ItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridBoundColumn UniqueName="ToBePaidFinal" HeaderText="To Be Paid €" DataField="ToBePaid" >
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="ToBePaid" FilterControlAltText="Filter PendingAmount column" HeaderText="Pending €" UniqueName="PendingAmount">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn AllowSorting="False" HeaderText="Remarks" ReadOnly="True" UniqueName="Remarks" DataField="Remarks">
            </telerik:GridBoundColumn>
        </Columns>
        <SortExpressions>
            <telerik:GridSortExpression FieldName="InvoiceId" SortOrder="Ascending" />
        </SortExpressions>
        <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" />
    </MasterTableView>
</telerik:RadGrid>

<telerik:RadWindowManager ID="RadWindowManagerInvoiceList" runat="server" Modal="true" Animation="None"
    DestroyOnClose="true" KeepInScreenBounds="true" EnableShadow="true" VisibleStatusbar="true" Width="1000"
    Height="655" ShowContentDuringLoad="false" Skin="Metro">
    <Windows>
        <telerik:RadWindow ID="RadWindowInvoiceDetails" runat="server" Title="Invoice Details">
        </telerik:RadWindow>
        <%--<telerik:RadWindow ID="RadWindowModifyDistributor" runat="server" Title="Modify Distributor">
        </telerik:RadWindow>--%>
    </Windows>
</telerik:RadWindowManager>
<p>
    &nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</p>
<table class="auto-style1">
    <tr>
        <td class="auto-style2"><strong>Summary:</strong></td>
        <td>
            &nbsp;</td>
    </tr>
    <tr>
        <td class="auto-style2">Grand Total for all Invoices:</td>
        <td>
    <asp:Label ID="LabelTotalGrandTotal" runat="server" Style="text-align: center; font-size: large; font-weight: 700"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="auto-style2">Total Pending Amounts:</td>
        <td>
    <asp:Label ID="LabelTotalPendingAmounts" runat="server" Style="text-align: center; font-size: large; font-weight: 700"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="auto-style2">Total Liabilities (Credit/Debit):</td>
        <td>
    <asp:Label ID="LabelTotalLiabilities" runat="server" Style="text-align: center; font-size: large; font-weight: 700"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="auto-style2">Total Finincial Interests:</td>
        <td>
    <asp:Label ID="LabelTotalFinancialInt" runat="server" Style="text-align: center; font-size: large; font-weight: 700"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="auto-style2">Total Incentives:</td>
        <td>
    <asp:Label ID="LabelTotalIncentive" runat="server" Style="text-align: center; font-size: large; font-weight: 700"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="auto-style2">Total Bank Charges:</td>
        <td>
    <asp:Label ID="LabelTotalBankCharges" runat="server" Style="text-align: center; font-size: large; font-weight: 700"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="auto-style2">Average Revenue Per Terminal:</td>
        <td>
    <asp:Label ID="LabelAverageRevenuePerTerminal" runat="server" Style="text-align: center; font-size: large; font-weight: 700"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="auto-style2">Number of Invoiced Terminals:</td>
        <td>
    <asp:Label ID="LabelNrOfBilledTerminals" runat="server" Style="text-align: center; font-size: large; font-weight: 700"></asp:Label>
        </td>
    </tr>
</table>


<p>
    &nbsp;</p>



