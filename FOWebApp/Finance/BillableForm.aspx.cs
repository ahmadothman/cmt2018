﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using FOWebApp.net.nimera.cmt.BOInvoicingControllerWSRef;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;

namespace FOWebApp.Finance
{
    public partial class BillableForm : System.Web.UI.Page
    {
        private BOInvoicingControllerWS _invoiceController = new BOInvoicingControllerWS();
        private BOAccountingControlWS _accountingController = new BOAccountingControlWS();
        protected Billable _billable = new Billable();

        protected void Page_Init(object sender, EventArgs e)
        {
            int billableId = int.Parse(Request.QueryString["id"]);

            _billable = _invoiceController.GetBillable(billableId);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // Fill the RadDropDownListAccount with data

                RadDropDownListAccount.DataSource = _invoiceController.GetAccounts();

                RadDropDownListAccount.DataValueField = "AccountId";
                RadDropDownListAccount.DataTextField = "Description";

                RadDropDownListAccount.SelectedValue = _billable.AccountId.ToString();

                RadDropDownListAccount.DataBind();

                // Fill the textboxes with data

                RadTextBoxDescription.Text = _billable.Description;
                RadNumericTextBoxPriceUsd.Value = (double)_billable.PriceUSD;
                RadNumericTextBoxPriceEur.Value = (double)_billable.PriceEU;

                //CMTINVOICE-73: Show linked SLAs when viewing billable details
                IEnumerable<ServiceLevel> slas = _accountingController.GetServicePacksByBillableId(_billable.Bid);
                IEnumerable<ListItem> listItems = slas.Select(sla =>
                {
                    string ispName = (_accountingController.GetISP(sla.IspId.Value)).CompanyName;

                    return new ListItem
                    {
                        Text = string.Format("{0} - {1} ({2})", ispName, sla.SlaName, sla.SlaId)
                    };
                });
                //check error message
                //listItems = new ListItem[]{};

                //foreach (ServiceLevel sla in slas)
                //{
                //    string slaName = sla.SlaName;
                //    string slaId = sla.SlaId.ToString();
                //    string ispName = (_accountingController.GetISP(sla.IspId.Value)).CompanyName;
                //    string output = string.Format("{0} - {1} ({2})", ispName, slaName, slaId);
                //}

                if (listItems.Any())
                {
                    BulletedList slaList = new BulletedList { DataSource = listItems };
                    slaList.DataBind();
                    PanelLinkedSlas.Controls.Add(slaList);
                }
                else
                {
                    LabelNoLinkedSlas.Visible = true;
                }
            }
        }

        protected void CustomValidatorDescription_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (args.Value.Length > 250)
            {
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }
        }

        protected void RadButtonSubmit_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                Billable updatedBillable = new Billable()
                {
                    Bid = _billable.Bid,
                    Description = RadTextBoxDescription.Text,
                    AccountId = int.Parse(RadDropDownListAccount.SelectedValue),
                    PriceUSD = (decimal)RadNumericTextBoxPriceUsd.Value,
                    PriceEU = (decimal)RadNumericTextBoxPriceEur.Value,
                    UserId = new Guid(Membership.GetUser().ProviderUserKey.ToString()),
                    LastUpdate = DateTime.UtcNow
                };

                try
                {
                    if (_invoiceController.UpdateBillable(updatedBillable))
                    {
                        LabelResult.ForeColor = Color.DarkGreen;
                        LabelResult.Text = "Billable updated succesfully";
                    }
                    else
                    {
                        LabelResult.ForeColor = Color.DarkRed;
                        LabelResult.Text = "Billable could not be updated";
                    }
                }
                catch (Exception ex)
                {
                    BOLogControlWS boLogControlWS = new BOLogControlWS();

                    CmtApplicationException cmtApplicationException = new CmtApplicationException()
                    {
                        ExceptionDateTime = DateTime.Now,
                        ExceptionDesc = ex.Message,
                        ExceptionStacktrace = ex.StackTrace,
                        ExceptionLevel = 3,
                        UserDescription = "BillableForm: Update billable"
                    };

                    boLogControlWS.LogApplicationException(cmtApplicationException);

                    LabelResult.ForeColor = Color.DarkRed;
                    LabelResult.Text = "Billable could not be updated";
                }
            }
        }
    }
}