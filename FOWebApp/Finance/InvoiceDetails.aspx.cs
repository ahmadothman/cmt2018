﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FOWebApp.HelperMethods.PdfFileWriterHelper;
using FOWebApp.net.nimera.cmt.BOInvoicingControllerWSRef;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;
using System.Threading;
using System.Globalization;

namespace FOWebApp.Finance
{
    public partial class InvoiceDetails : System.Web.UI.Page
    {
        private const string _HEXBYTE = "<0|1|2|3|4|5|6|7|8|9|a|b|c|d|e|f><0|1|2|3|4|5|6|7|8|9|a|b|c|d|e|f>";

        private BOInvoicingControllerWS _invoiceController = new BOInvoicingControllerWS();
        private BOAccountingControlWS _accountingController = new BOAccountingControlWS();
        private BOLogControlWS _logController = new BOLogControlWS();
        protected InvoiceHeader _invoice;

        protected void Page_PreInit(object sender, EventArgs e) 
        {
            // TG: CMTFO-157
            CultureInfo customCulture = (CultureInfo)Thread.CurrentThread.CurrentCulture.Clone();

            customCulture.NumberFormat.NumberDecimalSeparator = ".";

            Thread.CurrentThread.CurrentCulture = customCulture;
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            int invoiceId = int.Parse(Request.QueryString["id"]);

            _invoice = _invoiceController.GetInvoice(invoiceId);

            RadGridInvoiceDetails.ExportSettings.FileName = _invoice.InvoiceId;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                switch (_invoice.InvoiceMonth)
                {
                    case 1:
                        LiteralInvoiceMonth.Text = "January";
                        break;
                    case 2:
                        LiteralInvoiceMonth.Text = "February";
                        break;
                    case 3:
                        LiteralInvoiceMonth.Text = "March";
                        break;
                    case 4:
                        LiteralInvoiceMonth.Text = "April";
                        break;
                    case 5:
                        LiteralInvoiceMonth.Text = "May";
                        break;
                    case 6:
                        LiteralInvoiceMonth.Text = "June";
                        break;
                    case 7:
                        LiteralInvoiceMonth.Text = "July";
                        break;
                    case 8:
                        LiteralInvoiceMonth.Text = "August";
                        break;
                    case 9:
                        LiteralInvoiceMonth.Text = "September";
                        break;
                    case 10:
                        LiteralInvoiceMonth.Text = "October";
                        break;
                    case 11:
                        LiteralInvoiceMonth.Text = "November";
                        break;
                    case 12:
                        LiteralInvoiceMonth.Text = "December";
                        break;
                }

                RadDropDownListInvoiceState.SelectedValue = _invoice.InvoiceState.ToString();
                RadNumericTextBoxAlreadyPaid.Value = _invoice.AdvanceManual.HasValue ? (double)_invoice.AdvanceManual.Value : 0;
                RadButtonNoIncentive.Checked = _invoice.NoIncentive;
                RadNumericTextBoxExchangeRate.Value = (double?)_invoice.ExchangeRate;
                RadButtonNoBankCharges.Checked = !_invoice.BankCharges;
                RadButtonNoPenalties.Checked = _invoice.FinancialInterest > 0 ? false : true;
                RadTextBoxRemarks.Text = _invoice.Remarks;

                int total = Convert.ToInt32(Math.Round(_invoice.ToBePaid));
                LabelToBePaidEUR.Text = _invoice.CurrencyCode + " " + total;
            }

            if (_invoice.CurrencyCode == "EUR")
            {
                ExchangeRateRow.Visible = false;
            }
            else
            {
                decimal exchangeRate = 0;
                if (RadNumericTextBoxExchangeRate.Text != "" && RadNumericTextBoxExchangeRate != null)
                {
                    exchangeRate = Decimal.Parse(RadNumericTextBoxExchangeRate.Text);
                }
                decimal eur = _invoice.ToBePaid * exchangeRate;
                eur = Math.Round(eur, 2);
                LabelToBePaidEUR.Text = " (EUR " + eur.ToString() + ")";
            }
        }

        protected void RadButtonTransaction_Click(object sender, EventArgs e)
        {
            Color alertForeColor = Color.DarkRed;
            //string alertText = "Transaction not added";
            decimal amount = (decimal)RadNumericTextBoxAmount.Value;
            bool result = _invoiceController.AddTransaction(RadDropDownListTransactionType.SelectedValue, amount, _invoice.DistributorId, _invoice.Id, RadTextBoxTransactionRemark.Text);
            //if (result)
            //{
            //    alertForeColor = Color.DarkGreen;
            //    alertText = "Transaction successful";
                //int total;
                //if (_invoice.CurrencyCode == "EUR")
                //{
                //    total = Convert.ToInt32(Math.Round(_invoice.ToBePaid + _invoiceController.GetTransactions(_invoice.Id)));
                //    LabelToBePaidEUR.Text = _invoice.CurrencyCode + " " + total;
                //}
                //else
                //{
                //    decimal exchangeRate = 0;
                //    if (RadNumericTextBoxExchangeRate.Text != "" && RadNumericTextBoxExchangeRate != null)
                //    {
                //        exchangeRate = Decimal.Parse(RadNumericTextBoxExchangeRate.Text);
                //    }
                //    decimal eur = (_invoice.ToBePaid + _invoiceController.GetTransactions(_invoice.Id)) * exchangeRate;
                //    eur = Math.Round(eur, 2);
                //    LabelToBePaidEUR.Text = " (EUR " + eur.ToString() + ")";
                //}
            //} 
            //else
            //{
            //    alertForeColor = Color.DarkRed;
            //    alertText = "Transaction failed";
            //}
            Response.Redirect(Request.RawUrl);
            //RadWindowManagerInvoiceDetails.RadAlert(string.Format("<span style=\"color: {0}\">{1}</span>",
            //           ColorTranslator.ToHtml(alertForeColor), alertText), 300, 200, "Transaction Result", "insertUpdateAlertCallback");
        }

        protected void RadToolBarActions_ButtonClick(object sender, RadToolBarEventArgs e)
        {
            switch (e.Item.Value)
            {
                case "SaveButton":
                    Color alertForeColor = Color.DarkRed;
                    string alertText = "Invoice could not be updated";

                    if (Page.IsValid)
                    {
                        decimal financialInterest = Convert.ToDecimal(_invoice.FinancialInterest);
                        if (RadButtonNoPenalties.Checked)
                        {
                            financialInterest = 0;
                        }
                        InvoiceHeader updatedInvoiceHeader = new InvoiceHeader()
                        {
                            Id = _invoice.Id,
                            InvoiceNum = _invoice.InvoiceNum,
                            InvoiceYear = _invoice.InvoiceYear,
                            DistributorId = _invoice.DistributorId,
                            DistributorName = _invoice.DistributorName,
                            CreationDate = _invoice.CreationDate,
                            InvoiceMonth = _invoice.InvoiceMonth,
                            CurrencyCode = _invoice.CurrencyCode,
                            InvoiceState = short.Parse(RadDropDownListInvoiceState.SelectedValue),
                            Remarks = RadTextBoxRemarks.Text,
                            ActiveTerminals = _invoice.ActiveTerminals,
                            VAT = _invoice.VAT,
                            FinancialInterest = financialInterest,
                            BankCharges = !RadButtonNoBankCharges.Checked,
                            NoIncentive = RadButtonNoIncentive.Checked,
                            ExchangeRate = (decimal?)RadNumericTextBoxExchangeRate.Value,
                            AdvanceAutomatic = _invoice.AdvanceAutomatic,
                            AdvanceManual = (decimal?)RadNumericTextBoxAlreadyPaid.Value
                        };

                        try
                        {
                            if (_invoiceController.UpdateInvoice(updatedInvoiceHeader))
                            {    
                                //// Flag the voucher batch as paid
                                //if (!voucherController.FlagVoucherBatchAsPaid(vb.BatchId))
                                //{
                                //    Logger.Log("Failed to flag voucher batch " + vb.BatchId + " as paid in the CMT.");
                                //}
                                alertForeColor = Color.DarkGreen;
                                alertText = "Invoice updated succesfully";
                            }
                        }
                        catch (Exception ex)
                        {
                            CmtApplicationException cmtApplicationException = new CmtApplicationException()
                            {
                                ExceptionDateTime = DateTime.Now,
                                ExceptionDesc = ex.Message,
                                ExceptionStacktrace = ex.StackTrace,
                                ExceptionLevel = 3,
                                UserDescription = "InvoiceDetails: Update invoice"
                            };

                            _logController.LogApplicationException(cmtApplicationException);
                        }
                    }

                    RadWindowManagerInvoiceDetails.RadAlert(string.Format("<span style=\"color: {0}\">{1}</span>",
                        ColorTranslator.ToHtml(alertForeColor), alertText), 300, 200, "Invoice Storage Result", "invoiceSaveAlertCallback");

                    break;
                case "PdfButton":
                    PdfInvoiceHelper pdfInvoice = new PdfInvoiceHelper();

                    try
                    {
                        // Get the invoice again because changes could have been made.
                        pdfInvoice.Generate(_invoiceController.GetInvoice(_invoice.Id));
                        string filename = _invoice.InvoiceId.Replace("/", "-");
                               filename = _invoice.InvoiceId.Replace("&", "-");
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "Invoice as PDF",
                            string.Format("window.open(\'{0}/{1}.pdf\',\'_blank\');",
                                ResolveUrl("~/CMTInvoices"), filename),
                            true);
                    }
                    catch (Exception ex)
                    {
                        //throw;
                        CmtApplicationException cmtApplicationException = new CmtApplicationException()
                        {
                            ExceptionDateTime = DateTime.Now,
                            ExceptionDesc = ex.Message,
                            ExceptionStacktrace = ex.StackTrace,
                            ExceptionLevel = 4,
                            UserDescription = "InvoiceDetails: Generate PDF from Invoice"
                        };

                        _logController.LogApplicationException(cmtApplicationException);

                        RadWindowManagerInvoiceDetails.RadAlert(string.Format("<span style=\"color: {0}\">{1}</span>",
                        ColorTranslator.ToHtml(Color.DarkRed), "PDF creation failed.<br />Please try again later."), 300, 200, "PDF Creation Result", "pdfCreationAlertCallback");
                    }

                    break;
                case "DistributorButton":
                    break;
            }
        }

        protected void RadGridInvoiceDetails_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            ((RadGrid)sender).MasterTableView.DataSource = _invoiceController.GetInvoiceLines(_invoice.Id);
        }

        protected void RadGridInvoiceDetails_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem dataItem = (GridDataItem)e.Item;

                TableCell terminalIdCell = dataItem["TerminalId"];
                TableCell servicePackCell = dataItem["ServicePack"];

                // Take only a fixed amount of characters from the TerminalId and the ServicePack name

                SubstringTextTableCell(terminalIdCell, "LabelTerminalId", 0, 30);
                SubstringTextTableCell(servicePackCell, "LabelServicePack", 0, 13);
            }
        }

        protected void RadGridInvoiceDetails_BatchEditCommand(object sender, GridBatchEditingEventArgs e)
        {
            bool? insertUpdateSuccesful = null;

            foreach (GridBatchEditingCommand command in e.Commands)
            {
                switch (command.Type)
                {
                    //case GridBatchEditingCommandType.Delete:
                    //    break;

                    case GridBatchEditingCommandType.Insert:
                        InvoiceDetail newInvoiceDetail = new InvoiceDetail()
                        {
                            InvoiceYear = _invoice.InvoiceYear,
                            InvoiceNum = _invoice.InvoiceNum,
                            InvoiceId = _invoice.Id
                        };

                        foreach (DictionaryEntry newValue in command.NewValues)
                        {
                            try
                            {
                                UpdatePropertyInvoiceDetail(newInvoiceDetail, newValue.Key.ToString(), newValue.Value);
                            }
                            catch (Exception ex)
                            {
                                CmtApplicationException cmtApplicationException = new CmtApplicationException()
                                {
                                    ExceptionDateTime = DateTime.Now,
                                    ExceptionDesc = ex.Message,
                                    ExceptionStacktrace = ex.StackTrace,
                                    ExceptionLevel = 3,
                                    UserDescription = "InvoiceDetails: Insert invoice detail"
                                };

                                _logController.LogApplicationException(cmtApplicationException);
                            }
                        }

                        try
                        {
                            // Because the 'Add' method is called before the bool? is evaluated,
                            // an invoiceDetail will also be added when the updating or inserting
                            // of previous invoiceDetails failed
                            if (_invoiceController.AddInvoiceDetailLine(newInvoiceDetail) && insertUpdateSuccesful != false)
                            {
                                insertUpdateSuccesful = true;
                            }
                            else
                            {
                                insertUpdateSuccesful = false;
                            }
                        }
                        catch (Exception ex)
                        {
                            CmtApplicationException cmtApplicationException = new CmtApplicationException()
                            {
                                ExceptionDateTime = DateTime.Now,
                                ExceptionDesc = ex.Message,
                                ExceptionStacktrace = ex.StackTrace,
                                ExceptionLevel = 3,
                                UserDescription = "InvoiceDetails: Insert invoice detail"
                            };

                            _logController.LogApplicationException(cmtApplicationException);
                        }

                        break;
                    case GridBatchEditingCommandType.Update:
                        if (command.OldValues["Id"] != command.NewValues["Id"]) // Normally, this can't be the case
                        {
                            try
                            {
                                throw new InvalidOperationException("The ID of the Invoice Detail can't be changed.");
                            }
                            catch (Exception ex)
                            {
                                CmtApplicationException cmtApplicationException = new CmtApplicationException()
                                {
                                    ExceptionDateTime = DateTime.Now,
                                    ExceptionDesc = ex.Message,
                                    ExceptionStacktrace = ex.StackTrace,
                                    ExceptionLevel = 3,
                                    UserDescription = "InvoiceDetails: Update invoice detail"
                                };

                                _logController.LogApplicationException(cmtApplicationException);
                            }
                        }

                        InvoiceDetail updatedInvoiceDetail = _invoiceController.GetInvoiceDetailLine((int)command.OldValues["Id"]);

                        foreach (DictionaryEntry newValue in command.NewValues)
                        {
                            if (newValue.Value != command.OldValues[newValue.Key])
                            {
                                try
                                {
                                    UpdatePropertyInvoiceDetail(updatedInvoiceDetail, newValue.Key.ToString(), newValue.Value);
                                }
                                catch (Exception ex)
                                {
                                    CmtApplicationException cmtApplicationException = new CmtApplicationException()
                                    {
                                        ExceptionDateTime = DateTime.Now,
                                        ExceptionDesc = ex.Message,
                                        ExceptionStacktrace = ex.StackTrace,
                                        ExceptionLevel = 3,
                                        UserDescription = "InvoiceDetails: Update invoice detail"
                                    };

                                    _logController.LogApplicationException(cmtApplicationException);
                                }
                            }
                        }

                        // Set the Bid of the invoiceDetail to null
                        // This is done to prevent that when updating the description, there would still be a link to the billable, with another description
                        // The link to Bid is actually not needed
                        updatedInvoiceDetail.Bid = null;

                        try
                        {
                            // Because the 'Update' method is called before the bool? is evaluated,
                            // an invoiceDetail will also be updated when the updating or inserting
                            // of previous invoiceDetails failed
                            if (_invoiceController.UpdateInvoiceDetail(updatedInvoiceDetail) && insertUpdateSuccesful != false)
                            {
                                insertUpdateSuccesful = true;
                            }
                            else
                            {
                                insertUpdateSuccesful = false;
                            }

                        }
                        catch (Exception ex)
                        {
                            CmtApplicationException cmtApplicationException = new CmtApplicationException()
                            {
                                ExceptionDateTime = DateTime.Now,
                                ExceptionDesc = ex.Message,
                                ExceptionStacktrace = ex.StackTrace,
                                ExceptionLevel = 3,
                                UserDescription = "InvoiceDetails: Update invoice detail"
                            };

                            _logController.LogApplicationException(cmtApplicationException);
                        }

                        break;
                }
            }

            Color alertForeColor = Color.DarkRed;
            string alertText = "Some or all of the invoice detail lines could not be updated";

            switch (insertUpdateSuccesful)
            {
                case true:
                    alertForeColor = Color.DarkGreen;
                    alertText = "Invoice detail line(s) updated succesfully";

                    break;
                case null:
                    alertForeColor = Color.DarkBlue;
                    alertText = "No changes made";

                    break;
            }

            RadWindowManagerInvoiceDetails.RadAlert(string.Format("<span style=\"color: {0}\">{1}</span>",
                        ColorTranslator.ToHtml(alertForeColor), alertText), 300, 200, "Result of Changes", "insertUpdateAlertCallback");
        }

        protected void RadGridInvoiceDetails_ItemCommand(object sender, GridCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "DeleteInvoiceDetail":
                    int invoiceDetailId = int.Parse(e.CommandArgument.ToString());

                    Color alertForeColor = Color.Empty;
                    string alertText;

                    try
                    {
                        if (_invoiceController.DeleteInvoiceDetailLine(invoiceDetailId))
                        {
                            alertForeColor = Color.DarkGreen;
                            alertText = "Deletion succesful";

                            RadGridInvoiceDetails.DataSource = null; // Need to do this, otherwise the NeedDataSource event is not triggered on rebind
                            RadGridInvoiceDetails.Rebind();
                        }
                        else
                        {
                            alertForeColor = Color.DarkRed;
                            alertText = "Deletion failed";
                        }
                    }
                    catch (Exception ex)
                    {
                        CmtApplicationException cmtApplicationException = new CmtApplicationException()
                        {
                            ExceptionDateTime = DateTime.Now,
                            ExceptionDesc = ex.Message,
                            ExceptionStacktrace = ex.StackTrace,
                            ExceptionLevel = 3,
                            UserDescription = "InvoiceDetails: Delete invoice detail"
                        };

                        _logController.LogApplicationException(cmtApplicationException);

                        alertForeColor = Color.DarkRed;
                        alertText = "Deletion failed";
                    }

                    RadWindowManagerInvoiceDetails.RadAlert(string.Format("<span style=\"color: {0}\">{1}</span>",
                        ColorTranslator.ToHtml(alertForeColor), alertText), 300, 200, "Deletion Result", "deleteAlertCallback");

                    break;
            }
        }

        protected void RadComboBoxEditBillableDescription_ItemsRequested(object sender, RadComboBoxItemsRequestedEventArgs e)
        {
            RadComboBox radComboBoxEditBillableDescription = (RadComboBox)sender;

            // Clear items that were cached in the ViewState
            radComboBoxEditBillableDescription.Items.Clear();

            foreach (SelectedAccount account in _invoiceController.GetBillablesByDescription(e.Text))
            {
                RadComboBoxItem accountItem = new RadComboBoxItem(account.AccountDescription, account.AccountId.ToString());
                accountItem.Enabled = false;
                accountItem.ForeColor = Color.White;
                accountItem.BackColor = Color.DarkGray;

                radComboBoxEditBillableDescription.Items.Add(accountItem);

                foreach (Billable billableInAccount in account.SelectedBillables)
                {
                    RadComboBoxItem invoiceDetailItem = new RadComboBoxItem(billableInAccount.Description, billableInAccount.Description);

                    radComboBoxEditBillableDescription.Items.Add(invoiceDetailItem);
                }
            }
        }

        /// <summary>
        /// Takes a substring from the text of a given label in a given table cell when the text of that label is longer than the specified length.
        /// That substring is taken, an ellipsis is added to the end and is put again into the text field of the label.
        /// Also, when a substring is taken (in the case the length of the text in the label is longer than the specified length),
        /// a tooltip is added to the label with the full text.
        /// </summary>
        /// <param name="tableCell">The table cell in which the label should be searched</param>
        /// <param name="labelId">The ID of the label of which a substring from the text should be taken</param>
        /// <param name="startIndex">The index where the substring should start</param>
        /// <param name="length">
        /// The length of the substring.
        /// When the text of the found label is shorter than this length, no substring is taken. 
        /// </param>
        private void SubstringTextTableCell(TableCell tableCell, string labelId, int startIndex, int length)
        {
            Label labelTableCell = null; // Need to assign null explicitily, otherwise compile error

            foreach (Control control in tableCell.Controls)
            {
                if (control is Label && control.ID == labelId)
                {
                    labelTableCell = (Label)control;
                }
            }

            if (labelTableCell != null)
            {
                string labelText = labelTableCell.Text;

                if (labelText.Length > length)
                {
                    labelTableCell.Text = labelText.Substring(startIndex, length) + "...";
                    labelTableCell.ToolTip = labelText;
                }
            }
        }

        /// <summary>
        /// Updates the property with a given name (as string) of a given invoice detail with a specified value
        /// </summary>
        /// <param name="detail">The invoiceDetail for which the property update should take place</param>
        /// <param name="propertyName">The name of the property (as string) that should be updated</param>
        /// <param name="value">The new value of that property</param>
        private void UpdatePropertyInvoiceDetail(InvoiceDetail detail, string propertyName, object value)
        {
            string exceptionNullMessage = "The value parameter can't be null for this property name";

            switch (propertyName)
            {
                case "ActivityDate":
                    // TG: CMTFO-158

                    if (value == null || string.IsNullOrWhiteSpace(value.ToString()))
                    {
                        detail.ActivityDate = null;
                        break;
                    }

                    // TG: This doesn't specify the culture, so the input format may need to be different on different servers
                    if (value is DateTime)
                    {
                        detail.ActivityDate = (DateTime)value;
                        break;
                    }

                    detail.ActivityDate = null;
                    break;
                case "Bid":
                    detail.Bid = (int?)value;
                    break;
                case "BillableDescription":
                    if (value == null)
                    {
                        value = string.Empty;
                    }

                    detail.BillableDescription = !string.IsNullOrEmpty(value.ToString()) ? value.ToString() : null;
                    break;
                case "Items":
                    if (value == null)
                    {
                        throw new ArgumentNullException("value", exceptionNullMessage);
                    }

                    detail.Items = (int)value;
                    break;
                case "MacAddress":
                    if (value == null)
                    {
                        value = string.Empty;
                    }

                    detail.MacAddress = !string.IsNullOrEmpty(value.ToString()) ? value.ToString() : null;
                    break;
                case "Serial":
                    if (value == null)
                    {
                        value = string.Empty;
                    }

                    detail.Serial = !string.IsNullOrEmpty(value.ToString()) ? value.ToString() : null;
                    break;
                case "ServicePack":
                    if (value == null)
                    {
                        value = string.Empty;
                    }

                    string servicePackValueAsString = value.ToString();

                    if (!string.IsNullOrWhiteSpace(servicePackValueAsString) &&
                        servicePackValueAsString.Substring(servicePackValueAsString.Length - 3, 3) != "...")
                    {
                        // The ServicePack property may only be adapted when the new value doesn't end with '...'
                        // Because when it does, the new value is probably just the shortened name of the ServicePack
                        detail.ServicePack = !string.IsNullOrEmpty(servicePackValueAsString) ? servicePackValueAsString : null;
                    }
                    break;
                case "Status":
                    if (value == null)
                    {
                        value = string.Empty;
                    }

                    detail.Status = !string.IsNullOrEmpty(value.ToString()) ? value.ToString() : null;
                    break;
                case "TerminalId":
                    if (value == null)
                    {
                        value = string.Empty;
                    }

                    string terminalIdValueAsString = value.ToString();

                    if (!string.IsNullOrWhiteSpace(terminalIdValueAsString) &&
                        terminalIdValueAsString.Substring(terminalIdValueAsString.Length - 3, 3) != "...")
                    {
                        // The TerminalId property may only be adapted when the new value doesn't end with '...'
                        // Because when it does, the new value is probably just the shortened name of the TerminalId
                        detail.TerminalId = !string.IsNullOrEmpty(terminalIdValueAsString) ? terminalIdValueAsString : null;
                    }
                    break;
                case "UnitPrice":
                    if (value == null)
                    {
                        throw new ArgumentNullException("value", exceptionNullMessage);
                    }

                    detail.UnitPrice = (decimal)value;
                    break;
            }
        }

      
    }
}