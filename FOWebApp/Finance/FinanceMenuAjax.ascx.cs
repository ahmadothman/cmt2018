﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Web.Security;
using FOWebApp.Nocsa;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;

namespace FOWebApp.Finance
{
    public partial class FinanceMenuAjax : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //Set the user name as root node of the treeview
                if (HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    string user = HttpContext.Current.User.Identity.Name;
                    RadTreeViewDistributorMenu.Nodes[0].Text = "Welcome " + user;
                }
            }

            //Load the active component into the Main content placeholder
            RadTreeNode selectedNode = RadTreeViewDistributorMenu.SelectedNode;

            if (selectedNode != null)
            {
                //Get the MainContentPanel
                Control sheetContainer = scanForControl("SheetContentPlaceHolder", this);

                if (sheetContainer != null)
                {
                    Control contentContainer = sheetContainer.FindControl("MainContentPlaceHolder");

                    if (contentContainer != null)
                    {
                        contentContainer.Controls.Clear();
                        Control contentControl = this.LoadUserControl(selectedNode.Value);
                        if (contentControl != null)
                        {
                            contentControl.ID = selectedNode.Value + "_ID";
                            contentContainer.Controls.Add(contentControl);
                        }
                    }
                }
            }
        }

        protected void RadTreeViewDistributorMenu_NodeClick(object sender, Telerik.Web.UI.RadTreeNodeEventArgs e)
        {

        }

        protected Control scanForControl(string controlId, Control root)
        {
            Control resCtrl = root.FindControl(controlId);

            if (resCtrl == null)
            {
                root = root.Parent;
                if (root != null)
                {
                    resCtrl = this.scanForControl(controlId, root.Parent);
                }
            }

            return resCtrl;
        }

        protected Control LoadUserControl(string nodeSelector)
        {
            Control control = null;
            if (nodeSelector.Equals("n_Invoices"))
            {
                control = Page.LoadControl("~/Finance/InvoiceList.ascx");
            }
            else if (nodeSelector.Equals("n_log_off"))
            {
                //Close the session
                Session.Abandon();
                FormsAuthentication.SignOut();
                Response.Redirect("Default.aspx");
            }
            else if (nodeSelector.Equals("n_VNOs"))
            {
                control = Page.LoadControl("~/Nocsa/VNOList.ascx");
            }
            else if (nodeSelector.Equals("n_New_VNO"))
            {
                VNODetailsForm df = 
                    (VNODetailsForm) Page.LoadControl("~/Nocsa/VNODetailsForm.ascx");
                control = df;
            }
            else if (nodeSelector.Equals("n_Distributors"))
            {
                control = Page.LoadControl("~/Finance/DistributorList.ascx");
            }
            else if (nodeSelector.Equals("n_New_Distributor"))
            {
                DistributorDetailsForm df =
                    (DistributorDetailsForm)Page.LoadControl("~/Nocsa/DistributorDetailsForm.ascx");
                control = df;
            }
            else if (nodeSelector.Equals("n_Search_Distributor_By_Name"))
            {
                control = Page.LoadControl("TestUserControl.ascx");
            }
            else if (nodeSelector.Equals("n_Terminals"))
            {
                control = Page.LoadControl("~/Nocsa/TerminalList.ascx"); ;
            }
            else if (nodeSelector.Equals("n_New_Terminal"))
            {
                control = Page.LoadControl("~/Nocsa/CreateNewTerminal.ascx");
            }
            else if (nodeSelector.Equals("n_Search_By_MAC"))
            {
                control = Page.LoadControl("TerminalSearchByMac.ascx");
            }
            else if (nodeSelector.Equals("n_Search_By_SitId"))
            {
                control = Page.LoadControl("TerminalSearchBySitId.ascx");
            }
            else if (nodeSelector.Equals("n_Advanced_Search"))
            {
                control = Page.LoadControl("~/Nocsa/AdvancedSearch2.ascx");
            }
            else if (nodeSelector.Equals("n_Customers"))
            {
                control = Page.LoadControl("~/Nocsa/CustomerList.ascx");
            }
            else if (nodeSelector.Equals("n_Search_Customer_By_Name"))
            {
                control = Page.LoadControl("TestUserControl.ascx");
            }
            else if (nodeSelector.Equals("n_New_Customer"))
            {
                control = Page.LoadControl("~/Nocsa/NewCustomerForm.ascx");
            }
            else if (nodeSelector.Equals("n_EndUsers"))
            {
                control = Page.LoadControl("~/Nocsa/EndUserList.ascx");
            }
            else if (nodeSelector.Equals("n_NewEndUser"))
            {
                control = Page.LoadControl("~/Nocsa/NewEndUser.ascx");
            }
            else if (nodeSelector.Equals("n_Account"))
            {
                control = Page.LoadControl("TestUserControl.ascx");
            }
            else if (nodeSelector.Equals("n_Logs"))
            {
                control = Page.LoadControl("BlankControl.ascx");
            }
            else if (nodeSelector.Equals("n_Activity_Log"))
            {
                control = Page.LoadControl("~/Nocsa/TerminalActivityLogList.ascx");
            }
            else if (nodeSelector.Equals("n_Search_User_By_Name"))
            {
                control = Page.LoadControl("TestUserControl.ascx");
            }
            else if (nodeSelector.Equals("n_About"))
            {
                control = Page.LoadControl("AboutControl.ascx");
            }
            else if (nodeSelector.Equals("n_Reports_Accounting"))
            {
                control = Page.LoadControl("~/Nocsa/AccountReport.ascx");
            }
            else if (nodeSelector.Equals("n_Change_Password"))
            {
                control = Page.LoadControl("ChangePassword.ascx");
            }
            else if (nodeSelector.Equals("n_CreateVouchers"))
            {
                control = Page.LoadControl("~/Nocsa/CreateVouchers.ascx");
            }
            else if (nodeSelector.Equals("n_VoucherRequests"))
            {
                control = Page.LoadControl("~/Nocsa/VoucherRequestList.ascx");
            }
            else if (nodeSelector.Equals("n_Available"))
            {
                control = Page.LoadControl("~/Nocsa/AvailableVouchers.ascx");
            }
            else if (nodeSelector.Equals("n_ValidateVoucher"))
            {
                control = Page.LoadControl("~/Nocsa/ValidateVoucher.ascx");
            }
            else if (nodeSelector.Equals("n_ResetVoucher"))
            {
                control = Page.LoadControl("~/Nocsa/ResetVoucher.ascx");
            }
            else if (nodeSelector.Equals("n_VoucherVolumes"))
            {
                control = Page.LoadControl("~/Nocsa/VoucherVolumeList.ascx");
            }
            else if (nodeSelector.Equals("n_NewVoucherVolume"))
            {
                control = Page.LoadControl("~/Nocsa/NewVoucherVolume.ascx");
            }
            else if (nodeSelector.Equals("n_VoucherReports"))
            {
                control = Page.LoadControl("~/BlankControl.ascx");
            }
            else if (nodeSelector.Equals("n_VoucherReport"))
            {
                control = Page.LoadControl("~/Nocsa/VoucherReport.ascx");
            }
            else if (nodeSelector.Equals("n_VoucherBatchesReport"))
            {
                control = Page.LoadControl("~/Nocsa/AvailableVouchers.ascx");
            }
            else if (nodeSelector.Equals("n_NewIssue"))
            {
                control = Page.LoadControl("~/Nocsa/NewIssue.ascx");
            }
            else if (nodeSelector.Equals("n_NewCMTMessage"))
            {
                control = Page.LoadControl("~/Nocsa/CreateCMTMessage.ascx");
            }
            else if (nodeSelector.Equals("n_CMTMessages"))
            {
                control = Page.LoadControl("CMTMessages.ascx");
            }
            else if (nodeSelector.Equals("n_Service_Packs"))
            {
                control = Page.LoadControl("~/Nocsa/ServicePackList.ascx");
            }
            else if (nodeSelector.Equals("n_New_ServicePack"))
            {
                control = Page.LoadControl("~/Nocsa/ServicePackUpdate.ascx");
            }
            else if (nodeSelector.Equals("n_ServicePackWizard"))
            {
                control = Page.LoadControl("ServicePackWizard.ascx");
            }
            else if (nodeSelector.Equals("n_FreeZones"))
            {
                control = Page.LoadControl("~/Nocsa/FreeZoneList.ascx");
            }
            else if (nodeSelector.Equals("n_New_FreeZone"))
            {
                control = Page.LoadControl("~/Nocsa/NewFreeZone.ascx");
            }
            else if (nodeSelector.Equals("n_Services"))
            {
                control = Page.LoadControl("~/Nocsa/Services.ascx");
            }
            else if (nodeSelector.Equals("n_Create_New_Service"))
            {
                control = Page.LoadControl("~/Nocsa/CreateNewService.ascx");
            }
            else if (nodeSelector.Equals("n_Multicast"))
            {
                control = Page.LoadControl("~/Nocsa/MultiCastGroupList.ascx");
            }
            else if (nodeSelector.Equals("n_NewMulticast"))
            {
                control = Page.LoadControl("~/Nocsa/NewMultiCastGroup.ascx");
            }
            else if (nodeSelector.Equals("n_Procedures"))
            {
                control = Page.LoadControl("~/Finance/FinanceProcedures.ascx");
            }
            else if (nodeSelector.Equals("n_Dashboard"))
            {
                control = Page.LoadControl("~/Finance/FinanceDashboard.ascx");
            }
            else if (nodeSelector == "n_Configuration_Invoicing")
            {
                control = Page.LoadControl("~/Finance/InvoicingConfiguration.ascx");
            }
            else if (nodeSelector == "n_SetExchangeRates")
            {
                control = Page.LoadControl("~/Finance/SetExchangeRates.ascx");
            }
            else if (nodeSelector.Equals("n_Billables"))
            {
                control = Page.LoadControl("~/Finance/BillableList.ascx");
            }
            else if (nodeSelector.Equals("n_NewDistBillable"))
            {
                control = Page.LoadControl("~/Finance/NewDistBillable.ascx");
            }
            else if (nodeSelector.Equals("n_NewBillable"))
            {
                control = Page.LoadControl("~/Finance/NewBillable.ascx");
            }
            else if (nodeSelector.Equals("n_Accounts"))
            {
                control = Page.LoadControl("~/Finance/AccountList.ascx");
            }
            else if (nodeSelector.Equals("n_NewAccount"))
            {
                control = Page.LoadControl("~/Finance/NewAccount.ascx");
            }
            else if (nodeSelector == "n_Library")
            {
                control = Page.LoadControl("~/DocumentLibrary.ascx");
            }
            else
            {
                control = Page.LoadControl("BlankControl.ascx"); // TO DO: Change this back in "CMTMessageStart.ascx" when it has been implemented
            }

            return control;
        }
    }
}