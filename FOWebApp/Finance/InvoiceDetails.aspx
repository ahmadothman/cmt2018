﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InvoiceDetails.aspx.cs" Inherits="FOWebApp.Finance.InvoiceDetails" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html lang="en">
<head>
    <title>Invoice Details</title>
    <link href="<%= this.ResolveUrl("~/CMTStyle.css") %>" rel="stylesheet" />

    <style>
        div.RadToolBar .rtbUL {
            width: 100%;
            text-align: center;
        }

        div.RadToolBar .item {
            margin: 0 20px;
        }

        .italic {
            font-style: italic;
        }

        .bold {
            font-weight: bold;
        }

        .pull-left {
            float: left;
        }

        .pull-right {
            float: right;
        }

        .clearfix {
            clear: both;
        }
    </style>
</head>
<body>
    <form id="formInvoiceDetails" runat="server">
        <telerik:RadScriptManager ID="RadScriptManagerInvoiceDetails" runat="server"></telerik:RadScriptManager>

        <telerik:RadScriptBlock ID="RadScriptBlockInvoiceDetails" runat="server">
            <script>

                function deleteInvoiceDetailConfirm(sender, eventArgs) {
                    var processDelete = Function.createDelegate(sender, function (shouldSubmit) {
                        if (shouldSubmit === true) {
                            this.click();
                        }
                    });

                    eventArgs.set_cancel(true);

                    radconfirm('Are you sure you want to delete this invoice detail line?', processDelete, 300, 200, null, 'Delete Invoice Detail Line');
                }

                function invoiceSaveAlertCallback(arg) {
                    // Maybe do something here when user clicked 'OK'
                }

                function pdfCreationAlertCallback(arg) {
                    // Maybe do something here when user clicked 'OK'
                }

                function insertUpdateAlertCallback(arg) {
                    // Maybe do something here when user clicked 'OK'
                }

                function deleteAlertCallback(arg) {
                    // Maybe do something here when user clicked 'OK'
                }

                // This method returns true when a given string ends with a given suffix, otherwise false
                function endsWith(str, suffix) {
                    if (str === undefined || str === null) {
                        return false;
                    }

                    return str.indexOf(suffix, str.length - suffix.length) !== -1;
                }

                // This function gets the label in the ItemTemplate for the textboxes in the EditTemplates
                function GetAssociatedLabel(textBoxObject) {
                    var textBoxId = textBoxObject.get_id();
                    var labelId;

                    if (endsWith(textBoxId, 'RadTextBoxEditTerminalId')) {
                        labelId = 'LabelTerminalId';
                    } else if (endsWith(textBoxId, 'RadTextBoxEditServicePack')) {
                        labelId = 'LabelServicePack';
                    }

                    return $telerik.$('[id$="' + labelId + '"]', $telerik.$(textBoxObject.get_element()).parents('td.rgBatchCurrent'));
                }

                function OnFocusEditTextBox(sender, eventArgs) {
                    var labelServicePack = GetAssociatedLabel(sender);
                    var labelServicePackText = labelServicePack.html();
                    var labelServicePackTitle = labelServicePack.attr('title');

                    var textBoxValue;

                    if (endsWith(labelServicePackText, '...')) {
                        textBoxValue = labelServicePackTitle;
                    }
                    else {
                        textBoxValue = labelServicePackText;
                    }

                    sender.set_value(textBoxValue);
                }
            </script>
        </telerik:RadScriptBlock>

        <telerik:RadToolBar ID="RadToolBarActions" runat="server" Width="100%" OnButtonClick="RadToolBarActions_ButtonClick" Skin="Metro">
            <Items>
                <telerik:RadToolBarButton Text="Save Changes" ToolTip="Save the changes made in the invoice" ImageUrl="~/Images/floppy_disk.png"
                    OuterCssClass="item" Value="SaveButton">
                </telerik:RadToolBarButton>
                <telerik:RadToolBarButton Text="Save as PDF" ToolTip="Save the contents of this form as PDF on your hard disk" ImageUrl="~/Images/PDF.jpg"
                    OuterCssClass="item" Value="PdfButton">
                </telerik:RadToolBarButton>
            </Items>
        </telerik:RadToolBar>

        <table class="table-spaced pull-left">
            <tr>
                <td class="italic">Invoice Number:
                </td>
                <td colspan="3">
                    <telerik:RadCodeBlock ID="RadCodeBlockInvoiceId" runat="server"><%= _invoice.InvoiceId %></telerik:RadCodeBlock>
                </td>
            </tr>
            <tr>
                <td class="italic">Invoice Month:
                </td>
                <td>
                    <asp:Literal ID="LiteralInvoiceMonth" runat="server"></asp:Literal>
                </td>
                <td class="italic">Creation Date:
                </td>
                <td>
                    <telerik:RadCodeBlock ID="RadCodeBlockCreationDate" runat="server"><%= _invoice.CreationDate.ToString("dd/MM/yyyy") %></telerik:RadCodeBlock>
                </td>
            </tr>
            <tr>
                <td class="italic">Distributor Name:
                </td>
                <td colspan="3">
                    <telerik:RadCodeBlock ID="RadCodeBlockDistributorId" runat="server"><%= _invoice.DistributorName %></telerik:RadCodeBlock>
                </td>
            </tr>
            <tr>
                <td class="italic">Number of Active Terminals:</td>
                <td>
                    <telerik:RadCodeBlock ID="RadCodeBlockActiveTerminals" runat="server"><%= _invoice.ActiveTerminals ?? 0 %></telerik:RadCodeBlock>
                </td>
                <td class="italic">State:
                </td>
                <td>
                    <telerik:RadDropDownList ID="RadDropDownListInvoiceState" runat="server" Width="100"
                        DropDownWidth="100" DropDownHeight="110" Skin="Metro">
                        <Items>
                            <telerik:DropDownListItem runat="server" Text="Unvalidated" Value="0" />
                            <telerik:DropDownListItem runat="server" Text="Validated" Value="1" />
                            <telerik:DropDownListItem runat="server" Text="Released" Value="2" />
                            <telerik:DropDownListItem runat="server" Text="Accepted" Value="3" />
                            <telerik:DropDownListItem runat="server" Text="Paid" Value="4" />
                        </Items>
                    </telerik:RadDropDownList>
                </td>
            </tr>
            <tr>
                <td style="text-align: right">
                    <telerik:RadDropDownList ID="RadDropDownListTransactionType" runat="server" Width="100"
                        DropDownWidth="100" DropDownHeight="110" Skin="Metro">
                        <Items>
                            <telerik:DropDownListItem runat="server" Text="Add Credit:" Value="credit" />
                            <telerik:DropDownListItem runat="server" Text="Add Debit:" Value="debit" />
                        </Items>
                    </telerik:RadDropDownList>
                </td>
                <td>
                    <telerik:RadNumericTextBox ID="RadNumericTextBoxAmount" runat="server" Type="Number" Width="67px" EmptyMessage="Amount(€)">
                        <NumberFormat AllowRounding="true" KeepTrailingZerosOnFocus="true" DecimalDigits="2"
                            GroupSeparator=" " GroupSizes="3"/>
                    </telerik:RadNumericTextBox>
                    <telerik:RadTextBox ID="RadTextBoxTransactionRemark" runat="server" Width="80px" EmptyMessage="remark" MaxLength="30"></telerik:RadTextBox>
                </td>
                <td>
                    <telerik:RadButton ID="RadButtonTransaction" runat="server" Text="Submit"
                OnClick="RadButtonTransaction_Click" Height="22px" Skin="Metro">
            </telerik:RadButton>
                </td>
            </tr>
        </table>

        <table class="table-spaced pull-right">
            <tr>
                <td class="italic">Paid in Advance</td>
                <td>
                    <telerik:RadCodeBlock ID="RadCodeBlockAlreadyPaid" runat="server">
                        <%= _invoice.CurrencyCode %>
                        <%= (_invoice.AdvanceAutomatic.HasValue ? _invoice.AdvanceAutomatic.Value : 0).ToString("0.00") %>
                    </telerik:RadCodeBlock>
                    <telerik:RadNumericTextBox ID="RadNumericTextBoxAlreadyPaid" runat="server" Type="Number" Width="80">
                        <NumberFormat AllowRounding="true" KeepTrailingZerosOnFocus="true" DecimalDigits="2"
                            GroupSeparator=" " GroupSizes="3"
                            ZeroPattern="+n" PositivePattern="+n" NegativePattern="-n" />
                    </telerik:RadNumericTextBox>
                </td>
            </tr>
            <tr>
                <td class="italic">To Be Paid</td>
                <td>
                    <telerik:RadCodeBlock ID="RadCodeBlockToBePaid" runat="server">
                        <%--<%= _invoice.CurrencyCode %>
                        <%= _invoice.ToBePaid.ToString("0.00") %>--%>
                        <asp:Label ID="LabelToBePaidEUR" runat="server"></asp:Label>
                    </telerik:RadCodeBlock>
                </td>
            </tr>
            <tr>
                <td class="italic">Do Not Calculate Incentive</td>
                <td>
                    <telerik:RadButton ID="RadButtonNoIncentive" runat="server" ToggleType="CheckBox" ButtonType="ToggleButton" AutoPostBack="false" Skin="Metro"></telerik:RadButton>
                </td>
            </tr>
            <tr>
                <td class="italic">Do Not Apply Penalties</td>
                <td>
                    <telerik:RadButton ID="RadButtonNoPenalties" runat="server" ToggleType="CheckBox" ButtonType="ToggleButton" AutoPostBack="false" Skin="Metro"></telerik:RadButton>
                </td>
            </tr>
            <tr>
                <td class="italic">Do Not Apply Bank Charges</td>
                <td>
                    <telerik:RadButton ID="RadButtonNoBankCharges" runat="server" ToggleType="CheckBox" ButtonType="ToggleButton" AutoPostBack="false" Skin="Metro"></telerik:RadButton>
                </td>
            </tr>
            <tr id="ExchangeRateRow" runat="server">
                <td class="italic">Exchange Rate</td>
                <td>1 USD =
                    <telerik:RadNumericTextBox ID="RadNumericTextBoxExchangeRate" runat="server" Type="Number" Width="100" MinValue="0" Skin="Metro">
                        <NumberFormat AllowRounding="true" DecimalDigits="5" GroupSeparator=" " GroupSizes="3" />
                    </telerik:RadNumericTextBox>
                    EUR
                </td>
            </tr>
        </table>

        <div class="clearfix"></div>

        <hr />
        <telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxyInvoiceDetailsList" runat="server">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="RadGridInvoiceDetails">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="RadGridInvoiceDetails" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManagerProxy>

        <asp:ValidationSummary ID="ValidationSummaryRadGrid" runat="server" ForeColor="DarkRed" />

        <telerik:RadGrid ID="RadGridInvoiceDetails" runat="server" OnNeedDataSource="RadGridInvoiceDetails_NeedDataSource"
            AutoGenerateColumns="false" OnItemDataBound="RadGridInvoiceDetails_ItemDataBound" OnItemCommand="RadGridInvoiceDetails_ItemCommand"
            AllowPaging="true" OnBatchEditCommand="RadGridInvoiceDetails_BatchEditCommand" Skin="Metro">
            <ClientSettings EnableAlternatingItems="true" EnableRowHoverStyle="true">
                <Resizing AllowColumnResize="true" EnableRealTimeResize="true" ClipCellContentOnResize="false" ResizeGridOnColumnResize="false" />
            </ClientSettings>
            <ValidationSettings CommandsToValidate="Update, InitInsert, PerformInsert" />
            <MasterTableView Name="Invoices" DataKeyNames="Id" ClientDataKeyNames="Id" CommandItemDisplay="Top" PageSize="10" EditMode="Batch"
                InsertItemPageIndexAction="ShowItemOnLastPage" AllowMultiColumnSorting="true">
                <Columns>
                    <telerik:GridTemplateColumn UniqueName="TerminalId" HeaderText="Customer Reference Name">
                        <HeaderStyle Width="186" />
                        <ItemTemplate>
                            <asp:Label ID="LabelTerminalId" runat="server" Text='<%# Eval("TerminalId") %>' />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <telerik:RadTextBox ID="RadTextBoxEditTerminalId" runat="server"
                                Width="100%" MaxLength="100">
                                <ClientEvents OnFocus="OnFocusEditTextBox" />
                            </telerik:RadTextBox>
                        </EditItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn UniqueName="ServicePack" HeaderText="Service Profile" DataField="ServicePack">
                        <HeaderStyle Width="97" />
                        <ItemTemplate>
                            <asp:Label ID="LabelServicePack" runat="server" Text='<%# Eval("ServicePack") %>' />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <telerik:RadTextBox ID="RadTextBoxEditServicePack" runat="server"
                                Width="100%" MaxLength="50">
                                <ClientEvents OnFocus="OnFocusEditTextBox" />
                            </telerik:RadTextBox>
                        </EditItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn UniqueName="Serial"
                        HeaderText="Serial Number" DataField="Serial"
                        ColumnEditorID="GridTextBoxColumnEditorInvoiceDetailsSerial">
                        <HeaderStyle Width="90" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn UniqueName="MacAddress"
                        HeaderText="MAC Address" DataField="MacAddress"
                        ColumnEditorID="GridTextBoxColumnEditorInvoiceDetailsMacAddress">
                        <HeaderStyle Width="99" />
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn UniqueName="BillableDescription" HeaderText="Action">
                        <HeaderStyle Width="120" />
                        <ItemTemplate>
                            <%# Eval("BillableDescription") %>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <telerik:RadComboBox ID="RadComboBoxEditBillableDescription" runat="server" Width="100%"
                                AllowCustomText="true" DropDownWidth="300" Height="300" EnableLoadOnDemand="true"
                                OnItemsRequested="RadComboBoxEditBillableDescription_ItemsRequested" MaxLength="50" Skin="Metro">
                            </telerik:RadComboBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorEditBillableDescription" runat="server"
                                ControlToValidate="RadComboBoxEditBillableDescription" Text="*" ForeColor="DarkRed"
                                ErrorMessage="The field 'Description' is required" Display="Dynamic"></asp:RequiredFieldValidator>
                        </EditItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn UniqueName="ActivityDate" DataField="ActivityDate"
                        HeaderText="Activity Date" ColumnEditorID="GridTextBoxColumnEditorInvoiceDetailsActivityDate"
                        DataFormatString="{0:dd/MM/yyyy HH:mm}">
                        <HeaderStyle Width="100" />
                    </telerik:GridBoundColumn>
                    <telerik:GridNumericColumn UniqueName="UnitPrice"
                        HeaderText="Price" DataField="UnitPrice"
                        ColumnEditorID="GridNumericColumnEditorInvoiceDetailsDecimal">
                        <HeaderStyle Width="64" />
                        <ColumnValidationSettings EnableRequiredFieldValidation="true">
                            <RequiredFieldValidator ForeColor="DarkRed" Text="*" Display="Dynamic"
                                ErrorMessage="The field 'Unit Price' is required">
                            </RequiredFieldValidator>
                        </ColumnValidationSettings>
                    </telerik:GridNumericColumn>
                    <telerik:GridTemplateColumn UniqueName="EditDeleteBillable" Exportable="false">
                        <HeaderStyle Width="42" />
                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        <ItemTemplate>
                            <telerik:RadButton ID="RadButtonInvoiceDetailDelete" runat="server" ToolTip="Delete the invoice detail line"
                                Text="Delete" OnClientClicking="deleteInvoiceDetailConfirm"
                                Width="24" Height="24" CommandName="DeleteInvoiceDetail" CommandArgument='<%# Eval("Id") %>'>
                                <Image ImageUrl="~/Images/delete.png" />
                            </telerik:RadButton>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                </Columns>
                <PagerStyle Mode="Slider" Position="Bottom" />
                <SortExpressions>
                    <telerik:GridSortExpression FieldName="ActivityDate" SortOrder="Ascending" />
                    <telerik:GridSortExpression FieldName="TerminalId" SortOrder="Ascending" />
                </SortExpressions>
                <BatchEditingSettings EditType="Cell" OpenEditingEvent="DblClick" />
                <CommandItemSettings ShowExportToCsvButton="true" />
            </MasterTableView>
            <ExportSettings ExportOnlyData="false" IgnorePaging="true" OpenInNewWindow="true">
            </ExportSettings>
        </telerik:RadGrid>

        <telerik:GridTextBoxColumnEditor ID="GridTextBoxColumnEditorInvoiceDetailsMacAddress" runat="server" TextBoxMaxLength="17">
            <TextBoxStyle Width="100%" />
        </telerik:GridTextBoxColumnEditor>
        <telerik:GridTextBoxColumnEditor ID="GridTextBoxColumnEditorInvoiceDetailsSerial" runat="server" TextBoxMaxLength="30">
            <TextBoxStyle Width="100%" />
        </telerik:GridTextBoxColumnEditor>
        <telerik:GridTextBoxColumnEditor ID="GridTextBoxColumnEditorInvoiceDetailsActivityDate" runat="server" TextBoxMaxLength="25">
            <TextBoxStyle Width="100%" />
        </telerik:GridTextBoxColumnEditor>
        <telerik:GridNumericColumnEditor ID="GridNumericColumnEditorInvoiceDetailsDecimal" runat="server">
            <NumericTextBox ID="NumericTextBoxInvoiceDetailsEditorDecimal" runat="server" Width="100%">
                <NumberFormat AllowRounding="true" KeepTrailingZerosOnFocus="true" DecimalDigits="2"
                    DecimalSeparator="." GroupSeparator=" " GroupSizes="3" />
            </NumericTextBox>
        </telerik:GridNumericColumnEditor>
        <telerik:GridNumericColumnEditor ID="GridNumericColumnEditorInvoiceDetailsIntegerPositive" runat="server">
            <NumericTextBox ID="NumericTextBoxInvoiceDetailsEditorInteger" runat="server" Width="100%" MinValue="1">
                <NumberFormat AllowRounding="true" DecimalDigits="0" GroupSeparator=" " GroupSizes="3" />
            </NumericTextBox>
        </telerik:GridNumericColumnEditor>
        <hr />

        <table class="table-spaced">
            <tr>
                <td>Remarks
                </td>
                <td>
                    <telerik:RadTextBox ID="RadTextBoxRemarks" runat="server" Columns="100" Rows="4" TextMode="MultiLine"
                        Resize="Vertical">
                    </telerik:RadTextBox>
                </td>
            </tr>
        </table>

        <telerik:RadWindowManager ID="RadWindowManagerInvoiceDetails" runat="server" Modal="true" Animation="None"
            DestroyOnClose="true" KeepInScreenBounds="true" EnableShadow="true" VisibleStatusbar="true" Width="850"
            Height="655" ShowContentDuringLoad="false" Skin="Metro">
        </telerik:RadWindowManager>
    </form>
</body>
</html>
