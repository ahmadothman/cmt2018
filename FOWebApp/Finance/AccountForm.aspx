﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AccountForm.aspx.cs" Inherits="FOWebApp.Finance.AccountForm" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html lang="en">
<head>
    <title>Modify Account</title>
    <link href="<%= this.ResolveUrl("~/CMTStyle.css") %>" rel="stylesheet" />
</head>
<body>
    <form id="formModifyAccount" runat="server">
        <telerik:RadScriptManager ID="RadScriptManagerAccountForm" runat="server"></telerik:RadScriptManager>

        <telerik:RadScriptBlock ID="RadScriptBlockNewAccount" runat="server">
            <script>
                function DescriptionMaxLength(sender, eventArgs) {
                    if (eventArgs.Value.length > 60) {
                        eventArgs.IsValid = false;
                    } else {
                        eventArgs.IsValid = true;
                    }
                }
            </script>
        </telerik:RadScriptBlock>

        <table class="table-spaced">
            <tr>
                <td>ID</td>
                <td><%= _account.AccountId %></td>
                <td></td>
            </tr>
            <tr>
                <td class="requiredLabel">Description
                </td>
                <td>
                    <telerik:RadTextBox ID="RadTextBoxDescription" runat="server"
                        Columns="100" MaxLength="60">
                    </telerik:RadTextBox>
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorDescription" runat="server"
                        ControlToValidate="RadTextBoxDescription" ErrorMessage="Please enter a description" Display="Dynamic"></asp:RequiredFieldValidator>
                    <asp:CustomValidator ID="CustomValidatorDescription" runat="server" ControlToValidate="RadTextBoxDescription"
                        Display="Dynamic" ErrorMessage="Please provide a description of 60 characters or less"
                        ClientValidationFunction="DescriptionMaxLength" OnServerValidate="CustomValidatorDescription_ServerValidate"></asp:CustomValidator>
                </td>
            </tr>
            <tr>
                <td>Remarks
                </td>
                <td>
                    <telerik:RadTextBox ID="RadTextBoxRemarks" runat="server" Columns="100" Rows="8" TextMode="MultiLine"
                        Resize="Vertical">
                    </telerik:RadTextBox>
                </td>
                <td></td>
            </tr>
            <tr>
                <td>
                    <telerik:RadButton ID="RadButtonSubmit" runat="server" Text="Submit" OnClick="RadButtonSubmit_Click"></telerik:RadButton>
                </td>
                <td colspan="2">
                    <asp:Label ID="LabelResult" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
