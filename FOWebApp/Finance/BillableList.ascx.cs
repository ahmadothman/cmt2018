﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FOWebApp.net.nimera.cmt.BOInvoicingControllerWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;

namespace FOWebApp.Finance
{
    public partial class BillableList : System.Web.UI.UserControl
    {
        private BOInvoicingControllerWS _invoiceController = new BOInvoicingControllerWS();

        protected void RadGridBillableList_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            ((RadGrid)sender).MasterTableView.DataSource = _invoiceController.GetBillables();
        }

        protected void RadGridBillableList_ItemCommand(object sender, GridCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "DeleteBillable":
                    int billableId = int.Parse(e.CommandArgument.ToString());

                    Color alertForeColor = Color.Empty;
                    string alertText;

                    try
                    {
                        if (_invoiceController.DeleteBillable(billableId))
                        {
                            alertForeColor = Color.DarkGreen;
                            alertText = "Deletion succesful";

                            RadGridBillableList.DataSource = null; // Need to do this, otherwise the NeedDataSource event is not triggered on rebind
                            RadGridBillableList.Rebind();
                        }
                        else
                        {
                            alertForeColor = Color.DarkRed;
                            alertText = "Deletion failed";
                        }
                    }
                    catch (Exception ex)
                    {
                        BOLogControlWS boLogControlWS = new BOLogControlWS();

                        CmtApplicationException cmtApplicationException = new CmtApplicationException()
                        {
                            ExceptionDateTime = DateTime.Now,
                            ExceptionDesc = ex.Message,
                            ExceptionStacktrace = ex.StackTrace,
                            ExceptionLevel = 3,
                            UserDescription = "BillableList: Delete billable"
                        };

                        boLogControlWS.LogApplicationException(cmtApplicationException);

                        alertForeColor = Color.DarkRed;
                        alertText = "Deletion failed";
                    }

                    RadWindowManagerBillableList.RadAlert(string.Format("<span style=\"color: {0}\">{1}</span>",
                        ColorTranslator.ToHtml(alertForeColor), alertText), 300, 200, "Deletion Result", "deleteAlertCallback");

                    break;
            }
        }
    }
}