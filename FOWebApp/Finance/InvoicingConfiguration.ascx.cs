﻿using FOWebApp.net.nimera.cmt.BOInvoicingControllerWSRef;
using FOWebApp.net.nimera.cmt.BOConfigurationControlWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FOWebApp.Finance
{
    public partial class InvoicingConfiguration : System.Web.UI.UserControl
    {
        private BOInvoicingControllerWS _invoiceController = new BOInvoicingControllerWS();
        private BOConfigurationControllerWS _configurationController = new BOConfigurationControllerWS();
        private BOLogControlWS _logController = new BOLogControlWS();

        protected void Page_Load(object sender, EventArgs e)
        {
            //RadNumericTextBoxLastInvoiceNumber.Value = _invoiceController.GetLastInvoiceNumber();
            RadTextBoxCompanyName.Text = _configurationController.getParameter("CompanyName").Trim();
            RadTextBoxAddressLine1.Text =  _configurationController.getParameter("AddressLine1").Trim();
            RadTextBoxAddressLine2.Text = _configurationController.getParameter("AddressLine2").Trim();
            RadNumericTextBoxPostcode.Text = _configurationController.getParameter("PostCode").Trim();
            RadTextBoxLocation.Text = _configurationController.getParameter("Location").Trim();
            RadTextBoxCountry.Text = _configurationController.getParameter("Country").Trim();
            RadTextBoxVAT.Text = _configurationController.getParameter("VAT").Trim();

            RadTextBoxDetailsMessage.Text = _invoiceController.GetInvoiceMessage("DetailsMessage");
            RadTextBoxImportantMessage.Text = _invoiceController.GetInvoiceMessage("ImportantMessage");
            RadTextBoxInformativeMessage.Text = _invoiceController.GetInvoiceMessage("InformativeMessage");
            RadTextBoxThanksMessage.Text = _invoiceController.GetInvoiceMessage("ThanksMessage");
            RadTextBoxTermsAndConditions.Text = _invoiceController.GetInvoiceMessage("TermsAndConditions");
        }

        protected void RadButtonSaveConfiguration_Click(object sender, EventArgs e)
        {
            LabelResult.ForeColor = Color.DarkRed;
            LabelResult.Text = "Configuration settings could not be saved";
            //if (RadNumericTextBoxLastInvoiceNumber.Value < _invoiceController.GetLastInvoiceNumber())
            //{
            //    LabelResult.Text = "You can't reduce the last invoice number";
            //    return;
            //}
            try
            {
                //if (_invoiceController.SetLastInvoiceNumber((int)RadNumericTextBoxLastInvoiceNumber.Value))
                //{
                //    LabelResult.ForeColor = Color.DarkGreen;
                //    LabelResult.Text = "Configuration settings saved succesfully";
                //}
                _configurationController.setParameter("CompanyName", RadTextBoxCompanyName.Text);
                _configurationController.setParameter("AddressLine1", RadTextBoxAddressLine1.Text);
                _configurationController.setParameter("AddressLine2", RadTextBoxAddressLine2.Text);
                _configurationController.setParameter("PostCode", RadNumericTextBoxPostcode.Text);
                _configurationController.setParameter("Location", RadTextBoxLocation.Text);
                _configurationController.setParameter("Country", RadTextBoxCountry.Text);
                _configurationController.setParameter("VAT", RadTextBoxVAT.Text);

                _invoiceController.SetInvoiceMessage("DetailsMessage", RadTextBoxDetailsMessage.Text);
                _invoiceController.SetInvoiceMessage("ImportantMessage", RadTextBoxImportantMessage.Text);
                _invoiceController.SetInvoiceMessage("InformativeMessage", RadTextBoxInformativeMessage.Text);
                _invoiceController.SetInvoiceMessage("ThanksMessage", RadTextBoxThanksMessage.Text);
                _invoiceController.SetInvoiceMessage("TermsAndConditions", RadTextBoxTermsAndConditions.Text);

                LabelResult.ForeColor = Color.DarkGreen;
                LabelResult.Text = "Configuration settings saved succesfully";
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtApplicationException = new CmtApplicationException()
                {
                    ExceptionDateTime = DateTime.Now,
                    ExceptionDesc = ex.Message,
                    ExceptionStacktrace = ex.StackTrace,
                    ExceptionLevel = 3,
                    UserDescription = "InvoicingConfiguration: Save configuration settings"
                };

                _logController.LogApplicationException(cmtApplicationException);
            }
        }
    }
}