﻿using DotNet.Highcharts;
using DotNet.Highcharts.Enums;
using DotNet.Highcharts.Helpers;
using DotNet.Highcharts.Options;
using FOWebApp.Data;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web.Script.Serialization;
using System.Web.UI;

namespace FOWebApp
{
    public partial class GraphiteTestTab : System.Web.UI.UserControl
    {
        BOAccountingControlWS _boAccountingControl;
        public string MacAddress { get; set; }
        Terminal term;

        protected void Page_Load(object sender, EventArgs e)
        {
            _boAccountingControl = new BOAccountingControlWS();
            RadDatePickerStartDate.SelectedDate = DateTime.Now.AddDays(-7);
            RadDatePickerEndDate.SelectedDate = DateTime.Now;
            term = _boAccountingControl.GetTerminalDetailsByMAC(MacAddress);
            if (ViewState["GraphiteInitialTabSelection"] == null)
            {
                DateTime endDate = DateTime.Now.Date;
                int numDays = 30;
                DateTime startDate = endDate.AddDays(numDays * -1);
                endDate = endDate.AddDays(1);
                ViewState["GraphiteInitialTabSelection"] = false;
                GenerateChart(startDate, endDate);
            }
        }

        protected void RadButtonRefresh_Click(object sender, EventArgs e)
        {

        }

        protected void RadButtonDateSelection_Click(object sender, EventArgs e)
        {
            if ((bool)ViewState["GraphiteInitialTabSelection"] == false)
            {
                if (MacAddress != null && !MacAddress.Equals(""))
                {
                    DateTime startDateTime = (DateTime)RadDatePickerStartDate.SelectedDate;
                    DateTime endDateTime = (DateTime)RadDatePickerEndDate.SelectedDate;
                    if (startDateTime == endDateTime)
                    {
                        endDateTime = endDateTime.AddDays(1);
                    }
                    else if (endDateTime.Date == DateTime.Now.Date)
                    {
                        endDateTime = endDateTime.AddDays(1);
                    }

                    if (MacAddress != null && !MacAddress.Equals(""))
                    {
                        GenerateChart(startDateTime, endDateTime);
                    }
                }
            }
        }

        /// <summary>
        /// Generates an Highcharts graph with accumulated volume between a start and
        /// end date.
        /// </summary>
        /// <param name="startDate">Start date for the graph</param>
        /// <param name="EndDate">End Date for the graph</param>
        private void GenerateChart(DateTime startDateTime, DateTime endDateTime)
        {
            try
            {
                DateTime lastTerminalActivation = _boAccountingControl.GetTerminalActivations(term.SitId, term.IspId).LastOrDefault();
                if (lastTerminalActivation == null)
                {
                    lastTerminalActivation = Convert.ToDateTime(term.StartDate);
                }
                if (lastTerminalActivation > startDateTime)
                {
                    startDateTime = lastTerminalActivation;
                }
                string graphUrl = CreateGraphiteUrl(startDateTime, endDateTime);
                var javaScriptSerializer = new JavaScriptSerializer();
                javaScriptSerializer.MaxJsonLength = Int32.MaxValue;
                string jsonGraphData;
                using (WebClient client = new WebClient())
                {
                    jsonGraphData = client.DownloadString(graphUrl);
                }
                var graphdata = javaScriptSerializer.Deserialize<List<Graph>>(jsonGraphData);
                Highcharts chart = new Highcharts("chart").SetOptions(new GlobalOptions { Global = new DotNet.Highcharts.Options.Global { UseUTC = true } });
                chart.InitChart(new Chart { ZoomType = ZoomTypes.X, SpacingRight = 20 });
                chart.SetTitle(new Title { Text = "Traffic" });
                chart.SetSubtitle(new Subtitle { Text = "Click and drag in the plot area to zoom in" });
                chart.SetXAxis(new XAxis
                {
                    Type = AxisTypes.Datetime,
                    MinRange = 1800000,
                    Title = new XAxisTitle { Text = "" }
                });
                chart.SetYAxis(new YAxis
                {
                    Title = new YAxisTitle { Text = "BW (bit/s)" },
                    Min = 0.0,
                    StartOnTick = false,
                    EndOnTick = false
                });
                chart.SetTooltip(new Tooltip { Shared = true });
                chart.SetLegend(new Legend { Enabled = true, Align = HorizontalAligns.Center, VerticalAlign = VerticalAligns.Bottom, BorderWidth = 0 });
                chart.SetPlotOptions(new PlotOptions
                {
                    Area = new PlotOptionsArea
                    {
                        FillColor = null,
                        FillOpacity = 0.0,
                        LineWidth = 1,
                        Marker = new PlotOptionsAreaMarker
                        {
                            Enabled = false,
                            States = new PlotOptionsAreaMarkerStates
                            {
                                Hover = new PlotOptionsAreaMarkerStatesHover
                                {
                                    Enabled = true,
                                    Radius = 5
                                }
                            }
                            
                        },
                        Shadow = false,
                        States = new PlotOptionsAreaStates { Hover = new PlotOptionsAreaStatesHover { LineWidth = 1 } },
                        PointStart = new PointStart(startDateTime)
                    }
                });
                chart.SetSeries(new[]
                     {  new Series
                            {
                                Type = ChartTypes.Area,
                                Name = "Forward",
                                Data = new DotNet.Highcharts.Helpers.Data(graphdata.FirstOrDefault(x => x.target.Contains("FWD")).datapoints.Select( y => y.Swap(0, 1).ToList()).ToList().To2DArray())
                            },
                            new Series
                            {
                                Type = ChartTypes.Area,
                                Name = "Return",
                                Data = new DotNet.Highcharts.Helpers.Data(graphdata.FirstOrDefault(x => x.target.Contains("RTN")).datapoints.Select( y => y.Swap(0, 1).ToList()).ToList().To2DArray())
                            }
                         });
                ScriptManager.RegisterStartupScript(Page, typeof(Page), "chartKey", chart.ToJavaScriptString(), true);
            }
            catch (Exception ex)
            {
                BOLogControlWS boLogControlWS = new BOLogControlWS();
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDateTime = DateTime.Now;
                cmtEx.ExceptionDesc = ex.Message;
                cmtEx.UserDescription = "GraphiteTestTab: RadButtonDateSelection_Click";
                cmtEx.StateInformation = "";
                cmtEx.ExceptionLevel = 3;
                boLogControlWS.LogApplicationException(cmtEx);
            }
        }

        private string CreateGraphiteUrl(DateTime startDate, DateTime endDate)
        {
            //string graphUrl = "http://nms.satadsl.net:8082/" +
            string graphite = ConfigurationManager.AppSettings["Graphite"];
            string graphUrl = graphite +
           "/render?target=scale(Term./IPADD/.FWD%2C0.125)" +
           "&target=scale(Term./IPADD/.RTN%2C0.125)" +
           "&from=00%3A00_/FROMDT/&until=00%3A00_/TODT/";
            //Get the IP Address
            
            graphUrl = graphUrl.Replace("/IPADD/", term.IpAddress.Trim());
            graphUrl = graphUrl.Replace("/FROMDT/", startDate.ToString("yyyyMMdd"));
            graphUrl = graphUrl.Replace("/TODT/", endDate.ToString("yyyyMMdd"));
            graphUrl += "&format=json";
            return graphUrl;
        }
    }
}