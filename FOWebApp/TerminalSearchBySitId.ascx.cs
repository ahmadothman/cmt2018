﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;

namespace FOWebApp
{
    public partial class TerminalSearchBySitId : System.Web.UI.UserControl
    {
        BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void ButtonSearch_Click(object sender, EventArgs e)
        {
            // Gets the MacAddress (method below) by SitId and IspId, which are filled in by the client in TextBoxes the front end, and fires the TerminalDetailsWindow 
            // (RadWindowTerminalDetails from the front end.
            string macAddress = getMacAddress(TextBoxValue.Text, TextBoxISP.Text);
            RadWindowTerminalDetails.NavigateUrl = "/TerminalDetailsForm.aspx?mac=" + macAddress;
            string script = "function f(){$find(\"" + RadWindowTerminalDetails.ClientID + "\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "TerminalDetailsOpenScript", script, true);
        }

        /// <summary>
        /// Returns a MAC Address for the given sitId
        /// </summary>
        /// <param name="sitId"></param>
        /// <returns></returns>
        private string getMacAddress(string sitId, string ispId)
        {
            string macAddress = "";
            try
            {
                int iSitId = Int32.Parse(sitId);
                int iIspId = Int32.Parse(ispId);
                BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();
                Terminal terminal = boAccountingControl.GetTerminalDetailsBySitId(iSitId, iIspId);

                if (terminal != null)
                {
                    macAddress = terminal.MacAddress;
                }
            }
            catch (Exception ex)
            {
                BOLogControlWS boLogControlWS = new BOLogControlWS();
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDateTime = DateTime.Now;
                cmtEx.ExceptionDesc = ex.Message;
                cmtEx.UserDescription = "Method: TerminalSearchBySitId.getMacAddress";
                cmtEx.StateInformation = "SitId: " + sitId;
                boLogControlWS.LogApplicationException(cmtEx);
            }
            return macAddress;
        }
    }
}