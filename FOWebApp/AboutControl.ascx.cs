﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

namespace FOWebApp
{
    public partial class AboutControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            LabelVersion.Text = FOWebApp.Properties.Settings.Default.Version;
            LabelReleaseDate.Text = FOWebApp.Properties.Settings.Default.ReleaseDate;
            LabelCopyright.Text = "2011, " + DateTime.Now.Year;
            String[] userRoles = Roles.GetRolesForUser();

            foreach (string userRole in userRoles)
            {
                LabelRoles.Text = LabelRoles.Text + userRole + ", ";
            }
        }
    }
}