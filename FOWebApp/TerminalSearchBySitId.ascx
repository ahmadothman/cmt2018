﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TerminalSearchBySitId.ascx.cs" Inherits="FOWebApp.TerminalSearchBySitId" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


<div id="searchDiv">
    <table>
        <tr>
            <td>
                <asp:Label ID="LabelValue" runat="server" Text="Sit Id:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxValue" runat="server"></asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBoxValue"
                    ErrorMessage="Please enter a valid Sit Id" Display="Dynamic">
                </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelISPValue" runat="server" Text="ISP:"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="TextBoxISP" runat="server"></asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorIspId" runat="server" ControlToValidate="TextBoxISP"
                    ErrorMessage="Please enter a valid ISP" Display="Dynamic">
                </asp:RequiredFieldValidator>
            </td>
            <td>
                <asp:Button ID="ButtonSearch" runat="server" Text="Search" OnClick="ButtonSearch_Click" />
            </td>
        </tr>
    </table>
</div>
<telerik:RadWindowManager ID="RadWindowManager1" runat="server" Animation="None" AutoSize="False" Width="1000px" Height="700px"
    KeepInScreenBounds="True" EnableShadow="True" EnableAriaSupport="True" VisibleStatusbar="True" Modal="True" DestroyOnClose="True" ShowContentDuringLoad="false">
    <Windows>
        <telerik:RadWindow ID="RadWindowTerminalDetails" Title="Terminal Details" runat="server" NavigateUrl="TerminalDetailsForm.aspx">
        </telerik:RadWindow>
    </Windows>
</telerik:RadWindowManager>
