﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DocumentLibrary.ascx.cs" Inherits="FOWebApp.DocumentLibrary" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<h1>Documents</h1>
<telerik:RadGrid ID="RadGridDocuments" runat="server" OnNeedDataSource="RadGridDocuments_NeedDataSource" AutoGenerateColumns="False" CellSpacing="0" GridLines="None" Skin="Metro">
    <MasterTableView NoMasterRecordsText="No Documents in Archive">
        <RowIndicatorColumn Visible="False">
        </RowIndicatorColumn>
        <ExpandCollapseColumn Created="True">
        </ExpandCollapseColumn>
        <Columns>
            <telerik:GridHyperLinkColumn AllowSorting="False" DataTextField="Name" FilterControlAltText="Filter Name column" HeaderText="Document Name" UniqueName="Name" DataNavigateUrlFields="Name" DataNavigateUrlFormatString="~/Servlets/DocumentDownload.aspx?DocumentName={0}">
            </telerik:GridHyperLinkColumn>
            <telerik:GridBoundColumn DataField="Description" FilterControlAltText="Filter column column" HeaderText="Description" UniqueName="column">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="DateUploaded" DataType="System.DateTime" FilterControlAltText="Filter DateUploaded column" HeaderText="Date Uploaded" UniqueName="DateUploaded">
            </telerik:GridBoundColumn>
        </Columns>
    </MasterTableView>
</telerik:RadGrid>