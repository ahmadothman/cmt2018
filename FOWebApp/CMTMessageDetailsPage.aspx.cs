﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FOWebApp
{
    public partial class CMTMessageDetailsPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Load the details form and initialize the data
            string id = Request.Params["id"].ToString();

            CMTMessageDetailsForm df =
                (CMTMessageDetailsForm)Page.LoadControl("CMTMessageDetailsForm.ascx");

            df.componentDataInit(id);

            PlaceHolderMessageDetailsForm.Controls.Add(df);
        }
    }
}