﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TerminalAlarms.ascx.cs" Inherits="FOWebApp.TerminalAlarms" %>
<link href="../Content/bootstrap.min.css" rel="stylesheet" type="text/css" />

<style>
    .alarmMargins {
        margin-top: 5px;
        margin-right: 10px;
        margin-bottom: 5px;
    }
</style>

<%--<telerik:RadComboBox ID="RadComboBoxDistributor" runat="server" Skin="Metro"
    Width="272px" Visible="false" AutoPostBack="true" OnSelectedIndexChanged="RadComboBoxDistributor_SelectedIndexChanged" class="form-control alarmMargins">
</telerik:RadComboBox>--%>

<div class="col-xs-4 alarmMargins">
    <asp:DropDownList ID="distributorSelect" class="form-control" runat="server" OnSelectedIndexChanged="Distributor_SelectedIndexChanged" AutoPostBack="true" Visible="false">
    </asp:DropDownList>
</div>

<div id="table_container" class="alarmMargins" runat="server"></div>
<div id="chart_container" class="alarmMargins"></div>

<telerik:RadScriptBlock ID="RadScriptBlockHighchart" runat="server">
    <script type="text/javascript" src="../Scripts/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="../Scripts/highcharts.js"></script>
    <script type="text/javascript" src="../Scripts/drilldown.js"></script>
    <script type="text/javascript" src="../Scripts/exporting.js"></script>
    <script type="text/javascript" src="../Scripts/offline-exporting.js"></script>
</telerik:RadScriptBlock>

