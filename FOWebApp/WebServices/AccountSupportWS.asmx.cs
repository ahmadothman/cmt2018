﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOServicesControllerWSRef;

namespace FOWebApp.WebServices
{
    /// <summary>
    /// Summary description for AccountSupportWS
    /// </summary>
    [WebService(Namespace = "http://nimera.net/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class AccountSupportWS : System.Web.Services.WebService
    {
        /// <summary>
        /// Check if the given MAC address is true
        /// </summary>
        /// <param name="macAddress"></param>
        /// <returns></returns>
        [WebMethod]
        public Boolean IsMacAddressUnique(string macAddress)
        {
            Boolean isUnique = false;

            BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();
            Terminal term = boAccountingControl.GetTerminalDetailsByMAC(macAddress);

            if (term != null)
            {
                //Terminal already exists, clear the mac address field and show an error
                //window
                isUnique = false;
            }
            else
            {
                isUnique = true;
            }
            return isUnique;
        }

        /// <summary>
        /// Adds an ISP to a distributor
        /// </summary>
        /// <remarks>
        /// This method calls the parallel method of the BOAccountingControl website
        /// </remarks>
        /// <param name="distributorId">The distributor identifier</param>
        /// <param name="ispId">The ISP identifier</param>
        /// <returns>True if the ISP was successfully added to the Distributor</returns>
        [WebMethod]
        public bool AddISPToDistributor(string distributorId, string ispId)
        {
            BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();
            return boAccountingControl.AddISPToDistributor(Int32.Parse(distributorId), Int32.Parse(ispId));
        }

        /// <summary>
        /// This method de-allocates an ISP from a distributor
        /// </summary>
        /// <remarks>
        /// This method calls the parallel method of the BOAccountingControl website
        /// </remarks>
        /// <param name="distributorId">The distributor identifier</param>
        /// <param name="ispId">The ISP identifier</param>
        /// <returns>True if the de-allocation succeeded</returns>
        [WebMethod]
        public bool RemoveISPFromDistributor(string distributorId, string ispId)
        {
            BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();
            return boAccountingControl.RemoveISPFromDistributor(Int32.Parse(distributorId), Int32.Parse(ispId));
        }

        /// <summary>
        /// Links a Service to a VNO. This adds a record to the ServicesVNOs table.
        /// </summary>
        /// <param name="VNOId">The VNO ID the service should be linked to</param>
        /// <param name="ServiceId">The ID of the service to be linked to the VNO</param>
        /// <returns>True if successful, else false</returns>
        [WebMethod]
        public bool LinkServiceToVNO(string VNOId, string ServiceId)
        {
            BOServicesControllerWS _controller = new BOServicesControllerWS();
            return _controller.LinkServiceToVNO(Convert.ToInt32(VNOId), ServiceId);
        }

        /// <summary>
        /// Removes the link between a Service and a VNO.
        /// </summary>
        /// <param name="VNOId">The VNO ID the service should be removed from</param>
        /// <param name="ServiceId">The ID of the service to be removed from the VNO</param>
        /// <returns>True if successful, else false</returns>
        [WebMethod]
        public bool RemoveServiceFromVNO(string VNOId, string ServiceId)
        {
            BOServicesControllerWS _controller = new BOServicesControllerWS();
            return _controller.RemoveServiceFromVNO(Convert.ToInt32(VNOId), ServiceId);
        }

        /// <summary>
        /// Links a Service to a role. This adds a record to the ServicesRoles table.
        /// </summary>
        /// <param name="RoleName">The name of the role the service should be linked to</param>
        /// <param name="ServiceId">The ID of the service to be linked to the role</param>
        /// <returns>True if successful, else false</returns>
        [WebMethod]
        public bool LinkServiceToRole(string RoleName, string ServiceId)
        {
            BOServicesControllerWS _controller = new BOServicesControllerWS();
            return _controller.LinkServiceToRole(RoleName, ServiceId);
        }

        /// <summary>
        /// Removes the link between a Service and a role.
        /// </summary>
        /// <param name="RoleName">The name of the role the service should be removed from</param>
        /// <param name="ServiceId">The ID of the service to be removed from the role</param>
        /// <returns>True if successful, else false</returns>
        [WebMethod]
        public bool RemoveServiceFromRole(string RoleName, string ServiceId)
        {
            BOServicesControllerWS _controller = new BOServicesControllerWS();
            return _controller.RemoveServiceFromRole(RoleName, ServiceId);
        }

        /// <summary>
        /// Returns true if the given ispId belongs to a secondary ISP.
        /// </summary>
        /// <remarks>
        /// <b>>Some definitions:</b>
        /// - Edge NMS, This is the Network Management System which connects to the teleports’ satellite modem infrastructure. 
        /// An Edge NMS manages the terminals and applies the SLAs configured by the Satellite operator. 
        /// - Secondary NMS or Slave NMS, This component is in essence an extension of an Edge NMS. In general a Secondary NMS is 
        /// connected to the Edge NMS by means of a GRE tunnel and manages those Network Elements (NEs) which provide 
        /// an additional functionality to the network. These NEs include Traffic or Packet Shapers, Bandwidth managers etc. 
        /// </remarks>
        /// <param name="ispId">The unique identifier of the ISP</param>
        /// <returns>True if the ISP is indeed a secondary ISP, configured on a secondary NMS</returns>
        [WebMethod]
        public bool IsSecondaryISP(string ispId)
        {
            BOAccountingControlWS controller = new BOAccountingControlWS();
            Isp isp = controller.GetISP(Int32.Parse(ispId));
            return (isp.ISPType == 2);
        }

        /// <summary>
        /// Returns a list of SLAs for a given isp
        /// </summary>
        /// <param name="ispId">The unique ISP Idnetifier</param>
        /// <returns>A list of ISPs</returns>
        [WebMethod]
        public ServiceLevel[] GetServicePacksForIsp(string ispId)
        {
            BOAccountingControlWS controller = new BOAccountingControlWS();
            ServiceLevel[] SLAs = controller.GetServicePacksByIsp(Int32.Parse(ispId));
            return SLAs;
        }
    }
}
