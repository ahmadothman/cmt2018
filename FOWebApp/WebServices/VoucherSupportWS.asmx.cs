﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Security;
using System.IO;
using FOWebApp.net.nimera.cmt.BOVoucherControllerWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;

namespace FOWebApp.WebServices
{
    /// <summary>
    /// This webservice offers Voucher support to web pages
    /// </summary>
    [WebService(Namespace = "http://nimera.net/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService]
    public class VoucherSupportWS : System.Web.Services.WebService
    {
        /// <summary>
        /// Creates numVouchers of Vouchers for the given distributor
        /// </summary>
        /// <param name="distributorId"></param>
        /// <param name="numVouchers"></param>
        /// <returns></returns>
        [WebMethod]
        public string CreateVouchers(int distributorId, int numVouchers, int volume)
        {
            string resultMsg = "";

            BOLogControlWS boLogControl = new BOLogControlWS();

            string voucherPath = FOWebApp.Properties.Settings.Default.VoucherPath;

            FileInfo fi = new FileInfo(voucherPath);
            if (!fi.Directory.Exists)
            {
                fi.Directory.Create();
            }

            //Get the current user.
            BOVoucherControllerWS boVoucherController = new BOVoucherControllerWS();
            int batchId;

            MembershipUser myObject = Membership.GetUser();
            Guid userId = new Guid(myObject.ProviderUserKey.ToString());
            try
            {
                Voucher[] vouchers = boVoucherController.CreateVouchers(distributorId, numVouchers, userId, volume, out batchId);
                resultMsg = "<b>Done! Download vouchers <a href=\"Servlets/VoucherDownload.aspx?BatchId=" + batchId + "\">here</a></b>";
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDateTime = DateTime.Now;
                cmtEx.ExceptionStacktrace = ex.StackTrace;
                cmtEx.ExceptionDesc = ex.Message;
                cmtEx.UserDescription = "Creating voucher output file failed";
                boLogControl.LogApplicationException(cmtEx);
                resultMsg = "<b>Voucher creation failed</b>";
            }

            return resultMsg;
        }

        /// <summary>
        /// Returns the avaialble vouchers for the given distributor. The list includes both 
        /// released and un-released voucher batches
        /// </summary>
        /// <param name="distributorId">The distributor identifier</param>
        /// <returns>A list of VoucherBatchInfo objects. This list can be empty</returns>
        [WebMethod]
        public VoucherBatchInfo[] GetAvailableVouchers(int distributorId)
        {
            BOVoucherControllerWS boVoucherController = new BOVoucherControllerWS();
            return boVoucherController.GetAvailableVoucherBatches(distributorId);
        }

        /// <summary>
        /// Returns the available AND released vouchers for the given distributor. So in this
        /// list only the voucher batches with a release flag set to true are included
        /// </summary>
        /// <param name="distributorId">The distributor identifier</param>
        /// <returns></returns>
        [WebMethod]
        public VoucherBatchInfo[] GetReleasedVouchers(int distributorId)
        {
            BOVoucherControllerWS boVoucherController = new BOVoucherControllerWS();
            return boVoucherController.GetReleasedVoucherBatches(distributorId);
        }

        [WebMethod]
        public Boolean ValidateVoucher(string code, string crc, string macAddress)
        {
            BOVoucherControllerWS boVoucherController = new BOVoucherControllerWS();
            return boVoucherController.ValidateVoucher(code, crc, macAddress);
        }

        [WebMethod]
        public Boolean ResetVoucher(string code, string crc)
        {
            BOVoucherControllerWS boVoucherController = new BOVoucherControllerWS();
            return boVoucherController.ResetVoucher(code, crc);
        }

        /// <summary>
        /// Returns the avaialble vouchers for the given distributor. The list includes both 
        /// released and un-released voucher batches
        /// </summary>
        /// <param name="distributorId">The distributor identifier</param>
        /// <returns>A list of VoucherBatchInfo objects. This list can be empty</returns>
        [WebMethod]
        public VoucherBatchInfo[] GetVouchersBatchesReport(int distributorId,int paid,DateTime? startDate, DateTime? endDate)
        {
                        
            BOVoucherControllerWS boVoucherController = new BOVoucherControllerWS();
            return boVoucherController.getVouchersBatchesReport(distributorId,paid, startDate, endDate);
        }

    }
}
