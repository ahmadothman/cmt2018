﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CMTMessageDetailsForm.ascx.cs" Inherits="FOWebApp.CMTMessageDetailsForm" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<style type="text/css">
    .auto-style2 {
        width: 102%;
    }
</style>
<telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
    <script type="text/javascript">
        function DoBlur(fld) {
            fld.className = 'normalfld';
        }

        function DoFocus(fld) {
            fld.className = 'focusfld';
        }

        function DoChange(fld) {
            fld.className = 'changedfld';
        }
    </script>
</telerik:RadCodeBlock>
<telerik:RadAjaxPanel ID="RadAjaxPanelIssueDetails" runat="server" GroupingText="Issue details" Height="200px" Width="232px"
    EnableHistory="True" Style="margin-right: 114px">
    <table cellpadding="10" cellspacing="5">
        <tr>
            <td><b>Sent to:</b></td>
            <td>
                <asp:Label ID="LabelRecipient" runat="server"></asp:Label>
            </td>
            <td>
                <asp:CheckBox ID="CheckBoxRelease" runat="server" Text="Release:" ToolTip="Leave unchecked during the authoring of the message" TextAlign="Left" />
            </td>
        </tr>
        <tr>
            <td><b>Visible from:</b></td>
            <td colspan="2"><telerik:RadDateTimePicker ID="RadDateTimePickerStartTime" runat="server" Enabled="false"></telerik:RadDateTimePicker></td>
        </tr>
        <tr>
            <td><b><asp:Label ID="LabelEndTime" runat="server" Text="Visible until:"></asp:Label></b></td>
            <td colspan="2"><telerik:RadDateTimePicker ID="RadDateTimePickerEndTime" runat="server" Enabled="false"></telerik:RadDateTimePicker></td>
        </tr>
        <tr>
            <td><b>Message title:</b></td>
            <td colspan="2"><asp:TextBox ID="TextBoxTitle" runat="server" ReadOnly="true" Width="600px"></asp:TextBox></td>
        </tr>
        <tr>
            <td><asp:Label ID="LabelContent1" runat="server" Text="Message content:" Font-Bold="True" /></td>
            <td colspan="2"><asp:Label ID="LabelContent2" runat="server"></asp:Label></td>
        </tr>
        <tr>
            <td colspan="3"><asp:Label ID="LabelContent3" runat="server" Visible="false" Text="Message content:" Font-Bold="True"/></td>
        </tr>
        <tr>
            <td colspan="3"><telerik:RadEditor ID="RadEditorMessage" runat="server" Visible="false" 
                    ImageManager-ViewPaths="~/Content/Images" ImageManager-UploadPaths="~/Content/Images" 
                            DocumentManager-ViewPaths="~/Content/Documents" DocumentManager-UploadPaths="~/Content/Documents" DocumentManager-MaxUploadFileSize="5000000">
                    <Tools>
                        <telerik:EditorToolGroup Tag="MainToolbar"> 
                            <telerik:EditorTool Name="Print" ShortCut="CTRL+P" />
                            <telerik:EditorTool Name="AjaxSpellCheck" />
                            <telerik:EditorTool Name="FindAndReplace" ShortCut="CTRL+F" />
                            <telerik:EditorTool Name="SelectAll" ShortCut="CTRL+A" />
                            <telerik:EditorTool Name="Cut" />
                            <telerik:EditorTool Name="Copy" ShortCut="CTRL+C" />
                            <telerik:EditorTool Name="Paste" ShortCut="CTRL+V" />
                            <telerik:EditorSeparator />
                            <telerik:EditorSplitButton Name="Undo">
                            </telerik:EditorSplitButton>
                            <telerik:EditorSplitButton Name="Redo">
                            </telerik:EditorSplitButton>
                        </telerik:EditorToolGroup>
                        <telerik:EditorToolGroup Tag="InsertToolbar">
                            <telerik:EditorTool Enabled="True" Name="ImageManager" ShortCut="CTRL+G" />
                            <telerik:EditorTool Enabled="True" Name="DocumentManager" />
                            <telerik:EditorTool Name="LinkManager" ShortCut="CTRL+K" />
                            <telerik:EditorTool Name="Unlink" ShortCut="CTRL+SHIFT+K" />
                        </telerik:EditorToolGroup>
                        <telerik:EditorToolGroup>
                            <telerik:EditorTool Name="Superscript" />
                            <telerik:EditorTool Name="Subscript" />
                            <telerik:EditorTool Name="InsertHorizontalRule" />
                            <telerik:EditorSeparator />
                            <telerik:EditorTool Name="FormatCodeBlock" />
                        </telerik:EditorToolGroup>
                        <telerik:EditorToolGroup>
                            <telerik:EditorDropDown Name="FormatBlock">
                            </telerik:EditorDropDown>
                            <telerik:EditorDropDown Name="FontName">
                            </telerik:EditorDropDown>
                            <telerik:EditorDropDown Name="RealFontSize">
                            </telerik:EditorDropDown>
                        </telerik:EditorToolGroup>
                        <telerik:EditorToolGroup>
                            <telerik:EditorTool Name="Bold" ShortCut="CTRL+B" />
                            <telerik:EditorTool Name="Italic" ShortCut="CTRL+I" />
                            <telerik:EditorTool Name="Underline" ShortCut="CTRL+U" />
                            <telerik:EditorTool Name="StrikeThrough" />
                            <telerik:EditorSeparator />
                            <telerik:EditorTool Name="JustifyLeft" />
                            <telerik:EditorTool Name="JustifyCenter" />
                            <telerik:EditorTool Name="JustifyRight" />
                            <telerik:EditorTool Name="JustifyFull" />
                            <telerik:EditorTool Name="JustifyNone" />
                            <telerik:EditorSeparator />
                            <telerik:EditorTool Name="Indent" />
                            <telerik:EditorTool Name="Outdent" />
                            <telerik:EditorSeparator />
                            <telerik:EditorTool Name="InsertOrderedList" />
                            <telerik:EditorTool Name="InsertUnorderedList" />
                            <telerik:EditorSeparator />
                            <telerik:EditorTool Name="ToggleTableBorder" />
                        </telerik:EditorToolGroup>
                        <telerik:EditorToolGroup>
                            <telerik:EditorSplitButton Name="ForeColor">
                            </telerik:EditorSplitButton>
                            <telerik:EditorSplitButton Name="BackColor">
                            </telerik:EditorSplitButton>
                            <telerik:EditorToolStrip Name="FormatStripper">
                            </telerik:EditorToolStrip>
                        </telerik:EditorToolGroup>
                        <telerik:EditorToolGroup Tag="DropdownToolbar">
                            <telerik:EditorSplitButton Name="InsertSymbol">
                            </telerik:EditorSplitButton>
                            <telerik:EditorToolStrip Name="InsertTable">
                            </telerik:EditorToolStrip>
                            <telerik:EditorSeparator />
                            <telerik:EditorTool Name="ConvertToLower" />
                            <telerik:EditorTool Name="ConvertToUpper" />
                            <telerik:EditorSeparator />
                            <telerik:EditorTool Name="ToggleScreenMode" ShortCut="F11" />
                            <telerik:EditorTool Name="AboutDialog" />
                        </telerik:EditorToolGroup>
                    </Tools>
                    <Content>
                    </Content>
                    <TrackChangesSettings CanAcceptTrackChanges="False"></TrackChangesSettings>
                </telerik:RadEditor>
            </td>
        </tr>
        <tr>
            <td colspan="3"><asp:Button ID="ButtonUpdate" runat="server" OnClick="ButtonUpdate_Click" Text="Update message" Visible="false" Enabled="false"/>&nbsp;<asp:Label ID="LabelResult" runat="server"></asp:Label></td>
        </tr>
    </table>
</telerik:RadAjaxPanel>
<asp:Label ID="HiddenLabelId" runat="server" Visible="false"></asp:Label>