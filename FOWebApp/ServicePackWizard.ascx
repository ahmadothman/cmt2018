﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ServicePackWizard.ascx.cs" Inherits="FOWebApp.ServicePackWizard" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<style type="text/css">
    .auto-style1 {
        height: 47px;
    }
</style>
<script language="javascript" type="text/javascript">

    function OnClientSelectedIndexChanged(sender, eventArgs) {
        var item = eventArgs.get_item();
        FOWebApp.WebServices.AccountSupportWS.IsSecondaryISP(item.get_value(), OnRequestComplete, OnError)
    }

    function OnRequestComplete(result) {
        var textBoxEdgeISPId = $find("<%= RadNumericTextBoxEdgeISP.ClientID %>");
        var textBoxEdgeSLA = $find("<%= RadNumericTextBoxEdgeSLA.ClientID %>");
        if (result) {
            textBoxEdgeISPId.enable();
            textBoxEdgeSLA.enable();
        }
        else {
            textBoxEdgeISPId.set_value("");
            textBoxEdgeISPId.disable();
            textBoxEdgeSLA.set_value("");
            textBoxEdgeSLA.disable();
        }
    }

    function OnError() {
        alert("Error while checking for secondary ISP");
    }

    function DisplayElement(element) {
        x = element.value;
        if (x == "1") {
            document.getElementById('agg1').style.display = '';
            document.getElementById('agg2').style.display = '';
            document.getElementById('agg3').style.display = '';
            document.getElementById('agg4').style.display = '';
            document.getElementById('sep1').style.display = 'none';
            document.getElementById('sep2').style.display = 'none';
            document.getElementById('sep3').style.display = 'none';
            document.getElementById('sep4').style.display = 'none';
            document.getElementById('sep5').style.display = 'none';
            document.getElementById('sep6').style.display = 'none';
            document.getElementById('sep7').style.display = 'none';
            document.getElementById('sep8').style.display = 'none';
            document.getElementById('selectType').style.display = 'none';
        }
        else if (x == "2") {
            document.getElementById('agg1').style.display = 'none';
            document.getElementById('agg2').style.display = 'none';
            document.getElementById('agg3').style.display = 'none';
            document.getElementById('agg4').style.display = 'none';
            document.getElementById('sep1').style.display = '';
            document.getElementById('sep2').style.display = '';
            document.getElementById('sep3').style.display = '';
            document.getElementById('sep4').style.display = '';
            document.getElementById('sep5').style.display = '';
            document.getElementById('sep6').style.display = '';
            document.getElementById('sep7').style.display = '';
            document.getElementById('sep8').style.display = '';
            document.getElementById('selectType').style.display = 'none';
        }
        else {
            document.getElementById('agg1').style.display = 'none';
            document.getElementById('agg2').style.display = 'none';
            document.getElementById('agg3').style.display = 'none';
            document.getElementById('agg4').style.display = 'none';
            document.getElementById('sep1').style.display = 'none';
            document.getElementById('sep2').style.display = 'none';
            document.getElementById('sep3').style.display = 'none';
            document.getElementById('sep4').style.display = 'none';
            document.getElementById('sep5').style.display = 'none';
            document.getElementById('sep6').style.display = 'none';
            document.getElementById('sep7').style.display = 'none';
            document.getElementById('sep8').style.display = 'none';
            document.getElementById('selectType').style.display = '';
        }
    }
</script>
<asp:Wizard ID="Wizard" runat="server" ActiveStepIndex="0" Height="483px" Width="700px" OnFinishButtonClick="Wizard_FinishButtonClick" OnNextButtonClick="Wizard_NextButtonClick" OnPreviousButtonClick="Wizard_PreviousButtonClick">
    <SideBarTemplate>
        <asp:DataList ID="SideBarList" runat="server">
            <ItemTemplate>
                <asp:LinkButton ID="SideBarButton" runat="server"></asp:LinkButton>
            </ItemTemplate>
            <SelectedItemStyle Font-Bold="True" />
        </asp:DataList>
    </SideBarTemplate>
    <WizardSteps>
        <asp:WizardStep ID="WizardStep1" Title=" " runat="server" OnActivate="WizardStep1_Activate">
            <h1>General information</h1>
            <table cellpadding="2" cellspacing="5">
                <tr>
                    <td>ISP:</td>
                    <td>
                        <telerik:RadComboBox ID="RadComboBoxISPId" runat="server" Width="300px" OnClientSelectedIndexChanged="OnClientSelectedIndexChanged" Skin="Metro"></telerik:RadComboBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="You must select an ISP!"
                            ControlToValidate="RadComboBoxISPId" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>Service Line:</td>
                    <td>
                        <telerik:RadComboBox ID="RadComboBoxServiceLine" runat="server" Width="300px" Skin="Metro"></telerik:RadComboBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="You must select a Service Line!"
                            ControlToValidate="RadComboBoxServiceLine" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>Service Pack ID:</td>
                    <td>
                        <asp:TextBox ID="TextBoxId" runat="server" Columns="4" Width="100px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="You must provide an SLA ID!"
                            ControlToValidate="TextBoxId" SetFocusOnError="True" Display="Dynamic"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidatorId" runat="server" ErrorMessage="The SLA ID can only contain digits"
                            ControlToValidate="TextBoxId" ValidationExpression="\d+" Display="Dynamic" />
                        <asp:CustomValidator ID="CustomValidatorId" runat="server" ErrorMessage="There has already a service pack been found with this ID. Please provide a unique ID."
                            OnServerValidate="CustomValidatorId_ServerValidate" ControlToValidate="TextBoxId" Display="Dynamic" />
                    </td>
                </tr>
                <tr>
                    <td>Name:</td>
                    <td>
                        <asp:TextBox ID="TextBoxName" runat="server" Columns="50" Width="300px">
                        </asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="You must provide an SLA Name!"
                            ControlToValidate="TextBoxName" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>Common Name:</td>
                    <td>
                        <asp:TextBox ID="TextBoxSlaCommonName" runat="server" Columns="50" Width="300px">
                        </asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LabelEdgeISP" runat="server" Text="Edge ISP:"></asp:Label>
                    </td>
                    <td>
                        <telerik:RadNumericTextBox ID="RadNumericTextBoxEdgeISP" runat="server" InputType="Number" Enabled="False">
                            <telerik:NumberFormat DecimalDigits="0"></telerik:NumberFormat>
                            </telerik:RadNumericTextBox>
                        <%--<asp:RequiredFieldValidator runat="server" ErrorMessage="Please provide an Edge ISP" ControlToValidate="RadNumericTextBoxEdgeISP"></asp:RequiredFieldValidator>--%>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LabelEdgeSLA" runat="server" Text="Edge SLA:"></asp:Label>
                    </td>
                    <td>
                        <telerik:RadNumericTextBox ID="RadNumericTextBoxEdgeSLA" runat="server" InputType="Number" Enabled="False"></telerik:RadNumericTextBox>
                    </td>
                </tr>
            </table>
        </asp:WizardStep>
        <asp:WizardStep ID="WizardStep2" Title=" " runat="server" OnActivate="WizardStep2_Activate">
            <h1>Bandwidth</h1>
            <table cellpadding="2" cellspacing="5">
                <tr>
                    <td>
                        <asp:Label ID="LabelCIRFWD" runat="server">CIR forward:</asp:Label></td>
                    <td>
                        <telerik:RadNumericTextBox ID="RadNumericTextBoxCIRFWD" runat="server" ShowSpinButtons="True" Culture="nl-BE" Skin="Metro" Value="0" Width="125px">
                            <NumberFormat ZeroPattern="n" DecimalDigits="0"></NumberFormat>
                        </telerik:RadNumericTextBox><asp:Label ID="LabelKbps5" runat="server"> Kbps</asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LabelCIRRTN" runat="server">CIR return:</asp:Label></td>
                    <td>
                        <telerik:RadNumericTextBox ID="RadNumericTextBoxCIRRTN" runat="server" ShowSpinButtons="True" Culture="nl-BE" Skin="Metro" Value="0" Width="125px">
                            <NumberFormat ZeroPattern="n" DecimalDigits="0"></NumberFormat>
                        </telerik:RadNumericTextBox><asp:Label ID="LabelKbps6" runat="server"> Kbps</asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LabelForwardSpeed" runat="server">Forward speed:</asp:Label></td>
                    <td>
                        <telerik:RadNumericTextBox ID="RadNumericTextBoxForwardDefault" runat="server" ShowSpinButtons="True" Culture="nl-BE" Skin="Metro" Value="0" Width="125px">
                            <NumberFormat ZeroPattern="n" DecimalDigits="0"></NumberFormat>
                        </telerik:RadNumericTextBox><asp:Label ID="LabelKbps1" runat="server"> Kbps</asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LabelReturnSpeed" runat="server">Return speed:</asp:Label></td>
                    <td>
                        <telerik:RadNumericTextBox ID="RadNumericTextBoxReturnDefault" runat="server" ShowSpinButtons="True" Culture="nl-BE" Skin="Metro" Value="0" Width="125px">
                            <NumberFormat ZeroPattern="n" DecimalDigits="0"></NumberFormat>
                        </telerik:RadNumericTextBox><asp:Label ID="LabelKbps2" runat="server"> Kbps</asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LabelMinBWFWD" runat="server">Minimum forward bandwidth:</asp:Label></td>
                    <td>
                        <telerik:RadNumericTextBox ID="RadNumericTextBoxMinBWFWD" runat="server" ShowSpinButtons="True" Culture="nl-BE" Skin="Metro" Value="0" Width="125px">
                            <NumberFormat ZeroPattern="n" DecimalDigits="0"></NumberFormat>
                        </telerik:RadNumericTextBox><asp:Label ID="LabelKbps7" runat="server"> Kbps</asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LabelMinBWRTN" runat="server">Minimum return bandwidth:</asp:Label></td>
                    <td>
                        <telerik:RadNumericTextBox ID="RadNumericTextBoxMinBWRTN" runat="server" ShowSpinButtons="True" Culture="nl-BE" Skin="Metro" Value="0" Width="125px">
                            <NumberFormat ZeroPattern="n" DecimalDigits="0"></NumberFormat>
                        </telerik:RadNumericTextBox><asp:Label ID="LabelKbps8" runat="server"> Kbps</asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LabelFWDSpeedAboveFUP" runat="server">Forward speed above FUP:</asp:Label></td>
                    <td>
                        <telerik:RadNumericTextBox ID="RadNumericTextBoxForwardAboveFup" runat="server" ShowSpinButtons="True" Culture="nl-BE" Skin="Metro" Value="0" Width="125px">
                            <NumberFormat ZeroPattern="n" DecimalDigits="0"></NumberFormat>
                        </telerik:RadNumericTextBox><asp:Label ID="LabelKbps3" runat="server"> Kbps</asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LabelRTNSpeedAboveFUP" runat="server">Return speed Above FUP:</asp:Label></td>
                    <td>
                        <telerik:RadNumericTextBox ID="RadNumericTextBoxReturnAboveFup" runat="server" ShowSpinButtons="True" Culture="nl-BE" Skin="Metro" Value="0" Width="125px">
                            <NumberFormat ZeroPattern="n" DecimalDigits="0"></NumberFormat>
                        </telerik:RadNumericTextBox><asp:Label ID="LabelKbps4" runat="server"> Kbps</asp:Label>
                    </td>
                </tr>
            </table>
        </asp:WizardStep>
        <asp:WizardStep ID="WizardStep3" Title=" " runat="server" OnActivate="WizardStep3_Activate">
            <h1>Volume</h1>
            <table cellpadding="2" cellspacing="5">
                <tr>
                    <td>
                        <asp:Label ID="LabelForwardFUP" runat="server">Forward FUP:</asp:Label></td>
                    <td>
                        <telerik:RadNumericTextBox ID="RadNumericTextBox21" runat="server" Culture="en-US" ToolTip="Value in GB" ShowSpinButtons="True" Skin="Metro" Value="0" Width="125px">
                            <IncrementSettings Step="0.01" />
                            <NumberFormat ZeroPattern="n"></NumberFormat>
                        </telerik:RadNumericTextBox>
                        <asp:Label ID="LabelGB1" runat="server">GB</asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LabelReturnFUP" runat="server">Return FUP:</asp:Label></td>
                    <td>
                        <telerik:RadNumericTextBox ID="RadNumericTextBox22" Culture="en-US" runat="server" ToolTip="Value in GB" ShowSpinButtons="True" Skin="Metro" Value="0" Width="125px">
                            <IncrementSettings Step="0.01" />
                            <NumberFormat ZeroPattern="n"></NumberFormat>
                        </telerik:RadNumericTextBox>
                        <asp:Label ID="LabelGB2" runat="server">GB</asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LabelVolumeAgg" runat="server">NMS Volume type:</asp:Label></td>
                    <td>
                        <asp:RadioButton runat="server" Checked="false" value="1" onclick="DisplayElement(this)" ID="RadioButtonVolAgg" Text="Aggregated volumes" GroupName="VolGroup" />&nbsp;<asp:RadioButton runat="server" Checked="false" value="2" onclick="DisplayElement(this)" ID="RadioButtonVolSep" Text="Separate volumes" GroupName="VolGroup" />
                        <div id="selectType"><a style="color: red">Please select a type</a></div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div id="sep5" style="display: none">
                            <asp:Label ID="LabelHighVolFWD" runat="server">High Volume watermark Forward:</asp:Label></div>
                    </td>
                    <td>
                        <div id="sep6" style="display: none">
                            <telerik:RadNumericTextBox ID="RadNumericTextBoxHighVolFWD" Culture="en-US" runat="server" ToolTip="Value in GB" ShowSpinButtons="True" Skin="Metro" Value="0" Width="125px">
                                <IncrementSettings Step="0.01" />
                                <NumberFormat ZeroPattern="n"></NumberFormat>
                            </telerik:RadNumericTextBox>
                            <asp:Label ID="LabelGB5" runat="server">GB</asp:Label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div id="sep7" style="display: none">
                            <asp:Label ID="LabelHighVolRTN" runat="server">High Volume watermark Return:</asp:Label></div>
                    </td>
                    <td>
                        <div id="sep8" style="display: none">
                            <telerik:RadNumericTextBox ID="RadNumericTextBoxHighVolRTN" Culture="en-US" runat="server" ToolTip="Value in GB" ShowSpinButtons="True" Skin="Metro" Value="0" Width="125px">
                                <IncrementSettings Step="0.01" />
                                <NumberFormat ZeroPattern="n"></NumberFormat>
                            </telerik:RadNumericTextBox>
                            <asp:Label ID="LabelGB6" runat="server">GB</asp:Label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div id="sep1" style="display: none">
                            <asp:Label ID="LabelLowVolFWD" runat="server">Low Volume watermark Forward:</asp:Label></div>
                    </td>
                    <td>
                        <div id="sep2" style="display: none">
                            <telerik:RadNumericTextBox ID="RadNumericTextBoxLowVolFWD" Culture="en-US" runat="server" ToolTip="Value in GB" ShowSpinButtons="True" Skin="Metro" Value="0" Width="125px">
                                <IncrementSettings Step="0.01" />
                                <NumberFormat ZeroPattern="n"></NumberFormat>
                            </telerik:RadNumericTextBox>
                            <asp:Label ID="LabelGB3" runat="server">GB</asp:Label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div id="sep3" style="display: none">
                            <asp:Label ID="LabelLowVolRTN" runat="server">Low Volume watermark Return:</asp:Label></div>
                    </td>
                    <td>
                        <div id="sep4" style="display: none">
                            <telerik:RadNumericTextBox ID="RadNumericTextBoxLowVolRTN" Culture="en-US" runat="server" ToolTip="Value in GB" ShowSpinButtons="True" Skin="Metro" Value="0" Width="125px">
                                <IncrementSettings Step="0.01" />
                                <NumberFormat ZeroPattern="n"></NumberFormat>
                            </telerik:RadNumericTextBox>
                            <asp:Label ID="LabelGB4" runat="server">GB</asp:Label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div id="agg1" style="display: none">
                            <asp:Label ID="Label1" runat="server">High Volume watermark:</asp:Label></div>
                    </td>
                    <td>
                        <div id="agg2" style="display: none">
                            <telerik:RadNumericTextBox ID="RadNumericTextBoxHighVolSUM" Culture="en-US" runat="server" ToolTip="Value in GB" ShowSpinButtons="True" Skin="Metro" Value="0" Width="125px">
                                <IncrementSettings Step="0.01" />
                                <NumberFormat ZeroPattern="n"></NumberFormat>
                            </telerik:RadNumericTextBox>
                            <asp:Label ID="Label2" runat="server">GB</asp:Label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div id="agg3" style="display: none">
                            <asp:Label ID="Label3" runat="server">Low Volume watermark:</asp:Label></div>
                    </td>
                    <td>
                        <div id="agg4" style="display: none">
                            <telerik:RadNumericTextBox ID="RadNumericTextBoxLowVolSUM" Culture="en-US" runat="server" ToolTip="Value in GB" ShowSpinButtons="True" Skin="Metro" Value="0" Width="125px">
                                <IncrementSettings Step="0.01" />
                                <NumberFormat ZeroPattern="n"></NumberFormat>
                            </telerik:RadNumericTextBox>
                            <asp:Label ID="Label4" runat="server">GB</asp:Label>
                        </div>
                    </td>
                </tr>
            </table>
        </asp:WizardStep>
        <asp:WizardStep ID="WizardStep4" Title=" " runat="server" OnActivate="WizardStep4_Activate">
            <h1>Options</h1>
            <asp:Panel ID="PanelAccounting" runat="server" GroupingText="Monthly Subscription Fee" BackColor="#CCFFCC" BorderStyle="Ridge" Visible="False">
                <table>
                    <tr>
                        <td width="40%">
                            <asp:Label ID="LabelBaseAmount" runat="server">Service Pack Base Amount:</asp:Label></td>
                        <td>$US
                            <telerik:RadNumericTextBox ID="RadNumericTextBoxServicePackAmt" runat="server" Culture="en-US" ShowSpinButtons="True" Skin="Metro" ToolTip="Amount in $US, use dot for decimal point" Width="100px" Value="0">
                                <IncrementSettings Step="0.5" />
                                <NumberFormat ZeroPattern="$n" />
                            </telerik:RadNumericTextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="Please specify base amount" ControlToValidate="RadNumericTextBoxServicePackAmt"></asp:RequiredFieldValidator>
                        </td>
                        <td>EUR
                            <telerik:RadNumericTextBox ID="RadNumericTextBoxServicePackAmtEUR" runat="server" Culture="en-US" ShowSpinButtons="True" Skin="Metro" ToolTip="Amount in EUR, use dot for decimal point" Width="100px" Value="0">
                                <IncrementSettings Step="0.5" />
                                <NumberFormat ZeroPattern="$n" />
                            </telerik:RadNumericTextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Please specify base amount" ControlToValidate="RadNumericTextBoxServicePackAmt"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="LabelFreezone" runat="server">Freezone Option:</asp:Label></td>
                        <td>
                            <asp:Label ID="LabelFreezoneUSD" runat="server">$US</asp:Label>
                            <telerik:RadNumericTextBox ID="RadNumericTextBoxFreeZoneAmt" runat="server" Culture="en-US" ShowSpinButtons="True" Skin="Metro" ToolTip="Amount in $US, use dot for decimal point" Width="100px" Value="0">
                                <IncrementSettings Step="0.5" />
                                <NumberFormat ZeroPattern="$n" />
                            </telerik:RadNumericTextBox>
                        </td>
                        <td>
                            <asp:Label ID="LabelFreezoneEUR" runat="server">EUR</asp:Label>
                            <telerik:RadNumericTextBox ID="RadNumericTextBoxFreeZoneAmtEUR" runat="server" Culture="en-US" ShowSpinButtons="True" Skin="Metro" ToolTip="Amount in EUR, use dot for decimal point" Width="100px" Value="0">
                                <IncrementSettings Step="0.5" />
                                <NumberFormat ZeroPattern="$n" />
                            </telerik:RadNumericTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="LabelVoIP" runat="server">VoIP Option:</asp:Label></td>
                        <td>
                            <asp:Label ID="LabelVoIPUSD" runat="server">$US</asp:Label>
                            <telerik:RadNumericTextBox ID="RadNumericTextBoxVoIPAmt" runat="server" Culture="en-US" ShowSpinButtons="True" Skin="Metro" ToolTip="Amount in $US, use dot for decimal point" Width="100px" Value="0">
                                <IncrementSettings Step="0.5" />
                                <NumberFormat ZeroPattern="$n" />
                            </telerik:RadNumericTextBox>
                        </td>
                        <td>
                            <asp:Label ID="LabelVoIPEUR" runat="server">EUR</asp:Label>
                            <telerik:RadNumericTextBox ID="RadNumericTextBoxVoIPAmtEUR" runat="server" Culture="en-US" ShowSpinButtons="True" Skin="Metro" ToolTip="Amount in EUR, use dot for decimal point" Width="100px" Value="0">
                                <IncrementSettings Step="0.5" />
                                <NumberFormat ZeroPattern="$n" />
                            </telerik:RadNumericTextBox>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <br />
            <table>
                <tr>
                    <td>
                        <asp:Label ID="LabelWeight" runat="server">Weight:</asp:Label></td>
                    <td>
                        <telerik:RadNumericTextBox ID="RadNumericTextBoxWeight" runat="server" MinValue="0" MaxValue="100" Culture="en-US" ShowSpinButtons="True" Skin="Metro" Width="100px" Value="0">
                            <IncrementSettings Step="1" />
                            <NumberFormat AllowRounding="true" KeepTrailingZerosOnFocus="true" DecimalDigits="2" />
                        </telerik:RadNumericTextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Please specify the weight" ControlToValidate="radnumerictextboxweight"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LabelForVNO" runat="server">For VNO:</asp:Label></td>
                    <td>
                        <telerik:RadComboBox ID="RadComboBoxVNOId" runat="server" Width="345px" ExpandDirection="Up" Skin="Metro"></telerik:RadComboBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LabelHide" runat="server">Hide for distributor:</asp:Label></td>
                    <td>
                        <asp:CheckBox ID="CheckBoxHide" runat="server" ToolTip="This flag determines if the ServicePack is visible to the distributor or not" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LabelFreezone2" runat="server">Free Zone:</asp:Label></td>
                    <td>
                        <asp:CheckBox ID="CheckBoxFreeZone" runat="server" ToolTip="Check if this service pack contains the Freezone option" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LabelVoIP2" runat="server">VoIP:</asp:Label></td>
                    <td>
                        <asp:CheckBox ID="CheckBoxVoIP" runat="server" ToolTip="Check if this service pack contains a VoIP option" />
                    </td>
                </tr>
            </table>
            <asp:Label ID="LabelResult" runat="server"></asp:Label>
        </asp:WizardStep>
    </WizardSteps>
</asp:Wizard>
