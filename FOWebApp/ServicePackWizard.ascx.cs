﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOMonitorControlWSRef;
using Telerik.Web.UI;

namespace FOWebApp
{
    public partial class ServicePackWizard : System.Web.UI.UserControl
    {
        public BOAccountingControlWS _boAccountingControlWS;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            //Load the ISP list
            RadComboBoxISPId.Items.Clear();
            _boAccountingControlWS = new BOAccountingControlWS();
            Isp[] ispList = _boAccountingControlWS.GetIsps();
            RadComboBoxISPId.DataValueField = "Id";
            RadComboBoxISPId.DataTextField = "CompanyName";
            RadComboBoxISPId.DataSource = ispList;
            RadComboBoxISPId.DataBind();

            //In case an ISP is selected, we must set the seleted value of this combobox back
            //to the previous selected value.
            if (Session["ISP"] != null)
            {
                RadComboBoxISPId.SelectedValue = Session["ISP"].ToString();
            }
            
            //Load the service lines
            string[] serviceLines = { "", "SoHo Services", "Business Services", "Corporate Services", "Specialized Tailored Services", "Multicast Service", "VNO Service", "Monthly SOHO" };
            int i = 1;
            foreach (string sl in serviceLines)
            {
                RadComboBoxItem myItem = new RadComboBoxItem();
                myItem.Text = sl;
                myItem.Value = i.ToString();
                RadComboBoxServiceLine.Items.Add(myItem);
                i++;
            }

            //Set the ServiceLine value to the selected value if necessary
            if (Session["ServiceLine"] != null)
            {
                RadComboBoxServiceLine.SelectedValue = Session["ServiceLine"].ToString();
            }

            //Load the VNO list
            RadComboBoxVNOId.Items.Clear();
            Distributor[] VNOList = _boAccountingControlWS.GetVNOs();
            RadComboBoxVNOId.DataValueField = "Id";
            RadComboBoxVNOId.DataTextField = "FullName";
            RadComboBoxVNOId.DataSource = VNOList;
            RadComboBoxVNOId.DataBind();
            RadComboBoxVNOId.Items.Insert(0, new RadComboBoxItem(""));

            //Set the VNO to the previous selected value
            if (Session["VNO"] != null)
            {
                RadComboBoxVNOId.SelectedValue = Session["VNO"].ToString();
            }
        }

        protected void CustomValidatorId_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (Request.Params["id"] == null && _boAccountingControlWS.GetServicePack(int.Parse(args.Value)) != null)
            {
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }
        }

        protected void Wizard_FinishButtonClick(object sender, WizardNavigationEventArgs e)
        {
            //set session labels from last page of wizard
            Session["BaseAmount"] = RadNumericTextBoxServicePackAmt.Value * 100;
            Session["FreeZoneAmount"] = RadNumericTextBoxFreeZoneAmt.Value * 100;
            Session["VoIPAmount"] = RadNumericTextBoxVoIPAmt.Value * 100;
            Session["BaseAmountEUR"] = RadNumericTextBoxServicePackAmtEUR.Value * 100;
            Session["FreeZoneAmountEUR"] = RadNumericTextBoxFreeZoneAmtEUR.Value * 100;
            Session["VoIPAmountEUR"] = RadNumericTextBoxVoIPAmtEUR.Value * 100;
            Session["Weight"] = RadNumericTextBoxWeight.Value;

            // automatically set VNO value if user is VNO
            if (HttpContext.Current.User.IsInRole("VNO"))
            {
                MembershipUser myObject = Membership.GetUser();
                string userId = myObject.ProviderUserKey.ToString();
                Session["VNO"] = (_boAccountingControlWS.GetDistributorForUser(userId).Id).ToString();
            }
            else
            {
                Session["VNO"] = RadComboBoxVNOId.SelectedValue.ToString();
            }

            if (CheckBoxFreeZone.Checked)
            {
                Session["FreeZoneCheckBox"] = "true";
            }
            else
            {
                Session["FreeZoneCheckBox"] = "false";
            }

            if (CheckBoxHide.Checked)
            {
                Session["Hide"] = "true";
            }
            else
            {
                Session["Hide"] = "false";
            }

            if (CheckBoxVoIP.Checked)
            {
                Session["VoIPCheckBox"] = "true";
            }
            else
            {
                Session["VoIPCheckBox"] = "false";
            }
            
            //Create a new Service pack
            ServiceLevel serviceLevel = new ServiceLevel();
            serviceLevel.SlaId = Int32.Parse(Session["ServicePackID"].ToString());
            serviceLevel.SlaName = Session["ServicePackName"].ToString();
            serviceLevel.FupThreshold = Decimal.Parse(Session["ForwardFUP"].ToString());
            serviceLevel.RTNFUPThreshold = Decimal.Parse(Session["ReturnFUP"].ToString());
            serviceLevel.DRFWD = Int32.Parse(Session["ForwardSpeed"].ToString());
            serviceLevel.DRRTN = Int32.Parse(Session["ReturnSpeed"].ToString());
            serviceLevel.DrAboveFup = Session["SpeedAboveFUPFWD"].ToString() + "/" + Session["SpeedAboveFUPRTN"].ToString();
            serviceLevel.CIRFWD = Int32.Parse(Session["CIRFWD"].ToString());
            serviceLevel.CIRRTN = Int32.Parse(Session["CIRRTN"].ToString());
            serviceLevel.SLAWeight = (double)Convert.ToDouble(Session["Weight"].ToString());
            if (Session["ServicePackCommonName"].ToString() != null)
            {
                serviceLevel.SlaCommonName = Session["ServicePackCommonName"].ToString();
            }
            serviceLevel.IspId = Int32.Parse(Session["ISP"].ToString());
            serviceLevel.ServiceClass = Int32.Parse(Session["ServiceLine"].ToString());
            if (serviceLevel.ServiceClass == 1)
            {
                serviceLevel.Voucher = true;
                serviceLevel.Business = false;
            }
            else if (serviceLevel.ServiceClass == 2)
            {
                serviceLevel.Business = true;
                serviceLevel.Voucher = false;
            }
            else if (serviceLevel.ServiceClass == 3)
            {
                serviceLevel.Business = false;
                serviceLevel.Voucher = false;
                if ((serviceLevel.DRFWD != 0) && (serviceLevel.CIRFWD != 0))
                {
                    int contention = (int)serviceLevel.DRFWD / serviceLevel.CIRFWD;
                    switch (contention)
                    {
                        case 1:
                            serviceLevel.SLAWeight = 100;
                            break;
                        case 2:
                            serviceLevel.SLAWeight = 53.23;
                            break;
                        case 4:
                            serviceLevel.SLAWeight = 28.32;
                            break;
                        case 8:
                            serviceLevel.SLAWeight = 16.93;
                            break;
                        case 10:
                            serviceLevel.SLAWeight = 15;
                            break;
                        case 15:
                            serviceLevel.SLAWeight = 13.04;
                            break;
                        case 20:
                            serviceLevel.SLAWeight = 12.69;
                            break;
                        case 25:
                            serviceLevel.SLAWeight = 12.49;
                            break;
                        default:
                            serviceLevel.SLAWeight = 12.49;
                            break;
                    }
                }
            }

            if (Session["VNO"].ToString() != "0" && Session["VNO"].ToString() != "")
            {
                serviceLevel.VNOId = Int32.Parse(Session["VNO"].ToString());
            }
            if (Session["FreeZoneCheckBox"] != null)
            {
                if (Session["FreeZoneCheckBox"].ToString() == "true")
                {
                    serviceLevel.FreeZone = true;
                }
                else
                {
                    serviceLevel.FreeZone = false;
                }
            }
            else
            {
                serviceLevel.FreeZone = false;
            }
            
            if (Session["Hide"] != null)
            {
                if (Session["Hide"].ToString() == "true")
                {
                    serviceLevel.Hide = true;
                }
                else
                {
                    serviceLevel.Hide = false;
                }
            }
            else
            {
                serviceLevel.Hide = false;
            }
            
            if (Session["VoIPCheckBox"] != null)
            {
                if (Session["VoIPCheckBox"].ToString() == "true")
                {
                    serviceLevel.VoIPFlag = true;
                }
                else
                {
                    serviceLevel.VoIPFlag = false;
                }
            }
            else
            {
                serviceLevel.VoIPFlag = false;
            }

            serviceLevel.ServicePackAmt = (float)(Convert.ToInt32(Session["BaseAmount"].ToString())) / 100;
            serviceLevel.FreeZoneAmt = (float)(Convert.ToInt32(Session["FreeZoneAmount"].ToString())) / 100;
            serviceLevel.VoIPAmt = (float)(Convert.ToInt32(Session["VoIPAmount"].ToString())) / 100;
            serviceLevel.ServicePackAmtEUR = (float)(Convert.ToInt32(Session["BaseAmountEUR"].ToString())) / 100;
            serviceLevel.FreeZoneAmtEUR = (float)(Convert.ToInt32(Session["FreeZoneAmountEUR"].ToString())) / 100;
            serviceLevel.VoIPAmtEUR = (float)(Convert.ToInt32(Session["VoIPAmountEUR"].ToString())) / 100;
           // serviceLevel.SLAWeight = (double)Convert.ToDouble(Session["Weight"].ToString());

            serviceLevel.MinBWFWD = int.Parse(Session["MinFWDBW"].ToString());
            serviceLevel.MinBWRTN = int.Parse(Session["MinRTNBW"].ToString());
            serviceLevel.LowVolumeFWD = (decimal)(Convert.ToInt32(Session["LowVolFWD"].ToString())) / 100;
            serviceLevel.LowVolumeRTN = (decimal)(Convert.ToInt32(Session["LowVolRTN"].ToString())) / 100;
            serviceLevel.HighVolumeFWD = (decimal)(Convert.ToInt32(Session["HighVolFWD"].ToString())) / 100;
            serviceLevel.HighVolumeRTN = (decimal)(Convert.ToInt32(Session["HighVolRTN"].ToString())) / 100;
            serviceLevel.HighVolumeSUM = (decimal)(Convert.ToInt32(Session["HighVolSUM"].ToString())) / 100;
            serviceLevel.LowVolumeSUM = (decimal)(Convert.ToInt32(Session["LowVolSUM"].ToString())) / 100;
            if (Session["RadioButtonVol"] != null)
            {
                if (Session["RadioButtonVol"].ToString() == "1")
                {
                    serviceLevel.AggregateFlag = true;
                }
                else
                {
                    serviceLevel.AggregateFlag = false;
                }
            }

            if (Session["EdgeISP"] != null)
            {
                serviceLevel.EdgeISP = (int)Int32.Parse(Session["EdgeISP"].ToString());
            }

            //if (!string.IsNullOrWhiteSpace(RadTextBoxEdgeISP.Text))
            //{
            //    serviceLevel.EdgeISP = Int32.Parse(Session["EdgeISP"].ToString());
            //}

            if (Session["EdgeSLA"] != null)
            {
                serviceLevel.EdgeSLA = (int)Int32.Parse(Session["EdgeSLA"].ToString());
            }

            //if (!string.IsNullOrWhiteSpace(RadTextBoxEdgeSLA.Text))
            //{
            //    serviceLevel.EdgeSLA = Int32.Parse(Session["EdgeSLA"].ToString());
            //}

            //The SerivcePack class is used for the NMS
            ServicePack servicePack = this.serviceLevelToServicePack(serviceLevel);
            BOMonitorControlWS boMonitorControlWS = new BOMonitorControlWS();

            if (boMonitorControlWS.NMSUpdateServicePack(servicePack)) //will return true for all non-NMS SLA's as well
            {
                if (_boAccountingControlWS.UpdateServicePack(serviceLevel)) //writes the SLA to the CMT database
                {
                    LabelResult.ForeColor = Color.DarkGreen;
                    LabelResult.Text = "Servicepack update succeeded";
                }
                else
                {
                    LabelResult.ForeColor = Color.Red;
                    LabelResult.Text = "Servicepack update failed";
                }
            }
            else
            {
                LabelResult.ForeColor = Color.Red;
                LabelResult.Text = "Servicepack update failed";
            }
        }

        protected void Wizard_NextButtonClick(object sender, WizardNavigationEventArgs e)
        {
            if (Wizard.ActiveStepIndex == 0)
            {
                if (Page.IsValid)
                {
                    Session["ISP"] = RadComboBoxISPId.SelectedValue;
                    Session["ServiceLine"] = RadComboBoxServiceLine.SelectedValue;
                    Session["ServicePackID"] = TextBoxId.Text;
                    Session["ServicePackName"] = TextBoxName.Text;
                    Session["ServicePackCommonName"] = TextBoxSlaCommonName.Text;
                }
                Session["EdgeISP"] = RadNumericTextBoxEdgeISP.Value;
                Session["EdgeSLA"] = RadNumericTextBoxEdgeSLA.Value;
            }
            else if (Wizard.ActiveStepIndex == 1)
            {
                Session["CIRFWD"] = RadNumericTextBoxCIRFWD.Text;
                Session["CIRRTN"] = RadNumericTextBoxCIRRTN.Text;
                Session["ForwardSpeed"] = RadNumericTextBoxForwardDefault.Text;
                Session["ReturnSpeed"] = RadNumericTextBoxReturnDefault.Text;
                Session["MinFWDBW"] = RadNumericTextBoxMinBWFWD.Text;
                Session["MinRTNBW"] = RadNumericTextBoxMinBWRTN.Text;
                Session["SpeedAboveFUPFWD"] = RadNumericTextBoxForwardAboveFup.Text;
                Session["SpeedAboveFUPRTN"] = RadNumericTextBoxReturnAboveFup.Text;

                if (Session["ServiceLine"].ToString() == "1" || Session["ServiceLine"].ToString() == "3")
                {
                    Wizard.ActiveStepIndex = 3;
                }
                else if (Session["ServiceLine"].ToString() == "2" || Session["ServiceLine"].ToString() == "4")
                {
                    Wizard.ActiveStepIndex = 2;
                }

                //also set the variables from page 3, in case it is skipped by the wizard
                Session["ForwardFUP"] = "0";
                Session["ReturnFUP"] = "0";
                Session["LowVolFWD"] = "0";
                Session["LowVolRTN"] = "0";
                Session["LowVolSUM"] = "0";
                Session["HighVolSUM"] = "0";
                Session["HighVolFWD"] = "0";
                Session["HighVolRTN"] = "0";
            }
            else if (Wizard.ActiveStepIndex == 2)
            {
                Session["ForwardFUP"] = RadNumericTextBox21.Value * 100;
                Session["ReturnFUP"] = RadNumericTextBox22.Value * 100;

                if (RadioButtonVolAgg.Checked)
                {
                    Session["HighVolSUM"] = RadNumericTextBoxHighVolSUM.Value * 100;
                    Session["HighVolFWD"] = RadNumericTextBoxHighVolSUM.Value * 100;
                    Session["HighVolRTN"] = RadNumericTextBoxHighVolSUM.Value * 100;
                    Session["LowVolFWD"] = RadNumericTextBoxLowVolSUM.Value * 100;
                    Session["LowVolRTN"] = RadNumericTextBoxLowVolSUM.Value * 100;
                    Session["LowVolSUM"] = RadNumericTextBoxLowVolSUM.Value * 100;
                    Session["RadioButtonVol"] = 1;
                }
                else if (RadioButtonVolSep.Checked)
                {
                    Session["LowVolFWD"] = RadNumericTextBoxLowVolFWD.Value * 100;
                    Session["LowVolRTN"] = RadNumericTextBoxLowVolRTN.Value * 100;
                    Session["LowVolSUM"] = (RadNumericTextBoxLowVolFWD.Value + RadNumericTextBoxLowVolRTN.Value) * 100;
                    Session["HighVolFWD"] = RadNumericTextBoxHighVolFWD.Value * 100;
                    Session["HighVolRTN"] = RadNumericTextBoxHighVolRTN.Value * 100;
                    Session["HighVolSUM"] = (RadNumericTextBoxHighVolFWD.Value + RadNumericTextBoxHighVolRTN.Value) * 100;
                    Session["RadioButtonVol"] = 2;
                }
            }
            else if (Wizard.ActiveStepIndex == 3)
            {
                Session["BaseAmount"] = RadNumericTextBoxServicePackAmt.Value * 100;
                Session["FreeZoneAmount"] = RadNumericTextBoxFreeZoneAmt.Value * 100;
                Session["VoIPAmount"] = RadNumericTextBoxVoIPAmt.Value * 100;
                Session["VNO"] = RadComboBoxVNOId.SelectedValue.ToString();
                Session["BaseAmountEUR"] = RadNumericTextBoxServicePackAmtEUR.Value * 100;
                Session["FreeZoneAmountEUR"] = RadNumericTextBoxFreeZoneAmtEUR.Value * 100;
                Session["VoIPAmountEUR"] = RadNumericTextBoxVoIPAmtEUR.Value * 100;
                Session["Weight"] = RadNumericTextBoxWeight.Value;
                if (CheckBoxFreeZone.Checked)
                {
                    Session["FreeZoneCheckBox"] = "true";
                }
                else
                {
                    Session["FreeZoneCheckBox"] = "false";
                }

                if (CheckBoxHide.Checked)
                {
                    Session["Hide"] = "true";
                }
                else
                {
                    Session["Hide"] = "false";
                }

                if (CheckBoxVoIP.Checked)
                {
                    Session["VoIPCheckBox"] = "true";
                }
                else
                {
                    Session["VoIPCheckBox"] = "false";
                }
            }
        }

        protected void WizardStep1_Activate(object sender, EventArgs e)
        {
            //Load in values
            if (Session["ISP"] != null)
            {
                RadComboBoxISPId.SelectedValue = Session["ISP"].ToString();
            }
            if (Session["ServiceLine"] != null)
            {
                RadComboBoxServiceLine.SelectedValue = Session["ServiceLine"].ToString();
            }
            if (Session["ServicePackID"] != null)
            {
                TextBoxId.Text = Session["ServicePackID"].ToString();
            }
            if (Session["ServicePackName"] != null)
            {
                TextBoxName.Text = Session["ServicePackName"].ToString();
            }
            if (Session["ServicePackCommonName"] != null)
            {
                TextBoxSlaCommonName.Text = Session["ServicePackCommonName"].ToString();
            }
            if (Session["EdgeISP"] != null)
            {
                RadNumericTextBoxEdgeISP.Enabled = true;
                RadNumericTextBoxEdgeISP.Value = (int)Convert.ToInt32(Session["EdgeISP"].ToString());
            }
            else
            {
                RadNumericTextBoxEdgeISP.Enabled = false;
                RadNumericTextBoxEdgeISP.Text = "";
            }

            if (Session["EdgeSLA"] != null)
            {
                RadNumericTextBoxEdgeSLA.Enabled = true;
                RadNumericTextBoxEdgeSLA.Value = (int)Convert.ToInt32(Session["EdgeSLA"].ToString());
            }
            else
            {
                RadNumericTextBoxEdgeSLA.Enabled = false;
                RadNumericTextBoxEdgeSLA.Text = "";
            }
        }

        protected void WizardStep2_Activate(object sender, EventArgs e)
        {
            //Load in values
            if (Session["CIRFWD"] != null)
            {
                RadNumericTextBoxCIRFWD.Text = Session["CIRFWD"].ToString();
            }
            if (Session["CIRRTN"] != null)
            {
                RadNumericTextBoxCIRRTN.Text = Session["CIRRTN"].ToString();
            }
            if (Session["ForwardSpeed"] != null)
            {
                RadNumericTextBoxForwardDefault.Text = Session["ForwardSpeed"].ToString();
            }
            if (Session["ReturnSpeed"] != null)
            {
                RadNumericTextBoxReturnDefault.Text = Session["ReturnSpeed"].ToString();
            }
            if (Session["MinFWDBW"] != null)
            {
                RadNumericTextBoxMinBWFWD.Text = Session["MinFWDBW"].ToString();
            }
            if (Session["MinRTNBW"] != null)
            {
                RadNumericTextBoxMinBWRTN.Text = Session["MinRTNBW"].ToString();
            }
            if (Session["SpeedAboveFUPFWD"] != null)
            {
                RadNumericTextBoxForwardAboveFup.Text = Session["SpeedAboveFUPFWD"].ToString();
            }
            if (Session["SpeedAboveFUPRTN"] != null)
            {
                RadNumericTextBoxReturnAboveFup.Text = Session["SpeedAboveFUPRTN"].ToString();
            }
            
            //show options depending on service line
            if (Session["ServiceLine"].ToString() == "1")
            {
                LabelForwardSpeed.Visible = true;
                RadNumericTextBoxReturnDefault.Visible = true;
                LabelKbps1.Visible = true;
                LabelReturnSpeed.Visible = true;
                RadNumericTextBoxReturnDefault.Visible = true;
                LabelKbps2.Visible = true;
                LabelFWDSpeedAboveFUP.Visible = false;
                RadNumericTextBoxForwardAboveFup.Visible = false;
                LabelKbps3.Visible = false;
                LabelRTNSpeedAboveFUP.Visible = false;
                RadNumericTextBoxReturnAboveFup.Visible = false;
                LabelKbps4.Visible = false;
                LabelCIRFWD.Visible = false;
                RadNumericTextBoxCIRFWD.Visible = false;
                LabelKbps5.Visible = false;
                LabelCIRRTN.Visible = false;
                RadNumericTextBoxCIRRTN.Visible = false;
                LabelKbps6.Visible = false;
                LabelMinBWFWD.Visible = false;
                RadNumericTextBoxMinBWFWD.Visible = false;
                LabelKbps7.Visible = false;
                LabelMinBWRTN.Visible = false;
                RadNumericTextBoxMinBWRTN.Visible = false;
                LabelKbps8.Visible = false;
            
            }
            else if (Session["ServiceLine"].ToString() == "2")
            {
                LabelForwardSpeed.Visible = true;
                RadNumericTextBoxReturnDefault.Visible = true;
                LabelKbps1.Visible = true;
                LabelReturnSpeed.Visible = true;
                RadNumericTextBoxReturnDefault.Visible = true;
                LabelKbps2.Visible = true;
                LabelFWDSpeedAboveFUP.Visible = false;
                RadNumericTextBoxForwardAboveFup.Visible = false;
                LabelKbps3.Visible = false;
                LabelRTNSpeedAboveFUP.Visible = false;
                RadNumericTextBoxReturnAboveFup.Visible = false;
                LabelKbps4.Visible = false;
                LabelCIRFWD.Visible = false;
                RadNumericTextBoxCIRFWD.Visible = false;
                LabelKbps5.Visible = false;
                LabelCIRRTN.Visible = false;
                RadNumericTextBoxCIRRTN.Visible = false;
                LabelKbps6.Visible = false;
                LabelMinBWFWD.Visible = true;
                RadNumericTextBoxMinBWFWD.Visible = true;
                LabelKbps7.Visible = true;
                LabelMinBWRTN.Visible = true;
                RadNumericTextBoxMinBWRTN.Visible = true;
                LabelKbps8.Visible = true;
            
            }
            else if (Session["ServiceLine"].ToString() == "3")
            {
                LabelForwardSpeed.Visible = true;
                RadNumericTextBoxReturnDefault.Visible = true;
                LabelKbps1.Visible = true;
                LabelReturnSpeed.Visible = true;
                RadNumericTextBoxReturnDefault.Visible = true;
                LabelKbps2.Visible = true;
                LabelFWDSpeedAboveFUP.Visible = false;
                RadNumericTextBoxForwardAboveFup.Visible = false;
                LabelKbps3.Visible = false;
                LabelRTNSpeedAboveFUP.Visible = false;
                RadNumericTextBoxReturnAboveFup.Visible = false;
                LabelKbps4.Visible = false;
                LabelCIRFWD.Visible = true;
                RadNumericTextBoxCIRFWD.Visible = true;
                LabelKbps5.Visible = true;
                LabelCIRRTN.Visible = true;
                RadNumericTextBoxCIRRTN.Visible = true;
                LabelKbps6.Visible = true;
                LabelMinBWFWD.Visible = false;
                RadNumericTextBoxMinBWFWD.Visible = false;
                LabelKbps7.Visible = false;
                LabelMinBWRTN.Visible = false;
                RadNumericTextBoxMinBWRTN.Visible = false;
                LabelKbps8.Visible = false;
            }
            else if (Session["ServiceLine"].ToString() == "4")
            {
                LabelForwardSpeed.Visible = true;
                RadNumericTextBoxReturnDefault.Visible = true;
                LabelKbps1.Visible = true;
                LabelReturnSpeed.Visible = true;
                RadNumericTextBoxReturnDefault.Visible = true;
                LabelKbps2.Visible = true;
                LabelFWDSpeedAboveFUP.Visible = true;
                RadNumericTextBoxForwardAboveFup.Visible = true;
                LabelKbps3.Visible = true;
                LabelRTNSpeedAboveFUP.Visible = true;
                RadNumericTextBoxReturnAboveFup.Visible = true;
                LabelKbps4.Visible = true;
                LabelCIRFWD.Visible = true;
                RadNumericTextBoxCIRFWD.Visible = true;
                LabelKbps5.Visible = true;
                LabelCIRRTN.Visible = true;
                RadNumericTextBoxCIRRTN.Visible = true;
                LabelKbps6.Visible = true;
                LabelMinBWFWD.Visible = true;
                RadNumericTextBoxMinBWFWD.Visible = true;
                LabelKbps7.Visible = true;
                LabelMinBWRTN.Visible = true;
                RadNumericTextBoxMinBWRTN.Visible = true;
                LabelKbps8.Visible = true;
            }
        }

        protected void WizardStep3_Activate(object sender, EventArgs e)
        {
            //load in values
            if (Session["ForwardFUP"] != null)
            {
                RadNumericTextBox21.Value = (double)(Convert.ToInt32(Session["ForwardFUP"].ToString())) / 100;
            }
            if (Session["ReturnFUP"] != null)
            {
                RadNumericTextBox22.Value = (double)(Convert.ToInt32(Session["ReturnFUP"].ToString())) / 100; 
            }
            if (Session["LowVolFWD"] != null)
            {
                RadNumericTextBoxLowVolFWD.Value = (double)(Convert.ToInt32(Session["LowVolFWD"].ToString())) / 100;
            }
            if (Session["LowVolRTN"] != null)
            {
                RadNumericTextBoxLowVolRTN.Value = (double)(Convert.ToInt32(Session["LowVolRTN"].ToString())) / 100;
            }
            if (Session["HighVolFWD"] != null)
            {
                RadNumericTextBoxHighVolFWD.Value = (double)(Convert.ToInt32(Session["HighVolFWD"].ToString())) / 100;
            }
            if (Session["HighVolRTN"] != null)
            {
                RadNumericTextBoxHighVolRTN.Value = (double)(Convert.ToInt32(Session["HighVolRTN"].ToString())) / 100;
            }
            if (Session["HighVolSUM"] != null)
            {
                RadNumericTextBoxHighVolSUM.Value = (double)(Convert.ToInt32(Session["HighVolSUM"].ToString())) / 100;
            }
            if (Session["LowVolSUM"] != null)
            {
                RadNumericTextBoxLowVolSUM.Value = (double)(Convert.ToInt32(Session["LowVolSUM"].ToString())) / 100;
            }

            if (Session["RadioButtonVol"] != null)
            {
                if (Session["RadioButtonVol"].ToString() == "1")
                {
                    RadioButtonVolAgg.Checked = true;
                }
                else if (Session["RadioButtonVol"].ToString() == "2")
                {
                    RadioButtonVolSep.Checked = true;
                }
            }

            if (Session["ServiceLine"].ToString() == "4")
            {
                LabelForwardFUP.Visible = true;
                RadNumericTextBox21.Visible = true;
                LabelGB1.Visible = true;
                LabelReturnFUP.Visible = true;
                RadNumericTextBox22.Visible = true;
                LabelGB2.Visible = true;
                RadNumericTextBoxLowVolFWD.Visible = true;
                LabelGB3.Visible = true;
                LabelLowVolRTN.Visible = true;
                RadNumericTextBoxLowVolRTN.Visible = true;
                LabelGB4.Visible = true;
                LabelHighVolFWD.Visible = true;
                RadNumericTextBoxHighVolFWD.Visible = true;
                LabelGB5.Visible = true;
                LabelHighVolRTN.Visible = true;
                RadNumericTextBoxHighVolRTN.Visible = true;
                LabelGB6.Visible = true;
            }
            else if (Session["ServiceLine"].ToString() == "2")
            {
                LabelForwardFUP.Visible = false;
                RadNumericTextBox21.Visible = false;
                LabelGB1.Visible = false;
                LabelReturnFUP.Visible = false;
                RadNumericTextBox22.Visible = false;
                LabelGB2.Visible = false;
                LabelLowVolFWD.Visible = true;
                RadNumericTextBoxLowVolFWD.Visible = true;
                LabelGB3.Visible = true;
                LabelLowVolRTN.Visible = true;
                RadNumericTextBoxLowVolRTN.Visible = true;
                LabelGB4.Visible = true;
                LabelHighVolFWD.Visible = true;
                RadNumericTextBoxHighVolFWD.Visible = true;
                LabelGB5.Visible = true;
                LabelHighVolRTN.Visible = true;
                RadNumericTextBoxHighVolRTN.Visible = true;
                LabelGB6.Visible = true;
            }
            else
            {
                LabelForwardFUP.Visible = false;
                RadNumericTextBox21.Visible = false;
                LabelGB1.Visible = false;
                LabelReturnFUP.Visible = false;
                RadNumericTextBox22.Visible = false;
                LabelGB2.Visible = false;
                LabelLowVolFWD.Visible = false;
                RadNumericTextBoxLowVolFWD.Visible = false;
                LabelGB3.Visible = false;
                LabelLowVolRTN.Visible = false;
                RadNumericTextBoxLowVolRTN.Visible = false;
                LabelGB4.Visible = false;
                LabelHighVolFWD.Visible = false;
                RadNumericTextBoxHighVolFWD.Visible = false;
                LabelGB5.Visible = false;
                LabelHighVolRTN.Visible = false;
                RadNumericTextBoxHighVolRTN.Visible = false;
                LabelGB6.Visible = false;
            }
        }

        protected void WizardStep4_Activate(object sender, EventArgs e)
        {
            //Load in values
            if (Session["BaseAmount"] != null)
            {
                RadNumericTextBoxServicePackAmt.Value = (double)Convert.ToInt32(Session["BaseAmount"].ToString()) / 100;
            }
            if (Session["FreeZoneAmount"] != null)
            {
                RadNumericTextBoxFreeZoneAmt.Value = (double)Convert.ToInt32(Session["FreeZoneAmount"].ToString()) / 100;
            }
            if (Session["VoIPAmount"] != null)
            {
                RadNumericTextBoxVoIPAmt.Value = (double)Convert.ToInt32(Session["VoIPAmount"].ToString()) / 100;
            }
            if (Session["BaseAmountEUR"] != null)
            {
                RadNumericTextBoxServicePackAmtEUR.Value = (double)Convert.ToInt32(Session["BaseAmountEUR"].ToString()) / 100;
            }
            if (Session["FreeZoneAmountEUR"] != null)
            {
                RadNumericTextBoxFreeZoneAmtEUR.Value = (double)Convert.ToInt32(Session["FreeZoneAmountEUR"].ToString()) / 100;
            }
            if (Session["VoIPAmountEUR"] != null)
            {
                RadNumericTextBoxVoIPAmtEUR.Value = (double)Convert.ToInt32(Session["VoIPAmountEUR"].ToString()) / 100;
            }
            if (Session["Weight"] != null)
            {
                RadNumericTextBoxWeight.Value = (double)Convert.ToDouble(Session["Weight"].ToString());
            }

            if (Session["VNO"] != null)
            {
                RadComboBoxVNOId.SelectedValue = Session["VNO"].ToString();
            }

            if (Session["FreeZoneCheckBox"] != null)
            {
                if (Session["FreeZoneCheckBox"].ToString() == "true")
                {
                    CheckBoxFreeZone.Checked = true;
                }
            }
            else
            {
                CheckBoxFreeZone.Checked = false;
            }

            if (Session["Hide"] != null)
            {
                if (Session["Hide"].ToString() == "true")
                {
                    CheckBoxHide.Checked = true;
                }
            }
            else
            {
                CheckBoxHide.Checked = false;
            }

            if (Session["VoIPCheckBox"] != null)
            {
                if (Session["VoIPCheckBox"].ToString() == "true")
                {
                    CheckBoxVoIP.Checked = true;
                }
            }
            else
            {
                CheckBoxVoIP.Checked = false;
            }
            
            LabelBaseAmount.Visible = true;
            RadNumericTextBoxServicePackAmt.Visible = true;

            // hide VNO option and Hide-box for VNOs
            if (HttpContext.Current.User.IsInRole("VNO"))
            {
                RadComboBoxVNOId.Visible = false;
                LabelForVNO.Visible = false;
                LabelHide.Visible = false;
                CheckBoxHide.Visible = false;
            }

            if (Session["ServiceLine"].ToString() == "4" || Session["ServiceLine"].ToString() == "2")
            {
                LabelFreezone.Visible = true;
                RadNumericTextBoxFreeZoneAmt.Visible = true;
                LabelVoIP.Visible = true;
                RadNumericTextBoxVoIPAmt.Visible = true;
                LabelFreezone2.Visible = true;
                CheckBoxFreeZone.Visible = true;
                LabelVoIP2.Visible = true;
                CheckBoxVoIP.Visible = true;
                LabelFreezoneUSD.Visible = true;
                LabelFreezoneEUR.Visible = true;
                RadNumericTextBoxFreeZoneAmtEUR.Visible = true;
                LabelVoIPUSD.Visible = true;
                LabelVoIPEUR.Visible = true;
                RadNumericTextBoxVoIPAmtEUR.Visible = true;
            }
            else
            {
                LabelFreezone.Visible = false;
                RadNumericTextBoxFreeZoneAmt.Visible = false;
                LabelVoIP.Visible = false;
                RadNumericTextBoxVoIPAmt.Visible = false;
                LabelFreezone2.Visible = false;
                CheckBoxFreeZone.Visible = false;
                LabelVoIP2.Visible = false;
                CheckBoxVoIP.Visible = false;
                LabelFreezoneUSD.Visible = false;
                LabelFreezoneEUR.Visible = false;
                RadNumericTextBoxFreeZoneAmtEUR.Visible = false;
                LabelVoIPUSD.Visible = false;
                LabelVoIPEUR.Visible = false;
                RadNumericTextBoxVoIPAmtEUR.Visible = false;
            }
        }

        protected void Wizard_PreviousButtonClick(object sender, WizardNavigationEventArgs e)
        {
            if (Wizard.ActiveStepIndex == 0)
            {
                if (Page.IsValid)
                {
                    Session["ISP"] = RadComboBoxISPId.SelectedValue;
                    Session["ServiceLine"] = RadComboBoxServiceLine.SelectedValue;
                    Session["ServicePackID"] = TextBoxId.Text;
                    Session["ServicePackName"] = TextBoxName.Text;
                    Session["ServicePackCommonName"] = TextBoxSlaCommonName.Text;
                }
                Session["EdgeISP"] = RadNumericTextBoxEdgeISP.Value;
                Session["EdgeSLA"] = RadNumericTextBoxEdgeSLA.Value;
            }
            else if (Wizard.ActiveStepIndex == 1)
            {
                Session["CIRFWD"] = RadNumericTextBoxCIRFWD.Text;
                Session["CIRRTN"] = RadNumericTextBoxCIRRTN.Text;
                Session["ForwardSpeed"] = RadNumericTextBoxForwardDefault.Text;
                Session["ReturnSpeed"] = RadNumericTextBoxReturnDefault.Text;
                Session["MinFWDBW"] = RadNumericTextBoxMinBWFWD.Text;
                Session["MinRTNBW"] = RadNumericTextBoxMinBWRTN.Text;
                Session["SpeedAboveFUPFWD"] = RadNumericTextBoxForwardAboveFup.Text;
                Session["SpeedAboveFUPRTN"] = RadNumericTextBoxReturnAboveFup.Text;
            }
            else if (Wizard.ActiveStepIndex == 2)
            {
                Session["ForwardFUP"] = RadNumericTextBox21.Value * 100;
                Session["ReturnFUP"] = RadNumericTextBox22.Value * 100;
                
                if (RadioButtonVolAgg.Checked)
                {
                    Session["HighVolSUM"] = RadNumericTextBoxHighVolSUM.Value * 100;
                    Session["HighVolFWD"] = RadNumericTextBoxHighVolSUM.Value * 100;
                    Session["HighVolRTN"] = RadNumericTextBoxHighVolSUM.Value * 100;
                    Session["LowVolFWD"] = RadNumericTextBoxLowVolSUM.Value * 100;
                    Session["LowVolRTN"] = RadNumericTextBoxLowVolSUM.Value * 100;
                    Session["LowVolSUM"] = RadNumericTextBoxLowVolSUM.Value * 100;
                    Session["RadioButtonVol"] = 1;
                }
                else if (RadioButtonVolSep.Checked)
                {
                    Session["LowVolFWD"] = RadNumericTextBoxLowVolFWD.Value * 100;
                    Session["LowVolRTN"] = RadNumericTextBoxLowVolRTN.Value * 100;
                    Session["LowVolSUM"] = (RadNumericTextBoxLowVolFWD.Value + RadNumericTextBoxLowVolRTN.Value) * 100;
                    Session["HighVolFWD"] = RadNumericTextBoxHighVolFWD.Value * 100;
                    Session["HighVolRTN"] = RadNumericTextBoxHighVolRTN.Value * 100;
                    Session["HighVolSUM"] = (RadNumericTextBoxHighVolFWD.Value + RadNumericTextBoxHighVolRTN.Value) * 100;
                    Session["RadioButtonVol"] = 2;
                }
            }
            else if (Wizard.ActiveStepIndex == 3)
            {
                Session["BaseAmount"] = RadNumericTextBoxServicePackAmt.Value * 100;
                Session["FreeZoneAmount"] = RadNumericTextBoxFreeZoneAmt.Value * 100;
                Session["VoIPAmount"] = RadNumericTextBoxVoIPAmt.Value * 100;
                Session["VNO"] = RadComboBoxVNOId.SelectedValue.ToString();
                Session["BaseAmountEUR"] = RadNumericTextBoxServicePackAmtEUR.Value * 100;
                Session["FreeZoneAmountEUR"] = RadNumericTextBoxFreeZoneAmtEUR.Value * 100;
                Session["VoIPAmountEUR"] = RadNumericTextBoxVoIPAmtEUR.Value * 100;
                Session["Weight"] = RadNumericTextBoxWeight.Value;

                if (CheckBoxFreeZone.Checked)
                {
                    Session["FreeZoneCheckBox"] = "true";
                }
                else
                {
                    Session["FreeZoneCheckBox"] = "false";
                }

                if (CheckBoxHide.Checked)
                {
                    Session["Hide"] = "true";
                }
                else
                {
                    Session["Hide"] = "false";
                }

                if (CheckBoxVoIP.Checked)
                {
                    Session["VoIPCheckBox"] = "true";
                }
                else
                {
                    Session["VoIPCheckBox"] = "false";
                }
            }
        }

        private ServicePack serviceLevelToServicePack(ServiceLevel serviceLevel)
        {
            ServicePack servicePack = new ServicePack();

            servicePack.SlaId = serviceLevel.SlaId;
            servicePack.SlaName = serviceLevel.SlaName;
            servicePack.SlaCommonName = serviceLevel.SlaCommonName;
            servicePack.IspId = serviceLevel.IspId;
            servicePack.ServiceClass = serviceLevel.ServiceClass;
            //all NMS speeds must be transformed from kbps to bits/sec
            servicePack.DRFWD = serviceLevel.DRFWD * 1000;
            servicePack.DRRTN = serviceLevel.DRRTN * 1000;
            servicePack.CIRFWD = serviceLevel.CIRFWD * 1000;
            servicePack.CIRRTN = serviceLevel.CIRRTN * 1000;
            servicePack.MinBWFWD = serviceLevel.MinBWFWD * 1000;
            servicePack.MinBWRTN = serviceLevel.MinBWRTN * 1000;
            //NMS volumes must be transformed from GB to bytes
            servicePack.HighVolumeSUM = Convert.ToInt64(serviceLevel.HighVolumeSUM * 1000000000);
            servicePack.LowVolumeSUM = Convert.ToInt64(serviceLevel.LowVolumeSUM * 1000000000);
            servicePack.Weight = serviceLevel.SLAWeight;

            return servicePack;
        }
    }
}