﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TerminalActivationRequestList.ascx.cs" Inherits="FOWebApp.Nocsa.TerminalActivationRequestList" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script type="text/javascript">
    function RowSelected(sender, eventArgs) {
        var grid = sender;
        var masterDataView = grid.get_masterTableView();
        var rowNum = eventArgs.get_itemIndexHierarchical();
        var row = masterDataView.get_dataItems()[rowNum];
        var macAddress = masterDataView.getCellByColumnUniqueName(row, "MacAddressColumn").innerHTML;

        //Initialize and show the terminal details window
        var oWnd = radopen("Nocsa/TerminalForm.aspx?mac=" + macAddress, "RadWindowModifyTerminal");
    }

    function CommandSelected(sender, eventArgs) {
        var grid = sender;
        if (eventArgs.get_commandName() == "ModifyTerminal") {
            var macAddress = eventArgs.get_commandArgument();
            alert(macAddress);
            var oWnd = radopen("Nocsa/TerminalForm.aspx?mac=" + macAddress, "RadWindowModifyTerminal");
        }
    }
</script>
<telerik:RadGrid ID="RadGridTerminals" runat="server" AllowPaging="True" AllowSorting="True"
    AutoGenerateColumns="False" CellSpacing="0" GridLines="None" 
    Skin="Metro" ShowStatusBar="True"
    Width="100%" AllowFilteringByColumn="True" OnItemDataBound="RadGridTerminals_ItemDataBound"
    OnNeedDataSource="RadGridTerminals_NeedDataSource" OnColumnCreated="RadGridTerminals_ColumnCreated"
    OnItemCreated="RadGridTerminals_ItemCreated" 
    onitemcommand="RadGridTerminals_ItemCommand" 
    onpageindexchanged="RadGridTerminals_PageIndexChanged" 
    onsortcommand="RadGridTerminals_SortCommand">
    <GroupingSettings CaseSensitive="false" /> 
    <ClientSettings EnableRowHoverStyle="true">
        <Selecting AllowRowSelect="True" />
        <ClientEvents OnRowSelected="RowSelected" />
        <ClientEvents OnCommand="CommandSelected" />
    </ClientSettings>
    <MasterTableView CommandItemDisplay="Top" CommandItemSettings-ShowExportToExcelButton="True"
        CommandItemSettings-ShowExportToCsvButton="True" RowIndicatorColumn-Visible="True"
        RowIndicatorColumn-Resizable="True" RowIndicatorColumn-AndCurrentFilterFunction="Contains"
        CommandItemSettings-ShowAddNewRecordButton="False" PageSize="20" CommandItemSettings-ShowExportToWordButton="True">
        <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
        </RowIndicatorColumn>
        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
        </ExpandCollapseColumn>
        <Columns>
            <telerik:GridBoundColumn DataField="FullName" FilterControlAltText="Filter FullNameColumn column"
                HeaderText="Name" UniqueName="FullNameColumn" ShowFilterIcon="False" AutoPostBackOnFilter="True">
                <ItemStyle Wrap="False" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="MacAddress" FilterControlAltText="Filter MacAddressColumn column"
                HeaderText="MAC Address" MaxLength="12" ReadOnly="True" Resizable="False" UniqueName="MacAddressColumn"
                CurrentFilterFunction="Contains" ShowFilterIcon="False" AutoPostBackOnFilter="True">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="SitId" FilterControlAltText="Filter SitIdColumn column"
                HeaderText="Site ID" UniqueName="SitIdColumn" ShowFilterIcon="False">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="IpAddress" FilterControlAltText="Filter IpAddressColumn column"
                HeaderText="IP Address" ReadOnly="True" UniqueName="IpAddressColumn" ShowFilterIcon="False"
                AutoPostBackOnFilter="True">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn AllowSorting="True" DataField="AdmStatus" FilterControlAltText="Filter AdmStatusColumn column"
                HeaderText="Status" ReadOnly="True" UniqueName="AdmStatusColumn" ShowFilterIcon="False"
                AutoPostBackOnFilter="True" AllowFiltering="False" ItemStyle-Wrap="False">
                <ItemStyle Wrap="False"></ItemStyle>
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="StartDate" FilterControlAltText="Filter StartDateColumn column"
                HeaderText="Start Date" ReadOnly="True" UniqueName="StartDateColumn" ItemStyle-Wrap="False"
                ShowFilterIcon="False" DataFormatString="{0:dd/MM/yyyy}">
                <ItemStyle Wrap="False"></ItemStyle>
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn AllowSorting="False" DataField="ExpiryDate" DataType="System.DateTime"
                FilterControlAltText="Filter ExpiryDateColumn column" HeaderText="Expiry Date"
                ReadOnly="True" UniqueName="ExpiryDateColumn" ItemStyle-Wrap="False" ShowFilterIcon="True"
                AutoPostBackOnFilter="True" DataFormatString="{0:dd/MM/yyyy}">
                <ItemStyle Wrap="False"></ItemStyle>
            </telerik:GridBoundColumn>
        </Columns>
        <EditFormSettings>
            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
            </EditColumn>
        </EditFormSettings>
        <PagerStyle AlwaysVisible="True" />
    </MasterTableView>
    <FilterMenu EnableImageSprites="False">
    </FilterMenu>
    <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default">
    </HeaderContextMenu>
</telerik:RadGrid>
<telerik:RadWindowManager ID="RadWindowManager1" runat="server" Skin="Metro" DestroyOnClose="True">
    <Windows>
        <telerik:RadWindow ID="RadWindowTerminalDetails" runat="server" 
            Animation="None" Opacity="100" Skin="Metro" Title="Terminal Details" AutoSize="False"
            Width="850px" Height="600px" KeepInScreenBounds="True" EnableShadow="True" EnableAriaSupport="True"
            VisibleStatusbar="True" Modal="False" DestroyOnClose="False">
        </telerik:RadWindow>
        <telerik:RadWindow ID="RadWindowModifyTerminal" runat="server" Animation="None" Opacity="100"
            Skin="Metro" Title="Modify Terminal" AutoSize="False" Width="850px" Height="700px"
            KeepInScreenBounds="True" EnableShadow="True" EnableAriaSupport="True" VisibleStatusbar="True"
            Modal="False" DestroyOnClose="False">
        </telerik:RadWindow>
        <telerik:RadWindow ID="RadWindowModifyActivity" runat="server" Animation="None" Opacity="100"
            Skin="Metro" Title="Modify Activity" AutoSize="False" Width="850px" Height="655px"
            KeepInScreenBounds="True" EnableShadow="True" EnableAriaSupport="True" VisibleStatusbar="True"
            Modal="False" DestroyOnClose="False">
        </telerik:RadWindow>
    </Windows>
</telerik:RadWindowManager>