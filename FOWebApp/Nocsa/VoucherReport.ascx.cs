﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FOWebApp.net.nimera.cmt.BOVoucherControllerWSRef;

namespace FOWebApp.Nocsa
{
    public partial class VoucherReport : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int actMonth = DateTime.Now.Month;
            int actYear = DateTime.Now.Year;

            int daysInMonth = DateTime.DaysInMonth(actYear, actMonth);

            RadDatePickerStart.SelectedDate = new DateTime(actYear, actMonth, 1);
            RadDatePickerEnd.SelectedDate = new DateTime(actYear, actMonth, daysInMonth);
        }

        protected void ImageButtonQuery_Click(object sender, EventArgs e)
        {
            DateTime startDate;
            DateTime endDate;

            BOVoucherControllerWS boVoucherControl = new BOVoucherControllerWS();

            if (!RadDatePickerStart.IsEmpty || !RadDatePickerEnd.IsEmpty)
            {
                startDate = (DateTime)RadDatePickerStart.SelectedDate;
                endDate = (DateTime)RadDatePickerEnd.SelectedDate;

                int batchId = 0;
                if (RadNumericTextBoxBatchId.Value > 0)
                {
                    batchId = (int)RadNumericTextBoxBatchId.Value;
                }

                this.Session["voucherReport_StartDate"] = startDate;
                this.Session["voucherReport_EndDate"] = endDate;

                // Include end date in the calculation to create the report
                endDate = endDate.AddDays(1);

                RadGridVouchers.DataSource = boVoucherControl.GetVouchersForReport(startDate, endDate, batchId, true);
                RadGridVouchers.Rebind();
                RadGridVouchers.Visible = true;
            }
            else
            {
                RadGridVouchers.Visible = false;
            }
        }

        protected void ImageButtonCSVExport_Click(object sender, ImageClickEventArgs e)
        {
            this.ConfigureExport();
            RadGridVouchers.MasterTableView.ExportToCSV();
        }

        protected void RadGridVouchers_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            DateTime startDate;
            DateTime endDate;

            BOVoucherControllerWS boVoucherControl = new BOVoucherControllerWS();

            if (!RadDatePickerStart.IsEmpty || !RadDatePickerEnd.IsEmpty)
            {
                startDate = (DateTime)RadDatePickerStart.SelectedDate;
                endDate = (DateTime)RadDatePickerEnd.SelectedDate;

                int batchId = 0;
                if (RadNumericTextBoxBatchId.Value > 0)
                {
                    batchId = (int)RadNumericTextBoxBatchId.Value;
                }

                this.Session["voucherReport_StartDate"] = startDate;
                this.Session["voucherReport_EndDate"] = endDate;

                // Include end date in the calculation to create the report
                endDate = endDate.AddDays(1);

                RadGridVouchers.DataSource = boVoucherControl.GetVouchersForReport(startDate, endDate, batchId, true);
                RadGridVouchers.Visible = true;
            }
            else
            {
                RadGridVouchers.Visible = false;
            }
        }

        protected void ImageButtonPDFExport_Click(object sender, ImageClickEventArgs e)
        {
            //DateTime startDate = (DateTime)RadDatePickerStart.SelectedDate;
            //DateTime endDate = (DateTime)RadDatePickerEnd.SelectedDate;
            //RadGridVouchers.ExportSettings.Pdf.Title = "Voucher batches from " + startDate.Date + " To " + endDate.Date;
            //RadGridVouchers.ExportSettings.Pdf.FontType = Telerik.Web.Apoc.Render.Pdf.FontType.Embed;
            //RadGridVouchers.ExportSettings.Pdf.DefaultFontFamily = "Arial";
            //RadGridVouchers.ExportSettings.Pdf.PaperSize = GridPaperSize.A4;
            this.ConfigureExport();
            RadGridVouchers.MasterTableView.ExportToPdf();
        }

        protected void ImageButtonExcelExport_Click(object sender, ImageClickEventArgs e)
        {
            this.ConfigureExport();
            RadGridVouchers.MasterTableView.ExportToExcel();
        }

        protected void RadGridVouchers_ItemCommand(object sender, GridCommandEventArgs e)
        {
            this.ConfigureExport();
            if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToExcelCommandName)
            {
                RadGridVouchers.MasterTableView.ExportToExcel();
            }

            if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToCsvCommandName)
            {
                RadGridVouchers.MasterTableView.ExportToCSV();
            }

            if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToPdfCommandName)
            {
                RadGridVouchers.MasterTableView.ExportToPdf();
            }

            if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToWordCommandName)
            {
                RadGridVouchers.MasterTableView.ExportToWord();
            }
        }

        /// <summary>
        /// Configures the export settings for this radgrid
        /// </summary>
        public void ConfigureExport()
        {
            RadGridVouchers.ExportSettings.FileName = "CMTVouchers";
            RadGridVouchers.ExportSettings.ExportOnlyData = true;
            RadGridVouchers.ExportSettings.IgnorePaging = true;
            RadGridVouchers.ExportSettings.OpenInNewWindow = true;
            RadGridVouchers.ExportSettings.Excel.Format = GridExcelExportFormat.Html;
            RadGridVouchers.ExportSettings.Csv.ColumnDelimiter = GridCsvDelimiter.Semicolon;
            RadGridVouchers.ExportSettings.Csv.EncloseDataWithQuotes = false;
            RadGridVouchers.ExportSettings.Csv.RowDelimiter = GridCsvDelimiter.NewLine;
        }
    }
}