﻿using FOWebApp.net.nimera.cmt.BOConnectedDevicesControlWSRef;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FOWebApp.Nocsa
{
    public partial class NewDeviceType : System.Web.UI.UserControl
    {
        BOConnectedDevicesControlWS _connectedDevicesControl;

        protected void Page_Load(object sender, EventArgs e)
        {
            _connectedDevicesControl = new BOConnectedDevicesControlWS();
        }

        protected void ButtonAdd_Click(object sender, EventArgs e)
        {
            ConnectedDeviceType deviceType = new ConnectedDeviceType();
            deviceType.Name = TextBoxDeviceTypeName.Text;
            deviceType.Description = TextBoxDescription.Text;
            
            try
            {
                if (_connectedDevicesControl.InsertDeviceType(deviceType))
                {
                    LabelAdd.Text = "Device was successfully connected to terminal";
                    LabelAdd.ForeColor = Color.Green;
                }
            }
            catch (Exception ex)
            {
                LabelAdd.Text = "Adding device failed";
                LabelAdd.ForeColor = Color.Red;
            }
        }
    }
}