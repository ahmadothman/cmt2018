﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OutgoingEmailsLists.ascx.cs" Inherits="FOWebApp.Nocsa.OutgoingEmailsLists" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script type="text/javascript">
    function RowSelected(sender, eventArgs) {
        var grid = sender;
        var masterDataView = grid.get_masterTableView();
        var rowNum = eventArgs.get_itemIndexHierarchical();
        var row = masterDataView.get_dataItems()[rowNum];
        var date = masterDataView.getCellByColumnUniqueName(row, "Date2").innerHTML;

        //Initialize and show the ticket details window
        var oWnd = radopen("Nocsa/OutgoingEmailsReport.aspx?date=" + date, "RadWindowEmailReport");
    }
</script>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="RadGridEmailReports">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGridEmailReports" />
                <telerik:AjaxUpdatedControl ControlID="RadWindowManager1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Metro"/>
<telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" Height="200px" 
    Width="467px" HorizontalAlign="NotSet" 
    LoadingPanelID="RadAjaxLoadingPanel1">
 <telerik:RadGrid ID="RadGridEmailReports" runat="server" AllowSorting="False"
    AutoGenerateColumns="false" Visible="true" CellSpacing="0" GridLines="None" Skin="Metro"
    ShowStatusBar="True" Width="500px" AllowFilteringByColumn="False">
        <GroupingSettings CaseSensitive="false" /> 
    <ClientSettings>
        <Selecting AllowRowSelect="True" />
        <ClientEvents OnRowSelected="RowSelected" />
    </ClientSettings>
    <MasterTableView CommandItemDisplay="Top" CommandItemSettings-ShowExportToExcelButton="False" 
        CommandItemSettings-ShowExportToCsvButton="False" RowIndicatorColumn-Visible="True" RowIndicatorColumn-Resizable="True" RowIndicatorColumn-AndCurrentFilterFunction="Contains" CommandItemSettings-ShowAddNewRecordButton="False" ShowFooter="False">
        <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
        </RowIndicatorColumn>
        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
        </ExpandCollapseColumn>
        <Columns>
            <telerik:GridBoundColumn DataField="Date2" 
                FilterControlAltText="Filter Date2Column column" HeaderText="Date" 
                UniqueName="Date2" ShowFilterIcon="False" AutoPostBackOnFilter="True">
                <ItemStyle Wrap="False" />
            </telerik:GridBoundColumn>
        </Columns>
        <EditFormSettings>
            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
            </EditColumn>
        </EditFormSettings>
    </MasterTableView>
    <FilterMenu EnableImageSprites="False">
    </FilterMenu>
    <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default">
    </HeaderContextMenu>
</telerik:RadGrid>
<telerik:RadWindowManager ID="RadWindowManager1" runat="server" Skin="Metro" 
        DestroyOnClose="True">
    <Windows>
        <telerik:RadWindow ID="RadWindowEmailReport" runat="server" 
            NavigateUrl="Nocsa/OutgoingEmailsReport.aspx" Animation="Fade" Opacity="100" Skin="Metro" 
            Title="Email Report" AutoSize="False" Width="1000px" Height="700px" KeepInScreenBounds="True" EnableShadow="True" EnableAriaSupport="True" VisibleStatusbar="True" Modal="False" DestroyOnClose="False">
        </telerik:RadWindow>
    </Windows>
</telerik:RadWindowManager>
</telerik:RadAjaxPanel>
