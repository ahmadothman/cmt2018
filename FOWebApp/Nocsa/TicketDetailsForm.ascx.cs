﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;

namespace FOWebApp.Nocsa
{
    public partial class TicketDetailsForm : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public void componentDataInit(string ticketId)
        {
            BOLogControlWS boLogController = new BOLogControlWS();
            BOAccountingControlWS boAccountingController = new BOAccountingControlWS();
            CMTTicket ticket = boLogController.GetTicketById(ticketId);
            LabelTicketId.Text = ticket.TicketId;
            LabelCreationDate.Text = ticket.CreationDate.ToString("yyyy-MM-dd HH:mm");
            LabelMacAddress.Text = ticket.MacAddress;
            LabelSourceAdmState.Text = this.getTerminalStatus(ticket.SourceAdmState);
            LabelTargetAdmState.Text = this.getTerminalStatus(ticket.TargetAdmState);
            LabelTicketStatus.Text = this.getTicketStatus(ticket.TicketStatus);
            TextBoxFailureReason.Text = ticket.FailureReason;
            TextBoxFailureStackTrace.Text = ticket.FailureStackTrace;
            LabelTermActivity.Text = this.getTerminalActivity(ticket.TermActivity);
            if (ticket.newSla != 0 && ticket.newSla != null)
            {
                ServiceLevel sla = boAccountingController.GetServicePack(ticket.newSla);
                LabelNewSla.Text = ticket.newSla.ToString() + ": " + sla.SlaName;
            }
            Isp isp = boAccountingController.GetISP(ticket.IspId);
            LabelIspId.Text = ticket.IspId.ToString() + " " + isp.CompanyName;
            LabelNewMacAddress.Text = ticket.NewMacAddress;
            LabelNMSActivate.Text = this.getNMSActivateType(ticket.NMSActivate);
        }

        private string getTerminalStatus(int termState)
        {
            string status;

            if (termState == 1)
            {
                status = "LOCKED";
            }
            else if (termState == 2)
            {
                status = "OPERATIONAL";
            }
            else if (termState == 3)
            {
                status = "DECOMMISSIONED";
            }
            else if (termState == 5)
            {
                status = "REQUEST";
            }
            else if (termState == 6)
            {
                status = "DELETED";
            }
            else
            {
                status = "TEST";
            }

            return status;
        }

        private string getTicketStatus(int ticketStatusId)
        {
            string status = "BUSY";

            if (ticketStatusId == 1)
            {
                status = "FAILED";
            }
            else if (ticketStatusId == 2)
            {
                status = "SUCCESSFUL";
            }

            return status;
        }

        private string getTerminalActivity(int termActivityId)
        {
            string termActivity = "";
            
            if (termActivityId == 100)
            {
                termActivity = "Activation";
            }
            else if (termActivityId == 200)
            {
                termActivity = "Suspension";
            }
            else if (termActivityId == 300)
            {
                termActivity = "ReActivation";
            }
            else if (termActivityId == 400)
            {
                termActivity = "Decommissioning";
            }
            else if (termActivityId == 500)
            {
                termActivity = "Deletion";
            }
            else if (termActivityId == 600)
            {
                termActivity = "ChangeSla";
            }
            else if (termActivityId == 601)
            {
                termActivity = "ChangeEdgeSla";
            }
            else if (termActivityId == 700)
            {
                termActivity = "FUPReset";
            }
            else if (termActivityId == 900)
            {
                termActivity = "ChangeMac";
            }
            else if (termActivityId == 1000)
            {
                termActivity = "EnableStream";
            }
            else if (termActivityId == 1100)
            {
                termActivity = "DisableStream";
            }
            else if (termActivityId == 1002)
            {
                termActivity = "IPAddressChange";
            }
            else if (termActivityId == 1200)
            {
                termActivity = "ChangeWeight";
            }

            return termActivity;
        }

        private string getNMSActivateType(int nmsActivateId)
        {
            string type = "";

            if (nmsActivateId == 0)
            {
                type = "False";
            }
            else if (nmsActivateId == 1)
            {
                type = "True";
            }
            else if (nmsActivateId == 2)
            {
                type = "NMS ticket";
            }

            return type;
        }
    }
}