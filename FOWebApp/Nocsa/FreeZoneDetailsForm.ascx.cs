﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOMonitorControlWSRef;
using System.Drawing;

namespace FOWebApp.Nocsa
{
    public partial class FreeZoneDetailsForm : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            TextBoxMonOff.Mask = _HOUR + "<:>" + _MINUTE + "<:>" + _SECOND;
            TextBoxMonOn.Mask = _HOUR + "<:>" + _MINUTE + "<:>" + _SECOND;
            TextBoxTueOff.Mask = _HOUR + "<:>" + _MINUTE + "<:>" + _SECOND;
            TextBoxTueOn.Mask = _HOUR + "<:>" + _MINUTE + "<:>" + _SECOND;
            TextBoxWedOff.Mask = _HOUR + "<:>" + _MINUTE + "<:>" + _SECOND;
            TextBoxWedOn.Mask = _HOUR + "<:>" + _MINUTE + "<:>" + _SECOND;
            TextBoxThuOff.Mask = _HOUR + "<:>" + _MINUTE + "<:>" + _SECOND;
            TextBoxThuOn.Mask = _HOUR + "<:>" + _MINUTE + "<:>" + _SECOND;
            TextBoxFriOff.Mask = _HOUR + "<:>" + _MINUTE + "<:>" + _SECOND;
            TextBoxFriOn.Mask = _HOUR + "<:>" + _MINUTE + "<:>" + _SECOND;
            TextBoxSatOff.Mask = _HOUR + "<:>" + _MINUTE + "<:>" + _SECOND;
            TextBoxSatOn.Mask = _HOUR + "<:>" + _MINUTE + "<:>" + _SECOND;
            TextBoxSunOff.Mask = _HOUR + "<:>" + _MINUTE + "<:>" + _SECOND;
            TextBoxSunOn.Mask = _HOUR + "<:>" + _MINUTE + "<:>" + _SECOND;
        }

        public void componentDataInit(int freeZoneId)
        {
            //Load free zone data from NMS
            FreeZone freeZone = boMonitorControl.NMSgetFreeZone(freeZoneId, ispId);
            LabelId.Text = freeZone.Id.ToString();
            TextBoxName.Text = freeZone.Name;
            //if (freeZone.CurrentActive == 1)
            //{
            //    CheckBoxActive.Checked = true;
            //}
            TextBoxMonOff.Text = freeZone.MonOff.ToString();
            TextBoxMonOn.Text = freeZone.MonOn.ToString();
            TextBoxTueOff.Text = freeZone.TueOff.ToString();
            TextBoxTueOn.Text = freeZone.TueOn.ToString();
            TextBoxWedOff.Text = freeZone.WedOff.ToString();
            TextBoxWedOn.Text = freeZone.WedOn.ToString();
            TextBoxThuOn.Text = freeZone.ThuOn.ToString();
            TextBoxThuOff.Text = freeZone.ThuOff.ToString();
            TextBoxFriOn.Text = freeZone.FriOn.ToString();
            TextBoxFriOff.Text = freeZone.FriOff.ToString();
            TextBoxSatOn.Text = freeZone.SatOn.ToString();
            TextBoxSatOff.Text = freeZone.SatOff.ToString();
            TextBoxSunOn.Text = freeZone.SunOn.ToString();
            TextBoxSunOff.Text = freeZone.SunOff.ToString();
        }

        protected void ButtonUpdate_Click(object sender, EventArgs e)
        {
            FreeZoneCMT freeZone = new FreeZoneCMT();
            freeZone.Id = Convert.ToInt32(LabelId.Text);
            freeZone.Name = TextBoxName.Text;
            //if (CheckBoxActive.Checked)
            //{
            //    freeZone.CurrentActive = 1;
            //}
            //else
            //{
                freeZone.CurrentActive = 0;
            //}
            freeZone.MonOff = TextBoxMonOff.Text;
            freeZone.MonOn = TextBoxMonOn.Text;
            freeZone.TueOff = TextBoxTueOff.Text;
            freeZone.TueOn = TextBoxTueOn.Text;
            freeZone.WedOff = TextBoxWedOff.Text;
            freeZone.WedOn = TextBoxWedOn.Text;
            freeZone.ThuOff = TextBoxThuOff.Text;
            freeZone.ThuOn = TextBoxThuOn.Text;
            freeZone.FriOff = TextBoxFriOff.Text;
            freeZone.FriOn = TextBoxFriOn.Text;
            freeZone.SatOff = TextBoxSatOff.Text;
            freeZone.SatOn = TextBoxSatOn.Text;
            freeZone.SunOff = TextBoxSunOff.Text;
            freeZone.SunOn = TextBoxSunOn.Text;

            FreeZone freeZoneNMS = this.cmtToNms(freeZone);
            if (boMonitorControl.NMSupdateFreeZone(freeZoneNMS, ispId))
            {
                if (boAccountingControl.UpdateFreeZone(freeZone))
                {
                    LabelResult.Text = "Free zone updated successfully";
                    LabelResult.ForeColor = Color.Green;
                }
                else
                {
                    LabelResult.Text = "Updating free zone failed";
                    LabelResult.ForeColor = Color.Red;
                }
            }
            else
            {
                LabelResult.Text = "Updating free zone failed";
                LabelResult.ForeColor = Color.Red;
            }
        }

        protected FreeZone cmtToNms(FreeZoneCMT cmt)
        {
            FreeZone nms = new FreeZone();
            nms.Id = cmt.Id;
            nms.Name = cmt.Name;
            nms.CurrentActive = cmt.CurrentActive;
            nms.MonOff = cmt.MonOff;
            nms.MonOn = cmt.MonOn;
            nms.TueOff = cmt.TueOff;
            nms.TueOn = cmt.TueOn;
            nms.WedOff = cmt.WedOff;
            nms.WedOn = cmt.WedOn;
            nms.ThuOff = cmt.ThuOff;
            nms.ThuOn = cmt.ThuOn;
            nms.FriOff = cmt.FriOff;
            nms.FriOn = cmt.FriOn;
            nms.SatOff = cmt.SatOff;
            nms.SatOn = cmt.SatOn;
            nms.SunOff = cmt.SunOff;
            nms.SunOn = cmt.SunOn;

            return nms;
        }

        public BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();
        public BOMonitorControlWS boMonitorControl = new BOMonitorControlWS();
        public int ispId = 469; //ISP ID of the NMS ISP
        const string _HOUR = "<0|1|2><0|1|2|3|4|5|6|7|8|9>";
        const string _MINUTE = "<0|1|2|3|4|5><0|1|2|3|4|5|6|7|8|9>";
        const string _SECOND = "<0|1|2|3|4|5><0|1|2|3|4|5|6|7|8|9>";
    }
}