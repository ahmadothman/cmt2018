﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VoucherRequestDetailsForm.ascx.cs" Inherits="FOWebApp.Nocsa.VoucherRequestDetailsForm" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
     <script type="text/javascript">
    
    </script>
</telerik:RadCodeBlock>
<telerik:RadAjaxPanel ID="RadAjaxPanelVoucherRequestDetails" runat="server" GroupingText="Voucher request details" Height="200px" Width="832px"
    EnableHistory="True" Style="margin-right: 114px">
<table cellpadding="10" cellspacing="5" class="voucherRequestDetails">
    <tr>
        <td>Distributor:</td>
        <td><asp:label ID="LabelDistributor" runat="server"></asp:label></td>
    </tr>
    <tr>
        <td>Voucher volume:</td>
        <td><asp:Label ID="LabelVolume" runat="server"></asp:Label></td>
    </tr>
    <tr>
        <td>Number of vouchers:</td>
        <td><asp:Label ID="LabelNumVoucher" runat="server"></asp:Label></td>
    </tr>
    <tr>
        <td>
            &nbsp;</td>
        <td>
            <asp:Button ID="ButtonDeleteRequest" runat="server" Text="Delete Message" OnClick="ButtonDeleteRequest_Click"></asp:Button>
        </td>
    </tr>
    <tr>
        <td colspan="2"><asp:Label ID="LabelResult" runat="server"></asp:Label></td>
    </tr>
    <tr>
        <td class="style1" colspan="2">
            <asp:Image ID="ImageAjax" runat="server" ImageUrl="~/Images/ajax-loader.gif" style="display: none; vertical-align: middle"/><div ID="DivDownloadURL" runat="server"></div>
        </td>
    </tr>
</table>
</telerik:RadAjaxPanel>
<telerik:RadWindowManager ID="RadWindowManager1" runat="server" Skin="Metro" EnableShadow="True"
    Left="0px" Right="0px" Modal="True" VisibleStatusbar="False">
</telerik:RadWindowManager>
