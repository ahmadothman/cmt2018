﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DistributorDetailsPage.aspx.cs" Inherits="FOWebApp.Nocsa.DistributorDetailsPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Modify Distributor</title>
    <link href="../CMTStyle.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManagerDistributorDetails" runat="server">
        <Services>
            <asp:ServiceReference Path="../WebServices/AccountSupportWS.asmx" />
        </Services>
    </asp:ScriptManager>
    <div>
        <asp:PlaceHolder ID="PlaceHolderDistributorDetailsForm" runat="server"></asp:PlaceHolder>
    </div>
    </form>
</body>
</html>
