﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EndUserForm.aspx.cs" Inherits="FOWebApp.Nocsa.EndUserForm" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="../CMTStyle.css" rel="stylesheet" type="text/css" />
    <title>End User Update</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Label ID="LabelId" runat="server" Visible="False"></asp:Label>
        <table cellpadding="10" cellspacing="5" style="width: 717px">
            <tr>
                <td>
                    Sit Id:
                </td>
                <td>
                    <asp:TextBox ID="TextBoxSitId" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorSitId" runat="server" ErrorMessage="Please specify your SitId!"
                        ControlToValidate="TextBoxSitId">
                    </asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    First Name:
                </td>
                <td>
                    <asp:TextBox ID="TextBoxFirstName" runat="server" Width="285px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorFirstName" runat="server" ErrorMessage="Please specify your first name!"
                        ControlToValidate="TextBoxFirstName">
                    </asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Last Name:
                </td>
                <td>
                    <asp:TextBox ID="TextBoxLastName" runat="server" Width="285px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorLastName" runat="server" ErrorMessage="Please specify your last name!"
                        ControlToValidate="TextBoxLastName">
                    </asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Phone:
                </td>
                <td>
                    <asp:TextBox ID="TextBoxPhone" runat="server" Width="177px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorPhone" runat="server" ErrorMessage="Please provide a phone number!"
                        ControlToValidate="TextBoxPhone"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Mobile Phone:
                </td>
                <td>
                    <asp:TextBox ID="TextBoxMobilePhone" runat="server" Width="178px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    E-Mail:
                </td>
                <td>
                    <asp:TextBox ID="TextBoxEMail" runat="server" Width="288px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorEMail" runat="server" ErrorMessage="Please provide your E-Mail address!"
                        ControlToValidate="TextBoxEMail"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Button ID="ButtonSubmit" runat="server" Text="Submit" 
                        onclick="ButtonSubmit_Click"/>
                </td>
                <td>
                    <asp:Label ID="LabelResult" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
