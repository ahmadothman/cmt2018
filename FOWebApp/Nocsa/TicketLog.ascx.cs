﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using Telerik.Web.UI;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;

namespace FOWebApp.Nocsa
{
    public partial class TicketLog : System.Web.UI.UserControl
    {
        const string _HEXBYTE = "<0|1|2|3|4|5|6|7|8|9|a|b|c|d|e|f><0|1|2|3|4|5|6|7|8|9|a|b|c|d|e|f>";
        
        protected void Page_Load(object sender, EventArgs e)
        {
            //Create a blank input form
            RadMaskedTextBoxMacAddress.Mask =
               _HEXBYTE + "<:>" + _HEXBYTE + "<:>" + _HEXBYTE + "<:>" + _HEXBYTE + "<:>" + _HEXBYTE + "<:>" +
               _HEXBYTE;
        }

        protected void ButtonGetTickets_Click(object sender, EventArgs e)
        {
            string mac = RadMaskedTextBoxMacAddress.Text;
            BOLogControlWS boLogController = new BOLogControlWS();
            CMTTicket[] tickets = boLogController.GetTicketsByMacAddress(mac);

            RadGridTickets.Visible = true;
            RadGridTickets.DataSource = tickets;
            RadGridTickets.DataBind();
        }

        protected void RadGridTickets_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem dataItem = (GridDataItem)e.Item;

                if (dataItem["TicketStatus"].Text.Equals("0"))
                {
                    dataItem["TicketStatus"].Text = "Busy";
                }
                else if (dataItem["TicketStatus"].Text.Equals("1"))
                {
                    dataItem["TicketStatus"].Text = "Failed";
                    dataItem["TicketStatus"].ForeColor = Color.Red;
                }
                else if (dataItem["TicketStatus"].Text.Equals("2"))
                {
                    dataItem["TicketStatus"].Text = "Successful";
                    dataItem["TicketStatus"].ForeColor = Color.Green;
                }
            }
        }

        protected void RadGridTickets_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
        }

        protected void RadGridTickets_ItemCommand(object sender, GridCommandEventArgs e)
        {
        }
    }
}