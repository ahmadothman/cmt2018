﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FOWebApp.net.nimera.cmt.BOTaskControlWSRef;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;

namespace FOWebApp.Nocsa
{
    public partial class TaskList : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void RadGridTasks_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            //Load the open taks in the grid
            BOTaskControlWS boTaskControlWS = new BOTaskControlWS();
            Task[] tasks = boTaskControlWS.GetOpenTasks();
            RadGridTasks.DataSource = tasks;
        }

        protected void RadGridTasks_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            if (e.CommandName.Equals("Complete"))
            {
                //Get the Task id
                //Retrieve the task from the database
                //Update the task with the Complete flag set to true
                //Send confirmation mail to distributor if necessary
                //Rebind task list
                BOTaskControlWS boTaskControlWS = new BOTaskControlWS();
                GridDataKeyArray dataItems = e.Item.OwnerTableView.DataKeyValues;
                string strTaskId = dataItems[e.Item.ItemIndex]["Id"].ToString();
                Task task = boTaskControlWS.GetTask(Int32.Parse(strTaskId));

                if (task != null)
                {
                    task.Completed = true;
                    task.DateCompleted = DateTime.Now;
                    boTaskControlWS.UpdateTask(task);
                 
                    if (task.Type.DistInfo)
                    {
                        if (!this.SendConfirmationMail(task))
                        {
                            BOLogControlWS boLogControl = new BOLogControlWS();
                            CmtApplicationException cmtEx = new CmtApplicationException();
                            cmtEx.ExceptionDateTime = DateTime.Now;
                            cmtEx.ExceptionDesc = "Could not send E-Mail to Distributor";
                            cmtEx.UserDescription = "Completion of task: " + task.Id;
                            boLogControl.LogApplicationException(cmtEx);
                        }
                    }
                }
                Task[] tasks = boTaskControlWS.GetOpenTasks();
                RadGridTasks.DataSource = tasks;
                RadGridTasks.DataBind();
             }
        }

        protected bool SendConfirmationMail(Task task)
        {
            bool result = false;
            string eMailMsg = "";
            //First get the Distributor E-mail address
            BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();
            BOLogControlWS boLogControl = new BOLogControlWS();
            Distributor dist = boAccountingControl.GetDistributor(task.DistributorId);

            if (!dist.Email.Equals(""))
            {
                eMailMsg = "Dear Customer,<br/><br/>Please find hereafter a confirmation E-Mail for the following activity: <br/><br/>" +
                    task.Subject + "<br/><br/>" +
                    "Terminal Mac Address: " + task.MacAddress + "<br/><br/>" +
                    "Task completion date: " + task.DateCompleted + "<br/><br/>" + 
                    "Do not hesitate to contact the SatADSL support team in case you have further questions about this E-Mail." + 
                    "<br/><br/>Best regards,<br/><br/>Your SatADSL support team";
                string[] mailAddresses = dist.Email.Split(';');
                result = boLogControl.SendMail(eMailMsg, task.Subject, mailAddresses);
            }
            else
            {
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDateTime = DateTime.Now;
                cmtEx.ExceptionDesc = "Could not send E-Mail to Distributor, E-Mail address empty";
                cmtEx.UserDescription = "Completion of task: " + task.Id;
                boLogControl.LogApplicationException(cmtEx);
            }

            return result;
        }
    }
}