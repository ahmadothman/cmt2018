﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using Telerik.Web.UI;
using FOWebApp.net.nimera.cmt.BOUserControlWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;

namespace FOWebApp.Nocsa
{
    public partial class UserMaintenanceList : System.Web.UI.UserControl
    {
        BOUserControlWS _boUserControlWS;
        BOAccountingControlWS _boAccountingControlWS = new BOAccountingControlWS();

        public UserMaintenanceList()
        {
            _boUserControlWS = new BOUserControlWS();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            RadGridUserMaintenanceList_NeedDataSource(RadGridUserMaintenanceList, null);

            if (this.Session["_userMaintenanceListGrid_PageIdx"] != null)
            {
                RadGridUserMaintenanceList.CurrentPageIndex =
                            (int)this.Session["_userMaintenanceListGrid_PageIdx"];
            }
        }

        protected void RadGridUserMaintenanceList_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            //Load the users with their roles into the datagrid

            //MembershipUserCollection users = Membership.GetAllUsers();
            MembershipUserCollection users = new MembershipUserCollection();
            Guid[] userIds = _boUserControlWS.GetUsers();

            foreach (Guid userid in userIds)
            {
                MembershipUser user = Membership.GetUser(userid);
                users.Add(user);
            }
            RadGridUserMaintenanceList.DataSource = this.GetListOfUsersWithRole(users);
        }

        protected void RadGridUserMaintenanceList_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToExcelCommandName)
            {
                this.ConfigureExport();
                RadGridUserMaintenanceList.MasterTableView.ExportToExcel();
            }

            if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToCsvCommandName)
            {
                this.ConfigureExport();
                RadGridUserMaintenanceList.MasterTableView.ExportToCSV();
            }

            if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToPdfCommandName)
            {
                this.ConfigureExport();
                RadGridUserMaintenanceList.MasterTableView.ExportToPdf();
            }

            if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToWordCommandName)
            {
                this.ConfigureExport();
                RadGridUserMaintenanceList.MasterTableView.ExportToWord();
            }
        }

        protected void RadGridUserMaintenanceList_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            BOLogControlWS boLogControl = new BOLogControlWS();
            GridDataKeyArray dataItems = e.Item.OwnerTableView.DataKeyValues;
            string userId = dataItems[e.Item.ItemIndex]["User.ProviderUserKey"].ToString();
            string userName = dataItems[e.Item.ItemIndex]["User.UserName"].ToString();

            try
            {
                //CMTFO-166
                //_boUserControlWS.DeconnectUser(new Guid(userId));

                //Remove user from the Membership database
                //if (!Membership.DeleteUser(userName))
                if (!_boUserControlWS.DeleteUser(new Guid(userId), userName, DateTime.Now))
                {
                    CmtApplicationException cmtEx = new CmtApplicationException();
                    cmtEx.ExceptionDesc = "Deleting user: " + userName + " failed";
                    cmtEx.StateInformation = "Delete user failed";
                    boLogControl.LogApplicationException(cmtEx);
                }
                else
                {
                    UserActivity userActivity = new UserActivity();
                    userActivity.Action = "User delete";
                    userActivity.ActionDate = DateTime.Now;
                    userActivity.UserId = new Guid(userId);
                    boLogControl.LogUserActivity(userActivity);
                }

                MembershipUserCollection users = new MembershipUserCollection();
                Guid[] userIds = _boUserControlWS.GetUsers();

                foreach (Guid userid in userIds)
                {
                    MembershipUser user = Membership.GetUser(userid);
                    users.Add(user);
                }
                RadGridUserMaintenanceList.DataSource = this.GetListOfUsersWithRole(users);
                RadGridUserMaintenanceList.Rebind();
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDesc = ex.Message;
                cmtEx.ExceptionStacktrace = ex.StackTrace;
                cmtEx.StateInformation = "Delete user failed";
                boLogControl.LogApplicationException(cmtEx);
            }

        }

        protected void RadGridUserMaintenanceList_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            //Save the page index
            int pageIdx = e.NewPageIndex;
            this.Session["_userMaintenanceListGrid_PageIdx"] = pageIdx;
        }

        // Issue CMTFO-84

        // Init and Databound event are fired twice when a post occurs by the grid

        protected void RadComboBoxRoleFilterTemplate_Init(object sender, EventArgs e)
        {
            RadComboBox combobox = sender as RadComboBox;

            if (combobox != null)
            {
                combobox.DataSource = Roles.GetAllRoles();
                //combobox.DataBind(); // Not needed for a particular reason. When uncommented, the DataBound event is fired twice.
            }
        }

        protected void RadComboBoxRoleFilterTemplate_DataBound(object sender, EventArgs e)
        {
            RadComboBox combobox = sender as RadComboBox;

            if (combobox != null)
            {
                combobox.Items.Insert(0, new RadComboBoxItem("", "NoFilter"));
            }
        }

        /// <summary>
        /// Configures the export settings for this radgrid
        /// </summary>
        public void ConfigureExport()
        {
            RadGridUserMaintenanceList.ExportSettings.FileName = "CMTExport";
            RadGridUserMaintenanceList.ExportSettings.ExportOnlyData = true;
            RadGridUserMaintenanceList.ExportSettings.IgnorePaging = true;
            RadGridUserMaintenanceList.ExportSettings.OpenInNewWindow = true;
            RadGridUserMaintenanceList.ExportSettings.Excel.Format = GridExcelExportFormat.Html;
            RadGridUserMaintenanceList.ExportSettings.Csv.ColumnDelimiter = GridCsvDelimiter.Semicolon;
            RadGridUserMaintenanceList.ExportSettings.Csv.EncloseDataWithQuotes = false;
            RadGridUserMaintenanceList.ExportSettings.Csv.RowDelimiter = GridCsvDelimiter.NewLine;
        }

        // Issue CMTFO-84

        protected class UserWithRole
        {
            private string _roleName;
            private string _distName;

            public MembershipUser User { get; set; }

            public string RoleName
            {
                get
                {
                    if (string.IsNullOrEmpty(_roleName))
                    {
                        try
                        {
                            _roleName = Roles.GetRolesForUser(User.UserName)[0];
                        }
                        catch (IndexOutOfRangeException) // Exception is thrown when there are no roles defined for this user
                        {
                            _roleName = "EndUser";
                        }
                    }

                    return _roleName;
                }
                set
                {
                    _roleName = value;
                }
            }

            public string DistName
            {
                get
                {
                    return _distName;
                }
                set
                {
                    _distName = value;
                }
            }

            /// <summary>
            /// A constructor of the UserWithRole class.
            /// It is needed to provide a user, because otherwise a NullReferenceException could be thrown
            /// </summary>
            /// <param name="user">The user to be initialized</param>
            public UserWithRole(MembershipUser user)
            {
                User = user;
            }
        }

        protected List<UserWithRole> GetListOfUsersWithRole(MembershipUserCollection users)
        {
            List<UserWithRole> usersWithRoles = new List<UserWithRole>();

            foreach (MembershipUser user in users)
            {
                UserWithRole userWithRole = new UserWithRole(user);

                try
                {
                    userWithRole.RoleName = Roles.GetRolesForUser(user.UserName)[0];

                    if (userWithRole.RoleName == "Distributor" || userWithRole.RoleName == "DistributorReadOnly")
                    {
                        Distributor dist = _boAccountingControlWS.GetDistributorForUser(user.ProviderUserKey.ToString());
                        userWithRole.DistName = dist.FullName;
                    }
                    else if (userWithRole.RoleName == "Organization")
                    {
                        Organization orga = _boAccountingControlWS.GetOrganizationForUser(user.ProviderUserKey.ToString());
                        userWithRole.DistName = orga.Distributor.FullName;
                    }
                }
                catch (IndexOutOfRangeException) // Exception is thrown when there are no roles defined for this user
                {
                    userWithRole.RoleName = "EndUser";
                }
                catch (NullReferenceException) // Exception is thrown when distributor is null
                {
                    userWithRole.DistName = "";
                }

                usersWithRoles.Add(userWithRole);
            }

            return usersWithRoles;
        }
    }
}

