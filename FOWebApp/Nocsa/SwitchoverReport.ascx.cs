﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using Telerik.Web.UI;
using FOWebApp.Data;
using FOWebApp.net.nimera.cmt.BOSatDiversityControllerWSRef;
using System.Web.Security;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;

namespace FOWebApp.Nocsa
{
    public partial class SwitchoverReport : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int actMonth = DateTime.Now.Month;
            int actYear = DateTime.Now.Year;

            int daysInMonth = DateTime.DaysInMonth(actYear, actMonth);

            RadDatePickerStart.SelectedDate = new DateTime(actYear, actMonth, 1);
            RadDatePickerEnd.SelectedDate = new DateTime(actYear, actMonth, daysInMonth);
        }

        protected void ImageButtonQuery_Click(object sender, EventArgs e)
        {
            DateTime startDate;
            DateTime endDate;
            if (!RadDatePickerStart.IsEmpty || !RadDatePickerEnd.IsEmpty)
            {
                startDate = (DateTime)RadDatePickerStart.SelectedDate;
                endDate = (DateTime)RadDatePickerEnd.SelectedDate;

                this.Session["switchoverReport_StartDate"] = startDate;
                this.Session["switchoverReport_EndDate"] = endDate;

                this.generateReport(startDate, endDate);
                RadGridSwitchoverReport.Rebind();
                RadGridSwitchoverReport.Visible = true;
            }
            else
            {
                RadGridSwitchoverReport.Rebind();
                RadGridSwitchoverReport.Visible = false;
            }
        }

        /// <summary>
        /// Configures the export settings for this radgrid
        /// </summary>
        public void ConfigureExport()
        {
            RadGridSwitchoverReport.ExportSettings.FileName = "CMTExport";
            RadGridSwitchoverReport.ExportSettings.ExportOnlyData = true;
            RadGridSwitchoverReport.ExportSettings.IgnorePaging = true;
            RadGridSwitchoverReport.ExportSettings.OpenInNewWindow = true;
            RadGridSwitchoverReport.ExportSettings.Excel.Format = GridExcelExportFormat.Html;
            RadGridSwitchoverReport.ExportSettings.Csv.ColumnDelimiter = GridCsvDelimiter.Semicolon;
            RadGridSwitchoverReport.ExportSettings.Csv.EncloseDataWithQuotes = false;
            RadGridSwitchoverReport.ExportSettings.Csv.RowDelimiter = GridCsvDelimiter.NewLine;
        }

        protected void RadGridSwitchoverReport_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToExcelCommandName)
            {
                this.ConfigureExport();
                RadGridSwitchoverReport.MasterTableView.ExportToExcel();
            }

            if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToCsvCommandName)
            {
                RadGridSwitchoverReport.MasterTableView.ExportToCSV();
            }

            if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToPdfCommandName)
            {
                RadGridSwitchoverReport.MasterTableView.ExportToPdf();
            }

            if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToWordCommandName)
            {
                RadGridSwitchoverReport.MasterTableView.ExportToWord();
            }
        }

        protected void RadGridSwitchoverReport_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            DateTime startDate;
            DateTime endDate;

            if (!RadDatePickerStart.IsEmpty || !RadDatePickerEnd.IsEmpty)
            {
                startDate = (DateTime)RadDatePickerStart.SelectedDate;
                endDate = (DateTime)RadDatePickerEnd.SelectedDate;

                this.Session["switchoverReport_StartDate"] = startDate;
                this.Session["switchoverReport_EndDate"] = endDate;

                this.generateReport(startDate, endDate);

            }
            else
            {
                //Check if values were stored in session variables
                if (this.Session["switchoverReport_StartDate"] != null && this.Session["switchoverReport_EndDate"] != null)
                {
                    startDate = (DateTime)this.Session["switchoverReport_StartDate"];
                    endDate = (DateTime)this.Session["switchoverReport_EndDate"];

                    this.generateReport(startDate, endDate);
                }
                else
                {
                    RadGridSwitchoverReport.Visible = false;
                }
            }
        }

        private void generateReport(DateTime startDate, DateTime endDate)
        {
            BOAccountingControlWS boAccouningController = new BOAccountingControlWS();

            // Include end date in the calculation to create the report
            endDate = endDate.AddDays(1);

            if (HttpContext.Current.User.IsInRole("NOC Administrator"))
            {
                using (BOSatDiversityControllerWS boSatDiversityController = new BOSatDiversityControllerWS())
                {
                    //IEnumerable<SwitchOver> switchOvers = boSatDiversityController.GetAllSwitchOvers(startDate, endDate);
                    IEnumerable<SwitchOver> switchOvers = boSatDiversityController.GetAllSwitchOvers(startDate, endDate);
                    //CMTSUPPORT-142
                    //if (switchOvers != null && switchOvers.Count() != 0)
                    //{
                        RadGridSwitchoverReport.DataSource = switchOvers;
                        RadGridSwitchoverReport.Visible = true;
                    //}
                    //else
                    //{
                    //    RadGridSwitchoverReport.Visible = false;
                    //}
                }                
            }

            if (HttpContext.Current.User.IsInRole("Distributor"))
            {

                MembershipUser currentUser = Membership.GetUser();
                string userId = currentUser.ProviderUserKey.ToString();

                Distributor distributor = boAccouningController.GetDistributorForUser(userId);
                
                int distributorId = distributor.Id.Value;
                
                using (BOSatDiversityControllerWS boSatDiversityController = new BOSatDiversityControllerWS())
                {

                    //IEnumerable<SwitchOver> switchOvers = boSatDiversityController.GetAllSwitchOvers(startDate, endDate);
                    IEnumerable<SwitchOver> switchOvers = boSatDiversityController.GetSwitchOversForDistributor(startDate, endDate, distributorId);
                    //CMTSUPPORT 142
                    //if (switchOvers != null && switchOvers.Count() != 0)
                    //{
                        RadGridSwitchoverReport.DataSource = switchOvers;
                        RadGridSwitchoverReport.Visible = true;
                    //}
                    //else
                    //{
                    //    RadGridSwitchoverReport.Visible = false;
                    //}
                }
            }

            if (HttpContext.Current.User.IsInRole("Organization"))
            {
                MembershipUser currentUser = Membership.GetUser();
                string userId = currentUser.ProviderUserKey.ToString();

                Organization organization = boAccouningController.GetOrganizationForUser(userId);

                int organizationId = organization.Id.Value;

                using (BOSatDiversityControllerWS boSatDiversityController = new BOSatDiversityControllerWS())
                {
                    //IEnumerable<SwitchOver> switchOvers = boSatDiversityController.GetAllSwitchOvers(startDate, endDate);
                    IEnumerable<SwitchOver> switchOvers = boSatDiversityController.GetSwitchOversForCustomer(startDate, endDate, organizationId);
                    //CMTSUPPORT 142
                    //if (switchOvers != null && switchOvers.Count() != 0)
                    //{
                        RadGridSwitchoverReport.DataSource = switchOvers;
                        RadGridSwitchoverReport.Visible = true;
                    //}
                    //else
                    //{
                    //    RadGridSwitchoverReport.Visible = false;
                    //}
                }
            }

        }

        protected void ImageButtonCSVExport_Click(object sender, ImageClickEventArgs e)
        {
            RadGridSwitchoverReport.MasterTableView.ExportToCSV();
        }

        protected void ImageButtonPDFExport_Click(object sender, ImageClickEventArgs e)
        {
            DateTime startDate = (DateTime)RadDatePickerStart.SelectedDate;
            DateTime endDate = (DateTime)RadDatePickerEnd.SelectedDate;
            RadGridSwitchoverReport.ExportSettings.Pdf.Title = "Terminal Activities From " + startDate.Date + " To " + endDate.Date;
            RadGridSwitchoverReport.ExportSettings.Pdf.FontType = Telerik.Web.Apoc.Render.Pdf.FontType.Embed;
            RadGridSwitchoverReport.ExportSettings.Pdf.DefaultFontFamily = "Arial";
            RadGridSwitchoverReport.ExportSettings.Pdf.PaperSize = GridPaperSize.A4;
            RadGridSwitchoverReport.MasterTableView.ExportToPdf();
        }

        protected void ImageButtonExcelExport_Click(object sender, ImageClickEventArgs e)
        {
            RadGridSwitchoverReport.MasterTableView.ExportToExcel();
        }

        protected void RadGridSwitchoverReport_ItemCreated(object sender, GridItemEventArgs e)
        {
        }
    }
}