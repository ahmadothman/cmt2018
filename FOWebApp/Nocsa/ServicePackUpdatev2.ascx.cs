﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOMonitorControlWSRef;
using Telerik.Web.UI;

namespace FOWebApp.Nocsa
{
    public partial class ServicePackUpdatev2 : System.Web.UI.UserControl
    {
        //Boolean _newSla = false;
        BOAccountingControlWS _boAccountingControlWS = new BOAccountingControlWS();
        BOMonitorControlWS _boMonitorControlWS = new BOMonitorControlWS();

        protected void Page_Load(object sender, EventArgs e)
        {
            //Load the ISP list
            RadComboBoxISPId.Items.Clear();
            _boAccountingControlWS = new BOAccountingControlWS();
            Isp[] ispList = _boAccountingControlWS.GetIsps();
            RadComboBoxISPId.DataValueField = "Id";
            RadComboBoxISPId.DataTextField = "CompanyName";
            RadComboBoxISPId.DataSource = ispList;
            RadComboBoxISPId.DataBind();
            RadComboBoxISPId.Items.Insert(0, new RadComboBoxItem(""));

            //Load the VNO list
            RadComboBoxVNOId.Items.Clear();
            _boAccountingControlWS = new BOAccountingControlWS();
            Distributor[] VNOList = _boAccountingControlWS.GetVNOs();
            RadComboBoxVNOId.DataValueField = "Id";
            RadComboBoxVNOId.DataTextField = "FullName";
            RadComboBoxVNOId.DataSource = VNOList;
            RadComboBoxVNOId.DataBind();
            RadComboBoxVNOId.Items.Insert(0, new RadComboBoxItem(""));

            //Load the service lines
            RadComboBoxServiceLine.Items.Clear();
            string[] serviceLines = { "", "SoHo Services", "Business Services", "Corporate Services", "Specialized Tailored Services", "Multicast Service", "VNO Service" ,"Monthly SOHO"};
            int i = 0;
            foreach (string sl in serviceLines)
            {
                RadComboBoxItem myItem = new RadComboBoxItem();
                myItem.Text = sl;
                myItem.Value = i.ToString();
                RadComboBoxServiceLine.Items.Add(myItem);
                i++;
            }

            // hide VNO option and Hide-box for VNOs
            if (HttpContext.Current.User.IsInRole("VNO"))
            {
                RadComboBoxVNOId.Visible = false;
                LabelForVNO.Visible = false;
                LabelHide.Visible = false;
                CheckBoxHide.Visible = false;
            }

            if (Request.Params["id"] == null)
            {
                //Parameter passed
                //_newSla = true;

                // automatically set VNO value if user is VNO
                if (HttpContext.Current.User.IsInRole("VNO"))
                {
                    MembershipUser myObject = Membership.GetUser();
                    string userId = myObject.ProviderUserKey.ToString();
                    RadComboBoxVNOId.SelectedValue = _boAccountingControlWS.GetDistributorForUser(userId).Id.ToString();
                }

                //RadComboBoxEdgeISPId.Enabled = false;
                //RadComboBoxEdgeSLA.Enabled = false;
                buttonSubmit.CommandName = "Insert";

                ButtonDelete.Visible = false;
            }
            else //disable editing for critical fields for existing SLA
            {
                //_newSla = false;
                TextBoxId.Enabled = false;
                RadComboBoxISPId.Enabled = false;
                RadNumericTextBoxFUPThreshold.Enabled = false;
                RadNumericTextBoxRTNFUPThreshold.Enabled = false;
                RadNumericTextBoxForwardAboveFup.Enabled = false;
                RadNumericTextBoxReturnAboveFup.Enabled = false;
                RadNumericTextBoxForwardDefault.Enabled = true;
                Label6.Visible = true;
                RadNumericTextBoxReturnDefault.Enabled = true;
                Label7.Visible = true;
                RadNumericTextBoxCIRFWD.Enabled = true;
                Label8.Visible = true;
                RadNumericTextBoxCIRRTN.Enabled = true;
                Label9.Visible = true;
                RadNumericTextBoxMinBWFWD.Enabled = false;
                RadNumericTextBoxMinBWRTN.Enabled = false;
                RadioButtonVolAgg.Enabled = false;
                RadioButtonVolSep.Enabled = false;
                RadNumericTextBoxLowVolFWD.Enabled = true;
                RadNumericTextBoxLowVolRTN.Enabled = true;
                RadNumericTextBoxLowVolSUM.Enabled = true;
                RadNumericTextBoxHighVolFWD.Enabled = true;
                RadNumericTextBoxHighVolRTN.Enabled = true;
                RadNumericTextBoxHighVolSUM.Enabled = true;
                RadNumericTextBoxWeight.Enabled = true;
                Label5.Visible = true;
                RadComboBoxServiceLine.Enabled = false;
                CheckBoxVoIP.Enabled = false;
                CheckBoxFreeZone.Enabled = false;

                int slaId = int.Parse(Request.QueryString["id"]);

                this.LoadForm(slaId);

                //disable submit button if VNO isn't owner of the SLA
                // automatically set VNO value if user is VNO
                if (HttpContext.Current.User.IsInRole("VNO"))
                {
                    MembershipUser myObject = Membership.GetUser();
                    string userId = myObject.ProviderUserKey.ToString();
                    string vnoId = _boAccountingControlWS.GetDistributorForUser(userId).Id.ToString();
                    if (RadComboBoxVNOId.SelectedValue.ToString() != vnoId)
                    {
                        buttonSubmit.Enabled = false;
                    }
                }
                if (_boAccountingControlWS.GetTerminalsBySla(slaId).Length < 1
                    && !_boMonitorControlWS.NMSSlaHasAssociatedTerminals(slaId, int.Parse(RadComboBoxISPId.SelectedValue)))
                {
                    ButtonDelete.CommandArgument = "True";
                    ButtonDelete.Enabled = true;
                }
            }
        }

        protected void CustomValidatorId_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (Request.Params["id"] == null && _boAccountingControlWS.GetServicePack(int.Parse(args.Value)) != null)
            {
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }
        }

        protected void buttonSubmit_Command(object sender, CommandEventArgs e)
        {
            if (Page.IsValid) // Needed, because otherwise the SLA is updated even when the CustomValidator for the ID sets the args.IsValid on false
            {
                //Create a new Service pack
                ServiceLevel serviceLevel = new ServiceLevel();
                serviceLevel.SlaId = Int32.Parse(TextBoxId.Text);
                serviceLevel.SlaName = TextBoxName.Text;
                serviceLevel.FupThreshold = (decimal)RadNumericTextBoxFUPThreshold.Value;
                serviceLevel.RTNFUPThreshold = (decimal)RadNumericTextBoxRTNFUPThreshold.Value;
                serviceLevel.DRFWD = (int)RadNumericTextBoxForwardDefault.Value;
                serviceLevel.DRRTN = (int)RadNumericTextBoxReturnDefault.Value;
                if (RadNumericTextBoxForwardAboveFup.Text != "" && RadNumericTextBoxReturnAboveFup.Text != "")
                    serviceLevel.DrAboveFup = RadNumericTextBoxForwardAboveFup.Text + "/" + RadNumericTextBoxReturnAboveFup.Text;
                else
                    serviceLevel.DrAboveFup = "0/0" ;
                serviceLevel.CIRFWD = (int)RadNumericTextBoxCIRFWD.Value;
                serviceLevel.CIRRTN = (int)RadNumericTextBoxCIRRTN.Value;
                serviceLevel.SlaCommonName = TextBoxSlaCommonName.Text;
                serviceLevel.IspId = Int32.Parse(RadComboBoxISPId.SelectedValue);
                serviceLevel.SLAWeight = (double)RadNumericTextBoxWeight.Value;
                serviceLevel.ServiceClass = Int32.Parse(RadComboBoxServiceLine.SelectedValue);
                if (serviceLevel.ServiceClass == 1 || serviceLevel.ServiceClass == 7)
                {
                    serviceLevel.Voucher = true;
                    serviceLevel.Business = false;
                }
                else if (serviceLevel.ServiceClass == 2)
                {
                    serviceLevel.Business = true;
                    serviceLevel.Voucher = false;
                }
                else if (serviceLevel.ServiceClass == 3)
                {
                    serviceLevel.Business = false;
                    serviceLevel.Voucher = false;
                    if ((serviceLevel.SLAWeight<=0) &&(serviceLevel.DRFWD != 0) && (serviceLevel.CIRFWD != 0))
                    {
                        int contention = (int)serviceLevel.DRFWD / serviceLevel.CIRFWD;
                        switch (contention)
                        {
                            case 1:
                                serviceLevel.SLAWeight = 100;
                                break;
                            case 2:
                                serviceLevel.SLAWeight = 53.23;
                                break;
                            case 4:
                                serviceLevel.SLAWeight = 28.32;
                                break;
                            case 8:
                                serviceLevel.SLAWeight = 16.93;
                                break;
                            case 10:
                                serviceLevel.SLAWeight = 15;
                                break;
                            case 15:
                                serviceLevel.SLAWeight = 13.04;
                                break;
                            case 20:
                                serviceLevel.SLAWeight = 12.69;
                                break;
                            case 25:
                                serviceLevel.SLAWeight = 12.49;
                                break;
                            default:
                                serviceLevel.SLAWeight = 12.49;
                                break;
                        }
                    }
                }
                

                serviceLevel.FreeZone = CheckBoxFreeZone.Checked;
                serviceLevel.VoIPFlag = CheckBoxVoIP.Checked;
                serviceLevel.Hide = CheckBoxHide.Checked;
                if (RadComboBoxVNOId.SelectedValue.ToString() != "" && RadComboBoxVNOId.SelectedValue.ToString() != null)
                {
                    serviceLevel.VNOId = Int32.Parse(RadComboBoxVNOId.SelectedValue);
                }

                /*if (Page.IsValid)
                {
                    serviceLevel.VNOId = Int32.Parse(RadComboBoxVNOId.SelectedValue);
                }*/

                /*serviceLevel.ServicePackAmt = (float)RadNumericTextBoxServicePackAmt.Value;
                serviceLevel.FreeZoneAmt = (float)RadNumericTextBoxFreeZoneAmt.Value;
                serviceLevel.VoIPAmt = (float)Double.Parse(RadNumericTextBoxVoIPAmt.Text);
                serviceLevel.ServicePackAmtEUR = (float)RadNumericTextBoxServicePackAmtEUR.Value;
                serviceLevel.FreeZoneAmtEUR = (float)RadNumericTextBoxFreeZoneAmtEUR.Value;
                serviceLevel.VoIPAmtEUR = (float)Double.Parse(RadNumericTextBoxVoIPAmtEUR.Text);*/
                serviceLevel.MinBWFWD = Convert.ToInt32(RadNumericTextBoxMinBWFWD.Text);
                serviceLevel.MinBWRTN = Convert.ToInt32(RadNumericTextBoxMinBWRTN.Text);
               // serviceLevel.SLAWeight = (double)RadNumericTextBoxWeight.Value;

                if (RadioButtonVolAgg.Checked)
                {
                    serviceLevel.HighVolumeSUM = (decimal)RadNumericTextBoxHighVolSUM.Value;
                    serviceLevel.HighVolumeFWD = (decimal)RadNumericTextBoxHighVolSUM.Value;
                    serviceLevel.HighVolumeRTN = (decimal)RadNumericTextBoxHighVolSUM.Value;
                    serviceLevel.LowVolumeSUM = (decimal)RadNumericTextBoxLowVolSUM.Value;
                    serviceLevel.LowVolumeFWD = (decimal)RadNumericTextBoxLowVolSUM.Value;
                    serviceLevel.LowVolumeRTN = (decimal)RadNumericTextBoxLowVolSUM.Value;
                    serviceLevel.AggregateFlag = true;
                }
                else if (RadioButtonVolSep.Checked)
                {
                    serviceLevel.HighVolumeFWD = (decimal)RadNumericTextBoxHighVolFWD.Value;
                    serviceLevel.HighVolumeRTN = (decimal)RadNumericTextBoxHighVolRTN.Value;
                    serviceLevel.HighVolumeSUM = serviceLevel.HighVolumeFWD + serviceLevel.HighVolumeRTN;
                    serviceLevel.LowVolumeFWD = (decimal)RadNumericTextBoxLowVolFWD.Value;
                    serviceLevel.LowVolumeRTN = (decimal)RadNumericTextBoxLowVolRTN.Value;
                    serviceLevel.LowVolumeSUM = serviceLevel.LowVolumeFWD + serviceLevel.LowVolumeRTN;
                    serviceLevel.AggregateFlag = false;
                }

                //If the EdgeISPId textbox is enabled, this is an SLA for a secondary ISP
                if (RadTextBoxEdgeISPId.Enabled)
                {
                    serviceLevel.EdgeISP = Int32.Parse(RadTextBoxEdgeISPId.Text);

                    if (!string.IsNullOrWhiteSpace(RadTextBoxEdgeSLA.Text))
                    {
                        serviceLevel.EdgeSLA = Int32.Parse(RadTextBoxEdgeSLA.Text);
                    }
                    
                }
                serviceLevel.BillableId = (int?)RadNumericTextBoxBId.Value;

                //The SerivcePack class is used for the NMS
                ServicePack servicePack = this.serviceLevelToServicePack(serviceLevel);

                
                if (_boMonitorControlWS.NMSUpdateServicePack(servicePack)) //will return true for all non-NMS SLA's as well
                {
                    if (_boAccountingControlWS.UpdateServicePack(serviceLevel))
                    {
                        LabelResult.ForeColor = Color.DarkGreen;
                        LabelResult.Text = "Servicepack update succeeded";
                    }
                    else
                    {
                        LabelResult.ForeColor = Color.Red;
                        LabelResult.Text = "Servicepack update failed";
                    }
                }
                else
                {
                    LabelResult.ForeColor = Color.Red;
                    LabelResult.Text = "Servicepack update failed";
                }
            }
        }

        private ServicePack serviceLevelToServicePack(ServiceLevel serviceLevel)
        {
            ServicePack servicePack = new ServicePack();

            servicePack.SlaId = serviceLevel.SlaId;
            servicePack.SlaName = serviceLevel.SlaName;
            servicePack.SlaCommonName = serviceLevel.SlaCommonName;
            servicePack.IspId = serviceLevel.IspId;
            servicePack.ServiceClass = serviceLevel.ServiceClass;
            //all NMS speeds must be transformed from kbps to bits/sec
            servicePack.DRFWD = serviceLevel.DRFWD * 1000;
            servicePack.DRRTN = serviceLevel.DRRTN * 1000;
            servicePack.CIRFWD = serviceLevel.CIRFWD * 1000;
            servicePack.CIRRTN = serviceLevel.CIRRTN * 1000;
            servicePack.MinBWFWD = serviceLevel.MinBWFWD * 1000;
            servicePack.MinBWRTN = serviceLevel.MinBWRTN * 1000;
            //NMS volumes must be transformed from GB to bytes
            servicePack.HighVolumeSUM = Convert.ToInt64(serviceLevel.HighVolumeSUM * 1000000000);
            servicePack.LowVolumeSUM = Convert.ToInt64(serviceLevel.LowVolumeSUM * 1000000000);
            servicePack.Weight = serviceLevel.SLAWeight;


            return servicePack;
        }

        /// <summary>
        /// This method loads the SLA data into the form
        /// </summary>
        /// <param name="slaId">The identifier of the SLA to load</param>
        private void LoadForm(int slaId)
        {
            ServiceLevel sla = _boAccountingControlWS.GetServicePack(slaId);
            TextBoxId.Text = sla.SlaId.ToString();
            TextBoxName.Text = sla.SlaName.ToString().Trim();
            RadNumericTextBoxFUPThreshold.Value = (double)sla.FupThreshold;
            RadNumericTextBoxRTNFUPThreshold.Value = (double)sla.RTNFUPThreshold;
            RadNumericTextBoxForwardDefault.Value = (double)sla.DRFWD;
            RadNumericTextBoxReturnDefault.Value = (double)sla.DRRTN;
            String[] DRAboveFUPTokens = sla.DrAboveFup.Split('/');
            if (DRAboveFUPTokens.Length == 2)
            {
                RadNumericTextBoxForwardAboveFup.Value = Double.Parse(DRAboveFUPTokens[0]);
                RadNumericTextBoxReturnAboveFup.Value = Double.Parse(DRAboveFUPTokens[1]);
            }
            else
            {
                RadNumericTextBoxForwardAboveFup.Value = 0;
                RadNumericTextBoxReturnAboveFup.Value = 0;
            }
            RadNumericTextBoxCIRFWD.Value = (double)sla.CIRFWD;
            RadNumericTextBoxCIRRTN.Value = (double)sla.CIRRTN;
            TextBoxSlaCommonName.Text = sla.SlaCommonName.Trim();
            RadComboBoxISPId.SelectedValue = sla.IspId.ToString();
            RadComboBoxVNOId.SelectedValue = sla.VNOId.ToString();
            CheckBoxFreeZone.Checked = (bool)sla.FreeZone;
            CheckBoxVoIP.Checked = (bool)sla.VoIPFlag;
            CheckBoxHide.Checked = (bool)sla.Hide;
            RadComboBoxServiceLine.SelectedValue = sla.ServiceClass.ToString();
            RadNumericTextBoxWeight.Value = sla.SLAWeight;

            //Check if the ISP is an edge ISP.
            Isp isp = _boAccountingControlWS.GetISP((int)sla.IspId);
            if (isp.ISPType == 1)
            {
                //No need to set an edge ISP and edge SLA
                RadTextBoxEdgeISPId.Enabled = false;
                RadTextBoxEdgeSLA.Enabled = false;
            }
            else
            {
                RadTextBoxEdgeISPId.Text = sla.EdgeISP.ToString();
                RadTextBoxEdgeSLA.Text = sla.EdgeSLA.ToString();
            }

            if (sla.AggregateFlag)
            {
                RadioButtonVolAgg.Checked = true;
                RadioButtonVolSep.Checked = false;
            }
            else
            {
                RadioButtonVolAgg.Checked = false;
                RadioButtonVolSep.Checked = true;
            }

            if (sla.HighVolumeFWD != null)
            {
                RadNumericTextBoxHighVolFWD.Value = (double)sla.HighVolumeFWD;
            }
            if (sla.HighVolumeRTN != null)
            {
                RadNumericTextBoxHighVolRTN.Value = (double)sla.HighVolumeRTN;
            }
            if (sla.HighVolumeSUM != null)
            {
                RadNumericTextBoxHighVolSUM.Value = (double)sla.HighVolumeSUM;
            }
            if (sla.LowVolumeFWD != null)
            {
                RadNumericTextBoxLowVolFWD.Value = (double)sla.LowVolumeFWD;
            }
            if (sla.LowVolumeRTN != null)
            {
                RadNumericTextBoxLowVolRTN.Value = (double)sla.LowVolumeRTN;
            }
            if (sla.LowVolumeSUM != null)
            {
                RadNumericTextBoxLowVolSUM.Value = (double)sla.LowVolumeSUM;
            }
            if (sla.MinBWFWD != null)
            {
                RadNumericTextBoxMinBWFWD.Value = sla.MinBWFWD;
            }
            if (sla.MinBWRTN != null)
            {
                RadNumericTextBoxMinBWRTN.Value = sla.MinBWRTN;
            }
            RadNumericTextBoxBId.Value = sla.BillableId;
        }

      

        protected void ButtonDelete_Command(object sender, CommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Delete":
                    int slaId;

                    if (int.TryParse(Request.QueryString["id"], out slaId))
                    {
                        if (e.CommandArgument == "True")
                        {
                            if (_boMonitorControlWS.NMSDeleteServicePack(slaId, int.Parse(RadComboBoxISPId.SelectedValue)))
                            {
                                if (_boAccountingControlWS.DeleteServicePack(slaId))
                                {
                                    LabelResult.ForeColor = Color.DarkGreen;
                                    LabelResult.Text = "SLA successfully deleted";
                                }
                                else
                                {
                                    LabelResult.ForeColor = Color.Red;
                                    LabelResult.Text = "Deletion of SLA failed";
                                }
                            }
                            else
                            {
                                LabelResult.ForeColor = Color.Red;
                                LabelResult.Text = "Deletion of SLA failed";
                            }
                        }
                        else
                        {
                            LabelResult.ForeColor = Color.Red;
                            LabelResult.Text = "This SLA has still associated terminals";
                        }
                    }

                    break;
                default:
                    throw new NotImplementedException();
            }
        }
    }
}