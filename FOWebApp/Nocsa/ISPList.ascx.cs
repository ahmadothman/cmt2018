﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FOWebApp.Data;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;

namespace FOWebApp.Nocsa
{
    public partial class ISPList : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Read the ISPs from the database
            DataHelper dataHelper = new DataHelper();
            List<Isp> ispList = dataHelper.getISPs();
            foreach (Isp isp in ispList)
            {
                if (isp.Satellite.ID != 0)
                {
                    BOAccountingControlWS _boAccountingController = new BOAccountingControlWS();
                    Satellite[] satellites = _boAccountingController.GetSatellites();
                    foreach (Satellite satellite in satellites)
                    {
                        if (satellite.ID == isp.Satellite.ID)
                        {
                            isp.Satellite = satellite;
                            break;
                        }
                    }
                }
                //else
                //{
                //    isp.Satellite.SatelliteName = "";
                //}
            }
            RadGridISPList.DataSource = ispList;
            RadGridISPList.DataBind();
            // Add method to add satellite to the grid
        }
    }
}