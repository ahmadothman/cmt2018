﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using Telerik.Web.UI;

namespace FOWebApp.Nocsa
{
    public partial class ISPDetails : System.Web.UI.UserControl
    {
        BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();
        Satellite[] satelliteList;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack && Session["NewISP"].Equals(true))
            {
                //SDPDEV-23
                Session["NewISP"] = false;
                satelliteList = boAccountingControl.GetSatellites();
                foreach (Satellite sat in satelliteList)
                {
                    RadComboBoxSatellite.Items.Add(new RadComboBoxItem(sat.SatelliteName, sat.ID.ToString()));
                    if (sat.SatelliteName == "Unknown")
                    {
                        RadComboBoxSatellite.SelectedValue = sat.ID.ToString();
                    }
                }
            }
        }

        protected void ButtonSubmit_Click(object sender, EventArgs e)
        {
            LabelResult.Visible = false;

            //Read data from controls and update
            try
            {
                boAccountingControl =
                             new BOAccountingControlWS();


                Isp isp = new Isp();
                Address address = new Address();
                isp.Address = address;
                isp.Id = Int32.Parse(TextBoxISPID.Text);
                isp.CompanyName = TextBoxISPName.Text;
                isp.Address.AddressLine1 = TextBoxAddressLine1.Text;
                isp.Address.AddressLine2 = TextBoxAddressLine2.Text;
                isp.Address.Location = TextBoxLocation.Text;
                isp.Address.Country = CountryList.SelectedValue;
                isp.Address.PostalCode = TextBoxPostCode.Text;
                isp.Phone = TextBoxPhone.Text;
                isp.Fax = TextBoxFax.Text;
                isp.Email = TextBoxEMail.Text;
                
                ////SDPDEV-23
                foreach (Satellite sat in satelliteList)
                {
                    if (RadComboBoxSatellite.SelectedValue == sat.ID.ToString())
                    {
                        isp.Satellite = sat;
                    }
                }


                isp.Ispif_Password = TextBoxISPIFPassword.Text;
                isp.Ispif_User = TextBoxISPIFUserName.Text;
                isp.Dashboard_Password = TextBoxDashPassword.Text;
                isp.Dashboard_User = TextBoxDashUserName.Text;
                isp.ISPType = Int32.Parse(RadComboBoxISPType.SelectedValue);

                if (boAccountingControl.UpdateIsp(isp))
                {
                    LabelResult.ForeColor = Color.Green;
                    LabelResult.Text = "ISP creation/update successfully";
                }
                else
                {
                    LabelResult.ForeColor = Color.Red;
                    LabelResult.Text = "Could not create ISP!";
                }

            }
            catch (Exception ex)
            {
                LabelResult.ForeColor = Color.Red;
                LabelResult.Text = ex.Message;
            }

            LabelResult.Visible = true;
        }

        /// <summary>
        /// Initializes the form and loads the data for the 
        /// given isp id into the form fields.
        /// </summary>
        /// <param name="ispId">ISP ID</param>
        public void initForm(int ispId)
        {
            BOAccountingControlWS boAccountingControl =
                                            new BOAccountingControlWS();

            Isp isp = boAccountingControl.GetISP(ispId);

            if (isp != null)
            {
                TextBoxISPID.Text = isp.Id.ToString();
                TextBoxISPName.Text = isp.CompanyName;
                TextBoxAddressLine1.Text = isp.Address.AddressLine1;
                TextBoxAddressLine2.Text = isp.Address.AddressLine2;

                CountryList.SelectedValue = isp.Address.Country;

                TextBoxLocation.Text = isp.Address.Location;
                TextBoxEMail.Text = isp.Email;
                TextBoxPostCode.Text = isp.Address.PostalCode;
                TextBoxFax.Text = isp.Fax;
                TextBoxPhone.Text = isp.Phone;
                RadComboBoxISPType.SelectedValue = isp.ISPType.ToString();

                if (isp.Ispif_User != null)
                {
                    TextBoxISPIFUserName.Text = isp.Ispif_User.Trim();
                }

                if (isp.Ispif_Password != null)
                {
                    TextBoxISPIFPassword.Text = isp.Ispif_Password.Trim();
                }

                if (isp.Dashboard_User != null)
                {
                    TextBoxDashUserName.Text = isp.Dashboard_User.Trim();
                }

                if (isp.Dashboard_Password != null)
                {
                    TextBoxDashPassword.Text = isp.Dashboard_Password.Trim();
                }
                ////SDPDEV23
                satelliteList = boAccountingControl.GetSatellites();
                foreach (Satellite sat in satelliteList)
                {
                    RadComboBoxItem listItem = new RadComboBoxItem(sat.SatelliteName, sat.ID.ToString());
                    RadComboBoxSatellite.Items.Add(listItem);
                    if (isp.Satellite.SatelliteName == sat.SatelliteName)
                    {
                        RadComboBoxSatellite.SelectedValue = listItem.Value;
                    }
                }
            }
        }
    }
}