﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NewFreeZone.ascx.cs" Inherits="FOWebApp.Nocsa.NewFreeZone" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<telerik:RadAjaxPanel ID="RadAjaxPanelCreateFreeZone" runat="server" Height="1600px"
    Width="100%" LoadingPanelID="RadAjaxLoadingPanelCreateFreeZone">
    <h2>Create a new free zone</h2>
    <table cellpadding="10" cellspacing="5">
        <tr>
            <td class="requiredLabel">
                ID:
            </td>
            <td>
                <asp:TextBox ID="TextBoxId" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="requiredLabel">
                Name:
            </td>
            <td colspan="2">
                <asp:TextBox ID="TextBoxName" runat="server" Width="300px"></asp:TextBox>
            </td>
        </tr>
        <%--<tr>
            <td>Active:</td>
            <td>
                <asp:CheckBox ID="CheckBoxActive" runat="server" />
            </td>
        </tr>--%>
        <tr>
            <td></td>
            <td>Free zone ends</td>
            <td>Free zone starts</td>
        </tr>
        <tr>
            <td>Monday</td>
            <td><telerik:RadMaskedTextBox ID="TextBoxMonOff" runat="server" Width="100px"></telerik:RadMaskedTextBox></td>
            <td><telerik:RadMaskedTextBox ID="TextBoxMonOn" runat="server" Width="100px"></telerik:RadMaskedTextBox></td>
        </tr>
        <tr>
            <td>Tuesday</td>
            <td><telerik:RadMaskedTextBox ID="TextBoxTueOff" runat="server" Width="100px"></telerik:RadMaskedTextBox></td>
            <td><telerik:RadMaskedTextBox ID="TextBoxTueOn" runat="server" Width="100px"></telerik:RadMaskedTextBox></td>
        </tr>
        <tr>
            <td>Wednesday</td>
            <td><telerik:RadMaskedTextBox ID="TextBoxWedOff" runat="server" Width="100px"></telerik:RadMaskedTextBox></td>
            <td><telerik:RadMaskedTextBox ID="TextBoxWedOn" runat="server" Width="100px"></telerik:RadMaskedTextBox></td>
        </tr>
        <tr>
            <td>Thursday</td>
            <td><telerik:RadMaskedTextBox ID="TextBoxThuOff" runat="server" Width="100px"></telerik:RadMaskedTextBox></td>
            <td><telerik:RadMaskedTextBox ID="TextBoxThuOn" runat="server" Width="100px"></telerik:RadMaskedTextBox></td>
        </tr>
        <tr>
            <td>Friday</td>
            <td><telerik:RadMaskedTextBox ID="TextBoxFriOff" runat="server" Width="100px"></telerik:RadMaskedTextBox></td>
            <td><telerik:RadMaskedTextBox ID="TextBoxFriOn" runat="server" Width="100px"></telerik:RadMaskedTextBox></td>
        </tr>
        <tr>
            <td>Saturday</td>
            <td><telerik:RadMaskedTextBox ID="TextBoxSatOff" runat="server" Width="100px"></telerik:RadMaskedTextBox></td>
            <td><telerik:RadMaskedTextBox ID="TextBoxSatOn" runat="server" Width="100px"></telerik:RadMaskedTextBox></td>
        </tr>
        <tr>
            <td>Sunday</td>
            <td><telerik:RadMaskedTextBox ID="TextBoxSunOff" runat="server" Width="100px"></telerik:RadMaskedTextBox></td>
            <td><telerik:RadMaskedTextBox ID="TextBoxSunOn" runat="server" Width="100px"></telerik:RadMaskedTextBox></td>
        </tr>
    </table>
    <table>
        <tr>
            <td>
                <asp:Button ID="ButtonSumbit" runat="server" Text="Submit" OnClick="ButtonSubmit_Click" />
            </td>
            <td>
                <asp:Label ID="LabelResult" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
<p>
     All times in GMT.
</p>
</telerik:RadAjaxPanel>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanelCreateFreeZone" runat="server"
    Skin="Metro">
</telerik:RadAjaxLoadingPanel>
<telerik:RadWindowManager ID="RadWindowManager1" runat="server" Skin="Metro" EnableShadow="True"
    Left="0px" Right="0px" Modal="True" VisibleStatusbar="False">
</telerik:RadWindowManager>