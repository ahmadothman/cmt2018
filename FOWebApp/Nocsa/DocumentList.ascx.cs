﻿using System;
using System.Linq;
using FOWebApp.net.nimera.cmt.BODocumentControllerWSRef;
using Telerik.Web.UI;

namespace FOWebApp.Nocsa
{
    public partial class DocumentList : System.Web.UI.UserControl
    {
        BODocumentControllerWS _documentController = null;
        

        protected void Page_Load(object sender, EventArgs e)
        {
            _documentController = new BODocumentControllerWS();
        }

        protected void RadGridDocuments_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            //Retrieve the documents from the database
            Document[] documents = _documentController.GetAllDocuments();
            RadGridDocuments.DataSource = documents;
        }

        protected void RadGridDocuments_DeleteCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        { 
            //Get the documentId
            GridDataKeyArray dataItems = e.Item.OwnerTableView.DataKeyValues;
            long docId = (long)dataItems[e.Item.ItemIndex]["Id"];

            //Delete the document from the database
            _documentController.DeleteDocument(docId);
            
            //Find a way to automatically refresh the table only
            labelDeleteMessage.InnerText = "Document deleted. Refresh the table to view the new list.";
        }
    }
}