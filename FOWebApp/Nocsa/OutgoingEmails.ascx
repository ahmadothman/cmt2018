﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OutgoingEmails.ascx.cs" Inherits="FOWebApp.Nocsa.OutgoingEmails" %>
<style type="text/css">
    .auto-style2 {
        width: 102%;
    }
</style>
<table cellpadding="10" cellspacing="5">
    <tr>
        <td><b><asp:label ID="LabelHeader" runat="server"></asp:label></b></td>
    </tr>
    <tr>
        <td>
            <asp:GridView ID="GridViewEmailList" AutoGenerateColumns="True" runat="server">
            </asp:GridView>
        </td>
    </tr>
</table>