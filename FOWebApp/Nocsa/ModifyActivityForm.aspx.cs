﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FOWebApp.Nocsa
{
    public partial class ModifyActivityForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                nimModifyActivity.NewFlag = false;
                string activityId = Request.Params["Id"];

                if (activityId != null)
                {
                    nimModifyActivity.ActivityId = Int32.Parse(activityId);
                }
            }
        }
    }
}