﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VoucherVolumeList.ascx.cs" Inherits="FOWebApp.Nocsa.VoucherVolumeList" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script type="text/javascript">
    function RowSelected(sender, eventArgs) {
        var grid = sender;
        var masterTable = grid.get_masterTableView();
        var dataItems = masterTable.get_selectedItems();
        if (dataItems.length != 0) {
            var id = dataItems[0].get_element().cells[0].innerHTML;
            var oWnd = radopen("Nocsa/VoucherVolumeDetailsPage.aspx?id=" + id, "RadWindowVoucherVolumeDetails");
        }
    }
</script>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="RadGridVoucherVolumes">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGridVoucherVolumes" />
                <telerik:AjaxUpdatedControl ControlID="RadWindowManager1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Metro"/>
<telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" Height="200px" 
    Width="467px" HorizontalAlign="NotSet" 
    LoadingPanelID="RadAjaxLoadingPanel1">
<telerik:RadGrid ID="RadGridVoucherVolumes" runat="server" AllowSorting="True"
    AutoGenerateColumns="False" CellSpacing="0" GridLines="None" Skin="Metro"
    ShowStatusBar="True" Width="992px" AllowFilteringByColumn="False" 
        onitemdatabound="RadGridVoucherVolumes_ItemDataBound" 
        onpagesizechanged="RadGridVoucherVolumes_PageSizeChanged" 
        PageSize="50" onitemcommand="RadGridVoucherVolumes_ItemCommand">
        <GroupingSettings CaseSensitive="false" /> 
    <ClientSettings EnableRowHoverStyle="true">
        <Selecting AllowRowSelect="True" />
        <ClientEvents OnRowSelected="RowSelected" />
    </ClientSettings>
    <MasterTableView CommandItemDisplay="Top" CommandItemSettings-ShowExportToExcelButton="False" 
        CommandItemSettings-ShowExportToCsvButton="False" RowIndicatorColumn-Visible="True" RowIndicatorColumn-Resizable="True" RowIndicatorColumn-AndCurrentFilterFunction="Contains" CommandItemSettings-ShowAddNewRecordButton="False" ShowFooter="False">
        <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
        </RowIndicatorColumn>
        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
        </ExpandCollapseColumn>
        <Columns>
            <telerik:GridBoundColumn DataField="Id"
                FilterControlAltText="Filter IdColumn column" HeaderText="Volume ID" 
                UniqueName="Key" ShowFilterIcon="False" AutoPostBackOnFilter="True">
                <ItemStyle Wrap="True" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Description"
                FilterControlAltText="Filter DescriptionColumn column" HeaderText="Description" 
                ShowFilterIcon="False" AutoPostBackOnFilter="True">
                <ItemStyle Wrap="True" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="VolumeMB"
                FilterControlAltText="Filter VolumeMBColumn column" HeaderText="Volume (MB)" 
                ShowFilterIcon="False" AutoPostBackOnFilter="True">
                <ItemStyle Wrap="True" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="UnitPriceEUR"
                FilterControlAltText="Filter UnitPriceEURColumn column" HeaderText="Unit price EUR" 
                ShowFilterIcon="False" AutoPostBackOnFilter="True" DataFormatString="{0:0.00}">
                <ItemStyle Wrap="True" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="UnitPriceUSD"
                FilterControlAltText="Filter UnitPriceUSDColumn column" HeaderText="Unit price USD" 
                ShowFilterIcon="False" AutoPostBackOnFilter="True" DataFormatString="{0:0.00}">
                <ItemStyle Wrap="True" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="ValidityPeriod"
                FilterControlAltText="Filter ValidityPeriodColumn column" HeaderText="Validity (days)" 
                ShowFilterIcon="False" AutoPostBackOnFilter="True">
                <ItemStyle Wrap="True" />
            </telerik:GridBoundColumn>            
        </Columns>
        <EditFormSettings>
            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
            </EditColumn>
        </EditFormSettings>
    </MasterTableView>
    <FilterMenu EnableImageSprites="False">
    </FilterMenu>
    <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default">
    </HeaderContextMenu>
</telerik:RadGrid>
<telerik:RadWindowManager ID="RadWindowManager1" runat="server" Skin="Metro" 
        DestroyOnClose="True">
    <Windows>
        <telerik:RadWindow ID="RadWindowVoucherVolumeDetails" runat="server" 
            NavigateUrl="NOCSA/VoucherVolumeDetailsPage.aspx" Animation="Fade" Opacity="100" Skin="Metro" 
            Title="Free Zone Details" AutoSize="False" Width="1000px" Height="700px" KeepInScreenBounds="True" EnableShadow="True" EnableAriaSupport="True" VisibleStatusbar="True" Modal="False" DestroyOnClose="False">
        </telerik:RadWindow>
    </Windows>
</telerik:RadWindowManager>
</telerik:RadAjaxPanel>