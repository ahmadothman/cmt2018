﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using Telerik.Web.UI;
using FOWebApp.Data;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;

namespace FOWebApp.Nocsa
{
    public partial class CustomerDetailsForm : System.Web.UI.Page
    {
        BOAccountingControlWS boAccountingControl;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //Read the Organization details from the database and fill out the form
                string strOrgId = Request.Params["id"];
                int orgId = Int32.Parse(strOrgId);

                LabelId.Text = strOrgId;

                boAccountingControl =
                  new net.nimera.cmt.BOAccountingControlWSRef.BOAccountingControlWS();

                Organization organization = boAccountingControl.GetOrganization(orgId);

                DataHelper dataHelper = new DataHelper();
                //Load the Distributor combobox
                Distributor[] distributors = boAccountingControl.GetDistributorsAndVNOs();

                foreach (Distributor distributor in distributors)
                {
                    RadComboBoxItem item = new RadComboBoxItem();
                    item.Value = distributor.Id.ToString();
                    item.Text = distributor.FullName;
                    RadComboBoxDistributor.Items.Add(item);
                }
                RadComboBoxDistributor.SelectedValue = "" + organization.Distributor.Id;


                string[] Sectors = { "","Financial", "Government","Radio","eLearning","Posts Offices","Religious","Other" };
                int j = 0;
                foreach (string dt in Sectors)
                {
                    RadComboBoxItem myItem = new RadComboBoxItem();
                    myItem.Text = dt;
                    myItem.Value = j.ToString();
                    RadComboBoxSector.Items.Add(myItem);
                    j++;
                }

                RadComboBoxSector.SelectedValue = organization.Sector.ToString();

                TextBoxAddressLine1.Text = organization.Address.AddressLine1;
                TextBoxAddressLine2.Text = organization.Address.AddressLine2;
                TextBoxEMail.Text = organization.Email;
                TextBoxFax.Text = organization.Fax;
                TextBoxLocation.Text = organization.Address.Location;
                TextBoxName.Text = organization.FullName;
                TextBoxPCO.Text = organization.Address.PostalCode;
                TextBoxPhone.Text = organization.Phone;
                TextBoxRemarks.Text = organization.Remarks;
                TextBoxVAT.Text = organization.Vat;
                if (organization.VNO != null)
                {
                    if ((bool)organization.VNO)
                    {
                        CheckBoxVNO.Checked = true;
                    }
                }
                if (organization.MIR != null)
                {
                    RadNumericTextBoxMIR.Value = organization.MIR;
                }
                if (organization.CIR != null)
                {
                    RadNumericTextBoxCIR.Value = organization.CIR;
                }
                TextBoxWebSite.Text = organization.WebSite;
                CountryList.SelectedValue = organization.Address.Country;

                RadComboBoxISP.Items.Clear();
                Isp[] ispList = boAccountingControl.GetIsps();
                RadComboBoxISP.DataValueField = "Id";
                RadComboBoxISP.DataTextField = "CompanyName";
                RadComboBoxISP.DataSource = ispList;
                RadComboBoxISP.DataBind();

                ServiceLevel[] slaList = boAccountingControl.GetServicePacks();
                //RadComboBoxISP.Items.Insert(0, new RadComboBoxItem(""));
                RadComboBoxDefaultSLA.DataValueField = "SlaID";
                RadComboBoxDefaultSLA.DataTextField = "SlaName";
                RadComboBoxDefaultSLA.DataSource = slaList;
                RadComboBoxDefaultSLA.DataBind();
                RadComboBoxDefaultSLA.SelectedValue = organization.DefaultSla.ToString();
            }

        }

        protected void ButtonSubmit_Click(object sender, EventArgs e)
        {
            
                boAccountingControl = new net.nimera.cmt.BOAccountingControlWSRef.BOAccountingControlWS();
                Organization organization = boAccountingControl.GetOrganization(Int32.Parse(LabelId.Text));

                organization.Id = Int32.Parse(LabelId.Text);
                organization.Address.AddressLine1 = TextBoxAddressLine1.Text;
                organization.Address.AddressLine2 = TextBoxAddressLine2.Text;
                organization.Email = TextBoxEMail.Text;
                organization.Fax = TextBoxFax.Text;
                organization.Address.Location = TextBoxLocation.Text;
                organization.FullName = TextBoxName.Text;
                organization.Address.PostalCode = TextBoxPCO.Text;
                organization.Phone = TextBoxPhone.Text;
                organization.Remarks = TextBoxRemarks.Text;
                organization.WebSite = TextBoxWebSite.Text;
                organization.Address.Country = CountryList.SelectedValue;
                organization.Distributor.Id = Int32.Parse(RadComboBoxDistributor.SelectedValue);
                organization.Sector = Int32.Parse(RadComboBoxSector.SelectedValue);
                organization.Vat = TextBoxVAT.Text;
                if (CheckBoxVNO.Checked)
                {
                    organization.VNO = true;
                    organization.MIR = Convert.ToInt32(RadNumericTextBoxMIR.Text);
                    organization.CIR = Convert.ToInt32(RadNumericTextBoxCIR.Text);
                    organization.DefaultSla = Convert.ToInt32(RadComboBoxDefaultSLA.SelectedValue);
                }
                else
                {
                    organization.VNO = false;
                    organization.MIR = 0;
                    organization.CIR = 0;
                    organization.DefaultSla = null;
                }

                if (boAccountingControl.UpdateOrganization(organization))
                {
                    LabelResult.Visible = true;
                    LabelResult.ForeColor = Color.DarkGreen;
                    LabelResult.Text = "Update of customer succeeded!";
                }
                else
                {
                    LabelResult.Visible = true;
                    LabelResult.ForeColor = Color.Red;
                    LabelResult.Text = "Update of customer failed!";
                }
            
        }

        protected void RadComboBoxISP_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            RadComboBoxDefaultSLA.Text = "";
            RadComboBoxDefaultSLA.ClearSelection();
            BOAccountingControlWS boAccountingControl =
                  new net.nimera.cmt.BOAccountingControlWSRef.BOAccountingControlWS();
            //Search Service packs for the given ISP
            int ispId = Int32.Parse(e.Value);
            ServiceLevel[] servicePacks = boAccountingControl.GetServicePacksByIspWithHide(ispId);
            RadComboBoxDefaultSLA.DataSource = servicePacks;
            RadComboBoxDefaultSLA.DataValueField = "SlaID";
            RadComboBoxDefaultSLA.DataTextField = "SlaName";
            RadComboBoxDefaultSLA.DataBind();
            RadComboBoxDefaultSLA.Items.Insert(0, new RadComboBoxItem(""));
        }
    }
}