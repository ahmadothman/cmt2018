﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FOWebApp.net.nimera.cmt.BOConnectedDevicesControlWSRef;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOMultiCastGroupControllerWSRef;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace FOWebApp.Nocsa
{
    public partial class ConnectedDevicesList : System.Web.UI.UserControl
    {
        BOAccountingControlWS accountingControl = new BOAccountingControlWS();
        BOConnectedDevicesControlWS connectedDevicesControl = new BOConnectedDevicesControlWS();
        BOMultiCastGroupControllerWS mcGroupControl = new BOMultiCastGroupControllerWS();

        TableCell cell = new TableCell();

        protected void Page_Load(object sender, EventArgs e)
        {
            //BOConnectedDevicesControlWS connectedDevicesControl = new BOConnectedDevicesControlWS();
            //ConnectedDevice[] devices = connectedDevicesControl.GetAllDevices();
            //BOMultiCastGroupControllerWS mcGroupControl = new BOMultiCastGroupControllerWS();
            //MultiCastGroup[] mcGroups = mcGroupControl.GetMultiCastGroups();
            //RadGridMulticastGroups.DataSource = mcGroups;
            //RadGridMulticastGroups.DataBind();
        }

        protected void RadGridMulticastGroups_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            String ConnString = ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
            SqlConnection conn = new SqlConnection(ConnString);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = new SqlCommand("SELECT m.ID, Name, d.FullName, d.Id AS DistId FROM MultiCastGroups AS m " 
                                                   + "LEFT JOIN Distributors AS d ON m.DistributorId = d.Id", conn);

            DataTable myDataTable = new DataTable();

            conn.Open();
            try
            {
                adapter.Fill(myDataTable);
            }
            finally
            {
                conn.Close();
            }

            RadGridMulticastGroups.DataSource = myDataTable;
        }

        protected void RadGridMulticastGroups_ItemDataBound(object sender, GridItemEventArgs e)
        {
        }

        protected void RadWindowManager1_Disposed(object sender, EventArgs e)
        {
        }

        protected void RadWindowManager1_Unload(object sender, EventArgs e)
        {
        }

        protected void RadGridMulticastGroups_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
        }

        protected void RadGridMulticastGroups_ItemCommand(object sender, GridCommandEventArgs e)
        {
            GridDataItem dataItem = e.Item as GridDataItem;
            cell = dataItem["DistId"];
        }

        protected void RadGridMulticastGroups_DetailTableDataBind(object sender, GridDetailTableDataBindEventArgs e)
        {
            //GridDataItem dataItem = (GridDataItem)e.DetailTableView.ParentItem;
            //int mcGroupId = (int)dataItem.GetDataKeyValue("Id");

            List<ConnectedDevice> connectedDevices = new List<ConnectedDevice>();

            string distId = cell.Text;
            ConnectedDevice[] devices = connectedDevicesControl.GetAllDevices();
            Terminal[] terminals = accountingControl.GetTerminalsByDistributorId(Convert.ToInt32(distId));

            foreach (ConnectedDevice device in devices)
            {
                foreach (Terminal term in terminals)
                {
                    if (term.MacAddress == device.macAddress)
                    {
                        //Distributor dist = accountingControl.GetDistributor((int)term.DistributorId);
                        //Terminal[] terminalsByDist = accountingControl.GetTerminalsByDistributor(dist.Id.ToString());
                        //foreach (Terminal termByDist in terminalsByDist)
                        //{
                        //    if (termByDist.MacAddress == device.macAddress)
                        //    {

                        //    }
                        //}

                        connectedDevices.Add(device);
                        break;
                    }
                }
            }
            e.DetailTableView.DataSource = connectedDevices;


            //if (this.Session["TerminalList_SortExpression"] != null)
            //{
            //    string sortExpression = (string)this.Session["TerminalList_SortExpression"];
            //    GridSortExpression gridSortExpression = new GridSortExpression();
            //    gridSortExpression.FieldName = sortExpression;
            //    gridSortExpression.SortOrder = (GridSortOrder)this.Session["TerminalList_SortOrder"];
            //    e.DetailTableView.SortExpressions.Add(gridSortExpression);
            //}

            ////Set the pageindex of the detail table
            //if (this.Session["_terminalListGrid_PageIdx"] != null)
            //{
            //    e.DetailTableView.CurrentPageIndex =
            //                (int)this.Session["_terminalListGrid_PageIdx"];
            //}
        }
    }
}