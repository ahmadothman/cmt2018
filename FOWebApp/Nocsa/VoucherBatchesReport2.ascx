﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VoucherBatchesReport2.ascx.cs" Inherits="FOWebApp.Nocsa.VoucherBatchesReport2" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<table cellpadding="5" cellspacing="2">
    <tr>
        <td>Start date:</td>
        <td> 
            <telerik:RadDatePicker ID="RadDatePickerStart" runat="server">
            </telerik:RadDatePicker>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Provide a start date" ControlToValidate="RadDatePickerStart" Display="Dynamic"></asp:RequiredFieldValidator>
        </td>
        <td>End date:</td>
        <td>
            <telerik:RadDatePicker ID="RadDatePickerEnd" runat="server">
            </telerik:RadDatePicker>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Provide an end date" ControlToValidate="RadDatePickerEnd"></asp:RequiredFieldValidator>
        </td>
        <td>
            <asp:ImageButton ID="ImageButtonQuery" runat="server" 
                ImageUrl="~/Images/invoice_euro.png" onclick="ImageButtonQuery_Click" 
                ToolTip="Create accounting report" />
        </td>
        <td>
            <asp:ImageButton ID="ImageButtonCSVExport" runat="server" 
                ImageUrl="~/Images/CSV.png" onclick="ImageButtonCSVExport_Click" 
                Enabled="True" ToolTip="Export as a CSV file" />
        </td>
        <td>
            <asp:ImageButton ID="ImageButtonExcelExport" runat="server" Enabled="True" 
                ImageUrl="~/Images/Excel.png" onclick="ImageButtonExcelExport_Click" 
                ToolTip="Export as an Excel file" />
        </td>
        <%--<td>
            <asp:ImageButton ID="ImageButtonPDFExport" runat="server" Enabled="true" ImageUrl="~/Images/PDF.jpg" OnClick="ImageButtonPDFExport_Click" ToolTip="Export to PDF" />
        </td>--%>
    </tr>
</table>
<telerik:RadGrid ID="RadGridVouchers" runat="server" AllowSorting="True"
    AutoGenerateColumns="False" Visible="false" CellSpacing="0" GridLines="None" Skin="WebBlue"
    ShowStatusBar="True" Width="992px" AllowFilteringByColumn="False" 
    onitemcommand="RadGridVouchers_ItemCommand" onneeddatasource="RadGridVouchers_NeedDataSource" >
        <GroupingSettings CaseSensitive="false" /> 
    <ClientSettings>
        <Selecting AllowRowSelect="True" />
        <ClientEvents OnRowSelected="RowSelected" />
    </ClientSettings>
    <MasterTableView ShowFooter="False" ShowGroupFooter="False">
    <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
        </RowIndicatorColumn>
        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
        </ExpandCollapseColumn>
        <Columns>
            <telerik:GridBoundColumn DataField="DistId" 
                FilterControlAltText="Filter DistIdColumn column" HeaderText="Dist ID" 
                UniqueName="DistId" ShowFilterIcon="False" AutoPostBackOnFilter="True">
                <ItemStyle Wrap="False" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="BatchId" 
                FilterControlAltText="Filter BatchIdColumn column" HeaderText="Batch ID" 
                UniqueName="BatchId" ShowFilterIcon="False" AutoPostBackOnFilter="True">
                <ItemStyle Wrap="False" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="DateCreated" FilterControlAltText="Filter DateCreatedColumn column"
                HeaderText="Date" ReadOnly="True" Resizable="False" 
                UniqueName="DateCreated" CurrentFilterFunction="Contains" DataFormatString="{0:dd/MM/yyyy}" DataType="System.DateTime"
                ShowFilterIcon="False" AutoPostBackOnFilter="True">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="MB" 
                FilterControlAltText="Filter MBColumn column" HeaderText="MB" 
                UniqueName="MB" ShowFilterIcon="False" AutoPostBackOnFilter="True">
                <ItemStyle Wrap="False" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Vouchers" 
                FilterControlAltText="Filter VouchersColumn column" HeaderText="Vouchers" 
                UniqueName="Vouchers" ShowFilterIcon="False" AutoPostBackOnFilter="True">
                <ItemStyle Wrap="False" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="UP" 
                FilterControlAltText="Filter UnitPriceEURColumn column" HeaderText="Unit price" 
                UniqueName="UP" ShowFilterIcon="False" AutoPostBackOnFilter="True">
                <ItemStyle Wrap="False" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="SubTotal" 
                FilterControlAltText="Filter TotalColumn column" HeaderText="Total" 
                UniqueName="Total" ShowFilterIcon="False" AutoPostBackOnFilter="True">
                <ItemStyle Wrap="False" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Currency" 
                FilterControlAltText="Filter CurrencyColumn column" HeaderText="Currency" 
                UniqueName="Currency" ShowFilterIcon="False" AutoPostBackOnFilter="True">
                <ItemStyle Wrap="False" />
            </telerik:GridBoundColumn>
        </Columns>
        <EditFormSettings>
<EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
</EditFormSettings>
</MasterTableView>
    <FilterMenu EnableImageSprites="False">
    </FilterMenu>
    <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default">
    </HeaderContextMenu>
</telerik:RadGrid>