﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Drawing;
using Telerik.Web.UI;
using FOWebApp.Data;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOMonitorControlWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;

namespace FOWebApp.Nocsa
{
    public partial class CustomerVNOList : System.Web.UI.UserControl
    {
        private Hashtable _customersExpandedState;

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void RadGridCustomers_DetailTableDataBind(object sender, Telerik.Web.UI.GridDetailTableDataBindEventArgs e)
        {
            GridDataItem dataItem = (GridDataItem)e.DetailTableView.ParentItem;
            int custId = (int)dataItem.GetDataKeyValue("Id");

            BOAccountingControlWS boAccountingControl =
             new net.nimera.cmt.BOAccountingControlWSRef.BOAccountingControlWS();

            Terminal[] terminals = boAccountingControl.GetTerminalsByOrganization(custId);

            e.DetailTableView.DataSource = terminals;

            if (this.Session["CustTerminalList_SortExpression"] != null)
            {
                string sortExpression = (string)this.Session["CustTerminalList_SortExpression"];
                GridSortExpression gridSortExpression = new GridSortExpression();
                gridSortExpression.FieldName = sortExpression;
                gridSortExpression.SortOrder = (GridSortOrder)this.Session["CustTerminalList_SortOrder"];
                e.DetailTableView.SortExpressions.Add(gridSortExpression);
            }

            //Set the pageindex of the detail table
            if (this.Session["_custTerminalListGrid_PageIdx"] != null)
            {
                e.DetailTableView.CurrentPageIndex =
                            (int)this.Session["_custTerminalListGrid_PageIdx"];
            }
        }

        /// <summary>
        /// Configures the export settings for this radgrid
        /// </summary>
        public void ConfigureExport()
        {
            RadGridCustomers.ExportSettings.FileName = "CMTExport";
            RadGridCustomers.ExportSettings.ExportOnlyData = true;
            RadGridCustomers.ExportSettings.IgnorePaging = true;
            RadGridCustomers.ExportSettings.OpenInNewWindow = true;
            RadGridCustomers.ExportSettings.Excel.Format = GridExcelExportFormat.Html;
            RadGridCustomers.ExportSettings.Csv.ColumnDelimiter = GridCsvDelimiter.Semicolon;
            RadGridCustomers.ExportSettings.Csv.EncloseDataWithQuotes = false;
            RadGridCustomers.ExportSettings.Csv.RowDelimiter = GridCsvDelimiter.NewLine;
        }

        protected void RadGridCustomers_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToExcelCommandName)
            {
                this.ConfigureExport();
                RadGridCustomers.MasterTableView.ExportToExcel();
            }

            if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToCsvCommandName)
            {
                this.ConfigureExport();
                RadGridCustomers.MasterTableView.ExportToCSV();
            }

            if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToPdfCommandName)
            {
                this.ConfigureExport();
                RadGridCustomers.MasterTableView.ExportToPdf();
            }

            if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToWordCommandName)
            {
                this.ConfigureExport();
                RadGridCustomers.MasterTableView.ExportToWord();
            }

            //Save the expanded state in the session
            if (e.CommandName == RadGrid.ExpandCollapseCommandName)
            {
                //Is the item about to be expanded or collapsed
                if (!e.Item.Expanded)
                {
                    //Save its unique index among all the items in the hierarchy
                    this.ExpandedStates[e.Item.ItemIndexHierarchical] = true;
                }
                else //Collapsed
                {
                    this.ExpandedStates.Remove(e.Item.ItemIndexHierarchical);
                    this.ClearExpandedChildren(e.Item.ItemIndexHierarchical);
                }
            }

            if (e.CommandName == RadGrid.FilterCommandName)
            {
                Pair filterPair = new Pair();
                filterPair.Second = ((Pair)e.CommandArgument).Second;

                //Set the session variables to mark the filter
                GridColumn column = RadGridCustomers.MasterTableView.GetColumnSafe(filterPair.Second.ToString());
                string columnAndFilterValue = column.AndCurrentFilterValue;
                filterPair.First = column.CurrentFilterFunction;
                this.Session["_customerList_FilterPair"] = filterPair;

                string currentFilter = column.CurrentFilterValue;
                this.Session["_customerList_FilterValue"] = currentFilter;
            }
        }

        protected void RadGridCustomers_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item.OwnerTableView.Name.Equals("TerminalsTableView"))
            {
                if (e.Item is GridDataItem)
                {
                    GridDataItem dataItem = (GridDataItem)e.Item;


                    if (dataItem["AdmStatusColumn"].Text.Equals("1"))
                    {
                        dataItem["AdmStatusColumn"].Text = "Locked";
                        dataItem["AdmStatusColumn"].ForeColor = Color.Red;
                    }

                    if (dataItem["AdmStatusColumn"].Text.Equals("2"))
                    {
                        dataItem["AdmStatusColumn"].Text = "Unlocked";
                        dataItem["AdmStatusColumn"].ForeColor = Color.Green;
                    }

                    if (dataItem["AdmStatusColumn"].Text.Equals("3"))
                    {
                        dataItem["AdmStatusColumn"].Text = "Suspended";
                        dataItem["AdmStatusColumn"].ForeColor = Color.Blue;
                    }

                    string slaId = dataItem["ServiceColumn"].Text;
                    if (!string.IsNullOrEmpty(slaId))
                    {
                        try
                        {
                            BOAccountingControlWS boAccountingControl =
                                                    new net.nimera.cmt.BOAccountingControlWSRef.BOAccountingControlWS();


                            dataItem["ServiceColumn"].Text = boAccountingControl.GetServicePack(int.Parse(slaId)).SlaName;
                        }
                        catch (Exception ex)
                        {

                            BOLogControlWS logController = new BOLogControlWS();
                            CmtApplicationException cmtAppEx = new CmtApplicationException
                            {
                                ExceptionDateTime = DateTime.UtcNow,
                                ExceptionDesc = ex.Message,
                                ExceptionStacktrace = ex.StackTrace,
                                ExceptionLevel = 2,
                                UserDescription = "CustomerList: The Service Pack Could not be found"
                            };
                            logController.LogApplicationException(cmtAppEx);
                        }
                    }

                    //Put the MAC addresses of the non-operational terminals in red
                    if (dataItem["CNo"].Text.Equals("-1.00") || dataItem["CNo"].Text.Equals("") || dataItem["CNo"].Text.Equals("-1,00"))
                    {
                        dataItem["MacAddressColumn"].ForeColor = Color.Red;
                    }
                    else
                    {
                        dataItem["MacAddressColumn"].ForeColor = Color.Green;
                    }
                }
            }
        }

        //Save/load expanded states Hash from the session
        //this can also be implemented in the ViewState
        private Hashtable ExpandedStates
        {
            get
            {
                if (this.Session["_customersListState"] == null)
                {
                    this.Session["_customersListState"] = new Hashtable();
                }
                _customersExpandedState = this.Session["_customersListState"] as Hashtable;
                return _customersExpandedState;
            }
        }

        //Clear the state for all expanded  children if a parent item is collapsed
        private void ClearExpandedChildren(string parentHierarchicalIndex)
        {
            string[] indexes = new string[this.ExpandedStates.Keys.Count];
            this.ExpandedStates.Keys.CopyTo(indexes, 0);

            foreach (string index in indexes)
            {
                //All indexes of child items
                if (index.StartsWith(parentHierarchicalIndex + "_") ||
                    index.StartsWith(parentHierarchicalIndex + ":"))
                {
                    this.ExpandedStates.Remove(index);
                }
            }
        }

        protected void RadGridCustomers_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            //Load data into the grid view. This view lists all terminals for the 
            //authenticated user.
            BOAccountingControlWS boAccountingControl =
              new net.nimera.cmt.BOAccountingControlWSRef.BOAccountingControlWS();

            //Get the authenticated user
            MembershipUser myObject = Membership.GetUser();
            string userId = myObject.ProviderUserKey.ToString();

            //Get all Customer VNOs
            DataHelper dataHelper = new DataHelper();
            Organization[] organizations = boAccountingControl.GetCustomerVNOs();
            
            RadGridCustomers.DataSource = organizations;

            Organization org = new Organization();
            
            if (this.Session["CustomerList_SortExpression"] != null)
            {
                string sortExpression = (string)this.Session["CustomerList_SortExpression"];
                GridSortExpression gridSortExpression = new GridSortExpression();
                gridSortExpression.FieldName = sortExpression;
                gridSortExpression.SortOrder = (GridSortOrder)this.Session["CustomerList_SortOrder"];
                RadGridCustomers.MasterTableView.SortExpressions.Add(gridSortExpression);
            }

            if (this.Session["_customerList_PageSize"] != null)
            {
                RadGridCustomers.PageSize =
                    (int)this.Session["_customerList_PageSize"];
            }

            if (this.Session["_customerListGrid_PageIdx"] != null)
            {
                RadGridCustomers.CurrentPageIndex =
                    (int)this.Session["_customerListGrid_PageIdx"];
            }

            if (this.Session["_customerList_FilterPair"] != null)
            {
                Pair filterPair = (Pair)this.Session["_customerList_FilterPair"];
                GridColumn col = RadGridCustomers.MasterTableView.GetColumnSafe(filterPair.Second.ToString());

                col.CurrentFilterValue = this.Session["_customerList_FilterValue"].ToString();
                col.CurrentFilterFunction = (GridKnownFunction)filterPair.First;
                RadGridCustomers.MasterTableView.FilterExpression = "([" + filterPair.Second.ToString() + "] LIKE \'%" + this.Session["_customerList_FilterValue"].ToString() + "%\') ";
            }
        }

        protected void RadGridCustomers_DataBound(object sender, EventArgs e)
        {
            //Expand all items using our custom storage
            string[] indexes = new string[this.ExpandedStates.Keys.Count];
            this.ExpandedStates.Keys.CopyTo(indexes, 0);

            ArrayList arr = new ArrayList(indexes);

            //Sort so we can guarantee that a parent item is expanded before any of
            //its children
            arr.Sort();

            foreach (string key in arr)
            {
                bool value = (bool)this.ExpandedStates[key];
                if (value)
                {
                    try
                    {
                        RadGridCustomers.Items[key].Expanded = true;
                    }
                    catch (Exception ex)
                    {
                        BOLogControlWS boLogControlWS = new BOLogControlWS();
                        CmtApplicationException cmtEx = new CmtApplicationException();
                        cmtEx.ExceptionDesc = ex.Message;
                        cmtEx.ExceptionStacktrace = ex.StackTrace;
                        boLogControlWS.LogApplicationException(cmtEx);
                    }
                }
            }
        }

        protected void RadGridCustomers_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            //Save the page index
            int pageIdx = e.NewPageIndex;

            string gridName = e.Item.OwnerTableView.Name;

            if (gridName.Equals("TerminalsTableView"))
            {
                this.Session["_custTerminalListGrid_PageIdx"] = pageIdx;
            }
            else
            {
                this.Session["_customerListGrid_PageIdx"] = pageIdx;
            }
        }

        protected void RadGridCustomers_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            this.Session["_customerList_PageSize"] = e.NewPageSize;
        }

        protected void RadGridCustomers_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            GridDataKeyArray dataItems = e.Item.OwnerTableView.DataKeyValues;
            string orgId = dataItems[e.Item.ItemIndex]["Id"].ToString();
            BOLogControlWS boLogControlWS = new BOLogControlWS();

            //Delete the user with the given Id
            try
            {

                BOAccountingControlWS boAccountingControlWS = new BOAccountingControlWS();
                if (!boAccountingControlWS.DeleteOrganizations(Int32.Parse(orgId)))
                {
                    CmtApplicationException cmtException = new CmtApplicationException();
                    cmtException.ExceptionDateTime = DateTime.Now;
                    cmtException.ExceptionDesc = "Could not delete customer with Id: " + orgId;
                    cmtException.StateInformation = "User Id probably not a number";
                    boLogControlWS.LogApplicationException(cmtException);
                    RadWindowManager1.RadAlert("Deleting customer failed! This customer may still have active terminals.", 250, 200, "Error", null);
                }
                RadGridCustomers.DataSource = boAccountingControlWS.GetOrganizations();
                RadGridCustomers.Rebind();
            }
            catch (Exception ex)
            {

                CmtApplicationException cmtException = new CmtApplicationException();
                cmtException.ExceptionDateTime = DateTime.Now;
                cmtException.ExceptionDesc = ex.Message;
                cmtException.StateInformation = "User Id probably not a number";
                boLogControlWS.LogApplicationException(cmtException);
            }
        }

        protected void RadGridCustomers_SortCommand(object sender, GridSortCommandEventArgs e)
        {
            string gridName = e.Item.OwnerTableView.Name;

            if (gridName.Equals("TerminalsTableView"))
            {
                this.Session["CustTerminalList_SortExpression"] = e.SortExpression;
                this.Session["CustTerminalList_SortOrder"] = e.NewSortOrder;
            }
            else
            {
                this.Session["CustomerList_SortExpression"] = e.SortExpression;
                this.Session["CustomerList_SortOrder"] = e.NewSortOrder;
            }
        }
    }
}