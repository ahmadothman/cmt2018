﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using FOWebApp.net.nimera.cmt.BOServicesControllerWSRef;
using System.Drawing;
using Telerik.Web.UI;

namespace FOWebApp.Nocsa
{
    public partial class RoleDetailsForm : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public void componentDataInit(string roleName)
        {
            LabelRoleName.Style["Font-Weight"] = "bold";
            LabelRoleName.Text = roleName;
            int usersInRole = Roles.GetUsersInRole(roleName).Length;
            LabelUsersInRole.Text = usersInRole.ToString();

            //List of roles that may not be deleted
            string[] lockedRoles = {"Distributor", "EndUser", "NOC Administrator", "NOC Operator", "Organization"};
            bool roleLocked = false;
            foreach (string s in lockedRoles)
            {
                if (s == roleName)
                {
                    roleLocked = true;
                }
            }

            if (usersInRole > 0) //Roles with users may not be deleted
            {
                ButtonDelete.Enabled = false;
            }
            else if (roleLocked)
            {
                ButtonDelete.Enabled = false;
            }
            else
            {
                ButtonDelete.Enabled = true;
            }

            //Load the Services Checkbox list
            PanelServicesAllocation.Visible = true;
            RadListBoxServices.Visible = true;
            RadListBoxItem listItem;
            BOServicesControllerWS boServiceControl = new BOServicesControllerWS();
            Service[] serviceList1 = boServiceControl.GetAllAvailableServices("1");
            foreach (Service serv1 in serviceList1)
            {
                listItem = new RadListBoxItem(serv1.ServiceName, serv1.ServiceId.ToString());
                RadListBoxServices.Items.Add(listItem);
                Service[] serviceList2 = boServiceControl.GetChildren(serv1);
                if (serviceList2.Length > 0)
                {
                    foreach (Service serv2 in serviceList2)
                    {
                        listItem = new RadListBoxItem("---" + serv2.ServiceName, serv2.ServiceId.ToString());
                        RadListBoxServices.Items.Add(listItem);
                        Service[] serviceList3 = boServiceControl.GetChildren(serv2);
                        if (serviceList3.Length > 0)
                        {
                            foreach (Service serv3 in serviceList3)
                            {
                                listItem = new RadListBoxItem("------" + serv3.ServiceName, serv3.ServiceId.ToString());
                                RadListBoxServices.Items.Add(listItem);
                            }
                        }
                    }
                }
            }

            //Check the Services allocated to this Role
            Service[] services = boServiceControl.GetServicesForRole(roleName);

            foreach (Service service in services)
            {
                for (int i = 0; i < RadListBoxServices.Items.Count; i++)
                {
                    if (!RadListBoxServices.Items[i].Checked && RadListBoxServices.Items[i].Value.Equals(service.ServiceId.ToString()))
                    {
                        RadListBoxServices.Items[i].Checked = true;
                    }
                }
            }
        }
        
        protected void ButtonDelete_Click(object sender, EventArgs e)
        {
            BOServicesControllerWS boServiceControl = new BOServicesControllerWS();
            string roleName = LabelRoleName.Text.ToString();
            
            if (Roles.DeleteRole(roleName))
            {
                Service[] services = boServiceControl.GetServicesForRole(roleName);

                foreach (Service service in services)
                {
                    boServiceControl.RemoveServiceFromRole(roleName, service.ServiceId.ToString());
                }
                
                LabelDelete.Text = "Role successfully deleted";
                LabelDelete.ForeColor = Color.Green;
            }
            else
            {
                LabelDelete.Text = "Deleting role failed. Please check log for details.";
                LabelDelete.ForeColor = Color.Red;
            }
        }
    }
}