﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using Telerik.Web.UI;
using FOWebApp.Data;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;

namespace FOWebApp.Nocsa
{
    public partial class VNOList : System.Web.UI.UserControl
    {
        private Hashtable _VNOListGrid_ExpandedState;

        private Hashtable ExpandedStates
        {
            get
            {
                if (this.Session["_VNOListGrid_ExpandedState"] == null)
                {
                    this.Session["_VNOListGrid_ExpandedState"] = new Hashtable();
                }

                _VNOListGrid_ExpandedState = this.Session["_VNOListGrid_ExpandedState"] as Hashtable;
                return _VNOListGrid_ExpandedState;
            }
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Configures the export settings for this radgrid
        /// </summary>
        public void ConfigureExport()
        {
            RadGridVNOList.ExportSettings.FileName = "CMTVNOs";
            RadGridVNOList.ExportSettings.ExportOnlyData = true;
            RadGridVNOList.ExportSettings.IgnorePaging = true;
            RadGridVNOList.ExportSettings.OpenInNewWindow = true;
            RadGridVNOList.ExportSettings.Excel.Format = GridExcelExportFormat.Html;
            RadGridVNOList.ExportSettings.Csv.ColumnDelimiter = GridCsvDelimiter.Semicolon;
            RadGridVNOList.ExportSettings.Csv.EncloseDataWithQuotes = false;
            RadGridVNOList.ExportSettings.Csv.RowDelimiter = GridCsvDelimiter.NewLine;
        }

        protected void RadGridVNOList_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToExcelCommandName)
            {
                this.ConfigureExport();
                RadGridVNOList.MasterTableView.ExportToExcel();
            }

            if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToCsvCommandName)
            {
                this.ConfigureExport();
                RadGridVNOList.MasterTableView.ExportToCSV();
            }

            if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToPdfCommandName)
            {
                this.ConfigureExport();
                RadGridVNOList.MasterTableView.ExportToPdf();
            }

            if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToWordCommandName)
            {
                this.ConfigureExport();
                RadGridVNOList.MasterTableView.ExportToWord();
            }

            //Save the expanded state in the session
            if (e.CommandName == RadGrid.ExpandCollapseCommandName)
            {
                //Is the item about to be expanded or collapsed
                if (!e.Item.Expanded)
                {
                    //Save its unique index among all the items in the hierarchy
                    this.ExpandedStates[e.Item.ItemIndexHierarchical] = true;
                }
                else //Collapsed
                {
                    this.ExpandedStates.Remove(e.Item.ItemIndexHierarchical);
                    this.ClearExpandedChildren(e.Item.ItemIndexHierarchical);
                }
            }


            if (e.CommandName == RadGrid.FilterCommandName)
            {
                Pair filterPair = new Pair();
                filterPair.Second = ((Pair)e.CommandArgument).Second;

                //Set the session variables to mark the filter
                GridColumn column = RadGridVNOList.MasterTableView.GetColumnSafe(filterPair.Second.ToString());
                string columnAndFilterValue = column.AndCurrentFilterValue;
                filterPair.First = column.CurrentFilterFunction;
                this.Session["VNOList_FilterPair"] = filterPair;

                string currentFilter = column.CurrentFilterValue;
                this.Session["VNOList_FilterValue"] = currentFilter;

                string filterExpression = RadGridVNOList.MasterTableView.FilterExpression;
                this.Session["VNOList_FilterExpression"] = filterExpression;

            }

        }

        //Clear the state for all expanded children if a parent item is collapsed
        private void ClearExpandedChildren(string parentHierarchicalIndex)
        {
            string[] indexes = new string[this.ExpandedStates.Keys.Count];
            this.ExpandedStates.Keys.CopyTo(indexes, 0);

            foreach (string index in indexes)
            {
                //All indexes of child items
                if (index.StartsWith(parentHierarchicalIndex + "_") ||
                    index.StartsWith(parentHierarchicalIndex = ":"))
                {
                    this.ExpandedStates.Remove(index);
                }
            }
        }

        protected void RadGridVNOList_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            //Read the Distributors from the database
            //DataHelper dataHelper = new DataHelper();
            BOAccountingControlWS boAccountingControlWS = new BOAccountingControlWS();
            //List<Distributor> list =
            RadGridVNOList.DataSource = boAccountingControlWS.GetVNOs();

            if (this.Session["VNOList_SortExpression"] != null)
            {
                string sortExpression = (string)this.Session["VNOList_SortExpression"];
                GridSortExpression gridSortExpression = new GridSortExpression();
                gridSortExpression.FieldName = sortExpression;
                gridSortExpression.SortOrder = (GridSortOrder)this.Session["VNOList_SortOrder"];
                RadGridVNOList.MasterTableView.SortExpressions.Add(gridSortExpression);
            }

            if (this.Session["_VNOListGrid_PageSize"] != null)
            {
                RadGridVNOList.PageSize =
                            (int)this.Session["_VNOListGrid_PageSize"];
            }

            if (this.Session["_VNOListGrid_PageIdx"] != null)
            {
                RadGridVNOList.CurrentPageIndex =
                            (int)this.Session["_VNOListGrid_PageIdx"];
            }

            if (this.Session["VNOList_FilterPair"] != null)
            {
                Pair filterPair = (Pair)this.Session["VNOList_FilterPair"];
                GridColumn col = RadGridVNOList.MasterTableView.GetColumnSafe(filterPair.Second.ToString());

                col.CurrentFilterValue = this.Session["VNOList_FilterValue"].ToString();
                col.CurrentFilterFunction = (GridKnownFunction)filterPair.First;
                RadGridVNOList.MasterTableView.FilterExpression = "([" + filterPair.Second.ToString() + "] LIKE \'%" + this.Session["VNOList_FilterValue"].ToString() + "%\') ";
            }
        }

        protected void RadGridVNOList_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            //Save the page index
            int pageIdx = e.NewPageIndex;

            string gridName = e.Item.OwnerTableView.Name;

            if (gridName.Equals("TerminalsTableView"))
            {
                this.Session["_terminalListGrid_PageIdx"] = pageIdx;
            }
            else
            {
                this.Session["_VNOListGrid_PageIdx"] = pageIdx;
            }
        }

        protected void RadGridVNOList_DetailTableDataBind(object sender, GridDetailTableDataBindEventArgs e)
        {
            GridDataItem dataItem = (GridDataItem)e.DetailTableView.ParentItem;
            int VNOId = (int)dataItem.GetDataKeyValue("Id");

            BOAccountingControlWS boAccountingControl =
             new net.nimera.cmt.BOAccountingControlWSRef.BOAccountingControlWS();

            Terminal[] terminals = boAccountingControl.GetTerminalsByDistributorId(VNOId);

            e.DetailTableView.DataSource = terminals;

            if (this.Session["TerminalList_SortExpression"] != null)
            {
                string sortExpression = (string)this.Session["TerminalList_SortExpression"];
                GridSortExpression gridSortExpression = new GridSortExpression();
                gridSortExpression.FieldName = sortExpression;
                gridSortExpression.SortOrder = (GridSortOrder)this.Session["TerminalList_SortOrder"];
                e.DetailTableView.SortExpressions.Add(gridSortExpression);
            }

            //Set the pageindex of the detail table
            if (this.Session["_terminalListGrid_PageIdx"] != null)
            {
                e.DetailTableView.CurrentPageIndex =
                            (int)this.Session["_terminalListGrid_PageIdx"];
            }
        }

        protected void RadGridVNOList_DataBound(object sender, EventArgs e)
        {
            //Expand all items using our customer storage
            string[] indexes = new string[this.ExpandedStates.Keys.Count];
            this.ExpandedStates.Keys.CopyTo(indexes, 0);

            ArrayList arr = new ArrayList(indexes);

            //Sort so we can guarantee that a parent item is expanded before any
            //of its children
            arr.Sort();

            foreach (string key in arr)
            {
                bool value = (bool)this.ExpandedStates[key];
                if (value)
                {
                    try
                    {
                        RadGridVNOList.Items[key].Expanded = true;
                    }
                    catch (Exception ex)
                    {
                        BOLogControlWS boLogControlWS = new BOLogControlWS();
                        CmtApplicationException cmtEx = new CmtApplicationException();
                        cmtEx.ExceptionDesc = ex.Message;
                        cmtEx.ExceptionStacktrace = ex.StackTrace;
                        boLogControlWS.LogApplicationException(cmtEx);
                    }
                }
            }
        }

        protected void RadGridVNOList_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item.OwnerTableView.Name.Equals("TerminalsTableView"))
            {
                if (e.Item is GridDataItem)
                {
                    GridDataItem dataItem = (GridDataItem)e.Item;


                    if (dataItem["AdmStatusColumn"].Text.Equals("1"))
                    {
                        dataItem["AdmStatusColumn"].Text = "Locked";
                        dataItem["AdmStatusColumn"].ForeColor = Color.Red;
                    }

                    if (dataItem["AdmStatusColumn"].Text.Equals("2"))
                    {
                        dataItem["AdmStatusColumn"].Text = "Unlocked";
                        dataItem["AdmStatusColumn"].ForeColor = Color.Green;
                    }

                    if (dataItem["AdmStatusColumn"].Text.Equals("3"))
                    {
                        dataItem["AdmStatusColumn"].Text = "Decommissioned";
                        dataItem["AdmStatusColumn"].ForeColor = Color.Blue;
                    }

                    if (dataItem["AdmStatusColumn"].Text.Equals("5"))
                    {
                        dataItem["AdmStatusColumn"].Text = "Request";
                        dataItem["AdmStatusColumn"].ForeColor = Color.DarkOrange;
                    }

                    if (dataItem["AdmStatusColumn"].Text.Equals("6"))
                    {
                        dataItem["AdmStatusColumn"].Text = "Deleted";
                        dataItem["AdmStatusColumn"].ForeColor = Color.Black;
                    }

                    //Put the MAC addresses of the non-operational terminals in red
                    if (dataItem["CNo"].Text.Equals("-1.00") || dataItem["CNo"].Text.Equals("") || dataItem["CNo"].Text.Equals("-1,00"))
                    {
                        dataItem["MacAddressColumn"].ForeColor = Color.Red;
                    }
                    else
                    {
                        dataItem["MacAddressColumn"].ForeColor = Color.Green;
                    }
                }
            }
        }

        protected void RadGridVNOList_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            this.Session["_VNOListGrid_PageSize"] = e.NewPageSize;
        }

        protected void RadGridVNOList_SortCommand(object sender, GridSortCommandEventArgs e)
        {
            string gridName = e.Item.OwnerTableView.Name;

            if (gridName.Equals("TerminalsTableView"))
            {
                this.Session["TerminalList_SortExpression"] = e.SortExpression;
                this.Session["TerminalList_SortOrder"] = e.NewSortOrder;
            }
            else
            {
                this.Session["VNOList_SortExpression"] = e.SortExpression;
                this.Session["VNOList_SortOrder"] = e.NewSortOrder;
            }
        }

        protected void RadGridVNOList_PreRender(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Returns the identifier for the given status
        /// </summary>
        /// <param name="statusValue"></param>
        /// <returns></returns>
        private int getStatusValueId(string statusValue)
        {
            BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();
            TerminalStatus[] termStatusList = boAccountingControl.GetTerminalStatus();
            int statusId = 0;

            foreach (TerminalStatus termStatus in termStatusList)
            {
                string statusDesc = termStatus.StatusDesc.Trim();
                if (statusDesc.Equals(statusValue))
                {
                    statusId = (int)termStatus.Id;
                }
            }

            return statusId;
        }

        protected void RadGridVNOList_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            GridDataKeyArray dataItems = e.Item.OwnerTableView.DataKeyValues;
            string orgId = dataItems[e.Item.ItemIndex]["Id"].ToString();
            BOLogControlWS boLogControlWS = new BOLogControlWS();

            //Delete the user with the given Id
            try
            {

                BOAccountingControlWS boAccountingControlWS = new BOAccountingControlWS();
                if (!boAccountingControlWS.DeleteDistributor(Int32.Parse(orgId)))
                {
                    CmtApplicationException cmtException = new CmtApplicationException();
                    cmtException.ExceptionDateTime = DateTime.Now;
                    cmtException.ExceptionDesc = "Could not delete VNO with Id: " + orgId;
                    cmtException.StateInformation = "User Id probably not a number";
                    boLogControlWS.LogApplicationException(cmtException);
                    RadWindowManager1.RadAlert("Deleting VNO failed! This VNO may still have active terminals.", 250, 200, "Error", null);
                }
                RadGridVNOList.DataSource = boAccountingControlWS.GetVNOs();
                RadGridVNOList.Rebind();
            }
            catch (Exception ex)
            {

                CmtApplicationException cmtException = new CmtApplicationException();
                cmtException.ExceptionDateTime = DateTime.Now;
                cmtException.ExceptionDesc = ex.Message;
                cmtException.StateInformation = "User Id probably not a number";
                boLogControlWS.LogApplicationException(cmtException);
            }
        }
    }
}