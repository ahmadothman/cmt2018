﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VoucherBatchRelease.aspx.cs" Inherits="FOWebApp.Nocsa.VoucherBatchRelease" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="../CMTStyle.css" rel="stylesheet" type="text/css" />
    <title>Voucher Batch Release State</title>
</head>
<body>
    <form id="formVoucherBatchRelease" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <div>
            <table cellpadding="10" cellspacing="5">
                <tr>
                    <td>Batch Id:
                    </td>
                    <td colspan="2">
                        <asp:Label ID="LabelBatchId" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>Release Status:
                    </td>
                    <td colspan="2">
                        <asp:CheckBox ID="CheckBoxRelease" runat="server" Text=""
                            TextAlign="Left" Enabled="False" />
                    </td>
                </tr>
                <tr>
                    <td>Paid:
                </td>
                    <td colspan="2">
                        <asp:CheckBox ID="CheckBoxPaid" runat="server" Enabled="false" />
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <telerik:RadButton ID="RadButtonSubmit" runat="server" Skin="Metro"
                            Text="Submit" OnClick="RadButtonSubmit_Click">
                        </telerik:RadButton>
                    </td>
                    <td align="center">
                        <telerik:RadButton ID="RadButtonSwitchPaid" runat="server" Skin="Metro"
                            Text="Switch Paid" OnClick="RadButtonSwitchPaid_Click">
                        </telerik:RadButton>
                    </td>
                    <td>
                        <telerik:RadButton ID="RadButton3" runat="server" Skin="Metro"
                            Text="Delete Batch" OnClick="RadButton3_Click">
                        </telerik:RadButton>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Label ID="LabelResult" runat="server"></asp:Label>
                    </td>
                    <td align="center">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
