﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FOWebApp.Nocsa
{
    public partial class TicketDetailsPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Load the details form and initialize the data
            string id = Request.Params["id"].ToString();

            TicketDetailsForm df =
                (TicketDetailsForm)Page.LoadControl("TicketDetailsForm.ascx");

            df.componentDataInit(id);

            PlaceHolderTicketDetailsForm.Controls.Add(df);
        }
    }
}