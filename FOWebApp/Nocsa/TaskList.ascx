﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TaskList.ascx.cs" Inherits="FOWebApp.Nocsa.TaskList" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<style type="text/css">
    .rgAltRow, .rgRow {
        cursor: pointer !important;
    }
</style>
<script type="text/javascript">
    function RowSelected(sender, eventArgs) {
        var grid = sender;
        var masterDataView = grid.get_masterTableView();
        var rowNum = eventArgs.get_itemIndexHierarchical();
        var row = masterDataView.get_dataItems()[rowNum];
        var macAddress = masterDataView.getCellByColumnUniqueName(row, "MacAddress").innerHTML;

        //Initialize and show the terminal details window
        var oWnd = radopen("Nocsa/TerminalForm.aspx?mac=" + macAddress, "RadWindowTerminalDetails");
    }
</script>
<telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
    <h2>Your tasks for today <%=DateTime.Now.ToString("dd/MM/yyyy") %></h2>
</telerik:RadCodeBlock>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxyTasks" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="RadGridTasks">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadWindowManager1" />
                <telerik:AjaxUpdatedControl ControlID="RadGridTasks" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadGrid ID="RadGridTasks" runat="server" AutoGenerateColumns="False"
    CellSpacing="0" GridLines="None" OnNeedDataSource="RadGridTasks_NeedDataSource"
    Skin="Metro" OnItemCommand="RadGridTasks_ItemCommand">
    <ClientSettings EnableRowHoverStyle="true">
        <Selecting AllowRowSelect="True" />
        <ClientEvents OnRowSelected="RowSelected" />
    </ClientSettings>
    <MasterTableView DataKeyNames="Id" CommandItemDisplay="Top">
        <CommandItemSettings ExportToPdfText="Export to PDF" AddNewRecordText=""
            ShowAddNewRecordButton="False"></CommandItemSettings>

        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
            <HeaderStyle Width="20px"></HeaderStyle>
        </RowIndicatorColumn>

        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
            <HeaderStyle Width="20px"></HeaderStyle>
        </ExpandCollapseColumn>

        <Columns>
            <telerik:GridBoundColumn AllowFiltering="False" AllowSorting="False"
                DataField="Id" FilterControlAltText="Filter TaskId column" HeaderText="Id"
                ShowSortIcon="False" UniqueName="Id">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn AllowFiltering="False" AllowSorting="False"
                DataField="Subject" FilterControlAltText="Filter Subject column"
                HeaderText="Task" UniqueName="Subject">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn AllowFiltering="False" AllowSorting="False"
                DataField="Type.Type" FilterControlAltText="Filter Type column"
                HeaderText="Type" ShowSortIcon="False" UniqueName="Type">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn AllowFiltering="False" AllowSorting="False"
                DataField="MacAddress" FilterControlAltText="Filter MacAddress column"
                HeaderText="Mac Address" ShowSortIcon="False" UniqueName="MacAddress">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn AllowFiltering="False" DataField="DateCreated"
                FilterControlAltText="Filter Created column" HeaderText="Created"
                UniqueName="DateCreated" DataFormatString="{0:dd/MM/yyyy HH:mm}" DataType="System.DateTime">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn AllowFiltering="False" DataField="DateDue"
                FilterControlAltText="Filter DateDue column" HeaderText="Due"
                UniqueName="DateDue" DataFormatString="{0:dd/MM/yyyy HH:mm}" DataType="System.DateTime">
            </telerik:GridBoundColumn>
            <telerik:GridButtonColumn HeaderText="Mark as complete" ImageUrl="~/Images/checkbox.png" ConfirmText="Mark this task as completed?"
                UniqueName="Completed" ButtonType="ImageButton" HeaderButtonType="PushButton"
                Text="Complete" CommandName="Complete">
                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </telerik:GridButtonColumn>
        </Columns>

        <EditFormSettings>
            <EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
        </EditFormSettings>
    </MasterTableView>

    <FilterMenu EnableImageSprites="False"></FilterMenu>

    <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default"
        EnableRoundedCorners="True" EnableShadows="True">
    </HeaderContextMenu>
</telerik:RadGrid>

<telerik:RadWindowManager ID="RadWindowManager1" runat="server" Skin="Metro" DestroyOnClose="True">
    <Windows>
        <telerik:RadWindow ID="RadWindowTerminalDetails" runat="server"
            Animation="None" Opacity="100" Skin="Metro" Title="Terminal Details" AutoSize="False"
            Width="1000px" Height="700px" KeepInScreenBounds="True" EnableShadow="True" EnableAriaSupport="True"
            VisibleStatusbar="True" Modal="False" DestroyOnClose="False">
        </telerik:RadWindow>
        <telerik:RadWindow ID="RadWindowModifyTerminal" runat="server" Animation="None" Opacity="100"
            Skin="Metro" Title="Modify Terminal" AutoSize="False" Width="850px" Height="700px"
            KeepInScreenBounds="True" EnableShadow="True" EnableAriaSupport="True" VisibleStatusbar="True"
            Modal="False" DestroyOnClose="False">
        </telerik:RadWindow>
        <telerik:RadWindow ID="RadWindowModifyActivity" runat="server" Animation="None" Opacity="100"
            Skin="Metro" Title="Modify Activity" AutoSize="False" Width="850px" Height="655px"
            KeepInScreenBounds="True" EnableShadow="True" EnableAriaSupport="True" VisibleStatusbar="True"
            Modal="False" DestroyOnClose="False">
        </telerik:RadWindow>
    </Windows>
</telerik:RadWindowManager>
