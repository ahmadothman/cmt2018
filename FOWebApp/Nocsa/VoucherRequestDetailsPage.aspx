﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VoucherRequestDetailsPage.aspx.cs" Inherits="FOWebApp.Nocsa.VoucherRequestDetailsPage" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Voucher request details</title>
    <link href="../CMTStyle.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManagerVoucherRequestDetails" runat="server">
        <Services>
            <asp:ServiceReference Path="../WebServices/AccountSupportWS.asmx" />
        </Services>
    </asp:ScriptManager>
    <div>
        <asp:PlaceHolder ID="PlaceHolderVoucherRequestDetailsForm" runat="server"></asp:PlaceHolder>
    </div>
    </form>
</body>
</html>
