﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;
using FOWebApp.net.nimera.cmt.BOVoucherControllerWSRef;

namespace FOWebApp.Nocsa
{
    public partial class CreateVouchers : System.Web.UI.UserControl
    {
        BOAccountingControlWS _boAccountingControlWS = new BOAccountingControlWS();
        BOVoucherControllerWS _boVoucherControlWS = new BOVoucherControllerWS();


        protected void Page_Load(object sender, EventArgs e)
        {
            //Load distributors into the combobox
            Distributor[] distributors = _boAccountingControlWS.GetDistributors();

            foreach (Distributor distributor in distributors)
            {
                RadComboBoxItem item = new RadComboBoxItem();
                item.Value = distributor.Id.ToString();
                item.Text = distributor.FullName;
                RadComboBoxDistributors.Items.Add(item);
            }

            
        }

        protected void RadButtonCreateVouchers_Click(object sender, EventArgs e)
        {
            string test = e.ToString();
        }

        protected void RadComboBoxDistributors_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            //Load available voucher volumes into the combobox
            
            Isp[] isps = _boAccountingControlWS.GetISPsForDistributor(Int32.Parse(e.Value));
            RadTreeView slaTree = (RadTreeView)RadComboBoxVoucherVolume.Items[0].FindControl("RadTreeViewVV");
            VoucherVolume[] vvList = _boVoucherControlWS.GetVoucherVolumes();
            foreach (Isp isp in isps)
            {
                RadTreeNode rtn = new RadTreeNode(isp.CompanyName, isp.Id.ToString());
                rtn.ImageUrl = "~/Images/contract.png";
                rtn.ToolTip = isp.Id.ToString();

                //Add child nodes to the tree node
                RadTreeNode slNode = null;
                ServiceLevel[] serviceLevels = _boAccountingControlWS.GetServicePacksByIsp((int)isp.Id);

                foreach (ServiceLevel sl in serviceLevels)
                {
                    foreach (VoucherVolume vv in vvList)
                        if ((Convert.ToInt32(sl.SlaId) == vv.Sla) && (_boAccountingControlWS.IsVoucherSLA(vv.Sla)))
                        {
                            slNode = new RadTreeNode();
                            slNode.Value = vv.Id.ToString();
                            slNode.Text = sl.SlaName + vv.VolumeMB + " MB - Valid " + vv.ValidityPeriod + " days - " + vv.UnitPriceEUR.ToString("0.00") + "€ / " + vv.UnitPriceUSD.ToString("0.00") + "$";
                            rtn.Nodes.Add(slNode);
                        }
                }
                slaTree.Nodes.Add(rtn);
            }
            foreach (VoucherVolume vv in vvList)
            {
                RadComboBoxItem item = new RadComboBoxItem();
                item.Value = vv.Id.ToString();
                item.Text = vv.VolumeMB + " MB - Valid " + vv.ValidityPeriod + " days - " + vv.UnitPriceEUR.ToString("0.00") + "€ / " + vv.UnitPriceUSD.ToString("0.00") + "$";
                RadComboBoxVoucherVolume.Items.Add(item);
            }
        }
    }
}