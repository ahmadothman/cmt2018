﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FOWebApp.Nocsa
{
    public partial class VoucherRequestDetailsPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Load the details form and initialize the data
            int id = Convert.ToInt32(Request.Params["id"]);

            VoucherRequestDetailsForm df =
                (VoucherRequestDetailsForm)Page.LoadControl("VoucherRequestDetailsForm.ascx");

            df.componentDataInit(id);

            PlaceHolderVoucherRequestDetailsForm.Controls.Add(df);
        }
    }
}