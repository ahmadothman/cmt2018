﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VoucherReport.ascx.cs" Inherits="FOWebApp.Nocsa.VoucherReport" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<table cellpadding="5" cellspacing="2">
    <tr>
        <td>Start date:</td>
        <td> 
            <telerik:RadDatePicker ID="RadDatePickerStart" runat="server" Skin="Metro">
                <DateInput DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy"  runat="server"/> 
            </telerik:RadDatePicker>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Provide a start date" ControlToValidate="RadDatePickerStart" Display="Dynamic"></asp:RequiredFieldValidator>
        </td>
        <td>End date:</td>
        <td>
            <telerik:RadDatePicker ID="RadDatePickerEnd" runat="server" Skin="Metro">
                <DateInput DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy"  runat="server"/> 
            </telerik:RadDatePicker>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Provide an end date" ControlToValidate="RadDatePickerEnd"></asp:RequiredFieldValidator>
        </td>
        <td>Batch ID:</td>
        <td>
            <telerik:RadNumericTextBox ID="RadNumericTextBoxBatchId" runat="server" Width="50px" NumberFormat-DecimalDigits="0" Skin="Metro"></telerik:RadNumericTextBox>
        </td>
        <td>
            <%--<asp:ImageButton ID="ImageButtonQuery" runat="server" 
                ImageUrl="~/Images/invoice_euro.png" onclick="ImageButtonQuery_Click" 
                ToolTip="Create accounting report" />--%>
            <asp:Button ID="voucherReportButton" runat="server" 
                Text="Show" onclick="ImageButtonQuery_Click" 
                ToolTip="Create accounting report" />
        </td>
        <%--<td>
            <asp:ImageButton ID="ImageButtonCSVExport" runat="server" 
                ImageUrl="~/Images/CSV.png" onclick="ImageButtonCSVExport_Click" 
                Enabled="True" ToolTip="Export as a CSV file" />
        </td>--%>
        <td>
            <asp:ImageButton ID="ImageButtonExcelExport" runat="server" Enabled="True" 
                ImageUrl="~/Images/Excel.png" onclick="ImageButtonExcelExport_Click" 
                ToolTip="Export as an Excel file" />
        </td>
        <td>
            <asp:ImageButton ID="ImageButtonPDFExport" runat="server" Enabled="true" ImageUrl="~/Images/PDF.jpg" OnClick="ImageButtonPDFExport_Click" ToolTip="Export to PDF" />
        </td>
    </tr>
</table>
<telerik:RadGrid ID="RadGridVouchers" runat="server" AllowSorting="True"
    AutoGenerateColumns="False" Visible="false" CellSpacing="0" GridLines="None" Skin="Metro"
    ShowStatusBar="True" Width="992px" AllowFilteringByColumn="False" 
    onitemcommand="RadGridVouchers_ItemCommand" onneeddatasource="RadGridVouchers_NeedDataSource" >
        <GroupingSettings CaseSensitive="false" /> 
    <ClientSettings EnableRowHoverStyle="true">
        <Selecting AllowRowSelect="True" />
        <ClientEvents OnRowSelected="RowSelected" />
    </ClientSettings>
    <MasterTableView ShowFooter="False" ShowGroupFooter="False">
    <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
        </RowIndicatorColumn>
        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
        </ExpandCollapseColumn>
        <Columns>
            <telerik:GridBoundColumn DataField="Code" 
                FilterControlAltText="Filter CodeColumn column" HeaderText="Code" 
                UniqueName="Code" ShowFilterIcon="False" AutoPostBackOnFilter="True">
                <ItemStyle Wrap="False" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="BatchId" 
                FilterControlAltText="Filter BatchIdColumn column" HeaderText="Batch ID" 
                UniqueName="BatchId" ShowFilterIcon="False" AutoPostBackOnFilter="True">
                <ItemStyle Wrap="False" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="DateValidated" FilterControlAltText="Filter DateValidatedColumn column"
                HeaderText="Date validated on" ReadOnly="True" Resizable="False" 
                UniqueName="DateValidated" CurrentFilterFunction="Contains" DataFormatString="{0:dd/MM/yyyy}" DataType="System.DateTime"
                ShowFilterIcon="False" AutoPostBackOnFilter="True">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="MacAddress" 
                FilterControlAltText="Filter MacAddressColumn column" HeaderText="MAC Address" 
                UniqueName="MacAddress" ShowFilterIcon="False" AutoPostBackOnFilter="True">
                <ItemStyle Wrap="False" />
            </telerik:GridBoundColumn>
        </Columns>
        <EditFormSettings>
<EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
</EditFormSettings>
</MasterTableView>
    <FilterMenu EnableImageSprites="False">
    </FilterMenu>
    <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default">
    </HeaderContextMenu>
</telerik:RadGrid>