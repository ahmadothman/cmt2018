﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FOWebApp.Nocsa
{
    public partial class LogsMenuPage : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Session["LogMenuPage_LogList"] != null)
            {
                ActivityLogList activityLogList = (ActivityLogList)Page.LoadControl("~/Nocsa/ActivityLogList.ascx");
                activityLogList.ActivityType = (string)this.Session["LogMenuPage_LogList"];
                this.Controls.Clear();
                this.Controls.Add(activityLogList);
            }
        }

        protected void RadButtonUsageLog_Click(object sender, EventArgs e)
        {
            ActivityLogList activityLogList = (ActivityLogList)Page.LoadControl("~/Nocsa/ActivityLogList.ascx");
            activityLogList.ActivityType = "User_Log";
            this.Controls.Clear();
            this.Controls.Add(activityLogList);
            this.Session["LogMenuPage_LogList"] = "User_Log";
        }

        protected void RadButtonActivityLog_Click(object sender, EventArgs e)
        {
            ActivityLogList activityLogList = (ActivityLogList)Page.LoadControl("~/Nocsa/ActivityLogList.ascx");
            activityLogList.ActivityType = "Term_Activity_Log";
            this.Controls.Clear();
            this.Controls.Add(activityLogList);
        }

        protected void RadButtonExceptionLog_Click(object sender, EventArgs e)
        {
            ActivityLogList activityLogList = (ActivityLogList)Page.LoadControl("~/Nocsa/ActivityLogList.ascx");
            activityLogList.ActivityType = "Exception_Log";
            this.Controls.Clear();
            this.Controls.Add(activityLogList);
        }
    }
}