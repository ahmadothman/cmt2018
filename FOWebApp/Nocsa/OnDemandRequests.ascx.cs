﻿using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOSatLinkManagerControlWSRef;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FOWebApp.Nocsa
{
    public partial class OnDemandRequests : System.Web.UI.UserControl
    {
        BOSatLinkManagerControlWS _satLinkController = new BOSatLinkManagerControlWS();

        protected void Page_Load(object sender, EventArgs e)
        {
            RadGridSLMBookings.DataSource = _satLinkController.GetSLMBookingsByStatus("New");
            RadGridSLMBookings.DataBind();
        }


    }
}