﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FOWebApp.Nocsa
{
    public partial class RoleDetailsPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Load the details form and initialize the data
            string roleName = (string)Request.Params["roleName"];

            RoleDetailsForm df =
                (RoleDetailsForm)Page.LoadControl("~/Nocsa/RoleDetailsForm.ascx");

            df.componentDataInit(roleName);

            PlaceHolderRoleDetailsForm.Controls.Add(df);
        }
    }
}