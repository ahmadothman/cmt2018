﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Telerik.Web.UI;

namespace FOWebApp.Nocsa
{
    public partial class OutgoingEmailsLists : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string[] files = Directory.GetFiles("C:\\nimera\\logs\\volumeaccountingservice\\");
            string fileDate = "";
            FileNameListItem listItem;
            List<FileNameListItem> fileNames = new List<FileNameListItem>();
            foreach (string s in files)
            {
                string[] filePath = s.Split('_');
                fileDate = filePath[1];
                filePath = fileDate.Split('.');
                fileDate = filePath[0];
                listItem = new FileNameListItem();
                listItem.Date = fileDate;
                listItem.Date2 = fileDate.Replace("-", "/");
                listItem.FileName = s;
                fileNames.Add(listItem);
            }
            RadGridEmailReports.DataSource = fileNames;
            RadGridEmailReports.DataBind();
        }

        public class FileNameListItem
        {
            public string Date { get; set; }
            public string Date2 { get; set; }
            public string FileName { get; set; }
        }
    }
}