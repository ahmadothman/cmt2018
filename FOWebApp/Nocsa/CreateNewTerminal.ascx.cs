﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using Telerik.Web.UI;
using FOWebApp.Data;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;

namespace FOWebApp.Nocsa
{
    public partial class CreateNewTerminal : System.Web.UI.UserControl
    {
        DataHelper _dataHelper = null;
        BOAccountingControlWS _boAccountingControlWS = new BOAccountingControlWS();
        const string _HEXBYTE = "<0|1|2|3|4|5|6|7|8|9|a|b|c|d|e|f><0|1|2|3|4|5|6|7|8|9|a|b|c|d|e|f>";
        //int ispId = 112;

        public CreateNewTerminal()
        {
            _dataHelper = new DataHelper();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Create a blank input form
            RadMaskedTextBoxMacAddress.Mask =
               _HEXBYTE + "<:>" + _HEXBYTE + "<:>" + _HEXBYTE + "<:>" + _HEXBYTE + "<:>" + _HEXBYTE + "<:>" +
               _HEXBYTE;

            TextBoxSitId.Text = "";
            TextBoxSerial.Text = "";
            TextBoxTerminalName.Text = "";
            TextBoxIP.Text = "";
            RadDatePickerStartDate.SelectedDate = DateTime.Now;
            RadDatePickerExpiryDate.SelectedDate = DateTime.Now.AddDays(30);

            LabelSitIdRequired.Text = "";

            //Fill the lookup comboboxes
            List<Isp> IspList = _dataHelper.getISPs();

            foreach (Isp isp in IspList)
            {
                RadComboBoxItem item = new RadComboBoxItem();
                item.Value = isp.Id.ToString();
                item.Text = isp.CompanyName;
                RadComboBoxISP.Items.Add(item);
            }

            AddInitialValueToComboBox(RadComboBoxISP);

            FreeZoneCMT[] freeZoneList = _boAccountingControlWS.GetFreeZones();

            foreach (FreeZoneCMT fz in freeZoneList)
            {
                RadComboBoxItem item = new RadComboBoxItem();
                item.Value = fz.Id.ToString();
                item.Text = fz.Name;
                RadComboBoxFreeZone.Items.Add(item);
            }

            //if (this.Session["CreateNewTerminal_ISP"] != null)
            //{
            //    ispId = (int)this.Session["CreateNewTerminal_ISP"];
            //}

            //RadComboBoxISP.SelectedValue = "" + ispId;

            //Load distributor into the combobox
            Distributor[] distributors = _boAccountingControlWS.GetDistributorsAndVNOs();

            foreach (Distributor distributor in distributors)
            {
                RadComboBoxItem item = new RadComboBoxItem();
                item.Value = distributor.Id.ToString();
                item.Text = distributor.FullName;
                RadComboBoxDistributor.Items.Add(item);
            }

            AddInitialValueToComboBox(RadComboBoxDistributor);

            ////CMTFO-12
            ////Fill-up the treeview in the RadComboBox

            //RadTreeView distVNOTree = (RadTreeView)RadComboBoxDistributor.Items[0].FindControl("RadTreeViewDistributor");
            ////Distributors
            //RadTreeNode rtn = new RadTreeNode("Distributors", "0");
            //rtn.ImageUrl = "~/Images/businesspeople.png";
            //rtn.ToolTip = "Distributors";
            ////Distributor[] distributors = _boAccountingControlWS.GetDistributors();
            //RadTreeNode distVNONode = null;
            //foreach (Distributor dist in distributors)
            //{
            //    distVNONode = new RadTreeNode(dist.FullName, dist.Id.ToString());
            //    distVNONode.ToolTip = dist.FullName;
            //    rtn.Nodes.Add(distVNONode);
            //}

            //distVNOTree.Nodes.Add(rtn);
            
            ////VNOs
            //rtn = new RadTreeNode("VNOs", "1");
            //rtn.ImageUrl = "~/Images/magician.png";
            //rtn.ToolTip = "VNOs";
            //Distributor[] VNOs = _boAccountingControlWS.GetVNOs();
            //distVNONode = null;
            //foreach (Distributor vno in VNOs)
            //{
            //    distVNONode = new RadTreeNode(vno.FullName, vno.Id.ToString());
            //    distVNONode.ToolTip = vno.FullName;
            //    rtn.Nodes.Add(distVNONode);
            //}
            //distVNOTree.Nodes.Add(rtn);
        }

        protected void CustomValidatorMacAddress_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (args.Value == "00:00:00:00:00:00")
            {
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }
        }

        protected void ButtonSave_Click(object sender, EventArgs e)
        {
            //First check if the SitId is unique
            BOAccountingControlWS boAccountingControl =
                                        new BOAccountingControlWS();

            int ispId = Int32.Parse(RadComboBoxISP.SelectedValue);

            //When using the ISPIF the IspId is generated by the Hub
            /*try
            {
                if (ispId == 112)
                {
                    if (TextBoxSitId.Text.Equals(""))
                    {
                        LabelSitIdRequired.Text = "You must specify a SitId!";
                    }
                    else
                    {
                        int sitId = Int32.Parse(TextBoxSitId.Text);
                        if (!boAccountingControl.IsSitIdUnique(sitId, ispId))
                        {
                            LabelResult.ForeColor = Color.Red;
                            LabelResult.Text = "This SitId already exists!";
                        }
                        else
                        {
                            saveTerminalData(ispId);
                        }
                    }
                }
                else
                {
                    saveTerminalData(ispId);
                    
                }
            }*/
            try
            {
                this.saveTerminalData(ispId);
            }
            catch (Exception ex)
            {
                BOLogControlWS boLogControlWS = new BOLogControlWS();
                CmtApplicationException cmtApplicationException = new CmtApplicationException();
                cmtApplicationException.ExceptionDateTime = DateTime.Now;
                cmtApplicationException.ExceptionDesc = ex.Message;
                cmtApplicationException.ExceptionStacktrace = ex.StackTrace;
                cmtApplicationException.UserDescription = "Update new terminal";
                boLogControlWS.LogApplicationException(cmtApplicationException);

                LabelResult.ForeColor = Color.Red;
                LabelResult.Text = "Creation of terminal failed!";
            }

        }

        protected void RadXmlHttpPanelCustomer_ServiceRequest(object sender, RadXmlHttpPanelEventArgs e)
        {
            int distributorId = Int32.Parse(e.Value);
            Organization[] organizations = _boAccountingControlWS.GetOrganisationsByDistributorId(distributorId.ToString());

            if (organizations.Length != 0)
            {
                RadComboBoxCustomer.Items.Clear();

                RadComboBoxCustomer.DataSource = organizations;
                RadComboBoxCustomer.DataTextField = "FullName";
                RadComboBoxCustomer.DataValueField = "Id";
                RadComboBoxCustomer.DataBind();

                AddInitialValueToComboBox(RadComboBoxCustomer);
            }
            else
            {
                RadComboBoxCustomer.Text = "No customers defined for this Distributor";
                RadComboBoxCustomer.Enabled = false;
            }
        }

        protected void RadXmlHttpPanelSLA_ServiceRequest(object sender, RadXmlHttpPanelEventArgs e)
        {
            int ispId = Int32.Parse(e.Value);
            ServiceLevel[] slaItems = _boAccountingControlWS.GetServicePacksByIsp(ispId);
            
            if (!(slaItems.Length == 0))
            {
                RadComboBoxSLA.Items.Clear();

                RadComboBoxSLA.DataSource = slaItems;
                RadComboBoxSLA.DataTextField = "SlaName";
                RadComboBoxSLA.DataValueField = "SlaId";
                RadComboBoxSLA.DataBind();

                AddInitialValueToComboBox(RadComboBoxSLA);
            }
            else
            {
                RadComboBoxSLA.Text = "No SLAs defined for this ISP or SLAs are hidden";
                RadComboBoxSLA.Enabled = false;
            }

            //Store the isp for successive pageloads
            //this.Session["CreateNewTerminal_ISP"] = ispId;
        }

        private void saveTerminalData(int ispId)
        {
            //First check if the SitId is unique
            BOAccountingControlWS boAccountingControl =
                                        new BOAccountingControlWS();

            //Check if the MAC address exists
            Terminal terminal = boAccountingControl.GetTerminalDetailsByMAC(RadMaskedTextBoxMacAddress.Text);
            if (terminal != null)
            {
                LabelResult.ForeColor = Color.Red;
                LabelResult.Text = "MAC address already exists";
                return;
            }

            //Update data into the database
            terminal = new Terminal();
            terminal.Address = new Address();

            terminal.MacAddress = RadMaskedTextBoxMacAddress.Text;
            terminal.IspId = ispId;

            //When using the ISPIF the IspId is generated by the ISPIF
            if (!TextBoxSitId.Text.Equals(""))
            {
                terminal.SitId = Int32.Parse(TextBoxSitId.Text);
            }
            else
            {
                terminal.SitId = 0;
            }

            terminal.Serial = TextBoxSerial.Text;
            terminal.FullName = TextBoxTerminalName.Text.Trim();
            //SDPDEV-16
            //terminal.ExtFullName = TextBoxTerminalName.Text.Trim();
            terminal.ExtFullName = string.Concat(terminal.MacAddress + terminal.Serial).Replace(":","-");
            terminal.DistributorId = Int32.Parse(RadComboBoxDistributor.SelectedValue);
            terminal.OrgId = Int32.Parse(RadComboBoxCustomer.SelectedValue);
            terminal.SlaId = Int32.Parse(RadComboBoxSLA.SelectedValue);
            terminal.IpAddress = TextBoxIP.Text;
            terminal.StartDate = RadDatePickerStartDate.SelectedDate;
            terminal.ExpiryDate = RadDatePickerExpiryDate.SelectedDate;
            terminal.AdmStatus = 5;
            terminal.TestMode = CheckBoxTestMode.Checked;
            terminal.CNo = (decimal)-1.0;
            terminal.IPMask = Int32.Parse(RadComboBoxIPMask.SelectedValue);
            terminal.IPRange = CheckBoxIPRange.Checked;
            terminal.TermWeight = 100; //default weight for all new terminals as setting weights is only done on customer VNO dashboard
            terminal.StaticIPFlag = CheckBoxStaticIP.Checked;
            ServiceLevel sla = _boAccountingControlWS.GetServicePack((int)terminal.SlaId);
            terminal.FreeZone = 0;
            if (sla.FreeZone != null)
            {
                if ((bool)sla.FreeZone)
                {
                    terminal.FreeZone = Int32.Parse(RadComboBoxFreeZone.SelectedValue);
                }
            }
            //CMTFO-9
            terminal.Modem = RadComboBoxModem.SelectedItem.Text;
            terminal.iLNBBUC = RadComboBoxILNBBUC.SelectedItem.Text;
            terminal.Antenna = RadComboBoxAntenna.SelectedItem.Text;

            //SATCORP-59
            terminal.DialogTechnology = RadComboBoxDialogFrequency.Text;
            terminal.CenterFrequency = Convert.ToInt64(RadNumericCenterFrequency.Value);

            // Sets the selected value of the RadComboBox of the ISPs back to the initial value with empty text
            // That way, the information shown in the SLA Combobox is also correct after pressing the button and going to the server
            RadComboBoxISP.SelectedIndex = 0;

            if (boAccountingControl.UpdateTerminal(terminal))
            {

                //Reset the session variable
                //this.Session["CreateNewTerminal_ISP"] = null;
                LabelResult.ForeColor = Color.DarkGreen;
                LabelResult.Text = "Creation of terminal succeeded, if necessary you may now activate the terminal.";
            }
            else
            {
                LabelResult.ForeColor = Color.DarkRed;
                LabelResult.Text = "We are sorry, but the creation of terminal failed, please contact SatADSL";
            }
        }

        /// <summary>
        /// Inserts a RadComboBoxItem with an emtpy string as text and 0 as value in the RadComboBox at index 0
        /// </summary>
        /// <param name="comboBox">The RadComboBox in which to insert the RadComboBoxItem</param>
        private void AddInitialValueToComboBox(RadComboBox comboBox)
        {
            RadComboBoxItem item = new RadComboBoxItem(string.Empty, "0");
            comboBox.Items.Insert(0, item);
        }

        protected void RadComboBoxDistributor_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            RadComboBoxCustomer.Text = "";
            RadComboBoxCustomer.ClearSelection(); 
            Organization[] customers = _boAccountingControlWS.GetOrganisationsByDistributorId(RadComboBoxDistributor.SelectedValue);
            RadComboBoxCustomer.DataSource = customers;
            RadComboBoxCustomer.DataValueField = "Id";
            RadComboBoxCustomer.DataTextField = "FullName";
            RadComboBoxCustomer.DataBind();
            RadComboBoxCustomer.Items.Insert(0, new RadComboBoxItem(""));
        }
    }
}