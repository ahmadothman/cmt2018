﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using Telerik.Web.UI;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOVoucherControllerWSRef;

namespace FOWebApp.Nocsa
{
    public partial class AvailableVouchers : System.Web.UI.UserControl
    {
        BOAccountingControlWS _boAccountingControlWS = new BOAccountingControlWS();
        BOVoucherControllerWS _boVouchercontrollerWS = new BOVoucherControllerWS();
        int tBatches = 0;
        int tVouchers = 0;
        double tAmount = 0;
        

        protected void Page_Load(object sender, EventArgs e)
        {
            
            //int actMonth = DateTime.Now.Month;
            //int actYear = DateTime.Now.Year;
            //int daysInMonth = DateTime.DaysInMonth(actYear, actMonth);
            //RadDatePickerStart.SelectedDate = new DateTime(actYear, actMonth, 1);
            //RadDatePickerEnd.SelectedDate = new DateTime(actYear, actMonth, daysInMonth);
            //Load customers into the combobox
            Distributor[] distributors = _boAccountingControlWS.GetDistributors();
            RadComboBoxItem item = new RadComboBoxItem();
            item.Value = "0";
            item.Text = "All";
            RadComboBoxDistributors.Items.Add(item);
            foreach (Distributor distributor in distributors)
            {
                item = new RadComboBoxItem();
                item.Value = distributor.Id.ToString();
                item.Text = distributor.FullName;
                RadComboBoxDistributors.Items.Add(item);
            }

            try
            {
               
                RadComboBoxDistributors.SelectedValue = Session["Distributor"].ToString();
                RadDropDownListVoucherState.SelectedValue = Session["VoucherStatus"].ToString();
            }
            catch { }
        }


        protected void RadGridVouchers_Rebind()
        {
            BOVoucherControllerWS boVoucherControl = new BOVoucherControllerWS();
            //List<Voucher> vouchers = new List<Voucher>();
            int distributorID = Int32.Parse(this.RadComboBoxDistributors.SelectedValue);
            int paidState = Int32.Parse(this.RadDropDownListVoucherState.SelectedValue);
            DateTime? start = this.RadDatePickerStart.SelectedDate;
            DateTime? end = this.RadDatePickerEnd.SelectedDate;
            VoucherBatchInfo[] vbinfo = boVoucherControl.getVouchersBatchesReport(distributorID, paidState, start, end);
            RadGridVouchers.PageSize = vbinfo.Length;
            RadGridVouchers.MasterTableView.DataSource = vbinfo;
            RadGridVouchers.MasterTableView.DataBind();
            // = vouchers;
            //((RadGrid)sender).PageSize = ((RadGrid)sender).MasterTableView.PageCount;
        }

        protected void RadGridVouchers_ItemDataBound(object sender, GridItemEventArgs e)
        {

            if (e.Item is GridDataItem)
            {
                GridDataItem dataItem = (GridDataItem)e.Item;
                TableCell totalVouchers = dataItem["NumVouchers"];
                TableCell available = dataItem["Available"];
                TableCell distributorName = dataItem["Distributor"];
                TableCell description = dataItem["Volume"];
                TableCell payment = dataItem["Payment"];
                if (available.Text != "&nbsp;") //to skip the processing of the first grid (default one)
                {
                    int batchID = Int32.Parse(available.Text);
                    int distID = Int32.Parse(distributorName.Text);
                    int vvID = Int32.Parse(description.Text);
                    int totalV = Int32.Parse(totalVouchers.Text);
                    Distributor distributor = _boAccountingControlWS.GetDistributor(distID);
                    VoucherVolume vv = _boVouchercontrollerWS.GetVoucherVolumeById(vvID);
                    int availableVouchers = _boVouchercontrollerWS.getAvailableVouchersforbatch(batchID);
                    double amount = 0;


                    distributorName.Text = distributor.FullName;
                    available.Text = availableVouchers.ToString();
                    description.Text = vv.Description;

                    if (distributor.Currency == "USD")
                    {
                        amount = totalV * vv.UnitPriceUSD;
                        payment.Text = amount.ToString() + " USD";
                    }
                    else
                    {
                        amount = totalV * vv.UnitPriceEUR;
                        payment.Text = amount.ToString() + " EUR";
                    }

                    tBatches++;
                    tVouchers += totalV;
                    tAmount += totalV * vv.UnitPriceEUR;

                }
            }

        }

        protected void RadButtonSubmit_Click(object sender, EventArgs e)
        {
            //RadGridVouchers.DataSource = null; // Need to do this, otherwise the NeedDataSource event is not triggered on rebind

            if (Page.IsValid)
            {
                try
                {
                    RadGridVouchers.DataSource = null; // Need to do this, otherwise the NeedDataSource event is not triggered on rebind
                    this.RadGridVouchers_Rebind();
                    LabelNoBatch.Text = tBatches.ToString();
                    LabelNoVoucher.Text = tVouchers.ToString();
                    LabelAmount.Text = tAmount.ToString()+ " Euro";
                    tBatches = 0;
                    tVouchers = 0;
                    tAmount = 0;
                    Session["Distributor"] = RadComboBoxDistributors.SelectedValue;
                    Session["VoucherStatus"]=RadDropDownListVoucherState.SelectedValue ;
                }
                catch (Exception ex)
                {

                }
            }
        }
    }
}