﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CustomerList.ascx.cs"
    Inherits="FOWebApp.Nocsa.CustomerList" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script type="text/javascript">
    function RowSelected(sender, eventArgs) {
        var grid = sender;
        var detailTables = grid.get_detailTables();
        if (detailTables.length == 1) {
            var dataItems = detailTables[0].get_selectedItems();
            if (dataItems.length != 0) {
                var macAddress = dataItems[0].get_element().cells[1].innerHTML;
                var oWnd = radopen("Nocsa/TerminalForm.aspx?mac=" + macAddress, "RadWindowModifyTerminal");
            }
        }
        else {
            //The master table has been clicked
            var masterTable = grid.get_masterTableView();
            var dataItems = masterTable.get_selectedItems();
            if (dataItems.length != 0) {
                var custId = dataItems[0].get_element().cells[1].innerHTML;
                var oWnd = radopen("Nocsa/CustomerDetailsForm.aspx?id=" + custId, "RadWindowDetails");
            }
        }
    }
</script>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="RadGridCustomers">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGridCustomers" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadGrid ID="RadGridCustomers" runat="server" AllowPaging="True" AllowSorting="True"
    AutoGenerateColumns="False" AutoGenerateHierarchy="True" CellSpacing="0" EnableAriaSupport="True"
    GridLines="None" Skin="Metro" Width="100%" ShowStatusBar="True" OnDetailTableDataBind="RadGridCustomers_DetailTableDataBind"
    PageSize="20" OnItemCommand="RadGridCustomers_ItemCommand" OnItemDataBound="RadGridCustomers_ItemDataBound"
    OnDataBound="RadGridCustomers_DataBound" OnNeedDataSource="RadGridCustomers_NeedDataSource"
    OnPageIndexChanged="RadGridCustomers_PageIndexChanged"
    OnPageSizeChanged="RadGridCustomers_PageSizeChanged"
    OnDeleteCommand="RadGridCustomers_DeleteCommand"
    OnSortCommand="RadGridCustomers_SortCommand" EnableLinqExpressions="False">
    <ClientSettings EnableRowHoverStyle="true">
        <Selecting AllowRowSelect="True" />
        <ClientEvents OnRowSelected="RowSelected" />
    </ClientSettings>
    <MasterTableView AllowPaging="True" CommandItemDisplay="Top" CommandItemSettings-ShowAddNewRecordButton="False"
        AllowFilteringByColumn="True" DataKeyNames="Id" CommandItemSettings-ShowExportToCsvButton="True"
        CommandItemSettings-ShowExportToWordButton="True" CommandItemSettings-ShowExportToExcelButton="True">
        <DetailTables>
            <telerik:GridTableView runat="server" Name="TerminalsTableView" AllowCustomPaging="false"
                AllowSorting="true" AllowPaging="true">
                <CommandItemSettings ExportToPdfText="Export to PDF" />
                <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
                </RowIndicatorColumn>
                <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridBoundColumn DataField="FullName" FilterControlAltText="Filter FullNameColumn column"
                        HeaderText="Full Name" UniqueName="FullNameColumn" ShowFilterIcon="False">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="MacAddress" FilterControlAltText="Filter MacAddressColumn column"
                        HeaderText="MAC Address" MaxLength="12" ReadOnly="True" Resizable="False" UniqueName="MacAddressColumn"
                        CurrentFilterFunction="Contains">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="SitId" FilterControlAltText="Filter SitIdColumn column"
                        HeaderText="Site ID" UniqueName="SitIdColumn">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="IpAddress" FilterControlAltText="Filter IpAddressColumn column"
                        HeaderText="IP Address" ReadOnly="True" UniqueName="IpAddressColumn">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="SlaId" FilterControlAltText="Filter SLA column"
                        HeaderText="SLA" ReadOnly="True" UniqueName="ServiceColumn" ShowFilterIcon="False" AutoPostBackOnFilter="True">
                    </telerik:GridBoundColumn>

                    <telerik:GridBoundColumn AllowSorting="False" DataField="AdmStatus" FilterControlAltText="Filter AdmStatusColumn column"
                        HeaderText="Status" ReadOnly="True" UniqueName="AdmStatusColumn">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="StartDate" FilterControlAltText="Filter StartDateColumn column"
                        HeaderText="Start Date" ReadOnly="True" UniqueName="StartDateColumn" DataFormatString="{0:dd/MM/yyyy}">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn AllowSorting="False" DataField="ExpiryDate" DataType="System.DateTime"
                        FilterControlAltText="Filter ExpiryDateColumn column" HeaderText="Expiry Date"
                        ReadOnly="True" UniqueName="ExpiryDateColumn" DataFormatString="{0:dd/MM/yyyy}">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CNo"
                        FilterControlAltText="Filter CNo column" HeaderText="C\No" UniqueName="CNo"
                        Visible="True">
                    </telerik:GridBoundColumn>
                </Columns>
                <EditFormSettings>
                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                    </EditColumn>
                </EditFormSettings>
                <PagerStyle AlwaysVisible="True"></PagerStyle>
            </telerik:GridTableView>
        </DetailTables>
        <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
            <HeaderStyle Width="20px"></HeaderStyle>
        </RowIndicatorColumn>
        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" Visible="True">
            <HeaderStyle Width="20px"></HeaderStyle>
        </ExpandCollapseColumn>
        <Columns>
            <telerik:GridBoundColumn DataField="Id" DataType="System.Int32" FilterControlAltText="Filter IdColumn column"
                HeaderText="Id" UniqueName="Id">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="FullName" FilterControlAltText="Filter FullNameColumn column"
                HeaderText="Customer" UniqueName="FullName" ReadOnly="True"
                ItemStyle-Wrap="False">
                <ItemStyle Wrap="False"></ItemStyle>
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Phone" FilterControlAltText="Filter PhoneColumn column"
                HeaderText="Phone" UniqueName="Phone" ReadOnly="True">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Fax" FilterControlAltText="Filter FaxColumn column"
                HeaderText="Fax" UniqueName="Fax" ReadOnly="True">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Email" FilterControlAltText="Filter EmailColumn column"
                HeaderText="E-Mail" UniqueName="Email" ReadOnly="True">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="WebSite" FilterControlAltText="Filter WebSiteColumn column"
                HeaderText="WebSite" UniqueName="WebSite" ReadOnly="True">
            </telerik:GridBoundColumn>
            <telerik:GridButtonColumn HeaderText="Delete" ImageUrl="~/Images/delete.png" ConfirmText="Are you sure you want to delete this customer?"
                UniqueName="DeleteColumn" ButtonType="ImageButton" HeaderButtonType="PushButton"
                Text="Delete" CommandName="Delete">
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </telerik:GridButtonColumn>
        </Columns>
        <EditFormSettings>
            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
            </EditColumn>
        </EditFormSettings>
    </MasterTableView>
    <FilterMenu EnableImageSprites="False">
    </FilterMenu>
    <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default">
    </HeaderContextMenu>
</telerik:RadGrid>
<telerik:RadWindowManager ID="RadWindowManager1" runat="server" Skin="Metro">
    <Windows>
        <telerik:RadWindow ID="RadWindowDetails" runat="server" Modal="False" NavigateUrl="TerminalDetailsForm.aspx"
            Animation="Fade" Opacity="100" Skin="Metro" Title="Customer Details" AutoSize="False"
            Width="800px" Height="600px" KeepInScreenBounds="True" EnableShadow="True" EnableAriaSupport="True"
            VisibleStatusbar="True">
        </telerik:RadWindow>
        <telerik:RadWindow ID="RadWindowModifyTerminal" runat="server" Animation="None"
            Opacity="100" Skin="Metro" Title="Modify Terminal Data" AutoSize="False" Width="800px"
            Height="700px" KeepInScreenBounds="True" EnableShadow="True" EnableAriaSupport="True"
            VisibleStatusbar="True" Modal="False" DestroyOnClose="False">
        </telerik:RadWindow>
        <telerik:RadWindow ID="RadWindowTerminalDetails" runat="server"
            Animation="None" Opacity="100" Skin="Metro" Title="Terminal Details" AutoSize="False"
            Width="1000px" Height="700px" KeepInScreenBounds="True" EnableShadow="True" EnableAriaSupport="True"
            VisibleStatusbar="True" Modal="True" DestroyOnClose="False">
        </telerik:RadWindow>
        <telerik:RadWindow ID="RadWindowModifyActivity" runat="server" Animation="None" Opacity="100"
            Skin="Metro" Title="Modify Activity" AutoSize="False" Width="850px" Height="655px"
            KeepInScreenBounds="True" EnableShadow="True" EnableAriaSupport="True" VisibleStatusbar="True"
            Modal="False" DestroyOnClose="False">
        </telerik:RadWindow>
    </Windows>
</telerik:RadWindowManager>
