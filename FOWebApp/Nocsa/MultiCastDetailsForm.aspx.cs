﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FOWebApp.net.nimera.cmt.BOMultiCastGroupControllerWSRef;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;

namespace FOWebApp.Nocsa
{
    public partial class MultiCastDetailsForm : System.Web.UI.Page
    {
        BOMultiCastGroupControllerWS _boMultiCastGroupController = new BOMultiCastGroupControllerWS();
        BOAccountingControlWS _boAccountingController = new BOAccountingControlWS();
        public bool distChanged = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //Fill the form fields
                String id = Request.Params["id"];
                if (id != null)
                {
                    MultiCastGroup mc = _boMultiCastGroupController.GetMultiCastGroupByID(Convert.ToInt32(id));

                    TextBoxIPAddress.Text = mc.IPAddress;
                    TextBoxSourceURL.Text = mc.sourceURL;
                    TextBoxBandwidth.Text = mc.BandWidth.ToString();
                    TextBoxMacAddress.Text = mc.MacAddress.ToString();
                    TextBoxName.Text = mc.Name;
                    TextBoxPortNr.Text = mc.portNr.ToString();
                    CheckBoxState.Checked = mc.State;
                    LabelISP.Text = _boAccountingController.GetISP(mc.IspId).CompanyName;
                    RadDatePickerFirstActivationDate.SelectedDate = mc.FirstActivationDate;
                    string distId = mc.DistributorId.ToString();

                    //Fill up the distributors list
                    Distributor[] distributors = _boAccountingController.GetDistributorsAndVNOs();

                    foreach (Distributor distributor in distributors)
                    {
                        RadComboBoxItem item = new RadComboBoxItem();
                        item.Value = distributor.Id.ToString();
                        item.Text = distributor.FullName;
                        RadComboBoxDistributor.Items.Add(item);
                    }

                    if (this.Session["MulticastGroupDetails_Dist"] != null)
                    {
                        RadComboBoxDistributor.SelectedValue = this.Session["MulticastGroupDetails_Dist"].ToString();
                        distId = this.Session["MulticastGroupDetails_Dist"].ToString();
                    }
                    else
                    {
                        RadComboBoxDistributor.SelectedValue = distId;
                        this.Session["MulticastGroupDetails_Dist"] = distId;
                    }

                    //Fill up the customer list
                    Organization[] orgItems = _boAccountingController.GetOrganisationsByDistributorId(distId);

                    RadComboBoxCustomer.Items.Clear();
                    RadComboBoxCustomer.ClearSelection();
                    RadComboBoxCustomer.DataSource = orgItems;
                    RadComboBoxCustomer.DataTextField = "FullName";
                    RadComboBoxCustomer.DataValueField = "Id";
                    RadComboBoxCustomer.DataBind();

                    if (!(orgItems.Length == 0))
                    {
                        RadComboBoxItem item = new RadComboBoxItem();
                        item.Value = "0";
                        item.Text = "";
                        RadComboBoxCustomer.Items.Add(item);
                        RadComboBoxCustomer.SortItems();
                        if (!distChanged)
                        {
                            RadComboBoxCustomer.SelectedValue = mc.OrganizationId.ToString();
                        }
                        else
                        {
                            RadComboBoxCustomer.SelectedValue = "0";
                        }
                    }
                    else
                    {
                        RadComboBoxCustomer.Text = "No customers defined for this distributor";
                        RadComboBoxCustomer.Enabled = false;
                    }

                    if (mc.State)
                    {
                        RadToolBarMultiCastGroup.Items[1].Enabled = false;
                        RadToolBarMultiCastGroup.Items[2].Enabled = true;
                    }
                    else
                    {
                        RadToolBarMultiCastGroup.Items[1].Enabled = true;
                        RadToolBarMultiCastGroup.Items[2].Enabled = false;
                    }
                }
            }
        }

        protected void RadToolBarMultiCastGroup_ButtonClick(object sender, RadToolBarEventArgs e)
        {
            //Check the button commmand and execute the command
            RadToolBarItem toolbarItem = e.Item;

            MultiCastGroup mc = _boMultiCastGroupController.GetMultiCastGroupByIP(TextBoxIPAddress.Text);

                mc.Name = TextBoxName.Text;
                mc.sourceURL = TextBoxSourceURL.Text;
                mc.portNr = Int32.Parse(TextBoxPortNr.Text);
                mc.BandWidth = Int32.Parse(TextBoxBandwidth.Text);
                mc.FirstActivationDate = (DateTime)RadDatePickerFirstActivationDate.SelectedDate;
                mc.DistributorId = Int32.Parse(RadComboBoxDistributor.SelectedValue);
                mc.OrganizationId = Int32.Parse(RadComboBoxCustomer.SelectedValue);

            if (toolbarItem.Value.Equals("Save_Button"))
            {
                

                if (!_boMultiCastGroupController.UpdateMultiCastGroup(mc))
                {
                    RadWindowManager1.RadAlert("Update of Multicast group failed", 250, 100, "Update result", null);
                }
                else
                {
                    RadWindowManager1.RadAlert("Update of Multicast group succeeded", 250, 100, "Update result", null);
                }
            }

            if (toolbarItem.Value.Equals("Enable_Button"))
            {
                if (!_boMultiCastGroupController.EnableMultiCastGroup(TextBoxIPAddress.Text, TextBoxMacAddress.Text, mc.IspId, new Guid())) // requires userId / username
                {
                    RadWindowManager1.RadAlert("Enable of Multicast group failed", 250, 100, "Enable result", null);
                }
                else
                {
                    RadWindowManager1.RadAlert("Enable of Multicast group succeeded", 250, 100, "Enable result", null);
                }
            }

            if (toolbarItem.Value.Equals("Disable_Button"))
            {

                if (!_boMultiCastGroupController.DisableMultiCastGroup(TextBoxIPAddress.Text, TextBoxMacAddress.Text, mc.IspId, new Guid())) // requires userId / username
                {
                    RadWindowManager1.RadAlert("Disable of Multicast group failed", 250, 100, "Disable result", null);
                }
                else
                {
                    RadWindowManager1.RadAlert("Disable of Multicast group succeeded", 250, 100, "Disable result", null);
                }
            }
        }

        protected void RadXmlHttpPanelCustomer_ServiceRequest(object sender, RadXmlHttpPanelEventArgs e)
        {
            string distId = e.Value.ToString();
            Organization[] orgItems = _boAccountingController.GetOrganisationsByDistributorId(distId);

            RadComboBoxCustomer.Items.Clear();
            RadComboBoxCustomer.ClearSelection();
            RadComboBoxCustomer.DataSource = orgItems;
            RadComboBoxCustomer.DataTextField = "FullName";
            RadComboBoxCustomer.DataValueField = "Id";
            RadComboBoxCustomer.DataBind();

            if (!(orgItems.Length == 0))
            {
                RadComboBoxItem item = new RadComboBoxItem();
                item.Value = "0";
                item.Text = "";
                RadComboBoxCustomer.Items.Add(item);
                RadComboBoxCustomer.SortItems();
                RadComboBoxCustomer.SelectedValue = "0";
                RadComboBoxCustomer.Text = "";
            }
            else
            {
                RadComboBoxCustomer.Text = "No customers defined for this distributor";
                RadComboBoxCustomer.Enabled = false;
            }

            //Store the isp for successive pageloads
            this.Session["MulticastGroupDetails_Dist"] = distId;
            distChanged = true;
        }
    }
}