﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Web.Security;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOSatLinkManagerControlWSRef;

namespace FOWebApp.Nocsa
{
    public partial class NOCSAMenuAjax : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //Set the user name as root node of the treeview
                if (HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    string user = HttpContext.Current.User.Identity.Name;
                    RadTreeViewDistributorMenu.Nodes[0].Text = "Welcome " + user /*+ " - Tasks"*/;
                }
            }

            AboutNode.Visible = !(Boolean)Session["WhiteLabel"]; //No about node in case of a non-
                                                                 //branded CMT

            //Update the Requests and OnDemandRequests node
            BOAccountingControlWS boAccountingControl =
                                                            new BOAccountingControlWS();
            BOSatLinkManagerControlWS _satLinkController = new BOSatLinkManagerControlWS();
            RadTreeViewDistributorMenu.Nodes[0].Nodes[7].Text = "Requests - " +
                                    boAccountingControl.GetTerminalRequests().Length;
            //RadTreeViewDistributorMenu.Nodes[0].Nodes[8].Text = "On demand requests - " +
            //                        _satLinkController.GetSLMBookingsByStatus("New").Length;

            //Load the active component into the Main content placeholder
            RadTreeNode selectedNode = RadTreeViewDistributorMenu.SelectedNode;

            if (selectedNode != null)
            {
                //Get the MainContentPanel
                Control sheetContainer = scanForControl("SheetContentPlaceHolder", this);

                if (sheetContainer != null)
                {
                    Control contentContainer = sheetContainer.FindControl("MainContentPlaceHolder");

                    if (contentContainer != null)
                    {
                        contentContainer.Controls.Clear();
                        Control contentControl = this.LoadUserControl(selectedNode.Value);
                        if (contentControl != null)
                        {
                            contentControl.ID = selectedNode.Value + "_ID";
                            contentContainer.Controls.Add(contentControl);
                        }
                    }
                }
            }
        }

        protected void RadTreeViewDistributorMenu_NodeClick(object sender, Telerik.Web.UI.RadTreeNodeEventArgs e)
        {

        }

        protected Control scanForControl(string controlId, Control root)
        {
            Control resCtrl = root.FindControl(controlId);

            if (resCtrl == null)
            {
                root = root.Parent;
                if (root != null)
                {
                    resCtrl = this.scanForControl(controlId, root.Parent);
                }
            }

            return resCtrl;
        }

        protected Control LoadUserControl(string nodeSelector)
        {
            Control control = null;
            if (nodeSelector.Equals("n_Tasks"))
            {
                control = Page.LoadControl("~/Nocsa/TaskList.ascx");
            }
            else if (nodeSelector.Equals("n_ISP"))
            {
                Session["NewISP"] = false;
                control = Page.LoadControl("~/Nocsa/ISPList.ascx");
            } 
            else if (nodeSelector.Equals("n_New_ISP"))
            {
                //SDPDEV-23
                Session["NewISP"] = true;
                control = Page.LoadControl("~/Nocsa/ISPDetails.ascx");
            }
            else if (nodeSelector.Equals("n_log_off"))
            {
                Boolean branding = (Boolean)Session["WhiteLabel"]; //Keep the session variable

                //Close the session
                Session.Abandon();
                FormsAuthentication.SignOut();
                if (branding)
                {
                    Response.Redirect("Default.aspx?Branding=Wl");
                }
                else
                {
                    Response.Redirect("Default.aspx");
                }
            }
            else if (nodeSelector.Equals("n_Roles"))
            {
                control = Page.LoadControl("~/Nocsa/RoleList.ascx");
            }
            else if (nodeSelector.Equals("n_New_Role"))
            {
                control = Page.LoadControl("~/Nocsa/CreateNewRole.ascx");
            }
            else if (nodeSelector.Equals("n_VNOs"))
            {
                control = Page.LoadControl("~/Nocsa/VNOList.ascx");
            }
            else if (nodeSelector.Equals("n_New_VNO"))
            {
                VNODetailsForm df = 
                    (VNODetailsForm) Page.LoadControl("~/Nocsa/VNODetailsForm.ascx");
                control = df;
            }
            else if (nodeSelector.Equals("n_Distributors"))
            {
                control = Page.LoadControl("~/Nocsa/DistributorList.ascx");
            }
            else if (nodeSelector.Equals("n_Inactive_Distributors"))
            {
                control = Page.LoadControl("~/Nocsa/InactiveDistributorList.ascx");
            }
            else if (nodeSelector.Equals("n_New_Distributor"))
            {
                DistributorDetailsForm df =
                    (DistributorDetailsForm)Page.LoadControl("~/Nocsa/DistributorDetailsForm.ascx");
                control = df;
            }
            else if (nodeSelector.Equals("n_Search_Distributor_By_Name"))
            {
                control = Page.LoadControl("TestUserControl.ascx");
            }
            else if (nodeSelector.Equals("n_Terminals"))
            {
                control = Page.LoadControl("~/Nocsa/TerminalList.ascx"); ;
            }
            else if (nodeSelector.Equals("n_New_Terminal"))
            {
                control = Page.LoadControl("~/Nocsa/CreateNewTerminal.ascx");
            }
            else if (nodeSelector.Equals("n_New_RedundantSetup"))
            {
                control = Page.LoadControl("~/Nocsa/CreateNewRedundantSetup.ascx");
            }
            else if (nodeSelector.Equals("n_Search_By_MAC"))
            {
                control = Page.LoadControl("TerminalSearchByMac.ascx");
            }
            else if (nodeSelector.Equals("n_Search_By_SitId"))
            {
                control = Page.LoadControl("TerminalSearchBySitId.ascx");
            }
            else if (nodeSelector.Equals("n_Advanced_Search"))
            {
                control = Page.LoadControl("~/Nocsa/AdvancedSearch2.ascx");
            }
            else if (nodeSelector.Equals("n_Terminal_Alarms"))
            {
                control = Page.LoadControl("TerminalAlarms.ascx");
            }
            else if (nodeSelector.Equals("n_Customers"))
            {
                control = Page.LoadControl("~/Nocsa/CustomerList.ascx");
            }
            else if (nodeSelector.Equals("n_CustomerVNOs"))
            {
                control = Page.LoadControl("~/Nocsa/CustomerVNOList.ascx");
            }
            else if (nodeSelector.Equals("n_Search_Customer_By_Name"))
            {
                control = Page.LoadControl("TestUserControl.ascx");
            }
            else if (nodeSelector.Equals("n_New_Customer"))
            {
                control = Page.LoadControl("~/Nocsa/NewCustomerForm.ascx");
            }
            else if (nodeSelector.Equals("n_EndUsers"))
            {
                control = Page.LoadControl("~/Nocsa/EndUserList.ascx");
            }
            else if (nodeSelector.Equals("n_NewEndUser"))
            {
                control = Page.LoadControl("~/Nocsa/NewEndUser.ascx");
            }
            else if (nodeSelector.Equals("n_Requests"))
            {
                control = Page.LoadControl("~/Nocsa/TerminalActivationRequestList.ascx");
            }
            else if (nodeSelector.Equals("n_OnDemandRequests"))
            {
                control = Page.LoadControl("~/Nocsa/OnDemandRequests.ascx");
            }
            else if (nodeSelector.Equals("n_All_bookings"))
            {
                control = Page.LoadControl("~/Nocsa/AllBookings.ascx");
            }
            else if (nodeSelector.Equals("n_Activations"))
            {
                control = Page.LoadControl("TestUserControl.ascx");
            }
            else if (nodeSelector.Equals("n_Suspensions"))
            {
                control = Page.LoadControl("TestUserControl.ascx");
            }
            else if (nodeSelector.Equals("n_Account"))
            {
                control = Page.LoadControl("TestUserControl.ascx");
            }
            else if (nodeSelector.Equals("n_Logs"))
            {
                control = Page.LoadControl("BlankControl.ascx");
            }
            else if (nodeSelector.Equals("n_Usage_Log"))
            {
                ActivityLogList activityLogList = (ActivityLogList)Page.LoadControl("~/Nocsa/ActivityLogList.ascx");
                activityLogList.ActivityType = "User_Log";
                control = activityLogList;
            }
            else if (nodeSelector.Equals("n_Activity_Log"))
            {
                control = Page.LoadControl("~/Nocsa/TerminalActivityLogList.ascx");
            }
            else if (nodeSelector.Equals("n_Error_Log"))
            {
                //ActivityLogList activityLogList = (ActivityLogList)Page.LoadControl("~/Nocsa/ActivityLogList.ascx");
                //activityLogList.ActivityType = "Exception_Log";
                ErrorLogList errorLogList = (ErrorLogList)Page.LoadControl("~/Nocsa/ErrorLogList.ascx");
                errorLogList.ActivityType = "Exception_Log";
                control = errorLogList;
            }
            else if (nodeSelector.Equals("n_Tickets"))
            {
                control = Page.LoadControl("~/Nocsa/TicketLog.ascx");
            }
            else if (nodeSelector.Equals("n_New_Activity"))
            {
                NewActivityLogItem newActivityLogItem = (NewActivityLogItem)Page.LoadControl("~/Nocsa/NewActivityLogItem.ascx");
                newActivityLogItem.NewFlag = true;
                control = newActivityLogItem;
            }
            else if (nodeSelector.Equals("n_Users"))
            {
                control = Page.LoadControl("~/Nocsa/UserMaintenanceList.ascx");
            }
            else if (nodeSelector.Equals("n_New_User"))
            {
                control = Page.LoadControl("~/Nocsa/CreateUser.ascx");
            }
            else if (nodeSelector.Equals("n_Search_User_By_Name"))
            {
                control = Page.LoadControl("TestUserControl.ascx");
            }
            else if (nodeSelector.Equals("n_About"))
            {
                control = Page.LoadControl("AboutControl.ascx");
            }
            else if (nodeSelector.Equals("n_Reports_Accounting"))
            {
                control = Page.LoadControl("~/Nocsa/AccountReport.ascx");
            }
            else if (nodeSelector.Equals("n_Reports_HotspotTransactions"))
            {
                control = Page.LoadControl("~/Nocsa/MandacTransactionsReport.ascx");
            }
            else if (nodeSelector.Equals("n_Suspended_Terminals"))
            {
                control = Page.LoadControl("~/Nocsa/SuspendedTerminals.ascx");
            }
            else if (nodeSelector.Equals("n_Reports_SwitchOvers"))
            {
                control = Page.LoadControl("~/Nocsa/SwitchoverReport.ascx");
            }
            else if (nodeSelector.Equals("n_VoucherReport"))
            {
                control = Page.LoadControl("~/Nocsa/VoucherReport.ascx");
            }
            else if (nodeSelector.Equals("n_OutgoingEmailsReport"))
            {
                control = Page.LoadControl("~/Nocsa/OutgoingEmailsLists.ascx");
            }
            else if (nodeSelector.Equals("n_Change_Password"))
            {
                control = Page.LoadControl("ChangePassword.ascx");
            }
            else if (nodeSelector.Equals("n_CreateVouchers"))
            {
                control = Page.LoadControl("~/Nocsa/CreateVouchers.ascx");
            }
            else if (nodeSelector.Equals("n_VoucherRequests"))
            {
                control = Page.LoadControl("~/Nocsa/VoucherRequestList.ascx");
            }
            else if (nodeSelector.Equals("n_Available"))
            {
                control = Page.LoadControl("~/Nocsa/AvailableVouchers.ascx");
            }
            else if (nodeSelector.Equals("n_ValidateVoucher"))
            {
                control = Page.LoadControl("~/Nocsa/ValidateVoucher.ascx");
            }
            else if (nodeSelector.Equals("n_ResetVoucher"))
            {
                control = Page.LoadControl("~/Nocsa/ResetVoucher.ascx");
            }
            else if (nodeSelector.Equals("n_VoucherBatchesReport"))
            {
                control = Page.LoadControl("~/Nocsa/AvailableVouchers.ascx");
            }
            else if (nodeSelector.Equals("n_VoucherVolumes"))
            {
                control = Page.LoadControl("~/Nocsa/VoucherVolumeList.ascx");
            }
            else if (nodeSelector.Equals("n_NewVoucherVolume"))
            {
                control = Page.LoadControl("~/Nocsa/NewVoucherVolume.ascx");
            }
            else if (nodeSelector.Equals("n_NewIssue"))
            {
                control = Page.LoadControl("~/Nocsa/NewIssue.ascx");
            }
            else if (nodeSelector.Equals("n_NewCMTMessage"))
            {
                control = Page.LoadControl("~/Nocsa/CreateCMTMessage.ascx");
            }
            else if (nodeSelector.Equals("n_CMTMessages"))
            {
                control = Page.LoadControl("CMTMessages.ascx");
            }
            else if (nodeSelector.Equals("n_Service_Packs"))
            {
                control = Page.LoadControl("~/Nocsa/ServicePackList.ascx");
            }
            else if (nodeSelector.Equals("n_New_ServicePack"))
            {
                control = Page.LoadControl("~/Nocsa/ServicePackUpdatev2.ascx");
            }
            else if (nodeSelector.Equals("n_ServicePackWizard"))
            {
                control = Page.LoadControl("ServicePackWizard.ascx");
            }
            //CMTFO-88: not ready for implementing
            else if (nodeSelector.Equals("n_TestServicePackWizard"))
            {
                control = Page.LoadControl("TestServicePackWizard.ascx");
            }
            else if (nodeSelector.Equals("n_GlobalSettings"))
            {
                control = Page.LoadControl("~/Nocsa/GlobalSettings.ascx");
            }
            else if (nodeSelector.Equals("n_FreeZones"))
            {
                control = Page.LoadControl("~/Nocsa/FreeZoneList.ascx");
            }
            else if (nodeSelector.Equals("n_New_FreeZone"))
            {
                control = Page.LoadControl("~/Nocsa/NewFreeZone.ascx");
            }
            else if (nodeSelector.Equals("n_ConnectedDevices"))
            {
                control = Page.LoadControl("~/Nocsa/ConnectedDevicesList.ascx");
            }
            else if (nodeSelector.Equals("n_New_ConnectedDevice"))
            {
                control = Page.LoadControl("~/Nocsa/NewConnectedDevice.ascx");
            }
            else if (nodeSelector.Equals("n_Services"))
            {
                control = Page.LoadControl("~/Nocsa/Services.ascx");
            }
            else if (nodeSelector.Equals("n_Create_New_Service"))
            {
                control = Page.LoadControl("~/Nocsa/CreateNewService.ascx");
            }
            else if (nodeSelector.Equals("n_NMSDashboard"))
            {
                control = Page.LoadControl("~/Nocsa/NMSDashboard.ascx");
            }
            else if (nodeSelector.Equals("n_Multicast"))
            {
                control = Page.LoadControl("~/Nocsa/MultiCastGroupList.ascx");
            }
            else if (nodeSelector.Equals("n_NewMulticast"))
            {
                control = Page.LoadControl("~/Nocsa/NewMultiCastGroup.ascx");
            }
            else if (nodeSelector.Equals("n_Roles"))
            {
                control = Page.LoadControl("~/Nocsa/RoleList.ascx");
            }
            else if (nodeSelector.Equals("n_New_Role"))
            {
                control = Page.LoadControl("~/Nocsa/NewRole.ascx");
            }
            else if (nodeSelector.Equals("n_UploadDoc"))
            {
                control = Page.LoadControl("~/Nocsa/UploadDocumentForm.ascx");
            }
            else if (nodeSelector.Equals("n_Library"))
            {
                control = Page.LoadControl("~/Nocsa/DocumentList.ascx");
            }
            else
            {
                control = Page.LoadControl("BlankControl.ascx");
            }

            return control;
        }
    }
}