﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FOWebApp.net.nimera.cmt.BOConnectedDevicesControlWSRef;
using Telerik.Web.UI;
using System.Drawing;

namespace FOWebApp.Nocsa
{
    public partial class NewConnectedDevice : System.Web.UI.UserControl
    {
        const string _HEXBYTE = "<0|1|2|3|4|5|6|7|8|9|a|b|c|d|e|f><0|1|2|3|4|5|6|7|8|9|a|b|c|d|e|f>";
        BOConnectedDevicesControlWS _connectedDevicesControl;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            RadMaskedTextBoxMacAddress.Mask =
               _HEXBYTE + "<:>" + _HEXBYTE + "<:>" + _HEXBYTE + "<:>" + _HEXBYTE + "<:>" + _HEXBYTE + "<:>" +
               _HEXBYTE;

            _connectedDevicesControl = new BOConnectedDevicesControlWS();

            ConnectedDeviceType[] deviceTypes = _connectedDevicesControl.GetAllConnectedDeviceTypes();
            foreach (ConnectedDeviceType type in deviceTypes)
            {
                RadComboBoxItem item = new RadComboBoxItem();
                item.Value = type.ID.ToString();
                item.Text = type.Name;
                RadComboBoxDeviceType.Items.Add(item);   
            }
        }

        protected void ButtonAdd_Click(object sender, EventArgs e)
        {
            ConnectedDevice device = new ConnectedDevice();
            device.DeviceId = TextBoxDeviceId.Text;
            device.DeviceName = TextBoxDeviceName.Text;
            device.DeviceType = RadComboBoxDeviceType.SelectedItem.Value;
            device.macAddress = RadMaskedTextBoxMacAddress.Text;

            
            try
            {
                if (_connectedDevicesControl.AddDeviceToTerminal(device))
                {
                    LabelAdd.Text = "Device was successfully connected to terminal";
                    LabelAdd.ForeColor = Color.Green;
                }
            }
            catch (Exception ex)
            {
                LabelAdd.Text = "Adding device failed";
                LabelAdd.ForeColor = Color.Red;
            }
        }
    }
}