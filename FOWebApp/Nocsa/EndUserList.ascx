﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EndUserList.ascx.cs" Inherits="FOWebApp.Nocsa.EndUserList" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script type="text/javascript">
    function RowSelected(sender, eventArgs) {
        var grid = sender;
        var masterDataView = grid.get_masterTableView();
        var rowNum = eventArgs.get_itemIndexHierarchical();
        var row = masterDataView.get_dataItems()[rowNum];
        var EndUserId = masterDataView.getCellByColumnUniqueName(row, "IdColumn").innerHTML;

        //Initialize and show the terminal details window
        var oWnd = radopen("Nocsa/EndUserForm.aspx?id=" + EndUserId, "RadWindowEndUserDetails");
    }
</script>
<telerik:RadGrid ID="RadGridEndUsers" runat="server" AllowPaging="True" 
    AutoGenerateColumns="False" CellSpacing="0" GridLines="None" 
    onneeddatasource="RadGridEndUsers_NeedDataSource" PageSize="50" 
    Skin="Metro" AllowFilteringByColumn="True" AllowSorting="True" 
    ShowStatusBar="True" onpageindexchanged="RadGridEndUsers_PageIndexChanged" 
    onsortcommand="RadGridEndUsers_SortCommand" 
    ondeletecommand="RadGridEndUsers_DeleteCommand">
<GroupingSettings CaseSensitive="false" /> 
<ClientSettings EnableRowHoverStyle="true">
        <Selecting AllowRowSelect="True" />
        <ClientEvents OnRowSelected="RowSelected" />
</ClientSettings>
<MasterTableView AllowPaging="False" PageSize="10" 
        RowIndicatorColumn-Visible="True" CommandItemDisplay="Top"
CommandItemSettings-ShowAddNewRecordButton="False" DataKeyNames="Id">
<CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>

<RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
<HeaderStyle Width="20px"></HeaderStyle>
</RowIndicatorColumn>

<ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
<HeaderStyle Width="20px"></HeaderStyle>
</ExpandCollapseColumn>

    <Columns>
        <telerik:GridBoundColumn DataField="Id" 
            FilterControlAltText="Filter IdColumn column" HeaderText="Id" 
            UniqueName="IdColumn" AllowSorting="False">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="FirstName" 
            FilterControlAltText="Filter FirstNameColumn column" HeaderText="First Name" 
            UniqueName="FirstNameColumn" AllowSorting="False">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="LastName" 
            FilterControlAltText="Filter LastNameColumn column" HeaderText="Last Name" 
            UniqueName="LastNameColumn">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="Email" 
            FilterControlAltText="Filter EMailColumn column" HeaderText="E-Mail" 
            UniqueName="EMailColumn" AllowSorting="False">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="Phone" 
            FilterControlAltText="Filter PhoneColumn column" HeaderText="Phone" 
            UniqueName="PhoneColumn" AllowSorting="False">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="MobilePhone" 
            FilterControlAltText="Filter MobilePhoneColumn column" HeaderText="Mobile" 
            UniqueName="MobilePhoneColumn" AllowSorting="False">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="SitId" 
            FilterControlAltText="Filter SitIdColumn column" HeaderText="Sit Id" 
            UniqueName="SitIdColumn">
        </telerik:GridBoundColumn>
        <telerik:GridButtonColumn 
            HeaderText="Delete" ImageUrl="~/Images/delete.png" 
            ConfirmText="Are you sure you want to delete this End User?" 
            UniqueName="DeleteColumn" ButtonType="ImageButton"
            HeaderButtonType="PushButton" Text="Delete" CommandName="Delete">
            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
        </telerik:GridButtonColumn>
    </Columns>

<EditFormSettings>
<EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
</EditFormSettings>
</MasterTableView>

<FilterMenu EnableImageSprites="False"></FilterMenu>

<HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default"></HeaderContextMenu>
</telerik:RadGrid>
<telerik:RadWindowManager ID="RadWindowManager1" runat="server" Skin="Metro" DestroyOnClose="True">
    <Windows>
        <telerik:RadWindow ID="RadWindowEndUserDetails" runat="server" 
            Animation="None" Opacity="100" Skin="Metro" Title="End User Details" AutoSize="False"
            Width="600px" Height="450px" KeepInScreenBounds="True" EnableShadow="True" EnableAriaSupport="True"
            VisibleStatusbar="True" Modal="False" DestroyOnClose="False">
        </telerik:RadWindow>
    </Windows>
</telerik:RadWindowManager>