﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FOWebApp.Nocsa;

namespace FOWebApp.Nocsa
{
    public partial class DistributorDetailsPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Load the details form and initialize the data
            int id = Int32.Parse(Request.Params["Id"]);

            DistributorDetailsForm df = 
                (DistributorDetailsForm)Page.LoadControl("~/Nocsa/DistributorDetailsForm.ascx");

            df.componentDataInit(id);

            PlaceHolderDistributorDetailsForm.Controls.Add(df);
        }
    }
}