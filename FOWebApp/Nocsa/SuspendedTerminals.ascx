﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SuspendedTerminals.ascx.cs" 
    Inherits="FOWebApp.Nocsa.SuspendedTerminals" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<table cellpadding="5" cellspacing="2">
    <tr>
        <td>Start date:</td>
        <td> 
            <telerik:RadDatePicker ID="RadDatePickerStart" runat="server" Skin="Metro">
                <DateInput DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy"  runat="server"/> 
            </telerik:RadDatePicker>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Provide a start date" ControlToValidate="RadDatePickerStart" Display="Dynamic"></asp:RequiredFieldValidator>
        </td>
        <td>End date:</td>
        <td>
            <telerik:RadDatePicker ID="RadDatePickerEnd" runat="server" Skin="Metro">
                <DateInput DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy"  runat="server"/> 
            </telerik:RadDatePicker>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Provide an end date" ControlToValidate="RadDatePickerEnd"></asp:RequiredFieldValidator>
        </td>
        <td>
            <%--<asp:ImageButton ID="ImageButtonQuery" runat="server" 
                ImageUrl="~/Images/invoice_euro.png" ToolTip="Show user log" 
                onclick="ImageButtonQuery_Click" />--%>
            <asp:Button ID="suspendedLogButton" Text="Show" runat="server" ToolTip="Show user log" 
                onclick="ImageButtonQuery_Click"/>
        </td>
        <td>
            <asp:ImageButton ID="ImageButtonCSVExport" runat="server" 
                ImageUrl="~/Images/CSV.png" onclick="ImageButtonExport_Click" 
                Enabled="True" ToolTip="Export as a CSV file" />
        </td>
        <td>
            <asp:ImageButton ID="ImageButtonExcelExport" runat="server" Enabled="True" 
                ImageUrl="~/Images/Excel.png" onclick="ImageButtonExport_Click" 
                ToolTip="Export as an Excel file" />
        </td>
    </tr>
</table>
<telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" Height="800px" Width="90%">
    <telerik:RadGrid ID="RadGridSuspendedTerminals" runat="server"
        CellSpacing="0" GridLines="None" Skin="Metro" Width="90%" 
        OnNeedDatasource="RadGridSuspendedTerminals_NeedDataSource" AutoGenerateColumns="false">
    <ExportSettings IgnorePaging="True" OpenInNewWindow="True">
        <Pdf Author="CMT" Creator="CMT"
            DefaultFontFamily="Arial Unicode MS" FontType="Embed" PageBottomMargin="20mm" 
            PageFooterMargin="20mm" PageHeight="297mm" PageLeftMargin="20mm" 
            PageRightMargin="20mm" 
            PageWidth="210mm" PaperSize="A4" Producer="SatADSL" />
        <Csv ColumnDelimiter="Semicolon" EncloseDataWithQuotes="False" FileExtension="CSV"/>
    </ExportSettings>
        <MasterTableView CommandItemDisplay="Top" RowIndicatorColumn-Visible="True"
            RowIndicatorColumn-Resizable="True" RowIndicatorColumn-AndCurrentFilterFunction="Contains"
            AllowMultiColumnSorting="True" CanRetrieveAllData="True" AllowFilteringByColumn="False"
            AllowSorting="false" AutoGenerateColumns="true">
            <CommandItemSettings  ExportToPdfText="Export to PDF" 
                ShowAddNewRecordButton="False" ShowExportToCsvButton="False" 
                ShowExportToExcelButton="False" ShowExportToPdfButton="False" 
                ShowExportToWordButton="False" ShowRefreshButton="False"/>
        </MasterTableView>
    </telerik:RadGrid>
</telerik:RadAjaxPanel>
