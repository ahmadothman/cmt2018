﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CreateCMTMessage.ascx.cs" Inherits="FOWebApp.Nocsa.CreateCMTMessage" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script language="javascript" type="text/javascript">
    function nodeClickingUsers(sender, args) {
        var comboBox = $find("<%= RadComboBoxUsers.ClientID %>");

     var node = args.get_node()
     if (node.get_level() != 0) {

         comboBox.set_text(node.get_text());
         comboBox.set_value(node.get_value());
         document.getElementById('HiddenFieldDistributorId').value = node.get_value();
         comboBox.trackChanges();
         comboBox.get_items().getItem(0).set_text(node.get_text());
         comboBox.commitChanges();

         comboBox.hideDropDown();
     }
 }
</script>
<table cellspacing="5px">
    <tr>
        <td>
            <table cellspacing="5px">
                <tr>
                    <td><b>Send message to:</b></td>
                    <td>
                        <asp:RadioButton runat="server" Checked="false" value="1" ID="RadioButtonEveryone" Text="Everyone" GroupName="RadioButtonRecipient" />
                        <br /><asp:RadioButton runat="server" Checked="false" value="2" ID="RadioButtonVNOs" Text="All VNOs" GroupName="RadioButtonRecipient" />
                        <br /><asp:RadioButton runat="server" Checked="false" value="3" ID="RadioButtonDistributors" Text="All Distributors" GroupName="RadioButtonRecipient" />
                        <br /><asp:RadioButton runat="server" Checked="false" value="7" ID="RadioButtonSuspendedDistributors" Text="Suspended Distributors" GroupName="RadioButtonRecipient" />
                        <br /><asp:RadioButton runat="server" Checked="false" value="4" ID="RadioButtonCustomers" Text="All Customers" GroupName="RadioButtonRecipient" />
                        <br /><asp:RadioButton runat="server" Checked="false" value="5" ID="RadioButtonEndUsers" Text="All End Users" GroupName="RadioButtonRecipient" />
                        <br /><asp:RadioButton runat="server" Checked="false" value="6" ID="RadioButtonSpecific" Text="Specific:" GroupName="RadioButtonRecipient" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<telerik:RadComboBox ID="RadComboBoxUsers" runat="server" Skin="Metro" Sort="Ascending" Width="430px" EnableVirtualScrolling="True" EmptyMessage="Select a specific recipient"
                                MaxHeight="300px" Height="300px" ExpandDirection="Down">
                                <ItemTemplate>
                                    <telerik:RadTreeView ID="RadTreeViewUsers" runat="server" Skin="Metro" OnClientNodeClicking="nodeClickingUsers">
                                    </telerik:RadTreeView>
                                </ItemTemplate>
                                <Items>
                                    <telerik:RadComboBoxItem Text="" />
                                </Items>
                                <ExpandAnimation Type="Linear" />
                            </telerik:RadComboBox>
                        <br /><asp:label id="LabelSelectRecipient" runat="server"></asp:label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:CheckBox ID="CheckBoxRelease" runat="server" Font-Bold="True" Text="Release" TextAlign="Left" ToolTip="Check if you are ready to release this message" /></td>
                </tr>
                <tr>
                    <td><b>Message start-time:</b></td>
                    <td>
                        <telerik:RadDateTimePicker ID="RadDateTimePickerStartTime" runat="server">
                        </telerik:RadDateTimePicker>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Please provide a start-time" ControlToValidate="RadDateTimePickerStartTime"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td><b>Message end-time:</b></td>
                    <td>
                        <telerik:RadDateTimePicker ID="RadDateTimePickerEndTime" runat="server">
                        </telerik:RadDateTimePicker>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please provide an end-time" ControlToValidate="RadDateTimePickerEndTime"></asp:RequiredFieldValidator>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td><b>&nbsp;Message title:</b>&nbsp;&nbsp;<asp:TextBox ID="TextBoxTitle" runat="server" Width="400px"></asp:TextBox></td>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please provide a title" ControlToValidate="TextBoxTitle"></asp:RequiredFieldValidator>
    </tr>
    <tr>
        <td><b>&nbsp;Message content:</b></td>
    </tr>
    <tr>
        <td>
            <telerik:RadEditor ID="RadEditorMessage" runat="server" 
                    ImageManager-ViewPaths="~/Content/Images" ImageManager-UploadPaths="~/Content/Images" 
                            DocumentManager-ViewPaths="~/Content/Documents" DocumentManager-UploadPaths="~/Content/Documents" DocumentManager-MaxUploadFileSize="5000000">
                <Tools>
                    <telerik:EditorToolGroup Tag="MainToolbar">
                        <telerik:EditorTool Name="Print" ShortCut="CTRL+P" />
                        <telerik:EditorTool Name="AjaxSpellCheck" />
                        <telerik:EditorTool Name="FindAndReplace" ShortCut="CTRL+F" />
                        <telerik:EditorTool Name="SelectAll" ShortCut="CTRL+A" />
                        <telerik:EditorTool Name="Cut" />
                        <telerik:EditorTool Name="Copy" ShortCut="CTRL+C" />
                        <telerik:EditorTool Name="Paste" ShortCut="CTRL+V" />
                        <telerik:EditorSeparator />
                        <telerik:EditorSplitButton Name="Undo">
                        </telerik:EditorSplitButton>
                        <telerik:EditorSplitButton Name="Redo">
                        </telerik:EditorSplitButton>
                    </telerik:EditorToolGroup>
                    <telerik:EditorToolGroup Tag="InsertToolbar">
                        <telerik:EditorTool Enabled="True" Name="ImageManager" ShortCut="CTRL+G" />
                        <telerik:EditorTool Enabled="True" Name="DocumentManager" />
                        <telerik:EditorTool Name="LinkManager" ShortCut="CTRL+K" />
                        <telerik:EditorTool Name="Unlink" ShortCut="CTRL+SHIFT+K" />
                    </telerik:EditorToolGroup>
                    <telerik:EditorToolGroup>
                        <telerik:EditorTool Name="Superscript" />
                        <telerik:EditorTool Name="Subscript" />
                        <telerik:EditorTool Name="InsertHorizontalRule" />
                        <telerik:EditorSeparator />
                        <telerik:EditorTool Name="FormatCodeBlock" />
                    </telerik:EditorToolGroup>
                    <telerik:EditorToolGroup>
                        <telerik:EditorDropDown Name="FormatBlock">
                        </telerik:EditorDropDown>
                        <telerik:EditorDropDown Name="FontName">
                        </telerik:EditorDropDown>
                        <telerik:EditorDropDown Name="RealFontSize">
                        </telerik:EditorDropDown>
                    </telerik:EditorToolGroup>
                    <telerik:EditorToolGroup>
                        <telerik:EditorTool Name="Bold" ShortCut="CTRL+B" />
                        <telerik:EditorTool Name="Italic" ShortCut="CTRL+I" />
                        <telerik:EditorTool Name="Underline" ShortCut="CTRL+U" />
                        <telerik:EditorTool Name="StrikeThrough" />
                        <telerik:EditorSeparator />
                        <telerik:EditorTool Name="JustifyLeft" />
                        <telerik:EditorTool Name="JustifyCenter" />
                        <telerik:EditorTool Name="JustifyRight" />
                        <telerik:EditorTool Name="JustifyFull" />
                        <telerik:EditorTool Name="JustifyNone" />
                        <telerik:EditorSeparator />
                        <telerik:EditorTool Name="Indent" />
                        <telerik:EditorTool Name="Outdent" />
                        <telerik:EditorSeparator />
                        <telerik:EditorTool Name="InsertOrderedList" />
                        <telerik:EditorTool Name="InsertUnorderedList" />
                        <telerik:EditorSeparator />
                        <telerik:EditorTool Name="ToggleTableBorder" />
                    </telerik:EditorToolGroup>
                    <telerik:EditorToolGroup>
                        <telerik:EditorSplitButton Name="ForeColor">
                        </telerik:EditorSplitButton>
                        <telerik:EditorSplitButton Name="BackColor">
                        </telerik:EditorSplitButton>
                        <telerik:EditorToolStrip Name="FormatStripper">
                        </telerik:EditorToolStrip>
                    </telerik:EditorToolGroup>
                    <telerik:EditorToolGroup Tag="DropdownToolbar">
                        <telerik:EditorSplitButton Name="InsertSymbol">
                        </telerik:EditorSplitButton>
                        <telerik:EditorToolStrip Name="InsertTable">
                        </telerik:EditorToolStrip>
                        <telerik:EditorSeparator />
                        <telerik:EditorTool Name="ConvertToLower" />
                        <telerik:EditorTool Name="ConvertToUpper" />
                        <telerik:EditorSeparator />
                        <telerik:EditorTool Name="ToggleScreenMode" ShortCut="F11" />
                        <telerik:EditorTool Name="AboutDialog" />
                    </telerik:EditorToolGroup>
                </Tools>
                <Content>
                </Content>

                <TrackChangesSettings CanAcceptTrackChanges="False"></TrackChangesSettings>
            </telerik:RadEditor>
        </td>
    </tr>
    <tr>
        <td>
            <asp:button ID="ButtonSubmit" runat="server" text="Submit" OnClick="ButtonSubmit_Click"></asp:button>&nbsp;<asp:Label ID="LabelResult" runat="server"></asp:Label><br />
        </td>
    </tr>
</table>