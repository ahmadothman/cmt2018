﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CreateNewTerminal.ascx.cs"
    Inherits="FOWebApp.Nocsa.CreateNewTerminal" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script language="javascript" type="text/javascript">
    function OnClientSelectedIndexChanged_ISP(sender, eventArgs) {
        var item = eventArgs.get_item();
        //sender.set_text("You selected ISP with Id: " + item.get_value());

        var panel = $find("<%=RadXmlHttpPanelSLA.ClientID %>");
        panel.set_value(item.get_value());
        return false;
    }

    function OnClientSelectedIndexChanged_Distributor(sender, eventArgs) {
        var item = eventArgs.get_item();
        //sender.set_text("You selected ISP with Id: " + item.get_value());

        var rcb = $find("<%=RadXmlHttpPanelCustomer.ClientID %>");
        rcb.set_value(item.get_value());
        return false;
    }

    function nodeClickingDist(sender, args) {
        var comboBox = $find("<%= RadComboBoxDistributor.ClientID %>");

        var node = args.get_node()
        if (node.get_level() == 1) {

            comboBox.set_text(node.get_text());
            comboBox.set_value(node.get_value());
            document.getElementById('HiddenFieldDistributorId').value = node.get_value();
            comboBox.trackChanges();
            comboBox.get_items().getItem(0).set_text(node.get_text());
            comboBox.commitChanges();

            comboBox.hideDropDown();
        }
    }

    function nodeClicking(sender, args) {
        var comboBox = $find("<%= RadComboBoxSLA.ClientID %>");

        var node = args.get_node()
        if (node.get_level() == 1) {
            comboBox.set_text(node.get_text());
            comboBox.set_value(node.get_value());
            document.getElementById('HiddenFieldSLAId').value = node.get_value();
            comboBox.trackChanges();
            comboBox.get_items().getItem(0).set_text(node.get_text());
            comboBox.commitChanges();

            comboBox.hideDropDown();
        }
    }

    function IPRangeChanged(chkBoxId) {
        if (chkBoxId.checked == true) {
            document.getElementById('<%= RadComboBoxIPMask.ClientID %>').disabled = false;
            ValidatorEnable(document.getElementById('<%= RequiredFieldValidatorStartIP.ClientID %>'), true);
        }
        else {
            document.getElementById('<%= RadComboBoxIPMask.ClientID %>').disabled = true;
            ValidatorEnable(document.getElementById('<%= RequiredFieldValidatorStartIP.ClientID %>'), false);
        }
    }

    function MacInvalidValue(sender, eventArgs) {
        if (eventArgs.Value === '00:00:00:00:00:00') {
            eventArgs.IsValid = false;
        } else {
            eventArgs.IsValid = true;
        }
    }

    function OnMacAddressChanged(sender, eventArgs) {
        var macAddress = eventArgs.get_newValue();
        FOWebApp.WebServices.AccountSupportWS.IsMacAddressUnique(macAddress, OnRequestComplete, OnError);
        return false;
    }

    function OnRequestComplete(result) {
        if (result == false) {
            document.getElementById('<%= LabelMacExists.ClientID %>').innerHTML = "Sorry but this MAC address already exists!";
        }
        else {
            document.getElementById('<%= LabelMacExists.ClientID %>').innerHTML = "";
        }
    }

    function OnError(result) {
        document.getElementById('<%= LabelMacExists.ClientID %>').innerHTML = "An error occured while checking the MAC address";
    }
</script>
<table cellpadding="10" cellspacing="5">
    <tr>
        <td class="requiredLabel">Mac Address:
        </td>
        <td>
            <telerik:RadMaskedTextBox ID="RadMaskedTextBoxMacAddress" runat="server" Skin="Metro"
                ControlToValidate="RadMaskedTextBoxMacAddress" ClientEvents-OnValueChanged="OnMacAddressChanged">
            </telerik:RadMaskedTextBox>
            <asp:CustomValidator ID="CustomValidatorMacAddress" runat="server" ErrorMessage="The MAC address can't be 00:00:00:00:00:00"
                ControlToValidate="RadMaskedTextBoxMacAddress" ClientValidationFunction="MacInvalidValue" OnServerValidate="CustomValidatorMacAddress_ServerValidate"
                Display="Dynamic" />
            <asp:Label ID="LabelMacExists" runat="server" ForeColor="Red"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="requiredLabel">ISP:
        </td>
        <td>
            <telerik:RadComboBox ID="RadComboBoxISP" runat="server" Skin="Metro" OnClientSelectedIndexChanged="OnClientSelectedIndexChanged_ISP"
                Width="350px">
            </telerik:RadComboBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorIsp" runat="server" ErrorMessage="Please select an ISP"
                ControlToValidate="RadComboBoxISP" />
        </td>
    </tr>
    <!--
    <tr>
        <td>Sit Id:
        </td>
        <td>
            <asp:TextBox ID="TextBoxSitId" runat="server"></asp:TextBox>
            <asp:Label ID="LabelSitIdRequired" runat="server" ForeColor="#FF3300"></asp:Label>
        </td>
    </tr>
    -->
    <tr>
        <td>Dialog Technology:
        </td>
        <td>
            <telerik:RadComboBox ID="RadComboBoxDialogFrequency" runat="server" Skin="Metro">
                <Items>
                    <telerik:RadComboBoxItem Text="" Value="0"/>
                    <telerik:RadComboBoxItem Text="MxDMA" Value="1"/>
                    <telerik:RadComboBoxItem Text="SCPC" Value="2"/>
                </Items>
            </telerik:RadComboBox>
        </td>
    </tr>
    <tr>
        <td>Centre Frequency:
        </td>
        <td>
            <telerik:RadNumericTextBox ID="RadNumericCenterFrequency" runat="server" Width="75px" NumberFormat-DecimalDigits="0"></telerik:RadNumericTextBox>
        </td>
    </tr>
    <tr>
        <td class="requiredLabel">Serial:
        </td>
        <td>
            <asp:TextBox ID="TextBoxSerial" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please provide the serial number!"
                ControlToValidate="TextBoxSerial"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td class="requiredLabel">Terminal Name:
        </td>
        <td>
            <asp:TextBox ID="TextBoxTerminalName" runat="server" Width="425px" MaxLength="50"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Please provide a unique terminal name!"
                ControlToValidate="TextBoxTerminalName" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidatorTerminalName" runat="server"
                ErrorMessage="The terminal name can only contain characters, digits, spaces and the symbols '-' and '_'. It also has a maximal length of 50 characters."
                ValidationExpression="[A-Za-z\d-_ ]{1,50}" ControlToValidate="TextBoxTerminalName" />
        </td>
    </tr>
    <tr>
        <td class="requiredLabel">Modem:
        </td>
        <td>
            <telerik:RadComboBox ID="RadComboBoxModem" runat="server" Skin="Metro" Width="100px">
                <Items>
                    <telerik:RadComboBoxItem Text="" Value="0" />
                    <telerik:RadComboBoxItem Text="NTC/2250" Value="1" />
                    <telerik:RadComboBoxItem Text="MDM/2200" Value="2" />
                    <telerik:RadComboBoxItem Text="MDM/3100" Value="3" />
                    <telerik:RadComboBoxItem runat="server" Text="iD_X1" Value="4" />
                    <telerik:RadComboBoxItem runat="server" Text="iD_X3" Value="5" />
                    <telerik:RadComboBoxItem runat="server" Text="iD_X5" Value="6" />
                    <telerik:RadComboBoxItem runat="server" Text="iD_X7" Value="7" />
                </Items>
            </telerik:RadComboBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorModem" runat="server" ErrorMessage="Please select a modem"
                ControlToValidate="RadComboBoxModem" />
        </td>
    </tr>
    <tr>
        <td class="requiredLabel">iLNB/BUC:
        </td>
        <td>
            <telerik:RadComboBox ID="RadComboBoxILNBBUC" runat="server" Skin="Metro" Width="100px">
                <Items>
                    <telerik:RadComboBoxItem Text="" Value="0" />
                    <telerik:RadComboBoxItem Text="500mW" Value="1" />
                    <telerik:RadComboBoxItem Text="800mW" Value="2" />
                    <telerik:RadComboBoxItem Text="2W" Value="3" />
                    <telerik:RadComboBoxItem Text="3W" Value="4" />
                    <telerik:RadComboBoxItem Text="4W" Value="5" />
                </Items>
            </telerik:RadComboBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorILNBBUC" runat="server" ErrorMessage="Please select an iLNB/BUC"
                ControlToValidate="RadComboBoxILNBBUC" />
        </td>
    </tr>
    <tr>
        <td class="requiredLabel">Antenna:
        </td>
        <td>
            <telerik:RadComboBox ID="RadComboBoxAntenna" runat="server" Skin="Metro" Width="100px">
                <Items>
                    <telerik:RadComboBoxItem Text="" Value="0" />
                    <telerik:RadComboBoxItem Text="1m" Value="1" />
                    <telerik:RadComboBoxItem Text="1.2m" Value="2" />
                    <telerik:RadComboBoxItem Text="1.8m" Value="3" />
                    <telerik:RadComboBoxItem Text="2.4m" Value="4" />
                    <telerik:RadComboBoxItem Text="Other" Value="5" />
                </Items>
            </telerik:RadComboBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorAntenna" runat="server" ErrorMessage="Please select an antenna"
                ControlToValidate="RadComboBoxAntenna" />
        </td>
    </tr>
    <tr>
        <td>Not-Billable:
        </td>
        <td>
            <asp:CheckBox ID="CheckBoxTestMode" runat="server" />
        </td>
    </tr>
    <tr>
        <%--CMTFO-12--%>
        <td class="requiredLabel">Distributor:
        </td>
        <td>
            <%--<telerik:RadComboBox ID="RadComboBoxDistributor" runat="server" Skin="Metro" ExpandDirection="Up"
                Height="200px" Width="430px">
            </telerik:RadComboBox>--%>

           <%-- CMTFO-12
            <telerik:RadComboBox ID="RadComboBoxDistributorVNO" runat="server" Skin="Metro" Sort="Ascending" Width="430px" EnableVirtualScrolling="True" EmptyMessage="Select a Distributor or VNO"
                MaxHeight="300px">
                <ItemTemplate>
                    <telerik:RadTreeView ID="RadTreeViewDistributorVNO" runat="server" Skin="Metro" OnClientNodeClicking="nodeClickingDistVNO">
                    </telerik:RadTreeView>
                </ItemTemplate>
                <Items>
                    <telerik:RadComboBoxItem Text="" />
                </Items>
                <ExpandAnimation Type="Linear" />
            </telerik:RadComboBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorDistributorVNO" runat="server" ErrorMessage="Please select a distributor or VNO"
                ControlToValidate="RadComboBoxDistributorVNO" />--%>
            <telerik:RadComboBox ID="RadComboBoxDistributor" runat="server" EmptyMessage="Select a Distributor" OnClientSelectedIndexChanged="OnClientSelectedIndexChanged_Distributor" Skin="Metro">
                <%--CMTFO-12--%>
                <%--<ItemTemplate>
                    <telerik:RadTreeView ID="RadTreeViewDistributor" runat="server" Skin="Metro" OnClientNodeClicking="nodeClickingDist">
                    </telerik:RadTreeView>
                </ItemTemplate>
                <Items>
                    <telerik:RadComboBoxItem Text="" Height="300px" />
                </Items>
                <ExpandAnimation Type="Linear" />--%>
            </telerik:RadComboBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorDistributor" runat="server" ErrorMessage="Please select a distributor"
                ControlToValidate="RadComboBoxDistributor" />
        </td>
    </tr>
    <tr>
        <td class="requiredLabel">Customer:
        </td>
        <td>
            <%--<telerik:RadComboBox ID="RadComboBoxCustomer" runat="server" Skin="Metro" Sort="Ascending" Width="430px" EnableVirtualScrolling="true" EmptyMessage="Select a Customer"
                MaxHeight="300px"></telerik:RadComboBox>--%>

            <telerik:RadXmlHttpPanel ID="RadXmlHttpPanelCustomer" runat="server" EnableClientScriptEvaluation="True"
                OnServiceRequest="RadXmlHttpPanelCustomer_ServiceRequest">
                <telerik:RadComboBox ID="RadComboBoxCustomer" runat="server" Skin="Metro" ExpandDirection="Up"
                    MaxHeight="200px" Width="430px">
                </telerik:RadComboBox>
            </telerik:RadXmlHttpPanel>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorCustomer" runat="server" ErrorMessage="Please select a customer"
                ControlToValidate="RadComboBoxCustomer" />
        </td>
    </tr>
    <tr>
        <td class="requiredLabel">Service Package:
        </td>
        <td>
            <telerik:RadXmlHttpPanel ID="RadXmlHttpPanelSLA" runat="server" EnableClientScriptEvaluation="True"
                OnServiceRequest="RadXmlHttpPanelSLA_ServiceRequest">
                <telerik:RadComboBox ID="RadComboBoxSLA" runat="server" Skin="Metro" ExpandDirection="Up"
                    Height="200px" Width="430px">
                </telerik:RadComboBox>
            </telerik:RadXmlHttpPanel>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorSla" runat="server" ErrorMessage="Please select an SLA"
                ControlToValidate="RadComboBoxSLA" Display="Dynamic" />
            <asp:CompareValidator ID="CompareValidatorSla" runat="server" ErrorMessage="Please select an ISP that has SLAs defined" 
                ValueToCompare="No SLAs defined for this ISP or SLAs are hidden" Operator="NotEqual" ControlToValidate="RadComboBoxSLA"  />
        </td>
    </tr>
    <tr>
        <td>Free zone schedule:
        </td>
        <td>
            <telerik:RadComboBox ID="RadComboBoxFreeZone" runat="server" Skin="Metro" ExpandDirection="Up"
                Height="200px" Width="430px">
            </telerik:RadComboBox>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <table>
                <tr>
                    <td>(Start) IP Address:
                        <asp:TextBox ID="TextBoxIP" runat="server" Columns="39"></asp:TextBox></td>
                    <td>IP Mask:
                        <!-- <asp:TextBox ID="TextBoxIPMask" runat="server" ToolTip="Specify an IP Mask (\ value)" Enabled="False" Columns="39"></asp:TextBox></td> -->
                        <telerik:RadComboBox ID="RadComboBoxIPMask" runat="server" Enabled="false" Width="60" Skin="Metro">
                            <Items>
                                <telerik:RadComboBoxItem runat="server" Text="32" Value="32" />
                                <telerik:RadComboBoxItem runat="server" Text="31" Value="31" />
                                <telerik:RadComboBoxItem runat="server" Text="30" Value="30" />
                                <telerik:RadComboBoxItem runat="server" Text="29" Value="29" />
                                <telerik:RadComboBoxItem runat="server" Text="28" Value="28" />
                                <telerik:RadComboBoxItem runat="server" Text="27" Value="27" />
                                <telerik:RadComboBoxItem runat="server" Text="26" Value="26" />
                                <telerik:RadComboBoxItem runat="server" Text="25" Value="25" />
                                <telerik:RadComboBoxItem runat="server" Text="24" Value="24" />
                            </Items>
                        </telerik:RadComboBox>
                    </td>
                    <td>
                        <asp:CheckBox ID="CheckBoxIPRange" runat="server" Text="Specify IP Range" ToolTip="Check this box if the terminal has an IP Address range. In this case specify an end range IP address." OnClick="javascript:IPRangeChanged(this);" AutoPostBack="False" /></td>
                </tr>
                <tr>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorStartIP" runat="server" ErrorMessage="Please specify a start IP address" Enabled="False" ControlToValidate="TextBoxIP" Display="Dynamic"></asp:RequiredFieldValidator></td>
                    <td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>Static IP address:
        </td>
        <td>
            <asp:CheckBox ID="CheckBoxStaticIP" runat="server" />
        </td>
    </tr>
    <tr>
        <td>Subscription Start Date:
        </td>
        <td>
            <telerik:RadDatePicker ID="RadDatePickerStartDate" runat="server" Skin="Metro">
                <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x"
                    Skin="Metro" runat="server">
                </Calendar>
                <DateInput DisplayDateFormat="d/MM/yyyy" DateFormat="d/MM/yyyy" runat="server">
                </DateInput>
                <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
            </telerik:RadDatePicker>
        </td>
    </tr>
    <tr>
        <td>Subscription Expiry Date:
        </td>
        <td>
            <telerik:RadDatePicker ID="RadDatePickerExpiryDate" runat="server" Skin="Metro">
                <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x"
                    Skin="Metro" runat="server">
                </Calendar>
                <DateInput DisplayDateFormat="d/MM/yyyy" DateFormat="d/MM/yyyy" runat="server">
                </DateInput>
                <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
            </telerik:RadDatePicker>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Button ID="ButtonSave" runat="server" Text="Save" OnClick="ButtonSave_Click" />
        </td>
        <td>
            <asp:Label ID="LabelResult" runat="server"></asp:Label>
        </td>
    </tr>
</table>
