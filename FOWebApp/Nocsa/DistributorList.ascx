﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DistributorList.ascx.cs"
    Inherits="FOWebApp.Nocsa.DistributorList" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<style type="text/css">
    .rgAltRow, .rgRow
    {
        cursor: pointer !important;
    }
</style>
<script type="text/javascript">
    function RowSelected(sender, eventArgs) {
        var grid = sender;
        var detailTables = grid.get_detailTables();
        if (detailTables.length == 1) {
            var dataItems = detailTables[0].get_selectedItems();
            if (dataItems.length != 0) {
                var macAddress = dataItems[0].get_element().cells[1].innerHTML;
                var oWnd = radopen("Nocsa/TerminalForm.aspx?mac=" + macAddress, "RadWindowModifyTerminal");
            }
        }
        else {
            var masterDataView = grid.get_masterTableView();
            var rowNum = eventArgs.get_itemIndexHierarchical();
            var row = masterDataView.get_dataItems()[rowNum];
            var distributorId = masterDataView.getCellByColumnUniqueName(row, "Id").innerHTML;

            //Initialize and show the terminal details window
            var oWnd = radopen("Nocsa/DistributorDetailsPage.aspx?Id=" + distributorId, "RadWindowDistributorDetails");
        }
    }
</script>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxyDistributorList" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="RadGridDistributorList">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGridDistributorList" />
                <telerik:AjaxUpdatedControl ControlID="RadWindowManager1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadGrid ID="RadGridDistributorList" runat="server" AllowFilteringByColumn="True"
    AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" CellSpacing="0"
    GridLines="None" PageSize="20" ShowStatusBar="True" Skin="Metro" OnItemCommand="RadGridDistributorList_ItemCommand"
    OnNeedDataSource="RadGridDistributorList_NeedDataSource"
    OnPageIndexChanged="RadGridDistributorList_PageIndexChanged"
    OnDetailTableDataBind="RadGridDistributorList_DetailTableDataBind"
    OnDataBound="RadGridDistributorList_DataBound"
    OnItemDataBound="RadGridDistributorList_ItemDataBound"
    OnPageSizeChanged="RadGridDistributorList_PageSizeChanged"
    OnSortCommand="RadGridDistributorList_SortCommand"
    OnPreRender="RadGridDistributorList_PreRender"
    OnDeleteCommand="RadGridDistributorList_DeleteCommand"
    EnableLinqExpressions="False">
    <GroupingSettings CaseSensitive="false" />
    <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True" EnableRowHoverStyle="true">
        <Selecting AllowRowSelect="True" />
        <ClientEvents OnRowSelected="RowSelected" />
    </ClientSettings>
    <MasterTableView CommandItemDisplay="Top" CommandItemSettings-ShowAddNewRecordButton="False"
        AllowFilteringByColumn="True" DataKeyNames="Id" PageSize="20" CommandItemSettings-ShowExportToCsvButton="True"
        CommandItemSettings-ShowExportToExcelButton="True" CommandItemSettings-ShowExportToWordButton="True">
        <DetailTables>
            <telerik:GridTableView runat="server" Name="TerminalsTableView" AllowCustomPaging="false"
                AllowSorting="true">
                <CommandItemSettings ExportToPdfText="Export to PDF" />
                <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
                </RowIndicatorColumn>
                <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridBoundColumn DataField="FullName" FilterControlAltText="Filter FullNameColumn column"
                        HeaderText="Full Name" UniqueName="FullNameColumn" ShowFilterIcon="False">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="MacAddress" FilterControlAltText="Filter MacAddressColumn column"
                        HeaderText="MAC Address" MaxLength="12" ReadOnly="True" Resizable="False" UniqueName="MacAddressColumn"
                        CurrentFilterFunction="Contains">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="SitId" FilterControlAltText="Filter SitIdColumn column"
                        HeaderText="Site ID" UniqueName="SitIdColumn">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="IpAddress" FilterControlAltText="Filter IpAddressColumn column"
                        HeaderText="IP Address" ReadOnly="True" UniqueName="IpAddressColumn">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="SlaId" FilterControlAltText="Filter SLA column"
                        HeaderText="SLA" ReadOnly="True" UniqueName="ServiceColumn" ShowFilterIcon="False" AutoPostBackOnFilter="True">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn AllowSorting="True" DataField="AdmStatus" FilterControlAltText="Filter AdmStatusColumn column"
                        HeaderText="Status" ReadOnly="True" UniqueName="AdmStatusColumn" ShowFilterIcon="False" CurrentFilterFunction="EqualTo" AutoPostBackOnFilter="True">
                    </telerik:GridBoundColumn>
                    <telerik:GridDateTimeColumn DataField="StartDate" FilterControlAltText="Filter StartDateColumn column" PickerType="DatePicker"
                        HeaderText="Start Date" ReadOnly="True" UniqueName="StartDateColumn" DataFormatString="{0:dd/MM/yyyy}" DataType="System.DateTime">
                    </telerik:GridDateTimeColumn>
                    <telerik:GridDateTimeColumn AllowSorting="True" DataField="ExpiryDate" DataType="System.DateTime" PickerType="DatePicker"
                        FilterControlAltText="Filter ExpiryDateColumn column" HeaderText="Expiry Date"
                        ReadOnly="True" UniqueName="ExpiryDateColumn" DataFormatString="{0:dd/MM/yyyy}">
                    </telerik:GridDateTimeColumn>
                    <telerik:GridBoundColumn DataField="CNo"
                        FilterControlAltText="Filter CNo column" HeaderText="C\No" UniqueName="CNo"
                        Visible="True">
                    </telerik:GridBoundColumn>
                </Columns>
                <EditFormSettings>
                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                    </EditColumn>
                </EditFormSettings>
                <PagerStyle AlwaysVisible="True"></PagerStyle>
            </telerik:GridTableView>
        </DetailTables>
        <CommandItemSettings ExportToPdfText="Export to PDF" />
        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
            <HeaderStyle Width="20px" />
        </RowIndicatorColumn>
        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
            <HeaderStyle Width="20px" />
        </ExpandCollapseColumn>
        <Columns>
            <telerik:GridBoundColumn DataField="Id" FilterControlAltText="Filter IdColumn column"
                HeaderText="Id" UniqueName="Id">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="FullName" FilterControlAltText="Filter FullNameColumn column"
                HeaderText="Distributor Name" UniqueName="FullName">
                <ItemStyle Width="200px" Wrap="False" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Phone" FilterControlAltText="Filter PhoneColumn column"
                HeaderText="Phone" UniqueName="Phone">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Fax" FilterControlAltText="Filter FaxColumn column"
                HeaderText="Fax" UniqueName="Fax">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Email" FilterControlAltText="Filter EMailColumn column"
                HeaderText="E-Mail" UniqueName="EMail">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="WebSite" FilterControlAltText="Filter WebsiteColumn column"
                HeaderText="Website" UniqueName="Website">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Vat" FilterControlAltText="Filter VatColumn column"
                HeaderText="VAT" UniqueName="Vat">
            </telerik:GridBoundColumn>
            <telerik:GridButtonColumn HeaderText="Delete" ImageUrl="~/Images/delete.png" ConfirmText="Are you sure you want to delete this distributor?"
                UniqueName="DeleteColumn" ButtonType="ImageButton" HeaderButtonType="PushButton"
                Text="Delete" CommandName="Delete">
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </telerik:GridButtonColumn>
        </Columns>
        <EditFormSettings>
            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
            </EditColumn>
        </EditFormSettings>
    </MasterTableView>
    <FilterMenu EnableImageSprites="False">
    </FilterMenu>
    <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default">
    </HeaderContextMenu>
</telerik:RadGrid>
<telerik:RadWindowManager ID="RadWindowManager1" runat="server" Skin="Metro" DestroyOnClose="False">
    <Windows>
        <telerik:RadWindow ID="RadWindowDistributorDetails" runat="server" NavigateUrl="~/Nocsa/DistributorDetailsPage.aspx"
            Animation="Fade" Opacity="100" Skin="Metro" Title="Distributor Details" AutoSize="False"
            Width="1000px" Height="700px" KeepInScreenBounds="True" EnableShadow="True" EnableAriaSupport="True"
            VisibleStatusbar="True" Modal="True" DestroyOnClose="False" BackColor="DarkGray">
        </telerik:RadWindow>
        <telerik:RadWindow ID="RadWindowModifyTerminal" runat="server" Animation="None" Opacity="100"
            Skin="Metro" Title="Modify Terminal Data" AutoSize="False" Width="850px" Height="700px"
            KeepInScreenBounds="True" EnableShadow="True" EnableAriaSupport="True" VisibleStatusbar="True"
            Modal="False" DestroyOnClose="False">
        </telerik:RadWindow>
        <telerik:RadWindow ID="RadWindowTerminalDetails" runat="server"
            Animation="None" Opacity="100" Skin="Metro" Title="Terminal Details" AutoSize="False"
            Width="1000px" Height="700px" KeepInScreenBounds="True" EnableShadow="True" EnableAriaSupport="True"
            VisibleStatusbar="True" Modal="True" DestroyOnClose="False">
        </telerik:RadWindow>
        <telerik:RadWindow ID="RadWindowModifyActivity" runat="server" Animation="None" Opacity="100"
            Skin="Metro" Title="Modify Activity" AutoSize="False" Width="850px" Height="655px"
            KeepInScreenBounds="True" EnableShadow="True" EnableAriaSupport="True" VisibleStatusbar="True"
            Modal="False" DestroyOnClose="False">
        </telerik:RadWindow>
    </Windows>
</telerik:RadWindowManager>
