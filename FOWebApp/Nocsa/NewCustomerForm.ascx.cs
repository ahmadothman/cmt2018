﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using Telerik.Web.UI;
using FOWebApp.Data;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;

namespace FOWebApp.Nocsa
{
    public partial class NewCustomerForm : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BOAccountingControlWS boAccountingControl =
                  new net.nimera.cmt.BOAccountingControlWSRef.BOAccountingControlWS();
            //Load the Distributor combobox
            Distributor[] distributors = boAccountingControl.GetDistributorsAndVNOs();

            foreach (Distributor distributor in distributors)
            {
                RadComboBoxItem item = new RadComboBoxItem();
                item.Value = distributor.Id.ToString();
                item.Text = distributor.FullName;
                RadComboBoxDistributor.Items.Add(item);
            }

            string[] Sectors = { "", "Financial", "Government", "Radio", "eLearning", "Posts Offices", "Religious", "Other" };
            int j = 0;
            foreach (string dt in Sectors)
            {
                RadComboBoxItem myItem = new RadComboBoxItem();
                myItem.Text = dt;
                myItem.Value = j.ToString();
                RadComboBoxSector.Items.Add(myItem);
                j++;
            }

            RadComboBoxISP.Items.Clear();
            Isp[] ispList = boAccountingControl.GetIsps();
            RadComboBoxISP.DataValueField = "Id";
            RadComboBoxISP.DataTextField = "CompanyName";
            foreach(Isp sp in ispList)
            {
                RadComboBoxItem myItem = new RadComboBoxItem();
                myItem.Text = sp.CompanyName;
                myItem.Value = sp.Id.ToString();
                RadComboBoxISP.Items.Add(myItem);
            }
           
            RadComboBoxISP.Items.Insert(0, new RadComboBoxItem(""));
        }

        //recursive function that adds attribute to all child controls 
        private void addBlurAtt(Control cntrl)
        {
            if (cntrl.Controls.Count > 0)
            {
                foreach (Control childControl in cntrl.Controls)
                {
                    addBlurAtt(childControl);
                }
            }
            if (cntrl.GetType() == typeof(TextBox))
            {
                TextBox TempTextBox = (TextBox)cntrl;
                //TempTextBox.Attributes.Add("onFocus", "DoFocus(this);");
                //TempTextBox.Attributes.Add("onBlur", "DoBlur(this);");
                TempTextBox.Attributes.Add("onchange", "DoChange(this);");
            }
            if (cntrl.GetType() == typeof(ListBox))
            {
                ListBox TempTextBox = (ListBox)cntrl;
                //TempTextBox.Attributes.Add("onFocus", "DoFocus(this);");
                //TempTextBox.Attributes.Add("onBlur", "DoBlur(this);");
                TempTextBox.Attributes.Add("onchange", "DoChange(this);");
            }
            if (cntrl.GetType() == typeof(DropDownList))
            {
                DropDownList TempTextBox = (DropDownList)cntrl;
                //TempTextBox.Attributes.Add("onFocus", "DoFocus(this);");
                //TempTextBox.Attributes.Add("onBlur", "DoBlur(this);");
                TempTextBox.Attributes.Add("onchange", "DoChange(this);");
            }
        }

        protected void ButtonSubmit_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                BOAccountingControlWS boAccountingControl =
                  new net.nimera.cmt.BOAccountingControlWSRef.BOAccountingControlWS();

                //This is a new organization
                Organization organization = new Organization();
                organization.Address = new Address();
                organization.Distributor = new Distributor();

                organization.Address.AddressLine1 = TextBoxAddressLine1.Text;
                organization.Address.AddressLine2 = TextBoxAddressLine2.Text;
                organization.Email = TextBoxEMail.Text;
                organization.Fax = TextBoxFax.Text;
                organization.Address.Location = TextBoxLocation.Text;
                organization.FullName = TextBoxName.Text;
                organization.Address.PostalCode = TextBoxPCO.Text;
                organization.Phone = TextBoxPhone.Text;
                organization.Remarks = TextBoxRemarks.Text;
                organization.WebSite = TextBoxWebSite.Text;
                organization.Address.Country = CountryList.SelectedValue;
                organization.Distributor.Id = Int32.Parse(RadComboBoxDistributor.SelectedValue);
                organization.Vat = TextBoxVAT.Text;
                organization.Sector = Int32.Parse(RadComboBoxSector.SelectedValue);
               
                

                if (CheckBoxVNO.Checked)
                {
                    organization.VNO = true;
                    organization.MIR = Convert.ToInt32(RadNumericTextBoxMIR.Text);
                    organization.CIR = Convert.ToInt32(RadNumericTextBoxCIR.Text);
                    organization.DefaultSla = Convert.ToInt32(RadComboBoxDefaultSLA.SelectedValue);
                }
                else
                {
                    organization.VNO = false;
                    organization.MIR = 0;
                    organization.CIR = 0;
                    organization.DefaultSla = null;
                }

                if (boAccountingControl.UpdateOrganization(organization))
                {
                    LabelResult.Text = "Customer added successfully!";
                    LabelResult.ForeColor = Color.DarkGreen;
                    ButtonSubmit.Enabled = false;
                }
                else
                {
                    LabelResult.Text = "Adding customer failed!";
                    LabelResult.ForeColor = Color.Red;
                }
            }
        }

        //protected void RadXmlHttpPanelDefaultSLA_ServiceRequest(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        //{
           
        //}

        //protected void RadXmlHttpPanelDefaultSLA_ServiceRequest1(object sender, RadXmlHttpPanelEventArgs e)
        //{
        //    RadComboBoxDefaultSLA.Text = "";
        //    RadComboBoxDefaultSLA.ClearSelection();
        //    BOAccountingControlWS boAccountingControl =
        //          new net.nimera.cmt.BOAccountingControlWSRef.BOAccountingControlWS();
        //    //Search Service packs for the given ISP
        //    int ispId = Int32.Parse(e.Value);
        //    ServiceLevel[] servicePacks = boAccountingControl.GetServicePacksByIspWithHide(ispId);
        //    RadComboBoxDefaultSLA.DataSource = servicePacks;
        //    RadComboBoxDefaultSLA.DataValueField = "SlaID";
        //    RadComboBoxDefaultSLA.DataTextField = "SlaName";
        //    RadComboBoxDefaultSLA.DataBind();
        //    RadComboBoxDefaultSLA.Items.Insert(0, new RadComboBoxItem(""));
        //}

        protected void RadComboBoxISP_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {

            RadComboBoxDefaultSLA.Text = "";
            RadComboBoxDefaultSLA.ClearSelection();
            BOAccountingControlWS boAccountingControl =
                  new net.nimera.cmt.BOAccountingControlWSRef.BOAccountingControlWS();
            //Search Service packs for the given ISP
            int ispId = Int32.Parse(e.Value);
            ServiceLevel[] servicePacks = boAccountingControl.GetServicePacksByIspWithHide(ispId);
            RadComboBoxDefaultSLA.DataSource = servicePacks;
            RadComboBoxDefaultSLA.DataValueField = "SlaID";
            RadComboBoxDefaultSLA.DataTextField = "SlaName";
            RadComboBoxDefaultSLA.DataBind();
            RadComboBoxDefaultSLA.Items.Insert(0, new RadComboBoxItem(""));
        }
    }
}