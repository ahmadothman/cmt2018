﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GlobalSettings.ascx.cs" Inherits="FOWebApp.Nocsa.GlobalSettings" %>
<telerik:RadAjaxPanel ID="RadAjaxPanelConnectedDevice" runat="server" Style="margin-right: 114px">
    <table cellpadding="10" cellspacing="5">
        <tr>
            <td>
                Daily data volume threshold: 
            </td>
            <td>
                <telerik:RadNumericTextBox runat="server" ID="RadNumericTextBoxDailyDataVolume" Width="100px"></telerik:RadNumericTextBox> kb
            </td>
        </tr>
        <tr>
            <td></td>
            <td>
                <telerik:RadButton runat="server" ID="RadButtonSave" Text="Save" OnClick="RadButtonSave_Click"></telerik:RadButton>
                <asp:Label ID="LabelResult" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
</telerik:RadAjaxPanel>