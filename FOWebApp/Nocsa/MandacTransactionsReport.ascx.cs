﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FOWebApp.net.nimera.cmt.BOMandacControlWSRef;

namespace FOWebApp.Nocsa
{
    public partial class MandacTransactionsReport : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BOMandacControlWS boMandacControl = new BOMandacControlWS();
            MandacTransaction[] transactions = boMandacControl.GetAllTransactions();
            RadGridMandac.DataSource = transactions;
            RadGridMandac.DataBind();
        }
    }
}