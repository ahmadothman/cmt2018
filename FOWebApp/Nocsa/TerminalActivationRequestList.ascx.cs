﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using Telerik.Web.UI;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.Data;

namespace FOWebApp.Nocsa
{
    public partial class TerminalActivationRequestList : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Session["TerminalRequests_PageIdx"] != null)
            {
                RadGridTerminals.CurrentPageIndex =
                            (int)this.Session["TerminalRequests_PageIdx"];
            }

            if (this.Session["TerminalRequests_SortExpression"] != null)
            {
                string sortExpression = (string)this.Session["TerminalRequests_SortExpression"];
                GridSortExpression gridSortExpression = new GridSortExpression();
                gridSortExpression.FieldName = sortExpression;
                gridSortExpression.SortOrder = GridSortOrder.Ascending;
                RadGridTerminals.MasterTableView.SortExpressions.Add(gridSortExpression);
            }
        }

        protected void RadGridTerminals_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();
            RadGridTerminals.DataSource = boAccountingControl.GetTerminalRequests();
        }

        protected void RadGridTerminals_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem dataItem = (GridDataItem)e.Item;

                if (dataItem["AdmStatusColumn"].Text.Equals("1"))
                {
                    dataItem["AdmStatusColumn"].Text = "Locked";
                    dataItem["AdmStatusColumn"].ForeColor = Color.Red;
                }

                if (dataItem["AdmStatusColumn"].Text.Equals("2"))
                {
                    dataItem["AdmStatusColumn"].Text = "Un-locked";
                    dataItem["AdmStatusColumn"].ForeColor = Color.Green;
                }

                if (dataItem["AdmStatusColumn"].Text.Equals("3"))
                {
                    dataItem["AdmStatusColumn"].Text = "Decommissioned";
                    dataItem["AdmStatusColumn"].ForeColor = Color.Blue;
                }

                if (dataItem["AdmStatusColumn"].Text.Equals("5"))
                {
                    dataItem["AdmStatusColumn"].Text = "Request";
                    dataItem["AdmStatusColumn"].ForeColor = Color.DarkOrange;
                }

                if (dataItem["AdmStatusColumn"].Text.Equals("6"))
                {
                    dataItem["AdmStatusColumn"].Text = "Deleted";
                    dataItem["AdmStatusColumn"].ForeColor = Color.Black;
                }
            }
        }

        protected void RadGridTerminals_ItemCreated(object sender, GridItemEventArgs e)
        {

        }

        protected void RadGridTerminals_ColumnCreated(object sender, GridColumnCreatedEventArgs e)
        {
        }

        /// <summary>
        /// Configures the export settings for this radgrid
        /// </summary>
        public void ConfigureExport()
        {
            RadGridTerminals.ExportSettings.FileName = "CMTExport";
            RadGridTerminals.ExportSettings.ExportOnlyData = true;
            RadGridTerminals.ExportSettings.IgnorePaging = true;
            RadGridTerminals.ExportSettings.OpenInNewWindow = true;
            RadGridTerminals.ExportSettings.Excel.Format = GridExcelExportFormat.Html;
            RadGridTerminals.ExportSettings.Csv.ColumnDelimiter = GridCsvDelimiter.Semicolon;
            RadGridTerminals.ExportSettings.Csv.EncloseDataWithQuotes = false;
            RadGridTerminals.ExportSettings.Csv.RowDelimiter = GridCsvDelimiter.NewLine;
        }

        protected void RadGridTerminals_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToExcelCommandName)
            {
                this.ConfigureExport();
                RadGridTerminals.MasterTableView.ExportToExcel();
            }

            if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToCsvCommandName)
            {
                this.ConfigureExport();
                RadGridTerminals.MasterTableView.ExportToCSV();
            }

            if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToPdfCommandName)
            {
                this.ConfigureExport();
                RadGridTerminals.MasterTableView.ExportToPdf();
            }

            if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToWordCommandName)
            {
                this.ConfigureExport();
                RadGridTerminals.MasterTableView.ExportToWord();
            }
        }

        protected void RadGridTerminals_SortCommand(object sender, GridSortCommandEventArgs e)
        {
            this.Session["TerminalRequests_SortExpression"] = e.SortExpression;
        }

        protected void RadGridTerminals_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            //Save the page index
            int pageIdx = e.NewPageIndex;
            this.Session["TerminalRequests_PageIdx"] = pageIdx;
        }
    }
}