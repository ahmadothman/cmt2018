﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FOWebApp.Nocsa
{
    public partial class VNODetailsPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Load the details form and initialize the data
            int id = Int32.Parse(Request.Params["Id"]);

            VNODetailsForm df =
                (VNODetailsForm)Page.LoadControl("~/Nocsa/VNODetailsForm.ascx");

            df.componentDataInit(id);

            PlaceHolderVNODetailsForm.Controls.Add(df);
        }
    }
}