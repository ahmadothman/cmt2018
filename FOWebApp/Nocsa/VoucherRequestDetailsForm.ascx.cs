﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.IO;
using System.Drawing;
using Telerik.Web.UI;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;
using FOWebApp.net.nimera.cmt.BOVoucherControllerWSRef;

namespace FOWebApp.Nocsa
{
    public partial class VoucherRequestDetailsForm : System.Web.UI.UserControl
    {
        BOAccountingControlWS _boAccountingControlWS = new BOAccountingControlWS();
        BOVoucherControllerWS _boVoucherControlWS = new BOVoucherControllerWS();
        BOLogControlWS _boLogControlWS = new BOLogControlWS();
        VoucherRequest vr;
        Distributor dist;

        
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        public void componentDataInit(int voucherRequestId)
        {
            //Get the voucher request data
            vr = _boVoucherControlWS.GetVoucherRequest(voucherRequestId);
            dist = _boAccountingControlWS.GetDistributor(vr.DistributorId);
            LabelDistributor.Text = dist.FullName;
            VoucherVolume vv = _boVoucherControlWS.GetVoucherVolumeById(vr.VoucherVolumeId);
            LabelVolume.Text = vv.VolumeMB + " MB - Valid " + vv.ValidityPeriod + " days - " + vv.UnitPriceEUR.ToString("0.00") + "€ / " + vv.UnitPriceUSD.ToString("0.00") + "$";
            LabelNumVoucher.Text = vr.NumVouchers.ToString();
        }

        protected void ButtonDeleteRequest_Click(object sender, EventArgs e)
        {
            vr.RequestState = 2;
            if (_boVoucherControlWS.UpdateVoucherRequest(vr))
            {
                ButtonDeleteRequest.Enabled = false;
                LabelResult.Text = "Message deleted";
                LabelResult.ForeColor = Color.Green;
            }
            else
            {
                LabelResult.Text = "Deleting Message failed";
                LabelResult.ForeColor = Color.Red;
            }
        }
        
        protected void ButtonCreateVouchers_Click(object sender, EventArgs e)
        {
            //Notify user and set the display
            RadWindowManager1.RadAlert("You are about to create new vouchers! Are you sure you want to continue", 250, 200, "Create new vouchers", null);
            DivDownloadURL.InnerHtml = "Creating vouchers, please  wait";
            ImageAjax.Style["Display"] = "";
            
            //Create the vouchers
            string result = this.CreateVouchers(vr.DistributorId, vr.NumVouchers, vr.VoucherVolumeId);
            ImageAjax.Style["Display"] = "none";
            if (result != "failed")
            {
                LabelResult.Text = "Vouchers successfully created.";
                LabelResult.ForeColor = Color.Green;
                vr.RequestState = 1;
                _boVoucherControlWS.UpdateVoucherRequest(vr);
                ButtonDeleteRequest.Enabled = false;

                //Send an email to notify the distributor
                string[] eMailRecipients = dist.Email.Split(';');
                string emailBody = "Dear Distributor,<br/>" +
                                   "A new voucher batch has been created as per your request.<br/>" +
                                   "Voucher volume details: " + LabelVolume.Text + "<br/>" +
                                   "No. of vouchers: " + vr.NumVouchers + "<br/>" +
                                   "The batch is now available in the list of vouchers. <br/><br/>" +
                                   "CMT automated call do not respond.";
                _boLogControlWS.SendMail(emailBody, "New voucher batch", eMailRecipients);
            }
            else
            {
                LabelResult.Text = "Voucher creation failed";
                LabelResult.ForeColor = Color.Red;
            }
            
        }

        /// <summary>
        /// Method copied from the VoucherSupport webservice
        /// </summary>
        /// <param name="distributorId">The distributor ID for which the batch is created</param>
        /// <param name="numVouchers">The number of vouchers</param>
        /// <param name="volume">The volume ID of the batch</param>
        /// <returns>A download link if succesful</returns>
        protected string CreateVouchers(int distributorId, int numVouchers, int volume)
        {
            string resultMsg = "";

            BOLogControlWS boLogControl = new BOLogControlWS();

            string voucherPath = FOWebApp.Properties.Settings.Default.VoucherPath;

            FileInfo fi = new FileInfo(voucherPath);
            if (!fi.Directory.Exists)
            {
                fi.Directory.Create();
            }

            //Get the current user.
            BOVoucherControllerWS boVoucherController = new BOVoucherControllerWS();
            int batchId;

            MembershipUser myObject = Membership.GetUser();
            Guid userId = new Guid(myObject.ProviderUserKey.ToString());
            try
            {
                Voucher[] vouchers = boVoucherController.CreateVouchers(distributorId, numVouchers, userId, volume, out batchId);
                resultMsg = "<b>Done! Download vouchers <a href=\"~/Servlets/VoucherDownload.aspx?BatchId=" + batchId + "\">here</a></b>";
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDateTime = DateTime.Now;
                cmtEx.ExceptionStacktrace = ex.StackTrace;
                cmtEx.ExceptionDesc = ex.Message;
                cmtEx.UserDescription = "Creating voucher output file failed";
                boLogControl.LogApplicationException(cmtEx);
                resultMsg = "failed";
            }

            return resultMsg;
        }
    }
}