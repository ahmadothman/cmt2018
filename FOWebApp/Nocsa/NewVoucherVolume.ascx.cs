﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FOWebApp.net.nimera.cmt.BOVoucherControllerWSRef;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using Telerik.Web.UI;
using System.Drawing;
using FOWebApp.Data;

namespace FOWebApp.Nocsa
{
    public partial class NewVoucherVolume : System.Web.UI.UserControl
    {
        DataHelper _dataHelper = null;
        BOVoucherControllerWS _voucherController;
        BOAccountingControlWS _accountingController;

        public NewVoucherVolume()
        {
            _dataHelper = new DataHelper();
        }
        private string[] _currencyCodes = { "EUR", "USD" };
        
        protected void Page_Load(object sender, EventArgs e)
        {
            _voucherController = new BOVoucherControllerWS();
            _accountingController = new BOAccountingControlWS();

            

            // fill up the list of ISPs
            List<Isp> IspList = _dataHelper.getISPs();
            foreach (Isp isp in IspList)
            {
                RadComboBoxItem item = new RadComboBoxItem();
                item.Value = isp.Id.ToString();
                item.Text = isp.CompanyName;
                RadComboBoxISP.Items.Add(item);
            }

            AddInitialValueToComboBox(RadComboBoxISP);
        }

        protected void RadXmlHttpPanelSLA_ServiceRequest(object sender, RadXmlHttpPanelEventArgs e)
        {
            int ispId = Int32.Parse(e.Value);
            ServiceLevel[] slaItems = _accountingController.GetServicePacksByIsp(ispId);

            if (!(slaItems.Length == 0))
            {
                RadComboBoxSLA.Items.Clear();

                RadComboBoxSLA.DataSource = slaItems;
                RadComboBoxSLA.DataTextField = "SlaName";
                RadComboBoxSLA.DataValueField = "SlaId";
                RadComboBoxSLA.DataBind();

                AddInitialValueToComboBox(RadComboBoxSLA);
            }
            else
            {
                RadComboBoxSLA.Text = "No SLAs defined for this ISP or SLAs are hidden";
                RadComboBoxSLA.Enabled = false;
            }

            //Store the isp for successive pageloads
            //this.Session["CreateNewTerminal_ISP"] = ispId;
        }

        private void AddInitialValueToComboBox(RadComboBox comboBox)
        {
            RadComboBoxItem item = new RadComboBoxItem(string.Empty, "0");
            comboBox.Items.Insert(0, item);
        }


        protected void ButtonSubmit_Click(object sender, EventArgs e)
        {
            string additionalComment = "";
            VoucherVolume vv = new VoucherVolume();
            vv.Id = Convert.ToInt32(RadNumericTextBoxId.Value);
            vv.Description = RadTextBoxDescription.Text;
            vv.VolumeMB = Convert.ToInt32(RadNumericTextBoxVolumeMB.Value);
            vv.UnitPrice = (double)0;
            vv.CurrencyCode = "USD";
            vv.UnitPriceEUR = (double)RadNumericTextBoxUnitPriceEUR.Value;
            vv.UnitPriceUSD = (double)RadNumericTextBoxUnitPriceUSD.Value;
            vv.Sla= Convert.ToInt32(RadComboBoxSLA.SelectedValue);
            ServiceLevel sla = _accountingController.GetServicePack(Convert.ToInt32(RadComboBoxSLA.SelectedValue));
            vv.VolumeId = vv.Id;

            if (sla.ServiceClass == 7 && Convert.ToInt32(RadNumericTextBoxValidityPeriod.Value) > 0)
            {
                vv.ValidityPeriod = 0;
                additionalComment = " , Validity Period has been automatically set to 0. This is a monthly SoHo service.";
            }
            else
            {
                vv.ValidityPeriod = Convert.ToInt32(RadNumericTextBoxValidityPeriod.Value);
            }
            
            
            if (_voucherController.AddVoucherVolume(vv))
            {
                LabelSubmit.Text = "Voucher volume successfully added! "+additionalComment;
                LabelSubmit.ForeColor = Color.Green;
            }
            else
            {
                LabelSubmit.Text = "Adding voucher volume failed!";
                LabelSubmit.ForeColor = Color.Red;
            }
        }
    }
}