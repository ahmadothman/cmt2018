﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NOCSAMenuAjax.ascx.cs" Inherits="FOWebApp.Nocsa.NOCSAMenuAjax" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<telerik:RadTreeView ID="RadTreeViewDistributorMenu" runat="server"
    OnNodeClick="RadTreeViewDistributorMenu_NodeClick" Skin="Metro"
    ValidationGroup="Administrator main menu">
    <Nodes>
        <telerik:RadTreeNode runat="server" Expanded="True"
            ImageUrl="~/Images/earth_preferences.png" Owner="RadTreeViewDistributorMenu"
            Text="NOCSA Menu" Font-Bold="True" Value="n_Tasks" ToolTip="List of tasks to be completed">
            <Nodes>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/satellite_dish.png"
                    Text="ISP" Value="n_ISP" ToolTip="ISP Management">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Text="New ISP" Value="n_New_ISP" ToolTip="Creation of a new ISP">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/magician.png"
                    Owner="" Text="VNOs" Value="n_VNOs" ToolTip="Virtual Network Operator Management" Visible="False">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Owner="" Text="New VNO"
                            Value="n_New_VNO" ToolTip="Create a new VNO">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/businesspeople.png"
                    Owner="" Text="Distributors" Value="n_Distributors" ToolTip="Distributor Management">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Owner="" Text="Inactive Distributors"
                            Value="n_Inactive_Distributors" ToolTip="This is a list of inactive distributors">
                        </telerik:RadTreeNode>
                        <telerik:RadTreeNode runat="server" Owner="" Text="New distributor"
                            Value="n_New_Distributor" ToolTip="Create a new Distributor">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" Owner="" Text="Terminals" Value="n_Terminals"
                    ImageUrl="~/Images/Terminal.png" ToolTip="Terminal Management">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Owner="" Text="New Terminal" Value="n_New_Terminal" ToolTip="Create a new Terminal"></telerik:RadTreeNode>
                        <telerik:RadTreeNode runat="server" Owner="" Text="New Redundant Setup" Value="n_New_RedundantSetup" ToolTip="Create a new redundant terminal setup"></telerik:RadTreeNode>
                        <telerik:RadTreeNode runat="server" Owner="" Text="Search By MAC"
                            Value="n_Search_By_MAC" Visible="False">
                        </telerik:RadTreeNode>
                        <telerik:RadTreeNode runat="server" Owner="" Text="Search By SitId"
                            Value="n_Search_By_SitId" Visible="False">
                        </telerik:RadTreeNode>
                        <telerik:RadTreeNode runat="server" Text="Search"
                            Value="n_Advanced_Search" ToolTip="Advanced Terminal Search">
                        </telerik:RadTreeNode>
                        <telerik:RadTreeNode runat="server" Text="Alarms"
                            Value="n_Terminal_Alarms" ToolTip="Terminal monitoring">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/radio.png" Text="Multicast Groups" ToolTip="Multicast group management" Value="n_Multicast">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Text="New Multicast Group" Value="n_NewMulticast" ToolTip="Create a new Multicast Group">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/customersNew.png"
                    Owner="" Text="Customer Orgs" Value="n_Customers" ToolTip="Customer Management">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Text="VNOs"
                            Value="n_CustomerVNOs" Visible="true" ToolTip="Customer VNO management">
                        </telerik:RadTreeNode>
                        <telerik:RadTreeNode runat="server" Owner="" Text="New Customer Org"
                            Value="n_New_Customer" ToolTip="Create a new Customer">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/customers.png"
                    Text="End Users" Value="n_EndUsers" ToolTip="End User Management" Visible="False">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Text="New End User" Value="n_NewEndUser" ToolTip="Create a new End User">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/form_blue.png"
                    Text="Requests" Value="n_Requests" ToolTip="Activation Requests">
                </telerik:RadTreeNode>
                <%--<telerik:RadTreeNode runat="server" ImageUrl="~/Images/form_blue.png"
                    Text="On Demand Requests" Value="n_OnDemandRequests" ToolTip="On demand requests">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Text="All bookings"
                            Value="n_All_bookings" ToolTip="List of all bookings">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>--%>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/lock_ok.png" Text="Change Password"
                    ToolTip="Change your password" Value="n_Change_Password">
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/Logs.png" Owner=""
                    Text="Logs" Value="n_Logs" ToolTip="Logs and Reports">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Owner="" Text="Usage Log"
                            Value="n_Usage_Log" ToolTip="User activity over a period of time">
                        </telerik:RadTreeNode>
                        <telerik:RadTreeNode runat="server" Owner="" Text="Activity Log"
                            Value="n_Activity_Log" ToolTip="Log of Terminal activities">
                            <Nodes>
                                <telerik:RadTreeNode runat="server" Text="New Activity" Value="n_New_Activity" ToolTip="Add a new Activity">
                                </telerik:RadTreeNode>
                            </Nodes>
                        </telerik:RadTreeNode>
                        <telerik:RadTreeNode runat="server" Owner="" Text="Error Log"
                            Value="n_Error_Log" ToolTip="Log of application errors">
                        </telerik:RadTreeNode>
                        <telerik:RadTreeNode runat="server" Owner="" Text="Tickets" Value="n_Tickets" ToolTip="Verify Tickets for a specific MAC address"></telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/report.png"
                    Text="Reports" Value="n_Reports" ToolTip="Operational Reports">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Text="Accounting"
                            Value="n_Reports_Accounting" ToolTip="Accounting report over a period of time">
                        </telerik:RadTreeNode>
                        <telerik:RadTreeNode runat="server" Text="Hotspot transactions"
                            Value="n_Reports_HotspotTransactions" ToolTip="Accounting report on external hotspot transactions">
                        </telerik:RadTreeNode>
                        <telerik:RadTreeNode runat="server" Owner="" Text="Suspended terminals" Value="n_Suspended_Terminals"
                            ToolTip="Search suspended terminal in a given period">
                        </telerik:RadTreeNode>
                        <%--<telerik:RadTreeNode runat="server" Text="Terminal Switchovers"
                            Value="n_Reports_SwitchOvers" ToolTip="Switchover report over a period of time">
                        </telerik:RadTreeNode>
                        <telerik:RadTreeNode runat="server" Text="Vouchers"
                            Value="n_VoucherReports" ToolTip="Reports on vouchers">
                            <Nodes>
                                <telerik:RadTreeNode runat="server" Text="Validated Vouchers"
                                    Value="n_VoucherReport" ToolTip="Validated vouchers">
                                </telerik:RadTreeNode>
                                <telerik:RadTreeNode runat="server" Text="Voucher Batches"
                                    Value="n_VoucherBatchesReport" ToolTip="Voucher batches per Distributor">
                                </telerik:RadTreeNode>
                            </Nodes>
                        </telerik:RadTreeNode>--%>
                        <telerik:RadTreeNode runat="server" Text="Email warnings"
                            Value="n_OutgoingEmailsReport" ToolTip="Email warnings sent to Distributors" Visible="false">
                            <%-- Set Visible to true when it has been completely implemented --%>
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/ticket_blue.png"
                    Text="Vouchers" Value="n_Vouchers" ToolTip="Voucher creation and Management">
                    <Nodes>
                       <%-- <telerik:RadTreeNode runat="server" Text="Voucher requests"
                            Value="n_VoucherRequests" ToolTip="View voucher requests from distributors">
                        </telerik:RadTreeNode>--%>
                        <telerik:RadTreeNode runat="server" Text="Create Vouchers"
                            Value="n_CreateVouchers" ToolTip="Create a new Voucher batch">
                        </telerik:RadTreeNode>
                        <telerik:RadTreeNode runat="server" Text="Batches Report"
                            Value="n_Available" ToolTip="Check on the batches and available vouchers for a Distributor">
                        </telerik:RadTreeNode>
                        <telerik:RadTreeNode runat="server" Text="Disable voucher"
                            Value="n_ValidateVoucher" ToolTip="Validate an individual voucher, take care this does not unlock the terminal!">
                        </telerik:RadTreeNode>
                        <telerik:RadTreeNode runat="server" Text="Reset voucher"
                            Value="n_ResetVoucher" ToolTip="Reset a voucher so it can be validated again">
                        </telerik:RadTreeNode>
                        <telerik:RadTreeNode runat="server" Text="Voucher volumes"
                            Value="n_VoucherVolumes" ToolTip="View and modify voucher volumes">
                            <Nodes>
                                <telerik:RadTreeNode runat="server" Text="New voucher volume"
                                    Value="n_NewVoucherVolume" ToolTip="Add a new voucher volume">
                                </telerik:RadTreeNode>
                            </Nodes>
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" Text="Issues" ImageUrl="~/Images/user_headset.png" Value="n_Issues" ToolTip="Issue tracking system integration">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Text="New Issue" Value="n_NewIssue" ToolTip="Submit a new issue">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" Text="CMT Messages" ImageUrl="~/Images/message.jpg" Value="n_CMTMessages" ToolTip="Send messages to CMT users">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Text="New Message" Value="n_NewCMTMessage" ToolTip="Submit a new message">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/key.png" Owner=""
                    Text="System Users" Value="n_Users" ToolTip="Create and manage System Users">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Owner="" Text="New User" Value="n_New_User" ToolTip="Create a new System User">
                        </telerik:RadTreeNode>
                       <%-- <telerik:RadTreeNode runat="server" Owner="" Text="Roles" Value="n_Roles" ToolTip="CMT Role Management">
                            <Nodes>
                                <telerik:RadTreeNode runat="server" Owner="" Text="New Role"
                                    Value="n_New_Role" ToolTip="Create a new Role">
                                </telerik:RadTreeNode>
                            </Nodes>
                        </telerik:RadTreeNode>--%>
                    </Nodes>
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" Text="Documents" ToolTip="Document library management" Value="n_DocumentsLib" ImageUrl="~/Images/books_blue_preferences.png">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Text="Upload Document" ToolTip="Upload a new document" Value="n_UploadDoc">
                        </telerik:RadTreeNode>
                        <telerik:RadTreeNode runat="server" Text="Archive" ToolTip="Overview of the uploaded documents" Value="n_Library">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>

            </Nodes>
        </telerik:RadTreeNode>
        <telerik:RadTreeNode runat="server" ImageUrl="~/Images/configuration.png"
            Text="Configuration" Value="n_Configuration" ToolTip="CMT OSS Configurations">
            <Nodes>
                <telerik:RadTreeNode runat="server" Text="Terminal States"
                    Value="n_TerminalStates" Visible="False" ToolTip="Create and manage Terminal States">
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" Text="User Activity Types"
                    Value="n_UserActivity_Types" Visible="False" ToolTip="Create and Manage User Activity Types">
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" Text="Service Packs"
                    Value="n_Service_Packs" ToolTip="Create and Manage Service Packs">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Text="New Service Pack"
                            Value="n_New_ServicePack" ToolTip="Create a new Service Pack">
                        </telerik:RadTreeNode>
                        <telerik:RadTreeNode runat="server" Text="Service Pack Wizard"
                            Value="n_ServicePackWizard" ToolTip="Create a new Service Pack with the Service Pack wizard">
                        </telerik:RadTreeNode>
                        <telerik:RadTreeNode runat="server" Text="TEST Service Pack Wizard"
                            Value="n_TestServicePackWizard" ToolTip="Development Service Pack wizard. Only use for testing.">
                        </telerik:RadTreeNode>
                        <telerik:RadTreeNode runat="server" Text="Global settings"
                            Value="n_GlobalSettings" ToolTip="">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" Text="Free Zones"
                    Value="n_FreeZones" ToolTip="Create and Manage FreeZones">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Text="New Free Zone"
                            Value="n_New_FreeZone" ToolTip="Create a new FreeZone">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" Text="Services"
                    Value="n_Services" ToolTip="Create and Manage Service Applications">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Text="Create New Service"
                            Value="n_Create_New_Service" ToolTip="Add a new Service">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" Text="Procedures" Value="n_Procedures"
                    Visible="False">
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" Text="Connected Devices"
                    Value="n_ConnectedDevices" ToolTip="Add and manage connected devices">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Text="New connected device"
                            Value="n_New_ConnectedDevice" Visible="True" ToolTip="New connected device">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <%--<telerik:RadTreeNode runat="server" ImageUrl="~/Images/masks.png" Text="Roles" ToolTip="Create and manage System Roles" Value="n_Roles">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Text="New Role" ToolTip="Create a new Role" Value="n_New_Role">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>--%>
            </Nodes>
        </telerik:RadTreeNode>
        <telerik:RadTreeNode runat="server" ImageUrl="~/Images/server_to_client.png"
            Owner="RadTreeViewDistributorMenu" Text="NMS" Value="n_NMSDashboard" ToolTip="Link to the NMS Webpages" Visible="False">
            <%--<Nodes>
                <telerik:RadTreeNode runat="server" Text="NMS Dashboard" Value="n_NMSDashboard" ToolTip="The main NMS Dashboard">
                </telerik:RadTreeNode>
            </Nodes>--%>
        </telerik:RadTreeNode>
        <telerik:RadTreeNode ID="AboutNode" runat="server" ImageUrl="~/Images/information.png"
            Text="About" Value="n_About" ToolTip="Shows About information">
        </telerik:RadTreeNode>
        <telerik:RadTreeNode runat="server" ImageUrl="~/Images/exit.png" Text="Log Off"
            ToolTip="Close the session" Value="n_log_off">
        </telerik:RadTreeNode>
    </Nodes>
</telerik:RadTreeView>
