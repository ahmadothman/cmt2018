﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CreateVouchers.ascx.cs"
    Inherits="FOWebApp.Nocsa.CreateVouchers" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script type="text/javascript" id="telerikClientEvents1">

</script>
<script type="text/javascript">
    function CreateVouchersButtonClicked() {
        radconfirm('Are you sure to continue?', confirmCreateCallBackFn, 400, 200, null, 'You are about to create new vouchers!');
    }

    function confirmCreateCallBackFn(arg) {
        if (arg == true) {
            document.getElementById("<%= DownloadURL.ClientID %>").innerHTML = "Creating vouchers, please  wait";
            document.getElementById("<%= ImageAjax.ClientID %>").style.display = "";     

            //Get the distributor Id and number of vouchers from the controls
            var distributorCombo = $find("<%= RadComboBoxDistributors.ClientID %>");
            var distributorId = parseInt(distributorCombo.get_value());

            
            var volumeId = parseInt(document.getElementById('HiddenFieldSLAId').value);

            var numVoucherTextbox = $find("<%= RadNumericTextBoxvouchers.ClientID %>");
            var numVouchers = parseInt(numVoucherTextbox.get_textBoxValue());

            //Call the webservice
            FOWebApp.WebServices.VoucherSupportWS.CreateVouchers(distributorId, numVouchers, volumeId, OnVoucherCreationComplete);
        }
    }

    function OnVoucherCreationComplete(result) {
        document.getElementById("<%= ImageAjax.ClientID %>").style.display = "none";
        document.getElementById("<%= DownloadURL.ClientID %>").innerHTML = result;
    }
    function nodeClicking(sender, args) {
            var comboBox = $find("<%= RadComboBoxVoucherVolume.ClientID %>");

            var node = args.get_node()
            if (node.get_level() == 1) {

                comboBox.set_text(node.get_text());
                comboBox.set_value(node.get_value());
                document.getElementById('HiddenFieldSLAId').value = node.get_value();
                comboBox.trackChanges();
                comboBox.get_items().getItem(0).set_text(node.get_text());
                comboBox.commitChanges();
                comboBox.hideDropDown();
                
            }            
        }
            
</script>
<asp:HiddenField ID="HiddenFieldSLAId" runat="server" ClientIDMode="Static" />
<style type="text/css">
    .style1
    {
        width: 308px;
    }
</style>
<table cellpadding="10" cellspacing="5">
    <tr>
        <td>
            Create vouchers for Distributor:
        </td>
        <td class="style1">
            <telerik:RadComboBox ID="RadComboBoxDistributors" runat="server" Height="27px" Skin="Metro"
                Width="300px" ExpandDirection="Up" MaxHeight="500px" NoWrap="True" ShowMoreResultsBox="True" OnSelectedIndexChanged="RadComboBoxDistributors_SelectedIndexChanged" AutoPostBack="True">
                <ExpandAnimation Type="OutCubic" />
            </telerik:RadComboBox>
        </td>
    </tr>
    <tr>
        <td>
            Voucher volume:
        </td>
        <td class="auto-style1">
            <telerik:RadComboBox ID="RadComboBoxVoucherVolume" runat="server" Skin="Metro" Sort="Ascending" Width="532px" EnableVirtualScrolling="True" EmptyMessage="Select a Service Pack"
                    MaxHeight="300px" Height="16px">
                    <ItemTemplate>
                        <telerik:RadTreeView ID="RadTreeViewVV" runat="server" Skin="Metro" OnClientNodeClicking="nodeClicking">
                        </telerik:RadTreeView>
                    </ItemTemplate>
                    <Items>
                        <telerik:RadComboBoxItem Text="" />
                    </Items>
                    <ExpandAnimation Type="Linear" />
                </telerik:RadComboBox>
        </td>
    </tr>
    <tr>
        <td>
            Number of vouchers to create:
        </td>
        <td class="style1">
            <telerik:RadNumericTextBox ID="RadNumericTextBoxvouchers" runat="server" 
                EmptyMessage="Number of vouchers" MaxValue="1000" MinValue="0" NumberFormat-DecimalDigits="0"
                ShowButton="False" ShowSpinButtons="True" Value="10" Height="21px" Skin="Metro"
                Width="125px" Culture="en-GB">
<NumberFormat DecimalDigits="0" ZeroPattern="n" decimalseparator=" " groupseparator=""></NumberFormat>
            </telerik:RadNumericTextBox>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <telerik:RadButton ID="RadButtonCreateVouchers" runat="server" OnClientClicked="CreateVouchersButtonClicked"
                Skin="Metro" Text="Create Voucher(s)" OnClick="RadButtonCreateVouchers_Click"
                AutoPostBack="False">
            </telerik:RadButton>
        </td>
    </tr>
    <tr>
        <td class="style1" colspan="2">
            <asp:Image ID="ImageAjax" runat="server" ImageUrl="~/Images/ajax-loader.gif" style="display: none; vertical-align: middle"/><asp:Label ID="DownloadURL" runat="server"></asp:Label>
        </td>
    </tr>
</table>
<telerik:RadWindowManager ID="RadWindowManager1" runat="server" Skin="Metro" EnableShadow="True"
    Left="0px" Right="0px" Modal="True" VisibleStatusbar="False">
</telerik:RadWindowManager>
