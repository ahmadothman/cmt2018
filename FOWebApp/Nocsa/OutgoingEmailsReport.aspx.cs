﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FOWebApp.Nocsa
{
    public partial class OutgoingEmailsReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Load the details form and initialize the data
            string date = Request.Params["Date"].ToString();

            OutgoingEmails df =
                (OutgoingEmails)Page.LoadControl("OutgoingEmails.ascx");

            df.componentDataInit(date);

            PlaceHolderOutgoingEmails.Controls.Add(df);
        }
    }
}