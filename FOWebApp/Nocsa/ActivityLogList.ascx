﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ActivityLogList.ascx.cs" Inherits="FOWebApp.Nocsa.ActivityLogList" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<table cellpadding="5" cellspacing="2">
    <tr>
        <td>Start date:</td>
        <td> 
            <telerik:RadDatePicker ID="RadDatePickerStart" runat="server"  Skin="Metro">
                <DateInput DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy" runat="server"/> 
            </telerik:RadDatePicker>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Provide a start date" ControlToValidate="RadDatePickerStart" Display="Dynamic"></asp:RequiredFieldValidator>
        </td>
        <td>End date:</td>
        <td>
            <telerik:RadDatePicker ID="RadDatePickerEnd" runat="server" Skin="Metro">
                <DateInput DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy"  runat="server" /> 
            </telerik:RadDatePicker>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Provide an end date" ControlToValidate="RadDatePickerEnd"></asp:RequiredFieldValidator>
        </td>
        <td>
            <%--<asp:ImageButton ID="ImageButtonQuery" runat="server" 
                ImageUrl="~/Images/invoice_euro.png" ToolTip="Show user log" 
                onclick="ImageButtonQuery_Click" />--%>
            <asp:Button ID="userLogButton" runat="server" Text="Show" ToolTip="Show user log" onclick="ImageButtonQuery_Click" />
        </td>
    </tr>
</table>
<telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" Height="800px" Width="90%">
    <telerik:RadGrid ID="RadGridUserActivityLog" runat="server" AllowSorting="True" 
        CellSpacing="0" GridLines="None" 
        Skin="Metro" Visible="False" 
        onpageindexchanged="RadGridUserActivityLog_PageIndexChanged" Width="90%" 
        onneeddatasource="RadGridUserActivityLog_NeedDataSource" 
        onpagesizechanged="RadGridUserActivityLog_PageSizeChanged" 
        onsortcommand="RadGridUserActivityLog_SortCommand">
        <MasterTableView AllowPaging="True" 
            PageSize="20">
            <CommandItemSettings ExportToPdfText="Export to PDF" />
            <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
                <HeaderStyle Width="20px" />
            </RowIndicatorColumn>
            <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
                <HeaderStyle Width="20px" />
            </ExpandCollapseColumn>
            <EditFormSettings>
                <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                </EditColumn>
            </EditFormSettings>
        </MasterTableView>
        <FilterMenu EnableImageSprites="False">
        </FilterMenu>
        <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default">
        </HeaderContextMenu>
    </telerik:RadGrid>
</telerik:RadAjaxPanel>


