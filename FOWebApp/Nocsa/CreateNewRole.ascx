﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CreateNewRole.ascx.cs" Inherits="FOWebApp.Nocsa.CreateNewRole" %>
<style type="text/css">
    .auto-style2 {
        width: 102%;
    }
</style>
<telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
    <script type="text/javascript">
        
    </script>
</telerik:RadCodeBlock>
<telerik:RadAjaxPanel ID="RadAjaxPanelRole" runat="server" Height="200px" Width="232px"
    EnableHistory="True" Style="margin-right: 114px">
    <table width="800px" id="RoleTable" cellpadding="10" cellspacing="5">
        <tr>
            <td>
                <table id="RoleLabelTable" cellpadding="10" cellspacing="5">
                    <tr>
                        <td>Role name:</td>
                        <td>
                            <asp:TextBox ID="TextBoxRoleName" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="PanelServicesAllocation" runat="server" Width="410px" GroupingText="Allocate services to role" Visible="False">
                    <telerik:RadListBox ID="RadListBoxServices" runat="server" Height="300px" Skin="Metro" Visible="False" Width="405px" CheckBoxes="True" SelectionMode="Multiple">
                    </telerik:RadListBox>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:button ID="ButtonCreate" runat="server" OnClick="ButtonCreate_Click" Text="Create Role"/>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelCreate" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
</telerik:RadAjaxPanel>