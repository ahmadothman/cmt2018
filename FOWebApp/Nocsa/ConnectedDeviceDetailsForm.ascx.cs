﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FOWebApp.net.nimera.cmt.BOConnectedDevicesControlWSRef;
using Telerik.Web.UI;
using System.Drawing;

namespace FOWebApp.Nocsa
{
    public partial class ConnectedDeviceDetailsForm : System.Web.UI.UserControl
    {
        const string _HEXBYTE = "<0|1|2|3|4|5|6|7|8|9|a|b|c|d|e|f><0|1|2|3|4|5|6|7|8|9|a|b|c|d|e|f>";
        BOConnectedDevicesControlWS _connectedDevicesControl;
        
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public void componentDataInit(string deviceId)
        {
            RadMaskedTextBoxMacAddress.Mask =
               _HEXBYTE + "<:>" + _HEXBYTE + "<:>" + _HEXBYTE + "<:>" + _HEXBYTE + "<:>" + _HEXBYTE + "<:>" +
               _HEXBYTE;

            _connectedDevicesControl = new BOConnectedDevicesControlWS();

            ConnectedDevice device = _connectedDevicesControl.GetDevice(deviceId);
            ConnectedDeviceType[] deviceTypes = _connectedDevicesControl.GetAllConnectedDeviceTypes();
            foreach (ConnectedDeviceType type in deviceTypes)
            {
                RadComboBoxItem item = new RadComboBoxItem();
                item.Value = type.ID.ToString();
                item.Text = type.Name;
                item.Selected = type.Name == device.DeviceType;
                RadComboBoxDeviceType.Items.Add(item);
            }

            TextBoxDeviceName.Text = device.DeviceName;
            //RadComboBoxDeviceType.SelectedValue = device.DeviceType;
            RadMaskedTextBoxMacAddress.Text = device.macAddress;
            LabelDeviceId.Text = device.DeviceId;
        }

        protected void ButtonUpdate_Click(object sender, EventArgs e)
        {
            ConnectedDevice device = new ConnectedDevice();
            device.DeviceId = LabelDeviceId.Text;
            device.DeviceName = TextBoxDeviceName.Text;
            device.DeviceType = RadComboBoxDeviceType.SelectedItem.Value;
            device.macAddress = RadMaskedTextBoxMacAddress.Text;

            try
            {
                if (_connectedDevicesControl.UpdateDevice(device))
                {
                    LabelResult.Text = "Device was successfully updated";
                    LabelResult.ForeColor = Color.Green;
                }
            }
            catch (Exception ex)
            {
                LabelResult.Text = "Updating device failed";
                LabelResult.ForeColor = Color.Red;
            }
        }

        protected void ButtonDelete_Click(object sender, EventArgs e)
        {
            ConnectedDevice device = new ConnectedDevice();
            device.DeviceId = LabelDeviceId.Text;
            try
            {
                if (_connectedDevicesControl.RemoveDeviceFromTerminal(device))
                {
                    LabelResult.Text = "Device was successfully deleted from the CMT";
                    LabelResult.ForeColor = Color.Green;
                }
            }
            catch (Exception ex)
            {
                LabelResult.Text = "Deleting device failed";
                LabelResult.ForeColor = Color.Red;
            }
        }
    }
}