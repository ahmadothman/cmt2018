﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FOWebApp.net.nimera.cmt.BOMultiCastGroupControllerWSRef;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;

namespace FOWebApp.Nocsa
{
    public partial class MultiCastGroupList : System.Web.UI.UserControl
    {
        BOMultiCastGroupControllerWS _boMultiCastGroupController = null;
        BOAccountingControlWS _boAccountingController = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            _boMultiCastGroupController = new BOMultiCastGroupControllerWS();
            _boAccountingController = new BOAccountingControlWS();
        }

        protected void RadGridMultiCastGroups_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            MultiCastGroup[] mcList = _boMultiCastGroupController.GetMultiCastGroups();
            RadGridMultiCastGroups.DataSource = mcList;
        }

        protected void RadGridMultiCastGroups_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem dataItem = (GridDataItem)e.Item;

                Distributor dist = _boAccountingController.GetDistributor(Convert.ToInt32(dataItem["DistributorColumn"].Text));
                if (dist != null)
	            {
		             dataItem["DistributorColumn"].Text = dist.FullName;
	            }
            }
        }
    }
}