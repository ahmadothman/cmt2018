﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOMonitorControlWSRef;
using System.Drawing;

namespace FOWebApp.Nocsa
{
    public partial class NewFreeZone : System.Web.UI.UserControl
    {
        const string _HOUR = "<0|1|2><0|1|2|3|4|5|6|7|8|9>";
        const string _MINUTE = "<0|1|2|3|4|5><0|1|2|3|4|5|6|7|8|9>";
        const string _SECOND = "<0|1|2|3|4|5><0|1|2|3|4|5|6|7|8|9>";
        
        protected void Page_Load(object sender, EventArgs e)
        {
            TextBoxMonOff.Mask = _HOUR + "<:>" + _MINUTE + "<:>" + _SECOND;
            TextBoxMonOn.Mask = _HOUR + "<:>" + _MINUTE + "<:>" + _SECOND;
            TextBoxTueOff.Mask = _HOUR + "<:>" + _MINUTE + "<:>" + _SECOND;
            TextBoxTueOn.Mask = _HOUR + "<:>" + _MINUTE + "<:>" + _SECOND;
            TextBoxWedOff.Mask = _HOUR + "<:>" + _MINUTE + "<:>" + _SECOND;
            TextBoxWedOn.Mask = _HOUR + "<:>" + _MINUTE + "<:>" + _SECOND;
            TextBoxThuOff.Mask = _HOUR + "<:>" + _MINUTE + "<:>" + _SECOND;
            TextBoxThuOn.Mask = _HOUR + "<:>" + _MINUTE + "<:>" + _SECOND;
            TextBoxFriOff.Mask = _HOUR + "<:>" + _MINUTE + "<:>" + _SECOND;
            TextBoxFriOn.Mask = _HOUR + "<:>" + _MINUTE + "<:>" + _SECOND;
            TextBoxSatOff.Mask = _HOUR + "<:>" + _MINUTE + "<:>" + _SECOND;
            TextBoxSatOn.Mask = _HOUR + "<:>" + _MINUTE + "<:>" + _SECOND;
            TextBoxSunOff.Mask = _HOUR + "<:>" + _MINUTE + "<:>" + _SECOND;
            TextBoxSunOn.Mask = _HOUR + "<:>" + _MINUTE + "<:>" + _SECOND;
        }

        protected void ButtonSubmit_Click(object sender, EventArgs e)
        {
            FreeZoneCMT freeZone = new FreeZoneCMT();
            freeZone.Id = Convert.ToInt32(TextBoxId.Text);
            
            //create random freezone ID
            //bool unique = false;
            //Random r = new Random();
            //int id = 0;
            //BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();
            //FreeZoneCMT[] freeZones = boAccountingControl.GetFreeZones();
            //while (!unique)
            //{
            //    id = r.Next(0, 99);
            //    unique = true;
            //    foreach (FreeZoneCMT FZ in freeZones)
            //    {
            //        if (FZ.Id == id)
            //        {
            //            unique = false;
            //        }
            //    }
            //}
            //freeZone.Id = id;

            freeZone.Name = TextBoxName.Text;
            //if (CheckBoxActive.Checked)
            //{
            //    freeZone.CurrentActive = 1;
            //}
            //else
            //{
                freeZone.CurrentActive = 0;
            //}
            freeZone.MonOff = TextBoxMonOff.Text;
            freeZone.MonOn = TextBoxMonOn.Text;
            freeZone.TueOff = TextBoxTueOff.Text;
            freeZone.TueOn = TextBoxTueOn.Text;
            freeZone.WedOff = TextBoxWedOff.Text;
            freeZone.WedOn = TextBoxWedOn.Text;
            freeZone.ThuOff = TextBoxThuOff.Text;
            freeZone.ThuOn = TextBoxThuOn.Text;
            freeZone.FriOff = TextBoxFriOff.Text;
            freeZone.FriOn = TextBoxFriOn.Text;
            freeZone.SatOff = TextBoxSatOff.Text;
            freeZone.SatOn = TextBoxSatOn.Text;
            freeZone.SunOff = TextBoxSunOff.Text;
            freeZone.SunOn = TextBoxSunOn.Text;
            
            BOMonitorControlWS boMonitorControl = new BOMonitorControlWS();
            FreeZone freeZoneNMS = this.cmtToNms(freeZone);
            if (boMonitorControl.NMScreateFreeZone(freeZoneNMS, ispId))
            {
                BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();
                if (boAccountingControl.CreateFreeZone(freeZone))
                {
                    LabelResult.Text = "Free zone created successfully";
                    LabelResult.ForeColor = Color.Green;
                }
                else
                {
                    LabelResult.Text = "Creating free zone failed";
                    LabelResult.ForeColor = Color.Red;
                }
            }
            else
            {
                LabelResult.Text = "Creating free zone failed";
                LabelResult.ForeColor = Color.Red;
            }
        }

        protected FreeZone cmtToNms(FreeZoneCMT cmt)
        {
            FreeZone nms = new FreeZone();
            nms.Id = cmt.Id;
            nms.Name = cmt.Name;
            nms.CurrentActive = cmt.CurrentActive;
            nms.MonOff = cmt.MonOff;
            nms.MonOn = cmt.MonOn;
            nms.TueOff = cmt.TueOff;
            nms.TueOn = cmt.TueOn;
            nms.WedOff = cmt.WedOff;
            nms.WedOn = cmt.WedOn;
            nms.ThuOff = cmt.ThuOff;
            nms.ThuOn = cmt.ThuOn;
            nms.FriOff = cmt.FriOff;
            nms.FriOn = cmt.FriOn;
            nms.SatOff = cmt.SatOff;
            nms.SatOn = cmt.SatOn;
            nms.SunOff = cmt.SunOff;
            nms.SunOn = cmt.SunOn;

            return nms;
        }
        public int ispId = 469; //ISP ID of the NMS ISP
    }
}