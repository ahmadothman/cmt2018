﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NewMultiCastGroup.ascx.cs" Inherits="FOWebApp.Nocsa.NewMultiCastGroup" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script language="javascript" type="text/javascript">
    function OnClientSelectedIndexChanged(sender, eventArgs) {
        var item = eventArgs.get_item();
        
        var panel = $find("<%=RadXmlHttpPanelCustomer.ClientID %>");
        panel.set_value(item.get_value());
        return false;
    }
</script>
<table cellpadding="10" cellspacing="5">
    <tr>
        <td class="requiredLabel">IP Address:</td>
        <td>
            <asp:TextBox ID="TextBoxIPAddress" runat="server" ControlToValidate="TextBoxIPAddress"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="You must provide an IP Address" ControlToValidate="TextBoxIPAddress"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>Mac Address:</td>
        <td>
            <asp:TextBox ID="TextBoxMacAddress" runat="server" Width="280px" Enabled="False"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="requiredLabel">Name:</td>
        <td>
            <asp:TextBox ID="TextBoxName" runat="server" Width="430px"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="You must provide a Name" ControlToValidate="TextBoxName"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td class="requiredLabel">Source URL:</td>
        <td>
            <asp:TextBox ID="TextBoxSourceURL" runat="server" Width="427px"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorSourceUrl" runat="server" ErrorMessage="You must provide a source URL"
                ControlToValidate="TextBoxSourceURL" />
        </td>
    </tr>
    <tr>
        <td class="requiredLabel">Port Nr:</td>
        <td>
            <asp:TextBox ID="TextBoxPortNr" runat="server" Width="45px"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorPortNr" runat="server" ErrorMessage="You must provide a port number"
                ControlToValidate="TextBoxPortNr" />
        </td>
    </tr>
    <tr>
        <td class="requiredLabel">Bandwidth (kbps):</td>
        <td>
            <asp:TextBox ID="TextBoxBandwidth" runat="server"></asp:TextBox>            
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorBandwidth" runat="server" ErrorMessage="You must provide a value for bandwidth"
                ControlToValidate="TextBoxBandwidth" />
        </td>
    </tr>
     <tr>
        <td>State:</td>
        <td>
            <asp:CheckBox ID="CheckBoxState" runat="server" Enabled="False" />
        </td>
    </tr>
    <tr>
        <td class="requiredLabel">IspId:</td>
        <td>
            <telerik:RadComboBox ID="RadComboBoxISP" runat="server" Width="351px" Skin="Metro"></telerik:RadComboBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorISP" runat="server" ErrorMessage="You must select an ISP"
                ControlToValidate="RadComboBoxISP" />
        </td>
    </tr>
    <tr>
        <td class="requiredLabel">FirstActivationDate:</td>
        <td>
            <telerik:RadDatePicker ID="RadDatePickerFirstActivationDate" runat="server" Skin="Metro">
                    <Calendar ID="Calendar4" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False"
                        ViewSelectorText="x" Skin="Metro" runat="server">
                    </Calendar>
                    <DateInput ID="DateInput2" DisplayDateFormat="d/MM/yyyy" DateFormat="d/MM/yyyy" runat="server">
                    </DateInput>
                    <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                </telerik:RadDatePicker>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorFirstActivationDate" runat="server" ErrorMessage="You must provide the activation date"
                ControlToValidate="RadDatePickerFirstActivationDate" />
        </td>
    </tr>
    <tr>
        <td class="requiredLabel">Distributor/VNO:</td>
        <td>
            <Telerik:RadComboBox ID="RadComboBoxDistributor" runat="server" OnClientSelectedIndexChanged="OnClientSelectedIndexChanged" Width="422px" Skin="Metro"></Telerik:RadComboBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorDistributor" runat="server" ErrorMessage="You must select a distributor"
                ControlToValidate="RadComboBoxDistributor" />
        </td>
    </tr>
    <tr>
        <td class="requiredLabel">Customer:</td>
        <td>
            <telerik:RadXmlHttpPanel ID="RadXmlHttpPanelCustomer" runat="server" EnableClientScriptEvaluation="True"
                OnServiceRequest="RadXmlHttpPanelCustomer_ServiceRequest">
                <telerik:RadComboBox ID="RadComboBoxCustomer" runat="server" Skin="Metro" ExpandDirection="Up"
                    Height="200px" Width="430px">
                </telerik:RadComboBox>
            </telerik:RadXmlHttpPanel>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorCustomer" runat="server" ErrorMessage="You must select a customer"
                ControlToValidate="RadComboBoxCustomer" Display="Dynamic" />
            <asp:CompareValidator ID="CompareValidatorCustomer" runat="server" ErrorMessage="You must select a distibutor or VNO that have customers" 
                ValueToCompare="No customers defined for this distributor" Operator="NotEqual" ControlToValidate="RadComboBoxCustomer"  />
        </td>
    </tr>
    <tr>
        <td>
            <asp:Button ID="ButtonSubmit" runat="server" Text="Submit" OnClick="ButtonSubmit_Click" />
        </td>
        <td>
            <asp:Label ID="LabelResult" runat="server"></asp:Label>
        </td>
    </tr>
</table>