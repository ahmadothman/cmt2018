﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NewConnectedDevice.ascx.cs" Inherits="FOWebApp.Nocsa.NewConnectedDevice" %>
<style type="text/css">
    .auto-style2 {
        width: 102%;
    }
</style>
<telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
    <script type="text/javascript">
        
    </script>
</telerik:RadCodeBlock>
<telerik:RadAjaxPanel ID="RadAjaxPanelConnectedDevice" runat="server" Height="200px" Width="232px"
    EnableHistory="True" Style="margin-right: 114px">
    <table width="800px" id="ConnectedDeviceTable" cellpadding="10" cellspacing="5">
        <tr>
            <td>Device name:</td>
            <td>
                <asp:TextBox ID="TextBoxDeviceName" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>Device ID:</td>
            <td>
                <asp:TextBox ID="TextBoxDeviceId" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorDeviceId" runat="server" ErrorMessage="Missing ID" ControlToValidate="TextBoxDeviceId"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>Device type:</td>
            <td>
                <telerik:RadComboBox ID="RadComboBoxDeviceType" runat="server" Enabled="True" Skin="Metro">
                </telerik:RadComboBox>
            </td>
        </tr>
        <tr>
            <td>Terminal MAC address:</td>
            <td>
                <telerik:RadMaskedTextBox ID="RadMaskedTextBoxMacAddress" runat="server" Skin="Metro"
                    ControlToValidate="RadMaskedTextBoxMacAddress">
                </telerik:RadMaskedTextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorMacAddress" runat="server" ErrorMessage="Missing MAC address" ControlToValidate="RadMaskedTextBoxMacAddress"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:button ID="ButtonAdd" runat="server" OnClick="ButtonAdd_Click" Text="Add Connected Device"/>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Label ID="LabelAdd" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
</telerik:RadAjaxPanel>