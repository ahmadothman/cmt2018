﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TicketDetailsForm.ascx.cs" Inherits="FOWebApp.Nocsa.TicketDetailsForm" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<style type="text/css">
    .auto-style2 {
        width: 102%;
    }
</style>
<telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
    <script type="text/javascript">
        function DoBlur(fld) {
            fld.className = 'normalfld';
        }

        function DoFocus(fld) {
            fld.className = 'focusfld';
        }

        function DoChange(fld) {
            fld.className = 'changedfld';
        }
    </script>
</telerik:RadCodeBlock>
    <table cellpadding="10" cellspacing="5">
        <tr>
            <td width="25%">Ticket ID:</td>
            <td><asp:Label ID="LabelTicketId" runat="server"></asp:Label></td>
        </tr>
        <tr>
            <td>Creation date:</td>
            <td><asp:Label ID="LabelCreationDate" runat="server"></asp:Label></td>
        </tr>
        <tr>
            <td>MAC Address:</td>
            <td><asp:Label ID="LabelMacAddress" runat="server"></asp:Label></td>
        </tr>
        <tr>
            <td>Source admin state:</td>
            <td><asp:Label ID="LabelSourceAdmState" runat="server"></asp:Label></td>
        </tr>
        <tr>
            <td>Target admin state:</td>
            <td><asp:Label ID="LabelTargetAdmState" runat="server"></asp:Label></td>
        </tr>
        <tr>
            <td>Ticket status:</td>
            <td><asp:Label ID="LabelTicketStatus" runat="server"></asp:Label></td>
        </tr>
        <tr>
            <td>Failure reason:</td>
            <td><asp:TextBox ID="TextBoxFailureReason" runat="server" TextMode="MultiLine" Width="600px" Rows="5" ReadOnly="true"></asp:TextBox></td>
        </tr>
        <tr>
            <td>Failure stack trace:</td>
            <td><asp:TextBox ID="TextBoxFailureStackTrace" runat="server" TextMode="MultiLine" Width="600px" Rows="5" ReadOnly="true"></asp:TextBox></td>
        </tr>
        <tr>
            <td>Terminal activity:</td>
            <td><asp:Label ID="LabelTermActivity" runat="server"></asp:Label></td>
        </tr>
        <tr>
            <td>New SLA:</td>
            <td><asp:Label ID="LabelNewSla" runat="server"></asp:Label></td>
        </tr>
        <tr>
            <td>ISP ID:</td>
            <td><asp:Label ID="LabelIspId" runat="server"></asp:Label></td>
        </tr>
        <tr>
            <td>New MAC address:</td>
            <td><asp:Label ID="LabelNewMacAddress" runat="server"></asp:Label></td>
        </tr>
        <tr>
            <td>NMS activate:</td>
            <td><asp:Label ID="LabelNMSActivate" runat="server"></asp:Label></td>
        </tr>
    </table>