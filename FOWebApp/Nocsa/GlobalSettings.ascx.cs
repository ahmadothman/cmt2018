﻿using FOWebApp.net.nimera.cmt.BOConfigurationControlWSRef;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FOWebApp.Nocsa
{
    public partial class GlobalSettings : System.Web.UI.UserControl
    {
        private BOConfigurationControllerWS _configController;

        protected void Page_Load(object sender, EventArgs e)
        {
            _configController = new BOConfigurationControllerWS();
            RadNumericTextBoxDailyDataVolume.Text = _configController.getParameter("DailyDataVolumeThres").Trim();
        }

        protected void RadButtonSave_Click(object sender, EventArgs e)
        {
            if (_configController.setParameter("DailyDataVolumeThres", RadNumericTextBoxDailyDataVolume.Text.ToString()))
            {
                LabelResult.ForeColor = Color.Green;
                LabelResult.Text = "Succesfully saved";
            }
            else
            {
                LabelResult.ForeColor = Color.Red;
                LabelResult.Text = "Failed to save data";
            }
        }
    }
}