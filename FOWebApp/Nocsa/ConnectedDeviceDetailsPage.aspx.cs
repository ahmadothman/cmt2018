﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FOWebApp.Nocsa
{
    public partial class ConnectedDeviceDetailsPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Load the details form and initialize the data
            string id = Request.Params["id"];

            ConnectedDeviceDetailsForm df =
                (ConnectedDeviceDetailsForm)Page.LoadControl("ConnectedDeviceDetailsForm.ascx");

            df.componentDataInit(id);

            PlaceHolderConnectedDeviceDetailsForm.Controls.Add(df);
        }
    }
}