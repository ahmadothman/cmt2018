﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;

namespace FOWebApp.Nocsa
{
    public partial class EndUserList : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Session["EndUserList_PageIdx"] != null)
            {
                RadGridEndUsers.CurrentPageIndex =
                            (int)this.Session["EndUserList_PageIdx"];
            }

            if (this.Session["EndUserList_SortExpression"] != null)
            {
                string sortExpression = (string)this.Session["EndUserList_SortExpression"];
                GridSortExpression gridSortExpression = new GridSortExpression();
                gridSortExpression.FieldName = sortExpression;
                gridSortExpression.SortOrder = GridSortOrder.Ascending;
                RadGridEndUsers.MasterTableView.SortExpressions.Add(gridSortExpression);
            }
        }

        protected void RadGridEndUsers_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            BOAccountingControlWS boAccountingControlWS = new BOAccountingControlWS();
            EndUser[] endUsers = boAccountingControlWS.GetEndUsers();
            if (endUsers != null)
            {
                RadGridEndUsers.DataSource = endUsers;
            }
        }

        protected void RadGridEndUsers_SortCommand(object sender, Telerik.Web.UI.GridSortCommandEventArgs e)
        {
            this.Session["EndUserList_SortExpression"] = e.SortExpression;
        }

        protected void RadGridEndUsers_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            //Save the page index
            this.Session["EndUserList_PageIdx"] = e.NewPageIndex;
        }

        /// <summary>
        /// Sets the delete flag for the selected end user
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadGridEndUsers_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            GridDataKeyArray dataItems = e.Item.OwnerTableView.DataKeyValues;
            string endUserId = dataItems[e.Item.ItemIndex]["Id"].ToString();
            BOLogControlWS boLogControlWS = new BOLogControlWS();

            //Delete the user with the given Id
            try
            {
                
                BOAccountingControlWS boAccountingControlWS = new BOAccountingControlWS();
                if (!boAccountingControlWS.DeleteEndUser(Int32.Parse(endUserId)))
                {
                    CmtApplicationException cmtException = new CmtApplicationException();
                    cmtException.ExceptionDateTime = DateTime.Now;
                    cmtException.ExceptionDesc = "Could not delete End user with Id: " + endUserId;
                    cmtException.StateInformation = "User Id probably not a number";
                    boLogControlWS.LogApplicationException(cmtException);
                    RadWindowManager1.RadAlert("Deleting end user failed!", 250, 200, "Error", null);
                }
                RadGridEndUsers.DataSource = boAccountingControlWS.GetEndUsers();
                RadGridEndUsers.Rebind();
            }
            catch (Exception ex)
            {
              
                CmtApplicationException cmtException = new CmtApplicationException();
                cmtException.ExceptionDateTime = DateTime.Now;
                cmtException.ExceptionDesc = ex.Message;
                cmtException.StateInformation = "User Id probably not a number";
                boLogControlWS.LogApplicationException(cmtException);
            }
        }
    }
}