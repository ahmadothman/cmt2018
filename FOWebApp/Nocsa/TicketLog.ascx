﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TicketLog.ascx.cs" Inherits="FOWebApp.Nocsa.TicketLog" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script type="text/javascript">
    function RowSelected(sender, eventArgs) {
        var grid = sender;
        var masterDataView = grid.get_masterTableView();
        var rowNum = eventArgs.get_itemIndexHierarchical();
        var row = masterDataView.get_dataItems()[rowNum];
        var id = masterDataView.getCellByColumnUniqueName(row, "ID").innerHTML;

        //Initialize and show the ticket details window
        var oWnd = radopen("Nocsa/TicketDetailsPage.aspx?id=" + id, "RadWindowTicketDetails");
    }
</script>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="RadGridTickets">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGridTickets" />
                <telerik:AjaxUpdatedControl ControlID="RadWindowManager1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<table>
    <tr>
        <td><asp:Label ID="LabelMacAddress" runat="server">MAC Address:</asp:Label></td>
        <td>
            <telerik:RadMaskedTextBox ID="RadMaskedTextBoxMacAddress" runat="server" Skin="Metro"
                ControlToValidate="RadMaskedTextBoxMacAddress">
            </telerik:RadMaskedTextBox>
        </td>
        <td><asp:Button ID="ButtonGetTickets" runat="server" Text="Get Tickets" OnClick="ButtonGetTickets_Click" /></td>
    </tr>
</table>
<telerik:RadGrid ID="RadGridTickets" runat="server" AllowSorting="True"
    AutoGenerateColumns="False" Visible="false" CellSpacing="0" GridLines="None" Skin="Metro"
    ShowStatusBar="True" Width="992px" AllowFilteringByColumn="False" 
        onitemdatabound="RadGridTickets_ItemDataBound" 
        onpagesizechanged="RadGridTickets_PageSizeChanged" 
        PageSize="50" onitemcommand="RadGridTickets_ItemCommand">
        <GroupingSettings CaseSensitive="false" /> 
    <ClientSettings EnableRowHoverStyle="true">
        <Selecting AllowRowSelect="True" />
        <ClientEvents OnRowSelected="RowSelected" />
    </ClientSettings>
    <MasterTableView CommandItemDisplay="Top" CommandItemSettings-ShowExportToExcelButton="False" 
        CommandItemSettings-ShowExportToCsvButton="False" RowIndicatorColumn-Visible="True" RowIndicatorColumn-Resizable="True" RowIndicatorColumn-AndCurrentFilterFunction="Contains" CommandItemSettings-ShowAddNewRecordButton="False" ShowFooter="False">
        <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
        </RowIndicatorColumn>
        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
        </ExpandCollapseColumn>
        <Columns>
            <telerik:GridBoundColumn DataField="TicketId" 
                FilterControlAltText="Filter TicketIDColumn column" HeaderText="Ticket ID" 
                UniqueName="ID" ShowFilterIcon="False" AutoPostBackOnFilter="True">
                <ItemStyle Wrap="False" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="CreationDate" FilterControlAltText="Filter TicketDateTimeColumn column"
                HeaderText="Created on" ReadOnly="True" Resizable="False" 
                UniqueName="CreationDate" CurrentFilterFunction="Contains" DataFormatString="{0:dd/MM/yyyy HH:mm}" DataType="System.DateTime"
                ShowFilterIcon="False" AutoPostBackOnFilter="True">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="MACAddress" FilterControlAltText="Filter MACAddressColumn column"
                HeaderText="MAC address" ReadOnly="True" Resizable="False" 
                UniqueName="MacAddress" CurrentFilterFunction="Contains" 
                ShowFilterIcon="False" AutoPostBackOnFilter="True">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="TicketStatus" FilterControlAltText="Filter TicketStatusColumn column"
                HeaderText="Ticket Status" ReadOnly="True" UniqueName="TicketStatus" 
                ShowFilterIcon="False" AutoPostBackOnFilter="True">
            </telerik:GridBoundColumn>
        </Columns>
        <EditFormSettings>
            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
            </EditColumn>
        </EditFormSettings>
    </MasterTableView>
    <FilterMenu EnableImageSprites="False">
    </FilterMenu>
    <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default">
    </HeaderContextMenu>
</telerik:RadGrid>
<telerik:RadWindowManager ID="RadWindowManager1" runat="server" Skin="Metro" 
        DestroyOnClose="True">
    <Windows>
        <telerik:RadWindow ID="RadWindowTicketDetails" runat="server" 
            NavigateUrl="Nocsa/TicketDetailsPage.aspx" Animation="Fade" Opacity="100" Skin="Metro" 
            Title="Ticket Details" AutoSize="False" Width="1000px" Height="700px" KeepInScreenBounds="True" EnableShadow="True" EnableAriaSupport="True" VisibleStatusbar="True" Modal="False" DestroyOnClose="False">
        </telerik:RadWindow>
    </Windows>
</telerik:RadWindowManager>
</telerik:RadAjaxPanel>