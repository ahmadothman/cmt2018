﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ConnectedDeviceDetailsPage.aspx.cs" Inherits="FOWebApp.Nocsa.ConnectedDeviceDetailsPage" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Connected device details</title>
    <link href="../CMTStyle.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManagerConnectedDeviceDetails" runat="server">
        <Services>
            <asp:ServiceReference Path="../WebServices/AccountSupportWS.asmx" />
        </Services>
    </asp:ScriptManager>
    <div>
        <asp:PlaceHolder ID="PlaceHolderConnectedDeviceDetailsForm" runat="server"></asp:PlaceHolder>
    </div>
    </form>
</body>
</html>