﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ServicePackUpdate.ascx.cs"
    Inherits="FOWebApp.Nocsa.ServicePackUpdate" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<style type="text/css">
    .auto-style1 {
        height: 47px;
    }
    .auto-style2 {
        width: 557px;
    }
    .auto-style3 {
        height: 47px;
        width: 557px;
    }
</style>
<script type="text/javascript">
    function OnClientSelectedIndexChangedISP(sender, eventArgs) {
        var item = eventArgs.get_item();
        var panel = $find("<%=RadXmlHttpPanelSLA.ClientID %>");
        panel.set_value(item.get_value());
        return false;
    }

    function OnClientSelectedIndexChanged(sender, eventArgs) {
        var item = eventArgs.get_item();
        FOWebApp.WebServices.AccountSupportWS.IsSecondaryISP(item.get_value(), OnRequestComplete, OnError)
    }

    function OnRequestComplete(result) {
        var comboBoxEdgeISPId = $find("<%= RadComboBoxEdgeISPId.ClientID %>");
        var comboBoxEdgeSLA = $find("<%= RadComboBoxEdgeSLA.ClientID %>");
        comboBoxEdgeISPId.set_enabled(result);
        comboBoxEdgeSLA.set_enabled(result);
        comboBoxEdgeISPId.clearSelection();
        comboBoxEdgeSLA.clearSelection();
        var inputElementEdgeISP = comboBoxEdgeISPId.get_inputDomElement();
        var inputElementEdgeSLA = comboBoxEdgeSLA.get_inputDomElement();
        if (result) {

            inputElementEdgeISP.style.backgroundColor = "#FF9999";
            inputElementEdgeSLA.style.backgroundColor = "#FF9999";
        }
        else {
            inputElementEdgeISP.style.backgroundColor = "#FFFFFF";
            inputElementEdgeSLA.style.backgroundColor = "#FFFFFF";
        }
    }

    function OnError()
    {
        alert("Error while checking for secondary ISP");
    }
</script>
<table cellpadding="2" cellspacing="5">
    <tr>
        <td>
            Id:
        </td>
        <td class="auto-style2">
            <telerik:RadTextBox ID="TextBoxId"  runat="server" Columns="4" Width="177px" Skin="Metro"></telerik:RadTextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="You must provide a SLA ID!"
                ControlToValidate="TextBoxId" SetFocusOnError="True" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidatorId" runat="server" ErrorMessage="The SLA ID can only contain digits"
                ControlToValidate="TextBoxId" ValidationExpression="\d+" Display="Dynamic" />
            <asp:CustomValidator ID="CustomValidatorId" runat="server" ErrorMessage="There has already a service pack been found with this ID. Please provide a unique ID."
                OnServerValidate="CustomValidatorId_ServerValidate" ControlToValidate="TextBoxId" Display="Dynamic" />
        </td>
    </tr>
    <tr>
        <td>
            Name:
        </td>
        <td class="auto-style2">
            <telerik:RadTextBox ID="TextBoxName" runat="server" Columns="50" Skin="Metro">
            </telerik:RadTextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="You must provide a SLA Name!"
                ControlToValidate="TextBoxName" SetFocusOnError="True"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Forward FUP:
        </td>
        <td class="auto-style2">
            <telerik:RadNumericTextBox ID="RadNumericTextBoxFUPThreshold" runat="server" Culture="en-US" ToolTip="Value in GB" ShowSpinButtons="True" Skin="Metro" Value="0" Width="125px">
                <IncrementSettings Step="0.01" />
                <NumberFormat ZeroPattern="n"></NumberFormat>
            </telerik:RadNumericTextBox>
             GB
        </td>
    </tr>
    <tr>
        <td>
            Return FUP:
        </td>
        <td class="auto-style2">
            <telerik:RadNumericTextBox ID="RadNumericTextBoxRTNFUPThreshold" Culture="en-US" runat="server" ToolTip="Value in GB" ShowSpinButtons="True" Skin="Metro" Value="0" Width="125px">
                <IncrementSettings Step="0.01" />
                <NumberFormat ZeroPattern="n"></NumberFormat>
            </telerik:RadNumericTextBox>
             GB
        </td>
    </tr>
    <tr>
        <td class="auto-style1">
            Forward speed Above FUP:
        </td>
        <td class="auto-style3">
            <telerik:RadNumericTextBox ID="RadNumericTextBoxForwardAboveFup" runat="server" ShowSpinButtons="True" Culture="nl-BE" Skin="Metro" Value="0" Width="125px">
                <NumberFormat ZeroPattern="n" DecimalDigits="0"></NumberFormat>
            </telerik:RadNumericTextBox> Kbps
        </td>
    </tr>
    <tr>
        <td>
            Return speed Above FUP:
        </td>
        <td class="auto-style2">
            <telerik:RadNumericTextBox ID="RadNumericTextBoxReturnAboveFup" runat="server" ShowSpinButtons="True" Culture="nl-BE" Skin="Metro" Value="0" Width="125px">
                <NumberFormat ZeroPattern="n" DecimalDigits="0"></NumberFormat>
            </telerik:RadNumericTextBox> Kbps
        </td>
    </tr>
    <tr>
        <td class="auto-style1">
            Forward speed (NMS only):
        </td>
        <td class="auto-style3">
            <telerik:RadNumericTextBox ID="RadNumericTextBoxForwardDefault" runat="server" ShowSpinButtons="True" Culture="nl-BE" Skin="Metro" Value="0" Width="125px">
                <NumberFormat ZeroPattern="n" DecimalDigits="0"></NumberFormat>
            </telerik:RadNumericTextBox> Kbps
        </td>
    </tr>
    <tr>
        <td class="auto-style1">
            Return speed (NMS only):
        </td>
        <td class="auto-style3">
            <telerik:RadNumericTextBox ID="RadNumericTextBoxReturnDefault" runat="server" ShowSpinButtons="True" Culture="nl-BE" Skin="Metro" Value="0" Width="125px">
                <NumberFormat ZeroPattern="n" DecimalDigits="0"></NumberFormat>
            </telerik:RadNumericTextBox> Kbps
        </td>
    </tr>
    <tr>
        <td class="auto-style1">
            CIR forward (NMS only):
        </td>
        <td class="auto-style3">
            <telerik:RadNumericTextBox ID="RadNumericTextBoxCIRFWD" runat="server" ShowSpinButtons="True" Culture="nl-BE" Skin="Metro" Value="0" Width="125px">
                <NumberFormat ZeroPattern="n" DecimalDigits="0"></NumberFormat>
            </telerik:RadNumericTextBox> Kbps
        </td>
    </tr>
    <tr>
        <td class="auto-style1">
            CIR return (NMS only):
        </td>
        <td class="auto-style3">
            <telerik:RadNumericTextBox ID="RadNumericTextBoxCIRRTN" runat="server" ShowSpinButtons="True" Culture="nl-BE" Skin="Metro" Value="0" Width="125px">
                <NumberFormat ZeroPattern="n" DecimalDigits="0"></NumberFormat>
            </telerik:RadNumericTextBox> Kbps
        </td>
    </tr>
    <tr>
        <td><asp:Label ID="LabelMinBWFWD" runat="server">Minimum forward bandwidth (NMS only):</asp:Label></td>
        <td class="auto-style2">
            <telerik:RadNumericTextBox ID="RadNumericTextBoxMinBWFWD" runat="server" ShowSpinButtons="True" Culture="nl-BE" Skin="Metro" Value="0" Width="125px">
                <NumberFormat ZeroPattern="n" DecimalDigits="0"></NumberFormat>
            </telerik:RadNumericTextBox><asp:Label ID="LabelKbps7" runat="server"> Kbps</asp:Label>
        </td>
    </tr>
    <tr>
        <td><asp:Label ID="LabelMinBWRTN" runat="server">Minimum return bandwidth (NMS only):</asp:Label></td>
        <td class="auto-style2">
            <telerik:RadNumericTextBox ID="RadNumericTextBoxMinBWRTN" runat="server" ShowSpinButtons="True" Culture="nl-BE" Skin="Metro" Value="0" Width="125px">
                <NumberFormat ZeroPattern="n" DecimalDigits="0"></NumberFormat>
            </telerik:RadNumericTextBox><asp:Label ID="LabelKbps8" runat="server"> Kbps</asp:Label>
        </td>
    </tr>
    <tr>
        <td><asp:Label ID="LabelVolumeAgg" runat="server">NMS Volume type:</asp:Label></td>
        <td class="auto-style2">
            <asp:RadioButton runat="server" Checked="false" value="1" onclick="DisplayElement(this)" ID="RadioButtonVolAgg" Text="Aggregated volumes" GroupName="VolGroup" />&nbsp;<asp:RadioButton runat="server" Checked="false" value="2" onclick="DisplayElement(this)" ID="RadioButtonVolSep" Text="Separate volumes" GroupName="VolGroup" />
        </td>
    </tr>
    <tr>
        <td><asp:Label ID="LabelLowVolFWD" runat="server">Low Volume watermark Forward (NMS only):</asp:Label></td>
        <td class="auto-style2">
            <telerik:RadNumericTextBox ID="RadNumericTextBoxLowVolFWD" Culture="en-US" runat="server" ToolTip="Value in GB" ShowSpinButtons="True" Skin="Metro" Value="0" Width="125px">
                <IncrementSettings Step="0.01" />
                <NumberFormat ZeroPattern="n"></NumberFormat>
            </telerik:RadNumericTextBox>
                <asp:Label ID="LabelGB3" runat="server">GB</asp:Label>
        </td>
    </tr>
    <tr>
        <td><asp:Label ID="LabelLowVolRTN" runat="server">Low Volume watermark Return (NMS only):</asp:Label></td>
        <td class="auto-style2">
            <telerik:RadNumericTextBox ID="RadNumericTextBoxLowVolRTN" Culture="en-US" runat="server" ToolTip="Value in GB" ShowSpinButtons="True" Skin="Metro" Value="0" Width="125px">
                <IncrementSettings Step="0.01" />
                <NumberFormat ZeroPattern="n"></NumberFormat>
            </telerik:RadNumericTextBox>
                <asp:Label ID="LabelGB4" runat="server">GB</asp:Label>
        </td>
    </tr>
        <tr>
        <td><div id="agg3" ><asp:Label ID="Label3" runat="server">Low Volume watermark Sum (NMS only):</asp:Label></div></td>
        <td class="auto-style2">
            <div id="agg4" >
            <telerik:RadNumericTextBox ID="RadNumericTextBoxLowVolSUM" Culture="en-US" runat="server" ToolTip="Value in GB" ShowSpinButtons="True" Skin="Metro" Value="0" Width="125px">
                <IncrementSettings Step="0.01" />
                <NumberFormat ZeroPattern="n"></NumberFormat>
            </telerik:RadNumericTextBox>
                <asp:Label ID="Label4" runat="server">GB</asp:Label>
            </div>
        </td>
    </tr>
    <tr>
        <td><asp:Label ID="LabelHighVolFWD" runat="server">High Volume watermark Forward (NMS only):</asp:Label></td>
        <td class="auto-style2">
            <telerik:RadNumericTextBox ID="RadNumericTextBoxHighVolFWD" Culture="en-US" runat="server" ToolTip="Value in GB" ShowSpinButtons="True" Skin="Metro" Value="0" Width="125px">
                <IncrementSettings Step="0.01" />
                <NumberFormat ZeroPattern="n"></NumberFormat>
            </telerik:RadNumericTextBox>
                <asp:Label ID="LabelGB5" runat="server">GB</asp:Label>
        </td>
    </tr>
    <tr>
        <td><asp:Label ID="LabelHighVolRTN" runat="server">High Volume watermark Return (NMS only):</asp:Label></td>
        <td class="auto-style2">
            <telerik:RadNumericTextBox ID="RadNumericTextBoxHighVolRTN" Culture="en-US" runat="server" ToolTip="Value in GB" ShowSpinButtons="True" Skin="Metro" Value="0" Width="125px">
                <IncrementSettings Step="0.01" />
                <NumberFormat ZeroPattern="n"></NumberFormat>
            </telerik:RadNumericTextBox>
                <asp:Label ID="LabelGB6" runat="server">GB</asp:Label>
        </td>
    </tr>
    <tr>
        <td><div id="agg1" ><asp:Label ID="Label1" runat="server">High Volume watermark Sum (NMS only):</asp:Label></div></td>
        <td class="auto-style2">
            <div id="agg2" >
            <telerik:RadNumericTextBox ID="RadNumericTextBoxHighVolSUM" Culture="en-US" runat="server" ToolTip="Value in GB" ShowSpinButtons="True" Skin="Metro" Value="0" Width="125px">
                <IncrementSettings Step="0.01" />
                <NumberFormat ZeroPattern="n"></NumberFormat>
            </telerik:RadNumericTextBox>
                <asp:Label ID="Label2" runat="server">GB</asp:Label>
            </div>
        </td>
    </tr>
    <tr>
        <td>Weight:</td>
        <td class="auto-style2">
            <telerik:RadNumericTextBox ID="RadNumericTextBoxWeight" runat="server" MinValue="0" MaxValue="100" Culture="en-US" ShowSpinButtons="True" Skin="Metro" Width="100px" Value="0">
                <IncrementSettings Step="0.01" />
                <NumberFormat ZeroPattern="n" />
            </telerik:RadNumericTextBox>
        </td>
    </tr>
    <tr>
        <td>
            Common Name:
        </td>
        <td class="auto-style2">
            <telerik:RadTextBox ID="TextBoxSlaCommonName" runat="server"
                Columns="50" Skin="Metro">
            </telerik:RadTextBox>
        </td>
    </tr>
    <tr>
        <td>
            ISP:
        </td>
        <td class="auto-style2">
            <telerik:RadComboBox ID="RadComboBoxISPId" runat="server" Width="345px" OnClientSelectedIndexChanged="OnClientSelectedIndexChanged"
                ToolTip="If this SLA is managed by a Secondary ISP, please specify an Edge ISP" Skin="Metro"></telerik:RadComboBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="You must provide an ISP!"
                ControlToValidate="RadComboBoxISPId" SetFocusOnError="True"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Edge ISP:
        </td>
        <td class="auto-style2">
            <telerik:RadComboBox ID="RadComboBoxEdgeISPId" runat="server" Width="345px" ExpandDirection="Up" ToolTip="Specify the Edge ISP for this Secondary ISP" Skin="Metro" 
                OnClientSelectedIndexChanged="OnClientSelectedIndexChangedISP"></telerik:RadComboBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorEdgeIsp" runat="server" ErrorMessage="Please provide an edge ISP"
                ControlToValidate="RadComboBoxEdgeISPId" SetFocusOnError="True"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Edge SLA:
        </td>
        <td class="auto-style2">
            <telerik:RadXmlHttpPanel ID="RadXmlHttpPanelSLA" runat="server" EnableClientScriptEvaluation="True"
                OnServiceRequest="RadXmlHttpPanelSLA_ServiceRequest">
            <telerik:RadComboBox ID="RadComboBoxEdgeSLA" runat="server" Width="345px" ExpandDirection="Up" ToolTip="Specify the Edge ISP for this Secondary ISP" Skin="Metro"></telerik:RadComboBox>
            </telerik:RadXmlHttpPanel>
        </td>
    </tr>
    <tr>
        <td>Service class:</td>
        <td class="auto-style2">
            <telerik:RadComboBox ID="RadComboBoxServiceLine" runat="server" Width="300px" Skin="Metro" ></telerik:RadComboBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="You must select a Service Line!"
                ControlToValidate="RadComboBoxServiceLine" SetFocusOnError="True"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td><asp:label ID="LabelForVNO" runat="server">For VNO:</asp:label></td>
        <td class="auto-style2">
            <telerik:RadComboBox ID="RadComboBoxVNOId" runat="server" Width="345px" ExpandDirection="Up" Skin="Metro"></telerik:RadComboBox>
        </td>
    </tr>
    <%--<tr>
        <td>
            Business:
        </td>
        <td>
            <asp:CheckBox ID="CheckBoxBusinesss" runat="server" ToolTip="Check if  this is a business pack SLA" />
        </td>
    </tr>--%>
    <tr>
        <td><asp:label ID="LabelCheckBoxFreeZone" runat="server">Free Zone:</asp:label></td>
        <td class="auto-style2">
            <asp:CheckBox ID="CheckBoxFreeZone" runat="server" ToolTip="Check if this service pack contains the Freezone option" />
        </td>
    </tr>
    <tr>
        <td>
            VoIP:
        </td>
        <td class="auto-style2">
            <asp:CheckBox ID="CheckBoxVoIP" runat="server" ToolTip="Check if this service pack contains a VoIP option" />
        </td>
    </tr>
    <%--<tr>
        <td>
            Voucher:
        </td>
        <td>
            <asp:CheckBox ID="CheckBoxVoucher" runat="server" ToolTip="Check if the service pack is a voucher based SLA" />
        </td>
    </tr>--%>
    <tr>
        <td><asp:Label ID="LabelHide" runat="server">Hide for distributor:</asp:Label></td>
        <td class="auto-style2">
            <asp:CheckBox ID="CheckBoxHide" runat="server" ToolTip="This flag determines if the ServicePack is visible to distributors or not" />
        </td>
    </tr>
</table>
<br />
 <telerik:RadButton ID="buttonSubmit" Text= "Submit" runat="server" OnCommand="buttonSubmit_Command" OnClick="buttonSubmit_Click" Skin="Metro">
 </telerik:RadButton>
&nbsp;&nbsp;
<telerik:RadButton ID="ButtonDelete" Text="Delete" runat="server" Enabled="false"  CommandName="Delete" OnCommand="ButtonDelete_Command" CommandArgument="False"
    Skin="Metro" />
&nbsp;&nbsp;
<asp:Label ID="LabelResult" runat="server"></asp:Label>

