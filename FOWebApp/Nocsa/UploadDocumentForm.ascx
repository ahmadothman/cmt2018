﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UploadDocumentForm.ascx.cs" Inherits="FOWebApp.Nocsa.UploadDocumentForm" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
    <script language="javascript" type="text/javascript">
        function nodeClickingUsers(sender, args) {
            var comboBox = $find("<%= RadComboBoxUsers.ClientID %>");

        var node = args.get_node()
        if (node.get_level() != 0) {

            comboBox.set_text(node.get_text());
            comboBox.set_value(node.get_value());
            document.getElementById('HiddenFieldDistributorId').value = node.get_value();
            comboBox.trackChanges();
            comboBox.get_items().getItem(0).set_text(node.get_text());
            comboBox.commitChanges();

            comboBox.hideDropDown();
        }
    }
    </script>
</telerik:RadCodeBlock>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
</telerik:RadAjaxManagerProxy>
<table cellspacing="5px">
    <tr>
        <td style="vertical-align: top"><b>Upload document for:</b></td>
        <td>
            <asp:RadioButton runat="server" Checked="True" value="1" ID="RadioButtonEveryone" Text="Everyone" GroupName="RadioButtonRecipient" />
            <br />
            <asp:RadioButton runat="server" Checked="false" value="2" ID="RadioButtonVNOs" Text="All VNOs" GroupName="RadioButtonRecipient" />
            <br />
            <asp:RadioButton runat="server" Checked="false" value="3" ID="RadioButtonDistributors" Text="All Distributors, Suspended Distributors or Read Only Distributors" GroupName="RadioButtonRecipient" />
            <br />
            <asp:RadioButton runat="server" Checked="false" value="4" ID="RadioButtonCustomers" Text="All Customers" GroupName="RadioButtonRecipient" />
            <br />
            <asp:RadioButton runat="server" Checked="false" value="5" ID="RadioButtonEndUsers" Text="All End Users" GroupName="RadioButtonRecipient" />
            <br />
            <asp:RadioButton runat="server" Checked="false" value="6" ID="RadioButtonFinanceAdmin" Text="All Finance Administators" GroupName="RadioButtonRecipient" />
            <br />
            <asp:RadioButton runat="server" Checked="false" value="7" ID="RadioButtonMSO" Text="All Multicast Service Operators" GroupName="RadioButtonRecipient" />
            <br />
            <asp:RadioButton runat="server" Checked="false" value="8" ID="RadioButtonSpecific" Text="Specific:" GroupName="RadioButtonRecipient" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<telerik:RadComboBox ID="RadComboBoxUsers" runat="server" Skin="Metro" Sort="Ascending" Width="430px" EnableVirtualScrolling="True" EmptyMessage="Select a specific recipient"
                MaxHeight="300px" Height="300px" ExpandDirection="Down">
                <ItemTemplate>
                    <telerik:RadTreeView ID="RadTreeViewUsers" runat="server" Skin="Metro" OnClientNodeClicking="nodeClickingUsers">
                    </telerik:RadTreeView>
                </ItemTemplate>
                <Items>
                    <telerik:RadComboBoxItem Text="" />
                </Items>
                <ExpandAnimation Type="Linear" />
            </telerik:RadComboBox>
            <br />
            <asp:Label ID="LabelSelectRecipient" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>Document upload Description:</td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxDocDescription" runat="server" InputType="Text" TextMode="MultiLine" Height="76px" Width="533px"></telerik:RadTextBox>
        </td>
    </tr>
    <tr>
        <td>Date Uploaded:</td>
        <td>
            <asp:Label ID="LabelDateUploaded" runat="server" Text=""></asp:Label>
        </td>
    </tr>
    <tr>
        <td>Document(s) to Upload:
        </td>
        <td>

            <telerik:RadAsyncUpload ID="RadAsyncUploadDoc" runat="server" AllowedFileExtensions="pdf,txt,doc,docx,rtf,xls,xlsx,gif,png,dbf,zip" MultipleFileSelection="Automatic" OnFileUploaded="RadAsyncUploadDoc_FileUploaded" Skin="Metro">
            </telerik:RadAsyncUpload>

        </td>
    </tr>
    <tr>
        <td>
            <telerik:RadButton ID="RadButtonSubmit" runat="server" Text="Submit" OnClick="RadButtonSubmit_Click" Skin="Metro"></telerik:RadButton>
        </td>
        <td>
            <asp:Label ID="LabelResult" runat="server" Text=""></asp:Label>
        </td>
    </tr>
</table>
