﻿using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;
using FOWebApp.net.nimera.cmt.BOSatLinkManagerControlWSRef;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace FOWebApp.Nocsa
{
    public partial class AllBookings : System.Web.UI.UserControl
    {
        BOAccountingControlWS _accountingController = new BOAccountingControlWS();
        BOSatLinkManagerControlWS _satLinkController = new BOSatLinkManagerControlWS();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void RadButtonFilter_Click(object sender, EventArgs e)
        {
            DateTime startDate = Convert.ToDateTime(RadDatePickerStartDate.SelectedDate.ToString());
            DateTime endDate = Convert.ToDateTime(RadDatePickerEndDate.SelectedDate.ToString()).AddDays(1);
            SLMBooking[] bookings = _satLinkController.GetSLMBookingsBetweenDates(startDate, endDate);
            RadGridSLMBookings.DataSource = bookings;
            RadGridSLMBookings.DataBind();
        }

        protected void RadGridSLMBookings_ItemDataBound(object sender, Telerik.Web.UI.GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem dataItem = (GridDataItem)e.Item;

                string slaId = dataItem["BookingSla"].Text;
                if (!string.IsNullOrEmpty(slaId))
                {
                    try
                    {
                        BOAccountingControlWS boAccountingControl =
                                                new net.nimera.cmt.BOAccountingControlWSRef.BOAccountingControlWS();

                        //Display the Name of the SLA in stead of the ID
                        ServiceLevel sl = boAccountingControl.GetServicePack(int.Parse(slaId));
                        dataItem["BookingSla"].Text = sl.SlaName;
                    }
                    catch (Exception ex)
                    {
                        BOLogControlWS logController = new BOLogControlWS();
                        CmtApplicationException cmtAppEx = new CmtApplicationException
                        {
                            ExceptionDateTime = DateTime.UtcNow,
                            ExceptionDesc = ex.Message,
                            ExceptionStacktrace = ex.StackTrace,
                            ExceptionLevel = 2,
                            UserDescription = "TerminalList: The Service Pack Could not be found"
                        };
                        logController.LogApplicationException(cmtAppEx);
                    }
                }
            }
        }
    }
}