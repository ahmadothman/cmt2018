﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using Telerik.Web.UI;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Web.UI.DataVisualization.Charting;
using System.Globalization;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOMonitorControlWSRef;
using FOWebApp.net.nimera.cmt.BOUserControlWSRef;

namespace FOWebApp.Nocsa
{
    public partial class RoleList : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Session["_RoleList_PageSize"] == null)
            {
                RadGridRoles.PageSize = 20;
            }
            else
            {
                RadGridRoles.PageSize = (int)this.Session["_RoleList_PageSize"];
            }
            
            //Load data into the grid view. This view lists all services in the database.

            
            //Get the roles
            string[] roles = Roles.GetAllRoles(); 

            //Create role objects and fetch number of users
            List<RoleObject> roleObjects = new List<RoleObject>();
            RoleObject role;
            foreach (string r in roles)
            {
                role = new RoleObject();
                role.RoleName = r;
                role.UsersInRole = Roles.GetUsersInRole(r).Length;
                roleObjects.Add(role);
            }

            //Databind
            RadGridRoles.DataSource = roleObjects;
            RadGridRoles.DataBind();
        }
        
        protected void RadGridRoles_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridItemCollection selectedItems = RadGridRoles.SelectedItems;

            if (selectedItems.Count > 0)
            {
                GridItem selectedItem = selectedItems[0];
                RoleObject role = (RoleObject)selectedItem.DataItem;
                string roleName = role.RoleName;
            }
        }

        protected void RadGridRoles_ItemDataBound(object sender, GridItemEventArgs e)
        {
        }

        protected void RadWindowManager1_Disposed(object sender, EventArgs e)
        {
        }

        protected void RadWindowManager1_Unload(object sender, EventArgs e)
        {
        }

        protected void RadGridRoles_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            this.Session["_RoleList_PageSize"] = e.NewPageSize;
        }

        protected void RadGridRoles_ItemCommand(object sender, GridCommandEventArgs e)
        {
        }


        protected void RadGridRoles_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            //GridDataKeyArray dataItems = e.Item.OwnerTableView.DataKeyValues;
            //string serviceId = dataItems[e.Item.ItemIndex]["ServiceId"].ToString();
            //BOServicesControllerWS boServiceControl = new BOServicesControllerWS();
            //Service service = boServiceControl.GetService(serviceId);
            //bool deleted = boServiceControl.DeleteService(service);

            //if (!deleted)
            //{
            //    RadWindowManager1.RadAlert("Deleting service failed!", 250, 200, "Error", null);
            //}
            //this.Page_Load(sender, (EventArgs)e);
        }

        private class RoleObject
        {
            public string RoleName { get; set; }
            public int UsersInRole { get; set; }
        }
    }
}