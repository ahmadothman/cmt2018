﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TerminalForm.aspx.cs" Inherits="FOWebApp.Nocsa.TerminalForm" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="ActivityList" TagName="TerminalActivityList" Src="~/TerminalActivityTab.ascx" %>
<%@ Register TagPrefix="Tools" TagName="TerminalTools" Src="~/Nocsa/ToolsTab.ascx" %>
<!DOCTYPE html>
<html>
<head runat="server">
    <title>Terminal Editor</title>
    <link href="~/CMTStyle.css" rel="stylesheet" type="text/css" />
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <style type="text/css">
        html {
            height: 100%;
        }

        body {
            height: 100%;
            margin: 0;
            padding: 0;
        }

        #map_canvas {
            height: 401px;
        }
    </style>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBa2ts8udooyb7CZJGT4BrBj8b4XkoclDU&sensor=false"> 
    </script>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            var buttonClicked;

            function OnClientSelectedIndexChanged(sender, eventArgs) {
                var item = eventArgs.get_item();
                var panel = $find("<%=RadXmlHttpPanelSLA.ClientID %>");
                panel.set_value(item.get_value());
                return false;
            }

            function OnClientSelectedIndexChanged2(sender, eventArgs) {
                var item = eventArgs.get_item();
                var panel = $find("<%=RadXmlHttpPanelCustomer.ClientID %>");
                panel.set_value(item.get_value());
                return false;
            }

            function initialize() {
                var latlong = new google.maps.LatLng(7.274838, 10.919670);

                var mapOptions =
                {
                    zoom: 8,
                    center: latlong,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };

                var map = new google.maps.Map(document.getElementById("map_canvas"),
                                                                    mapOptions);

            }

            function showMacAddressHelp() {
                alert("A MAC address can only be changed if the terminal is locked. Changing a MAC address may take a few minutes to take effect.");
            }

            function AssociatedTermClicked(sender, eventArgs) {
                // jquery to get the content of the TextBox, filled in by the client
                var macAddress = $telerik.$('#<%= TextBoxAssociatedTerminal.ClientID %>').val();

                //Initialize and show a new terminal details window
                //var openTerminalDetails = radopen("TerminalForm.aspx?mac=" + macAddress, "RadWindowModifyTerminal");
                window.open("TerminalForm.aspx?mac=" + macAddress, "WindowPopup", "width=1000px, height=700px, resizable, scrollbars");
                return false;
            }

            function onButtonClicked(sender, args) {
                //Initialize and show the terminal details window
                //Check the button clicked
                var button = args.get_item();
                buttonClicked = button.get_index();

                if (button.get_text() == "Details") {
                    var macAddress = document.getElementById("TextBoxMacAddress").value;
                    var oManager = GetRadWindow().get_windowManager();
                    setTimeout(function () {
                        oManager.open("TerminalDetailsForm.aspx?mac=" + macAddress, "RadWindowTerminalDetails");
                    }, 0);
                }

                if (button.get_text() == "Delete") {
                    radconfirm('Are you sure you want to delete this terminal?', confirmDeleteCallBackFn, 400, 200, null, 'Terminal Deletion');
                }

                if (button.get_text() == "Activate") {
                    radconfirm('Are you sure you want to activate this terminal?', confirmActivateCallBackFn, 400, 200, null, 'Terminal Activation');
                }

                if (button.get_text() == "Reactivate") {
                    radconfirm('Are you sure you want to reactivate this terminal?', confirmReActivateCallBackFn, 400, 200, null, 'Terminal Reactivation');
                }

                if (button.get_text() == "Suspend") {

                    radconfirm('Are you sure you want to suspend this terminal?', confirmSuspendCallBackFn, 400, 200, null, 'Terminal Suspension');
                }

                if (button.get_text() == "Decommission") {

                    radconfirm('Are you sure you want to decommission this terminal?', confirmDecommissionCallBackFn, 400, 200, null, 'Terminal Decommission');
                }

                if (button.get_text() == "FUP Reset") {

                    radconfirm('Are you sure you want to reset the volume for this terminal?', confirmFUPResetCallBackFn, 400, 200, null, 'Volume Reset');
                }
            }

            function confirmDeleteCallBackFn(arg) {
                if (arg == true) {
                    __doPostBack("RadToolBarTerminal", buttonClicked); //Delete button idx.
                }
            }

            function confirmActivateCallBackFn(arg) {
                if (arg == true) {
                    __doPostBack("RadToolBarTerminal", buttonClicked); //Activate button idx.
                }
            }

            function confirmReActivateCallBackFn(arg) {
                if (arg == true) {
                    __doPostBack("RadToolBarTerminal", buttonClicked); //ReActivate button.
                }
            }

            function confirmSuspendCallBackFn(arg) {
                if (arg == true) {
                    __doPostBack("RadToolBarTerminal", buttonClicked); //Suspend button.
                }
            }

            function confirmDecommissionCallBackFn(arg) {
                if (arg == true) {
                    __doPostBack("RadToolBarTerminal", buttonClicked); //Decommission button.
                }
            }

            function confirmFUPResetCallBackFn(arg) {
                if (arg == true) {
                    __doPostBack("RadToolBarTerminal", buttonClicked); //Decommission button.
                }
            }


            function GetRadWindow() {
                var oWindow = null; if (window.radWindow)
                    oWindow = window.radWindow; else if (window.frameElement.radWindow)
                        oWindow = window.frameElement.radWindow; return oWindow;
            }

            function IPRangeChanged(chkBoxId) {
                if (chkBoxId.checked == true) {
                    document.getElementById('<%= TextBoxIPMask.ClientID %>').disabled = false;
                }
                else {
                    document.getElementById('<%= TextBoxIPMask.ClientID %>').disabled = true;
                }
            }
        </script>
    </telerik:RadCodeBlock>
</head>
<body onload="initialize()" bgcolor="#F7F7F7">
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" Height="100%" Width="100%">
            <div>
                <telerik:RadToolBar ID="RadToolBarTerminal" runat="server" BorderStyle="Groove" BorderWidth="1px"
                    EnableRoundedCorners="True" EnableShadows="True" Skin="Metro" Width="100%"
                    OnButtonClick="RadToolBarTerminal_ButtonClick" OnClientButtonClicked="onButtonClicked">
                    <Items>
                        <telerik:RadToolBarButton runat="server" ImageUrl="~/Images/floppy_disk.png" Owner="RadToolBarTerminal"
                            Text="Save" ToolTip="Submit terminal data" Value="Save_Button" CommandName="Save">
                        </telerik:RadToolBarButton>
                        <telerik:RadToolBarButton runat="server" ImageUrl="~/Images/refresh.png" Text="Refresh"
                            Value="Refresh_Button" ToolTip="Refresh the page">
                        </telerik:RadToolBarButton>
                        <telerik:RadToolBarButton runat="server" ImageUrl="~/Images/chart_column.png" Text="Details"
                            Value="Details" ToolTip="Retrieve terminal volume data">
                        </telerik:RadToolBarButton>
                        <telerik:RadToolBarButton runat="server" ImageUrl="~/Images/gear_run.png" Text="Activate"
                            Value="Activate_Button" Enabled="False" ToolTip="Activate a terminal" PostBack="False">
                        </telerik:RadToolBarButton>
                        <telerik:RadToolBarButton runat="server" Enabled="False" ImageUrl="~/Images/media_play_green.png"
                            Text="Reactivate" ToolTip="Reactivate a locked terminal" Value="ReActivate_Button"
                            PostBack="False">
                        </telerik:RadToolBarButton>
                        <telerik:RadToolBarButton runat="server" Enabled="False" ImageUrl="~/Images/stop.png"
                            Text="Suspend" ToolTip="Suspend an un-locked terminal" Value="Suspend_Button"
                            PostBack="False">
                        </telerik:RadToolBarButton>
                        <telerik:RadToolBarButton runat="server" Enabled="False" ImageUrl="~/Images/gear_stop.png"
                            Text="Decommission" ToolTip="Decommission a terminal." Value="Decommission_Button"
                            PostBack="False">
                        </telerik:RadToolBarButton>
                        <telerik:RadToolBarButton runat="server" Enabled="False" ImageUrl="~/Images/delete.png"
                            Text="Delete" Value="Delete_Button" PostBack="False">
                        </telerik:RadToolBarButton>
                        <telerik:RadToolBarButton runat="server" ImageUrl="~/Images/nav_undo_green.png" Text="FUP Reset"
                            ToolTip="Reset the volume for this terminal" Value="FUPReset_Button" PostBack="False">
                        </telerik:RadToolBarButton>
                    </Items>
                </telerik:RadToolBar>
                <table cellpadding="5" cellspacing="2">
                    <tr>
                        <td>
                            <asp:Literal ID="Literal1" runat="server">MacAddress:</asp:Literal>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxMacAddress" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="ImageButton1" runat="server" ImageAlign="Middle" ImageUrl="~/Images/help2.png"
                                OnClientClick="showMacAddressHelp()" />
                        </td>
                        <td>
                            <asp:Literal ID="Literal2" runat="server">Sit Id:</asp:Literal>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxSitId" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Literal ID="Literal6" runat="server">Terminal Name:</asp:Literal>
                        </td>
                        <td colspan="3">
                            <asp:TextBox ID="TextBoxTerminalName" runat="server" Width="398px" MaxLength="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Literal ID="Literal3" runat="server">ISP:</asp:Literal>
                        </td>
                        <td>
                            <telerik:RadComboBox ID="RadComboBoxISP" runat="server" Enabled="True" OnClientSelectedIndexChanged="OnClientSelectedIndexChanged"
                                Skin="Metro" Width="250px" EnableLoadOnDemand="True">
                            </telerik:RadComboBox>
                        </td>
                        <td>
                            <asp:Literal ID="Literal7" runat="server">Serial:</asp:Literal>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxSerial" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Literal ID="Literal4" runat="server">Service Level Agreement:</asp:Literal>
                        </td>
                        <td>
                            <telerik:RadXmlHttpPanel ID="RadXmlHttpPanelSLA" runat="server" EnableClientScriptEvaluation="True"
                                OnServiceRequest="RadXmlHttpPanelSLA_ServiceRequest">
                                <telerik:RadComboBox ID="RadComboBoxSLA" runat="server" Skin="Metro" Width="250px" EnableLoadOnDemand="True">
                                </telerik:RadComboBox>
                            </telerik:RadXmlHttpPanel>
                        </td>
                        <td>
                            Base IP:</td>
                        <td>
                            <asp:TextBox ID="TextBoxIP" runat="server" Columns="39"></asp:TextBox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:CheckBox ID="CheckBoxTestMode" runat="server" Text="Not-Billable" TextAlign="Left" />
                            <asp:CheckBox ID="CheckBoxCurrentBool" runat="server" Visible="false"></asp:CheckBox>
                        </td>
                        <td>
                            Weight: <asp:TextBox ID="TextBoxWeight" runat="server" Columns="5" Width="60px"></asp:TextBox>
                        </td>
                        <td>
                            IP Mask:</td>
                        <td>
                            <asp:TextBox ID="TextBoxIPMask" runat="server" Columns="5" Width="60px"></asp:TextBox>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:CheckBox ID="CheckBoxIPRange" runat="server" Text="IP Range" TextAlign="Left" OnClick="javascript:IPRangeChanged(this);" AutoPostBack="False"/>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:CheckBox ID="CheckBoxStaticIP" runat="server" Text="Static IP" TextAlign="Left" />
                        </td>
                    </tr>
                </table>
                <telerik:RadTabStrip ID="RadTabStripTerminal" runat="server" Skin="Metro"
                    MultiPageID="RadMultiPageTerminal" SelectedIndex="0">
                    <Tabs>
                        <telerik:RadTab runat="server" Text="Address" Selected="True" ImageUrl="~/Images/symbol_at.png">
                        </telerik:RadTab>
                        <telerik:RadTab runat="server" Text="Subscription" ImageUrl="~/Images/form_blue_edit.png">
                        </telerik:RadTab>
                        <telerik:RadTab runat="server" Text="GeoLocation" ImageUrl="~/Images/earth_preferences.png">
                        </telerik:RadTab>
                        <telerik:RadTab runat="server" Text="Hardware" ImageUrl="~/Images/satellite.png">
                        </telerik:RadTab>
                        <telerik:RadTab runat="server" Text="Remarks" ImageUrl="~/Images/edit.png">
                        </telerik:RadTab>
                        <telerik:RadTab runat="server" Text="Terminal Activity" ImageUrl="~/Images/Terminal.png">
                        </telerik:RadTab>
                        <telerik:RadTab Id="RadTabTools" runat="server" Text="Tools" Visible="False" ImageUrl="~/Images/skull.png">
                        </telerik:RadTab>
                    </Tabs>
                </telerik:RadTabStrip>
                <telerik:RadMultiPage ID="RadMultiPageTerminal" runat="server" BorderStyle="Groove"
                    BorderWidth="1px" Height="500px" SelectedIndex="0" BackColor="#999999">
                    <telerik:RadPageView ID="RadPageViewAddress" runat="server" Width="100%">
                        <table cellpadding="5" cellspacing="2">
                            <tr>
                                <td>
                                    <span class="tablabel">Address Line 1:</span>
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBoxAddressLine1" runat="server" Width="350px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="tablabel">Address Line 2:</span>
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBoxAddressLine2" runat="server" Width="350px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="tablabel">City/Town:</span>
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBoxLocation" runat="server" Width="350px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="tablabel">Postal Code:</span>
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBoxPCO" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="tablabel">Country:</span>
                                </td>
                                <td>
                                    <asp:DropDownList ID="CountryList" runat="server">
                                        <asp:ListItem Value="AF">Afghanistan</asp:ListItem>
                                        <asp:ListItem Value="AL">Albania</asp:ListItem>
                                        <asp:ListItem Value="DZ">Algeria</asp:ListItem>
                                        <asp:ListItem Value="AS">American Samoa</asp:ListItem>
                                        <asp:ListItem Value="AD">Andorra</asp:ListItem>
                                        <asp:ListItem Value="AO">Angola</asp:ListItem>
                                        <asp:ListItem Value="AI">Anguilla</asp:ListItem>
                                        <asp:ListItem Value="AQ">Antarctica</asp:ListItem>
                                        <asp:ListItem Value="AG">Antigua And Barbuda</asp:ListItem>
                                        <asp:ListItem Value="AR">Argentina</asp:ListItem>
                                        <asp:ListItem Value="AM">Armenia</asp:ListItem>
                                        <asp:ListItem Value="AW">Aruba</asp:ListItem>
                                        <asp:ListItem Value="AU">Australia</asp:ListItem>
                                        <asp:ListItem Value="AT">Austria</asp:ListItem>
                                        <asp:ListItem Value="AZ">Azerbaijan</asp:ListItem>
                                        <asp:ListItem Value="BS">Bahamas</asp:ListItem>
                                        <asp:ListItem Value="BH">Bahrain</asp:ListItem>
                                        <asp:ListItem Value="BD">Bangladesh</asp:ListItem>
                                        <asp:ListItem Value="BB">Barbados</asp:ListItem>
                                        <asp:ListItem Value="BY">Belarus</asp:ListItem>
                                        <asp:ListItem Value="BE">Belgium</asp:ListItem>
                                        <asp:ListItem Value="BZ">Belize</asp:ListItem>
                                        <asp:ListItem Value="BJ">Benin</asp:ListItem>
                                        <asp:ListItem Value="BM">Bermuda</asp:ListItem>
                                        <asp:ListItem Value="BT">Bhutan</asp:ListItem>
                                        <asp:ListItem Value="BO">Bolivia</asp:ListItem>
                                        <asp:ListItem Value="BA">Bosnia And Herzegowina</asp:ListItem>
                                        <asp:ListItem Value="BW">Botswana</asp:ListItem>
                                        <asp:ListItem Value="BV">Bouvet Island</asp:ListItem>
                                        <asp:ListItem Value="BR">Brazil</asp:ListItem>
                                        <asp:ListItem Value="IO">British Indian Ocean Territory</asp:ListItem>
                                        <asp:ListItem Value="BN">Brunei Darussalam</asp:ListItem>
                                        <asp:ListItem Value="BG">Bulgaria</asp:ListItem>
                                        <asp:ListItem Value="BF">Burkina Faso</asp:ListItem>
                                        <asp:ListItem Value="BI">Burundi</asp:ListItem>
                                        <asp:ListItem Value="KH">Cambodia</asp:ListItem>
                                        <asp:ListItem Value="CM">Cameroon</asp:ListItem>
                                        <asp:ListItem Value="CA">Canada</asp:ListItem>
                                        <asp:ListItem Value="CV">Cape Verde</asp:ListItem>
                                        <asp:ListItem Value="KY">Cayman Islands</asp:ListItem>
                                        <asp:ListItem Value="CF">Central African Republic</asp:ListItem>
                                        <asp:ListItem Value="TD">Chad</asp:ListItem>
                                        <asp:ListItem Value="CL">Chile</asp:ListItem>
                                        <asp:ListItem Value="CN">China</asp:ListItem>
                                        <asp:ListItem Value="CX">Christmas Island</asp:ListItem>
                                        <asp:ListItem Value="CC">Cocos (Keeling) Islands</asp:ListItem>
                                        <asp:ListItem Value="CO">Colombia</asp:ListItem>
                                        <asp:ListItem Value="KM">Comoros</asp:ListItem>
                                        <asp:ListItem Value="CG">Congo</asp:ListItem>
                                        <asp:ListItem Value="CK">Cook Islands</asp:ListItem>
                                        <asp:ListItem Value="CR">Costa Rica</asp:ListItem>
                                        <asp:ListItem Value="CI">Cote D'Ivoire</asp:ListItem>
                                        <asp:ListItem Value="HR">Croatia (Local Name: Hrvatska)</asp:ListItem>
                                        <asp:ListItem Value="CU">Cuba</asp:ListItem>
                                        <asp:ListItem Value="CY">Cyprus</asp:ListItem>
                                        <asp:ListItem Value="CZ">Czech Republic</asp:ListItem>
                                        <asp:ListItem Value="DK">Denmark</asp:ListItem>
                                        <asp:ListItem Value="DJ">Djibouti</asp:ListItem>
                                        <asp:ListItem Value="DM">Dominica</asp:ListItem>
                                        <asp:ListItem Value="DO">Dominican Republic</asp:ListItem>
                                        <asp:ListItem Value="TP">East Timor</asp:ListItem>
                                        <asp:ListItem Value="EC">Ecuador</asp:ListItem>
                                        <asp:ListItem Value="EG">Egypt</asp:ListItem>
                                        <asp:ListItem Value="SV">El Salvador</asp:ListItem>
                                        <asp:ListItem Value="GQ">Equatorial Guinea</asp:ListItem>
                                        <asp:ListItem Value="ER">Eritrea</asp:ListItem>
                                        <asp:ListItem Value="EE">Estonia</asp:ListItem>
                                        <asp:ListItem Value="ET">Ethiopia</asp:ListItem>
                                        <asp:ListItem Value="FK">Falkland Islands (Malvinas)</asp:ListItem>
                                        <asp:ListItem Value="FO">Faroe Islands</asp:ListItem>
                                        <asp:ListItem Value="FJ">Fiji</asp:ListItem>
                                        <asp:ListItem Value="FI">Finland</asp:ListItem>
                                        <asp:ListItem Value="FR">France</asp:ListItem>
                                        <asp:ListItem Value="GF">French Guiana</asp:ListItem>
                                        <asp:ListItem Value="PF">French Polynesia</asp:ListItem>
                                        <asp:ListItem Value="TF">French Southern Territories</asp:ListItem>
                                        <asp:ListItem Value="GA">Gabon</asp:ListItem>
                                        <asp:ListItem Value="GM">Gambia</asp:ListItem>
                                        <asp:ListItem Value="GE">Georgia</asp:ListItem>
                                        <asp:ListItem Value="DE">Germany</asp:ListItem>
                                        <asp:ListItem Value="GH">Ghana</asp:ListItem>
                                        <asp:ListItem Value="GI">Gibraltar</asp:ListItem>
                                        <asp:ListItem Value="GR">Greece</asp:ListItem>
                                        <asp:ListItem Value="GL">Greenland</asp:ListItem>
                                        <asp:ListItem Value="GD">Grenada</asp:ListItem>
                                        <asp:ListItem Value="GP">Guadeloupe</asp:ListItem>
                                        <asp:ListItem Value="GU">Guam</asp:ListItem>
                                        <asp:ListItem Value="GT">Guatemala</asp:ListItem>
                                        <asp:ListItem Value="GN">Guinea</asp:ListItem>
                                        <asp:ListItem Value="GW">Guinea-Bissau</asp:ListItem>
                                        <asp:ListItem Value="GY">Guyana</asp:ListItem>
                                        <asp:ListItem Value="HT">Haiti</asp:ListItem>
                                        <asp:ListItem Value="HM">Heard And Mc Donald Islands</asp:ListItem>
                                        <asp:ListItem Value="VA">Holy See (Vatican City State)</asp:ListItem>
                                        <asp:ListItem Value="HN">Honduras</asp:ListItem>
                                        <asp:ListItem Value="HK">Hong Kong</asp:ListItem>
                                        <asp:ListItem Value="HU">Hungary</asp:ListItem>
                                        <asp:ListItem Value="IS">Icel And</asp:ListItem>
                                        <asp:ListItem Value="IN">India</asp:ListItem>
                                        <asp:ListItem Value="ID">Indonesia</asp:ListItem>
                                        <asp:ListItem Value="IR">Iran (Islamic Republic Of)</asp:ListItem>
                                        <asp:ListItem Value="IQ">Iraq</asp:ListItem>
                                        <asp:ListItem Value="IE">Ireland</asp:ListItem>
                                        <asp:ListItem Value="IL">Israel</asp:ListItem>
                                        <asp:ListItem Value="IT">Italy</asp:ListItem>
                                        <asp:ListItem Value="JM">Jamaica</asp:ListItem>
                                        <asp:ListItem Value="JP">Japan</asp:ListItem>
                                        <asp:ListItem Value="JO">Jordan</asp:ListItem>
                                        <asp:ListItem Value="KZ">Kazakhstan</asp:ListItem>
                                        <asp:ListItem Value="KE">Kenya</asp:ListItem>
                                        <asp:ListItem Value="KI">Kiribati</asp:ListItem>
                                        <asp:ListItem Value="KP">Korea, Dem People'S Republic</asp:ListItem>
                                        <asp:ListItem Value="KR">Korea, Republic Of</asp:ListItem>
                                        <asp:ListItem Value="KW">Kuwait</asp:ListItem>
                                        <asp:ListItem Value="KG">Kyrgyzstan</asp:ListItem>
                                        <asp:ListItem Value="LA">Lao People'S Dem Republic</asp:ListItem>
                                        <asp:ListItem Value="LV">Latvia</asp:ListItem>
                                        <asp:ListItem Value="LB">Lebanon</asp:ListItem>
                                        <asp:ListItem Value="LS">Lesotho</asp:ListItem>
                                        <asp:ListItem Value="LR">Liberia</asp:ListItem>
                                        <asp:ListItem Value="LY">Libyan Arab Jamahiriya</asp:ListItem>
                                        <asp:ListItem Value="LI">Liechtenstein</asp:ListItem>
                                        <asp:ListItem Value="LT">Lithuania</asp:ListItem>
                                        <asp:ListItem Value="LU">Luxembourg</asp:ListItem>
                                        <asp:ListItem Value="MO">Macau</asp:ListItem>
                                        <asp:ListItem Value="MK">Macedonia</asp:ListItem>
                                        <asp:ListItem Value="MG">Madagascar</asp:ListItem>
                                        <asp:ListItem Value="MW">Malawi</asp:ListItem>
                                        <asp:ListItem Value="MY">Malaysia</asp:ListItem>
                                        <asp:ListItem Value="MV">Maldives</asp:ListItem>
                                        <asp:ListItem Value="ML">Mali</asp:ListItem>
                                        <asp:ListItem Value="MT">Malta</asp:ListItem>
                                        <asp:ListItem Value="MH">Marshall Islands</asp:ListItem>
                                        <asp:ListItem Value="MQ">Martinique</asp:ListItem>
                                        <asp:ListItem Value="MR">Mauritania</asp:ListItem>
                                        <asp:ListItem Value="MU">Mauritius</asp:ListItem>
                                        <asp:ListItem Value="YT">Mayotte</asp:ListItem>
                                        <asp:ListItem Value="MX">Mexico</asp:ListItem>
                                        <asp:ListItem Value="FM">Micronesia, Federated States</asp:ListItem>
                                        <asp:ListItem Value="MD">Moldova, Republic Of</asp:ListItem>
                                        <asp:ListItem Value="MC">Monaco</asp:ListItem>
                                        <asp:ListItem Value="MN">Mongolia</asp:ListItem>
                                        <asp:ListItem Value="MS">Montserrat</asp:ListItem>
                                        <asp:ListItem Value="MA">Morocco</asp:ListItem>
                                        <asp:ListItem Value="MZ">Mozambique</asp:ListItem>
                                        <asp:ListItem Value="MM">Myanmar</asp:ListItem>
                                        <asp:ListItem Value="NA">Namibia</asp:ListItem>
                                        <asp:ListItem Value="NR">Nauru</asp:ListItem>
                                        <asp:ListItem Value="NP">Nepal</asp:ListItem>
                                        <asp:ListItem Value="NL">Netherlands</asp:ListItem>
                                        <asp:ListItem Value="AN">Netherlands Ant Illes</asp:ListItem>
                                        <asp:ListItem Value="NC">New Caledonia</asp:ListItem>
                                        <asp:ListItem Value="NZ" Selected="True">New Zealand</asp:ListItem>
                                        <asp:ListItem Value="NI">Nicaragua</asp:ListItem>
                                        <asp:ListItem Value="NE">Niger</asp:ListItem>
                                        <asp:ListItem Value="NG">Nigeria</asp:ListItem>
                                        <asp:ListItem Value="NU">Niue</asp:ListItem>
                                        <asp:ListItem Value="NF">Norfolk Island</asp:ListItem>
                                        <asp:ListItem Value="MP">Northern Mariana Islands</asp:ListItem>
                                        <asp:ListItem Value="NO">Norway</asp:ListItem>
                                        <asp:ListItem Value="OM">Oman</asp:ListItem>
                                        <asp:ListItem Value="PK">Pakistan</asp:ListItem>
                                        <asp:ListItem Value="PW">Palau</asp:ListItem>
                                        <asp:ListItem Value="PA">Panama</asp:ListItem>
                                        <asp:ListItem Value="PG">Papua New Guinea</asp:ListItem>
                                        <asp:ListItem Value="PY">Paraguay</asp:ListItem>
                                        <asp:ListItem Value="PE">Peru</asp:ListItem>
                                        <asp:ListItem Value="PH">Philippines</asp:ListItem>
                                        <asp:ListItem Value="PN">Pitcairn</asp:ListItem>
                                        <asp:ListItem Value="PL">Poland</asp:ListItem>
                                        <asp:ListItem Value="PT">Portugal</asp:ListItem>
                                        <asp:ListItem Value="PR">Puerto Rico</asp:ListItem>
                                        <asp:ListItem Value="QA">Qatar</asp:ListItem>
                                        <asp:ListItem Value="CD">République Démocratique du Congo</asp:ListItem>
                                        <asp:ListItem Value="RE">Reunion</asp:ListItem>
                                        <asp:ListItem Value="RO">Romania</asp:ListItem>
                                        <asp:ListItem Value="RU">Russian Federation</asp:ListItem>
                                        <asp:ListItem Value="RW">Rwanda</asp:ListItem>
                                        <asp:ListItem Value="KN">Saint K Itts And Nevis</asp:ListItem>
                                        <asp:ListItem Value="LC">Saint Lucia</asp:ListItem>
                                        <asp:ListItem Value="VC">Saint Vincent, The Grenadines</asp:ListItem>
                                        <asp:ListItem Value="WS">Samoa</asp:ListItem>
                                        <asp:ListItem Value="SM">San Marino</asp:ListItem>
                                        <asp:ListItem Value="ST">Sao Tome And Principe</asp:ListItem>
                                        <asp:ListItem Value="SA">Saudi Arabia</asp:ListItem>
                                        <asp:ListItem Value="SN">Senegal</asp:ListItem>
                                        <asp:ListItem Value="SC">Seychelles</asp:ListItem>
                                        <asp:ListItem Value="SL">Sierra Leone</asp:ListItem>
                                        <asp:ListItem Value="SG">Singapore</asp:ListItem>
                                        <asp:ListItem Value="SK">Slovakia (Slovak Republic)</asp:ListItem>
                                        <asp:ListItem Value="SI">Slovenia</asp:ListItem>
                                        <asp:ListItem Value="SB">Solomon Islands</asp:ListItem>
                                        <asp:ListItem Value="SO">Somalia</asp:ListItem>
                                        <asp:ListItem Value="ZA">South Africa</asp:ListItem>
                                        <asp:ListItem Value="GS">South Georgia , S Sandwich Is.</asp:ListItem>
                                        <asp:ListItem Value="ES">Spain</asp:ListItem>
                                        <asp:ListItem Value="LK">Sri Lanka</asp:ListItem>
                                        <asp:ListItem Value="SH">St. Helena</asp:ListItem>
                                        <asp:ListItem Value="PM">St. Pierre And Miquelon</asp:ListItem>
                                        <asp:ListItem Value="SD">Sudan</asp:ListItem>
                                        <asp:ListItem Value="SR">Suriname</asp:ListItem>
                                        <asp:ListItem Value="SJ">Svalbard, Jan Mayen Islands</asp:ListItem>
                                        <asp:ListItem Value="SZ">Sw Aziland</asp:ListItem>
                                        <asp:ListItem Value="SE">Sweden</asp:ListItem>
                                        <asp:ListItem Value="CH">Switzerland</asp:ListItem>
                                        <asp:ListItem Value="SY">Syrian Arab Republic</asp:ListItem>
                                        <asp:ListItem Value="TW">Taiwan</asp:ListItem>
                                        <asp:ListItem Value="TJ">Tajikistan</asp:ListItem>
                                        <asp:ListItem Value="TZ">Tanzania, United Republic Of</asp:ListItem>
                                        <asp:ListItem Value="TH">Thailand</asp:ListItem>
                                        <asp:ListItem Value="TG">Togo</asp:ListItem>
                                        <asp:ListItem Value="TK">Tokelau</asp:ListItem>
                                        <asp:ListItem Value="TO">Tonga</asp:ListItem>
                                        <asp:ListItem Value="TT">Trinidad And Tobago</asp:ListItem>
                                        <asp:ListItem Value="TN">Tunisia</asp:ListItem>
                                        <asp:ListItem Value="TR">Turkey</asp:ListItem>
                                        <asp:ListItem Value="TM">Turkmenistan</asp:ListItem>
                                        <asp:ListItem Value="TC">Turks And Caicos Islands</asp:ListItem>
                                        <asp:ListItem Value="TV">Tuvalu</asp:ListItem>
                                        <asp:ListItem Value="UG">Uganda</asp:ListItem>
                                        <asp:ListItem Value="UA">Ukraine</asp:ListItem>
                                        <asp:ListItem Value="AE">United Arab Emirates</asp:ListItem>
                                        <asp:ListItem Value="GB">United Kingdom</asp:ListItem>
                                        <asp:ListItem Value="US">United States</asp:ListItem>
                                        <asp:ListItem Value="UM">United States Minor Is.</asp:ListItem>
                                        <asp:ListItem Value="UY">Uruguay</asp:ListItem>
                                        <asp:ListItem Value="UZ">Uzbekistan</asp:ListItem>
                                        <asp:ListItem Value="VU">Vanuatu</asp:ListItem>
                                        <asp:ListItem Value="VE">Venezuela</asp:ListItem>
                                        <asp:ListItem Value="VN">Viet Nam</asp:ListItem>
                                        <asp:ListItem Value="VG">Virgin Islands (British)</asp:ListItem>
                                        <asp:ListItem Value="VI">Virgin Islands (U.S.)</asp:ListItem>
                                        <asp:ListItem Value="WF">Wallis And Futuna Islands</asp:ListItem>
                                        <asp:ListItem Value="EH">Western Sahara</asp:ListItem>
                                        <asp:ListItem Value="YE">Yemen</asp:ListItem>
                                        <asp:ListItem Value="YU">Yugoslavia</asp:ListItem>
                                        <asp:ListItem Value="ZM">Zambia</asp:ListItem>
                                        <asp:ListItem Value="ZW">Zimbabwe</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="tablabel">Phone:</span>
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBoxPhone" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="tablabel">Fax:</span>
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBoxFax" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="tablabel">E-Mail:</span>
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBoxEMail" runat="server" Width="350px"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </telerik:RadPageView>
                    <telerik:RadPageView ID="RadPageViewSubscription" runat="server" Width="100%">
                        <table cellpadding="5" cellspacing="2">
                            <tr>
                                <td>
                                    <span class="tablabel">Distributor:</span>
                                </td>
                                <td>
                                    <telerik:RadComboBox ID="RadComboBoxDistributor" runat="server" OnClientSelectedIndexChanged="OnClientSelectedIndexChanged2" Skin="Metro">
                                    </telerik:RadComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="tablabel">Customer:</span>
                                </td>
                                <td>
                                    <telerik:RadXmlHttpPanel ID="RadXmlHttpPanelCustomer" runat="server" EnableClientScriptEvaluation="True"
                                        OnServiceRequest="RadXmlHttpPanelCustomer_ServiceRequest2">
                                        <telerik:RadComboBox ID="RadComboBoxCustomer" runat="server" Skin="Metro">
                                        </telerik:RadComboBox>
                                    </telerik:RadXmlHttpPanel>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="tablabel">Free zone:</span>
                                </td>
                                <td>
                                    <Telerik:RadComboBox ID="RadComboBoxFreeZone" runat="server" Skin="Metro"></Telerik:RadComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="tablabel">Start Date:</span>
                                </td>
                                <td>
                                    <telerik:RadDatePicker ID="RadDatePickerStartDate" runat="server">
                                        <Calendar Skin="Metro" UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False"
                                            ViewSelectorText="x" runat="server">
                                        </Calendar>
                                        <DateInput DateFormat="d/MM/yyyy" DisplayDateFormat="d/MM/yyyy" runat="server">
                                        </DateInput>
                                    </telerik:RadDatePicker>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="tablabel">Expiry Date:</span>
                                </td>
                                <td>
                                    <telerik:RadDatePicker ID="RadDatePickerExpiryDate" runat="server">
                                        <Calendar Skin="Metro" UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False"
                                            ViewSelectorText="x" runat="server">
                                        </Calendar>
                                        <DateInput DateFormat="d/MM/yyyy" DisplayDateFormat="d/MM/yyyy" runat="server">
                                        </DateInput>
                                    </telerik:RadDatePicker>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="tablabel">Admin Status:</span>
                                </td>
                                <td>
                                    <asp:Label ID="LabelStatus" runat="server" Font-Bold="True"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="tablabel">
                                        Terminal Invoicing:
                                    </span>
                                    <asp:Image ID="ImageTerminalInvoicingTooltip" runat="server" ImageUrl="~/Images/help2.png"
                                        AlternateText="Terminal Invoicing Help" Style="cursor: help;" />

                                    <telerik:RadToolTip ID="RadToolTipTerminalInvoicing" runat="server" ShowEvent="OnMouseOver"
                                        HideEvent="LeaveTargetAndToolTip" TargetControlID="ImageTerminalInvoicingTooltip" Position="BottomCenter"
                                        RelativeTo="Element" AutoCloseDelay="10000"
                                        Text="Invoice this terminal using the 'Invoice Interval'<br />that has been set at the terminal level<br />instead of the one that has been set at the distributor level.">
                                    </telerik:RadToolTip>
                                </td>
                                <td>
                                    <asp:CheckBox ID="CheckBoxTerminalInvoicing" runat="server" Checked="false" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="tablabel">
                                        Invoice Interval:
                                    </span>
                                </td>
                                <td>
                                    <span class="tablabel">every</span>
                                    <telerik:RadNumericTextBox ID="RadNumericTextBoxInvoiceInterval" runat="server" Width="55px"
                                        MinValue="1" MaxValue="12" Value="1" ShowSpinButtons="true">
                                        <NumberFormat AllowRounding="true" DecimalDigits="0" GroupSeparator=" " GroupSizes="3" />
                                    </telerik:RadNumericTextBox>
                                    <span class="tablabel">months</span>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorInvoiceInterval" runat="server"
                                        ControlToValidate="RadNumericTextBoxInvoiceInterval" ErrorMessage="Please insert an invoice interval"
                                        Display="Dynamic">
                                    </asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="tablabel">Last Invoice Date:</span>
                                </td>
                                <td>
                                    <span class="tablabel">
                                        <%--<asp:Literal ID="LiteralLastInvoiceDate" runat="server"></asp:Literal>--%>
                                        <telerik:RadDatePicker ID="RadDatePickerLastInvoice" runat="server"></telerik:RadDatePicker>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="tablabel" Visible="false">Redundant setup:</span>
                                </td>
                                <td>
                                    <asp:CheckBox ID="CheckBoxRedundantSetup" runat="server" Checked="false" enabled="false" Visible="true"/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="tablabel" Visible="false">Primary/secondary:</span>
                                </td>
                                <td>
                                    <Telerik:RadComboBox ID="RadComboBoxPrimary" runat="server" Skin="Metro" Visible="true"></Telerik:RadComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="tablabel" Visible="false">Associated terminal:</span>
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBoxAssociatedTerminal" runat="server" Visible="true"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:Button ID="ButtonAssociatedTerminal" runat="server" Text="Open Terminal form" OnClientClick="AssociatedTermClicked(); return false;" Visible="true" />
                                </td>
                            </tr>
                            <tr>
                                <td VALIGN="top">
                                    <span class="tablabel">Connected device(s):</span>
                                </td>
                                <td><span class="tablabel"><asp:Label ID="LabelConnectedDevices" runat="server"></asp:Label></span></td>
                            </tr>
                        </table>
                    </telerik:RadPageView>
                    <telerik:RadPageView ID="RadPageViewLocation" runat="server" Width="100%">
                        <table cellpadding="5" cellspacing="2">
                            <tr>
                                <td>
                                    <span class="tablabel">Latitude:</span>
                                </td>
                                <td valign="top">
                                    <asp:TextBox ID="TextBoxLatitude" runat="server"></asp:TextBox>
                                </td>
                                <td>
                                    <span class="tablabel">Longitude:</span>
                                </td>
                                <td valign="top">
                                    <asp:TextBox ID="TextBoxLongitude" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                        <div id="map_canvas" width="500px" heigth="300px">
                        </div>
                    </telerik:RadPageView>
                    <telerik:RadPageView ID="RadPageViewHub" runat="server" Width="100%">
                        <table cellpadding="5" cellspacing="2">
                            <tr>
                                <td>
                                    <span class="tablabel">Blade:</span>
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBoxBlade" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="tablabel">IP Management:</span>
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBoxIPMgm" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="tablabel">IP Tunnel:</span>
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBoxIPTunnel" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="tablabel">Return Pool:</span>
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBoxRTPool" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="tablabel">Forward Pool:</span>
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBoxFWPool" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="tablabel">Spot ID:</span>
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBoxSpotID" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="tablabel">Modem:</span>
                                </td>
                                <td>
                                    <telerik:RadComboBox ID="RadComboBoxModem" runat="server" Skin="Metro" Width="100px">
                                        <Items>
                                            <telerik:RadComboBoxItem Text="" Value="0" />
                                            <telerik:RadComboBoxItem Text="NTC/2250" Value="1" />
                                            <telerik:RadComboBoxItem Text="MDM/2200" Value="2" />
                                            <telerik:RadComboBoxItem Text="MDM/3100" Value="3" />
                                             <telerik:RadComboBoxItem runat="server" Text="iD_X1" Value="4" />
                                            <telerik:RadComboBoxItem runat="server" Text="iD_X3" Value="5" />
                                            <telerik:RadComboBoxItem runat="server" Text="iD_X5" Value="6" />
                                            <telerik:RadComboBoxItem runat="server" Text="iD_X7" Value="7" />
                                        </Items>
                                    </telerik:RadComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="tablabel">iLNB/BUC:</span>
                                </td>
                                <td>
                                    <telerik:RadComboBox ID="RadComboBoxILNBBUC" runat="server" Skin="Metro" Width="100px">
                                        <Items>
                                            <telerik:RadComboBoxItem Text="" Value="0" />
                                            <telerik:RadComboBoxItem Text="500mW" Value="1" />
                                            <telerik:RadComboBoxItem Text="800mW" Value="2" />
                                            <telerik:RadComboBoxItem Text="2W" Value="3" />
                                            <telerik:RadComboBoxItem Text="3W" Value="4" />
                                            <telerik:RadComboBoxItem Text="4W" Value="5" />
                                        </Items>
                                    </telerik:RadComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="tablabel">Antenna:</span>
                                </td>
                                <td>
                                    <telerik:RadComboBox ID="RadComboBoxAntenna" runat="server" Skin="Metro" Width="100px">
                                        <Items>
                                            <telerik:RadComboBoxItem Text="" Value="0" />
                                            <telerik:RadComboBoxItem Text="1m" Value="1" />
                                            <telerik:RadComboBoxItem Text="1.2m" Value="2" />
                                            <telerik:RadComboBoxItem Text="1.8m" Value="3" />
                                            <telerik:RadComboBoxItem Text="2.4m" Value="4" />
                                            <telerik:RadComboBoxItem Text="Other" Value="5" />
                                        </Items>
                                    </telerik:RadComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="tablabel">Dialog Technology:</span>
                                </td>
                                <td>
                                    <telerik:RadComboBox ID="RadComboBoxDialogFrequency" runat="server" Skin="Metro">
                                        <Items>
                                            <telerik:RadComboBoxItem Text="" Value="0"/>
                                            <telerik:RadComboBoxItem Text="MxDMA" Value="1"/>
                                            <telerik:RadComboBoxItem Text="SCPC" Value="2"/>
                                        </Items>
                                    </telerik:RadComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="tablabel">Centre Frequency:</span>
                                </td>
                                <td>
                                    <telerik:RadNumericTextBox ID="RadNumericCenterFrequency" runat="server" Width="175px" NumberFormat-DecimalDigits="0"></telerik:RadNumericTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="tablabel">Symbol rate:</span>
                                </td>
                                <td>
                                    <telerik:RadNumericTextBox ID="RadNumericTextBoxSymbolRate" runat="server" Width="175px" NumberFormat-DecimalDigits="0"></telerik:RadNumericTextBox>
                                </td>
                            </tr>
                        </table>
                    </telerik:RadPageView>
                    <telerik:RadPageView ID="RadPageViewRemarks" runat="server" Width="100%">
                        <asp:TextBox ID="TextBoxRemarks" runat="server" BackColor="#FBFEFF" Height="398px"
                            Width="100%" TextMode="MultiLine"></asp:TextBox>
                    </telerik:RadPageView>
                    <telerik:RadPageView ID="RadPageActivityList" runat="server" Width="100%">
                        <ActivityList:TerminalActivityList ID="TermActivityList" runat="server"></ActivityList:TerminalActivityList>
                    </telerik:RadPageView>
                    <telerik:RadPageView ID="RadPageTools" runat="server" Width="100%">
                        <Tools:TerminalTools ID="TerminalTools" runat="server"></Tools:TerminalTools>
                    </telerik:RadPageView>
                </telerik:RadMultiPage>
            </div>
        </telerik:RadAjaxPanel>
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanelTerminal" runat="server" Skin="Metro">
        </telerik:RadAjaxLoadingPanel>
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server" Skin="Metro" EnableShadow="True"
            Left="0px" Right="0px" Modal="True" VisibleStatusbar="False" Animation="None">
            <Windows>
                <telerik:RadWindow ID="RadWindowModifyTerminal" runat="server" Animation="None" Opacity="100"
                    Skin="Metro" Title="Modify Terminal" AutoSize="False" Width="1000px" Height="700px"
                    KeepInScreenBounds="True" EnableShadow="True" EnableAriaSupport="True" VisibleStatusbar="True"
                    Modal="False" DestroyOnClose="False">
                </telerik:RadWindow>
            </Windows>
        </telerik:RadWindowManager>
    </form>
</body>
</html>
