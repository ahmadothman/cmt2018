﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using Telerik.Web.UI;
using FOWebApp.Data;

namespace FOWebApp.Nocsa
{
    public partial class AccountReport : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int actMonth = DateTime.Now.Month;
            int actYear = DateTime.Now.Year;
            int daysInMonth = DateTime.DaysInMonth(actYear, actMonth);
            RadDatePickerStart.SelectedDate = new DateTime(actYear, actMonth, 1);
            RadDatePickerEnd.SelectedDate = new DateTime(actYear, actMonth, daysInMonth);
        }

        protected void ImageButtonQuery_Click(object sender, EventArgs e)
        {
            DateTime startDate;
            DateTime endDate;
            if (!RadDatePickerStart.IsEmpty || !RadDatePickerEnd.IsEmpty)
            {
                startDate = (DateTime)RadDatePickerStart.SelectedDate;
                endDate = (DateTime)RadDatePickerEnd.SelectedDate;

                this.Session["accountReport_StartDate"] = startDate;
                this.Session["accountReport_EndDate"] = endDate;

                this.generateReport(startDate, endDate);
                RadGridAcccountingReport.Rebind();
                RadGridAcccountingReport.Visible = true;
            }
            else
            {
                RadGridAcccountingReport.Visible = false;
            }
        }

        /// <summary>
        /// Configures the export settings for this radgrid
        /// </summary>
        public void ConfigureExport()
        {
            RadGridAcccountingReport.ExportSettings.FileName = "CMTExport";
            RadGridAcccountingReport.ExportSettings.ExportOnlyData = true;
            RadGridAcccountingReport.ExportSettings.IgnorePaging = true;
            RadGridAcccountingReport.ExportSettings.OpenInNewWindow = true;
            RadGridAcccountingReport.ExportSettings.Excel.Format = GridExcelExportFormat.Html;
            RadGridAcccountingReport.ExportSettings.Csv.ColumnDelimiter = GridCsvDelimiter.Semicolon;
            RadGridAcccountingReport.ExportSettings.Csv.EncloseDataWithQuotes = false;
            RadGridAcccountingReport.ExportSettings.Csv.RowDelimiter = GridCsvDelimiter.NewLine;
        }

        protected void RadGridAcccountingReport_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToExcelCommandName)
            {
                this.ConfigureExport();
                RadGridAcccountingReport.MasterTableView.ExportToExcel();
            }

            if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToCsvCommandName)
            {
                RadGridAcccountingReport.MasterTableView.ExportToCSV();
            }

            if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToPdfCommandName)
            {
                RadGridAcccountingReport.MasterTableView.ExportToPdf();
            }

            if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToWordCommandName)
            {
                RadGridAcccountingReport.MasterTableView.ExportToWord();
            }
        }

        protected void RadGridAcccountingReport_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            DateTime startDate;
            DateTime endDate;

            if (!RadDatePickerStart.IsEmpty || !RadDatePickerEnd.IsEmpty)
            {
                startDate = (DateTime)RadDatePickerStart.SelectedDate;
                endDate = (DateTime)RadDatePickerEnd.SelectedDate;

                this.Session["accountReport_StartDate"] = startDate;
                this.Session["accountReport_EndDate"] = endDate;

                this.generateReport(startDate, endDate);

            }
            else
            {
                //Check if values were stored in session variables
                if (this.Session["accountReport_StartDate"] != null && this.Session["accountReport_EndDate"] != null)
                {
                    startDate = (DateTime)this.Session["accountReport_StartDate"];
                    endDate = (DateTime)this.Session["accountReport_EndDate"];

                    this.generateReport(startDate, endDate);
                 }
                else
                {
                    RadGridAcccountingReport.Visible = false;
                }
            }
        }

        private void generateReport(DateTime startDate, DateTime endDate)
        {
            // Include end date in the calculation to create the report
            endDate = endDate.AddDays(1);

            //Fill data grid
            DataHelper dg = new DataHelper();
            DataTable dt = dg.getAccountReport(startDate, endDate);
            if (dt != null)
            {
                RadGridAcccountingReport.DataSource = dt;
                RadGridAcccountingReport.Visible = true;
                
            }
            else
            {
                RadGridAcccountingReport.Visible = false;
            }
        }

        protected void ImageButtonCSVExport_Click(object sender, ImageClickEventArgs e)
        {
            DateTime startDate = (DateTime)RadDatePickerStart.SelectedDate;
            DateTime endDate = (DateTime)RadDatePickerEnd.SelectedDate;
            if (!RadDatePickerStart.IsEmpty || !RadDatePickerEnd.IsEmpty)
            {
                this.Session["accountReport_StartDate"] = startDate;
                this.Session["accountReport_EndDate"] = endDate;
                this.generateReport(startDate, endDate);
            }
            RadGridAcccountingReport.ExportSettings.FileName = "Accounting report " + startDate.ToString("dd/MM/yyyy") + " to " + endDate.ToString("dd/MM/yyyy");
            RadGridAcccountingReport.MasterTableView.ExportToCSV();
        }

        protected void ImageButtonPDFExport_Click(object sender, ImageClickEventArgs e)
        {
            DateTime startDate = (DateTime)RadDatePickerStart.SelectedDate;
            DateTime endDate = (DateTime)RadDatePickerEnd.SelectedDate;
            RadGridAcccountingReport.ExportSettings.Pdf.Title = "Terminal Activities From " + startDate.Date + " To " + endDate.Date;
            RadGridAcccountingReport.ExportSettings.Pdf.FontType = Telerik.Web.Apoc.Render.Pdf.FontType.Embed;
            RadGridAcccountingReport.ExportSettings.Pdf.DefaultFontFamily = "Arial";
            RadGridAcccountingReport.ExportSettings.Pdf.PaperSize = GridPaperSize.A4;
            RadGridAcccountingReport.MasterTableView.ExportToPdf();
        }

        protected void ImageButtonExcelExport_Click(object sender, ImageClickEventArgs e)
        {
            DateTime startDate = (DateTime)RadDatePickerStart.SelectedDate;
            DateTime endDate = (DateTime)RadDatePickerEnd.SelectedDate;
            if (!RadDatePickerStart.IsEmpty || !RadDatePickerEnd.IsEmpty)
            {
                this.Session["accountReport_StartDate"] = startDate;
                this.Session["accountReport_EndDate"] = endDate;
                this.generateReport(startDate, endDate);
            }
            RadGridAcccountingReport.ExportSettings.FileName = "Accounting report " + startDate.ToString("dd/MM/yyyy") + " to " + endDate.ToString("dd/MM/yyyy");
            RadGridAcccountingReport.ExportSettings.ExportOnlyData = true;
            RadGridAcccountingReport.ExportSettings.IgnorePaging = true;
            RadGridAcccountingReport.ExportSettings.OpenInNewWindow = true;
            RadGridAcccountingReport.Rebind();
            RadGridAcccountingReport.MasterTableView.ExportToExcel();
        }

        protected void RadGridAcccountingReport_ItemCreated(object sender, GridItemEventArgs e)
        {
        }
    }
}