﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ValidateVoucher.ascx.cs" Inherits="FOWebApp.Nocsa.ValidateVoucher" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script type="text/javascript">
function ValidateVouchersButtonClicked() {
        document.getElementById("<%= LabelResult.ClientID %>").style.display = "";
        radconfirm('Are you sure you want to continue?', validationCallBackFn, 400, 200, null, 'You are about to validate a voucher!');
    }

    function validationCallBackFn(arg) {
        if (arg == true) {
            //Get the distributor Id and number of vouchers from the controls
            var codeTextBox = $find("<%= RadMaskedTextBoxCode.ClientID%>");
            var code = codeTextBox.get_textBoxValue();

            var crcTextBox = $find("<%= RadMaskedTextBoxCrc.ClientID%>");
            var crc = crcTextBox.get_textBoxValue();

            //Call the webservice
            FOWebApp.WebServices.VoucherSupportWS.ValidateVoucher(code, crc, "00:00:00:00:00:00", OnValidateVoucherComplete);
        }
    }

    function OnValidateVoucherComplete(result) {
        if (result)
        {
            document.getElementById("<%= LabelResult.ClientID %>").style.Color = "green"; 
            document.getElementById("<%= LabelResult.ClientID %>").innerHTML = "Voucher validated successfully!";
        }
        else
        {
        document.getElementById("<%= LabelResult.ClientID %>").style.Color = "red"; 
            document.getElementById("<%= LabelResult.ClientID %>").innerHTML = "Voucher validation failed!<br/>(Already validated, wrong crc or non-existing)";
        }
    }
</script>
<h1>Validate Voucher</h1>
<table cellpadding="10" cellspacing="5">
    <tr>
        <td>
            <telerik:RadMaskedTextBox ID="RadMaskedTextBoxCode" runat="server" Columns="13" 
                Mask="#############" Rows="1" Skin="Metro" Font-Bold="True" 
                ForeColor="#0066FF" Width="100px"  EmptyMessage="Code">
            </telerik:RadMaskedTextBox> - 
            <telerik:RadMaskedTextBox ID="RadMaskedTextBoxCrc" Runat="server" 
                EmptyMessage="Crc" Font-Bold="True" ForeColor="#0066FF" Mask="aaaa" Rows="1" 
                Skin="Metro" Width="32px">
            </telerik:RadMaskedTextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                ErrorMessage="Please enter the 13 digit code!" 
                ControlToValidate="RadMaskedTextBoxCode" SetFocusOnError="True" 
                Display="None"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                ErrorMessage="Please enter the 4 character check digit!" 
                ControlToValidate="RadMaskedTextBoxCrc" SetFocusOnError="True" 
                Display="None"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            <telerik:RadButton ID="RadButtonValidate" runat="server" 
                Text="Validate Voucher" Skin="Metro" AutoPostBack="False" OnClientClicked="ValidateVouchersButtonClicked">
            </telerik:RadButton>
        </td>
    </tr>
    <tr>
        <td><asp:Label ID="LabelResult" runat="server"></asp:Label></td>
    </tr>
    <tr>
        <td>
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
        </td>
    </tr>
</table>
<telerik:RadWindowManager ID="RadWindowManager1" runat="server" Skin="Metro" EnableShadow="True"
    Left="0px" Right="0px" Modal="True" VisibleStatusbar="False">
</telerik:RadWindowManager>