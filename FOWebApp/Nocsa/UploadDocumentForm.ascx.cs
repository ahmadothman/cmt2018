﻿using System;
using System.Linq;
using System.Configuration;
using System.Drawing;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using Telerik.Web.UI;
using FOWebApp.net.nimera.cmt.BODocumentControllerWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;


namespace FOWebApp.Nocsa
{
    public partial class UploadDocumentForm : System.Web.UI.UserControl
    {
        BOAccountingControlWS _boAccountingControlWS = new BOAccountingControlWS();
        BODocumentControllerWS _boDocumentControllerWS = new BODocumentControllerWS();
        BOLogControlWS _boLogControlWS = new BOLogControlWS();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Fill-up the treeview in the RadComboBox

                RadTreeView userTree = (RadTreeView)RadComboBoxUsers.Items[0].FindControl("RadTreeViewUsers");
                //VNOs
                RadTreeNode rtn = new RadTreeNode("VNOs", "0");
                rtn.ImageUrl = "~/Images/magician.png";
                rtn.ToolTip = "VNOs";
                Distributor[] VNOs = _boAccountingControlWS.GetVNOs();
                RadTreeNode userNode = null;
                foreach (Distributor vno in VNOs)
                {
                    userNode = new RadTreeNode(vno.FullName, vno.Id.ToString());
                    userNode.ToolTip = vno.FullName;
                    rtn.Nodes.Add(userNode);
                }
                userTree.Nodes.Add(rtn);
                //Distributors
                rtn = new RadTreeNode("Distributors", "1");
                rtn.ImageUrl = "~/Images/businesspeople.png";
                rtn.ToolTip = "Distributors";
                Distributor[] distributors = _boAccountingControlWS.GetDistributors();
                userNode = null;
                foreach (Distributor dist in distributors)
                {
                    userNode = new RadTreeNode(dist.FullName, dist.Id.ToString());
                    userNode.ToolTip = dist.FullName;
                    rtn.Nodes.Add(userNode);
                }
                userTree.Nodes.Add(rtn);
                //Customers
                rtn = new RadTreeNode("Customers", "2");
                rtn.ImageUrl = "~/Images/businesspeople.png";
                rtn.ToolTip = "Customers";
                Organization[] customers = _boAccountingControlWS.GetOrganizations();
                userNode = null;
                foreach (Organization customer in customers)
                {
                    userNode = new RadTreeNode(customer.FullName, customer.Id.ToString());
                    userNode.ToolTip = customer.FullName;
                    rtn.Nodes.Add(userNode);
                }
                userTree.Nodes.Add(rtn);
                //End-users
                rtn = new RadTreeNode("End Users", "3");
                rtn.ImageUrl = "~/Images/customers.png";
                rtn.ToolTip = "End Users";
                EndUser[] endUsers = _boAccountingControlWS.GetEndUsers();
                userNode = null;
                foreach (EndUser endUser in endUsers)
                {
                    userNode = new RadTreeNode(endUser.FirstName + " " + endUser.LastName, endUser.Id.ToString());
                    userNode.ToolTip = endUser.FirstName + " " + endUser.LastName;
                    rtn.Nodes.Add(userNode);
                }
                userTree.Nodes.Add(rtn);

                //Initialize other controls
                LabelDateUploaded.Text = DateTime.UtcNow.ToString();
                RadAsyncUploadDoc.TargetFolder = ConfigurationManager.AppSettings["RadAsyncUploadDoc.TargetFolder"];
                RadAsyncUploadDoc.TemporaryFolder = ConfigurationManager.AppSettings["RadAsyncUploadDoc.TargetTempFolder"];

            }
            catch (Exception ex)
            {
                CmtApplicationException cmtApplicationException = new CmtApplicationException();
                cmtApplicationException.ExceptionDateTime = DateTime.Now;
                cmtApplicationException.ExceptionDesc = ex.Message;
                cmtApplicationException.ExceptionStacktrace = ex.StackTrace;
                cmtApplicationException.ExceptionLevel = (short)4;
                cmtApplicationException.UserDescription = ex.Message;
                cmtApplicationException.StateInformation = "Error";
                _boLogControlWS.LogApplicationException(cmtApplicationException);
            }
        }

        protected void RadButtonSubmit_Click(object sender, EventArgs e)
        {
            RadTextBoxDocDescription.Text = "";
            RadioButtonEveryone.Checked = true;
        }

        /// <summary>
        /// Apparently this method is called each time a file is uploaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadAsyncUploadDoc_FileUploaded(object sender, FileUploadedEventArgs e)
        {
            String fileName = e.File.FileName;
            Document doc = null;
            Boolean existingDocumentFlag = _boDocumentControllerWS.AlreadyExists(fileName);
            Boolean result = false;

            if (existingDocumentFlag)
            {
                doc = _boDocumentControllerWS.GetDocumentByName(fileName);
            }
            else
            {
                doc = new Document();
            }
            doc.DateUploaded = DateTime.Now;
            doc.Description = RadTextBoxDocDescription.Text;
            doc.Name = fileName;

            if (RadioButtonEveryone.Checked)
            {
                doc.Recipient = "Everyone";
                doc.RecipientFullName = "Everyone";
            }
            else if (RadioButtonVNOs.Checked)
            {
                doc.Recipient = "VNO";
                doc.RecipientFullName = "VNO";
            }
            else if (RadioButtonDistributors.Checked)
            {
                doc.Recipient = "Distributor";
                doc.RecipientFullName = "Distributor";
            }
            else if (RadioButtonMSO.Checked)
            {
                doc.Recipient = "MSO";
                doc.RecipientFullName = "Multicast Service Operators";
            }
            else if (RadioButtonCustomers.Checked)
            {
                doc.Recipient = "Organization";
                doc.RecipientFullName = "Organization";
            }
            else if (RadioButtonEndUsers.Checked)
            {
                doc.Recipient = "EndUser";
                doc.RecipientFullName = "End User";
            }
            else if (RadioButtonFinanceAdmin.Checked)
            {
                doc.Recipient = "FinanceAdmin";
                doc.RecipientFullName = "Financial Administrator";
            }
            else if (RadioButtonMSO.Checked)
            {
                doc.Recipient = "MSO";
                doc.Recipient = "Multicast Service Operator";
            }
            else if (RadioButtonSpecific.Checked)
            {
                doc.Recipient = RadComboBoxUsers.SelectedValue;
                doc.RecipientFullName = RadComboBoxUsers.Text;
            }
            else
            {
                doc.Recipient = "everyone";
                doc.RecipientFullName = "everyone";
            }

            if (existingDocumentFlag)
            {
                result = _boDocumentControllerWS.UpdateDocument(doc);
            }
            else
            {
                result = _boDocumentControllerWS.CreateNewDocument(doc) > 0;
            }

            if (!result)
            {
                LabelResult.ForeColor = Color.Red;
                LabelResult.Text = "Some of the document uploads failed";
            }
 
        }
    }
}