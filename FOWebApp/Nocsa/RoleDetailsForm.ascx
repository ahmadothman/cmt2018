﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RoleDetailsForm.ascx.cs" Inherits="FOWebApp.Nocsa.RoleDetailsForm" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<style type="text/css">
    .auto-style2 {
        width: 102%;
    }
</style>
<telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
    <script type="text/javascript">
        function DoBlur(fld) {
            fld.className = 'normalfld';
        }

        function DoFocus(fld) {
            fld.className = 'focusfld';
        }

        function DoChange(fld) {
            fld.className = 'changedfld';
        }

        function OnCheckBoxServiceSelected(sender, eventArgs) {
            var item = eventArgs.get_item();
            var RoleNameLabel = document.getElementById('<%=LabelRoleName.ClientID%>');
            if (item.get_checked() == true) {
                //Add service to role
                var result = FOWebApp.WebServices.AccountSupportWS.LinkServiceToRole(RoleNameLabel.innerHTML, item.get_value(), OnRequestComplete, OnError);
            }
            else {
                //Remove service from role
                var result = FOWebApp.WebServices.AccountSupportWS.RemoveServiceFromRole(RoleNameLabel.innerHTML, item.get_value(), OnRequestComplete, OnError);
            }
        }

        function OnRequestComplete(result) {
            if (result == false) {
                document.getElementById('<%=ListBoxError.ClientID%>').innerHTML = 'Operation failed on server';
            }
            else {
                document.getElementById('<%=ListBoxError.ClientID%>').innerHTML = '';
            }
        }

        function OnError(result) {
            alert(result.get_message());
        }
    </script>
</telerik:RadCodeBlock>
<telerik:RadAjaxPanel ID="RadAjaxPanelRole" runat="server" Height="200px" Width="232px"
    EnableHistory="True" Style="margin-right: 114px">
    <table width="800px" id="RoleTable" cellpadding="10" cellspacing="5">
        <tr>
            <td>
                <table id="RoleLabelTable" cellpadding="10" cellspacing="5">
                    <tr>
                        <td>Role:</td>
                        <td>
                            <asp:Label ID="LabelRoleName" runat="server" ></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>System users in role:</td>
                        <td>
                            <asp:Label ID="LabelUsersInRole" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="PanelServicesAllocation" runat="server" Width="410px" GroupingText="Allocate services to role" Visible="False">
                    <telerik:RadListBox ID="RadListBoxServices" runat="server" Height="300px" Skin="Metro" Visible="False" Width="405px" CheckBoxes="True" OnClientItemChecked="OnCheckBoxServiceSelected" SelectionMode="Multiple">
                    </telerik:RadListBox>
                </asp:Panel>
                <p>
                    <asp:Label ID="ListBoxError" runat="server" ForeColor="Red"></asp:Label>
                </p>
            </td>
        </tr>
        <tr>
            <td>
                <asp:button ID="ButtonDelete" runat="server" OnClick="ButtonDelete_Click" Text="Delete Role"/>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LabelDelete" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
</telerik:RadAjaxPanel>