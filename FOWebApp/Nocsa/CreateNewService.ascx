﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CreateNewService.ascx.cs" Inherits="FOWebApp.Nocsa.CreateNewService" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script language="javascript" type="text/javascript">
    function OnClientSelectedIndexChanged(sender, eventArgs) {
        var item = eventArgs.get_item();
        //sender.set_text("You selected ISP with Id: " + item.get_value());

        var panel = $find("<%=RadXmlHttpPanelParent2.ClientID %>");
        panel.set_value(item.get_value());
        return false;
    }

    function DisplayElement(element) {
        x = element.value;
        if (x == "1") {
            document.getElementById('top1').style.display = '';
            document.getElementById('top2').style.display = '';
            document.getElementById('sub1').style.display = 'none';
            document.getElementById('sub2').style.display = 'none';
            document.getElementById('sub3').style.display = 'none';
            document.getElementById('sub4').style.display = 'none';
            document.getElementById('selectLevel').style.display = 'none';
        }
        else if (x == "2") {
            document.getElementById('top1').style.display = 'none';
            document.getElementById('top2').style.display = 'none';
            document.getElementById('sub1').style.display = '';
            document.getElementById('sub2').style.display = '';
            document.getElementById('sub3').style.display = 'none';
            document.getElementById('sub4').style.display = 'none';
            document.getElementById('selectLevel').style.display = 'none';
        }
        else if (x == "3") {
            document.getElementById('top1').style.display = 'none';
            document.getElementById('top2').style.display = 'none';
            document.getElementById('sub1').style.display = '';
            document.getElementById('sub2').style.display = '';
            document.getElementById('sub3').style.display = '';
            document.getElementById('sub4').style.display = '';
            document.getElementById('selectLevel').style.display = 'none';
        }
        else {
            document.getElementById('top1').style.display = 'none';
            document.getElementById('top2').style.display = 'none';
            document.getElementById('sub1').style.display = 'none';
            document.getElementById('sub2').style.display = 'none';
            document.getElementById('sub3').style.display = 'none';
            document.getElementById('sub4').style.display = 'none';
            document.getElementById('selectLevel').style.display = '';
        }
    }    
</script>

<table>
    <tr>
        <td valign="top" width="600">
            <table cellpadding="10" cellspacing="5">
                <tr>
                    <td width="121">Name</td>
                    <td>
                        <asp:TextBox ID="TextBoxServiceName" runat="server" Width="400px"></asp:TextBox><br />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please specify a service name" ControlToValidate="TextBoxServiceName"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>Description</td>
                    <td>
                        <asp:TextBox ID="TextBoxDescription" runat="server" TextMode="MultiLine"
                                Width="400px" Rows="4" BackColor="#FFFFCC"></asp:TextBox><br />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Please specify a description" ControlToValidate="TextBoxDescription"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>Controller</td>
                    <td>
                        <asp:TextBox ID="TextBoxController" runat="server" Width="400px"></asp:TextBox><br />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please specify a service controller" ControlToValidate="TextBoxController"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>Level</td>
                    <td>
                        <asp:RadioButton runat="server" Checked="false" value="1" onclick="DisplayElement(this)" ID="RadioButtonLevel1" Text="1" GroupName="LevelsGroup" />&nbsp;<asp:RadioButton runat="server" Checked="false" value="2" onclick="DisplayElement(this)" ID="RadioButtonLevel2" Text="2" GroupName="LevelsGroup" />&nbsp;<asp:RadioButton runat="server" Checked="false" onclick="DisplayElement(this)" value="3" ID="RadioButtonLevel3" Text="3" GroupName="LevelsGroup" /><br />
                        <div id="selectLevel"><a style="color:red">Please select a level</a></div>
                    </td>
                </tr>
                <tr>
                    <td><div id="top1" style="display:none">Icon</div></td>
                    <td>
                        <div id="top2" style="display:none">
                            <asp:FileUpload runat="server" ID="FileUpload1" />
                        </div>
                        <asp:Label ID="LabelUploadStatus" runat="server"></asp:Label>
                    </td>              
                </tr>
                <tr>
                    <td><div id="sub1" style="display:none">Parent (level 1)</div></td>
                    <td>
                        <div id="sub2" style="display:none">
                            <telerik:RadComboBox ID="RadComboBoxParent1" runat="server" Skin="Metro" OnClientSelectedIndexChanged="OnClientSelectedIndexChanged" Width="400px">
                            </telerik:RadComboBox>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td><div id="sub3" style="display:none">Parent (level 2)</div></td>
                    <td>
                        <div id="sub4" style="display:none">
                            <telerik:RadXmlHttpPanel ID="RadXmlHttpPanelParent2" runat="server" EnableClientScriptEvaluation="True"
                                OnServiceRequest="RadXmlHttpPanelParent2_ServiceRequest">
                                <telerik:RadComboBox ID="RadComboBoxParent2" runat="server" Skin="Metro" Width="400px">
                                </telerik:RadComboBox>
                            </telerik:RadXmlHttpPanel>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td><asp:Button ID="ButtonSubmit" runat="server" Text="Submit" OnClick="ButtonSubmit_Click" /></td>
                    <td><asp:Label ID="LabelSubmit" runat="server"></asp:Label></td>
                </tr>
            </table>
        </td>
        <td>
            <telerik:RadGrid ID="RadGridServiceControllers" runat="server" Width="300px" AllowPaging="False" 
    AllowSorting="False" AutoGenerateColumns="False" CellSpacing="0"
    GridLines="None" Skin="Metro">
    <ClientSettings Selecting-AllowRowSelect="True">
        <Selecting EnableDragToSelectRows="False" />
        <ClientEvents OnRowSelected="RowSelected" />
    </ClientSettings>
    <MasterTableView PageSize="20" RowIndicatorColumn-Visible="True" CommandItemDisplay="Top" CommandItemSettings-ShowAddNewRecordButton="False">
        <CommandItemSettings ExportToPdfText="Export to PDF" ShowRefreshButton="false" />
        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
            <HeaderStyle Width="20px" />
        </RowIndicatorColumn>
        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
            <HeaderStyle Width="20px" />
        </ExpandCollapseColumn>
        <Columns>
            <telerik:GridBoundColumn DataField="Item" FilterControlAltText="Filter NameColumn column" HeaderText="Service controller" 
                ReadOnly="True" UniqueName="ItemColumn">
            </telerik:GridBoundColumn>
        </Columns>
        <EditFormSettings>
            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
            </EditColumn>
        </EditFormSettings>
    </MasterTableView>

    <SelectedItemStyle BackColor="#99CCFF" />

<FilterMenu EnableImageSprites="False"></FilterMenu>

<HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default"></HeaderContextMenu>
</telerik:RadGrid>
        </td>
    </tr>
</table>

