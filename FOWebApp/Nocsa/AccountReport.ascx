﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AccountReport.ascx.cs" Inherits="FOWebApp.Nocsa.AccountReport" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="RadGridAcccountingReport">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGridAcccountingReport" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="ImageButtonQuery">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGridAcccountingReport" />
                <telerik:AjaxUpdatedControl ControlID="ImageButtonQuery" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="ImageButtonExcelExport">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGridAcccountingReport" />
                <telerik:AjaxUpdatedControl ControlID="ImageButtonExcelExport" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="ImageButtonCSVExport">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGridAcccountingReport" />
                <telerik:AjaxUpdatedControl ControlID="ImageButtonCSVExport" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<table>
    <tr>
        <td>Start date:</td>
        <td>
            <telerik:RadDatePicker ID="RadDatePickerStart" runat="server" Skin="Metro">
                <DateInput DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy" runat="server" />
            </telerik:RadDatePicker>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Provide a start date" ControlToValidate="RadDatePickerStart" Display="Dynamic"></asp:RequiredFieldValidator>
        </td>
        <td>End date:</td>
        <td>
            <telerik:RadDatePicker ID="RadDatePickerEnd" runat="server" Skin="Metro">
                <DateInput DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy" runat="server" />
            </telerik:RadDatePicker>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Provide an end date" ControlToValidate="RadDatePickerEnd"></asp:RequiredFieldValidator>
        </td>
        <td>
            <%--<asp:ImageButton ID="ImageButtonQuery" runat="server"
                ImageUrl="~/Images/invoice_euro.png" OnClick="ImageButtonQuery_Click"
                ToolTip="Create accounting report" />--%>
            <asp:Button ID="accountReportButton" runat="server" 
                Text="Show" onclick="ImageButtonQuery_Click" 
                ToolTip="Create accounting report" />
        </td>
        <td>
            <asp:ImageButton ID="ImageButtonCSVExport" runat="server"
                ImageUrl="~/Images/CSV.png" OnClick="ImageButtonCSVExport_Click"
                ToolTip="Export as a CSV file" />
        </td>
        <td>
            <asp:ImageButton ID="ImageButtonExcelExport" runat="server"
                ImageUrl="~/Images/Excel.png" OnClick="ImageButtonExcelExport_Click"
                ToolTip="Export as an Excel file" />
        </td>
    </tr>
</table>
<telerik:RadGrid ID="RadGridAcccountingReport" runat="server"
    CellSpacing="0" GridLines="None"
    OnItemCommand="RadGridAcccountingReport_ItemCommand"
    OnNeedDataSource="RadGridAcccountingReport_NeedDataSource"
    Skin="Metro" OnItemCreated="RadGridAcccountingReport_ItemCreated" PageSize="10" AllowPaging="false" AllowSorting="true">
    <ExportSettings IgnorePaging="True" OpenInNewWindow="True" ExportOnlyData="True">
        <%--<Pdf Author="CMT" Creator="CMT" Title="CMT Billable Termnal Activities"
            DefaultFontFamily="Arial Unicode MS" FontType="Embed" PageBottomMargin="20mm"
            PageFooterMargin="20mm" PageHeight="297mm" PageLeftMargin="20mm"
            PageRightMargin="20mm" PageTitle="Billable Terminal Activities"
            PageWidth="210mm" PaperSize="A4" Producer="SatADSL"
            Subject="Billable Activity Overview" AllowPrinting="False" />--%>
        <Excel Format="Html" />
        <Csv ColumnDelimiter="Semicolon" EncloseDataWithQuotes="False" />
    </ExportSettings>
    <ClientSettings AllowColumnsReorder="True">
    </ClientSettings>
    <MasterTableView CommandItemDisplay="None" UseAllDataFields="true" AutoGenerateColumns="true">
       <CommandItemSettings ExportToPdfText="Export to PDF"
            ShowAddNewRecordButton="False" ShowExportToCsvButton="false"
            ShowExportToExcelButton="false" ShowExportToPdfButton="False"
            ShowExportToWordButton="False" ShowRefreshButton="False" />
        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
            <HeaderStyle Width="20px" />
        </RowIndicatorColumn>
        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
            <HeaderStyle Width="20px" />
        </ExpandCollapseColumn>
        <EditFormSettings>
            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
            </EditColumn>
        </EditFormSettings>
    </MasterTableView>
    <FilterMenu EnableImageSprites="False">
    </FilterMenu>
    <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default">
    </HeaderContextMenu>
    <PagerStyle Mode="NumericPages" />
</telerik:RadGrid>
