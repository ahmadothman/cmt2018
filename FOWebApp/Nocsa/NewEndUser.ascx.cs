﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;

namespace FOWebApp.Nocsa
{
    public partial class NewEndUser : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ButtonSubmit_Click(object sender, EventArgs e)
        {
            //Create a new End User
            EndUser endUser = new EndUser();
            endUser.FirstName = TextBoxFirstName.Text;
            endUser.LastName = TextBoxLastName.Text;
            endUser.Email = TextBoxEMail.Text;
            endUser.MobilePhone = TextBoxMobilePhone.Text;
            endUser.Phone = TextBoxPhone.Text;
            endUser.SitId = Int32.Parse(TextBoxSitId.Text);
            
            BOAccountingControlWS boAccountingControlWS = new BOAccountingControlWS();

            try
            {
                if (!boAccountingControlWS.UpdateEndUser(endUser))
                {
                    LabelResult.ForeColor = Color.Red;
                    LabelResult.Text = "Create of new End User failed!";
                }
                else
                {
                    LabelResult.ForeColor = Color.DarkGreen;
                    LabelResult.Text = "Create of new End User succeeded!";
                    ButtonSubmit.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                BOLogControlWS boLogControlWS = new BOLogControlWS();
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDesc = ex.Message;
                cmtEx.ExceptionStacktrace = ex.StackTrace;
                boLogControlWS.LogApplicationException(cmtEx);
                LabelResult.ForeColor = Color.Red;
                LabelResult.Text = "Sit Id does not exist!";
            }
        }
    }
}