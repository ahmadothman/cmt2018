﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CreateUser.ascx.cs" Inherits="FOWebApp.Nocsa.CreateUser" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<style type="text/css">
    .style1
    {
        width: 454px;
    }
</style>
<h1>Create a new user</h1>
<asp:Panel ID="PanelUserData" runat="server" BorderStyle="Groove" 
    BorderWidth="1px" BorderColor="#666666" ToolTip="User Data" Wrap="False" 
    CssClass="datapanel">
<table cellpadding="10" cellspacing="5">
    <tr>
        <td>
            User Name:
        </td>
        <td class="style1">
            <asp:TextBox ID="TextBoxUserName" runat="server" Width="301px"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorUserName" runat="server" ErrorMessage="Missing User Name" ControlToValidate="TextBoxUserName"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Password:
        </td>
        <td class="style1">
            <asp:TextBox ID="TextBoxPassword" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorPassword" runat="server" ErrorMessage="Missing Password" ControlToValidate="TextBoxPassword"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            E-Mail Address:
        </td>
        <td class="style1">
            <asp:TextBox ID="TextBoxEMailAddress" runat="server" Width="297px"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <asp:CheckBox ID="CheckBoxActivated" runat="server" Text="Activated:" 
                TextAlign="Left" />
        </td>
    </tr>
</table>
</asp:Panel>
<br />
<asp:Panel ID="PanelRoles" runat="server" BorderColor="#666666" 
    BorderStyle="Groove" BorderWidth="1px" ToolTip="Role" CssClass="datapanel">
<table cellpadding="10" cellspacing="5">
    <tr>
        <td colspan="2">
            <asp:RadioButton ID="RadioButtonNOCSA" runat="server" 
                Text="NOC Administrator" GroupName="RolesGroup" /> 
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <asp:RadioButton ID="RadioButtonNOCOP" runat="server" Text="NOC Operator" 
                GroupName="RolesGroup"/> 
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <asp:RadioButton ID="RadioButtonFinanceAdmin" runat="server" Text="Finance Administrator" 
                GroupName="RolesGroup"/> 
        </td>
    </tr>
    <%--<tr>
        <td>
            <asp:RadioButton ID="RadioButtonVNO" runat="server" Text="VNO" 
                GroupName="RolesGroup" />
        </td>
        <td>
            <telerik:RadComboBox ID="RadComboBoxVNO" runat="server" Enabled="True">
            </telerik:RadComboBox>
        </td>
    </tr>--%>
    <tr>
        <td>
            <asp:RadioButton ID="RadioButtonDistributor" runat="server" Text="Distributor" 
                GroupName="RolesGroup" />
        </td>
        <td>
            <telerik:RadComboBox ID="RadComboBoxDistributor" runat="server" Enabled="True" Skin="Metro">
            </telerik:RadComboBox>
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton ID="RadioButtonDistributorReadOnly" runat="server" Text="Distributor Read-Only" 
                GroupName="RolesGroup" />
        </td>
        <td>
            <telerik:RadComboBox ID="RadComboBoxDistributorReadOnly" runat="server" Enabled="True" Skin="Metro">
            </telerik:RadComboBox>
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton ID="RadioButtonOrganization" runat="server" 
                Text="Customer" GroupName="RolesGroup"/>
        </td>
        <td>
            <telerik:RadComboBox ID="RadComboBoxOrganization" runat="server" Enabled="True" Skin="Metro">
            </telerik:RadComboBox> 
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton ID="RadioButtonEndUser" runat="server" Text="EndUser" 
                GroupName="RolesGroup" />
        </td>
        <td>
            <telerik:RadComboBox ID="RadComboBoxEndUsers" runat="server" Enabled="True" Skin="Metro">
            </telerik:RadComboBox>
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton ID="RadioButtonMSODistributor" runat="server" Text="Multicast Service Operator" 
                GroupName="RolesGroup" />
        </td>
        <td>
            <telerik:RadComboBox ID="RadComboBoxDistributorMSO" runat="server" Enabled="True" Skin="Metro">
            </telerik:RadComboBox>
        </td>
    </tr>
</table>
</asp:Panel>
<br />
<p>
<asp:Button ID="ButtonCreate" runat="server" Text="Create User" 
    onclick="ButtonCreate_Click" />
</p>
<p>
    <asp:Label ID="LabelStatus" runat="server"></asp:Label>
</p>


