﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NewVoucherVolume.ascx.cs" Inherits="FOWebApp.Nocsa.NewVoucherVolume" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
    <script type="text/javascript">
        function OnClientSelectedIndexChanged(sender, eventArgs) {
        var item = eventArgs.get_item();
        //sender.set_text("You selected ISP with Id: " + item.get_value());

        var panel = $find("<%=RadXmlHttpPanelSLA.ClientID %>");
        panel.set_value(item.get_value());
        return false;
    }
        function DoBlur(fld) {
            fld.className = 'normalfld';
        }

        function DoFocus(fld) {
            fld.className = 'focusfld';
        }

        function DoChange(fld) {
            fld.className = 'changedfld';
        }

        function nodeClicking(sender, args) {
        var comboBox = $find("<%= RadComboBoxSLA.ClientID %>");
        var node = args.get_node()
        if (node.get_level() == 1) {
            comboBox.set_text(node.get_text());
            comboBox.set_value(node.get_value());
            comboBox.trackChanges();
            comboBox.get_items().getItem(0).set_text(node.get_text());
            comboBox.commitChanges();
            comboBox.hideDropDown();
        }
    }
    </script>
    
</telerik:RadCodeBlock>
<telerik:RadAjaxPanel ID="RadAjaxPanelVoucherVolume" runat="server" GroupingText="Voucher volume" Height="200px" Width="832px"
    EnableHistory="True" Style="margin-right: 114px">
<table cellpadding="10" cellspacing="5" class="VoucherVolume">
    <tr>
        <td>
            Volume ID:
        </td>
        <td>
            <telerik:RadNumericTextBox ID="RadNumericTextBoxId" runat="server" NumberFormat-DecimalDigits="0" MinValue="0" Width="200px"></telerik:RadNumericTextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please specify a volume ID" ControlToValidate="RadNumericTextBoxId"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Description:
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxDescription" runat="server" Width="200px"></telerik:RadTextBox>
        </td>
    </tr>
    <tr>
        <td>
            Volume (MB):
        </td>
        <td>
            <telerik:RadNumericTextBox ID="RadNumericTextBoxVolumeMB" runat="server" ShowSpinButtons="true" NumberFormat-DecimalDigits="0" Width="100px" MinValue="0" Value="0"></telerik:RadNumericTextBox>
        </td>
    </tr>
    <tr>
        <td>
            Validity period (days):
        </td>
        <td>
            <telerik:RadNumericTextBox ID="RadNumericTextBoxValidityPeriod" runat="server" ShowSpinButtons="true" NumberFormat-DecimalDigits="0" Width="100px" MinValue="0" Value="30"></telerik:RadNumericTextBox>
        </td>
    </tr>
    <tr>
        <td>
            Unit price (EUR):
        </td>
        <td>
            <telerik:RadNumericTextBox ID="RadNumericTextBoxUnitPriceEUR" runat="server" ShowSpinButtons="true" NumberFormat-DecimalDigits="2" Width="100px" MinValue="0" Value="0"></telerik:RadNumericTextBox>
        </td>
    </tr>
    <tr>
        <td>
            Unit price (USD):
        </td>
        <td>
            <telerik:RadNumericTextBox ID="RadNumericTextBoxUnitPriceUSD" runat="server" ShowSpinButtons="true" NumberFormat-DecimalDigits="2" Width="100px" MinValue="0" Value="0"></telerik:RadNumericTextBox>
        </td>
    </tr>
    <tr>
        <td>
            ISP:
        </td>
        <td>
            <telerik:RadComboBox ID="RadComboBoxISP" runat="server" Skin="Metro" OnClientSelectedIndexChanged="OnClientSelectedIndexChanged"
                Width="350px">
            </telerik:RadComboBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorIsp" runat="server" ErrorMessage="Please select an ISP"
                ControlToValidate="RadComboBoxISP" />
        </td>
    </tr>
    <tr>
        <td>Service Package :</td>
        <td>
            <telerik:RadXmlHttpPanel ID="RadXmlHttpPanelSLA" runat="server" EnableClientScriptEvaluation="True"
                OnServiceRequest="RadXmlHttpPanelSLA_ServiceRequest">
               <telerik:RadComboBox ID="RadComboBoxSLA" runat="server" Skin="Metro" ExpandDirection="Up"
                    Height="200px" Width="430px">
             </telerik:RadComboBox>
            </telerik:RadXmlHttpPanel>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorSla" runat="server" ErrorMessage="Please select an SLA"
                ControlToValidate="RadComboBoxSLA" Display="Dynamic" />
            <asp:CompareValidator ID="CompareValidatorSla" runat="server" ErrorMessage="Please select an ISP that has SLAs defined" 
                ValueToCompare="No SLAs defined for this ISP or SLAs are hidden" Operator="NotEqual" ControlToValidate="RadComboBoxSLA"  />
        </td>
    </tr>
    <tr>
        <td>
            <asp:button ID="ButtonSubmit" text="Submit" runat="server" OnClick="ButtonSubmit_Click" />
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <asp:Label ID="LabelSubmit" runat="server"></asp:Label>
        </td>
    </tr>
</table>
</telerik:RadAjaxPanel>
