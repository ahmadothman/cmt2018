﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MandacTransactionsReport.ascx.cs" Inherits="FOWebApp.Nocsa.MandacTransactionsReport" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<telerik:RadGrid ID="RadGridMandac" runat="server" AllowSorting="True"
    AutoGenerateColumns="False" Visible="true" CellSpacing="0" GridLines="None" Skin="Metro"
    ShowStatusBar="True" Width="992px" AllowFilteringByColumn="False" >
        <GroupingSettings CaseSensitive="false" /> 
    <ClientSettings>
        <Selecting AllowRowSelect="True" />
        <ClientEvents OnRowSelected="RowSelected" />
    </ClientSettings>
    <MasterTableView ShowFooter="False" ShowGroupFooter="False">
    <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
        </RowIndicatorColumn>
        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
        </ExpandCollapseColumn>
        <Columns>
            <telerik:GridBoundColumn DataField="TransactionId" 
                FilterControlAltText="Filter TransactionIdColumn column" HeaderText="Transaction ID" 
                UniqueName="Code" ShowFilterIcon="False" AutoPostBackOnFilter="True" Visible="false">
                <ItemStyle Wrap="False" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="TransactionDate" 
                FilterControlAltText="Filter TransactionDateColumn column" HeaderText="Transaction date" 
                UniqueName="Code" ShowFilterIcon="False" AutoPostBackOnFilter="True" DataFormatString="{0:dd/MM/yyyy}" DataType="System.DateTime">
                <ItemStyle Wrap="False" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="VolumePurchasedGB" 
                FilterControlAltText="Filter VolumePurchasedGBColumn column" HeaderText="Volume purchased (GB)" 
                UniqueName="Code" ShowFilterIcon="False" AutoPostBackOnFilter="True" DataFormatString="{0:0.00}">
                <ItemStyle Wrap="False" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="FeePaid" 
                FilterControlAltText="Filter FeePaidColumn column" HeaderText="Fee paid" 
                UniqueName="Code" ShowFilterIcon="False" AutoPostBackOnFilter="True" DataFormatString="{0:0.00}">
                <ItemStyle Wrap="False" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="CurrencyCode" 
                FilterControlAltText="Filter CurrencyCodeColumn column" HeaderText="Currency" 
                UniqueName="Code" ShowFilterIcon="False" AutoPostBackOnFilter="True">
                <ItemStyle Wrap="False" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="PaymentMethod" 
                FilterControlAltText="Filter PaymentMethodColumn column" HeaderText="Payment method" 
                UniqueName="Code" ShowFilterIcon="False" AutoPostBackOnFilter="True">
                <ItemStyle Wrap="False" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="VolumeUsed" 
                FilterControlAltText="Filter VolumeUsedGBColumn column" HeaderText="Volume used (GB)" 
                UniqueName="Code" ShowFilterIcon="False" AutoPostBackOnFilter="True" DataFormatString="{0:0.00}">
                <ItemStyle Wrap="False" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="ExpiryDate" 
                FilterControlAltText="Filter ExpiryDateColumn column" HeaderText="Expiry date" 
                UniqueName="Code" ShowFilterIcon="False" AutoPostBackOnFilter="True" DataFormatString="{0:dd/MM/yyyy}" DataType="System.DateTime">
                <ItemStyle Wrap="False" />
            </telerik:GridBoundColumn>            <telerik:GridBoundColumn DataField="HotSpotId" 
                FilterControlAltText="Filter HotSpotIdColumn column" HeaderText="Hotspot ID" 
                UniqueName="Code" ShowFilterIcon="False" AutoPostBackOnFilter="True">
                <ItemStyle Wrap="False" />
            </telerik:GridBoundColumn>
        </Columns>
        <EditFormSettings>
<EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
</EditFormSettings>
</MasterTableView>
    <FilterMenu EnableImageSprites="False">
    </FilterMenu>
    <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default">
    </HeaderContextMenu>
</telerik:RadGrid>