﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ConnectedDevicesList.ascx.cs" Inherits="FOWebApp.Nocsa.ConnectedDevicesList" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script type="text/javascript">
    //function RowSelected(sender, eventArgs) {
    //    var grid = sender;
    //    var masterTable = grid.get_masterTableView();
    //    var dataItems = masterTable.get_selectedItems();
    //    if (dataItems.length != 0) {
    //        var id = dataItems[0].get_element().cells[0].innerHTML;
    //        var oWnd = radopen("Nocsa/ConnectedDeviceDetailsPage.aspx?id=" + id, "RadWindowDeviceDetails");
    //    }
    //}

    function RowSelected(sender, eventArgs) {
        var grid = sender;
        var detailTables = grid.get_detailTables();
        if (detailTables.length == 1) {
            var dataItems = detailTables[0].get_selectedItems();
            if (dataItems.length != 0) {
                var id = dataItems[0].get_element().cells[0].innerHTML;
                var oWnd = radopen("Nocsa/ConnectedDeviceDetailsPage.aspx?id=" + id, "RadWindowDeviceDetails");
            }
        }
    }
</script>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="RadGridMulticastGroups">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGridMulticastGroups" />
                <telerik:AjaxUpdatedControl ControlID="RadWindowManager1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Metro"/>
<telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" Height="200px" 
    Width="467px" HorizontalAlign="NotSet" 
    LoadingPanelID="RadAjaxLoadingPanel1">
<telerik:RadGrid ID="RadGridMulticastGroups" runat="server" AllowSorting="True"
    AutoGenerateColumns="False" CellSpacing="0" GridLines="None" Skin="Metro"
    ShowStatusBar="True" Width="992px" AllowFilteringByColumn="False"
        OnNeedDataSource="RadGridMulticastGroups_NeedDataSource"
        onitemdatabound="RadGridMulticastGroups_ItemDataBound" 
        onpagesizechanged="RadGridMulticastGroups_PageSizeChanged" 
        PageSize="50" onitemcommand="RadGridMulticastGroups_ItemCommand"
        OnDetailTableDataBind="RadGridMulticastGroups_DetailTableDataBind">
        <GroupingSettings CaseSensitive="false" /> 
    <ClientSettings>
        <Selecting AllowRowSelect="True" />
        <ClientEvents OnRowSelected="RowSelected" />
    </ClientSettings>
    <MasterTableView CommandItemDisplay="Top" CommandItemSettings-ShowExportToExcelButton="False" 
        CommandItemSettings-ShowExportToCsvButton="False" RowIndicatorColumn-Visible="True" 
        RowIndicatorColumn-Resizable="True" RowIndicatorColumn-AndCurrentFilterFunction="Contains" 
        CommandItemSettings-ShowAddNewRecordButton="False" ShowFooter="False">
        <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
        </RowIndicatorColumn>
        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
        </ExpandCollapseColumn>
        <DetailTables>
            <telerik:GridTableView runat="server" Name="ConnectedDevicesTableView" AllowCustomPaging="false"
                AllowSorting="true">
                <Columns>
                    <telerik:GridBoundColumn DataField="DeviceId"
                        FilterControlAltText="Filter IdColumn column" HeaderText="ID" 
                        UniqueName="Id" ShowFilterIcon="False" AutoPostBackOnFilter="True">
                        <ItemStyle Wrap="True" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="DeviceName"
                        FilterControlAltText="Filter IdColumn column" HeaderText="Name" 
                        UniqueName="Name" ShowFilterIcon="False" AutoPostBackOnFilter="True">
                        <ItemStyle Wrap="True" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="DeviceType"
                        FilterControlAltText="Filter IdColumn column" HeaderText="Type" 
                        UniqueName="Type" ShowFilterIcon="False" AutoPostBackOnFilter="True">
                        <ItemStyle Wrap="True" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="macAddress"
                        FilterControlAltText="Filter IdColumn column" HeaderText="Terminal MAC address" 
                        UniqueName="macAddress" ShowFilterIcon="False" AutoPostBackOnFilter="True">
                        <ItemStyle Wrap="True" />
                    </telerik:GridBoundColumn>
                </Columns>
            </telerik:GridTableView>
        </DetailTables>
        <Columns>
            <telerik:GridBoundColumn DataField="Id" FilterControlAltText="Filter IdColumn column"
                HeaderText="Id" UniqueName="Id" HeaderStyle-Width="20px">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Name" FilterControlAltText="Filter NameColumn column"
                HeaderText="Multicast Group Name" UniqueName="Name">
                <ItemStyle Width="200px" Wrap="False" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="FullName" FilterControlAltText="Filter FullNameColumn column"
                HeaderText="Distributor Name" UniqueName="FullName">
                <ItemStyle Width="200px" Wrap="False" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="DistId" FilterControlAltText="Filter DistIdColumn column"
                HeaderText="Distributor Id" Visible="true" UniqueName="DistId" HeaderStyle-Width="50px">
                <ItemStyle Width="200px" Wrap="False" />
            </telerik:GridBoundColumn>
        </Columns>
        <EditFormSettings>
            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
            </EditColumn>
        </EditFormSettings>
    </MasterTableView>
    <FilterMenu EnableImageSprites="False">
    </FilterMenu>
    <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default">
    </HeaderContextMenu>
</telerik:RadGrid>
<telerik:RadWindowManager ID="RadWindowManager1" runat="server" Skin="Metro" 
        DestroyOnClose="True">
    <Windows>
        <telerik:RadWindow ID="RadWindowDeviceDetails" runat="server" 
            NavigateUrl="NOCSA/ConnectedDeviceDetailsPage.aspx" Animation="Fade" Opacity="100" Skin="Metro" 
            Title="Connected Device Details" AutoSize="False" Width="1000px" Height="700px" KeepInScreenBounds="True" EnableShadow="True" EnableAriaSupport="True" VisibleStatusbar="True" Modal="False" DestroyOnClose="False">
        </telerik:RadWindow>
    </Windows>
</telerik:RadWindowManager>
</telerik:RadAjaxPanel>