﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Drawing;
using FOWebApp.net.nimera.cmt.BOCMTMessageControllerWSRef;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using System.Web.Security;

namespace FOWebApp.Nocsa
{
    public partial class CreateCMTMessage : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BOAccountingControlWS _boAccountingControlWS = new BOAccountingControlWS();
            
            //Fill-up the treeview in the RadComboBox

            RadTreeView userTree = (RadTreeView)RadComboBoxUsers.Items[0].FindControl("RadTreeViewUsers");
            //VNOs
            RadTreeNode rtn = new RadTreeNode("VNOs", "0");
            rtn.ImageUrl = "~/Images/magician.png";
            rtn.ToolTip = "VNOs";
            Distributor[] VNOs = _boAccountingControlWS.GetVNOs();
            RadTreeNode userNode = null;
            foreach (Distributor vno in VNOs)
            {
                userNode = new RadTreeNode(vno.FullName, vno.Id.ToString());
                userNode.ToolTip = vno.FullName;
                rtn.Nodes.Add(userNode);
            }
            userTree.Nodes.Add(rtn);
            //Distributors
            rtn = new RadTreeNode("Distributors", "1");
            rtn.ImageUrl = "~/Images/businesspeople.png";
            rtn.ToolTip = "Distributors";
            Distributor[] distributors = _boAccountingControlWS.GetDistributors();
            userNode = null;
            foreach (Distributor dist in distributors)
            {
                userNode = new RadTreeNode(dist.FullName, dist.Id.ToString());
                userNode.ToolTip = dist.FullName;
                rtn.Nodes.Add(userNode);
            }
            userTree.Nodes.Add(rtn);
            //Customers
            rtn = new RadTreeNode("Customers", "2");
            rtn.ImageUrl = "~/Images/businesspeople.png";
            rtn.ToolTip = "Customers";
            Organization[] customers = _boAccountingControlWS.GetOrganizations();
            userNode = null;
            foreach (Organization customer in customers)
            {
                userNode = new RadTreeNode(customer.FullName, customer.Id.ToString());
                userNode.ToolTip = customer.FullName;
                rtn.Nodes.Add(userNode);
            }
            userTree.Nodes.Add(rtn);
            //End-users
            rtn = new RadTreeNode("End Users", "3");
            rtn.ImageUrl = "~/Images/customers.png";
            rtn.ToolTip = "End Users";
            EndUser[] endUsers = _boAccountingControlWS.GetEndUsers();
            userNode = null;
            foreach (EndUser endUser in endUsers)
            {
                userNode = new RadTreeNode(endUser.FirstName + " " + endUser.LastName, endUser.Id.ToString());
                userNode.ToolTip = endUser.FirstName + " " + endUser.LastName;
                rtn.Nodes.Add(userNode);
            }
            userTree.Nodes.Add(rtn);
            
        }

        protected void ButtonSubmit_Click(object sender, EventArgs e)
        {
            CMTMessage message = new CMTMessage();
            
            if (RadioButtonEveryone.Checked)
            {
                message.Recipient = "everyone";
                message.RecipientFullName = "everyone";
            }
            else if (RadioButtonVNOs.Checked)
            {
                message.Recipient = "VNOs";
                message.RecipientFullName = "VNOs";
            }
            else if (RadioButtonDistributors.Checked)
            {
                message.Recipient = "distributors";
                message.RecipientFullName = "distributors";
            }
            else if (RadioButtonSuspendedDistributors.Checked)
            {
                message.Recipient = "suspendedDistributors";
                message.RecipientFullName = "suspendedDistributors";
            }
            else if (RadioButtonCustomers.Checked)
            {
                message.Recipient = "customers";
                message.RecipientFullName = "customers";
            }
            else if (RadioButtonEndUsers.Checked)
            {
                message.Recipient = "endusers";
                message.RecipientFullName = "end users";
            }
            else if (RadioButtonSpecific.Checked)
            {
                message.Recipient = RadComboBoxUsers.SelectedValue;
                message.RecipientFullName = RadComboBoxUsers.Text;
            }
            else
            {
                LabelSelectRecipient.Text = "Please select the recipient(s)";
                LabelSelectRecipient.ForeColor = Color.Red;
                return;
            }
            message.Release = CheckBoxRelease.Checked;
            message.EndTime = (DateTime)RadDateTimePickerEndTime.SelectedDate;
            message.StartTime = (DateTime)RadDateTimePickerStartTime.SelectedDate;
            message.MessageTitle = TextBoxTitle.Text;
            message.MessageContent = RadEditorMessage.Content;
            if (message.MessageContent.Length == 0)
            {
                LabelResult.Text = "Message has no content";
                LabelResult.ForeColor = Color.Red;
                return;
            }

            // add the GUID of the message
            message.MessageId = Guid.NewGuid();

            //Submit the message
            BOCMTMessageControllerWS cmtMessageControl = new BOCMTMessageControllerWS();
            if (cmtMessageControl.SubmitCMTMessage(message))
            {
                LabelResult.Text = "Message submitted succesfully";
                LabelResult.ForeColor = Color.Green;
            }
            else
            {
                LabelResult.Text = "Submitting message failed";
                LabelResult.ForeColor = Color.Red;
            }            
        }
    }
}