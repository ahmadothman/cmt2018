﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NewDeviceType.ascx.cs" Inherits="FOWebApp.Nocsa.NewDeviceType" %>
<telerik:RadAjaxPanel ID="RadAjaxPanelConnectedDevice" runat="server" Height="200px" Width="232px"
    EnableHistory="True" Style="margin-right: 114px">
    <table width="800px" id="ConnectedDeviceTable" cellpadding="10" cellspacing="5">
        <tr>
            <td>Device type name:</td>
            <td>
                <asp:TextBox ID="TextBoxDeviceTypeName" runat="server">
                </asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidatorDeviceTypeName" runat="server" ErrorMessage="Enter a name" ControlToValidate="TextBoxDeviceTypeName"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>Decription:</td>
            <td>
                <asp:TextBox ID="TextBoxDescription" runat="server" Width="250"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:button ID="ButtonAdd" runat="server" OnClick="ButtonAdd_Click" Text="Add Connected Device"/>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Label ID="LabelAdd" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
</telerik:RadAjaxPanel>