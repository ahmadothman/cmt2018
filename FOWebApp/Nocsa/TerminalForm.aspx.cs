﻿using FOWebApp.Data;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOConnectedDevicesControlWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;
using FOWebApp.net.nimera.cmt.BOMonitorControlWSRef;
using FOWebApp.net.nimera.cmt.BOTaskControlWSRef;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Web;
using System.Web.Security;
using Telerik.Web.UI;
using FOWebApp.NMSGatewayServiceRef;

namespace FOWebApp.Nocsa
{
    public partial class TerminalForm : System.Web.UI.Page
    {
        NMSGatewayServiceRef.NMSGatewayService nms;
        BOAccountingControlWS _boAccountingControlWS = null;
        BOLogControlWS _boLogControlWS = null;
        BOMonitorControlWS _boMonitorControlWS = null;
        BOTaskControlWS _boTaskControlWS = null;
        BOConnectedDevicesControlWS _boConnectedDevicesControlWS = null;
        DataHelper _dataHelper;
        string reasonMsg = "";

        const int _activateIdx = 3;
        const int _reactivateIdx = 4;
        const int _suspendIdx = 5;
        const int _decommissionIdx = 6;
        const int _deleteIdx = 7;
        const int _fupresetIdx = 8;

        public TerminalForm()
        {
            nms = new NMSGatewayServiceRef.NMSGatewayService();
            _boAccountingControlWS = new BOAccountingControlWS();
            _boLogControlWS = new BOLogControlWS();
            _boMonitorControlWS = new BOMonitorControlWS();
            _boTaskControlWS = new BOTaskControlWS();
            _boConnectedDevicesControlWS = new BOConnectedDevicesControlWS();
            _dataHelper = new DataHelper();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Terminal terminal = null;

            if (!IsPostBack)
            {

                //Initialize the form
                string macAddress = Request.Params["mac"];

                if (macAddress != null)
                {
                    //Fill in the form
                    terminal = _boAccountingControlWS.GetTerminalDetailsByMAC(macAddress);
                    if (!(Roles.IsUserInRole("NOC Administrator") || Roles.IsUserInRole("FinanceAdmin") || Roles.IsUserInRole("NOC Operator")))
                    {
                        Response.Redirect("~/Unauthorized.html");
                    }
                    //Store the terminal as a session value for subsequent calss
                    this.Session["TerminalForm_Terminal"] = terminal;

                    this.loadForm(terminal);
                }
                if (Roles.IsUserInRole("NOC Administrator"))
                {
                    RadTabTools.Visible = true;
                }
                ServiceLevel sla = _boAccountingControlWS.GetServicePack((int)(terminal.SlaId));
                if (sla.ServiceClass == 1)
                {
                    RadDatePickerExpiryDate.Enabled = false;
                }
                else
                {
                    RadDatePickerExpiryDate.Enabled = true;
                }
            }
            else
            {
                //Just load the correct Service Packs
                terminal = (Terminal)this.Session["TerminalForm_Terminal"];
            }

        }

        protected void RadToolBarTerminal_ButtonClick(object sender, RadToolBarEventArgs e)
        {
            MembershipUser myObject = Membership.GetUser();
            string userId = myObject.ProviderUserKey.ToString();

            //Check the button commmand and execute the command
            RadToolBarItem toolbarItem = e.Item;
            if (toolbarItem.Value.Equals("Save_Button"))
            {
                //Save the data back to the database
                if (this.saveTerminalData())
                {
                    RadWindowManager1.RadAlert("Terminal data was saved successfully. If the SLA was changed, Wait approx. 10 minutes for this new SLA to change in the HUB", 250, 200, "Save terminal data", null);
                }
                else
                {
                    RadWindowManager1.RadAlert("Saving terminal data failed!" + reasonMsg, 250, 200, "Save terminal data", null);
                }
            }

            if (toolbarItem.Value.Equals("Delete_Button"))
            {
                Terminal term = (Terminal)this.Session["TerminalForm_Terminal"];

                //Set the delete flag in the database
                LabelStatus.Text = "Deleted";
                LabelStatus.ForeColor = Color.Black;
                this.saveTerminalData();

                //Update TerminalActivity log
                TerminalActivity terminalActivity = new TerminalActivity();
                terminalActivity.Action = "Delete";
                terminalActivity.ActionDate = DateTime.Now;
                terminalActivity.MacAddress = term.MacAddress;
                terminalActivity.SlaName = term.SlaId.ToString();

                //User who did the delete
                //MembershipUser myObject = Membership.GetUser();
                //string userId = myObject.ProviderUserKey.ToString();

                terminalActivity.UserId = new Guid(userId);

                _boLogControlWS.LogTerminalActivity(terminalActivity);
            }

            if (toolbarItem.Value.Equals("Refresh_Button"))
            {
                Response.Redirect(HttpContext.Current.Request.Url.ToString(), true);
                //Terminal term = (Terminal)this.Session["TerminalForm_Terminal"];
                //if (term != null)
                //{
                //    this.loadForm(term);
                //}
            }

            if (toolbarItem.Value.Equals("Activate_Button"))
            {
                Terminal term = (Terminal)this.Session["TerminalForm_Terminal"];

                //Check if the sitId has changed 
                if (_boMonitorControlWS.TerminalActivate(term.ExtFullName, term.MacAddress, (long)term.SlaId, term.IspId, new Guid(userId)))
                {
                    this.CreateNewTask("Activation", "Activation of a new terminal", term);
                    //perform the activity for the other terminal in the redundant setup
                    if (term.RedundantSetup != null && (bool)term.RedundantSetup)
                    {
                        Terminal associatedTerm = _boAccountingControlWS.GetTerminalDetailsByMAC(term.AssociatedMacAddress);
                        if (_boMonitorControlWS.TerminalActivate(associatedTerm.ExtFullName, associatedTerm.MacAddress, (long)associatedTerm.SlaId, associatedTerm.IspId, new Guid(userId)))
                        {
                            this.CreateNewTask("Activation", "Activation of a new terminal", associatedTerm);
                            Terminal virtualTerm = _boAccountingControlWS.GetTerminalDetailsByMAC(term.VirtualTerminal);
                            _boMonitorControlWS.NMSActivateSatelliteDiversityTerminal(virtualTerm.ExtFullName, virtualTerm.MacAddress, virtualTerm.IpAddress, (long)virtualTerm.SlaId, virtualTerm.IspId, 0, term.MacAddress, associatedTerm.MacAddress, new Guid(userId));
                            RadWindowManager1.RadAlert("Activation requests sent to hubs.<br/>Wait approx. 10 minutes for completion!", 250, 200, "Terminal activation", null);
                            this.CreateNewTask("Activation", "Activation of a redundant setup", virtualTerm);
                        }
                        else
                        {
                            RadWindowManager1.RadAlert("Terminal activation failed!", 250, 100, "Activation result", null);
                        }
                    }
                    else
                    {
                        RadWindowManager1.RadAlert("Terminal activation request sent to the hub.<br/>Wait approx. 10 minutes for completion!", 250, 200, "Terminal activation", null);
                    }
                }
                else
                {
                    RadWindowManager1.RadAlert("Terminal activation failed!", 250, 100, "Activation result", null);
                }
            }

            if (toolbarItem.Value.Equals("Suspend_Button"))
            {
                Terminal term = (Terminal)this.Session["TerminalForm_Terminal"];
                if (term.ExtFullName == null || term.ExtFullName.Equals(""))
                {
                    this.logError("term.ExtFullName is null or empty for SitId: " + term.SitId, "", "Suspending a terminal");
                    RadWindowManager1.RadAlert("Terminal suspension failed", 250, 100, "Suspension result", null);
                }
                else
                {
                    if (_boMonitorControlWS.TerminalSuspend(term.ExtFullName, term.MacAddress, term.IspId, new Guid(userId)))
                    {
                        this.CreateNewTask("Suspend", "Suspension of an existing terminal", term);
                        //perform the activity for the other terminal in the redundant setup
                        if (term.RedundantSetup != null && (bool)term.RedundantSetup)
                        {
                            Terminal associatedTerm = _boAccountingControlWS.GetTerminalDetailsByMAC(term.AssociatedMacAddress);
                            if (_boMonitorControlWS.TerminalSuspend(associatedTerm.ExtFullName, associatedTerm.MacAddress, associatedTerm.IspId, new Guid(userId)))
                            {
                                RadWindowManager1.RadAlert("Suspension requests sent to the hubs.<br/>Wait approx. 10 minutes for completion!", 250, 200, "Terminal suspension", null);
                                this.CreateNewTask("Suspend", "Suspension of an existing terminal", associatedTerm);
                            }
                            else
                            {
                                RadWindowManager1.RadAlert("Terminal suspension failed", 250, 100, "Suspension result", null);
                            }
                        }
                        else
                        {
                            RadWindowManager1.RadAlert("Terminal suspension request sent to the hub.<br/>Wait approx. 10 minutes for completion!", 250, 200, "Terminal suspension", null);
                        }
                    }
                    else
                    {
                        RadWindowManager1.RadAlert("Terminal suspension failed", 250, 100, "Suspension result", null);
                    }
                }
            }

            if (toolbarItem.Value.Equals("ReActivate_Button"))
            {
                Terminal term = (Terminal)this.Session["TerminalForm_Terminal"];
                if (_boMonitorControlWS.TerminalReActivate(term.ExtFullName, term.MacAddress, term.IspId, new Guid(userId)))
                {
                    this.CreateNewTask("Re-Activate", "Re-activation of an existing terminal", term);
                    //perform the activity for the other terminal in the redundant setup
                    if (term.RedundantSetup != null && (bool)term.RedundantSetup)
                    {
                        Terminal associatedTerm = _boAccountingControlWS.GetTerminalDetailsByMAC(term.AssociatedMacAddress);
                        if (_boMonitorControlWS.TerminalReActivate(associatedTerm.ExtFullName, associatedTerm.MacAddress, associatedTerm.IspId, new Guid(userId)))
                        {
                            this.CreateNewTask("Re-Activate", "Re-activation of an existing terminal", associatedTerm);
                            RadWindowManager1.RadAlert("Re-activation requests sent to hubs.<br/>Wait approx. 10 minutes for completion!", 250, 200, "Terminal re-activation", null);
                        }
                        else
                        {
                            RadWindowManager1.RadAlert("Terminal re-activation failed", 250, 100, "Terminal re-activation", null);
                        }
                    }
                    else
                    {
                        RadWindowManager1.RadAlert("Terminal re-activation request sent to hub.<br/>Wait approx. 10 minutes for completion!", 250, 200, "Terminal re-activation", null);
                    }
                }
                else
                {
                    RadWindowManager1.RadAlert("Terminal re-activation failed", 250, 100, "Terminal re-activation", null);
                }

            }

            if (toolbarItem.Value.Equals("Decommission_Button"))
            {
                Terminal term = (Terminal)this.Session["TerminalForm_Terminal"];
                if (_boMonitorControlWS.TerminalDecommission(term.ExtFullName, term.MacAddress, term.IspId, new Guid(userId)))
                {
                    this.CreateNewTask("Decommissioning", "Decommissioning of a terminal", term);
                    //perform the activity for the other terminal in the redundant setup
                    if (term.RedundantSetup != null && (bool)term.RedundantSetup)
                    {
                        Terminal associatedTerm = _boAccountingControlWS.GetTerminalDetailsByMAC(term.AssociatedMacAddress);
                        if (_boMonitorControlWS.TerminalDecommission(associatedTerm.ExtFullName, associatedTerm.MacAddress, associatedTerm.IspId, new Guid(userId)))
                        {
                            this.CreateNewTask("Decommissioning", "Decommissioning of a terminal", term);
                            RadWindowManager1.RadAlert("Decommissioning request sent to hubs.<br/>Wait approx. 10 minutes for completion!", 250, 200, "Terminal decommissioning", null);
                        }
                        else
                        {
                            RadWindowManager1.RadAlert("Terminal decommissioning failed", 250, 100, "Terminal decommissioning", null);
                        }
                    }
                    else
                    {
                        RadWindowManager1.RadAlert("Terminal decommissioning request sent to the hub.<br/>Wait approx. 10 minutes for completion!", 250, 200, "Terminal decommissioning", null);
                    }
                }
                else
                {
                    RadWindowManager1.RadAlert("Terminal decommissioning failed", 250, 100, "Terminal decommissioning", null);
                }
            }

            if (toolbarItem.Value.Equals("FUPReset_Button"))
            {
                Terminal term = (Terminal)this.Session["TerminalForm_Terminal"];
                if (_boMonitorControlWS.TerminalUserReset(term.ExtFullName, term.IspId))
                {
                    //Update TerminalActivity log
                    TerminalActivity terminalActivity = new TerminalActivity();
                    terminalActivity.Action = "FUPReset";
                    terminalActivity.ActionDate = DateTime.Now;
                    terminalActivity.MacAddress = term.MacAddress;
                    terminalActivity.AccountingFlag = true;
                    terminalActivity.SlaName = term.SlaId.ToString();


                    //User who did the FUP reset
                    //MembershipUser myObject = Membership.GetUser();
                    //string userId = myObject.ProviderUserKey.ToString();

                    terminalActivity.UserId = new Guid(userId);

                    _boLogControlWS.LogTerminalActivity(terminalActivity);

                    this.CreateNewTask("FUP Reset", "Fair Usage Policy (FUP) Volume Reset", term);
                    //perform the activity for the other terminal in the redundant setup
                    if (term.RedundantSetup != null && (bool)term.RedundantSetup)
                    {
                        Terminal associatedTerm = _boAccountingControlWS.GetTerminalDetailsByMAC(term.AssociatedMacAddress);
                        if (_boMonitorControlWS.TerminalUserReset(associatedTerm.ExtFullName, associatedTerm.IspId))
                        {
                            //Update TerminalActivity log
                            terminalActivity = new TerminalActivity();
                            terminalActivity.Action = "FUPReset";
                            terminalActivity.ActionDate = DateTime.Now;
                            terminalActivity.MacAddress = associatedTerm.MacAddress;
                            terminalActivity.AccountingFlag = true;
                            terminalActivity.SlaName = associatedTerm.SlaId.ToString();

                            //User who did the FUP reset

                            terminalActivity.UserId = new Guid(userId);

                            _boLogControlWS.LogTerminalActivity(terminalActivity);
                            this.CreateNewTask("FUP Reset", "Fair Usage Policy (FUP) Volume Reset", associatedTerm);
                            RadWindowManager1.RadAlert("Terminal volume reset succeeded.", 250, 100, "FUP Reset", null);

                        }
                        else
                        {
                            RadWindowManager1.RadAlert("Terminal volume reset failed.", 250, 100, "FUP Reset", null);
                        }
                    }
                    else
                    {
                        RadWindowManager1.RadAlert("Terminal volume reset succeeded.", 250, 100, "FUP Reset", null);
                    }
                }
                else
                {
                    RadWindowManager1.RadAlert("Terminal volume reset failed.", 250, 100, "FUP Reset", null);
                }
            }

        }

        //Reads the data from the form and stores the information in the database
        private bool saveTerminalData()
        {
            bool result = false;
            bool slaChangedFlag = false; //Indicates if the SLA has been changed
            bool macAddressChangeFlag = false; //Indicates if the MAC Address has been changed
            //bool ispChangeFlag = false; //Indicates if the ISP has been changed
            bool ipAddressChangeFlag = false; //Indicates if the IP Address has changed
            bool ipMaskChangeFlag = false; //Indicates if the IP Mask has changed
            bool freeZoneChangeFlag = false;

            string newMacAddress = ""; //The new MAC Address
            string newIP = ""; //The new IP Address
            string newIPMask = ""; //The new IP Mask
            string newSLAId = ""; //The new SLA Id
            int newFreeZone = 0;
            bool freeZoneEnabled;

            MembershipUser myObject = Membership.GetUser();
            string userId = myObject.ProviderUserKey.ToString();

            Terminal terminal = (Terminal)this.Session["TerminalForm_Terminal"];

            try
            {
                //Check if the SLA has been changed.
                slaChangedFlag = (terminal.SlaId != Int32.Parse(RadComboBoxSLA.SelectedValue));

                //Check if the MAC Address has been changed.
                macAddressChangeFlag = (!terminal.MacAddress.Equals(TextBoxMacAddress.Text));

                //Check if the ISP has been changed
                //ispChangeFlag = (!terminal.IspId.Equals(Int32.Parse(RadComboBoxISP.SelectedValue)));

                //Check if the IP address
                string ipAddressTrimmed = terminal.IpAddress.Trim();
                ipAddressChangeFlag = (!ipAddressTrimmed.Equals(TextBoxIP.Text));

                //Check if the IP mask has changed
                ipMaskChangeFlag = (terminal.IPMask != Int32.Parse(TextBoxIPMask.Text));

                //Keep the new MAC Address, at this stage the terminal keeps the old MAC address in the
                //CMT. Only if the change of MAC address is reported as successfully by the TicketMaster
                //the terminal is recreated with a the new MAC address. Not if the status is request
                if (macAddressChangeFlag && terminal.AdmStatus != 5)
                {
                    newMacAddress = TextBoxMacAddress.Text.Trim();
                }
                else
                {
                    terminal.MacAddress = TextBoxMacAddress.Text.Trim();
                }

                ///Keep the new IP Address, at this stage the terminal keeps the old IP address AND mask
                ///in the CMT. The behaviour is identical to the MAC address change.
                if (ipAddressChangeFlag && terminal.AdmStatus != 5)
                {
                    newIP = TextBoxIP.Text.Trim();
                }
                else
                {
                    terminal.IpAddress = TextBoxIP.Text.Trim();
                }

                if (ipMaskChangeFlag && terminal.AdmStatus != 5)
                {
                    newIPMask = TextBoxIPMask.Text.Trim();
                }
                else
                {
                    if (TextBoxIPMask.Text.Equals(""))
                    {
                        terminal.IPMask = 32;
                    }
                    else
                    {
                        terminal.IPMask = Int32.Parse(TextBoxIPMask.Text.Trim());
                    }
                    newIPMask = terminal.IPMask.ToString();
                }

                ///Keep the new SLA as above
                if (slaChangedFlag && terminal.AdmStatus != 5)
                {
                    newSLAId = RadComboBoxSLA.SelectedValue;
                }
                else
                {
                    terminal.SlaId = Int32.Parse(RadComboBoxSLA.SelectedValue);
                }

                if (!TextBoxSitId.Text.Equals(""))
                {
                    terminal.SitId = Int32.Parse(TextBoxSitId.Text);
                }

                terminal.FullName = TextBoxTerminalName.Text;

                if (!RadComboBoxISP.SelectedValue.Equals("") && terminal.AdmStatus==5)
                {
                    terminal.IspId = Int32.Parse(RadComboBoxISP.SelectedValue);
                }


                terminal.Serial = TextBoxSerial.Text;

                //Keep the old SLA until the modification has been completed
                /*if (!RadComboBoxSLA.SelectedValue.Equals(""))
                {
                    terminal.SlaId = Int32.Parse(RadComboBoxSLA.SelectedValue);
                }*/

                //Keep the old IP Address until the modification has been completed
                /*terminal.IpAddress = TextBoxIP.Text;

                if (!TextBoxIPMask.Text.Equals(""))
                {
                    terminal.IPMask = Int32.Parse(TextBoxIPMask.Text);
                }*/

                terminal.IPRange = CheckBoxIPRange.Checked;
                if (!CheckBoxTestMode.Checked)
                {
                    if (CheckBoxTestMode.Checked != CheckBoxCurrentBool.Checked)
                    {
                        TerminalActivity termActivity = new TerminalActivity();
                        termActivity.Action = "Activation";
                        termActivity.ActionDate = DateTime.Now;
                        termActivity.MacAddress = TextBoxMacAddress.Text;
                        termActivity.SlaName = RadComboBoxSLA.SelectedItem.Value;
                        termActivity.TerminalActivityId = 100;
                        termActivity.AccountingFlag = true;
                        termActivity.UserId = new Guid(userId);
                        if (terminal.LateBird == true)
                            termActivity.Prepaid = true;

                        try
                        {
                            _boLogControlWS.LogTerminalActivity(termActivity);
                        }
                        catch (Exception ex)
                        {
                            CmtApplicationException cmtEx = new CmtApplicationException();
                            cmtEx.ExceptionDateTime = DateTime.Now;
                            cmtEx.ExceptionDesc = ex.Message;
                            cmtEx.UserDescription = "Could not insert new activity message";
                            cmtEx.ExceptionStacktrace = ex.StackTrace;
                            _boLogControlWS.LogApplicationException(cmtEx);
                        }
                    }
                }
                terminal.TestMode = CheckBoxTestMode.Checked;
                terminal.StaticIPFlag = CheckBoxStaticIP.Checked;

                //Address information
                if (terminal.Address != null)
                {
                    terminal.Address.AddressLine1 = TextBoxAddressLine1.Text;
                    terminal.Address.AddressLine2 = TextBoxAddressLine2.Text;
                    terminal.Address.Location = TextBoxLocation.Text;
                    terminal.Address.PostalCode = TextBoxPCO.Text;
                    terminal.Address.Country = CountryList.SelectedValue;
                }
                terminal.Phone = TextBoxPhone.Text;
                terminal.Fax = TextBoxFax.Text;
                terminal.Email = TextBoxEMail.Text;

                //Subscription
                if (!RadComboBoxCustomer.SelectedValue.Equals(""))
                {
                    terminal.OrgId = Int32.Parse(RadComboBoxCustomer.SelectedValue);
                }

                if (!RadComboBoxDistributor.SelectedValue.Equals(""))
                {
                    terminal.DistributorId = Int32.Parse(RadComboBoxDistributor.SelectedValue);
                }

                //set the FreeZone variable for old terminals to avoid problems with NULL
                if (terminal.FreeZone == null)
                {
                    terminal.FreeZone = 0;
                }

                //Check if SLA has FreeZone option
                ServiceLevel sla = _boAccountingControlWS.GetServicePack(Convert.ToInt32(RadComboBoxSLA.SelectedValue));
                if (sla.FreeZone != null)
                {
                    if (!(bool)sla.FreeZone)
                    {
                        RadComboBoxFreeZone.SelectedValue = "0";
                    }
                }

                //Check if the FreeZone has been changed
                freeZoneChangeFlag = (terminal.FreeZone.ToString() != RadComboBoxFreeZone.SelectedValue);

                //store the FreeZone selection
                if (freeZoneChangeFlag && terminal.AdmStatus != 5)
                {
                    newFreeZone = Convert.ToInt32(RadComboBoxFreeZone.SelectedValue);
                    if (_boMonitorControlWS.NMSModifyFreeZoneForTerminal(terminal.ExtFullName, newFreeZone, terminal.IspId))
                    {
                        terminal.FreeZone = newFreeZone;
                    }
                    else
                    {
                        this.logError("Change of free zone failed for terminal: "
                        + terminal.FullName, "Original free zone ID: " + terminal.FreeZone, "Method TerminalForm - saveTerminalData", 2);
                    }
                }
                else
                {
                    terminal.FreeZone = Convert.ToInt32(RadComboBoxFreeZone.SelectedValue);
                }

                terminal.StartDate = RadDatePickerStartDate.SelectedDate;
                terminal.ExpiryDate = RadDatePickerExpiryDate.SelectedDate;
                terminal.AdmStatus = _dataHelper.getStatusId(LabelStatus.Text);

                terminal.LastInvoiceDate = RadDatePickerLastInvoice.SelectedDate;
                terminal.TerminalInvoicing = CheckBoxTerminalInvoicing.Checked;
                terminal.InvoiceInterval = (int)RadNumericTextBoxInvoiceInterval.Value;

                //Check the satellite diversity parameters
                if (terminal.RedundantSetup != null)
                {
                    if ((bool)terminal.RedundantSetup)
                    {
                        if (RadComboBoxPrimary.SelectedValue == "Primary")
                        {
                            terminal.PrimaryTerminal = true;
                        }
                        else if (RadComboBoxPrimary.SelectedValue == "Secondary")
                        {
                            terminal.PrimaryTerminal = false;
                        }
                        else
                        {
                            terminal.PrimaryTerminal = null;
                        }
                        terminal.AssociatedMacAddress = TextBoxAssociatedTerminal.Text;
                        Terminal associatedTerm = _boAccountingControlWS.GetTerminalDetailsByMAC(terminal.AssociatedMacAddress);
                        if (terminal.PrimaryTerminal != null && associatedTerm.PrimaryTerminal != null)
                        {
                            if (terminal.PrimaryTerminal == associatedTerm.PrimaryTerminal)
                            {
                                //Only one terminal can be the primary and only one terminal can be the secondary
                                RadWindowManager1.RadAlert("The value for Primary terminal<br/>cannot be the same for the two terminals<br/>in a redundant setup", 250, 100, "Primary terminal conflict", null);
                                return false;
                            }
                        }
                    }
                }

                //Geolocation tab
                Coordinate coordinates = new Coordinate();

                if (!TextBoxLatitude.Text.Equals(""))
                {
                    coordinates.Latitude = Double.Parse(TextBoxLatitude.Text);
                }

                if (!TextBoxLongitude.Text.Equals(""))
                {
                    coordinates.Longitude = Double.Parse(TextBoxLongitude.Text);
                }

                terminal.LatLong = coordinates;

                //Hub
                if (!TextBoxBlade.Text.Equals(""))
                {
                    terminal.Blade = Int32.Parse(TextBoxBlade.Text);
                }

                terminal.IPMgm = TextBoxIPMgm.Text;
                terminal.IPTunnel = TextBoxIPTunnel.Text;

                if (!TextBoxRTPool.Text.Equals(""))
                {
                    terminal.RTPool = Int32.Parse(TextBoxRTPool.Text);
                }

                if (!TextBoxFWPool.Text.Equals(""))
                {
                    terminal.FWPool = Int32.Parse(TextBoxFWPool.Text);
                }

                if (!TextBoxSpotID.Text.Equals(""))
                {
                    terminal.SpotID = Int32.Parse(TextBoxSpotID.Text);
                }

                if (!RadComboBoxModem.SelectedValue.Equals(0))
                {
                    terminal.Modem = RadComboBoxModem.SelectedItem.Text;
                }

                if (!RadComboBoxILNBBUC.SelectedValue.Equals(0))
                {
                    terminal.iLNBBUC = RadComboBoxILNBBUC.SelectedItem.Text;
                }

                if (!RadComboBoxAntenna.SelectedValue.Equals(0))
                {
                    terminal.Antenna = RadComboBoxAntenna.SelectedItem.Text;
                }

                terminal.DialogTechnology = RadComboBoxDialogFrequency.SelectedItem.Text;
                terminal.CenterFrequency = Convert.ToInt64(RadNumericCenterFrequency.Value);
                terminal.SymbolRate = Convert.ToInt64(RadNumericTextBoxSymbolRate.Value);

                //Remarks
                terminal.Remarks = TextBoxRemarks.Text;

                //Update in the new CMT Database
                result = _boAccountingControlWS.UpdateTerminal(terminal);

                if (result && terminal.AdmStatus != 5)
                {
                    //Update the SLA and MacAddress in the HUB if necessary, only if the status is NOT request
                    if (slaChangedFlag)
                    {
                        //SMT-19 Expand SLA change

                        /**********DONT'T FORGET TO CHECK IF SATELLITE MATCH BOTH SLA'S**********/

                        ServiceLevel oldSla = _boAccountingControlWS.GetServicePack((int)(terminal.SlaId));
                        ServiceLevel newSla = _boAccountingControlWS.GetServicePack((int)Convert.ToInt32(newSLAId));
                        Isp ispOldSLA = _boAccountingControlWS.GetISP((int)oldSla.IspId);
                        Isp ispNewSLA = _boAccountingControlWS.GetISP((int)newSla.IspId);
                        if (oldSla.ServiceClass == newSla.ServiceClass)
                        {
                            
                            if (!_boMonitorControlWS.TerminalChangeSla(terminal.ExtFullName, terminal.MacAddress,
                                                         Convert.ToInt32(newSLAId), (AdmStatus)(terminal.AdmStatus), terminal.IspId, new Guid(userId)))
                            {
                                result = false;
                                this.logError("Change of SLA for End UserId: " + terminal.FullName + " failed!",
                                                "Change of SLA failed", "", 2);
                            }
                            else
                            {
                                if (oldSla.IspId != newSla.IspId)
                                {
                                    terminal.IspId = (int)newSla.IspId;
                                    result = _boAccountingControlWS.UpdateTerminal(terminal);
                                }
                            }

                        }
                        else
                        {
                            if (ispOldSLA.Satellite.SatelliteName==ispNewSLA.Satellite.SatelliteName && terminal.AdmStatus == 1)
                            {
                                // exceptional call from the frontEnd to the NMS via CMT-IF machine
                                RequestTicket rt1 = nms.removeRegistration(terminal.ExtFullName, terminal.MacAddress);

                                while (rt1.requestStatus == RequestStatus.BUSY) // Wait until removing terminal from NMS completed
                                {
                                    rt1.requestStatus=(RequestStatus)_boMonitorControlWS.NMSLookupRequestTicket(rt1.id, terminal.IspId).requestStatus;
                                }

                                RequestTicket rt2 = null;
                                if (terminal.OrgId != null)
                                {
                                    Organization organization = _boAccountingControlWS.GetOrganization((int)terminal.OrgId);
                                    if (organization.VNO != null && (bool)organization.VNO)
                                    {
                                         rt2 = nms.createRegistrationWithVNO(terminal.ExtFullName, terminal.MacAddress, terminal.IPMask, (int)Convert.ToInt32(newSLAId), (int)newSla.IspId, terminal.IpAddress, terminal.SitId, (int)terminal.FreeZone, organization.FullName);
                                    }
                                    else
                                    {
                                         rt2 = nms.createRegistration(terminal.ExtFullName, terminal.MacAddress, terminal.IPMask, (int)Convert.ToInt32(newSLAId), (int)newSla.IspId, terminal.IpAddress, terminal.SitId, (int)terminal.FreeZone);
                                    }
                                }
                                
                                else
                                {
                                     rt2 = nms.createRegistration(terminal.ExtFullName, terminal.MacAddress, terminal.IPMask, (int)Convert.ToInt32(newSLAId), (int)newSla.IspId, terminal.IpAddress, terminal.SitId, (int)terminal.FreeZone);
                                }


                                while (rt2.requestStatus == RequestStatus.BUSY) // Wait until new registeration of termianl has completed
                                {
                                    rt2.requestStatus = (RequestStatus)_boMonitorControlWS.NMSLookupRequestTicket(rt2.id, terminal.IspId).requestStatus;
                                }


                                terminal.IspId = (int)newSla.IspId;
                                terminal.SlaId = newSla.SlaId;
                                result=_boAccountingControlWS.UpdateTerminal(terminal);

                                if ((!result)||(rt1 == null) || (rt2 == null) || !_boMonitorControlWS.TerminalChangeSla(terminal.ExtFullName, terminal.MacAddress,
                                                        Convert.ToInt32(newSLAId), (AdmStatus)(terminal.AdmStatus), terminal.IspId, new Guid(userId)))
                                {
                                    result = false;
                                    this.logError("Change of SLA for End UserId: " + terminal.FullName + " failed!",
                                                    "Change of SLA failed", "", 2);
                                }
                            }
                            else
                            {
                                result = false;
                                reasonMsg = reasonMsg + "<br/> Failed to change Terminal SLA <br/> Terminal should be locked <br/>  and on the same Satellite.";
                                this.logError("Change of SLA for End UserId: " + terminal.FullName + " failed!",
                                                "Terminal should be locked and on the same Satellite", "", 2);
                            }

                        }


                        //if (!_boMonitorControlWS.TerminalChangeSla(terminal.ExtFullName, terminal.MacAddress,
                        //                                 Convert.ToInt32(newSLAId), (AdmStatus)(terminal.AdmStatus), terminal.IspId, new Guid(userId)))
                        //{
                        //    result = false;
                        //    this.logError("Change of SLA for End UserId: " + terminal.FullName + " failed!",
                        //                    "Change of SLA failed", "", 2);
                        //}
                    }

                    if (result && macAddressChangeFlag)
                    {
                        if (terminal.AdmStatus == 2) // SMT-14
                        {
                            if (!_boMonitorControlWS.ChangeMacAddress(terminal.ExtFullName, terminal.MacAddress, terminal.IspId, newMacAddress, new Guid(userId)))
                            {
                                result = false;
                                this.logError("Change of Mac Address failed for End UserId: " + terminal.FullName + " failed!",
                                    "Original Mac Address: " + terminal.MacAddress + ", Target Mac Address: " + newMacAddress,
                                    "Method TerminalForm - saveTerminalData", 2);
                            }
                            else
                            {
                                string oldMacAddress = terminal.MacAddress;
                                terminal.MacAddress = newMacAddress;
                                this.CreateNewTask("MacAddressChange", "Verify MAC address change. Original MAC Address: " + oldMacAddress + ", Target Mac Address: " + newMacAddress, terminal);
                            }
                        }
                        else
                        {
                            result = false;
                            reasonMsg = reasonMsg + "<br/> Failed to change MAC address <br/> Terminal should be operational (Unlocked)";
                        }
                    }

                    //if (result && ispChangeFlag)
                    //{
                    //    int newIsp = Int32.Parse(RadComboBoxISP.SelectedValue);
                    //    if (!_boMonitorControlWS.ChangeISP(terminal.ExtFullName, terminal.IspId, newIsp))
                    //    {
                    //        result = false;
                    //        this.logError("Change of ISP failed for End UserId: " + terminal.FullName, "Original ISP: " + terminal.IspId + ", Target ISP: " + terminal.IspId,
                    //                        "Method TerminalForm - saveTerminalData", 2);
                    //    }
                    //}

                    if (result && (ipAddressChangeFlag || ipMaskChangeFlag))
                    {
                        if (!_boMonitorControlWS.NMSChangeIPAddress(terminal.ExtFullName, terminal.MacAddress, terminal.IpAddress, terminal.IPMask,
                                                newIP, Int32.Parse(newIPMask), terminal.IspId, new Guid(userId)))
                        {
                            result = false;
                            this.logError("Change of IP Address and/or IP Mask failed for terminal: "
                                + terminal.FullName, "Original IP Address/Mask: " + terminal.IpAddress + " /"
                                + terminal.IPMask, "Method TerminalForm - saveTerminalData", 2);
                        }
                        else
                        {
                            if (ipAddressChangeFlag)
                            {
                                this.CreateNewTask("IPAddressChange", "Verify IP Address change from: "
                                    + terminal.IpAddress + " to " + newIP, terminal);
                            }
                            else if (ipMaskChangeFlag)
                            {
                                this.CreateNewTask("IPMaskChange", "Verify IP Mask change from: "
                                    + terminal.IPMask + " to " + newIPMask, terminal);
                            }
                        }
                    }
                }

                //Store new version in session variable
                if (result)
                {
                    this.Session["TerminalForm_Terminal"] = terminal;
                    this.loadForm(terminal);
                }
            }
            catch (Exception ex)
            {
                this.logError(ex.Message, "TerminalForm - saveTerminalData failed", ex.StackTrace, 4);
                result = false;
            }

            return result;
        }

        /// <summary>
        /// Loads the form with the given terminal data
        /// </summary>
        /// <param name="terminal"></param>
        public void loadForm(Terminal terminal)
        {
            if (terminal != null)
            {
                //Setup the terminal management buttons
                this.initTerminalManagementButtons(terminal);

                TextBoxMacAddress.Text = terminal.MacAddress.Trim();

                if (terminal.AdmStatus == (int)AdmStatus.Request + 1 || terminal.AdmStatus == (int)AdmStatus.LOCKED + 1)
                {
                    RadComboBoxISP.Enabled = true;
                    TextBoxMacAddress.Enabled = true;
                    RadComboBoxSLA.Enabled = true;
                }
                else
                {
                    RadComboBoxISP.Enabled = false;
                    TextBoxMacAddress.Enabled = false;
                }

                //if (terminal.AdmStatus == (int)AdmStatus.LOCKED + 1)
                //{
                //    TextBoxMacAddress.Enabled = true;
                //    RadComboBoxSLA.Enabled = false;
                //}
                //else
                //{
                //    TextBoxMacAddress.Enabled = false;
                //}

                if (terminal.AdmStatus == (int)AdmStatus.OPERATIONAL + 1)
                {
                    TextBoxMacAddress.Enabled = true;
                    RadComboBoxSLA.Enabled = true;
                }

                TextBoxSitId.Text = terminal.SitId.ToString();
                TextBoxTerminalName.Text = terminal.FullName;
                TextBoxSerial.Text = terminal.Serial;

                if (terminal.TestMode != null)
                {
                    CheckBoxTestMode.Checked = (bool)terminal.TestMode;
                    CheckBoxCurrentBool.Checked = (bool)terminal.TestMode;
                }

                CheckBoxStaticIP.Checked = terminal.StaticIPFlag ?? false;

                List<Isp> IspList = _dataHelper.getISPs();
                RadComboBoxISP.Items.Clear();
                foreach (Isp isp in IspList)
                {
                    RadComboBoxItem item = new RadComboBoxItem();
                    item.Value = isp.Id.ToString();
                    item.Text = isp.CompanyName;
                    RadComboBoxISP.Items.Add(item);
                }

                RadComboBoxISP.SelectedValue = terminal.IspId.ToString();

                ServiceLevel[] slaItems = _boAccountingControlWS.GetServicePacksByIspWithHide(terminal.IspId);
                RadComboBoxSLA.Items.Clear();
                foreach (ServiceLevel slaItem in slaItems)
                {
                    RadComboBoxItem item = new RadComboBoxItem();
                    item.Value = slaItem.SlaId.ToString();
                    item.Text = slaItem.SlaName.Trim();
                    RadComboBoxSLA.Items.Add(item);
                }

                RadComboBoxSLA.SelectedValue = terminal.SlaId.ToString();
                TextBoxWeight.Text = terminal.TermWeight.ToString();
                TextBoxWeight.Enabled = false;
                if (terminal.IpAddress != null)
                    TextBoxIP.Text = terminal.IpAddress.Trim();
                else
                    TextBoxIP.Text = "";

                if (terminal.IPRange != null)
                {
                    CheckBoxIPRange.Checked = (bool)terminal.IPRange;
                    if ((bool)terminal.IPRange)
                    {
                        TextBoxIPMask.Text = terminal.IPMask.ToString();
                        TextBoxIPMask.Enabled = true;
                    }
                    else
                    {
                        TextBoxIPMask.Text = "32";
                        TextBoxIPMask.Enabled = false;
                    }
                }
                else
                {
                    CheckBoxIPRange.Checked = false;
                    TextBoxIPMask.Text = "32"; // set the mask to a value as other methods might break otherwise
                    TextBoxIPMask.Enabled = false;
                }

                if (terminal.Address != null)
                {
                    TextBoxAddressLine1.Text = terminal.Address.AddressLine1;
                    TextBoxAddressLine2.Text = terminal.Address.AddressLine2;
                    TextBoxLocation.Text = terminal.Address.Location;
                    TextBoxPCO.Text = terminal.Address.PostalCode;
                    CountryList.SelectedValue = terminal.Address.Country;
                }
                TextBoxPhone.Text = terminal.Phone;
                TextBoxFax.Text = terminal.Fax;
                TextBoxEMail.Text = terminal.Email;

                //Load the Distributor combobox
                Distributor[] distributors = _boAccountingControlWS.GetDistributorsAndVNOs();

                foreach (Distributor distributor in distributors)
                {
                    RadComboBoxItem item = new RadComboBoxItem();
                    item.Value = distributor.Id.ToString();
                    item.Text = distributor.FullName;
                    RadComboBoxDistributor.Items.Add(item);
                }

                RadComboBoxDistributor.SelectedValue = "" + terminal.DistributorId;

                //Load customers into the combobox
                Organization[] organizations = _boAccountingControlWS.GetOrganisationsByDistributorId(terminal.DistributorId.ToString());

                RadComboBoxCustomer.Items.Clear();
                RadComboBoxCustomer.ClearSelection();
                RadComboBoxCustomer.DataSource = organizations;
                RadComboBoxCustomer.DataTextField = "FullName";
                RadComboBoxCustomer.DataValueField = "Id";
                RadComboBoxCustomer.DataBind();

                if (!(organizations.Length == 0))
                {
                    RadComboBoxItem item = new RadComboBoxItem();
                    item.Value = "0";
                    item.Text = "";
                    RadComboBoxCustomer.Items.Add(item);
                    RadComboBoxCustomer.SortItems();
                    if (terminal.OrgId != null)
                    {
                        RadComboBoxCustomer.SelectedValue = terminal.OrgId.ToString();
                    }
                    else
                    {
                        RadComboBoxCustomer.SelectedValue = "0";
                    }
                }
                else
                {
                    RadComboBoxCustomer.Text = "No customers defined";
                    RadComboBoxCustomer.Enabled = false;
                }


                //Load the FreeZone list
                FreeZoneCMT[] freeZoneList = _boAccountingControlWS.GetFreeZones();

                foreach (FreeZoneCMT fz in freeZoneList)
                {
                    RadComboBoxItem item = new RadComboBoxItem();
                    item.Value = fz.Id.ToString();
                    item.Text = fz.Name;
                    RadComboBoxFreeZone.Items.Add(item);
                }

                if (terminal.FreeZone != null)
                {
                    RadComboBoxFreeZone.SelectedValue = terminal.FreeZone.ToString();
                }
                else
                {
                    RadComboBoxFreeZone.SelectedValue = "0";
                }

                // CMTFO-89

                bool freeZoneFlag = _boAccountingControlWS.IsFreeZoneSLA((int)(terminal.SlaId));

                if (freeZoneFlag == true)
                {
                    RadComboBoxFreeZone.Enabled = true;
                }
                else
                {
                    RadComboBoxFreeZone.Enabled = false;
                }


                RadDatePickerStartDate.SelectedDate = terminal.StartDate;
                RadDatePickerExpiryDate.SelectedDate = terminal.ExpiryDate;

                if (terminal.FirstActivationDate == null)
                {
                    terminal.FirstActivationDate = terminal.StartDate;
                }

                //Load status data
                LabelStatus.Text = _dataHelper.getStatusById((int)terminal.AdmStatus);
                this.Title = "Modify Terminal - " + terminal.MacAddress + " - " + LabelStatus.Text;
                if (terminal.AdmStatus == 1)
                    LabelStatus.ForeColor = Color.Red;

                if (terminal.AdmStatus == 2)
                    LabelStatus.ForeColor = Color.Green;

                if (terminal.AdmStatus == 3)
                    LabelStatus.ForeColor = Color.Blue;

                if (terminal.AdmStatus == 5)
                    LabelStatus.ForeColor = Color.DarkOrange;

                CheckBoxTerminalInvoicing.Checked = terminal.TerminalInvoicing;

                RadNumericTextBoxInvoiceInterval.Value = terminal.InvoiceInterval;

                if (terminal.LastInvoiceDate.HasValue)
                {
                    RadDatePickerLastInvoice.SelectedDate = terminal.LastInvoiceDate;
                }

                //Get Satellite diversity properties
                RadComboBoxPrimary.Items.Clear();
                RadComboBoxPrimary.ClearSelection();
                string[] primaryTerminal = new string[3] { "Non-redundant", "Primary", "Secondary" };
                for (int i = 0; i < primaryTerminal.Length; i++)
                {
                    RadComboBoxItem item = new RadComboBoxItem();
                    item.Value = primaryTerminal[i];
                    item.Text = primaryTerminal[i];
                    RadComboBoxPrimary.Items.Add(item);
                }
                RadComboBoxPrimary.SelectedValue = "Non-redundant";
                if (terminal.RedundantSetup != null)
                {
                    if ((bool)terminal.RedundantSetup)
                    {
                        CheckBoxRedundantSetup.Checked = true;
                        if (terminal.PrimaryTerminal != null)
                        {
                            if ((bool)terminal.PrimaryTerminal)
                            {
                                RadComboBoxPrimary.SelectedValue = "Primary";
                            }
                            else
                            {
                                RadComboBoxPrimary.SelectedValue = "Secondary";
                            }
                        }
                        TextBoxAssociatedTerminal.Text = terminal.AssociatedMacAddress;
                    }
                }

                //Load connected devices
                ConnectedDevice[] connectedDevices = _boConnectedDevicesControlWS.GetDevicesForTerminal(terminal.MacAddress);
                string cd = "";
                foreach (ConnectedDevice c in connectedDevices)
                {
                    cd += c.DeviceName + " (" + c.DeviceType + ")<br/>";
                }
                LabelConnectedDevices.Text = cd;

                if (terminal.LatLong != null)
                {
                    TextBoxLatitude.Text = terminal.LatLong.Latitude.ToString();
                }

                if (terminal.LatLong != null)
                {
                    TextBoxLongitude.Text = terminal.LatLong.Longitude.ToString();
                }

                if (terminal.Blade != null)
                {
                    TextBoxBlade.Text = terminal.Blade.ToString();
                }

                if (terminal.IPMgm != null)
                {
                    TextBoxIPMgm.Text = terminal.IPMgm;
                }

                if (terminal.IPTunnel != null)
                {
                    TextBoxIPTunnel.Text = terminal.IPTunnel;
                }

                TextBoxRTPool.Text = terminal.RTPool.ToString();
                TextBoxFWPool.Text = terminal.FWPool.ToString();

                if (terminal.SpotID != null)
                {
                    TextBoxSpotID.Text = terminal.SpotID.ToString();
                }


                if (terminal.Modem != null)
                {
                    RadComboBoxItem itemModem = RadComboBoxModem.FindItemByText(terminal.Modem.Trim());
                    itemModem.Selected = true;
                }
                if (terminal.iLNBBUC != null)
                {
                    RadComboBoxItem itemIlnbbuc = RadComboBoxILNBBUC.FindItemByText(terminal.iLNBBUC.Trim());
                    itemIlnbbuc.Selected = true;
                }
                if (terminal.Antenna != null)
                {
                    RadComboBoxItem itemAntenna = RadComboBoxAntenna.FindItemByText(terminal.Antenna.Trim());
                    itemAntenna.Selected = true;
                }
                if (HttpContext.Current.User.IsInRole("NOC Administrator") || HttpContext.Current.User.IsInRole("NOC Operator"))
                {
                    RadComboBoxModem.Enabled = true;
                    RadComboBoxILNBBUC.Enabled = true;
                    RadComboBoxAntenna.Enabled = true;
                }
                else
                {
                    RadComboBoxModem.Enabled = false;
                    RadComboBoxILNBBUC.Enabled = false;
                    RadComboBoxAntenna.Enabled = false;
                }
                if (terminal.DialogTechnology != null)
                {
                    RadComboBoxItem itemDialogTechnology = RadComboBoxDialogFrequency.FindItemByText(terminal.DialogTechnology.Trim());
                    itemDialogTechnology.Selected = true;
                }
                if (terminal.CenterFrequency != null)
                {
                    RadNumericCenterFrequency.Value = terminal.CenterFrequency;
                }
                if (terminal.SymbolRate != null)
                {
                    RadNumericTextBoxSymbolRate.Value = terminal.SymbolRate;
                }


                if (terminal.Remarks != null)
                {
                    TextBoxRemarks.Text = terminal.Remarks;
                }

                //Set the SitId and IspId to the terminal activity list control
                TermActivityList.IspId = terminal.IspId;
                TermActivityList.SitId = terminal.SitId;
                TermActivityList.Height = 800;

                //Set the MacAddress, IspId and FullName to the tools form
                TerminalTools.IspId = terminal.IspId;
                TerminalTools.MacAddress = terminal.MacAddress;
                TerminalTools.FullName = terminal.FullName;
                TerminalTools.ExtFullName = terminal.ExtFullName;
                TerminalTools.AdmStatus = terminal.AdmStatus.ToString();
            }
            else
            {
                RadAjaxPanel1.Enabled = false;
            }
        }

        /// <summary>
        /// Initializes the buttons depending on the status of the terminal
        /// </summary>
        /// <param name="term"></param>
        public void initTerminalManagementButtons(Terminal term)
        {
            switch (term.AdmStatus)
            {
                case 1:
                    //Locked
                    RadToolBarTerminal.Items[_activateIdx].Enabled = false;
                    RadToolBarTerminal.Items[_deleteIdx].Enabled = false;
                    RadToolBarTerminal.Items[_reactivateIdx].Enabled = true;
                    RadToolBarTerminal.Items[_suspendIdx].Enabled = false;
                    RadToolBarTerminal.Items[_decommissionIdx].Enabled = true;
                    RadToolBarTerminal.Items[_fupresetIdx].Enabled = false;
                    TextBoxSitId.Enabled = false;
                    break;

                case 2:
                    //Unlocked
                    RadToolBarTerminal.Items[_activateIdx].Enabled = false;
                    RadToolBarTerminal.Items[_deleteIdx].Enabled = false;
                    RadToolBarTerminal.Items[_reactivateIdx].Enabled = false;
                    RadToolBarTerminal.Items[_suspendIdx].Enabled = true;
                    RadToolBarTerminal.Items[_decommissionIdx].Enabled = false;
                    RadToolBarTerminal.Items[_fupresetIdx].Enabled = true;
                    TextBoxSitId.Enabled = false;
                    break;

                case 3:
                    //Decommissioned
                    RadToolBarTerminal.Items[_activateIdx].Enabled = false;
                    RadToolBarTerminal.Items[_deleteIdx].Enabled = true;
                    RadToolBarTerminal.Items[_reactivateIdx].Enabled = false;
                    RadToolBarTerminal.Items[_suspendIdx].Enabled = false;
                    RadToolBarTerminal.Items[_decommissionIdx].Enabled = false;
                    RadToolBarTerminal.Items[_fupresetIdx].Enabled = false;
                    TextBoxSitId.Enabled = false;
                    break;

                case 5:
                    //Request
                    RadToolBarTerminal.Items[_activateIdx].Enabled = true;
                    RadToolBarTerminal.Items[_deleteIdx].Enabled = true;
                    RadToolBarTerminal.Items[_reactivateIdx].Enabled = false;
                    RadToolBarTerminal.Items[_suspendIdx].Enabled = false;
                    RadToolBarTerminal.Items[_decommissionIdx].Enabled = false;
                    RadToolBarTerminal.Items[_fupresetIdx].Enabled = false;
                    RadToolBarTerminal.Items[2].Visible = false;
                    TextBoxSitId.Enabled = true;
                    TextBoxSitId.ForeColor = Color.DarkRed;
                    break;

                case 6:
                    //Deleted
                    RadToolBarTerminal.Items[_activateIdx].Enabled = false;
                    RadToolBarTerminal.Items[_deleteIdx].Enabled = false;
                    RadToolBarTerminal.Items[_reactivateIdx].Enabled = false;
                    RadToolBarTerminal.Items[_suspendIdx].Enabled = false;
                    RadToolBarTerminal.Items[_decommissionIdx].Enabled = false;
                    RadToolBarTerminal.Items[_fupresetIdx].Enabled = false;
                    TextBoxSitId.Enabled = false;
                    break;
            }
        }

        private bool SendActivationAcknowledgement(Terminal term)
        {
            bool result = false;
            string termMessage = "";

            termMessage = DateTime.Now.ToLongDateString() + " - " + " terminal activation completed.<br/>";
            termMessage = termMessage + "Your terminal with Mac Address: " + term.MacAddress + " has been activated.<br/>";
            termMessage = termMessage + "New SitId = " + term.SitId + "<br/><br/>";
            termMessage = termMessage + "<b>This is an automated mail, do not respond.</b></b></b>";
            termMessage = termMessage + "Your SatADSL support team!";

            //Get the Distributor E-Mail address
            if (term.DistributorId != null)
            {
                Distributor dist = _boAccountingControlWS.GetDistributor((int)term.DistributorId);
                if (dist != null)
                {
                    string[] distributorEmail = dist.Email.Split(';');

                    if (!_boLogControlWS.SendMail(termMessage, "Terminal Activation", distributorEmail))
                    {
                        this.logError("Could not send E-Mail to distributor: " + term.DistributorId, "Terminal Activation", "SendActivationAcknowledgment", 3);
                    }
                    else
                    {
                        result = true;
                    }

                    if (term.OrgId != null)
                    {
                        Organization org = _boAccountingControlWS.GetOrganization((int)term.OrgId);

                        if (org != null)
                        {
                            string[] organizationEmail = { org.Email };
                            if (!_boLogControlWS.SendMail(termMessage, "Terminal Activation", organizationEmail))
                            {
                                this.logError("Could not send E-Mail to organization: " + term.OrgId, "Terminal Activation", "SendActivationAcknowledgment", 3);
                            }
                        }
                        else
                        {
                            this.logError("Could not retrieve organization for id: " + term.OrgId,
                                        "Error while retrieving organization while activating terminal",
                                        "SendActivationAcknowledgment", 3);
                        }
                    }
                }
                else
                {
                    this.logError("Could not retrieve distributor for id: " + term.DistributorId,
                                        "Error while retrieving distributor while activating terminal",
                                        "SendActivationAcknowledgment", 3);
                    result = false;
                }
            }

            return result;
        }

        /// <summary>
        /// Logs a CMT Application Excpetion
        /// </summary>
        /// <param name="ex">Exception message</param>
        /// <param name="userdesc">User description of the message</param>
        /// <param name="stack">The exception stack trace</param>
        /// <param name="errLevel">The error level, default = 1</param>
        private void logError(string ex, string userdesc, string stack, short errLevel = 1)
        {
            CmtApplicationException cmtApplicationException = new CmtApplicationException();
            cmtApplicationException.ExceptionDateTime = DateTime.Now;
            cmtApplicationException.ExceptionDesc = ex;
            cmtApplicationException.ExceptionStacktrace = stack;
            cmtApplicationException.ExceptionLevel = errLevel;
            cmtApplicationException.UserDescription = userdesc;
            cmtApplicationException.StateInformation = "Error";
            _boLogControlWS.LogApplicationException(cmtApplicationException);
        }

        /// <summary>
        /// Creates a new task in the database
        /// </summary>
        /// <param name="task"></param>
        /// <param name="subject"></param>
        private void CreateNewTask(string task, string subject, Terminal term)
        {
            MembershipUser myObject = Membership.GetUser();
            string userId = myObject.ProviderUserKey.ToString();

            TaskType[] taskTypes = _boTaskControlWS.GetTaskTypes();
            TaskType currentTaskType = null;
            foreach (TaskType taskType in taskTypes)
            {
                if (taskType.Type.Equals(task))
                {
                    currentTaskType = taskType;
                    break;
                }
            }


            Task activateTask = new Task();
            activateTask.Completed = false;
            activateTask.DateCreated = DateTime.Now;
            activateTask.Type = currentTaskType;
            activateTask.DistributorId = (int)term.DistributorId;
            activateTask.DateDue = DateTime.Now.AddDays(1.0);
            activateTask.MacAddress = term.MacAddress;
            activateTask.Subject = subject;
            activateTask.UserId = new Guid(userId);


            //Add task to task list.
            if (!_boTaskControlWS.UpdateTask(activateTask))
            {
                CmtApplicationException cmtAppEx = new CmtApplicationException();
                cmtAppEx.ExceptionDateTime = DateTime.Now;
                cmtAppEx.ExceptionDesc = "Could not add " + task + " task to task table";
                cmtAppEx.StateInformation = "Adding task failed";
                cmtAppEx.UserDescription = "Check " + task + " of terminal and next update in Dashboard";
                _boLogControlWS.LogApplicationException(cmtAppEx);
            }
        }

        protected void RadXmlHttpPanelSLA_ServiceRequest(object sender, RadXmlHttpPanelEventArgs e)
        {
            int ispId = Int32.Parse(e.Value);
            ServiceLevel[] slaItems = _boAccountingControlWS.GetServicePacksByIsp(ispId);

           // RadComboBoxSLA.Items.Clear();
            RadComboBoxSLA.Items.Clear();
            RadComboBoxItem item;
            item = new RadComboBoxItem();
            item.Value = "";
            item.Text = "";
            RadComboBoxSLA.Items.Add(item);

            foreach (ServiceLevel slaItem in slaItems)
            {
                item = new RadComboBoxItem();
                item.Value = slaItem.SlaId.ToString();
                item.Text = slaItem.SlaName.Trim();
                RadComboBoxSLA.Items.Add(item);
            }

            RadComboBoxSLA.SelectedValue = "";
            RadComboBoxSLA.Text = "";


        }

        protected void RadXmlHttpPanelCustomer_ServiceRequest2(object sender, RadXmlHttpPanelEventArgs e)
        {
            string distId = e.Value.ToString();
            Organization[] orgItems = _boAccountingControlWS.GetOrganisationsByDistributorId(distId);

            RadComboBoxCustomer.Items.Clear();
            RadComboBoxCustomer.ClearSelection();
            RadComboBoxCustomer.DataSource = orgItems;
            RadComboBoxCustomer.DataTextField = "FullName";
            RadComboBoxCustomer.DataValueField = "Id";
            RadComboBoxCustomer.DataBind();

            if (!(orgItems.Length == 0))
            {
                RadComboBoxItem item = new RadComboBoxItem();
                item.Value = "0";
                item.Text = "";
                RadComboBoxCustomer.Items.Add(item);
                RadComboBoxCustomer.SortItems();
                RadComboBoxCustomer.SelectedValue = "0";
                RadComboBoxCustomer.Text = "";
            }
            else
            {
                RadComboBoxCustomer.Text = "No customers defined for this distributor";
                RadComboBoxCustomer.Enabled = false;
            }

            //Store the isp for successive pageloads
            this.Session["MulticastGroupDetails_Dist"] = distId;
        }

        protected void ButtonAssociatedTerminal_Click(object sender, EventArgs e)
        {
            string macAddress = Request.Params["mac"];

            if (macAddress != null)
            {
                //Get the terminal details
                Terminal terminal = (Terminal)Session["TerminalForm_Terminal"];
                Terminal associatedTerm = _boAccountingControlWS.GetTerminalDetailsByMAC(terminal.AssociatedMacAddress);

                //Load the associated terminal
                if (associatedTerm != null)
                {
                    this.Session["TerminalForm_Terminal"] = associatedTerm;
                    this.loadForm(associatedTerm);

                }
            }
        }
    }
}