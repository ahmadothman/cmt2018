﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FOWebApp.net.nimera.cmt.BOVoucherControllerWSRef;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using Telerik.Web.UI;
using System.Drawing;

namespace FOWebApp.Nocsa
{
    public partial class VoucherVolumeDetailsForm : System.Web.UI.UserControl
    {
        BOVoucherControllerWS _voucherController;
        BOAccountingControlWS _accountingController;
        VoucherVolume vv;
        
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void componentDataInit(int vvId)
        {
            _voucherController = new BOVoucherControllerWS();
            _accountingController = new BOAccountingControlWS();

            //Load in the volume voucher details
            vv = _voucherController.GetVoucherVolumeById(vvId);
            LabelId.Text = vv.Id.ToString();
            //Isp isp = _accountingController.GetISP(vv.Isp);
            //LabelISP.Text = isp.CompanyName;
            LabelValidityPeriod.Text = vv.ValidityPeriod.ToString();
            LabelVolumeMB.Text = vv.VolumeMB.ToString();
            RadTextBoxDescription.Text = vv.Description;
            ServiceLevel sl = _accountingController.GetServicePack(vv.Sla);
            if (sl != null)
                LabelSLA.Text = sl.SlaName;
            else
                LabelSLA.Text = "No SLA available for this voucher type";

            RadNumericTextBoxUnitPriceEUR.Value = vv.UnitPriceEUR;
            RadNumericTextBoxUnitPriceUSD.Value = vv.UnitPriceUSD;
        }

        protected void ButtonSubmit_Click(object sender, EventArgs e)
        {
            vv.Description = RadTextBoxDescription.Text;
            vv.UnitPriceEUR = (double)RadNumericTextBoxUnitPriceEUR.Value;
            vv.UnitPriceUSD = (double)RadNumericTextBoxUnitPriceUSD.Value;
            
            if (_voucherController.UpdateVoucherVolume(vv))
            {
                LabelSubmit.Text = "Voucher volume successfully added!";
                LabelSubmit.ForeColor = Color.Green;
            }
            else
            {
                LabelSubmit.Text = "Adding voucher volume failed!";
                LabelSubmit.ForeColor = Color.Red;
            }
        }

        protected void ButtonDelete_Click(object sender, EventArgs e) 
        {
            vv.Deleted = true;
            if (_voucherController.UpdateVoucherVolume(vv))
            {
                LabelSubmit.Text = "Voucher volume successfully deleted!";
                LabelSubmit.ForeColor = Color.Green;
            }
            else
            {
                LabelSubmit.Text = "Deleting voucher volume failed!";
                LabelSubmit.ForeColor = Color.Red;
            }
        }
    }
}