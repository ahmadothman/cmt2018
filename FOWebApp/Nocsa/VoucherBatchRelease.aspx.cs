﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using Telerik.Web.UI;
using FOWebApp.net.nimera.cmt.BOVoucherControllerWSRef;

namespace FOWebApp.Nocsa
{
    public partial class VoucherBatchRelease : System.Web.UI.Page
    {
        BOVoucherControllerWS _boVoucherControl = new BOVoucherControllerWS();

        protected void Page_Load(object sender, EventArgs e)
        {
            //Get BatchId parameter
            string batchId = Request.Params["bid"];
            LabelBatchId.Text = batchId;

            if (!IsPostBack)
            {
                VoucherBatch vb = _boVoucherControl.GetVoucherBatchInfo(Int32.Parse(batchId));
                CheckBoxRelease.Checked = vb.Release;

                if (vb.Release)
                {
                    RadButtonSubmit.Text = "Un-Release Voucher";
                }
                else
                {
                    RadButtonSubmit.Text = "Release Voucher";
                }

                CheckBoxPaid.Checked = vb.Paid;
                RadButtonSwitchPaid.Text = vb.Paid ? "Mark as unpaid" : "Mark as paid";
            }
        }

        protected void RadButtonSubmit_Click(object sender, EventArgs e)
        {
            if (RadButtonSubmit.Text.Equals("Release Voucher"))
            {
                //Release voucher batch
                if (_boVoucherControl.ReleaseVoucherBatch(Int32.Parse(LabelBatchId.Text)))
                {
                    LabelResult.ForeColor = Color.Green;
                    LabelResult.Text = "Release of voucher batch succeeded";
                    RadButtonSubmit.Text = "Un-Release Voucher";
                    CheckBoxRelease.Checked = true;
                }
                else
                {
                    LabelResult.ForeColor = Color.Red;
                    LabelResult.Text = "Release of voucher batch failed";
                }
            }
            else
            {
                //Unrelease voucher batch
                if (_boVoucherControl.UnreleaseVoucherBatch(Int32.Parse(LabelBatchId.Text)))
                {
                    LabelResult.ForeColor = Color.Green;
                    LabelResult.Text = "Un-Release of voucher batch succeeded";
                    RadButtonSubmit.Text = "Release Voucher";
                    CheckBoxRelease.Checked = false;
                }
                else
                {
                    LabelResult.ForeColor = Color.Red;
                    LabelResult.Text = "Un-Release of voucher batch failed";
                }
            }

        }

        protected void RadButtonSwitchPaid_Click(object sender, EventArgs e)
        {
            if (sender is RadButton)
            {
                RadButton switchPaidButton = (RadButton)sender;

                switch (switchPaidButton.Text)
                {
                    case "Mark as unpaid":

                        if (_boVoucherControl.FlagVoucherBatchAsUnPaid(int.Parse(LabelBatchId.Text)))
                        {
                            LabelResult.ForeColor = Color.Green;
                            LabelResult.Text = "Voucher batch successfully marked as unpaid";
                            RadButtonSwitchPaid.Text = "Mark as paid";
                            CheckBoxPaid.Checked = false;
                        }
                        else
                        {
                            LabelResult.ForeColor = Color.Red;
                            LabelResult.Text = "Marking the voucher batch as unpaid failed";
                        }

                        break;

                    case "Mark as paid":

                        if (_boVoucherControl.FlagVoucherBatchAsPaid(int.Parse(LabelBatchId.Text)))
                        {
                            LabelResult.ForeColor = Color.Green;
                            LabelResult.Text = "Voucher batch successfully marked as paid";
                            RadButtonSwitchPaid.Text = "Mark as unpaid";
                            CheckBoxPaid.Checked = true;
                        }
                        else
                        {
                            LabelResult.ForeColor = Color.Red;
                            LabelResult.Text = "Marking the voucher batch as paid failed";
                        }

                        break;
                } 
            }
        }

        protected void RadButton3_Click(object sender, EventArgs e)
        {
            int result = _boVoucherControl.DeleteVoucherBatch(Int32.Parse(LabelBatchId.Text));
            if (result==0)
            {
                LabelResult.ForeColor = Color.Green;
                LabelResult.Text = "Batch deleted successfuly";
            }
            else if (result==1)
            {
                LabelResult.ForeColor = Color.Red;
                LabelResult.Text = "Cannot delete batch, at least one voucher has been validated";
            }
            else if(result==2)
            {
                LabelResult.ForeColor = Color.Red;
                LabelResult.Text = "Cannot delete batch, batch already released";
            }
            else if (result == 3)
            {
                LabelResult.ForeColor = Color.Red;
                LabelResult.Text = "Cannot delete batch, batch already paid";
            }

        }
    }
}