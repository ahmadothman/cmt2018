﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using Telerik.Web.UI;
using FOWebApp.Data;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;

namespace FOWebApp.Nocsa
{
    public partial class CreateNewRedundantSetup : System.Web.UI.UserControl
    {
        DataHelper _dataHelper = null;
        BOAccountingControlWS _boAccountingControlWS = new BOAccountingControlWS();
        const string _HEXBYTE = "<0|1|2|3|4|5|6|7|8|9|a|b|c|d|e|f><0|1|2|3|4|5|6|7|8|9|a|b|c|d|e|f>";
        private const int _satDivISP = 900;

        public CreateNewRedundantSetup()
        {
            _dataHelper = new DataHelper();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Create a blank input form
            RadMaskedTextBoxMacAddress.Mask =
               _HEXBYTE + "<:>" + _HEXBYTE + "<:>" + _HEXBYTE + "<:>" + _HEXBYTE + "<:>" + _HEXBYTE + "<:>" +
               _HEXBYTE;
            RadMaskedTextBoxMacAddress2.Mask =
               _HEXBYTE + "<:>" + _HEXBYTE + "<:>" + _HEXBYTE + "<:>" + _HEXBYTE + "<:>" + _HEXBYTE + "<:>" +
               _HEXBYTE;
            RadMaskedTextBoxMacAddress3.Mask =
               _HEXBYTE + "<:>" + _HEXBYTE + "<:>" + _HEXBYTE + "<:>" + _HEXBYTE + "<:>" + _HEXBYTE + "<:>" +
               _HEXBYTE;

            TextBoxSitId.Text = "";
            TextBoxSerial.Text = "";
            TextBoxTerminalName.Text = "";
            TextBoxIP.Text = "";
            RadDatePickerStartDate.SelectedDate = DateTime.Now;
            RadDatePickerExpiryDate.SelectedDate = DateTime.Now.AddDays(30);

            LabelSitIdRequired.Text = "";

            //Fill the lookup comboboxes
            List<Isp> IspList = _dataHelper.getISPs();

            foreach (Isp isp in IspList)
            {
                RadComboBoxItem item = new RadComboBoxItem();
                item.Value = isp.Id.ToString();
                item.Text = isp.CompanyName;
                RadComboBoxISP.Items.Add(item);
            }
            AddInitialValueToComboBox(RadComboBoxISP);

            foreach (Isp isp in IspList)
            {
                RadComboBoxItem item = new RadComboBoxItem();
                item.Value = isp.Id.ToString();
                item.Text = isp.CompanyName;
                RadComboBoxISP2.Items.Add(item);
            }
            AddInitialValueToComboBox(RadComboBoxISP2);

            FreeZoneCMT[] freeZoneList = _boAccountingControlWS.GetFreeZones();

            foreach (FreeZoneCMT fz in freeZoneList)
            {
                RadComboBoxItem item = new RadComboBoxItem();
                item.Value = fz.Id.ToString();
                item.Text = fz.Name;
                RadComboBoxFreeZone.Items.Add(item);
            }


            //Fill-up the treeview in the RadComboBox

            RadTreeView distVNOTree = (RadTreeView)RadComboBoxDistributorVNO.Items[0].FindControl("RadTreeViewDistributorVNO");
            //Distributors
            RadTreeNode rtn = new RadTreeNode("Distributors", "0");
            rtn.ImageUrl = "~/Images/businesspeople.png";
            rtn.ToolTip = "Distributors";
            Distributor[] distributors = _boAccountingControlWS.GetDistributors();
            RadTreeNode distVNONode = null;
            foreach (Distributor dist in distributors)
            {
                distVNONode = new RadTreeNode(dist.FullName, dist.Id.ToString());
                distVNONode.ToolTip = dist.FullName;
                rtn.Nodes.Add(distVNONode);
            }
            distVNOTree.Nodes.Add(rtn);
            //VNOs
            rtn = new RadTreeNode("VNOs", "1");
            rtn.ImageUrl = "~/Images/magician.png";
            rtn.ToolTip = "VNOs";
            Distributor[] VNOs = _boAccountingControlWS.GetVNOs();
            distVNONode = null;
            foreach (Distributor vno in VNOs)
            {
                distVNONode = new RadTreeNode(vno.FullName, vno.Id.ToString());
                distVNONode.ToolTip = vno.FullName;
                rtn.Nodes.Add(distVNONode);
            }
            distVNOTree.Nodes.Add(rtn);

            //get the SLAs for the Sat Diversity ISP
            ServiceLevel[] satDivSLAs = _boAccountingControlWS.GetServicePacksByIsp(_satDivISP);
            if (!(satDivSLAs.Length == 0))
            {
                RadComboBoxSLA3.Items.Clear();

                RadComboBoxSLA3.DataSource = satDivSLAs;
                RadComboBoxSLA3.DataTextField = "SlaName";
                RadComboBoxSLA3.DataValueField = "SlaId";
                RadComboBoxSLA3.DataBind();

                AddInitialValueToComboBox(RadComboBoxSLA3);
            }
            else
            {
                RadComboBoxSLA3.Text = "No SLAs defined for this ISP or SLAs are hidden";
                RadComboBoxSLA3.Enabled = false;
            }

        }

        protected void CustomValidatorMacAddress_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (args.Value == "00:00:00:00:00:00")
            {
                args.IsValid = false;
            }
            else
            {
                args.IsValid = true;
            }
        }

        protected void ButtonSave_Click(object sender, EventArgs e)
        {
            //First check if the SitId is unique
            BOAccountingControlWS boAccountingControl =
                                        new BOAccountingControlWS();

            int ispIdPrimary = Int32.Parse(RadComboBoxISP.SelectedValue);
            int ispIdSecondary = Int32.Parse(RadComboBoxISP2.SelectedValue);
            
            try
            {
                this.saveTerminalData(ispIdPrimary, ispIdSecondary);
            }
            catch (Exception ex)
            {
                BOLogControlWS boLogControlWS = new BOLogControlWS();
                CmtApplicationException cmtApplicationException = new CmtApplicationException();
                cmtApplicationException.ExceptionDateTime = DateTime.Now;
                cmtApplicationException.ExceptionDesc = ex.Message;
                cmtApplicationException.ExceptionStacktrace = ex.StackTrace;
                cmtApplicationException.UserDescription = "Update new terminal";
                boLogControlWS.LogApplicationException(cmtApplicationException);

                LabelResult.ForeColor = Color.Red;
                LabelResult.Text = "Creation of terminal failed!";
            }

        }

        protected void RadXmlHttpPanelSLA_ServiceRequest(object sender, RadXmlHttpPanelEventArgs e)
        {
            int ispId = Int32.Parse(e.Value);
            ServiceLevel[] slaItems = _boAccountingControlWS.GetServicePacksByIsp(ispId);

            if (!(slaItems.Length == 0))
            {
                RadComboBoxSLA.Items.Clear();

                RadComboBoxSLA.DataSource = slaItems;
                RadComboBoxSLA.DataTextField = "SlaName";
                RadComboBoxSLA.DataValueField = "SlaId";
                RadComboBoxSLA.DataBind();

                AddInitialValueToComboBox(RadComboBoxSLA);
            }
            else
            {
                RadComboBoxSLA.Text = "No SLAs defined for this ISP or SLAs are hidden";
                RadComboBoxSLA.Enabled = false;
            }

            //Store the isp for successive pageloads
            //this.Session["CreateNewTerminal_ISP"] = ispId;
        }

        protected void RadXmlHttpPanelSLA2_ServiceRequest(object sender, RadXmlHttpPanelEventArgs e)
        {
            int ispId = Int32.Parse(e.Value);
            ServiceLevel[] slaItems = _boAccountingControlWS.GetServicePacksByIsp(ispId);

            if (!(slaItems.Length == 0))
            {
                RadComboBoxSLA2.Items.Clear();

                RadComboBoxSLA2.DataSource = slaItems;
                RadComboBoxSLA2.DataTextField = "SlaName";
                RadComboBoxSLA2.DataValueField = "SlaId";
                RadComboBoxSLA2.DataBind();

                AddInitialValueToComboBox(RadComboBoxSLA2);
            }
            else
            {
                RadComboBoxSLA2.Text = "No SLAs defined for this ISP or SLAs are hidden";
                RadComboBoxSLA2.Enabled = false;
            }

            //Store the isp for successive pageloads
            //this.Session["CreateNewTerminal_ISP"] = ispId;
        }

        private void saveTerminalData(int ispIdPrimary, int ispIdSecondary)
        {
            //First check if the SitId is unique
            BOAccountingControlWS boAccountingControl =
                                        new BOAccountingControlWS();

            //Primary terminal
            //Check if the MAC address exists
            Terminal terminalP = boAccountingControl.GetTerminalDetailsByMAC(RadMaskedTextBoxMacAddress.Text);
            if (terminalP != null)
            {
                LabelResult.ForeColor = Color.Red;
                LabelResult.Text = "MAC address already exists";
                return;
            }

            //Update data into the database
            terminalP = new Terminal();
            terminalP.Address = new Address();

            terminalP.MacAddress = RadMaskedTextBoxMacAddress.Text;
            terminalP.IspId = ispIdPrimary;
            terminalP.SitId = 0;
            terminalP.Serial = TextBoxSerial.Text;
            terminalP.FullName = TextBoxTerminalName.Text.Trim();
            terminalP.ExtFullName = string.Concat(terminalP.MacAddress + terminalP.Serial).Replace(":", "-");
            terminalP.DistributorId = Int32.Parse(RadComboBoxDistributorVNO.SelectedValue);
            terminalP.SlaId = Int32.Parse(RadComboBoxSLA.SelectedValue);
            terminalP.IpAddress = TextBoxIP.Text;
            terminalP.StartDate = RadDatePickerStartDate.SelectedDate;
            terminalP.ExpiryDate = RadDatePickerExpiryDate.SelectedDate;
            terminalP.AdmStatus = 5;
            terminalP.TestMode = CheckBoxTestMode.Checked;
            terminalP.CNo = (decimal)-1.0;
            terminalP.IPMask = Int32.Parse(RadComboBoxIPMask.SelectedValue);
            terminalP.IPRange = CheckBoxIPRange.Checked;
            terminalP.TermWeight = 100; //default weight for all new terminals as setting weights is only done on customer VNO dashboard
            terminalP.StaticIPFlag = CheckBoxStaticIP.Checked;
            ServiceLevel sla = _boAccountingControlWS.GetServicePack((int)terminalP.SlaId);
            terminalP.FreeZone = 0;
            if (sla.FreeZone != null)
            {
                if ((bool)sla.FreeZone)
                {
                    terminalP.FreeZone = Int32.Parse(RadComboBoxFreeZone.SelectedValue);
                }
            }
            
            //set the Satellite diversity properties
            terminalP.RedundantSetup = true;
            terminalP.PrimaryTerminal = true;
            terminalP.AssociatedMacAddress = RadMaskedTextBoxMacAddress2.Text;
            terminalP.VirtualTerminal = RadMaskedTextBoxMacAddress3.Text;

            // Sets the selected value of the RadComboBox of the ISPs back to the initial value with empty text
            // That way, the information shown in the SLA Combobox is also correct after pressing the button and going to the server
            RadComboBoxISP.SelectedIndex = 0;

            //Create the secondary terminal
            Terminal terminalS = boAccountingControl.GetTerminalDetailsByMAC(RadMaskedTextBoxMacAddress2.Text);
            if (terminalS != null)
            {
                LabelResult.ForeColor = Color.Red;
                LabelResult.Text = "MAC address already exists";
                return;
            }

            //Update data into the database
            terminalS = new Terminal();
            terminalS.Address = new Address();

            terminalS.MacAddress = RadMaskedTextBoxMacAddress2.Text;
            terminalS.IspId = ispIdSecondary;
            terminalS.SitId = 0;
            terminalS.Serial = TextBoxSerial2.Text;
            terminalS.FullName = TextBoxTerminalName2.Text.Trim();
            terminalS.ExtFullName = string.Concat(terminalS.MacAddress + terminalS.Serial).Replace(":", "-");
            terminalS.DistributorId = Int32.Parse(RadComboBoxDistributorVNO.SelectedValue);
            terminalS.SlaId = Int32.Parse(RadComboBoxSLA2.SelectedValue);
            terminalS.IpAddress = TextBoxIP2.Text;
            terminalS.StartDate = RadDatePickerStartDate.SelectedDate;
            terminalS.ExpiryDate = RadDatePickerExpiryDate.SelectedDate;
            terminalS.AdmStatus = 5;
            terminalS.TestMode = CheckBoxTestMode.Checked;
            terminalS.CNo = (decimal)-1.0;
            terminalS.IPMask = Int32.Parse(RadComboBoxIPMask2.SelectedValue);
            terminalS.IPRange = CheckBoxIPRange2.Checked;
            terminalS.TermWeight = 100; //default weight for all new terminals as setting weights is only done on customer VNO dashboard
            terminalS.StaticIPFlag = CheckBoxStaticIP2.Checked;
            ServiceLevel sla2 = _boAccountingControlWS.GetServicePack((int)terminalS.SlaId);
            terminalS.FreeZone = 0; 
            if (sla2.FreeZone != null)
            {
                if ((bool)sla2.FreeZone)
                {
                    terminalS.FreeZone = Int32.Parse(RadComboBoxFreeZone.SelectedValue);
                }
            }
            
            //set the Satellite diversity properties
            terminalS.RedundantSetup = true;
            terminalS.PrimaryTerminal = false;
            terminalS.AssociatedMacAddress = RadMaskedTextBoxMacAddress.Text;
            terminalS.VirtualTerminal = RadMaskedTextBoxMacAddress3.Text;

            // Sets the selected value of the RadComboBox of the ISPs back to the initial value with empty text
            // That way, the information shown in the SLA Combobox is also correct after pressing the button and going to the server
            RadComboBoxISP2.SelectedIndex = 0;

            //create the virtual terminal
            Terminal terminalV = boAccountingControl.GetTerminalDetailsByMAC(RadMaskedTextBoxMacAddress3.Text);
            if (terminalV != null)
            {
                LabelResult.ForeColor = Color.Red;
                LabelResult.Text = "MAC address already exists";
                return;
            }

            //Update data into the database
            terminalV = new Terminal();
            terminalV.Address = new Address();

            terminalV.MacAddress = RadMaskedTextBoxMacAddress3.Text;
            terminalV.IspId = _satDivISP;
            terminalV.SitId = 0;
            terminalV.Serial = "123";
            terminalV.FullName = "SatDiv terminal - " + terminalV.MacAddress.Replace(":","");
            terminalV.ExtFullName = string.Concat(terminalV.MacAddress + terminalV.Serial).Replace(":", "-");
            terminalV.DistributorId = Int32.Parse(RadComboBoxDistributorVNO.SelectedValue);
            terminalV.SlaId = Int32.Parse(RadComboBoxSLA3.SelectedValue);
            terminalV.IpAddress = TextBoxIP3.Text;
            terminalV.StartDate = RadDatePickerStartDate.SelectedDate;
            terminalV.ExpiryDate = RadDatePickerExpiryDate.SelectedDate;
            terminalV.AdmStatus = 5;
            terminalV.TestMode = CheckBoxTestMode.Checked;
            terminalV.CNo = (decimal)-1.0;
            terminalV.IPMask = 32;
            terminalV.IPRange = false;
            terminalV.TermWeight = 100; //default weight for all new terminals as setting weights is only done on customer VNO dashboard
            terminalV.StaticIPFlag = null;
            ServiceLevel sla3 = _boAccountingControlWS.GetServicePack((int)terminalV.SlaId);
            terminalV.FreeZone = 0;
            if (sla3.FreeZone != null)
            {
                if ((bool)sla3.FreeZone)
                {
                    terminalV.FreeZone = Int32.Parse(RadComboBoxFreeZone.SelectedValue);
                }
            }

            //set the Satellite diversity properties
            terminalV.RedundantSetup = true;
            terminalV.PrimaryTerminal = false;
            terminalV.AssociatedMacAddress = RadMaskedTextBoxMacAddress.Text;
            
            //store the primary terminal
            if (boAccountingControl.UpdateTerminal(terminalP))
            {
                //store the secondary terminal
                if (boAccountingControl.UpdateTerminal(terminalS))
                {
                    if (boAccountingControl.UpdateTerminal(terminalV))
                    {
                        //Reset the session variable
                        //this.Session["CreateNewTerminal_ISP"] = null;
                        LabelResult.ForeColor = Color.DarkGreen;
                        LabelResult.Text = "Creation of terminal succeeded, if necessary you may now activate the terminal.";
                    }
                    else
                    {
                        LabelResult.ForeColor = Color.DarkRed;
                        LabelResult.Text = "We are sorry but the creation of terminal failed, please contact SatADSL";
                    }
                }
                else
                {
                    LabelResult.ForeColor = Color.DarkRed;
                    LabelResult.Text = "We are sorry but the creation of terminal failed, please contact SatADSL";
                }
            }
            else
            {
                LabelResult.ForeColor = Color.DarkRed;
                LabelResult.Text = "We are sorry but the creation of terminal failed, please contact SatADSL";
            }
        }

        /// <summary>
        /// Inserts a RadComboBoxItem with an emtpy string as text and 0 as value in the RadComboBox at index 0
        /// </summary>
        /// <param name="comboBox">The RadComboBox in which to insert the RadComboBoxItem</param>
        private void AddInitialValueToComboBox(RadComboBox comboBox)
        {
            RadComboBoxItem item = new RadComboBoxItem(string.Empty, "0");
            comboBox.Items.Insert(0, item);
        }
    }
}