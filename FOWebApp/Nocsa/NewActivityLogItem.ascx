﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NewActivityLogItem.ascx.cs"
    Inherits="FOWebApp.Nocsa.NewActivityLogItem" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy2" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="RadButtonSubmit">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="LabelStatus" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<asp:HiddenField ID="HiddenFieldSLAId" runat="server" ClientIDMode="Static" />
<asp:HiddenField ID="HiddenFieldNewSlAId" runat="server" ClientIDMode="Static" />
<asp:HiddenField ID="HiddenFieldActivityId" runat ="server" ClientIDMode="Static" />
<h2>
    <asp:Label ID="LabelTitle" runat="server"></asp:Label>
</h2>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="RadButtonSubmit">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="LabelStatus" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<div id="ActivityDiv">
<table cellpadding="10" cellspacing="5">
    <tr>
        <td>
            Terminal MAC address:
        </td>
        <td>
            <telerik:RadMaskedTextBox ID="RadMaskedTextBoxMacAddress" runat="server" Skin="Metro"
                Width="125px">
            </telerik:RadMaskedTextBox>
            <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Please provide a valid MAC address!"
                ControlToValidate="RadMaskedTextBoxMacAddress" Operator="NotEqual" ValueToCompare="00:00:00:00:00:00"></asp:CompareValidator>
        </td>
    </tr>
    <tr>
        <td>
            Service Level Agreement:
        </td>
        <td>
            <telerik:RadComboBox ID="RadComboBoxSLA" runat="server" EmptyMessage="Select a Service Pack" Width="250px" Skin="Metro">
            </telerik:RadComboBox>
        </td>
    </tr>
    <tr>
        <td>
            New Service Level Agreement:
        </td>
        <td>
            <telerik:RadComboBox ID="RadComboBoxNewSLA" runat="server" EmptyMessage="Select a Service Pack" Width="250px" Skin="Metro">
            </telerik:RadComboBox>
        </td>
    </tr>
    <tr>
        <td>
            Activity Date:
        </td>
        <td>
            <telerik:RadDatePicker ID="RadDatePickerTimeStamp" runat="server" Skin="Metro" >
                <DateInput ID="DateInputRadDatePickerTimeStamp" DisplayDateFormat="dd/MM/yyyy" DateFormat="d/MM/yyyy" runat="server"/>
            </telerik:RadDatePicker>
        </td>
    </tr>
    <tr>
        <td>
            Terminal Activity:
        </td>
        <td>
            <%--<telerik:RadComboBox ID="RadComboBoxTerminalActivity" runat="server" DataTextField="Action"
                DataValueField="Id" Skin="Metro">
            </telerik:RadComboBox>--%>
            <telerik:RadComboBox ID="RadComboBoxTerminalActivity" runat="server" Skin="Metro">
            </telerik:RadComboBox>
        </td>
    </tr>
    <tr>
        <td style="vertical-align: top">
            Comment
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxComment" runat="server" EmptyMessage="Additional information" Columns="100" Rows="5" Skin="Metro" TextMode="MultiLine"></telerik:RadTextBox>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <asp:CheckBox ID="CheckBoxAccountingFlag" runat="server" Text="Include in accounting report:" TextAlign="Left" ToolTip="Check if you want this activity to be part of the accounting report" />
        </td>
    </tr>
    <tr>
        <td>
            <telerik:RadButton ID="RadButtonSubmit" runat="server" Text="Submit" OnClick="RadButtonSubmit_Click" Skin="Metro">
            </telerik:RadButton>
        </td>
        <td>
            <asp:Label ID="LabelStatus" runat="server"></asp:Label>
        </td>
    </tr>
</table>
</div>