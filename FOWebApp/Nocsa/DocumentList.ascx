﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DocumentList.ascx.cs" Inherits="FOWebApp.Nocsa.DocumentList" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<h1>Documents</h1>
<label runat="server" id="labelDeleteMessage" style="color:green"></label>
<telerik:RadGrid ID="RadGridDocuments" runat="server" OnNeedDataSource="RadGridDocuments_NeedDataSource" AutoGenerateColumns="False" CellSpacing="0" GridLines="None" OnDeleteCommand="RadGridDocuments_DeleteCommand" ShowStatusBar="True" Skin="Metro">
    <ExportSettings>
        <Pdf PageWidth="">
        </Pdf>
    </ExportSettings>
    <MasterTableView CommandItemDisplay="Top" CommandItemSettings-ShowAddNewRecordButton="False" DataKeyNames="Id" EnableHeaderContextMenu="True" NoMasterRecordsText="No documents available">
        <RowIndicatorColumn Visible="False">
        </RowIndicatorColumn>
        <ExpandCollapseColumn Created="True">
        </ExpandCollapseColumn>
        <Columns>
            <telerik:GridBoundColumn DataField="Id" DataType="System.Int32" FilterControlAltText="Filter DocId column" HeaderText="Id" UniqueName="DocId">
            </telerik:GridBoundColumn>
            <telerik:GridHyperLinkColumn AllowSorting="False" DataTextField="Name" FilterControlAltText="Filter Name column" HeaderText="Document Name" UniqueName="Name" DataNavigateUrlFields="Name" DataNavigateUrlFormatString="~/Servlets/DocumentDownload.aspx?DocumentName={0}">
            </telerik:GridHyperLinkColumn>
            <telerik:GridBoundColumn DataField="Description" FilterControlAltText="Filter column column" HeaderText="Description" UniqueName="column">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="DateUploaded" DataType="System.DateTime" FilterControlAltText="Filter DateUploaded column" HeaderText="Date Uploaded" UniqueName="DateUploaded">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="RecipientFullName" FilterControlAltText="Filter RecipientFullName column" HeaderText="Recipient" UniqueName="RecipientFullName">
            </telerik:GridBoundColumn>
            <telerik:GridButtonColumn HeaderText="" ImageUrl="~/Images/delete.png" ConfirmText="Are you sure you want to delete this document?"
                UniqueName="DeleteColumn" ButtonType="ImageButton" HeaderButtonType="PushButton"
                Text="Delete" CommandName="Delete">
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </telerik:GridButtonColumn>
        </Columns>
    </MasterTableView>
</telerik:RadGrid>