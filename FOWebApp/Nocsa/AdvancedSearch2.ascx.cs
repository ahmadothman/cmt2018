﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;
using System.Drawing;

namespace FOWebApp.Nocsa
{
    public partial class AdvancedSearch2 : System.Web.UI.UserControl
    {
        BOAccountingControlWS _boAccountingControl;

        public AdvancedSearch2()
        {
            _boAccountingControl = new BOAccountingControlWS();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Distributor[] distList = _boAccountingControl.GetDistributorsAndVNOs().Where(x => x.Inactive == false).ToArray();
            RadComboBoxDistributor.DataValueField = "Id";
            RadComboBoxDistributor.DataTextField = "FullName";
            RadComboBoxDistributor.DataSource = distList;
            RadComboBoxDistributor.DataBind();
            RadComboBoxDistributor.Items.Insert(0, new RadComboBoxItem(""));

            TerminalStatus[] termStatusList = _boAccountingControl.GetTerminalStatus();
            RadComboBoxAdminState.DataValueField = "Id";
            RadComboBoxAdminState.DataTextField = "StatusDesc";
            RadComboBoxAdminState.DataSource = termStatusList;
            RadComboBoxAdminState.DataBind();
            RadComboBoxAdminState.Items.Insert(0, new RadComboBoxItem(""));

            ServiceLevel[] servicePacks = _boAccountingControl.GetServicePacks();
            RadComboBoxSLA.DataValueField = "SlaId";
            RadComboBoxSLA.DataTextField = "SlaName";
            RadComboBoxSLA.DataSource = servicePacks;
            RadComboBoxSLA.DataBind();
            RadComboBoxSLA.Items.Insert(0, new RadComboBoxItem(""));

            RadTreeView ISPSatelliteTree = (RadTreeView)RadComboBoxISP.Items[0].FindControl("RadTreeViewSatellite");
            Satellite[] satellites = _boAccountingControl.GetSatellites();
            foreach (Satellite sat in satellites)
            {

                RadTreeNode rtn = new RadTreeNode(sat.SatelliteName, sat.ID.ToString());
                rtn.ToolTip = sat.SatelliteName;
                Isp[] ISPList = _boAccountingControl.GetISPsBySatellite(sat.ID);
                RadTreeNode ISPNode = null;
                bool satHasISP = false;
                foreach (Isp isp in ISPList)
                {
                    ISPNode = new RadTreeNode(isp.CompanyName, isp.Id.ToString());
                    ISPNode.ToolTip = isp.CompanyName;
                    rtn.Nodes.Add(ISPNode);
                    satHasISP = true;
                }
                if (satHasISP != true)
                {
                    ISPNode = new RadTreeNode("Satellite has no ISPs", "");
                    ISPNode.ToolTip = "Satellite has no ISPs";
                    ISPNode.Enabled = false;
                    rtn.Nodes.Add(ISPNode);
                }
                ISPSatelliteTree.Nodes.Add(rtn);
            }
        }

        protected void RadXmlHttpPanelSla_ServiceRequest(object sender, RadXmlHttpPanelEventArgs e)
        {
            ServiceLevel[] servicePacks = (string.IsNullOrEmpty(e.Value)) ?
                _boAccountingControl.GetServicePacks() : _boAccountingControl.GetServicePacksByIspWithHide(int.Parse(e.Value));

            RadComboBoxSLA.DataValueField = "SlaId";
            RadComboBoxSLA.DataTextField = "SlaName";
            RadComboBoxSLA.DataSource = servicePacks;
            RadComboBoxSLA.DataBind();
            RadComboBoxSLA.Items.Insert(0, new RadComboBoxItem(""));
        }

        protected void generateList()
        {
            RadTreeView ISPSatelliteTree = (RadTreeView)RadComboBoxISP.Items[0].FindControl("RadTreeViewSatellite");
            //Retrieve the different parameters and call the Advanced Query method
            AdvancedQueryParams advQP = new AdvancedQueryParams();

            if (RadComboBoxISP.Text != "" && RadComboBoxISP.Text != "Satellite has no ISPs")
            {
                if (ISPSatelliteTree.SelectedNode.Level == 0)
                {
                    advQP.SatelliteId = Int32.Parse(ISPSatelliteTree.SelectedNode.Value);
                    advQP.IspId = -1;
                }
                else if (ISPSatelliteTree.SelectedNode.Level == 1)
                {
                    advQP.IspId = Int32.Parse(ISPSatelliteTree.SelectedNode.Value);
                    advQP.SatelliteId = -1;
                }
            }
            else
            {
                advQP.IspId = -1;
                advQP.SatelliteId = -1;
            }


            if (RadComboBoxAdminState.SelectedValue != "")
                advQP.AdmState = Int32.Parse(RadComboBoxAdminState.SelectedValue);
            else
                advQP.AdmState = -1; //Means no value

            if (RadComboBoxDistributor.SelectedValue != "")
                advQP.DistributorId = Int32.Parse(RadComboBoxDistributor.SelectedValue);
            else
                advQP.DistributorId = -1;

            if (RadDatePickerExpiryDate.SelectedDate != null)
                advQP.ExpDate = (DateTime)RadDatePickerExpiryDate.SelectedDate;

            advQP.FullName = RadTextBoxFullName.Text;
            advQP.EMail = RadTextBoxEMail.Text;
            advQP.Serial = RadTextBoxSerial.Text;

            if (RadMaskedTextBoxFWDPool.Text != "")
                advQP.FwdPool = Int32.Parse(RadMaskedTextBoxFWDPool.Text);
            else
                advQP.FwdPool = -1;

            if (RadMaskedTextBoxRTNPool.Text != "")
                advQP.RtnPool = Int32.Parse(RadMaskedTextBoxRTNPool.Text);
            else
                advQP.RtnPool = -1;

            if (RadMaskedTextBoxSitId.Text != "")
                advQP.SitId = Int32.Parse(RadMaskedTextBoxSitId.Text);
            else
                advQP.SitId = -1;

            if (RadComboBoxSLA.SelectedValue != "")
                advQP.SlaId = Int32.Parse(RadComboBoxSLA.SelectedValue);
            else
                advQP.SlaId = -1;

            advQP.FreeZone = CheckBoxFreeZone.Checked;

            advQP.IpAddress = RadTextBoxIPAddress.Text;
            advQP.MacAddress = RadTextBoxMacAddress.Text;

            advQP.TestTerminal = CheckBoxTestTerminal.Checked;
            advQP.StaticIPFlag = CheckBoxStaticIP.Checked;

            //SatDiversity Redundant Setup Flag
            advQP.RedundantSetup = CheckBoxRedundantSetup.Checked;

            advQP.ExpDateOper = RadComboBoxExpiryDate.SelectedValue;
            Terminal[] terms = _boAccountingControl.GetTerminalsWithAdvancedQuery(advQP);

            if (terms.Length > 0)
            {
                RadGridTerminals.Visible = true;
                RadGridTerminals.DataSource = terms;
                RadGridTerminals.DataBind();
                LabelNumTerminals.Text = terms.Length.ToString();
                LabelNumberOfTerminals.Visible = true;
                LabelNumTerminals.Visible = true;
                //RadGridTerminals.MasterTableView.Caption = "Number of terminals: " + terms.Length;
            }
            else
            {
                LabelNumTerminals.Text = "0";
                LabelNumberOfTerminals.Visible = true;
                LabelNumTerminals.Visible = true;
                RadGridTerminals.Visible = false;
            }
        }

        protected void RadButtonQuery_Click(object sender, EventArgs e)
        {
            this.generateList();
        }
        //CMTFO-93
        protected void ImageButtonExport_Click(object sender, ImageClickEventArgs e)
        {
            this.generateList();

            ImageButton button = sender as ImageButton;
            if (RadGridTerminals.Visible == true)
            {
                if (button.ID == "ImageButtonExcelExport")
                {
                    RadGridTerminals.MasterTableView.ExportToExcel();
                }
            }
        }

        protected void RadGridTerminals_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem dataItem = (GridDataItem)e.Item;

                switch (dataItem["AdmStatus"].Text)
                {
                    case "1":
                        dataItem["AdmStatus"].Text = "Locked";
                        dataItem["AdmStatus"].ForeColor = Color.Red;
                        dataItem["MacAddress"].ForeColor = Color.Red;
                        break;
                    case "2":
                        dataItem["AdmStatus"].Text = "Unlocked";
                        dataItem["AdmStatus"].ForeColor = Color.Green;
                        dataItem["MacAddress"].ForeColor = GetMacAddressColor(dataItem["CNo"].Text);
                        break;
                    case "3":
                        dataItem["AdmStatus"].Text = "Decommissioned";
                        dataItem["AdmStatus"].ForeColor = Color.Blue;
                        dataItem["MacAddress"].ForeColor = Color.Red;
                        break;
                    case "4":
                        break;
                    case "5":
                        dataItem["AdmStatus"].Text = "Request";
                        dataItem["AdmStatus"].ForeColor = Color.DarkOrange;
                        dataItem["MacAddress"].ForeColor = GetMacAddressColor(dataItem["CNo"].Text);
                        break;
                    case "6":
                        dataItem["AdmStatus"].Text = "Deleted";
                        dataItem["AdmStatus"].ForeColor = Color.Black;
                        break;
                    default:
                        break;
                }

                string slaId = dataItem["ServiceColumn"].Text;
                if (!string.IsNullOrEmpty(slaId))
                {
                    try
                    {
                        BOAccountingControlWS boAccountingControl = new net.nimera.cmt.BOAccountingControlWSRef.BOAccountingControlWS();
                        ServiceLevel serviceLevel = boAccountingControl.GetServicePack(int.Parse(slaId));
                        dataItem["ServiceColumn"].Text = serviceLevel.SlaName;
                        dataItem["CNo"].ForeColor = GetCNoColor(Convert.ToDouble(dataItem["CNo"].Text), serviceLevel.DRRTN);
                    }
                    catch (Exception ex)
                    {

                        BOLogControlWS logController = new BOLogControlWS();
                        CmtApplicationException cmtAppEx = new CmtApplicationException
                        {
                            ExceptionDateTime = DateTime.UtcNow,
                            ExceptionDesc = ex.Message,
                            ExceptionStacktrace = ex.StackTrace,
                            ExceptionLevel = 2,
                            UserDescription = "AdvancedSearch2: The Service Pack Could not be found"
                        };
                        logController.LogApplicationException(cmtAppEx);
                    }
                }
            }
        }
        protected Color GetMacAddressColor(string CNo)
        {
            if (CNo.Equals("-1.00") || CNo.Equals("") || CNo.Equals("-1,00"))
            {
                return Color.Red;
            }
            else
            {
                return Color.Green;
            }
        }

        protected Color GetCNoColor(double CNo, int slaDrrtn)
        {
            if (slaDrrtn <= 256) //In later stage this should be customized according to Satellite (Provider)

            {
                if (CNo < 52.71)
                {
                    return Color.Red;
                }
                else if (CNo < 59.64)
                {
                    return Color.DarkOrange;
                }
                else
                {
                    return Color.DarkGreen;
                }
            }
            else
            {
                if (CNo < 58.73)
                {
                    return Color.Red;
                }
                else if (CNo < 61.74)
                {
                    return Color.DarkOrange;
                }
                else
                {
                    return Color.DarkGreen;
                }
            }
        }
    }
}