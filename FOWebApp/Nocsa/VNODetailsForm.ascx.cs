﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOServicesControllerWSRef;
using Telerik.Web.UI;

namespace FOWebApp.Nocsa
{
    public partial class VNODetailsForm : System.Web.UI.UserControl
    {
        BOAccountingControlWS _boAccountingControl = new BOAccountingControlWS();

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public void componentDataInit(int VNOId)
        {
            //Get the Distributor information an fill up the form
            BOAccountingControlWS boAccountingControl =
              new net.nimera.cmt.BOAccountingControlWSRef.BOAccountingControlWS();

            //Get the Distributor linked to the user
            Distributor dist = boAccountingControl.GetDistributor(VNOId);

            if (dist != null)
            {
                TextBoxAddressLine1.Text = dist.Address.AddressLine1;
                TextBoxAddressLine2.Text = dist.Address.AddressLine2;
                //TextBoxCountry.Text = dist.Address.Country;
                CountryList.SelectedValue = dist.Address.Country;
                TextBoxVNOName.Text = dist.FullName;
                TextBoxEMail.Text = dist.Email;
                TextBoxFax.Text = dist.Fax;
                TextBoxLocation.Text = dist.Address.Location;
                TextBoxPCO.Text = dist.Address.PostalCode;
                TextBoxPhone.Text = dist.Phone;
                TextBoxRemarks.Text = dist.Remarks;
                TextBoxVAT.Text = dist.Vat;
                if (dist.Currency == "EUR")
                {
                    CheckBoxCurrency.Checked = true;
                }
                TextBoxWebSite.Text = dist.WebSite;
                LabelVNOId.Text = "" + dist.Id;
                PanelISPAllocation.Visible = true;
                RadListBoxIsp.Visible = true;

                //Load the ISP Checkbox list
                RadListBoxItem listItem;
                Isp[] ispList = _boAccountingControl.GetIsps();
                foreach (Isp isp in ispList)
                {
                    listItem = new RadListBoxItem(isp.CompanyName, isp.Id.ToString());
                    RadListBoxIsp.Items.Add(listItem);
                }

                //Check the ISPs allocated to this VNO
                Isp[] ispListForDistributor = _boAccountingControl.GetISPsForDistributor(VNOId);

                foreach (Isp isp in ispListForDistributor)
                {
                    for (int i = 0; i < RadListBoxIsp.Items.Count; i++)
                    {
                        if (!RadListBoxIsp.Items[i].Checked && RadListBoxIsp.Items[i].Value.Equals(isp.Id.ToString()))
                        {
                            RadListBoxIsp.Items[i].Checked = true;
                        }
                    }
                }

                //Load the Services Checkbox list
                PanelServicesAllocation.Visible = false;
                RadListBoxServices.Visible = false;
                BOServicesControllerWS boServiceControl = new BOServicesControllerWS();
                Service[] serviceList1 = boServiceControl.GetAllAvailableServices("1");
                foreach (Service serv1 in serviceList1)
                {
                    listItem = new RadListBoxItem(serv1.ServiceName, serv1.ServiceId.ToString());
                    RadListBoxServices.Items.Add(listItem);
                    Service[] serviceList2 = boServiceControl.GetChildren(serv1);
                    if (serviceList2.Length > 0)
                    {
                        foreach (Service serv2 in serviceList2)
                        {
                            listItem = new RadListBoxItem("---" + serv2.ServiceName, serv2.ServiceId.ToString());
                            RadListBoxServices.Items.Add(listItem);
                            Service[] serviceList3 = boServiceControl.GetChildren(serv2);
                            if (serviceList3.Length > 0)
                            {
                                foreach (Service serv3 in serviceList3)
                                {
                                    listItem = new RadListBoxItem("------" + serv3.ServiceName, serv3.ServiceId.ToString());
                                    RadListBoxServices.Items.Add(listItem);
                                }
                            }
                        }
                    }
                }

                //Check the Services allocated to this VNO
                Service[] services = boServiceControl.GetServicesForVNO(VNOId);

                foreach (Service service in services)
                {
                    for (int i = 0; i < RadListBoxServices.Items.Count; i++)
                    {
                        if (!RadListBoxServices.Items[i].Checked && RadListBoxServices.Items[i].Value.Equals(service.ServiceId.ToString()))
                        {
                            RadListBoxServices.Items[i].Checked = true;
                        }
                    }
                }
            }
        }

        //recursive function that adds attribute to all child controls 
        private void addBlurAtt(Control cntrl)
        {
            if (cntrl.Controls.Count > 0)
            {
                foreach (Control childControl in cntrl.Controls)
                {
                    addBlurAtt(childControl);
                }
            }
            if (cntrl.GetType() == typeof(TextBox))
            {
                TextBox TempTextBox = (TextBox)cntrl;
                //TempTextBox.Attributes.Add("onFocus", "DoFocus(this);");
                //TempTextBox.Attributes.Add("onBlur", "DoBlur(this);");
                TempTextBox.Attributes.Add("onchange", "DoChange(this);");
            }
            if (cntrl.GetType() == typeof(ListBox))
            {
                ListBox TempTextBox = (ListBox)cntrl;
                //TempTextBox.Attributes.Add("onFocus", "DoFocus(this);");
                //TempTextBox.Attributes.Add("onBlur", "DoBlur(this);");
                TempTextBox.Attributes.Add("onchange", "DoChange(this);");
            }
            if (cntrl.GetType() == typeof(DropDownList))
            {
                DropDownList TempTextBox = (DropDownList)cntrl;
                //TempTextBox.Attributes.Add("onFocus", "DoFocus(this);");
                //TempTextBox.Attributes.Add("onBlur", "DoBlur(this);");
                TempTextBox.Attributes.Add("onchange", "DoChange(this);");
            }
        }

        protected void ButtonSubmit_Click(object sender, EventArgs e)
        {
            //Submit the data to the database
            Distributor dist = new Distributor();
            dist.Address = new Address();
            Isp isp = new Isp();
            FOWebApp.net.nimera.cmt.BOAccountingControlWSRef.VNO vno = new net.nimera.cmt.BOAccountingControlWSRef.VNO();

            //In one of the next releases remove the Isp field from the Distributor entity
            //This is not necessary anymore
            isp.Id = 112;
            dist.Isp = isp;

            try
            {
                if (!LabelVNOId.Text.Equals(""))
                {
                    dist.Id = Int32.Parse(LabelVNOId.Text);
                }
                dist.Address.AddressLine1 = TextBoxAddressLine1.Text;
                dist.Address.AddressLine2 = TextBoxAddressLine2.Text;
                dist.Address.Country = CountryList.SelectedValue;
                dist.FullName = TextBoxVNOName.Text;
                dist.Email = TextBoxEMail.Text;
                dist.Fax = TextBoxFax.Text;
                dist.Address.Location = TextBoxLocation.Text;
                dist.Address.PostalCode = TextBoxPCO.Text;
                dist.Phone = TextBoxPhone.Text;
                dist.Remarks = TextBoxRemarks.Text;
                dist.Vat = TextBoxVAT.Text;
                if (CheckBoxCurrency.Checked)
                {
                    dist.Currency = "EUR";
                }
                else
                {
                    dist.Currency = "USD";
                }
                dist.WebSite = TextBoxWebSite.Text;
                dist.VNO = true; //Is always true for a VNO and cannot be changed from the GUI

                //Update data
                BOAccountingControlWS boAccountingControl =
                    new net.nimera.cmt.BOAccountingControlWSRef.BOAccountingControlWS();
                //update the Distributor object
                if (boAccountingControl.UpdateDistributor(dist))
                {
                    LabelResult.Text = "VNO modified successfully!";
                    LabelResult.ForeColor = Color.DarkGreen;
                    ButtonSubmit.Enabled = false;
                }
                else
                {
                    LabelResult.Text = "Adding VNO failed!";
                    LabelResult.ForeColor = Color.Red;
                }
            }
            catch (Exception ex)
            {
                //Display an error message
                LabelResult.Text = ex.Message;
                LabelResult.ForeColor = Color.Red;
            }
        }
    }
}