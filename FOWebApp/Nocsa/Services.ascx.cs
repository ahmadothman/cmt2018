﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using Telerik.Web.UI;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Web.UI.DataVisualization.Charting;
using System.Globalization;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOMonitorControlWSRef;
using FOWebApp.net.nimera.cmt.BOServicesControllerWSRef;


namespace FOWebApp.Nocsa
{
    public partial class Services : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Session["_ServiceList_PageSize"] == null)
            {
                RadGridServices.PageSize = 20;
            }
            else
            {
                RadGridServices.PageSize = (int)this.Session["_ServiceList_PageSize"];
            }
            //Load data into the grid view. This view lists all services in the database.

            BOServicesControllerWS boServiceControl = new BOServicesControllerWS();
                        
            //Get the services
            Service[] services = boServiceControl.GetAllAvailableServices("all");

            //Change parent ID to parent name
            List<Service> serviceList = new List<Service>();
            foreach (Service service in services)
            {
                string parentId = service.ParentServiceId;
                if (parentId != "none")
                {
                    Service parentService = boServiceControl.GetService(parentId);
                    service.ParentServiceId = parentService.ServiceName;
                }
                serviceList.Add(service);
            }

            RadGridServices.DataSource = serviceList;
            RadGridServices.DataBind();
        }

        protected void RadGridServices_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridItemCollection selectedItems = RadGridServices.SelectedItems;

            if (selectedItems.Count > 0)
            {
                GridItem selectedItem = selectedItems[0];
                Service service = (Service)selectedItem.DataItem;
                string serviceId = service.ServiceId.ToString();
            }
        }

        protected void RadGridServices_ItemDataBound(object sender, GridItemEventArgs e)
        {
        }

        protected void RadWindowManager1_Disposed(object sender, EventArgs e)
        {
        }

        protected void RadWindowManager1_Unload(object sender, EventArgs e)
        {
        }

        protected void RadGridServices_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            this.Session["_ServiceList_PageSize"] = e.NewPageSize;
        }

        protected void RadGridServices_ItemCommand(object sender, GridCommandEventArgs e)
        {
        }


        protected void RadGridServices_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            GridDataKeyArray dataItems = e.Item.OwnerTableView.DataKeyValues;
            string serviceId = dataItems[e.Item.ItemIndex]["ServiceId"].ToString();
            BOServicesControllerWS boServiceControl = new BOServicesControllerWS();
            Service service = boServiceControl.GetService(serviceId);
            bool deleted = boServiceControl.DeleteService(service);

            if (!deleted)
            {
                RadWindowManager1.RadAlert("Deleting service failed!", 250, 200, "Error", null);
            }
            this.Page_Load(sender, (EventArgs)e);

        }
    }
}