﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Drawing;
using Telerik.Web.UI;
using FOWebApp.Data;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOUserControlWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;

namespace FOWebApp.Nocsa
{
    public partial class ModifyUser : System.Web.UI.Page
    {
        MembershipUserCollection users = Membership.GetAllUsers();
        BOAccountingControlWS boAccountingControlWS = new BOAccountingControlWS();
        BOUserControlWS boUserControlWS = new BOUserControlWS();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                

                DataHelper dataHelper = new DataHelper();

                string userName = Request.Params["UserName"];

                //Load the Membership info
                MembershipUser user = users[userName];

                //Fill the textboxes
                TextBoxUserName.Text = user.UserName;
                TextBoxEMailAddress.Text = user.Email;

                string[] roles = Roles.GetAllRoles();
                
                foreach (string s in roles)
                {
                    RadComboBoxItem item = new RadComboBoxItem();
                    item.Value = s;
                    item.Text = s;
                    RadComboBoxRoles.Items.Add(item);
                }

                //CMTFO-117
                Organization[] organizations = boAccountingControlWS.GetOrganizations();

                foreach (Organization org in organizations)
                {
                    RadComboBoxItem item = new RadComboBoxItem();
                    item.Value = org.Id.ToString();
                    item.Text = org.FullName;
                    RadComboBoxRelatedOrganization.Items.Add(item);
                }

                //Load the EndUsers combobox
                EndUser[] endUsers = boAccountingControlWS.GetEndUsers();

                foreach (EndUser endUser in endUsers)
                {
                    RadComboBoxItem item = new RadComboBoxItem();
                    item.Value = endUser.Id.ToString();
                    item.Text = endUser.FirstName + " " + endUser.LastName;
                    RadComboBoxRelatedEnduser.Items.Add(item);
                }

                Distributor[] distributors = boAccountingControlWS.GetDistributorsAndVNOs();

                foreach (Distributor distributor in distributors)
                {
                    RadComboBoxItem item = new RadComboBoxItem();
                    item.Value = distributor.Id.ToString();
                    item.Text = distributor.FullName;
                    RadComboBoxRelatedDistributor.Items.Add(item);
                }

                if (Roles.IsUserInRole(user.UserName, "NOC Administrator"))
                {
                    RadComboBoxRoles.SelectedValue = "NOC Administrator";
                    LabelRelatedEntity.Visible= false;
                    HiddenFieldRole.Value = "NOC Administrator";
                }
                else if (Roles.IsUserInRole(user.UserName, "NOC Operator"))
                {
                    RadComboBoxRoles.SelectedValue = "NOC Operator";
                    LabelRelatedEntity.Visible = false;
                    HiddenFieldRole.Value = "NOC Operator";
                }
                else if (Roles.IsUserInRole(user.UserName, "Distributor"))
                {
                    RadComboBoxRoles.SelectedValue = "Distributor";
                    Distributor dist = boAccountingControlWS.GetDistributorForUser(user.ProviderUserKey.ToString());
                    LabelRelatedEntity.Text = "Distributor:";
                    RadComboBoxRelatedDistributor.Visible = true;
                    RadComboBoxRelatedDistributor.SelectedValue = dist.Id.ToString();
                    HiddenFieldRelatedEntity.Value = dist.Id.ToString();
                    HiddenFieldRole.Value = "Distributor";
                }
                else if (Roles.IsUserInRole(user.UserName, "VNO"))
                {
                    RadComboBoxRoles.SelectedValue = "VNO";
                    // The name of the VNO should be displayed here. However, there is no method in the AccountingController to get the VNO for a given user
                    LabelRelatedEntity.Visible = false;
                    HiddenFieldRole.Value = "VNO";
                }
                else if (Roles.IsUserInRole(user.UserName, "Organization"))
                {
                    RadComboBoxRoles.SelectedValue = "Organization";
                    Organization org = boAccountingControlWS.GetOrganizationForUser(user.ProviderUserKey.ToString());
                    LabelRelatedEntity.Text = "Organization/Customer:";
                    RadComboBoxRelatedOrganization.Visible = true;
                    RadComboBoxRelatedOrganization.SelectedValue = org.Id.ToString();
                    HiddenFieldRelatedEntity.Value = org.Id.ToString();
                    HiddenFieldRole.Value = "Organization";
                }
                else if (Roles.IsUserInRole(user.UserName, "EndUser"))
                {
                    RadComboBoxRoles.SelectedValue = "EndUser";
                    EndUser endUser = boAccountingControlWS.GetEndUser(user.ProviderUserKey.ToString());
                    LabelRelatedEntity.Text = "End User:";
                    RadComboBoxRelatedEnduser.Visible = true;
                    RadComboBoxRelatedEnduser.SelectedValue = endUser.Id.ToString();
                    HiddenFieldRelatedEntity.Value = endUser.Id.ToString();
                    HiddenFieldRole.Value = "EndUser";
                }
                else if (Roles.IsUserInRole(user.UserName, "Suspended Distributor"))
                {
                    RadComboBoxRoles.SelectedValue = "Suspended Distributor";
                    Distributor dist = boAccountingControlWS.GetDistributorForUser(user.ProviderUserKey.ToString());
                    LabelRelatedEntity.Text = "Distributor:";
                    RadComboBoxRelatedDistributor.Visible = true;
                    RadComboBoxRelatedDistributor.SelectedValue = dist.Id.ToString();
                    HiddenFieldRelatedEntity.Value = dist.Id.ToString();
                    HiddenFieldRole.Value = "Suspended Distributor";
                }
                else if (Roles.IsUserInRole(user.UserName, "FinanceAdmin"))
                {
                    RadComboBoxRoles.SelectedValue = "FinanceAdmin";
                    LabelRelatedEntity.Visible = false;
                    HiddenFieldRole.Value = "FinanceAdmin";
                }
                else
                {
                    RadComboBoxRoles.SelectedValue = "EndUser";
                    LabelRelatedEntity.Visible = false;
                    HiddenFieldRole.Value = "EndUser";
                }

                CheckBoxActivated.Checked = user.IsApproved;
                CheckBoxLockedOut.Checked = user.IsLockedOut;
                if (user.IsLockedOut)
                {
                    CheckBoxLockedOut.Visible = true;
                    LabelLockOutDate.Visible = true;
                    LabelLockOutDate.Text = "Last lock-out date: " + user.LastLockoutDate.ToShortDateString();
                }
                else
                {
                    CheckBoxLockedOut.Visible = false;
                    LabelLockOutDate.Visible = false;
                }
            }
        }

        protected void ButtonCreate_Click(object sender, EventArgs e)
        {
            string errorMsg = "";
            bool errorFlag = false;

            //Update the database with the modified data
            string userName = TextBoxUserName.Text;

            //Load the Membership info
            MembershipUserCollection storedUsers = Membership.GetAllUsers();
            MembershipUser storedUser = storedUsers[userName];

            storedUser.IsApproved = CheckBoxActivated.Checked;
            storedUser.Email = TextBoxEMailAddress.Text;
            Membership.UpdateUser(storedUser);

            
            if (storedUser.IsLockedOut && !CheckBoxLockedOut.Checked)
            {
                //Unlock user
                if (!storedUser.UnlockUser())
                {
                    errorMsg = "Could not unlock user!";
                    errorFlag = true;
                }
            }

            if (RadComboBoxRoles.SelectedValue != HiddenFieldRole.Value)
	        {
                try 
	            {	        
		            Roles.AddUserToRole(storedUser.UserName, RadComboBoxRoles.SelectedValue);
                    Roles.RemoveUserFromRole(storedUser.UserName, HiddenFieldRole.Value);
                    HiddenFieldRole.Value = RadComboBoxRoles.SelectedValue;
	            }
	            catch (Exception ex)
	            {
		            errorMsg = "Could not change roles!";
                    errorFlag = true;
	            }
            }

            // Change the related entity if changed. 
            Guid userId = new Guid(storedUser.ProviderUserKey.ToString());
            if (RadComboBoxRelatedDistributor.Visible && RadComboBoxRelatedDistributor.SelectedValue != HiddenFieldRelatedEntity.Value)
            {
                int distributorId = Int32.Parse(RadComboBoxRelatedDistributor.SelectedValue);
                boUserControlWS.DeconnectUser(userId);
                boUserControlWS.ConnectUserToDistributor(userId, distributorId);
            }
            if (RadComboBoxRelatedOrganization.Visible && RadComboBoxRelatedOrganization.SelectedValue != HiddenFieldRelatedEntity.Value)
            {
                int organizationId = Int32.Parse(RadComboBoxRelatedOrganization.SelectedValue);
                boUserControlWS.DeconnectUser(userId);
                boUserControlWS.ConnectUserToCustomer(userId, organizationId);
            }
            if (RadComboBoxRelatedEnduser.Visible && RadComboBoxRelatedEnduser.SelectedValue != HiddenFieldRelatedEntity.Value)
            {
                int endUserId = Int32.Parse(RadComboBoxRelatedEnduser.SelectedValue);
                boUserControlWS.DeconnectUser(userId);
                boUserControlWS.ConnectUserToEndUser(userId, endUserId);
            }

            if (errorFlag)
            {
                LabelStatus.Visible = true;
                LabelStatus.ForeColor = Color.DarkRed;
                LabelStatus.Text = errorMsg;
            }
            else
            {
                LabelStatus.Visible = true;
                LabelStatus.ForeColor = Color.DarkGreen;
                LabelStatus.Text = "User modified successfully";
            }
        }

        protected void ButtonPasswordReset_Click(object sender, EventArgs e)
        {
            //Update the database with the modified data
            string userName = TextBoxUserName.Text;

            //Load the Membership info
            MembershipUserCollection storedUsers = Membership.GetAllUsers();
            MembershipUser storedUser = storedUsers[userName];

            storedUser.IsApproved = CheckBoxActivated.Checked;
            storedUser.Email = TextBoxEMailAddress.Text;

            //Reset the password of the user
            string newPassword = storedUser.ResetPassword();

            //Forward the password by E-Mail
            BOLogControlWS logControl = new BOLogControlWS();

            string bodyText = "Dear customer, <br/> your password has been reset. " + 
                "For your next logon into the CMT please use the following password: " + 
                "<b><br/><br/>" + newPassword + "</b><br/><br/>" + "Please change your password as soon as possible " +
                "by means of the Change Password option in the CMT menu.<br/><br/>" +
                "Best regards,<br/><br/>" +
                "Your SatADSL support team";

            string[] toAddress = {TextBoxEMailAddress.Text};

            if (logControl.SendMail(bodyText, "Password change", toAddress))
            {
                LabelStatus.Visible = true;
                LabelStatus.ForeColor = Color.DarkGreen;
                LabelStatus.Text = "Customer password successfully reset!";
            }
            else
            {
                LabelStatus.Visible = true;
                LabelStatus.ForeColor = Color.DarkRed;
                LabelStatus.Text = "Customer password reset failed!";
            }
        }
    }
}