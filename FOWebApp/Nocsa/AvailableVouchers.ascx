﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AvailableVouchers.ascx.cs"
    Inherits="FOWebApp.Nocsa.AvailableVouchers" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<telerik:RadScriptBlock ID="RadScriptBlockVouchers" runat="server">
<script>
    function RadGrid_OnCommand(sender, args) {
        //intentionally left blank 
    }

    function RowSelected(sender, eventArgs) {
        var grid = sender;
        var masterDataView = grid.get_masterTableView();
        var rowNum = eventArgs.get_itemIndexHierarchical();
        var row = masterDataView.get_dataItems()[rowNum];
        var batchId = masterDataView.getCellByColumnUniqueName(row, "BatchId").innerHTML;
        
        //Initialize and show the terminal details window
        var oWnd = radopen("Nocsa/VoucherBatchRelease.aspx?bid=" + batchId, "RadWindowReleaseVoucherBatch");
    }

</script>
</telerik:RadScriptBlock>
<div style="margin-top: 10px">
    <asp:Label ID="LabelDistributor" runat="server" Text="Distributor:"></asp:Label>&nbsp
    <telerik:RadComboBox ID="RadComboBoxDistributors" runat="server" Skin="Metro" >
    </telerik:RadComboBox>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Start date:<telerik:RadDatePicker ID="RadDatePickerStart" runat="server" Skin="Metro">
                <DateInput DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy"  runat="server" /> 
            </telerik:RadDatePicker>
            &nbsp;&nbsp;&nbsp;&nbsp; End date:<telerik:RadDatePicker ID="RadDatePickerEnd" runat="server" Skin="Metro">
                <DateInput DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy"  runat="server"/> 
            </telerik:RadDatePicker>
            &nbsp;Batch Status:&nbsp;
                    <telerik:RadDropDownList ID="RadDropDownListVoucherState" runat="server" Width="130px"
                        DropDownWidth="130px" DropDownHeight="130px" SelectedText="All" SelectedValue="2" Skin="Metro">
                        <Items>
                            <telerik:DropDownListItem runat="server" Text="All" Value="2" Selected="True" />
                            <telerik:DropDownListItem runat="server" Text="Paid" Value="1" />
                            <telerik:DropDownListItem runat="server" Text="Unpaid" Value="0" />
                            <telerik:DropDownListItem runat="server" Text="Released" Value="3" />
                            <telerik:DropDownListItem runat="server" Text="Released & paid" Value="4" />
                            <telerik:DropDownListItem runat="server" Text="Released & unpaid" Value="5" />
                        </Items>
                    </telerik:RadDropDownList>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <telerik:RadButton ID="RadButtonSubmit" runat="server" Text="Submit" 
        Skin="Metro" OnClick="RadButtonSubmit_Click">
    </telerik:RadButton>
    <br />
    <br />
    <telerik:RadGrid ID="RadGridVouchers" runat="server" CellSpacing="0"
        GridLines="None" AutoGenerateColumns="False" Skin="Metro" 
        ShowStatusBar="True"  OnItemDataBound="RadGridVouchers_ItemDataBound">
        <ClientSettings ClientEvents-OnRowSelected="RowSelected">
            <Selecting AllowRowSelect="True" />
            <ClientEvents OnCommand="RadGrid_OnCommand" />

        </ClientSettings>
<MasterTableView  AutoGenerateColumns="False" PageSize="50" >
<CommandItemSettings ExportToPdfText="Export to PDF" ShowAddNewRecordButton="False" 
        ShowExportToCsvButton="True"></CommandItemSettings>

<RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
<HeaderStyle Width="20px"></HeaderStyle>
</RowIndicatorColumn>

<ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
<HeaderStyle Width="20px"></HeaderStyle>
</ExpandCollapseColumn>

    <Columns>
        <telerik:GridCheckBoxColumn DataField="Release" DataType="System.Boolean" 
            FilterControlAltText="Filter Release column" 
            HeaderImageUrl="~\Images\lock_ok.png" HeaderText="Rel" 
            HeaderTooltip="Release Voucher Batch" UniqueName="Release">
        </telerik:GridCheckBoxColumn>
        <telerik:GridBoundColumn DataField="BatchId" 
            FilterControlAltText="Filter BatchId column" HeaderText="Batch Id" 
            UniqueName="BatchId">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="DistributorId" FilterControlAltText="Filter Distributor column" HeaderText="Distributor" UniqueName="Distributor">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="NumVouchers" 
            FilterControlAltText="Filter column column" HeaderText="Total Vouchers" 
            UniqueName="NumVouchers">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="BatchId" FilterControlAltText="Filter Available column" HeaderText="Available Vouchers" UniqueName="Available">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="DateCreated" 
            FilterControlAltText="Filter column1 column" HeaderText="Date Created" 
            UniqueName="DateCreated" DataFormatString="{0:dd/MM/yyyy}">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="UserName" 
            FilterControlAltText="Filter column2 column" HeaderText="Created By" 
            UniqueName="UserName">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="Volume" 
            FilterControlAltText="Filter Volume column" HeaderText="Description" 
            UniqueName="Volume">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="Volume" FilterControlAltText="Filter Payment column" HeaderText="Payment" UniqueName="Payment">
        </telerik:GridBoundColumn>
        <telerik:GridCheckBoxColumn DataField="Paid" DataType="System.Boolean" 
            FilterControlAltText="Filter Paid column" 
            UniqueName="Paid" HeaderText="Paid">
        </telerik:GridCheckBoxColumn>
         <telerik:GridBoundColumn DataField="PaymentMethod" FilterControlAltText="Filter Payment Method Column " HeaderText="Payment Method" UniqueName="PaymentMethod">
        </telerik:GridBoundColumn>
        <telerik:GridHyperLinkColumn 
            FilterControlAltText="Filter BatchIdLink column" HeaderText="Download" 
            UniqueName="BatchIdLink" DataNavigateUrlFields="BatchId" 
            DataNavigateUrlFormatString="~/Servlets/VoucherDownload.aspx?BatchId={0}" 
            ImageUrl="../Images/download.png">
            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>

            <ItemStyle HorizontalAlign="Center"></ItemStyle>
        </telerik:GridHyperLinkColumn>
    </Columns>

<EditFormSettings>
<EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
</EditFormSettings>
</MasterTableView>

<FilterMenu EnableImageSprites="False"></FilterMenu>

<HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default"></HeaderContextMenu>
    </telerik:RadGrid>
</div>
<telerik:RadWindowManager ID="RadWindowManager1" runat="server" Skin="Metro" DestroyOnClose="True">
    <Windows>
        <telerik:RadWindow ID="RadWindowReleaseVoucherBatch" runat="server" Animation="None" Opacity="100"
            Skin="Metro" Title="Modify Terminal" AutoSize="False" Width="550px" Height="270px"
            KeepInScreenBounds="True" EnableShadow="True" EnableAriaSupport="True" VisibleStatusbar="False"
            Modal="False" DestroyOnClose="False" VisibleTitlebar="True" IconUrl="~/Images/lock_open.png">
        </telerik:RadWindow>
    </Windows>
</telerik:RadWindowManager>
<p>
    &nbsp;</p>
<table class="auto-style1">
    <tr>
        <td class="auto-style2"><strong>Statistics:</strong></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td class="auto-style2">Total number of batches:</td>
        <td>
            <asp:Label ID="LabelNoBatch" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="auto-style2">Total number of vouchers:&nbsp;&nbsp; </td>
        <td>
            <asp:Label ID="LabelNoVoucher" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="auto-style2">Total amount:</td>
        <td>
            <asp:Label ID="LabelAmount" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="auto-style2">&nbsp;</td>
        <td>
            &nbsp;</td>
    </tr>
</table>


