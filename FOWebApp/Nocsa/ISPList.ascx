﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ISPList.ascx.cs" Inherits="FOWebApp.Nocsa.ISPList" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script type="text/javascript">
    function RowSelected(sender, eventArgs) {
        var grid = sender;
        var masterTable = grid.get_masterTableView();
         var dataItems = masterTable.get_selectedItems();
         if (dataItems.length != 0) {
              var ispId = dataItems[0].get_element().cells[0].innerHTML;
              var oWnd = radopen("Nocsa/ISPDetailsForm.aspx?id=" + ispId, "RadWindowISPDetails");
        }
    }
</script>
<telerik:RadAjaxPanel ID="RadAjaxPanelISPList" runat="server" Height="800px" Width="600px" LoadingPanelID="RadAjaxLoadingPanelISPList">
<telerik:RadGrid ID="RadGridISPList" runat="server" AllowPaging="True" 
    AllowSorting="True" AutoGenerateColumns="False" CellSpacing="0" 
    GridLines="None" Skin="Metro">
    <ClientSettings>
        <Selecting AllowRowSelect="True" />
        <Selecting EnableDragToSelectRows="False" />
        <ClientEvents OnRowSelected="RowSelected" />
    </ClientSettings>
    <MasterTableView PageSize="20" CommandItemDisplay="Top" CommandItemSettings-ShowAddNewRecordButton="False">
        <CommandItemSettings ExportToPdfText="Export to PDF" />
        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
            <HeaderStyle Width="20px" />
        </RowIndicatorColumn>
        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
            <HeaderStyle Width="20px" />
        </ExpandCollapseColumn>
        <Columns>
            <telerik:GridBoundColumn DataField="Id" DataType="System.Int32" 
                FilterControlAltText="Filter IdColumn column" HeaderText="Id" MaxLength="5" 
                ReadOnly="True" UniqueName="IdColumn">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="CompanyName" 
                FilterControlAltText="Filter CompanyColumn column" HeaderText="Company" 
                ItemStyle-Wrap="False" UniqueName="CompanyColumn">
                <ItemStyle Wrap="False" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Phone" 
                FilterControlAltText="Filter PhoneColumn column" HeaderText="Phone" 
                MaxLength="20" UniqueName="PhoneColumn">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Fax" 
                FilterControlAltText="Filter FaxColumn column" HeaderText="Fax" MaxLength="20" 
                UniqueName="FaxColumn">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Email" 
                FilterControlAltText="Filter EMailColumn column" HeaderText="E-Mail" 
                MaxLength="50" UniqueName="EMailColumn">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Satellite.SatelliteName" 
                FilterControlAltText="Filter SatelliteColumn column" HeaderText="Satellite" 
                UniqueName="SatelliteColumn">
            </telerik:GridBoundColumn>
        </Columns>
        <EditFormSettings>
            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
            </EditColumn>
        </EditFormSettings>
    </MasterTableView>

    <%--<SelectedItemStyle BackColor="#99CCFF" />--%>

<FilterMenu EnableImageSprites="False"></FilterMenu>

<HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default"></HeaderContextMenu>
</telerik:RadGrid>
</telerik:RadAjaxPanel>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanelISPList" runat="server" Skin="Metro">
</telerik:RadAjaxLoadingPanel>
<telerik:RadWindowManager ID="RadWindowManagerISP" runat="server">
    <Windows>
        <telerik:RadWindow  runat="server" ID="RadWindowISPDetails"  NavigateUrl="TerminalDetailsForm.aspx"
                Animation="Fade" Opacity="100" Skin="Metro" Title="Terminal Details" AutoSize="False"
                Width="900px" Height= "700px" KeepInScreenBounds="False" EnableShadow="True" EnableAriaSupport="True"
                VisibleStatusbar="True" Modal="true">
        </telerik:RadWindow>
    </Windows>
</telerik:RadWindowManager>


