﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SwitchoverReport.ascx.cs" Inherits="FOWebApp.Nocsa.SwitchoverReport" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="ImageButtonQuery">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGridSwitchoverReport" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<table cellpadding="5" cellspacing="2">
    <tr>
        <td>Start date:</td>
        <td> 
            <telerik:RadDatePicker ID="RadDatePickerStart" runat="server" Skin="Metro">
            </telerik:RadDatePicker>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Provide a start date" ControlToValidate="RadDatePickerStart" Display="Dynamic"></asp:RequiredFieldValidator>
        </td>
        <td>End date:</td>
        <td>
            <telerik:RadDatePicker ID="RadDatePickerEnd" runat="server" Skin="Metro">
            </telerik:RadDatePicker>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Provide an end date" ControlToValidate="RadDatePickerEnd" Display="Dynamic"></asp:RequiredFieldValidator>
        </td>
        <td>
            <%--<asp:ImageButton ID="ImageButtonQuery" runat="server" 
                ImageUrl="~/Images/invoice_euro.png" onclick="ImageButtonQuery_Click" 
                ToolTip="Create switchover report" />--%>
            <asp:Button ID="switchOverButton" runat="server" Text="Show"
                 onclick="ImageButtonQuery_Click" 
                ToolTip="Create switchover report"/>
        </td>
        <td>
            <asp:ImageButton ID="ImageButtonCSVExport" runat="server" 
                ImageUrl="~/Images/CSV.png" onclick="ImageButtonCSVExport_Click" 
                Enabled="True" ToolTip="Export as a CSV file" />
        </td>
        <td>
            <asp:ImageButton ID="ImageButtonExcelExport" runat="server" Enabled="True" 
                ImageUrl="~/Images/Excel.png" onclick="ImageButtonExcelExport_Click" 
                ToolTip="Export as an Excel file" />
        </td>
    </tr>
</table>
<telerik:RadGrid ID="RadGridSwitchoverReport" runat="server" 
        CellSpacing="0" GridLines="None" ShowStatusBar="True"
        onitemcommand="RadGridSwitchoverReport_ItemCommand" 
        onneeddatasource="RadGridSwitchoverReport_NeedDataSource" 
        onitemcreated="RadGridSwitchoverReport_ItemCreated" AutoGenerateColumns="False" 
        Skin="Metro" AllowSorting="True" CellPadding="0" width="600px">
    <ExportSettings IgnorePaging="True" OpenInNewWindow="True">
        <Pdf Author="CMT" Creator="CMT" Title="CMT Termnal Switchover Activities" 
            DefaultFontFamily="Arial Unicode MS" FontType="Embed" PageBottomMargin="20mm" 
            PageFooterMargin="20mm" PageHeight="297mm" PageLeftMargin="20mm" 
            PageRightMargin="20mm" PageTitle="Terminal Switchover Activities" 
            PageWidth="210mm" PaperSize="A4" Producer="SatADSL" 
            Subject="Terminal Switchover Overview" />
        <Csv ColumnDelimiter="Semicolon" EncloseDataWithQuotes="False" />
    </ExportSettings>
    <ClientSettings ReorderColumnsOnClient="True" EnableRowHoverStyle="true">
    </ClientSettings>
    <MasterTableView CommandItemDisplay="None">
        <CommandItemSettings ExportToPdfText="Export to PDF" 
            ShowAddNewRecordButton="False" ShowExportToCsvButton="False" 
            ShowExportToExcelButton="False" ShowExportToPdfButton="False" 
            ShowExportToWordButton="False" ShowRefreshButton="False" />
            <Columns>
                <telerik:GridBoundColumn DataField="TimeStamp" FilterControlAltText="Filter TimeStampColumn column"
                    HeaderText="Time stamp" UniqueName="TimeStamp" ShowFilterIcon="False"
                    AutoPostBackOnFilter="True" DataType="System.DateTime" DataFormatString="{0:dd/MM/yyyy HH:mm:ss}">
                    <ItemStyle Wrap="True" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="FromMac" FilterControlAltText="Filter FromMacColumn column"
                    HeaderText="From Mac" UniqueName="FromMac" ShowFilterIcon="False"
                    AutoPostBackOnFilter="True">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="ToMac" FilterControlAltText="Filter ToMacColumn column"
                    HeaderText="To Mac" UniqueName="ToMac" ShowFilterIcon="false">
                </telerik:GridBoundColumn>
            </Columns>
        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
            <HeaderStyle Width="20px" />
        </RowIndicatorColumn>
        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
            <HeaderStyle Width="20px" />
        </ExpandCollapseColumn>
        <EditFormSettings>
            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
            </EditColumn>
        </EditFormSettings>
    </MasterTableView>
    <FilterMenu EnableImageSprites="False">
    </FilterMenu>
    <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default">
    </HeaderContextMenu>
</telerik:RadGrid>

