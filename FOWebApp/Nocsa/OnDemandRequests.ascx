﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OnDemandRequests.ascx.cs" Inherits="FOWebApp.Nocsa.OnDemandRequests" %>

<script type="text/javascript">
    function RowSelected(sender, eventArgs) {
        var grid = sender;
        var masterDataView = grid.get_masterTableView();
        var rowNum = eventArgs.get_itemIndexHierarchical();
        var row = masterDataView.get_dataItems()[rowNum];
        var CMTId = masterDataView.getCellByColumnUniqueName(row, "CMTId").innerHTML;


        var oWnd = radopen("SLMBookingDetailsForm.aspx?cmtid=" + CMTId, "RadWindowBookingDetails");
    }

    function CommandSelected(sender, eventArgs) {
        var grid = sender;
        if (eventArgs.get_commandName() == "BookingDetails") {
            var CMTId = eventArgs.get_commandArgument();
            alert(CMTId);
            var oWnd = radopen("SLMBookingDetailsForm.ascx?cmtid=" + CMTId, "RadWindowBookingDetails");
        }
    }
</script>

<telerik:RadGrid ID="RadGridSLMBookings" runat="server" AllowSorting="True"
    AutoGenerateColumns="False" CellSpacing="0" GridLines="None" Skin="Metro"
    ShowStatusBar="True" Width="992px"
    PageSize="50">
    <GroupingSettings CaseSensitive="false" />
    <ClientSettings>
        <Selecting AllowRowSelect="True" />
        <ClientEvents OnRowSelected="RowSelected" />
        <ClientEvents OnCommand="CommandSelected" />
    </ClientSettings>
    <MasterTableView CommandItemDisplay="Top" CommandItemSettings-ShowExportToExcelButton="False"
        CommandItemSettings-ShowExportToCsvButton="False" DataKeyNames="CMTId" RowIndicatorColumn-Visible="True" RowIndicatorColumn-Resizable="True" 
        CommandItemSettings-ShowAddNewRecordButton="False" ShowFooter="False">
        <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
        </RowIndicatorColumn>
        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
        </ExpandCollapseColumn>
        <Columns>
            <telerik:GridBoundColumn DataField="CMTId" FilterControlAltText="Filter CMTIdColumn column" 
                HeaderText="CMT Id" HeaderStyle-Width="10px"
                UniqueName="CMTId" ShowFilterIcon="False" AutoPostBackOnFilter="True">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="TerminalMac" FilterControlAltText="Filter TerminalMacColumn column" 
                HeaderText="Terminal Mac"
                UniqueName="TerminalMac" ShowFilterIcon="False" AutoPostBackOnFilter="True">
            </telerik:GridBoundColumn>
            <telerik:GridDateTimeColumn DataField="StartTime" FilterControlAltText="Filter StartTimeColumn column"
                HeaderText="Start Time" ShowFilterIcon="False" AutoPostBackOnFilter="True" DataType="System.DateTime" DataFormatString="{0:dd/MM/yyyy HH:mm:ss}">
            </telerik:GridDateTimeColumn>
            <telerik:GridDateTimeColumn DataField="EndTime" FilterControlAltText="Filter EndTimeColumn column"
                HeaderText="End Time" ShowFilterIcon="False" AutoPostBackOnFilter="True" DataType="System.DateTime"                DataFormatString="{0:dd/MM/yyyy HH:mm:ss}">
            </telerik:GridDateTimeColumn>
            <telerik:GridDateTimeColumn DataField="Type" FilterControlAltText="Filter TypeColumn column"
                HeaderText="Type" ShowFilterIcon="False" AutoPostBackOnFilter="True">
            </telerik:GridDateTimeColumn>
            <telerik:GridDateTimeColumn DataField="Status" FilterControlAltText="Filter StatusColumn column"
                HeaderText="Status" ShowFilterIcon="False" AutoPostBackOnFilter="True">
            </telerik:GridDateTimeColumn>
        </Columns>
        <EditFormSettings>
            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
            </EditColumn>
        </EditFormSettings>
    </MasterTableView>
    <FilterMenu EnableImageSprites="False">
    </FilterMenu>
    <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default">
    </HeaderContextMenu>
</telerik:RadGrid>
<telerik:RadWindowManager ID="RadWindowManager1" runat="server" Skin="Metro" DestroyOnClose="True">
    <Windows>
        <telerik:RadWindow ID="RadWindowBookingDetails" runat="server" Animation="None" Opacity="100"
            Skin="Metro" Title="Modify Booking" AutoSize="False" Width="600px" Height="550px"
            KeepInScreenBounds="True" EnableShadow="True" EnableAriaSupport="True" VisibleStatusbar="True"
            Modal="False" DestroyOnClose="False">
        </telerik:RadWindow>
    </Windows>
</telerik:RadWindowManager>