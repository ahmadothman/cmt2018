﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AllBookings.ascx.cs" Inherits="FOWebApp.Nocsa.AllBookings" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<telerik:RadAjaxPanel ID="RadAjaxPanelReservation" runat="server" Height="1600px"
    Width="100%" LoadingPanelID="RadAjaxLoadingPanelReservation">

    <table>
        <tr>
            <td>Start date:
                <telerik:RadDatePicker runat="server" ID="RadDatePickerStartDate">
                    <DateInput DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy" runat="server" Skin="Metro"/>
                </telerik:RadDatePicker>
            </td>
            <td>End date:
                <telerik:RadDatePicker runat="server" ID="RadDatePickerEndDate">
                    <DateInput DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy"  runat="server" Skin="Metro"/> 
                </telerik:RadDatePicker>
            </td>
            <td>
                <telerik:RadButton runat="server" ID="RadButtonFilter" Text="Filter" OnClick="RadButtonFilter_Click" Skin="Metro"></telerik:RadButton>
            </td>
        </tr>
    </table>


    <telerik:RadGrid ID="RadGridSLMBookings" runat="server" AllowSorting="False"
        AutoGenerateColumns="False" CellSpacing="0" GridLines="None" Skin="Metro"
        ShowStatusBar="True" Width="992px" AllowFilteringByColumn="False"
        PageSize="50" OnItemDataBound="RadGridSLMBookings_ItemDataBound" MasterTableView-CommandItemSettings-ShowRefreshButton="False">
        <GroupingSettings CaseSensitive="false" />
        <ClientSettings>
            <Selecting AllowRowSelect="True" />
        </ClientSettings>
        <MasterTableView CommandItemDisplay="Top" CommandItemSettings-ShowExportToExcelButton="False"
            CommandItemSettings-ShowExportToCsvButton="False" DataKeyNames="CMTId" RowIndicatorColumn-Visible="False" RowIndicatorColumn-Resizable="False"
            CommandItemSettings-ShowAddNewRecordButton="False" ShowFooter="False">
            <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
            <Columns>
                <telerik:GridBoundColumn DataField="TerminalMac" FilterControlAltText="Filter TerminalMacColumn column"
                    HeaderText="Terminal Mac"
                    UniqueName="TerminalMac" ShowFilterIcon="False" AutoPostBackOnFilter="True">
                </telerik:GridBoundColumn>
                <telerik:GridDateTimeColumn DataField="StartTime" FilterControlAltText="Filter StartTimeColumn column"
                    HeaderText="Start Time" ShowFilterIcon="False" AutoPostBackOnFilter="True" DataType="System.DateTime" DataFormatString="{0:dd/MM/yyyy HH:mm:ss}">
                </telerik:GridDateTimeColumn>
                <telerik:GridDateTimeColumn DataField="EndTime" FilterControlAltText="Filter EndTimeColumn column"
                    HeaderText="End Time" ShowFilterIcon="False" AutoPostBackOnFilter="True" DataType="System.DateTime" DataFormatString="{0:dd/MM/yyyy HH:mm:ss}">
                </telerik:GridDateTimeColumn>
                <telerik:GridDateTimeColumn DataField="BookingSlaId" FilterControlAltText="Filter BookingSlaIdColumn column"
                    HeaderText="Booking Sla" UniqueName="BookingSla" ShowFilterIcon="False" AutoPostBackOnFilter="True">
                </telerik:GridDateTimeColumn>
                <telerik:GridDateTimeColumn DataField="Type" FilterControlAltText="Filter TypeColumn column"
                    HeaderText="Type" ShowFilterIcon="False" AutoPostBackOnFilter="True">
                </telerik:GridDateTimeColumn>
                <telerik:GridDateTimeColumn DataField="Status" FilterControlAltText="Filter StatusColumn column"
                    HeaderText="Status" ShowFilterIcon="False" AutoPostBackOnFilter="True">
                </telerik:GridDateTimeColumn>
                <telerik:GridButtonColumn HeaderText="Delete" ImageUrl="~/Images/delete.png" ConfirmText="Are you sure you want to delete this booking?"
                    UniqueName="DeleteColumn" ButtonType="ImageButton" HeaderButtonType="PushButton"
                    Text="Delete" CommandName="Delete">
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                </telerik:GridButtonColumn>
            </Columns>
        </MasterTableView>
        <FilterMenu EnableImageSprites="False">
        </FilterMenu>
        <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default">
        </HeaderContextMenu>
    </telerik:RadGrid>
</telerik:RadAjaxPanel>
