﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VoucherRequestList.ascx.cs" Inherits="FOWebApp.Nocsa.VoucherRequestList" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script type="text/javascript">
    function RowSelected(sender, eventArgs) {
        var grid = sender;
        var masterTable = grid.get_masterTableView();
        var dataItems = masterTable.get_selectedItems();
        if (dataItems.length != 0) {
            var id = dataItems[0].get_element().cells[0].innerHTML;
            var oWnd = radopen("Nocsa/VoucherRequestDetailsPage.aspx?id=" + id, "RadWindowVoucherRequestDetails");
        }
    }
</script>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="RadGridVoucherRequests">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGridVoucherRequests" />
                <telerik:AjaxUpdatedControl ControlID="RadWindowManager1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Metro"/>
<telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" Height="200px" 
    Width="467px" HorizontalAlign="NotSet" 
    LoadingPanelID="RadAjaxLoadingPanel1">
<telerik:RadGrid ID="RadGridVoucherRequests" runat="server" AllowSorting="True"
    AutoGenerateColumns="False" CellSpacing="0" GridLines="None" Skin="Metro"
    ShowStatusBar="True" Width="992px" AllowFilteringByColumn="False" 
        onitemdatabound="RadGridVoucherRequests_ItemDataBound" 
        onpagesizechanged="RadGridVoucherRequests_PageSizeChanged" 
        PageSize="50" onitemcommand="RadGridVoucherRequests_ItemCommand">
        <GroupingSettings CaseSensitive="false" /> 
    <ClientSettings EnableRowHoverStyle="true">
        <Selecting AllowRowSelect="True" />
        <ClientEvents OnRowSelected="RowSelected" />
    </ClientSettings>
    <MasterTableView CommandItemDisplay="Top" CommandItemSettings-ShowExportToExcelButton="False" 
        CommandItemSettings-ShowExportToCsvButton="False" RowIndicatorColumn-Visible="True" RowIndicatorColumn-Resizable="True" RowIndicatorColumn-AndCurrentFilterFunction="Contains" CommandItemSettings-ShowAddNewRecordButton="False" ShowFooter="False">
        <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
        </RowIndicatorColumn>
        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
        </ExpandCollapseColumn>
        <Columns>
            <telerik:GridBoundColumn DataField="Id"
                FilterControlAltText="Filter IdColumn column" HeaderText="ID" 
                UniqueName="Key" ShowFilterIcon="False" AutoPostBackOnFilter="True">
                <ItemStyle Wrap="True" />
            </telerik:GridBoundColumn>            
            <telerik:GridBoundColumn DataField="Distributor"
                FilterControlAltText="Filter DistributorColumn column" HeaderText="Distributor" 
                UniqueName="Key" ShowFilterIcon="False" AutoPostBackOnFilter="True">
                <ItemStyle Wrap="True" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="VoucherVolume"
                FilterControlAltText="Filter VoucherVolumeColumn column" HeaderText="Voucher volume" 
                UniqueName="Key" ShowFilterIcon="False" AutoPostBackOnFilter="True">
                <ItemStyle Wrap="True" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="NumVouchers"
                FilterControlAltText="Filter NumVouchersColumn column" HeaderText="Number of vouchers" 
                UniqueName="Key" ShowFilterIcon="False" AutoPostBackOnFilter="True">
                <ItemStyle Wrap="True" />
            </telerik:GridBoundColumn>
        </Columns>
        <EditFormSettings>
            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
            </EditColumn>
        </EditFormSettings>
    </MasterTableView>
    <FilterMenu EnableImageSprites="False">
    </FilterMenu>
    <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default">
    </HeaderContextMenu>
</telerik:RadGrid>
<telerik:RadWindowManager ID="RadWindowManager1" runat="server" Skin="Metro" 
        DestroyOnClose="True">
    <Windows>
        <telerik:RadWindow ID="RadWindowVoucherRequestDetails" runat="server" 
            NavigateUrl="NOCSA/VoucherRequestDetailsPage.aspx" Animation="Fade" Opacity="100" Skin="Metro" 
            Title="Voucher Request Details" AutoSize="False" Width="1000px" Height="700px" KeepInScreenBounds="True" EnableShadow="True" EnableAriaSupport="True" VisibleStatusbar="True" Modal="False" DestroyOnClose="False">
        </telerik:RadWindow>
    </Windows>
</telerik:RadWindowManager>
</telerik:RadAjaxPanel>