﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FOWebApp.Nocsa
{
    public partial class VoucherVolumeDetailsPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Load the details form and initialize the data
            int id = Convert.ToInt32(Request.Params["id"]);

            VoucherVolumeDetailsForm df =
                (VoucherVolumeDetailsForm)Page.LoadControl("VoucherVolumeDetailsForm.ascx");

            df.componentDataInit(id);

            PlaceHolderVoucherVolumeDetailsForm.Controls.Add(df);
        }
    }
}