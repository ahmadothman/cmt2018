﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FreeZoneDetailsForm.ascx.cs" Inherits="FOWebApp.Nocsa.FreeZoneDetailsForm" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<style type="text/css">
    .auto-style2 {
        width: 102%;
    }
</style>
<telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
    <script type="text/javascript">
        function DoBlur(fld) {
            fld.className = 'normalfld';
        }

        function DoFocus(fld) {
            fld.className = 'focusfld';
        }

        function DoChange(fld) {
            fld.className = 'changedfld';
        }
    </script>
</telerik:RadCodeBlock>
<telerik:RadAjaxPanel ID="RadAjaxPanelIssueDetails" runat="server" GroupingText="Issue details" Height="200px" Width="232px"
    EnableHistory="True" Style="margin-right: 114px">
<table cellpadding="10" cellspacing="5" class="issueDetails">
    <tr>
        <td>ID:</td>
        <td><asp:label ID="LabelId" runat="server"></asp:label></td>
    </tr>
    <tr>
        <td class="requiredLabel">
            Name:
        </td>
        <td colspan="2">
            <asp:TextBox ID="TextBoxName" runat="server" Width="300px"></asp:TextBox>
        </td>
    </tr>
    <%--<tr>
        <td>Active:</td>
        <td>
            <asp:CheckBox ID="CheckBoxActive" runat="server" />
        </td>
    </tr>--%>
    <tr>
        <td></td>
        <td>Free zone ends</td>
        <td>Free zone starts</td>
    </tr>
    <tr>
        <td>Monday</td>
        <td><telerik:RadMaskedTextBox ID="TextBoxMonOff" runat="server" Width="100px"></telerik:RadMaskedTextBox></td>
        <td><telerik:RadMaskedTextBox ID="TextBoxMonOn" runat="server" Width="100px"></telerik:RadMaskedTextBox></td>
    </tr>
    <tr>
        <td>Tuesday</td>
        <td><telerik:RadMaskedTextBox ID="TextBoxTueOff" runat="server" Width="100px"></telerik:RadMaskedTextBox></td>
        <td><telerik:RadMaskedTextBox ID="TextBoxTueOn" runat="server" Width="100px"></telerik:RadMaskedTextBox></td>
    </tr>
    <tr>
        <td>Wednesday</td>
        <td><telerik:RadMaskedTextBox ID="TextBoxWedOff" runat="server" Width="100px"></telerik:RadMaskedTextBox></td>
        <td><telerik:RadMaskedTextBox ID="TextBoxWedOn" runat="server" Width="100px"></telerik:RadMaskedTextBox></td>
    </tr>
    <tr>
        <td>Thursday</td>
        <td><telerik:RadMaskedTextBox ID="TextBoxThuOff" runat="server" Width="100px"></telerik:RadMaskedTextBox></td>
        <td><telerik:RadMaskedTextBox ID="TextBoxThuOn" runat="server" Width="100px"></telerik:RadMaskedTextBox></td>
    </tr>
    <tr>
        <td>Friday</td>
        <td><telerik:RadMaskedTextBox ID="TextBoxFriOff" runat="server" Width="100px"></telerik:RadMaskedTextBox></td>
        <td><telerik:RadMaskedTextBox ID="TextBoxFriOn" runat="server" Width="100px"></telerik:RadMaskedTextBox></td>
    </tr>
    <tr>
        <td>Saturday</td>
        <td><telerik:RadMaskedTextBox ID="TextBoxSatOff" runat="server" Width="100px"></telerik:RadMaskedTextBox></td>
        <td><telerik:RadMaskedTextBox ID="TextBoxSatOn" runat="server" Width="100px"></telerik:RadMaskedTextBox></td>
    </tr>
    <tr>
        <td>Sunday</td>
        <td><telerik:RadMaskedTextBox ID="TextBoxSunOff" runat="server" Width="100px"></telerik:RadMaskedTextBox></td>
        <td><telerik:RadMaskedTextBox ID="TextBoxSunOn" runat="server" Width="100px"></telerik:RadMaskedTextBox></td>
    </tr>
    <tr>
        <td>
            <asp:Button ID="ButtonUpdate" runat="server" Text="Update" OnClick="ButtonUpdate_Click" />
        </td>
        <td colspan="2">
            <asp:Label ID="LabelResult" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td colspan="2">All times in GMT.</td>
    </tr>
</table>
</telerik:RadAjaxPanel>
