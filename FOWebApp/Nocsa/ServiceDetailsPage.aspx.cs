﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FOWebApp.Distributors;

namespace FOWebApp.Nocsa
{
    public partial class ServiceDetailsPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Load the details form and initialize the data
            string serviceId = Request.Params["serviceId"].ToString();

            ServiceDetailsForm df =
                (ServiceDetailsForm)Page.LoadControl("ServiceDetailsForm.ascx");

            df.componentDataInit(serviceId);

            PlaceHolderServiceDetailsForm.Controls.Add(df);
        }
    }
}