﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FOWebApp.Nocsa
{
    public partial class ISPDetailsForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!IsPostBack)
            {
                ISPDetails ispDetails = 
                    (ISPDetails) Page.LoadControl("~/Nocsa/ISPDetails.ascx");

                string strIspId = Request.Params["id"];
                int ispId = Int32.Parse(strIspId);

                ispDetails.initForm(ispId);

                PlaceHolderISPDetails.Controls.Add(ispDetails);

            }
        }
    }
}