﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OutgoingEmailsLists.ascx.cs" Inherits="FOWebApp.Nocsa.OutgoingEmailsLists" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script type="text/javascript">
    function RowSelected(sender, eventArgs) {
        var grid = sender;
        var masterDataView = grid.get_masterTableView();
        var rowNum = eventArgs.get_itemIndexHierarchical();
        var row = masterDataView.get_dataItems()[rowNum];
        var date = masterDataView.getCellByColumnUniqueName(row, "Date").innerHTML;

        //Initialize and show the ticket details window
        var oWnd = radopen("Nocsa/OutgoingEmailsReport.aspx?date=" + date, "RadWindowTicketDetails");
    }
</script>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="RadGridEmailReports">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGridEmailReports" />
                <telerik:AjaxUpdatedControl ControlID="RadWindowManager1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadGrid ID="RadGridEmailReports" runat="server" AllowSorting="False"
    AutoGenerateColumns="false" Visible="true" CellSpacing="0" GridLines="None" Skin="WebBlue"
    ShowStatusBar="True" Width="100px" AllowFilteringByColumn="False" 
        PageSize="50">
        <GroupingSettings CaseSensitive="false" /> 
    <ClientSettings>
        <Selecting AllowRowSelect="True" />
        <ClientEvents OnRowSelected="RowSelected" />
    </ClientSettings>
    <MasterTableView CommandItemDisplay="Top" CommandItemSettings-ShowExportToExcelButton="False" 
        CommandItemSettings-ShowExportToCsvButton="False" RowIndicatorColumn-Visible="True" RowIndicatorColumn-Resizable="True" RowIndicatorColumn-AndCurrentFilterFunction="Contains" CommandItemSettings-ShowAddNewRecordButton="False" ShowFooter="False" ShowHeader="false">
        <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
        </RowIndicatorColumn>
        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
        </ExpandCollapseColumn>
        <Columns>
            <telerik:GridBoundColumn DataField="Date" 
                FilterControlAltText="Filter TicketIDColumn column" HeaderText="Date" 
                UniqueName="Date" ShowFilterIcon="False" AutoPostBackOnFilter="True">
                <ItemStyle Wrap="False" />
            </telerik:GridBoundColumn>
        </Columns>
        <EditFormSettings>
            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
            </EditColumn>
        </EditFormSettings>
    </MasterTableView>
    <FilterMenu EnableImageSprites="False">
    </FilterMenu>
    <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default">
    </HeaderContextMenu>
</telerik:RadGrid>
<telerik:RadWindowManager ID="RadWindowManager1" runat="server" Skin="Vista" 
        DestroyOnClose="True">
    <Windows>
        <telerik:RadWindow ID="RadWindowEmailReport" runat="server" 
            NavigateUrl="Nocsa/OutgoingEmailsReport.aspx" Animation="Fade" Opacity="100" Skin="WebBlue" 
            Title="Ticket Details" AutoSize="False" Width="1000px" Height="700px" KeepInScreenBounds="True" EnableShadow="True" EnableAriaSupport="True" VisibleStatusbar="True" Modal="False" DestroyOnClose="False">
        </telerik:RadWindow>
    </Windows>
</telerik:RadWindowManager>
</telerik:RadAjaxPanel>