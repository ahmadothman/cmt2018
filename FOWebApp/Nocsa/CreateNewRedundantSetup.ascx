﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CreateNewRedundantSetup.ascx.cs" Inherits="FOWebApp.Nocsa.CreateNewRedundantSetup" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script language="javascript" type="text/javascript">
    function OnClientSelectedIndexChanged(sender, eventArgs) {
        var item = eventArgs.get_item();
        //sender.set_text("You selected ISP with Id: " + item.get_value());

        var panel = $find("<%=RadXmlHttpPanelSLA.ClientID %>");
        panel.set_value(item.get_value());
        return false;
    }

    function OnClientSelectedIndexChanged2(sender, eventArgs) {
        var item = eventArgs.get_item();
        //sender.set_text("You selected ISP with Id: " + item.get_value());

        var panel = $find("<%=RadXmlHttpPanelSLA2.ClientID %>");
        panel.set_value(item.get_value());
        return false;
    }

    function nodeClickingDistVNO(sender, args) {
        var comboBox = $find("<%= RadComboBoxDistributorVNO.ClientID %>");

        var node = args.get_node()
        if (node.get_level() == 1) {

            comboBox.set_text(node.get_text());
            comboBox.set_value(node.get_value());
            document.getElementById('HiddenFieldDistributorId').value = node.get_value();
            comboBox.trackChanges();
            comboBox.get_items().getItem(0).set_text(node.get_text());
            comboBox.commitChanges();

            comboBox.hideDropDown();
        }
    }

    function nodeClicking(sender, args) {
        var comboBox = $find("<%= RadComboBoxSLA.ClientID %>");

        var node = args.get_node()
        if (node.get_level() == 1) {
            comboBox.set_text(node.get_text());
            comboBox.set_value(node.get_value());
            document.getElementById('HiddenFieldSLAId').value = node.get_value();
            comboBox.trackChanges();
            comboBox.get_items().getItem(0).set_text(node.get_text());
            comboBox.commitChanges();

            comboBox.hideDropDown();
        }
    }

    function IPRangeChanged(chkBoxId) {
        if (chkBoxId.checked == true) {
            document.getElementById('<%= RadComboBoxIPMask.ClientID %>').disabled = false;
            ValidatorEnable(document.getElementById('<%= RequiredFieldValidatorStartIP.ClientID %>'), true);
        }
        else {
            document.getElementById('<%= RadComboBoxIPMask.ClientID %>').disabled = true;
            ValidatorEnable(document.getElementById('<%= RequiredFieldValidatorStartIP.ClientID %>'), false);
        }
    }

    function MacInvalidValue(sender, eventArgs) {
        if (eventArgs.Value === '00:00:00:00:00:00') {
            eventArgs.IsValid = false;
        } else {
            eventArgs.IsValid = true;
        }
    }

    function OnMacAddressChanged(sender, eventArgs) {
        var macAddress = eventArgs.get_newValue();
        FOWebApp.WebServices.AccountSupportWS.IsMacAddressUnique(macAddress, OnRequestComplete, OnError);
        return false;
    }

    function OnRequestComplete(result) {
        if (result == false) {
            document.getElementById('<%= LabelMacExists.ClientID %>').innerHTML = "Sorry but this MAC address already exists!";
        }
        else {
            document.getElementById('<%= LabelMacExists.ClientID %>').innerHTML = "";
        }
    }

    function OnError(result) {
        document.getElementById('<%= LabelMacExists.ClientID %>').innerHTML = "An error occured while checking the MAC address";
    }
</script>
<table cellpadding="10" cellspacing="5">
    <tr>
        <td class="requiredLabel">Distributor/VNO:
        </td>
        <td>
            <%--<telerik:RadComboBox ID="RadComboBoxDistributor" runat="server" Skin="Metro" ExpandDirection="Up"
                Height="200px" Width="430px">
            </telerik:RadComboBox>--%>
            <telerik:RadComboBox ID="RadComboBoxDistributorVNO" runat="server" Skin="Metro" Sort="Ascending" Width="430px" EnableVirtualScrolling="True" EmptyMessage="Select a Distributor or VNO"
                MaxHeight="300px" Height="200px">
                <ItemTemplate>
                    <telerik:RadTreeView ID="RadTreeViewDistributorVNO" runat="server" Skin="Metro" OnClientNodeClicking="nodeClickingDistVNO">
                    </telerik:RadTreeView>
                </ItemTemplate>
                <Items>
                    <telerik:RadComboBoxItem Text="" />
                </Items>
                <ExpandAnimation Type="Linear" />
            </telerik:RadComboBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorDistributorVNO" runat="server" ErrorMessage="Please select a distributor or VNO"
                ControlToValidate="RadComboBoxDistributorVNO" />
        </td>
    </tr>
    <tr>
        <td>Free zone schedule:
        </td>
        <td>
            <telerik:RadComboBox ID="RadComboBoxFreeZone" runat="server" Skin="Metro" ExpandDirection="Up"
                Height="200px" Width="430px">
            </telerik:RadComboBox>
        </td>
    </tr>
    <tr>
        <td>Not-Billable:
        </td>
        <td>
            <asp:CheckBox ID="CheckBoxTestMode" runat="server" />
        </td>
    </tr>
    <tr>
        <td>Subscription Start Date:
        </td>
        <td>
            <telerik:RadDatePicker ID="RadDatePickerStartDate" runat="server" Skin="Metro">
                <Calendar ID="Calendar1" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x"
                    Skin="Metro" runat="server">
                </Calendar>
                <DateInput ID="DateInput1" DisplayDateFormat="d/MM/yyyy" DateFormat="d/MM/yyyy" runat="server">
                </DateInput>
                <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
            </telerik:RadDatePicker>
        </td>
    </tr>
    <tr>
        <td>Subscription Expiry Date:
        </td>
        <td>
            <telerik:RadDatePicker ID="RadDatePickerExpiryDate" runat="server" Skin="Metro">
                <Calendar ID="Calendar2" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x"
                    Skin="Metro" runat="server">
                </Calendar>
                <DateInput ID="DateInput2" DisplayDateFormat="d/MM/yyyy" DateFormat="d/MM/yyyy" runat="server">
                </DateInput>
                <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
            </telerik:RadDatePicker>
        </td>
    </tr>
    <tr>
        <td>
            <h3>Primary terminal</h3>
        </td>
    </tr>
    <tr>
        <td class="requiredLabel">Mac Address:
        </td>
        <td>
            <telerik:RadMaskedTextBox ID="RadMaskedTextBoxMacAddress" runat="server" Skin="Metro"
                ControlToValidate="RadMaskedTextBoxMacAddress" ClientEvents-OnValueChanged="OnMacAddressChanged">
            </telerik:RadMaskedTextBox>
            <asp:CustomValidator ID="CustomValidatorMacAddress" runat="server" ErrorMessage="The MAC address can't be 00:00:00:00:00:00"
                ControlToValidate="RadMaskedTextBoxMacAddress" ClientValidationFunction="MacInvalidValue" OnServerValidate="CustomValidatorMacAddress_ServerValidate"
                Display="Dynamic" />
            <asp:Label ID="LabelMacExists" runat="server" ForeColor="Red"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="requiredLabel">ISP:
        </td>
        <td>
            <telerik:RadComboBox ID="RadComboBoxISP" runat="server" Skin="Metro" OnClientSelectedIndexChanged="OnClientSelectedIndexChanged"
                Width="350px">
            </telerik:RadComboBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorIsp" runat="server" ErrorMessage="Please select an ISP"
                ControlToValidate="RadComboBoxISP" />
        </td>
    </tr>
    <tr>
        <td class="requiredLabel">Serial:
        </td>
        <td>
            <asp:TextBox ID="TextBoxSerial" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Please provide the serial number!"
                ControlToValidate="TextBoxSerial"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <!--
    <tr>
        <td>Sit Id:
        </td>
        <td>
            <asp:TextBox ID="TextBoxSitId" runat="server"></asp:TextBox>
            <asp:Label ID="LabelSitIdRequired" runat="server" ForeColor="#FF3300"></asp:Label>
        </td>
    </tr>
    -->
    <tr>
        <td class="requiredLabel">Terminal Name:
        </td>
        <td>
            <asp:TextBox ID="TextBoxTerminalName" runat="server" Width="425px" MaxLength="50"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Please provide a unique terminal name!"
                ControlToValidate="TextBoxTerminalName" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidatorTerminalName" runat="server"
                ErrorMessage="The terminal name can only contain characters, digits, spaces and the symbols '-' and '_'. It also has a maximal length of 50 characters."
                ValidationExpression="[A-Za-z\d-_ ]{1,50}" ControlToValidate="TextBoxTerminalName" />
        </td>
    </tr>
    <tr>
        <td class="requiredLabel">Service Package:
        </td>
        <td>
            <telerik:RadXmlHttpPanel ID="RadXmlHttpPanelSLA" runat="server" EnableClientScriptEvaluation="True"
                OnServiceRequest="RadXmlHttpPanelSLA_ServiceRequest">
                <telerik:RadComboBox ID="RadComboBoxSLA" runat="server" Skin="Metro" ExpandDirection="Up"
                    Height="200px" Width="430px">
                </telerik:RadComboBox>
            </telerik:RadXmlHttpPanel>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorSla" runat="server" ErrorMessage="Please select an SLA"
                ControlToValidate="RadComboBoxSLA" Display="Dynamic" />
            <asp:CompareValidator ID="CompareValidatorSla" runat="server" ErrorMessage="Please select an ISP that has SLAs defined" 
                ValueToCompare="No SLAs defined for this ISP or SLAs are hidden" Operator="NotEqual" ControlToValidate="RadComboBoxSLA"  />
        </td>
    </tr>
    <tr>
        <td>(Start) IP Address:</td>
        <td>
            <asp:TextBox ID="TextBoxIP" runat="server" Columns="39"></asp:TextBox> IP Mask:
            <!-- <asp:TextBox ID="TextBoxIPMask" runat="server" ToolTip="Specify an IP Mask (\ value)" Enabled="False" Columns="39"></asp:TextBox></td> -->
            <telerik:RadComboBox ID="RadComboBoxIPMask" runat="server" Enabled="false" Width="60" Skin="Metro">
                <Items>
                    <telerik:RadComboBoxItem runat="server" Text="32" Value="32" />
                    <telerik:RadComboBoxItem runat="server" Text="31" Value="31" />
                    <telerik:RadComboBoxItem runat="server" Text="30" Value="30" />
                    <telerik:RadComboBoxItem runat="server" Text="29" Value="29" />
                    <telerik:RadComboBoxItem runat="server" Text="28" Value="28" />
                    <telerik:RadComboBoxItem runat="server" Text="27" Value="27" />
                    <telerik:RadComboBoxItem runat="server" Text="26" Value="26" />
                    <telerik:RadComboBoxItem runat="server" Text="25" Value="25" />
                    <telerik:RadComboBoxItem runat="server" Text="24" Value="24" />
                </Items>
            </telerik:RadComboBox>
            <asp:CheckBox ID="CheckBoxIPRange" runat="server" Text="Specify IP Range" ToolTip="Check this box if the terminal has an IP Address range. In this case specify an end range IP address." OnClick="javascript:IPRangeChanged(this);" AutoPostBack="False" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorStartIP" runat="server" ErrorMessage="Please specify a start IP address" Enabled="False" ControlToValidate="TextBoxIP" Display="Dynamic"></asp:RequiredFieldValidator></td>
        <td>
    </tr>
    <tr>
        <td>Static IP address:
        </td>
        <td>
            <asp:CheckBox ID="CheckBoxStaticIP" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            <h3>Secondary terminal</h3>
        </td>
    </tr>
    <tr>
        <td class="requiredLabel">Mac Address:
        </td>
        <td>
            <telerik:RadMaskedTextBox ID="RadMaskedTextBoxMacAddress2" runat="server" Skin="Metro"
                ControlToValidate="RadMaskedTextBoxMacAddress2" ClientEvents-OnValueChanged="OnMacAddressChanged">
            </telerik:RadMaskedTextBox>
            <asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="The MAC address can't be 00:00:00:00:00:00"
                ControlToValidate="RadMaskedTextBoxMacAddress2" ClientValidationFunction="MacInvalidValue" OnServerValidate="CustomValidatorMacAddress_ServerValidate"
                Display="Dynamic" />
            <asp:Label ID="Label1" runat="server" ForeColor="Red"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="requiredLabel">ISP:
        </td>
        <td>
            <telerik:RadComboBox ID="RadComboBoxISP2" runat="server" Skin="Metro" OnClientSelectedIndexChanged="OnClientSelectedIndexChanged2"
                Width="350px">
            </telerik:RadComboBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please select an ISP"
                ControlToValidate="RadComboBoxISP2" />
        </td>
    </tr>
    <tr>
        <td class="requiredLabel">Serial:
        </td>
        <td>
            <asp:TextBox ID="TextBoxSerial2" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please provide the serial number!"
                ControlToValidate="TextBoxSerial2"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td class="requiredLabel">Terminal Name:
        </td>
        <td>
            <asp:TextBox ID="TextBoxTerminalName2" runat="server" Width="425px" MaxLength="50"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Please provide a unique terminal name!"
                ControlToValidate="TextBoxTerminalName2" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                ErrorMessage="The terminal name can only contain characters, digits, spaces and the symbols '-' and '_'. It also has a maximal length of 50 characters."
                ValidationExpression="[A-Za-z\d-_ ]{1,50}" ControlToValidate="TextBoxTerminalName2" />
        </td>
    </tr>
    <tr>
        <td class="requiredLabel">Service Package:
        </td>
        <td>
            <telerik:RadXmlHttpPanel ID="RadXmlHttpPanelSLA2" runat="server" EnableClientScriptEvaluation="True"
                OnServiceRequest="RadXmlHttpPanelSLA2_ServiceRequest">
                <telerik:RadComboBox ID="RadComboBoxSLA2" runat="server" Skin="Metro" ExpandDirection="Up"
                    Height="200px" Width="430px">
                </telerik:RadComboBox>
            </telerik:RadXmlHttpPanel>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Please select an SLA"
                ControlToValidate="RadComboBoxSLA2" Display="Dynamic" />
            <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Please select an ISP that has SLAs defined" 
                ValueToCompare="No SLAs defined for this ISP or SLAs are hidden" Operator="NotEqual" ControlToValidate="RadComboBoxSLA2"  />
        </td>
    </tr>
    <tr>
        <td>(Start) IP Address:</td>
        <td>
            <asp:TextBox ID="TextBoxIP2" runat="server" Columns="39"></asp:TextBox> IP Mask:
            <!-- <asp:TextBox ID="TextBoxIPMask2" runat="server" ToolTip="Specify an IP Mask (\ value)" Enabled="False" Columns="39"></asp:TextBox></td> -->
            <telerik:RadComboBox ID="RadComboBoxIPMask2" runat="server" Enabled="false" Width="60" Skin="Metro">
                <Items>
                    <telerik:RadComboBoxItem runat="server" Text="32" Value="32" />
                    <telerik:RadComboBoxItem runat="server" Text="31" Value="31" />
                    <telerik:RadComboBoxItem runat="server" Text="30" Value="30" />
                    <telerik:RadComboBoxItem runat="server" Text="29" Value="29" />
                    <telerik:RadComboBoxItem runat="server" Text="28" Value="28" />
                    <telerik:RadComboBoxItem runat="server" Text="27" Value="27" />
                    <telerik:RadComboBoxItem runat="server" Text="26" Value="26" />
                    <telerik:RadComboBoxItem runat="server" Text="25" Value="25" />
                    <telerik:RadComboBoxItem runat="server" Text="24" Value="24" />
                </Items>
            </telerik:RadComboBox>
            <asp:CheckBox ID="CheckBoxIPRange2" runat="server" Text="Specify IP Range" ToolTip="Check this box if the terminal has an IP Address range. In this case specify an end range IP address." OnClick="javascript:IPRangeChanged(this);" AutoPostBack="False" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="Please specify a start IP address" Enabled="False" ControlToValidate="TextBoxIP2" Display="Dynamic"></asp:RequiredFieldValidator></td>
        <td>
    </tr>
    <tr>
        <td>Static IP address:
        </td>
        <td>
            <asp:CheckBox ID="CheckBoxStaticIP2" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            <h3>Virtual terminal (Microtik)</h3>
        </td>
    </tr>
    <tr>
        <td class="requiredLabel">Mac Address:
        </td>
        <td>
            <telerik:RadMaskedTextBox ID="RadMaskedTextBoxMacAddress3" runat="server" Skin="Metro"
                ControlToValidate="RadMaskedTextBoxMacAddress3" ClientEvents-OnValueChanged="OnMacAddressChanged">
            </telerik:RadMaskedTextBox>
            <asp:CustomValidator ID="CustomValidator3" runat="server" ErrorMessage="The MAC address can't be 00:00:00:00:00:00"
                ControlToValidate="RadMaskedTextBoxMacAddress3" ClientValidationFunction="MacInvalidValue" OnServerValidate="CustomValidatorMacAddress_ServerValidate"
                Display="Dynamic" />
            <asp:Label ID="Label2" runat="server" ForeColor="Red"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="requiredLabel">IP address:</td>
        <td>
            <asp:TextBox ID="TextBoxIP3" runat="server" Columns="39"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="Please provide an IP address!"
                ControlToValidate="TextBoxIP3" Display="Dynamic"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td class="requiredLabel">Service Package:
        </td>
        <td>
            <telerik:RadComboBox ID="RadComboBoxSLA3" runat="server" Skin="Metro" ExpandDirection="Up"
                    Height="200px" Width="430px"></telerik:RadComboBox>
         </td>
    </tr>
    <tr>
        <td>
            <asp:Button ID="ButtonSave" runat="server" Text="Save" OnClick="ButtonSave_Click" />
        </td>
        <td>
            <asp:Label ID="LabelResult" runat="server"></asp:Label>
        </td>
    </tr>
</table>
