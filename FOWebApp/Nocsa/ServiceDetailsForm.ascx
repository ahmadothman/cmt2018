﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ServiceDetailsForm.ascx.cs" Inherits="FOWebApp.Nocsa.ServiceDetailsForm" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<style type="text/css">
    .auto-style2 {
        width: 102%;
    }
</style>
<telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
    <script type="text/javascript">
        function OnClientSelectedIndexChanged(sender, eventArgs) {
            var item = eventArgs.get_item();
            //sender.set_text("You selected ISP with Id: " + item.get_value());

            var panel = $find("<%=RadXmlHttpPanelParent2.ClientID %>");
        panel.set_value(item.get_value());
        return false;
        }

        
    </script>
</telerik:RadCodeBlock>

<table cellpadding="10" cellspacing="5" class="serviceDetails">
    <tr>
        <td>Service ID</td>
        <td><asp:Label ID="LabelServiceId" runat="server"></asp:Label></td>
    </tr>
    <tr>
        <td>Name</td>
        <td>
            <asp:TextBox ID="TextBoxServiceName" runat="server" Width="400px"></asp:TextBox><br />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please specify a service name" ControlToValidate="TextBoxServiceName"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>Description</td>
        <td>
            <asp:TextBox ID="TextBoxDescription" runat="server" TextMode="MultiLine"
                    Width="400px" Rows="4" BackColor="#FFFFCC"></asp:TextBox><br />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Please specify a description" ControlToValidate="TextBoxDescription"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>Controller</td>
        <td>
            <asp:TextBox ID="TextBoxController" runat="server" Width="400px"></asp:TextBox><br />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please specify a service controller" ControlToValidate="TextBoxController"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>Level</td>
        <td>
            <asp:RadioButton runat="server" Checked="false" value="1" onclick="DisplayElement(this)" ID="RadioButtonLevel1" Text="1" GroupName="LevelsGroup" />&nbsp;<asp:RadioButton runat="server" Checked="false" value="2" onclick="DisplayElement(this)" ID="RadioButtonLevel2" Text="2" GroupName="LevelsGroup" />&nbsp;<asp:RadioButton runat="server" Checked="false" onclick="DisplayElement(this)" value="3" ID="RadioButtonLevel3" Text="3" GroupName="LevelsGroup" /><br />
            <div id="selectLevel" style="display:none"><a style="color:red">Please select a level</a></div>
        </td>
    </tr>
    <tr>
        <td>Icon</td>
        <td>
            <asp:Image ID="ImageIcon" runat="server" Visible="true" />
            <asp:FileUpload runat="server" ID="FileUpload1" />
            <asp:Label ID="LabelUploadStatus" runat="server"></asp:Label>
        </td> 
    </tr>
    <tr>
        <td><asp:Label ID="LabelParent1" runat="server" Text="Parent (level 1)"></asp:Label></td>
        <td>
            <asp:Label ID="LabelParent2" runat="server">
                <telerik:RadComboBox ID="RadComboBoxParent1" runat="server" Skin="Metro" OnClientSelectedIndexChanged="OnClientSelectedIndexChanged" Width="400px">
                </telerik:RadComboBox>
            </asp:Label>
        </td>
    </tr>
    <tr>
        <td><asp:Label ID="LabelParent3" runat="server" text="Parent (level 2)"></asp:Label ></td>
        <td>
            <asp:Label ID="LabelParent4" runat="server" >
                <telerik:RadXmlHttpPanel ID="RadXmlHttpPanelParent2" runat="server" EnableClientScriptEvaluation="True"
                    OnServiceRequest="RadXmlHttpPanelParent2_ServiceRequest">
                    <telerik:RadComboBox ID="RadComboBoxParent2" runat="server" Skin="Metro" Width="400px">
                    </telerik:RadComboBox>
                </telerik:RadXmlHttpPanel>
            </asp:Label >
        </td>
    </tr>
    <tr>
        <td><asp:Button runat="server" Text="Save Changes" ID="UpdateButton" OnClick="UpdateButton_Click" /></td>
        <td><asp:Label runat="server" ID="LabelUpdate"></asp:Label></td>
    </tr>
    <tr>
        <td><asp:Button runat="server" ID="DeleteButton" Text="Delete" OnClick="DeleteButton_Click" OnClientClick="return confirm('Are you sure you want to delete this service?');" /></td>
        <td><asp:Label runat="server" ID="LabelDelete"></asp:Label></td>
    </tr>
 </table>