﻿using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace FOWebApp.Nocsa
{
    public partial class SuspendedTerminals : System.Web.UI.UserControl
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
            RadDatePickerStart.SelectedDate = DateTime.Now.AddDays(-1.0);
            RadDatePickerEnd.SelectedDate = DateTime.Now;
        }

        public void ConfigureExport()
        {
            RadGridSuspendedTerminals.ExportSettings.FileName = "CMTSuspendedTerminals";
            RadGridSuspendedTerminals.ExportSettings.ExportOnlyData = false;
            RadGridSuspendedTerminals.ExportSettings.IgnorePaging = true;
            RadGridSuspendedTerminals.ExportSettings.OpenInNewWindow = true;
            RadGridSuspendedTerminals.ExportSettings.Excel.Format = GridExcelExportFormat.Html;
            RadGridSuspendedTerminals.ExportSettings.Csv.ColumnDelimiter = GridCsvDelimiter.Semicolon;
            RadGridSuspendedTerminals.ExportSettings.Csv.EncloseDataWithQuotes = false;
            RadGridSuspendedTerminals.ExportSettings.Csv.RowDelimiter = GridCsvDelimiter.NewLine;
        }

        protected void ImageButtonQuery_Click(object sender, EventArgs e)
        {
            DateTime startDate;
            DateTime endDate;

            if (!RadDatePickerStart.IsEmpty || !RadDatePickerEnd.IsEmpty)
            {
                startDate = (DateTime)RadDatePickerStart.SelectedDate;
                Session["StartDate"] = startDate;
                endDate = (DateTime)RadDatePickerEnd.SelectedDate;
                Session["EndDate"] = endDate;

                this.generateReport(startDate, endDate);
                RadGridSuspendedTerminals.DataBind();
                RadGridSuspendedTerminals.Visible = true;
            }
            else
            {
                RadGridSuspendedTerminals.Visible = false;
            }
        }

        protected void ImageButtonExport_Click(object sender, ImageClickEventArgs e)
        {
            DateTime startDate = Convert.ToDateTime(Session["StartDate"]);
            DateTime endDate = Convert.ToDateTime(Session["EndDate"]);
            this.generateReport(startDate, endDate);
            this.ConfigureExport();

            ImageButton button = sender as ImageButton;
            if (button.ID == "ImageButtonCSVExport")
            {
                RadGridSuspendedTerminals.MasterTableView.ExportToCSV();
            }
            else
            {
                RadGridSuspendedTerminals.MasterTableView.ExportToExcel();
            }
        }

        private void generateReport(DateTime startDate, DateTime endDate)
        {
            BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();
            DataTable dt = null;

            dt = boAccountingControl.GetSuspendedTerminals(startDate, endDate);

            if (dt != null)
            {
                RadGridSuspendedTerminals.DataSource = dt;
                RadGridSuspendedTerminals.Visible = true;
            }
            else
            {
                RadGridSuspendedTerminals.Visible = false;
            }
        }

        protected void RadGridSuspendedTerminals_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            DateTime startDate;
            DateTime endDate;

            if (!RadDatePickerStart.IsEmpty || !RadDatePickerEnd.IsEmpty)
            {
                startDate = (DateTime)RadDatePickerStart.SelectedDate;
                endDate = (DateTime)RadDatePickerEnd.SelectedDate;

                this.generateReport(startDate, endDate);
            }
            else
            {
                RadGridSuspendedTerminals.Visible = false;
            }
        }
    }
}