﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOMonitorControlWSRef;
using FOWebApp.net.nimera.cmt.BOVoucherControllerWSRef;

namespace FOWebApp.Nocsa
{
    public partial class VoucherRequestList : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BOVoucherControllerWS voucherControl = new BOVoucherControllerWS();
            BOAccountingControlWS accountingControl = new BOAccountingControlWS();
            //Fetch the voucher requests
            VoucherRequest[] voucherRequests = voucherControl.GetAllPendingVoucherRequests();
            
            //Make the voucher requests more readable
            List<VRListObject> voucherRequestList = new List<VRListObject>();
            VRListObject vrLO;
            VoucherVolume vv;
            foreach (VoucherRequest vr in voucherRequests)
            {
                vrLO = new VRListObject();
                vrLO.Id = vr.RequestId;
                vrLO.Distributor = accountingControl.GetDistributor(vr.DistributorId).FullName;
                vv = voucherControl.GetVoucherVolumeById(vr.VoucherVolumeId);
                vrLO.VoucherVolume = vv.VolumeMB + " MB - Valid " + vv.ValidityPeriod + " days - " + vv.UnitPriceEUR.ToString("0.00") + "€ / " + vv.UnitPriceUSD.ToString("0.00") + "$";
                vrLO.NumVouchers = vr.NumVouchers;
                voucherRequestList.Add(vrLO);
            }

            RadGridVoucherRequests.DataSource = voucherRequestList;
            RadGridVoucherRequests.DataBind();
        }

        protected void RadGridVoucherRequests_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridItemCollection selectedItems = RadGridVoucherRequests.SelectedItems;

            if (selectedItems.Count > 0)
            {
                GridItem selectedItem = selectedItems[0];
                VoucherRequest vr = (VoucherRequest)selectedItem.DataItem;
                string id = vr.RequestId.ToString();
            }
        }

        protected void RadGridVoucherRequests_ItemDataBound(object sender, GridItemEventArgs e)
        {
        }

        protected void RadWindowManager1_Disposed(object sender, EventArgs e)
        {
        }

        protected void RadWindowManager1_Unload(object sender, EventArgs e)
        {
        }

        protected void RadGridVoucherRequests_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            this.Session["_VoucherRequestsList_PageSize"] = e.NewPageSize;
        }

        protected void RadGridVoucherRequests_ItemCommand(object sender, GridCommandEventArgs e)
        {
        }

        /// <summary>
        /// Object for the display of voucher requests in a readable list
        /// </summary>
        private class VRListObject
        {
            //the ID of the request
            public int Id { get; set; }
            //the name of the distributor that made the request
            public string Distributor { get; set; }
            //the description of the voucher volume
            public string VoucherVolume { get; set; }
            //the number of vouchers
            public int NumVouchers { get; set; }
        }
    }
}