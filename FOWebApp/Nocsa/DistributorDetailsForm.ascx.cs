﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOInvoicingControllerWSRef;
using Telerik.Web.UI;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;

namespace FOWebApp.Nocsa
{
    public partial class DistributorDetailsForm : System.Web.UI.UserControl
    {
        BOAccountingControlWS _boAccountingControl = new BOAccountingControlWS();
        BOLogControlWS _boLogControlWS = new BOLogControlWS();
        string[] eMailRecipients = { FOWebApp.Properties.Settings.Default.SupportEmail };

        private bool BadDistributorDB = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            //Set empty item in list of organisations
            RadComboBoxOrg.Items.Add(new RadComboBoxItem("", "0"));
        }

        public void componentDataInit(int distributorId)
        {
            //Get the Distributor information an fill up the form
            BOAccountingControlWS boAccountingControl =
              new net.nimera.cmt.BOAccountingControlWSRef.BOAccountingControlWS();

            //Get the Distributor linked to the user
            Distributor dist = boAccountingControl.GetDistributor(distributorId);
            
            if (dist != null)
            {
                TextBoxAddressLine1.Text = dist.Address.AddressLine1;
                TextBoxAddressLine2.Text = dist.Address.AddressLine2;
                //TextBoxCountry.Text = dist.Address.Country;
                CountryList.SelectedValue = dist.Address.Country;
                TextBoxDistributorName.Text = dist.FullName;
                TextBoxEMail.Text = dist.Email;
                TextBoxFax.Text = dist.Fax;
                TextBoxLocation.Text = dist.Address.Location;
                TextBoxPCO.Text = dist.Address.PostalCode;
                TextBoxPhone.Text = dist.Phone;
                TextBoxRemarks.Text = dist.Remarks;

                RadTextBoxInvoicingEMail.Text = dist.InvoicingEmail;
                TextBoxVAT.Text = dist.Vat;
                if (dist.Currency == "USD")
                {
                    CheckBoxCurrency.Checked = true;
                }
                if (dist.LastInvoiceDate != null)
                {
                    if (dist.LastInvoiceDate == new DateTime())
                    {
                        dist.LastInvoiceDate = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, 1);
                    }
                    else
                    {
                        //LiteralLastInvoiceDate.Text = dist.LastInvoiceDate.ToShortDateString();
                        RadDatePickerLastInvoice.SelectedDate = dist.LastInvoiceDate;
                    }
               
                    //RadDatePickerInvoiceMonth.SelectedDate = dist.LastInvoiceDate;
                    HiddenFieldLastInvoiceDate.Value = dist.LastInvoiceDate.ToString();
                }
                if (dist.Inactive == true)
                {
                    CheckBoxInactive.Checked = true;
                }
                // Load BadDistributor Checkbox 
                // Checks if the content of the CheckBox differs from the value in the DB by the BadDistributorDB, set as a privatge bool (false) before the PageLoad
                if (dist.BadDistributor == true)
                {
                    CheckBoxBadDistributor0.Checked = true;
                    BadDistributorDB = true;
                }
                if (dist.InvoiceInterval == null)
                {
                    RadNumericTextBoxInvoiceInterval.Value = 1;
                    
                }
                else
                {
                    RadNumericTextBoxInvoiceInterval.Value = dist.InvoiceInterval; 
                }
                CheckBoxBankCharges.Checked = dist.BankCharges;

                TextBoxWebSite.Text = dist.WebSite;
                LabelDistId.Text = "" + dist.Id;
                PanelISPAllocation.Visible = true;
                RadListBoxIsp.Visible = true;

                //Load the Customer list
                Organization[] organizations = _boAccountingControl.GetOrganisationsByDistributorId(dist.Id.ToString());
                foreach (Organization org in organizations)
                {
                    RadComboBoxItem listItem = new RadComboBoxItem(org.FullName, org.Id.ToString());
                    RadComboBoxOrg.Items.Add(listItem);
                }
                
                if (dist.DefaultOrganization != null)
                {
                    RadComboBoxOrg.SelectedValue = dist.DefaultOrganization.ToString();
                }
                
                
                //Load the Checkbox list
                Isp[] ispList = _boAccountingControl.GetIsps();
                foreach (Isp isp in ispList)
                {
                    RadListBoxItem listItem = new RadListBoxItem(isp.CompanyName, isp.Id.ToString());
                    RadListBoxIsp.Items.Add(listItem);
                }

                //Check the ISPs allocated to this distributor
                Isp[] ispListForDistributor = _boAccountingControl.GetISPsForDistributor(distributorId);

                foreach (Isp isp in ispListForDistributor)
                {
                    for (int i = 0; i < RadListBoxIsp.Items.Count; i++)
                    {
                        if (!RadListBoxIsp.Items[i].Checked && RadListBoxIsp.Items[i].Value.Equals(isp.Id.ToString()))
                        {
                            RadListBoxIsp.Items[i].Checked = true;
                        }
                    }
                }
                RadComboBoxDistributorType.SelectedValue = dist.DistType.ToString() ;

               
            }
        }

        //recursive function that adds attribute to all child controls 
        private void addBlurAtt(Control cntrl)
        {
            if (cntrl.Controls.Count > 0)
            {
                foreach (Control childControl in cntrl.Controls)
                {
                    addBlurAtt(childControl);
                }
            }
            if (cntrl.GetType() == typeof(TextBox))
            {
                TextBox TempTextBox = (TextBox)cntrl;
                //TempTextBox.Attributes.Add("onFocus", "DoFocus(this);");
                //TempTextBox.Attributes.Add("onBlur", "DoBlur(this);");
                TempTextBox.Attributes.Add("onchange", "DoChange(this);");
            }
            if (cntrl.GetType() == typeof(ListBox))
            {
                ListBox TempTextBox = (ListBox)cntrl;
                //TempTextBox.Attributes.Add("onFocus", "DoFocus(this);");
                //TempTextBox.Attributes.Add("onBlur", "DoBlur(this);");
                TempTextBox.Attributes.Add("onchange", "DoChange(this);");
            }
            if (cntrl.GetType() == typeof(DropDownList))
            {
                DropDownList TempTextBox = (DropDownList)cntrl;
                //TempTextBox.Attributes.Add("onFocus", "DoFocus(this);");
                //TempTextBox.Attributes.Add("onBlur", "DoBlur(this);");
                TempTextBox.Attributes.Add("onchange", "DoChange(this);");
            }
        }

        protected void ButtonSubmit_Click(object sender, EventArgs e)
        {
            //Submit the data to the database
            Distributor dist = new Distributor();
            dist.Address = new Address();
            Isp isp = new Isp();

            //In one of the next releases remove the Isp field from the Distributor entity
            //This is not necessary anymore
            isp.Id = 112;
            dist.Isp = isp;

            try
            {
                if (!LabelDistId.Text.Equals(""))
                {
                    dist.Id = Int32.Parse(LabelDistId.Text);
                }
                dist.Address.AddressLine1 = TextBoxAddressLine1.Text;
                dist.Address.AddressLine2 = TextBoxAddressLine2.Text;
                dist.Address.Country = CountryList.SelectedValue;
                dist.FullName = TextBoxDistributorName.Text;
                dist.Email = TextBoxEMail.Text;
                dist.Fax = TextBoxFax.Text;
                dist.Address.Location = TextBoxLocation.Text;
                dist.Address.PostalCode = TextBoxPCO.Text;
                dist.Phone = TextBoxPhone.Text;
                dist.Remarks = TextBoxRemarks.Text;
                dist.Vat = TextBoxVAT.Text;
                if (CheckBoxCurrency.Checked)
                {
                    dist.Currency = "USD";
                }
                else
                {
                    dist.Currency = "EUR";
                }
                // Active/Inactive Distributor
                if (CheckBoxInactive.Checked)
                {
                    dist.Inactive = true;
                }
                else
                {
                    dist.Inactive = false;
                }

                // Distributor Type
                dist.DistType = Int32.Parse(RadComboBoxDistributorType.SelectedValue);
                // Bad Distributor 
                if (CheckBoxBadDistributor0.Checked)
                {
                    dist.BadDistributor = true;
                    if (BadDistributorDB == false)
                    {
                        _boAccountingControl.BadDistributorChanged((int)dist.Id, dist.BadDistributor);
                        _boLogControlWS.SendMail("Please suspend the service for the Distributor mentioned above.", "Distributor "+dist.FullName+" Marked as Bad Payer", eMailRecipients);
                    }
                }
                else
                {
                    dist.BadDistributor = false;
                    if (BadDistributorDB == true)
                    {
                        _boAccountingControl.BadDistributorChanged((int)dist.Id, dist.BadDistributor);
                        _boLogControlWS.SendMail("Please activate the service for the Distributor mentioned above.", "Distributor " + dist.FullName + " Un-Marked as Bad Payer", eMailRecipients);
                    }
                    
                }
                //dist.LastInvoiceDate = (DateTime)RadDatePickerInvoiceMonth.SelectedDate;
                DateTime lastInvoiceDate;
                if (DateTime.TryParse(RadDatePickerLastInvoice.SelectedDate.ToString(), out lastInvoiceDate))
                {
                    dist.LastInvoiceDate = lastInvoiceDate;
                }
                else
                {
                    dist.LastInvoiceDate = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, 1);
                }

                if (RadNumericTextBoxInvoiceInterval.Value != null)
                {
                    
                    
                    dist.InvoiceInterval = (int)RadNumericTextBoxInvoiceInterval.Value;
                }
                else
                {
                    dist.InvoiceInterval = 1;
                }
                dist.BankCharges = CheckBoxBankCharges.Checked;
                dist.InvoicingEmail = RadTextBoxInvoicingEMail.Text;
                dist.WebSite = TextBoxWebSite.Text;
                dist.VNO = false; //Is always false for a distributor created or edited from the distributor screen.
                dist.DefaultOrganization = Convert.ToInt32(RadComboBoxOrg.SelectedValue);

                bool advancePaymentresult = true;
                if (!String.IsNullOrEmpty(AdvancePayment.Text))
                {
                    BOInvoicingControllerWS invoicingController = new BOInvoicingControllerWS();
                    advancePaymentresult = invoicingController.AddAdvancePayment(Convert.ToDecimal(AdvancePayment.Text), Convert.ToInt32(dist.Id));
                }

                //Update data
                BOAccountingControlWS boAccountingControl = new net.nimera.cmt.BOAccountingControlWSRef.BOAccountingControlWS();
                if (boAccountingControl.UpdateDistributor(dist) && advancePaymentresult)
                {
                    LabelResult.Text = "Distributor modified successfully";
                    LabelResult.ForeColor = Color.DarkGreen;
                    ButtonSubmit.Enabled = false;
                }
                else if (!advancePaymentresult)
                {
                    LabelResult.Text = "Advance Payment failed";
                    LabelResult.ForeColor = Color.Red;
                }
                else
                {
                    LabelResult.Text = "Modifying distributor failed";
                    LabelResult.ForeColor = Color.Red;
                }
            }
            catch (Exception ex)
            {
                BOLogControlWS logControl = new BOLogControlWS();

                CmtApplicationException cmtApplicationException = new CmtApplicationException()
                {
                    ExceptionDateTime = DateTime.UtcNow,
                    ExceptionDesc = ex.Message,
                    ExceptionStacktrace = ex.StackTrace,
                    ExceptionLevel = 3,
                    UserDescription = "DistributorDetailsForm: Insert / Update Distributor"
                };

                logControl.LogApplicationException(cmtApplicationException);

                //Display an error message
                LabelResult.Text = "Modifying distributor failed!";
                LabelResult.ForeColor = Color.Red;
            }
        }
    }
}