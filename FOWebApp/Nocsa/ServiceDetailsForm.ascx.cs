﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Drawing;
using Telerik.Web.UI;
using FOWebApp.net.nimera.cmt.BOServicesControllerWSRef;
using System.IO;

namespace FOWebApp.Nocsa
{
    public partial class ServiceDetailsForm : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public void componentDataInit(string serviceId)
        {
            servicesController = new BOServicesControllerWS();

            // get the service details
            serviceObject = servicesController.GetService(serviceId);
            LabelServiceId.Text = serviceObject.ServiceId.ToString();
            TextBoxServiceName.Text = serviceObject.ServiceName;
            TextBoxDescription.Text = serviceObject.ServiceDescription;
            TextBoxController.Text = serviceObject.ServiceController;
            if (serviceObject.Icon != null)
            {
                ImageIcon.ImageUrl = "~/Nocsa/ServicesImageHandler.ashx?serviceId=" + serviceObject.ServiceId.ToString();
                ImageIcon.Visible = true;
            }
            if (serviceObject.Level == 1)
            {
                RadioButtonLevel1.Checked = true;
            }
            else if (serviceObject.Level == 2)
            {
                RadioButtonLevel2.Checked = true;
            }
            else if (serviceObject.Level == 3)
            {
                RadioButtonLevel3.Checked = true;
            }
            
            // fill up the 1st parent list
            Service[] servicesLevel1 = servicesController.GetAllAvailableServices("1");

            foreach (Service service in servicesLevel1)
            {
                RadComboBoxItem item = new RadComboBoxItem();
                item.Value = service.ServiceId.ToString();
                item.Text = service.ServiceName;
                RadComboBoxParent1.Items.Add(item);
            }
            RadComboBoxParent1.Items.Insert(0, new RadComboBoxItem(""));

            RadComboBoxParent1.ClearSelection();

            // select the actual parent of the service, if applicable
            Service parentService = servicesController.GetParent(serviceObject);

            if (serviceObject.Level == 1)
            {
                RadComboBoxParent1.SelectedIndex = 0;
            }
            else if (serviceObject.Level == 2)
            {
                parent1Id = parentService.ServiceId.ToString();
            }
            else if (serviceObject.Level == 3)
            {
                parent1Id = parentService.ParentServiceId;
                parentService = servicesController.GetService(parent1Id);
            }

            if (this.Session["CreateNewService_Parent1"] != null)
            {
                parent1Id = this.Session["CreateNewService_Parent1"].ToString();
            }
            RadComboBoxParent1.SelectedValue = "" + parent1Id;

            // fill up the 2nd parent list
            Service[] servicesLevel2 = servicesController.GetChildren(parentService);

            foreach (Service service in servicesLevel2)
            {
                RadComboBoxItem item = new RadComboBoxItem();
                item.Value = service.ServiceId.ToString();
                item.Text = service.ServiceName;
                RadComboBoxParent2.Items.Add(item);
            }
            RadComboBoxParent2.Items.Insert(0, new RadComboBoxItem(""));
            RadComboBoxParent2.SortItems();

            // select the actual parent of the service, if applicable
            if (serviceObject.Level == 3)
            {
                parent2Id = serviceObject.ParentServiceId;
                RadComboBoxParent2.SelectedValue = "" + parent2Id;
            }
            

            
        }
        
        protected void UpdateButton_Click(object sender, EventArgs e)
        {
            if (RadioButtonLevel1.Checked || RadioButtonLevel2.Checked || RadioButtonLevel3.Checked)
            {
                serviceObject.ServiceName = TextBoxServiceName.Text.ToString();
                serviceObject.ServiceDescription = TextBoxDescription.Text.ToString();
                serviceObject.ServiceController = TextBoxController.Text.ToString();
                
                if (RadioButtonLevel1.Checked)
                {
                    serviceObject.Level = 1;
                    serviceObject.ParentServiceId = "none";
                    if (FileUpload1.HasFile)
                    {
                        // only files of type jpeg and png are accepted
                        if (FileUpload1.PostedFile.ContentType == "image/jpeg" || FileUpload1.PostedFile.ContentType == "image/png")
                        {
                            // only files smaller than 512kb are accepted
                            if (FileUpload1.PostedFile.ContentLength < 512000)
                            {
                                try
                                {
                                    string filename = Path.GetFileName(FileUpload1.FileName);
                                    FileUpload1.SaveAs("C:\\temp\\" + filename);
                                    serviceObject.Icon = filename;
                                }
                                catch (Exception ex)
                                {
                                    LabelUploadStatus.Text = "The file could not be uploaded. The following error occured: " + ex.Message;
                                    LabelUploadStatus.ForeColor = Color.Red;
                                    return;
                                }
                            }
                            else
                            {
                                LabelUploadStatus.Text = "The file is too large. Please use a smaller file";
                                LabelUploadStatus.ForeColor = Color.Red;
                                return;
                            }
                        }
                        else
                        {
                            LabelUploadStatus.Text = "Please select a JPEG or a PNG file";
                            LabelUploadStatus.ForeColor = Color.Red;
                            return;
                        }
                    }
                    else
                    {
                        serviceObject.Icon = "";
                    }
                }
                else if (RadioButtonLevel2.Checked)
                {
                    serviceObject.Level = 2;
                    serviceObject.ParentServiceId = RadComboBoxParent1.SelectedValue;
                }
                else if (RadioButtonLevel3.Checked)
                {
                    serviceObject.Level = 3;
                    serviceObject.ParentServiceId = RadComboBoxParent2.SelectedItem.Value;
                }

                bool add = servicesController.UpdateService(serviceObject);
                if (add)
                {
                    LabelUpdate.Text = "Service successfully changed";
                    LabelUpdate.ForeColor = Color.Green;
                }
                else
                {
                    LabelUpdate.Text = "Saving changes failed. See logs for details.";
                    LabelUpdate.ForeColor = Color.Red;
                }
            }
        }

        protected void DeleteButton_Click(object sender, EventArgs e)
        {
            bool deleted = servicesController.DeleteService(serviceObject);

            if (deleted)
            {
                LabelDelete.Text = "Service successfully deleted";
                LabelDelete.ForeColor = Color.Green;
            }
            else
            {
                LabelDelete.Text = "Deleting service failed. See logs for details.";
                LabelDelete.ForeColor = Color.Red;
            }
        }

        protected void RadXmlHttpPanelParent2_ServiceRequest(object sender, Telerik.Web.UI.RadXmlHttpPanelEventArgs e)
        {
            parent1Id = e.Value;
            Service serviceTemp = servicesController.GetService(parent1Id);
            Service[] servicesLevelTwo = servicesController.GetChildren(serviceTemp);

            RadComboBoxParent2.ClearSelection();
            RadComboBoxParent2.Items.Clear();

            foreach (Service service in servicesLevelTwo)
            {
                RadComboBoxItem item = new RadComboBoxItem();
                item.Value = service.ServiceId.ToString();
                item.Text = service.ServiceName;
                RadComboBoxParent2.Items.Add(item);
            }
            RadComboBoxParent2.Items.Insert(0, new RadComboBoxItem(""));
            RadComboBoxParent2.SelectedIndex = 0;
            
            //Store the parent ID for successive pageloads
            this.Session["ServiceDetails_Parent1"] = parent1Id;
        }
        
        public BOServicesControllerWS servicesController;
        public string parent1Id = "";
        public string parent2Id = "";
        public Service serviceObject;
    }
}