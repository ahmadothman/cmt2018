﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;

namespace FOWebApp.Nocsa
{
    public partial class TerminalActivityLogList : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            RadDatePickerStart.SelectedDate = DateTime.Now.AddDays(-1.0);
            RadDatePickerEnd.SelectedDate = DateTime.Now;
        }

        protected void RadGridTerminalActivityList_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {

        }

        protected void ImageButtonQuery_Click(object sender, EventArgs e)
        {
            DateTime startDt;
            DateTime endDt;

            startDt = (DateTime)RadDatePickerStart.SelectedDate;
            endDt = (DateTime)RadDatePickerEnd.SelectedDate;

            // Include end date in the calculation to create the report
            endDt = endDt.AddDays(1);

            BOLogControlWS boLogController = new BOLogControlWS();
            DataTable dtTable = boLogController.GetTermActivitiesBetweenDates(startDt, endDt);

            RadGridTerminalActivityList.DataSource = dtTable;
            RadGridTerminalActivityList.DataBind();
            RadGridTerminalActivityList.Visible = true;
        }
    }
}