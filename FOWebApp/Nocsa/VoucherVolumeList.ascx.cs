﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using FOWebApp.net.nimera.cmt.BOVoucherControllerWSRef;

namespace FOWebApp.Nocsa
{
    public partial class VoucherVolumeList : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BOVoucherControllerWS voucherController = new BOVoucherControllerWS();
            VoucherVolume[] vvList = voucherController.GetVoucherVolumes();
            RadGridVoucherVolumes.DataSource = vvList;
            RadGridVoucherVolumes.DataBind();
        }

        protected void RadGridVoucherVolumes_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridItemCollection selectedItems = RadGridVoucherVolumes.SelectedItems;

            if (selectedItems.Count > 0)
            {
                GridItem selectedItem = selectedItems[0];
                VoucherVolume vv = (VoucherVolume)selectedItem.DataItem;
                string id = vv.Id.ToString();
            }
        }

        protected void RadGridVoucherVolumes_ItemDataBound(object sender, GridItemEventArgs e)
        {
        }

        protected void RadWindowManager1_Disposed(object sender, EventArgs e)
        {
        }

        protected void RadWindowManager1_Unload(object sender, EventArgs e)
        {
        }

        protected void RadGridVoucherVolumes_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            this.Session["_VoucherVolumeList_PageSize"] = e.NewPageSize;
        }

        protected void RadGridVoucherVolumes_ItemCommand(object sender, GridCommandEventArgs e)
        {
        }
    }
}