﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserMaintenanceList.ascx.cs" Inherits="FOWebApp.Nocsa.UserMaintenanceList" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script type="text/javascript">
    function RowSelected(sender, eventArgs) {
        var grid = sender;
        var masterDataView = grid.get_masterTableView();
        var rowNum = eventArgs.get_itemIndexHierarchical();
        var row = masterDataView.get_dataItems()[rowNum];
        var userName = masterDataView.getCellByColumnUniqueName(row, "UserNameColumn").innerHTML;

        //Initialize and show the terminal details window
        var oWnd = radopen("Nocsa/ModifyUser.aspx?UserName=" + userName, "RadWindowModifyUser");
    }
</script>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxyUserMaintenanceList" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="RadGridUserMaintenanceList">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGridUserMaintenanceList" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadGrid ID="RadGridUserMaintenanceList" runat="server"
    AllowFilteringByColumn="True" AllowPaging="false" AllowSorting="True"
    CellSpacing="0" GridLines="None"
    OnNeedDataSource="RadGridUserMaintenanceList_NeedDataSource" Skin="Metro"
    Width="100%" AutoGenerateColumns="False" PageSize="20"
    ShowStatusBar="True" EnableAriaSupport="True"
    OnDeleteCommand="RadGridUserMaintenanceList_DeleteCommand"
    OnItemCommand="RadGridUserMaintenanceList_ItemCommand"
    OnPageIndexChanged="RadGridUserMaintenanceList_PageIndexChanged">
    <GroupingSettings CaseSensitive="false" />
    <ExportSettings IgnorePaging="True" OpenInNewWindow="True">
    </ExportSettings>
    <ClientSettings EnableRowHoverStyle="true">
        <Selecting AllowRowSelect="True" />
        <ClientEvents OnRowSelected="RowSelected" />
    </ClientSettings>
    <MasterTableView PageSize="20" EditMode="PopUp" CommandItemSettings-ShowExportToCsvButton="True" CommandItemSettings-ShowExportToExcelButton="True" CommandItemSettings-ShowExportToPdfButton="False" CommandItemSettings-ShowExportToWordButton="True" CommandItemSettings-ShowAddNewRecordButton="False" CommandItemDisplay="Top" DataKeyNames="User.ProviderUserKey,User.UserName">
        <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>

        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
            <HeaderStyle Width="20px"></HeaderStyle>
        </RowIndicatorColumn>

        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
            <HeaderStyle Width="20px"></HeaderStyle>
        </ExpandCollapseColumn>

        <Columns>
            <telerik:GridBoundColumn DataField="User.ProviderUserKey"
                FilterControlAltText="Filter UserIdColumn column" HeaderText="User Id"
                UniqueName="UserIdColumn" Visible="False" AllowSorting="False" AllowFiltering="False" MaxLength="10">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="User.UserName"
                FilterControlAltText="Filter UserNameColumn column" HeaderText="User Name"
                ShowFilterIcon="False" UniqueName="UserNameColumn"
                AndCurrentFilterFunction="Contains" AutoPostBackOnFilter="True">
                <ItemStyle Wrap="False" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="User.CreationDate"
                FilterControlAltText="Filter CreationDateColumn column" HeaderText="Created"
                UniqueName="CreationDateColumn" AllowFiltering="False">
                <ItemStyle Wrap="False" />
            </telerik:GridBoundColumn>
            <%-- Issue CMTFO-84 --%>
            <telerik:GridBoundColumn DataField="RoleName" HeaderStyle-Width="150px"
                FilterControlAltText="Filter RoleNameColumn column" HeaderText="Role"
                UniqueName="RoleNameColumn" AutoPostBackOnFilter="true"
                AndCurrentFilterFunction="Contains" ShowFilterIcon="False">
                <FilterTemplate>
                    <telerik:RadComboBox ID="RadComboBoxRoleFilterTemplate" runat="server" Skin="Metro"
                        SelectedValue='<%# ((GridItem)Container).OwnerTableView.GetColumn("RoleNameColumn").CurrentFilterValue %>'
                        OnClientSelectedIndexChanged="RoleFilterTemplate_SelectedIndexChanged" OnInit="RadComboBoxRoleFilterTemplate_Init"
                        OnDataBound="RadComboBoxRoleFilterTemplate_DataBound">
                    </telerik:RadComboBox>
                    <telerik:RadScriptBlock ID="RadScriptBlock2" runat="server">
                        <script type="text/javascript">
                            function RoleFilterTemplate_SelectedIndexChanged(sender, args) {
                                var tableView = $find('<%# ((GridItem)Container).OwnerTableView.ClientID %>');
                                var selectedValue = args.get_item().get_value();

                                if (selectedValue === 'NoFilter') {
                                    tableView.filter('RoleNameColumn', selectedValue, 'NoFilter');
                                } else {
                                    tableView.filter('RoleNameColumn', selectedValue, 'EqualTo');
                                }
                            }
                        </script>
                    </telerik:RadScriptBlock>
                </FilterTemplate>
                <ItemStyle Wrap="False" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="DistName" FilterControlAltText="Filter DistributorColumn column" 
                HeaderText="Distributor" ShowFilterIcon="False" UniqueName="DistributorColumn" 
                AndCurrentFilterFunction="Contains" AutoPostBackOnFilter="True">
                <ItemStyle Wrap="false" />
            </telerik:GridBoundColumn>
            <telerik:GridCheckBoxColumn DataField="User.IsOnline" 
                FilterControlAltText="Filter IsOnlineColumn column" HeaderText="Online" 
                UniqueName="IsOnlineColumn" AndCurrentFilterFunction="EqualTo"
                ShowFilterIcon="False" AutoPostBackOnFilter="True">
            </telerik:GridCheckBoxColumn>
            <telerik:GridCheckBoxColumn DataField="User.IsApproved" DataType="System.Boolean"
                FilterControlAltText="Filter IsApprovedColumn column" HeaderText="Approved"
                UniqueName="IsApprovedColumn" AndCurrentFilterFunction="EqualTo"
                ShowFilterIcon="False" AutoPostBackOnFilter="True">
            </telerik:GridCheckBoxColumn>
            <telerik:GridCheckBoxColumn DataField="User.IsLockedOut"
                FilterControlAltText="Filter IsLockedOutColumn column" HeaderText="Locked Out"
                UniqueName="IsLockedOutColumn" AndCurrentFilterFunction="EqualTo"
                ShowFilterIcon="False" AutoPostBackOnFilter="True">
            </telerik:GridCheckBoxColumn>
            <telerik:GridBoundColumn DataField="User.LastActivityDate"
                DataType="System.DateTime"
                FilterControlAltText="Filter LastActivityDateColumn column"
                HeaderText="Last Activity Date" UniqueName="LastActivityDateColumn"
                AllowFiltering="False">
                <ItemStyle Wrap="False" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="User.LastLoginDate"
                FilterControlAltText="Filter LastLoginDateColumn column"
                HeaderText="Last Login Date" UniqueName="LastLoginDateColumn"
                AllowFiltering="False">
                <ItemStyle Wrap="False" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="User.LastPasswordChangedDate"
                FilterControlAltText="Filter PasswordChangedDateColumn column"
                HeaderText="Password Changed" UniqueName="PasswordChangedDateColumn"
                AllowFiltering="False">
                <ItemStyle Wrap="False" />
            </telerik:GridBoundColumn>
            <telerik:GridButtonColumn
                HeaderText="Delete" ImageUrl="~/Images/delete.png"
                ConfirmText="Are you sure you want to delete this User?\n\nWARNING: After deleting the user, please refresh the list."
                UniqueName="DeleteColumn" ButtonType="ImageButton"
                HeaderButtonType="PushButton" Text="Delete, does not work when filtering is on!" CommandName="Delete">
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </telerik:GridButtonColumn>
        </Columns>

        <EditFormSettings>
            <EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
        </EditFormSettings>
    </MasterTableView>

    <FilterMenu EnableImageSprites="False"></FilterMenu>

    <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_WebBlue"></HeaderContextMenu>
</telerik:RadGrid>
<telerik:RadWindowManager ID="RadWindowManager1" runat="server" Skin="Metro" DestroyOnClose="True">
    <Windows>
        <telerik:RadWindow ID="RadWindowModifyUser" runat="server" Animation="None" Opacity="100"
            Skin="Metro" Title="Modify Terminal" AutoSize="True" Width="900px" Height="800px"
            KeepInScreenBounds="True" EnableShadow="True" EnableAriaSupport="True" VisibleStatusbar="True"
            Modal="True" DestroyOnClose="False">
        </telerik:RadWindow>
    </Windows>
</telerik:RadWindowManager>
