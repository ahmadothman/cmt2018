﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VoucherVolumeDetailsForm.ascx.cs" Inherits="FOWebApp.Nocsa.VoucherVolumeDetailsForm" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
    <script type="text/javascript">
        function DoBlur(fld) {
            fld.className = 'normalfld';
        }

        function DoFocus(fld) {
            fld.className = 'focusfld';
        }

        function DoChange(fld) {
            fld.className = 'changedfld';
        }
    </script>
</telerik:RadCodeBlock>
<telerik:RadAjaxPanel ID="RadAjaxPanelVoucherVolume" runat="server" GroupingText="Voucher volume" Height="200px" Width="832px"
    EnableHistory="True" Style="margin-right: 114px">
<table cellpadding="10" cellspacing="5" class="VoucherVolume">
    <tr>
        <td>
            Volume ID:
        </td>
        <td>
            <asp:Label ID="LabelId" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            Description:
        </td>
        <td>
            <telerik:RadTextBox ID="RadTextBoxDescription" runat="server" Width="200px"></telerik:RadTextBox>
        </td>
    </tr>
    <tr>
        <td>
            Volume (MB):
        </td>
        <td>
            <asp:Label ID="LabelVolumeMB" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>SLA</td>
        <td>
            <asp:Label ID="LabelSLA" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            Validity period (days):
        </td>
        <td>
            <asp:Label ID="LabelValidityPeriod" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            Unit price (EUR):
        </td>
        <td>
            <telerik:RadNumericTextBox ID="RadNumericTextBoxUnitPriceEUR" runat="server" ShowSpinButtons="true" NumberFormat-DecimalDigits="2" Width="100px" MinValue="0" Value="0"></telerik:RadNumericTextBox>
        </td>
    </tr>
    <tr>
        <td>
            Unit price (USD):
        </td>
        <td>
            <telerik:RadNumericTextBox ID="RadNumericTextBoxUnitPriceUSD" runat="server" ShowSpinButtons="true" NumberFormat-DecimalDigits="2" Width="100px" MinValue="0" Value="0"></telerik:RadNumericTextBox>
        </td>
    </tr>
    <tr>
        <td>
            ISP:
        </td>
        <td>
            <asp:Label ID="LabelISP" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:button ID="ButtonSubmit" text="Submit" runat="server" OnClick="ButtonSubmit_Click" />
        </td>
        <td>
            <asp:button ID="ButtonDelete" text="Delete" runat="server" OnClick="ButtonDelete_Click" />
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <asp:Label ID="LabelSubmit" runat="server"></asp:Label>
        </td>
    </tr>
</table>
</telerik:RadAjaxPanel>