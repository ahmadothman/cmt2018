﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ModifyUser.aspx.cs" Inherits="FOWebApp.Nocsa.ModifyUser" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Modify User</title>
    <link href="~/CMTStyle.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .style1
        {
            width: 454px;
        }
    </style>
</head>
<body>
    <form id="formModifyUser" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div>
        <h1>
            Modify user</h1>
        <asp:Panel ID="PanelUserData" runat="server" BorderStyle="Groove" BorderWidth="1px"
            BorderColor="#666666" ToolTip="User Data" Wrap="False" CssClass="datapanel">
            <table cellpadding="10" cellspacing="5">
                <tr>
                    <td>
                        User Name:
                    </td>
                    <td class="style1">
                        <asp:TextBox ID="TextBoxUserName" runat="server" Width="301px" Enabled="False" 
                            ReadOnly="True"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        E-Mail Address:
                    </td>
                    <td class="style1">
                        <asp:TextBox ID="TextBoxEMailAddress" runat="server" Width="297px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Role:
                    </td>
                    <td class="style1">
                        <telerik:RadComboBox ID="RadComboBoxRoles" runat="server" Enabled="true" Skin="Metro"></telerik:RadComboBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="LabelRelatedEntity" runat="server"></asp:Label>
                    </td>
                    <td class="style1">
                        <telerik:RadComboBox ID="RadComboBoxRelatedDistributor" runat="server" Visible="false" Skin="Metro"></telerik:RadComboBox>
                        <telerik:RadComboBox ID="RadComboBoxRelatedOrganization" runat="server" Visible="false" Skin="Metro"></telerik:RadComboBox>
                        <telerik:RadComboBox ID="RadComboBoxRelatedEnduser" runat="server" Visible="false" Skin="Metro"></telerik:RadComboBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:CheckBox ID="CheckBoxActivated" runat="server" Text="Activated:" TextAlign="Left" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox ID="CheckBoxLockedOut" runat="server" Text="Locked out:" TextAlign="Left" />
                    </td>
                    <td>
                        <asp:Label ID="LabelLockOutDate" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="ButtonCreate" runat="server" Text="Modify User" 
                            onclick="ButtonCreate_Click" />
                    </td>
                    <td>
                        <asp:Button ID="ButtonPasswordReset" runat="server" Text="Password Reset" 
                            onclick="ButtonPasswordReset_Click" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <br />
        <p>
            <asp:Label ID="LabelStatus" runat="server"></asp:Label>
            <asp:HiddenField ID="HiddenFieldRole" runat="server" />
            <asp:HiddenField ID="HiddenFieldRelatedEntity" runat="server" />
        </p>
    </div>
    </form>
</body>
</html>
