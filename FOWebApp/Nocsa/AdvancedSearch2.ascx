﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdvancedSearch2.ascx.cs"
    Inherits="FOWebApp.Nocsa.AdvancedSearch2" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<telerik:RadCodeBlock ID="RadCodeBlockAdvancedSearch2" runat="server">
    <style type="text/css">
        .rgAltRow, .rgRow {
            cursor: pointer !important;
        }

        /*.rcbScroll {
            height: auto !important;
            min-height: 200px !important;
            max-height: 400px !important;
        }*/

        #terminalCount {
            width: 100px;
        }

        #terminalCountNumber {
            width: 50px;
        }

        #excelButton {
            width: auto;
            text-align: right;
        }
        /** {
            border: 1px solid;
        }*/
    </style>
    <link href="../Content/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function RowSelected(sender, eventArgs) {
            var grid = sender;
            var masterDataView = grid.get_masterTableView();
            var rowNum = eventArgs.get_itemIndexHierarchical();
            var row = masterDataView.get_dataItems()[rowNum];
            var macAddress = masterDataView.getCellByColumnUniqueName(row, "MacAddress").innerHTML;

            //Initialize and show the terminal details window
            var oWnd = radopen("Nocsa/TerminalForm.aspx?mac=" + macAddress, "RadWindowModifyTerminal");
        }

        function CommandSelected(sender, eventArgs) {
            var grid = sender;
            if (eventArgs.get_commandName() == "ModifyTerminal") {
                var macAddress = eventArgs.get_commandArgument();
                alert(macAddress);
                var oWnd = radopen("Nocsa/TerminalForm.aspx?mac=" + macAddress, "RadWindowModifyTerminal");
            }
        }

        function nodeClicking(sender, args) {
            var comboBox = $find("<%= RadComboBoxISP.ClientID %>");

            var node = args.get_node()
            comboBox.set_text(node.get_text());
            comboBox.set_value(node.get_value());
            comboBox.trackChanges();
            comboBox.get_items().getItem(0).set_text(node.get_text());
            comboBox.commitChanges();

            comboBox.hideDropDown();
        }

        function RadComboBoxISP_SelectedIndexChanged(sender, eventArgs) {
            var item = eventArgs.get_item();
            var panel = $find('<%= RadXmlHttpPanelSla.ClientID %>');
            panel.set_value(item.get_value());
            return false;
        }

        function ClearForm() {
            $find("<%= RadTextBoxFullName.ClientID %>").clear();
            $find("<%= RadComboBoxISP.ClientID %>").clearSelection();
            $find("<%= RadMaskedTextBoxSitId.ClientID %>").clear();
            $find("<%= RadComboBoxDistributor.ClientID %>").clearSelection();
            $find("<%= RadComboBoxAdminState.ClientID %>").clearSelection();
            $find("<%= RadTextBoxIPAddress.ClientID %>").clear();
            $find("<%= RadTextBoxMacAddress.ClientID %>").clear();
            $find("<%= RadTextBoxSerial.ClientID %>").clear();
            $find("<%= RadComboBoxSLA.ClientID %>").clearSelection();
            $find("<%= RadMaskedTextBoxFWDPool.ClientID  %>").clear();
            $find("<%= RadMaskedTextBoxRTNPool.ClientID %>").clear();
            var combo = $find("<%= RadComboBoxExpiryDate.ClientID %>");
            combo.trackChanges();
            combo.get_items().getItem(0).select();
            combo.updateClientState();
            combo.commitChanges();
            $find("<%= RadTextBoxEMail.ClientID %>").clear();
        }
    </script>
</telerik:RadCodeBlock>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="RadButtonQuery">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="LabelNumberOfTerminals" />
                <telerik:AjaxUpdatedControl ControlID="LabelNumTerminals" />
                <telerik:AjaxUpdatedControl ControlID="RadGridTerminals" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<asp:Panel ID="searchTerminalsPanel" runat="server" DefaultButton="RadButtonQuery">
    <table>
        <tr>
            <td valign="top">
                <table class="table">

                    <%-- <tr>
                    <td colspan="2">ISP:
                    </td>
                </tr>--%>
                    <tr>
                        <td colspan="2">
                            <telerik:RadComboBox ID="RadComboBoxISP" runat="server" Skin="Metro" Sort="Ascending" EnableVirtualScrolling="True"
                                EmptyMessage="ISP/Satellite" MaxHeight="100px" OnClientSelectedIndexChanged="RadComboBoxISP_SelectedIndexChanged"
                                RenderMode="Lightweight" Width="100%">
                                <ItemTemplate>
                                    <div>
                                        <telerik:RadTreeView ID="RadTreeViewSatellite" runat="server" Skin="Metro" OnClientNodeClicking="nodeClicking">
                                        </telerik:RadTreeView>
                                    </div>
                                </ItemTemplate>
                                <Items>
                                    <telerik:RadComboBoxItem Text="" />
                                </Items>
                                <ExpandAnimation Type="Linear" />
                            </telerik:RadComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <telerik:RadXmlHttpPanel ID="RadXmlHttpPanelSla" runat="server" EnableClientScriptEvaluation="True"
                                OnServiceRequest="RadXmlHttpPanelSla_ServiceRequest">
                                <telerik:RadComboBox ID="RadComboBoxSLA" runat="server" Skin="Metro" EmptyMessage="SLA" Width="100%">
                                </telerik:RadComboBox>
                            </telerik:RadXmlHttpPanel>
                        </td>
                    </tr>
                    <%--<tr>
                    <td colspan="2">SIT:
                    </td>
                </tr>--%>

                    <%--<tr>
                    <td colspan="2">Terminal Name:
                    </td>
                </tr>--%>

                    <%--<tr>
                    <td colspan="2">Distributor/VNO:
                    </td>
                </tr>--%>
                    <tr>
                        <td colspan="2">
                            <telerik:RadComboBox ID="RadComboBoxDistributor" runat="server" Skin="Metro" EmptyMessage="Distributor/VNO" Width="100%">
                            </telerik:RadComboBox>
                        </td>
                    </tr>
                    <%--  <tr>
                    <td colspan="2">Admin State:
                    </td>
                </tr>--%>
                    <tr>
                        <td colspan="2">
                            <telerik:RadComboBox ID="RadComboBoxAdminState" runat="server" Skin="Metro" EmptyMessage="Admin State">
                            </telerik:RadComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td>SIT:</td>
                        <td>
                            <telerik:RadMaskedTextBox ID="RadMaskedTextBoxSitId" runat="server" Mask="############" Skin="Metro">
                            </telerik:RadMaskedTextBox>
                        </td>
                        <%-- <td >
                        <telerik:RadMaskedTextBox ID="RadMaskedTextBoxSitId" runat="server" Mask="############">
                        </telerik:RadMaskedTextBox>
                    </td>--%>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <telerik:RadTextBox ID="RadTextBoxFullName" runat="server" Skin="Metro" EmptyMessage="Enter terminal name" Width="100%">
                            </telerik:RadTextBox>
                        </td>
                    </tr>
                    <%-- <tr>
                    <td colspan="2">IP Address:
                    </td>
                </tr>--%>
                    <tr>
                        <td colspan="2">
                            <telerik:RadTextBox ID="RadTextBoxIPAddress" runat="server" Columns="39" EmptyMessage="Enter an IP address" Skin="Metro" Width="100%">
                            </telerik:RadTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Static IP terminals only:
                        </td>
                        <td>
                            <asp:CheckBox ID="CheckBoxStaticIP" runat="server" />
                        </td>
                    </tr>
                    <%-- <tr>
                    <td colspan="2">MAC Address:
                    </td>
                </tr>--%>
                    <tr>
                        <td colspan="2">
                            <telerik:RadTextBox ID="RadTextBoxMacAddress" runat="server" EmptyMessage="Enter a MAC address" Skin="Metro">
                            </telerik:RadTextBox>
                        </td>
                    </tr>
                    <%-- <tr>
                    <td colspan="2">Serial:
                    </td>
                </tr>--%>
                    <tr>
                        <td colspan="2">
                            <telerik:RadTextBox ID="RadTextBoxSerial" runat="server" EmptyMessage="Enter a serial number" Skin="Metro">
                            </telerik:RadTextBox>
                        </td>
                    </tr>
                    <%--<tr>
                    <td colspan="2">SLA:
                    </td>
                </tr>--%>

                    <tr>
                        <td>Free Zone:
                        </td>
                        <td>
                            <asp:CheckBox ID="CheckBoxFreeZone" runat="server" />
                        </td>
                    </tr>
                    <%--<tr>
                    <td colspan="2">Forward pool:
                    </td>
                </tr>--%>
                    <tr>
                        <td>Forward pool:
                        <telerik:RadMaskedTextBox ID="RadMaskedTextBoxFWDPool" runat="server"
                            Skin="Metro" Mask="#####" Width="50px">
                        </telerik:RadMaskedTextBox>
                        </td>
                        <td>Return pool:
                         <telerik:RadMaskedTextBox ID="RadMaskedTextBoxRTNPool" runat="server"
                             Skin="Metro" Mask="#####" Width="50px">
                         </telerik:RadMaskedTextBox>
                        </td>
                    </tr>
                    <%--<tr>
                    <td colspan="2">Return pool:
                    </td>
                </tr>--%>
                    <%--<tr>
                    <td>Return pool:
                    </td>
                    <td>
                        <telerik:RadMaskedTextBox ID="RadMaskedTextBoxRTNPool" runat="server"
                            Skin="Metro" Mask="#####" Width="50px">
                        </telerik:RadMaskedTextBox>
                    </td>
                </tr>--%>
                    <%-- <tr>
                    <td colspan="2">Expiration Date:
                    </td>
                </tr>--%>
                    <tr>
                        <td>
                            <telerik:RadComboBox ID="RadComboBoxExpiryDate" runat="server" Skin="Metro" EmptyMessage="Expiration date">
                                <Items>
                                    <telerik:RadComboBoxItem runat="server" Text="expires before" Value="&lt;" Selected="true" />
                                    <telerik:RadComboBoxItem runat="server" Text="expires after" Value="&gt;" />
                                    <telerik:RadComboBoxItem runat="server" Text="expires on" Value="=" />
                                    <%--<telerik:RadComboBoxItem runat="server" Text="=&lt;" Value="=&lt;" />
                                <telerik:RadComboBoxItem runat="server" Text="&gt;=" Value="&gt;=" />--%>
                                </Items>
                            </telerik:RadComboBox>
                        </td>
                        <td>
                            <telerik:RadDatePicker ID="RadDatePickerExpiryDate" runat="server" Skin="Metro">
                                <Calendar ID="Calendar1" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False"
                                    ViewSelectorText="x" Skin="Metro" runat="server">
                                </Calendar>
                                <DateInput ID="DateInput1" DisplayDateFormat="d/MM/yyyy" DateFormat="d/MM/yyyy"
                                    runat="server">
                                </DateInput>
                                <%--<DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>--%>
                            </telerik:RadDatePicker>
                        </td>
                    </tr>
                    <%--<tr>
                    <td colspan="2">E-Mail:
                    </td>
                </tr>--%>
                    <tr>
                        <td colspan="2">
                            <telerik:RadTextBox ID="RadTextBoxEMail" runat="server" EmptyMessage="Enter an email" Skin="Metro">
                            </telerik:RadTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Redundant Setup only:
                        </td>
                        <td>
                            <asp:CheckBox ID="CheckBoxRedundantSetup" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>Non-Billable terminals only:
                        </td>
                        <td>
                            <asp:CheckBox ID="CheckBoxTestTerminal" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <telerik:RadButton ID="RadButtonClear" runat="server" Text="Clear Form"
                                Skin="Metro" OnClientClicked="ClearForm" AutoPostBack="False">
                            </telerik:RadButton>
                        </td>
                        <td class="text-right">
                            <telerik:RadButton ID="RadButtonQuery" runat="server" Text="Search Terminals"
                                OnClick="RadButtonQuery_Click" Skin="Metro">
                            </telerik:RadButton>
                        </td>
                    </tr>
                </table>
            </td>
            <td valign="top">
                <table>
                    <tr>
                        <td id="terminalCount">
                            <asp:Label ID="LabelNumberOfTerminals" runat="server" Text="Terminal count:" Visible="False"></asp:Label>
                        </td>
                        <td id="terminalCountNumber">
                            <asp:Label ID="LabelNumTerminals" Text="0" runat="server" Visible="False" ForeColor="#0033CC"></asp:Label>
                        </td>
                        <td id="excelButton">
                            <asp:ImageButton ID="ImageButtonExcelExport" runat="server"
                                ImageUrl="~/Images/Excel.png" OnClick="ImageButtonExport_Click"
                                ToolTip="Export as an Excel file" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <telerik:RadGrid ID="RadGridTerminals" runat="server" Skin="Metro"
                                AutoGenerateColumns="False" CellSpacing="0" GridLines="None" Width="100%"
                                Visible="False" AllowSorting="True" ShowFooter="True" OnItemDataBound="RadGridTerminals_ItemDataBound" ClientSettings-Resizing-AllowColumnResize="true" Height="600px">
                                <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True" EnableRowHoverStyle="true">
                                    <Selecting AllowRowSelect="True" />
                                    <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                                    <ClientEvents OnRowSelected="RowSelected" />
                                </ClientSettings>
                                <MasterTableView>
                                    <CommandItemTemplate>
                                        <div>
                                            <div style="float: left">
                                                <asp:Label ID="LabelTotals" runat="server" Text="Label"></asp:Label>
                                            </div>
                                        </div>
                                    </CommandItemTemplate>
                                    <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>

                                    <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
                                        <HeaderStyle Width="20px"></HeaderStyle>
                                    </RowIndicatorColumn>

                                    <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
                                        <HeaderStyle Width="20px"></HeaderStyle>
                                    </ExpandCollapseColumn>

                                    <Columns>
                                        <telerik:GridBoundColumn DataField="FullName"
                                            FilterControlAltText="Filter FullName column" HeaderText="Name"
                                            UniqueName="FullName" MaxLength="50">
                                            <HeaderStyle Width="320" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="MacAddress"
                                            FilterControlAltText="Filter MacAddress column" HeaderText="MAC Address"
                                            UniqueName="MacAddress">
                                            <HeaderStyle Width="120" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="SlaId"
                                            FilterControlAltText="Filter SLA column" HeaderText="SLA"
                                            UniqueName="ServiceColumn">
                                            <HeaderStyle Width="120" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="SitId"
                                            FilterControlAltText="Filter SitId column" HeaderText="SIT" UniqueName="SitId">
                                            <HeaderStyle Width="70" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="IpAddress" FilterControlAltText="Filter IpAddress column" HeaderText="IP" UniqueName="IpAddress">
                                            <HeaderStyle Width="100" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="AdmStatus" FilterControlAltText="Filter AdmStatus column" HeaderText="Status" UniqueName="AdmStatus">
                                            <HeaderStyle Width="100" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="CNo" FilterControlAltText="Filter CNo column" HeaderText="C/No" UniqueName="CNo">
                                            <HeaderStyle Width="70" />
                                        </telerik:GridBoundColumn>
                                    </Columns>

                                    <EditFormSettings>
                                        <EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
                                    </EditFormSettings>
                                </MasterTableView>

                                <FilterMenu EnableImageSprites="False"></FilterMenu>

                                <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_WebBlue"></HeaderContextMenu>
                            </telerik:RadGrid>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Panel>

<telerik:RadWindowManager ID="RadWindowManager1" runat="server" Skin="Metro" DestroyOnClose="True" Left="0px" Right="0px">
    <Windows>
        <telerik:RadWindow ID="RadWindowTerminalDetails" runat="server"
            Animation="None" Opacity="100" Skin="Metro" Title="Terminal Details" AutoSize="False" Width="1000px" Height="700px" KeepInScreenBounds="True" EnableShadow="True" EnableAriaSupport="True" VisibleStatusbar="True"
            Modal="False" DestroyOnClose="False" RenderMode="Classic" Behaviors="Move,Resize,Close">
        </telerik:RadWindow>
        <telerik:RadWindow ID="RadWindowModifyTerminal" runat="server"
            Animation="None" Opacity="100" Skin="Metro" Title="Modify Terminal" AutoSize="False" Width="1000px" Height="700px"
            KeepInScreenBounds="True" EnableShadow="True" EnableAriaSupport="True" VisibleStatusbar="True"
            Modal="False" DestroyOnClose="False" RenderMode="Classic" Behaviors="Move,Resize,Close">
        </telerik:RadWindow>
        <telerik:RadWindow ID="RadWindowModifyActivity" runat="server"
            Animation="None" Opacity="100" Skin="Metro" Title="Modify Activity" AutoSize="False" Width="920px" Height="655px"
            KeepInScreenBounds="True" EnableShadow="True" EnableAriaSupport="True" VisibleStatusbar="True"
            Modal="False" DestroyOnClose="False" RenderMode="Classic" Behaviors="Move,Resize,Close">
        </telerik:RadWindow>
    </Windows>
</telerik:RadWindowManager>
