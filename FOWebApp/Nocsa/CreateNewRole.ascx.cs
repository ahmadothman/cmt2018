﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using FOWebApp.net.nimera.cmt.BOServicesControllerWSRef;
using System.Drawing;
using Telerik.Web.UI;

namespace FOWebApp.Nocsa
{
    public partial class CreateNewRole : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Load the Services Checkbox list
            PanelServicesAllocation.Visible = true;
            RadListBoxServices.Visible = true;
            RadListBoxItem listItem;
            boServiceControl = new BOServicesControllerWS();
            Service[] serviceList1 = boServiceControl.GetAllAvailableServices("1");
            foreach (Service serv1 in serviceList1)
            {
                listItem = new RadListBoxItem(serv1.ServiceName, serv1.ServiceId.ToString());
                RadListBoxServices.Items.Add(listItem);
                Service[] serviceList2 = boServiceControl.GetChildren(serv1);
                if (serviceList2.Length > 0)
                {
                    foreach (Service serv2 in serviceList2)
                    {
                        listItem = new RadListBoxItem("---" + serv2.ServiceName, serv2.ServiceId.ToString());
                        RadListBoxServices.Items.Add(listItem);
                        Service[] serviceList3 = boServiceControl.GetChildren(serv2);
                        if (serviceList3.Length > 0)
                        {
                            foreach (Service serv3 in serviceList3)
                            {
                                listItem = new RadListBoxItem("------" + serv3.ServiceName, serv3.ServiceId.ToString());
                                RadListBoxServices.Items.Add(listItem);
                            }
                        }
                    }
                }
            }
        }

        protected void ButtonCreate_Click(object sender, EventArgs e)
        {
            try
            {
                string roleName = TextBoxRoleName.Text.ToString();
                Roles.CreateRole(roleName);
                for (int i = 0; i < RadListBoxServices.Items.Count; i++)
                {
                    if (RadListBoxServices.Items[i].Checked)
                    {
                        boServiceControl.LinkServiceToRole(roleName, RadListBoxServices.Items[i].Value.ToString());
                    }
                }
                LabelCreate.Text = "Role successfully created";
                LabelCreate.ForeColor = Color.Green;
            }
            catch (Exception ex)
            {
                LabelCreate.Text = "Allocating services to role failed. Please check log for details.";
                LabelCreate.ForeColor = Color.Red;
            }
        }

        public BOServicesControllerWS boServiceControl;
    }
}