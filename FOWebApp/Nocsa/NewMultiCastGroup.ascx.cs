﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Drawing;
using Telerik.Web.UI;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOMultiCastGroupControllerWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;
using net.nimera.supportlibrary;

namespace FOWebApp.Nocsa
{
    public partial class NewMultiCastGroup : System.Web.UI.UserControl
    {
        BOAccountingControlWS _boAccountingControlWS = null;
        BOMultiCastGroupControllerWS _boMultiCastGroupController = null;
        BOLogControlWS _boLogControlWS = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            _boAccountingControlWS = new BOAccountingControlWS();
            _boMultiCastGroupController = new BOMultiCastGroupControllerWS();
            _boLogControlWS = new BOLogControlWS();

            //Load the drop down boxes
            //Fill the lookup comboboxes
            Isp[] IspList = _boAccountingControlWS.GetIsps();

            foreach (Isp isp in IspList)
            {
                RadComboBoxItem item = new RadComboBoxItem();
                item.Value = isp.Id.ToString();
                item.Text = isp.CompanyName;
                RadComboBoxISP.Items.Add(item);
            }

            AddInitialValueToComboBox(RadComboBoxISP);

            //Load customers into the combobox
            Distributor[] distributors = _boAccountingControlWS.GetDistributorsAndVNOs();

            foreach (Distributor distributor in distributors)
            {
                RadComboBoxItem item = new RadComboBoxItem();
                item.Value = distributor.Id.ToString();
                item.Text = distributor.FullName;
                RadComboBoxDistributor.Items.Add(item);
            }

            AddInitialValueToComboBox(RadComboBoxDistributor);

            //if (this.Session["NewMulticastGroup_Dist"] != null)
            //{
            //    RadComboBoxDistributor.SelectedValue = this.Session["NewMulticastGroup_Dist"].ToString();
            //}
        }

        protected void ButtonSubmit_Click(object sender, EventArgs e)
        {
            // Deletes the distributor ID that has been kept in the Session
            // This is done so the user has to select a distributor again when an error occurs
            // and he goes back to the page to make a new MultiCastGroup
            // That way, the information shown in the Customer Combobox is always correct
            //if (this.Session["NewMulticastGroup_Dist"] != null)
            //{
            //    this.Session.Remove("NewMulticastGroup_Dist");
            //}
            
            //Dump the data to the CMT Database and NMS
            //First calculate the MAC address from the IP Address
            string macAddress = MultiCastHelper.CreateMacFromIP(TextBoxIPAddress.Text);
            MultiCastGroup mc = new MultiCastGroup();

            //Determine the actual user
            MembershipUser user = Membership.GetUser();

            mc.IPAddress = TextBoxIPAddress.Text;
            mc.MacAddress = macAddress;
            mc.Name = TextBoxName.Text;
            mc.OrganizationId = Int32.Parse(RadComboBoxCustomer.SelectedValue);
            mc.portNr = Int32.Parse(TextBoxPortNr.Text);
            mc.sourceURL = TextBoxSourceURL.Text;
            mc.State = false;
            mc.UserId = (Guid)user.ProviderUserKey;
            mc.Bytes = 0L;
            mc.Deleted = false;
            mc.BandWidth = Int32.Parse(TextBoxBandwidth.Text);
            mc.FirstActivationDate = (DateTime)RadDatePickerFirstActivationDate.SelectedDate;
            mc.DistributorId = Int32.Parse(RadComboBoxDistributor.SelectedValue);
            mc.IspId = Int32.Parse(RadComboBoxISP.SelectedValue);

            // Sets the selected value of the RadComboBox of the distributors back to the initial value with empty text
            // That way, the information shown in the Customer Combobox is also correct after pressing the button and going to the server
            RadComboBoxDistributor.SelectedIndex = 0;

            if (_boMultiCastGroupController.CreateMultiCastGroup(mc))
            {
                LabelResult.ForeColor = Color.DarkGreen;
                LabelResult.Text = "Multicast group creation succeeded";
                TextBoxMacAddress.Text = macAddress;
            }
            else
            {
                LabelResult.ForeColor = Color.Red;
                LabelResult.Text = "Multicast group creation failed";
            }
        }

        protected void RadXmlHttpPanelCustomer_ServiceRequest(object sender, RadXmlHttpPanelEventArgs e)
        {
            string distId = e.Value.ToString();
            Organization[] orgItems = _boAccountingControlWS.GetOrganisationsByDistributorId(distId);

            if (!(orgItems.Length == 0))
            {
                RadComboBoxCustomer.Items.Clear();

                RadComboBoxCustomer.DataSource = orgItems;
                RadComboBoxCustomer.DataTextField = "FullName";
                RadComboBoxCustomer.DataValueField = "Id";
                RadComboBoxCustomer.DataBind();

                AddInitialValueToComboBox(RadComboBoxCustomer);
            }
            else
            {
                RadComboBoxCustomer.Text = "No customers defined for this distributor";
                RadComboBoxCustomer.Enabled = false;
            }

            //Store the isp for successive pageloads
            //this.Session["NewMulticastGroup_Dist"] = distId;
        }

        /// <summary>
        /// Inserts a RadComboBoxItem with an emtpy string as text and 0 as value in the RadComboBox at index 0
        /// </summary>
        /// <param name="comboBox">The RadComboBox in which to insert the RadComboBoxItem</param>
        private void AddInitialValueToComboBox(RadComboBox comboBox) 
        {
            RadComboBoxItem item = new RadComboBoxItem(string.Empty, "0");
            comboBox.Items.Insert(0, item);
        }
    }
}