﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace FOWebApp
{
    public partial class TerminalTabView : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void RadTabStripTerminalView_TabClick(object sender, Telerik.Web.UI.RadTabStripEventArgs e)
        {
            AddPageView(e.Tab);
            e.Tab.PageView.Selected = true;
        }

        private void AddPageView(RadTab tab)
        {
            RadPageView pageView = new RadPageView();
            pageView.ID = tab.Text;
            RadMultiPageTerminalView.PageViews.Add(pageView);
            pageView.CssClass = "pageView";
            tab.PageViewID = pageView.ID;
        }

        protected void RadMultiPageTerminalView_PageViewCreated(object sender, RadMultiPageEventArgs e)
        {
            Control control = null;
            //Add content to the page view depending on the selected tab
            if (e.PageView.ID.Equals("Terminal"))
            {
                //Load the TerminalDetailTab control
                TerminalDetailTab terminalDetailTab =
                    (TerminalDetailTab)Page.LoadControl("TerminalDetailTab.ascx");
                terminalDetailTab.MacAddress = LabelMacAddress.Text;
                control = terminalDetailTab;
                control.ID = e.PageView.ID + "_userControl";
            }
            else
            {
                control = Page.LoadControl("TestUserControl.ascx");
                control.ID = e.PageView.ID + "_userControl";
            }

            e.PageView.Controls.Add(control);
           
        }

        public void initTab(string macAddress)
        {
            RadTab terminalsTab = RadTabStripTerminalView.FindTabByText("Terminal");
            LabelMacAddress.Text = macAddress;
            AddPageView(terminalsTab);
        }

    }
}