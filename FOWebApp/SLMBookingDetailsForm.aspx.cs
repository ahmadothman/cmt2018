﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOSatLinkManagerControlWSRef;
using System.Drawing;

namespace FOWebApp
{
    public partial class SLMBookingDetailsForm : System.Web.UI.Page
    {
        SLMBooking booking;
        BOAccountingControlWS _accountingController = new BOAccountingControlWS();
        BOSatLinkManagerControlWS _satLinkController = new BOSatLinkManagerControlWS();

        protected void Page_Load(object sender, EventArgs e)
        {
            int cmtId = Convert.ToInt32(Request.Params["cmtid"].ToString());

            booking = _satLinkController.GetSLMBookingById(cmtId);

            if (booking.DistributorId != 0)
            {
                LabelDistOrCust.Text = "Distributor";
                LabelDistributorOrCustomer.Text = _accountingController.GetDistributor(booking.DistributorId).FullName;
            }
            else
            {
                LabelDistOrCust.Text = "Customer";
                LabelDistributorOrCustomer.Text = _accountingController.GetOrganization(booking.CustomerId).FullName;
            }
            LabelMacAddress.Text = booking.TerminalMac;
            LabelStartTime.Text = booking.StartTime.ToString();
            LabelEndTime.Text = booking.EndTime.ToString();
            LabelBookingSLA.Text = _accountingController.GetServicePack(booking.BookingSlaId).SlaName;
            LabelTerminalSLA.Text = _accountingController.GetServicePack(booking.TerminalSlaId).SlaName;
            LabelStatus.Text = booking.Status;
            LabelType.Text = booking.Type;
        }

        protected void ButtonApprove_Click(object sender, EventArgs e)
        {
            string result = _satLinkController.CreateSLMBookingInHub(booking.CMTId);
            if (result == "") //successful
            {
                LabelResult.ForeColor = Color.Green;
                result = "Booking successfully made in hub";
            }
            else
            {
                LabelResult.ForeColor = Color.Red;
            }
            LabelResult.Text = result;
        }

        protected void ButtonDecline_Click(object sender, EventArgs e)
        {
            booking.Status = "Declined";

            if (_satLinkController.UpdateSLMBooking(booking))
            {
                LabelResult.ForeColor = Color.Green;
                LabelResult.Text = "Booking succesfully declined";
            }
            else
            {
                LabelResult.ForeColor = Color.Red;
                LabelResult.Text = "Declining failed";
            }
        }
    }
}