﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GraphiteTestTab.ascx.cs" Inherits="FOWebApp.GraphiteTestTab" %>

<style type="text/css">
    .graphChart {
        height: 500px;
        width: 875px;
    }
</style>

<div style="padding-top: 5px; padding-bottom: 2px">
    <telerik:RadDatePicker ID="RadDatePickerStartDate" runat="server">
        <DateInput DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy"  runat="server"/> 
    </telerik:RadDatePicker>
    <telerik:RadDatePicker ID="RadDatePickerEndDate" runat="server">
        <DateInput DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy"  runat="server"/> 
    </telerik:RadDatePicker>
    <%--&nbsp&nbsp--%>
        <telerik:RadButton ID="RadButtonDateSelection" runat="server" Text="Show Chart"
            OnClick="RadButtonDateSelection_Click">
        </telerik:RadButton>
</div>
<div id='chart_container' class="graphChart" ></div>
<telerik:RadScriptBlock ID="RadScriptBlockHighchart" runat="server">
   <script type="text/javascript" src="../Scripts/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="../Scripts/highcharts.js"></script>
    <script type="text/javascript" src="../Scripts/exporting.js"></script>
    <script type="text/javascript" src="../Scripts/offline-exporting.js"></script>
</telerik:RadScriptBlock>

