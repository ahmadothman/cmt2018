﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;

namespace FOWebApp
{
    public partial class DistributorAccountForm : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            addBlurAtt(this);

            //Get the Distributor information an fill up the form
            //Load data into the grid view. This view lists all terminals for the 
            //authenticated user.
            BOAccountingControlWS boAccountingControl =
              new net.nimera.cmt.BOAccountingControlWSRef.BOAccountingControlWS();

            //Get the authenticated user
            MembershipUser myObject = Membership.GetUser();
            string userId = myObject.ProviderUserKey.ToString();


            //Get the Distributor linked to the user
            Distributor dist = boAccountingControl.GetDistributorForUser(userId);

            if (dist != null)
            {
                TextBoxAddressLine1.Text = dist.Address.AddressLine1;
                TextBoxAddressLine2.Text = dist.Address.AddressLine2;
                //TextBoxCountry.Text = dist.Address.Country;
                CountryList.SelectedValue = dist.Address.Country;
                TextBoxDistributorName.Text = dist.FullName;
                TextBoxEMail.Text = dist.Email;
                TextBoxFax.Text = dist.Fax;
                TextBoxLocation.Text = dist.Address.Location;
                TextBoxPCO.Text = dist.Address.PostalCode;
                TextBoxPhone.Text = dist.Phone;
                TextBoxRemarks.Text = dist.Remarks;
                TextBoxVAT.Text = dist.Vat;
                TextBoxWebSite.Text = dist.WebSite;
                LabelDistId.Text = "" + dist.Id;
            }
        }

        public void componentDataInit()
        {
            //Get the Distributor information an fill up the form
            //Load data into the grid view. This view lists all terminals for the 
            //authenticated user.
            BOAccountingControlWS boAccountingControl =
              new net.nimera.cmt.BOAccountingControlWSRef.BOAccountingControlWS();

            //Get the authenticated user
            MembershipUser myObject = Membership.GetUser();
            string userId = myObject.ProviderUserKey.ToString();


            //Get the Distributor linked to the user
            Distributor dist = boAccountingControl.GetDistributorForUser(userId);

            if (dist != null)
            {
                TextBoxAddressLine1.Text = dist.Address.AddressLine1;
                TextBoxAddressLine2.Text = dist.Address.AddressLine2;
                //TextBoxCountry.Text = dist.Address.Country;
                CountryList.SelectedValue = dist.Address.Country;
                TextBoxDistributorName.Text = dist.FullName;
                TextBoxEMail.Text = dist.Email;
                TextBoxFax.Text = dist.Fax;
                TextBoxLocation.Text = dist.Address.Location;
                TextBoxPCO.Text = dist.Address.PostalCode;
                TextBoxPhone.Text = dist.Phone;
                TextBoxRemarks.Text = dist.Remarks;
                TextBoxVAT.Text = dist.Vat;
                TextBoxWebSite.Text = dist.WebSite;
                LabelDistId.Text = "" + dist.Id;
            }
        }

        protected void ButtonSubmit_Click(object sender, EventArgs e)
        {
            //Read data into a Distributor object and perform an upgrade
            Distributor dist = new Distributor();
            dist.Address = new Address();

            Isp isp = new Isp();

            isp.Id = 112;
            dist.Isp = isp;

            try
            {
                dist.Id = Int32.Parse(LabelDistId.Text);
                dist.Address.AddressLine1 = TextBoxAddressLine1.Text;
                dist.Address.AddressLine2 = TextBoxAddressLine2.Text;
                //dist.Address.Country = TextBoxCountry.Text;
                dist.Address.Country = CountryList.SelectedValue;
                dist.FullName = TextBoxDistributorName.Text;
                dist.Email = TextBoxEMail.Text;
                dist.Fax = TextBoxFax.Text;
                dist.Address.Location = TextBoxLocation.Text;
                dist.Address.PostalCode = TextBoxPCO.Text;
                dist.Phone = TextBoxPhone.Text;
                dist.Remarks = TextBoxRemarks.Text;
                dist.Vat = TextBoxVAT.Text;
                dist.WebSite = TextBoxWebSite.Text;

                //Update data
                BOAccountingControlWS boAccountingControl =
                    new net.nimera.cmt.BOAccountingControlWSRef.BOAccountingControlWS();

                boAccountingControl.UpdateDistributor(dist);
            }
            catch (Exception ex)
            {
                BOLogControlWS boLogControlWS = new BOLogControlWS();
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDesc = ex.Message;
                cmtEx.ExceptionStacktrace = ex.StackTrace;
                boLogControlWS.LogApplicationException(cmtEx);
            }
        }

        //recursive function that adds attribute to all child controls 
        private void addBlurAtt(Control cntrl)
        {
            if (cntrl.Controls.Count > 0)
            {
                foreach (Control childControl in cntrl.Controls)
                {
                    addBlurAtt(childControl);
                }
            }
            if (cntrl.GetType() == typeof(TextBox))
            {
                TextBox TempTextBox = (TextBox)cntrl;
                //TempTextBox.Attributes.Add("onFocus", "DoFocus(this);");
                //TempTextBox.Attributes.Add("onBlur", "DoBlur(this);");
                TempTextBox.Attributes.Add("onchange", "DoChange(this);");
            }
            if (cntrl.GetType() == typeof(ListBox))
            {
                ListBox TempTextBox = (ListBox)cntrl;
                //TempTextBox.Attributes.Add("onFocus", "DoFocus(this);");
                //TempTextBox.Attributes.Add("onBlur", "DoBlur(this);");
                TempTextBox.Attributes.Add("onchange", "DoChange(this);");
            }
            if (cntrl.GetType() == typeof(DropDownList))
            {
                DropDownList TempTextBox = (DropDownList)cntrl;
                //TempTextBox.Attributes.Add("onFocus", "DoFocus(this);");
                //TempTextBox.Attributes.Add("onBlur", "DoBlur(this);");
                TempTextBox.Attributes.Add("onchange", "DoChange(this);");
            }
        } 


    }
}