﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TerminalManagementTab.ascx.cs"
    Inherits="FOWebApp.TerminalManagementTab" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<style type="text/css">
    .style1 {
        width: 489px;
    }
</style>
<telerik:RadScriptBlock ID="RadScriptBlockTerminalManagement" runat="server">
    <script type="text/javascript">
        var buttonClicked;

        function onFupButtonClicked(button, args) {
            if (window.confirm('Are you sure you want to reset the volume for this terminal?')) {
                button.set_autoPostBack(true);
            }
            else {
                button.set_autoPostBack(false);
            }
        }

        function onSlaButtonClicked(button, args) {
            if (window.confirm('Are you sure you want to change the SLA for this terminal?')) {
                button.set_autoPostBack(true);
            }
            else {
                button.set_autoPostBack(false);
            }
        }

        function onActionButtonClicked(button, args) {
            if (window.confirm('Are you sure you want to change the terminal activity?')) {
                button.set_autoPostBack(true);
            }
            else {
                button.set_autoPostBack(false);
            }
        }

        function onMigrateButtonClicked(button, args) {
            if (window.confirm('Are you sure you want to migrate this terminal to astra4a?')) {
                button.set_autoPostBack(true);
            }
            else {
                button.set_autoPostBack(false);
            }
        }

        function onCustomerChangeButtonClicked(button, args) {
            if (window.confirm('Are you sure you want to change the customer for this terminal?')) {
                button.set_autoPostBack(true);
            }
            else {
                button.set_autoPostBack(false);
            }
        }

        function showMacAddressHelp() {
            alert("A Mac Address can only be changed if the terminal is locked! Changing a Mac Address may take a few minutes to take effect.");
        }

        function showSlaHelp() {
            alert("Changing a Service Level Agreement is done by the NOCSA and might take a short while to complete.");
        }
    </script>
</telerik:RadScriptBlock>
<asp:Panel ID="Panel1" runat="server" GroupingText="Terminal Info">
    <table border="0" cellspacing="5px" class="termDetails" frame="void">
        <tr>
            <td width="200px">
                <asp:Literal ID="Literal1" Text="Expiry Date" runat="server" />
            </td>
            <td>
                <telerik:RadDatePicker ID="RadDatePickerExpiryDate" runat="server" Skin="Metro"
                    OnSelectedDateChanged="RadDatePickerExpiryDate_SelectedDateChanged" MinDate="2011-11-01"
                    Culture="nl-BE" FocusedDate="2011-11-01">
                    <Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x"
                        Skin="Metro">
                    </Calendar>
                    <DateInput DisplayDateFormat="d/MM/yyyy" DateFormat="d/MM/yyyy" EmptyMessage="Not Set">
                    </DateInput>
                    <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                </telerik:RadDatePicker>
            </td>
        </tr>
        <tr>
            <td>Change terminal name:
            </td>
            <td>
                <asp:TextBox ID="TextBoxTerminalName" runat="server" Columns="50" MaxLength="50" Enabled="False"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>Change Mac Address:
            </td>
            <td>
                <telerik:RadMaskedTextBox ID="RadMaskedTextBoxMacAddress" runat="server" Skin="Metro"
                    Width="125px">
                </telerik:RadMaskedTextBox>
                <asp:ImageButton ID="ImageButton1" runat="server" ImageAlign="Middle"
                    ImageUrl="~/Images/help2.png" Width="16px" OnClientClick="showMacAddressHelp()" />
                <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Please provide a valid MAC address!"
                    ControlToValidate="RadMaskedTextBoxMacAddress" Operator="NotEqual" ValueToCompare="00:00:00:00:00:00">
                </asp:CompareValidator>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <telerik:RadButton ID="RadButtonTerminalUpdate" runat="server"
                    Text="Update Terminal Info" OnClick="RadButtonTerminalUpdate_Click" Visible="False" Skin="Metro">
                </telerik:RadButton>
            </td>
        </tr>
    </table>
</asp:Panel>
<br />
<asp:Panel ID="Panel2" runat="server" GroupingText="Terminal State Management">
    <table border="0" cellspacing="5px" class="termDetails" frame="void">
        <tr>
            <td>
                <telerik:RadButton ID="RadButtonAction" runat="server" Text="Suspend Terminal" OnClick="RadButtonAction_Click"
                    Width="177px" CommadName="Suspend" OnClientClicked="onActionButtonClicked" AutoPostBack="False" Skin="Metro">
                </telerik:RadButton>
            </td>
        </tr>
        <tr>
            <td>
                <telerik:RadButton ID="RadButtonFUPReset" runat="server" Text="Reset FUP (Billable)"
                    ToolTip="Sets the consumed volume to 0. This is a billable operation!" Width="177px"
                    OnClick="RadButtonFUPReset_Click" OnClientClicked="onFupButtonClicked" AutoPostBack="False" Skin="Metro"/>
            </td>
        </tr>
    </table>
</asp:Panel>
<br />
<asp:Panel ID="Panel3" runat="server" GroupingText="SLA Management">
    <asp:HiddenField ID="HiddenFieldSLA" runat="server" />
    <table border="0" cellspacing="5px" class="termDetails" frame="void">
        <tr>
            <td>SLA:
            </td>
            <td class="style1">
                <telerik:RadComboBox ID="RadComboBoxSLA" runat="server" Skin="Metro" DropDownWidth="220px" Height="22px" Width="198px">
                </telerik:RadComboBox>
                <asp:ImageButton ID="ImageButton2" runat="server" ImageAlign="Middle"
                    ImageUrl="~/Images/help2.png" Width="16px" OnClientClick="showSlaHelp()" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <telerik:RadButton ID="RadButtonSlaChange" runat="server" Text="Change SLA (Billable)"
                    ToolTip="Informs the NOCSA to modify the SLA for this terminal. This is a billable operation!" Width="177px"
                    OnClientClicked="onSlaButtonClicked" AutoPostBack="False"
                    OnClick="RadButtonSlaChange_Click" Skin="Metro"/>
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:Panel ID="Panel4" runat="server" GroupingText="Customer Organizations">
    <asp:HiddenField ID="HiddenFieldCustomerOrganizations" runat="server" />
    <table border="0" cellspacing="5px" class="termDetails" frame="void">
        <tr>
            <td>Customer:
            </td>
            <td class="style1">
                <telerik:RadComboBox ID="RadComboBoxCustomerOrganizations" runat="server" Skin="Metro">
                </telerik:RadComboBox>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <telerik:RadButton ID="RadButtonChangeCustomer" runat="server" Text="Change Customer"
                    ToolTip="Change the Customer for a Terminal" Width="177px"
                    OnClientClicked="onCustomerChangeButtonClicked" AutoPostBack="False"
                    OnClick="RadButtonCustomerChange_Click" Skin="Metro"/>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Label ID="LabelResult" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Panel>

<telerik:RadWindowManager ID="RadWindowManager1" runat="server" Skin="Metro" EnableShadow="True"
    Left="0px" Right="0px" Modal="True" VisibleStatusbar="False">
</telerik:RadWindowManager>
