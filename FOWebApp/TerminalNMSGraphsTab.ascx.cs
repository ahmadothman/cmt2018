﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using FOWebApp.net.nimera.cmt.BOMonitorControlWSRef;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;
using FOWebApp.HelperMethods;

namespace FOWebApp
{
    public partial class TerminalNMSGraphsTab : System.Web.UI.UserControl
    {

        public string MacAddress { get; set; }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(MacAddress))
            {
                try
                {
                    string url = "http://nms.satadsl.net:3000/";
                    
                    // Scramble terminal/ in the path + scramble the MacAddress through the CalculateMD5Hash extension method
                    //labelTerminalMacAddresScramble.InnerText = ("terminal/" + MacAddress).CalculateMD5Hash();

                    // Call method AddTerminalNMSURL from BOAccountingControlWS.asmx to make the key/value pair needed to hide the real URL and display the scrambled one
                    //BOAccountingControlWS boAccountingControlWS = new BOAccountingControlWS();
                    //boAccountingControlWS.AddTerminalNMSURL(url + "terminal/" + MacAddress.Replace(";",""), url + labelTerminalMacAddresScramble.InnerText);

                    // iframe URL can be found in "scr" from front end. 
                    // Macaddress is added to the string, but may not contain any characters other than numbers/letters,":" is removed from Macaddress
                    iframeTerminalNMSGraphsTab.Attributes["src"] = url + "terminal/" + MacAddress.Replace(":", "");
                    labelTerminalMacAddresScramble.InnerText = iframeTerminalNMSGraphsTab.Attributes["src"].ToString();
                }
                catch (Exception ex)
                {
                    BOLogControlWS logController = new BOLogControlWS();
                    CmtApplicationException cmtAppEx = new CmtApplicationException
                    {
                        ExceptionDateTime = DateTime.UtcNow,
                        ExceptionDesc = ex.Message,
                        ExceptionStacktrace = ex.StackTrace,
                        ExceptionLevel = 2,
                        UserDescription = "TerminalNMSGraphs: The MacAddress could not be found"

                    };
                    logController.LogApplicationException(cmtAppEx);
                }
            }
        }


    }
}