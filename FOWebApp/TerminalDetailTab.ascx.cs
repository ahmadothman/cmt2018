﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Drawing;
using System.Web.Security;
using FOWebApp.net.nimera.cmt.BOMonitorControlWSRef;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;
using FOWebApp.TerminalDetails;

namespace FOWebApp
{
    public partial class TerminalDetailTab : System.Web.UI.UserControl
    {
        private string _macAddress;
        private CultureInfo _culture = new CultureInfo("en-US");
        private const string PathToDetailControls = @"~/TerminalDetails/";

        BOAccountingControlWS _boAccountingControl = new BOAccountingControlWS();
        BOMonitorControlWS _boMonitoringControl = new BOMonitorControlWS();

        /// <summary>
        /// MacAddress which must be set before loading the component!
        /// </summary>
        public string MacAddress
        {
            get { return _macAddress; }
            set { _macAddress = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //bool termChecked = false;
            
            //// Check if the _macAddress belongs to the distributor
            //MembershipUser user = Membership.GetUser();
            //if (Roles.IsUserInRole("Distributor"))
            //{
            //    Distributor dist = _boAccountingControl.GetDistributorForUser(user.ProviderUserKey.ToString());
            //    int distributorId = (int)dist.Id;

            //    try
            //    {
            //        termChecked = _boAccountingControl.IsTermOwnedByDistributor(distributorId, _macAddress);
            //    }
            //    catch (Exception ex)
            //    {
            //        BOLogControlWS boLogControlWS = new BOLogControlWS();
            //        CmtApplicationException cmtEx = new CmtApplicationException();
            //        cmtEx.ExceptionDesc = ex.Message;
            //        cmtEx.ExceptionStacktrace = ex.StackTrace;
            //        boLogControlWS.LogApplicationException(cmtEx);
            //        termChecked = false;
            //    }
            //}
            //else
            //{
            //    termChecked = true;
            //}

            //if (termChecked)
            //{
                // We must fetch the terminal information first  to get to the ISP identifier
                _macAddress = Request.Params["mac"];
                Terminal terminal = _boAccountingControl.GetTerminalDetailsByMAC(_macAddress);
                TerminalInfo terminalInfo = _boMonitoringControl.getTerminalInfoByMacAddress(_macAddress, terminal.IspId);

                // SATCORP-48 & 49: if term is ina redundant setup, go check NMS if the terminal is active
                bool redundantSetup = false;
                if (terminal.RedundantSetup != null && (bool)terminal.RedundantSetup)
                {
                    redundantSetup = true;
                }
                bool isSatDivTermActive = false;
                if (redundantSetup == true)
                {
                    isSatDivTermActive = _boAccountingControl.IsSatDivTerminalActive(_macAddress);
                }

                LabelAddressLine1.Text = terminal.Address.AddressLine1;
                LabelAddressLine2.Text = terminal.Address.AddressLine2;
                LabelLocation.Text = terminal.Address.Location;
                LabelPostalCode.Text = terminal.Address.PostalCode;
                LabelCountry.Text = terminal.Address.Country;
                LabelPhone.Text = terminal.Phone;
                LabelFax.Text = terminal.Fax;
                LabelEmail.Text = terminal.Email;
                
                
                if (terminalInfo.ErrorFlag)
                {
                    LiteralError.Text = terminalInfo.ErrorMsg;
                    PanelTerminalDetails.Visible = false;
                    PanelError.Visible = true;
                }
                else
                {
                    // Read terminal accounting data
                    ServiceLevel sla = _boAccountingControl.GetServicePack((int)(terminal.SlaId));
                    Isp isp = _boAccountingControl.GetISP(terminal.IspId);

                    TerminalStatus[] terminalStatusList = _boAccountingControl.GetTerminalStatus();
                    string terminalStatus = terminalStatusList[(int)--terminal.AdmStatus].StatusDesc.Trim();

                    if (sla.ServiceClass == 1 || sla.ServiceClass == 7) // SoHo and monthly SOHO SLA
                    {
                        TerminalDetailTabSoHo terminalDetailSoHo = (TerminalDetailTabSoHo)Page.LoadControl(string.Format("{0}TerminalDetailTabSoHo.ascx", PathToDetailControls));
                        terminalDetailSoHo.Initialize(terminal, terminalInfo, terminalStatus, sla, isp, _culture, redundantSetup, isSatDivTermActive);
                        PanelTerminalDetails.Controls.Add(terminalDetailSoHo);
                    }
                    else if (sla.ServiceClass == 2) // Business SLA
                    {
                        TerminalDetailTabBusiness terminalDetailBusiness = (TerminalDetailTabBusiness)Page.LoadControl(string.Format("{0}TerminalDetailTabBusiness.ascx", PathToDetailControls));
                        terminalDetailBusiness.Initialize(terminal, terminalInfo, terminalStatus, sla, isp, _culture, redundantSetup, isSatDivTermActive);
                        PanelTerminalDetails.Controls.Add(terminalDetailBusiness);
                    }
                    else if (sla.ServiceClass == 3) // Corporate SLA
                    {
                        TerminalDetailTabCorporate terminalDetailCorporate = (TerminalDetailTabCorporate)Page.LoadControl(string.Format("{0}TerminalDetailTabCorporate.ascx", PathToDetailControls));
                        terminalDetailCorporate.Initialize(terminal, terminalInfo, terminalStatus, sla, isp, _culture, redundantSetup, isSatDivTermActive);
                        PanelTerminalDetails.Controls.Add(terminalDetailCorporate);
                    }
                    else // Default to cover old SLAs and possible new configurations
                    {
                        TerminalDetailTabDefault terminalDetailDefault = (TerminalDetailTabDefault)Page.LoadControl(string.Format("{0}TerminalDetailTabDefault.ascx", PathToDetailControls));
                        terminalDetailDefault.Initialize(terminal, terminalInfo, terminalStatus, sla, isp, _culture, redundantSetup, isSatDivTermActive);
                        PanelTerminalDetails.Controls.Add(terminalDetailDefault);
                    }                    
            //    }
            //}
            //else
            //{
            //    LiteralError.Text = "Terminal not found";
            //    PanelTerminalDetails.Visible = false;
            //    PanelError.Visible = true;
            }
        }
    }
}