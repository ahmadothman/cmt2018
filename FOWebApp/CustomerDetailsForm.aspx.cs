﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Web.Security;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;

namespace FOWebApp
{
    public partial class CustomerDetailsForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //Read the Organization details from the database and fill out the form
                string strOrgId = Request.Params["id"];
                int orgId = Int32.Parse(strOrgId);

                LabelId.Text = strOrgId;

                BOAccountingControlWS boAccountingControl =
                  new net.nimera.cmt.BOAccountingControlWSRef.BOAccountingControlWS();

                MembershipUser user = Membership.GetUser();
                if (Roles.IsUserInRole("NOCSA"))
                {
                    CheckBoxVNO.Enabled = true;
                    RadNumericTextBoxCIR.Enabled = true;
                    RadNumericTextBoxMIR.Enabled = true;
                }
                
                Organization organization = boAccountingControl.GetOrganization(orgId);
                LabelDistributor.Text = "" + organization.Distributor.Id;
                TextBoxAddressLine1.Text = organization.Address.AddressLine1;
                TextBoxAddressLine2.Text = organization.Address.AddressLine2;
                TextBoxEMail.Text = organization.Email;
                TextBoxFax.Text = organization.Fax;
                TextBoxLocation.Text = organization.Address.Location;
                TextBoxName.Text = organization.FullName;
                TextBoxPCO.Text = organization.Address.PostalCode;
                TextBoxPhone.Text = organization.Phone;
                TextBoxRemarks.Text = organization.Remarks;
                TextBoxVAT.Text = organization.Vat;
                if (organization.VNO != null)
                {
                    if ((bool)organization.VNO)
                    {
                        CheckBoxVNO.Checked = true;
                    }
                }
                if (organization.MIR != null)
                {
                    RadNumericTextBoxMIR.Value = organization.MIR;
                }
                if (organization.CIR != null)
                {
                    RadNumericTextBoxCIR.Value = organization.CIR;
                }
                TextBoxWebSite.Text = organization.WebSite;
                CountryList.SelectedValue = organization.Address.Country;
            }

        }

        protected void ButtonSubmit_Click(object sender, EventArgs e)
        {
            LabelEmail.Visible = false;
            if (TextBoxName.Equals("") || TextBoxEMail.Equals(""))
            {
                LabelResult.ForeColor = Color.Red;
                LabelResult.Text = "Customer update failed!";
            }
            else
            {
                BOAccountingControlWS boAccountingControl =
                      new net.nimera.cmt.BOAccountingControlWSRef.BOAccountingControlWS();

                Organization organization = boAccountingControl.GetOrganization(Int32.Parse(LabelId.Text));
                Distributor distributor = boAccountingControl.GetDistributor(Int32.Parse(LabelDistributor.Text));

                organization.Id = Int32.Parse(LabelId.Text);
                organization.Address.AddressLine1 = TextBoxAddressLine1.Text;
                organization.Address.AddressLine2 = TextBoxAddressLine2.Text;
                organization.Email = TextBoxEMail.Text;
                organization.Fax = TextBoxFax.Text;
                organization.Address.Location = TextBoxLocation.Text;
                organization.FullName = TextBoxName.Text;
                organization.Address.PostalCode = TextBoxPCO.Text;
                organization.Phone = TextBoxPhone.Text;
                organization.Remarks = TextBoxRemarks.Text;
                organization.WebSite = TextBoxWebSite.Text;
                organization.Address.Country = CountryList.SelectedValue;
                organization.Distributor.Id = Int32.Parse(LabelDistributor.Text);
                organization.Vat = TextBoxVAT.Text;
                if (CheckBoxVNO.Checked)
                {
                    //organization.VNO = true;
                    organization.MIR = Convert.ToInt32(RadNumericTextBoxMIR.Text);
                    organization.CIR = Convert.ToInt32(RadNumericTextBoxCIR.Text);
                    //check that email address of Customer VNO is on a different domain from the Distributor
                    string[] distDomain = distributor.Email.Split('@');
                    string[] orgDomain = organization.Email.Split('@');
                    if (distDomain[1].Trim() == orgDomain[1].Trim())
                    {
                        LabelEmail.Text = "E-mail of Customer VNO may not belong to the Distributor";
                        LabelEmail.ForeColor = Color.Red;
                        LabelEmail.Visible = true;
                        return;
                    }
                }
                //else
                //{
                //    organization.VNO = false;
                //    organization.MIR = 0;
                //    organization.CIR = 0;
                //}
                if (boAccountingControl.UpdateOrganization(organization))
                {
                    LabelResult.ForeColor = Color.Green;
                    LabelResult.Text = "Customer update successful!";
                }
                else
                {
                    LabelResult.ForeColor = Color.Red;
                    LabelResult.Text = "Customer update failed!";
                }
            }
        }
    }
}