﻿using DotNet.Highcharts;
using DotNet.Highcharts.Enums;
using DotNet.Highcharts.Helpers;
using DotNet.Highcharts.Options;
using FOWebApp.Data;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;
using FOWebApp.net.nimera.cmt.BOMonitorControlWSRef;
using FOWebApp.net.nimera.cmt.BOSatDiversityControllerWSRef;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FOWebApp
{
    public partial class TerminalAccumulatedTab : System.Web.UI.UserControl
    {
        string _macAddress;
        /// <summary>
        /// MacAddress which must be set before loading the component!
        /// </summary>
        public string MacAddress
        {
            get { return _macAddress; }
            set { _macAddress = value; }
        }

        string _associatedMacAddress;
        /// <summary>
        /// MacAddress of the associated Terminal
        /// </summary>
        public string AssociatedMacAddress
        {
            get { return _associatedMacAddress; }
            set { _associatedMacAddress = value; }
        }

        BOAccountingControlWS _boAccountingControl = null;
        BOMonitorControlWS _boMonitorControl = null;
        BOSatDiversityControllerWS _boSatDiversityControl = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            _boAccountingControl = new BOAccountingControlWS();
            _boMonitorControl = new BOMonitorControlWS();
            _boSatDiversityControl = new BOSatDiversityControllerWS();
            if (ViewState["TerminalAccumulatedInitialTabSelection"] == null)
            {
                DateTime endDate = DateTime.Now.Date;
                int numDays = 30;
                DateTime startDate = endDate.AddDays(numDays * -1);
                endDate = endDate.AddDays(1);
                ViewState["TerminalAccumulatedInitialTabSelection"] = false;
                GenerateChart(startDate, endDate);
            }
        }

        protected void RadButtonPeriod_Click(object sender, EventArgs e)
        {
            if ((bool)ViewState["TerminalAccumulatedInitialTabSelection"] == false)
            {
                DateTime endDate = DateTime.Now.Date;
                int numDays = Int32.Parse(RadComboBoxPeriodSelection.SelectedValue);
                DateTime startDate = endDate.AddDays(numDays * -1);
                endDate = endDate.AddDays(1);
                GenerateChart(startDate, endDate); 
            }
        }

        protected void RadButtonDateSelection_Click(object sender, EventArgs e)
        {
            if ((bool)ViewState["TerminalAccumulatedInitialTabSelection"] == false)
            {
                DateTime startDate = (DateTime)RadDatePickerStartDate.SelectedDate;
                DateTime endDate = (DateTime)RadDatePickerEndDate.SelectedDate;
                if (startDate != null && endDate != null)
                {
                    if (startDate == endDate)
                    {
                        endDate = endDate.AddDays(1);
                    }
                    else if (endDate.Date == DateTime.Now.Date)
                    {
                        endDate = endDate.AddDays(1);
                    }
                    GenerateChart(startDate, endDate);
                } 
            }
        }

        /// <summary>
        /// Generates an Highcharts graph with accumulated volume between a start and
        /// end date.
        /// </summary>
        /// <param name="startDate">Start date for the graph</param>
        /// <param name="EndDate">End Date for the graph</param>
        private void GenerateChart(DateTime startDate, DateTime endDate)
        {
            try
            {
                Terminal terminal = _boAccountingControl.GetTerminalDetailsByMAC(MacAddress);
                TerminalInfo terminalInfo = _boMonitorControl.getTerminalInfoByMacAddress(terminal.MacAddress, terminal.IspId);
                ServiceLevel serviceLevel = _boAccountingControl.GetServicePack((int)terminal.SlaId);
                var _boLogControl = new BOLogControlWS();
               

                int fupResetDay = 1;
                if (serviceLevel.ServiceClass == 2)
                {
                    if (int.TryParse(terminalInfo.InvoiceDayOfMonth, out fupResetDay))
                    {
                        if (fupResetDay <= startDate.Day)
                        {
                            startDate = new DateTime(startDate.Year, startDate.Month, fupResetDay);
                        }
                        else
                        {
                            startDate = new DateTime(startDate.Year, startDate.AddMonths(-1).Month, fupResetDay);
                        }
                    }
                }
                List<XAxisPlotLines> xAxisPlotlines = new List<XAxisPlotLines>();
                //DateTime lastTerminalActivation = _boLogControl.GetTerminalActivityBySitId(terminal.SitId, terminal.IspId).FirstOrDefault(x => x.Action == "Activation").ActionDate;
                DateTime lastTerminalActivation = _boAccountingControl.GetTerminalActivations(terminal.SitId, terminal.IspId).LastOrDefault();
                if (lastTerminalActivation == null)
                {
                    lastTerminalActivation = Convert.ToDateTime(terminal.StartDate);
                }
                if (lastTerminalActivation > startDate)
                {
                    startDate = lastTerminalActivation;
                    xAxisPlotlines.Add(
                            new XAxisPlotLines
                            {
                                Value = startDate.ToUnixTimestamp() * 1000,
                                Width = 1,
                                DashStyle = DashStyles.Solid,
                                Color = Color.Black,
                                Label = new XAxisPlotLinesLabel
                                {
                                    Text = "Terminal Activation",
                                    VerticalAlign = VerticalAligns.Middle,
                                    Style = "color:'#000000'"
                                }
                            });
                }
                DateTime[] manualFupResets = _boAccountingControl.GetTerminalManualFupResets(terminal.SitId, terminal.IspId, startDate, endDate);
                DateTime[] volumeVoucherAdds = _boAccountingControl.GetTerminalVolumeVoucherAdds(terminal.SitId, terminal.IspId, startDate, endDate);
                List<DateTime> fupResets = new List<DateTime>();
                Highcharts chart = new Highcharts("chart").SetOptions(new GlobalOptions { Global = new DotNet.Highcharts.Options.Global { UseUTC = true } });
                chart.InitChart(new DotNet.Highcharts.Options.Chart { ZoomType = ZoomTypes.X, SpacingRight = 20 });
                string title = "Accumulated Volume";
                if (serviceLevel.FreeZone == true)
                {
                    title += " (free zone included)";
                }
                chart.SetTitle(new DotNet.Highcharts.Options.Title { Text = title });
                chart.SetSubtitle(new Subtitle { Text = "Click and drag in the plot area to zoom in" });
                var xAxis = new XAxis
                {
                    Type = AxisTypes.Datetime,
                    MinRange = 1800000,
                    Title = new XAxisTitle { Text = "" }
                };
                chart.SetXAxis(xAxis);
                chart.SetTooltip(new Tooltip { Shared = true });
                chart.SetLegend(new DotNet.Highcharts.Options.Legend { Enabled = true, Align = HorizontalAligns.Center, VerticalAlign = VerticalAligns.Bottom, BorderWidth = 0 });
                chart.SetPlotOptions(new PlotOptions
                {
                    Area = new PlotOptionsArea
                    {
                        FillColor = null,
                        FillOpacity = 0.0,
                        LineWidth = 1,
                        Marker = new PlotOptionsAreaMarker
                        {
                            Enabled = false,
                            States = new PlotOptionsAreaMarkerStates
                            {
                                Hover = new PlotOptionsAreaMarkerStatesHover
                                {
                                    Enabled = true,
                                    Radius = 5
                                }
                            }
                        },
                        Shadow = false,
                        States = new PlotOptionsAreaStates { Hover = new PlotOptionsAreaStatesHover { LineWidth = 1 } },
                        PointStart = new PointStart(startDate)
                    },
                    Series = new PlotOptionsSeries
                    {
                        ConnectNulls = true
                    }
                });

                var javaScriptSerializer = new JavaScriptSerializer();
                javaScriptSerializer.MaxJsonLength = Int32.MaxValue;
                decimal? volumeThreshold = 0;
                
                switch (serviceLevel.ServiceClass)
                {
                    case 2:
                        List<DateTime> dates = new List<DateTime>(manualFupResets);
                        foreach (DateTime date in EachMonth(startDate, endDate, fupResetDay))
                        {
                            fupResets.Add(date);
                        }
                        dates.AddRange(fupResets);
                        dates.Insert(0, startDate); 
                        dates.Add(endDate);
                        dates.Sort((a, b) => a.CompareTo(b));
                        var datapoints = new List<List<object>>();

                        for (int i = 0; i < dates.Count - 1; i++)
                        {
                            string graphUrl = CreateGraphiteUrl(dates[i], dates[i + 1], terminal, TrafficType.Summed);
                            string jsonGraphData;
                            using (WebClient client = new WebClient())
                            {
                                jsonGraphData = client.DownloadString(graphUrl);
                            }
                            var graphdata = javaScriptSerializer.Deserialize<List<Graph>>(jsonGraphData);
                            if (graphdata.FirstOrDefault() != null)
                            {
                                datapoints.AddRange(graphdata.FirstOrDefault().datapoints);
                            }
                        }
                        volumeThreshold = serviceLevel.HighVolumeSUM * 1000000000;
                        chart.SetSeries(new[]
                            {
                                new DotNet.Highcharts.Options.Series
                                    {
                                        Type = ChartTypes.Area,
                                        Name = "Volume consumed",
                                        Data = new DotNet.Highcharts.Helpers.Data(datapoints.Select( y => y.Swap(0, 1).ToList()).ToList().To2DArray())
                                    }
                            });

                        break;
                    default:
                        List<DateTime> voucherDates = new List<DateTime>(volumeVoucherAdds);
                        voucherDates.Insert(0, startDate);
                        voucherDates.Add(endDate);
                        voucherDates.Sort((a, b) => a.CompareTo(b));
                        var fwdDatapoints = new List<List<object>>();
                        var rtnDatapoints = new List<List<object>>();
                        for (int i = 0; i < voucherDates.Count - 1; i++)
                        {
                            string fwdRtnGraphUrl = CreateGraphiteUrl(voucherDates[i], voucherDates[i + 1], terminal, TrafficType.ForwardReturn);
                            string fwdRtnJsonGraphData;
                            using (WebClient client = new WebClient())
                            {
                                fwdRtnJsonGraphData = client.DownloadString(fwdRtnGraphUrl);
                            }
                            var graphdata = javaScriptSerializer.Deserialize<List<Graph>>(fwdRtnJsonGraphData);
                            if (graphdata.FirstOrDefault(x => x.target.Contains("FWD")) != null)
                            {
                                fwdDatapoints.AddRange(graphdata.FirstOrDefault(x => x.target.Contains("FWD")).datapoints);
                            }
                            if (graphdata.FirstOrDefault(x => x.target.Contains("RTN")) != null)
                            {
                                rtnDatapoints.AddRange(graphdata.FirstOrDefault(x => x.target.Contains("RTN")).datapoints);
                            }
                        }
                        chart.SetSeries(new[]
                           {
                                new DotNet.Highcharts.Options.Series
                                    {
                                        Type = ChartTypes.Area,
                                        Name = "Forward Volume",
                                        Data = new DotNet.Highcharts.Helpers.Data(fwdDatapoints.Select( y => y.Swap(0, 1).ToList()).ToList().To2DArray())
                                    },
                                new DotNet.Highcharts.Options.Series
                                    {
                                        Type = ChartTypes.Area,
                                        Name = "Return Volume",
                                        Data = new DotNet.Highcharts.Helpers.Data(rtnDatapoints.Select( y => y.Swap(0, 1).ToList()).ToList().To2DArray())
                                    },
                            });
                        volumeThreshold = terminalInfo.FUPThreshold * 1000000000;
                        foreach (var voucher in volumeVoucherAdds)
                        {
                            xAxisPlotlines.Add(
                                new XAxisPlotLines
                                {
                                    Value = voucher.ToUnixTimestamp() * 1000,
                                    Width = 1,
                                    DashStyle = DashStyles.LongDashDot,
                                    Color = Color.DarkGreen,
                                    Label = new XAxisPlotLinesLabel
                                    {
                                        Text = "Volume added",
                                        VerticalAlign = VerticalAligns.Middle,
                                        Style = "color:'#006400'"
                                    }
                                });
                        }
                        xAxis.PlotLines = xAxisPlotlines.ToArray();
                        break;
                }
                var yAxis = new YAxis
                {
                    Title = new YAxisTitle { Text = "Bytes" },
                    Min = 0,
                    MinRange = Convert.ToDouble(volumeThreshold),
                    StartOnTick = false,
                    EndOnTick = false
                };
                if (volumeThreshold > 0)
                {
                    yAxis.PlotLines = new[]
                               {
                        new YAxisPlotLines
                        {
                            Value = Convert.ToDouble(volumeThreshold),
                            Width = 1,
                            DashStyle = DashStyles.ShortDash,
                            Color = Color.Red,
                            Label = new YAxisPlotLinesLabel
                            {
                                Text = "Allocated Volume",
                                Align = HorizontalAligns.Left,
                                Style = "color: 'red'"
                            }
                        }
                    };
                    
                    foreach (var manualReset in manualFupResets)
                    {
                        xAxisPlotlines.Add(
                            new XAxisPlotLines
                            {
                                Value = manualReset.ToUnixTimestamp() * 1000,
                                Width = 1,
                                DashStyle = DashStyles.LongDashDot,
                                Color = Color.DarkGreen,
                                Label = new XAxisPlotLinesLabel
                                {
                                    Text = "Manual reset",
                                    VerticalAlign = VerticalAligns.Middle,
                                    Style = "color:'#006400'"
                                }
                            });
                    }
                    foreach (var fupReset in fupResets)
                    {
                        xAxisPlotlines.Add(
                            new XAxisPlotLines
                            {
                                Value = fupReset.ToUnixTimestamp() * 1000,
                                Width = 1,
                                DashStyle = DashStyles.LongDashDot,
                                Color = Color.Navy,
                                Label = new XAxisPlotLinesLabel
                                {
                                    Text = "FUP reset",
                                    VerticalAlign = VerticalAligns.Middle,
                                    Style = "color:'#000080'"
                                }
                            });
                    }
                    if (xAxisPlotlines.Count > 0)
                    {
                        xAxis.PlotLines = xAxisPlotlines.ToArray();
                    }
                }
                chart.SetYAxis(yAxis);
                ScriptManager.RegisterStartupScript(Page, typeof(Page), "chartKey", chart.ToJavaScriptString(), true);
            }
            catch (Exception ex)
            {
                BOLogControlWS boLogControlWS = new BOLogControlWS();
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDateTime = DateTime.Now;
                cmtEx.ExceptionDesc = ex.Message;
                cmtEx.UserDescription = "TerminalAccumulatedTab: GenerateChart";
                cmtEx.StateInformation = "";
                cmtEx.ExceptionLevel = 3;
                boLogControlWS.LogApplicationException(cmtEx);
            }
        }

        enum TrafficType
        {
            Summed,
            ForwardReturn
        };

        private string CreateGraphiteUrl(DateTime startDate, DateTime endDate, Terminal terminal, TrafficType trafficType)
        {
            string graphite = ConfigurationManager.AppSettings["Graphite"];
            List<string> terminalIpAddresses = new List<string>();
            if (terminal.IPMask < 32)
            {
                int exponent = 32 - terminal.IPMask;
                int numberOfHosts = Convert.ToInt32(Math.Pow(2, exponent));
                var splitOctets = terminal.IpAddress.Trim().Split('.').ToList();
                int lastOctet = Convert.ToInt32(splitOctets.Last());
                splitOctets.RemoveAt(splitOctets.Count - 1);
                for (int i = 1; i < numberOfHosts - 1; i++)
                {
                    string ipAddress = String.Join(".", splitOctets) + "." + (lastOctet + i);
                    terminalIpAddresses.Add(ipAddress);
                }
            }
            else
            {
                terminalIpAddresses.Add(terminal.IpAddress.Trim());
            }
            

            string graphUrl = graphite + "/render?target=integral(sumSeries(";
            switch (trafficType)
            {
                case TrafficType.Summed:
                    {
                        var sumIpAddresses = terminalIpAddresses.Select(x => "Term." + x + ".*");
                        string graphiteSumIpAddresses = String.Join(",", sumIpAddresses);
                        graphUrl += graphiteSumIpAddresses + "))";
                        break;
                    }
                case TrafficType.ForwardReturn:
                    {
                        var fwdIpAddresses = terminalIpAddresses.Select(x => "Term." + x + ".FWD");
                        var rtnIpAddresses = terminalIpAddresses.Select(x => "Term." + x + ".RTN");
                        string graphiteFwdIpAddresses = String.Join(",", fwdIpAddresses);
                        string graphiteRtnIpAddresses = String.Join(",", rtnIpAddresses);
                        graphUrl += graphiteFwdIpAddresses + "))&target=integral(sumSeries(" + graphiteRtnIpAddresses + "))";
                        break;
                    }
                default:
                    {
                        var sumIpAddresses = terminalIpAddresses.Select(x => "Term." + x + ".*");
                        string graphiteSumIpAddresses = String.Join(",", sumIpAddresses);
                        graphUrl += graphiteSumIpAddresses + "))";
                        break;
                    }
            }
            graphUrl += "&from=" + startDate.ToString("HH:mm_yyyyMMdd") +
                        "&until=" + endDate.ToString("HH:mm_yyyyMMdd") +
                        "&format=json";

            return graphUrl;
        }

        public IEnumerable<DateTime> EachMonth(DateTime startDate, DateTime endDate, int day)
        {
            for (var date = startDate.Date; date.Date <= endDate.Date; date = date.AddMonths(1))
            {
                DateTime fupResetDay = new DateTime(date.Year, date.Month, day);
                if (fupResetDay.Day <= date.Day)
                {
                    fupResetDay = fupResetDay.AddMonths(1);
                }
                if (fupResetDay.Ticks > date.Ticks && fupResetDay.Ticks < endDate.Ticks)
                {
                    yield return fupResetDay;
                }
            }
        }
    }
}