﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TerminalDetailsForm.aspx.cs"
    Inherits="FOWebApp.TerminalDetailsForm" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Terminal Details</title>
    <link href="CMTStyle.css" rel="stylesheet" type="text/css" />
    <%--<script type="text/javascript">
        function onTabSelecting(sender, args) {
            if (args.get_tab().get_pageViewID()) {
                args.get_tab().set_postBack(false);
            }
        }
    </script>--%>
</head>
<body>
    <form id="formTerminalListDetails" runat="server">
    <asp:ScriptManager ID="ScriptManagerTerminalDetailsForm" runat="server">
    </asp:ScriptManager>
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" Height="550px" Width="950px"
        CssClass="tabPane" LoadingPanelID="RadAjaxLoadingPanel1" Visible ="true">
        <telerik:RadTabStrip ID="RadTabStripTerminalView" runat="server" MultiPageID="RadMultiPageTerminalView"
            Skin="Metro" SelectedIndex="0" AutoPostBack="true"><%--OnClientTabSelecting="onTabSelecting"--%>
            <Tabs> <%-- When the tabs (or their order) are changed, the numbers in the LoadSelectedTab() method should also be changed --%>
                <telerik:RadTab ID="RadTabTerminal" runat="server" Text="Terminal" Selected="True">
                </telerik:RadTab>
                <telerik:RadTab ID="RadTabTerminal2" runat="server" Text="Associated Terminal" Visible="false">
                </telerik:RadTab>
                <telerik:RadTab ID="RadTabStatus" runat="server" Text="Status">
                </telerik:RadTab>
                <telerik:RadTab ID="RadTabTraffic" runat="server" Text="Traffic" Visible="false">
                </telerik:RadTab>
                 <telerik:RadTab ID="RadTabVolumes" runat="server" Text="Accumulated Volumes">
                </telerik:RadTab>
                <telerik:RadTab ID="RadTabManagement" runat="server" Text="Management" Visible="False">
                </telerik:RadTab>
                <telerik:RadTab ID="RadTabActivities" runat="server" Text="Terminal Activities" Visible="False">
                </telerik:RadTab>
                <telerik:RadTab ID="RadTabTrafficDetails" runat="server" Text="Attached Device" Visible="False">
                </telerik:RadTab>
                <telerik:RadTab ID="RadTabNMSGraphs" runat="server" Text="Real-time Data" Visible="False">
                </telerik:RadTab>
                <telerik:RadTab ID="RadTabNMSSpeedtest" runat="server" Text="Speedtest" Visible="False">
                </telerik:RadTab>
                <telerik:RadTab ID="RadTabGraphiteTest" runat="server" Text="Traffic" Visible="false">
                </telerik:RadTab>
                <telerik:RadTab ID="RadTabTrafficClassification" runat="server" Text="Traffic Classification" Visible="false">
                </telerik:RadTab>
            </Tabs>
        </telerik:RadTabStrip>
        <telerik:RadMultiPage ID="RadMultiPageTerminalView" runat="server" BorderColor="#CCCCCC"
            BorderStyle="Solid" BorderWidth="1px" Height=" 550px" Width="900px" ScrollBars="Auto"
            SelectedIndex="0">
            <telerik:RadPageView ID="RadPageViewTerminal" runat="server" Width="100%">
            </telerik:RadPageView>
            <telerik:RadPageView ID="RadPageViewTerminal2" runat="server" Width="100%">
            </telerik:RadPageView>
            <telerik:RadPageView ID="RadPageViewStatus" runat="server" Width="100%">
            </telerik:RadPageView>
            <telerik:RadPageView ID="RadPageViewConsumption" runat="server" Width="100%">
            </telerik:RadPageView>
            <telerik:RadPageView ID="RadPageViewAccumulated" runat="server" Width="100%">
            </telerik:RadPageView>
            <telerik:RadPageView ID="RadPageViewManagement" runat="server" Width="100%">
            </telerik:RadPageView>
            <telerik:RadPageView ID="RadPageViewActivities" runat="server" Width="100%">
            </telerik:RadPageView>
            <telerik:RadPageView ID="RadPageViewConnectedDevice" runat="server" Width="100%">
            </telerik:RadPageView>
            <telerik:RadPageView ID="RadPageViewNMSGraphs" runat="server" Width="100%">
            </telerik:RadPageView>
            <telerik:RadPageView ID="RadPageViewNMSSpeedtest" runat="server" Width="100%">
            </telerik:RadPageView>
            <telerik:RadPageView ID="RadPageViewGraphiteTest" runat="server" Width="100%">
            </telerik:RadPageView>
            <telerik:RadPageView ID="RadPageViewTrafficClassification" runat="server" Width="100%">
            </telerik:RadPageView>
            <telerik:RadPageView ID="RadPageViewTerminalNotFound" runat="server" Width="100%">
            </telerik:RadPageView>
        </telerik:RadMultiPage>
        <asp:Label ID="LabelMacAddress" runat="server" Visible="false"></asp:Label>
    </telerik:RadAjaxPanel>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Default" Width="900" Height="700">
    </telerik:RadAjaxLoadingPanel>
    </form>
</body>
</html>
