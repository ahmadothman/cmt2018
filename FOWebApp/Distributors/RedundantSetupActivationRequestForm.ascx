﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RedundantSetupActivationRequestForm.ascx.cs" Inherits="FOWebApp.Distributors.RedundantSetupActivationRequestForm" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
    <script type="text/javascript" language="javascript">
        function SerialChanged(textControl) {
            document.getElementById("LabelSerial").innerHTML = textControl.value;
        }

        function SerialChanged2(textControl) {
            document.getElementById("LabelSerial2").innerHTML = textControl.value;
        }

        function nodeClicking(sender, args) {
            var comboBox = $find("<%= RadComboBoxSLA.ClientID %>");

            var node = args.get_node()
            if (node.get_level() == 1) {

                comboBox.set_text(node.get_text());
                comboBox.set_value(node.get_value());
                document.getElementById('HiddenFieldSLAId').value = node.get_value();
                comboBox.trackChanges();
                comboBox.get_items().getItem(0).set_text(node.get_text());
                comboBox.commitChanges();

                comboBox.hideDropDown();

                var comboBoxOrg = $find("<%= RadComboBoxCustomer.ClientID %>");
                var panel = $find("<%=RadXmlHttpPanelCustomer.ClientID %>");
                panel.set_value(node.get_value());
            }
        }

        function nodeClicking2(sender, args) {
            var comboBox = $find("<%= RadComboBoxSLA2.ClientID %>");

            var node = args.get_node()
            if (node.get_level() == 1) {

                comboBox.set_text(node.get_text());
                comboBox.set_value(node.get_value());
                document.getElementById('HiddenFieldSLAId2').value = node.get_value();
                comboBox.trackChanges();
                comboBox.get_items().getItem(0).set_text(node.get_text());
                comboBox.commitChanges();

                comboBox.hideDropDown();

                var comboBoxOrg = $find("<%= RadComboBoxCustomer.ClientID %>");
                var panel = $find("<%=RadXmlHttpPanelCustomer.ClientID %>");
                panel.set_value(node.get_value());
            }
        }

        function OnMacAddressChanged(sender, eventArgs) {
            var macAddress = eventArgs.get_newValue();
            FOWebApp.WebServices.AccountSupportWS.IsMacAddressUnique(macAddress, OnRequestComplete, OnError);
            return false;
        }

        function OnMacAddressChanged2(sender, eventArgs) {
            var macAddress = eventArgs.get_newValue();
            FOWebApp.WebServices.AccountSupportWS.IsMacAddressUnique(macAddress, OnRequestComplete, OnError);
            return false;
        }

        function OnRequestComplete(result) {
            if (result == false) {
                document.getElementById('<%= LabelMacExists.ClientID %>').innerHTML = "Sorry but this MAC address already exists!";
            }
            else {
                document.getElementById('<%= LabelMacExists.ClientID %>').innerHTML = "";
            }
        }

        function OnError(result) {
            document.getElementById('<%= LabelMacExists.ClientID %>').innerHTML = "An error occured while checking the MAC address";
        }
    </script>
</telerik:RadCodeBlock>
<asp:HiddenField ID="HiddenFieldSLAId" runat="server" ClientIDMode="Static" />
<asp:HiddenField ID="HiddenFieldSLAId2" runat="server" ClientIDMode="Static" />
<asp:HiddenField ID="HiddenFieldCustomer" runat="server" />
<telerik:RadAjaxPanel ID="RadAjaxPanelCreateTerminal" runat="server" Height="1600px"
    Width="100%" LoadingPanelID="RadAjaxLoadingPanelCreateTerminal">
    <h1>
        SatADSL Activation Form - End User Service Agreement</h1>
    <p>
        This form is legally binding as proof of service application. Please provide the
        required information.</p>
    <h2>
        Service and Equipment Details - Primary Terminal</h2>
    <table cellpadding="10" cellspacing="5">
        <tr>
            <td class="requiredLabel">
                Modem Serial:
            </td>
            <td>
                <asp:TextBox ID="TextBoxSerial" runat="server" Width="187px" onchange="javascript: SerialChanged( this );"
                    Columns="13" MaxLength="13"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="This is a required field!"
                    ControlToValidate="TextBoxSerial"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="requiredLabel">
                Terminal name:
            </td>
            <td>
                <asp:Label ID="LabelDistribName" runat="server"></asp:Label>-
                <asp:TextBox ID="TextBoxTerminalName" runat="server" Columns="20" MaxLength="20"></asp:TextBox>-<span
                    id="LabelSerial"></span>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="This is a required field!"
                    ControlToValidate="TextBoxTerminalName"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="requiredLabel">
                Service Package:
            </td>
            <td>
                <telerik:RadComboBox ID="RadComboBoxSLA" runat="server" Skin="Metro" Sort="Ascending" Width="352px" EnableVirtualScrolling="True" EmptyMessage="Select a Service Pack"
                    MaxHeight="300px" Height="200px">
                    <ItemTemplate>
                        <telerik:RadTreeView ID="RadTreeViewSLA" runat="server" Skin="Metro" OnClientNodeClicking="nodeClicking">
                        </telerik:RadTreeView>
                    </ItemTemplate>
                    <Items>
                        <telerik:RadComboBoxItem Text="" />
                    </Items>
                    <ExpandAnimation Type="Linear" />
                </telerik:RadComboBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="This is a required field!"
                    ControlToValidate="RadComboBoxSLA"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="requiredLabel">
                AIR Mac Address:
            </td>
            <td>
                <telerik:RadXmlHttpPanel ID="RadXmlHttpPanelMacAddress" runat="server" 
                    onservicerequest="RadXmlHttpPanelMacAddress_ServiceRequest">
                    <telerik:RadMaskedTextBox ID="RadMaskedTextBoxMacAddress" runat="server" Skin="Metro"
                        Width="125px">
                        <ClientEvents OnValueChanged="OnMacAddressChanged" />
                    </telerik:RadMaskedTextBox>
                    <asp:Label ID="LabelMacExists" runat="server" ForeColor="Red"></asp:Label>
                </telerik:RadXmlHttpPanel>
            </td>
        </tr>
    </table>
    <h2>
    Service and Equipment Details - Secondary Terminal</h2>
    <table cellpadding="10" cellspacing="5">
        <tr>
            <td class="requiredLabel">
                Modem Serial:
            </td>
            <td>
                <asp:TextBox ID="TextBoxSerial2" runat="server" Width="187px" onchange="javascript: SerialChanged2( this );"
                    Columns="13" MaxLength="13"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="This is a required field!"
                    ControlToValidate="TextBoxSerial2"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="requiredLabel">
                Terminal name:
            </td>
            <td>
                <asp:Label ID="LabelDistribName2" runat="server"></asp:Label>-
                <asp:TextBox ID="TextBoxTerminalName2" runat="server" Columns="20" MaxLength="20"></asp:TextBox>-<span
                    id="LabelSerial2"></span>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="This is a required field!"
                    ControlToValidate="TextBoxTerminalName2"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="requiredLabel">
                Service Package:
            </td>
            <td>
                <telerik:RadComboBox ID="RadComboBoxSLA2" runat="server" Skin="Metro" Sort="Ascending" Width="352px" EnableVirtualScrolling="True" EmptyMessage="Select a Service Pack"
                    MaxHeight="300px" Height="200px">
                    <ItemTemplate>
                        <telerik:RadTreeView ID="RadTreeViewSLA2" runat="server" Skin="Metro" OnClientNodeClicking="nodeClicking2">
                        </telerik:RadTreeView>
                    </ItemTemplate>
                    <Items>
                        <telerik:RadComboBoxItem Text="" />
                    </Items>
                    <ExpandAnimation Type="Linear" />
                </telerik:RadComboBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="This is a required field!"
                    ControlToValidate="RadComboBoxSLA2"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="requiredLabel">
                AIR Mac Address:
            </td>
            <td>
                <telerik:RadXmlHttpPanel ID="RadXmlHttpPanelMacAddress2" runat="server" 
                    onservicerequest="RadXmlHttpPanelMacAddress2_ServiceRequest">
                    <telerik:RadMaskedTextBox ID="RadMaskedTextBoxMacAddress2" runat="server" Skin="Metro"
                        Width="125px">
                        <ClientEvents OnValueChanged="OnMacAddressChanged2" />
                    </telerik:RadMaskedTextBox>
                    <asp:Label ID="LabelMacExists2" runat="server" ForeColor="Red"></asp:Label>
                </telerik:RadXmlHttpPanel>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <h3>
                    Terminal setup location:</h3>
            </td>
        </tr>
        <tr>
            <td class="requiredLabel">
                Address Line 1:
            </td>
            <td>
                <asp:TextBox ID="TextBoxAddressLine1" runat="server" Width="343px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="This is a required field!"
                    ControlToValidate="TextBoxAddressLine1"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                Address Line 2:
            </td>
            <td>
                <asp:TextBox ID="TextBoxAddressLine2" runat="server" Width="341px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="requiredLabel">
                Location of Terminal:
            </td>
            <td>
                <asp:TextBox ID="TextBoxTerminalLocation" runat="server" Width="337px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="This is a required field!"
                    ControlToValidate="TextBoxTerminalLocation"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="requiredLabel">
                Postcode:
            </td>
            <td>
                <asp:TextBox ID="TextBoxPCO" runat="server" Width="131px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="This is a required field!"
                    ControlToValidate="TextBoxPCO"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="requiredLabel">
                Country:
            </td>
            <td>
                <asp:DropDownList ID="CountryList" runat="server">
                    <asp:ListItem Value="AF">Afghanistan</asp:ListItem>
                    <asp:ListItem Value="AL">Albania</asp:ListItem>
                    <asp:ListItem Value="DZ">Algeria</asp:ListItem>
                    <asp:ListItem Value="AS">American Samoa</asp:ListItem>
                    <asp:ListItem Value="AD">Andorra</asp:ListItem>
                    <asp:ListItem Value="AO">Angola</asp:ListItem>
                    <asp:ListItem Value="AI">Anguilla</asp:ListItem>
                    <asp:ListItem Value="AQ">Antarctica</asp:ListItem>
                    <asp:ListItem Value="AG">Antigua And Barbuda</asp:ListItem>
                    <asp:ListItem Value="AR">Argentina</asp:ListItem>
                    <asp:ListItem Value="AM">Armenia</asp:ListItem>
                    <asp:ListItem Value="AW">Aruba</asp:ListItem>
                    <asp:ListItem Value="AU">Australia</asp:ListItem>
                    <asp:ListItem Value="AT">Austria</asp:ListItem>
                    <asp:ListItem Value="AZ">Azerbaijan</asp:ListItem>
                    <asp:ListItem Value="BS">Bahamas</asp:ListItem>
                    <asp:ListItem Value="BH">Bahrain</asp:ListItem>
                    <asp:ListItem Value="BD">Bangladesh</asp:ListItem>
                    <asp:ListItem Value="BB">Barbados</asp:ListItem>
                    <asp:ListItem Value="BY">Belarus</asp:ListItem>
                    <asp:ListItem Value="BE">Belgium</asp:ListItem>
                    <asp:ListItem Value="BZ">Belize</asp:ListItem>
                    <asp:ListItem Value="BJ">Benin</asp:ListItem>
                    <asp:ListItem Value="BM">Bermuda</asp:ListItem>
                    <asp:ListItem Value="BT">Bhutan</asp:ListItem>
                    <asp:ListItem Value="BO">Bolivia</asp:ListItem>
                    <asp:ListItem Value="BA">Bosnia And Herzegowina</asp:ListItem>
                    <asp:ListItem Value="BW">Botswana</asp:ListItem>
                    <asp:ListItem Value="BV">Bouvet Island</asp:ListItem>
                    <asp:ListItem Value="BR">Brazil</asp:ListItem>
                    <asp:ListItem Value="IO">British Indian Ocean Territory</asp:ListItem>
                    <asp:ListItem Value="BN">Brunei Darussalam</asp:ListItem>
                    <asp:ListItem Value="BG">Bulgaria</asp:ListItem>
                    <asp:ListItem Value="BF">Burkina Faso</asp:ListItem>
                    <asp:ListItem Value="BI">Burundi</asp:ListItem>
                    <asp:ListItem Value="KH">Cambodia</asp:ListItem>
                    <asp:ListItem Value="CM">Cameroon</asp:ListItem>
                    <asp:ListItem Value="CA">Canada</asp:ListItem>
                    <asp:ListItem Value="CV">Cape Verde</asp:ListItem>
                    <asp:ListItem Value="KY">Cayman Islands</asp:ListItem>
                    <asp:ListItem Value="CF">Central African Republic</asp:ListItem>
                    <asp:ListItem Value="TD">Chad</asp:ListItem>
                    <asp:ListItem Value="CL">Chile</asp:ListItem>
                    <asp:ListItem Value="CN">China</asp:ListItem>
                    <asp:ListItem Value="CX">Christmas Island</asp:ListItem>
                    <asp:ListItem Value="CC">Cocos (Keeling) Islands</asp:ListItem>
                    <asp:ListItem Value="CO">Colombia</asp:ListItem>
                    <asp:ListItem Value="KM">Comoros</asp:ListItem>
                    <asp:ListItem Value="CG">Congo</asp:ListItem>
                    <asp:ListItem Value="CK">Cook Islands</asp:ListItem>
                    <asp:ListItem Value="CR">Costa Rica</asp:ListItem>
                    <asp:ListItem Value="CI">Cote D'Ivoire</asp:ListItem>
                    <asp:ListItem Value="HR">Croatia (Local Name: Hrvatska)</asp:ListItem>
                    <asp:ListItem Value="CU">Cuba</asp:ListItem>
                    <asp:ListItem Value="CY">Cyprus</asp:ListItem>
                    <asp:ListItem Value="CZ">Czech Republic</asp:ListItem>
                    <asp:ListItem Value="DK">Denmark</asp:ListItem>
                    <asp:ListItem Value="DJ">Djibouti</asp:ListItem>
                    <asp:ListItem Value="DM">Dominica</asp:ListItem>
                    <asp:ListItem Value="DO">Dominican Republic</asp:ListItem>
                    <asp:ListItem Value="TP">East Timor</asp:ListItem>
                    <asp:ListItem Value="EC">Ecuador</asp:ListItem>
                    <asp:ListItem Value="EG">Egypt</asp:ListItem>
                    <asp:ListItem Value="SV">El Salvador</asp:ListItem>
                    <asp:ListItem Value="GQ">Equatorial Guinea</asp:ListItem>
                    <asp:ListItem Value="ER">Eritrea</asp:ListItem>
                    <asp:ListItem Value="EE">Estonia</asp:ListItem>
                    <asp:ListItem Value="ET">Ethiopia</asp:ListItem>
                    <asp:ListItem Value="FK">Falkland Islands (Malvinas)</asp:ListItem>
                    <asp:ListItem Value="FO">Faroe Islands</asp:ListItem>
                    <asp:ListItem Value="FJ">Fiji</asp:ListItem>
                    <asp:ListItem Value="FI">Finland</asp:ListItem>
                    <asp:ListItem Value="FR">France</asp:ListItem>
                    <asp:ListItem Value="GF">French Guiana</asp:ListItem>
                    <asp:ListItem Value="PF">French Polynesia</asp:ListItem>
                    <asp:ListItem Value="TF">French Southern Territories</asp:ListItem>
                    <asp:ListItem Value="GA">Gabon</asp:ListItem>
                    <asp:ListItem Value="GM">Gambia</asp:ListItem>
                    <asp:ListItem Value="GE">Georgia</asp:ListItem>
                    <asp:ListItem Value="DE">Germany</asp:ListItem>
                    <asp:ListItem Value="GH">Ghana</asp:ListItem>
                    <asp:ListItem Value="GI">Gibraltar</asp:ListItem>
                    <asp:ListItem Value="GR">Greece</asp:ListItem>
                    <asp:ListItem Value="GL">Greenland</asp:ListItem>
                    <asp:ListItem Value="GD">Grenada</asp:ListItem>
                    <asp:ListItem Value="GP">Guadeloupe</asp:ListItem>
                    <asp:ListItem Value="GU">Guam</asp:ListItem>
                    <asp:ListItem Value="GT">Guatemala</asp:ListItem>
                    <asp:ListItem Value="GN">Guinea</asp:ListItem>
                    <asp:ListItem Value="GW">Guinea-Bissau</asp:ListItem>
                    <asp:ListItem Value="GY">Guyana</asp:ListItem>
                    <asp:ListItem Value="HT">Haiti</asp:ListItem>
                    <asp:ListItem Value="HM">Heard And Mc Donald Islands</asp:ListItem>
                    <asp:ListItem Value="VA">Holy See (Vatican City State)</asp:ListItem>
                    <asp:ListItem Value="HN">Honduras</asp:ListItem>
                    <asp:ListItem Value="HK">Hong Kong</asp:ListItem>
                    <asp:ListItem Value="HU">Hungary</asp:ListItem>
                    <asp:ListItem Value="IS">Icel And</asp:ListItem>
                    <asp:ListItem Value="IN">India</asp:ListItem>
                    <asp:ListItem Value="ID">Indonesia</asp:ListItem>
                    <asp:ListItem Value="IR">Iran (Islamic Republic Of)</asp:ListItem>
                    <asp:ListItem Value="IQ">Iraq</asp:ListItem>
                    <asp:ListItem Value="IE">Ireland</asp:ListItem>
                    <asp:ListItem Value="IL">Israel</asp:ListItem>
                    <asp:ListItem Value="IT">Italy</asp:ListItem>
                    <asp:ListItem Value="JM">Jamaica</asp:ListItem>
                    <asp:ListItem Value="JP">Japan</asp:ListItem>
                    <asp:ListItem Value="JO">Jordan</asp:ListItem>
                    <asp:ListItem Value="KZ">Kazakhstan</asp:ListItem>
                    <asp:ListItem Value="KE">Kenya</asp:ListItem>
                    <asp:ListItem Value="KI">Kiribati</asp:ListItem>
                    <asp:ListItem Value="KP">Korea, Dem People'S Republic</asp:ListItem>
                    <asp:ListItem Value="KR">Korea, Republic Of</asp:ListItem>
                    <asp:ListItem Value="KW">Kuwait</asp:ListItem>
                    <asp:ListItem Value="KG">Kyrgyzstan</asp:ListItem>
                    <asp:ListItem Value="LA">Lao People'S Dem Republic</asp:ListItem>
                    <asp:ListItem Value="LV">Latvia</asp:ListItem>
                    <asp:ListItem Value="LB">Lebanon</asp:ListItem>
                    <asp:ListItem Value="LS">Lesotho</asp:ListItem>
                    <asp:ListItem Value="LR">Liberia</asp:ListItem>
                    <asp:ListItem Value="LY">Libyan Arab Jamahiriya</asp:ListItem>
                    <asp:ListItem Value="LI">Liechtenstein</asp:ListItem>
                    <asp:ListItem Value="LT">Lithuania</asp:ListItem>
                    <asp:ListItem Value="LU">Luxembourg</asp:ListItem>
                    <asp:ListItem Value="MO">Macau</asp:ListItem>
                    <asp:ListItem Value="MK">Macedonia</asp:ListItem>
                    <asp:ListItem Value="MG">Madagascar</asp:ListItem>
                    <asp:ListItem Value="MW">Malawi</asp:ListItem>
                    <asp:ListItem Value="MY">Malaysia</asp:ListItem>
                    <asp:ListItem Value="MV">Maldives</asp:ListItem>
                    <asp:ListItem Value="ML">Mali</asp:ListItem>
                    <asp:ListItem Value="MT">Malta</asp:ListItem>
                    <asp:ListItem Value="MH">Marshall Islands</asp:ListItem>
                    <asp:ListItem Value="MQ">Martinique</asp:ListItem>
                    <asp:ListItem Value="MR">Mauritania</asp:ListItem>
                    <asp:ListItem Value="MU">Mauritius</asp:ListItem>
                    <asp:ListItem Value="YT">Mayotte</asp:ListItem>
                    <asp:ListItem Value="MX">Mexico</asp:ListItem>
                    <asp:ListItem Value="FM">Micronesia, Federated States</asp:ListItem>
                    <asp:ListItem Value="MD">Moldova, Republic Of</asp:ListItem>
                    <asp:ListItem Value="MC">Monaco</asp:ListItem>
                    <asp:ListItem Value="MN">Mongolia</asp:ListItem>
                    <asp:ListItem Value="MS">Montserrat</asp:ListItem>
                    <asp:ListItem Value="MA">Morocco</asp:ListItem>
                    <asp:ListItem Value="MZ">Mozambique</asp:ListItem>
                    <asp:ListItem Value="MM">Myanmar</asp:ListItem>
                    <asp:ListItem Value="NA">Namibia</asp:ListItem>
                    <asp:ListItem Value="NR">Nauru</asp:ListItem>
                    <asp:ListItem Value="NP">Nepal</asp:ListItem>
                    <asp:ListItem Value="NL">Netherlands</asp:ListItem>
                    <asp:ListItem Value="AN">Netherlands Ant Illes</asp:ListItem>
                    <asp:ListItem Value="NC">New Caledonia</asp:ListItem>
                    <asp:ListItem Value="NZ">New Zealand</asp:ListItem>
                    <asp:ListItem Value="NI">Nicaragua</asp:ListItem>
                    <asp:ListItem Value="NE">Niger</asp:ListItem>
                    <asp:ListItem Value="NG">Nigeria</asp:ListItem>
                    <asp:ListItem Value="NU">Niue</asp:ListItem>
                    <asp:ListItem Value="NF">Norfolk Island</asp:ListItem>
                    <asp:ListItem Value="MP">Northern Mariana Islands</asp:ListItem>
                    <asp:ListItem Value="NO">Norway</asp:ListItem>
                    <asp:ListItem Value="OM">Oman</asp:ListItem>
                    <asp:ListItem Value="PK">Pakistan</asp:ListItem>
                    <asp:ListItem Value="PW">Palau</asp:ListItem>
                    <asp:ListItem Value="PA">Panama</asp:ListItem>
                    <asp:ListItem Value="PG">Papua New Guinea</asp:ListItem>
                    <asp:ListItem Value="PY">Paraguay</asp:ListItem>
                    <asp:ListItem Value="PE">Peru</asp:ListItem>
                    <asp:ListItem Value="PH">Philippines</asp:ListItem>
                    <asp:ListItem Value="PN">Pitcairn</asp:ListItem>
                    <asp:ListItem Value="PL">Poland</asp:ListItem>
                    <asp:ListItem Value="PT">Portugal</asp:ListItem>
                    <asp:ListItem Value="PR">Puerto Rico</asp:ListItem>
                    <asp:ListItem Value="QA">Qatar</asp:ListItem>
                    <asp:ListItem Value="CD">République Démocratique Du Congo</asp:ListItem>
                    <asp:ListItem Value="RE">Reunion</asp:ListItem>
                    <asp:ListItem Value="RO">Romania</asp:ListItem>
                    <asp:ListItem Value="RU">Russian Federation</asp:ListItem>
                    <asp:ListItem Value="RW">Rwanda</asp:ListItem>
                    <asp:ListItem Value="KN">Saint K Itts And Nevis</asp:ListItem>
                    <asp:ListItem Value="LC">Saint Lucia</asp:ListItem>
                    <asp:ListItem Value="VC">Saint Vincent, The Grenadines</asp:ListItem>
                    <asp:ListItem Value="WS">Samoa</asp:ListItem>
                    <asp:ListItem Value="SM">San Marino</asp:ListItem>
                    <asp:ListItem Value="ST">Sao Tome And Principe</asp:ListItem>
                    <asp:ListItem Value="SA">Saudi Arabia</asp:ListItem>
                    <asp:ListItem Value="SN">Senegal</asp:ListItem>
                    <asp:ListItem Value="SC">Seychelles</asp:ListItem>
                    <asp:ListItem Value="SL">Sierra Leone</asp:ListItem>
                    <asp:ListItem Value="SG">Singapore</asp:ListItem>
                    <asp:ListItem Value="SK">Slovakia (Slovak Republic)</asp:ListItem>
                    <asp:ListItem Value="SI">Slovenia</asp:ListItem>
                    <asp:ListItem Value="SB">Solomon Islands</asp:ListItem>
                    <asp:ListItem Value="SO">Somalia</asp:ListItem>
                    <asp:ListItem Value="ZA">South Africa</asp:ListItem>
                    <asp:ListItem Value="GS">South Georgia , S Sandwich Is.</asp:ListItem>
                    <asp:ListItem Value="ES">Spain</asp:ListItem>
                    <asp:ListItem Value="LK">Sri Lanka</asp:ListItem>
                    <asp:ListItem Value="SH">St. Helena</asp:ListItem>
                    <asp:ListItem Value="PM">St. Pierre And Miquelon</asp:ListItem>
                    <asp:ListItem Value="SD">Sudan</asp:ListItem>
                    <asp:ListItem Value="SR">Suriname</asp:ListItem>
                    <asp:ListItem Value="SJ">Svalbard, Jan Mayen Islands</asp:ListItem>
                    <asp:ListItem Value="SZ">Sw Aziland</asp:ListItem>
                    <asp:ListItem Value="SE">Sweden</asp:ListItem>
                    <asp:ListItem Value="CH">Switzerland</asp:ListItem>
                    <asp:ListItem Value="SY">Syrian Arab Republic</asp:ListItem>
                    <asp:ListItem Value="TW">Taiwan</asp:ListItem>
                    <asp:ListItem Value="TJ">Tajikistan</asp:ListItem>
                    <asp:ListItem Value="TZ">Tanzania, United Republic Of</asp:ListItem>
                    <asp:ListItem Value="TH">Thailand</asp:ListItem>
                    <asp:ListItem Value="TG">Togo</asp:ListItem>
                    <asp:ListItem Value="TK">Tokelau</asp:ListItem>
                    <asp:ListItem Value="TO">Tonga</asp:ListItem>
                    <asp:ListItem Value="TT">Trinidad And Tobago</asp:ListItem>
                    <asp:ListItem Value="TN">Tunisia</asp:ListItem>
                    <asp:ListItem Value="TR">Turkey</asp:ListItem>
                    <asp:ListItem Value="TM">Turkmenistan</asp:ListItem>
                    <asp:ListItem Value="TC">Turks And Caicos Islands</asp:ListItem>
                    <asp:ListItem Value="TV">Tuvalu</asp:ListItem>
                    <asp:ListItem Value="UG">Uganda</asp:ListItem>
                    <asp:ListItem Value="UA">Ukraine</asp:ListItem>
                    <asp:ListItem Value="AE">United Arab Emirates</asp:ListItem>
                    <asp:ListItem Value="GB">United Kingdom</asp:ListItem>
                    <asp:ListItem Value="US">United States</asp:ListItem>
                    <asp:ListItem Value="UM">United States Minor Is.</asp:ListItem>
                    <asp:ListItem Value="UY">Uruguay</asp:ListItem>
                    <asp:ListItem Value="UZ">Uzbekistan</asp:ListItem>
                    <asp:ListItem Value="VU">Vanuatu</asp:ListItem>
                    <asp:ListItem Value="VE">Venezuela</asp:ListItem>
                    <asp:ListItem Value="VN">Viet Nam</asp:ListItem>
                    <asp:ListItem Value="VG">Virgin Islands (British)</asp:ListItem>
                    <asp:ListItem Value="VI">Virgin Islands (U.S.)</asp:ListItem>
                    <asp:ListItem Value="WF">Wallis And Futuna Islands</asp:ListItem>
                    <asp:ListItem Value="EH">Western Sahara</asp:ListItem>
                    <asp:ListItem Value="YE">Yemen</asp:ListItem>
                    <asp:ListItem Value="YU">Yugoslavia</asp:ListItem>
                    <asp:ListItem Value="ZM">Zambia</asp:ListItem>
                    <asp:ListItem Value="ZW">Zimbabwe</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="This is a required field!"
                    ControlToValidate="CountryList"></asp:RequiredFieldValidator>
            </td>
        </tr>
    </table>
    <h2>
        Subscriber Details</h2>
    <table cellpadding="10" cellspacing="5">
        <tr>
            <td class="requiredLabel">
                Customer Organisation:
            </td>
            <td>
                <telerik:RadXmlHttpPanel ID="RadXmlHttpPanelCustomer" runat="server" EnableClientScriptEvaluation="true" OnServiceRequest="RadXmlHttpPanelCustomer_ServiceRequest">
                    <telerik:RadComboBox ID="RadComboBoxCustomer" runat="server" Skin="Metro" ExpandDirection="Up"
                        Height="200px" Width="430px" CssClass="requiredField" Sort="Ascending">
                    </telerik:RadComboBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="This is a required field!"
                        ControlToValidate="RadComboBoxCustomer"></asp:RequiredFieldValidator>
                </telerik:RadXmlHttpPanel>
            </td>
        </tr>
        <tr>
            <td>
                Contact First Name:
            </td>
            <td>
                <asp:TextBox ID="TextBoxFirstName" runat="server" Width="335px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Contact Surname:
            </td>
            <td>
                <asp:TextBox ID="TextBoxSurname" runat="server" Width="333px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Fixed Phone:
            </td>
            <td>
                <asp:TextBox ID="TextBoxPhone" runat="server" Width="202px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Mobile Phone:
            </td>
            <td>
                <asp:TextBox ID="TextBoxMobile" runat="server" Width="200px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                E-Mail Address:
            </td>
            <td>
                <asp:TextBox ID="TextBoxEMailAddress" runat="server" Width="293px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Remark :
            </td>
            <td>
                <asp:TextBox ID="TextBoxInstEngineer" runat="server" Width="556px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Requested installation Date:
            </td>
            <td>
                <telerik:RadDatePicker ID="RadDatePickerInstallationDate" runat="server" Skin="Metro">
                    <Calendar ID="Calendar4" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False"
                        ViewSelectorText="x" Skin="Metro" runat="server">
                    </Calendar>
                    <DateInput ID="DateInput2" DisplayDateFormat="d/MM/yyyy" DateFormat="d/MM/yyyy" runat="server">
                    </DateInput>
                    <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                </telerik:RadDatePicker>
            </td>
        </tr>
    </table>
    <h2>
        Subscription details:
    </h2>
    <table cellpadding="10" cellspacing="5">
        <tr>
            <td class="requiredLabel">
                Date of First Activation:
            </td>
            <td>
                <telerik:RadDatePicker ID="RadDatePickerActivationDate" runat="server" Skin="Metro">
                    <Calendar ID="Calendar3" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False"
                        ViewSelectorText="x" Skin="Metro" runat="server">
                    </Calendar>
                    <DateInput ID="DateInput3" DisplayDateFormat="d/MM/yyyy" DateFormat="d/MM/yyyy" runat="server">
                    </DateInput>
                    <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                </telerik:RadDatePicker>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="This is a required field!"
                    ControlToValidate="RadDatePickerActivationDate"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="requiredLabel">
                Subscription Start Date:
            </td>
            <td>
                <telerik:RadDatePicker ID="RadDatePickerStartDate" runat="server" Skin="Metro">
                    <Calendar ID="Calendar1" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False"
                        ViewSelectorText="x" Skin="Metro" runat="server">
                    </Calendar>
                    <DateInput ID="DateInput1" DisplayDateFormat="d/MM/yyyy" DateFormat="d/MM/yyyy" runat="server">
                    </DateInput>
                    <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                </telerik:RadDatePicker>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="This is a required field!"
                    ControlToValidate="RadDatePickerStartDate"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                Subscription Expiry Date:
            </td>
            <td>
                <telerik:RadDatePicker ID="RadDatePickerExpiryDate" runat="server" Skin="Metro">
                    <Calendar ID="Calendar2" UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False"
                        ViewSelectorText="x" Skin="Metro" runat="server">
                    </Calendar>
                    <DateInput ID="DateInputExpiryDate" DisplayDateFormat="d/MM/yyyy" DateFormat="d/MM/yyyy"
                        runat="server">
                    </DateInput>
                    <DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                </telerik:RadDatePicker>
            </td>
        </tr>
    </table>
    <h2>
        Authorized Distributor:
        <asp:Label ID="LabelDistributor" runat="server"></asp:Label></h2>
    <table cellpadding="10" cellspacing="5">
        <tr>
            <td>
                <asp:CheckBox ID="CheckBoxDistributorAcceptance" runat="server" Text="I confirm that the installation and integration has been undertaken to acceptable and proper standards."
                    Font-Bold="True" ForeColor="#FF3300" />
            </td>
        </tr>
    </table>
    <h2>
        Final Acceptance By Customer</h2>
    <table cellpadding="10" cellspacing="5">
        <tr>
            <td colspan="2">
                <asp:CheckBox ID="CheckBoxCustomerAcceptance" runat="server" Text="I confirm that the installation has been done to my satisfaction and that the performance is satisfactorily."
                    Font-Bold="True" ForeColor="#FF3300" CausesValidation="True" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="ButtonSave" runat="server" Text="Send Request to SatADSL" OnClick="ButtonSave_Click" />
            </td>
            <td>
                <asp:Label ID="LabelResult" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
</telerik:RadAjaxPanel>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanelCreateTerminal" runat="server"
    Skin="Metro">
</telerik:RadAjaxLoadingPanel>
<telerik:RadWindowManager ID="RadWindowManager1" runat="server" Skin="Metro" EnableShadow="True"
    Left="0px" Right="0px" Modal="True" VisibleStatusbar="False">
</telerik:RadWindowManager>