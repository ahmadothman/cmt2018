﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InvoiceList.ascx.cs" Inherits="FOWebApp.Distributors.InvoiceList" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<telerik:RadScriptBlock ID="RadScriptBlockInvoiceList" runat="server">
    <script>
        function clientExecuteQuery() {
            $telerik.$(<%= LabelResult.ClientID %>)
                .html('Executing query...')
                .css('color', 'DarkBlue');
        }

        function rowSelected(sender, eventArgs) {
            switch (eventArgs.get_tableView().get_name()) {
                case 'Invoices':
                    var invoiceId = eventArgs.getDataKeyValue('Id');
                    var oWnd = radopen('<%= this.ResolveUrl("~/Distributors/InvoiceDetails.aspx") %>?id=' + invoiceId, 'RadWindowInvoiceDetails');

                    break;
                default:
                    console.log('Not implemented!');
                    break;
            }
        }

        function ShowPayForm(id) {
            window.open("Distributors/InvoicePayment.aspx?InvoiceId=" + id, "WindowPopup", "width=600px, height=400px, resizable, scrollbars");
            return false;
        }
    </script>
</telerik:RadScriptBlock>

<table class="table-spaced">
    <tr>
        <td>Year
        </td>
        <td>
            <telerik:RadDropDownList ID="RadDropDownListYear" runat="server" Width="150" DefaultMessage="Select a year..."
                DropDownWidth="150" DropDownHeight="300" AutoPostBack="true" OnClientSelectedIndexChanged="clientExecuteQuery"
                OnSelectedIndexChanged="RadDropDownListYear_SelectedIndexChanged"
                Skin="Metro" CausesValidation="false">
                <Items>
                    <telerik:DropDownListItem runat="server" Text="" Value="-1" Selected="true"/>
                    <telerik:DropDownListItem runat="server" Text="2011" Value="2011" />
                    <telerik:DropDownListItem runat="server" Text="2012" Value="2012" />
                    <telerik:DropDownListItem runat="server" Text="2013" Value="2013" />
                    <telerik:DropDownListItem runat="server" Text="2014" Value="2014" />
                    <telerik:DropDownListItem runat="server" Text="2015" Value="2015" />
                    <telerik:DropDownListItem runat="server" Text="2016" Value="2016" />
                    <telerik:DropDownListItem runat="server" Text="2017" Value="2017" />
                    <telerik:DropDownListItem runat="server" Text="2018" Value="2018" />
                    <telerik:DropDownListItem runat="server" Text="2019" Value="2019" />
                    <telerik:DropDownListItem runat="server" Text="2020" Value="2020" />
                    <telerik:DropDownListItem runat="server" Text="2021" Value="2021" />
                    <telerik:DropDownListItem runat="server" Text="2022" Value="2022" />
                    <telerik:DropDownListItem runat="server" Text="2023" Value="2023" />
                    <telerik:DropDownListItem runat="server" Text="2024" Value="2024" />
                    <telerik:DropDownListItem runat="server" Text="2025" Value="2025" />
                    <telerik:DropDownListItem runat="server" Text="2026" Value="2026" />
                    <telerik:DropDownListItem runat="server" Text="2027" Value="2027" />
                    <telerik:DropDownListItem runat="server" Text="2028" Value="2028" />
                    <telerik:DropDownListItem runat="server" Text="2029" Value="2029" />
                    <telerik:DropDownListItem runat="server" Text="2030" Value="2030" />
                    <telerik:DropDownListItem runat="server" Text="2031" Value="2031" />
                    <telerik:DropDownListItem runat="server" Text="2032" Value="2032" />
                </Items>
            </telerik:RadDropDownList>
        </td>
        <td>
            <asp:Label ID="LabelResult" runat="server"></asp:Label>
        </td>
    </tr>
</table>

<hr />

<telerik:RadAjaxManager ID="RadAjaxManagerInvoiceList" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="RadGridInvoiceList">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGridInvoiceList" LoadingPanelID="RadAjaxLoadingPanelInvoiceList" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManager>

<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanelInvoiceList" runat="server" Skin="Metro"></telerik:RadAjaxLoadingPanel>

<telerik:RadGrid ID="RadGridInvoiceList" runat="server" OnNeedDataSource="RadGridInvoiceList_NeedDataSource"
    AutoGenerateColumns="false" OnItemDataBound="RadGridInvoiceList_ItemDataBound" Skin="Metro">
    <ClientSettings EnableAlternatingItems="true" EnableRowHoverStyle="true">
        <Resizing AllowColumnResize="true" EnableRealTimeResize="true" ClipCellContentOnResize="false" ResizeGridOnColumnResize="false" />
        <Selecting AllowRowSelect="true" />
        <ClientEvents OnRowSelected="rowSelected" />
    </ClientSettings>
    <MasterTableView Name="Invoices" DataKeyNames="Id"
        ClientDataKeyNames="Id" CommandItemDisplay="Top">
        <Columns>
            <telerik:GridBoundColumn UniqueName="InvoiceId"
                HeaderText="Invoice Number" DataField="InvoiceId">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn UniqueName="InvoiceMonth"
                HeaderText="Invoice Month" DataField="InvoiceMonth">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn UniqueName="InvoiceState"
                HeaderText="Invoice Status" DataField="InvoiceState">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn UniqueName="CreationDate"
                HeaderText="Creation Date" DataField="CreationDate"
                DataFormatString="{0:dd/MM/yyyy}">
            </telerik:GridBoundColumn>
            <telerik:GridTemplateColumn UniqueName="ToBePaid" HeaderText="Amount To Be Paid">
                <ItemTemplate>
                    <%# Eval("CurrencyCode") %>
                    <%# Eval("ToBePaid", "{0:0.00}") %>
                </ItemTemplate>
            </telerik:GridTemplateColumn>
            <telerik:GridTemplateColumn UniqueName="Payment2" HeaderText="Pay">
                <ItemTemplate>
                    <asp:HyperLink ID="PayLink" runat="server" ImageUrl="../Images/credit_cards.png"></asp:HyperLink>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                <ItemStyle HorizontalAlign="Center"></ItemStyle>
            </telerik:GridTemplateColumn>
            <telerik:GridBoundColumn UniqueName="Id" HeaderText="Id" DataField="Id" Display="false">
            </telerik:GridBoundColumn>
        </Columns>
        <SortExpressions>
            <telerik:GridSortExpression FieldName="CreationDate" SortOrder="Descending" />
        </SortExpressions>
        <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="true" />
    </MasterTableView>
</telerik:RadGrid>

<telerik:RadWindowManager ID="RadWindowManagerInvoiceList" runat="server" Modal="true" Animation="None"
    DestroyOnClose="true" KeepInScreenBounds="true" EnableShadow="true" VisibleStatusbar="true" Width="1000"
    Height="655" ShowContentDuringLoad="false" Skin="Metro">
    <Windows>
        <telerik:RadWindow ID="RadWindowInvoiceDetails" runat="server" Title="Invoice Details">
        </telerik:RadWindow>
    </Windows>
</telerik:RadWindowManager>
