﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PaypalCheckoutCompleteForm.ascx.cs" Inherits="FOWebApp.Distributors.PaypalCheckoutCompleteForm" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<style type="text/css">
    .auto-style2 {
        width: 102%;
    }
</style>
<telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
    <script type="text/javascript">
        function DoBlur(fld) {
            fld.className = 'normalfld';
        }

        function DoFocus(fld) {
            fld.className = 'focusfld';
        }

        function DoChange(fld) {
            fld.className = 'changedfld';
        }
    </script>
</telerik:RadCodeBlock>
<telerik:RadAjaxPanel ID="RadAjaxPanelCheckoutStart" runat="server" GroupingText="Checkout start" Height="200px" Width="700px"
    EnableHistory="True" Style="margin-right: 114px">
<table cellpadding="10" cellspacing="5" class="checkoutStart">
    <tr>
        <td class="style1">
            <asp:Label ID="Label1" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="style1">
            <asp:Label ID="Label2" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:button ID="ButtonClose" runat="server" text="Close Window" OnClientClick="javascript:window.close(); return false;" />
        </td>
    </tr>
</table>
</telerik:RadAjaxPanel>