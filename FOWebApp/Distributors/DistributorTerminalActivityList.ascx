﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DistributorTerminalActivityList.ascx.cs" Inherits="FOWebApp.DistributorTerminalActivityList" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<h2>Terminal Activity List</h2>
<telerik:RadGrid ID="RadGridTerminalActivity" runat="server" Skin="Metro" 
    EnableHeaderContextMenu="True" ShowStatusBar="True" 
    onneeddatasource="RadGridTerminalActivity_NeedDataSource">
    <ExportSettings>
        <Csv ColumnDelimiter="Semicolon" />
    </ExportSettings>
<MasterTableView>
<CommandItemSettings ExportToPdfText="Export to PDF" ShowAddNewRecordButton="False" 
        ShowExportToCsvButton="True"></CommandItemSettings>

<RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
</RowIndicatorColumn>

<ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
</ExpandCollapseColumn>

<EditFormSettings>
<EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
</EditFormSettings>
</MasterTableView>

<FilterMenu EnableImageSprites="False"></FilterMenu>

<HeaderContextMenu CssClass="GridContextMenu GridContextMenu_WebBlue"></HeaderContextMenu>
</telerik:RadGrid>