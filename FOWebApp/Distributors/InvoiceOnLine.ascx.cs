﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Security;
using System.Web.UI.WebControls;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;

namespace FOWebApp.Distributors
{
    public partial class InvoiceOnLine : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void RadButtonLoadInvoice_Click(object sender, EventArgs e)
        {
            //Prepare the invoice and load the invoice control
            Invoice invoiceControl = (Invoice) Page.LoadControl("~/Invoice.ascx");

            invoiceControl.InvoiceMonth = Int32.Parse(RadComboBoxMonth.SelectedValue);
            invoiceControl.InvoiceYear = Int32.Parse(RadComboBoxYear.SelectedValue);

            //Retrieve the distributor id
            MembershipUser myObject = Membership.GetUser();
            string userId = myObject.ProviderUserKey.ToString();

            //Get the Distributor linked to the user
            BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();
            Distributor dist = boAccountingControl.GetDistributorForUser(userId);

            invoiceControl.DistributorId = (int) dist.Id;
            PlaceHolderInvoice.Controls.Add(invoiceControl);
        }
    }
}