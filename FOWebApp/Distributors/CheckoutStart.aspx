﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CheckoutStart.aspx.cs" Inherits="FOWebApp.Distributors.CheckoutStart" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Checkout</title>
    <link rel="shortcut icon" href="~/Images/favicon_new.ico"/>
    <link rel="icon" type="image/png" href="~/Images/favicon_shortcut.png"/>
    <link href="../CMTStyle.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script type="text/javascript" src="../Scripts/hipay-fullservice-sdk.min.js"></script>
    <script type="text/javascript" src="../Scripts/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="../Scripts/AppConfig.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManagerCheckoutStart" runat="server">
        <Services>
            <asp:ServiceReference Path="../WebServices/AccountSupportWS.asmx" />
        </Services>
    </asp:ScriptManager>
    <div>
        <asp:PlaceHolder ID="PlaceHolderCheckoutStartForm" runat="server"></asp:PlaceHolder>
    </div>
    </form>
</body>
</html>