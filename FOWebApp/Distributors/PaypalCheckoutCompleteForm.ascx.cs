﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using Telerik.Web.UI;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOVoucherControllerWSRef;
using FOWebApp.net.nimera.cmt.BOPaymentControllerWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;

namespace FOWebApp.Distributors
{
    public partial class PaypalCheckoutCompleteForm : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public void componentDataInit()
        {
            string completed = Session["userCheckoutCompleted"].ToString();
            decimal amount = Convert.ToDecimal(Session["amount"]);
            string cur = Session["currency"].ToString();
            string payerId = Session["payerId"].ToString();
            string token = Session["token"].ToString();
            int batchId = Convert.ToInt32(Session["batchId"]);
            string action = Session["Action"].ToString();

            BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();
            BOVoucherControllerWS bovouchercontrol = new BOVoucherControllerWS();
            BOLogControlWS _boLogControlWS = new BOLogControlWS();
            MembershipUser myObject = Membership.GetUser();
            Guid userId = new Guid(myObject.ProviderUserKey.ToString());
            Distributor dist = boAccountingControl.GetDistributorForUser(userId.ToString());
            int distributorId = Convert.ToInt32(dist.Id);

            // Verify user has agreed to complete the checkout process.
            if (completed != "true")
            {
                Session["userCheckoutCompleted"] = "";
                Response.Redirect("CheckoutError.aspx?" + "Desc=Unvalidated%20Checkout.");
            }

            BOPaymentControllerWS boPaymentControl = new BOPaymentControllerWS();

            // Carry out the final step of the Paypal checkout procedure
            string retUrl = boPaymentControl.PaypalCheckoutComplete(amount, cur, payerId, token, action, distributorId);

            // "1" means checkout was succesful 
            if (retUrl != "1")
            {
                Response.Redirect(retUrl);
            }
            else
            {
                Label1.ForeColor = Color.Green;
                Label2.ForeColor = Color.Green;
                Label1.Text = "Payment completed";
                if (action.IndexOf("Activation fee", StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    string mac = Session["macAddress"].ToString();
                    Terminal term = boAccountingControl.GetTerminalDetailsByMAC(mac);
                    term.LateBird = true;
                    if (boAccountingControl.UpdateTerminal(term))
                    {
                        Label2.Text = "Activation fee has been received";
                        string[] eMailRecipients = { "admin@satadsl.net", "sales@satadsl.net" };
                        //string[] eMailRecipients = { "ahmad.satadsl@gmail.com" };
                        string emailBody = "Activation fee for the Terminal : " + mac + " from distributor: " + dist.FullName + " has just been paid via CMT<br/>" +
                                           " <br/>" +
                                           "CMT automated call,please do not reply.";
                        _boLogControlWS.SendMail(emailBody, "Activation fee has just paid", eMailRecipients);
                    }
                    else
                    {
                        Label2.Text = "Activation fee has been received";
                        string[] eMailRecipients = { "admin@satadsl.net", "sales@satadsl.net","support@satadsl.net" };
                        //string[] eMailRecipients = { "ahmad.satadsl@gmail.com" };
                        string emailBody = "Activation fee for the Terminal : " + mac + " from distributor: " + dist.FullName + " has just been paid via CMT <br/>" +
                                           "Dear support team , <br/>" +
                                           "CMT was not able to mark this activation as non-billable, please do it manually after activating this terminal <br/>" +
                                           "CMT automated call,please do not reply.";
                        _boLogControlWS.SendMail(emailBody, "Activation fee has just paid but NOT REGISTERED", eMailRecipients);
                    }

                }
                else
                {
                    VoucherBatch vb = bovouchercontrol.GetVoucherBatchInfo(batchId);
                    bovouchercontrol.FlagVoucherBatchAsPaid(batchId);
                    bovouchercontrol.ReleaseVoucherBatch(batchId);
                    
                    Label2.Text = "Voucher batch " + batchId + " has been released";

                    string[] eMailRecipients = { "admin@satadsl.net", "sales@satadsl.net" };
                    //string[] eMailRecipients = { "ahmad.satadsl@gmail.com" };
                    string emailBody = "Voucher batch with id " + batchId + " from distributor: " + dist.FullName + " has just been paid via CMT<br/>" +
                                       "Voucher volume details: <br/>" +
                                       "SLA: " + boAccountingControl.GetServicePack(vb.Sla).SlaName + "<br/>" +
                                       "No. of vouchers: " + vb.NumVouchers + "<br/>" +
                                       "Paid amount: " + amount + " " + cur + "<br/>" +
                                       "CMT automated call,please do not reply.";
                    _boLogControlWS.SendMail(emailBody, "Voucher batch has just paid", eMailRecipients);
                }
            }
        }
    }
}