﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Reservation.ascx.cs" Inherits="FOWebApp.Distributors.Reservation" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>



<telerik:RadAjaxPanel ID="RadAjaxPanelReservation" runat="server" Height="1600px"
    Width="100%" LoadingPanelID="RadAjaxLoadingPanelReservation">
    <h2>Terminal reservation request form</h2>
    <table cellpadding="10" cellspacing="5">
        <tr>
            <td>
                Terminal:
            </td>
            <td>
                <telerik:RadComboBox runat="server" ID="RadComboBoxTerminal" Skin="Metro" Width="300px">
                </telerik:RadComboBox>
                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ErrorMessage="Choose a terminal" ControlToValidate="RadComboBoxTerminal"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                Start time (UTC time):
            </td>
            <td>
                <telerik:RadDateTimePicker runat="server" ID="RadDateTimePickerStartTime" Skin="Metro" Width="200px">
                </telerik:RadDateTimePicker>
                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ErrorMessage="Choose a date" ControlToValidate="RadDateTimePickerStartTime"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                End time (UTC time):
            </td>
            <td>
                <telerik:RadDateTimePicker runat="server" ID="RadDateTimePickerEndTime" Skin="Metro" Width="200px"></telerik:RadDateTimePicker>
            </td>
        </tr>
        <tr>
            <td>
                SLA:
            </td>
            <td>
                <telerik:RadComboBox runat="server" ID="RadComboBoxSLA" Skin="Metro" Width="300px"></telerik:RadComboBox>
                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ErrorMessage="Choose a Service Pack" ControlToValidate="RadComboBoxSLA"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="ButtonSubmit" runat="server" Text="Submit" OnClick="ButtonSubmit_Click" />
            </td>
            <td>
                <asp:Label ID="LabelResult" runat="server" Text=""></asp:Label>
            </td>
        </tr>
    </table>
    <telerik:RadGrid ID="RadGridSLMBookings" runat="server" AllowSorting="True"
        AutoGenerateColumns="False" CellSpacing="0" GridLines="None" Skin="Metro"
        ShowStatusBar="True" Width="992px" AllowFilteringByColumn="True"
        OnDeleteCommand="RadGridSLMBookings_DeleteCommand"
        PageSize="50">
        <GroupingSettings CaseSensitive="false" />
        <ClientSettings>
            <Selecting AllowRowSelect="True" />
        </ClientSettings>
        <MasterTableView CommandItemDisplay="Top" CommandItemSettings-ShowExportToExcelButton="False"
            CommandItemSettings-ShowExportToCsvButton="False" DataKeyNames="CMTId" RowIndicatorColumn-Visible="True" RowIndicatorColumn-Resizable="True" 
            RowIndicatorColumn-AndCurrentFilterFunction="Contains" CommandItemSettings-ShowAddNewRecordButton="False" ShowFooter="False">
            <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
            <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
            </RowIndicatorColumn>
            <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
            </ExpandCollapseColumn>
            <Columns>
                <telerik:GridBoundColumn DataField="TerminalMac" FilterControlAltText="Filter TerminalMacColumn column" 
                    HeaderText="Terminal Mac"
                    UniqueName="TerminalMac" ShowFilterIcon="False" AutoPostBackOnFilter="True">
                </telerik:GridBoundColumn>
                <telerik:GridDateTimeColumn DataField="StartTime" FilterControlAltText="Filter StartTimeColumn column"
                    HeaderText="Start Time" ShowFilterIcon="False" AutoPostBackOnFilter="True">
                </telerik:GridDateTimeColumn>
                <telerik:GridDateTimeColumn DataField="EndTime" FilterControlAltText="Filter EndTimeColumn column"
                    HeaderText="End Time" ShowFilterIcon="False" AutoPostBackOnFilter="True">
                </telerik:GridDateTimeColumn>
                <telerik:GridDateTimeColumn DataField="Type" FilterControlAltText="Filter TypeColumn column"
                    HeaderText="Type" ShowFilterIcon="False" AutoPostBackOnFilter="True">
                </telerik:GridDateTimeColumn>
                <telerik:GridDateTimeColumn DataField="Status" FilterControlAltText="Filter StatusColumn column"
                    HeaderText="Status" ShowFilterIcon="False" AutoPostBackOnFilter="True">
                </telerik:GridDateTimeColumn>
                <telerik:GridButtonColumn HeaderText="Delete" ImageUrl="~/Images/delete.png" ConfirmText="Are you sure you want to delete this booking?"
                    UniqueName="DeleteColumn" ButtonType="ImageButton" HeaderButtonType="PushButton"
                    Text="Delete" CommandName="Delete">
                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                </telerik:GridButtonColumn>
            </Columns>
            <EditFormSettings>
                <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                </EditColumn>
            </EditFormSettings>
        </MasterTableView>
        <FilterMenu EnableImageSprites="False">
        </FilterMenu>
        <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default">
        </HeaderContextMenu>
    </telerik:RadGrid>

</telerik:RadAjaxPanel>