﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Web.Script.Serialization;
using System.Drawing;
using Telerik.Web.UI;
using FOWebApp.Data;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;
using FOWebApp.App_Code;

namespace FOWebApp.Distributors
{
    public partial class TerminalActivationRequestForm : System.Web.UI.UserControl
    {
        const string _HEXBYTE = "<0|1|2|3|4|5|6|7|8|9|a|b|c|d|e|f><0|1|2|3|4|5|6|7|8|9|a|b|c|d|e|f>";
        const int _Isp = 112;

        protected void Page_Load(object sender, EventArgs e)
        {
            //Initialize the form
            RadMaskedTextBoxMacAddress.Mask =
                _HEXBYTE + "<:>" + _HEXBYTE + "<:>" + _HEXBYTE + "<:>" + _HEXBYTE + "<:>" + _HEXBYTE + "<:>" +
                _HEXBYTE;

            DataHelper dataHelper = new DataHelper();

            //Get the Distributor information an fill up the form
            //Load data into the grid view. This view lists all terminals for the 
            //authenticated user.
            BOAccountingControlWS boAccountingControl =
              new net.nimera.cmt.BOAccountingControlWSRef.BOAccountingControlWS();

            //Get the authenticated user
            MembershipUser myObject = Membership.GetUser();
            string userId = myObject.ProviderUserKey.ToString();


            //Get the Distributor linked to the user
            Distributor dist = boAccountingControl.GetDistributorForUser(userId);

            LabelDistributor.Text = dist.FullName;

            if (dist.FullName.Length > 10)
            {
                LabelDistribName.Text = dist.FullName.Substring(0, 10);
            }
            else
            {
                LabelDistribName.Text = dist.FullName.Trim();
            }

            //Fill in the list of customers
            Organization[] organizations = boAccountingControl.GetOrganisationsByDistributor(userId);

            RadComboBoxItem item = null;

            foreach (Organization org in organizations)
            {
                item = new RadComboBoxItem();
                item.Value = org.Id.ToString();
                item.Text = org.FullName;
                RadComboBoxCustomer.Items.Add(item);
            }

            //Fill-up the treeview in the RadComboBox
            Isp[] isps = boAccountingControl.GetISPsForDistributor((int)dist.Id);
            RadTreeView slaTree = (RadTreeView)RadComboBoxSLA.Items[0].FindControl("RadTreeViewSLA");
            foreach (Isp isp in isps)
            {
                RadTreeNode rtn = new RadTreeNode(isp.CompanyName, isp.Id.ToString());
                rtn.ImageUrl = "~/Images/contract.png";
                rtn.ToolTip = isp.Id.ToString();

                //Add child nodes to the tree node
                ServiceLevel[] serviceLevels = boAccountingControl.GetServicePacksByIsp((int)isp.Id);
                
                RadTreeNode slNode = null;
                foreach (ServiceLevel sl in serviceLevels)
                {
                    slNode = new RadTreeNode(sl.SlaName, sl.SlaId.ToString());
                    slNode.ToolTip = sl.SlaCommonName;
                    rtn.Nodes.Add(slNode);
                }
                slaTree.Nodes.Add(rtn);
            }

            
        }

        protected void ButtonSave_Click(object sender, EventArgs e)
        {
            Terminal term = null;

            //Check acceptance checkboxes first
            if (CheckBoxDistributorAcceptance.Checked && CheckBoxCustomerAcceptance.Checked)
            {
                BOAccountingControlWS boAccountingControl =
                                new net.nimera.cmt.BOAccountingControlWSRef.BOAccountingControlWS();

                term = boAccountingControl.GetTerminalDetailsByMAC(RadMaskedTextBoxMacAddress.Text);

                //Save the data to the database and send out the necessary E-Mails
                if (term != null)
                {
                    LabelResult.ForeColor = Color.Red;
                    LabelResult.Text = "The MAC address is not unique";
                    return;
                }

                //First check if the MAC address is unique, otherwise set an error message and return
                BOLogControlWS boLogControlWS = new BOLogControlWS();

                try
                {

                    //Get the authenticated user
                    MembershipUser myObject = Membership.GetUser();
                    string userId = myObject.ProviderUserKey.ToString();


                    //Get the Distributor linked to the user
                    Distributor dist = boAccountingControl.GetDistributorForUser(userId);

                    //Get the Organization details specified for this terminal
                    Organization org = boAccountingControl.GetOrganization(Int32.Parse(RadComboBoxCustomer.SelectedValue));

                    term = new Terminal();
                    term.Address = new Address();

                    //CMTSUPPORT-244
                    term.MacAddress = RadMaskedTextBoxMacAddress.Text;
                    term.Serial = TextBoxSerial.Text;

                    //Fill the term entity
                    term.Address.AddressLine1 = TextBoxAddressLine1.Text;
                    term.Address.AddressLine2 = TextBoxAddressLine2.Text;
                    term.Address.Country = CountryList.SelectedValue;
                    term.Address.Location = TextBoxTerminalLocation.Text;
                    term.Address.PostalCode = TextBoxPCO.Text;
                    term.AddressType = 0; //Public IP
                    term.AdmStatus = 5; //Request
                    term.Blade = 3;
                    term.DistributorId = dist.Id;
                    term.DistributorName = dist.FullName;
                    term.Email = TextBoxEMailAddress.Text;
                    term.ExpiryDate = RadDatePickerExpiryDate.SelectedDate;
                    term.FirstActivationDate = RadDatePickerActivationDate.SelectedDate;
                    term.FullName = LabelDistribName.Text + "-" + TextBoxTerminalName.Text + "-" + TextBoxSerial.Text;
                    term.ExtFullName = string.Concat(term.MacAddress + term.Serial).Replace(":", "-");
                    term.CNo = (decimal)-1.0;
                    term.IPMask = 32; //Not selectable for the moment
                    term.iLNBBUC = RadComboBoxILNBBUC.SelectedItem.Text;
                    term.Antenna = RadComboBoxAntenna.SelectedItem.Text;
                    Coordinate termCo = new Coordinate();
                    termCo.Latitude= Convert.ToDouble(TextBoxLatitude.Text);
                    termCo.Longitude= Convert.ToDouble(TextBoxLongitude.Text);
                    term.LatLong = termCo;

                    HttpRequestWrapper requestWrapper = new HttpRequestWrapper(Request);
                    //int selectedSlaId = Int32.Parse(ExtractComboBoxValue(requestWrapper, "RadComboBoxSLA"));
                    int selectedSlaId = Int32.Parse(HiddenFieldSLAId.Value);
                    ServiceLevel sl = boAccountingControl.GetServicePack(selectedSlaId);
                    term.IspId = (int)sl.IspId;

                    term.OrgId = org.Id;
                    term.Phone = TextBoxPhone.Text;

                    //Add all the values we do not have regular fields for.
                    term.Remarks =
                        "Contact First Name: " + TextBoxFirstName.Text + "\n\r" +
                        "Contact Surname: " + TextBoxSurname.Text + "\n\r" +
                        "Contact fixed phone: " + TextBoxPhone.Text + "\n\r" +
                        "Contact Mobile phone: " + TextBoxMobile.Text + "\n\r" +
                        "Installation Engineer: " + TextBoxInstEngineer.Text + "\n\r" +
                        "Requested installation Date: " + RadDatePickerInstallationDate.SelectedDate;

                    term.SitId = 999999;
                    term.SlaId = sl.SlaId;
                    term.SlaName = sl.SlaName;
                    term.StartDate = RadDatePickerStartDate.SelectedDate;
                    term.TestMode = false;
                    term.IpAddress = "127.0.0.1"; //Localhost used for requests

                    //Write to the database
                    if (boAccountingControl.UpdateTerminal(term))
                    {
                        
                        String eMailMessage = this.prepareEMailText(term, dist);

                        //Send out the E-Mails to the NOCSA
                        //First get a list all E-Mail addresses which belong to the NOCSA role
                        string[] nocsaRoleNames = Roles.GetUsersInRole("NOC Administrator");

                        List<string> mailAddresses = new List<string>();
                        mailAddresses.Add(FOWebApp.Properties.Settings.Default.SupportEmail);

                        if (boLogControlWS.SendMail(eMailMessage, "Terminal Activation Request", mailAddresses.ToArray()))
                        {
                            //Flag a successful request
                            LabelResult.ForeColor = Color.Green;
                            LabelResult.Text = "Your request has been successully submitted to SatADSL. A notification email will be sent to you once the terminal is activated.";
                        }
                        else
                        {
                            LabelResult.ForeColor = Color.Red;
                            LabelResult.Text = "Your request has been successully submitted to SatADSL, please contact Support  to follow up on your request.";
                        }

                        if (CheckBoxPayActivation.Checked)
                        {
                            if (sl.Voucher == true)
                            {
                                
                                    LabelRedirect.ForeColor = Color.Green;
                                    LabelRedirect.Text = "You will be redirected to PayPal in order to pay the activation fee";
                                   ScriptManager.RegisterStartupScript(Page,this.GetType(), "PopupScript", String.Format("ShowPayForm('{0}');", term.MacAddress), true);
                                //csm.RegisterStartupScript(this.GetType(), "PopupScript", String.Format("ShowPayForm('{0}');", term.MacAddress), true);
                                //window.open("Distributors/CheckoutStart.aspx?BatchId=" + id, "WindowPopup", "width=1000px, height=700px, resizable, scrollbars");
                                //Response.Redirect("Distributors/CheckoutStartActivation.aspx?ama=" + term.MacAddress, false);
                            }
                            else
                            {
                                LabelRedirect.ForeColor = Color.Red;
                                LabelRedirect.Text = "You cannot pay an activation fee in advance for a non-voucher terminal";
                            }
                        }
                    }
                    else
                    {
                        LabelResult.ForeColor = Color.Red;
                        LabelResult.Text = "An error has occurred while submitting your request to SatADSL. Please contact support for more information.";
                    }

                    this.clearForm();
                }
                catch (Exception ex)
                {
                    CmtApplicationException cmtException = new CmtApplicationException();
                    cmtException.ExceptionDateTime = DateTime.Now;
                    cmtException.ExceptionDesc = ex.Message;
                    cmtException.ExceptionStacktrace = ex.StackTrace;
                    cmtException.UserDescription = "TerminalActivationRequestForm - ButtonSave_Click";
                    boLogControlWS.LogApplicationException(cmtException);

                    LabelResult.ForeColor = Color.Red;
                    LabelResult.Text = "An error has occurred while submitting your request to SatADSL. Please contact support for more information.";
                }
            }
            else
            {
                if (!CheckBoxDistributorAcceptance.Checked)
                {
                    RadWindowManager1.RadAlert("You must accept the installation before the activation request can be handled by SatADSL", 400, 200, "Distributor acceptance", null);
                }
                else if (!CheckBoxCustomerAcceptance.Checked)
                {
                    RadWindowManager1.RadAlert("The customer must accept the installation before the activation request can be handled by SatADSL", 250, 200, "Distributor acceptance", null);
                }
            }
        }

        /// <summary>
        /// Prepares the text which needs to be sent to the NOCSA
        /// </summary>
        /// <param name="term"></param>
        /// <returns></returns>
        private string prepareEMailText(Terminal term, Distributor dist)
        {
            string msgTxt = "Activation Request from Distributor: " + dist.FullName + "<br/>";
            msgTxt = msgTxt + "Date and time filed: " + DateTime.Now + "<br/>";
            msgTxt = msgTxt + "Mac Address: " + term.MacAddress + "<br/>";
            msgTxt = msgTxt + "Terminal Fullname: " + term.FullName + "<br/>";

            BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();
            ServiceLevel sl = boAccountingControl.GetServicePack((int)term.SlaId);
            if (sl != null)
            {
                msgTxt = msgTxt + "SLA: " + term.SlaId + ", " + sl.SlaCommonName + "<br/>";
            }
            msgTxt = msgTxt + "<br/>";

            msgTxt = msgTxt + "The request is now available in the list of terminal activation requests<br/>";

            msgTxt = msgTxt + "CMT automated call do not respond.";
            
            return msgTxt;
        }

        /// <summary>
        /// Just clears the form
        /// </summary>
        private void clearForm()
        {
            RadMaskedTextBoxMacAddress.Text = "";
            TextBoxAddressLine1.Text = "";
            TextBoxAddressLine2.Text = "";
            TextBoxEMailAddress.Text = "";
            TextBoxTerminalName.Text = "";
            TextBoxFirstName.Text = "";
            TextBoxInstEngineer.Text = "";
            TextBoxMobile.Text = "";
            TextBoxPCO.Text = "";
            TextBoxPhone.Text = "";
            TextBoxSerial.Text = "";
            TextBoxSurname.Text = "";
            TextBoxTerminalLocation.Text = "";
            CountryList.SelectedIndex = 0;
            RadComboBoxCustomer.SelectedIndex = 0;
            RadDatePickerActivationDate.Clear();
            RadDatePickerExpiryDate.Clear();
            RadDatePickerInstallationDate.Clear();
            RadDatePickerStartDate.Clear();
            CheckBoxCustomerAcceptance.Checked = false;
            CheckBoxDistributorAcceptance.Checked = false;
            CheckBoxPayActivation.Checked = false;
            RadComboBoxSLA.Text = String.Empty;
            RadComboBoxAntenna.SelectedIndex = 0;
            RadTreeView slaTree = (RadTreeView)RadComboBoxSLA.Items[0].FindControl("RadTreeViewSLA");
            slaTree.UnselectAllNodes();
            slaTree.CollapseAllNodes();
            RadComboBoxSLA.ClearSelection();
            RadComboBoxSLA.Text = string.Empty;
        }

        #region RadComboBox helpers
        /// <summary>
        /// Extracts the String value from a RadComboBox control
        /// </summary>
        /// <param name="request"></param>
        /// <param name="controlId"></param>
        /// <returns></returns>
        public static string ExtractStringValue(HttpRequestBase request, string controlId)
        {
            return request.Form.Keys.OfType<string>()
                .Where(postedValue => postedValue.EndsWith(controlId))
                .Select(postedValue => request.Form[postedValue])
                .FirstOrDefault();
        }

        #endregion

        /// <summary>
        /// Checks if the MAC address exists
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadXmlHttpPanelMacAddress_ServiceRequest(object sender, RadXmlHttpPanelEventArgs e)
        {
            BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();
            string macAddress = e.Value;
            Terminal term = boAccountingControl.GetTerminalDetailsByMAC(macAddress);

            if (term != null)
            {
                //Terminal already exists, clear the mac address field and show an error
                //window
                LabelMacExists.ForeColor = Color.Red;
                LabelMacExists.Text = "Sorry but this MAC address already exists!";
            }
            else
            {
                if (macAddress.Equals("00:00:00:00:00:00"))
                {
                    LabelMacExists.ForeColor = Color.Red;
                    LabelMacExists.Text = "You must provide a valid MAC address!";
                }
                else
                {
                    LabelMacExists.ForeColor = Color.Green;
                    LabelMacExists.Text = "MAC address accepted!";
                }
            }
            RadMaskedTextBoxMacAddress.ClientEvents.OnValueChanged = "OnMacAddressChanged";
            RadMaskedTextBoxMacAddress.Text = macAddress;
        }

        protected void RadXmlHttpPanelCustomer_ServiceRequest(object sender, RadXmlHttpPanelEventArgs e)
        {
            BOAccountingControlWS boAccountingControl = new BOAccountingControlWS(); 
            int slaId = Convert.ToInt32(e.Value);
            ServiceLevel sl = boAccountingControl.GetServicePack(slaId);
            int ispId = Convert.ToInt32(sl.IspId);

            if (ispId == 473) //SoHo ISP
            {
                MembershipUser myObject = Membership.GetUser();
                string userId = myObject.ProviderUserKey.ToString();
                Distributor distributor = boAccountingControl.GetDistributorForUser(userId);
                RadComboBoxCustomer.SelectedValue = distributor.DefaultOrganization.ToString();
                RadComboBoxCustomer.Enabled = false;
            }
            else
            {
                RadComboBoxCustomer.Enabled = true;
            }
        }
    }
}