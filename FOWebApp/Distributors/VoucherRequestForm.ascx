﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VoucherRequestForm.ascx.cs" Inherits="FOWebApp.Distributors.VoucherRequestForm" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<style type="text/css">
    .auto-style1 {
        width: 450px;
    }
    .auto-style2 {
        height: 65px;
    }
</style>
<script type="text/javascript">
        function nodeClicking(sender, args) {
            var comboBox = $find("<%= RadComboBoxSLA.ClientID %>");

            var node = args.get_node()
            if (node.get_level() == 1) {

                comboBox.set_text(node.get_text());
                comboBox.set_value(node.get_value());
                document.getElementById('HiddenFieldSLAId').value = node.get_value();
                comboBox.trackChanges();
                comboBox.get_items().getItem(0).set_text(node.get_text());
                comboBox.commitChanges();
                comboBox.hideDropDown();
                
            }            
        }
            
</script>
<asp:HiddenField ID="HiddenFieldSLAId" runat="server" ClientIDMode="Static" />
<table cellpadding="10" cellspacing="5">
    <tr>
        <td class="auto-style2"><h1>Request a new voucher batch</h1></td>
    </tr>
    <tr>
        <td>
            &nbsp;</td>
        <td class="auto-style1">
            &nbsp;</td>
    </tr>
    <tr>
        <td>
            Voucher Type</td>
        <td class="auto-style1">
            <telerik:RadComboBox ID="RadComboBoxSLA" runat="server" Skin="Metro" Sort="Ascending" Width="532px" EnableVirtualScrolling="True" EmptyMessage="Select a Service Pack"
                    Height="200px">
                    <ItemTemplate>
                        <telerik:RadTreeView ID="RadTreeViewSLA" runat="server" Skin="Metro" OnClientNodeClicking="nodeClicking">
                        </telerik:RadTreeView>
                    </ItemTemplate>
                    <Items>
                        <telerik:RadComboBoxItem Text="" />
                    </Items>
                    <ExpandAnimation Type="Linear" />
                </telerik:RadComboBox>
        </td>
    </tr>
    <tr>
        <td>
            Number of vouchers to create:
        </td>
        <td class="auto-style1">
            <telerik:RadNumericTextBox ID="RadNumericTextBoxvouchers" runat="server" 
                EmptyMessage="Number of vouchers" MaxValue="1000" MinValue="0" NumberFormat-DecimalDigits="0"
                ShowButton="False" ShowSpinButtons="True" Value="10" Height="21px" Skin="Metro"
                Width="125px" Culture="en-GB">
                <NumberFormat DecimalDigits="0" ZeroPattern="n" decimalseparator=" " groupseparator=""></NumberFormat>
            </telerik:RadNumericTextBox>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <asp:button ID="ButtonRequestVouchers" runat="server" Text="Request Voucher(s)" OnClick="ButtonRequestVouchers_Click">
            </asp:button>
        </td>
    </tr>
    <tr>
        <td colspan="2"><asp:Label ID="LabelResult" runat="server"></asp:Label></td>
    </tr>
</table>
        