﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BillableList.ascx.cs" Inherits="FOWebApp.Distributors.BillableList" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxyBillableList" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="RadGridBillableList">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGridBillableList" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>

<%-- Using the loading panel in Default.aspx --%>
<%--<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanelBillableList" runat="server"></telerik:RadAjaxLoadingPanel>--%>

<telerik:RadGrid ID="RadGridBillableList" runat="server" OnNeedDataSource="RadGridBillableList_NeedDataSource" AutoGenerateColumns="false"
    OnLoad="RadGridBillableList_Load">
    <ClientSettings EnableAlternatingItems="true" EnableRowHoverStyle="true">
        <Resizing AllowColumnResize="true" EnableRealTimeResize="true" ClipCellContentOnResize="false" ResizeGridOnColumnResize="false" />
    </ClientSettings>
    <MasterTableView Name="Billables" DataKeyNames="Bid" ClientDataKeyNames="Bid" CommandItemDisplay="Top">
        <Columns>
            <telerik:GridBoundColumn UniqueName="Bid"
                HeaderText="ID" DataField="Bid">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn UniqueName="Description"
                HeaderText="Description" DataField="Description">
            </telerik:GridBoundColumn>
            <telerik:GridNumericColumn UniqueName="PriceUsd"
                HeaderText="Price" DataField="PriceUSD"
                DataFormatString="$ {0:0.00}" Visible="false">
            </telerik:GridNumericColumn>
            <telerik:GridNumericColumn UniqueName="PriceEu"
                HeaderText="Price" DataField="PriceEU"
                DataFormatString="€ {0:0.00}" Visible="false">
            </telerik:GridNumericColumn>
        </Columns>
        <SortExpressions>
            <telerik:GridSortExpression FieldName="Bid" SortOrder="Ascending" />
        </SortExpressions>
        <CommandItemSettings ShowAddNewRecordButton="false" ShowExportToCsvButton="true" />
    </MasterTableView>
    <ExportSettings ExportOnlyData="false" IgnorePaging="true" OpenInNewWindow="true" FileName="Billables">
    </ExportSettings>
</telerik:RadGrid>