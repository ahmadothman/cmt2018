﻿using FOWebApp.net.nimera.cmt.BOInvoicingControllerWSRef;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace FOWebApp.Distributors
{
    public partial class InvoiceList : System.Web.UI.UserControl
    {
        private BOInvoicingControllerWS _invoiceController = new BOInvoicingControllerWS();
        private BOAccountingControlWS _accountingController = new BOAccountingControlWS();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (RadDropDownListYear.FindItemByValue("-1").Selected == true)
            {
                RadDropDownListYear.FindItemByValue("-1").Selected = false;
                RadDropDownListYear.FindItemByValue(DateTime.Now.Year.ToString()).Selected = true;
            }
        }

        protected void RadDropDownListYear_SelectedIndexChanged(object sender, Telerik.Web.UI.DropDownListEventArgs e)
        {
            try
            {
                RadGridInvoiceList.DataSource = null; // Need to do this, otherwise the NeedDataSource event is not triggered on rebind
                RadGridInvoiceList.Rebind();
                LabelResult.ForeColor = Color.DarkGreen;
                LabelResult.Text = "Query execution completed";
            }
            catch (Exception)
            {
                LabelResult.ForeColor = Color.DarkRed;
                LabelResult.Text = "Query execution failed";
            }
        }
              
        protected void RadGridInvoiceList_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (RadDropDownListYear.FindItemByValue("-1").Selected == false)
            {
                // Null needs to be given explicitily as a parameter so the method knows this property doesn't need to be searched for
                string[] selectedYears = new string[] { RadDropDownListYear.SelectedValue };
                ((RadGrid)sender).MasterTableView.DataSource = _invoiceController.QueryInvoicesMultipleArgs(
                    years: selectedYears,
                    months: null,
                    invoiceNums: null,
                    invoiceStates: new[] { "2", "3", "4" },
                    distIds: new[] { _accountingController.GetDistributorForUser(Membership.GetUser().ProviderUserKey.ToString()).Id.ToString() }
                    );
            }
        }

        protected void RadGridInvoiceList_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem dataItem = (GridDataItem)e.Item;

                TableCell invoiceMonthCell = dataItem["InvoiceMonth"];
                TableCell invoiceStateCell = dataItem["InvoiceState"];
                string invoiceId = Convert.ToString(dataItem["Id"].Text);
                //TableCell invoiceTotalPriceCell = dataItem["TotalPrice"];

                switch (invoiceMonthCell.Text)
                {
                    case "1":
                        invoiceMonthCell.Text = "January";
                        break;
                    case "2":
                        invoiceMonthCell.Text = "February";
                        break;
                    case "3":
                        invoiceMonthCell.Text = "March";
                        break;
                    case "4":
                        invoiceMonthCell.Text = "April";
                        break;
                    case "5":
                        invoiceMonthCell.Text = "May";
                        break;
                    case "6":
                        invoiceMonthCell.Text = "June";
                        break;
                    case "7":
                        invoiceMonthCell.Text = "July";
                        break;
                    case "8":
                        invoiceMonthCell.Text = "August";
                        break;
                    case "9":
                        invoiceMonthCell.Text = "September";
                        break;
                    case "10":
                        invoiceMonthCell.Text = "October";
                        break;
                    case "11":
                        invoiceMonthCell.Text = "November";
                        break;
                    case "12":
                        invoiceMonthCell.Text = "December";
                        break;
                }

                switch (invoiceStateCell.Text)
                {
                    case "0":
                        invoiceStateCell.Text = "Unvalidated";
                        dataItem["Payment2"].ToolTip = "Waiting for validation";
                        break;
                    case "1":
                        invoiceStateCell.Text = "Validated";
                        dataItem["Payment2"].ToolTip = "Waiting for release";
                        break;
                    case "2":
                        invoiceStateCell.Text = "Released";
                        // enable the hyperlink to proceed to the checkout
                        HyperLink payLink = (HyperLink)e.Item.FindControl("PayLink");
                        payLink.Attributes["href"] = "javascript:void(0);";
                        payLink.Attributes["onclick"] = String.Format("return ShowPayForm('{0}');", invoiceId);
                        break;
                    case "3":
                        invoiceStateCell.Text = "Accepted";
                        // enable the hyperlink to proceed to the checkout
                        HyperLink payLinkAccepted = (HyperLink)e.Item.FindControl("PayLink");
                        payLinkAccepted.Attributes["href"] = "javascript:void(0);";
                        payLinkAccepted.Attributes["onclick"] = String.Format("return ShowPayForm('{0}');", invoiceId);
                        break;
                    case "4":
                        invoiceStateCell.Text = "Paid";
                        dataItem["Payment2"].ToolTip = "Paid";
                        break;
                }
            }
        }
    }
}