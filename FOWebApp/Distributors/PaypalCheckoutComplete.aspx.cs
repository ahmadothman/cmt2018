﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using Telerik.Web.UI;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOVoucherControllerWSRef;
using FOWebApp.net.nimera.cmt.BOPaymentControllerWSRef;

namespace FOWebApp.Distributors
{
    public partial class PaypalCheckoutComplete : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Load the details form and initialize the data

            PaypalCheckoutCompleteForm df =
                (PaypalCheckoutCompleteForm)Page.LoadControl("PaypalCheckoutCompleteForm.ascx");

            df.componentDataInit();

            PlaceHolderPaypalCheckoutCompleteForm.Controls.Add(df);
        }
    }
}