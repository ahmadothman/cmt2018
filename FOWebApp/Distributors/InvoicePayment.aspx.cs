﻿using FOWebApp.net.nimera.cmt.BOLogControlWSRef;
using Stripe;
using System;
using System.Configuration;
using System.Drawing;
using FOWebApp.net.nimera.cmt.BOInvoicingControllerWSRef;

namespace FOWebApp.Distributors
{
    public partial class InvoicePayment : System.Web.UI.Page
    {
        private BOInvoicingControllerWS invoiceController = new BOInvoicingControllerWS();
        private InvoiceHeader invoice = new InvoiceHeader();
        public int invoiceId;
        public string currency;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                invoiceId = Convert.ToInt32(Request.Params["InvoiceId"]);
                invoice = invoiceController.GetInvoice(invoiceId);
                ViewState["invoice"] = invoice;
                string currencySign = invoice.CurrencyCode.ToLower().Trim() == "eur" ? "€" : "$";
                double fee = currencySign == "€" ? 0.25 : 0.30;
                var amount = Math.Round((invoice.ToBePaid * (decimal)1.029) + (decimal)fee, 2) ;
                labelAmountToPayStripeInvoice.Text = amount + currencySign + " (charges included)";
            }
            else
            {
                string stripeToken = this.Request.Form.Get("inputStripeToken");
                string hiPayToken = this.Request.Form.Get("inputHiPayToken");
                if (!String.IsNullOrEmpty(stripeToken))
                {
                    SubmitStripePayment(stripeToken);
                    //SubmitHiPayPayment(hiPayToken);
                }
            }
        }

        private void SubmitStripePayment(string token)
        {
            invoice = (InvoiceHeader)ViewState["invoice"];
            if (invoice.InvoiceState == 4)
            {
                labelPaymentResultInvoice.ForeColor = Color.Blue;
                labelPaymentResultInvoice.Text = "Invoice has already been paid for.";
                return;
            }
            StripeConfiguration.SetApiKey(ConfigurationManager.AppSettings["StripeSecretKey"]);
            var myCharge = new StripeChargeCreateOptions();
            // always set these properties
            string currency = invoice.CurrencyCode.ToLower().Trim();
            int fee = currency == "eur" ? 25 : 30;
            myCharge.Amount = Convert.ToInt32(Math.Round(invoice.ToBePaid * (decimal)1.029, 2) * 100) + fee;
            myCharge.Currency = invoice.CurrencyCode.ToLower().Trim();
            // set this if you want to
            myCharge.Description = "Distributor " + invoice.DistributorName + "  id " + invoice.DistributorId + " - Payment for invoice " + invoice.InvoiceId;
            myCharge.SourceTokenOrExistingSourceId = token;
            // set this if you have your own application fees (you must have your application configured first within Stripe)
            //myCharge.ApplicationFee = 25;
            // (not required) set this to false if you don't want to capture the charge yet - requires you call capture later
            myCharge.Capture = true;
            var chargeService = new StripeChargeService();
            try
            {
                StripeCharge stripeCharge = chargeService.Create(myCharge);
                if (stripeCharge.Paid)
                {
                    labelPaymentResultInvoice.ForeColor = Color.Green;
                    string paymentResult = "Payment successful. Invoice marked as paid.";
                    labelPaymentResultInvoice.Text = paymentResult;
                    invoice.InvoiceState = 4;
                    invoiceController.UpdateInvoice(invoice);
                }
                else
                {
                    labelPaymentResultInvoice.ForeColor = Color.Red;
                    labelPaymentResultInvoice.Text = "Transaction failed. " + stripeCharge.FailureMessage;
                }
            }
            catch (Exception e)
            {
                labelPaymentResultInvoice.ForeColor = Color.Red;
                labelPaymentResultInvoice.Text = e.Message;
                BOLogControlWS boLogControlWS = new BOLogControlWS();
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDateTime = DateTime.Now;
                cmtEx.ExceptionDesc = e.Message;
                cmtEx.UserDescription = myCharge.Description;
                cmtEx.ExceptionLevel = 4;
                boLogControlWS.LogApplicationException(cmtEx);
            }
        }
    }
}