﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PaypalCheckoutReview.ascx.cs" Inherits="FOWebApp.Distributors.PaypalCheckoutReview1" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<style type="text/css">
    .auto-style2 {
        width: 102%;
    }
</style>
<telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
    <script type="text/javascript">
        function DoBlur(fld) {
            fld.className = 'normalfld';
        }

        function DoFocus(fld) {
            fld.className = 'focusfld';
        }

        function DoChange(fld) {
            fld.className = 'changedfld';
        }
    </script>
</telerik:RadCodeBlock>
<telerik:RadAjaxPanel ID="RadAjaxPanelCheckoutStart" runat="server" GroupingText="Checkout start" Height="200px" Width="700px"
    EnableHistory="True" Style="margin-right: 114px">
<table cellpadding="10" cellspacing="5" class="checkoutReview">
    <tr>
        <td colspan="2">
            <h2>Please review the details below and confirm the payment</h2>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="Label1" runat="server" Text="Activity" Width="150px"></asp:Label>
        </td>
        <td class="style1">
            <asp:Label ID="LabelAction" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="Label5" runat="server" Text="Amount to pay:"></asp:Label>
        </td>
        <td class="style1" style="white-space:nowrap;">
            <asp:Label ID="LabelAmountToPay" runat="server" Font-Bold="True"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Button ID="ButtonConfirmPayment" runat="server" Text="Confirm Payment" OnClick="ButtonConfirmPayment_Click" />
        </td>
        <td></td>
    </tr>
    <tr>
        <td>
            <asp:Button ID="ButtonCancel" runat="server" Text="Cancel" OnClick="ButtonCancel_Click" />
        </td>
        <td></td>
    </tr>
</table>
</telerik:RadAjaxPanel>