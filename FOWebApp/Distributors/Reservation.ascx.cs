﻿using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;
using FOWebApp.net.nimera.cmt.BOSatLinkManagerControlWSRef;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace FOWebApp.Distributors
{
    public partial class Reservation : System.Web.UI.UserControl
    {
        BOAccountingControlWS _accountingController = new BOAccountingControlWS();
        BOSatLinkManagerControlWS _satLinkController = new BOSatLinkManagerControlWS();
        Distributor dist;
        Organization org;

        protected void Page_Load(object sender, EventArgs e)
        {
            MembershipUser myObject = Membership.GetUser();
            string userId = myObject.ProviderUserKey.ToString();

            //Get the Distributor linked to the user
            Terminal[] terminals = new Terminal[]{};

            // Get the role of the current user to know whether to give the terminal list of the distributor or organization.
            // 
            if (HttpContext.Current.User.IsInRole("Distributor"))
            {
                terminals = _accountingController.GetTerminalsByDistributor(userId);
                dist = _accountingController.GetDistributorForUser(userId);
                RadGridSLMBookings.DataSource = _satLinkController.GetSLMBookingsByDistributor((int)dist.Id);
                RadGridSLMBookings.DataBind();
            }
            else if (HttpContext.Current.User.IsInRole("Organization"))
            {
                Organization org = _accountingController.GetOrganizationForUser(userId);
                terminals = _accountingController.GetTerminalsByOrganization((int)org.Id);
                RadGridSLMBookings.DataSource = _satLinkController.GetSLMBookingsByCustomer((int)org.Id);
                RadGridSLMBookings.DataBind();
            }

            //The ISP id's (211,201) are only used by CMT to access SES Dialog, 201 is regular data access and 211 for SLM
            ServiceLevel[] servicePacks = _accountingController.GetServicePacksByIsp(211);

            foreach (Terminal term in terminals)
            {
                if (term.IspId == 201)
                {
                    RadComboBoxItem item = new RadComboBoxItem();
                    item.Value = term.MacAddress;
                    item.Text = term.FullName;
                    RadComboBoxTerminal.Items.Add(item);
                }
            }
            foreach (ServiceLevel sla in servicePacks)
            {
                RadComboBoxItem item = new RadComboBoxItem();
                item.Value = sla.SlaId.ToString();
                item.Text = sla.SlaName;
                RadComboBoxSLA.Items.Add(item);
            }

            AddInitialValueToComboBox(RadComboBoxTerminal);
            AddInitialValueToComboBox(RadComboBoxSLA);
        }

        /// <summary>
        /// Inserts a RadComboBoxItem with an emtpy string as text and 0 as value in the RadComboBox at index 0
        /// </summary>
        /// <param name="comboBox">The RadComboBox in which to insert the RadComboBoxItem</param>
        private void AddInitialValueToComboBox(RadComboBox comboBox)
        {
            RadComboBoxItem item = new RadComboBoxItem(string.Empty, "0");
            comboBox.Items.Insert(0, item);
        }

        protected void ButtonSubmit_Click(object sender, EventArgs e)
        {
            SLMBooking booking = new SLMBooking();
            Terminal term = _accountingController.GetTerminalDetailsByMAC(RadComboBoxTerminal.SelectedValue.ToString());

            if (HttpContext.Current.User.IsInRole("Distributor"))
            {
                booking.DistributorId = (int)dist.Id;
                booking.CustomerId = 0;
            }
            else
            {
                booking.CustomerId = (int)org.Id;
                booking.DistributorId = 0;
            }
            booking.TerminalMac = RadComboBoxTerminal.SelectedValue.Trim();// testing mac address OFIS A2G "00:06:39:8a:26:fe" 

            if (RadDateTimePickerStartTime.SelectedDate > DateTime.UtcNow)
            {
                booking.StartTime = Convert.ToDateTime(RadDateTimePickerStartTime.SelectedDate.ToString());
            }
            else
            {
                LabelResult.ForeColor = Color.Red;
                LabelResult.Text = "The start time has to be later than This moment.";
                return;
            }
            if (RadDateTimePickerEndTime.SelectedDate != null)
            {
                if (RadDateTimePickerEndTime.SelectedDate > RadDateTimePickerStartTime.SelectedDate)
                {
                    booking.EndTime = Convert.ToDateTime(RadDateTimePickerEndTime.SelectedDate.ToString());
                }
                else
                {
                    LabelResult.ForeColor = Color.Red;
                    LabelResult.Text = "The end date cannot be sooner than the start date.";
                    return;
                }
            }
            else
            {
                booking.EndTime = null;
            }

            booking.BookingSlaId = Convert.ToInt32(RadComboBoxSLA.SelectedValue.ToString());
            // This one gets reset if anything fails and the method stops, meaning the combobox is empty, but appears to still be filled. Same for SLA
            booking.TerminalSlaId = (int)term.SlaId;
            booking.Type = null;
            booking.Status = "New";
            booking.SLMId = null;

            if (_satLinkController.UpdateSLMBooking(booking))
            {
                LabelResult.ForeColor = Color.Green;
                LabelResult.Text = "SLMBooking succesfully added.";
            }
            else
            {
                LabelResult.ForeColor = Color.Red;
                LabelResult.Text = "Adding SLMBooking failed.";
            }
        }

        protected void RadGridSLMBookings_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            GridDataKeyArray dataItems = e.Item.OwnerTableView.DataKeyValues;
            int cmtId = (int)dataItems[e.Item.ItemIndex]["CMTId"];
            BOLogControlWS boLogControlWS = new BOLogControlWS();

            //Delete the booking with the given CMTId
            try
            {
                BOAccountingControlWS boAccountingControlWS = new BOAccountingControlWS();
                if (!_satLinkController.DeleteSLMBooking(cmtId))
                {
                    CmtApplicationException cmtException = new CmtApplicationException();
                    cmtException.ExceptionDateTime = DateTime.Now;
                    cmtException.ExceptionDesc = "Could not delete booking with CMTId: " + cmtId;
                    cmtException.StateInformation = "CMTId probably not a number";
                    boLogControlWS.LogApplicationException(cmtException);
                }

                if (HttpContext.Current.User.IsInRole("Distributor"))
                {
                    RadGridSLMBookings.DataSource = _satLinkController.GetSLMBookingsByDistributor((int)dist.Id);
                    RadGridSLMBookings.Rebind();
                }
                else
                {
                    RadGridSLMBookings.DataSource = _satLinkController.GetSLMBookingsByCustomer((int)org.Id);
                    RadGridSLMBookings.Rebind();
                }
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtException = new CmtApplicationException();
                cmtException.ExceptionDateTime = DateTime.Now;
                cmtException.ExceptionDesc = ex.Message;
                cmtException.StateInformation = "CMTId probably not a number";
                boLogControlWS.LogApplicationException(cmtException);
            }
        }
    }
}