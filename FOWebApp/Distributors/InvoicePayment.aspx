﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InvoicePayment.aspx.cs" Inherits="FOWebApp.Distributors.InvoicePayment" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="invoicePaymentHead" runat="server">
    <title>Checkout</title>
    <link rel="shortcut icon" href="~/Images/favicon_new.ico" />
    <link rel="icon" type="image/png" href="~/Images/favicon_shortcut.png" />
    <link href="../CMTStyle.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script type="text/javascript" src="../Scripts/hipay-fullservice-sdk.min.js"></script>
    <script type="text/javascript" src="../Scripts/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="../Scripts/AppConfig.js"></script>
    <style type="text/css">
        .auto-style2 {
            width: 102%;
        }

        .red {
            color: red;
        }

        .blue {
            color: blue;
        }

        .green {
            color: green;
        }
    </style>
    <script type="text/javascript">
        Stripe.setPublishableKey(config.stripePublishableKey);

        function getToken() {
            $("#submitPaymentInvoice").prop('disabled', true);
            $("span[id$='labelPaymentResultInvoice']").addClass("blue");
            $("span[id$='labelPaymentResultInvoice']").text("Processing... Please wait");
            var cardNumber = $('#txtCardNumberInvoice').val();
            var cvc = $('#txtCVCInvoice').val();
            var month = $('#txtMonthInvoice').val();
            var year = $('#txtYearInvoice').val();
            var accept = $('#acceptCheckbox').is(':checked');

            if (!accept) {
                reportError("You must accept the terms and conditions of sale");
                return;
            }

            if (!Stripe.card.validateCardNumber(cardNumber)) {
                reportError("Invalid card number");
                return;
            }

            if (!Stripe.card.validateCVC(cvc)) {
                reportError("Invalid CVC number");
                return;
            }

            if (!Stripe.card.validateExpiry(month, year)) {
                reportError("Invalid expiration date");
                return;
            }

            Stripe.card.createToken({
                number: cardNumber,
                cvc: cvc,
                exp_month: month,
                exp_year: year
            }, stripeResponseHandler);
        }

        function stripeResponseHandler(status, response) {
            if (response.error) {
                // Show the errors on the form
                reportError(response.error.message);
                $("#submitPaymentInvoice").prop('disabled', false);
            } else {
                // response contains id and card, which contains additional card details
                var token = response.id;
                // Insert the token into the form so it gets submitted to the server
                $('#inputStripeTokenInvoice').val(token);
                // and submit
                document.forms[0].submit();
            }
        }

        function reportError(error) {
            error.fontcolor("red");
            $("span[id$='labelPaymentResultInvoice']").addClass("red");
            $("span[id$='labelPaymentResultInvoice']").text(error);
            $("#submitPaymentInvoice").prop('disabled', false);
        }

        function getHiPayToken() {

            $("span[id$='labelPaymentResultInvoice']").addClass("blue");
            $("span[id$='labelPaymentResultInvoice']").text("Processing... Please Wait");
            $("#submitPaymentInvoice").prop('disabled', true);
            var params = {
                card_number: $('#txtCardNumber').val(),
                cvc: $('#txtCVC').val(),
                card_expiry_month: $('#txtMonth').val(),
                card_expiry_year: $('#txtYear').val(),
                //card_holder: $('#txtName').val(),
                multi_use: '0'
            };

            HiPay.setTarget(config.hiPayTarget); // default is production/live
            // These are fake credentials, put your own credentials here (HiPay Enterprise back office > Integration > Security settings and create credentials with public visibility)
            HiPay.setCredentials('11111111.stage-secure-gateway.hipay-tpp.com', config.hiPayPublishableKey);

            HiPay.create(params,
              function (result) {
                  // Insert the token into the form so it gets submitted to the server
                  $('#inputHiPayToken').val(result.token);
                  // and submit
                  document.forms[0].submit();
              },
              function (errors) {
                  // An error occurred
                  $("span[id$='labelPaymentResultInvoice']").addClass("red");
                  $("#submitPaymentInvoice").prop('disabled', false);
                  if (typeof errors.message != "undefined") {
                      $("span[id$='labelPaymentResultInvoice']").text(errors.message);
                  } else {
                      $("span[id$='labelPaymentResultInvoice']").text("An error occurred with the request.");
                  }
              }
            );

        }
</script>
</head>
<body>
    <form id="formInvoicePayment" runat="server">
        <div style="width: 400px; margin: 0 auto;">
            <table class="checkoutStart" cellpadding="10" cellspacing="5">
                <tr>
                    <td colspan="2">
                        <h2>Use Credit Card</h2>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="txtCardNumberInvoice">Card number</label>
                    </td>
                    <td>
                        <input type="text" id="txtCardNumberInvoice" placeholder="e.g. 4242 4242 4242 4242" autocomplete="off" size="24"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="txtCVC">CVC</label>
                    </td>
                    <td>
                        <input type="text" id="txtCVCInvoice" placeholder="e.g. 545" autocomplete="off" maxlength="4" size="8" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="txtExpirationDate">Expiration date</label>
                    </td>
                    <td>
                        <input type="text" id="txtMonthInvoice" placeholder="MM" autocomplete="off" maxlength="2" size="2" />
                        <label>/</label>
                        <input type="text" id="txtYearInvoice" placeholder="YY" autocomplete="off" maxlength="2" size="2" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="amountToPayStripeInvoice" runat="server" Text="Amount to pay"></asp:Label>
                    </td>
                    <td class="style1">
                        <asp:Label ID="labelAmountToPayStripeInvoice" runat="server" Font-Bold="True"></asp:Label>
                    </td>
                    <%--<td class="style1">
                        <asp:Label ID="labelAmountToPayHipay" runat="server" Font-Bold="True"></asp:Label>
                    </td>--%>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:CheckBox ID="acceptCheckbox" runat="server" Text="I have read and accept the <a target='_blank' href='http://www.satadsl.net/general-terms-conditions-sale/'>terms and conditions of sale</a>" ClientIDMode="Static"/>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Label ID="labelPaymentResultInvoice" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
            &nbsp;&nbsp;&nbsp;<input id="submitPaymentInvoice" type="button" value="Submit Payment" onclick="getToken(); return false;" />
            <input type="hidden" name="inputStripeToken" id="inputStripeTokenInvoice" />
            <input type="hidden" name="inputHiPayToken" id="inputHiPayTokenInvoice" />
        </div>
    </form>
</body>
</html>
