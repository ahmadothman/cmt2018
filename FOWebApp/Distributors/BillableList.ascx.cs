﻿using FOWebApp.net.nimera.cmt.BOInvoicingControllerWSRef;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace FOWebApp.Distributors
{
    public partial class BillableList : System.Web.UI.UserControl
    {
        private BOInvoicingControllerWS _invoiceController = new BOInvoicingControllerWS();
        private BOAccountingControlWS _accountingController = new BOAccountingControlWS();

        protected Distributor _distributor = new Distributor();

        protected void Page_Init(object sender, EventArgs e)
        {
            _distributor = _accountingController.GetDistributorForUser(Membership.GetUser().ProviderUserKey.ToString());
        }

        protected void RadGridBillableList_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            ((RadGrid)sender).MasterTableView.DataSource = _invoiceController.GetBillablesForDistributor((int)_distributor.Id);
        }

        protected void RadGridBillableList_Load(object sender, EventArgs e)
        {
            RadGrid grid = (RadGrid)sender;

            switch (_distributor.Currency)
            {
                case "EUR":
                    grid.Columns.FindByUniqueName("PriceEu").Visible = true;

                    break;
                case "USD":
                    grid.Columns.FindByUniqueName("PriceUsd").Visible = true;

                    break;
                default:
                    grid.Columns.FindByUniqueName("PriceEu").Visible = true;

                    break;
            }
        }
    }
}