﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;

namespace FOWebApp.Distributors
{
    public partial class CheckoutCancel : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Label1.ForeColor = Color.Red;
            Label1.Text = "Payment procedure cancelled by user";
        }
    }
}