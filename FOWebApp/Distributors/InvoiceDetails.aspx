﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InvoiceDetails.aspx.cs" Inherits="FOWebApp.Distributors.InvoiceDetails" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html lang="en">
<head>
    <title>Invoice Details</title>
    <link href="<%= this.ResolveUrl("~/CMTStyle.css") %>" rel="stylesheet" />

    <style>
        div.RadToolBar .rtbUL {
            width: 100%;
            text-align: center;
        }

        div.RadToolBar .item {
            margin: 0 20px;
        }

        .italic {
            font-style: italic;
        }

        .bold {
            font-weight: bold;
        }

        .pull-left {
            float: left;
        }

        .pull-right {
            float: right;
        }

        .clearfix {
            clear: both;
        }
    </style>
</head>
<body>
    <form id="formInvoiceDetails" runat="server">
        <telerik:RadScriptManager ID="RadScriptManagerInvoiceDetails" runat="server"></telerik:RadScriptManager>

        <telerik:RadScriptBlock ID="RadScriptBlockInvoiceDetails" runat="server">
            <script>
                function pdfCreationAlertCallback(arg) {
                    // Maybe do something here when user clicked 'OK'
                }
            </script>
        </telerik:RadScriptBlock>

        <telerik:RadToolBar ID="RadToolBarActions" runat="server" Width="100%" OnButtonClick="RadToolBarActions_ButtonClick" Skin="Metro">
            <Items>
                <telerik:RadToolBarButton Text="Save as PDF" ToolTip="Save the contents of this form as PDF on your hard disk" ImageUrl="~/Images/PDF.jpg"
                    OuterCssClass="item" Value="PdfButton">
                </telerik:RadToolBarButton>
            </Items>
        </telerik:RadToolBar>

        <table class="table-spaced pull-left">
            <tr>
                <td class="italic">Invoice Number
                </td>
                <td colspan="3">
                    <telerik:RadCodeBlock ID="RadCodeBlockInvoiceId" runat="server"><%= _invoice.InvoiceId %></telerik:RadCodeBlock>
                </td>
            </tr>
            <tr>
                <td class="italic">Invoice Month
                </td>
                <td>
                    <asp:Literal ID="LiteralInvoiceMonth" runat="server"></asp:Literal>
                </td>
                <td class="italic">Creation Date
                </td>
                <td>
                    <telerik:RadCodeBlock ID="RadCodeBlockCreationDate" runat="server"><%= _invoice.CreationDate.ToString("dd/MM/yyyy") %></telerik:RadCodeBlock>
                </td>
            </tr>
            <tr>
                <td class="italic">Distributor Name
                </td>
                <td colspan="3">
                    <telerik:RadCodeBlock ID="RadCodeBlockDistributorId" runat="server"><%= _invoice.DistributorName %></telerik:RadCodeBlock>
                </td>
            </tr>
            <tr>
                <td class="italic">Number of Active Terminals</td>
                <td>
                    <telerik:RadCodeBlock ID="RadCodeBlockActiveTerminals" runat="server"><%= _invoice.ActiveTerminals ?? 0 %></telerik:RadCodeBlock>
                </td>
                <td class="italic">State
                </td>
                <td>
                    <asp:Literal ID="LiteralInvoiceState" runat="server"></asp:Literal>
                </td>
            </tr>
        </table>

        <table class="table-spaced pull-right">
            <%--<tr class="bold">
                <td class="italic">TOTAL PRICE</td>
                <td>
                    <telerik:RadCodeBlock ID="RadCodeBlockTotalAmount" runat="server">
                        <%= _invoice.CurrencyCode %>
                        <%= _invoice.TotalAmount.ToString("0.00") %>
                    </telerik:RadCodeBlock>
                </td>
            </tr>
            <tr>
                <td class="italic">Incentive</td>
                <td>
                    <telerik:RadCodeBlock ID="RadCodeBlockIncentive" runat="server"><%= _invoice.Incentive.ToString("P1") %></telerik:RadCodeBlock>
                </td>
            </tr>
            <tr>
                <td class="italic">VAT</td>
                <td>
                    <telerik:RadCodeBlock ID="RadCodeBlockVat" runat="server"><%= _invoice.VAT.HasValue ? _invoice.VAT.Value.ToString("P0") : "0%" %></telerik:RadCodeBlock>
                </td>
            </tr>
            <tr class="bold">
                <td class="italic">GRAND TOTAL</td>
                <td>
                    <telerik:RadCodeBlock ID="RadCodeBlockGrandTotal" runat="server">
                        <%= _invoice.CurrencyCode %>
                        <%= _invoice.GrandTotal.ToString("0.00") %>
                    </telerik:RadCodeBlock>
                </td>
            </tr>
            <tr>
                <td class="italic">Financial Interest</td>
                <td>
                    <telerik:RadCodeBlock ID="RadCodeBlockFinancialInterest" runat="server">
                        <%= _invoice.CurrencyCode %>
                        <%= _invoice.FinancialInterest.HasValue ? _invoice.FinancialInterest.Value.ToString("0.00") : "0.00" %>
                    </telerik:RadCodeBlock>
                </td>
            </tr>
            <tr class="bold">
                <td class="italic">TO BE PAID</td>
                <td>
                    <telerik:RadCodeBlock ID="RadCodeBlockToBePaid" runat="server">
                        <%= _invoice.CurrencyCode %>
                        <%= _invoice.ToBePaid.ToString("0.00") %>
                    </telerik:RadCodeBlock>
                </td>
            </tr>--%>
            <tr>
                <td class="italic">To Be Paid</td>
                <td>
                    <telerik:RadCodeBlock ID="RadCodeBlockToBePaid" runat="server">
                        <%= _invoice.CurrencyCode %>
                        <%= _invoice.ToBePaid.ToString("0.00") %>
                    </telerik:RadCodeBlock>
                </td>
            </tr>
        </table>

        <div class="clearfix"></div>

        <hr />

        <telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxyInvoiceDetailsList" runat="server">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="RadGridInvoiceDetails">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="RadGridInvoiceDetails" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManagerProxy>

        <%-- Using the loading panel in Default.aspx --%>
        <%--<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanelInvoiceDetails" runat="server"></telerik:RadAjaxLoadingPanel>--%>

        <telerik:RadGrid ID="RadGridInvoiceDetails" runat="server" OnNeedDataSource="RadGridInvoiceDetails_NeedDataSource"
            AutoGenerateColumns="false" OnItemDataBound="RadGridInvoiceDetails_ItemDataBound" AllowPaging="true" Skin="Metro">
            <ClientSettings EnableAlternatingItems="true" EnableRowHoverStyle="true">
                <Resizing AllowColumnResize="true" EnableRealTimeResize="true" ClipCellContentOnResize="false" ResizeGridOnColumnResize="false" />
            </ClientSettings>
            <MasterTableView Name="Invoices" DataKeyNames="Id" ClientDataKeyNames="Id" CommandItemDisplay="Top" PageSize="10" AllowMultiColumnSorting="true">
                <Columns>
                    <telerik:GridTemplateColumn UniqueName="TerminalId" HeaderText="Customer Reference Name">
                        <HeaderStyle Width="186" />
                        <ItemTemplate>
                            <asp:Label ID="LabelTerminalId" runat="server" Text='<%# Eval("TerminalId") %>' />
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn UniqueName="ServicePack" HeaderText="Service Profile" DataField="ServicePack">
                        <HeaderStyle Width="97" />
                        <ItemTemplate>
                            <asp:Label ID="LabelServicePack" runat="server" Text='<%# Eval("ServicePack") %>' />
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn UniqueName="Serial"
                        HeaderText="Serial Number" DataField="Serial">
                        <HeaderStyle Width="90" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn UniqueName="MacAddress"
                        HeaderText="MAC Address" DataField="MacAddress">
                        <HeaderStyle Width="99" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn UniqueName="BillableDescription"
                        HeaderText="Action" DataField="BillableDescription">
                        <HeaderStyle Width="120" />
                    </telerik:GridBoundColumn>
                    <telerik:GridDateTimeColumn UniqueName="ActivityDate" DataField="ActivityDate"
                        HeaderText="Activity Date" DataFormatString="{0:dd/MM/yyyy HH:mm}">
                        <HeaderStyle Width="100" />
                    </telerik:GridDateTimeColumn>
                    <telerik:GridNumericColumn UniqueName="UnitPrice"
                        HeaderText="Price" DataField="UnitPrice">
                        <%--DataFormatString="{0:0.00}"--%>
                        <HeaderStyle Width="64" />
                    </telerik:GridNumericColumn>
                    <telerik:GridNumericColumn UniqueName="Items"
                        HeaderText="#" DataField="Items">
                        <HeaderStyle Width="40" />
                    </telerik:GridNumericColumn>
                    <telerik:GridCalculatedColumn UniqueName="TotalPrice"
                        HeaderText="Total" DataFields="UnitPrice, Items" Expression="{0} * {1}">
                        <%--DataFormatString="{0:0.00}"--%>
                        <HeaderStyle Width="47" />
                    </telerik:GridCalculatedColumn>
                </Columns>
                <PagerStyle Mode="Slider" Position="Bottom" />
                <SortExpressions>
                    <telerik:GridSortExpression FieldName="TerminalId" SortOrder="Ascending" />
                    <telerik:GridSortExpression FieldName="ActivityDate" SortOrder="Ascending" />
                </SortExpressions>
                <CommandItemSettings ShowAddNewRecordButton="false" ShowExportToCsvButton="true" />
            </MasterTableView>
            <ExportSettings ExportOnlyData="false" IgnorePaging="true" OpenInNewWindow="true">
            </ExportSettings>
        </telerik:RadGrid>

        <hr />

       <%-- <table class="table-spaced">
            <tr>
                <td>Remarks
                </td>
                <td>
                    <telerik:RadTextBox ID="RadTextBoxRemarks" runat="server" Columns="100" Rows="4" TextMode="MultiLine"
                        Resize="Vertical" Enabled="false" Visible="false" >
                    </telerik:RadTextBox>
                </td>
            </tr>
        </table>--%>

        <telerik:RadWindowManager ID="RadWindowManagerInvoiceDetails" runat="server" Modal="true" Animation="None"
            DestroyOnClose="true" KeepInScreenBounds="true" EnableShadow="true" VisibleStatusbar="true" Width="850"
            Height="655" ShowContentDuringLoad="false" Skin="Metro">
        </telerik:RadWindowManager>
    </form>
</body>
</html>
