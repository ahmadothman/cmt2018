﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using Telerik.Web.UI;
using FOWebApp.net.nimera.cmt.BOMonitorControlWSRef;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;
using FOWebApp.net.nimera.cmt.BOIssuesControllerWSRef;

namespace FOWebApp.Distributors
{
    public partial class IssueDetailsForm : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public void componentDataInit(string jiraIssueKey)
        {
            // get the issue details form Jira
            jiraIssue = boIssuesControl.GetJiraIssueDetails(jiraIssueKey);

            LabelIssueKey.Text = jiraIssue.key;
            LabelSummary.Text = jiraIssue.fields.summary;
            LabelStatus.Text = jiraIssue.fields.status.name;
            LabelPriority.Text = jiraIssue.fields.priority.name;
            TextBoxIssueDescription.Text = jiraIssue.fields.description;
            LabelDateReported.Text = jiraIssue.fields.created.Date.ToShortDateString();
            LabelDateUpdated.Text = jiraIssue.fields.updated.Date.ToShortDateString();

            // hide conditional labels
            LabelComments.Visible = false;
            LabelResolution.Visible = false;

            // check for comments
            if (jiraIssue.fields.comment.comments.Count() > 0)
            {
                LabelComments.Visible = true;

                List<commentObject> commentList = new List<commentObject>();
                commentObject commentDetails = new commentObject();
                
                foreach (JiraIssueComments co in jiraIssue.fields.comment.comments)
                {
                    commentDetails = new commentObject();
                    commentDetails.Date = co.updated.ToShortDateString();
                    commentDetails.Author = co.updateAuthor.displayName;
                    commentDetails.Comment = co.body;
                    commentList.Add(commentDetails);
                }

                GridViewComments.DataSource = commentList;
                //GridViewComments.DataSource = jiraIssue.fields.comment.comment;
                GridViewComments.DataBind();
            }

            // check if issue is resolved
            if (jiraIssue.fields.status.name == "Resolved")
            {
                LabelResolution.Visible = true;
                LabelResolutionText.Text = jiraIssue.fields.resolution.name;
            }
        }

        protected void ButtonSubmitComment_Click(object sender, EventArgs e)
        {
            string comment = TextBoxNewComment.Text.ToString();
            if (comment != "")
            {
                if (boIssuesControl.SubmitComment(comment, jiraIssue.key))
                {
                    LabelCommentSubmission.Text = "Comment successfully submitted";
                    LabelCommentSubmission.ForeColor = Color.Green;
                }
                else
                {
                    LabelCommentSubmission.Text = "Submitting the comment failed";
                    LabelCommentSubmission.ForeColor = Color.Red;
                }
            }
        }

        public class commentObject
        {
            public string Date { get; set; }
            public string Author  { get; set; }
            public string Comment  { get; set; }
        }

        public BOIssuesControllerWS boIssuesControl = new BOIssuesControllerWS();
        public JiraIssue jiraIssue = new JiraIssue();
    }
}