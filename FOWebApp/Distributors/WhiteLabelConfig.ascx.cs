﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.IO;
using System.Drawing;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;
using net.nimera.supportlibrary;

namespace FOWebApp.Distributors
{
	public partial class WhiteLabelConfig : System.Web.UI.UserControl
	{
		protected void Page_Load(object sender, EventArgs e)
		{
            Theme theme = null;
            String contentPath = "Content/Distributors/";
            String basePath = HttpRuntime.AppDomainAppPath + contentPath;

            //Retrieve the distributor id
            MembershipUser myObject = Membership.GetUser();
            string userId = myObject.ProviderUserKey.ToString();

            //Get the Distributor linked to the user
            BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();
            Distributor dist = boAccountingControl.GetDistributorForUser(userId);
            LabelKey.Text = dist.Id.ToString();

            try
            {
                //Set the download locations for documents and images for this distributor
                //We are not using this feature yet!!!!!
                /*FileInfo fi = new FileInfo(basePath + dist.Id + "/Images/");
                if (!fi.Exists)
                {
                    fi.Directory.Create(); //Create the location under Content
                }

                fi = new FileInfo(basePath + dist.Id + "/Documents/"); //For documents

                if (!fi.Exists)
                {
                    fi.Directory.Create();
                }

                String[] imgPaths = {"~/" + contentPath + dist.Id + "/Images/"};
                String[] docPaths = {"~/" + contentPath + dist.Id + "/Documents/"};
                RadEditorIntroText.ImageManager.ViewPaths = imgPaths;
                RadEditorIntroText.ImageManager.UploadPaths = imgPaths;
                RadEditorIntroText.DocumentManager.ViewPaths = docPaths;
                RadEditorIntroText.DocumentManager.UploadPaths = docPaths;*/

                if (dist != null)
                {
                    //Get the theme for the distributor
                    theme = boAccountingControl.GetThemeForDistributor((int)dist.Id);

                    if (theme != null)
                    {
                        ColorConverter colConv = new ColorConverter();
                        RadComboBoxTheme.SelectedValue = theme.ThemeName;
                        RadEditorIntroText.Content = theme.IntroText;
                        RadColorPickerBackground.SelectedColor =  Color.FromArgb(HexDecConvertor.ConvertToInt(theme.BackgroundColor.Trim()));
                    }
                    else
                    {
                        RadComboBoxTheme.SelectedIndex = 0;
                        RadEditorIntroText.Content = "";
                    }
                }
            }
            catch (Exception ex)
            {
                BOLogControlWS logger = new BOLogControlWS();
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDateTime = DateTime.Now;
                cmtEx.ExceptionDesc = ex.Message;
                cmtEx.ExceptionLevel = 4; //Critical
                cmtEx.ExceptionStacktrace = ex.StackTrace;
                cmtEx.StateInformation = "Creation of image and document folders failed";
                cmtEx.UserDescription = "White Label configuration by distributor failed";
                logger.LogApplicationException(cmtEx);
            }

		}

        protected void RadButtonSubmit_Click(object sender, EventArgs e)
        {
            Theme theme = new Theme();
            theme.DistId = Int32.Parse(LabelKey.Text);
            theme.ThemeName = RadComboBoxTheme.SelectedValue;
            theme.IntroText = RadEditorIntroText.Content;
            theme.BackgroundColor = HexDecConvertor.ConvertToHex(RadColorPickerBackground.SelectedColor.ToArgb());

            BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();
            if (boAccountingControl.UpdateTheme(theme))
            {
                LabelResult.ForeColor = Color.DarkGreen;
                LabelResult.Text = "Theme updated";
            }
            else
            {
                LabelResult.ForeColor = Color.DarkRed;
                LabelResult.Text = "Theme update failed";
            }
        }
	}
}