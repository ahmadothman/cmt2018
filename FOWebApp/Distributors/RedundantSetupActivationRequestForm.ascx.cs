﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Web.Script.Serialization;
using System.Drawing;
using Telerik.Web.UI;
using FOWebApp.Data;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;
using FOWebApp.App_Code;

namespace FOWebApp.Distributors
{
    public partial class RedundantSetupActivationRequestForm : System.Web.UI.UserControl
    {
        const string _HEXBYTE = "<0|1|2|3|4|5|6|7|8|9|a|b|c|d|e|f><0|1|2|3|4|5|6|7|8|9|a|b|c|d|e|f>";
        const int _Isp = 112;

        protected void Page_Load(object sender, EventArgs e)
        {
            //Initialize the form
            RadMaskedTextBoxMacAddress.Mask =
                _HEXBYTE + "<:>" + _HEXBYTE + "<:>" + _HEXBYTE + "<:>" + _HEXBYTE + "<:>" + _HEXBYTE + "<:>" +
                _HEXBYTE;
            RadMaskedTextBoxMacAddress2.Mask =
                _HEXBYTE + "<:>" + _HEXBYTE + "<:>" + _HEXBYTE + "<:>" + _HEXBYTE + "<:>" + _HEXBYTE + "<:>" +
                _HEXBYTE;

            DataHelper dataHelper = new DataHelper();

            //Get the Distributor information an fill up the form
            //Load data into the grid view. This view lists all terminals for the 
            //authenticated user.
            BOAccountingControlWS boAccountingControl =
              new net.nimera.cmt.BOAccountingControlWSRef.BOAccountingControlWS();

            //Get the authenticated user
            MembershipUser myObject = Membership.GetUser();
            string userId = myObject.ProviderUserKey.ToString();


            //Get the Distributor linked to the user
            Distributor dist = boAccountingControl.GetDistributorForUser(userId);

            LabelDistributor.Text = dist.FullName;

            if (dist.FullName.Length > 10)
            {
                LabelDistribName.Text = dist.FullName.Substring(0, 10);
                LabelDistribName2.Text = dist.FullName.Substring(0, 10);
            }
            else
            {
                LabelDistribName.Text = dist.FullName.Trim();
                LabelDistribName2.Text = dist.FullName.Trim();
            }

            //Fill in the list of customers
            Organization[] organizations = boAccountingControl.GetOrganisationsByDistributor(userId);

            RadComboBoxItem item = null;

            foreach (Organization org in organizations)
            {
                item = new RadComboBoxItem();
                item.Value = org.Id.ToString();
                item.Text = org.FullName;
                RadComboBoxCustomer.Items.Add(item);
            }

            //Fill-up the treeview in the RadComboBoxex
            Isp[] isps = boAccountingControl.GetISPsForDistributor((int)dist.Id);
            RadTreeView slaTree = (RadTreeView)RadComboBoxSLA.Items[0].FindControl("RadTreeViewSLA");
            foreach (Isp isp in isps)
            {
                RadTreeNode rtn = new RadTreeNode(isp.CompanyName, isp.Id.ToString());
                rtn.ImageUrl = "~/Images/contract.png";
                rtn.ToolTip = isp.Id.ToString();

                //Add child nodes to the tree node
                ServiceLevel[] serviceLevels = boAccountingControl.GetServicePacksByIsp((int)isp.Id);

                RadTreeNode slNode = null;
                foreach (ServiceLevel sl in serviceLevels)
                {
                    slNode = new RadTreeNode(sl.SlaName, sl.SlaId.ToString());
                    slNode.ToolTip = sl.SlaCommonName;
                    rtn.Nodes.Add(slNode);
                }
                slaTree.Nodes.Add(rtn);
            }
            RadTreeView slaTree2 = (RadTreeView)RadComboBoxSLA2.Items[0].FindControl("RadTreeViewSLA2");
            foreach (Isp isp in isps)
            {
                RadTreeNode rtn = new RadTreeNode(isp.CompanyName, isp.Id.ToString());
                rtn.ImageUrl = "~/Images/contract.png";
                rtn.ToolTip = isp.Id.ToString();

                //Add child nodes to the tree node
                ServiceLevel[] serviceLevels = boAccountingControl.GetServicePacksByIsp((int)isp.Id);

                RadTreeNode slNode = null;
                foreach (ServiceLevel sl in serviceLevels)
                {
                    slNode = new RadTreeNode(sl.SlaName, sl.SlaId.ToString());
                    slNode.ToolTip = sl.SlaCommonName;
                    rtn.Nodes.Add(slNode);
                }
                slaTree2.Nodes.Add(rtn);
            }
            //int i = 0;
        }

        protected void ButtonSave_Click(object sender, EventArgs e)
        {
            //Check acceptance checkboxes first
            if (CheckBoxDistributorAcceptance.Checked && CheckBoxCustomerAcceptance.Checked)
            {
                BOAccountingControlWS boAccountingControl =
                                new net.nimera.cmt.BOAccountingControlWSRef.BOAccountingControlWS();

                //The primary terminal
                Terminal termP = null;
                termP = boAccountingControl.GetTerminalDetailsByMAC(RadMaskedTextBoxMacAddress.Text);
                //The secondary terminal
                Terminal termS = null;
                termS = boAccountingControl.GetTerminalDetailsByMAC(RadMaskedTextBoxMacAddress2.Text);

                //First check if the MAC addresses are unique, otherwise set an error message and return
                if (termP != null || termS != null)
                {
                    LabelResult.ForeColor = Color.Red;
                    LabelResult.Text = "One of the MAC addresses already exist in the database";
                    return;
                }

                BOLogControlWS boLogControlWS = new BOLogControlWS();

                try
                {
                    //Get the authenticated user
                    MembershipUser myObject = Membership.GetUser();
                    string userId = myObject.ProviderUserKey.ToString();

                    //Get the Distributor linked to the user
                    Distributor dist = boAccountingControl.GetDistributorForUser(userId);

                    //Get the Organization details specified for this terminal
                    Organization org = boAccountingControl.GetOrganization(Int32.Parse(RadComboBoxCustomer.SelectedValue));

                    termP = new Terminal();
                    termP.Address = new Address();

                    //CMTSUPPORT-244
                    termP.MacAddress = RadMaskedTextBoxMacAddress.Text;
                    termP.Serial = TextBoxSerial.Text;

                    //Fill the term entity
                    termP.Address.AddressLine1 = TextBoxAddressLine1.Text;
                    termP.Address.AddressLine2 = TextBoxAddressLine2.Text;
                    termP.Address.Country = CountryList.SelectedValue;
                    termP.Address.Location = TextBoxTerminalLocation.Text;
                    termP.Address.PostalCode = TextBoxPCO.Text;
                    termP.AddressType = 0; //Public IP
                    termP.AdmStatus = 5; //Request
                    termP.Blade = 3;
                    termP.DistributorId = dist.Id;
                    termP.DistributorName = dist.FullName;
                    termP.Email = TextBoxEMailAddress.Text;
                    termP.ExpiryDate = RadDatePickerExpiryDate.SelectedDate;
                    termP.FirstActivationDate = RadDatePickerActivationDate.SelectedDate;
                    termP.FullName = LabelDistribName.Text + "-" + TextBoxTerminalName.Text + "-" + TextBoxSerial.Text;
                    termP.ExtFullName = string.Concat(termP.MacAddress + termP.Serial).Replace(":", "-"); ;
                    termP.CNo = (decimal)-1.0;
                    termP.IPMask = 32; //Not selectable for the moment

                    HttpRequestWrapper requestWrapper = new HttpRequestWrapper(Request);
                    //int selectedSlaId = Int32.Parse(ExtractComboBoxValue(requestWrapper, "RadComboBoxSLA"));
                    int selectedSlaId = Int32.Parse(HiddenFieldSLAId.Value);
                    ServiceLevel sl = boAccountingControl.GetServicePack(selectedSlaId);
                    termP.IspId = (int)sl.IspId;

                    termP.OrgId = org.Id;
                    termP.Phone = TextBoxPhone.Text;

                    //Add all the values we do not have regular fields for.
                    termP.Remarks =
                        "Contact First Name: " + TextBoxFirstName.Text + "\n\r" +
                        "Contact Surname: " + TextBoxSurname.Text + "\n\r" +
                        "Contact fixed phone: " + TextBoxPhone.Text + "\n\r" +
                        "Contact Mobile phone: " + TextBoxMobile.Text + "\n\r" +
                        "Installation Engineer: " + TextBoxInstEngineer.Text + "\n\r" +
                        "Requested installation Date: " + RadDatePickerInstallationDate.SelectedDate;

                    termP.SitId = 999999;
                    termP.SlaId = sl.SlaId;
                    termP.SlaName = sl.SlaName;
                    termP.StartDate = RadDatePickerStartDate.SelectedDate;
                    termP.TestMode = false;
                    termP.IpAddress = "127.0.0.1"; //Localhost used for requests
                    //Satellite diversity properties
                    termP.RedundantSetup = true;
                    termP.PrimaryTerminal = true;
                    termP.AssociatedMacAddress = RadMaskedTextBoxMacAddress2.Text;

                    //The secondary terminal
                    termS = new Terminal();
                    termS.Address = new Address();

                    //CMTSUPPORT-244
                    termS.MacAddress = RadMaskedTextBoxMacAddress2.Text;
                    termS.Serial = TextBoxSerial2.Text;

                    //Fill the term entity
                    termS.Address.AddressLine1 = TextBoxAddressLine1.Text;
                    termS.Address.AddressLine2 = TextBoxAddressLine2.Text;
                    termS.Address.Country = CountryList.SelectedValue;
                    termS.Address.Location = TextBoxTerminalLocation.Text;
                    termS.Address.PostalCode = TextBoxPCO.Text;
                    termS.AddressType = 0; //Public IP
                    termS.AdmStatus = 5; //Request
                    termS.Blade = 3;
                    termS.DistributorId = dist.Id;
                    termS.DistributorName = dist.FullName;
                    termS.Email = TextBoxEMailAddress.Text;
                    termS.ExpiryDate = RadDatePickerExpiryDate.SelectedDate;
                    termS.FirstActivationDate = RadDatePickerActivationDate.SelectedDate;
                    termS.FullName = LabelDistribName2.Text + "-" + TextBoxTerminalName2.Text + "-" + TextBoxSerial2.Text;
                    termS.ExtFullName = string.Concat(termS.MacAddress + termS.Serial).Replace(":", "-");
                    termS.CNo = (decimal)-1.0;
                    termS.IPMask = 32; //Not selectable for the moment

                    selectedSlaId = Int32.Parse(HiddenFieldSLAId2.Value);
                    sl = boAccountingControl.GetServicePack(selectedSlaId);
                    termS.IspId = (int)sl.IspId;

                    termS.OrgId = org.Id;
                    termS.Phone = TextBoxPhone.Text;

                    //Add all the values we do not have regular fields for.
                    termS.Remarks =
                        "Contact First Name: " + TextBoxFirstName.Text + "\n\r" +
                        "Contact Surname: " + TextBoxSurname.Text + "\n\r" +
                        "Contact fixed phone: " + TextBoxPhone.Text + "\n\r" +
                        "Contact Mobile phone: " + TextBoxMobile.Text + "\n\r" +
                        "Installation Engineer: " + TextBoxInstEngineer.Text + "\n\r" +
                        "Requested installation Date: " + RadDatePickerInstallationDate.SelectedDate;

                    termS.SitId = 999999;
                    termS.SlaId = sl.SlaId;
                    termS.SlaName = sl.SlaName;
                    termS.StartDate = RadDatePickerStartDate.SelectedDate;
                    termS.TestMode = false;
                    termS.IpAddress = "127.0.0.1"; //Localhost used for requests
                    //Satellite diversity properties
                    termP.RedundantSetup = true;
                    termP.PrimaryTerminal = false;
                    termP.AssociatedMacAddress = RadMaskedTextBoxMacAddress.Text;

                    //Write to the database
                    if (boAccountingControl.UpdateTerminal(termP))
                    {
                        if (boAccountingControl.UpdateTerminal(termS))
                        {
                            String eMailMessage = this.prepareEMailText(termP, termS, dist);

                            //Send out the E-Mails to the NOCSA
                            //First get a list all E-Mail addresses which belong to the NOCSA role
                            string[] nocsaRoleNames = Roles.GetUsersInRole("NOC Administrator");

                            List<string> mailAddresses = new List<string>();
                            mailAddresses.Add(FOWebApp.Properties.Settings.Default.SupportEmail);

                            if (boLogControlWS.SendMail(eMailMessage, "Redundant Setup Activation Request", mailAddresses.ToArray()))
                            {
                                //Flag a successful request
                                LabelResult.ForeColor = Color.Green;
                                LabelResult.Text = "Request entered successfully. You will get an acknowledgement E-Mail when the terminals are activated";
                            }
                            else
                            {
                                LabelResult.ForeColor = Color.Red;
                                LabelResult.Text = "Your request is entered successfully but I could not inform NOCSA by E-Mail! Please contact SatADSL.";
                            }
                        }
                        else
                        {
                            LabelResult.ForeColor = Color.Red;
                            LabelResult.Text = "Sorry but the registration failed. Please contact SatADSL for more information.";
                        }
                    }
                    else
                    {
                        LabelResult.ForeColor = Color.Red;
                        LabelResult.Text = "Sorry but the registration failed. Please contact SatADSL for more information.";
                    }

                    this.clearForm();
                }
                catch (Exception ex)
                {
                    CmtApplicationException cmtException = new CmtApplicationException();
                    cmtException.ExceptionDateTime = DateTime.Now;
                    cmtException.ExceptionDesc = ex.Message;
                    cmtException.ExceptionStacktrace = ex.StackTrace;
                    cmtException.UserDescription = "TerminalActivationRequestForm - ButtonSave_Click";
                    boLogControlWS.LogApplicationException(cmtException);

                    LabelResult.ForeColor = Color.Red;
                    LabelResult.Text = "Sorry but the registration failed. Please try again or contact SatADSL for more information.";
                }
            }
            else
            {
                if (!CheckBoxDistributorAcceptance.Checked)
                {
                    RadWindowManager1.RadAlert("You (the distributor) must accept the installation before the activation request can be handled by SatADSL!", 400, 200, "Distributor acceptance", null);
                }
                else if (!CheckBoxCustomerAcceptance.Checked)
                {
                    RadWindowManager1.RadAlert("The customer must accept the installation before the activation request can be handled by SatADSL!", 250, 200, "Distributor acceptance", null);
                }
            }
        }

        /// <summary>
        /// Prepares the text which needs to be sent to the NOCSA
        /// </summary>
        /// <param name="termP">The primary terminal</param>
        /// <param name="termS">The secondary terminal</param>
        /// <returns></returns>
        private string prepareEMailText(Terminal termP, Terminal termS, Distributor dist)
        {
            string msgTxt = "Redundant Setup Request from Distributor: " + dist.FullName + "<br/>";
            msgTxt = msgTxt + "Date and time filed: " + DateTime.Now + "<br/>";
            msgTxt = msgTxt + "Primary terminal MAC address: " + termP.MacAddress + "<br/>";
            msgTxt = msgTxt + "Primary terminal Fullname: " + termP.FullName + "<br/>";
            BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();
            ServiceLevel sl = boAccountingControl.GetServicePack((int)termP.SlaId);
            if (sl != null)
            {
                msgTxt = msgTxt + "Primary terminal SLA: " + termP.SlaId + ", " + sl.SlaCommonName + "<br/>";
            }
            msgTxt = msgTxt + "Secondary terminal MAC address: " + termS.MacAddress + "<br/>";
            msgTxt = msgTxt + "Secondary terminal Fullname: " + termS.FullName + "<br/>";
            sl = boAccountingControl.GetServicePack((int)termS.SlaId);
            if (sl != null)
            {
                msgTxt = msgTxt + "Secondary terminal SLA: " + termS.SlaId + ", " + sl.SlaCommonName + "<br/>";
            }
            msgTxt = msgTxt + "<br/>";

            msgTxt = msgTxt + "The request is now available in the list of terminal activation requests<br/>";

            msgTxt = msgTxt + "CMT automated call do not respond.";

            return msgTxt;
        }

        /// <summary>
        /// Just clears the form
        /// </summary>
        private void clearForm()
        {
            RadMaskedTextBoxMacAddress.Text = "";
            TextBoxAddressLine1.Text = "";
            TextBoxAddressLine2.Text = "";
            TextBoxEMailAddress.Text = "";
            TextBoxTerminalName.Text = "";
            TextBoxFirstName.Text = "";
            TextBoxInstEngineer.Text = "";
            TextBoxMobile.Text = "";
            TextBoxPCO.Text = "";
            TextBoxPhone.Text = "";
            TextBoxSerial.Text = "";
            TextBoxSurname.Text = "";
            TextBoxTerminalLocation.Text = "";
            CountryList.SelectedIndex = 0;
            RadComboBoxCustomer.SelectedIndex = 0;
            RadDatePickerActivationDate.Clear();
            RadDatePickerExpiryDate.Clear();
            RadDatePickerInstallationDate.Clear();
            RadDatePickerStartDate.Clear();
            CheckBoxCustomerAcceptance.Checked = false;
            CheckBoxDistributorAcceptance.Checked = false;
            RadComboBoxSLA.Text = String.Empty;
            RadTreeView slaTree = (RadTreeView)RadComboBoxSLA.Items[0].FindControl("RadTreeViewSLA");
            slaTree.UnselectAllNodes();
            slaTree.CollapseAllNodes();
            RadComboBoxSLA.ClearSelection();
            RadComboBoxSLA.Text = string.Empty;
            RadMaskedTextBoxMacAddress2.Text = "";
            TextBoxSerial2.Text = "";
            RadTreeView slaTree2 = (RadTreeView)RadComboBoxSLA2.Items[0].FindControl("RadTreeViewSLA2");
            slaTree2.UnselectAllNodes();
            slaTree2.CollapseAllNodes();
            RadComboBoxSLA2.ClearSelection();
            RadComboBoxSLA2.Text = string.Empty;
        }

        #region RadComboBox helpers
        /// <summary>
        /// Extracts the String value from a RadComboBox control
        /// </summary>
        /// <param name="request"></param>
        /// <param name="controlId"></param>
        /// <returns></returns>
        public static string ExtractStringValue(HttpRequestBase request, string controlId)
        {
            return request.Form.Keys.OfType<string>()
                .Where(postedValue => postedValue.EndsWith(controlId))
                .Select(postedValue => request.Form[postedValue])
                .FirstOrDefault();
        }

        #endregion

        /// <summary>
        /// Checks if the MAC address exists
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadXmlHttpPanelMacAddress_ServiceRequest(object sender, RadXmlHttpPanelEventArgs e)
        {
            BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();
            string macAddress = e.Value;
            Terminal term = boAccountingControl.GetTerminalDetailsByMAC(macAddress);

            if (term != null)
            {
                //Terminal already exists, clear the mac address field and show an error
                //window
                LabelMacExists.ForeColor = Color.Red;
                LabelMacExists.Text = "Sorry but this MAC address already exists!";
            }
            else
            {
                if (macAddress.Equals("00:00:00:00:00:00"))
                {
                    LabelMacExists.ForeColor = Color.Red;
                    LabelMacExists.Text = "You must provide a valid MAC address!";
                }
                else
                {
                    LabelMacExists.ForeColor = Color.Green;
                    LabelMacExists.Text = "MAC address accepted!";
                }
            }
            RadMaskedTextBoxMacAddress.ClientEvents.OnValueChanged = "OnMacAddressChanged";
            RadMaskedTextBoxMacAddress.Text = macAddress;
        }

        /// <summary>
        /// Checks if the MAC address exists
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadXmlHttpPanelMacAddress2_ServiceRequest(object sender, RadXmlHttpPanelEventArgs e)
        {
            BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();
            string macAddress = e.Value;
            Terminal term = boAccountingControl.GetTerminalDetailsByMAC(macAddress);

            if (term != null)
            {
                //Terminal already exists, clear the mac address field and show an error
                //window
                LabelMacExists.ForeColor = Color.Red;
                LabelMacExists.Text = "Sorry but this MAC address already exists!";
            }
            else
            {
                if (macAddress.Equals("00:00:00:00:00:00"))
                {
                    LabelMacExists.ForeColor = Color.Red;
                    LabelMacExists.Text = "You must provide a valid MAC address!";
                }
                else
                {
                    LabelMacExists.ForeColor = Color.Green;
                    LabelMacExists.Text = "MAC address accepted!";
                }
            }
            RadMaskedTextBoxMacAddress.ClientEvents.OnValueChanged = "OnMacAddressChanged";
            RadMaskedTextBoxMacAddress.Text = macAddress;
        }

        protected void RadXmlHttpPanelCustomer_ServiceRequest(object sender, RadXmlHttpPanelEventArgs e)
        {
            BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();
            int slaId = Convert.ToInt32(e.Value);
            ServiceLevel sl = boAccountingControl.GetServicePack(slaId);
            int ispId = Convert.ToInt32(sl.IspId);

            if (ispId == 473) //SoHo ISP
            {
                MembershipUser myObject = Membership.GetUser();
                string userId = myObject.ProviderUserKey.ToString();
                Distributor distributor = boAccountingControl.GetDistributorForUser(userId);
                RadComboBoxCustomer.SelectedValue = distributor.DefaultOrganization.ToString();
                RadComboBoxCustomer.Enabled = false;
            }
            else
            {
                RadComboBoxCustomer.Enabled = true;
            }
        }
    }
}