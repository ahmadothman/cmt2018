﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AvailableVouchers2.ascx.cs" Inherits="FOWebApp.Distributors.AvailableVouchers2" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script type="text/javascript">
    function RowSelected(sender, eventArgs) {
        var grid = sender;
        var masterDataView = grid.get_masterTableView();
        var rowNum = eventArgs.get_itemIndexHierarchical();
        var row = masterDataView.get_dataItems()[rowNum];
        var key = masterDataView.getCellByColumnUniqueName(row, "Key").innerHTML;

        //Initialize and show the issue details window
        var oWnd = radopen("Distributors/IssueDetailsPage.aspx?key=" + key, "RadWindowIssueDetails");
    }

    function ShowPayForm(id) {
        // using regular popup instead of radwindow due to Paypal security issues
        //window.radopen("Distributors/CheckoutStart.aspx?BatchId=" + id, "RadWindowCheckout");
        window.open("Distributors/CheckoutStart.aspx?BatchId=" + id, "WindowPopup", "width=1000px, height=700px, resizable, scrollbars");
        return false;
    }
</script>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="RadGridVouchers">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGridVouchers" />
                <telerik:AjaxUpdatedControl ControlID="RadWindowManager1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Metro"/>
<telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" Height="200px"
    Width="467px" HorizontalAlign="NotSet"
    LoadingPanelID="RadAjaxLoadingPanel1">
    <br>
    &nbsp;Batch Status:&nbsp;
    <telerik:RadDropDownList ID="RadDropDownListVoucherAvailability" runat="server" Width="130px"
        DropDownWidth="130px" DropDownHeight="130px" SelectedText="All" SelectedValue="2" OnSelectedIndexChanged="RadDropDownListVoucherAvailability_SelectedIndexChanged" AutoPostBack="true" Skin="Metro">
        <Items>
            <telerik:DropDownListItem runat="server" Text="All" Value="All" />
            <telerik:DropDownListItem runat="server" Text="Available" Value="Available" Selected="True" />
            <telerik:DropDownListItem runat="server" Text="Expired" Value="Expired" />
        </Items>
    </telerik:RadDropDownList>
    <br>
    <br>
    <telerik:RadGrid ID="RadGridVouchers" runat="server"
        AutoGenerateColumns="False" CellSpacing="0" GridLines="None" Skin="Metro"
        ShowStatusBar="True" Width="992px"
        OnItemDataBound="RadGridVouchers_ItemDataBound"
        OnPageSizeChanged="RadGridVouchers_PageSizeChanged"
        PageSize="50" OnItemCommand="RadGridVouchers_ItemCommand"
        OnItemCreated="RadGridVouchers_ItemCreated">
        <GroupingSettings CaseSensitive="false" />
        <ClientSettings EnableRowHoverStyle="true">
            <Selecting AllowRowSelect="false" />
            <%--<ClientEvents OnRowSelected="RowSelected" />--%>
        </ClientSettings>
        <MasterTableView CommandItemDisplay="Top" CommandItemSettings-ShowExportToExcelButton="False"
            CommandItemSettings-ShowExportToCsvButton="False" RowIndicatorColumn-Visible="True" RowIndicatorColumn-Resizable="True" RowIndicatorColumn-AndCurrentFilterFunction="Contains" CommandItemSettings-ShowAddNewRecordButton="False" ShowFooter="False">
            <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
            <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
            </RowIndicatorColumn>
            <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
            </ExpandCollapseColumn>
            <Columns>
                <telerik:GridCheckBoxColumn DataField="Release" DataType="System.Boolean"
                    FilterControlAltText="Filter Release column"
                    HeaderImageUrl="~\Images\lock_ok.png" HeaderText="Released"
                    HeaderTooltip="Release Voucher Batch" UniqueName="Release">
                </telerik:GridCheckBoxColumn>
                <telerik:GridBoundColumn DataField="Release" HeaderText="Released?" UniqueName="Release2" Visible="false">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="BatchId"
                    FilterControlAltText="Filter BatchId column" HeaderText="Batch Id"
                    UniqueName="BatchId">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="AvailableVouchers"
                    FilterControlAltText="Filter column column" HeaderText="Available Vouchers"
                    UniqueName="AvailableVouchers">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="DateCreated"
                    FilterControlAltText="Filter column1 column" HeaderText="Date Created"
                    UniqueName="DateCreated" DataFormatString="{0:dd/MM/yyyy HH:mm}">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Volume"
                    FilterControlAltText="Filter Volume column" HeaderText="Volume"
                    UniqueName="Volume">
                </telerik:GridBoundColumn>
                <telerik:GridHyperLinkColumn
                    FilterControlAltText="Filter BatchIdLink column" HeaderText="Download"
                    UniqueName="BatchIdLink" DataNavigateUrlFields="BatchId"
                    DataNavigateUrlFormatString="~/Servlets/VoucherDownload.aspx?BatchId={0}"
                    ImageUrl="../Images/download.png" Target="_blank">
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>

                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </telerik:GridHyperLinkColumn>
                <%--<telerik:GridHyperLinkColumn 
            FilterControlAltText="Filter Payment column" HeaderText="Pay" 
            UniqueName="Payment" DataNavigateUrlFields="BatchId" 
            DataNavigateUrlFormatString="~/Distributors/CheckoutStart.aspx?BatchId={0}" 
            Text="PAY">
        </telerik:GridHyperLinkColumn>--%>
                <telerik:GridTemplateColumn UniqueName="Payment2" HeaderText="Pay">
                    <ItemTemplate>
                        <asp:HyperLink ID="PayLink" runat="server" ImageUrl="../Images/credit_cards.png"></asp:HyperLink>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>

                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </telerik:GridTemplateColumn>
            </Columns>
            <EditFormSettings>
                <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                </EditColumn>
            </EditFormSettings>
        </MasterTableView>
        <FilterMenu EnableImageSprites="False">
        </FilterMenu>
        <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default">
        </HeaderContextMenu>
    </telerik:RadGrid>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" Skin="Metro"
        DestroyOnClose="True">
        <Windows>
            <telerik:RadWindow ID="RadWindowCheckout" runat="server"
                NavigateUrl="Distributors/CheckoutStart.aspx" Animation="Fade" Opacity="100" Skin="Metro"
                Title="Voucher payment" AutoSize="False" Width="1000px" Height="700px" KeepInScreenBounds="True" EnableShadow="True" EnableAriaSupport="True" VisibleStatusbar="True" Modal="False" DestroyOnClose="False">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
</telerik:RadAjaxPanel>
