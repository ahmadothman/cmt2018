﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Drawing;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;

namespace FOWebApp.Distributors
{
    public partial class NewCustomerForm : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ButtonSubmit_Click(object sender, EventArgs e)
        {
            if (TextBoxName.Equals("") || TextBoxEMail.Equals(""))
            {
                LabelResult.ForeColor = Color.Red;
                LabelResult.Text = "You must provide a customer name and E-Mail address";
            }
            else
            {
                LabelEmail.Visible = false;
                //Determine the Distributor Id from the logged-on user
                MembershipUser myObject = Membership.GetUser();
                string userId = myObject.ProviderUserKey.ToString();

                BOAccountingControlWS boAccountingControl =
                 new net.nimera.cmt.BOAccountingControlWSRef.BOAccountingControlWS();

                Distributor distributor = boAccountingControl.GetDistributorForUser(userId);

                //Read in the data from the form and store to the database
                Organization org = new Organization();
                org.Address = new Address();

                org.Distributor = distributor;
                org.FullName = TextBoxName.Text;
                org.Address.AddressLine1 = TextBoxAddressLine1.Text;
                org.Address.AddressLine2 = TextBoxAddressLine2.Text;
                org.Address.Country = CountryList.SelectedValue;
                org.Address.Location = TextBoxLocation.Text;
                org.Address.PostalCode = TextBoxLocation.Text;
                org.Email = TextBoxEMail.Text;
                org.Phone = TextBoxPhone.Text;
                org.Remarks = TextBoxRemarks.Text;
                org.Vat = TextBoxVAT.Text;
                if (CheckBoxVNO.Checked)
                {
                    org.VNO = true;
                    // org.MIR = Convert.ToInt32(RadNumericTextBoxMIR.Text);
                    //org.CIR = Convert.ToInt32(RadNumericTextBoxCIR.Text);
                    org.MIR = 0;
                    org.CIR = 0;
                    //check that email address of Customer VNO is on a different domain from the Distributor
                    string[] distDomain = distributor.Email.Split('@');
                    string[] orgDomain = org.Email.Split('@');
                    if (distDomain[1].Trim() == orgDomain[1].Trim())
                    {
                        LabelEmail.Text = "E-mail of Customer VNO may not belong to the Distributor";
                        LabelEmail.ForeColor = Color.Red;
                        LabelEmail.Visible = true;
                        return;
                    }
                }
                else
                {
                    org.VNO = false;
                    org.MIR = 0;
                    org.CIR = 0;
                }
                org.WebSite = TextBoxWebSite.Text;

                if (boAccountingControl.UpdateOrganization(org))
                {
                    LabelResult.ForeColor = Color.Green;
                    LabelResult.Text = "Create new customer successfully!!";
                }
                else
                {
                    LabelResult.ForeColor = Color.Red;
                    LabelResult.Text = "Customer created failed!";
                }
            }
        }
    }
}