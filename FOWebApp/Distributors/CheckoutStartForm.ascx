﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CheckoutStartForm.ascx.cs" Inherits="FOWebApp.Distributors.CheckoutStartForm" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<style type="text/css">
    .auto-style2 {
        width: 102%;
    }

    .red {
        color: red;
    }

    .blue {
        color: blue;
    }

    .green {
        color: green;
    }
</style>

<telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
    <script type="text/javascript">
        function DoBlur(fld) {
            fld.className = 'normalfld';
        }

        function DoFocus(fld) {
            fld.className = 'focusfld';
        }

        function DoChange(fld) {
            fld.className = 'changedfld';
        }
    </script>
</telerik:RadCodeBlock>
<telerik:RadAjaxPanel ID="RadAjaxPanelCheckoutStart" runat="server" GroupingText="Checkout start" Height="200px" Width="700px"
    EnableHistory="True" Style="margin-right: 114px">
    <div style="width: 900px;">
        <div style="width: 400px; float: left;">
            <table cellpadding="10" cellspacing="5" class="checkoutStart">
                <tr>
                    <td colspan="2">
                        <h2>Use PayPal</h2>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label1" runat="server" Text="Batch ID:" Width="150px"></asp:Label>
                    </td>
                    <td class="style1">
                        <asp:Label ID="LabelBatchID" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label2" runat="server" Text="Volume:"></asp:Label>
                    </td>
                    <td class="style1">
                        <asp:Label ID="LabelVolume" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label3" runat="server" Text="No. of vouchers:"></asp:Label>
                    </td>
                    <td class="style1">
                        <asp:Label ID="LabelNoOfVouchers" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label4" runat="server" Text="Creation date:"></asp:Label>
                    </td>
                    <td class="style1">
                        <asp:Label ID="LabelCreationDate" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label5" runat="server" Text="Amount to pay:"></asp:Label>
                    </td>
                    <td class="style1">
                        <asp:Label ID="LabelAmountToPay" runat="server" Font-Bold="True"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:ImageButton ID="ButtonPaypalCheckout" runat="server"
                            ImageUrl="https://www.paypal.com/en_US/i/btn/btn_xpressCheckout.gif"
                            Width="145" AlternateText="Pay with PayPal"
                            OnClick="ButtonPaypalCheckout_Click"
                            BackColor="Transparent" BorderWidth="0" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="ButtonCancel" runat="server" Text="Cancel" OnClick="ButtonCancel_Click" />
                    </td>
                </tr>
            </table>
        </div>
        <%-- <table class="inlineTable">
        <tr>
            <td>
                 <asp:Button ID="Button1" runat="server" Text="Stripe" OnClick="ButtonStripe_Click" />
            </td>
        </tr>
    </table>--%>
        <div style="width: 400px; float: right;">
            <table cellpadding="10" cellspacing="5" class="checkoutStart">
                <tr>
                    <td colspan="2">
                        <h2>Use Credit Card</h2>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="txtCardNumber">Card number</label>
                    </td>
                    <td>
                        <input type="text" id="txtCardNumber" placeholder="e.g. 4242 4242 4242 4242" autocomplete="off" size="24"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="txtCVC">CVC</label>
                    </td>
                    <td>
                        <input type="text" id="txtCVC" placeholder="e.g. 545" autocomplete="off" maxlength="4" size="8" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="txtExpirationDate">Expiration date</label>
                    </td>
                    <td>
                        <input type="text" id="txtMonth" placeholder="MM" autocomplete="off" maxlength="2" size="2" />
                        <label>/</label>
                        <input type="text" id="txtYear" placeholder="YY" autocomplete="off" maxlength="2" size="2" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="amountToPayStripe" runat="server" Text="Amount to pay"></asp:Label>
                    </td>
                    <td class="style1">
                        <asp:Label ID="labelAmountToPayStripe" runat="server" Font-Bold="True"></asp:Label>
                    </td>
                    <%--<td class="style1">
                        <asp:Label ID="labelAmountToPayHipay" runat="server" Font-Bold="True"></asp:Label>
                    </td>--%>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:CheckBox ID="acceptCheckbox" runat="server" Text="I have read and accept the <a target='_blank' href='http://www.satadsl.net/general-terms-conditions-sale/'>terms and conditions of sale</a>" ClientIDMode="Static"/>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Label ID="labelPaymentResult" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
            &nbsp;&nbsp;&nbsp;<input id="submitPayment" type="button" value="Submit Payment" onclick="getToken(); return false;" />
            <input type="hidden" name="inputStripeToken" id="inputStripeToken">
            <input type="hidden" name="inputHiPayToken" id="inputHiPayToken">
        </div>
    </div>
</telerik:RadAjaxPanel>


<script type="text/javascript">
    Stripe.setPublishableKey(config.stripePublishableKey);

    function getToken() {
        $("#submitPayment").prop('disabled', true);
        $("span[id$='labelPaymentResult']").addClass("blue");
        $("span[id$='labelPaymentResult']").text("Processing... Please Wait");
        var cardNumber = $('#txtCardNumber').val();
        var cvc = $('#txtCVC').val();
        var month = $('#txtMonth').val();
        var year = $('#txtYear').val();
        var accept = $('#acceptCheckbox').is(':checked');

        if (!accept) {
            reportError("You must accept the terms and conditions of sale");
            return;
        }

        if (!Stripe.card.validateCardNumber(cardNumber)) {
            reportError("Invalid card number");
            return;
        }

        if (!Stripe.card.validateCVC(cvc)) {
            reportError("Invalid CVC number");
            return;
        }

        if (!Stripe.card.validateExpiry(month, year)) {
            reportError("Invalid expiration date");
            return;
        }

        Stripe.card.createToken({
            number: cardNumber,
            cvc: cvc,
            exp_month: month,
            exp_year: year
        }, stripeResponseHandler);
    }

    function stripeResponseHandler(status, response) {
        if (response.error) {
            // Show the errors on the form
            reportError(response.error.message);
            $("#submitPayment").prop('disabled', false);
        } else {
            // response contains id and card, which contains additional card details
            var token = response.id;
            // Insert the token into the form so it gets submitted to the server
            $('#inputStripeToken').val(token);
            // and submit
            document.forms[0].submit();
        }
    }

    function reportError(error) {
        error.fontcolor("red");
        $("span[id$='labelPaymentResult']").addClass("red");
        $("span[id$='labelPaymentResult']").text(error);
        $("#submitPayment").prop('disabled', false);
    }

    function getHiPayToken() {

        $("span[id$='labelPaymentResult']").addClass("blue");
        $("span[id$='labelPaymentResult']").text("Processing... Please Wait");
        $("#submitPayment").prop('disabled', true);
        var params = {
            card_number: $('#txtCardNumber').val(),
            cvc: $('#txtCVC').val(),
            card_expiry_month: $('#txtMonth').val(),
            card_expiry_year: $('#txtYear').val(),
            //card_holder: $('#txtName').val(),
            multi_use: '0'
        };

        HiPay.setTarget(config.hiPayTarget); // default is production/live
        // These are fake credentials, put your own credentials here (HiPay Enterprise back office > Integration > Security settings and create credentials with public visibility)
        HiPay.setCredentials('11111111.stage-secure-gateway.hipay-tpp.com', config.hiPayPublishableKey);

        HiPay.create(params,
          function (result) {
              // Insert the token into the form so it gets submitted to the server
              $('#inputHiPayToken').val(result.token);
              // and submit
              document.forms[0].submit();
          },
          function (errors) {
              // An error occurred
              $("span[id$='labelPaymentResult']").addClass("red");
              $("#submitPayment").prop('disabled', false);
              if (typeof errors.message != "undefined") {
                  $("span[id$='labelPaymentResult']").text(errors.message);
              } else {
                  $("span[id$='labelPaymentResult']").text("An error occurred with the request.");
              }
          }
        );

    }
</script>
