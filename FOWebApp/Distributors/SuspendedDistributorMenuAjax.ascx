﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SuspendedDistributorMenuAjax.ascx.cs" Inherits="FOWebApp.Distributors.SuspendedDistributorMenuAjax" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<telerik:RadTreeView ID="RadTreeViewDistributorMenu" runat="server" 
    onnodeclick="RadTreeViewDistributorMenu_NodeClick" Skin="Metro" 
    ValidationGroup="Distributors main menu">
    <Nodes>
        <telerik:RadTreeNode runat="server" Expanded="True" 
            ImageUrl="~/Images/earth_preferences.png" Owner="RadTreeViewDistributorMenu" 
            Text="Distributor Menu" Font-Bold="True">
            <Nodes>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/Account.png" Text="Account" 
                    ToolTip="Show account information" Value="n_account">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Text="Change Password" 
                            Value="n_Change_Password" ToolTip="Change your password">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/exit.png" Text="Log Off" 
                    ToolTip="Close the session" Value="n_log_off">
                </telerik:RadTreeNode>
            </Nodes>
        </telerik:RadTreeNode>
        <telerik:RadTreeNode ID="AboutNode" runat="server" ImageUrl="~/Images/information.png" 
            Text="About" Value="n_about" ToolTip="About this application">
        </telerik:RadTreeNode>
    </Nodes>
</telerik:RadTreeView>
