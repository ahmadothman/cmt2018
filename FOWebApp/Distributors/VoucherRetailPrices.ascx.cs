﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Security;
using System.Web.UI.WebControls;
using System.Drawing;
using Telerik.Web.UI;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;
using FOWebApp.net.nimera.cmt.BOVoucherControllerWSRef;

namespace FOWebApp.Distributors
{
    public partial class VoucherRetailPrices : System.Web.UI.UserControl
    {
        BOAccountingControlWS boAccountingControlWS;
        BOVoucherControllerWS _boVoucherControlWS;
        BOLogControlWS _boLogControlWS;
        Distributor dist;
        
        protected void Page_Load(object sender, EventArgs e)
        {

            boAccountingControlWS = new BOAccountingControlWS();
            _boLogControlWS = new BOLogControlWS();
            MembershipUser myObject = Membership.GetUser();
            string userId = myObject.ProviderUserKey.ToString();
            dist = boAccountingControlWS.GetDistributorForUser(userId);
            //Load available voucher volumes into the combobox
            _boVoucherControlWS = new BOVoucherControllerWS();
            VoucherVolume[] vvList = _boVoucherControlWS.GetVoucherVolumes();

            Isp[] isps = boAccountingControlWS.GetISPsForDistributor((int)dist.Id);
            RadTreeView slaTree = (RadTreeView)RadComboBoxVoucherVolume.Items[0].FindControl("RadTreeViewSLA");
            foreach (Isp isp in isps)
                if ((isp.CompanyName.IndexOf("soho", StringComparison.OrdinalIgnoreCase) >= 0) || (isp.CompanyName.IndexOf("unlimited", StringComparison.OrdinalIgnoreCase) >= 0) || (isp.CompanyName.IndexOf("unl", StringComparison.OrdinalIgnoreCase) >= 0))
                {
                    RadTreeNode rtn = new RadTreeNode(isp.CompanyName, isp.Id.ToString());
                    rtn.ImageUrl = "~/Images/contract.png";
                    rtn.ToolTip = isp.Id.ToString();

                    //Add child nodes to the tree node
                    RadTreeNode slNode = null;
                    ServiceLevel[] serviceLevels = boAccountingControlWS.GetServicePacksByIsp((int)isp.Id);

                    foreach (ServiceLevel sl in serviceLevels)
                    {
                        foreach (VoucherVolume vv in vvList)
                            if ((Convert.ToInt32(sl.SlaId) == vv.Sla) && (boAccountingControlWS.IsVoucherSLA(vv.Sla)))
                            {
                                slNode = new RadTreeNode();
                                slNode.Value = vv.Id.ToString();
                                if ((sl.SlaName.IndexOf("unlimited", StringComparison.OrdinalIgnoreCase) >= 0)|| (sl.SlaName.IndexOf("unl", StringComparison.OrdinalIgnoreCase) >= 0))
                                    slNode.Text = sl.SlaName + vv.ValidityPeriod + " days - " + vv.UnitPriceEUR.ToString("0.00") + "€ / " + vv.UnitPriceUSD.ToString("0.00") + "$";
                                else
                                    slNode.Text = sl.SlaName + vv.VolumeMB + " MB - Valid " + vv.ValidityPeriod + " days - " + vv.UnitPriceEUR.ToString("0.00") + "€ / " + vv.UnitPriceUSD.ToString("0.00") + "$";
                                rtn.Nodes.Add(slNode);
                            }
                    }
                    slaTree.Nodes.Add(rtn);
                }


            //Load prices already set by the distributor
       
            VoucherVolume[] rpList = _boVoucherControlWS.GetVoucherRetailPricesForDistributor((int)dist.Id);
            string setPrices = "";
            foreach (VoucherVolume rp in rpList)
            {
                foreach (VoucherVolume vv in vvList)
                {
                    
                    if (rp.Id == vv.Id)
                    {
                        ServiceLevel sl = boAccountingControlWS.GetServicePack(vv.Sla);
                        if (sl != null)
                        {
                            if ((sl.SlaName.IndexOf("unlimited", StringComparison.OrdinalIgnoreCase) >= 0)|| (sl.SlaName.IndexOf("unl", StringComparison.OrdinalIgnoreCase) >= 0))
                                setPrices += sl.SlaName + vv.ValidityPeriod + " days - " + rp.UnitPriceEUR.ToString("0.00") + "€ / " + rp.UnitPriceUSD.ToString("0.00") + "$" + "<br/>";
                            else
                                setPrices += sl.SlaName + vv.VolumeMB + " MB - Valid " + vv.ValidityPeriod + " days - " + rp.UnitPriceEUR.ToString("0.00") + "€ / " + rp.UnitPriceUSD.ToString("0.00") + "$" + "<br/>";
                        }else
                            setPrices +=  vv.VolumeMB + " MB - Valid " + vv.ValidityPeriod + " days - " + rp.UnitPriceEUR.ToString("0.00") + "€ / " + rp.UnitPriceUSD.ToString("0.00") + "$" + "<br/>";

                    }
                }
            }
            LabelPricesSet.Text = setPrices;
        }

        protected void ButtonSetPrice_Click(object sender, EventArgs e)
        {
            if (_boVoucherControlWS.SetVoucherRetailPrice((int)dist.Id, Int32.Parse(HiddenFieldVVId.Value), (double)RadNumericTextBoxUnitPriceUSD.Value, (double)RadNumericTextBoxUnitPriceEUR.Value))
            {
                LabelResult.Text = "The retail price was successfully stored";
                LabelResult.ForeColor = Color.Green;
            }
            else
            {
                LabelResult.Text = "Storing the retail price failed";
                LabelResult.ForeColor = Color.Red;
            }
        }
    }
}