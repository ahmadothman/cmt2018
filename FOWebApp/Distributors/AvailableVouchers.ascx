﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AvailableVouchers.ascx.cs" Inherits="FOWebApp.Distributors.AvailableVouchers" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script type="text/javascript">
    function AvailableVouchersButtonClicked() {
        //Get the distributor Id and number of vouchers from the controls
        var distributorField = $find("<%= DistributorId.ClientID %>");
        var distributorId = parseInt(distributorField.get_value());

        //Call the webservice
        FOWebApp.WebServices.VoucherSupportWS.GetAvailableVouchers(distributorId, OnAvailableVouchersComplete);
    }

    function OnAvailableVouchersComplete(result) {
        //Set result to grid
        var tableView = $find("<%= RadGridVouchers.ClientID %>").get_masterTableView();
        tableView.set_dataSource(result);
        tableView.dataBind();
    }

    function RadGrid_OnCommand(sender, args) {
        //intentionally left blank 
    }

    function ShowPayForm(id) {
        window.radopen("Distributors/CheckoutStart.aspx?BatchId=" + id, "RadWindowCheckout");
        return false;
    }
</script>
<div style="margin-top: 10px">
    <div>
        <telerik:RadTextBox ID="DistributorId" runat="server" ReadOnly="True" ReadOnlyStyle-BorderStyle="None" ForeColor="White">
        <ReadOnlyStyle BorderStyle="None" />
        </telerik:RadTextBox>
    </div> 
    <telerik:RadGrid ID="RadGridVouchers" runat="server" CellSpacing="0"
        GridLines="None" AutoGenerateColumns="False" Skin="Metro" PageSize="20" OnItemDataBound="RadGridVouchers_ItemDataBound" OnItemCommand="RadGridVouchers_ItemCommand">
        <ClientSettings ClientEvents-OnGridCreated="AvailableVouchersButtonClicked" EnableRowHoverStyle="true">
            <ClientEvents OnCommand="RadGrid_OnCommand" />
            
        </ClientSettings>
<MasterTableView AutoGenerateColumns="False" PageSize="50" DataKeyNames="BatchId">
<CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>

<RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
<HeaderStyle Width="20px"></HeaderStyle>
</RowIndicatorColumn>

<ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
<HeaderStyle Width="20px"></HeaderStyle>
</ExpandCollapseColumn>

    <Columns>
         <telerik:GridCheckBoxColumn DataField="Release" DataType="System.Boolean" 
            FilterControlAltText="Filter Release column" 
            HeaderImageUrl="~\Images\lock_ok.png" HeaderText="Rel" 
            HeaderTooltip="Release Voucher Batch" UniqueName="Release">
        </telerik:GridCheckBoxColumn>
        <telerik:GridBoundColumn DataField="Release" HeaderText="Released?" UniqueName="Released2" Visible="false">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="BatchId" 
            FilterControlAltText="Filter BatchId column" HeaderText="Batch Id" 
            UniqueName="BatchId">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="AvailableVouchers" 
            FilterControlAltText="Filter column column" HeaderText="Available Vouchers" 
            UniqueName="AvailableVouchers">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="DateCreated" 
            FilterControlAltText="Filter column1 column" HeaderText="Date Created" 
            UniqueName="DateCreated">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="Volume" 
            FilterControlAltText="Filter Volume column" HeaderText="Volume" 
            UniqueName="Volume">
        </telerik:GridBoundColumn>
        <telerik:GridHyperLinkColumn 
            FilterControlAltText="Filter BatchIdLink column" HeaderText="Download" 
            UniqueName="BatchIdLink" DataNavigateUrlFields="BatchId" 
            DataNavigateUrlFormatString="Servlets/VoucherDownload.aspx?BatchId={0}" 
            ImageUrl="../Images/download.png" Target="RadWindowCheckout">
        </telerik:GridHyperLinkColumn>
        <telerik:GridHyperLinkColumn 
            FilterControlAltText="Filter Payment column" HeaderText="Pay" 
            UniqueName="Payment" DataNavigateUrlFields="BatchId" 
            DataNavigateUrlFormatString="~/Distributors/CheckoutStart.aspx?BatchId={0}" 
            Text="PAY">
        </telerik:GridHyperLinkColumn>
        <%--<telerik:GridTemplateColumn UniqueName="Payment2" HeaderText="Pay">
                    <ItemTemplate>
                        <asp:HyperLink ID="PayLink" runat="server" Text="Pay"></asp:HyperLink>
                    </ItemTemplate>
        </telerik:GridTemplateColumn>--%>
        <telerik:GridButtonColumn HeaderText="Complete" ImageUrl="~/Images/checkbox.png"
                UniqueName="Completed" ButtonType="ImageButton" HeaderButtonType="PushButton"
                Text="Complete" CommandName="Complete">
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </telerik:GridButtonColumn>
  
    </Columns>

<EditFormSettings>
<EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
</EditFormSettings>
</MasterTableView>

<FilterMenu EnableImageSprites="False"></FilterMenu>

<HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default"></HeaderContextMenu>
    </telerik:RadGrid>
</div>
<telerik:RadWindowManager ID="RadWindowManager1" runat="server" Skin="Metro" 
        DestroyOnClose="True">
    <Windows>
        <telerik:RadWindow ID="RadWindowCheckout" runat="server" 
            NavigateUrl="Distributors/CheckoutStart.aspx" Animation="Fade" Opacity="100" Skin="Metro" 
            Title="Voucher payment" AutoSize="False" Width="1000px" Height="700px" KeepInScreenBounds="True" EnableShadow="True" EnableAriaSupport="True" VisibleStatusbar="True" Modal="False" DestroyOnClose="False">
        </telerik:RadWindow>
    </Windows>
</telerik:RadWindowManager>
<b>test: </b><asp:Label ID="label1" runat="server"></asp:Label>
    
