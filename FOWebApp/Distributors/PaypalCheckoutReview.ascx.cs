﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using Telerik.Web.UI;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Web.UI.DataVisualization.Charting;
using System.Globalization;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOVoucherControllerWSRef;
using FOWebApp.net.nimera.cmt.BOPaymentControllerWSRef;

namespace FOWebApp.Distributors
{
    public partial class PaypalCheckoutReview1 : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public void componentDataInit()
        {
            action = Session["Action"].ToString();
            amount = Convert.ToDecimal(Session["amount"]);
            token = Session["token"].ToString();
            LabelAction.Text = action;
            LabelAmountToPay.Text = amount + Session["currency"].ToString();
        }

        public void ButtonConfirmPayment_Click(object sender, EventArgs e)
        {
            BOPaymentControllerWS boPaymentControl = new BOPaymentControllerWS();
            string payerId = "";
            string url = boPaymentControl.PaypalCheckoutProceed(token, ref payerId);

            Session["userCheckoutCompleted"] = "true"; // User has comfirmed the completion of the checkout
            Session["payerId"] = payerId;
            Response.Redirect(url);
        }

        public void ButtonCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("CheckoutCancel.aspx");
        }

        public string token = "";
        public string action = "";
        public decimal amount = 0M;
    }
}