﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VoucherRetailPrices.ascx.cs" Inherits="FOWebApp.Distributors.VoucherRetailPrices" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script type="text/javascript">
    function nodeClicking(sender, args) {
            var comboBox = $find("<%= RadComboBoxVoucherVolume.ClientID %>");

            var node = args.get_node()
            if (node.get_level() == 1) {

                comboBox.set_text(node.get_text());
                comboBox.set_value(node.get_value());
                document.getElementById('HiddenFieldVVId').value = node.get_value();
                comboBox.trackChanges();
                comboBox.get_items().getItem(0).set_text(node.get_text());
                comboBox.commitChanges();
                comboBox.hideDropDown();
                
            }            
        }
</script>
<asp:HiddenField ID="HiddenFieldVVId" runat="server" ClientIDMode="Static" />
<table cellpadding="10" cellspacing="5">
    <tr>
        <td colspan="2"><h1>Set the retail price for a voucher:</h1></td>
    </tr>
    <tr>
        <td>
            Voucher volume:
        </td>
        <td>
            <telerik:RadComboBox ID="RadComboBoxVoucherVolume" runat="server" Skin="Metro"
                Width="250px" ExpandDirection="Up" Height="200px" NoWrap="True" EnableVirtualScrolling="True" DropDownAutoWidth="Enabled">
                <ItemTemplate>
                        <telerik:RadTreeView ID="RadTreeViewSLA" runat="server" Skin="Metro" OnClientNodeClicking="nodeClicking">
                        </telerik:RadTreeView>
                    </ItemTemplate>
                    <Items>
                        <telerik:RadComboBoxItem Text="" />
                    </Items>
                <ExpandAnimation Type="OutCubic" />
            </telerik:RadComboBox>
        </td>
    </tr>
    <tr>
        <td>
            Unit price in US Dollar:
        </td>
        <td>
            <telerik:RadNumericTextBox ID="RadNumericTextBoxUnitPriceUSD" runat="server" ShowSpinButtons="true" NumberFormat-DecimalDigits="2" Width="100px" MinValue="0" Value="0"></telerik:RadNumericTextBox>
        </td>
    </tr>
    <tr>
        <td>
            Unit price in Euro:
        </td>
        <td>
            <telerik:RadNumericTextBox ID="RadNumericTextBoxUnitPriceEUR" runat="server" ShowSpinButtons="true" NumberFormat-DecimalDigits="2" Width="100px" MinValue="0" Value="0"></telerik:RadNumericTextBox>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="margin-left: 40px">
            <asp:button ID="ButtonRequestVouchers" runat="server" Text="Set price" OnClick="ButtonSetPrice_Click" ToolTip="Set current price for the selected voucher volume">
            </asp:button>
        </td>
            
    </tr>
    <tr>
        <td colspan="2"><asp:Label ID="LabelResult" runat="server"></asp:Label></td>
    </tr>
    <tr>
        <td colspan="2">
            Your current prices:
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <asp:Label ID="LabelPricesSet" runat="server"></asp:Label>
        </td>
    </tr>
</table>