﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CheckoutStartFormActivation.ascx.cs" Inherits="FOWebApp.Distributors.CheckoutStartFormActivation" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
    <script type="text/javascript">
        function DoBlur(fld) {
            fld.className = 'normalfld';
        }

        function DoFocus(fld) {
            fld.className = 'focusfld';
        }

        function DoChange(fld) {
            fld.className = 'changedfld';
        }
    </script>
</telerik:RadCodeBlock>
<telerik:RadAjaxPanel ID="RadAjaxPanelCheckoutStart" runat="server" GroupingText="Checkout start" Height="200px" Width="700px"
    EnableHistory="True" Style="margin-right: 114px">
<table cellpadding="10" cellspacing="5" class="checkoutStart">
    <tr>
        <td colspan="2">
            <h2>Please proceed with activation fee payment via PayPal</h2>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="Label3" runat="server" Text="Air Mac Address"></asp:Label>
        </td>
        <td class="style1">
            <asp:Label ID="LabelAirMac" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="Label4" runat="server" Text="Activation date:"></asp:Label>
        </td>
        <td class="style1">
            <asp:Label ID="LabelActivationDate" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="Label5" runat="server" Text="Amount to pay:"></asp:Label>
        </td>
        <td class="style1">
            <asp:Label ID="LabelAmountToPay" runat="server" Font-Bold="True"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            <asp:ImageButton ID="ButtonPaypalCheckout" runat="server" AlternateText="Pay with PayPal" BackColor="Transparent" BorderWidth="0" ImageUrl="https://www.paypal.com/en_US/i/btn/btn_xpressCheckout.gif" OnClick="ButtonPaypalCheckout_Click" Width="145" />
        </td>
        <td class="style1">&nbsp;</td>
    </tr>
    <tr>
        <td>
            <asp:Button ID="ButtonCancel" runat="server" OnClick="ButtonCancel_Click" Text="Cancel" />
        </td>
        <td class="style1">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="2">
            Note: Cancelling the payment will not cancel your activation request. Please contact SatADSL Support for more info.</td>
    </tr>
</table>
</telerik:RadAjaxPanel>