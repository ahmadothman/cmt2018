﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FOWebApp.Distributors;

namespace FOWebApp.Distributors
{
    public partial class IssueDetailsForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Load the details form and initialize the data
            string key = Request.Params["key"].ToString();

            IssueDetailsForm df =
                (IssueDetailsForm)Page.LoadControl("IssueDetailsForm.ascx");

            df.componentDataInit(key);

            PlaceHolderIssueDetailsForm.Controls.Add(df);
        }
    }
}