﻿using FOWebApp.HelperMethods.PdfFileWriterHelper;
using FOWebApp.net.nimera.cmt.BOInvoicingControllerWSRef;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace FOWebApp.Distributors
{
    public partial class InvoiceDetails : System.Web.UI.Page
    {
        private BOInvoicingControllerWS _invoiceController = new BOInvoicingControllerWS();
        private BOAccountingControlWS _accountingController = new BOAccountingControlWS();
        private BOLogControlWS _logController = new BOLogControlWS();
        protected InvoiceHeader _invoice;

        protected void Page_PreInit(object sender, EventArgs e)
        {
            // This page may only be accessed by distributors
            if (!Roles.IsUserInRole("Distributor"))
            {
                CmtApplicationException cmtApplicationException = new CmtApplicationException()
                {
                    ExceptionDateTime = DateTime.Now,
                    ExceptionDesc = string.Format("User {0} in role {1} tried to access a page he may not access.", Membership.GetUser().UserName, Roles.GetRolesForUser().FirstOrDefault()),
                    ExceptionLevel = 2,
                    UserDescription = "InvoiceDetails: PreInit Page"
                };

                _logController.LogApplicationException(cmtApplicationException);

                throw new Exception();
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            int invoiceId = int.Parse(Request.QueryString["id"]);

            _invoice = _invoiceController.GetInvoice(invoiceId);

            // This page may only be accessed by the distributor that corresponds to the invoice. No other distributors may access it.
            if (_accountingController.GetDistributorForUser(Membership.GetUser().ProviderUserKey.ToString()).Id != _invoice.DistributorId)
            {
                CmtApplicationException cmtApplicationException = new CmtApplicationException()
                {
                    ExceptionDateTime = DateTime.Now,
                    ExceptionDesc = string.Format("User {0} in role {1} tried to access an invoice ({2}) he may not access.",
                    Membership.GetUser().UserName, Roles.GetRolesForUser().FirstOrDefault(), _invoice.InvoiceId),
                    ExceptionLevel = 2,
                    UserDescription = "InvoiceDetails: Init Page"
                };

                _logController.LogApplicationException(cmtApplicationException);

                throw new Exception();
            }

            RadGridInvoiceDetails.ExportSettings.FileName = _invoice.InvoiceId;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                switch (_invoice.InvoiceMonth)
                {
                    case 1:
                        LiteralInvoiceMonth.Text = "January";
                        break;
                    case 2:
                        LiteralInvoiceMonth.Text = "February";
                        break;
                    case 3:
                        LiteralInvoiceMonth.Text = "March";
                        break;
                    case 4:
                        LiteralInvoiceMonth.Text = "April";
                        break;
                    case 5:
                        LiteralInvoiceMonth.Text = "May";
                        break;
                    case 6:
                        LiteralInvoiceMonth.Text = "June";
                        break;
                    case 7:
                        LiteralInvoiceMonth.Text = "July";
                        break;
                    case 8:
                        LiteralInvoiceMonth.Text = "August";
                        break;
                    case 9:
                        LiteralInvoiceMonth.Text = "September";
                        break;
                    case 10:
                        LiteralInvoiceMonth.Text = "October";
                        break;
                    case 11:
                        LiteralInvoiceMonth.Text = "November";
                        break;
                    case 12:
                        LiteralInvoiceMonth.Text = "December";
                        break;
                }

                switch (_invoice.InvoiceState)
                {
                    case 0:
                        LiteralInvoiceState.Text = "Unvalidated";
                        break;
                    case 1:
                        LiteralInvoiceState.Text = "Validated";
                        break;
                    case 2:
                        LiteralInvoiceState.Text = "Released";
                        break;
                    case 3:
                        LiteralInvoiceState.Text = "Accepted";
                        break;
                    case 4:
                        LiteralInvoiceState.Text = "Paid";
                        break;
                }

                //RadTextBoxRemarks.Text = _invoice.Remarks;
            }
        }

        protected void RadToolBarActions_ButtonClick(object sender, RadToolBarEventArgs e)
        {
            switch (e.Item.Value)
            {
                case "PdfButton":
                    PdfInvoiceHelper pdfInvoice = new PdfInvoiceHelper();

                    try
                    {
                        // Get the invoice again because changes could have been made.
                        pdfInvoice.Generate(_invoiceController.GetInvoice(_invoice.Id));
                        string filename = _invoice.InvoiceId.Replace("/", "-");
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "Invoice as PDF",
                            string.Format("window.open(\'{0}/{1}.pdf\',\'_blank\');",
                                ResolveUrl("~/CMTInvoices"), filename),
                            true);
                    }
                    catch (Exception ex)
                    {
                        CmtApplicationException cmtApplicationException = new CmtApplicationException()
                        {
                            ExceptionDateTime = DateTime.Now,
                            ExceptionDesc = ex.Message,
                            ExceptionStacktrace = ex.StackTrace,
                            ExceptionLevel = 4,
                            UserDescription = "InvoiceDetails: Generate PDF from Invoice"
                        };

                        _logController.LogApplicationException(cmtApplicationException);

                        RadWindowManagerInvoiceDetails.RadAlert(string.Format("<span style=\"color: {0}\">{1}</span>",
                        ColorTranslator.ToHtml(Color.DarkRed), "PDF creation failed.<br />Please try again later."), 300, 200, "PDF Creation Result", "pdfCreationAlertCallback");
                    }

                    break;
            }
        }

        protected void RadGridInvoiceDetails_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            ((RadGrid)sender).MasterTableView.DataSource = _invoiceController.GetInvoiceLines(_invoice.Id);
        }

        protected void RadGridInvoiceDetails_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem dataItem = (GridDataItem)e.Item;

                TableCell terminalIdCell = dataItem["TerminalId"];
                TableCell servicePackCell = dataItem["ServicePack"];

                // Take only a fixed amount of characters from the TerminalId and the ServicePack name

                SubstringTextTableCell(terminalIdCell, "LabelTerminalId", 0, 30);
                SubstringTextTableCell(servicePackCell, "LabelServicePack", 0, 13);
            }
        }

        /// <summary>
        /// Takes a substring from the text of a given label in a given table cell when the text of that label is longer than the specified length.
        /// That substring is taken, an ellipsis is added to the end and is put again into the text field of the label.
        /// Also, when a substring is taken (in the case the length of the text in the label is longer than the specified length),
        /// a tooltip is added to the label with the full text.
        /// </summary>
        /// <param name="tableCell">The table cell in which the label should be searched</param>
        /// <param name="labelId">The ID of the label of which a substring from the text should be taken</param>
        /// <param name="startIndex">The index where the substring should start</param>
        /// <param name="length">
        /// The length of the substring.
        /// When the text of the found label is shorter than this length, no substring is taken. 
        /// </param>
        private void SubstringTextTableCell(TableCell tableCell, string labelId, int startIndex, int length)
        {
            Label labelTableCell = null; // Need to assign null explicitily, otherwise compile error

            foreach (Control control in tableCell.Controls)
            {
                if (control is Label && control.ID == labelId)
                {
                    labelTableCell = (Label)control;
                }
            }

            if (labelTableCell != null)
            {
                string labelText = labelTableCell.Text;

                if (labelText.Length > length)
                {
                    labelTableCell.Text = labelText.Substring(startIndex, length) + "...";
                    labelTableCell.ToolTip = labelText;
                }
            }
        }
    }
}