﻿using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
//using HiPay;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;
using FOWebApp.net.nimera.cmt.BOPaymentControllerWSRef;
using FOWebApp.net.nimera.cmt.BOVoucherControllerWSRef;
using Stripe;
using System;
using System.Configuration;
using System.Drawing;
using System.Web.Security;

namespace FOWebApp.Distributors
{
    public partial class CheckoutStartForm : System.Web.UI.UserControl
    {
        public int batchId = 0;
        public decimal amountUSD = 0;
        public decimal amountUsdStripe = 0;
        public decimal amountUsdHiPay = 0;
        public decimal amountEUR = 0;
        public decimal amountEurStripe = 0;
        public decimal amountEurHiPay = 0;
        public int distributorId = 0;
        public string distributorFullName;
        public string currency;
        BOVoucherControllerWS boVoucherControl;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                string stripeToken = this.Request.Form.Get("inputStripeToken");
                string hiPayToken = this.Request.Form.Get("inputHiPayToken");
                if (!String.IsNullOrEmpty(stripeToken))
                {
                    SubmitStripePayment(stripeToken);
                    //SubmitHiPayPayment(hiPayToken);
                }
            }
        }

        public void componentDataInit(int batchIdInit)
        {
            batchId = batchIdInit;

            // get the distributor ID of the current user
            BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();
            MembershipUser myObject = Membership.GetUser();
            Guid userId = new Guid(myObject.ProviderUserKey.ToString());
            Distributor dist = boAccountingControl.GetDistributorForUser(userId.ToString());
            distributorId = Convert.ToInt32(dist.Id);
            distributorFullName = dist.FullName;

            // get the batch details form the database
            boVoucherControl = new BOVoucherControllerWS();
            VoucherBatch vBatch = boVoucherControl.GetVoucherBatchInfo(batchId);

            // get the unit price and volume from the database
            int volumeID = vBatch.Volume;
            VoucherVolume voucherVolume = boVoucherControl.GetVoucherVolumeById(volumeID);
            string batchVolume;

            if (voucherVolume.VolumeMB > 100000)
                batchVolume = "Unlimited";
            else
                batchVolume = voucherVolume.VolumeMB.ToString() + " MB";

            int unitPriceUSD = Convert.ToInt32(voucherVolume.UnitPriceUSD);
            int unitPriceEUR = Convert.ToInt32(voucherVolume.UnitPriceEUR);

            string amount;
            amountUSD = decimal.Round(vBatch.NumVouchers * unitPriceUSD * (decimal)1.06, 2);
            amountUsdStripe = decimal.Round((vBatch.NumVouchers * unitPriceUSD * (decimal)1.029) + (decimal)0.30, 2);
            //amountUsdHiPay = decimal.Round((vBatch.NumVouchers * unitPriceUSD * (decimal)1.029) + (decimal)0.30, 2);
            amountEUR = decimal.Round(vBatch.NumVouchers * unitPriceEUR * (decimal)1.06, 2);
            amountEurStripe = decimal.Round((vBatch.NumVouchers * unitPriceEUR * (decimal)1.029) + (decimal)0.25, 2);
            //amountEurHiPay = decimal.Round((vBatch.NumVouchers * unitPriceEUR * (decimal)1.029) + (decimal)0.25, 2);

            LabelBatchID.Text = batchId.ToString();
            LabelVolume.Text = batchVolume;
            LabelNoOfVouchers.Text = vBatch.NumVouchers.ToString();
            LabelCreationDate.Text = vBatch.DateCreated.ToShortDateString();
            currency = dist.Currency.ToLower().Trim();
            if (dist.Currency == "USD")
            {
                amount = amountUSD + " USD (incl. 6% PayPal fee)";
                labelAmountToPayStripe.Text = amountUsdStripe + "$ (charges included)";
                //labelAmountToPayHipay.Text = amountUsdHiPay + "$ (charges included)";
            }
            else
            {
                amount = amountEUR + " EUR (incl. 6% PayPal fee)";
                labelAmountToPayStripe.Text = amountEurStripe + "€ (charges included)";
                //labelAmountToPayHipay.Text = amountEurHiPay + "€ (charges included)";
            }
            LabelAmountToPay.Text = amount;
        }

        public void ButtonPaypalCheckout_Click(object sender, EventArgs e)
        {
            string token = "";
            BOPaymentControllerWS boPaymentControl = new BOPaymentControllerWS();
            string url;

            Session["token"] = token;
            if (LabelAmountToPay.Text.IndexOf("USD", StringComparison.OrdinalIgnoreCase) >= 0)
            {
                url = boPaymentControl.CheckoutStart("paypal", "Purchase vouchers batch Nr: " + batchId, distributorId, amountUSD, "USD", ref token);
                Session["token"] = token;
                Session["amount"] = amountUSD;
                Session["Action"] = "Purchase vouchers butch Nr: " + batchId;
                Session["batchId"] = batchId;
                Session["currency"] = "USD";
                Response.Redirect(url);
            }
            else
            {
                url = boPaymentControl.CheckoutStart("paypal", "Purchase vouchers batch Nr: " + batchId, distributorId, amountEUR, "EUR", ref token);
                Session["token"] = token;
                Session["amount"] = amountEUR;
                Session["currency"] = "EUR";
                Session["Action"] = "Purchase vouchers butch Nr: " + batchId;
                Session["batchId"] = batchId;
                Response.Redirect(url);
            }
        }

        public void ButtonCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("CheckoutCancel.aspx");
        }

        private void SubmitStripePayment(string token)
        {
            StripeConfiguration.SetApiKey(ConfigurationManager.AppSettings["StripeSecretKey"]);
            var myCharge = new StripeChargeCreateOptions();
            // always set these properties
            decimal amountToCharge = currency == "eur" ? amountEurStripe : amountUsdStripe;
            myCharge.Amount = Convert.ToInt32(amountToCharge * 100);
            myCharge.Currency = currency;
            // set this if you want to
            myCharge.Description = "Distributor " + distributorFullName + "  id " + distributorId + " - Payment for batch id " + batchId;
            myCharge.SourceTokenOrExistingSourceId = token;
            // set this if you have your own application fees (you must have your application configured first within Stripe)
            //myCharge.ApplicationFee = 25;
            // (not required) set this to false if you don't want to capture the charge yet - requires you call capture later
            myCharge.Capture = true;
            var chargeService = new StripeChargeService();
            try
            {
                StripeCharge stripeCharge = chargeService.Create(myCharge);
                if (stripeCharge.Paid)
                {
                    labelPaymentResult.ForeColor = Color.Green;
                    string paymentResult = "Payment successful.";
                    boVoucherControl.MarkVoucherBatchAsPaid(batchId,"Stripe");
                    if (boVoucherControl.ReleaseVoucherBatch(batchId))
                    {
                        paymentResult += " Voucher batch id " + batchId + " released.";
                    }
                    labelPaymentResult.Text = paymentResult;
                }
                else
                {
                    labelPaymentResult.ForeColor = Color.Red;
                    labelPaymentResult.Text = "Transaction failed. " + stripeCharge.FailureMessage;
                }
            }
            catch (Exception e)
            {
                labelPaymentResult.ForeColor = Color.Red;
                labelPaymentResult.Text = e.Message;
                BOLogControlWS boLogControlWS = new BOLogControlWS();
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDateTime = DateTime.Now;
                cmtEx.ExceptionDesc = e.Message;
                cmtEx.UserDescription = myCharge.Description;
                cmtEx.ExceptionLevel = 4;
                boLogControlWS.LogApplicationException(cmtEx);
            }
        }

        private void SubmitHiPayPayment(string token)
        {
            /* decimal amountToCharge = currency == "eur" ? amountEurStripe : amountUsdStripe;
            string description = "Distributor " + distributorFullName + "  id " + distributorId + " - Payment for batch id " + batchId;
            try
            {
                string responseString = null;
                using (var client = new WebClient())
                {
                    string authorization = ConfigurationManager.AppSettings["HiPaySecretKey"];
                    client.Headers.Set("Accept", "application/json");
                    client.Headers.Add("Authorization", "Basic " + authorization);
                    
                    var orderRequest = new NameValueCollection();
                    orderRequest["orderid"] = "Order_" + batchId;
                    orderRequest["operation"] = "Sale";
                    orderRequest["description"] = description;
                    orderRequest["currency"] = currency.ToUpper();
                    orderRequest["amount"] = Convert.ToString(amountToCharge);

                    var response = client.UploadValues("https://stage-secure-gateway.hipay-tpp.com/rest/", orderRequest);
                    responseString = Encoding.Default.GetString(response);
                }

                if (!String.IsNullOrEmpty(responseString))
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    serializer.MaxJsonLength = Int32.MaxValue;
                    //HiPayOrderRequestResponse orderResponse = serializer.Deserialize<HiPayOrderRequestResponse>(responseString);
                    if (orderResponse.state == "completed")
                    {
                        labelPaymentResult.ForeColor = Color.Green;
                        string paymentResult = "Payment successful.";
                        boVoucherControl.FlagVoucherBatchAsPaid(batchId);
                        if (boVoucherControl.ReleaseVoucherBatch(batchId))
                        {
                            paymentResult += " Voucher batch id " + batchId + " released.";
                        }
                        labelPaymentResult.Text = paymentResult;
                    }
                    else
                    {
                        labelPaymentResult.ForeColor = Color.Red;
                        labelPaymentResult.Text = "Transaction failed. " + orderResponse.reason;
                    }
                }
                else
                {
                    throw new Exception("Empty response received from the server.");
                }
            }
            catch (Exception e)
            {
                labelPaymentResult.ForeColor = Color.Red;
                labelPaymentResult.Text = e.Message;
                BOLogControlWS boLogControlWS = new BOLogControlWS();
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDateTime = DateTime.Now;
                cmtEx.ExceptionDesc = e.Message;
                cmtEx.UserDescription = description;
                cmtEx.ExceptionLevel = 4;
                boLogControlWS.LogApplicationException(cmtEx);
            }*/
        }
    }
}