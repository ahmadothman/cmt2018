﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DistributorMenuAjax.ascx.cs" Inherits="FOWebApp.Distributors.DistributorMenuAjax" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<telerik:RadTreeView ID="RadTreeViewDistributorMenu" runat="server"
    OnNodeClick="RadTreeViewDistributorMenu_NodeClick" Skin="Metro"
    ValidationGroup="Distributors main menu">
    <Nodes>
        <telerik:RadTreeNode runat="server" Expanded="True"
            ImageUrl="~/Images/earth_preferences.png" Owner="RadTreeViewDistributorMenu"
            Text="Distributor Menu" Font-Bold="True">
            <Nodes>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/Terminal.png"
                    Owner="" Text="Terminals" Value="n_Terminals" ToolTip="Manage and Monitor Terminals">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Owner="" Text="Search by MAC"
                            Value="n_Search_By_Mac" ToolTip="Find a terminal by means of its MAC Address">
                        </telerik:RadTreeNode>
                        <telerik:RadTreeNode runat="server" Owner="" Text="Search by SitId"
                            Value="n_Search_By_SitId" ToolTip="Find a terminal by means of its SitId">
                        </telerik:RadTreeNode>
                        <telerik:RadTreeNode runat="server" Owner="" Text="Alarms"
                            Value="n_Terminal_Alarms" ToolTip="Terminal monitoring">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/customersNew.png"
                    Owner="" Text="Customer Organisations" Value="n_Customers" ToolTip="Create and Manage Customer Organizations">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Text="Customer VNOs"
                            Value="n_CustomerVNOs" Visible="true" ToolTip="Customer VNO management">
                        </telerik:RadTreeNode>
                        <telerik:RadTreeNode runat="server" Owner="" Text="New customer organisation"
                            Value="n_Create_New" Visible="True" ToolTip="Create a new Customer Organization">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
               <%-- <telerik:RadTreeNode ImageUrl="~/Images/invoice.png" Text="Invoicing"
                    ToolTip="Invoicing Management" Value="n_Invoicing">
                    <Nodes>--%>
                        <telerik:RadTreeNode ImageUrl="~/Images/invoice.png" Text="Invoices" Value="n_InvoiceList"
                            ToolTip="View Released or Paid Invoices">
                        </telerik:RadTreeNode>
                        <%-- CMTSUPPORT-238 --%>
                        <%--<telerik:RadTreeNode Text="Billables" Value="n_BillableList"
                            Tooltip="View all Billable Actions with their Price"></telerik:RadTreeNode>--%>
                    <%--</Nodes>
                </telerik:RadTreeNode>--%>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/form_blue.png" Owner=""
                    Text="Request Forms" Value="n_Request_Form" ToolTip="Activation and information Request Forms">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Owner="" Text="Activation" Visible="True" Value="n_ActivationRequest" ToolTip="Request For Terminal Activation">
                        </telerik:RadTreeNode>
                        <telerik:RadTreeNode runat="server" Owner="" Text="Reservation" Visible="True" Value="n_ReservationRequest" ToolTip="Request For Terminal Reservation">
                        </telerik:RadTreeNode>
                        <telerik:RadTreeNode runat="server" Owner="" Text="Suspension" Visible="False">
                        </telerik:RadTreeNode>
                        <%--<telerik:RadTreeNode runat="server" Owner="" Text="Redundant setup" Visible="True" Value="n_SatDiversityActivationRequest" ToolTip="Request For Redundant Terminal Setup">
                        </telerik:RadTreeNode>--%>
                        <telerik:RadTreeNode runat="server" Owner="" Text="Contact SatADSL" Value="n_Contact" ToolTip="Contact SatADSL">
                        </telerik:RadTreeNode>
                        <telerik:RadTreeNode runat="server" Owner="" Text="Request vouchers" Value="n_VoucherRequest" ToolTip="Request Vouchers">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/ticket_blue.png"
                    Text="Vouchers" Value="n_Vouchers" ToolTip="Manage your Vouchers">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Owner="" Text="Voucher retail prices" Visible="True" Value="n_VoucherRetailPrices" ToolTip="Set retail prices for vouchers">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/report.png"
                    Text="Reports" Value="n_Reports" ToolTip="Operational Reports">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Text="Terminal Switchovers"
                            Value="n_Reports_SwitchOvers" ToolTip="Switchover report over a period of time">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/flower_white.png" Text="White Label CMT" Value="n_WhiteLabel">
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/Account.png" Text="Account"
                    ToolTip="Show account information" Value="n_account">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Text="Change Password"
                            Value="n_Change_Password" ToolTip="Change your password">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/cash_flow.png"
                    Text="Billing &amp; Invoicing" ToolTip="Billing and invoicing"
                    Value="n_Billing" Visible="False">
                    <Nodes>
                        <telerik:RadTreeNode runat="server"
                            Text="Terminal Activity" ToolTip="Terminal Activity Report"
                            Value="n_TerminalActivity">
                        </telerik:RadTreeNode>
                        <telerik:RadTreeNode runat="server"
                            Text="Invoice On-Line" ToolTip="Your invoice On-Line" Value="n_Invoices"
                            Visible="True">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" Text="Issues" ImageUrl="~/Images/user_headset.png" Value="n_Issues" ToolTip="Show issues">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Text="New Issue" Value="n_NewIssue" ToolTip="Submit a new issue">
                        </telerik:RadTreeNode>
                        <telerik:RadTreeNode runat="server" Text="Speed test" Value="n_Speedtest">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/books_blue_preferences.png" Text="Documents" Value="n_Library">
                </telerik:RadTreeNode>
            </Nodes>
        </telerik:RadTreeNode>
        <telerik:RadTreeNode ID="AboutNode" runat="server" ImageUrl="~/Images/information.png"
            Text="About" Value="n_about" ToolTip="About this application">
        </telerik:RadTreeNode>
        <telerik:RadTreeNode runat="server" ImageUrl="~/Images/exit.png" Text="Log Off"
            ToolTip="Close the session" Value="n_log_off">
        </telerik:RadTreeNode>
    </Nodes>
</telerik:RadTreeView>
