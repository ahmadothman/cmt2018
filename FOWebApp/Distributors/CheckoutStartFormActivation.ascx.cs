﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using Telerik.Web.UI;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Web.UI.DataVisualization.Charting;
using System.Globalization;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOVoucherControllerWSRef;
using FOWebApp.net.nimera.cmt.BOPaymentControllerWSRef;
using FOWebApp.net.nimera.cmt.BOInvoicingControllerWSRef;

namespace FOWebApp.Distributors
{
    public partial class CheckoutStartFormActivation : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public void componentDataInit(string macAddressInit)
        {
            macAddress = macAddressInit;

            // get the distributor ID of the current user
            BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();
            BOInvoicingControllerWS invoiceController = new BOInvoicingControllerWS();
            MembershipUser myObject = Membership.GetUser();
            Guid userId = new Guid(myObject.ProviderUserKey.ToString());
            Distributor dist = boAccountingControl.GetDistributorForUser(userId.ToString());
            distributorId = Convert.ToInt32(dist.Id);

            // get the batch details form the database
            Terminal term = boAccountingControl.GetTerminalDetailsByMAC(macAddress);
            // get activation price from the database
            Billable distBillableActSoHo = invoiceController.GetBillableForDistributor(4013, distributorId);

            
            amountUSD = decimal.Round(distBillableActSoHo.PriceUSD * (decimal)1.06, 2);
            amountEUR = decimal.Round(distBillableActSoHo.PriceEU * (decimal)1.06, 2);

            string amount;
            
            if (dist.Currency=="USD")
                amount = amountUSD + " USD (incl. 6% PayPal fee)";
            else
                amount = amountEUR + " EUR (incl. 6% PayPal fee)";

            LabelAirMac.Text = term.MacAddress;
            LabelActivationDate.Text = term.FirstActivationDate.ToString();
            LabelAmountToPay.Text = amount;

        }

        public void ButtonPaypalCheckout_Click(object sender, EventArgs e)
        {
            string token = "";
            BOPaymentControllerWS boPaymentControl = new BOPaymentControllerWS();
            string url;

            Session["token"] = token;
            if (LabelAmountToPay.Text.IndexOf("USD", StringComparison.OrdinalIgnoreCase) >= 0)
            {
                url = boPaymentControl.CheckoutStart("paypal", "Activation fee", distributorId, amountUSD, "USD", ref token);
                Session["token"] = token;
                Session["amount"] = amountUSD;
                Session["Action"] = "Activation fee for Terminal: "+ macAddress;
                Session["macAddress"] = macAddress;
                Session["currency"] = "USD";
                Response.Redirect(url);
            }
            else
            {
                url = boPaymentControl.CheckoutStart("paypal", "Activation fee", distributorId, amountEUR, "EUR", ref token);
                Session["token"] = token;
                Session["amount"] = amountEUR;
                Session["macAddress"] = macAddress;
                Session["currency"] = "EUR";
                Session["Action"] = "Activation fee for Terminal: " + macAddress;
                Response.Redirect(url);
            }


        }

        public void ButtonCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("CheckoutCancel.aspx");
        }

        public string macAddress = "";
        public decimal amountUSD = 0;
        public decimal amountEUR = 0;
        public int distributorId = 0;
    }
}