﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WhiteLabelConfig.ascx.cs" Inherits="FOWebApp.Distributors.WhiteLabelConfig" %>
<style type="text/css">
    .auto-style1 {
        height: 39px;
    }
</style>
<h2>Configure your CMT plug-in</h2>
<table cellpadding="10" cellspacing="5">
    <tr>
        <td colspan="2">Your key is
            <asp:Label ID="LabelKey" runat="server" Font-Bold="True"
                ForeColor="#FF3300"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="auto-style1">
            Background Color:
        </td>
        <td class="auto-style1">
            <telerik:RadColorPicker ID="RadColorPickerBackground" runat="server" Columns="20" EnableCustomColor="True" Overlay="True" PaletteModes="All" ShowIcon="True" ShowRecentColors="True" Skin="Metro" ToolTip="Select your background color" Width="260px"></telerik:RadColorPicker>
        </td>
    </tr>
    <tr>
        <td>Select your theme:</td>
        <td>
            <telerik:RadComboBox ID="RadComboBoxTheme" runat="server" Skin="Metro">
                <Items>
                    <telerik:RadComboBoxItem runat="server" Text="None" Value="None" />
                    <telerik:RadComboBoxItem runat="server" Text="Blue Lagoon" Value="BlueLagoon" />
                    <telerik:RadComboBoxItem runat="server" Text="Green Forest" Value="GreenForest" />
                    <telerik:RadComboBoxItem runat="server" Text="Dessert Plain" Value="DessertPlain" />
                    <telerik:RadComboBoxItem runat="server" Text="Night Moves" Value="NightMoves" />
                    <telerik:RadComboBoxItem runat="server" Text="Simple" Value="Simple" />
                </Items>
            </telerik:RadComboBox>
        </td>
    </tr>
    <tr>
        <td colspan="2">Log-on page text: </td>
    </tr>
    <tr>
        <td colspan ="2">
            <telerik:RadEditor ID="RadEditorIntroText" runat="server" DocumentManager-MaxUploadFileSize="5000000" Skin="Metro">
                <Tools>
                    <telerik:EditorToolGroup Tag="MainToolbar">
                        <telerik:EditorTool Name="Print" ShortCut="CTRL+P" />
                        <telerik:EditorTool Name="FindAndReplace" ShortCut="CTRL+F" />
                        <telerik:EditorTool Name="SelectAll" ShortCut="CTRL+A" />
                        <telerik:EditorTool Name="Cut" />
                        <telerik:EditorTool Name="Copy" ShortCut="CTRL+C" />
                        <telerik:EditorTool Name="Paste" ShortCut="CTRL+V" />
                        <telerik:EditorSeparator />
                        <telerik:EditorSplitButton Name="Undo">
                        </telerik:EditorSplitButton>
                        <telerik:EditorSplitButton Name="Redo">
                        </telerik:EditorSplitButton>
                    </telerik:EditorToolGroup>
                    <telerik:EditorToolGroup Tag="InsertToolbar">
                        <telerik:EditorTool Enabled="False" Name="ImageManager" ShortCut="CTRL+G" />
                        <telerik:EditorTool Enabled="False" Name="DocumentManager" />
                        <telerik:EditorTool Name="LinkManager" ShortCut="CTRL+K" />
                        <telerik:EditorTool Name="Unlink" ShortCut="CTRL+SHIFT+K" />
                    </telerik:EditorToolGroup>
                    <telerik:EditorToolGroup>
                        <telerik:EditorTool Name="Superscript" />
                        <telerik:EditorTool Name="Subscript" />
                        <telerik:EditorTool Name="InsertHorizontalRule" />
                        <telerik:EditorSeparator />
                        <telerik:EditorTool Name="FormatCodeBlock" />
                    </telerik:EditorToolGroup>
                    <telerik:EditorToolGroup>
                        <telerik:EditorDropDown Name="FormatBlock">
                        </telerik:EditorDropDown>
                        <telerik:EditorDropDown Name="FontName">
                        </telerik:EditorDropDown>
                        <telerik:EditorDropDown Name="RealFontSize">
                        </telerik:EditorDropDown>
                    </telerik:EditorToolGroup>
                    <telerik:EditorToolGroup>
                        <telerik:EditorTool Name="Bold" ShortCut="CTRL+B" />
                        <telerik:EditorTool Name="Italic" ShortCut="CTRL+I" />
                        <telerik:EditorTool Name="Underline" ShortCut="CTRL+U" />
                        <telerik:EditorTool Name="StrikeThrough" />
                        <telerik:EditorSeparator />
                        <telerik:EditorTool Name="JustifyLeft" />
                        <telerik:EditorTool Name="JustifyCenter" />
                        <telerik:EditorTool Name="JustifyRight" />
                        <telerik:EditorTool Name="JustifyFull" />
                        <telerik:EditorTool Name="JustifyNone" />
                        <telerik:EditorSeparator />
                        <telerik:EditorTool Name="Indent" />
                        <telerik:EditorTool Name="Outdent" />
                        <telerik:EditorSeparator />
                        <telerik:EditorTool Name="InsertOrderedList" />
                        <telerik:EditorTool Name="InsertUnorderedList" />
                        <telerik:EditorSeparator />
                        <telerik:EditorTool Name="ToggleTableBorder" />
                    </telerik:EditorToolGroup>
                    <telerik:EditorToolGroup>
                        <telerik:EditorSplitButton Name="ForeColor">
                        </telerik:EditorSplitButton>
                        <telerik:EditorSplitButton Name="BackColor">
                        </telerik:EditorSplitButton>
                        <telerik:EditorToolStrip Name="FormatStripper">
                        </telerik:EditorToolStrip>
                    </telerik:EditorToolGroup>
                    <telerik:EditorToolGroup Tag="DropdownToolbar">
                        <telerik:EditorSplitButton Name="InsertSymbol">
                        </telerik:EditorSplitButton>
                        <telerik:EditorToolStrip Name="InsertTable">
                        </telerik:EditorToolStrip>
                        <telerik:EditorSeparator />
                        <telerik:EditorTool Name="ConvertToLower" />
                        <telerik:EditorTool Name="ConvertToUpper" />
                        <telerik:EditorSeparator />
                        <telerik:EditorTool Name="ToggleScreenMode" ShortCut="F11" />
                    </telerik:EditorToolGroup>
                </Tools>
                <Content>
                </Content>
            </telerik:RadEditor>
        </td>
    </tr>
    <tr>
        <td>
            <telerik:RadButton ID="RadButtonSubmit" runat="server" Text="Submit" OnClick="RadButtonSubmit_Click" Skin="Metro"></telerik:RadButton>
        </td>
        <td>
            <asp:Label ID="LabelResult" runat="server" Text=""></asp:Label>
        </td>
    </tr>
</table>
