﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Security;
using System.Web.UI.WebControls;
using System.Drawing;
using Telerik.Web.UI;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;
using FOWebApp.net.nimera.cmt.BOVoucherControllerWSRef;
using FOWebApp.Data;

namespace FOWebApp.Distributors
{
    public partial class VoucherRequestForm : System.Web.UI.UserControl
    {
        BOAccountingControlWS boAccountingControl;
        BOVoucherControllerWS _boVoucherControlWS;
        BOLogControlWS _boLogControlWS;
        string[] eMailRecipients = {FOWebApp.Properties.Settings.Default.SupportEmail};
        
        protected void Page_Load(object sender, EventArgs e)
        {
            DataHelper dataHelper = new DataHelper();

             boAccountingControl =
              new net.nimera.cmt.BOAccountingControlWSRef.BOAccountingControlWS();
            MembershipUser myObject = Membership.GetUser();
            string userId = myObject.ProviderUserKey.ToString();
            Distributor dist = boAccountingControl.GetDistributorForUser(userId);



            _boLogControlWS = new BOLogControlWS();

            //Load available voucher volumes into the combobox
            _boVoucherControlWS = new BOVoucherControllerWS();
            VoucherVolume[] vvList = _boVoucherControlWS.GetVoucherVolumes();
            



            //Fill-up the treeview in the RadComboBox
            Isp[] isps = boAccountingControl.GetISPsForDistributor((int)dist.Id);
            RadTreeView slaTree = (RadTreeView)RadComboBoxSLA.Items[0].FindControl("RadTreeViewSLA");
            foreach (Isp isp in isps)
                if ((isp.CompanyName.IndexOf("soho",StringComparison.OrdinalIgnoreCase)>=0)|| (isp.CompanyName.IndexOf("unlimited", StringComparison.OrdinalIgnoreCase) >= 0) || (isp.CompanyName.IndexOf("unl", StringComparison.OrdinalIgnoreCase) >= 0))
            {
                RadTreeNode rtn = new RadTreeNode(isp.CompanyName, isp.Id.ToString());
                rtn.ImageUrl = "~/Images/contract.png";
                rtn.ToolTip = isp.Id.ToString();

                //Add child nodes to the tree node
                RadTreeNode slNode = null;
                ServiceLevel[] serviceLevels = boAccountingControl.GetServicePacksByIsp((int)isp.Id);

                    foreach (ServiceLevel sl in serviceLevels)
                    {
                        foreach (VoucherVolume vv in vvList)
                            if ((Convert.ToInt32(sl.SlaId) == vv.Sla) && (boAccountingControl.IsVoucherSLA(vv.Sla)))
                            {
                                slNode = new RadTreeNode();
                                slNode.Value = vv.Id.ToString();
                                string UnitPrice;

                                //Display price according to Distributor default price
                                if (dist.Currency == "USD")
                                    UnitPrice = vv.UnitPriceUSD.ToString("0.00") + " $";
                                else
                                    UnitPrice = vv.UnitPriceEUR.ToString("0.00") + " €";

                                if ((sl.SlaName.IndexOf("unlimited", StringComparison.OrdinalIgnoreCase) >= 0)|| (sl.SlaName.IndexOf("unl", StringComparison.OrdinalIgnoreCase) >= 0))
                                    slNode.Text = sl.SlaName + vv.ValidityPeriod + " days - " + UnitPrice;
                                else if (sl.ServiceClass==7 )
                                    slNode.Text = vv.Description + " Volume only voucher - " + UnitPrice;
                                else
                                    slNode.Text = sl.SlaName + vv.VolumeMB + " MB - Valid " + vv.ValidityPeriod + " days - " + UnitPrice;
                                rtn.Nodes.Add(slNode);
                        }
                }
                slaTree.Nodes.Add(rtn);
            }

        }

        protected void ButtonRequestVouchers_Click(object sender, EventArgs e)
        {
            BOVoucherControllerWS _boVoucherControlWS = new BOVoucherControllerWS();
            //Get the authenticated user
            MembershipUser myObject = Membership.GetUser();
            string userId = myObject.ProviderUserKey.ToString();
            Distributor dist = boAccountingControl.GetDistributorForUser(userId);

            
            
            //Create the request
            HttpRequestWrapper requestWrapper = new HttpRequestWrapper(Request);
            VoucherRequest vr = new VoucherRequest();
            vr.DistributorId = (int)dist.Id;
            vr.VoucherVolumeId = Int32.Parse(HiddenFieldSLAId.Value);
            vr.NumVouchers = (int)RadNumericTextBoxvouchers.Value;
            vr.Sla = _boVoucherControlWS.GetVoucherVolumeById(Int32.Parse(HiddenFieldSLAId.Value)).Sla;

            //Generate the vouchers
            int bid;

            VoucherVolume vv = _boVoucherControlWS.GetVoucherVolumeById(vr.VoucherVolumeId);
            if (vv.VolumeMB >= 10000 || vv.VolumeMB * vr.NumVouchers >= 10000)
            {
                _boVoucherControlWS.CreateVouchers(vr.DistributorId, vr.NumVouchers,(Guid)myObject.ProviderUserKey, vr.VoucherVolumeId, out bid);

                _boVoucherControlWS.CreacteVoucherRequest(vr);
            
                //send an email to SatADSL
                string emailBody = "Voucher batch with id "+bid+" from distributor: " + dist.FullName + "has been created<br/>" +
                                   "Voucher volume details: " + RadComboBoxSLA.Text + "<br/>" +
                                   "No. of vouchers: " + vr.NumVouchers + "<br/>" +
                                   "This information messsage is now available in the list of voucher requests<br/><br/>" +
                                   "Be careful that this voucher batch should be paid before your next invoice due date otherwise the CMT will delete it automatically.<br/><br/>" +
                                   "CMT automated call do not respond.";
                _boLogControlWS.SendMail(emailBody, "New voucher batch create", eMailRecipients);
                
                LabelResult.ForeColor = Color.Green;
                LabelResult.Text = "Voucher batch successfully created. <br/> Be careful that this voucher batch should be paid before your next invoice due date otherwise the CMT will automatically delete it.";
            }
            else
            {
                LabelResult.ForeColor = Color.Red;
                LabelResult.Text = "Failed to create a voucher batch , Minimum order is 10 GB";
            }
        }   
    }
}