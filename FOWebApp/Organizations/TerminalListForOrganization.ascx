﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TerminalListForOrganization.ascx.cs"
    Inherits="FOWebApp.Organizations.TerminalListForOrganization" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script type="text/javascript">
    function RowSelected(sender, eventArgs) {
        var grid = sender;
        var masterDataView = grid.get_masterTableView();
        var rowNum = eventArgs.get_itemIndexHierarchical();
        var row = masterDataView.get_dataItems()[rowNum];
        var macAddress = masterDataView.getCellByColumnUniqueName(row, "MacAddressColumn").innerHTML;

        //Initialize and show the terminal details window
        var oWnd = radopen("TerminalDetailsForm.aspx?mac=" + macAddress, "RadWindowTerminalDetails");
    }
</script>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="RadGridTerminals">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGridTerminals" />
                <telerik:AjaxUpdatedControl ControlID="RadWindowManager1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Metro"/>
<telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" Height="200px"
    Width="467px" HorizontalAlign="NotSet">
    <telerik:RadGrid ID="RadGridTerminals" runat="server" AllowSorting="True"
        AutoGenerateColumns="False" CellSpacing="0" GridLines="None" Skin="Metro"
        ShowStatusBar="True" Width="700px" AllowFilteringByColumn="True"
        OnItemDataBound="RadGridTerminals_ItemDataBound"
        OnPageSizeChanged="RadGridTerminals_PageSizeChanged" AllowPaging="True"
        PageSize="50">
        <GroupingSettings CaseSensitive="false" />
        <ClientSettings EnableRowHoverStyle="true">
            <Selecting AllowRowSelect="True" />
            <ClientEvents OnRowSelected="RowSelected" />
        </ClientSettings>
        <MasterTableView CommandItemDisplay="Top" CommandItemSettings-ShowExportToExcelButton="False"
            CommandItemSettings-ShowExportToCsvButton="False"
            RowIndicatorColumn-Visible="True" RowIndicatorColumn-Resizable="True"
            RowIndicatorColumn-AndCurrentFilterFunction="Contains"
            CommandItemSettings-ShowAddNewRecordButton="False" ShowFooter="False"
            AllowPaging="False" PageSize="10">
            <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
            <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
            </RowIndicatorColumn>
            <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
            </ExpandCollapseColumn>
            <Columns>
                <telerik:GridBoundColumn DataField="FullName"
                    FilterControlAltText="Filter FullNameColumn column" HeaderText="Name"
                    UniqueName="FullNameColumn" ShowFilterIcon="False" AutoPostBackOnFilter="True">
                    <ItemStyle Wrap="False" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="MacAddress" FilterControlAltText="Filter MacAddressColumn column"
                    HeaderText="MAC Address" MaxLength="12" ReadOnly="True" Resizable="False"
                    UniqueName="MacAddressColumn" CurrentFilterFunction="Contains" ShowFilterIcon="False" AutoPostBackOnFilter="True">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="SitId" FilterControlAltText="Filter SitIdColumn column"
                    HeaderText="Site ID" UniqueName="SitIdColumn" ShowFilterIcon="False"
                    AllowFiltering="False">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="IpAddress" FilterControlAltText="Filter IpAddressColumn column"
                    HeaderText="IP Address" ReadOnly="True" UniqueName="IpAddressColumn" ShowFilterIcon="False" AutoPostBackOnFilter="True">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="SlaId" FilterControlAltText="Filter SLA column"
                    HeaderText="SLA" ReadOnly="True" UniqueName="ServiceColumn" ShowFilterIcon="False" AutoPostBackOnFilter="True">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn AllowSorting="True" DataField="AdmStatus" FilterControlAltText="Filter AdmStatusColumn column"
                    HeaderText="Status" ReadOnly="True" UniqueName="AdmStatusColumn" ShowFilterIcon="False" AutoPostBackOnFilter="True" AllowFiltering="False" ItemStyle-Wrap="False">
                    <ItemStyle Wrap="False" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="StartDate" FilterControlAltText="Filter StartDateColumn column"
                    HeaderText="Start Date" ReadOnly="True" UniqueName="StartDateColumn"
                    ItemStyle-Wrap="False" DataFormatString="{0:dd/MM/yyyy}">
                    <ItemStyle Wrap="False" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn AllowSorting="False" DataField="ExpiryDate" DataType="System.DateTime"
                    FilterControlAltText="Filter ExpiryDateColumn column" HeaderText="Expiry Date"
                    ReadOnly="True" UniqueName="ExpiryDateColumn" ItemStyle-Wrap="False"
                    AutoPostBackOnFilter="True" DataFormatString="{0:dd/MM/yyyy}">
                    <ItemStyle Wrap="False" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="CNo"
                    FilterControlAltText="Filter CNo column" HeaderText="C/No" UniqueName="CNo"
                    Visible="False">
                </telerik:GridBoundColumn>
                <telerik:GridTemplateColumn HeaderText="Redundant" UniqueName="RedundantSetup">
                <ItemTemplate>
                    <%--<asp:HiddenField runat="server" ID="RedundantSetupValue" Value="<%#Eval("RedundantSetup") %>" />--%>
                    <asp:Image runat="server" ID="RedundantSetupImage" ImageUrl="~/Images/satellite_dish_dual.png" ImageAlign="Middle" Visible="false" AlternateText='<%# Eval("RedundantSetup") %>' />
                </ItemTemplate>
            </telerik:GridTemplateColumn>
            </Columns>
            <EditFormSettings>
                <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                </EditColumn>
            </EditFormSettings>
        </MasterTableView>
        <FilterMenu EnableImageSprites="False">
        </FilterMenu>
        <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default">
        </HeaderContextMenu>
    </telerik:RadGrid>
    <p>
        Number of terminals in this list:
   
        <asp:Label ID="LabelNumRows" runat="server" Font-Bold="True"></asp:Label>
    </p>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" Skin="Metro"
        DestroyOnClose="True">
        <Windows>
            <telerik:RadWindow ID="RadWindowTerminalDetails" runat="server"
                NavigateUrl="TerminalDetailsForm.aspx" Animation="Fade" Opacity="100" Skin="Metro"
                Title="Terminal Details" AutoSize="False" Width="1000px" Height="700px" KeepInScreenBounds="True" EnableShadow="True" EnableAriaSupport="True" VisibleStatusbar="True" Modal="False" DestroyOnClose="False">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadWindowModifyActivity" runat="server" Animation="None" Opacity="100"
                Skin="Metro" Title="Modify Activity" AutoSize="False" Width="850px" Height="655px"
                KeepInScreenBounds="True" EnableShadow="True" EnableAriaSupport="True" VisibleStatusbar="True"
                Modal="False" DestroyOnClose="False">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
</telerik:RadAjaxPanel>


