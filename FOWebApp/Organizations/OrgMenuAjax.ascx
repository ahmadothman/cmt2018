﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OrgMenuAjax.ascx.cs" Inherits="FOWebApp.Organizations.OrgMenuAjax" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<telerik:RadTreeView ID="RadTreeViewOrganizationMenu" runat="server" Skin="Metro"
    ValidationGroup="Organization main menu"
    OnNodeClick="RadTreeViewOrganizationMenu_NodeClick">
    <Nodes>
        <telerik:RadTreeNode runat="server" Expanded="True"
            ImageUrl="~/Images/earth_preferences.png" Owner="RadTreeViewOrganizationMenu"
            Text="Organization Menu" Font-Bold="True">
            <Nodes>
                <telerik:RadTreeNode runat="server" Owner="" Text="Terminals" Value="n_Terminals"
                    ImageUrl="~/Images/Terminal.png" ToolTip="Manage Your Terminals">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Owner="" Text="Search By MAC"
                            Value="n_Search_By_MAC" ToolTip="Find a Terminal by its MAC Address">
                        </telerik:RadTreeNode>
                        <telerik:RadTreeNode runat="server" Owner="" Text="Search By SitId"
                            Value="n_Search_By_SitId" ToolTip="Find a Terminal by means of its SitId">
                        </telerik:RadTreeNode>
                        <telerik:RadTreeNode runat="server" Owner="" Text="VNO dashboard"
                            Value="n_VNO_dashboard" ToolTip="Manage terminal bandwidth correction">
                        </telerik:RadTreeNode>
                        <telerik:RadTreeNode runat="server" Text="Alarms"
                            Value="n_Terminal_Alarms" ToolTip="Terminal monitoring">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/form_blue.png" Owner=""
                    Text="Request Forms" Value="n_Request_Form" ToolTip="Activation and information Request Forms">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Owner="" Text="Reservation" Visible="False" Value="n_ReservationRequest" ToolTip="Request For Terminal Reservation">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/report.png"
                    Text="Reports" Value="n_Reports" ToolTip="Operational Reports">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Text="Terminal Switchovers"
                            Value="n_Reports_SwitchOvers" ToolTip="Switchover report over a period of time">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/Account.png" Text="Account"
                    ToolTip="Show account information" Value="n_Account">
                    <Nodes>
                        <telerik:RadTreeNode runat="server" Text="Change Password"
                            Value="n_Change_Password" ToolTip="Change your password">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeNode>
                <telerik:RadTreeNode runat="server" ImageUrl="~/Images/books_blue_preferences.png" Text="Documents" ToolTip="List of available documents" Value="n_Library">
                </telerik:RadTreeNode>
            </Nodes>
        </telerik:RadTreeNode>
        <telerik:RadTreeNode ID="AboutNode" runat="server" ImageUrl="~/Images/information.png"
            Text="About" Value="n_About" ToolTip="About this application">
        </telerik:RadTreeNode>
        <telerik:RadTreeNode runat="server" ImageUrl="~/Images/exit.png" Text="Log Off"
            ToolTip="Close the session" Value="n_log_off">
        </telerik:RadTreeNode>
    </Nodes>
</telerik:RadTreeView>
