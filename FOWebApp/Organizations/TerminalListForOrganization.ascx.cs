﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using Telerik.Web.UI;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Web.UI.DataVisualization.Charting;
using System.Globalization;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOMonitorControlWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;

namespace FOWebApp.Organizations
{
    public partial class TerminalListForOrganization : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Session["_termListForDistributor_PageSize"] == null)
            {
                RadGridTerminals.PageSize = 20;
            }
            else
            {
                RadGridTerminals.PageSize = (int)this.Session["_termListForDistributor_PageSize"];
            }
            //Load data into the grid view. This view lists all terminals for the 
            //authenticated user.
            BOAccountingControlWS boAccountingControl =
              new net.nimera.cmt.BOAccountingControlWSRef.BOAccountingControlWS();

            //Get the authenticated user
            MembershipUser myObject = Membership.GetUser();
            string userId = myObject.ProviderUserKey.ToString();

            Organization org = boAccountingControl.GetOrganizationForUser(userId);

            if (org != null)
            {
                //Get the Distributor linked to the user
                Terminal[] terminals = boAccountingControl.GetTerminalsByOrganization((int)org.Id);

                RadGridTerminals.DataSource = terminals;
                RadGridTerminals.DataBind();

                LabelNumRows.Text = terminals.Length.ToString();
            }
        }

        protected void RadGridTerminals_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridItemCollection selectedItems = RadGridTerminals.SelectedItems;

            if (selectedItems.Count > 0)
            {
                GridItem selectedItem = selectedItems[0];
                Terminal terminal = (Terminal)selectedItem.DataItem;
                string macAddress = terminal.MacAddress;
            }

        }

        protected void RadGridTerminals_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem dataItem = (GridDataItem)e.Item;

                if (dataItem["AdmStatusColumn"].Text.Equals("1"))
                {
                    dataItem["AdmStatusColumn"].Text = "Locked";
                    dataItem["AdmStatusColumn"].ForeColor = Color.Red;
                }

                if (dataItem["AdmStatusColumn"].Text.Equals("2"))
                {
                    dataItem["AdmStatusColumn"].Text = "Un-locked";
                    dataItem["AdmStatusColumn"].ForeColor = Color.Green;
                }

                if (dataItem["AdmStatusColumn"].Text.Equals("3"))
                {
                    dataItem["AdmStatusColumn"].Text = "Decommissioned";
                    dataItem["AdmStatusColumn"].ForeColor = Color.Blue;
                }

                if (dataItem["AdmStatusColumn"].Text.Equals("5"))
                {
                    dataItem["AdmStatusColumn"].Text = "Request";
                    dataItem["AdmStatusColumn"].ForeColor = Color.DarkOrange;
                }

                if (dataItem["AdmStatusColumn"].Text.Equals("6"))
                {
                    dataItem["AdmStatusColumn"].Text = "Deleted";
                    dataItem["AdmStatusColumn"].ForeColor = Color.Black;
                }
                
                string slaId = dataItem["ServiceColumn"].Text;
                if (!string.IsNullOrEmpty(slaId))
                {
                    try
                    {
                        BOAccountingControlWS boAccountingControl =
                                                new net.nimera.cmt.BOAccountingControlWSRef.BOAccountingControlWS();

                        //Display the Name of the SLA in stead of the ID
                        ServiceLevel sl = boAccountingControl.GetServicePack(int.Parse(slaId));
                        dataItem["ServiceColumn"].Text = sl.SlaName;

                        //Colour coding for CNo values
                        double CNo = Convert.ToDouble(dataItem["CNo"].Text);
                        if (sl.DRRTN <= 256)
                        {
                            if (CNo < 52.71)
                            {
                                dataItem["Cno"].ForeColor = Color.Red;
                            }
                            else if (CNo < 59.64)
                            {
                                dataItem["Cno"].ForeColor = Color.DarkOrange;
                            }
                            else
                            {
                                dataItem["Cno"].ForeColor = Color.DarkGreen;
                            }
                        }
                        else
                        {
                            if (CNo < 58.73)
                            {
                                dataItem["Cno"].ForeColor = Color.Red;
                            }
                            else if (CNo < 60.49)
                            {
                                dataItem["Cno"].ForeColor = Color.DarkOrange;
                            }
                            else
                            {
                                dataItem["Cno"].ForeColor = Color.DarkGreen;
                            }
                        }
                    }
                    catch (Exception ex)
                    {

                        BOLogControlWS logController = new BOLogControlWS();
                        CmtApplicationException cmtAppEx = new CmtApplicationException
                        {
                            ExceptionDateTime = DateTime.UtcNow,
                            ExceptionDesc = ex.Message,
                            ExceptionStacktrace = ex.StackTrace,
                            ExceptionLevel = 2,
                            UserDescription = "TerminalList: The Service Pack Could not be found"
                        };
                        logController.LogApplicationException(cmtAppEx);
                    }
                }

                //Put the MAC addresses of the non-operational terminals in red
                if (dataItem["CNo"].Text.Equals("-1,00") || dataItem["CNo"].Text.Equals("") || dataItem["CNo"].Text.Equals("-1.00"))
                {
                    dataItem["MacAddressColumn"].ForeColor = Color.Red;
                }
                else
                {
                    dataItem["MacAddressColumn"].ForeColor = Color.Green;
                }

                //RedundantSetup is a property (bool) of the Terminal entity, if RedundantSetup is true, the image should apear in the GridTemplateColumn in the front-end by setting
                //the visibility to true.
                System.Web.UI.WebControls.Image image = (System.Web.UI.WebControls.Image)dataItem["RedundantSetup"].FindControl("RedundantSetupImage");
                if (image.AlternateText == bool.TrueString)
                {
                    image.Visible = true;
                }
            }
        }

        protected void RadWindowManager1_Disposed(object sender, EventArgs e)
        {
        }

        protected void RadWindowManager1_Unload(object sender, EventArgs e)
        {
        }

        protected void RadGridTerminals_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            this.Session["_termListForDistributor_PageSize"] = e.NewPageSize;
        }
    }
}