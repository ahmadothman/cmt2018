﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CustomerVNOTerminalDashboard.ascx.cs" Inherits="FOWebApp.Organizations.CustomerVNOTerminalDashboard" %>
<script type="text/javascript">
    $telerik.$(function () {
        var oWnd = radopen("CustomerVNOTerminalDashboard.aspx", "RadWindowDetails");
    });
        
</script>
<telerik:RadWindowManager ID="RadWindowManager1" runat="server" Skin="Metro">
        <Windows>
            <telerik:RadWindow ID="RadWindowDetails" runat="server" Modal="False" NavigateUrl="TerminalDetailsForm.aspx"
                Animation="Fade" Opacity="100" Skin="Metro" Title="Customer VNO dashboard" AutoSize="False"
                Width="800px" Height= "600px" KeepInScreenBounds="True" EnableShadow="True" EnableAriaSupport="True"
                VisibleStatusbar="True">
            </telerik:RadWindow>
        </Windows>
</telerik:RadWindowManager>