﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using Telerik.Web.UI;

namespace FOWebApp.VNO
{
    public partial class ServicePackListForVNO : System.Web.UI.UserControl
    {
        BOAccountingControlWS _boAccountingControlWS = null;
        public MembershipUser myObject;
        public string userId;
        public int distributorId;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            RadComboBoxISP.Items.Clear();
            _boAccountingControlWS = new BOAccountingControlWS();
            myObject = Membership.GetUser();
            userId = myObject.ProviderUserKey.ToString();
            distributorId = Convert.ToInt32(_boAccountingControlWS.GetDistributorForUser(userId).Id);
            Isp[] ispList = _boAccountingControlWS.GetISPsForDistributor(distributorId);
            RadComboBoxISP.DataValueField = "Id";
            RadComboBoxISP.DataTextField = "CompanyName";
            RadComboBoxISP.DataSource = ispList;
            RadComboBoxISP.DataBind();
            RadComboBoxISP.Items.Insert(0, new RadComboBoxItem(""));
        }

        protected void RadGridServicePacks_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            RadGridServicePacks.DataSource = _boAccountingControlWS.GetServicePacks();
        }

        protected void RadGridServicePacks_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.UpdateCommandName)
            {
                GridEditableItem editedItem = e.Item as GridEditableItem;
            }
        }

        protected void RadGridServicePacks_EditCommand(object sender, GridCommandEventArgs e)
        {
        }

        protected void RadGridServicePacks_UpdateCommand(object sender, GridCommandEventArgs e)
        {
        }

        protected void RadComboBoxISP_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            //Search Service packs for the given ISP
            RadGridServicePacks.Visible = true;
            int ispId = Int32.Parse(e.Value);
            ServiceLevel[] servicePacksForVNO = _boAccountingControlWS.GetServicePacksForVNO(distributorId);
            List<ServiceLevel> servicePacks = new List<ServiceLevel>();
            foreach (ServiceLevel sl in servicePacksForVNO)
            {
                if (sl.IspId == ispId)
                {
                    servicePacks.Add(sl);
                }
            }
            ServiceLevel[] servicePacksByIsp = _boAccountingControlWS.GetServicePacksForAllVNOsByISP(ispId);
            foreach (ServiceLevel sl2 in servicePacksByIsp)
            {
                servicePacks.Add(sl2);
            }
            RadGridServicePacks.DataSource = servicePacks;
            RadGridServicePacks.DataBind();
        }

        protected void RadGridServicePacks_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem dataItem = (GridDataItem)e.Item;

                if (dataItem["Hide"].Text.Equals("True"))
                {
                    dataItem["SlaId"].ForeColor = Color.Red;
                }
                else
                {
                    dataItem["SlaId"].ForeColor = Color.Green;
                }
            }

        }
    }
}