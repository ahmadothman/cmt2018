﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ServicePackUpdateFormVNO.aspx.cs" Inherits="FOWebApp.VNO.ServicePackUpdateFormVNO" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Modify ServicePack</title>
    <link href="../CMTStyle.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManagerServicePackUpdate" runat="server"></asp:ScriptManager>
    <div>
        <asp:PlaceHolder ID="PlaceHolderServicePackUpdateForm" runat="server"></asp:PlaceHolder>
    </div>
    </form>
</body>
</html>
