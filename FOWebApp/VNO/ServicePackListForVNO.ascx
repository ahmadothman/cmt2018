﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ServicePackListForVNO.ascx.cs" Inherits="FOWebApp.VNO.ServicePackListForVNO" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<style type="text/css">
    .auto-style1 {
        width: 92px;
    }
</style>
<script type="text/javascript">
    function RowSelected(sender, eventArgs) {
        //The master table has been clicked
        var masterTable = sender.get_masterTableView();
        var dataItems = masterTable.get_selectedItems();
        if (dataItems.length != 0) {
            var SlaId = dataItems[0].get_element().cells[0].innerHTML;
            var oWnd = radopen("Nocsa/ServicePackUpdateForm.aspx?id=" + SlaId, "RadWindowModifyServicePack");
        }
    }
</script>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxyServicePacks" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="RadComboBoxISP">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGridServicePacks" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<table>
    <tr>
        <td>
            <asp:Label ID="LabelISP" runat="server" Text="ISP:"></asp:Label>
        </td>
        <td>
            <telerik:RadComboBox ID="RadComboBoxISP" runat="server" Skin="Metro" Width="402px" OnSelectedIndexChanged="RadComboBoxISP_SelectedIndexChanged" AutoPostBack="True"></telerik:RadComboBox>
        </td>
    </tr>
</table>
<telerik:RadGrid ID="RadGridServicePacks" runat="server" AutoGenerateColumns="False"
    CellSpacing="0" GridLines="None" OnNeedDataSource="RadGridServicePacks_NeedDataSource"
    Skin="Metro" OnItemCommand="RadGridServicePacks_ItemCommand"
    OnEditCommand="RadGridServicePacks_EditCommand" Visible="False" OnItemDataBound="RadGridServicePacks_ItemDataBound">
    <ClientSettings EnableRowHoverStyle="true">
        <Selecting AllowRowSelect="True" />
        <ClientEvents OnRowSelected="RowSelected" />
    </ClientSettings>
    <MasterTableView AllowSorting="False" CommandItemDisplay="Top" DataKeyNames="SlaId" EditMode="EditForms">
        <CommandItemSettings ExportToPdfText="Export to PDF" ShowAddNewRecordButton="False"></CommandItemSettings>
        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
            <HeaderStyle Width="20px"></HeaderStyle>
        </RowIndicatorColumn>
        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
            <HeaderStyle Width="20px"></HeaderStyle>
        </ExpandCollapseColumn>
        <Columns>
            <telerik:GridBoundColumn DataField="SlaId" FilterControlAltText="Filter SlaId column"
                HeaderText="Id" UniqueName="SlaId">
                <ItemStyle ForeColor="Red" />
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="SlaName" FilterControlAltText="Filter SlaName column"
                HeaderText="Name" UniqueName="SlaName">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="FUPThreshold" FilterControlAltText="Filter FUPThreshold column"
                HeaderText="Forward FUP" UniqueName="FUPThreshold">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="RTNFUPThreshold" FilterControlAltText="Filter RTNFUPThreshold column"
                HeaderText="Return FUP" UniqueName="RTNFUPThreshold">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="DrAboveFup" FilterControlAltText="Filter DrAboveFup column"
                HeaderText="Speeds Above FUP" UniqueName="DrAboveFup">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Business" FilterControlAltText="Filter Business column"
                HeaderText="Business" UniqueName="Business" DataType="System.Boolean" DefaultInsertValue="false">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="FreeZone" FilterControlAltText="Filter FreeZone column"
                HeaderText="Free Zone" UniqueName="FreeZone" DataType="System.Boolean" DefaultInsertValue="false">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Voucher" FilterControlAltText="Filter Voucher column"
                HeaderText="Voucher" UniqueName="Voucher">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="VoiPFlag" FilterControlAltText="Filter VoiPFlag column"
                HeaderText="VOIP" UniqueName="VoiPFlag">
            </telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Hide" FilterControlAltText="Filter Hide column"
                HeaderText="Hide" UniqueName="Hide" Visible="False">
            </telerik:GridBoundColumn>
        </Columns>

        <EditFormSettings>
            <EditColumn UniqueName="EditCommandColumn1" FilterControlAltText="Filter EditCommandColumn1 column"></EditColumn>
        </EditFormSettings>
    </MasterTableView>
    <FilterMenu EnableImageSprites="False">
    </FilterMenu>
    <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default">
    </HeaderContextMenu>
</telerik:RadGrid>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Metro"></telerik:RadAjaxLoadingPanel>
<telerik:RadWindowManager ID="RadWindowManager1" runat="server" Skin="Metro">
    <Windows>
        <telerik:RadWindow ID="RadWindowModifyServicePack" runat="server" Animation="None"
            Opacity="100" Skin="Metro" Title="Modify Terminal Data" AutoSize="False" Width="800px"
            Height="700px" KeepInScreenBounds="True" EnableShadow="True" EnableAriaSupport="True"
            VisibleStatusbar="True" Modal="False" DestroyOnClose="False">
        </telerik:RadWindow>
    </Windows>
</telerik:RadWindowManager>
