﻿using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOConnectedDevicesControlWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;
using FOWebApp.net.nimera.cmt.BOMonitorControlWSRef;
using System;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using Telerik.Web.UI;

namespace FOWebApp
{
    public partial class TerminalDetailsForm : System.Web.UI.Page
    {
        BOAccountingControlWS _boAccountingControl = new BOAccountingControlWS();
        BOMonitorControlWS _boMonitoringControl = new BOMonitorControlWS();
        BOConnectedDevicesControlWS boconnectedDevicesControl = new BOConnectedDevicesControlWS();
        bool termChecked = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            string macAddress = Request.Params["mac"];

            // Check if the _macAddress belongs to the distributor
            MembershipUser user = Membership.GetUser();
            if (Roles.IsUserInRole("Distributor"))
            {
                Distributor dist = _boAccountingControl.GetDistributorForUser(user.ProviderUserKey.ToString());
                int distributorId = (int)dist.Id;

                try
                {
                    termChecked = _boAccountingControl.IsTermOwnedByDistributor(distributorId, macAddress);
                }
                catch (Exception ex)
                {
                    BOLogControlWS boLogControlWS = new BOLogControlWS();
                    CmtApplicationException cmtEx = new CmtApplicationException();
                    cmtEx.ExceptionDesc = ex.Message;
                    cmtEx.ExceptionStacktrace = ex.StackTrace;
                    boLogControlWS.LogApplicationException(cmtEx);
                    termChecked = false;
                }
            }
            else
            {
                termChecked = true;
            }

            // If termChecked is false i.e. the Distributor is trying to access data of a Terminal that does not belongs to him, the regular content of the tabs won't be loaded,
            // but in stead the TerminalNotFound.ascx, return; added so that the rest of the method is not executed.
            if (!termChecked)
            {
                Control controlTerminalNotFound = Page.LoadControl("TerminalNotFound.ascx");
                RadPageView selectedPageView = RadMultiPageTerminalView.PageViews[RadTabStripTerminalView.SelectedIndex];
                selectedPageView.Controls.Add(controlTerminalNotFound);
                return;
            }

            if (!IsPostBack)
            {
                // Read the passed macAddress and load the data

                LabelMacAddress.Text = macAddress;
                Terminal term = _boAccountingControl.GetTerminalDetailsByMAC(macAddress);

                // Hide the status tab for Service Class 3 (Corporate SLAs) (CMTFO-61)
                ServiceLevel sla = _boAccountingControl.GetServicePack((int)term.SlaId);

                RadPageViewTerminal.Controls.Clear();
                RadPageViewStatus.Controls.Clear();
                RadPageViewConsumption.Controls.Clear();
                RadPageViewManagement.Controls.Clear();
                RadPageViewAccumulated.Controls.Clear();
                RadPageViewActivities.Controls.Clear();
                RadPageViewConnectedDevice.Controls.Clear();
                RadPageViewNMSGraphs.Controls.Clear();
                RadPageViewNMSSpeedtest.Controls.Clear();
                RadPageViewGraphiteTest.Controls.Clear();
                RadPageViewTrafficClassification.Controls.Clear();

                if (sla.ServiceClass == 3 || sla.ServiceClass == 1)
                {
                    RadTabStatus.Visible = false;
                }

                // Hide volume and traffic tabs for SoHo-terminals
                //if (sla.ServiceClass == 1 && (Roles.IsUserInRole("Distributor") || Roles.IsUserInRole("Organization") || Roles.IsUserInRole("EndUser")))
                //{
                //    RadTabVolumes.Visible = false;
                //    RadTabTraffic.Visible = false;
                //}

                // Make the management tab only visible for Distributors
                if (HttpContext.Current.User.IsInRole("Distributor"))
                {
                    RadTabManagement.Visible = true;
                    RadTabActivities.Visible = true;
                    RadTabGraphiteTest.Visible = true;
                    RadTabTrafficClassification.Visible = true; // Under development
                    RadTabTrafficDetails.Visible = true;

                }

                // Make the TerminalNMSGraphs tab only visible for Corporate Terminals
                if (sla.ServiceClass == 3)
                {
                    RadTabNMSGraphs.Visible = true;
                }

                // Show the speedtest tab to EndUsers
                if (Roles.IsUserInRole("EndUser"))
                {
                    RadTabNMSSpeedtest.Visible = true;
                    RadTabTrafficClassification.Visible = true;// Under development
                }

                // Show the second terminal details tab for Satellite Diversity terminals
                if (term.RedundantSetup != null && (bool)term.RedundantSetup)
                {
                    RadTabTerminal2.Visible = true;
                }

                //While testing, the Graphite tab should only be shown to NOCSA
                if (Roles.IsUserInRole("NOC Administrator"))
                {
                    RadTabGraphiteTest.Visible = true;
                    RadTabTrafficClassification.Visible = true;// Under development
                    RadTabTrafficDetails.Visible = true;
                    
                }

                if (Roles.IsUserInRole("Organization"))
                {
                    RadTabGraphiteTest.Visible = true;
                    RadTabTrafficClassification.Visible = true;// Under development
                }
            }
            LoadSelectedTab(RadTabStripTerminalView.SelectedIndex);
        }

        private void LoadSelectedTab(int tabIndex)
        {
            switch (tabIndex)
            {
                case 0:
                    this.loadTerminalTab();
                    break;
                case 1:
                    this.loadTerminal2Tab();
                    break;
                case 2:
                    this.loadStatusTab();
                    break;
                case 3:
                    this.loadConsumedTab();
                    break;
                case 4:
                    this.loadAccumulatedTab();
                    break;
                case 5:
                    this.loadManagementTab();
                    break;
                case 6:
                    this.loadTerminalActivitiesTab(_boAccountingControl.GetTerminalDetailsByMAC(Request.QueryString["mac"]));
                    break;
                case 7:
                    this.LoadConnectedDeviceTab();
                    break;
                case 8:
                    this.LoadTerminalNMSGraphsTab();
                    break;
                case 9:
                    this.LoadTerminalNMSSpeedtestTab();
                    break;

                case 10:
                    this.LoadTerminalTrafficTab();
                    break;
                case 11:
                    LoadTrafficClassificationTab();
                    break;
                default:
                    this.loadTerminalTab();
                    break;
            }
        }

        protected void loadTerminalTab()
        {
            //Load the TerminalDetailTab control
            TerminalDetailTab terminalDetailTab =
                (TerminalDetailTab)Page.LoadControl("TerminalDetailTab.ascx");
            terminalDetailTab.MacAddress = LabelMacAddress.Text;
            RadPageViewTerminal.Controls.Add(terminalDetailTab);
        }

        protected void loadTerminal2Tab()
        {
            //Load the Terminal2DetailTab control, this tab re-uses the TerminalDetailTab.ascx and TerminalDetail.ascx
            Terminal term = _boAccountingControl.GetTerminalDetailsByMAC(LabelMacAddress.Text);
            string associatedMacAddress = term.AssociatedMacAddress;
            TerminalDetailTab terminalDetailTab =
                (TerminalDetailTab)Page.LoadControl("TerminalDetailTab.ascx");
            terminalDetailTab.MacAddress = associatedMacAddress; /*"00:06:39:83:c4:fe"*/;
            RadPageViewTerminal2.Controls.Add(terminalDetailTab);
        }


        protected void loadStatusTab()
        {
            TerminalStatusTab terminalStatusTab =
                (TerminalStatusTab)Page.LoadControl("TerminalStatusTab.ascx");
            terminalStatusTab.MacAddress = LabelMacAddress.Text;
            RadPageViewStatus.Controls.Add(terminalStatusTab);
        }

        protected void loadConsumedTab()
        {
            TerminalConsumedTab terminalConsumedTab =
                (TerminalConsumedTab)Page.LoadControl("TerminalConsumedTab.ascx");
            terminalConsumedTab.MacAddress = LabelMacAddress.Text;
            RadPageViewConsumption.Controls.Add(terminalConsumedTab);
        }

        protected void loadAccumulatedTab()
        {
            Terminal term = _boAccountingControl.GetTerminalDetailsByMAC(LabelMacAddress.Text);
            TerminalAccumulatedTab terminalAccumulatedTab =
                (TerminalAccumulatedTab)Page.LoadControl("TerminalAccumulatedTab.ascx");
            terminalAccumulatedTab.MacAddress = LabelMacAddress.Text;
            terminalAccumulatedTab.AssociatedMacAddress = term.AssociatedMacAddress;
            RadPageViewAccumulated.Controls.Add(terminalAccumulatedTab);
            //terminalAccumulatedTab.initTab();
        }

        protected void loadRTNAccumulatedTab()
        {
            TerminalRTNAccumulatedTab terminalRTNAccumulatedTab =
                (TerminalRTNAccumulatedTab)Page.LoadControl("TerminalRTNAccumulatedTab.ascx");
            terminalRTNAccumulatedTab.MacAddress = LabelMacAddress.Text;
            RadPageViewAccumulated.Controls.Add(terminalRTNAccumulatedTab);
        }

        protected void loadManagementTab()
        {
            TerminalManagementTab terminalManagementControl =
                (TerminalManagementTab)Page.LoadControl("TerminalManagementTab.ascx");
            terminalManagementControl.MacAddress = LabelMacAddress.Text;
            RadPageViewManagement.Controls.Add(terminalManagementControl);
        }

        protected void loadTerminalActivitiesTab(Terminal term)
        {
            TerminalActivityTab terminalActivityControl =
                (TerminalActivityTab)Page.LoadControl("TerminalActivityTab.ascx");
            terminalActivityControl.SitId = term.SitId;
            terminalActivityControl.IspId = term.IspId;
            terminalActivityControl.Height = 525;
            RadPageViewActivities.Controls.Add(terminalActivityControl);

        }

        protected void LoadConnectedDeviceTab()
        {
            ConnectedDevice[] cd;
            cd = boconnectedDevicesControl.GetConnectedDevicesForTerminalByType(LabelMacAddress.Text, 3);
            if (cd.Length>0)
             {
                TerminalTrafficDetailsTab terminalTrafficDetailsTab =
                    (TerminalTrafficDetailsTab)Page.LoadControl("TerminalTrafficDetailsTab.ascx");
                terminalTrafficDetailsTab.MacAddress = LabelMacAddress.Text;
                RadPageViewConnectedDevice.Controls.Add(terminalTrafficDetailsTab);
            }
        }

        protected void LoadTerminalNMSGraphsTab()
        {
            TerminalNMSGraphsTab terminalNMSGraphsTab =
                (TerminalNMSGraphsTab)Page.LoadControl("TerminalNMSGraphsTab.ascx");
            terminalNMSGraphsTab.MacAddress = LabelMacAddress.Text;
            RadPageViewNMSGraphs.Controls.Add(terminalNMSGraphsTab);

        }

        protected void LoadTerminalNMSSpeedtestTab()
        {
            TerminalNMSSpeedtestTab terminalNMSSpeedtestTab =
                (TerminalNMSSpeedtestTab)Page.LoadControl("TerminalNMSSpeedtestTab.ascx");
            terminalNMSSpeedtestTab.MacAddress = LabelMacAddress.Text;
            RadPageViewNMSSpeedtest.Controls.Add(terminalNMSSpeedtestTab);
        }

        protected void LoadTerminalTrafficTab()
        {
            TerminalTrafficTab graphiteTestTab =
                (TerminalTrafficTab)Page.LoadControl("TerminalTrafficTab.ascx");
            graphiteTestTab.MacAddress = LabelMacAddress.Text;
            RadPageViewGraphiteTest.Controls.Add(graphiteTestTab);
        }

        protected void LoadTrafficClassificationTab()
        {
            TrafficClassificationTab trafficClassificationTab = (TrafficClassificationTab)Page.LoadControl("TrafficClassificationTab.ascx");
            trafficClassificationTab.MacAddress = LabelMacAddress.Text;
            RadPageViewTrafficClassification.Controls.Add(trafficClassificationTab);
        }
    }
}