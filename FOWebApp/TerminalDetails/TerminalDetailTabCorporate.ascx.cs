﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FOWebApp.HelperMethods;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;

namespace FOWebApp.TerminalDetails
{
    public partial class TerminalDetailTabCorporate : TerminalDetailsBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BOAccountingControlWS _accountingController = new BOAccountingControlWS();
            Terminal cmtTerm = _accountingController.GetTerminalDetailsByMAC(TermInfo.MacAddress);
            LiteralTerminalName.Text = TermInfo.EndUserId;
            LiteralSitId.Text = TermInfo.SitId;
            LiteralIsp.Text = TerminalDetailsHelper.IspNameAndIdToString(TerminalIsp);
            LiteralMacAddress.Text = TermInfo.MacAddress;
            LiteralIpAddress.Text = TermInfo.IPAddress;
            LiteralSla.Text = TermInfo.SlaName;

            LabelOperationalStatus.Text = TermInfo.Status;
            LabelOperationalStatus.ForeColor = TerminalDetailsHelper.OperationalStatusColorValidator(TermInfo.Status);
            LabelAdministrativeStatus.Text = TerminalStatus;
            LabelAdministrativeStatus.ForeColor = TerminalDetailsHelper.AdministrativeStatusColorValidator(TerminalStatus);

            LiteralForwardSpeed.Text = string.Format("{0} Kbps", Sla.DRFWD);
            LiteralReturnSpeed.Text = string.Format("{0} Kbps", Sla.DRRTN);
            LiteralCirForward.Text = string.Format("{0} Kbps", Sla.CIRFWD);
            LiteralCirReturn.Text = string.Format("{0} Kbps", Sla.CIRRTN);

            LiteralConsumed.Text = string.Format("{0:0.000} GB", (long.Parse(TermInfo.Consumed) + long.Parse(TermInfo.ConsumedReturn)) / (decimal)1000000000.0);

            Color labelTerminalPointingForeColor;
            string labelTerminalPointingToolTip;
            //Terminal pointing Es/No:
            if (cmtTerm.Modem!=null && cmtTerm.Modem.Substring(0,2) == "iD")
            {
                LabelTerminalPointingName.Text = "Tx Power";
                LabelTerminalPointing.Text = TerminalDetailsHelper.GetCnoValueToString(TermInfo.RTN, Sla.DRRTN, Culture, Term.IspId, out labelTerminalPointingForeColor, out labelTerminalPointingToolTip)+"dBm";
            }
            else
            {
                if (cmtTerm.DialogTechnology!=null && cmtTerm.DialogTechnology == "MxDMA")
                {
                    LabelTerminalPointingName.Text = "HUB SNR:";
                    LabelTerminalPointing.Text = TerminalDetailsHelper.GetCnoValueToString(TermInfo.RTN, Sla.DRRTN, Culture, Term.IspId, out labelTerminalPointingForeColor, out labelTerminalPointingToolTip) + "dB";
                }
                else
                {
                    LabelTerminalPointingName.Text = "Terminal pointing C / No:";
                    LabelTerminalPointing.Text = TerminalDetailsHelper.GetCnoValueToString(TermInfo.RTN, Sla.DRRTN, Culture, Term.IspId, out labelTerminalPointingForeColor, out labelTerminalPointingToolTip) + "dBHz";
                }
            }
            LabelTerminalPointing.ForeColor = labelTerminalPointingForeColor;
            LabelTerminalPointing.ToolTip = labelTerminalPointingToolTip;
            Color labelTerminalPointingFWDForeColor;
            string labelTerminalPointingFWDToolTip;

            //Terminal pointing C / No:
            if (cmtTerm.Modem!=null && cmtTerm.Modem.Substring(0, 2) == "iD")
            {
                LabelTerminalPointingFWDName.Text = "Rx SNR";
                LabelTerminalPointingFWD.Text = TerminalDetailsHelper.GetEsNoValueToString(TermInfo.FWD, Culture, Term.IspId, out labelTerminalPointingFWDForeColor, out labelTerminalPointingFWDToolTip)+"dB";
            }
            else
            {
                
                    LabelTerminalPointingFWDName.Text = "Terminal pointing Es/No";
                    LabelTerminalPointingFWD.Text = TerminalDetailsHelper.GetEsNoValueToString(TermInfo.FWD, Culture, Term.IspId, out labelTerminalPointingFWDForeColor, out labelTerminalPointingFWDToolTip)+"dB";
                
            }
            LabelTerminalPointingFWD.ForeColor = labelTerminalPointingFWDForeColor;
            LabelTerminalPointingFWD.ToolTip = labelTerminalPointingFWDToolTip;

            //SATCORP-48 & 49
            if (!RedundantSetup)
            {
                LiteralSatDiv.Text = "This Terminal is not a part of a Redundant Setup";
            }
            else
            {
                LiteralSatDiv.Text = IsSatDivTermActive ? "Active" : "Inactive";
            }

            //Color labelExpiryDateForeColor;
            LabelExpiryDate.Text = "N/A"; //TerminalDetailsHelper.GetExpiryDateToString(TermInfo.ExpiryDate, out labelExpiryDateForeColor);
            //LabelExpiryDate.ForeColor = labelExpiryDateForeColor;

            LabelModem.Text = Term.Modem;
            LabelILNBBUC.Text = Term.iLNBBUC;
            LabelAntenna.Text = Term.Antenna;
        }
    }
}