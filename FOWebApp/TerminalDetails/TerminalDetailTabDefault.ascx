﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TerminalDetailTabDefault.ascx.cs" Inherits="FOWebApp.TerminalDetails.TerminalDetailTabDefault" %>

<div>
    <fieldset>
        <legend>Terminal details</legend>
        <table class="termDetails" style="border: none; border-spacing: 5px; border-collapse: separate; width: 550px;">
            <tr>
                <td style="width: 40%;">Terminal:</td>
                <td style="width: 60%">
                    <asp:Literal ID="LiteralTerminalName" runat="server"></asp:Literal></td>
            </tr>
            <tr>
                <td>SIT ID:</td>
                <td>
                    <asp:Literal ID="LiteralSitId" runat="server"></asp:Literal></td>
            </tr>
            <tr>
                <td>ISP:</td>
                <td>
                    <asp:Literal ID="LiteralIsp" runat="server"></asp:Literal></td>
            </tr>
            <tr>
                <td>MAC address:</td>
                <td>
                    <asp:Literal ID="LiteralMacAddress" runat="server"></asp:Literal></td>
            </tr>
            <tr>
                <td>IP address:</td>
                <td>
                    <asp:Literal ID="LiteralIpAddress" runat="server"></asp:Literal></td>
            </tr>
            <tr>
                <td>SLA:</td>
                <td>
                    <asp:Literal ID="LiteralSla" runat="server"></asp:Literal></td>
            </tr>
            <tr>
                <td>Operational status:</td>
                <td style="font-weight: bold;">
                    <asp:Label ID="LabelOperationalStatus" runat="server" ToolTip="When 'Not operational', this can be due to bad pointing, loose connections or other problems on the local or satellite network."></asp:Label></td>
            </tr>
            <tr>
                <td>Administrative status:</td>
                <td style="font-weight: bold;">
                    <asp:Label ID="LabelAdministrativeStatus" runat="server" ToolTip="Indicates the administrative status of a terminal (locked, unlocked, ...)."></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Literal ID="LiteralTitleFwdAllocation" runat="server" Text="Forward volume allocation:"></asp:Literal>
                </td>
                <td>
                    <asp:Literal ID="LiteralFwdAllocation" runat="server"></asp:Literal>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Literal ID="LiteralTitleRtnAllocation" runat="server" Text="Return volume allocation:"></asp:Literal>
                </td>
                <td>
                    <asp:Literal ID="LiteralRtnAllocation" runat="server"></asp:Literal>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Literal ID="LiteralTitleFwdConsumed" runat="server" Text="Forward volume consumed:"></asp:Literal>
                </td>
                <td>
                    <asp:Label ID="LabelFwdConsumed" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Literal ID="LiteralTitleRtnConsumed" runat="server" Text="Return volume consumed:"></asp:Literal>
                </td>
                <td>
                    <asp:Label ID="LabelRtnConsumed" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Literal ID="LiteralTitleFwdRemaining" runat="server" Text="Forward volume remaining:" Visible="false"></asp:Literal>
                </td>
                <td>
                    <asp:Literal ID="LiteralFwdRemaining" runat="server" Visible="false"></asp:Literal>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Literal ID="LiteralTitleRtnRemaining" runat="server" Text="Return volume remaining:" Visible="false"></asp:Literal>
                </td>
                <td>
                    <asp:Literal ID="LiteralRtnRemaining" runat="server" Visible="false"></asp:Literal>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Literal ID="LiteralTitleFupResetDay" runat="server" Text="FUP Reset Day:"></asp:Literal>
                </td>
                <td style="color: #008000; font-weight: bold;">
                    <asp:Label ID="LabelFupResetDay" runat="server" ToolTip="Day of the month on which volume reset takes place."></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Terminal pointing Es/No:</td>
                <td style="font-weight: bold;">
                    <asp:Label ID="LabelTerminalPointingFWD" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td>Terminal pointing C/No:</td>
                <td style="font-weight: bold;">
                    <asp:Label ID="LabelTerminalPointing" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td>Expiry date:
                </td>
                <td>
                    <asp:Label ID="LabelExpiryDate" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Modem:
                </td>
                <td>
                    <asp:Label ID="LabelModem" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>iLNB/BUC:
                </td>
                <td>
                    <asp:Label ID="LabelILNBBUC" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Antenna:
                </td>
                <td>
                    <asp:Label ID="LabelAntenna" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>SatDiv:</td>
                <td>
                    <asp:Literal ID="LiteralSatDiv" runat="server"></asp:Literal></td>
            </tr>
        </table>
    </fieldset>
</div>
<asp:Panel ID="PanelFreeZoneDetails" runat="server" BackColor="#F1FEF3" GroupingText="Freezone details" Visible="false">
    <table class="termDetails" style="border: none; border-spacing: 5px; border-collapse: separate; width: 550px;">
        <tr>
            <td style="width: 40%;">Forward freezone consumed:
            </td>
            <td style="width: 60%;">
                <asp:Literal ID="LiteralFwdFreeZoneVolume" runat="server"></asp:Literal>
            </td>
        </tr>
        <tr>
            <td>Return freezone consumed:
            </td>
            <td>
                <asp:Literal ID="LiteralRtnFreeZoneVolume" runat="server"></asp:Literal>
            </td>
        </tr>
    </table>
</asp:Panel>
