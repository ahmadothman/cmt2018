﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOMonitorControlWSRef;

namespace FOWebApp.TerminalDetails
{
    public abstract class TerminalDetailsBase : System.Web.UI.UserControl
    {
        public Terminal Term { get; set; }
        public TerminalInfo TermInfo { get; set; }
        public string TerminalStatus { get; set; }
        public ServiceLevel Sla { get; set; }
        public Isp TerminalIsp { get; set; }
        public CultureInfo Culture { get; set; }
        public bool RedundantSetup { get; set; }
        public bool IsSatDivTermActive { get; set; }

        /// <summary>
        /// Initializes the TerminalDetailTab user controls with the required properties
        /// </summary>
        /// <param name="terminal">The terminal object</param>
        /// <param name="terminalInfo">The terminalInfo object</param>
        /// <param name="terminalStatus">The administrative status of the terminal as string</param>
        /// <param name="sla">The service level agreement of the terminal</param>
        /// <param name="isp">The internet service provider of the terminal</param>
        public virtual void Initialize(Terminal terminal, TerminalInfo terminalInfo, string terminalStatus, ServiceLevel sla, Isp isp, CultureInfo culture, bool redundantSetup, bool isSatDivTermActive)
        {
            Term = terminal;
            TermInfo = terminalInfo;
            TerminalStatus = terminalStatus;
            Sla = sla;
            TerminalIsp = isp;
            Culture = culture;
            RedundantSetup = redundantSetup;
            IsSatDivTermActive = isSatDivTermActive;
        }
    }
}