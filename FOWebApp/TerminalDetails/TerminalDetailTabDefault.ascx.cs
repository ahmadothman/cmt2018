﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using FOWebApp.HelperMethods;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOMonitorControlWSRef;

namespace FOWebApp.TerminalDetails
{
    public partial class TerminalDetailTabDefault : TerminalDetailsBase
    {
        private BOAccountingControlWS _boAccountingControl = new BOAccountingControlWS();
        private bool _sohoFlag = false;
        private bool _freeZoneFlag;
        private bool _voucherFlag;

        public override void Initialize(Terminal terminal, TerminalInfo terminalInfo, string terminalStatus, ServiceLevel sla, Isp isp, CultureInfo culture, bool redundantSetup, bool isSatDivTermActive)
        {
            base.Initialize(terminal, terminalInfo, terminalStatus, sla, isp, culture, redundantSetup, isSatDivTermActive);

            if (TerminalIsp.Id == 473)
            {
                _sohoFlag = true;
            }

            _freeZoneFlag = _boAccountingControl.IsFreeZoneSLA(int.Parse(TermInfo.SlaId));
            _voucherFlag = _boAccountingControl.IsVoucherSLA(int.Parse(TermInfo.SlaId));
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            LiteralTerminalName.Text = TermInfo.EndUserId;
            LiteralSitId.Text = TermInfo.SitId;
            LiteralIsp.Text = TerminalDetailsHelper.IspNameAndIdToString(TerminalIsp);
            LiteralMacAddress.Text = TermInfo.MacAddress;
            LiteralIpAddress.Text = TermInfo.IPAddress;
            LiteralSla.Text = TermInfo.SlaName;

            LabelOperationalStatus.Text = TermInfo.Status;
            LabelOperationalStatus.ForeColor = TerminalDetailsHelper.OperationalStatusColorValidator(TermInfo.Status);
            LabelAdministrativeStatus.Text = TerminalStatus;
            LabelAdministrativeStatus.ForeColor = TerminalDetailsHelper.AdministrativeStatusColorValidator(TerminalStatus);

            decimal fwdVolumeAllocation = TerminalDetailsHelper.CalculateVolumeAllocation(TermInfo, true);
            LiteralFwdAllocation.Text = (fwdVolumeAllocation != 0m) ? string.Format("{0:0.000} GB", fwdVolumeAllocation) : "N.A. (Not Applicable)";
            decimal rtnVolumeAllocation = TerminalDetailsHelper.CalculateVolumeAllocation(TermInfo, false);
            LiteralRtnAllocation.Text = (rtnVolumeAllocation != 0m) ? string.Format("{0:0.000} GB", fwdVolumeAllocation) : "N.A. (Not Applicable)";

            decimal fwdVolumeConsumed = TerminalDetailsHelper.CalculateVolumeConsumption(TermInfo, true);
            LabelFwdConsumed.Text = (fwdVolumeConsumed != 0m) ? string.Format("{0:0.000} GB", fwdVolumeConsumed) : "N.A. (Not Applicable)";
            if (fwdVolumeConsumed > fwdVolumeAllocation)
            {
                LabelFwdConsumed.ForeColor = Color.Red;
            }

            decimal rtnVolumeConsumed = TerminalDetailsHelper.CalculateVolumeConsumption(TermInfo, false);
            LabelRtnConsumed.Text = (rtnVolumeConsumed != 0m) ? string.Format("{0:0.000} GB", rtnVolumeConsumed) : "N.A. (Not Applicable)";
            if (rtnVolumeConsumed > rtnVolumeAllocation)
            {
                LabelRtnConsumed.ForeColor = Color.Red;
            }

            LabelFupResetDay.Text = TermInfo.InvoiceDayOfMonth;

            Color labelTerminalPointingForeColor;
            string labelTerminalPointingToolTip;
            LabelTerminalPointing.Text = TerminalDetailsHelper.GetCnoValueToString(TermInfo.RTN, Sla.DRRTN, Culture, Term.IspId, out labelTerminalPointingForeColor, out labelTerminalPointingToolTip)+"dBHz";
            LabelTerminalPointing.ForeColor = labelTerminalPointingForeColor;
            LabelTerminalPointing.ToolTip = labelTerminalPointingToolTip;
            Color labelTerminalPointingFWDForeColor;
            string labelTerminalPointingFWDToolTip;
            LabelTerminalPointingFWD.Text = TerminalDetailsHelper.GetEsNoValueToString(TermInfo.FWD, Culture, Term.IspId, out labelTerminalPointingFWDForeColor, out labelTerminalPointingFWDToolTip) + "dB";
            LabelTerminalPointingFWD.ForeColor = labelTerminalPointingFWDForeColor;
            LabelTerminalPointingFWD.ToolTip = labelTerminalPointingFWDToolTip;
            Color labelExpiryDateForeColor;
            DateTime notApplicable = new DateTime(1, 1, 1);
            if (TermInfo.ExpiryDate == notApplicable)
            {
                LabelExpiryDate.Text = "Not set";
            }
            else
            {
                LabelExpiryDate.Text = TerminalDetailsHelper.GetExpiryDateToString(TermInfo.ExpiryDate, out labelExpiryDateForeColor);
                LabelExpiryDate.ForeColor = labelExpiryDateForeColor;
            }
            LabelModem.Text = Term.Modem;
            LabelILNBBUC.Text = Term.iLNBBUC;
            LabelAntenna.Text = Term.Antenna;

            // Calculate and display volume percentage consumed
            if (!_sohoFlag)
            {
                // Avoid divide by zero and display text (FWD)
                if (fwdVolumeAllocation != 0m)
                {
                    decimal fwdVolumeConsumedPercent = (fwdVolumeConsumed / fwdVolumeAllocation) * (decimal)100.0;
                    LabelFwdConsumed.Text += string.Format(" ({0:0.00} %)", fwdVolumeConsumedPercent);
                }
                else if (_voucherFlag)
                {
                    LabelFwdConsumed.Text += " (Voucher-based)";
                }
                else
                {
                    LabelFwdConsumed.Text += " (Unlimited)";
                }

                // Avoid divide by zero and display text (RTN)
                if (rtnVolumeAllocation != 0m)
                {
                    decimal rtnVolumeConsumedPercent = (rtnVolumeConsumed / rtnVolumeAllocation) * (decimal)100.0;
                    LabelRtnConsumed.Text += string.Format(" ({0:0.00} %)", rtnVolumeConsumedPercent);
                }
                else if (_voucherFlag)
                {
                    LabelRtnConsumed.Text += " (Voucher-based)";
                }
                else
                {
                    LabelRtnConsumed.Text += " (Unlimited)";
                }
            }

            // Set freezone display
            if (_freeZoneFlag)
            {
                LiteralFwdFreeZoneVolume.Text = string.Format("{0:0.000} GB", long.Parse(TermInfo.FreeZone) / (decimal)1000000000.0);
                LiteralRtnFreeZoneVolume.Text = string.Format("{0:0.000} GB", long.Parse(TermInfo.ConsumedFreeZone) / (decimal)1000000000.0);
                PanelFreeZoneDetails.Visible = true;
            }

            // Set SoHo-specific display
            if (_sohoFlag)
            {
                LiteralFwdRemaining.Text = string.Format("{0:0.000} GB", fwdVolumeAllocation - fwdVolumeConsumed);
                LiteralTitleFwdRemaining.Visible = true;
                LiteralFwdRemaining.Visible = true;

                LiteralRtnRemaining.Text = string.Format("{0:0.000} GB", rtnVolumeAllocation - rtnVolumeConsumed);
                LiteralTitleRtnRemaining.Visible = true;
                LiteralRtnRemaining.Visible = true;

                // Hide other volume information for distributors, customers and end-user
                if (Roles.IsUserInRole("Distributor") || Roles.IsUserInRole("Organization") || Roles.IsUserInRole("EndUser"))
                {
                    LiteralTitleFwdAllocation.Visible = false;
                    LiteralFwdAllocation.Visible = false;
                    LiteralTitleRtnAllocation.Visible = false;
                    LiteralRtnAllocation.Visible = false;

                    LiteralTitleFwdConsumed.Visible = false;
                    LabelFwdConsumed.Visible = false;
                    LiteralTitleRtnConsumed.Visible = false;
                    LabelRtnConsumed.Visible = false;

                    LiteralTitleFupResetDay.Visible = false;
                    LabelFupResetDay.Visible = false;
                }
            }

            //SATCORP-48 & 49
            if (!RedundantSetup)
            {
                LiteralSatDiv.Text = "This Terminal is not a part of a Redundant Setup";
            }
            else
            {
                LiteralSatDiv.Text = IsSatDivTermActive ? "Active" : "Inactive";
            }
        }
    }
}