﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FOWebApp.HelperMethods;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;

namespace FOWebApp.TerminalDetails
{
    public partial class TerminalDetailTabBusiness : TerminalDetailsBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BOAccountingControlWS _accountingController = new BOAccountingControlWS();
            Terminal cmtTerm = _accountingController.GetTerminalDetailsByMAC(TermInfo.MacAddress);
            LiteralTerminalName.Text = TermInfo.EndUserId;
            LiteralSitId.Text = TermInfo.SitId;
            LiteralIsp.Text = TerminalDetailsHelper.IspNameAndIdToString(TerminalIsp);
            LiteralMacAddress.Text = TermInfo.MacAddress;
            LiteralIpAddress.Text = TermInfo.IPAddress;
            LiteralSla.Text = TermInfo.SlaName;

            LabelOperationalStatus.Text = TermInfo.Status;
            LabelOperationalStatus.ForeColor = TerminalDetailsHelper.OperationalStatusColorValidator(TermInfo.Status);
            LabelAdministrativeStatus.Text = TerminalStatus;
            LabelAdministrativeStatus.ForeColor = TerminalDetailsHelper.AdministrativeStatusColorValidator(TerminalStatus);

            decimal volumeAllocated = Sla.LowVolumeSUM ?? 0;
            LiteralAllocation.Text = string.Format("{0:0.000} GB", volumeAllocated);

            Color labelConsumedForeColor;
            LabelConsumed.Text = CalculateBusinessConsumedVolume(long.Parse(TermInfo.Consumed), long.Parse(TermInfo.ConsumedReturn), volumeAllocated, out labelConsumedForeColor);
            LabelConsumed.ForeColor = labelConsumedForeColor;

            LabelFupResetDay.Text = TermInfo.InvoiceDayOfMonth;

            Color labelTerminalPointingForeColor;
            string labelTerminalPointingToolTip;
            if (cmtTerm.Modem != null && cmtTerm.Modem.Substring(0, 2) == "iD")
            {
                LabelTerminalPointingName.Text = "Tx Power";
                LabelTerminalPointing.Text = TerminalDetailsHelper.GetCnoValueToString(TermInfo.RTN, Sla.DRRTN, Culture, Term.IspId, out labelTerminalPointingForeColor, out labelTerminalPointingToolTip) + "dBm";
            }
            else
            {
                if (cmtTerm.DialogTechnology != null && cmtTerm.DialogTechnology == "MxDMA")
                {
                    LabelTerminalPointingName.Text = "HUB SNR:";
                    LabelTerminalPointing.Text = TerminalDetailsHelper.GetCnoValueToString(TermInfo.RTN, Sla.DRRTN, Culture, Term.IspId, out labelTerminalPointingForeColor, out labelTerminalPointingToolTip) + "dB";
                }
                else
                {
                    LabelTerminalPointingName.Text = "Terminal pointing C / No:";
                    LabelTerminalPointing.Text = TerminalDetailsHelper.GetCnoValueToString(TermInfo.RTN, Sla.DRRTN, Culture, Term.IspId, out labelTerminalPointingForeColor, out labelTerminalPointingToolTip) + "dBHz";
                }
            }
            LabelTerminalPointing.ForeColor = labelTerminalPointingForeColor;
            LabelTerminalPointing.ToolTip = labelTerminalPointingToolTip;
            Color labelTerminalPointingFWDForeColor;
            string labelTerminalPointingFWDToolTip;

            if (cmtTerm.Modem != null && cmtTerm.Modem.Substring(0, 2) == "iD")
            {
                LabelTerminalPointingFWDName.Text = "Rx SNR";
                LabelTerminalPointingFWD.Text = TerminalDetailsHelper.GetEsNoValueToString(TermInfo.FWD, Culture, Term.IspId, out labelTerminalPointingFWDForeColor, out labelTerminalPointingFWDToolTip) + "dB";
            }
            else
            {

                LabelTerminalPointingFWDName.Text = "Terminal pointing Es/No";
                LabelTerminalPointingFWD.Text = TerminalDetailsHelper.GetEsNoValueToString(TermInfo.FWD, Culture, Term.IspId, out labelTerminalPointingFWDForeColor, out labelTerminalPointingFWDToolTip) + "dB";

            }

            LabelTerminalPointingFWD.ForeColor = labelTerminalPointingFWDForeColor;
            LabelTerminalPointingFWD.ToolTip = labelTerminalPointingFWDToolTip;
            Color labelExpiryDateForeColor;
            DateTime notApplicable = new DateTime(1, 1, 1);
            if (TermInfo.ExpiryDate == notApplicable)
            {
                LabelExpiryDate.Text = "Not applicable";
            }
            else
            {
                LabelExpiryDate.Text = TerminalDetailsHelper.GetExpiryDateToString(TermInfo.ExpiryDate, out labelExpiryDateForeColor);
                LabelExpiryDate.ForeColor = labelExpiryDateForeColor;
            }
            LabelModem.Text = Term.Modem;
            LabelILNBBUC.Text = Term.iLNBBUC;
            LabelAntenna.Text = Term.Antenna;

            if (Sla.FreeZone == true)
            {
                LiteralFreeZoneVolume.Text = string.Format("{0:0.000} GB", (long.Parse(TermInfo.FreeZone) + long.Parse(TermInfo.ConsumedFreeZone)) / (decimal)1000000000.0);
                PanelFreeZoneDetails.Visible = true;
            }

            //SATCORP-48 & 49
            if (!RedundantSetup)
            {
                LiteralSatDiv.Text = "This Terminal is not a part of a Redundant Setup";
            }
            else
            {
                LiteralSatDiv.Text = IsSatDivTermActive ? "Active" : "Inactive";
            }
        }

        /// <summary>
        /// Calculates the consumed volume of a business SLA
        /// </summary>
        /// <param name="forwardConsumed">The forward volume that has been consumed</param>
        /// <param name="returnConsumed">The return volume that has been consumed</param>
        /// <param name="volumeAllocated">The total allocated volume</param>
        /// <param name="foreColor">Red when the consumed volume is larger than the allocated volume</param>
        /// <returns>The string representation of the total consumed volume</returns>
        private string CalculateBusinessConsumedVolume(long forwardConsumed, long returnConsumed, decimal volumeAllocated, out Color foreColor) 
        {
            decimal volumeConsumed = (forwardConsumed + returnConsumed) / (decimal)1000000000.0;

            foreColor = Color.Empty;

            if (volumeConsumed > volumeAllocated)
            {
                foreColor = Color.Red;
            }

            if (volumeAllocated > 0)
            {
                decimal consumedPercent = (volumeConsumed / volumeAllocated) * (decimal)100.0;
                return string.Format("{0:0.000} GB ({1:0.00} %)", volumeConsumed, consumedPercent);
            }
            else
            {
                return string.Format("{0:0.000} GB", volumeConsumed);
            }
        }
    }
}