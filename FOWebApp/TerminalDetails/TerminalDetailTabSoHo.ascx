﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TerminalDetailTabSoHo.ascx.cs" Inherits="FOWebApp.TerminalDetails.TerminalDetailTabSoHo" %>

<div>
    <fieldset>
        <legend>Terminal details</legend>
        <table class="termDetails" style="border: none; border-spacing: 5px; border-collapse: separate; width: 550px;">
            <tr>
                <td style="width: 40%;">Terminal:</td>
                <td style="width: 60%">
                    <asp:Literal ID="LiteralTerminalName" runat="server"></asp:Literal></td>
            </tr>
            <tr>
                <td>SIT ID:</td>
                <td>
                    <asp:Literal ID="LiteralSitId" runat="server"></asp:Literal></td>
            </tr>
            <tr>
                <td>ISP:</td>
                <td>
                    <asp:Literal ID="LiteralIsp" runat="server"></asp:Literal></td>
            </tr>
            <tr>
                <td>MAC address:</td>
                <td>
                    <asp:Literal ID="LiteralMacAddress" runat="server"></asp:Literal></td>
            </tr>
            <tr>
                <td>IP address:</td>
                <td>
                    <asp:Literal ID="LiteralIpAddress" runat="server"></asp:Literal></td>
            </tr>
            <tr>
                <td>SLA:</td>
                <td>
                    <asp:Literal ID="LiteralSla" runat="server"></asp:Literal></td>
            </tr>
            <tr>
                <td>Operational status:</td>
                <td style="font-weight: bold;">
                    <asp:Label ID="LabelOperationalStatus" runat="server" ToolTip="When 'Not operational', this can be due to bad pointing, loose connections or other problems on the local or satellite network."></asp:Label></td>
            </tr>
            <tr>
                <td>Administrative status:</td>
                <td style="font-weight: bold;">
                    <asp:Label ID="LabelAdministrativeStatus" runat="server" ToolTip="Indicates the administrative status of a terminal (locked, unlocked, ...)."></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Remaining volume:
                </td>
                <td>
                    <asp:Literal ID="LiteralRemaining" runat="server"></asp:Literal>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="LabelTerminalPointingFWDName" runat="server"></asp:Label></td>
                <td style="font-weight: bold;">
                    <asp:Label ID="LabelTerminalPointingFWD" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="LabelTerminalPointingName" runat="server"></asp:Label></td>
                <td style="font-weight: bold;">
                    <asp:Label ID="LabelTerminalPointing" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="LabelExpiryDateType" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="LabelExpiryDate" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Modem:
                </td>
                <td>
                    <asp:Label ID="LabelModem" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>iLNB/BUC:
                </td>
                <td>
                    <asp:Label ID="LabelILNBBUC" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Antenna:
                </td>
                <td>
                    <asp:Label ID="LabelAntenna" runat="server"></asp:Label>
                </td>
            </tr>
             <tr>
                <td>SatDiv:</td>
                <td>
                    <asp:Literal ID="LiteralSatDiv" runat="server"></asp:Literal></td>
            </tr>
        </table>
    </fieldset>
</div>
