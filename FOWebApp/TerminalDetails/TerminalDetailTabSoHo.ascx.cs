﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FOWebApp.HelperMethods;
using FOWebApp.net.nimera.cmt.BOMonitorControlWSRef;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;

namespace FOWebApp.TerminalDetails
{
    public partial class TerminalDetailTabSoHo : TerminalDetailsBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BOAccountingControlWS _accountingController = new BOAccountingControlWS();
            Terminal cmtTerm = _accountingController.GetTerminalDetailsByMAC(TermInfo.MacAddress);
            LiteralTerminalName.Text = TermInfo.EndUserId;
            LiteralSitId.Text = TermInfo.SitId;
            LiteralIsp.Text = TerminalDetailsHelper.IspNameAndIdToString(TerminalIsp);
            LiteralMacAddress.Text = TermInfo.MacAddress;
            LiteralIpAddress.Text = TermInfo.IPAddress;
            LiteralSla.Text = TermInfo.SlaName;

            LabelOperationalStatus.Text = TermInfo.Status;
            LabelOperationalStatus.ForeColor = TerminalDetailsHelper.OperationalStatusColorValidator(TermInfo.Status);
            LabelAdministrativeStatus.Text = TerminalStatus;
            LabelAdministrativeStatus.ForeColor = TerminalDetailsHelper.AdministrativeStatusColorValidator(TerminalStatus);

            LiteralRemaining.Text = CalculateSoHoRemainingVolume(TermInfo);

            if (Sla.ServiceClass == 1)
                LabelExpiryDateType.Text = "Volume Expiry Date :";
            else if (Sla.ServiceClass==7)
                LabelExpiryDateType.Text = "Volume Reset Date :";
            Color labelTerminalPointingForeColor;
            string labelTerminalPointingToolTip;

            if (cmtTerm.Modem != null && cmtTerm.Modem.Substring(0, 2) == "iD")
            {
                LabelTerminalPointingName.Text = "Tx Power";
                LabelTerminalPointing.Text = TerminalDetailsHelper.GetCnoValueToString(TermInfo.RTN, Sla.DRRTN, Culture, Term.IspId, out labelTerminalPointingForeColor, out labelTerminalPointingToolTip) + "dBm";
            }
            else
            {
                if (cmtTerm.DialogTechnology != null && cmtTerm.DialogTechnology == "MxDMA")
                {
                    LabelTerminalPointingName.Text = "HUB SNR:";
                    LabelTerminalPointing.Text = TerminalDetailsHelper.GetCnoValueToString(TermInfo.RTN, Sla.DRRTN, Culture, Term.IspId, out labelTerminalPointingForeColor, out labelTerminalPointingToolTip) + "dB";
                }
                else
                {
                    LabelTerminalPointingName.Text = "Terminal pointing C / No:";
                    LabelTerminalPointing.Text = TerminalDetailsHelper.GetCnoValueToString(TermInfo.RTN, Sla.DRRTN, Culture, Term.IspId, out labelTerminalPointingForeColor, out labelTerminalPointingToolTip) + "dBHz";
                }
            }

            LabelTerminalPointing.ForeColor = labelTerminalPointingForeColor;
            LabelTerminalPointing.ToolTip = labelTerminalPointingToolTip;
            Color labelTerminalPointingFWDForeColor;
            string labelTerminalPointingFWDToolTip;
            if (cmtTerm.Modem != null && cmtTerm.Modem.Substring(0, 2) == "iD")
            {
                LabelTerminalPointingFWDName.Text = "Rx SNR";
                LabelTerminalPointingFWD.Text = TerminalDetailsHelper.GetEsNoValueToString(TermInfo.FWD, Culture, Term.IspId, out labelTerminalPointingFWDForeColor, out labelTerminalPointingFWDToolTip) + "dB";
            }
            else
            {

                LabelTerminalPointingFWDName.Text = "Terminal pointing Es/No";
                LabelTerminalPointingFWD.Text = TerminalDetailsHelper.GetEsNoValueToString(TermInfo.FWD, Culture, Term.IspId, out labelTerminalPointingFWDForeColor, out labelTerminalPointingFWDToolTip) + "dB";

            }

            LabelTerminalPointingFWD.ForeColor = labelTerminalPointingFWDForeColor;
            LabelTerminalPointingFWD.ToolTip = labelTerminalPointingFWDToolTip;

            Color labelExpiryDateForeColor;
            if (TermInfo.ExpiryDate.Year == 2100)
                LabelExpiryDate.Text = "N/A";
            else if (TermInfo.ExpiryDate.Year > DateTime.UtcNow.AddYears(99).Year)
            {
                LabelExpiryDate.Text = TerminalDetailsHelper.GetExpiryDateToString(TermInfo.ExpiryDate.AddYears(-100), out labelExpiryDateForeColor);
                LabelExpiryDate.ForeColor = labelExpiryDateForeColor;
            }
            else
            {
                LabelExpiryDate.Text = TerminalDetailsHelper.GetExpiryDateToString(TermInfo.ExpiryDate, out labelExpiryDateForeColor);
                LabelExpiryDate.ForeColor = labelExpiryDateForeColor;
            }
            LabelModem.Text = Term.Modem;
            LabelILNBBUC.Text = Term.iLNBBUC;
            LabelAntenna.Text = Term.Antenna;

            //SATCORP-48 & 49
            if (!RedundantSetup)
            {
                LiteralSatDiv.Text = "This Terminal is not a part of a Redundant Setup";
            }
            else
            {
                LiteralSatDiv.Text = IsSatDivTermActive ? "Active" : "Inactive";
            }
        }

        /// <summary>
        /// Calculates the remaining volume of a terminal with a SoHo SLA
        /// </summary>
        /// <param name="terminalInfo">The terminal for which the remaining volume needs to be calculated</param>
        /// <returns>The remaining volume of the terminal as a string in GB</returns>
        private string CalculateSoHoRemainingVolume(TerminalInfo terminalInfo)
        {
            //updated calculations now using the correct parameters
            decimal remainingSum = (decimal)terminalInfo.MaxVolSum - (Convert.ToDecimal(terminalInfo.Consumed) + Convert.ToDecimal(terminalInfo.ConsumedReturn));
            remainingSum = remainingSum / (decimal)1000000000.0;
            //do not display values smaller than zero, for cosmetic reasons
            if (remainingSum < 0)
            {
                remainingSum = (decimal)0;
            }
            return string.Format("{0:0.000} GB", remainingSum);
        }
    }
}