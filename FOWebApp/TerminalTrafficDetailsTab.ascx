﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TerminalTrafficDetailsTab.ascx.cs" Inherits="FOWebApp.TerminalTrafficDetailsTab" %>
<style type="text/css">
    .auto-style1 {
        width: 100%;
    }
    .auto-style2 {
        width: 220px;
    }
    .auto-style3 {
        width: 220px;
        height: 26px;
    }
    .auto-style4 {
        height: 26px;
    }
</style>
<div style="padding-top: 5px; padding-bottom: 2px">
    <table class="auto-style1">
        <tr>
            <td class="auto-style2">
                <asp:Label ID="LabelDeviceNotConn" runat="server" ForeColor="Red"></asp:Label>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style2">Device Name:</td>
            <td>
                <asp:Label ID="LabelDeviceName" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style2">OS version:</td>
            <td>
                <asp:Label ID="LabelOSverion" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style3">Up time:</td>
            <td class="auto-style4">
                <asp:Label ID="LabelUpTime" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style2">Connected Users:</td>
            <td>
                <asp:Label ID="LabelUsers" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style2">Contact Person:</td>
            <td>
                <asp:Label ID="LabelContact" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style2">Location:</td>
            <td>
                <asp:Label ID="LabelLocation" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
</div>
<!-- Not needed for the moment
<div>
    <iframe ID="GraphFrame" width="850px" height="500px" runat="server"></iframe>
</div>
    -->
