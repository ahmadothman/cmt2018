﻿using System;
using System.Linq;
using System.Web.Security;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOCMTMessageControllerWSRef;
using FOWebApp.net.nimera.cmt.BODocumentControllerWSRef;

namespace FOWebApp
{
    public partial class DocumentLibrary : System.Web.UI.UserControl
    {
        BODocumentControllerWS _documentController = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            _documentController = new BODocumentControllerWS();
        }

        protected void RadGridDocuments_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            //Retrieve the documents from the database
            //Get user ID + role and load the documents
            String role = "";
            int userReference = 0;
            BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();
            MembershipUser myObject = Membership.GetUser();
            string userId = myObject.ProviderUserKey.ToString();

            if (Roles.IsUserInRole("VNO"))
            {
                userReference = Convert.ToInt32(boAccountingControl.GetDistributorForUser(userId).Id);
                role = "VNO";
            }
            else if (Roles.IsUserInRole("Distributor") || Roles.IsUserInRole("DistributorReadOnly") || Roles.IsUserInRole("SuspendedDistributor"))
            {
                userReference = Convert.ToInt32(boAccountingControl.GetDistributorForUser(userId).Id);
                role = "Distributor";
            }
            else if (Roles.IsUserInRole("Customer") || Roles.IsUserInRole("Organization"))
            {
                userReference = Convert.ToInt32(boAccountingControl.GetOrganizationForUser(userId).Id);
                role = "Organization";
            }
            else if (Roles.IsUserInRole("EndUser"))
            {
                userReference = Convert.ToInt32(boAccountingControl.GetEndUser(userId).Id);
                role = "EndUsers";
            }
            else if (Roles.IsUserInRole("MulticastServiceOperator"))
            {
                userReference = Convert.ToInt32(boAccountingControl.GetOrganizationForUser(userId).Id);
                role = "MSO";
            }
            else if (Roles.IsUserInRole("FinanceAdmin"))
            {
                userReference = 0;
                role = "FinanceAdmin";
            }

            Document[] documents = _documentController.GetDocumentsForRecipient(userReference.ToString(), role);
            RadGridDocuments.DataSource = documents;
        }
    }
}