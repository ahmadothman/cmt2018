﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Windows.Markup;
using Telerik.Web.UI;
using FOWebApp.Data;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOMonitorControlWSRef;
using FOWebApp.net.nimera.cmt.BOTaskControlWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;
using System.Configuration;
using DotNet.Highcharts.Options;
using DotNet.Highcharts;
using DotNet.Highcharts.Helpers;
using DotNet.Highcharts.Enums;
using System.Web.Script.Serialization;
using System.Net;
using FOWebApp.NMSGatewayServiceRef;

namespace FOWebApp
{
    public partial class CustomerVNOTerminalDashboard1 : System.Web.UI.Page
    {
        BOAccountingControlWS _boAccountingControl;
        NMSGatewayServiceRef.NMSGatewayService nms;
        BOTaskControlWS _boTaskControl;
        BOLogControlWS _boLogControl;
        List<Terminal> terminals = new List<Terminal>();

        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Header.DataBind();
            _boAccountingControl = new net.nimera.cmt.BOAccountingControlWSRef.BOAccountingControlWS();
            nms = new NMSGatewayServiceRef.NMSGatewayService();
            ShaperRule srFWD = null;
            ShaperRule srRTN = null;
            
            // get the ID of the customer VNO
            string strOrgId;
            if (HttpContext.Current.User.IsInRole("Organization"))
            {
                MembershipUser myObject = Membership.GetUser();
                string userId = myObject.ProviderUserKey.ToString();
                Organization org = _boAccountingControl.GetOrganizationForUser(userId);
                strOrgId = org.Id.ToString();
            }
            else
            {
                strOrgId = Request.Params["id"];
            }

            if (!IsPostBack)
            {
                LabelUpdateMIR.Visible = false;
                int orgId = Int32.Parse(strOrgId);
                Organization organization = _boAccountingControl.GetOrganization(orgId);
                LabelName.Text = organization.FullName;
                LabelName.Font.Bold = true;
                
                if (strOrgId == "968")
                {
                    srFWD = nms.getShaperRuleByName("VNO-Virunga-FWD-Talia-Arab5A");
                    srRTN = nms.getShaperRuleByName("VNO-Virunga-RTN-Talia-Arab5A");
                }
                else if (strOrgId == "804")
                {
                    srFWD = nms.getShaperRuleByName("VNO-T4T-FWD-EUTELSAT-E16A");
                    srRTN = nms.getShaperRuleByName("VNO-T4T-RTN-EUTELSAT-E16A");
                }
                else if (strOrgId == "985")
                {
                    srFWD = nms.getShaperRuleByName("VNO-Accor-FWD-Talia-Arab5A");
                    srRTN = nms.getShaperRuleByName("VNO-Accor-RTN-Talia-Arab5A");
                }
                else if (strOrgId == "992")
                {
                    srFWD = nms.getShaperRuleByName("VNO-Carabinieri-FWD-NI-E21B");
                    srRTN = nms.getShaperRuleByName("VNO-Carabinieri-RTN-NI-E21B");
                }
                else if (strOrgId == "1001")
                {
                    srFWD = nms.getShaperRuleByName("VNO-RCBURKINA-FWD-ASTRA-4A");
                    srRTN = nms.getShaperRuleByName("VNO-RCBURKINA-RTN-ASTRA-4A");
                }
                else
                {
                    srFWD = nms.getShaperRuleByName("VNO-Moshap-FWD-ASTRA-4A");
                    srRTN = nms.getShaperRuleByName("VNO-Moshap-RTN-ASTRA-4A");
                }

                RadNumericTextBoxCIRFWD.Value = srFWD.guaranteed_rate/1000;
                RadNumericTextBoxMIRFWD.Value = srFWD.peak_rate / 1000;

                RadNumericTextBoxCIRRTN.Value = srRTN.guaranteed_rate / 1000;
                RadNumericTextBoxMIRRTN.Value = srRTN.peak_rate / 1000;
                
                
                this.PopulateTerminalTableSeparate();
            }

            if (strOrgId == "955")
            {
                iframeVNOgraph.Src = "http://nms.satadsl.net:3000/rule/VNO-Moshap-ASTRA-4A";
                iframeVNOgraph.Visible = true;
            }
            else if (strOrgId == "968")
            {
                iframeVNOgraph.Src = "http://nms.satadsl.net:3000/rule/VNO-Virunga-Talia-Arab5A";
                iframeVNOgraph.Visible = true;
            }
            else if (strOrgId == "804")
            {
                iframeVNOgraph.Src = "http://nms.satadsl.net:3000/rule/VNO-T4T-EUTELSAT-E16A";
                iframeVNOgraph.Visible = true;
            }
            else if (strOrgId == "985")
            {
                iframeVNOgraph.Src = "http://nms.satadsl.net:3000/rule/VNO-Accor-Talia-Arab5A";
                iframeVNOgraph.Visible = true;
            }
            else if (strOrgId == "992")
            {
                iframeVNOgraph.Src = "http://nms.satadsl.net:3000/rule/VNO-Carabinieri-NI-E21B";
                iframeVNOgraph.Visible = true;
            }
            else if (strOrgId == "1001")
            {
                iframeVNOgraph.Src = "http://nms.satadsl.net:3000/rule/VNO-RCBURKINA-ASTRA-4A";
                iframeVNOgraph.Visible = true;
            }
        }

        protected void ButtonUpdateView_Click(object sender, EventArgs e)
        {
            BOMonitorControlWS boMonitorControl = new BOMonitorControlWS();
            MembershipUser myObject = Membership.GetUser();
            string userId = myObject.ProviderUserKey.ToString();
            string strOrgId;
            bool updateSucceeded = false;
            if (HttpContext.Current.User.IsInRole("Organization"))
            {
                
                Organization org = _boAccountingControl.GetOrganizationForUser(userId);
                strOrgId = org.Id.ToString();
            }
            else
            {
                strOrgId = Request.Params["id"];
            }
            LabelUpdateMIR.Visible = false;
            LabelTerminals.Visible = false;
            Terminal[] terminals = _boAccountingControl.GetTerminalsByOrganization(Int32.Parse(strOrgId));
            foreach (Terminal term in terminals)
            {
                if (Convert.ToInt32(Session["t" + term.SitId]) != 100)
                {
                    Session["t" + term.SitId] = 100.ToString();
                    AdmStatus status = this.getTerminalStatus((int)term.AdmStatus);
                    term.TermWeight = 100;
                    // update the NMS
                    if (boMonitorControl.NMSTerminalChangeWeight(term.ExtFullName, term.MacAddress, (int)term.TermWeight, (int)term.OrgId, status, term.IspId, new Guid(userId)))
                    {
                        //update the CMT
                        _boAccountingControl.UpdateTerminal(term);
                        updateSucceeded = true;
                    }
                    else
                    {
                        updateSucceeded = false;
                    }
                }

                }
                if (updateSucceeded)
                {
                    this.PopulateTerminalTableSeparate();
                    LabelTerminals.ForeColor = Color.Green;
                    LabelTerminals.Text = "Terminal(s) priorities has been successfuly reset";
                    LabelTerminals.Visible = true;

                }
                else
                {
                    LabelTerminals.ForeColor = Color.Red;
                    LabelTerminals.Text = "Failed to reset terminal(s) priorities";
                    LabelTerminals.Visible = true;
                }
            
            
        }

        protected void ButtonSaveChanges_Click(object sender, EventArgs e)
        {
            string strOrgId;
            BOMonitorControlWS boMonitorControl = new BOMonitorControlWS();
            _boTaskControl = new BOTaskControlWS();

            MembershipUser myObject = Membership.GetUser();
            string userId = myObject.ProviderUserKey.ToString();

            if (HttpContext.Current.User.IsInRole("Organization"))
            {
                MembershipUser myObject1 = Membership.GetUser();
                string userId1 = myObject1.ProviderUserKey.ToString();
                Organization org = _boAccountingControl.GetOrganizationForUser(userId1);
                strOrgId = org.Id.ToString();
            }
            else
            {
                strOrgId = Request.Params["id"];
            }
            int orgId = Int32.Parse(strOrgId);
            Organization organization = _boAccountingControl.GetOrganization(orgId);
            Terminal[] terminals1 = _boAccountingControl.GetTerminalsByOrganization(orgId);
            bool updateSucceeded = true;
            bool changedValues = false;
            int newBWCorrection = 0;
            int OldBWCorrection = 0;
            foreach (Terminal term in terminals1)
            {
                RadNumericTextBox radNumericTextBoxWeight = FindControl("TBW" + term.SitId) as RadNumericTextBox;
                OldBWCorrection = Convert.ToInt32(Session["t" + term.SitId]);
                if (radNumericTextBoxWeight != null)
                    newBWCorrection = (int)radNumericTextBoxWeight.Value;

                            
                
                
                if (newBWCorrection != OldBWCorrection && newBWCorrection != 0)
                {
                    Session["t" + term.SitId] = newBWCorrection.ToString();
                    AdmStatus status = this.getTerminalStatus((int)term.AdmStatus);
                    term.TermWeight = newBWCorrection;
                    // update the NMS
                    if (boMonitorControl.NMSTerminalChangeWeight(term.ExtFullName, term.MacAddress, (int)term.TermWeight, (int)term.OrgId, status, term.IspId, new Guid(userId)))
                    {
                        //update the CMT
                        _boAccountingControl.UpdateTerminal(term);
                        changedValues = true;
                        //add a task for the NOCSA to notify the NMS
                        Task changeTask = new Task();
                        changeTask.Completed = false;
                        changeTask.DateCreated = DateTime.Now;
                        changeTask.Type = _boTaskControl.GetTaskType(10);
                        changeTask.DistributorId = (int)term.DistributorId;
                        changeTask.DateDue = DateTime.Now.AddDays(1.0);
                        changeTask.MacAddress = term.MacAddress;
                        changeTask.Subject = "Change terminal weight in NMS";
                        changeTask.UserId = new Guid(userId);
                        if (!_boTaskControl.UpdateTask(changeTask))
                        {
                            CmtApplicationException cmtAppEx = new CmtApplicationException();
                            cmtAppEx.ExceptionDateTime = DateTime.Now;
                            cmtAppEx.ExceptionDesc = "Could not add weight change task to task table";
                            cmtAppEx.StateInformation = "Adding task failed";
                            cmtAppEx.UserDescription = "Check weight of terminal and update it in the NMS";
                            _boLogControl.LogApplicationException(cmtAppEx);
                        }
                    }
                    else
                    {
                        updateSucceeded = false;
                    }
                }
            }

            if (changedValues == true)
            {
                


                if (updateSucceeded)
                {
                    this.PopulateTerminalTableSeparate();
                    LabelTerminals.ForeColor = Color.Green;
                    LabelTerminals.Text = "Changes saved to terminal(s)";
                    LabelTerminals.Visible = true;

                }
                else
                    {
                        LabelTerminals.ForeColor = Color.Red;
                        LabelTerminals.Text = "Saving changes failed.";
                        LabelTerminals.Visible = true;
                    }
                }
            else
            {
                LabelTerminals.ForeColor = Color.Red;
                LabelTerminals.Text = "No changes have been made.";
                LabelTerminals.Visible = true;
            }
        }

        protected void PopulateTerminalTableSeparate()
        {
            string strOrgId;
            if (HttpContext.Current.User.IsInRole("Organization"))
            {
                MembershipUser myObject = Membership.GetUser();
                string userId = myObject.ProviderUserKey.ToString();
                Organization org = _boAccountingControl.GetOrganizationForUser(userId);
                strOrgId = org.Id.ToString();
            }
            else
            {
                strOrgId = Request.Params["id"];
            }
            int orgId = Int32.Parse(strOrgId);
            Organization organization = _boAccountingControl.GetOrganization(orgId);
            Terminal[] terminals = _boAccountingControl.GetTerminalsByOrganization(orgId);

            if (terminals.Length > 0)
            {
                //Calculate the total weight
                int totalWeight = 0;
                foreach (Terminal term in terminals)
                {
                    if (Session["t" + term.SitId] != null)
                    {
                        term.TermWeight = Convert.ToInt32(Session["t" + term.SitId]);
                    }
                    else
                    {
                        if (term.TermWeight == null)
                        {
                            term.TermWeight = 100;
                        }
                        Session["t" + term.SitId] = term.TermWeight.ToString();
                    }
                    totalWeight = totalWeight + (int)term.TermWeight;
                }

                //Set the weight and CIR for each terminal
                decimal terminalAdjustedWeight;
               
                if (organization.CIR == null)
                {
                    organization.CIR = 0;
                }
                if (organization.MIR == null)
                {
                    organization.MIR = 0;
                }
                foreach (Terminal term in terminals)
                {
                    term.TermWeight = Convert.ToInt32(Session["t" + term.SitId]);
                    terminalAdjustedWeight = (decimal)term.TermWeight / totalWeight;
                                       
                    RadNumericTextBox radNumericTextBoxWeight = FindControl("TBW" + term.SitId) as RadNumericTextBox;
                    if (radNumericTextBoxWeight != null)
                    {
                        radNumericTextBoxWeight.Value = term.TermWeight;
                        radNumericTextBoxWeight.Enabled = true;
                    }
                }

                ButtonUpdateView.Visible = true;
                ButtonUpdateView.Enabled = true;
                ButtonSaveChanges.Visible = true;
            }
        }

        protected void PopulateTerminalTableEqual()
        {
            string strOrgId;
            if (HttpContext.Current.User.IsInRole("Organization"))
            {
                MembershipUser myObject = Membership.GetUser();
                string userId = myObject.ProviderUserKey.ToString();
                Organization org = _boAccountingControl.GetOrganizationForUser(userId);
                strOrgId = org.Id.ToString();
            }
            else
            {
                strOrgId = Request.Params["id"];
            }
            int orgId = Int32.Parse(strOrgId);
            Organization organization = _boAccountingControl.GetOrganization(orgId);
            Terminal[] terminals = _boAccountingControl.GetTerminalsByOrganization(orgId);

            if (terminals.Length > 0)
            {
                //All terminal weights are set to 100
                int totalWeight = terminals.Length * 100;
                decimal terminalAdjustedWeight = (decimal)100 / totalWeight;
                if (organization.CIR == null)
                {
                    organization.CIR = 0;
                }
                if (organization.MIR == null)
                {
                    organization.MIR = 0;
                }
                decimal terminalCIR = terminalAdjustedWeight * (decimal)organization.CIR;
                decimal terminalMIR = terminalAdjustedWeight * (decimal)organization.MIR;
                foreach (Terminal term in terminals)
                {
                    term.TermWeight = 100;
                    Session["t" + term.SitId] = term.TermWeight.ToString();
                    TextBox textBoxCIR = FindControl("TBC" + term.SitId) as TextBox;
                    if (textBoxCIR != null)
                    {
                        textBoxCIR.Text = terminalCIR.ToString("0.00");
                    }
                    TextBox textBoxMIR = FindControl("TBM" + term.SitId) as TextBox;
                    if (textBoxMIR != null)
                    {
                        textBoxMIR.Text = terminalMIR.ToString("0.00");
                    }
                    RadNumericTextBox radNumericTextBoxWeight = FindControl("TBW" + term.SitId) as RadNumericTextBox;
                    if (radNumericTextBoxWeight != null)
                    {
                        radNumericTextBoxWeight.Value = term.TermWeight;
                        radNumericTextBoxWeight.Enabled = false;
                    }
                }

                ButtonUpdateView.Visible = true;
                ButtonUpdateView.Enabled = false;
                ButtonSaveChanges.Visible = true;
            }
        }

        protected void RadioButtonWeightType_CheckedChanged(object sender, EventArgs e)
        {
            LabelUpdateMIR.Visible = false;
            LabelTerminals.Visible = false;
            string strOrgId;
            if (HttpContext.Current.User.IsInRole("Organization"))
            {
                MembershipUser myObject = Membership.GetUser();
                string userId = myObject.ProviderUserKey.ToString();
                Organization org = _boAccountingControl.GetOrganizationForUser(userId);
                strOrgId = org.Id.ToString();
            }
            else
            {
                strOrgId = Request.Params["id"];
            }
            //if (RadioButtonSeparate.Checked)
            //{
            //    Terminal[] terminals = _boAccountingControl.GetTerminalsByOrganization(Int32.Parse(strOrgId));
            //    foreach (Terminal term in terminals)
            //    {
            //        RadNumericTextBox radNumericTextBoxWeight = FindControl("TBW" + term.SitId) as RadNumericTextBox;
            //        if (radNumericTextBoxWeight != null)
            //        {
            //            Session["t" + term.SitId] = radNumericTextBoxWeight.Value.ToString();
            //        }
            //    }
            //    this.PopulateTerminalTableSeparate();
            //}
            //else if (RadioButtonEqual.Checked)
            //{
            //    this.PopulateTerminalTableEqual();
            //}
        }

        private AdmStatus getTerminalStatus(int termState)
        {
            AdmStatus status;

            if (termState == 1)
            {
                status = AdmStatus.LOCKED;
            }
            else if (termState == 2)
            {
                status = AdmStatus.OPERATIONAL;
            }
            else if (termState == 3)
            {
                status = AdmStatus.DECOMMISSIONED;
            }
            else if (termState == 5)
            {
                status = AdmStatus.Request;
            }
            else if (termState == 6)
            {
                status = AdmStatus.Deleted;
            }
            else
            {
                status = AdmStatus.Test;
            }

            return status;
        }

        override protected void OnInit(EventArgs e)
        {
            _boAccountingControl = new net.nimera.cmt.BOAccountingControlWSRef.BOAccountingControlWS();
           


            // get the ID of the customer VNO
            string strOrgId;
            if (HttpContext.Current.User.IsInRole("Organization"))
            {
                MembershipUser myObject = Membership.GetUser();
                string userId = myObject.ProviderUserKey.ToString();
                Organization org = _boAccountingControl.GetOrganizationForUser(userId);
                strOrgId = org.Id.ToString();
            }
            else
            {
                strOrgId = Request.Params["id"];
            }

            int orgId = Int32.Parse(strOrgId);
            _boAccountingControl = new net.nimera.cmt.BOAccountingControlWSRef.BOAccountingControlWS();
            terminals = _boAccountingControl.GetTerminalsByOrganization(orgId).Where(x => !String.IsNullOrEmpty(x.IpAddress.Trim())).ToList();
            Organization organization = _boAccountingControl.GetOrganization(orgId);
            LabelName.Text = organization.FullName;
            LabelName.Font.Bold = true;
           
            if (terminals.Count == 0)
            {
                LabelTerminals.Text = "No terminals registered to this customer VNO";
                LabelTerminals.Visible = true;
                labelVolumeEmpty.Text = "No terminals registered to this customer VNO";
                labelVolumeEmpty.Visible = true;
                labelTrafficClassificationEmpty.Text = "No terminals registered to this customer VNO";
                labelTrafficClassificationEmpty.Attributes.Add("style", "display:inline");
            }
            else
            {
                //Build a table for the terminals

                //Set header row of table
                TableRow row = new TableRow();
                TableCell cellName = new TableCell();
                TableCell cellWeight = new TableCell();
                row.Style.Add("font-weight", "bold");
                cellName.Text = "Terminal name";
                cellWeight.Text = "Priority";
                row.Cells.Add(cellName);
                row.Cells.Add(cellWeight);
                TableTerminals.Rows.Add(row);

                //Create a row for each terminal
                foreach (Terminal term in terminals)
                {
                    row = new TableRow();
                    cellName = new TableCell();
                    cellWeight = new TableCell();
                    cellName.Text = term.FullName;
                    RadNumericTextBox radNumericTextBoxWeight = new RadNumericTextBox();
                    radNumericTextBoxWeight.ID = "TBW" + term.SitId;
                    radNumericTextBoxWeight.Width = 65;
                    radNumericTextBoxWeight.ShowSpinButtons = true;
                    radNumericTextBoxWeight.NumberFormat.DecimalDigits = 0;
                    cellWeight.Controls.Add(radNumericTextBoxWeight);

                    row.Cells.Add(cellName);
                    row.Cells.Add(cellWeight);
                    TableTerminals.Rows.Add(row);
                }
                ButtonUpdateView.Visible = true;
                ButtonUpdateView.Enabled = true;
                ButtonSaveChanges.Visible = true;
            }

            base.OnInit(e);

        }

        protected void trafficPeriodSelected(object sender, EventArgs e)
        {
            if (terminals.Count > 0)
            {
                DateTime endDate = DateTime.Now;
                int numDays = Int32.Parse(trafficPeriodSelect.SelectedItem.Value);
                DateTime startDate = endDate.AddDays(numDays * -1).Date;
                try
                {
                    if (ViewState["trafficChart"] == null)
                    {
                        string chart = HighchartsGraph.BuildFwdRtnTraffic(terminals, startDate, endDate).Replace("chart_container", "chart_container_traffic");
                        ScriptManager.RegisterStartupScript(Page, typeof(Page), "chartKey", chart, true);
                        ViewState["trafficChart"] = chart;
                        ViewState["previoustrafficChartPeriodSelected"] = trafficPeriodSelect.SelectedItem.Value;
                        labelTrafficEmpty.Attributes.Add("style", "display:none");
                        chart_container_traffic.Visible = true;
                    }
                    else if (Convert.ToString(ViewState["previousPeriodSelected"]) != periodSelect.SelectedItem.Value)
                    {
                        string chart = HighchartsGraph.BuildFwdRtnTraffic(terminals, startDate, endDate).Replace("chart_container", "chart_container_traffic");
                        ScriptManager.RegisterStartupScript(Page, typeof(Page), "chartKey", chart, true);
                        ViewState["trafficChart"] = chart;
                        ViewState["previoustrafficChartPeriodSelected"] = trafficPeriodSelect.SelectedItem.Value;
                        labelTrafficEmpty.Attributes.Add("style", "display:none");
                        chart_container_traffic.Visible = true;
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(Page, typeof(Page), "chartKey", Convert.ToString(ViewState["trafficChart"]), true);
                        labelTrafficEmpty.Attributes.Add("style", "display:none");
                        chart_container_traffic.Visible = true;
                    }
                }
                catch (Exception ex)
                {
                    chart_container_traffic.Visible = false;
                    labelTrafficEmpty.Text = ex.Message;
                    labelTrafficEmpty.Attributes.Add("style", "display:inline");
                }
                hidTAB.Value = "traffic";
            }
        }

        protected void periodSelected(object sender, EventArgs e)
        {
            if (terminals.Count > 0)
            {
                DateTime endDate = DateTime.Now.Date;
                int numDays = Int32.Parse(periodSelect.SelectedItem.Value);
                DateTime startDate = endDate.AddDays(numDays * -1);
                endDate = endDate.AddDays(1);
                try
                {
                    if (ViewState["volumeChart"] == null)
                    {
                        string chart = HighchartsGraph.BuildFwdRtnVolume(terminals, startDate, endDate);
                        ScriptManager.RegisterStartupScript(Page, typeof(Page), "chartKey", chart, true);
                        ViewState["volumeChart"] = chart;
                        ViewState["previousPeriodSelected"] = periodSelect.SelectedItem.Value;
                        labelVolumeEmpty.Attributes.Add("style", "display:none");
                        chart_container.Visible = true;
                    }
                    else if (Convert.ToString(ViewState["previousPeriodSelected"]) != periodSelect.SelectedItem.Value)
                    {
                        string chart = HighchartsGraph.BuildFwdRtnVolume(terminals, startDate, endDate);
                        ScriptManager.RegisterStartupScript(Page, typeof(Page), "chartKey", chart, true);
                        ViewState["volumeChart"] = chart;
                        ViewState["previousPeriodSelected"] = periodSelect.SelectedItem.Value;
                        labelVolumeEmpty.Attributes.Add("style", "display:none");
                        chart_container.Visible = true;
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(Page, typeof(Page), "chartKey", Convert.ToString(ViewState["volumeChart"]), true);
                        labelVolumeEmpty.Attributes.Add("style", "display:none");
                        chart_container.Visible = true;
                    }
                }
                catch (Exception ex)
                {
                    chart_container.Visible = false;
                    labelVolumeEmpty.Text = ex.Message;
                    labelVolumeEmpty.Attributes.Add("style", "display:inline");
                }
                hidTAB.Value = "volume";
            }
        }

        protected void trafficClassificationDateChange(object sender, EventArgs e)
        {
            if (terminals.Count > 0)
            {
                DateTime startDateClassification;
                DateTime endDateClassification;
                if (ViewState["trafficClassificationChart"] == null)
                {
                    startDateClassification = DateTime.Now.Date.AddDays(-1);
                    endDateClassification = DateTime.Now.Date;
                    DrawTrafficClassificationChart(startDateClassification, endDateClassification);
                }
                else if (!String.IsNullOrEmpty(classificationDateStart.Value) && !String.IsNullOrEmpty(classificationDateEnd.Value))
                {
                    startDateClassification = Convert.ToDateTime(classificationDateStart.Value);
                    endDateClassification = Convert.ToDateTime(classificationDateEnd.Value);
                    var previousStartDateClassification = Convert.ToDateTime(ViewState["previousStartDateClassification"]);
                    var previousEndDateClassification = Convert.ToDateTime(ViewState["previousEndDateClassification"]);
                    if (startDateClassification != previousStartDateClassification || endDateClassification != previousEndDateClassification)
                    {
                        labelTrafficClassificationEmpty.Attributes.Add("style", "display:none");
                        chart_container_classification.Visible = true;
                        DrawTrafficClassificationChart(startDateClassification, endDateClassification);
                    }
                    else
                    {
                        labelTrafficClassificationEmpty.Attributes.Add("style", "display:none");
                        chart_container_classification.Visible = true;
                        ScriptManager.RegisterStartupScript(Page, typeof(Page), "chartKey", Convert.ToString(ViewState["trafficClassificationChart"]), true);
                    }
                }
                else
                {
                    labelTrafficClassificationEmpty.Attributes.Add("style", "display:none");
                    chart_container_classification.Visible = true;
                    ScriptManager.RegisterStartupScript(Page, typeof(Page), "chartKey", Convert.ToString(ViewState["trafficClassificationChart"]), true);
                }
                hidTAB.Value = "trafficClassification";
            }
        }

        protected void DrawTrafficClassificationChart(DateTime startDateClassification, DateTime endDateClassification)
        {
            try
            {
                DateTime endDate = startDateClassification == endDateClassification ? endDateClassification.AddDays(1) : endDateClassification;
                string trafficClassificationChart = HighchartsGraph.BuildTrafficClassificationPieChart(terminals, startDateClassification, endDate).Replace("chart_container", "chart_container_classification");
                ScriptManager.RegisterStartupScript(Page, typeof(Page), "chartKeyClassification", trafficClassificationChart, true);
                ViewState["trafficClassificationChart"] = trafficClassificationChart;
                ViewState["previousStartDateClassification"] = startDateClassification;
                ViewState["previousEndDateClassification"] = endDateClassification;
            }
            catch (Exception ex)
            {
                chart_container_classification.Visible = false;
                labelTrafficClassificationEmpty.Text = ex.Message;
                labelTrafficClassificationEmpty.Attributes.Add("style", "display:inline");
            }
        }
    }
}