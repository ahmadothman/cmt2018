﻿using FOWebApp.Data;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOMonitorControlWSRef;
using FOWebApp.net.nimera.cmt.BOSatDiversityControllerWSRef;
using System;
using System.Web.UI;

namespace FOWebApp
{
    public partial class TrafficClassificationTab : System.Web.UI.UserControl
    {
        //string _macAddress;
        /// <summary>
        /// MacAddress which must be set before loading the component!
        /// </summary>
        public string MacAddress { get; set; }

        //string _associatedMacAddress;
        /// <summary>
        /// MacAddress of the associated Terminal
        /// </summary>
        public string AssociatedMacAddress { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (ViewState["TrafficClassificationInitialTabSelection"] == null)
            {
                DateTime endDate = DateTime.Now.Date;
                int numDays = 7;
                DateTime startDate = endDate.AddDays(numDays * -1).AddMinutes(15);
                ViewState["TrafficClassificationInitialTabSelection"] = false;
                try
                {
                    string chart = HighchartsGraph.BuildTrafficClassificationPieChart(MacAddress, startDate, endDate);
                    ScriptManager.RegisterStartupScript(Page, typeof(Page), "chartKey", chart, true);
                    errorMessage.Visible = false;
                }
                catch (Exception ex)
                {
                    errorMessage.InnerHtml = ex.Message;
                }
            }
        }


        //protected void Page_Load(object sender, EventArgs e)
        //{
        //    // TEMPORARY FIX CMT-92
        //    if (ViewState["TrafficClassificationInitialTabSelection"] == null)
        //    {
        //        DateTime endDate = DateTime.Now.Date;
        //        int numDays = 1;
        //        DateTime startDate = endDate.AddDays(numDays * -1).AddMinutes(15);
        //        ViewState["TrafficClassificationInitialTabSelection"] = false;
        //        try
        //        {
        //            string chart = HighchartsGraph.BuildTrafficClassification(MacAddress, startDate, endDate);
        //            ScriptManager.RegisterStartupScript(Page, typeof(Page), "chartKey", chart, true);
        //            errorMessage.Visible = false;
        //        }
        //        catch (Exception ex)
        //        {
        //            errorMessage.InnerHtml = ex.Message;
        //        }
        //    }
        //}

        protected void RadButtonDateSelection_Click(object sender, EventArgs e)
        {
            if ((bool)ViewState["TrafficClassificationInitialTabSelection"] == false)
            {
                DateTime startDate = ((DateTime)RadDatePickerStartDate.SelectedDate).AddMinutes(15);
                DateTime endDate = (DateTime)RadDatePickerEndDate.SelectedDate;
                if (startDate != null && endDate != null)
                {
                    if (startDate == endDate)
                    {
                        endDate = endDate.AddDays(1);
                    }
                    else if (endDate.Date == DateTime.Now.Date)
                    {
                        endDate = endDate.AddDays(1);
                    }
                    try
                    {
                        string chart = HighchartsGraph.BuildTrafficClassificationPieChart(MacAddress, startDate, endDate);
                        ScriptManager.RegisterStartupScript(Page, typeof(Page), "chartKey", chart, true);
                        errorMessage.Visible = false;
                    }
                    catch (Exception ex)
                    {
                        errorMessage.InnerHtml = ex.Message;
                    }
                }
            }
        }

        //protected void RadButtonDateSelection_Click(object sender, EventArgs e)
        //{
        //    // TEMPORARY FIX CMT-92
        //    if ((bool)ViewState["TrafficClassificationInitialTabSelection"] == false)
        //    {
        //        DateTime startDate = ((DateTime)RadDatePickerStartDate.SelectedDate).AddMinutes(15);
        //        DateTime endDate = startDate.AddDays(1);
        //        if (startDate != null)
        //        {
        //            try
        //            {
        //                string chart = HighchartsGraph.BuildTrafficClassification(MacAddress, startDate, endDate);
        //                ScriptManager.RegisterStartupScript(Page, typeof(Page), "chartKey", chart, true);
        //                errorMessage.Visible = false;
        //            }
        //            catch (Exception ex) 
        //            {
        //                errorMessage.InnerHtml = ex.Message;
        //            }
                    
        //        }
        //    }
        //}
    }
}