﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Drawing;
using Telerik.Web.UI;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOMonitorControlWSRef;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;
using FOWebApp.net.nimera.cmt.BOTaskControlWSRef;
using FOWebApp.net.nimera.cmt.BOConfigurationControlWSRef;
using FOWebApp.NMSGatewayServiceRef;

namespace FOWebApp
{
    public partial class TerminalManagementTab : System.Web.UI.UserControl
    {
        string _macAddress;
        const string _HEXBYTE = "<0|1|2|3|4|5|6|7|8|9|a|b|c|d|e|f><0|1|2|3|4|5|6|7|8|9|a|b|c|d|e|f>";

        /// <summary>
        /// MacAddress which must be set before loading the component!
        /// </summary>
        public string MacAddress
        {
            get { return _macAddress; }
            set { _macAddress = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Initialize the Expiry date control
            BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();
            Terminal terminal = boAccountingControl.GetTerminalDetailsByMAC(_macAddress);
            NMSGatewayService nmsService = new NMSGatewayService();
            NMSTerminalInfo nmsTerminal = nmsService.getTerminalInfoByMacAddress(_macAddress);


            RadMaskedTextBoxMacAddress.Mask =
               _HEXBYTE + "<:>" + _HEXBYTE + "<:>" + _HEXBYTE + "<:>" + _HEXBYTE + "<:>" + _HEXBYTE + "<:>" +
               _HEXBYTE;

            TextBoxTerminalName.Text = terminal.FullName;
            RadMaskedTextBoxMacAddress.Text = terminal.MacAddress;


            if (terminal.AdmStatus == 1)
            {
                RadButtonAction.Text = "Reactivate Terminal";
                RadButtonAction.CommandName = "ReActivate";
                RadButtonAction.Visible = true;
                RadButtonFUPReset.Enabled = false;
                RadMaskedTextBoxMacAddress.Enabled = true;
                //CMTFO-99
                RadComboBoxSLA.Enabled = false;
                RadButtonSlaChange.Enabled = false;
                RadComboBoxCustomerOrganizations.Enabled = false;
                RadButtonChangeCustomer.Enabled = false;
            }
            else if (terminal.AdmStatus == 2)
            {
                RadButtonAction.Text = "Suspend Terminal";
                RadButtonAction.CommandName = "Suspend";
                RadButtonAction.Visible = true;
                RadButtonFUPReset.Enabled = true;
                RadMaskedTextBoxMacAddress.Enabled = false;
                //CMTFO-99
                RadComboBoxSLA.Enabled = true;
                RadButtonSlaChange.Enabled = true;
                RadComboBoxCustomerOrganizations.Enabled = true;
                RadButtonChangeCustomer.Enabled = true;
            }
            else
            {
                RadButtonAction.Visible = false;
                RadMaskedTextBoxMacAddress.Enabled = false;
                RadButtonFUPReset.Enabled = false;
                //CMTFO-99
                RadComboBoxSLA.Enabled = false;
                RadButtonSlaChange.Enabled = false;
                RadComboBoxCustomerOrganizations.Enabled = false;
                RadButtonChangeCustomer.Enabled = false;
            }

            //Load the SLA combobox
            ServiceLevel[] serviceLevels = boAccountingControl.GetServicePacksByIspWithHide(terminal.IspId);

            for (int i = 0; i < serviceLevels.Length; i++)
            {
                RadComboBoxItem item = new RadComboBoxItem();
                item.Value = serviceLevels[i].SlaId.ToString();
                item.Text = serviceLevels[i].SlaName.Trim();
                if (serviceLevels[i].Hide == true)
                    item.Enabled = false;
                RadComboBoxSLA.Items.Add(item);
            }
            //Add hidden SLA Item to comboBox
            for (int i = 0; i < RadComboBoxSLA.Items.Count; i++)
            {
                if (RadComboBoxSLA.Items[i].Value != terminal.SlaId.ToString())
                {
                    RadComboBoxItem item = new RadComboBoxItem();
                    item.Value = terminal.SlaId.ToString();
                    item.Text = terminal.SlaName.Trim();
                    RadComboBoxSLA.Items.Add(item);
                    break;
                }
            }

            RadComboBoxSLA.SelectedValue = terminal.SlaId.ToString();
            HiddenFieldSLA.Value = terminal.SlaId.ToString();
            ServiceLevel sla = boAccountingControl.GetServicePack((int)terminal.SlaId);

            RadDatePickerExpiryDate.SelectedDate = terminal.ExpiryDate;

            //Hide FUP-reset button if voucher based SLA/SoHo ISP
            
            if (terminal.IspId == 473 || sla.ServiceClass == 1 || sla.ServiceClass == 3 || sla.ServiceClass==7)
            {
                RadButtonFUPReset.Visible = false;
                RadButtonFUPReset.Enabled = false;
            }
            else
            {
                RadButtonFUPReset.Visible = true;
            }

            if (sla.ServiceClass == 2 || sla.ServiceClass == 3)
            {
                RadDatePickerExpiryDate.Enabled = true;
                RadButtonTerminalUpdate.Visible = true;
            }
            else
            {
                RadDatePickerExpiryDate.Enabled = false;
                
                

            }
            if (sla.ServiceClass==1 && !nmsTerminal.OverVolume )
            {
                RadComboBoxSLA.Enabled = false;
                RadButtonSlaChange.Visible = false;
            }
            //temporarily prevent modem swap
            RadMaskedTextBoxMacAddress.Enabled = false;

            //Load the Customer combobox
            Organization[] organizations = boAccountingControl.GetOrganisationsByDistributorId(terminal.DistributorId.ToString());

            RadComboBoxCustomerOrganizations.Items.Clear();
            RadComboBoxCustomerOrganizations.ClearSelection();
            RadComboBoxCustomerOrganizations.DataSource = organizations;
            RadComboBoxCustomerOrganizations.DataTextField = "FullName";
            RadComboBoxCustomerOrganizations.DataValueField = "Id";
            RadComboBoxCustomerOrganizations.DataBind();

            if (!(organizations.Length == 0))
            {
                RadComboBoxItem item = new RadComboBoxItem();
                item.Value = "0";
                item.Text = "";
                RadComboBoxCustomerOrganizations.Items.Add(item);
                RadComboBoxCustomerOrganizations.SortItems();
                if (terminal.OrgId != null)
                {
                    RadComboBoxCustomerOrganizations.SelectedValue = terminal.OrgId.ToString();
                }
                else
                {
                    RadComboBoxCustomerOrganizations.SelectedValue = "0";
                }
            }
            else
            {
                RadComboBoxCustomerOrganizations.Text = "No customers defined";
                RadComboBoxCustomerOrganizations.Enabled = false;
            }

            //CMTSUPPORT-192
            if (terminal.TestMode == true && Roles.IsUserInRole("Distributor"))
            {
                RadDatePickerExpiryDate.Enabled = false;
                RadButtonTerminalUpdate.Visible = false;

            }
            
            
            if (sla.ServiceClass==1 && Roles.IsUserInRole("Distributor"))
            {
                Literal1.Text = "Volume expiry date:";
                
                

            }
        }

        protected void RadDatePickerExpiryDate_SelectedDateChanged(object sender, Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs e)
        {
            //Store the date in the database
            BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();
            Terminal terminal = boAccountingControl.GetTerminalDetailsByMAC(_macAddress);

            terminal.ExpiryDate = e.NewDate;

            boAccountingControl.UpdateTerminal(terminal);
        }

        protected void ButtonSave_Click(object sender, EventArgs e)
        {
            BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();
            Terminal terminal = boAccountingControl.GetTerminalDetailsByMAC(_macAddress);

            terminal.ExpiryDate = RadDatePickerExpiryDate.SelectedDate;

            boAccountingControl.UpdateTerminal(terminal);
        }

        protected void RadButtonAction_Click(object sender, EventArgs e)
        {
            BOMonitorControlWS boMonitorControl = new BOMonitorControlWS();
            BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();
            BOLogControlWS boLogControl = new BOLogControlWS();
            Terminal terminal = boAccountingControl.GetTerminalDetailsByMAC(_macAddress);
            Distributor dist = boAccountingControl.GetDistributor((int)terminal.DistributorId);
            terminal.DistributorName = dist.FullName;

            //User who did the delete
            MembershipUser myObject = Membership.GetUser();
            string userId = myObject.ProviderUserKey.ToString();

            if (RadButtonAction.CommandName.Equals("ReActivate"))
            {

                if (boMonitorControl.TerminalReActivate(terminal.ExtFullName, terminal.MacAddress, terminal.IspId, new Guid(userId)))
                {
                    RadWindowManager1.RadAlert("Terminal re-activation request sent to Hub.<br/>Wait approx. 10 minutes for completion!", 250, 200, "Terminal re-activation", null);

                    //Send E-Mail
                    List<string> mailAddresses = new List<string>();
                    mailAddresses.Add(FOWebApp.Properties.Settings.Default.SupportEmail);
                    boLogControl.SendMail("Sent by distributor: " + dist.FullName + "<br/>Re-activate terminal with MacAddress: " + terminal.MacAddress + "<br/>Terminal name: " + terminal.FullName + "<br/>SitId: " + terminal.SitId,
                                            "Terminal Re-Activation Request", mailAddresses.ToArray());

                    //Create a task to warn the NOCSA/NOCOP
                    BOTaskControlWS boTaskControl = new BOTaskControlWS();
                    Task ActivationTask = new Task();
                    ActivationTask.DateDue = DateTime.Now.AddDays(1);
                    ActivationTask.DistributorId = (int)terminal.DistributorId;
                    ActivationTask.DateCreated = DateTime.Now;
                    ActivationTask.MacAddress = terminal.MacAddress;
                    ActivationTask.Subject = "Verify re-activation done by distributor " + terminal.DistributorId + " for sitid " + terminal.SitId;
                    ActivationTask.Completed = false;
                    ActivationTask.UserId = new Guid(userId);
                    TaskType ActivationTaskType = new TaskType();
                    ActivationTaskType.DistInfo = false;
                    ActivationTaskType.Id = 5;
                    ActivationTaskType.Type = "Re-Activate";
                    ActivationTask.Type = ActivationTaskType;
                    boTaskControl.UpdateTask(ActivationTask);
                }
                else
                {
                    RadWindowManager1.RadAlert("Terminal re-activation failed, contact SatADSL.", 250, 100, "Terminal re-activation", null);
                }
            }

            if (RadButtonAction.CommandName.Equals("Suspend"))
            {
                if (boMonitorControl.TerminalSuspend(terminal.ExtFullName, terminal.MacAddress, terminal.IspId, new Guid(userId)))
                {
                    RadWindowManager1.RadAlert("Terminal suspension request sent to Hub.<br/>Wait approx. 10 minutes for completion!", 250, 200, "Terminal suspension", null);
                  
                    //Create a task to warn the NOCSA/NOCOP
                    BOTaskControlWS boTaskControl = new BOTaskControlWS();
                    Task SuspendTask = new Task();
                    SuspendTask.DateDue = DateTime.Now.AddDays(1);
                    SuspendTask.DistributorId = (int)terminal.DistributorId;
                    SuspendTask.DateCreated = DateTime.Now;
                    SuspendTask.MacAddress = terminal.MacAddress;
                    SuspendTask.Subject = "Verify suspension by distributor " + terminal.DistributorId + " for sitid " + terminal.SitId;
                    SuspendTask.Completed = false;
                    SuspendTask.UserId = new Guid(userId);
                    TaskType SuspendTaskType = new TaskType();
                    SuspendTaskType.DistInfo = false;
                    SuspendTaskType.Id = 3;
                    SuspendTaskType.Type = "Suspend";
                    SuspendTask.Type = SuspendTaskType;
                    boTaskControl.UpdateTask(SuspendTask);

                    //Send E-Mail
                    List<string> mailAddresses = new List<string>();
                    mailAddresses.Add(FOWebApp.Properties.Settings.Default.SupportEmail);
                    boLogControl.SendMail("Sent by distributor: " + dist.FullName + "<br/>Suspend terminal with MacAddress: " + terminal.MacAddress + "<br/>Terminal name: " + terminal.FullName + "<br/>SitId: " + terminal.SitId + "<br/>SLA: " + terminal.SlaName, 
                                            "Terminal Suspension", mailAddresses.ToArray());
                }
                else
                {
                    RadWindowManager1.RadAlert("Terminal suspension failed", 250, 100, "Suspension result", null);
                }
            }
        }

        protected void RadButtonFUPReset_Click(object sender, EventArgs e)
        {
            BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();
            Terminal terminal = boAccountingControl.GetTerminalDetailsByMAC(_macAddress);
            ServiceLevel sla = boAccountingControl.GetServicePack((int)terminal.SlaId);
            Distributor dist = boAccountingControl.GetDistributor((int)terminal.DistributorId);
            terminal.DistributorName = dist.FullName;
            ActivityType fupActType = null;

            if (sla.ServiceClass != 1) //Prevent FUP resets for SoHo SLAs
            {
                //Reset FUP
                BOMonitorControlWS boMonitorControl = new BOMonitorControlWS();
                BOLogControlWS boLogControl = new BOLogControlWS();

                //Uncomment if the suspension and reactivation is done via the ISPIF and not the emulator!!!!
                if (boMonitorControl.TerminalUserReset(terminal.ExtFullName, terminal.IspId))
                {
                    //Get info for this Terminal activity from the database
                    ActivityType[] activityTypes = boLogControl.GetTerminalActivityTypes();

                    foreach (ActivityType actType in activityTypes)
                    {
                        if (actType.Id == 700)
                        {
                            fupActType = actType;
                            break;
                        }
                    }

                    //Update TerminalActivity log
                    TerminalActivity terminalActivity = new TerminalActivity();
                    terminalActivity.Action = "FUPReset";
                    terminalActivity.ActionDate = DateTime.Now;
                    terminalActivity.MacAddress = terminal.MacAddress;
                    terminalActivity.SlaName = terminal.SlaId.ToString();

                    if (fupActType != null)
                    {
                        terminalActivity.AccountingFlag = (bool)fupActType.Billable;
                    }
                    else
                    {
                        terminalActivity.AccountingFlag = false;
                    }

                    //User who did the FUP reset
                    MembershipUser myObject = Membership.GetUser();
                    string userId = myObject.ProviderUserKey.ToString();

                    terminalActivity.UserId = new Guid(userId);
                    boLogControl.LogTerminalActivity(terminalActivity);
                    RadWindowManager1.RadAlert("Terminal volume reset succeeded.", 250, 100, "FUP Reset", null);

                    //Send E-Mail
                    List<string> mailAddresses = new List<string>();
                    mailAddresses.Add(FOWebApp.Properties.Settings.Default.SupportEmail);
                    boLogControl.SendMail("Sent by distributor: " + dist.FullName + "<br/>FUP Reset for terminal with MacAddress: " + terminal.MacAddress + "<br/>Terminal name: " + terminal.FullName + "<br/>SitId: " + terminal.SitId,
                                            "FUP Reset Request", mailAddresses.ToArray());

                    //Finally add a task for the NOCSA/NOCOP, this is not necessary anymore when the FUP Reset is done immediately
                    //via the ISPIF
                    BOTaskControlWS boTaskControl = new BOTaskControlWS();
                    Task FUPResetTask = new Task();
                    FUPResetTask.DateDue = DateTime.Now.AddDays(1);
                    FUPResetTask.DistributorId = (int)terminal.DistributorId;
                    FUPResetTask.DateCreated = DateTime.Now;
                    FUPResetTask.MacAddress = terminal.MacAddress;
                    FUPResetTask.Subject = "Verify FUP Reset done by distributor " + terminal.DistributorId + " for sitid " + terminal.SitId;
                    FUPResetTask.Completed = false;
                    FUPResetTask.UserId = new Guid(userId);
                    TaskType FUPResetTaskType = new TaskType();
                    FUPResetTaskType.DistInfo = false;
                    FUPResetTaskType.Id = 4;
                    FUPResetTaskType.Type = "FUP Reset";
                    FUPResetTask.Type = FUPResetTaskType;
                    boTaskControl.UpdateTask(FUPResetTask);
                }
                else
                {
                    RadWindowManager1.RadAlert("Terminal volume reset failed.", 250, 100, "FUP Reset", null);
                }
            }
        }

        /// <summary>
        /// Update the MAC Address, Expiry date and/or the terminal name if necessary
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadButtonTerminalUpdate_Click(object sender, EventArgs e)
        {
            BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();
            BOMonitorControlWS boMonitorControl = new BOMonitorControlWS();
            BOLogControlWS boLogControl = new BOLogControlWS();
            Terminal terminal = boAccountingControl.GetTerminalDetailsByMAC(_macAddress);
            Distributor dist = boAccountingControl.GetDistributor((int)terminal.DistributorId);
            terminal.DistributorName = dist.FullName;

            terminal.FullName = TextBoxTerminalName.Text;
            terminal.ExpiryDate = RadDatePickerExpiryDate.SelectedDate;
            
            //Update any modifications
            boAccountingControl.UpdateTerminal(terminal);

            //User who did the change of MAC address
            MembershipUser myObject = Membership.GetUser();
            string userId = myObject.ProviderUserKey.ToString();

            //Check if the Mac Address has changed - temporarily disabled
            //string newMacAddress = RadMaskedTextBoxMacAddress.Text;
            //if (!terminal.MacAddress.Equals(newMacAddress))
            //{
            //    if (boMonitorControl.ChangeMacAddress(terminal.ExtFullName, terminal.MacAddress, terminal.IspId, newMacAddress))
            //    {
            //        RadWindowManager1.RadAlert("Mac Address change succeeded.<br/>Wait approx. 10 minutes for completion!", 250, 100, "MAC Address change", null);

            //        //Send E-Mail
            //        List<string> mailAddresses = new List<string>();
            //        mailAddresses.Add(FOWebApp.Properties.Settings.Default.SupportEmail);                    
            //        boLogControl.SendMail("Sent by distributor: " + dist.FullName + "<br/>Change of MacAddress from: " + terminal.MacAddress  
            //            + "<br/> To Address: " + newMacAddress
            //            + "<br/>Terminal name: " + terminal.FullName
            //            + "<br/>SitId: " + terminal.SitId, "Change of MAC Address", mailAddresses.ToArray());

            //        //Create a task to warn the NOCSA/NOCOP
            //        BOTaskControlWS boTaskControl = new BOTaskControlWS();
            //        Task changeMacTask = new Task();
            //        changeMacTask.DateDue = DateTime.Now.AddDays(1);
            //        changeMacTask.DistributorId = (int)terminal.DistributorId;
            //        changeMacTask.DateCreated = DateTime.Now;
            //        changeMacTask.MacAddress = terminal.MacAddress;
            //        changeMacTask.Subject = "Verify change MAC by distributor " + terminal.DistributorId + " for sitid " + terminal.SitId;
            //        changeMacTask.Completed = false;
            //        changeMacTask.UserId = new Guid(userId);
            //        TaskType changeMacTaskType = new TaskType();
            //        changeMacTaskType.DistInfo = false;
            //        changeMacTaskType.Id = 3;
            //        changeMacTaskType.Type = "MacAddressChange";
            //        changeMacTask.Type = changeMacTaskType;
            //        boTaskControl.UpdateTask(changeMacTask);
                
            //    }
            //    else
            //    {
            //        RadWindowManager1.RadAlert("Mac Address change failed!.", 250, 100, "MAC Address change", null);
            //    }
            //}
        }

        protected void RadButtonSlaChange_Click(object sender, EventArgs e)
        {
            BOLogControlWS boLogControl = new BOLogControlWS();
            try
            {
                if (!RadComboBoxSLA.SelectedValue.Equals(HiddenFieldSLA.Value))
                {
                    //Send an E-Mail message to the NOCSA and add a task.
                    BOConfigurationControllerWS boConfigControl = new BOConfigurationControllerWS();
                    BOTaskControlWS boTaskControl = new BOTaskControlWS();
                    BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();
                    //User who initiated the SLA change
                    MembershipUser myObject = Membership.GetUser();
                    string userId = myObject.ProviderUserKey.ToString();

                    Terminal term = boAccountingControl.GetTerminalDetailsByMAC(_macAddress);

                    Distributor dist = boAccountingControl.GetDistributor((int)term.DistributorId);

                    //This call gives an error (remote server issue)
                    //string supportEMail = boConfigControl.getParameter("SupportEMail");
                    string supportEMail = "support@satadsl.net";

                    if (supportEMail == null || supportEMail.Equals(""))
                    {
                        //Error message
                        CmtApplicationException cmtEx = new CmtApplicationException();
                        cmtEx.ExceptionDesc = "SLA Change by Distributor failed";
                        cmtEx.ExceptionStacktrace = "Support EMail not found in configuration table";
                        cmtEx.UserDescription = "The SLA could not be changed because the EMail address of the NOCSA could not be found in the configuration table";
                        cmtEx.ExceptionDateTime = DateTime.Now;
                        boLogControl.LogApplicationException(cmtEx);

                        RadWindowManager1.RadAlert("Due to a system error the SLA change failed, please contact the NOCSA for more information!", 250, 100, "SLA Change", null);
                    }
                    else
                    {

                        //Send E-Mail and add task
                        string body = "Dear NOCSA/NOCOP<br/><br/>" +
                             "An SLA change has been initiated by Distributor: " +
                            term.DistributorId + ", " + dist.FullName + "<br/>" +
                            "Terminal MAC Address: " + term.MacAddress + "<br/>" +
                            "Terminal name: " + term.FullName + "<br/>" +
                            "Terminal SitId: " + term.SitId + "<br/>" +
                            "Original SLA: " + term.SlaName + "<br/>" +
                            "Target SLAName: " + RadComboBoxSLA.Text + "<br/>" +
                            "Target SLA Id: " + RadComboBoxSLA.SelectedValue + "<br/><br/>" +
                            "Request Date (UTC): " + DateTime.UtcNow + "<br/><br/>" +
                            "Please complete this query as soon as possible.";
                        string subject = "SLA Change";
                        string[] mailAddresses = { supportEMail.Trim() };
                        boLogControl.SendMail(body, subject, mailAddresses);

                        //Add task to task list
                        Task slaChangeTask = new Task();
                        slaChangeTask.Completed = false;
                        slaChangeTask.DateCreated = DateTime.UtcNow;
                        slaChangeTask.DateDue = DateTime.UtcNow;
                        slaChangeTask.DistributorId = (int)term.DistributorId;
                        slaChangeTask.MacAddress = term.MacAddress;
                        slaChangeTask.Subject = "Change SLA from " + term.SlaName + " to " + RadComboBoxSLA.Text;
                        slaChangeTask.UserId = new Guid(userId);
                        TaskType slaChangeTaskType = new TaskType();
                        slaChangeTaskType.DistInfo = true;
                        slaChangeTaskType.Id = 6;
                        slaChangeTaskType.Type = "SlaChange";
                        slaChangeTask.Type = slaChangeTaskType;

                        if (!boTaskControl.UpdateTask(slaChangeTask))
                        {
                            CmtApplicationException cmtEx = new CmtApplicationException();
                            cmtEx.ExceptionDesc = "SLA Change by Distributor failed to add a task to task list";
                            cmtEx.ExceptionStacktrace = "Could not add task to task list";
                            cmtEx.UserDescription = "";
                            cmtEx.ExceptionDateTime = DateTime.Now;
                            boLogControl.LogApplicationException(cmtEx);
                        }

                        RadWindowManager1.RadAlert("The SLA Change message was successfully forwarded to the NOCSA!<br/>You will receive a confirmation E-Mail shortly.", 250, 100, "SLA Change", null);

                    }
                }
            }
            catch (Exception ex)
            {
                CmtApplicationException cmtEx = new CmtApplicationException();
                cmtEx.ExceptionDesc = ex.Message;
                cmtEx.ExceptionStacktrace = ex.StackTrace;
                cmtEx.UserDescription = "Change Sla button clicked";
                cmtEx.ExceptionDateTime = DateTime.Now;
                boLogControl.LogApplicationException(cmtEx);
            }
        }

        /// <summary>
        /// Inserts a task for the NOCSA. Informs the NOCSA to migrate the terminal from T11N
        /// To Astra4a.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadButtonMigrate_Click(object sender, EventArgs e)
        {
            BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();
            BOTaskControlWS boTaskControl = new BOTaskControlWS();
            BOLogControlWS boLogControl = new BOLogControlWS();

            //User who did the delete
            MembershipUser myObject = Membership.GetUser();
            string userId = myObject.ProviderUserKey.ToString();

            Terminal terminal = boAccountingControl.GetTerminalDetailsByMAC(_macAddress);

            Task migrateTask = new Task();
            migrateTask.DateDue = DateTime.Now.AddDays(1);
            migrateTask.DistributorId = (int)terminal.DistributorId;
            migrateTask.DateCreated = DateTime.Now;
            migrateTask.MacAddress = terminal.MacAddress;
            migrateTask.Subject = "Migration asked by " + terminal.DistributorId + " for sitid " + terminal.SitId;
            migrateTask.Completed = false;
            migrateTask.UserId = new Guid(userId);
            TaskType migrateTaskType = new TaskType();
            migrateTaskType.DistInfo = true;
            migrateTaskType.Id = 8;
            migrateTaskType.Type = "Migration";
            migrateTask.Type = migrateTaskType;
            boTaskControl.UpdateTask(migrateTask);

            //Also add an activity to the list of activities
            TerminalActivity termActivity = new TerminalActivity();
            termActivity.Action = "Migration";
            termActivity.ActionDate = DateTime.UtcNow;
            termActivity.MacAddress = _macAddress;
            termActivity.SlaName = terminal.SlaId.ToString();
            termActivity.NewSlaName = "";
            termActivity.UserId = new Guid(userId);
            termActivity.AccountingFlag = false;
            boLogControl.LogTerminalActivity(termActivity);
        }

        protected void RadButtonCustomerChange_Click(object sender, EventArgs e)
        {
            BOMonitorControlWS boMonitorControl = new BOMonitorControlWS();
            BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();
            Terminal terminal = boAccountingControl.GetTerminalDetailsByMAC(_macAddress);
            terminal.OrgId = int.Parse(RadComboBoxCustomerOrganizations.SelectedValue);
            if (boAccountingControl.UpdateTerminal(terminal) )
            {
                LabelResult.Text = "Customer changed successfully!";
                    LabelResult.ForeColor = Color.DarkGreen;
            }
            else
            {
                LabelResult.Text = "Changing customer failed!";
                LabelResult.ForeColor = Color.Red;
            }
        }
    }
}