﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using Telerik.Web.UI;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Web.UI.DataVisualization.Charting;
using System.Globalization;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOMonitorControlWSRef;
using FOWebApp.net.nimera.cmt.BOIssuesControllerWSRef;

namespace FOWebApp
{
    public partial class Issues : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Session["_issueListForDistributor_PageSize"] == null)
            {
                RadGridIssues.PageSize = 20;
            }
            else
            {
                RadGridIssues.PageSize = (int)this.Session["_issueListForDistributor_PageSize"];
            }
            //Load data into the grid view. This view lists all issues for the authenticated user.
            
            BOIssuesControllerWS boIssuesControl = new net.nimera.cmt.BOIssuesControllerWSRef.BOIssuesControllerWS();
            BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();

            //Get the distributor ID of the active user
            MembershipUser myObject = Membership.GetUser();
            string userId = myObject.ProviderUserKey.ToString();
            int distributorId = Convert.ToInt32(boAccountingControl.GetDistributorForUser(userId).Id);

            //Get the Issues linked to the user
            Issue[] issues = boIssuesControl.GetIssuesByDistributor(distributorId);

            RadGridIssues.DataSource = issues;
            RadGridIssues.DataBind();

            LabelNumRows.Text = issues.Length.ToString();
        }

        protected void RadGridIssues_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridItemCollection selectedItems = RadGridIssues.SelectedItems;

            if (selectedItems.Count > 0)
            {
                GridItem selectedItem = selectedItems[0];
                Issue issue = (Issue)selectedItem.DataItem;
                string key = issue.Key;
            }
        }

        protected void RadGridIssues_ItemDataBound(object sender, GridItemEventArgs e)
        {
        }

        protected void RadWindowManager1_Disposed(object sender, EventArgs e)
        {
        }

        protected void RadWindowManager1_Unload(object sender, EventArgs e)
        {
        }

        protected void RadGridIssues_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            this.Session["_issueListForDistributor_PageSize"] = e.NewPageSize;
        }

        protected void RadGridIssues_ItemCommand(object sender, GridCommandEventArgs e)
        {
        }
    }
}