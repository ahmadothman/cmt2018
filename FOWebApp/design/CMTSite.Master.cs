﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FOWebApp
{
    public partial class CMTSite : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["WhiteLabel"] = false;

            if (Request.Params["Branding"] != null)
            {
                if (Request.Params["Branding"].Equals("Wl"))
                {
                    Session["WhiteLabel"] = true;
                }
                else
                {
                    Session["WhiteLabel"] = false;
                }
            }

            headerPaneHide.Visible = !(Boolean)Session["WhiteLabel"];
        }
    }
}