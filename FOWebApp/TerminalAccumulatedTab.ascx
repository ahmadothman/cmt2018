﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TerminalAccumulatedTab.ascx.cs" Inherits="FOWebApp.TerminalAccumulatedTab" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<style type="text/css">
    .graphChart {
        height: 500px;
        width: 875px;
    }
</style>

<div style="padding-top: 5px; padding-bottom: 2px">
Select period: 
    <telerik:RadComboBox ID="RadComboBoxPeriodSelection" runat="server" Skin="Metro" AutoPostBack="False">
    <Items>
        <telerik:RadComboBoxItem runat="server" Text="1 Month" Value="30" />
        <telerik:RadComboBoxItem runat="server" Text="2 Months" Value="60" />
        <telerik:RadComboBoxItem runat="server" Text="6 Months" Value="180" />
        <telerik:RadComboBoxItem runat="server" Text="1 Year" Value="365" />
    </Items>
</telerik:RadComboBox>
&nbsp&nbsp
    <telerik:RadButton ID="RadButtonPeriod" runat="server" Text="Show Chart" onclick="RadButtonPeriod_Click" Skin="Metro"> 
    </telerik:RadButton>
&nbsp&nbsp&nbsp
    <telerik:RadDatePicker ID="RadDatePickerStartDate" runat="server" Skin="Metro">
         <DateInput DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy"  runat="server"/> 
    </telerik:RadDatePicker>
    <telerik:RadDatePicker ID="RadDatePickerEndDate" runat="server" Skin="Metro">
         <DateInput DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy"  runat="server"/> 
    </telerik:RadDatePicker>
&nbsp&nbsp
    <telerik:RadButton ID="RadButtonDateSelection" runat="server" Text="Show Chart" onclick="RadButtonDateSelection_Click" Skin="Metro">
    </telerik:RadButton>
</div>
<div id='chart_container' class="graphChart" ></div>
<telerik:RadScriptBlock ID="RadScriptBlockHighchart" runat="server">
    <script type="text/javascript" src="../Scripts/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="../Scripts/highcharts.js"></script>
    <script type="text/javascript" src="../Scripts/exporting.js"></script>
    <script type="text/javascript" src="../Scripts/offline-exporting.js"></script>
</telerik:RadScriptBlock>

