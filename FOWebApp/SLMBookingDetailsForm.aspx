﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SLMBookingDetailsForm.aspx.cs" 
    Inherits="FOWebApp.SLMBookingDetailsForm" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table  cellpadding="10" cellspacing="5">
            <tr>
                <td><asp:Label runat="server" ID="LabelDistOrCust"></asp:Label></td>
                <td>
                    <asp:Label runat="server" ID="LabelDistributorOrCustomer"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Mac address:</td>
                <td>
                    <asp:Label runat="server" ID="LabelMacAddress"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Start Time:</td>
                <td>
                    <asp:Label runat="server" ID="LabelStartTime"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>End Time:</td>
                <td>
                    <asp:Label runat="server" ID="LabelEndTime"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Booking Service Pack:</td>
                <td>
                    <asp:Label runat="server" ID="LabelBookingSLA"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Terminal Service Pack</td>
                <td>
                    <asp:Label runat="server" ID="LabelTerminalSLA"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Type:</td>
                <td>
                    <asp:Label runat="server" ID="LabelType"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Status:</td>
                <td>
                    <asp:Label runat="server" ID="LabelStatus"></asp:Label>
                </td>
            </tr>
            <tr></tr>
            <tr>
                <td>
                    <asp:Button runat="server" ID="ButtonApprove" Text="Approve" OnClick="ButtonApprove_Click" />
                </td>
                <td>
                    <asp:Button runat="server" ID="ButtonDecline" Text="Decline" OnClick="ButtonDecline_Click" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="LabelResult"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
