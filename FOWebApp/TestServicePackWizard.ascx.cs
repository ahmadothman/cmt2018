﻿using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace FOWebApp
{
    public partial class TestServicePackWizard : System.Web.UI.UserControl
    {
        public BOAccountingControlWS _boAccountingControlWS;

        protected void Page_Load(object sender, EventArgs e)
        {
            //Load the ISP list
            RadComboBoxISPId.Items.Clear();
            _boAccountingControlWS = new BOAccountingControlWS();
            Isp[] ispList = _boAccountingControlWS.GetIsps();
            RadComboBoxISPId.DataValueField = "Id";
            RadComboBoxISPId.DataTextField = "CompanyName";
            RadComboBoxISPId.DataSource = ispList;
            RadComboBoxISPId.DataBind();

            //In case an ISP is selected, we must set the seleted value of this combobox back
            //to the previous selected value.
            if (Session["ISP"] != null)
            {
                RadComboBoxISPId.SelectedValue = Session["ISP"].ToString();
            }

            //Load the service lines
            string[] serviceLines = { "SoHo Services", "Business Services", "Corporate Services", "Specialized Tailored Services" };
            int i = 1;
            foreach (string sl in serviceLines)
            {
                RadComboBoxItem myItem = new RadComboBoxItem();
                myItem.Text = sl;
                myItem.Value = i.ToString();
                RadComboBoxServiceLine.Items.Add(myItem);
                i++;
            }

            //Set the ServiceLine value to the selected value if necessary
            if (Session["ServiceLine"] != null)
            {
                RadComboBoxServiceLine.SelectedValue = Session["ServiceLine"].ToString();
            }
        }

        protected void SetSessions(int isp, int serviceLine)
        {
            Session["ISP"] = isp;
            Session["ServiceLine"] = serviceLine;
        }

        //protected void btnNext_Click(object sender, EventArgs e)
        //{
        //    Control sheetContainer = scanForControl("SheetContentPlaceHolder", this);

        //    if (sheetContainer != null)
        //    {
        //        Control contentContainer = sheetContainer.FindControl("MainContentPlaceHolder");

        //        if (contentContainer != null)
        //        {
        //            contentContainer.Controls.Clear();
        //            Control contentControl = this.LoadControl(RadComboBoxServiceLine.SelectedValue);
        //            if (contentControl != null)
        //            {
        //                contentContainer.Controls.Add(contentControl);
        //            }
        //        }
        //    }

        //    Session["ISP"] = RadComboBoxISPId.SelectedItem.ToString();
        //    Session["ServiceLine"] = RadComboBoxServiceLine.SelectedValue;
        //}

        //protected Control scanForControl(string controlId, Control root)
        //{
        //    Control resCtrl = root.FindControl(controlId);

        //    if (resCtrl == null)
        //    {
        //        root = root.Parent;
        //        if (root != null)
        //        {
        //            resCtrl = this.scanForControl(controlId, root.Parent);
        //        }
        //    }

        //    return resCtrl;
        //}

        //protected Control LoadControl(string serviceLine)
        //{
        //    this.Page = new Page();
        //    Control control = null;
        //    if (serviceLine == "1")
        //    {
        //        control = Page.LoadControl("~/ServiceLines/SoHo.ascx");
        //    }
        //    else if (serviceLine == "2")
        //    {
        //        control = Page.LoadControl("~/ServiceLines/Business.ascx");
        //    }
        //    else if (serviceLine == "3")
        //    {
        //        control = Page.LoadControl("~/ServiceLines/Corporate.ascx");
        //    }
        //    else
        //    {
        //        control = Page.LoadControl("~/ServiceLines/SpecialTailored.ascx");
        //    }

        //    return control;
        //}
    }
}