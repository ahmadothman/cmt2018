﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Web.UI.DataVisualization.Charting;
using System.Globalization;
using FOWebApp.net.nimera.cmt.BOMonitorControlWSRef;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;

namespace FOWebApp
{
    public partial class TerminalDetail : System.Web.UI.UserControl
    {
        string _macAddress;

        /// <summary>
        /// MacAddress which must be set before loading the component!
        /// </summary>
        public string MacAddress
        {
            get { return _macAddress; }
            set { _macAddress = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            CultureInfo culture = new CultureInfo("en-US");
            BOMonitorControlWS boMonitorControl =
                new BOMonitorControlWS();

            BOAccountingControlWS boAccountingControl =
                new BOAccountingControlWS();

            //We must fetch the terminal information first  to get to the ISP identifier
            Terminal term = boAccountingControl.GetTerminalDetailsByMAC(_macAddress);
            TerminalInfo ti = boMonitorControl.getTerminalInfoByMacAddress(_macAddress, term.IspId);

            if (ti.ErrorFlag)
            {
                Label6.Visible = true;
                LabelError.Visible = true;
                LabelError.Text = ti.ErrorMsg;
            }
            else
            {
                //Get the ISP for this terminal
                Isp isp = boAccountingControl.GetISP(Int32.Parse(ti.IspId));

                //Load ti properties into fields.
                Label6.Visible = false;
                LabelError.Visible = false;
                LabelSiteId.Text = ti.SitId;
                LabelSiteName.Text = ti.EndUserId;
                LabelIPAddress.Text = ti.IPAddress;
                LabelMacAddress.Text = ti.MacAddress;

                if (isp != null)
                {
                    LabelISP.Text = isp.CompanyName + ", Id: " + isp.Id;
                }
                else
                {
                    LabelISP.Text = "Unknown";
                }

                decimal consumedGB = Convert.ToInt64(ti.Consumed) / (decimal)1000000000.0;
                LabelFConsumed.Text = "" + consumedGB.ToString("0.000") + " GB";

                LabelServicePackage.Text = ti.SlaName;
                decimal FUPThresholdBytes = ti.FUPThreshold * (decimal)1000000000.0;

                //double percentConsumed = (consumedMB / FUPThresholdMB) * 100.0;
                double percentConsumed = (Convert.ToDouble(ti.Consumed) / Convert.ToDouble(FUPThresholdBytes)) * 100.0;
                Decimal percentConsumedDec = new Decimal(percentConsumed);

                LabelVolumeAllocation.Text = ti.FUPThreshold + " GB";
                //LabelVolumeAllocation.Text = FUPThresholdBytes + " Bytes";
                LabelFConsumed.Text += " (" + percentConsumedDec.ToString("#.00") + " %)";
                LabelResetDayOfMOnth.Text = ti.InvoiceDayOfMonth;

                double rtnNum = Double.Parse(ti.RTN, culture);
                LabelRTN.Text = rtnNum.ToString("#.0") + " dBHz";
                //double rtnNum = Convert.ToDouble(ti.RTN);

                LabelRTN.ForeColor = this.CNOColorValidator(rtnNum, term.IspId);
                LabelStatus.Text = ti.Status;

                if (ti.Status == "Operational")
                {
                    LabelStatus.ForeColor = Color.Green;
                }
                else
                {
                    LabelStatus.ForeColor = Color.Red;
                }

                ChartVolume.Width = 640;
                ChartVolume.Titles.Add("Consumption Overview");
                ChartVolume.Titles[0].Font = new Font("Utopia", 16);
                ChartVolume.BackColor = Color.Gray;
                ChartVolume.BackSecondaryColor = Color.WhiteSmoke;
                ChartVolume.BackGradientStyle = System.Web.UI.DataVisualization.Charting.GradientStyle.DiagonalRight;
                ChartVolume.BorderSkin.SkinStyle = System.Web.UI.DataVisualization.Charting.BorderSkinStyle.Raised;
                //ChartVolume.ChartAreas[0].AlignmentOrientation = AreaAlignmentOrientations.Horizontal;
                ChartVolume.ChartAreas[0].BackColor = Color.LightSalmon;
                ChartVolume.ChartAreas[0].BackSecondaryColor = Color.LightGreen;
                ChartVolume.ChartAreas[0].BackGradientStyle = System.Web.UI.DataVisualization.Charting.GradientStyle.TopBottom;
                ChartVolume.ChartAreas[0].Area3DStyle.Enable3D = false;

                ChartVolume.Series.Add(new Series("FVolumeSeries"));

                ChartVolume.Series["FVolumeSeries"].ChartType = SeriesChartType.Column;
                ChartVolume.Series["FVolumeSeries"].BorderWidth = 3;
                ChartVolume.Series["FVolumeSeries"].ShadowOffset = 2;
                ChartVolume.Series["FVolumeSeries"].XValueType = ChartValueType.Double;

                Color chartColor = Color.Gray;

                if (percentConsumed <= 80.0)
                {
                    chartColor = Color.Green;
                }
                else if (percentConsumed > 80.0 && percentConsumed < 100.0)
                {
                    chartColor = Color.Orange;
                }
                else
                {
                    chartColor = Color.Red;
                }

                ChartVolume.Series["FVolumeSeries"].Color = chartColor;
                List<decimal> YValues = new List<decimal>();
                YValues.Add(consumedGB);
                string[] XValues = new String[] { "Forward Volume" };
                ChartVolume.Series["FVolumeSeries"].Points.DataBindXY(XValues, "Volume", YValues, "GB");
                ChartVolume.Series["FVolumeSeries"].LegendText = "Volumes in GB";

                ChartVolume.Series.Add(new Series("FUPThreshold"));
                ChartVolume.Series["FUPThreshold"].ChartType = SeriesChartType.Line;
                ChartVolume.Series["FUPThreshold"].BorderWidth = 3;
                if (percentConsumed < 100)
                {
                    ChartVolume.Series["FUPThreshold"].Color = Color.Red;
                }
                else
                {
                    ChartVolume.Series["FUPThreshold"].Color = Color.Wheat;
                }
                ChartVolume.Series["FUPThreshold"].LegendText = "FUP Threshold in GB";
                ChartVolume.Series["FUPThreshold"].AxisLabel = "FUP Threshold";
                ChartVolume.Series["FUPThreshold"].MarkerStyle = MarkerStyle.Diamond;
                ChartVolume.Series["FUPThreshold"].MarkerSize = 15;
                ChartVolume.Series["FUPThreshold"].IsValueShownAsLabel = true;
                ChartVolume.Series["FUPThreshold"].LabelForeColor = Color.Yellow;
                ChartVolume.Series["FUPThreshold"].LabelBackColor = Color.Black;



                List<decimal> FUPValues = new List<decimal>();
                FUPValues.Add(ti.FUPThreshold);
                ChartVolume.Series["FUPThreshold"].Points.DataBindY(FUPValues);

                Legend volumeLegend = new Legend("VolLegend");
                ChartVolume.Legends.Add(new Legend("Volume in GB"));
            }
        }

        /// <summary>
        /// Determines the color of the CNO pointing label depending on the 
        /// Return number value and the ISP.
        /// </summary>
        /// <remarks>
        /// The return value levels which determine the label color depend on the
        /// ISP. Later we should thing on a more flexible and "plugable" strategy to
        /// implement this feature.
        /// </remarks>
        /// <param name="rtnNum">The return number value</param>
        /// <param name="ispId">The ISP identifier</param>
        /// <returns>A Color value depending on the given rtnNum value and isp</returns>
        protected Color CNOColorValidator(double rtnNum, int ispId)
        {
            Color labelColor;

            if (ispId == 112)
            {
                if (rtnNum < 55.2)
                {
                    labelColor = Color.Red;
                }
                else if (rtnNum >= 55.2 && rtnNum < 58.4)
                {
                    labelColor = Color.DarkOrange;
                }
                else
                {
                    labelColor = Color.DarkGreen;
                }
                LabelRTN.ToolTip = "Above 58.4 dBHz, the terminal uses the most efficient return pool. " +
                    "Between 55.2 dBHz and 58.4 dBHz, the terminal must use a less efficient return pool. " +
                    "Below 55.2 dBHz the signal is too weak.";
            }
            else if (ispId == 412)
            {
                if (rtnNum < 52.7)
                {
                    labelColor = Color.Red;
                }
                else if (rtnNum >= 52.7 && rtnNum < 55.8)
                {
                    labelColor = Color.DarkOrange;
                }
                else
                {
                    labelColor = Color.DarkGreen;
                }
                LabelRTN.ToolTip = "Above 55.8 dBHz, the terminal uses the most efficient return pool. " +
                    "Between 52.7 dBHz and 55.8 dBHz, the terminal must use a less efficient return pool. " +
                    "Below 52.7 dBHz the signal is too weak.";
            }
            else if (ispId > 700 && ispId < 800)
            {
                if (rtnNum < 62)
                {
                    labelColor = Color.Red;
                }
                else if (rtnNum >= 62 && rtnNum < 66)
                {
                    labelColor = Color.DarkOrange;
                }
                else
                {
                    labelColor = Color.DarkGreen;
                }

                LabelRTN.ToolTip = "Above 66 dBHz, the terminal uses the most efficient return pool. " +
                "Between 62 dBHz and 66 dBHz, the terminal must use a less efficient return pool. " +
                "Below 62 dBHz the signal is too weak.";
            }
            else
            {
                //General case
                if (rtnNum < 55.2)
                {
                    labelColor = Color.Red;
                }
                else if (rtnNum >= 55.2 && rtnNum < 58.4)
                {
                    labelColor = Color.DarkOrange;
                }
                else
                {
                    labelColor = Color.DarkGreen;
                }
            }

            return labelColor;
        }
    
    }
}