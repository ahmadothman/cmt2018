﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FOWebApp.Properties {
    
    
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "14.0.0.0")]
    internal sealed partial class Settings : global::System.Configuration.ApplicationSettingsBase {
        
        private static Settings defaultInstance = ((Settings)(global::System.Configuration.ApplicationSettingsBase.Synchronized(new Settings())));
        
        public static Settings Default {
            get {
                return defaultInstance;
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://localhost:9090/BOUserControlWS.asmx")]
        public string FOWebApp_net_nimera_cmt_BOUserControlWSRef_BOUserControlWS {
            get {
                return ((string)(this["FOWebApp_net_nimera_cmt_BOUserControlWSRef_BOUserControlWS"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://localhost:9090/BOLogControlWS.asmx")]
        public string FOWebApp_net_nimera_cmt_BOLogControlWSRef_BOLogControlWS {
            get {
                return ((string)(this["FOWebApp_net_nimera_cmt_BOLogControlWSRef_BOLogControlWS"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://localhost:9090/BOTaskControlWS.asmx")]
        public string FOWebApp_net_nimera_cmt_BOTaskControlWS_BOTaskControlWS {
            get {
                return ((string)(this["FOWebApp_net_nimera_cmt_BOTaskControlWS_BOTaskControlWS"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("C:\\Temp\\Vouchers\\")]
        public string VoucherPath {
            get {
                return ((string)(this["VoucherPath"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://localhost:9090/BOConfigurationControllerWS.asmx")]
        public string FOWebApp_net_nimera_cmt_BOConfigurationControlWSRef_BOConfigurationControllerWS {
            get {
                return ((string)(this["FOWebApp_net_nimera_cmt_BOConfigurationControlWSRef_BOConfigurationControllerWS"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://localhost:9090/BOIssuesControllerWS.asmx")]
        public string FOWebApp_net_nimera_cmt_BOIssuesControllerWSRef_BOIssuesControllerWS {
            get {
                return ((string)(this["FOWebApp_net_nimera_cmt_BOIssuesControllerWSRef_BOIssuesControllerWS"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://localhost:9090/BOPaymentControllerWS.asmx")]
        public string FOWebApp_net_nimera_cmt_BOPaymentControllerWSRef_BOPaymentControllerWS {
            get {
                return ((string)(this["FOWebApp_net_nimera_cmt_BOPaymentControllerWSRef_BOPaymentControllerWS"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://localhost:9090/BOMultiCastGroupControllerWS.asmx")]
        public string FOWebApp_net_nimera_cmt_BOMultiCastGroupControllerWSRef_BOMultiCastGroupControllerWS {
            get {
                return ((string)(this["FOWebApp_net_nimera_cmt_BOMultiCastGroupControllerWSRef_BOMultiCastGroupControlle" +
                    "rWS"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://localhost:9090/BOCMTMessageControllerWS.asmx")]
        public string FOWebApp_net_nimera_cmt_BOCMTMessageControllerWSRef_BOCMTMessageControllerWS {
            get {
                return ((string)(this["FOWebApp_net_nimera_cmt_BOCMTMessageControllerWSRef_BOCMTMessageControllerWS"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://localhost:9090/BOServicesControllerWS.asmx")]
        public string FOWebApp_net_nimera_cmt_BOServicesControllerWSRef_BOServicesControllerWS {
            get {
                return ((string)(this["FOWebApp_net_nimera_cmt_BOServicesControllerWSRef_BOServicesControllerWS"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://localhost:9090/BOConnectedDevicesControlWS.asmx")]
        public string FOWebApp_net_nimera_cmt_BOConnectedDevicesControlWSRef_BOConnectedDevicesControlWS {
            get {
                return ((string)(this["FOWebApp_net_nimera_cmt_BOConnectedDevicesControlWSRef_BOConnectedDevicesControlW" +
                    "S"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://localhost:9090/BOMandacControlWS.asmx")]
        public string FOWebApp_net_nimera_cmt_BOMandacControlWSRef_BOMandacControlWS {
            get {
                return ((string)(this["FOWebApp_net_nimera_cmt_BOMandacControlWSRef_BOMandacControlWS"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("20/02/2015")]
        public string ReleaseDate {
            get {
                return ((string)(this["ReleaseDate"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://localhost:9090/BOMonitorControlWS.asmx")]
        public string FOWebApp_net_nimera_cmt_BOMonitorControlWSRef_BOMonitorControlWS {
            get {
                return ((string)(this["FOWebApp_net_nimera_cmt_BOMonitorControlWSRef_BOMonitorControlWS"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://localhost:9090/BOSatDiversityControllerWS.asmx")]
        public string FOWebApp_net_nimera_cmt_BOSatDiversityControllerWSRef_BOSatDiversityControllerWS {
            get {
                return ((string)(this["FOWebApp_net_nimera_cmt_BOSatDiversityControllerWSRef_BOSatDiversityControllerWS"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://localhost:9090/BODocumentControllerWS.asmx")]
        public string FOWebApp_net_nimera_cmt_BODocumentControllerWSRef_BODocumentControllerWS {
            get {
                return ((string)(this["FOWebApp_net_nimera_cmt_BODocumentControllerWSRef_BODocumentControllerWS"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://localhost:9090/BOInvoiceController.asmx")]
        public string FOWebApp_net_nimera_cmt_BOInvoiceControllerWSRef_BOInvoiceController {
            get {
                return ((string)(this["FOWebApp_net_nimera_cmt_BOInvoiceControllerWSRef_BOInvoiceController"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://localhost:9090/BOInvoicingControllerWSRef.asmx")]
        public string FOWebApp_net_nimera_cmt_BOInvoicingControllerWS_BOInvoicingControllerWS {
            get {
                return ((string)(this["FOWebApp_net_nimera_cmt_BOInvoicingControllerWS_BOInvoicingControllerWS"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://localhost:9090/BOInvoicingControllerWSRef.asmx")]
        public string FOWebApp_net_nimera_cmt_BOInvoivingControllerWSRef_BOInvoicingControllerWS {
            get {
                return ((string)(this["FOWebApp_net_nimera_cmt_BOInvoivingControllerWSRef_BOInvoicingControllerWS"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://localhost:9090/BOInvoicingControllerWS.asmx")]
        public string FOWebApp_net_nimera_cmt_BOInvoicingControllerWSRef1_BOInvoicingControllerWS {
            get {
                return ((string)(this["FOWebApp_net_nimera_cmt_BOInvoicingControllerWSRef1_BOInvoicingControllerWS"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://localhost:9090/BOSatLinkManagerControlWS.asmx")]
        public string FOWebApp_net_nimera_cmt_BOSatLinkManagerControlWSRef_BOSatLinkManagerControlWS {
            get {
                return ((string)(this["FOWebApp_net_nimera_cmt_BOSatLinkManagerControlWSRef_BOSatLinkManagerControlWS"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://localhost:9090/BOInvoicingControllerWS.asmx")]
        public string FOWebApp_net_nimera_cmt_BOInvoicingControllerWSRef_BOInvoicingControllerWS {
            get {
                return ((string)(this["FOWebApp_net_nimera_cmt_BOInvoicingControllerWSRef_BOInvoicingControllerWS"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://localhost:9090/BOAccountingControlWS.asmx")]
        public string FOWebApp_net_nimera_cmt_BOAccountingControlWSRef_BOAccountingControlWS {
            get {
                return ((string)(this["FOWebApp_net_nimera_cmt_BOAccountingControlWSRef_BOAccountingControlWS"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("ahmad.satadsl@gmail.com")]
        public string SupportEmail {
            get {
                return ((string)(this["SupportEmail"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("2.5.3")]
        public string Version {
            get {
                return ((string)(this["Version"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://192.168.100.110:8080/NMSGateway/NMSGatewayService.asmx")]
        public string FOWebApp_NMSGatewayServiceRef_NMSGatewayService {
            get {
                return ((string)(this["FOWebApp_NMSGatewayServiceRef_NMSGatewayService"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.WebServiceUrl)]
        [global::System.Configuration.DefaultSettingValueAttribute("http://localhost:9090/BOVoucherControllerWS.asmx")]
        public string FOWebApp_net_nimera_cmt_BOVoucherControllerWSRef_BOVoucherControllerWS {
            get {
                return ((string)(this["FOWebApp_net_nimera_cmt_BOVoucherControllerWSRef_BOVoucherControllerWS"]));
            }
        }
    }
}
