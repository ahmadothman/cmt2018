﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CustomerVNOTerminalDashboard.aspx.cs" Inherits="FOWebApp.CustomerVNOTerminalDashboard1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Customer VNO Terminal Dashboard</title>
    <%--<link href="../CMTStyle.css" rel="stylesheet" type="text/css" />--%>
    <link href="../Content/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../Content/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../Scripts/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="../Scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="../Scripts/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="../Scripts/highcharts.js"></script>
    <script type="text/javascript" src="../Scripts/exporting.js"></script>
    <script type="text/javascript" src="../Scripts/offline-exporting.js"></script>
    <script type="text/javascript">
         $(document).ready(function () {
            var tab = document.getElementById('<%# hidTAB.ClientID%>').value;
            $('#myTabs a[href="#' + tab + '"]').tab('show');
            $('#inputDateContainer .input-daterange').datepicker({
                format: "dd/mm/yyyy",
                autoclose: true,
                todayHighlight: true,
            });
            //$('#inputDateContainer .input-daterange').datepicker()
            //        .on('changeDate', function (e) {
            //            $("#chart_container_classification").hide();
            //            $("#tcLoadingContainer").show();
            //            $("#trafficClassificationDateButton").click();
            //        });
            $('#trafficClassificationDateButton').click(function () {
                $("#labelTrafficClassificationEmpty").hide();
                $("#chart_container_classification").hide();
                $("#tcLoadingContainer").show();
            });
            $('#periodSelect').change(function () {
                $("#labelVolumeEmpty").hide();
                $("#chart_container").hide();
                $("#volumeLoadingContainer").show();
            });
            $('#trafficPeriodSelect').change(function () {
                $("#labelTrafficEmpty").hide();
                $("#chart_container_traffic").hide();
                $("#trafficLoadingContainer").show();
            });
            $('a[data-toggle="pill"]').on('shown.bs.tab', function (e) {
                var target = $(e.target).attr("href") // activated tab
                if (target == "#traffic") {
                    $("#chart_container_traffic").hide();
                    $("#trafficLoadingContainer").show();
                    $("#trafficButton").click();
                }
                if (target == "#volume") {
                    $("#chart_container").hide();
                    $("#volumeLoadingContainer").show();
                    $("#volumeButton").click();
                }
                if (target == "#trafficClassification") {
                    $("#chart_container_classification").hide();
                    $("#tcLoadingContainer").show();
                    $("#trafficClassificationDateButton").click();
                }
            });
        });

    </script>

    <style type="text/css">
        /** { border: 1px solid black; }*/

        .graphChart {
            height: 390px;
            clear: left;
            /*height: 100%;*/
            width: 725px;
        }

        .Absolute-Center {
            margin: auto;
            position: absolute;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
        }

        .glyphicon-refresh-animate {
            -animation: spin .7s infinite linear;
            -webkit-animation: spin2 .7s infinite linear;
        }

        @-webkit-keyframes spin2 {
            from {
                -webkit-transform: rotate(0deg);
            }

            to {
                -webkit-transform: rotate(360deg);
            }
        }

        @keyframes spin {
            from {
                transform: scale(1) rotate(0deg);
            }

            to {
                transform: scale(1) rotate(360deg);
            }
        }
    </style>
</head>
<body>
    <div class="container" id="formContainer">
        <form id="formCustomerDetail" runat="server">
            <ul class="nav nav-pills nav-justified" id="myTabs">
                <li><a data-toggle="pill" href="#terminals">Terminals</a></li>
                <li><a data-toggle="pill" href="#traffic">Traffic</a></li>
                <li><a data-toggle="pill" href="#volume">Volume</a></li>
                <li><a data-toggle="pill" href="#trafficClassification">Traffic Classification</a></li>
                <li><a data-toggle="pill" href="#realtime">Real-time</a></li>
            </ul>
            <div class="tab-content">
                <div id="terminals" class="tab-pane fade">
                    <asp:ScriptManager ID="ScriptManager1" runat="server">
                    </asp:ScriptManager>
                    <table class="table">
                        <tr class="info">
                            <td style="text-align: right">Customer VNO:
                            </td>
                            <td>
                                <asp:Label ID="LabelName" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr class="info">
                            <td style="text-align: right">CIR FWD:
                            </td>
                            <td>
                                <telerik:RadNumericTextBox ID="RadNumericTextBoxCIRFWD" runat="server" ShowSpinButtons="False" Skin="Metro" Value="0" Width="125px" Enabled="false">
                                    <NumberFormat ZeroPattern="n" DecimalDigits="0"></NumberFormat>
                                </telerik:RadNumericTextBox>
                                kbps <%--<asp:Button ID="ButtonMIR" runat="server" Text="Update MIR & CIR" OnClick="ButtonMIR_Click" />--%>
                                <asp:Label ID="LabelUpdateMIR" runat="server"></asp:Label>
                            </td>
                            <td style="text-align: right">CIR RTN:
                            </td>
                            <td>
                                <telerik:RadNumericTextBox ID="RadNumericTextBoxCIRRTN" runat="server" ShowSpinButtons="False" Skin="Metro" Value="0" Width="125px" Enabled="false">
                                    <NumberFormat ZeroPattern="n" DecimalDigits="0"></NumberFormat>
                                </telerik:RadNumericTextBox>
                                kbps <%--<asp:Button ID="ButtonMIR" runat="server" Text="Update MIR & CIR" OnClick="ButtonMIR_Click" />--%>
                                <asp:Label ID="Label1" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr class="info">
                            <td style="text-align: right">MIR FWD:
                            </td>
                            <td>
                                <telerik:RadNumericTextBox ID="RadNumericTextBoxMIRFWD" runat="server" ShowSpinButtons="False" Skin="Metro" Value="0" Width="125px" Enabled="false">
                                    <NumberFormat ZeroPattern="n" DecimalDigits="0"></NumberFormat>
                                </telerik:RadNumericTextBox>
                                kbps
                            </td>
                            <td style="text-align: right">MIR RTN:
                            </td>
                            <td>
                                <telerik:RadNumericTextBox ID="RadNumericTextBoxMIRRTN" runat="server" ShowSpinButtons="False" Skin="Metro" Value="0" Width="125px" Enabled="false">
                                    <NumberFormat ZeroPattern="n" DecimalDigits="0"></NumberFormat>
                                </telerik:RadNumericTextBox>
                                kbps
                            </td>
                        </tr>
                        
                    </table>
                    <asp:Table ID="TableTerminals" runat="server" class="table table-hover table-bordered">
                    </asp:Table>
                    <table align="center">
                        <tr>
                            <td style="text-align: center" colspan="2">
                                <asp:Label ID="LabelTerminals" runat="server" Visible="false"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right">
                                <asp:Button ID="ButtonUpdateView" runat="server" Text="Reset Priorities" OnClick="ButtonUpdateView_Click" Visible="false" />
                            </td>
                            <td style="text-align: left">
                                <asp:Button ID="ButtonSaveChanges" runat="server" Text="Save Changes" OnClick="ButtonSaveChanges_Click" Visible="false" ToolTip="Click Update view first" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="traffic" class="tab-pane fade">
                    <div class="col-xs-3" style="float: left;">
                        <asp:DropDownList ID="trafficPeriodSelect" class="form-control" runat="server" OnSelectedIndexChanged="trafficPeriodSelected" AutoPostBack="true">
                            <asp:ListItem Enabled="true" Text="Today" Value="0"></asp:ListItem>
                            <asp:ListItem Text="Last week" Value="7"></asp:ListItem>
                            <asp:ListItem Text="Last month" Value="30"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div id='chart_container_traffic' class="graphChart" runat="server"></div>
                    <div runat="server" id="trafficLoadingContainer" style="display: none;">
                        <button class="btn btn-lg btn-info disabled" style="margin-top: 200px; margin-left: 120px;" disabled="disabled"><span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span> Loading...</button>
                    </div>
                    <div style="text-align: center">
                        <br />
                        <br />
                        <br />
                        <asp:Label ID="labelTrafficEmpty" runat="server" Style="display: none;"></asp:Label>
                    </div>
                    <asp:Button runat="server" ID="trafficButton" Text="" Style="display: none;" OnClick="trafficPeriodSelected" />
                </div>
                <div id="volume" class="tab-pane fade">
                    <div class="col-xs-3" style="float: left;">
                        <asp:DropDownList ID="periodSelect" class="form-control" runat="server" OnSelectedIndexChanged="periodSelected" AutoPostBack="true">
                            <asp:ListItem Enabled="true" Text="Last month" Value="30"></asp:ListItem>
                            <asp:ListItem Text="Last 2 months" Value="60"></asp:ListItem>
                            <asp:ListItem Text="Last 6 months" Value="180"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div id='chart_container' class="graphChart" runat="server"></div>
                    <div runat="server" id="volumeLoadingContainer" style="display: none;">
                        <button class="btn btn-lg btn-info disabled" style="margin-top: 200px; margin-left: 120px;" disabled="disabled"><span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span> Loading...</button>
                    </div>
                    <div style="text-align: center">
                        <br />
                        <br />
                        <br />
                               <asp:Label ID="labelVolumeEmpty" runat="server" Visible="false"></asp:Label>
                    </div>
                    <asp:Button runat="server" ID="volumeButton" Text="" Style="display: none;" OnClick="periodSelected" />
                </div>
              <div id="trafficClassification" class="tab-pane fade">
                    <div class="col-sm-5" id="inputDateContainer" style="float: left;">
                        <div class="input-daterange input-group">
                            <input type="text" class="input-sm form-control" runat="server" id="classificationDateStart" />
                            <span class="input-group-addon">to</span>
                            <input type="text" class="input-sm form-control" runat="server" id="classificationDateEnd" />
                        </div>
                    </div>
                    <div>
                        <asp:Button ID="trafficClassificationDateButton" runat="server" class="btn btn-success btn-sm" OnClick="trafficClassificationDateChange" type="button" Text="Show" />
                    </div>
                    <div id='chart_container_classification' class="graphChart" runat="server"></div>
                    <div runat="server" id="tcLoadingContainer" style="display: none;">
                        <button class="btn btn-lg btn-info disabled" style="margin-top: 200px; margin-left: 0px;" disabled="disabled"><span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span> Loading...</button>
                    </div>
                    <div style="text-align: center">
                        <br />
                        <br />
                        <br />
                        <asp:Label ID="labelTrafficClassificationEmpty" runat="server" Style="display: none;"></asp:Label>
                    </div>
                </div>
                  
                <div id="realtime" class="tab-pane fade">
                    <div id='chart_container_realtime'></div>
                    <iframe id="iframeVNOgraph" runat="server" src="http://nms.satadsl.net:82/new/VNOGraph.php" width="100%" height="450" visible="true"></iframe>
                </div>
            </div>
            </div>
            <asp:HiddenField ID="hidTAB" runat="server" Value="terminals" />
        </form>
    </div>
</body>
</html>
