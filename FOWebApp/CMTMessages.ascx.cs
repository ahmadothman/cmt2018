﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using Telerik.Web.UI;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Web.UI.DataVisualization.Charting;
using System.Globalization;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using FOWebApp.net.nimera.cmt.BOMonitorControlWSRef;
using FOWebApp.net.nimera.cmt.BOCMTMessageControllerWSRef;


namespace FOWebApp
{
    public partial class CMTMessages : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Get user ID + role and load the messages
            BOCMTMessageControllerWS boMessageControl = new BOCMTMessageControllerWS();
            BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();
            MembershipUser myObject = Membership.GetUser();
            string userId = myObject.ProviderUserKey.ToString();
            CMTMessage[] messages;

            if (Roles.IsUserInRole("VNO"))
            {
                int distributorId = Convert.ToInt32(boAccountingControl.GetDistributorForUser(userId).Id);
                messages = boMessageControl.GetCMTMessagesForUser(distributorId.ToString(), "VNOs");
            }
            else if (Roles.IsUserInRole("Distributor") || Roles.IsUserInRole("DistributorReadOnly"))
            {
                int distributorId = Convert.ToInt32(boAccountingControl.GetDistributorForUser(userId).Id);
                messages = boMessageControl.GetCMTMessagesForUser(distributorId.ToString(), "distributors");
            }
            else if (Roles.IsUserInRole("Customer") || Roles.IsUserInRole("Organization"))
            {
                int customerId = Convert.ToInt32(boAccountingControl.GetOrganizationForUser(userId).Id);
                messages = boMessageControl.GetCMTMessagesForUser(customerId.ToString(), "customers");
            }
            else if (Roles.IsUserInRole("EndUser"))
            {
                int endUserId = Convert.ToInt32(boAccountingControl.GetEndUser(userId).Id);
                messages = boMessageControl.GetCMTMessagesForUser(endUserId.ToString(), "endusers");
            }
            else
            {
                messages = boMessageControl.GetAllCMTMessages();
            }

            RadGridMessages.DataSource = messages;
            RadGridMessages.DataBind();

        }

        protected void RadGridMessages_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridItemCollection selectedItems = RadGridMessages.SelectedItems;

            if (selectedItems.Count > 0)
            {
                GridItem selectedItem = selectedItems[0];
                CMTMessage message = (CMTMessage)selectedItem.DataItem;
                string id = message.MessageId.ToString();
            }
        }

        protected void RadGridMessages_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem dataItem = (GridDataItem)e.Item;

                if (dataItem["Release"].Text.Equals("True"))
                {
                    dataItem["Release"].Text = "Yes";
                    dataItem["Id"].ForeColor = Color.Red;
                }
                else
                {
                    dataItem["Release"].Text = "No";
                    dataItem["Id"].ForeColor = Color.DarkGreen;
                }
            }
        }

        protected void RadWindowManager1_Disposed(object sender, EventArgs e)
        {
        }

        protected void RadWindowManager1_Unload(object sender, EventArgs e)
        {
        }

        protected void RadGridMessages_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            this.Session["_messageList_PageSize"] = e.NewPageSize;
        }

        protected void RadGridMessages_ItemCommand(object sender, GridCommandEventArgs e)
        {
        }
    }
}