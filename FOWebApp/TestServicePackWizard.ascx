﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TestServicePackWizard.ascx.cs" Inherits="FOWebApp.TestServicePackWizard" %>
<h1>Test service pack wizard</h1>
<h2>This wizard is only for testing at the current moment.</h2>

<script language="javascript" type="text/javascript">

    function OnClientSelectedIndexChanged(sender, eventArgs) {
        var item = eventArgs.get_item();
    }

    function OnClientClick(sender, eventArgs) {
        var isp = $find("<%=RadComboBoxISPId.ClientID %>");
        var serviceLine = $find("<%= RadComboBoxServiceLine.ClientID %>");

        if (serviceLine.get_value() == 1)
            var oWnd = radopen("ServiceLines/SoHo.aspx?isp=" + isp.get_value() + "&SL=" + serviceLine.get_value(), "RadWindowNewSoHoSLA");
        if (serviceLine.get_value() == 2)
            var oWnd = radopen("ServiceLines/Business.aspx?isp=" + isp.get_value() + "&SL=" + serviceLine.get_value(), "RadWindowNewBusinessSLA");
        if (serviceLine.get_value() == 3)
            var oWnd = radopen("ServiceLines/Corporate.aspx?isp=" + isp.get_value() + "&SL=" + serviceLine.get_value(), "RadWindowNewCorporateSLA");
        if (serviceLine.get_value() == 4)
            var oWnd = radopen("ServiceLines/SpecialTailored.aspx?isp=" + isp.get_value() + "&SL=" + serviceLine.get_value(), "RadWindowNewSpecialTailoredSLA");
    }
</script>
<table cellpadding="2" cellspacing="5">
    <tr>
        <td>
            ISP:
        </td>
        <td class="auto-style2">
            <telerik:RadComboBox ID="RadComboBoxISPId" runat="server" Width="345px" OnClientSelectedIndexChanged="OnClientSelectedIndexChanged"
                ToolTip="If this SLA is managed by a Secondary ISP, please specify an Edge ISP" Skin="Metro"></telerik:RadComboBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="You must provide an ISP!"
                ControlToValidate="RadComboBoxISPId" SetFocusOnError="True"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>Service class:</td>
        <td class="auto-style2">
            <telerik:RadComboBox ID="RadComboBoxServiceLine" runat="server" Width="300px" Skin="Metro" ></telerik:RadComboBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="You must select a Service Line!"
                ControlToValidate="RadComboBoxServiceLine" SetFocusOnError="True"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td></td>
        <td style="text-align:right; width:500px;">
            <asp:Label runat="server" ID="lblServiceLineValue"></asp:Label>
        </td>
    </tr>
</table>
<asp:Button runat="server" id="btnNext" Text="Next" OnClientClick="OnClientClick(); return false;" />

<telerik:RadWindowManager ID="RadWindowManager1" runat="server" Skin="Metro" DestroyOnClose="True">
    <Windows>
        <telerik:RadWindow ID="RadWindowNewSoHoSLA" runat="server" Animation="None" Opacity="100"
            Skin="Metro" Title="New SoHo SLA" AutoSize="False" Width="850px" Height="700px"
            KeepInScreenBounds="True" EnableShadow="True" EnableAriaSupport="True" VisibleStatusbar="True"
            Modal="False" DestroyOnClose="False">
        </telerik:RadWindow>
    </Windows>
    <Windows>
        <telerik:RadWindow ID="RadWindowNewBusinessSLA" runat="server" Animation="None" Opacity="100"
            Skin="Metro" Title="New Business SLA" AutoSize="False" Width="850px" Height="700px"
            KeepInScreenBounds="True" EnableShadow="True" EnableAriaSupport="True" VisibleStatusbar="True"
            Modal="False" DestroyOnClose="False">
        </telerik:RadWindow>
    </Windows>
    <Windows>
        <telerik:RadWindow ID="RadWindowNewCorporateSLA" runat="server" Animation="None" Opacity="100"
            Skin="Metro" Title="New Corporate SLA" AutoSize="False" Width="850px" Height="700px"
            KeepInScreenBounds="True" EnableShadow="True" EnableAriaSupport="True" VisibleStatusbar="True"
            Modal="False" DestroyOnClose="False">
        </telerik:RadWindow>
    </Windows>
    <Windows>
        <telerik:RadWindow ID="RadWindowNewSpecialTailoredSLA" runat="server" Animation="None" Opacity="100"
            Skin="Metro" Title="New Special Tailored SLA" AutoSize="False" Width="850px" Height="700px"
            KeepInScreenBounds="True" EnableShadow="True" EnableAriaSupport="True" VisibleStatusbar="True"
            Modal="False" DestroyOnClose="False">
        </telerik:RadWindow>
    </Windows>
</telerik:RadWindowManager>