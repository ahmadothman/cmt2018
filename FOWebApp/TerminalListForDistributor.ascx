﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TerminalListForDistributor.ascx.cs"
    Inherits="FOWebApp.TerminalListForDistributor" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script type="text/javascript">
    function RowSelected(sender, eventArgs) {
        var grid = sender;
        var masterDataView = grid.get_masterTableView();
        var rowNum = eventArgs.get_itemIndexHierarchical();
        var row = masterDataView.get_dataItems()[rowNum];
        var macAddress = masterDataView.getCellByColumnUniqueName(row, "MacAddress").innerHTML;

        //Initialize and show the terminal details window
        var oWnd = radopen("TerminalDetailsForm.aspx?mac=" + macAddress, "RadWindowTerminalDetails");
    }
</script>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="RadGridTerminals">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadGridTerminals" />
                <telerik:AjaxUpdatedControl ControlID="RadWindowManager1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Metro"/>
<telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" Height="200px"
    Width="467px" HorizontalAlign="NotSet"
    LoadingPanelID="RadAjaxLoadingPanel1">
    <telerik:RadGrid ID="RadGridTerminals" runat="server" AllowSorting="True"
        AutoGenerateColumns="False" CellSpacing="0" GridLines="None" Skin="Metro"
        ShowStatusBar="True" Width="992px" AllowFilteringByColumn="True"
        OnItemDataBound="RadGridTerminals_ItemDataBound"
        OnPageSizeChanged="RadGridTerminals_PageSizeChanged"
        PageSize="50" OnItemCommand="RadGridTerminals_ItemCommand">
        <GroupingSettings CaseSensitive="false" />
        <ClientSettings EnableRowHoverStyle="true">
            <Selecting AllowRowSelect="True" />
            <ClientEvents OnRowSelected="RowSelected"/>
            <Resizing AllowColumnResize="true" AllowResizeToFit="true" />
        </ClientSettings>
        <MasterTableView CommandItemDisplay="Top" CommandItemSettings-ShowExportToExcelButton="False"
            CommandItemSettings-ShowExportToCsvButton="False" RowIndicatorColumn-Visible="True" RowIndicatorColumn-Resizable="True" RowIndicatorColumn-AndCurrentFilterFunction="Contains" CommandItemSettings-ShowAddNewRecordButton="False" ShowFooter="False">
            <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
            <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
            </RowIndicatorColumn>
            <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
            </ExpandCollapseColumn>
            <Columns>
                <telerik:GridBoundColumn DataField="FullName"
                    FilterControlAltText="Filter FullNameColumn column" HeaderText="Name"
                    UniqueName="FullName" ShowFilterIcon="False" AutoPostBackOnFilter="True">
                    <HeaderStyle Width="300" />
                    <ItemStyle Wrap="True" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="MacAddress" FilterControlAltText="Filter MacAddressColumn column"
                    HeaderText="MAC Address" MaxLength="12" ReadOnly="True" Resizable="true"
                    UniqueName="MacAddress" CurrentFilterFunction="Contains"
                    ShowFilterIcon="False" AutoPostBackOnFilter="True">
                    <HeaderStyle Width="130" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="SitId" FilterControlAltText="Filter SitIdColumn column"
                    HeaderText="Site ID" UniqueName="SitId" ShowFilterIcon="False"
                    AllowFiltering="False">
                    <HeaderStyle Width="70" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="IpAddress" FilterControlAltText="Filter IpAddressColumn column"
                    HeaderText="IP Address" ReadOnly="True" UniqueName="IpAddress"
                    ShowFilterIcon="False" AutoPostBackOnFilter="True">
                    <HeaderStyle Width="130" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="SlaId" FilterControlAltText="Filter SLA column"
                    HeaderText="SLA" ReadOnly="True" UniqueName="ServiceColumn" ShowFilterIcon="False" AutoPostBackOnFilter="True">
                    <HeaderStyle Width="120" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn AllowSorting="True" DataField="AdmStatus"
                    FilterControlAltText="Filter AdmStatusColumn column" CurrentFilterFunction="EqualTo"
                    HeaderText="Status" ReadOnly="True" UniqueName="AdmStatus"
                    ShowFilterIcon="False" AutoPostBackOnFilter="True" AllowFiltering="True"
                    ItemStyle-Wrap="False">
                    <FilterTemplate>
                        <telerik:RadComboBox ID="RadComboBox1" DataTextField="AdmStatus"
                            DataValueField="AdmStatus" AppendDataBoundItems="true"
                            SelectedValue='<%# ((GridItem)Container).OwnerTableView.GetColumn("AdmStatus").CurrentFilterValue %>'
                            runat="server" OnClientSelectedIndexChanged="SelectedIndexChanged" Skin="Metro">
                            <Items>
                                <telerik:RadComboBoxItem Text="" />
                                <telerik:RadComboBoxItem Text="Locked" Value="1"/>
                                <telerik:RadComboBoxItem Text="Unlocked" Value="2" />
                                <telerik:RadComboBoxItem Text="Request" Value="5" />
                            </Items>
                        </telerik:RadComboBox>
                        <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
                            <script type="text/javascript">
                                function SelectedIndexChanged(sender, args) {
                                    var tableView = $find("<%# ((GridItem)Container).OwnerTableView.ClientID %>");
                                    tableView.filter("AdmStatus", args.get_item().get_value(), "EqualTo");
                                }
                            </script>
                        </telerik:RadScriptBlock>
                    </FilterTemplate>
                    <HeaderStyle Width="120" />
                    <ItemStyle Wrap="False" />
                </telerik:GridBoundColumn>
                <telerik:GridCalculatedColumn DataFields="SitId" FilterControlAltText="Filter FUPResetDayColumn column"
                    HeaderText="FUP Reset Day" UniqueName="FUPResetDay" ShowFilterIcon="False" Expression="{0}%28+1"
                    AllowFiltering="False">
                    <HeaderStyle Width="95px" />
                </telerik:GridCalculatedColumn>
                <telerik:GridDateTimeColumn DataField="StartDate" FilterControlAltText="Filter StartDateColumn column"
                    HeaderText="Start Date" ReadOnly="True" UniqueName="StartDate" PickerType="DatePicker"
                    ItemStyle-Wrap="False" DataFormatString="{0:dd/MM/yyyy}">
                    <HeaderStyle Width="100" />
                    <ItemStyle Wrap="False" />
                </telerik:GridDateTimeColumn>
                <telerik:GridDateTimeColumn AllowSorting="True" DataField="ExpiryDate" DataType="System.DateTime"
                    FilterControlAltText="Filter ExpiryDateColumn column" HeaderText="Expiry Date"
                    ReadOnly="True" UniqueName="ExpiryDate" ItemStyle-Wrap="False" PickerType="DatePicker"
                    AutoPostBackOnFilter="True" DataFormatString="{0:dd/MM/yyyy}">
                    <HeaderStyle Width="100" />
                    <ItemStyle Wrap="False" />
                </telerik:GridDateTimeColumn>
                <telerik:GridBoundColumn DataField="CNo"
                    FilterControlAltText="Filter CNo column" HeaderText="C/No"
                    UniqueName="CNo" Visible="True">
                    <HeaderStyle Width="70" />
                </telerik:GridBoundColumn>
                <telerik:GridTemplateColumn HeaderText="Redundant" UniqueName="RedundantSetup">
                    <ItemTemplate>
                        <%--<asp:HiddenField runat="server" ID="RedundantSetupValue" Value="<%#Eval("RedundantSetup") %>" />--%>
                        <asp:Image runat="server" ID="RedundantSetupImage" ImageUrl="~/Images/satellite_dish_dual.png" ImageAlign="Middle" Visible="false" AlternateText='<%# Eval("RedundantSetup") %>' />
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
            </Columns>
            <EditFormSettings>
                <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                </EditColumn>
            </EditFormSettings>
        </MasterTableView>
        <FilterMenu EnableImageSprites="False">
        </FilterMenu>
        <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default">
        </HeaderContextMenu>
    </telerik:RadGrid>
    <p>
        Total number of terminals in the non-filtered list:
    <asp:Label ID="LabelNumRows" runat="server" Font-Bold="True"></asp:Label>
    </p>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server" Skin="Metro"
        DestroyOnClose="True">
        <Windows>
            <telerik:RadWindow ID="RadWindowTerminalDetails" runat="server"
                NavigateUrl="TerminalDetailsForm.aspx" Animation="Fade" Opacity="100" Skin="Metro"
                Title="Terminal Details" AutoSize="False" Width="1000px" Height="700px" KeepInScreenBounds="True" EnableShadow="True" EnableAriaSupport="True" VisibleStatusbar="True" Modal="False" DestroyOnClose="False">
            </telerik:RadWindow>
            <telerik:RadWindow ID="RadWindowModifyActivity" runat="server" Animation="None" Opacity="100"
                Skin="Metro" Title="Modify Activity" AutoSize="False" Width="850px" Height="655px"
                KeepInScreenBounds="True" EnableShadow="True" EnableAriaSupport="True" VisibleStatusbar="True"
                Modal="False" DestroyOnClose="False">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
</telerik:RadAjaxPanel>


