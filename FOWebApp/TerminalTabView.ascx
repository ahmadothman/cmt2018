﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TerminalTabView.ascx.cs" Inherits="FOWebApp.TerminalTabView" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<telerik:RadAjaxPanel ID="RadAjaxPanelTerminalTabView" runat="server" Height="200px" Width="600px">
<script type="text/javascript">
    function onTabSelecting(sender, args) {
        if (args.get_tab().get_pageViewID()) {
            args.get_tab().set_postBack(false);
            alert("set_postBack(false)");
        }
    }
 </script>
<telerik:RadTabStrip ID="RadTabStripTerminalView" runat="server" 
    MultiPageID="RadMultiPageTerminalView" Skin="Metro" 
    SelectedIndex="0" OnClientTabSelecting="onTabSelecting" 
    ontabclick="RadTabStripTerminalView_TabClick">
    <Tabs>
        <telerik:RadTab runat="server" Text="Terminal">
        </telerik:RadTab>
        <telerik:RadTab runat="server" Text="Status">
        </telerik:RadTab>
        <telerik:RadTab runat="server" Text="Consumption">
        </telerik:RadTab>
        <telerik:RadTab runat="server" Text="Management" Visible="false">
        </telerik:RadTab>
    </Tabs>
</telerik:RadTabStrip>
<telerik:RadMultiPage ID="RadMultiPageTerminalView" runat="server" 
    BorderColor="#CCCCCC" BorderStyle="Solid" BorderWidth="1px" Height="400px" 
    Width="600px" ScrollBars="Auto" SelectedIndex="0" OnPageViewCreated="RadMultiPageTerminalView_PageViewCreated">
</telerik:RadMultiPage>
<asp:Label ID="LabelMacAddress" runat="server" Visible="true"></asp:Label>
</telerik:RadAjaxPanel>
