﻿using System;
using Hangfire.Logging;
using FOWebApp.net.nimera.cmt.BOLogControlWSRef;
using Hangfire.Server;
using Hangfire.Console;
using Hangfire;
using System.Threading;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using System.Collections.Generic;
using System.Linq;
using FOWebApp.net.nimera.cmt.BOMonitorControlWSRef;
using System.Web.Security;
using FOWebApp.Data;
using System.Configuration;

namespace FOWebApp.Jobs
{
    public class HangfireLogger : ILog
    {
        /// <summary>
        /// Log a message the specified log level.
        /// </summary>
        /// <param name="logLevel">The log level.</param>
        /// <param name="messageFunc">The message function.</param>
        /// <param name="exception">An optional exception.</param>
        /// <returns>true if the message was logged. Otherwise false.</returns>
        /// <remarks>
        /// Note to implementers: the message func should not be called if the loglevel is not enabled
        /// so as not to incur performance penalties.
        ///
        /// To check IsEnabled call Log with only LogLevel and check the return value, no event will be written
        /// </remarks>
        public bool Log(LogLevel logLevel, Func<string> messageFunc, Exception exception = null)
        {
            try
            {
                if (exception != null)
                {
                    BOLogControlWS boLogControlWS = new BOLogControlWS();
                    CmtApplicationException cmtEx = new CmtApplicationException();
                    cmtEx.ExceptionDateTime = DateTime.Now;
                    cmtEx.ExceptionDesc = exception.Message;
                    cmtEx.UserDescription = "Hangfire";
                    cmtEx.StateInformation = logLevel.ToString();
                    cmtEx.ExceptionLevel = (short)logLevel;
                    boLogControlWS.LogApplicationException(cmtEx);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }

    public class HangfireLogProvider : ILogProvider
    {
        public ILog GetLogger(string name)
        {
            return new HangfireLogger();
        }
    }

    public static class RecurrentTasks
    {
        public static void SuspendInactiveSohoTerminals(PerformContext context, IJobCancellationToken cancellationToken)
        {
            Guid userId = new Guid(Membership.GetUser("Automatic").ProviderUserKey.ToString());
            var progressBar = context.WriteProgressBar();
            context.WriteLine("Suspension of unused SOHO terminals started");
            var boMonitorControl = new BOMonitorControlWS();
            var boAccountingControl = new BOAccountingControlWS();
            var boLogControl = new BOLogControlWS();
            var terminals = new List<Terminal>();

            var terminalGroup = terminals.GroupBy(x => x.DistributorId);
            foreach (var distributorTerminals in terminalGroup.WithProgress(progressBar, terminalGroup.Count()))
            {
                var successfulSuspensions = new List<Terminal>();
                var distributor = boAccountingControl.GetDistributor((int)distributorTerminals.Key);
                context.WriteLine("Processing terminals from distributor " + distributor.FullName);
                foreach (var terminal in distributorTerminals)
                {
                    if (!String.IsNullOrEmpty(terminal.ExtFullName) && !String.IsNullOrEmpty(terminal.MacAddress) && terminal.IspId >= 0)
                    {
                        if (boMonitorControl.TerminalSuspend(terminal.ExtFullName, terminal.MacAddress, terminal.IspId, userId))
                        {
                            context.WriteLine(terminal.MacAddress + " suspension request sent to hub");
                            successfulSuspensions.Add(terminal);
                        }
                        else
                        {
                            context.WriteLine(terminal.MacAddress + " suspension failed");
                        }
                    }
                    else
                    {
                        context.WriteLine("unable to lock " + terminal.MacAddress + " because terminal has incomplete information");
                    }
                    cancellationToken.ThrowIfCancellationRequested();
                }
                if (successfulSuspensions.Count > 0)
                {
                    EmailMessage email = new EmailMessage(EmailMessage.MessageType.SohoTerminalSuspension, successfulSuspensions);
                    email.recipients.Add(distributor.Email);
                    BackgroundJob.Schedule(() => boLogControl.SendMail(email.body, email.subject, email.recipients.ToArray()), TimeSpan.FromMinutes(Convert.ToInt32(ConfigurationManager.AppSettings["SohoTerminalSuspensionEmailDelay"]))); 
                }
            }
        }

        public static void TestJob(PerformContext context, IJobCancellationToken cancellationToken)
        {
            var progressBar = context.WriteProgressBar();
            context.WriteLine("Test job started");
            var elements = Enumerable.Range(1, 60).ToList();
            foreach (var element in elements.WithProgress(progressBar))
            {
                context.WriteLine("Processing dummy element nr " + element);
                cancellationToken.ThrowIfCancellationRequested();
                Thread.Sleep(TimeSpan.FromSeconds(1));
            }
        }
    }
}