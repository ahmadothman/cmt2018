﻿using DotNet.Highcharts;
using DotNet.Highcharts.Enums;
using DotNet.Highcharts.Helpers;
using DotNet.Highcharts.Options;
using FOWebApp.Data;
using FOWebApp.net.nimera.cmt.BOAccountingControlWSRef;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace FOWebApp
{
    public partial class TerminalAlarms : System.Web.UI.UserControl
    {
        private readonly string connectionString = ConfigurationManager.ConnectionStrings["MySQLMonitorDb"].ConnectionString;
        private readonly string graphiteEndpoint = ConfigurationManager.AppSettings["GraphiteTerminalAlarms"];

        protected void Page_Load(object sender, EventArgs e)
        {
            var boAccountingControl = new BOAccountingControlWS();
            if (Roles.IsUserInRole("Distributor") || Roles.IsUserInRole("Organization"))
            {
                MembershipUser user = Membership.GetUser();
                string userId = user.ProviderUserKey.ToString();
                if (Roles.IsUserInRole("Distributor"))
                {
                    var terminals = boAccountingControl.GetTerminalsByDistributor(userId);
                    GetTerminalAlarms(terminals);
                }
                else
                {
                    Organization organization = boAccountingControl.GetOrganizationForUser(userId);
                    var terminals = boAccountingControl.GetTerminalsByOrganization(Convert.ToInt32(organization.Id));
                    GetTerminalAlarms(terminals);
                }
            }
            else if (Roles.IsUserInRole("NOC Administrator"))
            {
                Distributor[] distList = boAccountingControl.GetDistributorsAndVNOs().Where(x => x.Inactive == false).ToArray();
                distributorSelect.Visible = true;
                distributorSelect.Items.Insert(0, new ListItem(string.Empty, string.Empty));
                foreach (var dist in distList)
                {
                    distributorSelect.Items.Add(new ListItem(dist.FullName, Convert.ToString(dist.Id)));
                }
            }
        }

        protected void Distributor_SelectedIndexChanged(object sender, EventArgs e)
        {
            var distributorId = distributorSelect.SelectedValue;
            var boAccountingControl = new BOAccountingControlWS();
            var terminals = boAccountingControl.GetTerminalsByDistributorId(Convert.ToInt32(distributorId));
            GetTerminalAlarms(terminals);
        }

        private void GetTerminalAlarms()
        {
            var javaScriptSerializer = new JavaScriptSerializer();
            javaScriptSerializer.MaxJsonLength = Int32.MaxValue;
            string jsonGraphData;
            using (WebClient client = new WebClient())
            {
                jsonGraphData = client.DownloadString(CreateGraphiteUrl());
            }
            var graphData = javaScriptSerializer.Deserialize<List<Graph>>(jsonGraphData);
            var terminals = GetAllMonitoredTerminals();
            BuildAlarmTable(terminals, graphData);
            BuildColumnWithDrilldown(terminals, graphData);
        }

        private void GetTerminalAlarms(Terminal[] terminals)
        {
            var allMonitoredTerminals = GetAllMonitoredTerminals();
            var ipAddresses = terminals.Where(x => !String.IsNullOrEmpty(x.IpAddress)).Select(x => x.IpAddress.Trim());
            var monitoredTerminals = allMonitoredTerminals.Where(x => ipAddresses.Contains(x.Value.Item2)).ToDictionary(x => x.Key, x => x.Value);
            if (monitoredTerminals.Count() > 0)
            {
                var javaScriptSerializer = new JavaScriptSerializer();
                javaScriptSerializer.MaxJsonLength = Int32.MaxValue;
                string jsonGraphData;
                using (WebClient client = new WebClient())
                {
                    jsonGraphData = client.DownloadString(CreateGraphiteUrl(monitoredTerminals.Keys.ToList()));
                }
                var graphData = javaScriptSerializer.Deserialize<List<Graph>>(jsonGraphData);
                BuildAlarmTable(monitoredTerminals, graphData);
                BuildColumnWithDrilldown(monitoredTerminals, graphData);
            }
        }

        private void BuildColumnWithDrilldown(Dictionary<int, Tuple<string, string>> terminals, List<Graph> graphData)
        {
            List<string> categories = new List<string>(terminals.Values.Select(x => Regex.Replace(x.Item1, @"[\d-]|SN", string.Empty).Trim()));
            const string NAME = "Terminals";
            List<Point> points = new List<Point>();
            List<int> highestPing = new List<int>();
            foreach (var terminal in terminals)
            {
                if (graphData.FirstOrDefault(x => x.target.Contains(terminal.Key.ToString())) != null)
                {
                    var point = new Point();
                    point.Color = System.Drawing.Color.FromName("colors[0]");
                    var pingsRTT = graphData.FirstOrDefault(x => x.target.Contains(terminal.Key.ToString())).datapoints.Select(x => Convert.ToDouble(x[0]));
                    highestPing.Add(Convert.ToInt32(pingsRTT.Max()));
                    decimal successfulPings = pingsRTT.Where(x => x != 0).Count();
                    point.Y = Convert.ToDouble(decimal.Round((successfulPings / pingsRTT.Count()) * 100, 2, MidpointRounding.AwayFromZero));
                    var drilldown = new Drilldown();
                    drilldown.Color = System.Drawing.Color.FromName("colors[0]");
                    drilldown.Name = Regex.Replace(terminal.Value.Item1, @"[\d-]|SN", string.Empty).Trim();
                    drilldown.Categories = graphData.FirstOrDefault(x => x.target.Contains(terminal.Key.ToString())).datapoints.Select(x => Convert.ToDouble(x[1]).UnixTimeStampToDateTime().ToString("HH:mm")).ToArray();
                    List<string> drilldownDataFormatted = graphData.FirstOrDefault(x => x.target.Contains(terminal.Key.ToString())).datapoints.Select(x => Convert.ToString(x[0]).Before(".")).ToList();
                    List<object> drilldownData = new List<object>(drilldownDataFormatted);
                    drilldown.Data = new DotNet.Highcharts.Helpers.Data(drilldownData.ToArray());
                    point.Drilldown = drilldown;
                    points.Add(point);
                }
            }
            DotNet.Highcharts.Helpers.Data data = new DotNet.Highcharts.Helpers.Data(points.ToArray());
            Highcharts chart = new Highcharts("chart");
            chart.InitChart(new Chart { DefaultSeriesType = ChartTypes.Column });
            chart.SetTitle(new Title { Text = "Terminal availability (last hour)" });
            chart.SetSubtitle(new Subtitle { Text = "Click the column to view detailed info" });
            XAxis xAxis = new XAxis();
            xAxis.Categories = categories.ToArray();
            xAxis.Labels = new XAxisLabels()
            {
                Rotation = -45,
                Align = HorizontalAligns.Right,
                Style = "fontSize: '8px',fontFamily: 'Verdana, sans-serif'"
            };
            chart.SetXAxis(xAxis);
            chart.SetYAxis(new YAxis { Title = new YAxisTitle { Text = "Availability %" }, Min = 0, Max = 100 });
            chart.SetLegend(new Legend { Enabled = false });
            chart.SetTooltip(new Tooltip { Formatter = "TooltipFormatter" });
            chart.SetPlotOptions(new PlotOptions
            {
                Column = new PlotOptionsColumn
                {
                    Cursor = Cursors.Pointer,
                    Point = new PlotOptionsColumnPoint { Events = new PlotOptionsColumnPointEvents { Click = "ColumnPointClick" } },
                    DataLabels = new PlotOptionsColumnDataLabels
                    {
                        Enabled = true,
                        Color = System.Drawing.ColorTranslator.FromHtml("#808080"),
                        Formatter = "function() { return this.y + '%'; }",
                        Style = "fontWeight: 'bold'"
                    }
                },
                Line = new PlotOptionsLine
                {
                    Cursor = Cursors.Pointer,
                    Marker = new PlotOptionsLineMarker
                    {
                        Symbol = "diamond"
                    },
                    Point = new PlotOptionsLinePoint { Events = new PlotOptionsLinePointEvents { Click = "ColumnPointClick" } },
                    DataLabels = new PlotOptionsLineDataLabels
                    {
                        Enabled = true,
                        Color = System.Drawing.ColorTranslator.FromHtml("#808080"),
                        Formatter = "function() { return this.y; }",
                        Style = "fontWeight: 'bold'"
                    }
                }
            });
            chart.SetSeries(new Series
            {
                Name = "Terminals",
                Data = data,
                Color = System.Drawing.Color.White
            });
            chart.SetExporting(new Exporting { Enabled = true });
            chart.AddJavascripFunction(
                    "TooltipFormatter",
                    @"var point = this.point, s = this.x +':<b>'+ this.y;
                      if (point.drilldown) {
                        s += ' %</b><br/>Click to view '+ point.category +' last hour';
                      } else {
                        s += ' ms</b><br/>Click to return';
                      }
                      return s;"
                );
            chart.AddJavascripFunction(
                    "ColumnPointClick",
                    @"var drilldown = this.drilldown;
                      if (drilldown) { // drill down
                        chart.yAxis[0].setExtremes(0,parseInt(drilldownMax));
                        chart.yAxis[0].axisTitle.attr({
                            text: 'RTT ms'
                        });
                        setChart(drilldown.name, drilldown.categories, drilldown.data.data, drilldown.color, 'line');
                      } else { // restore
                        chart.yAxis[0].axisTitle.attr({
                            text: 'Availability %'
                        });
                        chart.yAxis[0].setExtremes(0,100);
                        setChart(name, categories, data.data, 'white', 'column');
                      }"
                );
            chart.AddJavascripFunction(
                    "setChart",
                    @"chart.xAxis[0].setCategories(categories);
                      chart.series[0].remove();
                      chart.addSeries({
                         type: type,
                         name: name,
                         data: data,
                         color: color || 'white'
                      });",
                    "name", "categories", "data", "color", "type"
                );
            chart.AddJavascripVariable("colors", "Highcharts.getOptions().colors");
            chart.AddJavascripVariable("name", "'{0}'".FormatWith(NAME));
            chart.AddJavascripVariable("drilldownMax", "'{0}'".FormatWith(highestPing.Max().ToString()));
            chart.AddJavascripVariable("categories", JsonSerializer.Serialize(categories.ToArray()));
            chart.AddJavascripVariable("data", JsonSerializer.Serialize(data));
            ScriptManager.RegisterStartupScript(Page, typeof(Page), "chartKey", chart.ToJavaScriptString(), true);
        }

        private Dictionary<int, Tuple<string, string>> GetAllMonitoredTerminals()
        {
            Dictionary<int, Tuple<string, string>> terminals = new Dictionary<int, Tuple<string, string>>();
            using (var conn = new MySqlConnection(connectionString))
            using (var cmd = conn.CreateCommand())
            {
                conn.Open();
                cmd.CommandText = "SELECT terminalId, terminalName, terminalIp FROM terminal LEFT JOIN company ON terminal.companyId = company.companyId WHERE alarmService = '1'";
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        terminals.Add(reader.GetInt32(0), new Tuple<string, string>(reader.GetString(1), reader.GetString(2)));
                    }
                }
            }
            return terminals;
        }

        private void BuildAlarmTable(Dictionary<int, Tuple<string, string>> terminals, List<Graph> graphData)
        {
            if (terminals.Count > 0)
            {
                string htmlTable = @"<table class=""table table-hover table-bordered""><tr class=""info""><th>Terminal</th><th>IP</th><th>Availability (last 5 min)</th></tr><tr>";
                foreach (var terminal in terminals)
                {
                    if (graphData.FirstOrDefault(x => x.target.Contains(terminal.Key.ToString())) != null)
                    {
                        var datapoints = graphData.FirstOrDefault(x => x.target.Contains(terminal.Key.ToString())).datapoints.TakeLast(5).Where(x => Convert.ToInt32(x[0]) != 0);
                        int successfulPings = datapoints.Count();
                        string classColor = "";
                        if (graphData.FirstOrDefault(x => x.target.Contains(terminal.Key.ToString())).datapoints.TakeLast(3).Where(x => Convert.ToInt32(x[0]) != 0).Count() == 0)
                        {
                            //No response in the last 5 times
                            classColor = @"class=""danger""";
                        }
                        else if (successfulPings == 5)
                        {
                            //100% response
                            classColor = @"class=""success""";
                        }
                        else if (successfulPings >= 3)
                        {
                            //More than 50% response in the last 5 min
                            classColor = @"class=""warning""";
                        }
                        else if (successfulPings < 3)
                        {
                            //Less than 50% response in the last 5 min
                            classColor = @"class=""danger""";
                        }
                        htmlTable += "<td>" + terminal.Value.Item1 + "</td><td>" + terminal.Value.Item2 + "</td></td><td " + classColor + ">" + (successfulPings * 100)/5  + "%</td></tr>";
                    }
                }
                htmlTable += "</table>";
                table_container.InnerHtml = htmlTable;
            }
        }

        private string CreateGraphiteUrl()
        {
            return graphiteEndpoint + "/render?target=terminal.*.ping&from=-1h&format=json";
        }

        private string CreateGraphiteUrl(List<int> terminalIds)
        {
            if (terminalIds.Count > 0)
            {
                string url = graphiteEndpoint + "/render?target=terminal." + terminalIds[0] + ".ping";
                for (int i = 1; i < terminalIds.Count; i++)
                {
                    url += "&target=terminal." + terminalIds[i] + ".ping";
                }
                url += "&from=-1h&format=json";
                return url;
            }
            else
            {
                throw new Exception("No terminals found for distributor or organization");
            }
        }
    }
}