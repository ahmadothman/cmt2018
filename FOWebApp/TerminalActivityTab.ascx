﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TerminalActivityTab.ascx.cs" Inherits="FOWebApp.TerminalActivityTab" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:HiddenField ID="HiddenFieldIspId" runat="server" Value="0" />
<asp:HiddenField ID="HiddenFieldSitId" runat="server" Value="0" />
<asp:HiddenField ID="HiddenFieldHeight" runat="server" Value="0" />
<script type="text/javascript">
    function RowSelected(sender, eventArgs) {
        var grid = sender;
        var masterDataView = grid.get_masterTableView();
        var rowNum = eventArgs.get_itemIndexHierarchical();
        var row = masterDataView.get_dataItems()[rowNum];
        var activityId = masterDataView.getCellByColumnUniqueName(row, "Id").innerHTML;

        var oManager = GetRadWindow().get_windowManager();
        setTimeout(function () {
            oManager.open("NOCSA/ModifyActivityForm.aspx?Id=" + activityId, "RadWindowModifyActivity");
        }, 0);
    }

    function GetRadWindow() {
        var oWindow = null;
        if (window.radWindow) {
             oWindow = window.radWindow;
        }
        else if (window.frameElement.radWindow) {
            oWindow = window.frameElement.radWindow;
        }
        return oWindow;
    }
</script>
<telerik:RadGrid ID="RadGridActivityList" runat="server" 
    AutoGenerateColumns="False" CellSpacing="0" GridLines="None" 
    Skin="Metro" onneeddatasource="RadGridActivityList_NeedDataSource" 
    Width="100%" onitemcommand="RadGridActivityList_ItemCommand" OnItemDataBound="RadGridActivityList_ItemDataBound">
    <ExportSettings IgnorePaging="True" OpenInNewWindow="True">
    </ExportSettings>
    <ClientSettings EnableRowHoverStyle="true"> 
        <Selecting AllowRowSelect="True" />
        <Scrolling AllowScroll="True" UseStaticHeaders="True" />
        <ClientEvents OnRowSelected="RowSelected" />
    </ClientSettings>
<MasterTableView>
<CommandItemSettings ExportToPdfText="Export to PDF" ShowAddNewRecordButton="False" 
        ShowRefreshButton="False"></CommandItemSettings>

<RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
<HeaderStyle Width="20px"></HeaderStyle>
</RowIndicatorColumn>

<ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
<HeaderStyle Width="20px"></HeaderStyle>
</ExpandCollapseColumn>

    <Columns>
        <telerik:GridBoundColumn DataField="Id" FilterControlAltText="Filter Id column" HeaderText="Id" UniqueName="Id">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="ActionDate" 
            FilterControlAltText="Filter ActionDate column" HeaderText="Date" 
            UniqueName="ActionDate" MaxLength="20">
            <ItemStyle Width="20px" />
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="Action" 
            FilterControlAltText="Filter Activity column" HeaderText="Activity" 
            UniqueName="Activity">
            <ItemStyle Width="50px" />
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="SlaName" 
            FilterControlAltText="Filter SlaName column" HeaderText="SLA Name" 
            UniqueName="SlaName">
            <ItemStyle Width="50px" />
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="NewSlaName" 
            FilterControlAltText="Filter NewSlaName column" HeaderText="New Sla" 
            UniqueName="NewSlaName">
            <ItemStyle Width="50px" />
        </telerik:GridBoundColumn>
         <telerik:GridBoundColumn DataField="UserName" 
            FilterControlAltText="Filter NewSlaName column" HeaderText="User" 
            UniqueName="UserName">
             <ItemStyle Width="50px" />
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="Comment" 
            FilterControlAltText="Filter Comment column" HeaderText="Comment" 
            UniqueName="Comment">
             <ItemStyle Width="100%" />
        </telerik:GridBoundColumn>
    </Columns>

<EditFormSettings>
<EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
</EditFormSettings>
</MasterTableView>

<FilterMenu EnableImageSprites="False"></FilterMenu>

<HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default"></HeaderContextMenu>
</telerik:RadGrid>
