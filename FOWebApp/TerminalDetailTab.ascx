﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TerminalDetailTab.ascx.cs" Inherits="FOWebApp.TerminalDetailTab" %>

<asp:Panel ID="PanelError" runat="server" Visible="false">
    <table class="termDetails" style="border: none; border-spacing: 5px; border-collapse: separate; width: 550px;">
        <tr>
            <td style="width: 40%;">Error message:</td>
            <td style="width: 60%; font-weight: bold; color: #ff0000;">
                <asp:Literal ID="LiteralError" runat="server" /></td>
        </tr>
    </table>
</asp:Panel>

<asp:Panel ID="PanelTerminalDetails" runat="server">
</asp:Panel>
<div>
    <fieldset>
        <legend>Terminal Contact</legend>
        <table class="termDetails" style="border: none; border-spacing: 5px; border-collapse: separate; width: 550px;">
            <tr>
                <td>Address Line 1</td>
                <td>
                    <asp:Label runat="server" ID="LabelAddressLine1"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Address Line 2</td>
                <td>
                    <asp:Label runat="server" ID="LabelAddressLine2"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Location</td>
                <td>
                    <asp:Label runat="server" ID="LabelLocation"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Postal Code</td>
                <td>
                    <asp:Label runat="server" ID="LabelPostalCode"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Country</td>
                <td>
                    <asp:Label runat="server" ID="LabelCountry"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Phone</td>
                <td>
                    <asp:Label runat="server" ID="LabelPhone"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Fax</td>
                <td>
                    <asp:Label runat="server" ID="LabelFax"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>E-mail</td>
                <td>
                    <asp:Label runat="server" ID="LabelEmail"></asp:Label>
                </td>
            </tr>
        </table>
    </fieldset>
</div>
