﻿using System;
using System.IO;
using TermImport.BOAccountingControlWSRef;

namespace TermImport
{
    /// <summary>
    ///   Imports terminal information into the CMTData database. This tool is a temporary
    ///   solution until the terminal information is created by means of the CMT.
    /// </summary>
    internal class TermImport
    {
        private static void Main(string[] args)
        {
            //var termImport = new TermImport();
            //termImport.mainBody(args);
            
            //SlaFix fix =  new SlaFix();
            FullNamefix fix = new FullNamefix();
            fix.Process();
            
        }

        /// <summary>
        ///   Main body of the application
        /// </summary>
        /// <param name = "args">Application arguments</param>
        private void mainBody(string[] args)
        {
            var inputFileName = @"C:\Storage\Cybernited\Projects\SatAdsl\sitids.csv"; //The input csv file
            var dbHelper = new DBHelper();

            var line = 1;
            var accountingController = new BOAccountingControlWS();

            if (args.Length != 0)
            {
                //Argument is the file name to process
                inputFileName = args[0];
            }

            try
            {
                //Read-in the input file and process the file line by line
                var reader = new StreamReader(inputFileName);
                reader.ReadLine(); //First line is header
                while (!reader.EndOfStream)
                {
                    var inputLine = reader.ReadLine();
                    line++;

                    var inputLineTokens = inputLine.Split(';');

                    //Parse the Identifier field
                    var namingFields = inputLineTokens[2].Split('|');

                    // create our isp 
                    var i = new Isp
                    {
                        Id = 112,
                        Email = "team@satadsl.net"
                    };

                    //Get the Distributor Id from the distributor name
                    var distId = dbHelper.GetDistributorIdFromName(namingFields[0].Trim());

                    Distributor d;
                    if (distId.HasValue)
                    {
                        d = accountingController.GetDistributor(distId.Value);
                        d.Isp = i;
                        accountingController.UpdateDistributor(d);
                    }
                    else
                    {
                        d = new Distributor {FullName = namingFields[0].Trim()};
                        i = accountingController.GetISP(112);
                        d.Isp = i;
                        accountingController.UpdateDistributor(d);
                    }

                    //Add an Organization (Customer)
                    var orgName = namingFields.Length == 2 ? namingFields[1].Trim() : namingFields[0].Trim();

                    var orgId = dbHelper.GetOrganizationFromName(orgName);
                    if (orgId == -1)
                    {
                        //Add a new organization
                        var o = new Organization
                                    {
                                        FullName = namingFields[1].Trim(),
                                        Distributor = d
                                    };
                        accountingController.UpdateOrganization(o);
                    }

                    //Finally update the terminal information
                    if (!dbHelper.TerminalExists(inputLineTokens[3]))
                    {
                        var t = new Terminal
                                    {
                                        MacAddress = inputLineTokens[3],
                                        IspId = 112,
                                        SitId = Int32.Parse(inputLineTokens[1]),
                                        SlaId = dbHelper.GetSlaIdFromName(inputLineTokens[9]),
                                        IpAddress = inputLineTokens[4]
                                    };

                        switch (inputLineTokens[7])
                        {
                            case "Locked" :
                                t.AdmStatus = 1;
                                break;
                            case "Unlocked":
                                t.AdmStatus = 2;
                                break;
                            case "Suspended":
                                t.AdmStatus = 3;
                                break;
                        }
                        t.Blade = Int32.Parse(inputLineTokens[10]);

                        //Get organization id
                        orgId = dbHelper.GetOrganizationFromName(orgName);
                        t.OrgId = orgId;

                        if (namingFields.Length == 3)
                            t.Remarks = namingFields[2];

                        t.StartDate = DateTime.Now;
                        t.FirstActivationDate = DateTime.Now;

                        accountingController.UpdateTerminal(t);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error at " + line + " : " + ex.Message);
            }
        }
    }
}
