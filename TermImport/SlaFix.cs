﻿using System;
using System.IO;
using BOControllerLibrary.Controllers;
using BOControllerLibrary.Model.Terminal;
using TermImport.BOAccountingControlWSRef;
using Address = BOControllerLibrary.Model.Address;
using Terminal = BOControllerLibrary.Model.Terminal.Terminal;

namespace TermImport
{
    public class SlaFix
    {
        public void Process()
        {
            var controller = new BOAccountingController(DBHelper.ConnectionString, DBHelper.DataProvider);
            var inputFileName = @"C:\Storage\Cybernited\Projects\SatAdsl\sitids2.csv"; //The input csv file
            var dbHelper = new DBHelper();

            var line = 1;

            var i = new Isp
                        {
                            Id = 112,
                            Email = "team@satadsl.net"
                        };

            string forCatch = "";
            try
            {
                //Read-in the input file and process the file line by line
                var reader = new StreamReader(inputFileName);
                reader.ReadLine(); //First line is header
                while (!reader.EndOfStream)
                {
                    var inputLine = reader.ReadLine();
                    line++;

                    forCatch = inputLine;

                    var inputLineTokens = inputLine.Split(';');
                    var namingFields = inputLineTokens[2].Split('|');

                    // distributor fields
                    var distributorName = namingFields[0].Trim();
                    var orgName = namingFields.Length == 2 ? namingFields[1].Trim() : namingFields[0].Trim();
                    string fullname = inputLineTokens[2];
                    var macAddress = inputLineTokens[3];
                    int? sitid = PrimitiveExensions.ParseSafe(inputLineTokens[1]);
                    if (!sitid.HasValue)
                        Console.WriteLine("Could not parse sitid");
                    var slaName = inputLineTokens[9];
                    var slaId = DBHelper.GetSlaIdForSlaName(slaName);

                    if (!slaId.HasValue || slaId.Value == 0)
                    {
                        using (StreamWriter writer = new StreamWriter(@"c:\temp\log.txt")) {
                            writer.WriteLine(string.Format("SLA not found;{0};{1}", macAddress, slaName));
                            Console.WriteLine("SLA not found: " + slaName);
                            continue;
                        }
                    }

                    //if (!slaId.HasValue)
                    //{
                    //    controller.UpdateServicePack(new ServiceLevel()
                    //                                     {
                                                             
                    //                                     });
                    //}

                    var ipaddress = inputLineTokens[4];
                    var admStatus = inputLineTokens[7];
                    var admStatusId = GetAdminStatus(admStatus);

                    if (!admStatusId.HasValue)
                    {
                        Console.WriteLine(admStatus);
                    }
                    //string blade = inputLineTokens[10];

                    var remarks = namingFields.Length == 3 ? namingFields[2] : "";

                    //Get the Distributor Id from the distributor name
                    var distId = dbHelper.GetDistributorIdFromName(distributorName);
                    var orgid = dbHelper.GetOrganizationFromName(orgName);
                    var terminal = controller.GetTerminalDetailsByMAC(macAddress);

                    if (terminal != null)
                    {
                        // terminal exists, now find the SLA id in the service pack table. 
                        //terminal.SlaId = slaId;

                        if (terminal.FirstActivationDate.HasValue && terminal.FirstActivationDate.Value.Date == DateTime.Now.Date)
                        {
                            Console.WriteLine();
                            terminal.FullName = fullname;
                            terminal.Remarks = remarks;
                            controller.UpdateTerminal(terminal);
                        }
                        else
                        {
                            // huh? :p.
                            Console.WriteLine();
                        }

                        //DBHelper.UpdateTerminalWithSlaId(macAddress, slaId.Value);
                        // update the terminal -- maybe update the field only?

                    }
                    else
                    {
                        // terminal does not exist, we'll create a new terminal and insert it. The distributor id is 216
                        var t = new Terminal()
                                         {
                                             Address = new Address(),
                                             MacAddress = macAddress,
                                             IspId = 112,
                                             SitId = sitid.Value,
                                             DistributorId = 216, // satadsl
                                             SlaId = slaId,
                                             IpAddress = ipaddress,
                                             AdmStatus = admStatusId,
                                             OrgId = orgid,
                                             Remarks = remarks,
                                             StartDate = DateTime.Now,
                                             FirstActivationDate = DateTime.Now
                                         };
                        controller.UpdateTerminal(t);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
                Console.WriteLine(forCatch);
            }
        }


        private static int? GetAdminStatus(string admStatus)
        {
            string newStatus = admStatus.Trim().ToLower();
            switch (newStatus)
            {
                case "locked":
                    return 1;
                case "unlocked":
                    return 2;
                case "suspended":
                    return 3;
                
            }
            return null;
        }
    }

    static class PrimitiveExensions
    {
        public static int? ParseSafe(string data)
        {
            int i;
            if (int.TryParse(data, out i))
                return i;
            return null;
        }
    }
}