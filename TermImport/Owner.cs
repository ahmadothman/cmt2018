﻿namespace TermImport
{
    /// <summary>
    ///   Contains the owner fields parsed from the Name field
    /// </summary>
    internal class Owner
    {
        protected Owner()
        {
        }

        public Owner(string name)
        {
            ParseNameField(name);
        }

        /// <summary>
        ///   The distributor who sold the terminal to the organization
        /// </summary>
        public string Distributor { get; set; }

        /// <summary>
        ///   The organization owning the terminal. This might also be an individual
        /// </summary>
        public string Organization { get; set; }

        /// <summary>
        ///   An individual user of a terminal associated with an organization.
        /// </summary>
        public string EndUser { get; set; }

        /// <summary>
        ///   Parses the name field into its individual parts
        /// </summary>
        /// <param name = "name">Field name</param>
        private void ParseNameField(string name)
        {
            var nameTokens = name.Split('|');

            if (nameTokens.Length >= 1)
                Distributor = nameTokens[0].Trim().ToUpper();

            if (nameTokens.Length >= 2)
                Organization = nameTokens[1].Trim().ToUpper();

            if (nameTokens.Length >= 3)
                EndUser = nameTokens[2].Trim().ToUpper();

        }
    }
}
