﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace TermImport
{
    //The DBHelper class contains database related methods which are not implemented
    //by the Accounting Controller
    internal class DBHelper
    {
        private const string DistrIdQueryCmd = "SELECT Id FROM Distributors WHERE FullName = @FullName";
        private const string OrganizationQueryCmd = "SELECT Id FROM Organizations WHERE FullName = @FullName";
        private const string TerminalQueryCmd = "SELECT MacAddress FROM Terminals WHERE MacAddress = @MacADdress";
        private const string SlaQueryCmd = "SELECT SlaID FROM ServicePacks WHERE SlaCommonName = @SlaName";

        //internal const string ConnectionString = "data source=cmtdata.nimera.net; Database=CMTData; User Id=SatFinAfrica; Password=Lagos#1302";
        internal const string ConnectionString = @"data source=.\SQLEXPRESS;Database=CMT; User Id=SatFinAfrica; Password=Lagos#1302";
        internal const string DataProvider = "System.Data.SqlClient";

        /// <summary>
        ///   Returns the identifier of the distributor or -1 if the distributor does not exist yet.
        /// </summary>
        /// <param name = "distributorName"></param>
        /// <returns></returns>
        public int? GetDistributorIdFromName(string distributorName)
        {
            using (var con = new SqlConnection(ConnectionString))
            {
                con.Open();
                try
                {
                    using (var command = new SqlCommand(DistrIdQueryCmd, con))
                    {
                        command.Parameters.Add("@FullName", SqlDbType.NVarChar);
                        command.Parameters["@FullName"].Value = distributorName;
                        var reader = command.ExecuteReader();

                        if (reader.Read())
                        {
                            return reader.GetInt32(0);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("SQL Read error: " + ex.Message);
                }
            }
            return null;
        }

        /// <summary>
        /// </summary>
        /// <param name = "organizationName"></param>
        /// <returns></returns>
        public int? GetOrganizationFromName(string organizationName)
        {
            using (var con = new SqlConnection(ConnectionString))
            {
                con.Open();
                try
                {
                    using (var command = new SqlCommand(OrganizationQueryCmd, con))
                    {
                        command.Parameters.Add("@FullName", SqlDbType.NVarChar);
                        command.Parameters["@FullName"].Value = organizationName;
                        var reader = command.ExecuteReader();

                        if (reader.Read())
                        {
                            return reader.GetInt32(0);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("SQL Read error: " + ex.Message);
                }
            }
            return null;
        }

        public bool TerminalExists(string macAddress)
        {
            using (var con = new SqlConnection(ConnectionString))
            {
                con.Open();
                try
                {
                    using (var command = new SqlCommand(TerminalQueryCmd, con))
                    {
                        command.Parameters.Add("@MacAddress", SqlDbType.NVarChar);
                        command.Parameters["@MacAddress"].Value = macAddress;
                        var reader = command.ExecuteReader();

                        return reader.HasRows;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("SQL Read error: " + ex.Message);
                    return false;
                }
            }
        }

        /// <summary>
        /// Modified this code: previous default sla id was 1109, but changed to return int? instead.
        /// </summary>
        /// <param name="slaName"></param>
        /// <returns></returns>
        public int? GetSlaIdFromName(string slaName)
        {
            using (var con = new SqlConnection(ConnectionString))
            {
                con.Open();
                try
                {
                    using (var command = new SqlCommand(SlaQueryCmd, con))
                    {
                        command.Parameters.Add("@SlaName", SqlDbType.NVarChar);
                        command.Parameters["@SlaName"].Value = slaName;
                        var reader = command.ExecuteReader();

                        return reader.Read() ? reader.GetInt32(0) : (int?)null;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("SQL Read error: " + ex.Message);
                }
            }
            return null;
        }

        public static void UpdateTerminalWithSlaId (string macAddress, int slaId)
        {
            using (var db = new PetaPoco.Database(ConnectionString, DataProvider))
            {
                try
                {
                    const string sql = "UPDATE terminals set slaid = @slaId where macaddress = @macAddress";
                    db.Execute(sql, new
                                            {
                                                slaId,
                                                macAddress
                                            });
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.WriteLine(ex.StackTrace);
                }
            }
        }
        public static int? GetSlaIdForSlaName (string slaName)
        {
            const string sql = "SELECT SlaID FROM ServicePacks WHERE SlaName = @slaName";

            using (var db = new PetaPoco.Database(ConnectionString, DataProvider))
            {
                return db.Fetch<int>(sql, new {slaName}).FirstOrDefault();
            }
        }
    }
}
