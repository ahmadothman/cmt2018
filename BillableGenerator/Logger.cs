﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BillableGenerator
{
    /// <summary>
    /// Simple logger methods
    /// </summary>
    public class Logger
    {
        public static void Log(string msg, Boolean boLog = false)
        {
            Console.WriteLine("[" + DateTime.Now + "] BillableGenerator - " + msg);
        }
    }
}
