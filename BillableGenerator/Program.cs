﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BillableGenerator.BOAccountingControllerRef;
using BillableGenerator.InvoiceControllerRef;

namespace BillableGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            Program prg = new Program();
            prg.mainBody(args);
        }

        private void mainBody(string[] args)
        {
            InvoiceControllerClient invoiceControl = new InvoiceControllerClient();
            BOAccountingControlWS boAccountingControl = new BOAccountingControlWS();
            ServiceLevel[] slas = boAccountingControl.GetServicePacks();
            List<billable> bills = new List<billable>();
            billable bill;
            //for (int i = 3155; i < 3573; i++)
            //{
            //    bill = invoiceControl.GetBillable(i);
            //    bills.Add(bill);
            //}
            
            int bID = 3574;
            foreach (ServiceLevel sl in slas)
            {
                //foreach (billable b in bills)
                //{
                //    if (b.Description == sl.SlaName)
                //    {
                //        sl.BillableId = b.Bid;
                //        boAccountingControl.UpdateServicePack(sl);
                //        Logger.Log("Set billable ID for SLA " + sl.SlaId);
                //        break;
                //    }
                //}

                bill = new billable();
                bill.AccountId = 1358;
                bill.Deleted = false;
                bill.Description = sl.SlaName;
                if (sl.ServicePackAmtEUR != null)
                {
                    bill.PriceEU = (decimal)sl.ServicePackAmtEUR;
                }
                else
                {
                    bill.PriceEU = 0;
                }
                if (sl.ServicePackAmt != null)
                {
                    bill.PriceUSD = (decimal)sl.ServicePackAmt;
                }
                else
                {
                    bill.PriceUSD = 0;
                }
                bill.LastUpdate = DateTime.Now;
                bill.Bid = bID;
                try
                {
                    invoiceControl.AddBillable(bill);
                    Logger.Log("Created billable for SLA " + sl.SlaId);
                    sl.BillableId = bID;
                    boAccountingControl.UpdateServicePack(sl);
                    bID++;
                }
                catch (Exception ex)
                {
                    Logger.Log("Failed to create billable for SLA " + sl.SlaId);
                }
            }
        }
    }
}
